import os

#   constants

ROOT = os.path.dirname(os.path.realpath(__file__))

#   user settings

# installation path of Second Galaxy
SG_PATH = "D:\\Program Files (x86)\\Steam\\steamapps\\common\\Second Galaxy"

# destination of the asset extractor
DATA_PATH = os.path.join(ROOT, 'assets')

# destination of the configdata extractor
CONFIG_DATA_PATH = os.path.join(ROOT, 'configdata')

# folder which holds the proto protocols for python
PROTO_PATH =  os.path.join(ROOT, 'protos')
PROTOPY_PATH =  os.path.join(ROOT, 'protos_py')


#   constants 2

# contains the packed assets
ASSET_PATH = os.path.join(SG_PATH,"SecondGalaxy_Data","StreamingAssets","ExportAssetBundle")