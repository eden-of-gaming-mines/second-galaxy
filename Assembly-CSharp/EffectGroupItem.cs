﻿using System;
using UnityEngine;

[Serializable]
public class EffectGroupItem
{
    public Transform EffectParentTransfrom;
    public float DelayTime;
}

