﻿namespace IL
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class ObjectArrayPools
    {
        private static IPool current = new QueuePool();

        [MethodImpl(0x8000)]
        public static void Delete(object[] objs)
        {
        }

        public static object[] New(int length) => 
            current.New(length);

        public static void SetCurrent(IPool pool)
        {
            current = pool;
        }

        public static void SetDefault()
        {
            current = new DefaultPool();
        }

        private class DefaultPool : ObjectArrayPools.IPool
        {
            void ObjectArrayPools.IPool.Delete(object[] objs)
            {
            }

            object[] ObjectArrayPools.IPool.New(int length) => 
                new object[length];
        }

        public interface IPool
        {
            void Delete(object[] objs);
            object[] New(int length);
        }

        private class ListPool : ObjectArrayPools.IPool
        {
            private List<object[]> objs = new List<object[]>();

            void ObjectArrayPools.IPool.Delete(object[] v)
            {
                int count = this.objs.Count;
                for (int i = 0; i < count; i++)
                {
                    if (this.objs[i] == null)
                    {
                        this.objs[i] = v;
                        return;
                    }
                }
                this.objs.Add(v);
            }

            object[] ObjectArrayPools.IPool.New(int length)
            {
                int count = this.objs.Count;
                object[] objArray = null;
                for (int i = 0; i < this.objs.Count; i++)
                {
                    objArray = this.objs[i];
                    if ((objArray != null) && (objArray.Length == length))
                    {
                        this.objs[i] = null;
                        return objArray;
                    }
                }
                return new object[length];
            }
        }

        private class QueuePool : ObjectArrayPools.IPool
        {
            private object[] Pools = new object[20];

            void ObjectArrayPools.IPool.Delete(object[] objs)
            {
                int index = objs.Length - 1;
                object obj2 = this.Pools[index];
                if (obj2 == null)
                {
                    this.Pools[index] = objs;
                }
                else
                {
                    Queue<object[]> queue = null;
                    if (obj2 is object[])
                    {
                        queue = new Queue<object[]>();
                        queue.Enqueue((object[]) obj2);
                        this.Pools[index] = queue;
                    }
                    else
                    {
                        queue = obj2 as Queue<object[]>;
                    }
                    queue.Enqueue(objs);
                }
            }

            object[] ObjectArrayPools.IPool.New(int length)
            {
                int index = length - 1;
                object obj2 = this.Pools[index];
                if (obj2 == null)
                {
                    return new object[length];
                }
                if (!(obj2 is object[]))
                {
                    Queue<object[]> queue = obj2 as Queue<object[]>;
                    return ((queue.Count == 0) ? new object[length] : queue.Dequeue());
                }
                object[] objArray = (object[]) obj2;
                this.Pools[index] = null;
                return objArray;
            }
        }
    }
}

