﻿namespace IL
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct Objects : IDisposable
    {
        public object[] objs;
        [MethodImpl(0x8000)]
        public Objects(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9, object p10)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9, object p10, object p11)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9, object p10, object p11, object p12)
        {
        }

        [MethodImpl(0x8000)]
        public Objects(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9, object p10, object p11, object p12, object p13, object p14)
        {
        }

        void IDisposable.Dispose()
        {
            ObjectArrayPools.Delete(this.objs);
        }
    }
}

