﻿namespace IL
{
    using System;

    public class RefOutParam<T>
    {
        public T value;

        public RefOutParam(T value)
        {
            this.value = value;
        }
    }
}

