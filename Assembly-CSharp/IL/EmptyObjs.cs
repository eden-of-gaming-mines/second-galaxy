﻿namespace IL
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, Size=1)]
    public struct EmptyObjs : IDisposable
    {
        private static object[] Emptys;
        public object[] objs =>
            Emptys;
        void IDisposable.Dispose()
        {
        }

        static EmptyObjs()
        {
            Emptys = new object[0];
        }
    }
}

