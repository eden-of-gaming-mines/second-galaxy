﻿namespace IL
{
    using Assets.XIL;
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.ConfigData;
    using BlackJack.BJFramework.Runtime.Hotfix;
    using BlackJack.BJFramework.Runtime.Log;
    using BlackJack.BJFramework.Runtime.PlayerContext;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.BJFramework.Utils;
    using BlackJack.ConfigData;
    using BlackJack.InvalidCharacterFilter;
    using BlackJack.LibClient;
    using BlackJack.PathFinding;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.GalaxyEditor;
    using BlackJack.ProjectX.LibClient;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using BlackJack.ProjectX.Runtime;
    using BlackJack.ProjectX.Runtime.SDK;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using BlackJack.ProjectX.Runtime.SolarSystem.Weapon;
    using BlackJack.ProjectX.Runtime.UI;
    using BlackJack.ProjectX.Runtime.UI.CrossScroll;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using BlackJack.ProjectX.Static;
    using BlackJack.PropertiesCalculater;
    using BlackJack.ToolUtil;
    using BlackJack.Utils;
    using Dest.Math;
    using ProtoBuf;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityEngine.Audio;
    using UnityEngine.Playables;
    using UnityEngine.SceneManagement;
    using UnityEngine.Timeline;
    using UnityEngine.UI;
    using Vectrosity;

    public class DelegateBridge
    {
        private MethodInfo methodInfo;
        private List<object> paramList;
        private object result;

        public DelegateBridge(MethodInfo methodInfo)
        {
            this.methodInfo = methodInfo;
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp10(object p0, bool p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime __Gen_Delegate_Imp100(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1000(object p0, WeaponCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSizeType __Gen_Delegate_Imp1001(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1002(object p0, ShipSizeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public RankType __Gen_Delegate_Imp1003(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1004(object p0, RankType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SubRankType __Gen_Delegate_Imp1005(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1006(object p0, SubRankType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<float> __Gen_Delegate_Imp1007(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<CommonPropertyInfo> __Gen_Delegate_Imp1008(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemObtainSourceType> __Gen_Delegate_Imp1009(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp101(object p0, object p1, int p2, object p3, object p4, int p5, int p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemType __Gen_Delegate_Imp1010(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1011(object p0, NpcShopItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemType __Gen_Delegate_Imp1012(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1013(object p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> __Gen_Delegate_Imp1014(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestRewardInfo> __Gen_Delegate_Imp1015(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<BattlePassRewardEffect> __Gen_Delegate_Imp1016(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BlackMarketShopItemType __Gen_Delegate_Imp1017(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1018(object p0, BlackMarketShopItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public BlackMarketShopPayType __Gen_Delegate_Imp1019(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp102(object p0, object p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1020(object p0, BlackMarketShopPayType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ResourceShopItemCategory __Gen_Delegate_Imp1021(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1022(object p0, ResourceShopItemCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public BlueprintCategory __Gen_Delegate_Imp1023(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1024(object p0, BlueprintCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestAcceptCondInfo> __Gen_Delegate_Imp1025(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BranchStoryCategory __Gen_Delegate_Imp1026(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1027(object p0, BranchStoryCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public BufType __Gen_Delegate_Imp1028(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1029(object p0, BufType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp103(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipFightBufType __Gen_Delegate_Imp1030(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1031(object p0, ShipFightBufType p1)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp1032(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigParams> __Gen_Delegate_Imp1033(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PropertyCategory __Gen_Delegate_Imp1034(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1035(object p0, PropertyCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChipType __Gen_Delegate_Imp1036(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1037(object p0, ChipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProfessionType __Gen_Delegate_Imp1038(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1039(object p0, ProfessionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp104(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<CharProfessionInfoInitPropertiesBasic> __Gen_Delegate_Imp1040(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<CharProfessionInfoAutoAddProperties> __Gen_Delegate_Imp1041(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LanguageType __Gen_Delegate_Imp1042(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1043(object p0, LanguageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction __Gen_Delegate_Imp1044(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1045(object p0, GrandFaction p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcFunctionType __Gen_Delegate_Imp1046(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1047(object p0, NpcFunctionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigableConstId __Gen_Delegate_Imp1048(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1049(object p0, ConfigableConstId p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp105(object p0, int p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConstellationType __Gen_Delegate_Imp1050(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1051(object p0, ConstellationType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CrackBoxType __Gen_Delegate_Imp1052(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1053(object p0, CrackBoxType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CurrencyType __Gen_Delegate_Imp1054(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1055(object p0, CurrencyType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LoginRewardInfo> __Gen_Delegate_Imp1056(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SignalType> __Gen_Delegate_Imp1057(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipType __Gen_Delegate_Imp1058(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1059(object p0, ShipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp106(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public EquipCategory __Gen_Delegate_Imp1060(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1061(object p0, EquipCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public DevelopmentNodeType __Gen_Delegate_Imp1062(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1063(object p0, DevelopmentNodeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public DevelopmentNodeIndex __Gen_Delegate_Imp1064(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1065(object p0, DevelopmentNodeIndex p1)
        {
        }

        [MethodImpl(0x8000)]
        public DevelopmentType __Gen_Delegate_Imp1066(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1067(object p0, DevelopmentType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<DevelopmentCostItemList> __Gen_Delegate_Imp1068(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> __Gen_Delegate_Imp1069(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp107(object p0, ushort p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<IntWeightListItem> __Gen_Delegate_Imp1070(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ResultRange> __Gen_Delegate_Imp1071(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DeviceRatingLevel __Gen_Delegate_Imp1072(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1073(object p0, DeviceRatingLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CommonFormatStringParamType> __Gen_Delegate_Imp1074(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<FormatStringParamInfo> __Gen_Delegate_Imp1075(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcEmotionType __Gen_Delegate_Imp1076(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1077(object p0, NpcEmotionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public DisplayEffectCreateRateType __Gen_Delegate_Imp1078(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1079(object p0, DisplayEffectCreateRateType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ushort __Gen_Delegate_Imp108(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DroneType __Gen_Delegate_Imp1080(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1081(object p0, DroneType p1)
        {
        }

        [MethodImpl(0x8000)]
        public DynamicSceneLocationType __Gen_Delegate_Imp1082(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1083(object p0, DynamicSceneLocationType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<DynamicSceneLocationInfoOrbitRadiusRange> __Gen_Delegate_Imp1084(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipEquipSlotType __Gen_Delegate_Imp1085(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1086(object p0, ShipEquipSlotType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ParamInfo> __Gen_Delegate_Imp1087(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditLevel __Gen_Delegate_Imp1088(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1089(object p0, FactionCreditLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp109(object p0, StreamingContext p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CreditRewardItemInfo> __Gen_Delegate_Imp1090(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<FactionCreditModifyRelativeInfoFactionFactorList> __Gen_Delegate_Imp1091(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<CreditRewardInfo> __Gen_Delegate_Imp1092(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GEBrushType __Gen_Delegate_Imp1093(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1094(object p0, GEBrushType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GEBrushInfoGradualLegendColorElementList> __Gen_Delegate_Imp1095(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GEBrushInfoBrushColorElementList> __Gen_Delegate_Imp1096(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GEConfigableConstId __Gen_Delegate_Imp1097(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1098(object p0, GEConfigableConstId p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GEGrayColorToSolarSystemTemplateMapInfoLevel> __Gen_Delegate_Imp1099(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp11(object p0, BlackJack.BJFramework.Runtime.AudioType p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp110(object p0, int p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<GEMoonCountDataInfoOrbitRadiusRange> __Gen_Delegate_Imp1100(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GEPlanetType __Gen_Delegate_Imp1101(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1102(object p0, GEPlanetType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GEPlanetDataInfoOrbitRadiusRange> __Gen_Delegate_Imp1103(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GEAtmosphereDensity __Gen_Delegate_Imp1104(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1105(object p0, GEAtmosphereDensity p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GEPlanetDataInfoDisplayOnStarMapOrbitSpace> __Gen_Delegate_Imp1106(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GERockyPlanetTemperatureInfoOrbitRadiusRange> __Gen_Delegate_Imp1107(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GEStringTableId __Gen_Delegate_Imp1108(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1109(object p0, GEStringTableId p1)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerContextStateMachine.StateCode __Gen_Delegate_Imp111(object p0, PlayerContextStateMachine.EventCode p1, PlayerContextStateMachine.StateCode p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public GiftPackageCategory __Gen_Delegate_Imp1110(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1111(object p0, GiftPackageCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public GiftPackageTheme __Gen_Delegate_Imp1112(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1113(object p0, GiftPackageTheme p1)
        {
        }

        [MethodImpl(0x8000)]
        public GiftPackageBuyCountLimitType __Gen_Delegate_Imp1114(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1115(object p0, GiftPackageBuyCountLimitType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GrandFactionInfoNPCCaptainPropertiesBasicMultiList> __Gen_Delegate_Imp1116(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildActionAdditionRewardInfoRewardItemList> __Gen_Delegate_Imp1117(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> __Gen_Delegate_Imp1118(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBenefitsType __Gen_Delegate_Imp1119(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerContextStateMachine.StateCode __Gen_Delegate_Imp112(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1120(object p0, GuildBenefitsType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBenefitsSubType __Gen_Delegate_Imp1121(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1122(object p0, GuildBenefitsSubType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingType __Gen_Delegate_Imp1123(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1124(object p0, GuildBuildingType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfIdLevelInfo> __Gen_Delegate_Imp1125(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBuildingLevelDetailInfoRecycleItem> __Gen_Delegate_Imp1126(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBuildingLevelDetailInfoGuildLogicEffectList> __Gen_Delegate_Imp1127(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCurrencyLogType __Gen_Delegate_Imp1128(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1129(object p0, GuildCurrencyLogType p1)
        {
        }

        [MethodImpl(0x8000)]
        public UnityEngine.Object __Gen_Delegate_Imp113(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCurrencyLogFilterType __Gen_Delegate_Imp1130(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1131(object p0, GuildCurrencyLogFilterType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildEvaluateScoreInfoBuildingScoreList> __Gen_Delegate_Imp1132(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipOptLogType __Gen_Delegate_Imp1133(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1134(object p0, GuildFlagShipOptLogType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildPermission> __Gen_Delegate_Imp1135(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMomentsType __Gen_Delegate_Imp1136(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1137(object p0, GuildMomentsType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildProduceCategory __Gen_Delegate_Imp1138(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1139(object p0, GuildProduceCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabControllerNextUpdateExecutor __Gen_Delegate_Imp114(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildProduceType __Gen_Delegate_Imp1140(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1141(object p0, GuildProduceType p1)
        {
        }

        [MethodImpl(0x8000)]
        public PropertyViewType __Gen_Delegate_Imp1142(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1143(object p0, PropertyViewType p1)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesALG __Gen_Delegate_Imp1144(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GenderType __Gen_Delegate_Imp1145(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1146(object p0, GenderType p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcPersonalityType __Gen_Delegate_Imp1147(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1148(object p0, NpcPersonalityType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<HelloDailogInfo> __Gen_Delegate_Imp1149(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UnityEngine.Object __Gen_Delegate_Imp115(object p0, object p1, object p2, AutoBindAttribute.InitState p3, object p4, object p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public List<IslandStarGroupInfoIslandInfoList> __Gen_Delegate_Imp1150(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<IslandTypeInfoSolarSystemGuildMinrealOutputInfoList> __Gen_Delegate_Imp1151(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemDropInfoUIShowItemList> __Gen_Delegate_Imp1152(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ItemObtainSourceType __Gen_Delegate_Imp1153(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1154(object p0, ItemObtainSourceType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<IdLevelWeightItem> __Gen_Delegate_Imp1155(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicalSoundResTableID __Gen_Delegate_Imp1156(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1157(object p0, LogicalSoundResTableID p1)
        {
        }

        [MethodImpl(0x8000)]
        public MineralType __Gen_Delegate_Imp1158(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1159(object p0, MineralType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp116(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public MonthlyCardPaymentType __Gen_Delegate_Imp1160(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1161(object p0, MonthlyCardPaymentType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<MonthlyCardPriviledgeInfo> __Gen_Delegate_Imp1162(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MonthlyCardPriviledgeType __Gen_Delegate_Imp1163(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1164(object p0, MonthlyCardPriviledgeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public NormalItemType __Gen_Delegate_Imp1165(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1166(object p0, NormalItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public FeatsBookType __Gen_Delegate_Imp1167(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1168(object p0, FeatsBookType p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainFeatsType __Gen_Delegate_Imp1169(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabControllerBase __Gen_Delegate_Imp117(object p0, object p1, object p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1170(object p0, NpcCaptainFeatsType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipCompListConfInfo> __Gen_Delegate_Imp1171(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcInteractionTemplateContinuousEffectInfo> __Gen_Delegate_Imp1172(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcInteractionTemplateOnceEffectInfo> __Gen_Delegate_Imp1173(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShipFunctionType __Gen_Delegate_Imp1174(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1175(object p0, NpcShipFunctionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public HudDisplayStatus __Gen_Delegate_Imp1176(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1177(object p0, HudDisplayStatus p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcScriptOptType __Gen_Delegate_Imp1178(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1179(object p0, NpcScriptOptType p1)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabControllerBase __Gen_Delegate_Imp118(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcShipInfoShipTemplateInfo> __Gen_Delegate_Imp1180(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfHighSlotInfo> __Gen_Delegate_Imp1181(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemCategory __Gen_Delegate_Imp1182(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1183(object p0, NpcShopItemCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationRoomType __Gen_Delegate_Imp1184(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1185(object p0, SpaceStationRoomType p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcTalkerScriptType __Gen_Delegate_Imp1186(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1187(object p0, NpcTalkerScriptType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<HelloDialogIdByCompletedMainQuestInfo> __Gen_Delegate_Imp1188(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SubRankType> __Gen_Delegate_Imp1189(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp119(object p0, object p1, out UnityEngine.Object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GrandFaction> __Gen_Delegate_Imp1190(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProfessionType> __Gen_Delegate_Imp1191(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcFunctionType> __Gen_Delegate_Imp1192(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcPersonalityType> __Gen_Delegate_Imp1193(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MailDeleteType __Gen_Delegate_Imp1194(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1195(object p0, MailDeleteType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<PreDefineNpcCaptainStaticInfoInitFeats> __Gen_Delegate_Imp1196(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BlueprintType __Gen_Delegate_Imp1197(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1198(object p0, BlueprintType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestEnvirmentInstanceInfoSolarSystemIdList> __Gen_Delegate_Imp1199(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp12(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable __Gen_Delegate_Imp120(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestEnvirmentInstanceInfoNpcList> __Gen_Delegate_Imp1200(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestEnvirmentInstanceInfoCustomStringList> __Gen_Delegate_Imp1201(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestEnvirmentInstanceInfoMonsterSelectList> __Gen_Delegate_Imp1202(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestGrandFactionRewardInfoRewardList> __Gen_Delegate_Imp1203(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestType __Gen_Delegate_Imp1204(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1205(object p0, QuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestCompleteCondInfo> __Gen_Delegate_Imp1206(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestPostEventInfo> __Gen_Delegate_Imp1207(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DynamicSceneNearCelestialType __Gen_Delegate_Imp1208(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1209(object p0, DynamicSceneNearCelestialType p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable __Gen_Delegate_Imp121(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<RecommendItemInfo> __Gen_Delegate_Imp1210(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SceneCameraAnimationClipFilterType> __Gen_Delegate_Imp1211(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SceneType __Gen_Delegate_Imp1212(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1213(object p0, SceneType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SceneWingShipSetting __Gen_Delegate_Imp1214(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1215(object p0, SceneWingShipSetting p1)
        {
        }

        [MethodImpl(0x8000)]
        public SceneScreenEffectType __Gen_Delegate_Imp1216(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1217(object p0, SceneScreenEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CameraAnimationInfo> __Gen_Delegate_Imp1218(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<RationalEquipType> __Gen_Delegate_Imp1219(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable __Gen_Delegate_Imp122(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipSizeType> __Gen_Delegate_Imp1220(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public EquipType __Gen_Delegate_Imp1221(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1222(object p0, EquipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionType __Gen_Delegate_Imp1223(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1224(object p0, EquipFunctionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipNamedEffectType __Gen_Delegate_Imp1225(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1226(object p0, ShipNamedEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SuperEquipFunctionType __Gen_Delegate_Imp1227(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1228(object p0, SuperEquipFunctionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<SoundInfo> __Gen_Delegate_Imp1229(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp123(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public TacticalEquipFunctionType __Gen_Delegate_Imp1230(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1231(object p0, TacticalEquipFunctionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipTypeInfoShipTypeInvadeArroundDistanceList> __Gen_Delegate_Imp1232(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SignalType __Gen_Delegate_Imp1233(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1234(object p0, SignalType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<MonsterSelectInfo> __Gen_Delegate_Imp1235(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SignalInfoMonsterDropInfo> __Gen_Delegate_Imp1236(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CameraPlatformInitPosType __Gen_Delegate_Imp1237(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1238(object p0, CameraPlatformInitPosType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationType __Gen_Delegate_Imp1239(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabResourceContainerBase.AssetCacheItem __Gen_Delegate_Imp124(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1240(object p0, SpaceStationType p1)
        {
        }

        [MethodImpl(0x8000)]
        public StringTableId __Gen_Delegate_Imp1241(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1242(object p0, StringTableId p1)
        {
        }

        [MethodImpl(0x8000)]
        public SystemFuncDescType __Gen_Delegate_Imp1243(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1244(object p0, SystemFuncDescType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SystemMessageId __Gen_Delegate_Imp1245(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1246(object p0, SystemMessageId p1)
        {
        }

        [MethodImpl(0x8000)]
        public SystemMessageType __Gen_Delegate_Imp1247(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1248(object p0, SystemMessageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SystemPushId __Gen_Delegate_Imp1249(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp125(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1250(object p0, SystemPushId p1)
        {
        }

        [MethodImpl(0x8000)]
        public TechType __Gen_Delegate_Imp1251(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1252(object p0, TechType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<PropertyCategory> __Gen_Delegate_Imp1253(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<UnlockItemInfo> __Gen_Delegate_Imp1254(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<UserGuideConditionInfo> __Gen_Delegate_Imp1255(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ExtraUserGuideConditionInfo> __Gen_Delegate_Imp1256(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<UserGuideStepInfoPageList> __Gen_Delegate_Imp1257(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<int, int>> __Gen_Delegate_Imp1258(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideTriggerPoint __Gen_Delegate_Imp1259(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BundleData.SingleBundleData __Gen_Delegate_Imp126(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1260(object p0, UserGuideTriggerPoint p1)
        {
        }

        [MethodImpl(0x8000)]
        public WeaponType __Gen_Delegate_Imp1261(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1262(object p0, WeaponType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<SceneCountParamList> __Gen_Delegate_Imp1263(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestRewardType __Gen_Delegate_Imp1264(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1265(object p0, QuestRewardType p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcInteractionContinuousEffectType __Gen_Delegate_Imp1266(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1267(object p0, NpcInteractionContinuousEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcInteractionOnceEffectType __Gen_Delegate_Imp1268(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1269(object p0, NpcInteractionOnceEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp127(object p0, byte[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp1270(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestAcceptCondType __Gen_Delegate_Imp1271(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1272(object p0, QuestAcceptCondType p1)
        {
        }

        [MethodImpl(0x8000)]
        public QuestCompleteCondType __Gen_Delegate_Imp1273(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1274(object p0, QuestCompleteCondType p1)
        {
        }

        [MethodImpl(0x8000)]
        public QuestPostEventType __Gen_Delegate_Imp1275(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1276(object p0, QuestPostEventType p1)
        {
        }

        [MethodImpl(0x8000)]
        public MonsterSelsetType __Gen_Delegate_Imp1277(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1278(object p0, MonsterSelsetType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CostType __Gen_Delegate_Imp1279(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] __Gen_Delegate_Imp128(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1280(object p0, CostType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildLogicEffectType __Gen_Delegate_Imp1281(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1282(object p0, GuildLogicEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideConditionType __Gen_Delegate_Imp1283(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1284(object p0, UserGuideConditionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ActivityRewardType __Gen_Delegate_Imp1285(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1286(object p0, ActivityRewardType p1)
        {
        }

        [MethodImpl(0x8000)]
        public LoginRewardType __Gen_Delegate_Imp1287(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1288(object p0, LoginRewardType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBStarfieldsSimpleInfo> __Gen_Delegate_Imp1289(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ResourceManager __Gen_Delegate_Imp129()
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBLinkInfo> __Gen_Delegate_Imp1290(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PVector3D __Gen_Delegate_Imp1291(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBStargroupInfo> __Gen_Delegate_Imp1292(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> __Gen_Delegate_Imp1293(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemEditorChannelInfo __Gen_Delegate_Imp1294(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<IdWeightInfo> __Gen_Delegate_Imp1295(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStarInfo __Gen_Delegate_Imp1296(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBPlanetInfo> __Gen_Delegate_Imp1297(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBPlanetLikeBuildingInfo> __Gen_Delegate_Imp1298(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBAsteroidBeltInfo __Gen_Delegate_Imp1299(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp13(object p0, object p1, object p2, PlaySoundOption p3, bool p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp130(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSpaceStationInfo> __Gen_Delegate_Imp1300(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBStargateInfo> __Gen_Delegate_Imp1301(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBWormholeGateInfo> __Gen_Delegate_Imp1302(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSceneDummyInfo> __Gen_Delegate_Imp1303(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBuildingLocationInfo> __Gen_Delegate_Imp1304(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBMoonInfo> __Gen_Delegate_Imp1305(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSpaceStationInfo __Gen_Delegate_Imp1306(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSpaceStationNpcTalkerInfo> __Gen_Delegate_Imp1307(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBAsteroidGatherPointPositionInfo> __Gen_Delegate_Imp1308(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<HelloDialogIdToCompletedMainQuestInfo> __Gen_Delegate_Imp1309(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp131(object p0, object p1, object p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<SceneObject3DInfo> __Gen_Delegate_Imp1310(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SceneDummyObjectInfo> __Gen_Delegate_Imp1311(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<EmptyDummyLocationInfo> __Gen_Delegate_Imp1312(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SceneWayPointListInfo> __Gen_Delegate_Imp1313(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CollisionInfo __Gen_Delegate_Imp1314(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<PVector3D> __Gen_Delegate_Imp1315(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SphereCollisionInfo> __Gen_Delegate_Imp1316(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<CapsuleCollisionInfo> __Gen_Delegate_Imp1317(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBGalaxyInfo __Gen_Delegate_Imp1318(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStarfieldsInfo __Gen_Delegate_Imp1319(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp132(object p0, object p1, int p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemInfo __Gen_Delegate_Imp1320(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo __Gen_Delegate_Imp1321(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSpaceStationInfo __Gen_Delegate_Imp1322(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStargroupInfo __Gen_Delegate_Imp1323(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> __Gen_Delegate_Imp1324(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, GDBSolarSystemSimpleInfo> __Gen_Delegate_Imp1325(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1326(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public GDBWormholeGateInfo __Gen_Delegate_Imp1327(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo __Gen_Delegate_Imp1328(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] __Gen_Delegate_Imp1329(object p0, float[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp133(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public float[] __Gen_Delegate_Imp1330(object p0, byte[] p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] __Gen_Delegate_Imp1331(float[] p0, VoiceChatCompressionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public float[] __Gen_Delegate_Imp1332(byte[] p0, int p1, VoiceChatCompressionType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1333(float[] p0, short[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1334(short[] p0, float[] p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public float[] __Gen_Delegate_Imp1335(byte[] p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] __Gen_Delegate_Imp1336(float[] p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1337(float[] p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1338(object p0, byte[] p1, byte[] p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext __Gen_Delegate_Imp1339(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp134(object p0, object p1, int p2, uint p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public IVideoPlayer __Gen_Delegate_Imp1340(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1341(object p0, object p1, uint p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStargateInfo __Gen_Delegate_Imp1342(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp1343(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemInfo __Gen_Delegate_Imp1344(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public System.Random __Gen_Delegate_Imp1345(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget __Gen_Delegate_Imp1346(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceDropBox __Gen_Delegate_Imp1347(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public IConfigDataLoader __Gen_Delegate_Imp1348(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<KeyValuePair<ulong, ILBInSpacePlayerShip>> __Gen_Delegate_Imp1349(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public ResourceManager.RMState __Gen_Delegate_Imp135(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp1350(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcess __Gen_Delegate_Imp1351(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStar __Gen_Delegate_Imp1352(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientPlanet __Gen_Delegate_Imp1353(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientPlanetLikeBuilding __Gen_Delegate_Imp1354(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientMoon __Gen_Delegate_Imp1355(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientMoon __Gen_Delegate_Imp1356(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ClientSpaceObject> __Gen_Delegate_Imp1357(object p0, double p1, Vector3D p2)
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetInfo __Gen_Delegate_Imp1358(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetLikeBuildingInfo __Gen_Delegate_Imp1359(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp136(object p0, ResourceManager.RMState p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientPlanet __Gen_Delegate_Imp1360(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceObject __Gen_Delegate_Imp1361(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStar __Gen_Delegate_Imp1362(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip __Gen_Delegate_Imp1363(object p0, uint p1, int p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip4FlagShip __Gen_Delegate_Imp1364(object p0, uint p1, int p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ClientOtherPlayerSpaceShip __Gen_Delegate_Imp1365(object p0, uint p1, int p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ClientOtherPlayerSpaceShip4FlagShip __Gen_Delegate_Imp1366(object p0, uint p1, int p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ClientNpcSpaceShip __Gen_Delegate_Imp1367(object p0, uint p1, int p2, object p3, object p4, object p5, NpcShipAnimEffectType p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ClientHiredCaptainSpaceShip __Gen_Delegate_Imp1368(object p0, uint p1, int p2, object p3, object p4, object p5, NpcShipAnimEffectType p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceSimpleObject __Gen_Delegate_Imp1369(object p0, uint p1, int p2, Vector3D p3, Vector3D p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp137(object p0, object p1, object p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceSceneObject __Gen_Delegate_Imp1370(object p0, uint p1, int p2, int p3, Vector3D p4, Vector3D p5)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceDropBox __Gen_Delegate_Imp1371(object p0, uint p1, object p2, Vector3D p3, Vector3D p4)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceStation __Gen_Delegate_Imp1372(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSpaceStationInfo __Gen_Delegate_Imp1373(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceStation __Gen_Delegate_Imp1374(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStarGate __Gen_Delegate_Imp1375(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStarGate __Gen_Delegate_Imp1376(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceObjectType __Gen_Delegate_Imp1377(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBMoonInfo __Gen_Delegate_Imp1378(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimMoon __Gen_Delegate_Imp1379(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp138(object p0, object p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1380(object p0, uint p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetInfo __Gen_Delegate_Imp1381(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimPlanet __Gen_Delegate_Imp1382(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ClientMoon> __Gen_Delegate_Imp1383(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetLikeBuildingInfo __Gen_Delegate_Imp1384(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimPlanetLikeBuilding __Gen_Delegate_Imp1385(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimStar __Gen_Delegate_Imp1386(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1387(object p0, uint p1, SyncEventFlag p2)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp1388(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp1389(object p0, Vector3D p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp139(object p0, object p1, bool p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1390(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceObject __Gen_Delegate_Imp1391(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<IClientSpaceObjectEventListener> __Gen_Delegate_Imp1392(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceSimpleObject3DInfo __Gen_Delegate_Imp1393(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceSimpleObject __Gen_Delegate_Imp1394(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1395(object p0, uint p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceStation3DInfo __Gen_Delegate_Imp1396(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceStation __Gen_Delegate_Imp1397(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStargateInfo __Gen_Delegate_Imp1398(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataStargate3DInfo __Gen_Delegate_Imp1399(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp14(object p0, BlackJack.BJFramework.Runtime.AudioType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp140(object p0, object p1, bool p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public SimStarGate __Gen_Delegate_Imp1400(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceContext __Gen_Delegate_Imp1401(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> __Gen_Delegate_Imp1402(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockObjCompDropBoxBase __Gen_Delegate_Imp1403(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SceneObject3DInfo __Gen_Delegate_Imp1404(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1405(object p0, uint p1, int p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompIFFBase __Gen_Delegate_Imp1406(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1407(object p0, uint p1, int p2, object p3, object p4, NpcShipAnimEffectType p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompItemStoreClient __Gen_Delegate_Imp1408(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipInfo __Gen_Delegate_Imp1409(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp141(object p0, object p1, out ResourceManager.BundleLoadingCtx p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterInfo __Gen_Delegate_Imp1410(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipTemplateInfo __Gen_Delegate_Imp1411(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelDropInfo __Gen_Delegate_Imp1412(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWingShipCtrlStubBase __Gen_Delegate_Imp1413(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipAIInfo __Gen_Delegate_Imp1414(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShipAnimEffectType __Gen_Delegate_Imp1415(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<CustomizedParameterInfo> __Gen_Delegate_Imp1416(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1417(object p0, CustomizedParameter p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1418(object p0, CustomizedParameter p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1419(object p0, CustomizedParameter p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp142(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1420(object p0, object p1, object p2, int p3, int p4, ProfessionType p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1421(object p0, uint p1, int p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFlagShipCompInSpaceBasicInfoBase __Gen_Delegate_Imp1422(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFlagShipCompTeleportBase __Gen_Delegate_Imp1423(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILogicBlockShipCompPlayerInfo __Gen_Delegate_Imp1424(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompPickDropBoxBase __Gen_Delegate_Imp1425(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWingShipCtrlBase __Gen_Delegate_Imp1426(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInteractionMakerBase __Gen_Delegate_Imp1427(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompGuildAllianceTeleportBase __Gen_Delegate_Imp1428(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompPVPBase __Gen_Delegate_Imp1429(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp143(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpacePlayerShipCaptain __Gen_Delegate_Imp1430(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IInSpacePlayerShipDataContainer __Gen_Delegate_Imp1431(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1432(object p0, SimSpaceShip.MoveCmd p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompAOIHackerClient __Gen_Delegate_Imp1433(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1434(object p0, uint p1, SyncEventFlag p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceEquipEffectVirtualBufContainerClient __Gen_Delegate_Imp1435(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp1436(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dest.Math.Quaternion __Gen_Delegate_Imp1437(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector4D __Gen_Delegate_Imp1438(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp1439(object p0, PropertiesId p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp144(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1440(object p0, object p1, ShipEquipSlotType p2, int p3, bool p4, bool p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1441(object p0, object p1, ShipEquipSlotType p2, int p3, bool p4, float p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1442(object p0, object p1, ShipEquipSlotType p2, int p3, bool p4, bool p5, object p6, object p7, int p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1443(object p0, object p1, ShipEquipSlotType p2, int p3, bool p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp1444(object p0, int p1, uint p2, float p3, object p4, EquipType p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1445(object p0, int p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1446(object p0, int p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1447(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp1448(object p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceBasicCtxBase __Gen_Delegate_Imp1449(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BundleData.SingleBundleData __Gen_Delegate_Imp145(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceCaptain __Gen_Delegate_Imp1450(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSpaceResource __Gen_Delegate_Imp1451(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSpaceTargetBase __Gen_Delegate_Imp1452(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceWeaponEquipBase __Gen_Delegate_Imp1453(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCompPropertiesCalc __Gen_Delegate_Imp1454(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompFireControllBase __Gen_Delegate_Imp1455(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompDroneFireControllBase __Gen_Delegate_Imp1456(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompTargetSelectBase __Gen_Delegate_Imp1457(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompProcessContainer __Gen_Delegate_Imp1458(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSyncEventBase __Gen_Delegate_Imp1459(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp146(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompBufContainerBase __Gen_Delegate_Imp1460(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILogicBlockShipCompAOI __Gen_Delegate_Imp1461(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILogicBlockShipCompSpaceObject __Gen_Delegate_Imp1462(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSceneBase __Gen_Delegate_Imp1463(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompDelayExec __Gen_Delegate_Imp1464(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompKillRecordBase __Gen_Delegate_Imp1465(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompGuildInfo __Gen_Delegate_Imp1466(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInteractionBase __Gen_Delegate_Imp1467(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipDataContainer __Gen_Delegate_Imp1468(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBBuffFactory __Gen_Delegate_Imp1469(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp147(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtx __Gen_Delegate_Imp1470(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipItemStoreDataContainer __Gen_Delegate_Imp1471(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompItemStoreBase __Gen_Delegate_Imp1472(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IInSpaceShipDataContainer __Gen_Delegate_Imp1473(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShip3DInfo __Gen_Delegate_Imp1474(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceShip __Gen_Delegate_Imp1475(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1476(object p0, uint p1, int p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1477(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpacePlayerShip __Gen_Delegate_Imp1478(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<ILBSpaceTarget> __Gen_Delegate_Imp1479(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BundleData __Gen_Delegate_Imp148(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<ILBSpaceDropBox> __Gen_Delegate_Imp1480(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1481(object p0, object p1, IFFState p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1482(object p0, LBSyncEventOnHitInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1483(object p0, uint p1, ulong p2, float p3, BufType p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1484(object p0, TargetNoticeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1485(object p0, SimSpaceShip.MoveCmd p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1486(object p0, object p1, Vector3D p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1487(object p0, double p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1488(object p0, NpcShipAnimEffectType p1, uint p2, uint p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1489(object p0, object p1, object p2, object p3, ushort p4)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp149(object p0, object p1, object p2, bool p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1490(object p0, object p1, object p2, ushort p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1491(object p0, object p1, StructProcessEquipLaunchBase p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1492(object p0, bool p1, float p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1493(object p0, int p1, int p2, bool p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1494(object p0, float p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1495(object p0, float p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1496(object p0, float p1, float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1497(object p0, object p1, StructProcessSuperEquipAoeLaunch p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1498(object p0, SceneWingShipSetting p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1499(object p0, uint p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp15(object p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp150(object p0, object p1, object p2, object p3, bool p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1500(object p0, SimSpaceShip.MoveCmd p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1501(object p0, out Vector3D p1, out Dest.Math.Quaternion p2, out Vector4D p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1502(object p0, Vector3D p1, Dest.Math.Quaternion p2, Vector4D p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1503(object p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1504(object p0, Vector3D p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1505(object p0, SimSpaceShip.MoveCmd.CmdType p1, SimSpaceShip.MoveCmd p2)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceShip.MoveCmd __Gen_Delegate_Imp1506(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip __Gen_Delegate_Imp1507(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1508(object p0, object p1, bool p2, double p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1509(object p0, int p1, StoreItemType p2, long p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp151(object p0, bool p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public TimeSpan __Gen_Delegate_Imp1510(object p0, DateTime p1)
        {
        }

        [MethodImpl(0x8000)]
        public TimeSpan __Gen_Delegate_Imp1511(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1512(object p0, int p1, int p2, int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public IdLevelInfo __Gen_Delegate_Imp1513(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<IdLevelInfo> __Gen_Delegate_Imp1514(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipPositionInfo __Gen_Delegate_Imp1515(Vector3D p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceGrideIndex __Gen_Delegate_Imp1516(double p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1517(double p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1518(double p0, SpaceGrideIndex p1)
        {
        }

        [MethodImpl(0x8000)]
        public GraphicQualityLevel __Gen_Delegate_Imp1519(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LayerLayoutDesc __Gen_Delegate_Imp152(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1520(object p0, GraphicQualityLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public GraphicEffectLevel __Gen_Delegate_Imp1521(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1522(object p0, GraphicEffectLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1523(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<IDFRFilterResultItem> __Gen_Delegate_Imp1524(object p0, object p1, object p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public IDFRDataItem __Gen_Delegate_Imp1525(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1526(object p0, ulong p1, int p2, GuildBattleType p3, int p4, DateTime p5, object p6, object p7, int p8, int p9, int p10)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1527(object p0, object p1, ulong p2, uint p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1528(object p0, FleetPosition p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1529(object p0, FleetPosition p1)
        {
        }

        [MethodImpl(0x8000)]
        public LayerRenderSettingDesc __Gen_Delegate_Imp153(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<FleetPosition> __Gen_Delegate_Imp1530(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1531(object p0, DateTime p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo __Gen_Delegate_Imp1532(object p0, GuildBuildingType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo __Gen_Delegate_Imp1533(object p0, ulong p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyStaticInfo.GuildOccupySolarSystemInfo __Gen_Delegate_Imp1534(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyStaticInfo.AllianceOccupySolarSystemInfo __Gen_Delegate_Imp1535(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo __Gen_Delegate_Imp1536(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapAllianceSimpleInfo __Gen_Delegate_Imp1537(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo __Gen_Delegate_Imp1538(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SimpleItemInfoWithCount __Gen_Delegate_Imp1539(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.BJFramework.Runtime.Scene.SceneManager __Gen_Delegate_Imp154()
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp1540(StoreItemType p0, int p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp1541(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1542(StoreItemType p0, int p1, out RankType p2, out SubRankType p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1543(GlobalSceneInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProVectorDouble __Gen_Delegate_Imp1544(Vector3D p0)
        {
        }

        [MethodImpl(0x8000)]
        public PVector3D __Gen_Delegate_Imp1545(Vector3D p0)
        {
        }

        [MethodImpl(0x8000)]
        public ushort __Gen_Delegate_Imp1546(double p0)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp1547(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProVectorUInt32 __Gen_Delegate_Imp1548(Vector3D p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipPositionInfo __Gen_Delegate_Imp1549(object p0, object p1, ShipPoseRequest p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp155(object p0, int p1, int p2, int p3, int p4, Vector3 p5, bool p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public SimMoveableSpaceObjectStateSnapshot __Gen_Delegate_Imp1550(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimSpaceShipStateSnapshot __Gen_Delegate_Imp1551(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProBaseMoveStateSnapshot __Gen_Delegate_Imp1552(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipMoveStateSnapshot __Gen_Delegate_Imp1553(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CollisionInfo __Gen_Delegate_Imp1554(object p0, Vector3D p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1555(object p0, GrandFaction p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1556(GrandFaction p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1557(WeaponCategory p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1558(StringTableId p0)
        {
        }

        [MethodImpl(0x8000)]
        public SignalInfo __Gen_Delegate_Imp1559(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp156(object p0, object p1, object p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public ProSignalInfo __Gen_Delegate_Imp1560(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<PlayerSimpleInfo> __Gen_Delegate_Imp1561(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProPlayerSimpleInfo> __Gen_Delegate_Imp1562(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimpleInfo __Gen_Delegate_Imp1563(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProPlayerSimpleInfo __Gen_Delegate_Imp1564(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProPVPSignalInfo __Gen_Delegate_Imp1565(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PVPSignalInfo __Gen_Delegate_Imp1566(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateMissionInvadeInfo __Gen_Delegate_Imp1567(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDelegateMissionInvadeInfo __Gen_Delegate_Imp1568(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateMissionInvadeEventInfo __Gen_Delegate_Imp1569(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp157(object p0, object p1, bool p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProDelegateMissionInvadeEventInfo __Gen_Delegate_Imp1570(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<DelegateMissionInvadeEventInfo> __Gen_Delegate_Imp1571(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProDelegateMissionInvadeEventInfo> __Gen_Delegate_Imp1572(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateMissionInfo __Gen_Delegate_Imp1573(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDelegateMissionInfo __Gen_Delegate_Imp1574(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProIdLevelInfo __Gen_Delegate_Imp1575(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IdLevelInfo __Gen_Delegate_Imp1576(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainSimpleDynamicInfo __Gen_Delegate_Imp1577(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProHiredCaptainSimpleDynamicInfo __Gen_Delegate_Imp1578(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainStaticInfo __Gen_Delegate_Imp1579(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp158(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProHiredCaptainStaticInfo __Gen_Delegate_Imp1580(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainInfo __Gen_Delegate_Imp1581(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProHiredCaptainInfo __Gen_Delegate_Imp1582(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MSRedeployInfo __Gen_Delegate_Imp1583(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProMSRedeployInfo __Gen_Delegate_Imp1584(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo __Gen_Delegate_Imp1585(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProItemInfo __Gen_Delegate_Imp1586(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> __Gen_Delegate_Imp1587(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MailInfo __Gen_Delegate_Imp1588(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProMailInfo __Gen_Delegate_Imp1589(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SceneLayerBase __Gen_Delegate_Imp159(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProStoreItemInfo __Gen_Delegate_Imp1590(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemInfo __Gen_Delegate_Imp1591(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipStoreItemInfo __Gen_Delegate_Imp1592(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipStoreItemInfo __Gen_Delegate_Imp1593(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipStoreItemInfo> __Gen_Delegate_Imp1594(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProStoreItemUpdateInfo __Gen_Delegate_Imp1595(StoreItemUpdateInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemUpdateInfo __Gen_Delegate_Imp1596(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipStoreItemUpdateInfo __Gen_Delegate_Imp1597(ShipStoreItemUpdateInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipStoreItemUpdateInfo __Gen_Delegate_Imp1598(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildStoreItemUpdateInfo __Gen_Delegate_Imp1599(GuildStoreItemUpdateInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp16(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SceneLayerBase __Gen_Delegate_Imp160(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildStoreItemUpdateInfo __Gen_Delegate_Imp1600(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProCustomizedParameterInfo __Gen_Delegate_Imp1601(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CustomizedParameterInfo __Gen_Delegate_Imp1602(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDailyLoginRewardInfo __Gen_Delegate_Imp1603(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DailyLoginRewardInfo __Gen_Delegate_Imp1604(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1605(DateTime p0, DateTime p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProCMDailySignInfo __Gen_Delegate_Imp1606(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProCommanderAuthInfo __Gen_Delegate_Imp1607(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipSlotGroupInfo __Gen_Delegate_Imp1608(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSlotGroupInfo __Gen_Delegate_Imp1609(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Camera __Gen_Delegate_Imp161(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipSlotAmmoInfo __Gen_Delegate_Imp1610(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProCostInfo __Gen_Delegate_Imp1611(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CostInfo __Gen_Delegate_Imp1612(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProTechUpgradeInfo __Gen_Delegate_Imp1613(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeInfo __Gen_Delegate_Imp1614(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProCurrencyUpdateInfo __Gen_Delegate_Imp1615(CurrencyUpdateInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public CurrencyUpdateInfo __Gen_Delegate_Imp1616(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProProductionLineInfo __Gen_Delegate_Imp1617(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProductionLineInfo __Gen_Delegate_Imp1618(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipCustomTemplateInfo __Gen_Delegate_Imp1619(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp162(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipCustomTemplateInfo __Gen_Delegate_Imp1620(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProStoreItemTransformInfo __Gen_Delegate_Imp1621(StoreItemTransformInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemTransformInfo __Gen_Delegate_Imp1622(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProQuestProcessingInfo __Gen_Delegate_Imp1623(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestProcessingInfo __Gen_Delegate_Imp1624(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProQuestWaitForAcceptInfo __Gen_Delegate_Imp1625(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestWaitForAcceptInfo __Gen_Delegate_Imp1626(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProQuestRewardInfo __Gen_Delegate_Imp1627(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestRewardInfo __Gen_Delegate_Imp1628(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProNpcDNId __Gen_Delegate_Imp1629(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Canvas __Gen_Delegate_Imp163(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId __Gen_Delegate_Imp1630(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1631(object p0, CurrencyType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1632(int p0, int p1, GrandFaction p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProKillRecordInfo __Gen_Delegate_Imp1633(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProKillRecordOpponentInfo __Gen_Delegate_Imp1634(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSimpleItemInfoWithCount> __Gen_Delegate_Imp1635(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLostItemInfo __Gen_Delegate_Imp1636(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LostItemInfo __Gen_Delegate_Imp1637(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSimpleItemInfoWithCount __Gen_Delegate_Imp1638(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public KillRecordInfo __Gen_Delegate_Imp1639(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CanvasGroup __Gen_Delegate_Imp164(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public KillRecordOpponentInfo __Gen_Delegate_Imp1640(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SimpleItemInfoWithCount> __Gen_Delegate_Imp1641(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProPlayerPVPInfo __Gen_Delegate_Imp1642(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerPVPInfo __Gen_Delegate_Imp1643(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDrivingLicenseUpgradeInfo __Gen_Delegate_Imp1644(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DrivingLicenseUpgradeInfo __Gen_Delegate_Imp1645(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProBitArrayExInfo __Gen_Delegate_Imp1646(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BitArrayEx __Gen_Delegate_Imp1647(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProActivityInfo __Gen_Delegate_Imp1648(ActivityInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public ActivityInfo __Gen_Delegate_Imp1649(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp165(object p0, Scene p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProPlayerActivityInfo __Gen_Delegate_Imp1650(PlayerActivityInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerActivityInfo __Gen_Delegate_Imp1651(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGlobalSceneInfo __Gen_Delegate_Imp1652(GlobalSceneInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public GlobalSceneInfo __Gen_Delegate_Imp1653(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProRankingPlayerInfo __Gen_Delegate_Imp1654(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public RankingPlayerBasicInfo __Gen_Delegate_Imp1655(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProRankingGuildInfo __Gen_Delegate_Imp1656(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public RankingGuildBasicInfo __Gen_Delegate_Imp1657(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProRankingAllianceInfo __Gen_Delegate_Imp1658(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public RankingAllianceBasicInfo __Gen_Delegate_Imp1659(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<string, string> __Gen_Delegate_Imp166(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProRankingListInfo __Gen_Delegate_Imp1660(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RankingListInfo __Gen_Delegate_Imp1661(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProNpcDialogInfo __Gen_Delegate_Imp1662(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDialogInfo __Gen_Delegate_Imp1663(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProFormatStringParamInfo __Gen_Delegate_Imp1664(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public FormatStringParamInfo __Gen_Delegate_Imp1665(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcCaptainSimpleInfo> __Gen_Delegate_Imp1666(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProNpcCaptainSimpleInfo> __Gen_Delegate_Imp1667(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProNpcCaptainSimpleInfo __Gen_Delegate_Imp1668(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainSimpleInfo __Gen_Delegate_Imp1669(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<string> __Gen_Delegate_Imp167(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProAuctionOrderBriefInfo __Gen_Delegate_Imp1670(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionOrderBriefInfo __Gen_Delegate_Imp1671(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProAuctionItemDetailInfo __Gen_Delegate_Imp1672(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionItemDetailInfo __Gen_Delegate_Imp1673(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProTradeListItemInfo __Gen_Delegate_Imp1674(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TradeListItemInfo __Gen_Delegate_Imp1675(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProTradeNpcShopItemInfo __Gen_Delegate_Imp1676(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TradeNpcShopItemInfo __Gen_Delegate_Imp1677(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProTradeNpcShopSaleItemInfo __Gen_Delegate_Imp1678(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TradeNpcShopSaleItemInfo __Gen_Delegate_Imp1679(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ServerSettingManager __Gen_Delegate_Imp168()
        {
        }

        [MethodImpl(0x8000)]
        public ProPVPInvadeRescueInfo __Gen_Delegate_Imp1680(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PVPInvadeRescueInfo __Gen_Delegate_Imp1681(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AgeType __Gen_Delegate_Imp1682(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConstellationType __Gen_Delegate_Imp1683(DateTime p0)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp1684(PropertyCategory p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1685(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp1686(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBasicInfo __Gen_Delegate_Imp1687(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBasicInfo __Gen_Delegate_Imp1688(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildDynamicInfo __Gen_Delegate_Imp1689(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp169(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildDynamicInfo __Gen_Delegate_Imp1690(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildLogoInfo __Gen_Delegate_Imp1691(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildLogoInfo __Gen_Delegate_Imp1692(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberListInfo __Gen_Delegate_Imp1693(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildMemberListInfo __Gen_Delegate_Imp1694(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimpleRuntimeInfo __Gen_Delegate_Imp1695(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildSimpleRuntimeInfo __Gen_Delegate_Imp1696(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyInfo __Gen_Delegate_Imp1697(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDiplomacyInfo __Gen_Delegate_Imp1698(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberInfo __Gen_Delegate_Imp1699(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp17(object p0, BlackJack.BJFramework.Runtime.AudioType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public MapSceneUpdatePipeLineCtxBase __Gen_Delegate_Imp170(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildMemberInfo __Gen_Delegate_Imp1700(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBasicInfo __Gen_Delegate_Imp1701(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildMemberBasicInfo __Gen_Delegate_Imp1702(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberDynamicInfo __Gen_Delegate_Imp1703(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildMemberDynamicInfo __Gen_Delegate_Imp1704(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildPurchaseLogInfo __Gen_Delegate_Imp1705(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildPurchaseOrderInfo __Gen_Delegate_Imp1706(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildPurchaseOrderInfo __Gen_Delegate_Imp1707(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberRuntimeInfo __Gen_Delegate_Imp1708(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildMemberRuntimeInfo __Gen_Delegate_Imp1709(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<string> __Gen_Delegate_Imp171(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo __Gen_Delegate_Imp1710(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProPlayerSimplestInfo __Gen_Delegate_Imp1711(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimplestInfo __Gen_Delegate_Imp1712(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildSimplestInfo __Gen_Delegate_Imp1713(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildJoinApplyViewInfo __Gen_Delegate_Imp1714(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildJoinApplyViewInfo __Gen_Delegate_Imp1715(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildJoinApplyBasicInfo __Gen_Delegate_Imp1716(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildJoinApplyBasicInfo __Gen_Delegate_Imp1717(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildStaffingLogInfo __Gen_Delegate_Imp1718(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildStoreItemInfo __Gen_Delegate_Imp1719(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SceneLayerBase __Gen_Delegate_Imp172(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildStoreItemInfo __Gen_Delegate_Imp1720(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCurrencyInfo __Gen_Delegate_Imp1721(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildCurrencyInfo __Gen_Delegate_Imp1722(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfo __Gen_Delegate_Imp1723(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildSolarSystemInfo __Gen_Delegate_Imp1724(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildCurrencyLogInfo __Gen_Delegate_Imp1725(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCurrencyLogInfo __Gen_Delegate_Imp1726(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo __Gen_Delegate_Imp1727(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBuildingInfo __Gen_Delegate_Imp1728(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBuildingBattleInfo __Gen_Delegate_Imp1729(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MapSceneTaskBase.LayerDesc[] __Gen_Delegate_Imp173(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingBattleInfo __Gen_Delegate_Imp1730(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFleetMemberBasicInfo __Gen_Delegate_Imp1731(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo __Gen_Delegate_Imp1732(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFleetMemberDynamicInfo __Gen_Delegate_Imp1733(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberDynamicInfo __Gen_Delegate_Imp1734(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFleetSimpleInfo __Gen_Delegate_Imp1735(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetSimpleInfo __Gen_Delegate_Imp1736(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFleetDetailInfo __Gen_Delegate_Imp1737(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetInfo __Gen_Delegate_Imp1738(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFleetMemberDynamicInfo> __Gen_Delegate_Imp1739(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp174(object p0, float p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFleetBasicInfo __Gen_Delegate_Imp1740(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarBasicInfo __Gen_Delegate_Imp1741(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildStaticFlagShipInstanceInfo> __Gen_Delegate_Imp1742(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildStaticFlagShipInstanceInfo __Gen_Delegate_Imp1743(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFlagShipDetailInfo __Gen_Delegate_Imp1744(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFlagShipHangarDetailInfo __Gen_Delegate_Imp1745(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarDetailInfo __Gen_Delegate_Imp1746(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildAntiTeleportInfo __Gen_Delegate_Imp1747(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo __Gen_Delegate_Imp1748(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTeleportTunnelEffectInfoClient __Gen_Delegate_Imp1749(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp175(object p0, object p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildTeleportTunnelInfo __Gen_Delegate_Imp1750(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1751(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1752(object p0, ShipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1753(object p0, GrandFaction p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1754(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp1755(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemQuestPoolCountInfo __Gen_Delegate_Imp1756(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemDelegateMissionPoolCountInfo __Gen_Delegate_Imp1757(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1758(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1759(DateTime p0, DateTime p1)
        {
        }

        [MethodImpl(0x8000)]
        public Task.TaskState __Gen_Delegate_Imp176(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildProductionLineInfo __Gen_Delegate_Imp1760(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildProductionLineInfo __Gen_Delegate_Imp1761(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProBuildingLostReport __Gen_Delegate_Imp1762(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BuildingLostReport __Gen_Delegate_Imp1763(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSovereignLostReport __Gen_Delegate_Imp1764(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SovereignLostReport __Gen_Delegate_Imp1765(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLossCompensation __Gen_Delegate_Imp1766(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LossCompensation __Gen_Delegate_Imp1767(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLostItem __Gen_Delegate_Imp1768(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LostItem __Gen_Delegate_Imp1769(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp177(object p0, Task.TaskState p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildBuildingLevelDetailInfo __Gen_Delegate_Imp1770(GuildBuildingType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1771(GuildBuildingType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildActionInfo __Gen_Delegate_Imp1772(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildActionInfo __Gen_Delegate_Imp1773(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1774(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1775(int p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattleSimpleReportInfo __Gen_Delegate_Imp1776(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleSimpleReportInfo __Gen_Delegate_Imp1777(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattleReportInfo __Gen_Delegate_Imp1778(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleReportInfo __Gen_Delegate_Imp1779(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TaskManager __Gen_Delegate_Imp178()
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattlePlayerKillLostStatInfo __Gen_Delegate_Imp1780(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattlePlayerKillLostStatInfo __Gen_Delegate_Imp1781(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattleGuildKillLostStatInfo __Gen_Delegate_Imp1782(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleGuildKillLostStatInfo __Gen_Delegate_Imp1783(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattleShipLostStatInfo __Gen_Delegate_Imp1784(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleShipLostStatInfo __Gen_Delegate_Imp1785(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattleGuildBuildingSimpleInfo __Gen_Delegate_Imp1786(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleGuildBuildingSimpleInfo __Gen_Delegate_Imp1787(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMomentsInfo __Gen_Delegate_Imp1788(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildMomentsInfo __Gen_Delegate_Imp1789(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp179(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1790(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyType __Gen_Delegate_Imp1791(object p0, object p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyStaticInfo __Gen_Delegate_Imp1792(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapGuildOccupyStaticInfo __Gen_Delegate_Imp1793(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo __Gen_Delegate_Imp1794(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapGuildOccupyDynamicInfo __Gen_Delegate_Imp1795(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo __Gen_Delegate_Imp1796(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapGuildSimpleInfo __Gen_Delegate_Imp1797(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapAllianceSimpleInfo __Gen_Delegate_Imp1798(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapAllianceSimpleInfo __Gen_Delegate_Imp1799(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp18(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<string, object> __Gen_Delegate_Imp180(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<FleetPosition> __Gen_Delegate_Imp1800(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1801(int p0, out FormationType p1, out FormationCreatingInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1802(Vector3D p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetInfo __Gen_Delegate_Imp1803(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1804(object p0, object p1, DynamicSceneNearCelestialType p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1805(object p0, object p1, GuildBuildingType p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1806(object p0, object p1, DynamicSceneLocationType p2, object p3, DynamicSceneNearCelestialType p4)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp1807(object p0, object p1, ref DynamicSceneLocationType p2, object p3, ref DynamicSceneNearCelestialType p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1808(object p0, BufGamePlayFlag p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1809(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp181(object p0, object p1, object p2, object p3, int p4, string[] p5)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBenefitsSovereignty __Gen_Delegate_Imp1810(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBenefitsInformationPoint __Gen_Delegate_Imp1811(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBenefitsCommon __Gen_Delegate_Imp1812(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCommonBenefitsInfo __Gen_Delegate_Imp1813(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildInformationPointBenefitsInfo __Gen_Delegate_Imp1814(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSovereigntyBenefitsInfo __Gen_Delegate_Imp1815(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildTradePurchaseInfo __Gen_Delegate_Imp1816(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePurchaseInfo __Gen_Delegate_Imp1817(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildTradeTransportInfo __Gen_Delegate_Imp1818(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradeTransportInfo __Gen_Delegate_Imp1819(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp182(object p0, object p1, string[] p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildTradePortExtraInfo __Gen_Delegate_Imp1820(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePortExtraInfo __Gen_Delegate_Imp1821(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildTradePurchaseOrderTransportLogInfo __Gen_Delegate_Imp1822(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePurchaseOrderTransportLogInfo __Gen_Delegate_Imp1823(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFlagShipOptLogInfo __Gen_Delegate_Imp1824(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipOptLogInfo __Gen_Delegate_Imp1825(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProAllianceBasicInfo __Gen_Delegate_Imp1826(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceBasicInfo __Gen_Delegate_Imp1827(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceLogoInfo __Gen_Delegate_Imp1828(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProAllianceLogoInfo __Gen_Delegate_Imp1829(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp183(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProAllianceInfo __Gen_Delegate_Imp1830(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceInfo __Gen_Delegate_Imp1831(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProAllianceMemberInfo __Gen_Delegate_Imp1832(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceMemberInfo __Gen_Delegate_Imp1833(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProAllianceInviteInfo __Gen_Delegate_Imp1834(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceInviteInfo __Gen_Delegate_Imp1835(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceInviteInfo __Gen_Delegate_Imp1836(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildAllianceInviteInfo __Gen_Delegate_Imp1837(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProAllianceSimplestInfo __Gen_Delegate_Imp1838(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceSimplestInfo __Gen_Delegate_Imp1839(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp184(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProRechargeOrder __Gen_Delegate_Imp1840(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProRechargeGiftPackage __Gen_Delegate_Imp1841(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProRechargeMonthlyCard __Gen_Delegate_Imp1842(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProRechargeItem __Gen_Delegate_Imp1843(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeOrderInfo __Gen_Delegate_Imp1844(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeGiftPackageInfo __Gen_Delegate_Imp1845(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeMonthlyCardInfo __Gen_Delegate_Imp1846(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeItemInfo __Gen_Delegate_Imp1847(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1848(object p0, bool p1, char p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1849(double p0, out int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp185(object p0, object p1, PlayableBinding p2)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp1850(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1851(object p0, int p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> __Gen_Delegate_Imp1852(object p0, object p1, object p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> __Gen_Delegate_Imp1853(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> __Gen_Delegate_Imp1854(object p0, int p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> __Gen_Delegate_Imp1855(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> __Gen_Delegate_Imp1856(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<DJNode, int>> __Gen_Delegate_Imp1857(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode>[] __Gen_Delegate_Imp1858(object p0, int p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1859(object p0, object p1, object p2, int p3, object p4, int p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp186(object p0, object p1, object p2, object p3, int p4, bool p5, bool p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public DJNode __Gen_Delegate_Imp1860(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1861(object p0, object p1, object p2, object p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1862(object p0, char[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public InvalidCharacterFilterBase __Gen_Delegate_Imp1863()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1864(object p0, KeyValuePair<char, char>[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp1865(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1866(object p0, int p1, out float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1867(object p0, out string p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1868(object p0, char p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp1869(char p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp187(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, int p8, object p9, object p10)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1870(object p0, int p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1871(object p0, object[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public BitArrayEx __Gen_Delegate_Imp1872(byte[] p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1873(object p0, byte[] p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp1874(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public byte __Gen_Delegate_Imp1875(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1876(object p0, ref Queue<uint> p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1877(object p0, uint p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.Utils.DelayExecHelper.IDelayExecItem __Gen_Delegate_Imp1878(object p0, object p1, int p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.Utils.DelayExecHelper.IDelayExecItem __Gen_Delegate_Imp1879(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public TimelineAutoBindAttribute __Gen_Delegate_Imp188(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1880(float p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime __Gen_Delegate_Imp1881(DateTime p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1882(object p0, int p1, out bool p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1883(object p0, out bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public IUserGuideStepInfo __Gen_Delegate_Imp1884(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IUserGuideGroupInfo __Gen_Delegate_Imp1885(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1886(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<int, object> __Gen_Delegate_Imp1887(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1888(object p0, object p1, ulong? p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1889(object p0, object p1, object p2, out string p3, out AddressFamily p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp189(object p0, object p1, object p2, object p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public IPAddress __Gen_Delegate_Imp1890(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConnectionState __Gen_Delegate_Imp1891(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1892(object p0, ConnectionState p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1893(object p0, byte[] p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp1894(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp1895(object p0, object p1, out int p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ArraySegment<byte> __Gen_Delegate_Imp1896(object p0, object p1, ulong? p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp1897(byte[] p0, int p1, int p2, ulong? p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1898(object p0, object p1, int p2, object p3, object p4, object p5, object p6, int p7, int p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1899(object p0, object p1, object p2, object p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp19()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp190(object p0, object p1, object p2, int p3, object p4, object p5, object p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp1900(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public IClient __Gen_Delegate_Imp1901(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public SessionToken __Gen_Delegate_Imp1902(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAuctionItemBriefInfo> __Gen_Delegate_Imp1903(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAuctionOrderBriefInfo> __Gen_Delegate_Imp1904(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionItemBuyReq __Gen_Delegate_Imp1905(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProStoreItemUpdateInfo __Gen_Delegate_Imp1906(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProCurrencyUpdateInfo __Gen_Delegate_Imp1907(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAuctionItemDetailInfo> __Gen_Delegate_Imp1908(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionItemOnShelveReq __Gen_Delegate_Imp1909(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp191(object p0, object p1, out Transform p2, out Component p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipStoreItemUpdateInfo __Gen_Delegate_Imp1910(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProCurrencyUpdateInfo> __Gen_Delegate_Imp1911(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProStoreItemUpdateInfo> __Gen_Delegate_Imp1912(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatInfo __Gen_Delegate_Imp1913(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatCententInfo __Gen_Delegate_Imp1914(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ulong> __Gen_Delegate_Imp1915(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProChatContentVoice> __Gen_Delegate_Imp1916(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentText __Gen_Delegate_Imp1917(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentVoice __Gen_Delegate_Imp1918(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatVoiceSimpleInfo __Gen_Delegate_Imp1919(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp192(object p0, object p1, object p2, out string p3, out string p4)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentLocation __Gen_Delegate_Imp1920(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentItem __Gen_Delegate_Imp1921(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentShip __Gen_Delegate_Imp1922(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentTeamInvite __Gen_Delegate_Imp1923(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentPlayer __Gen_Delegate_Imp1924(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentKillRecord __Gen_Delegate_Imp1925(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentDelegateMission __Gen_Delegate_Imp1926(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentGuild __Gen_Delegate_Imp1927(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentSailReport __Gen_Delegate_Imp1928(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProChatContentGuildBattleReport __Gen_Delegate_Imp1929(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TimelineObjectStorage __Gen_Delegate_Imp193(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProFormatStringParamInfo> __Gen_Delegate_Imp1930(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProVectorDouble __Gen_Delegate_Imp1931(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProShipSlotGroupInfo> __Gen_Delegate_Imp1932(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAmmoStoreItemInfo> __Gen_Delegate_Imp1933(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<byte[]> __Gen_Delegate_Imp1934(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipDetailInfo __Gen_Delegate_Imp1935(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<long> __Gen_Delegate_Imp1936(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProCharChipSlotInfo> __Gen_Delegate_Imp1937(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProItemInfo> __Gen_Delegate_Imp1938(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProFakeNpcInfo __Gen_Delegate_Imp1939(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp194(object p0, bool p1, double p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProHiredCaptainInfo> __Gen_Delegate_Imp1940(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProDelegateMissionInvadeInfo> __Gen_Delegate_Imp1941(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSpaceSignalResultInfo> __Gen_Delegate_Imp1942(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSignalInfo> __Gen_Delegate_Imp1943(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProHiredCaptainDynmicInfo __Gen_Delegate_Imp1944(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProIdLevelInfo> __Gen_Delegate_Imp1945(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProHiredCaptainShipInfo> __Gen_Delegate_Imp1946(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProCostInfo> __Gen_Delegate_Imp1947(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProShipTemplateHighSlotInfo> __Gen_Delegate_Imp1948(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLostItemInfo> __Gen_Delegate_Imp1949(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.BJFramework.Utils.DelayExecHelper.IDelayExecItem __Gen_Delegate_Imp195(object p0, object p1, int p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLostItem> __Gen_Delegate_Imp1950(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProDevelopmentHistoryInfo> __Gen_Delegate_Imp1951(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProRankingPlayerInfo> __Gen_Delegate_Imp1952(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProRankingGuildInfo> __Gen_Delegate_Imp1953(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProRankingAllianceInfo> __Gen_Delegate_Imp1954(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> __Gen_Delegate_Imp1955(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProHiredCaptainStaticInfo> __Gen_Delegate_Imp1956(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProHiredCaptainSimpleDynamicInfo> __Gen_Delegate_Imp1957(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProIdValueInfo __Gen_Delegate_Imp1958(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProFactionCreditLevelRewardInfoSub> __Gen_Delegate_Imp1959(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp196(object p0, uint p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProtoTypeShipListAck.ProShipInstanceData> __Gen_Delegate_Imp1960(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProFightOperationCmdInfo __Gen_Delegate_Imp1961(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<FriendListAck.FriendListData> __Gen_Delegate_Imp1962(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProPlayerBasicStaticInfo __Gen_Delegate_Imp1963(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DSVersionNtf __Gen_Delegate_Imp1964(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProConfigFileMD5Info> __Gen_Delegate_Imp1965(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProFreezingItemInfo> __Gen_Delegate_Imp1966(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public FreezingItemTimeOutConfirmReq __Gen_Delegate_Imp1967(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDSShipHangarInfo __Gen_Delegate_Imp1968(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProActivityInfo> __Gen_Delegate_Imp1969(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp197(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProPlayerActivityInfo> __Gen_Delegate_Imp1970(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProQuestEnvirmentInfo __Gen_Delegate_Imp1971(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProPlayerSimplestInfo> __Gen_Delegate_Imp1972(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildSimplestInfo> __Gen_Delegate_Imp1973(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProCustomizedParameterInfo> __Gen_Delegate_Imp1974(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProCreditCurrencyInfo> __Gen_Delegate_Imp1975(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProCharChipSchemeInfo> __Gen_Delegate_Imp1976(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProStoreItemInfo> __Gen_Delegate_Imp1977(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDSItemStoreInfo __Gen_Delegate_Imp1978(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProDSHiredCaptainInfo> __Gen_Delegate_Imp1979(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.BJFramework.Utils.DelayExecHelper.IDelayExecItem __Gen_Delegate_Imp198(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProFleetInfo> __Gen_Delegate_Imp1980(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProPVPSignalInfo> __Gen_Delegate_Imp1981(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProScanProbeInfo> __Gen_Delegate_Imp1982(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSolarSystemSpaceSignalInfo> __Gen_Delegate_Imp1983(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProDelegateMissionInfo> __Gen_Delegate_Imp1984(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProQuestEnvirmentInfo> __Gen_Delegate_Imp1985(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProQuestProcessingInfo> __Gen_Delegate_Imp1986(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProQuestWaitForAcceptInfo> __Gen_Delegate_Imp1987(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProStoredMailInfo> __Gen_Delegate_Imp1988(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDSMailInfo __Gen_Delegate_Imp1989(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] __Gen_Delegate_Imp199(byte[] p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProIdFloatValueInfo> __Gen_Delegate_Imp1990(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProFactionCreditLevelRewardInfo> __Gen_Delegate_Imp1991(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProFactionCreditNpcQuestInfo> __Gen_Delegate_Imp1992(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProIdValueInfo> __Gen_Delegate_Imp1993(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProTechUpgradeInfo> __Gen_Delegate_Imp1994(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProProductionLineInfo> __Gen_Delegate_Imp1995(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProShipCustomTemplateInfo> __Gen_Delegate_Imp1996(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProKillRecordInfo> __Gen_Delegate_Imp1997(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDSKillRecordInfo __Gen_Delegate_Imp1998(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProDevelopmentProjectInfo> __Gen_Delegate_Imp1999(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Camera __Gen_Delegate_Imp2(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AudioManager4Unity __Gen_Delegate_Imp20()
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp200(byte[] p0, ref byte[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProPVPInvadeRescueInfo> __Gen_Delegate_Imp2000(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProRechargeOrder> __Gen_Delegate_Imp2001(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProRechargeGiftPackage> __Gen_Delegate_Imp2002(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProRechargeMonthlyCard> __Gen_Delegate_Imp2003(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProRechargeItem> __Gen_Delegate_Imp2004(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProDailyLoginRewardInfo> __Gen_Delegate_Imp2005(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProStoreItemTransformInfo> __Gen_Delegate_Imp2006(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProAmmoStoreItemInfo __Gen_Delegate_Imp2007(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipSetAmmoInfo> __Gen_Delegate_Imp2008(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProShipStoreItemUpdateInfo> __Gen_Delegate_Imp2009(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp201(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GMServerAccountInfoAck.AccountUserInfo> __Gen_Delegate_Imp2010(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProHangerInfo> __Gen_Delegate_Imp2011(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAllianceBasicInfo> __Gen_Delegate_Imp2012(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAllianceMemberInfo> __Gen_Delegate_Imp2013(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildAllianceInviteInfo> __Gen_Delegate_Imp2014(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBattleSimpleReportInfo> __Gen_Delegate_Imp2015(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildAntiTeleportInfo> __Gen_Delegate_Imp2016(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildTeleportTunnelInfo> __Gen_Delegate_Imp2017(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBuildingInfo> __Gen_Delegate_Imp2018(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildMemberInfo> __Gen_Delegate_Imp2019(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp202(RuntimePlatform p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildSolarSystemInfo> __Gen_Delegate_Imp2020(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildStoreItemInfo> __Gen_Delegate_Imp2021(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildMineralBalanceInfo __Gen_Delegate_Imp2022(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildProduceInfo __Gen_Delegate_Imp2023(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBuildingBattleClientSyncInfo> __Gen_Delegate_Imp2024(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildPurchaseLogInfo> __Gen_Delegate_Imp2025(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFleetMemberBasicInfo> __Gen_Delegate_Imp2026(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFleetMemberDynamicInfo> __Gen_Delegate_Imp2027(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFleetDetailInfo __Gen_Delegate_Imp2028(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBattlePlayerKillLostStatInfo> __Gen_Delegate_Imp2029(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp203(object p0, object p1, object p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBattleGuildKillLostStatInfo> __Gen_Delegate_Imp2030(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBattleShipLostStatInfo> __Gen_Delegate_Imp2031(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBattleGuildBuildingSimpleInfo> __Gen_Delegate_Imp2032(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildSentryPlayerInfo> __Gen_Delegate_Imp2033(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildSentryInterestSceneShipTypeInfo> __Gen_Delegate_Imp2034(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildOccupySolarSystemInfo> __Gen_Delegate_Imp2035(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAllianceOccupySolarSystemInfo> __Gen_Delegate_Imp2036(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProStarMapGuildSimpleInfo> __Gen_Delegate_Imp2037(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProStarMapAllianceSimpleInfo> __Gen_Delegate_Imp2038(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSolarSystemDynamicInfo> __Gen_Delegate_Imp2039(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo __Gen_Delegate_Imp204(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildTradePurchaseOrderTransportLogInfo> __Gen_Delegate_Imp2040(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProAllianceInviteInfo> __Gen_Delegate_Imp2041(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLossCompensation> __Gen_Delegate_Imp2042(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFlagShipHangarVersionInfo __Gen_Delegate_Imp2043(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFlagShipHangarDetailInfo __Gen_Delegate_Imp2044(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFlagShipHangarVersionInfo> __Gen_Delegate_Imp2045(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFlagShipHangarDetailInfo> __Gen_Delegate_Imp2046(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFlagShipHangarShipSupplementFuelInfo> __Gen_Delegate_Imp2047(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFlagShipOptLogInfo> __Gen_Delegate_Imp2048(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFlagShipHangarBasicInfo __Gen_Delegate_Imp2049(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp205(object p0, object p1, UIProcess.ProcessExecMode p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFlagShipDetailInfo> __Gen_Delegate_Imp2050(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProOffLineBufInfo> __Gen_Delegate_Imp2051(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildFleetSimpleInfo> __Gen_Delegate_Imp2052(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSovereignLostReport> __Gen_Delegate_Imp2053(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildProductionLineInfo> __Gen_Delegate_Imp2054(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFleetDSInitInfo __Gen_Delegate_Imp2055(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildInfo __Gen_Delegate_Imp2056(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildMomentsInfo> __Gen_Delegate_Imp2057(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildJoinApplyViewInfo> __Gen_Delegate_Imp2058(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProIFFStatePairInfo> __Gen_Delegate_Imp2059(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateController __Gen_Delegate_Imp206(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildStaffingLogInfo> __Gen_Delegate_Imp2060(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildMessageInfo> __Gen_Delegate_Imp2061(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildActionInfo> __Gen_Delegate_Imp2062(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildStoreItemListInfo> __Gen_Delegate_Imp2063(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildStoreItemUpdateInfo> __Gen_Delegate_Imp2064(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildDonateRankingItemInfo> __Gen_Delegate_Imp2065(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildCurrencyLogInfo> __Gen_Delegate_Imp2066(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildPurchaseOrderVersionInfo> __Gen_Delegate_Imp2067(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildPurchaseOrderListItemInfo> __Gen_Delegate_Imp2068(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSolarSystemGuildBattleStatusInfo> __Gen_Delegate_Imp2069(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp207(object p0, object p1, object p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemBasicInfoReq> __Gen_Delegate_Imp2070(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemBuildingInfoReq> __Gen_Delegate_Imp2071(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemBattleInfoReq> __Gen_Delegate_Imp2072(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemBasicInfoAck> __Gen_Delegate_Imp2073(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemBuildingInfoAck> __Gen_Delegate_Imp2074(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemBattleInfoAck> __Gen_Delegate_Imp2075(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBuildingExInfo> __Gen_Delegate_Imp2076(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattleClientSyncInfo __Gen_Delegate_Imp2077(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBattleClientSyncInfo> __Gen_Delegate_Imp2078(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildSentrySceneInfo> __Gen_Delegate_Imp2079(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ButtonEx.ButtonDoubleClickedEvent __Gen_Delegate_Imp208(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBenefitsInformationPoint> __Gen_Delegate_Imp2080(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildBenefitsCommon> __Gen_Delegate_Imp2081(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProUInt64IdValueInfo> __Gen_Delegate_Imp2082(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildTradePurchaseInfo> __Gen_Delegate_Imp2083(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildTradePortExtraInfo> __Gen_Delegate_Imp2084(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGuildTradeTransportInfo> __Gen_Delegate_Imp2085(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProBufInfo> __Gen_Delegate_Imp2086(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProHateTargetInfo> __Gen_Delegate_Imp2087(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProShipStoreItemInfo> __Gen_Delegate_Imp2088(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProFlagShipInitExtraInfo __Gen_Delegate_Imp2089(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ButtonEx.ButtonLongPressStartEvent __Gen_Delegate_Imp209(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSynEventList4ObjInfo __Gen_Delegate_Imp2090(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSpaceObjectEnterInfo> __Gen_Delegate_Imp2091(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSpaceObjectMoveStateSnapshot> __Gen_Delegate_Imp2092(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSynEventList4ObjInfo> __Gen_Delegate_Imp2093(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProShipMoveCmdInfo __Gen_Delegate_Imp2094(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSceneCameraNameObjIdPair> __Gen_Delegate_Imp2095(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<InSpaceNpcTalkerChatNtf> __Gen_Delegate_Imp2096(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProGlobalSceneInfo> __Gen_Delegate_Imp2097(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGlobalSceneInfo __Gen_Delegate_Imp2098(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSceneGuildBuildingExtraDataInfo> __Gen_Delegate_Imp2099(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp21(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public ButtonEx.ButtonLongPressingEvent __Gen_Delegate_Imp210(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProJumpCtxSnapshot __Gen_Delegate_Imp2100(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProArroundTargetCtxSnapshot __Gen_Delegate_Imp2101(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProShipSlotAmmoInfo> __Gen_Delegate_Imp2102(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProCaptainInfo __Gen_Delegate_Imp2103(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSceneNpcInteractionInfo __Gen_Delegate_Imp2104(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpacePlayerShipViewInfo __Gen_Delegate_Imp2105(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceNpcShipViewInfo __Gen_Delegate_Imp2106(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSimpleObjectViewInfo __Gen_Delegate_Imp2107(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSceneObjectViewInfo __Gen_Delegate_Imp2108(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProDropBoxViewInfo __Gen_Delegate_Imp2109(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ButtonEx.ButtonLongPressEndEvent __Gen_Delegate_Imp211(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceHiredCaptainShipViewInfo __Gen_Delegate_Imp2110(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLBProcessSingleUnitLaunchInfo> __Gen_Delegate_Imp2111(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessDroneLaunchInfo __Gen_Delegate_Imp2112(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLBProcessSingleSuperMissileUnitLaunchInfo> __Gen_Delegate_Imp2113(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBProcessSingleUnitLaunchInfo __Gen_Delegate_Imp2114(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLBSpaceProcessDroneLaunchInfo> __Gen_Delegate_Imp2115(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProBufInfo __Gen_Delegate_Imp2116(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessEquipAttachBufLaunch __Gen_Delegate_Imp2117(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventInteractionEnd __Gen_Delegate_Imp2118(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessBulletGunLaunch __Gen_Delegate_Imp2119(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp212(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessLaserLaunch __Gen_Delegate_Imp2120(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessDroneFighterLaunch __Gen_Delegate_Imp2121(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessDroneDefenderLaunch __Gen_Delegate_Imp2122(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessDroneSniperLaunch __Gen_Delegate_Imp2123(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessSuperRailgunLaunch __Gen_Delegate_Imp2124(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessSuperMissileLaunch __Gen_Delegate_Imp2125(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessSuperPlasmaLaunch __Gen_Delegate_Imp2126(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessSuperLaserLaunch __Gen_Delegate_Imp2127(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessSuperDroneLaunch __Gen_Delegate_Imp2128(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSynEventSetInteractionInfo __Gen_Delegate_Imp2129(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp213(object p0, object p1, Color p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventAttachBuf __Gen_Delegate_Imp2130(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventDetachBuf __Gen_Delegate_Imp2131(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSynEventOnDotDamage __Gen_Delegate_Imp2132(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSynEventOnOnHotHeal __Gen_Delegate_Imp2133(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessEquipBlinkLaunch __Gen_Delegate_Imp2134(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventNpcShipAnimEffect __Gen_Delegate_Imp2135(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessEquipInvisibleLaunch __Gen_Delegate_Imp2136(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventResetWeaponEquipGroupCD __Gen_Delegate_Imp2137(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessEquipAddEnergyAndAttachBufLaunch __Gen_Delegate_Imp2138(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessEquipAddShieldAndAttachBufLaunch __Gen_Delegate_Imp2139(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AudioClip __Gen_Delegate_Imp214(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessSuperEquipAoeLaunch __Gen_Delegate_Imp2140(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch __Gen_Delegate_Imp2141(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventSuperEnergyModify __Gen_Delegate_Imp2142(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch __Gen_Delegate_Imp2143(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessEquipChannelLaunch __Gen_Delegate_Imp2144(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessEquipChannelEnd __Gen_Delegate_Imp2145(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventSetEnergy __Gen_Delegate_Imp2146(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessEquipPeriodicDamageLaunch __Gen_Delegate_Imp2147(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSpaceProcessEquipPeriodicDamageEnd __Gen_Delegate_Imp2148(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventSetShipShield __Gen_Delegate_Imp2149(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp215(object p0, object p1, bool p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventKillOtherTarget __Gen_Delegate_Imp2150(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventStartJumping __Gen_Delegate_Imp2151(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventTacticalEquipTimeInfo __Gen_Delegate_Imp2152(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventOnBlinkEnd __Gen_Delegate_Imp2153(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventOnBlinkInterrupted __Gen_Delegate_Imp2154(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventResetTacticalEquipGroupCD __Gen_Delegate_Imp2155(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessEquipAntiJumpingForceShieldLaunch __Gen_Delegate_Imp2156(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProSpaceProcessSuperEquipTransformToTeleportTunnelLaunch __Gen_Delegate_Imp2157(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventSuperEquipDurationEnd __Gen_Delegate_Imp2158(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProLBSyncEventAttachBufDuplicate __Gen_Delegate_Imp2159(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIStateDesc __Gen_Delegate_Imp216(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLBSyncEvent> __Gen_Delegate_Imp2160(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProLBSyncEventOnHitInfo> __Gen_Delegate_Imp2161(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProStationDynamicNpcTalkerStateInfo> __Gen_Delegate_Imp2162(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProNpcDialogOptInfo> __Gen_Delegate_Imp2163(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public System.Type __Gen_Delegate_Imp2164(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<System.Type, int> __Gen_Delegate_Imp2165(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProQuestRewardInfo> __Gen_Delegate_Imp2166(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProTeamMemberInfo> __Gen_Delegate_Imp2167(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProTeamMemberInfo __Gen_Delegate_Imp2168(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProInfectInfo> __Gen_Delegate_Imp2169(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIStateDesc __Gen_Delegate_Imp217(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProSolarSystemPoolCountInfo> __Gen_Delegate_Imp2170(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProTradeListItemInfo> __Gen_Delegate_Imp2171(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProTradeNpcShopItemInfo> __Gen_Delegate_Imp2172(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<PlayerActivityInfo> __Gen_Delegate_Imp2173(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2174(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2175(object p0, int p1, out ConfigDataVitalityRewardInfo p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceCompBasicBase __Gen_Delegate_Imp2176(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceCompChatBase __Gen_Delegate_Imp2177(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceCompInviteBase __Gen_Delegate_Imp2178(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceCompMailBase __Gen_Delegate_Imp2179(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp218(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceCompMembersBase __Gen_Delegate_Imp2180(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceInviteInfo> __Gen_Delegate_Imp2181(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceMemberInfo> __Gen_Delegate_Imp2182(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<AuctionItemDetailInfo> __Gen_Delegate_Imp2183(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionItemDetailInfo __Gen_Delegate_Imp2184(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2185(object p0, ulong p1, AuctionItemState p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2186(object p0, int p1, ulong p2, int p3, int p4, long p5, out int p6, out int p7, out long p8)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2187(object p0, ulong p1, out int p2, out AuctionItemDetailInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp2188(long p0, long p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2189(object p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public HorizontalLayoutGroup __Gen_Delegate_Imp219(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2190(object p0, int p1, int p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassPeriodInfo __Gen_Delegate_Imp2191(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2192(object p0, KeyValuePair<int, ConfigDataBattlePassLevelInfo> p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2193(KeyValuePair<int, ConfigDataBattlePassLevelInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassLevelInfo __Gen_Delegate_Imp2194(KeyValuePair<int, ConfigDataBattlePassLevelInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2195(object p0, KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo> p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2196(KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassChallangeQuestGroupInfo __Gen_Delegate_Imp2197(KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2198(object p0, KeyValuePair<int, ConfigDataBattlePassChallangeQuestAccRewardInfo> p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassChallangeQuestAccRewardInfo __Gen_Delegate_Imp2199(KeyValuePair<int, ConfigDataBattlePassChallangeQuestAccRewardInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp22(object p0, object p1, object p2, bool p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp220(object p0, double p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2200(object p0, object p1, out Dictionary<CurrencyType, long> p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public CurrencyType __Gen_Delegate_Imp2201(object p0, BlackMarketShopItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CurrencyType __Gen_Delegate_Imp2202(object p0, BlackMarketShopPayType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2203(object p0, int p1, out ConfigDataBranchStoryQuestListInfo p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public int? __Gen_Delegate_Imp2204(object p0, int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2205(object p0, object p1, object p2, uint p3, uint p4, object p5, object p6, float p7, EquipType p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2206(object p0, object p1, object p2, uint p3, uint p4, object p5, int p6, object p7, float p8, EquipType p9)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBufInfo __Gen_Delegate_Imp2207(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpaceShip __Gen_Delegate_Imp2208(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProBufInfo __Gen_Delegate_Imp2209(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp221(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2210(object p0, ref float p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2211(object p0, object p1, object p2, uint p3, uint p4, object p5, float p6, EquipType p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2212(object p0, object p1, object p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2213(object p0, ulong p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2214(object p0, int p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2215(object p0, uint p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2216(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2217(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2218(object p0, BufGamePlayFlag p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2219(object p0, BufGamePlayFlag p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp222(object p0, long p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBufBase> __Gen_Delegate_Imp2220(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2221(int p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2222(object p0, double p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2223(int p0, EquipType p1, object p2, out float p3, out PropertiesId p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ShipAnimationType __Gen_Delegate_Imp2224(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int? __Gen_Delegate_Imp2225(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillInfo __Gen_Delegate_Imp2226(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillLevelInfo __Gen_Delegate_Imp2227(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDrivingLicenseInfo __Gen_Delegate_Imp2228(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2229(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp223(object p0, TimeSpan p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBCharacterStaticShipCaptain __Gen_Delegate_Imp2230(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBShipCaptain __Gen_Delegate_Imp2231(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2232(object p0, object p1, OperatieLogMoneyChangeType p2, OperateLogItemChangeType p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2233(object p0, uint p1, PlayerExpAddSrcType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2234(object p0, long p1, OperatieLogMoneyChangeType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2235(object p0, CurrencyType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2236(object p0, CurrencyType p1, long p2, OperatieLogMoneyChangeType p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2237(object p0, CurrencyType p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip __Gen_Delegate_Imp2238(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2239(object p0, Vector3D p1, Vector3D p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp224(object p0, long p1, int p2, ref int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2240(object p0, object p1, OperatieLogMoneyChangeType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2241(object p0, long p1)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2242(long p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBCharChipSlot> __Gen_Delegate_Imp2243(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBCharChipSlot __Gen_Delegate_Imp2244(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2245(object p0, int p1, int p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2246(object p0, int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2247(object p0, out int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBCharChipActiveSuit __Gen_Delegate_Imp2248(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CharChipSlotInfo __Gen_Delegate_Imp2249(object p0, int p1, int p2, int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp225(object p0, double p1, ref int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBBuffContainer __Gen_Delegate_Imp2250(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBCharChip __Gen_Delegate_Imp2251(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipInfo __Gen_Delegate_Imp2252(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSuitInfo __Gen_Delegate_Imp2253(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSlotInfo __Gen_Delegate_Imp2254(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, float> __Gen_Delegate_Imp2255(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2256(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditLevel __Gen_Delegate_Imp2257(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2258(object p0, float p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2259(object p0, int p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp226(object p0, float p1, ref int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2260(object p0, int p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2261(object p0, int p1, StationOpPermissionType p2)
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction __Gen_Delegate_Imp2262(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2263(object p0, GrandFaction p1)
        {
        }

        [MethodImpl(0x8000)]
        public CurrencyType __Gen_Delegate_Imp2264(object p0, GrandFaction p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2265(object p0, CurrencyType p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2266(object p0, PropertyCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2267(object p0, PropertiesId p1, out float p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2268(object p0, object p1, object p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2269(object p0, out int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp227(object p0, int p1, int p2, ref int p3)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime? __Gen_Delegate_Imp2270(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2271(object p0, bool p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPassiveSkill> __Gen_Delegate_Imp2272(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill __Gen_Delegate_Imp2273(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill __Gen_Delegate_Imp2274(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2275(object p0, int p1, int p2, out int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill __Gen_Delegate_Imp2276(object p0, int p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2277(object p0, int p1, out LBPassiveSkill p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2278(object p0, CustomizedParameter p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2279(object p0, DateTime p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp228(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2280(object p0, out int p1, ChatLanguageChannel p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2281(object p0, int p1, out int p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2282(object p0, int p1, out int p2, out int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public CrackSlotInfo __Gen_Delegate_Imp2283(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2284(object p0, double p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalDelegateBase __Gen_Delegate_Imp2285(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcCaptainStaticInfo> __Gen_Delegate_Imp2286(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcCaptainSimpleDynamicInfo> __Gen_Delegate_Imp2287(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2288(object p0, object p1, object p2, out double p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2289(object p0, ulong p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Font __Gen_Delegate_Imp229(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2290(object p0, ulong p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2291(object p0, ulong p1, out List<ItemInfo> p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp2292(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBDelegateMission> __Gen_Delegate_Imp2293(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBDelegateMission __Gen_Delegate_Imp2294(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<DelegateMissionInfo> __Gen_Delegate_Imp2295(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBDelegateMission __Gen_Delegate_Imp2296(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public PVPInvadeRescueInfo __Gen_Delegate_Imp2297(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2298(object p0, ulong p1, int p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPVPInvadeRescue> __Gen_Delegate_Imp2299(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AudioListener __Gen_Delegate_Imp23(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public FontStyle __Gen_Delegate_Imp230(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPInvadeRescue __Gen_Delegate_Imp2300(object p0, ulong p1, int p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPInvadeRescue __Gen_Delegate_Imp2301(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2302(object p0, bool p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateMissionState __Gen_Delegate_Imp2303(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticHiredCaptain> __Gen_Delegate_Imp2304(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<DelegateMissionInvadeInfo> __Gen_Delegate_Imp2305(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp2306(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2307(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2308(object p0, object p1, int p2, RankType p3, SubRankType p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2309(object p0, int p1, object p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp231(object p0, FontStyle p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2310(object p0, int p1, RankType p2, SubRankType p3, out List<ItemInfo> p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2311(object p0, DeplomacyOptType p1, object p2, uint p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2312(object p0, int p1, int p2, int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBDrivingLicense> __Gen_Delegate_Imp2313(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBDrivingLicense __Gen_Delegate_Imp2314(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2315(float p0, float p1, float p2, float p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2316(float p0, float p1, float p2, float p3, float p4, float p5, float p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2317(float p0, float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2318(float p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2319(float p0, float p1, float p2, float p3, float p4, float p5, float p6, float p7)
        {
        }

        [MethodImpl(0x8000)]
        public TextAnchor __Gen_Delegate_Imp232(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2320(float p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2321(float p0, float p1, float p2, float p3, float p4, float p5, float p6)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2322(float p0, float p1, float p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2323(float p0, float p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2324(float p0, float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8, float p9)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2325(float p0, float p1, float p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp2326(float p0, float p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp2327(int p0, float p1, double p2, double p3)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp2328(int p0, int p1, double p2, int p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2329(float p0, float p1, float p2, float p3, float p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp233(object p0, TextAnchor p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2330(object p0, SystemFuncType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2331(object p0, SystemFuncType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2332(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> __Gen_Delegate_Imp2333(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo __Gen_Delegate_Imp2334(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> __Gen_Delegate_Imp2335(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo>[] __Gen_Delegate_Imp2336(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> __Gen_Delegate_Imp2337(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> __Gen_Delegate_Imp2338(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2339(object p0, object p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public FrequentChangeText.NumberFormatType __Gen_Delegate_Imp234(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2340(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2341(object p0, int p1, ref int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2342(object p0, int p1, int p2, int p3, ref int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2343(object p0, object p1, int p2, int p3, object p4, out ulong p5, ref int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2344(object p0, object p1, ref int p2)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildDataContainerBase __Gen_Delegate_Imp2345(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAllianceInviteInfo> __Gen_Delegate_Imp2346(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2347(object p0, object p1, ulong p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo __Gen_Delegate_Imp2348(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo __Gen_Delegate_Imp2349(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp235(object p0, FrequentChangeText.NumberFormatType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAntiTeleportEffectInfo> __Gen_Delegate_Imp2350(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2351(object p0, DateTime p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType __Gen_Delegate_Imp2352(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2353(object p0, GuildAllianceLanguageType p1, GuildJoinPolicy p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LossCompensation> __Gen_Delegate_Imp2354(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2355(object p0, ulong p1, CompensationCloseReason p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2356(object p0, long p1, GuildCurrencyLogType p2, object p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2357(object p0, GuildCurrencyLogType p1, object p2, long p3, ulong p4, int p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer __Gen_Delegate_Imp2358(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer __Gen_Delegate_Imp2359(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp236(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<IGuildFlagShipHangarDataContainer> __Gen_Delegate_Imp2360(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2361(object p0, object p1, int p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2362(object p0, object p1, ulong p2, int p3, ulong p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2363(object p0, object p1, ulong p2, int p3, DateTime p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2364(object p0, object p1, ulong p2, int p3, int p4, int p5, DateTime p6, out int p7)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2365(object p0, object p1, ulong p2, int p3, int p4, DateTime p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2366(object p0, object p1, ulong p2, int p3, DateTime p4, object p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2367(object p0, object p1, ulong p2, int p3, object p4, DateTime p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public RankType __Gen_Delegate_Imp2368(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticFlagShip __Gen_Delegate_Imp2369(object p0, ulong p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp237(object p0, Color p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticFlagShip> __Gen_Delegate_Imp2370(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2371(object p0, ulong p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2372(object p0, DateTime p1, ulong p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo __Gen_Delegate_Imp2373(object p0, ulong p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo __Gen_Delegate_Imp2374(object p0, ulong p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<IStaticFlagShipDataContainer> __Gen_Delegate_Imp2375(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer __Gen_Delegate_Imp2376(object p0, int p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer __Gen_Delegate_Imp2377(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticFlagShip __Gen_Delegate_Imp2378(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> __Gen_Delegate_Imp2379(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public FrequentChangeText.TimeFormat __Gen_Delegate_Imp238(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFlagShipOptLogInfo> __Gen_Delegate_Imp2380(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2381(object p0, object p1, int p2, DateTime p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2382(object p0, object p1, ulong p2, DateTime p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2383(object p0, object p1, int p2, object p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2384(object p0, ulong p1, int p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticFlagShip __Gen_Delegate_Imp2385(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticFlagShip> __Gen_Delegate_Imp2386(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticFlagShip> __Gen_Delegate_Imp2387(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2388(object p0, object p1, ulong p2, int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2389(object p0, object p1, ulong p2, bool p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp239(object p0, FrequentChangeText.TimeFormat p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2390(object p0, object p1, ulong p2, object p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2391(object p0, object p1, ulong p2, object p3, bool p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2392(object p0, object p1, ulong p2, object p3, object p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2393(object p0, object p1, ulong p2, object p3, FleetPosition p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2394(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo __Gen_Delegate_Imp2395(object p0, ulong p1, FleetPosition p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetListInfo __Gen_Delegate_Imp2396(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetInfo __Gen_Delegate_Imp2397(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2398(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2399(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp24(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public FrequentChangeText.PreMark __Gen_Delegate_Imp240(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2400(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> __Gen_Delegate_Imp2401(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2402(object p0, object p1, OperateLogGuildStoreItemChangeType p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2403(object p0, StoreItemType p1, int p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2404(object p0, int p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2405(object p0, object p1, long p2, ItemSuspendReason p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2406(object p0, object p1, long p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2407(object p0, StoreItemType p1, int p2, long p3, ItemSuspendReason p4, int p5, ulong p6, OperateLogGuildStoreItemChangeType p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2408(object p0, object p1, long p2, long p3, OperateLogGuildStoreItemChangeType p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2409(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp241(object p0, FrequentChangeText.PreMark p1)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2410(object p0, ItemSuspendReason p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2411(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2412(object p0, StoreItemType p1, int p2, long p3, OperateLogGuildStoreItemChangeType p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public BuildingLostReport __Gen_Delegate_Imp2413(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMemberBase> __Gen_Delegate_Imp2414(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBase __Gen_Delegate_Imp2415(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBase __Gen_Delegate_Imp2416(object p0, ushort p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMomentsInfo> __Gen_Delegate_Imp2417(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2418(object p0, ulong p1, int p2, int p3, out int p4, out ulong p5, out int p6, out List<CostInfo> p7)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2419(object p0, ulong p1, DateTime p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp242(object p0, long p1, object p2, FrequentChangeText.PreMark p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2420(object p0, ulong p1, int p2, ulong p3, DateTime p4, out int p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2421(object p0, int p1, GuildProduceCategory p2, ulong p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> __Gen_Delegate_Imp2422(object p0, int p1, GuildProduceCategory p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2423(object p0, int p1, GuildProduceCategory p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildProductionLineInfo> __Gen_Delegate_Imp2424(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildProductionLineInfo __Gen_Delegate_Imp2425(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2426(object p0, ulong p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> __Gen_Delegate_Imp2427(object p0, int p1, GuildProduceCategory p2, object p3, int p4, int p5, out ulong p6, out int p7)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2428(object p0, int p1, GuildProduceCategory p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2429(object p0, int p1, GuildPropertiesId p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp243(object p0, double p1, object p2, int p3, FrequentChangeText.PreMark p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2430(object p0, int p1, GuildFlag p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildPurchaseOrderInfo> __Gen_Delegate_Imp2431(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemInfo> __Gen_Delegate_Imp2432(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBuildingInfo> __Gen_Delegate_Imp2433(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfo __Gen_Delegate_Imp2434(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingSolarSystemEffectInfo __Gen_Delegate_Imp2435(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2436(object p0, object p1, out int p2, out GuildBuildingInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo __Gen_Delegate_Imp2437(object p0, int p1, GuildBuildingType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo __Gen_Delegate_Imp2438(object p0, int p1, ulong p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2439(object p0, object p1, GuildPermission p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp244(object p0, DateTime p1, FrequentChangeText.TimeFormat p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfo> __Gen_Delegate_Imp2440(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfo> __Gen_Delegate_Imp2441(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTeleportTunnelEffectInfo __Gen_Delegate_Imp2442(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePortExtraInfo __Gen_Delegate_Imp2443(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradePortExtraInfo> __Gen_Delegate_Imp2444(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompActionBase __Gen_Delegate_Imp2445(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompDynamicBase __Gen_Delegate_Imp2446(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompFlagShipBase __Gen_Delegate_Imp2447(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompBasicLogicBase __Gen_Delegate_Imp2448(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompBattleBase __Gen_Delegate_Imp2449(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp245(object p0, TimeSpan p1, FrequentChangeText.TimeFormat p2)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildBenefitsBase __Gen_Delegate_Imp2450(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompCompensationBase __Gen_Delegate_Imp2451(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompCurrencyBase __Gen_Delegate_Imp2452(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompCurrencyLogBase __Gen_Delegate_Imp2453(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompDiplomacyBase __Gen_Delegate_Imp2454(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompFleetBase __Gen_Delegate_Imp2455(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompItemStoreBase __Gen_Delegate_Imp2456(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompJoinApplyBase __Gen_Delegate_Imp2457(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMemberListBase __Gen_Delegate_Imp2458(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMessageBoardBase __Gen_Delegate_Imp2459(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Text __Gen_Delegate_Imp246(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMiningBase __Gen_Delegate_Imp2460(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompProductionBase __Gen_Delegate_Imp2461(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompPropertiesCalculaterBase __Gen_Delegate_Imp2462(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompPurchaseOrderBase __Gen_Delegate_Imp2463(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompSimpleRuntimeBase __Gen_Delegate_Imp2464(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompSolarSystemCtxBase __Gen_Delegate_Imp2465(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompStaffingLogBase __Gen_Delegate_Imp2466(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompAllianceBase __Gen_Delegate_Imp2467(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMomentsBase __Gen_Delegate_Imp2468(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompTradeBase __Gen_Delegate_Imp2469(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Text __Gen_Delegate_Imp247()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompLostReportBase __Gen_Delegate_Imp2470(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompAntiTeleportBase __Gen_Delegate_Imp2471(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompTeleportTunnelBase __Gen_Delegate_Imp2472(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2473(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2474(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildJobType __Gen_Delegate_Imp2475()
        {
        }

        [MethodImpl(0x8000)]
        public Regex __Gen_Delegate_Imp2476()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2477(GuildBuildingType p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2478(object p0, GuildJobType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2479(object p0, long p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp248(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildJobType> __Gen_Delegate_Imp2480(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildJobType __Gen_Delegate_Imp2481(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2482(object p0, GuildPermission p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2483(object p0, out int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSentryInterestScene> __Gen_Delegate_Imp2484(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSentryInterestScene __Gen_Delegate_Imp2485(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetPersonalSetting __Gen_Delegate_Imp2486(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2487(object p0, GuildFleetPersonalSetting p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2488(object p0, int p1, int p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2489(object p0, int p1, int p2, int? p3, bool? p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp249(object p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapGuildOccupyStaticInfo __Gen_Delegate_Imp2490(object p0, int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapGuildOccupyDynamicInfo __Gen_Delegate_Imp2491(object p0, int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2492(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, uint> __Gen_Delegate_Imp2493(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyStaticInfo __Gen_Delegate_Imp2494(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo __Gen_Delegate_Imp2495(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2496(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipCustomTemplateInfo> __Gen_Delegate_Imp2497(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, List<ShipCustomTemplateInfo>> __Gen_Delegate_Imp2498(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipCustomTemplateInfo __Gen_Delegate_Imp2499(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public AudioMixerGroup __Gen_Delegate_Imp25(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp250(object p0, Vector2 p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2500(object p0, bool p1, int p2, ulong p3, out ShipCustomTemplateInfo p4, out List<ItemInfo> p5, out double p6, out List<ItemInfo> p7, out int p8)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2501(object p0, int p1, ShipEquipSlotType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipCustomTemplateInfo> __Gen_Delegate_Imp2502(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip __Gen_Delegate_Imp2503(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticPlayerShip> __Gen_Delegate_Imp2504(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip __Gen_Delegate_Imp2505(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip __Gen_Delegate_Imp2506(object p0, ulong p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticPlayerShipDataContainer __Gen_Delegate_Imp2507(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2508(object p0, int p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2509(object p0, int p1, bool p2, out int p3, out bool p4, out int p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp251(object p0, UILineInfo p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainFeatsLevelInfo __Gen_Delegate_Imp2510(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill __Gen_Delegate_Imp2511(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2512(object p0, long p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2513(object p0, long p1, int p2, out long p3, out bool p4, out bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2514(object p0, object p1, object p2, out List<LBNpcCaptainFeats> p3, out List<LBNpcCaptainFeats> p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2515(object p0, object p1, out List<LBNpcCaptainFeats> p2, out List<LBNpcCaptainFeats> p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBNpcCaptainFeats> __Gen_Delegate_Imp2516(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats __Gen_Delegate_Imp2517(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats __Gen_Delegate_Imp2518(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats __Gen_Delegate_Imp2519(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp252(object p0, Vector2 p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainShipGrowthLineInfo __Gen_Delegate_Imp2520(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticHiredCaptain.ShipInfo> __Gen_Delegate_Imp2521(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain.ShipInfo __Gen_Delegate_Imp2522(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain.ShipInfo __Gen_Delegate_Imp2523(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2524(object p0, SignalType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo __Gen_Delegate_Imp2525(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2526(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public BasicPropertiesInfo __Gen_Delegate_Imp2527(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public BasicPropertiesInfo __Gen_Delegate_Imp2528(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BasicPropertiesInfo __Gen_Delegate_Imp2529(ProfessionType p0, SubRankType p1, GrandFaction p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIVertex[] __Gen_Delegate_Imp253(object p0, Vector2 p1, Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2530(object p0, out int p1, out int p2, out int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public IdLevelInfo __Gen_Delegate_Imp2531(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainState __Gen_Delegate_Imp2532(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2533(object p0, SubRankType p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain __Gen_Delegate_Imp2534(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2535(object p0, object p1, long p2, out long p3, out bool p4, out bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2536(object p0, object p1, int p2, int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp2537(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp2538(object p0, object p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2539(object p0, ulong p1, int p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp254(object p0, Vector2 p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2540(object p0, ulong p1, int p2, out int p3, out List<ShipCompListConfInfo> p4, out float p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2541(object p0, object p1, out int p2, out List<ShipCompListConfInfo> p3, out float p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2542(object p0, int p1, int p2, ulong p3, out ulong p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2543(object p0, object p1, out LBStaticHiredCaptain p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ulong[] __Gen_Delegate_Imp2544(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain __Gen_Delegate_Imp2545(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptainShipBase __Gen_Delegate_Imp2546(object p0, int p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2547(object p0, StoreItemType p1, int p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ItemSuspendReason __Gen_Delegate_Imp2548(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2549(object p0, long p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIVertex[] __Gen_Delegate_Imp255(object p0, Vector2[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp2550(int p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2551(StoreItemType p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2552(int p0, StoreItemType p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2553(int p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2554(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2555(object p0, StoreItemType p1, int p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp2556(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> __Gen_Delegate_Imp2557(object p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> __Gen_Delegate_Imp2558(object p0, NormalItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2559(object p0, StoreItemType p1, int p2, long p3, out int p4, OperateLogItemChangeType p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp256(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2560(object p0, int p1, long p2, out int p3, OperateLogItemChangeType p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2561(object p0, StoreItemType p1, int p2, long p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2562(object p0, int p1, long p2, out LBStoreItem p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp2563(object p0, StoreItemType p1, int p2, long p3, bool p4, ItemSuspendReason p5, int p6, ulong p7, OperateLogItemChangeType p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2564(object p0, object p1, long p2, long p3, OperateLogItemChangeType p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2565(object p0, object p1, OperateLogItemChangeType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2566(object p0, object p1, long p2, bool p3, OperateLogItemChangeType p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2567(object p0, StoreItemType p1, int p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2568(object p0, int p1, StoreItemType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2569(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp257(object p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime __Gen_Delegate_Imp2570(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2571(object p0, int p1, ItemSuspendReason p2)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2572(object p0, int p1, StoreItemType p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<KillRecordInfo> __Gen_Delegate_Imp2573(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public KillRecordInfo __Gen_Delegate_Imp2574(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public IdLevelInfo __Gen_Delegate_Imp2575(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp2576(object p0, SpaceObjectType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2577(object p0, SpaceObjectType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2578(object p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp2579(Vector3D p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp258(object p0, out string p1, out string p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2580(Vector3D p0, float p1, Vector3D p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<DailyLoginRewardInfo> __Gen_Delegate_Imp2581(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DailyLoginRewardInfo __Gen_Delegate_Imp2582(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2583(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public MailType __Gen_Delegate_Imp2584(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPredefineMailInfo __Gen_Delegate_Imp2585(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoredMailInfo __Gen_Delegate_Imp2586(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2587(object p0, out int p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoredMail __Gen_Delegate_Imp2588(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoredMail __Gen_Delegate_Imp2589(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp259(object p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoredMail __Gen_Delegate_Imp2590(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2591(object p0, int p1, int p2, out bool p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2592(object p0, int p1, int p2, ref bool p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2593(object p0, int p1, int p2, out DateTime p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2594(object p0, out bool p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcTalkerBase __Gen_Delegate_Imp2595(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2596(object p0, NpcShopFlag p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataNpcShopItemInfo> __Gen_Delegate_Imp2597(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2598(object p0, int p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBProductionLine> __Gen_Delegate_Imp2599(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp26(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp260(object p0, int p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBProductionLine __Gen_Delegate_Imp2600(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2601(object p0, int p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2602(object p0, int p1, int p2, int p3, ulong p4, out ProductionLineInfo p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2603(object p0, int p1, int p2, int p3, out DateTime p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> __Gen_Delegate_Imp2604(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2605(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2606(float p0, int p1, long p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2607(StoreItemType p0, CostType p1)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2608(StoreItemType p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2609(object p0, PropertiesId p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp261(object p0, object p1, int p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2610(PropertiesId p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBQuestEnv __Gen_Delegate_Imp2611(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestInfo __Gen_Delegate_Imp2612(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> __Gen_Delegate_Imp2613(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public QuestSrcType __Gen_Delegate_Imp2614(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2615(object p0, QuestFlag p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestRewardInfo> __Gen_Delegate_Imp2616(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public QuestEnvirmentInfo __Gen_Delegate_Imp2617(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId __Gen_Delegate_Imp2618(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase __Gen_Delegate_Imp2619(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp262(object p0, Vector2 p1, Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2620(object p0, int p1, int p2, int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2621(object p0, int p1, out int p2, out ConfigDataFactionCreditQuestWeeklyRewardInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase __Gen_Delegate_Imp2622(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBQuestEnv __Gen_Delegate_Imp2623(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2624(object p0, object p1, out LBQuestEnv p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestWaitForAcceptInfo> __Gen_Delegate_Imp2625(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2626(object p0, int p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2627(object p0, int p1, int p2, bool p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBProcessingQuestBase> __Gen_Delegate_Imp2628(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase __Gen_Delegate_Imp2629(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp263(object p0, Vector2 p1, Vector2 p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2630(object p0, int p1, out List<LBProcessingQuestBase> p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2631(object p0, int p1, out int p2, bool p3, bool p4, bool p5, int p6, int p7)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId __Gen_Delegate_Imp2632(object p0, int p1, int p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2633(object p0, int p1, bool p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2634(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2635(object p0, object p1, int p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2636(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2637(object p0, object p1, CurrencyType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2638(object p0, object p1, CurrencyType p2, int p3, float p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2639(object p0, CurrencyType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp264(object p0, object p1, bool p2, int p3, Vector2 p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2640(object p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo __Gen_Delegate_Imp2641(object p0, object p1, StoreItemType p2, int p3, int p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp2642(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp2643(object p0, object p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2644(object p0, object p1, out float p2)
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo __Gen_Delegate_Imp2645(object p0, object p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2646(object p0, int p1, int p2, int p3, int p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2647(object p0, QuestPostEventType p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2648(object p0, RankingListType p1, int p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2649(object p0, RankingListType p1, int p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp265(object p0, object p1, Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeGiftPackage> __Gen_Delegate_Imp2650(object p0, GiftPackageCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBRechargeGiftPackage __Gen_Delegate_Imp2651(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBRechargeItem __Gen_Delegate_Imp2652(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeItem> __Gen_Delegate_Imp2653(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBRechargeMonthlyCard __Gen_Delegate_Imp2654(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeMonthlyCard> __Gen_Delegate_Imp2655(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeOrderInfo __Gen_Delegate_Imp2656(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2657(object p0, RechargeGoodsType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeOrderInfo> __Gen_Delegate_Imp2658(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBInSpaceShipHiredCaptain __Gen_Delegate_Imp2659(object p0, object p1, object p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public Rect __Gen_Delegate_Imp266(object p0, Vector2 p1, Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemNpcShipInstanceInfo __Gen_Delegate_Imp2660(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2661(object p0, object p1, int p2, int p3, object p4, object p5, object p6, uint p7)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<ActivityType, int> __Gen_Delegate_Imp2662(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<CurrencyType, ulong> __Gen_Delegate_Imp2663(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UserSettingInServer __Gen_Delegate_Imp2664(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2665(object p0, object p1, int p2, int p3, object p4, object p5, object p6, int p7, int p8, object p9, object p10, object p11)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpaceShipCaptain __Gen_Delegate_Imp2666(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2667(object p0, PropertyCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public int? __Gen_Delegate_Imp2668(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2669(object p0, ShipState p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp267(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSlotGroupInfo[] __Gen_Delegate_Imp2670(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AmmoInfo[] __Gen_Delegate_Imp2671(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipSlotGroupInfo> __Gen_Delegate_Imp2672(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2673(object p0, ShipEquipSlotType p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2674(object p0, int p1, StoreItemType p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2675(object p0, object p1, long p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ShipState __Gen_Delegate_Imp2676(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<object> __Gen_Delegate_Imp2677(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<KeyValuePair<uint, uint>> __Gen_Delegate_Imp2678(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemPlayerShipInstanceInfo __Gen_Delegate_Imp2679(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp268(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFlagShipRelativeData __Gen_Delegate_Imp2680(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemFlagShipInstanceInfoClient __Gen_Delegate_Imp2681(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipDestroyState __Gen_Delegate_Imp2682(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2683(object p0, ShipDestroyState p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<OffLineBufInfo> __Gen_Delegate_Imp2684(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DropInfo __Gen_Delegate_Imp2685(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2686(object p0, StoreItemType p1, int p2, long p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceDropBox __Gen_Delegate_Imp2687(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2688(object p0, object p1, FormationCreatingInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2689(object p0, uint p1, out Vector3D p2)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp269(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp2690(object p0, double p1, double p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp2691(object p0, int p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2692(object p0, FormationType p1, FormationCreatingInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public FormationType __Gen_Delegate_Imp2693(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp2694(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public IFFState __Gen_Delegate_Imp2695(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public IFFState __Gen_Delegate_Imp2696(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2697(object p0, object p1, IFFState p2)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<uint> __Gen_Delegate_Imp2698(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget __Gen_Delegate_Imp2699(object p0, double p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp27(object p0, object p1, PlaySoundOption p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp270(object p0, object p1, Vector2 p2, Vector2 p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2700(object p0, object p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2701(object p0, float p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> __Gen_Delegate_Imp2702(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2703(object p0, uint p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2704(object p0, BufType p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2705(object p0, int p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public ILBBufOwner __Gen_Delegate_Imp2706(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBShipFightBuf> __Gen_Delegate_Imp2707(object p0, ShipFightBufType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2708(object p0, ShipFightBufType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<LBShipFightBuf> __Gen_Delegate_Imp2709(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp271(object p0, Vector2 p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2710(object p0, EquipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<LBBufBase> __Gen_Delegate_Imp2711(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipFightBuf __Gen_Delegate_Imp2712(object p0, ShipFightBufType p1, bool p2, BufType p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneFighterLaunch __Gen_Delegate_Imp2713(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget __Gen_Delegate_Imp2714(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2715(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2716(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2717(object p0, object p1, bool p2, out int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<ILBSpaceTarget> __Gen_Delegate_Imp2718(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2719(object p0, object p1, bool p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideUIControllerBase __Gen_Delegate_Imp272(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2720(object p0, uint p1, out int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2721(object p0, uint p1, object p2, uint p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2722(object p0, float p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2723(object p0, bool p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2724(object p0, out int p1, out ShipEnergyRecoveryModify p2)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2725(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2726(object p0, float p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2727(object p0, SceneInteractionType p1, uint p2, uint p3, int p4, SceneInteractionFlag p5, int p6, uint p7)
        {
        }

        [MethodImpl(0x8000)]
        public SceneInteractionType __Gen_Delegate_Imp2728(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SceneInteractionFlag __Gen_Delegate_Imp2729(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp273(object p0, object p1, object p2, object p3, UIProcess.ProcessExecMode p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcInteractionTemplate __Gen_Delegate_Imp2730(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2731(object p0, uint p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2732(object p0, out bool p1, out bool p2, out bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ILBScene __Gen_Delegate_Imp2733(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TargetNoticeType __Gen_Delegate_Imp2734(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool? __Gen_Delegate_Imp2735(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2736(object p0, bool? p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2737(object p0, uint p1, uint p2, object p3, object p4, ShipEquipSlotType p5, int p6, EquipType p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2738(object p0, uint p1, uint p2, object p3, ShipEquipSlotType p4, int p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceProcessEquipLaunchSource __Gen_Delegate_Imp2739(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp274()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceProcessEquipChannelLaunchSource __Gen_Delegate_Imp2740(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceProcessEquipInvisibleLaunchSource __Gen_Delegate_Imp2741(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2742(object p0, uint p1, uint p2, LBSpaceProcessType p3, object p4, object p5, ShipEquipSlotType p6, int p7, EquipType p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2743(object p0, uint p1, uint p2, uint p3, object p4, object p5, ShipEquipSlotType p6, int p7, EquipType p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2744(object p0, uint p1, uint p2, uint p3, LBSpaceProcessType p4, object p5, object p6, ShipEquipSlotType p7, int p8, EquipType p9, object p10)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2745(object p0, uint p1, uint p2, LBSpaceProcessType p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessType __Gen_Delegate_Imp2746(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2747(object p0, uint p1, uint p2, LBSpaceProcessType p3, object p4, object p5, int p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public byte __Gen_Delegate_Imp2748(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2749(object p0, uint p1, uint p2, uint p3, object p4, ShipEquipSlotType p5, int p6, EquipType p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp275(object p0, string[] p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2750(object p0, uint p1, uint p2, LBSpaceProcessType p3, object p4, EquipType p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBulletDamageInfo> __Gen_Delegate_Imp2751(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2752(object p0, float p1, float p2, float p3, float p4, float p5, float p6, float p7, float p8, float p9, WeaponCategory p10, float p11, float p12, float p13, float p14)
        {
        }

        [MethodImpl(0x8000)]
        public LBBulletDamageInfo __Gen_Delegate_Imp2753(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2754(object p0, uint p1, uint p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2755(object p0, uint p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> __Gen_Delegate_Imp2756(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2757(object p0, ushort p1, uint p2, uint p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2758(object p0, uint p1, uint p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2759(object p0, uint p1, uint p2, object p3, object p4, int p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp276(object p0, object p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBProcessSingleUnitLaunchInfo> __Gen_Delegate_Imp2760(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessSingleUnitLaunchInfo __Gen_Delegate_Imp2761(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceProcessWeaponLaunchSource __Gen_Delegate_Imp2762(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2763(object p0, uint p1, uint p2, object p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneDefenderLaunch.AttackChance __Gen_Delegate_Imp2764(object p0, byte p1, byte p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSpaceProcessDroneDefenderLaunch.AttackChance> __Gen_Delegate_Imp2765(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneDefenderLaunch.AttackChance __Gen_Delegate_Imp2766(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2767(object p0, uint p1, uint p2, object p3, object p4, int p5, object p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDroneInfo __Gen_Delegate_Imp2768(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2769(object p0, ushort p1, ushort p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp277(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2770(object p0, ushort p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2771(object p0, KeyValuePair<ushort, uint> p1, KeyValuePair<ushort, uint> p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<ushort, uint>> __Gen_Delegate_Imp2772(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DroneFighterState __Gen_Delegate_Imp2773(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2774(object p0, DroneFighterState p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceProcessDroneLaunchSource __Gen_Delegate_Imp2775(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2776(object p0, uint p1, uint p2, object p3, object p4, int p5, object p6, ShipEquipSlotType p7, int p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2777(object p0, bool p1, bool p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2778(object p0, ushort p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceProcessLaserLaunchSource __Gen_Delegate_Imp2779(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp278(object p0, object p1, short p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2780(object p0, ushort p1, uint p2, uint p3, LBSpaceProcessType p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2781(object p0, out uint p1, out uint p2, out uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2782(object p0, uint p1, uint p2, object p3, object p4, int p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2783(object p0, float p1, int p2, out uint p3, out uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2784(object p0, int p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletFly __Gen_Delegate_Imp2785(object p0, byte p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2786(object p0, object p1, object p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2787(object p0, uint p1, bool p2, bool p3, float p4, bool p5, bool p6, bool p7, bool p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2788(object p0, uint p1, int p2, float? p3, float? p4, float? p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2789(object p0, object p1, TargetNoticeType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp279(object p0, object p1, ushort p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2790(object p0, int p1, uint p2, int p3, uint p4, object p5, float p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2791(object p0, uint p1, float p2, BufType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2792(object p0, ShipEquipSlotType p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2793(object p0, ShipEquipSlotType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public SyncEventFlag __Gen_Delegate_Imp2794(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSyncEventOnHitInfo> __Gen_Delegate_Imp2795(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSyncEvent> __Gen_Delegate_Imp2796(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2797(PropertiesId p0, float p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2798(PropertiesId p0, float? p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2799(object p0, object p1, PropertiesId p2, out float p3)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp28(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp280(object p0, object p1, char p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2800(EquipType p0, PropertiesId p1, out float p2, float p3, float p4, float p5, float p6, float p7, float p8, float p9)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2801(object p0, object p1, PropertiesId p2, object p3, out float p4)
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionCompBlinkBase __Gen_Delegate_Imp2802(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesCalculaterBase __Gen_Delegate_Imp2803(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<int> __Gen_Delegate_Imp2804(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2805(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2806(object p0, object p1, float p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionCompInvisibleBase __Gen_Delegate_Imp2807(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2808(object p0, int p1, out float p2, out PropertiesId p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipEquipInfo __Gen_Delegate_Imp2809(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp281(object p0, object p1, byte p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2810(object p0, int p1, out PropertiesId p2)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2811(PropertiesId p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup __Gen_Delegate_Imp2812(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2813(object p0, PropertiesId p1, float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionCompInOutRangeTriggerBase __Gen_Delegate_Imp2814(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipRunningInfo __Gen_Delegate_Imp2815(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipInfo __Gen_Delegate_Imp2816(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionCompChannelBase __Gen_Delegate_Imp2817(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSuperWeaponInfo __Gen_Delegate_Imp2818(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ushort __Gen_Delegate_Imp2819(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp282(object p0, object p1, sbyte p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2820(object p0, uint p1, uint p2, uint p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2821(object p0, object p1, ushort p2, out LBSpaceProcessSuperLaserSingleFire p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperLaserSingleFire __Gen_Delegate_Imp2822(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2823(object p0, object p1, ushort p2, out LBSpaceProcessBulletFly p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletFly __Gen_Delegate_Imp2824(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBBulletDamageInfo __Gen_Delegate_Imp2825(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2826(object p0, object p1, ushort p2, out LBSpaceProcessSuperRailgunBulletFly p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperRailgunBulletFly __Gen_Delegate_Imp2827(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipTacticalEquipInfo __Gen_Delegate_Imp2828(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2829(object p0, object p1, PropertiesId p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp283(object p0, object p1, double p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2830(object p0, PropertiesId p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp2831(object p0, object p1, object p2, EquipType p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2832(object p0, PropertiesId p1, out float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2833(object p0, PropertiesId p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2834(object p0, ref float p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2835(object p0, ref float p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionCompKillingHitTriger __Gen_Delegate_Imp2836(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWeaponInfo __Gen_Delegate_Imp2837(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public uint? __Gen_Delegate_Imp2838(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2839(object p0, int p1, StoreItemType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp284(object p0, object p1, decimal p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2840(object p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2841(object p0, object p1, ushort p2, out LBSpaceProcessLaserSingleFire p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessLaserSingleFire __Gen_Delegate_Imp2842(object p0, ushort p1, object p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2843(object p0, object p1, out LBInSpaceWeaponEquipGroupBase p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2844(object p0, out LBInSpaceWeaponEquipGroupBase p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2845(object p0, out LBTacticalEquipGroupBase p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBInSpaceWeaponEquipGroupBase __Gen_Delegate_Imp2846(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSuperWeaponGroupBase __Gen_Delegate_Imp2847(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupBase __Gen_Delegate_Imp2848(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupBase __Gen_Delegate_Imp2849(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp285(object p0, object p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBInSpaceWeaponEquipGroupBase __Gen_Delegate_Imp2850(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> __Gen_Delegate_Imp2851(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<IPropertiesProvider> __Gen_Delegate_Imp2852(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupBase __Gen_Delegate_Imp2853(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBWeaponGroupBase> __Gen_Delegate_Imp2854(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupBase __Gen_Delegate_Imp2855(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBEquipGroupBase> __Gen_Delegate_Imp2856(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase __Gen_Delegate_Imp2857(object p0, ShipEquipSlotType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase __Gen_Delegate_Imp2858(object p0, EquipType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2859(object p0, EquipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp286(object p0, object p1, TimeSpan p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBSuperWeaponEquipGroupBase __Gen_Delegate_Imp2860(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase __Gen_Delegate_Imp2861(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2862(object p0, object p1, StoreItemType p2, int p3, int p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2863(object p0, object p1, StoreItemType p2, int p3, int p4, OperateLogItemChangeType p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2864(object p0, StoreItemType p1, int p2, int p3, OperateLogItemChangeType p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2865(object p0, bool p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpaceNpcShip __Gen_Delegate_Imp2866(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpacePlayerShip __Gen_Delegate_Imp2867(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemNpcShipInstanceInfo __Gen_Delegate_Imp2868(int p0, int p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelDropInfo __Gen_Delegate_Imp2869(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp287(object p0, object p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelBufInfo __Gen_Delegate_Imp2870(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemNpcShipInstanceInfo __Gen_Delegate_Imp2871(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase __Gen_Delegate_Imp2872(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBShipStoreItemBase> __Gen_Delegate_Imp2873(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase __Gen_Delegate_Imp2874(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase __Gen_Delegate_Imp2875(object p0, StoreItemType p1, int p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2876(object p0, object p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2877(object p0, object p1, out List<LBStoreItem> p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2878(object p0, object p1, out List<LBShipStoreItemBase> p2)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp2879(object p0, StoreItemType p1, int p2, long p3, bool p4, bool p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp288(object p0, object p1, out object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase __Gen_Delegate_Imp2880(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2881(object p0, object p1, long p2, OperateLogItemChangeType p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase __Gen_Delegate_Imp2882(object p0, int p1, StoreItemType p2, int p3, bool p4, bool p5, long p6, ulong p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2883(object p0, OperateLogItemChangeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2884(object p0, float p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2885(object p0, int p1, out float p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipConfigPropertiesProvider __Gen_Delegate_Imp2886(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticCaptain __Gen_Delegate_Imp2887(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticWeaponEquipBase __Gen_Delegate_Imp2888(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBPlayerContext __Gen_Delegate_Imp2889(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIIntent __Gen_Delegate_Imp289(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2890(object p0, ShipFlag p1)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2891(object p0, PropertiesId p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2892(object p0, PropertyGroup p1)
        {
        }

        [MethodImpl(0x8000)]
        public IPropertiesProvider __Gen_Delegate_Imp2893(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticPlayerShipDataContainer __Gen_Delegate_Imp2894(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2895(object p0, out float p1, out float p2, out float p3)
        {
        }

        [MethodImpl(0x8000)]
        public AmmoInfo __Gen_Delegate_Imp2896(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2897(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBWeaponEquipGroupBase __Gen_Delegate_Imp2898(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2899(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp29(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIManager __Gen_Delegate_Imp290()
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2900(WeaponCategory p0)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2901(EquipType p0)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2902(WeaponCategory p0, ShipSizeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2903(object p0, ShipEquipSlotType p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup __Gen_Delegate_Imp2904(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2905(object p0, bool p1, bool p2, object p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2906(object p0, object p1, object p2, int p3, int p4, bool p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2907(object p0, object p1, out int p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2908(object p0, object p1, object p2, int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2909(object p0, object p1, int p2, StoreItemType p3, int p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp291(object p0, object p1, bool p2, bool p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2910(object p0, object p1, StoreItemType p2, int p3, int p4, OperateLogItemChangeType p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2911(object p0, StoreItemType p1, int p2, int p3, OperateLogItemChangeType p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2912(object p0, object p1, object p2, out int p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupEnv __Gen_Delegate_Imp2913(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2914(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2915(object p0, int p1, int p2, out ItemInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2916(object p0, object p1, StoreItemType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2917(object p0, object p1, out AmmoInfo p2, out long p3)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp2918(object p0, out List<AmmoInfo> p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2919(object p0, object p1, object p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp292(object p0, object p1, object p2, bool p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2920(object p0, RankType p1, SubRankType p2, long p3, RankType p4, SubRankType p5, long p6)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2921(object p0, WeaponCategory p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2922(object p0, WeaponCategory p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp2923(object p0, WeaponCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public ushort __Gen_Delegate_Imp2924(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp2925(object p0, WeaponCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public ScanProbeType __Gen_Delegate_Imp2926(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2927(object p0, ScanProbeType p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2928(object p0, ulong p1, out LBSignalManualQuest p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2929(object p0, ulong p1, object p2, out List<LBStaticHiredCaptain> p3, out LBSignalBase p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp293(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalBase __Gen_Delegate_Imp2930(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalBase __Gen_Delegate_Imp2931(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualBase __Gen_Delegate_Imp2932(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualBase __Gen_Delegate_Imp2933(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPSignal __Gen_Delegate_Imp2934(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSignalDelegateBase> __Gen_Delegate_Imp2935(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSignalManualBase> __Gen_Delegate_Imp2936(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPVPSignal> __Gen_Delegate_Imp2937(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<ulong, LBSignalBase> __Gen_Delegate_Imp2938(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2939(object p0, ScanProbeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp294(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBScanProbe __Gen_Delegate_Imp2940(object p0, ScanProbeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SignalInfo __Gen_Delegate_Imp2941(object p0, int p1, ulong p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<SignalInfo> __Gen_Delegate_Imp2942(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2943(object p0, int p1, SignalType p2)
        {
        }

        [MethodImpl(0x8000)]
        public SignalType __Gen_Delegate_Imp2944(object p0, EquipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> __Gen_Delegate_Imp2945(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcCaptainInfo> __Gen_Delegate_Imp2946(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSignalInfo __Gen_Delegate_Imp2947(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalBase __Gen_Delegate_Imp2948(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2949(object p0, int p1, Vector3D p2, object p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp295(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2950(object p0, int p1, object p2, bool p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public QuestCompleteCondType? __Gen_Delegate_Imp2951(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2952(object p0, Vector3D p1, Vector3D p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSolarSystemInfoBase.CelestialResult __Gen_Delegate_Imp2953(object p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo __Gen_Delegate_Imp2954(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2955(object p0, object p1, bool p2, int p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcTalkerBase __Gen_Delegate_Imp2956(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcTalkerBase __Gen_Delegate_Imp2957(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2958(object p0, SpaceStationRoomType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo __Gen_Delegate_Imp2959(object p0, GrandFaction p1)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp296(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2960(object p0, ClientOSType p1, object p2, LanguageType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2961(object p0, int p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechInfo __Gen_Delegate_Imp2962(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechLevelInfo __Gen_Delegate_Imp2963(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesCalculater __Gen_Delegate_Imp2964(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBTech> __Gen_Delegate_Imp2965(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBTech __Gen_Delegate_Imp2966(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechLevelInfo __Gen_Delegate_Imp2967(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<TechUpgradeInfo> __Gen_Delegate_Imp2968(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeInfo __Gen_Delegate_Imp2969(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp297(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesCalculaterTiny __Gen_Delegate_Imp2970(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2971(object p0, int p1, int p2, ulong p3, out List<CostInfo> p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> __Gen_Delegate_Imp2972(object p0, int p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2973(float p0, int p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp2974(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2975(TechType p0, CostType p1)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId __Gen_Delegate_Imp2976(TechType p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2977(object p0, int p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2978(object p0, object p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2979(object p0, int p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp298(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceCompBasicClient __Gen_Delegate_Imp2980(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceCompChatClient __Gen_Delegate_Imp2981(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp2982(object p0, int p1, object p2, ChatExtraOutputLocationType p3)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceCompInviteClient __Gen_Delegate_Imp2983(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceCompMailClient __Gen_Delegate_Imp2984(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceCompMembersClient __Gen_Delegate_Imp2985(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceDCBasicClient __Gen_Delegate_Imp2986(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceCompOwnerClient __Gen_Delegate_Imp2987(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceDCInviteClient __Gen_Delegate_Imp2988(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAllianceDCMembersClient __Gen_Delegate_Imp2989(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp299(object p0, object p1, UIManager.UITaskNotifyType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2990(object p0, GuildAllianceLanguageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceInviteInfo __Gen_Delegate_Imp2991(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2992(object p0, uint p1, GuildAllianceLeaveReason p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2993(object p0, GuildAllianceLeaveReason p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2994(object p0, ulong p1, int p2, long p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp2995(object p0, object p1, uint p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2996(object p0, int p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase __Gen_Delegate_Imp2997(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBBuffFactory __Gen_Delegate_Imp2998()
        {
        }

        [MethodImpl(0x8000)]
        public List<CharChipSchemeInfo> __Gen_Delegate_Imp2999(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp30(object p0, CriAtomExPlayback p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp300(object p0, object p1, object p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public CharChipSchemeInfo __Gen_Delegate_Imp3000(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3001(object p0, CurrencyUpdateInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPassiveSkill> __Gen_Delegate_Imp3002(object p0, PropertyCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPassiveSkill> __Gen_Delegate_Imp3003(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataPassiveSkillInfo> __Gen_Delegate_Imp3004(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3005(object p0, ChatChannel p1, ChatLanguageChannel p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3006(object p0, ChatChannel p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<ChatInfo> __Gen_Delegate_Imp3007(object p0, ChatChannel p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3008(object p0, ChatChannel p1, ChatLanguageChannel p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3009(object p0, ChatChannel p1, object p2, ChatLanguageChannel p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp301(object p0, uint p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3010(object p0, ChatChannel p1, ChatLanguageChannel p2)
        {
        }

        [MethodImpl(0x8000)]
        public ChatInfo __Gen_Delegate_Imp3011(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ChatChannel __Gen_Delegate_Imp3012(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3013(object p0, int p1, int p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3014(object p0, int p1, int p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3015(object p0, object p1, out long p2)
        {
        }

        [MethodImpl(0x8000)]
        public IClientCustomDataContainer __Gen_Delegate_Imp3016(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3017(object p0, bool p1, ulong p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3018(object p0, ulong p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3019(object p0, DeplomacyOptType p1, object p2, uint p3, uint p4, ushort p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp302(object p0, object p1, object p2, UIManager.FullScreenActionPriority p3)
        {
        }

        [MethodImpl(0x8000)]
        public TargetListViewState __Gen_Delegate_Imp3020(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public TargetListViewState __Gen_Delegate_Imp3021(object p0, object p1, IFFState p2)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyState __Gen_Delegate_Imp3022(object p0, object p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildDataContainerClient __Gen_Delegate_Imp3023(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3024(object p0, GuildAllianceLanguageType p1, GuildJoinPolicy p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3025(object p0, out List<GuildCommonBenefitsInfo> p1, out GuildSovereigntyBenefitsInfo p2, out List<GuildInformationPointBenefitsInfo> p3, DateTime p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3026(object p0, DateTime p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3027(object p0, long p1, GuildCurrencyLogType p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildCurrencyLogInfo> __Gen_Delegate_Imp3028(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3029(object p0, ulong p1, ulong p2, int p3, ulong p4, int p5, DateTime p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp303(object p0, UIProcess[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3030(object p0, ulong p1, int p2, int p3, ulong p4, object p5, out LBStoreItem p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3031(object p0, ulong p1, int p2, ulong p3, int p4, int p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo __Gen_Delegate_Imp3032(object p0, ulong p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3033(object p0, int p1, ulong p2, StoreItemType p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3034(object p0, StoreItemType p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3035(object p0, object p1, int p2, ItemSuspendReason p3, int p4, ulong p5, out LBStoreItem p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3036(object p0, object p1, int p2, int p3, ulong p4, out LBStoreItem p5)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp3037(object p0, int p1, StoreItemType p2, int p3, long p4, ulong p5, uint p6)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp3038(object p0, int p1, object p2, bool p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<SovereignLostReport> __Gen_Delegate_Imp3039(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp304(object p0, UIProcess.StopOption p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3040(object p0, GuildJobType p1, object p2, bool p3, DateTime p4)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberInfo __Gen_Delegate_Imp3041(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3042(object p0, object p1, object p2, object p3, object p4, uint p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3043(object p0, bool p1, ulong p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompProtocolClient __Gen_Delegate_Imp3044(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompAllianceClient __Gen_Delegate_Imp3045(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompAntiTeleportClient __Gen_Delegate_Imp3046(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompBasicLogicClient __Gen_Delegate_Imp3047(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompBattleClient __Gen_Delegate_Imp3048(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompBenefitsClient __Gen_Delegate_Imp3049(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp305(object p0, UIProcess.StopOption p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompCompensationClient __Gen_Delegate_Imp3050(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompCurrencyClient __Gen_Delegate_Imp3051(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompCurrencyLogClient __Gen_Delegate_Imp3052(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompDiplomacyClient __Gen_Delegate_Imp3053(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompFlagShipClient __Gen_Delegate_Imp3054(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompFleetClient __Gen_Delegate_Imp3055(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompItemStoreClient __Gen_Delegate_Imp3056(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompLostReportClient __Gen_Delegate_Imp3057(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompMemberListClient __Gen_Delegate_Imp3058(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompMiningClient __Gen_Delegate_Imp3059(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess.DebugOnEndFunc __Gen_Delegate_Imp306(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompMomentsClient __Gen_Delegate_Imp3060(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompProductionClient __Gen_Delegate_Imp3061(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompPurchaseOrderClient __Gen_Delegate_Imp3062(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompSolarSystemCtxClient __Gen_Delegate_Imp3063(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompTeleportTunnelClient __Gen_Delegate_Imp3064(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompTradeClient __Gen_Delegate_Imp3065(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3066(object p0, object p1, object p2, object p3, uint p4, object p5, uint p6)
        {
        }

        [MethodImpl(0x8000)]
        public GuildClient __Gen_Delegate_Imp3067(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcGuildInfo __Gen_Delegate_Imp3068(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3069(object p0, uint p1, out GuildBasicInfo p2, out GuildSimpleRuntimeInfo p3, out GuildDynamicInfo p4, out List<GuildMomentsInfo> p5)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess.UIProcessState __Gen_Delegate_Imp307(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3070(object p0, object p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp3071(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildClient.GuildStaffingLogFullInfo __Gen_Delegate_Imp3072(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildClient.GuildApplyListInfo __Gen_Delegate_Imp3073(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildClient.GuildMessageCacheInfo __Gen_Delegate_Imp3074(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, List<GuildActionInfo>> __Gen_Delegate_Imp3075(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildActionInfo __Gen_Delegate_Imp3076(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfoClient __Gen_Delegate_Imp3077(object p0, object p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfoClient __Gen_Delegate_Imp3078(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3079(object p0, int p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp308(object p0, UIProcess.UIProcessState p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ProSolarSystemGuildBattleStatusInfo> __Gen_Delegate_Imp3080(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3081(object p0, int p1, uint p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemGuildBattleStatus __Gen_Delegate_Imp3082(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3083(object p0, object p1, object p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<LossCompensation> __Gen_Delegate_Imp3084(object p0, ShipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBenefitsInfoBase __Gen_Delegate_Imp3085(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3086(object p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBattleSimpleReportInfo> __Gen_Delegate_Imp3087(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleReportInfo __Gen_Delegate_Imp3088(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3089(object p0, object p1, object p2, LogicBlockGuildClient.MemberTitleArrowState p3, GuildJobType p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp309(object p0, UIProcess.ProcessExecMode p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3090(object p0, object p1, object p2, GuildJobType p3, LogicBlockGuildClient.MemberTitleArrowState p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3091(object p0, object p1, object p2, LogicBlockGuildClient.MemberTitleType p3, LogicBlockGuildClient.MemberTitleArrowState p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3092(object p0, LogicBlockGuildClient.MemberTitleArrowState p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3093(object p0, LogicBlockGuildClient.MemberTitleArrowState p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildClient.MemberTitleArrowState __Gen_Delegate_Imp3094(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3095(object p0, LogicBlockGuildClient.MemberTitleType p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildClient.MemberTitleType __Gen_Delegate_Imp3096(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3097(object p0, uint p1, out List<GuildMomentsInfo> p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3098(object p0, object p1, out List<GuildSimplestInfo> p2, ref List<uint> p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimplestInfo __Gen_Delegate_Imp3099(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp31(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp310(UIProcess.ProcessExecMode p0, UIPlayingEffectInfo[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3100(object p0, object p1, out List<AllianceBasicInfo> p2, ref List<uint> p3)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceBasicInfo __Gen_Delegate_Imp3101(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfoClient> __Gen_Delegate_Imp3102(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildDiplomacyFriendlyViewInfo __Gen_Delegate_Imp3103(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildDiplomacyEnemyViewInfo __Gen_Delegate_Imp3104(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBPlayerContextClient __Gen_Delegate_Imp3105(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3106(object p0, ulong p1, int p2, ulong p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3107(object p0, ulong p1, int p2, DateTime p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3108(object p0, ulong p1, int p2, int p3, int p4, DateTime p5, out int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3109(object p0, ulong p1, int p2, int p3, DateTime p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess __Gen_Delegate_Imp311(UIProcess.ProcessExecMode p0, CustomUIProcess.UIProcessExecutor[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3110(object p0, ulong p1, object p2, DateTime p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3111(object p0, ulong p1, int p2, DateTime p3, object p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3112(object p0, ulong p1, int p2, object p3, DateTime p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3113(object p0, ulong p1, uint p2, uint p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFleetSimpleInfo> __Gen_Delegate_Imp3114(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3115(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3116(object p0, ulong p1, out uint p2, out uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFleetMemberDynamicInfo> __Gen_Delegate_Imp3117(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3118(object p0, ulong p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3119(object p0, ulong p1, object p2, FleetPosition p3)
        {
        }

        [MethodImpl(0x8000)]
        public TaskNotifyUIProcess __Gen_Delegate_Imp312(UIProcess.ProcessExecMode p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3120(object p0, ulong p1, bool p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3121(object p0, ulong p1, object p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3122(object p0, ulong p1, object p2, object p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3123(object p0, ulong p1, object p2, FleetPosition p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo __Gen_Delegate_Imp3124(object p0, FleetPosition p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3125(object p0, GuildFleetOperationType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3126(object p0, GuildFleetOperationType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3127(object p0, ulong p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp3128(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradePurchaseInfo> __Gen_Delegate_Imp3129(object p0, int p1, StoreItemType p2)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskPipeLineCtx __Gen_Delegate_Imp313(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePurchaseInfo __Gen_Delegate_Imp3130(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradeTransportInfo __Gen_Delegate_Imp3131(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, GuildTradeTransportInfo> __Gen_Delegate_Imp3132(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradeTransportInfo> __Gen_Delegate_Imp3133(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3134(object p0, int p1, StoreItemType p2, object p3, object p4, bool p5, DateTime p6)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBuildingExInfo __Gen_Delegate_Imp3135(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBattleClientSyncInfo __Gen_Delegate_Imp3136(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public SovereignBattleSimpleInfo __Gen_Delegate_Imp3137(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBuildingBattleClientSyncInfo __Gen_Delegate_Imp3138(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceInfoDataDict __Gen_Delegate_Imp3139()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp314(object p0, object p1, bool p2, bool p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3140(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceInfo __Gen_Delegate_Imp3141(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetOperationType __Gen_Delegate_Imp3142(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3143(object p0, GuildFleetOperationType p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public ushort __Gen_Delegate_Imp3144(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp3145(object p0, int p1, out bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3146(object p0, bool p1, int p2, ulong p3, object p4, object p5, object p6, CurrencyUpdateInfo p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public ShipCustomTemplateInfo __Gen_Delegate_Imp3147(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3148(object p0, ulong p1, int p2, ulong p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3149(object p0, int p1, int p2, ulong p3, object p4, object p5, object p6, out LBStoreItem p7)
        {
        }

        [MethodImpl(0x8000)]
        public List<UITaskBase.LayerDesc> __Gen_Delegate_Imp315(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3150(object p0, ulong p1, ShipDestroyState p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockItemStoreClient __Gen_Delegate_Imp3151(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3152(object p0, int p1, bool p2, bool p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3153(object p0, ulong p1, object p2, out List<LBNpcCaptainFeats> p3, out List<LBNpcCaptainFeats> p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3154(object p0, object p1, int p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain __Gen_Delegate_Imp3155(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3156(object p0, object p1, int p2, object p3, out List<LBNpcCaptainFeats> p4, out List<LBNpcCaptainFeats> p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3157(object p0, ulong p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3158(object p0, int p1, int p2, out LogicBlockCompInfectClient.SolarSystemInfectInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3159(object p0, int p1, out DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp316(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3160(object p0, uint p1, int p2, long p3, long p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3161(object p0, int p1, float p2, bool p3, long p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, LogicBlockCompInfectClient.SolarSystemInfectInfo> __Gen_Delegate_Imp3162(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp3163(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3164(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CommonPropertyInfo> __Gen_Delegate_Imp3165(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3166(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3167(StoreItemType p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3168(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3169(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp317(object p0, object p1, bool p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3170(CurrencyType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3171(ShipEquipSlotType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3172(ShipType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3173(RankType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3174(SubRankType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3175(ShipSizeType p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSizeType __Gen_Delegate_Imp3176(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3177(StoreItemType p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3178(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3179(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase.LayerDesc __Gen_Delegate_Imp318(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3180(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemType __Gen_Delegate_Imp3181(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public BlueprintType __Gen_Delegate_Imp3182(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public BlueprintCategory __Gen_Delegate_Imp3183(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemObtainSourceType> __Gen_Delegate_Imp3184(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataItemObtainSourceTypeInfo> __Gen_Delegate_Imp3185(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3186(StoreItemType p0, int p1, StoreItemType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3187(EquipType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3188(DroneType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3189(NormalItemType p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp319(object p0, object p1, string[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3190(WeaponType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3191(EquipCategory p0)
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionType __Gen_Delegate_Imp3192(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3193(object p0, out bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3194(StoreItemType p0, object p1, out bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDrivingLicenseInfo __Gen_Delegate_Imp3195(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSuperWeaponInfo __Gen_Delegate_Imp3196(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipInfo __Gen_Delegate_Imp3197(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipTacticalEquipInfo __Gen_Delegate_Imp3198(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public WeaponCategory __Gen_Delegate_Imp3199(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp32(object p0, CriAtomExPlayback p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp320(object p0, UIManager.UITaskNotifyType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3200(object p0, int p1, ulong p2, StoreItemType p3, int p4, bool p5, long p6, LogicBlockItemStoreClient.ItemUpdateReason p7)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3201(object p0, StoreItemUpdateInfo p1, LogicBlockItemStoreClient.ItemUpdateReason p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3202(object p0, object p1, out LBStoreItem p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3203(object p0, int p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3204(object p0, StoreItemType p1, int p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3205(object p0, object p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3206(object p0, int p1, long p2, out double p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3207(object p0, object p1, out double p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3208(object p0, object p1, long p2, ItemSuspendReason p3, int p4, ulong p5, out LBStoreItem p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3209(object p0, object p1, long p2, int p3, ulong p4, out LBStoreItem p5)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase.LayerDesc[] __Gen_Delegate_Imp321(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStoreItemClient> __Gen_Delegate_Imp3210(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItemClient> __Gen_Delegate_Imp3211(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStoreItemClient __Gen_Delegate_Imp3212(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemRecommendMarkInfo __Gen_Delegate_Imp3213(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3214(object p0, StoreItemType p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp3215(object p0, StoreItemType p1, int p2, long p3, bool p4, ItemSuspendReason p5, int p6, ulong p7)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem __Gen_Delegate_Imp3216(object p0, int p1, StoreItemType p2, int p3, long p4, bool p5, bool p6, ulong p7, uint p8)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessSingleUnitLaunchInfo __Gen_Delegate_Imp3217(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessSingleMissileUnitLaunchInfo __Gen_Delegate_Imp3218(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletGunLaunch __Gen_Delegate_Imp3219(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase.UIControllerDesc[] __Gen_Delegate_Imp322(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperRailgunLaunch __Gen_Delegate_Imp3220(object p0, object p1, object p2, object p3, object p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperPlasmaLaunch __Gen_Delegate_Imp3221(object p0, object p1, object p2, object p3, object p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperMissileLaunch __Gen_Delegate_Imp3222(object p0, object p1, object p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessLaserLaunch __Gen_Delegate_Imp3223(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperLaserLaunch __Gen_Delegate_Imp3224(object p0, object p1, object p2, object p3, object p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneFighterLaunch __Gen_Delegate_Imp3225(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperEquipAoeLaunch __Gen_Delegate_Imp3226(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneDefenderLaunch __Gen_Delegate_Imp3227(object p0, object p1, object p2, double p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneSniperLaunch __Gen_Delegate_Imp3228(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAttachBufLaunch __Gen_Delegate_Imp3229(object p0, ShipEquipSlotType p1, int p2, EquipType p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp323(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAttachBufLaunch __Gen_Delegate_Imp3230(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAntiJumpingForceShieldAndAttachBufLaunch __Gen_Delegate_Imp3231(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipTransformToTeleportTunnelLaunch __Gen_Delegate_Imp3232(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAddEnergyAndAttachBufLaunch __Gen_Delegate_Imp3233(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch __Gen_Delegate_Imp3234(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuffLaunch __Gen_Delegate_Imp3235(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipChannelLaunch __Gen_Delegate_Imp3236(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipPeriodicDamageLaunch __Gen_Delegate_Imp3237(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAddShieldAndAttachBufLaunch __Gen_Delegate_Imp3238(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipBlinkLaunch __Gen_Delegate_Imp3239(object p0, ShipEquipSlotType p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp324(object p0, int[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipInvisibleLaunch __Gen_Delegate_Imp3240(object p0, ShipEquipSlotType p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSyncEvent> __Gen_Delegate_Imp3241(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3242(object p0, uint p1, DateTime p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoredMail> __Gen_Delegate_Imp3243(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoredMail> __Gen_Delegate_Imp3244(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3245(object p0, int p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<int, long>> __Gen_Delegate_Imp3246(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<int, long>> __Gen_Delegate_Imp3247(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp3248(object p0, object p1, ulong p2, ulong p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3249(object p0, object p1, out List<PlayerSimplestInfo> p2, ref List<string> p3, ref List<int> p4)
        {
        }

        [MethodImpl(0x8000)]
        public Func<string> __Gen_Delegate_Imp325()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo __Gen_Delegate_Imp3250(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo __Gen_Delegate_Imp3251(object p0, object p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3252(object p0, int p1, int p2, int p3, DateTime p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3253(object p0, int p1, GrandFaction p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3254(object p0, int p1, GrandFaction p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId __Gen_Delegate_Imp3255(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public QuestWaitForAcceptInfo __Gen_Delegate_Imp3256(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3257(object p0, int p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3258(object p0, QuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> __Gen_Delegate_Imp3259(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp326(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3260(object p0, object p1, out float p2, out float p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemQuestPoolCountInfo __Gen_Delegate_Imp3261(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public VirtualBuffDesc __Gen_Delegate_Imp3262(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3263(object p0, RankingListType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3264(object p0, RankingListType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public RankingListInfo __Gen_Delegate_Imp3265(object p0, RankingListType p1)
        {
        }

        [MethodImpl(0x8000)]
        public RankingTargetInfo __Gen_Delegate_Imp3266(object p0, RankingListType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeGiftPackage> __Gen_Delegate_Imp3267(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeGiftPackageDataContainerClient __Gen_Delegate_Imp3268(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeItem> __Gen_Delegate_Imp3269(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp327(byte[] p0)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeItemDataContainerClient __Gen_Delegate_Imp3270(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeMonthlyCard> __Gen_Delegate_Imp3271(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeMonthlyCardDataContainerClient __Gen_Delegate_Imp3272(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeOrderDataContainerClient __Gen_Delegate_Imp3273(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SailReportSolarSystemInfo> __Gen_Delegate_Imp3274(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3275(object p0, int p1, SailReportEventType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3276(object p0, int p1, SailReportEventType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public SailReportSolarSystemInfo __Gen_Delegate_Imp3277(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SailReportSolarSystemInfo.SailReportEvent __Gen_Delegate_Imp3278(object p0, int p1, SailReportEventType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3279(object p0, int p1, SailReportEventType p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp328(TimeSpan p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3280(object p0, uint p1, int p2, int p3, int p4, int p5, int p6, bool p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3281(object p0, int p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3282(object p0, uint p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSceneExtraDataNtf __Gen_Delegate_Imp3283(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneInfo __Gen_Delegate_Imp3284(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBShipFightBuf> __Gen_Delegate_Imp3285(object p0, ShipFightBufType p1, bool p2, BufType p3)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<LBBufBase> __Gen_Delegate_Imp3286(object p0, BufType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3287(object p0, object p1, object p2, uint p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3288(object p0, object p1, object p2, int p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3289(object p0, ulong p1, float p2, BufType p3)
        {
        }

        [MethodImpl(0x8000)]
        public System.Type __Gen_Delegate_Imp329(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3290(object p0, int p1, float p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public ProcessFactory __Gen_Delegate_Imp3291()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3292(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEquipFunctionCompBlinkOwnerClient __Gen_Delegate_Imp3293(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEquipFunctionCompChannelOwnerClient __Gen_Delegate_Imp3294(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEquipFunctionCompInOutRangeTriggerOwnerClient __Gen_Delegate_Imp3295(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEquipFunctionCompInvisibleOwnerClient __Gen_Delegate_Imp3296(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupBase __Gen_Delegate_Imp3297(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase __Gen_Delegate_Imp3298(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupNormalAttackClient __Gen_Delegate_Imp3299(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp33(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public System.Random __Gen_Delegate_Imp330()
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3300(object p0, int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<EquipEffectVirtualBufInfo> __Gen_Delegate_Imp3301(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3302(object p0, ShipFightBufType p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public EquipEffectVirtualBufInfo __Gen_Delegate_Imp3303(object p0, object p1, FakeViewBufLifeType p2, TacticalEquipFunctionType p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public FakeViewBufLifeType __Gen_Delegate_Imp3304(object p0, TacticalEquipFunctionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3305(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public FakeViewBufLifeType __Gen_Delegate_Imp3306(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3307(object p0, object p1, FakeViewBufLifeType p2, TacticalEquipFunctionType p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticResource __Gen_Delegate_Imp3308(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupBase __Gen_Delegate_Imp3309(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Hashtable __Gen_Delegate_Imp331(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupEnv4PlayerShip __Gen_Delegate_Imp3310(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponAmmoSetup4PlayerShip __Gen_Delegate_Imp3311(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3312(object p0, object p1, long p2, int p3, ulong p4, out LBShipStoreItemClient p5, out bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3313(object p0, object p1, long p2, int p3, ulong p4, out LBStoreItem p5, out bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3314(object p0, object p1, long p2, int p3, ulong p4, out LBStoreItem p5, out bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3315(object p0, StoreItemType p1, int p2, long p3, int p4, bool p5, ulong p6, out LBShipStoreItemClient p7)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3316(object p0, ShipStoreItemUpdateInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3317(object p0, StoreItemType p1, int p2, bool p3, bool p4, long p5, out bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtxClient __Gen_Delegate_Imp3318(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFlagShipCompStaticBasicInfo __Gen_Delegate_Imp3319(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp332(int[] p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupEnv4FlagShip __Gen_Delegate_Imp3320(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildItemStoreItemOperationBase __Gen_Delegate_Imp3321(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBFakeStaticShipClient __Gen_Delegate_Imp3322(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StringTableId __Gen_Delegate_Imp3323(object p0, ShipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3324(object p0, object p1, StoreItemType p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3325(object p0, object p1, StoreItemType p2, int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup __Gen_Delegate_Imp3326(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3327(object p0, object p1, StoreItemType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItemClient __Gen_Delegate_Imp3328(object p0, StoreItemType p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3329(object p0, object p1, object p2, object p3, int p4, out int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp333(Vector3 p0, Vector3 p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3330(object p0, object p1, int p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3331(object p0, object p1, int p2, ulong p3, out LBStoreItem p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3332(object p0, SimpleItemInfo p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3333(object p0, object p1, object p2, SimpleItemInfo p3, object p4, int p5, ulong p6, out LBStoreItem p7, out int p8)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3334(object p0, object p1, int p2, ulong p3, object p4, out LBStoreItem p5)
        {
        }

        [MethodImpl(0x8000)]
        public LBScanProbe __Gen_Delegate_Imp3335(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemDelegateMissionPoolCountInfo __Gen_Delegate_Imp3336(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public VirtualBuffDesc __Gen_Delegate_Imp3337(object p0, SignalType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public VirtualBuffDesc __Gen_Delegate_Imp3338(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3339(object p0, int p1, SignalType p2, DateTime p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp334(Vector2 p0, Vector2 p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemSpaceSignalInfo __Gen_Delegate_Imp3340(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3341(object p0, GlobalSceneInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public LinkedList<GlobalSceneInfo> __Gen_Delegate_Imp3342(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GlobalSceneInfo? __Gen_Delegate_Imp3343(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public GlobalSceneInfo? __Gen_Delegate_Imp3344(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3345(object p0, GlobalSceneInfo p1, GlobalSceneInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3346(object p0, int p1, out Dictionary<int, int> p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3347(object p0, int p1, out ConfigDataSolarSystemQuestPoolCountInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3348(object p0, int p1, out ConfigDataSolarSystemDelegateMissionPoolCountInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, int> __Gen_Delegate_Imp3349(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp335(UnityEngine.Quaternion p0, UnityEngine.Quaternion p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> __Gen_Delegate_Imp3350(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, LogicBlockStarMapInfoClient.LocationItemInfo> __Gen_Delegate_Imp3351(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public VirtualBuffDesc __Gen_Delegate_Imp3352(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<TeamMemberInfo> __Gen_Delegate_Imp3353(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TeamMemberInfo __Gen_Delegate_Imp3354(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3355(object p0, uint p1, int p2, object p3, DateTime p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3356(object p0, uint p1, object p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<TradeListItemInfo> __Gen_Delegate_Imp3357(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<TradeListItemInfo> __Gen_Delegate_Imp3358(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<TradeNpcShopItemInfo> __Gen_Delegate_Imp3359(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp336(float p0, float p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3360(FrameRateLevel p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3361(object p0, int p1, int p2, out ConfigDataWormholeStarGroupInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3362(object p0, uint p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholeStarGroupInfo> __Gen_Delegate_Imp3363(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3364(object p0, object p1, out string p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3365(object p0, object p1, Vector3 p2, ref Vector2 p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3366(object p0, object p1, ref Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3367(object p0, Vector2 p1)
        {
        }

        [MethodImpl(0x8000)]
        public Texture2D __Gen_Delegate_Imp3368(object p0, UnityUtil.ImageFilterMode p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp3369(object p0, object p1, object p2, float p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp337(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public PDSDK __Gen_Delegate_Imp3370()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3371(object p0, object p1, int p2, object p3, object p4, double p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3372(object p0, int p1, object p2, int p3, object p4, object p5, double p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3373(object p0, object p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3374(object p0, object p1, int p2, int p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3375(object p0, ref int p1, ref int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DataSectionShipHangarInfo __Gen_Delegate_Imp3376(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DataSectionMailInfo __Gen_Delegate_Imp3377(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DataSectionItemStoreInfo __Gen_Delegate_Imp3378(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DataSectionHiredCaptainInfo __Gen_Delegate_Imp3379(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemDesc __Gen_Delegate_Imp338(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DataSectionKillRecordInfo __Gen_Delegate_Imp3380(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildStoreItemListInfo __Gen_Delegate_Imp3381(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ISystemPushDataContainer __Gen_Delegate_Imp3382(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IBattlePassDataContainer __Gen_Delegate_Imp3383(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipSalvageDataContainer __Gen_Delegate_Imp3384(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3385(object p0, ClientOSType p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3386(object p0, object p1, int p2, int p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerInfoInitReq __Gen_Delegate_Imp3387(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientConfigDataLoader __Gen_Delegate_Imp3388(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientGDBDataLoader __Gen_Delegate_Imp3389(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp339(object p0, double p1, double p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3390(object p0, uint p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3391(object p0, int p1, int p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3392(object p0, uint p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3393(object p0, ulong p1, int p2, long p3, long p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3394(object p0, int p1, int p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockBattlePassBase __Gen_Delegate_Imp3395(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3396(object p0, object p1, object p2, int p3, MisbehaviorTargetType p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ChatInfo __Gen_Delegate_Imp3397(object p0, ChatContentType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3398(object p0, object p1, uint p2, uint p3, DeplomacyOptType p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3399(object p0, bool p1, bool p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp34(object p0, object p1, object p2, CriAtomExPlayback p3, CriPlayBackInfo.Mode p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp340(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3400(object p0, int p1, float p2, FactionCreditLevel p3, FactionCreditLevel p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3401(object p0, bool p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3402(object p0, uint p1, ushort p2, ushort p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3403(object p0, object p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3404(object p0, int p1, GuildJoinPolicy p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3405(object p0, object p1, object p2, int p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3406(object p0, bool p1, bool p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3407(object p0, object p1, GuildJobType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3408(object p0, uint p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3409(object p0, ulong p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public PlanetDesc __Gen_Delegate_Imp341(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3410(object p0, LostReportType p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3411(object p0, int p1, ulong p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3412(object p0, ulong p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3413(object p0, ulong p1, object p2, FleetPosition p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3414(object p0, GuildFleetPersonalSetting p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3415(object p0, ulong p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3416(object p0, ulong[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3417(object p0, ulong p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3418(object p0, uint p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3419(object p0, int p1, StoreItemType p2, int p3, int p4, long p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp342(object p0, object p1, object p2, object p3, int p4, object p5, object p6, object p7, int p8)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3420(object p0, ulong p1, int p2, int p3, long p4, ushort p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3421(object p0, ulong p1, int p2, ushort p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3422(object p0, int p1, StoreItemType p2, int p3, int p4, long p5, int p6, ulong p7)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3423(object p0, ushort p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3424(object p0, int p1, ushort p2, ushort p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3425(object p0, ulong p1, Vector3D p2, uint p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3426(object p0, ulong p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3427(object p0, ulong p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3428(object p0, ulong p1, int p2, int p3, int p4, int p5, int p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3429(object p0, ulong p1, int p2, int p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp343(object p0, object p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProItemInfo __Gen_Delegate_Imp3430(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3431(object p0, ulong p1, int p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceStationContext __Gen_Delegate_Imp3432(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Func<ulong> __Gen_Delegate_Imp3433(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GDBHelper __Gen_Delegate_Imp3434(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICharacterDataContainer __Gen_Delegate_Imp3435(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ITechDataContainer __Gen_Delegate_Imp3436(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILoginActivityDataContainer __Gen_Delegate_Imp3437(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IUserSettingDataContainer __Gen_Delegate_Imp3438(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICommanderAuthDataContainerClient __Gen_Delegate_Imp3439(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp344(object p0, object p1, object p2, int p3, object p4, object p5, int p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public ICMDailySignDataContainerClient __Gen_Delegate_Imp3440(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IProduceDataContainer __Gen_Delegate_Imp3441(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipCustomTemplateDataContainer __Gen_Delegate_Imp3442(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IDrivingLicenseDataContainer __Gen_Delegate_Imp3443(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICrackDataContainer __Gen_Delegate_Imp3444(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IActivityDataContainer __Gen_Delegate_Imp3445(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IFunctionOpenStateDataContainer __Gen_Delegate_Imp3446(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IDevelopmentDataContainer __Gen_Delegate_Imp3447(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAuctionDataContainer __Gen_Delegate_Imp3448(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildDataContainerForPlayerCtx __Gen_Delegate_Imp3449(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo __Gen_Delegate_Imp345(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IItemStoreRecommendMarkDataContainer __Gen_Delegate_Imp3450(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IDiplomacyDataContainer __Gen_Delegate_Imp3451(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockStationContextBase __Gen_Delegate_Imp3452(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICharacterPropertiesDataContainer __Gen_Delegate_Imp3453(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICharacterSkillDataContainer __Gen_Delegate_Imp3454(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterFactionBase __Gen_Delegate_Imp3455(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICharacterChipDataContainer __Gen_Delegate_Imp3456(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICharacterFactionDataContainer __Gen_Delegate_Imp3457(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICharacterPVPContainer __Gen_Delegate_Imp3458(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterBasicPropertiesBase __Gen_Delegate_Imp3459(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Action<NpcSpaceShipDummy> __Gen_Delegate_Imp346()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterSkillBase __Gen_Delegate_Imp3460(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterChipBase __Gen_Delegate_Imp3461(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public INpcShopDataContainer __Gen_Delegate_Imp3462(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockNpcShopBase __Gen_Delegate_Imp3463(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IMotherShipBasicDataContainer __Gen_Delegate_Imp3464(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IHiredCaptainManagementDataContainer __Gen_Delegate_Imp3465(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipHangarListDataContainer __Gen_Delegate_Imp3466(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IItemStoreDataContainer __Gen_Delegate_Imp3467(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IMailDataContainer __Gen_Delegate_Imp3468(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IDelegateMissionListDataContainer __Gen_Delegate_Imp3469(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Action<SpaceSimpleObjectDummy> __Gen_Delegate_Imp347()
        {
        }

        [MethodImpl(0x8000)]
        public IQuestDataContainer __Gen_Delegate_Imp3470(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IUserGuideDataContainer __Gen_Delegate_Imp3471(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ISignalDataContainer __Gen_Delegate_Imp3472(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ISpaceSignalDataContainer __Gen_Delegate_Imp3473(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IFleetListDataContainer __Gen_Delegate_Imp3474(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IKillRecordDataContainer __Gen_Delegate_Imp3475(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IChatDataContainer __Gen_Delegate_Imp3476(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IInvadeRescueDataContainer __Gen_Delegate_Imp3477(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IStarMapGuildOccupyDataContainer __Gen_Delegate_Imp3478(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeOrderDataContainer __Gen_Delegate_Imp3479(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSimpleObjectInfo __Gen_Delegate_Imp348(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeGiftPackageDataContainer __Gen_Delegate_Imp3480(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeMonthlyCardDataContainer __Gen_Delegate_Imp3481(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IRechargeItemDataContainer __Gen_Delegate_Imp3482(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ICustomizedParameterDataContainer __Gen_Delegate_Imp3483(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterBase __Gen_Delegate_Imp3484(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockMotherShipBase __Gen_Delegate_Imp3485(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockHiredCaptainManagementBase __Gen_Delegate_Imp3486(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipHangarsBase __Gen_Delegate_Imp3487(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockItemStoreBase __Gen_Delegate_Imp3488(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFleetList __Gen_Delegate_Imp3489(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp349(object p0, object p1, Vector3 p2, Vector3 p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSignalBase __Gen_Delegate_Imp3490(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSpaceSignalBase __Gen_Delegate_Imp3491(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockDelegateMissionBase __Gen_Delegate_Imp3492(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockQuestBase __Gen_Delegate_Imp3493(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFactionCreditQuestBase __Gen_Delegate_Imp3494(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCelestialScanBase __Gen_Delegate_Imp3495(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSpaceSceneClient __Gen_Delegate_Imp3496(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockUserGuideBase __Gen_Delegate_Imp3497(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockMailBase __Gen_Delegate_Imp3498(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockTeamBase __Gen_Delegate_Imp3499(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp35(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp350(object p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockChatBase __Gen_Delegate_Imp3500(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRankingListBase __Gen_Delegate_Imp3501(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockTechBase __Gen_Delegate_Imp3502(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockProduceBase __Gen_Delegate_Imp3503(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockKillRecordBase __Gen_Delegate_Imp3504(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCrackBase __Gen_Delegate_Imp3505(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockActivityBase __Gen_Delegate_Imp3506(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFunctionOpenStateBase __Gen_Delegate_Imp3507(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockDevelopmentBase __Gen_Delegate_Imp3508(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockNavigationBase __Gen_Delegate_Imp3509(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp351(object p0, SpaceGrideIndex p1, float p2, float p3, Vector3D p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCustomTemplateBase __Gen_Delegate_Imp3510(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockDrivingLicenseBase __Gen_Delegate_Imp3511(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterPVPBase __Gen_Delegate_Imp3512(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSolarSystemInfoClient __Gen_Delegate_Imp3513(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSolarSystemInfoBase __Gen_Delegate_Imp3514(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCompWormholeClient __Gen_Delegate_Imp3515(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCompInfectClient __Gen_Delegate_Imp3516(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSolarSystemPoolCountInfoClient __Gen_Delegate_Imp3517(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockClientCustomData __Gen_Delegate_Imp3518(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRedPointClient __Gen_Delegate_Imp3519(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp352(Vector3D p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockStarMapInfoClient __Gen_Delegate_Imp3520(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockPVPInvadeRescueBase __Gen_Delegate_Imp3521(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockAuctionBase __Gen_Delegate_Imp3522(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildBase __Gen_Delegate_Imp3523(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockDiplomacyBase __Gen_Delegate_Imp3524(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSailReportClient __Gen_Delegate_Imp3525(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockBlackMarketBase __Gen_Delegate_Imp3526(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockBranchStoryBase __Gen_Delegate_Imp3527(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipSalvageBase __Gen_Delegate_Imp3528(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeOrderBase __Gen_Delegate_Imp3529(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp353(Vector3 p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeGiftPackageBase __Gen_Delegate_Imp3530(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeMonthlyCardBase __Gen_Delegate_Imp3531(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeItemBase __Gen_Delegate_Imp3532(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCustomizedParameterBase __Gen_Delegate_Imp3533(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockStarMapGuildOccupyClient __Gen_Delegate_Imp3534(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockLoginActivityBase __Gen_Delegate_Imp3535(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockClientSdkInterface __Gen_Delegate_Imp3536(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockUserSettingClient __Gen_Delegate_Imp3537(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp3538(object p0, object p1, object p2, DynamicSceneLocationType p3, object p4, DynamicSceneNearCelestialType p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<ActivityInfo> __Gen_Delegate_Imp3539(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp354(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockTeamClient __Gen_Delegate_Imp3540(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StringTableManager __Gen_Delegate_Imp3541(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3542(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockPlayerInfoCacheClient __Gen_Delegate_Imp3543(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3544(object p0, int p1, int p2, int p3, ulong p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<ClientPlayerShipInfo> __Gen_Delegate_Imp3545(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientPlayerShipInfo __Gen_Delegate_Imp3546(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3547(object p0, RankingListType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3548(object p0, bool p1, int p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3549(object p0, ulong p1, int p2, int p3, int p4, uint p5, ulong p6)
        {
        }

        [MethodImpl(0x8000)]
        public ClientConfigDataLoader __Gen_Delegate_Imp355()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3550(object p0, ulong p1, ulong p2, int p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3551(object p0, StoreItemType p1, int p2, int p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3552(object p0, int p1, uint p2, ShipEquipSlotType p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3553(object p0, NavigationNodeType p1, int p2, int p3, ulong p4, int p5, uint p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3554(object p0, bool p1, bool p2, int p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public LBInSpaceShipPlayerCaptain __Gen_Delegate_Imp3555(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo __Gen_Delegate_Imp3556(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSolarSystem __Gen_Delegate_Imp3557(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip __Gen_Delegate_Imp3558(object p0, object p1, uint p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip4FlagShip __Gen_Delegate_Imp3559(object p0, object p1, uint p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<KeyValuePair<string, string>> __Gen_Delegate_Imp356(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceObject __Gen_Delegate_Imp3560(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStarGate __Gen_Delegate_Imp3561(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceStation __Gen_Delegate_Imp3562(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientOtherPlayerSpaceShip __Gen_Delegate_Imp3563(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public ClientOtherPlayerSpaceShip4FlagShip __Gen_Delegate_Imp3564(object p0, object p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemPlayerShipInstanceInfo __Gen_Delegate_Imp3565(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemFlagShipInstanceInfo __Gen_Delegate_Imp3566(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemNpcShipInstanceInfo __Gen_Delegate_Imp3567(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceObject __Gen_Delegate_Imp3568(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceDropBox __Gen_Delegate_Imp3569(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataActivityInfo __Gen_Delegate_Imp357(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBMoonInfo __Gen_Delegate_Imp3570(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSolarSystem __Gen_Delegate_Imp3571(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3572(object p0, ulong p1, int p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3573(object p0, bool p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3574(object p0, ulong p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3575(object p0, bool p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockTradeBase __Gen_Delegate_Imp3576(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3577(object p0, int p1, DailyLoginRewardState p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3578(object p0, ActivityType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3579(object p0, PlayerActivityInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataActivityInfo> __Gen_Delegate_Imp358(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3580(object p0, ulong p1, DateTime p2, DateTime p3, DateTime p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3581(object p0, CompensationCloseReason p1, DateTime? p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3582(object p0, int p1, ItemCompensationDonatorType p2, object p3, ItemCompensationStatus p4, DateTime? p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3583(object p0, ulong p1, CompensationCloseReason p2, DateTime? p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildCompensationDataSection __Gen_Delegate_Imp3584(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProjectXPlayerContext.GuildCompensationDataSection> __Gen_Delegate_Imp3585(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3586(object p0, DateTime p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3587(object p0, CustomizedParameter p1)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateMissionInfo __Gen_Delegate_Imp3588(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3589(object p0, ulong p1, DelegateMissionState p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable __Gen_Delegate_Imp359(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3590(object p0, ulong p1, DelegateMissionState p2, DateTime p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3591(object p0, ulong p1, object p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public float? __Gen_Delegate_Imp3592(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, FactionCreditLevelRewardInfo> __Gen_Delegate_Imp3593(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditNpcQuestInfo __Gen_Delegate_Imp3594(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer __Gen_Delegate_Imp3595(object p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer __Gen_Delegate_Imp3596(object p0, ulong p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer __Gen_Delegate_Imp3597(object p0, int p1, object p2, int p3, DateTime p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<IStaticFlagShipDataContainer> __Gen_Delegate_Imp3598(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer __Gen_Delegate_Imp3599(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp36(object p0, object p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAgeFeatsPoolInfo __Gen_Delegate_Imp360(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<FleetInfo> __Gen_Delegate_Imp3600(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public FleetInfo __Gen_Delegate_Imp3601(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3602(object p0, object p1, GuildJobType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3603(object p0, object p1, long p2, long p3, long p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildStoreItemListInfo> __Gen_Delegate_Imp3604(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildStoreItemInfo __Gen_Delegate_Imp3605(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3606(object p0, int p1, object p2, bool p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildStoreItemListInfoDataSection __Gen_Delegate_Imp3607(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3608(object p0, ulong p1, out uint p2, out uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3609(object p0, ulong p1, object p2, int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataAgeFeatsPoolInfo> __Gen_Delegate_Imp361(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3610(object p0, ulong p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3611(object p0, ulong p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public SovereignLostReport __Gen_Delegate_Imp3612(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3613(object p0, ulong p1, int p2, ItemCompensationDonatorType p3, object p4, ItemCompensationStatus p5, DateTime? p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3614(object p0, ulong p1, long p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3615(object p0, ulong p1, long p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3616(object p0, ulong p1, ref long p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildPurchaseOrderInfo __Gen_Delegate_Imp3617(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3618(object p0, ulong p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildBasicInfoDataSection __Gen_Delegate_Imp3619(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAgeInfo __Gen_Delegate_Imp362(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildDynamicInfoDataSection __Gen_Delegate_Imp3620(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildMemberListInfoDataSection __Gen_Delegate_Imp3621(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildDiplomacyInfoDataSection __Gen_Delegate_Imp3622(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildItemStoreDataSection __Gen_Delegate_Imp3623(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildCurrencyInfoDataSection __Gen_Delegate_Imp3624(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildSolarSystemInfoListDataContainer __Gen_Delegate_Imp3625(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildCompensationListDataSection __Gen_Delegate_Imp3626(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildPurchaseInfoDataSection __Gen_Delegate_Imp3627(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildMiningDataSection __Gen_Delegate_Imp3628(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildMomentsDataSection __Gen_Delegate_Imp3629(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataAgeInfo> __Gen_Delegate_Imp363(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildTradeDataSection __Gen_Delegate_Imp3630(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildAllianceInviteListDataSection __Gen_Delegate_Imp3631(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DataSectionGuildFlagShipHangarListInfo __Gen_Delegate_Imp3632(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildFlagShipOptLogDataSection __Gen_Delegate_Imp3633(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DateSectionGuildAntiTeleportInfo __Gen_Delegate_Imp3634(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.DateSectionGuildAllianceAvailableTeleportTunnelInfo __Gen_Delegate_Imp3635(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceInviteInfo __Gen_Delegate_Imp3636(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBenefitsInfoBase> __Gen_Delegate_Imp3637(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildBenefitsListDataSection __Gen_Delegate_Imp3638(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildCurrencyLogType> __Gen_Delegate_Imp3639(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAllianceIconBGList __Gen_Delegate_Imp364(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3640(object p0, long p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext.GuildBenefitsDataSection __Gen_Delegate_Imp3641(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3642(object p0, ulong p1, long p2, int p3, NpcCaptainState p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3643(object p0, ulong p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<PVPInvadeRescueInfo> __Gen_Delegate_Imp3644(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<StoreItemType, Dictionary<int, DateTime>> __Gen_Delegate_Imp3645(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<StoreItemInfo> __Gen_Delegate_Imp3646(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemInfo __Gen_Delegate_Imp3647(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<StoreItemRecommendMarkInfo> __Gen_Delegate_Imp3648(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemRecommendMarkInfo __Gen_Delegate_Imp3649(object p0, StoreItemType p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataAllianceIconBGList> __Gen_Delegate_Imp365(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3650(object p0, ulong p1, CompensationStatus p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<StoredMailInfo> __Gen_Delegate_Imp3651(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StoredMailInfo __Gen_Delegate_Imp3652(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3653(object p0, int p1, bool p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<CurrencyType, ulong>> __Gen_Delegate_Imp3654(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ProductionLineInfo> __Gen_Delegate_Imp3655(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<QuestEnvirmentInfo> __Gen_Delegate_Imp3656(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestProcessingInfo> __Gen_Delegate_Imp3657(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeGiftPackageInfo __Gen_Delegate_Imp3658(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeGiftPackageInfo> __Gen_Delegate_Imp3659(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAllianceIconPaintingList __Gen_Delegate_Imp366(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeMonthlyCardInfo> __Gen_Delegate_Imp3660(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeMonthlyCardInfo __Gen_Delegate_Imp3661(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeItemInfo> __Gen_Delegate_Imp3662(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeItemInfo __Gen_Delegate_Imp3663(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipCustomTemplateInfo __Gen_Delegate_Imp3664(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticPlayerShipDataContainer __Gen_Delegate_Imp3665(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticPlayerShipDataContainer __Gen_Delegate_Imp3666(object p0, int p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<SignalInfo> __Gen_Delegate_Imp3667(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SignalInfo __Gen_Delegate_Imp3668(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public EmergencySignalReqQueueItem __Gen_Delegate_Imp3669(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataAllianceIconPaintingList> __Gen_Delegate_Imp367(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<PVPSignalInfo> __Gen_Delegate_Imp3670(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PVPSignalInfo __Gen_Delegate_Imp3671(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ScanProbeInfo> __Gen_Delegate_Imp3672(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3673(object p0, ScanProbeType p1, int p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3674(object p0, int p1, SignalType p2, out SolarSystemSpaceSignalInfo p3, out SpaceSignalResultInfo p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3675(object p0, object p1, SignalType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3676(object p0, int p1, SignalType p2, ulong p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyStaticInfo __Gen_Delegate_Imp3677(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo __Gen_Delegate_Imp3678(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo __Gen_Delegate_Imp3679(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAmmoInfo __Gen_Delegate_Imp368(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapAllianceSimpleInfo __Gen_Delegate_Imp3680(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo __Gen_Delegate_Imp3681(object p0, int p1, int p2, int? p3, bool? p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3682(object p0, int p1, ushort p2, ushort p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceStationResInfo __Gen_Delegate_Imp3683(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeGoodsInfo> __Gen_Delegate_Imp3684(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeGoodsInfo __Gen_Delegate_Imp3685(object p0, RechargeGoodsType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeGoodsInfo __Gen_Delegate_Imp3686(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3687(object p0, object p1, out RechargeGoodsType p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3688(object p0, object p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3689(object p0, bool p1, object p2, object[] p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataAmmoInfo> __Gen_Delegate_Imp369(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3690(object p0, PurchaseRechargeGoodsResult p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController __Gen_Delegate_Imp3691(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public CutsceneActorDesc __Gen_Delegate_Imp3692(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<SpaceShipController> __Gen_Delegate_Imp3693(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController __Gen_Delegate_Imp3694(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3695(object p0, object p1, object p2, object p3, ushort p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3696(object p0, object p1, object p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public BulletObjectController __Gen_Delegate_Imp3697(object p0, BulletLaunchInfo p1, WeaponCategory p2, object p3, float p4, bool p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public BulletRailgunController __Gen_Delegate_Imp3698(object p0, BulletLaunchInfo p1, WeaponCategory p2, object p3, float p4, bool p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3699(object p0, Vector3 p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp37(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAuctionItemInfo __Gen_Delegate_Imp370(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3700(object p0, UnityEngine.Quaternion p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3701(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UnityEngine.Quaternion __Gen_Delegate_Imp3702(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<TimelineBindInfo> __Gen_Delegate_Imp3703(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ISceneAnimationPlayerController __Gen_Delegate_Imp3704(object p0, object p1, SceneAnimationPlayerType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3705(object p0, Vector3D p1, double p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3706(object p0, Vector4D p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3707(object p0, object p1, Vector3D p2, uint p3, uint p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public MovementModuleType __Gen_Delegate_Imp3708(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3709(object p0, Vector3D p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataAuctionItemInfo> __Gen_Delegate_Imp371(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp3710(object p0, Dest.Math.Quaternion p1)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp3711(object p0, Vector3D p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3712(object p0, out double p1, out double p2, double p3, double p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3713(object p0, double p1, Dest.Math.Quaternion p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3714(object p0, Vector3D p1, out double p2, out double p3)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp3715(object p0, double p1)
        {
        }

        [MethodImpl(0x8000)]
        public SimMovementModuleArroundTargetSyncData __Gen_Delegate_Imp3716(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp3717(object p0, double p1, ref uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp3718(object p0, double p1)
        {
        }

        [MethodImpl(0x8000)]
        public SimMovementModuleJumpingSyncData __Gen_Delegate_Imp3719(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAutoSaleItemListInfo __Gen_Delegate_Imp372(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3720(object p0, double p1, out double p2, out double p3, out bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3721(object p0, object p1, int p2, uint p3, uint p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3722(object p0, object p1, Vector3D p2, Dest.Math.Quaternion p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3723(object p0, object p1, Vector3D p2, Dest.Math.Quaternion p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp3724(Vector3D p0, Vector3D p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp3725(object p0, Vector3D p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3726(object p0, ShipPoseRequest p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3727(object p0, SimSpaceShip.MoveCmd.CmdType p1, Vector3D p2, uint p3, uint p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public SimMoveableSpaceObjectStateSnapshot __Gen_Delegate_Imp3728(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public SimMovementModule __Gen_Delegate_Imp3729(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataAutoSaleItemListInfo> __Gen_Delegate_Imp373(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ISimSpaceShipEventListener __Gen_Delegate_Imp3730(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3731(object p0, uint p1, object p2, object p3, object p4, ShipPoseRequest p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3732(object p0, double p1, double p2, double p3, double p4, double p5, double p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3733(object p0, Vector3D p1, Dest.Math.Quaternion p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3734(object p0, SimSpaceShip.MoveCmd p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3735(object p0, Vector3D p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3736(object p0, Dest.Math.Quaternion p1)
        {
        }

        [MethodImpl(0x8000)]
        public ISimSpaceHelper __Gen_Delegate_Imp3737(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SimColliderModule __Gen_Delegate_Imp3738(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3739(object p0, uint p1, object p2, float p3, object p4, object p5, Vector3D p6, Vector3D p7)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassChallangeQuestAccRewardInfo __Gen_Delegate_Imp374(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp3740(object p0, Vector3D p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3741(object p0, object p1, out Vector3D p2, out Dest.Math.Quaternion p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3742(object p0, uint p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataStarGateInfo __Gen_Delegate_Imp3743(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Func<float, float> __Gen_Delegate_Imp3744(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3745(ConfigableConstId p0)
        {
        }

        [MethodImpl(0x8000)]
        public JumpLoopShake __Gen_Delegate_Imp3746()
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3747(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public BoosterLoopShake __Gen_Delegate_Imp3748()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3749(object p0, SolarSystemCameraOperationController.SolarSystemCameraState p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBattlePassChallangeQuestAccRewardInfo> __Gen_Delegate_Imp375(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3750(object p0, bool p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3751(object p0, object p1, bool p2, bool p3, EnterSolarSystemType p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3752(object p0, object p1, bool p2, EnterSolarSystemType p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3753(object p0, object p1, bool p2, bool p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3754(object p0, Vector3 p1, Vector3 p2, float p3, bool p4, bool p5, bool p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3755(object p0, Vector3 p1, Vector3 p2, float p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3756(object p0, object p1, object p2, float? p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3757(object p0, SolarSystemCameraOperationController.SolarSystemCameraState p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3758(object p0, float p1, float p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemCameraOperationController.CameraStateController __Gen_Delegate_Imp3759(object p0, SolarSystemCameraOperationController.SolarSystemCameraState p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassChallangeQuestGroupInfo __Gen_Delegate_Imp376(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemCameraOperationController.SolarSystemCameraState __Gen_Delegate_Imp3760(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemCameraOperationController.CameraStateController __Gen_Delegate_Imp3761(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3762(object p0, CameraShakeSourceType p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3763(object p0, Vector3 p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3764(object p0, Vector3 p1, float p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController __Gen_Delegate_Imp3765(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float? __Gen_Delegate_Imp3766(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3767(object p0, float? p1)
        {
        }

        [MethodImpl(0x8000)]
        public EnterSolarSystemType __Gen_Delegate_Imp3768(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3769(object p0, EnterSolarSystemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBattlePassChallangeQuestGroupInfo> __Gen_Delegate_Imp377(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3770(object p0, Vector3 p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3771(object p0, float p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3772(object p0, float p1, float p2, float p3, float p4, float p5, float p6, float p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3773(object p0, ShakeParams p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3774(object p0, float p1, float p2, object p3, bool p4, object p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public ShakeCoolDown __Gen_Delegate_Imp3775(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ContinuouslyShakeParams __Gen_Delegate_Imp3776(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3777(object p0, object p1, float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3778(object p0, float p1, float p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3779(object p0, object p1, float p2, float p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassLevelInfo __Gen_Delegate_Imp378(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3780(object p0, object p1, object p2, float p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp3781(KeyValuePair<int, ConfigDataCameraShakeLevelConfig> p0, KeyValuePair<int, ConfigDataCameraShakeLevelConfig> p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3782(object p0, float p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3783(object p0, object p1, object p2, ref Vector3 p3, ref UnityEngine.Quaternion p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public SceneAnimationPlayerType __Gen_Delegate_Imp3784(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceObjectControllerBase __Gen_Delegate_Imp3785(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3786(object p0, object p1, float p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceObjectControllerBase __Gen_Delegate_Imp3787(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SceneCameraAnimationType __Gen_Delegate_Imp3788(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3789(object p0, SceneCameraAnimationType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBattlePassLevelInfo> __Gen_Delegate_Imp379(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceObjectControllerBase __Gen_Delegate_Imp3790(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3791(object p0, Vector3 p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.ProjectX.Runtime.SolarSystem.StarController __Gen_Delegate_Imp3792(object p0, object p1, Vector3 p2, Vector3 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public PlanetController __Gen_Delegate_Imp3793(object p0, object p1, Vector3 p2, Vector3 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public PlanetLikeBuildingController __Gen_Delegate_Imp3794(object p0, object p1, Vector3 p2, Vector3 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public MoonController __Gen_Delegate_Imp3795(object p0, object p1, Vector3 p2, Vector3 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3796(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3797(object p0, Vector3 p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController __Gen_Delegate_Imp3798(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController __Gen_Delegate_Imp3799(object p0, object p1, Vector3 p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp38(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBattlePassPeriodInfo __Gen_Delegate_Imp380(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public MoonController __Gen_Delegate_Imp3800(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public StarGateController __Gen_Delegate_Imp3801(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationController __Gen_Delegate_Imp3802(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSimpleObjectController __Gen_Delegate_Imp3803(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSceneAnimationController __Gen_Delegate_Imp3804(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSceneObjectController __Gen_Delegate_Imp3805(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceDropBoxController __Gen_Delegate_Imp3806(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3807(object p0, Vector3 p1, int p2, int p3, float p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<SpaceObjectControllerBase> __Gen_Delegate_Imp3808(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3809(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBattlePassPeriodInfo> __Gen_Delegate_Imp381(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3810(object p0, DroneState p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp3811(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public BulletLaunchInfo __Gen_Delegate_Imp3812(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3813(object p0, DroneLaunchSide p1)
        {
        }

        [MethodImpl(0x8000)]
        public DroneLaunchSide __Gen_Delegate_Imp3814(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3815(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3816(object p0, Vector3 p1, Vector3 p2, bool p3, out Vector3 p4, out Vector3 p5, out bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3817(object p0, Vector3 p1, Vector3 p2, bool p3, out Vector3 p4, out Vector3 p5, out Vector3 p6, out Vector3 p7, out bool p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3818(object p0, Vector3 p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3819(object p0, Vector3 p1, Vector3 p2, bool p3, out Vector3 p4, out Vector3 p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBlackMarketShopItemInfo __Gen_Delegate_Imp382(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3820(object p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public BulletDroneFlyController __Gen_Delegate_Imp3821(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IFFState __Gen_Delegate_Imp3822(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3823(object p0, IFFState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3824(object p0, float p1, float p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3825(object p0, float p1, float p2, object p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3826(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3827(object p0, BulletMissileFlyController.MissileFlyState p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3828(object p0, Vector3 p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3829(object p0, BulletObjectController.BulletControllerState p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBlackMarketShopItemInfo> __Gen_Delegate_Imp383(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3830(object p0, BulletObjectController.BulletControllerState p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3831(object p0, bool p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public BulletObjectController.BulletControllerState __Gen_Delegate_Imp3832(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3833(Vector3 p0, Vector3 p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3834(object p0, BulletSuperMissileFlyController.SuperMissileFlyState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3835(object p0, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4, Vector3 p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3836(object p0, Vector3 p1, Vector3 p2, Vector3 p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public BulletObjectController __Gen_Delegate_Imp3837(object p0, object p1, object p2, WeaponCategory p3)
        {
        }

        [MethodImpl(0x8000)]
        public SlotEffectObject __Gen_Delegate_Imp3838(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper __Gen_Delegate_Imp3839(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBlueprintCategoryInfo __Gen_Delegate_Imp384(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper __Gen_Delegate_Imp3840(object p0, Vector3 p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipEffectObjectBase __Gen_Delegate_Imp3841(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceGlobalEffectController __Gen_Delegate_Imp3842(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3843(object p0, ref int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceEffectManager __Gen_Delegate_Imp3844()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3845(object p0, object p1, object p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ClientMoon __Gen_Delegate_Imp3846(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3847(object p0, CelestialEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabCelestialOnHitEffectDesc __Gen_Delegate_Imp3848(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientPlanet __Gen_Delegate_Imp3849(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBlueprintCategoryInfo> __Gen_Delegate_Imp385(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Animator __Gen_Delegate_Imp3850(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientPlanetLikeBuilding __Gen_Delegate_Imp3851(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceGlobalEffectController __Gen_Delegate_Imp3852(object p0, object p1, object p2, float p3, uint p4, uint p5)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceSceneObject __Gen_Delegate_Imp3853(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3854(object p0, GuildBuildingStatus p1, GuildBattleStatus p2, DateTime p3, DateTime p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3855(object p0, GuildBuildingStatus p1, GuildBattleStatus p2, out string p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp3856(object p0, Color p1)
        {
        }

        [MethodImpl(0x8000)]
        public ControllerLODDispatchBase __Gen_Delegate_Imp3857(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3858(object p0, Vector3 p1, Vector3 p2, double p3, double p4, double p5, double p6, bool p7, bool p8, bool p9)
        {
        }

        [MethodImpl(0x8000)]
        public SelfSpaceShipControllerLODDispatch __Gen_Delegate_Imp3859(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBlueprintTypeInfo __Gen_Delegate_Imp386(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3860(object p0, Vector3 p1, Vector3 p2, bool p3, out Vector3 p4, out Vector3 p5, out Vector3 p6, out Vector3 p7)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipDesc __Gen_Delegate_Imp3861(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3862(object p0, double p1, double p2, double p3, double p4, bool p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp3863(object p0, double p1, double p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<LineRenderer> __Gen_Delegate_Imp3864(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ISpaceTargetProvider __Gen_Delegate_Imp3865(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipEffectDesc.ShipEffectScaleType __Gen_Delegate_Imp3866(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3867(object p0, PrefabShipEffectDesc.ShipEffectScaleType p1)
        {
        }

        [MethodImpl(0x8000)]
        public PlaySoundOption __Gen_Delegate_Imp3868(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3869(object p0, PlaySoundOption p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBlueprintTypeInfo> __Gen_Delegate_Imp387(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipEffectController __Gen_Delegate_Imp3870(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<string, GameObject> __Gen_Delegate_Imp3871(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3872(object p0, object p1, out ShipNamedEffectType p2)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipEffectBase __Gen_Delegate_Imp3873(object p0, object p1, object p2, float p3, LoopEffectChangingByLerpParam01Type p4, object[] p5)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipEffectBase __Gen_Delegate_Imp3874(object p0, object p1, object p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp3875(object p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3876(object p0, ShipNamedEffectType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3877(object p0, ShipNamedEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3878(object p0, out string p1, out bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect __Gen_Delegate_Imp3879(object p0, ShipNamedEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBranchStoryCategoryInfo __Gen_Delegate_Imp388(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_Booster __Gen_Delegate_Imp3880(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_Blink __Gen_Delegate_Imp3881(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3882(object p0, BulletLaunchInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSuperLaserEffect __Gen_Delegate_Imp3883(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3884(object p0, float p1, float p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3885(object p0, float p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3886(object p0, object p1, object p2, object p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3887(object p0, bool p1, object p2, PlaySoundOption p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3888(object p0, ShipNamedEffectType p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3889(object p0, ShipNamedEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBranchStoryCategoryInfo> __Gen_Delegate_Imp389(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3890(object p0, object p1, ShipNamedEffectType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3891(object p0, ShipLocalMovementController.MovementType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3892(object p0, double p1, double p2, double p3, double p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3893(object p0, double p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipExplosionEffectDesc __Gen_Delegate_Imp3894(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3895(object p0, FieldType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3896(object p0, FieldType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3897(object p0, Ray p1, float p2, out Vector3 p3, out Vector3 p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3898(object p0, Vector3 p1, float p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3899(object p0, int p1, Vector3 p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp39(object p0, object p1, PlaySoundOption p2, bool p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBranchStoryQuestExInfo __Gen_Delegate_Imp390(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipFieldEffectController __Gen_Delegate_Imp3900(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3901(object p0, Vector3 p1, float p2, float p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3902(object p0, ShipLocalMovementController.MovementType p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public ShipBuffEffectController __Gen_Delegate_Imp3903(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSuperEquipController __Gen_Delegate_Imp3904(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipTacticalEquipController __Gen_Delegate_Imp3905(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipLocalMovementController __Gen_Delegate_Imp3906(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSuperLaserEffectController __Gen_Delegate_Imp3907(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipEffectLODController __Gen_Delegate_Imp3908(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipGuildBuildingController __Gen_Delegate_Imp3909(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBranchStoryQuestExInfo> __Gen_Delegate_Imp391(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipCelestialWeaponDesc __Gen_Delegate_Imp3910(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipDroneInfoDesc __Gen_Delegate_Imp3911(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipLODController __Gen_Delegate_Imp3912(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipWeaponController __Gen_Delegate_Imp3913(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipSuperWeaponController __Gen_Delegate_Imp3914(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipEngineEffectController __Gen_Delegate_Imp3915(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipShieldController __Gen_Delegate_Imp3916(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CRIAudioSourceHelperImpl __Gen_Delegate_Imp3917(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipControllerLODDispatch __Gen_Delegate_Imp3918(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceShip __Gen_Delegate_Imp3919(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBranchStoryQuestListInfo __Gen_Delegate_Imp392(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SkinnedMeshRenderer __Gen_Delegate_Imp3920(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GameObject> __Gen_Delegate_Imp3921(object p0, int p1, int p2, out bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<GameObject> __Gen_Delegate_Imp3922(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabSuperWeaponDesc __Gen_Delegate_Imp3923(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabNormalWeaponDesc __Gen_Delegate_Imp3924(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IShipBurningController __Gen_Delegate_Imp3925(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BuildingNodeDisplaySwitcher __Gen_Delegate_Imp3926(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<WeaponSlotGroupConstructInfo> __Gen_Delegate_Imp3927(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3928(object p0, object p1, GrandFaction p2)
        {
        }

        [MethodImpl(0x8000)]
        public WeaponSlotGroupConstructInfo __Gen_Delegate_Imp3929(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBranchStoryQuestListInfo> __Gen_Delegate_Imp393(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public WeaponSlotGroupConstructInfo __Gen_Delegate_Imp3930(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Material __Gen_Delegate_Imp3931(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MeshFilter __Gen_Delegate_Imp3932(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper __Gen_Delegate_Imp3933(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSuperWeaponGroup __Gen_Delegate_Imp3934(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3935(object p0, int p1, object p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3936(object p0, int p1, object p2, object p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3937(object p0, int p1, object p2, float p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public BulletLaunchInfo __Gen_Delegate_Imp3938(object p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3939(object p0, uint p1, ushort p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBufEffectInfo __Gen_Delegate_Imp394(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3940(object p0, uint p1, ushort p2)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper __Gen_Delegate_Imp3941(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3942(object p0, int p1, int p2, object p3, float p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3943(object p0, int p1, int p2, object p3, float p4, float p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3944(object p0, int p1, int p2, object p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public BulletLaunchInfo __Gen_Delegate_Imp3945(object p0, int p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3946(object p0, DroneLaunchSide p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public WeaponSlotLODGroup __Gen_Delegate_Imp3947(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public DroneLaunchSide __Gen_Delegate_Imp3948(object p0, DroneLaunchSide p1)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3949(object p0, object p1, DroneLaunchSide p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBufEffectInfo> __Gen_Delegate_Imp395(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3950(object p0, int p1, out int p2, out int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<LogicWeaponSlot> __Gen_Delegate_Imp3951(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GameObject> __Gen_Delegate_Imp3952(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp3953(object p0, int p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public RealWeaponSlot __Gen_Delegate_Imp3954(object p0, Vector3 p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<RealWeaponSlot> __Gen_Delegate_Imp3955(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp3956(object p0, SubRankType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceDropBox __Gen_Delegate_Imp3957(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceObject __Gen_Delegate_Imp3958(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3959(object p0, object p1, object p2, object p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCameraShakeLevelConfig __Gen_Delegate_Imp396(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILODController __Gen_Delegate_Imp3960(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILODController __Gen_Delegate_Imp3961(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ControllerDesc> __Gen_Delegate_Imp3962(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ILODController __Gen_Delegate_Imp3963(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ControllerDesc[] __Gen_Delegate_Imp3964(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SelfSpaceShipLODController __Gen_Delegate_Imp3965(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public SelfSpaceShipLODController __Gen_Delegate_Imp3966(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSimpleObjectControllerLODDispatch __Gen_Delegate_Imp3967(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationLODController __Gen_Delegate_Imp3968(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceSimpleObject __Gen_Delegate_Imp3969(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCameraShakeLevelConfig> __Gen_Delegate_Imp397(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationControllerLODDispatch __Gen_Delegate_Imp3970(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceStation __Gen_Delegate_Imp3971(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabStarGateDesc __Gen_Delegate_Imp3972(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStarGate __Gen_Delegate_Imp3973(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3974(object p0, SpaceObjectMoveController.MoveState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3975(object p0, Vector3 p1, float p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3976(object p0, object p1, float p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3977(Vector3 p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp3978(Vector3 p0, Vector3 p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<Transform> __Gen_Delegate_Imp3979(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCameraShakeSourceTypeInfo __Gen_Delegate_Imp398(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3980(object p0, Vector3 p1, Vector3 p2, int p3, float p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3981(object p0, PlaySoundOption p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp3982(object p0, int p1, int p2, float p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp3983(object p0, bool p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public VectorLine __Gen_Delegate_Imp3984(object p0, Vector3 p1, int p2, int p3, float p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3985(object p0, Vector3 p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public TrailRenderer __Gen_Delegate_Imp3986(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3987(EnterSolarSystemType p0, bool p1, object p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<string> __Gen_Delegate_Imp3988()
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemTask.SolarSystemTaskUpdatePipeLineCtx __Gen_Delegate_Imp3989(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCameraShakeSourceTypeInfo> __Gen_Delegate_Imp399(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3990(object p0, TeleportEffect p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3991(object p0, int p1, uint p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3992(object p0, int p1, int p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3993(object p0, uint p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3994(object p0, uint p1, uint p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3995(object p0, GlobalSceneInfo? p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3996(object p0, SpaceObjectType p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public WeaponEquipAttackPromptType __Gen_Delegate_Imp3997(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp3998(object p0, bool p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceObject __Gen_Delegate_Imp3999(object p0, SpaceObjectType p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public AudioManager4CRI __Gen_Delegate_Imp4()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp40(object p0, object p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataChapterBranchQuestInfo __Gen_Delegate_Imp400(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4000(object p0, SpaceObjectType p1, uint p2, out Vector3D p3)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp4001(object p0, SpaceObjectType p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4002(float p0, EnterSolarSystemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public BulletMissileController __Gen_Delegate_Imp4003(object p0, uint p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public BulletDroneController __Gen_Delegate_Imp4004(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4005(object p0, bool p1, bool p2, EnterSolarSystemType p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4006(object p0, SpaceObjectType p1, uint p2, bool p3, bool p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4007(object p0, SpaceObjectType p1, uint p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4008(object p0, bool p1, bool p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4009(object p0, ref int[] p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataChapterBranchQuestInfo> __Gen_Delegate_Imp401(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4010(object p0, object p1, int p2, ref int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemTask.DisplayPerformance __Gen_Delegate_Imp4011(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4012(object p0, object p1, int p2, out int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4013(object p0, SpaceGrideIndex p1, double p2, double p3, Vector3D p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4014(object p0, uint p1, Vector3D p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildClient __Gen_Delegate_Imp4015(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4016(object p0, uint p1, SpaceObjectType p2, Vector3D p3, float p4, object p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4017(object p0, object p1, TargetListViewState p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4018(object p0, SpaceObjectType p1, uint p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4019(object p0, object p1, SpaceObjectType p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipInfo __Gen_Delegate_Imp402(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4020(object p0, object p1, int p2, int p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4021(object p0, uint p1, object p2, int p3, bool p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4022(object p0, object p1, object p2, out List<SyncEventCacheItem> p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4023(object p0, object p1, object p2, out bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4024(object p0, object p1, uint p2, uint p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4025(object p0, object p1, uint p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4026(object p0, object p1, object p2, StructProcessEquipLaunchBase p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4027(object p0, object p1, object p2, StructProcessSuperEquipAoeLaunch p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4028(object p0, object p1, object p2, StructProcessEquipLaunchBase p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4029(object p0, float p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCharChipInfo> __Gen_Delegate_Imp403(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4030(object p0, object p1, NpcShipAnimEffectType p2, uint p3, uint p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceGlobalEffectController __Gen_Delegate_Imp4031(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4032(object p0, object p1, LBSyncEventOnHitInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4033(object p0, object p1, uint p2, ulong p3, float p4, BufType p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4034(object p0, object p1, uint p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4035(object p0, object p1, SimSpaceShip.MoveCmd p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4036(object p0, object p1, double p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4037(object p0, object p1, object p2, object p3, object p4, ushort p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4038(object p0, object p1, object p2, DroneType p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4039(object p0, object p1, object p2, object p3, DroneType p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipRandomBufPoolInfo __Gen_Delegate_Imp404(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4040(object p0, SceneWingShipSetting p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp4041(object p0, Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D __Gen_Delegate_Imp4042(object p0, Vector3 p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp4043(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4044(bool p0)
        {
        }

        [MethodImpl(0x8000)]
        public SyncEventCacheItem __Gen_Delegate_Imp4045()
        {
        }

        [MethodImpl(0x8000)]
        public QueueSyncEventType __Gen_Delegate_Imp4046(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4047(object p0, QueueSyncEventType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXPlayerContext __Gen_Delegate_Imp4048()
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4049(SceneScreenEffectType p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCharChipRandomBufPoolInfo> __Gen_Delegate_Imp405(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4050(GlobalSceneInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4051(StoreItemType p0, int p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataInfectInfo __Gen_Delegate_Imp4052(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<string> __Gen_Delegate_Imp4053()
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4054(ShipType p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Sprite __Gen_Delegate_Imp4055(ShipType p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4056(object p0, SpaceObjectType p1, TargetListViewState p2, bool p3, bool p4, bool p5, bool p6, out string p7, out string p8)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4057(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4058(object p0, object p1, EnterSpaceStationType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4059(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSlotInfo __Gen_Delegate_Imp406(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4060(object p0, object p1, EnterSpaceStationType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4061(object p0, object p1, ChatExtraOutputLocationType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4062(object p0, ChatChannel p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4063(object p0, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4064(object p0, object p1, ActionPlanTabType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4065(object p0, SpaceStationUITask.PlayerListSortType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4066(object p0, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4067(object p0, object p1, ChatChannel p2)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess __Gen_Delegate_Imp4068(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess __Gen_Delegate_Imp4069(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCharChipSlotInfo> __Gen_Delegate_Imp407(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess __Gen_Delegate_Imp4070(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationBGUIBackgroupManager __Gen_Delegate_Imp4071(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4072(float p0, EnterSpaceStationType p1)
        {
        }

        [MethodImpl(0x8000)]
        public MotherShipUITask __Gen_Delegate_Imp4073(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4074(object p0, bool p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4075(object p0, int p1, object p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4076(object p0, ChatChannel p1, object p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4077(object p0, ActionPlanTabType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget __Gen_Delegate_Imp4078(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp4079(object p0, uint p1, PropertiesId p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSuitInfo __Gen_Delegate_Imp408(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4080(object p0, uint p1, PropertiesId p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemUITask.SolarSystemUITaskPipeLineCtx __Gen_Delegate_Imp4081(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockChatClient __Gen_Delegate_Imp4082(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemUITask.TargetItemUIInfo __Gen_Delegate_Imp4083(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInteractionMakerClient __Gen_Delegate_Imp4084(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4085(object p0, SolarSystemUITask.TargetListTableState p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4086(object p0, SolarSystemUITask.TargetListSortState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4087(object p0, bool p1, SolarSystemUITask.TargetFilterType p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4088(object p0, SpaceObjectType p1, bool p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemUITask.TargetFilterType __Gen_Delegate_Imp4089(object p0, SpaceObjectType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCharChipSuitInfo> __Gen_Delegate_Imp409(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemUITask.TargetListTableState __Gen_Delegate_Imp4090(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4091(object p0, SolarSystemUITask.TargetListTableState p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceWeaponEquipClient __Gen_Delegate_Imp4092(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4093(object p0, SolarSystemUITask.TargetListTableState p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4094(object p0, SolarSystemUITask.TargetListTableState p1, SolarSystemUITask.TargetListSortState p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo __Gen_Delegate_Imp4095(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4096(object p0, float p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public SceneLeaveNotfiyUITask __Gen_Delegate_Imp4097(float p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Image[] __Gen_Delegate_Imp4098(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4099(object p0, SolarSystemBuffNoticeItemUIController.BuffTypeUIInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp41(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharLevelInfo __Gen_Delegate_Imp410(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemBuffNoticeItemUIController __Gen_Delegate_Imp4100(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Transform __Gen_Delegate_Imp4101(object p0, ShipFightBufType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Transform[] __Gen_Delegate_Imp4102(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4103(object p0, SolarSystemBuffNoticeItemUIController.BuffTypeUIInfo p1, Vector3 p2, Vector3 p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4104(object p0, ShipFightBufType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4105(object p0, bool p1, int p2, int p3, string[] p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4106(object p0, SceneTextNoticeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemNoticeAndNPCChatUITask.SolarSystemNoticePipeLineCtx __Gen_Delegate_Imp4107(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4108(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4109(object p0, bool p1, object p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCharLevelInfo> __Gen_Delegate_Imp411(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4110(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4111(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public WormholeDebuffWarningUIController __Gen_Delegate_Imp4112(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4113(object p0, Vector3 p1, SubRankType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4114(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4115(object p0, bool p1, bool p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4116(object p0, uint p1, uint p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4117(object p0, float p1, float p2, float p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4118(object p0, uint p1, Vector3 p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4119(object p0, uint p1, LBSyncEventOnHitInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharProfessionInfo __Gen_Delegate_Imp412(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemUIDamageCountController __Gen_Delegate_Imp4120(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4121(object p0, bool p1, uint p2, SpaceObjectType p3, Vector3 p4, object p5, bool p6, double p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4122(object p0, uint p1, object p2, Vector3 p3, SpaceObjectType p4, bool p5, object p6, object p7, object p8, object p9, bool p10, object p11)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4123(object p0, uint p1, object p2, Vector3 p3, SpaceObjectType p4, bool p5, object p6, object p7, object p8, object p9, double p10, bool p11, object p12)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4124(object p0, uint p1, object p2, Vector3 p3, SpaceObjectType p4, bool p5, object p6, object p7, object p8, object p9, double p10, float p11, float p12, bool p13, object p14)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4125(object p0, object p1, Vector3 p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemTargetTagController __Gen_Delegate_Imp4126(object p0, uint p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemDropBoxTargetTagUIController __Gen_Delegate_Imp4127(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemTargetTagController __Gen_Delegate_Imp4128(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemDropBoxTargetTagUIController __Gen_Delegate_Imp4129(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCharProfessionInfo> __Gen_Delegate_Imp413(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Rect __Gen_Delegate_Imp4130(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4131(object p0, byte[] p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4132(object p0, float p1, object p2, SolarSystemTargetInteractionUIController.InteractType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4133(object p0, SolarSystemTargetInteractionUIController.InteractType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4134(object p0, bool p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public TweenMain[] __Gen_Delegate_Imp4135(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemTargetItemUIController __Gen_Delegate_Imp4136(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4137(object p0, double p1, float p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4138(object p0, SpaceObjectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4139(object p0, SpaceObjectType p1, bool p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharRangeInfo __Gen_Delegate_Imp414(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4140(object p0, bool p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4141(object p0, object p1, object p2, bool p3, SpaceObjectType p4, Vector3 p5, double p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4142(object p0, object p1, object p2, Vector3 p3, SpaceObjectType p4, bool p5, object p6, object p7, object p8, object p9, bool p10, object p11)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4143(object p0, object p1, object p2, Vector3 p3, SpaceObjectType p4, bool p5, object p6, object p7, bool p8, object p9, object p10, double p11, object p12)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4144(object p0, object p1, object p2, Vector3 p3, SpaceObjectType p4, bool p5, object p6, object p7, bool p8, object p9, object p10, double p11, object p12, float p13, float p14)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4145(object p0, int p1, int p2, int p3, int p4, int p5, double p6, float p7, float p8, float p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4146(object p0, object p1, SolarSystemUIManipulationSharp p2)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp4147(object p0, SolarSystemUIManipulationSharp p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4148(object p0, ShipFightBufType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4149(object p0, byte p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCharRangeInfo> __Gen_Delegate_Imp415(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TargetListViewState __Gen_Delegate_Imp4150(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4151(object p0, SpaceObjectType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4152(object p0, out float p1, out float p2)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemUITask.WeaponEquipGroupItemUIInfo __Gen_Delegate_Imp4153(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Image __Gen_Delegate_Imp4154(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public FrequentChangeText __Gen_Delegate_Imp4155(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<Button> __Gen_Delegate_Imp4156(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4157(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4158(object p0, string[][] p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4159(bool p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCMDailySignAccRewardData __Gen_Delegate_Imp416(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<string> __Gen_Delegate_Imp4160(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4161(object p0, long p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public GraphicQualityLevel __Gen_Delegate_Imp4162(object p0, DeviceRatingLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp4163(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProjectXGameManager.IosReviewResParams __Gen_Delegate_Imp4164(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4165(object p0, long p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4166(object p0, float p1, long p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp4167(SDKHelper.SharePlatform p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4168(object p0, out bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4169(SDKHelper.SpecialEditionMark p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCMDailySignAccRewardData> __Gen_Delegate_Imp417(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4170(SDKHelper.SpecialEditionMark p0)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp4171(DateTime p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4172(object p0, Playable p1)
        {
        }

        [MethodImpl(0x8000)]
        public ClipCaps __Gen_Delegate_Imp4173(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Playable __Gen_Delegate_Imp4174(object p0, PlayableGraph p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4175(object p0, Playable p1, FrameData p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Playable __Gen_Delegate_Imp4176(object p0, PlayableGraph p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4177(object p0, LanguageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4178(object p0, object p1, byte[] p2, int p3, LanguageType p4)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp4179(object p0, object p1, byte[] p2, int p3, LanguageType p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCommonFuncNpcTalkerSelectInfo __Gen_Delegate_Imp418(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4180(object p0, object p1, TranslateManager.TranslateAudio2StringResult p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp4181(object p0, object p1, byte[] p2, int p3, LanguageType p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public TranslateManager __Gen_Delegate_Imp4182()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4183(object p0, ActionPlanTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ActionPlanManagerUITask __Gen_Delegate_Imp4184(object p0, ActionPlanTabType p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4185(ActionPlanTabType p0)
        {
        }

        [MethodImpl(0x8000)]
        public ActionPlanManagerUITask __Gen_Delegate_Imp4186(object p0, int p1, object p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ActionPlanManagerUITask __Gen_Delegate_Imp4187(object p0, object p1, object p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4188(ActionPlanTabType p0, bool p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess __Gen_Delegate_Imp4189(object p0, bool p1, bool p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCommonFuncNpcTalkerSelectInfo> __Gen_Delegate_Imp419(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess __Gen_Delegate_Imp4190(object p0, bool p1, bool p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4191(object p0, ActionPlanTabType p1, bool p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4192(object p0, ActionPlanManagerUITask.PipeLineMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4193(object p0, ActionPlanManagerUITask.PipeLineMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockQuestClient __Gen_Delegate_Imp4194(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4195(object p0, GotoType p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4196(object p0, QuestType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4197(object p0, ShipHangarRepeatableUserGuideType p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4198(object p0, SceneType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4199(object p0, object p1, object p2, bool p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp42(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataConfigableConst __Gen_Delegate_Imp420(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIIntent __Gen_Delegate_Imp4200(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4201(object p0, ActivityInfo p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4202(object p0, int p1, out Vector3 p2, out ItemSimpleInfoUITask.PositionType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4203(object p0, long p1, long p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4204(object p0, int p1, float p2, long p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public ActivityInfoUITask __Gen_Delegate_Imp4205(bool p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4206(object p0, ActivityInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4207(object p0, object p1, int p2, Vector3 p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<ActivityInfo> __Gen_Delegate_Imp4208(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4209(object p0, ActivityInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataConfigableConst> __Gen_Delegate_Imp421(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4210(object p0, ActivityInfo p1, ActivityInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4211(object p0, object p1, Vector3 p2, ItemSimpleInfoUITask.PositionType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4212(object p0, ActionPlanTabType p1, int p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockActivityClient __Gen_Delegate_Imp4213(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp4214(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4215(object p0, ActivityInfo p1, PlayerActivityInfo p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4216(object p0, ActivityInfo p1, object p2, PlayerActivityInfo p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4217(object p0, PlayerActivityInfo p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataActivityInfo __Gen_Delegate_Imp4218(object p0, ActivityType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4219(object p0, object p1, int p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataConfigIDRangeInfo __Gen_Delegate_Imp422(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public BranchStoryUITask.BranchStoryItemInfo __Gen_Delegate_Imp4220(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4221(object p0, int p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public BranchStoryUITask.BranchStorySubQuestInfo __Gen_Delegate_Imp4222(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4223(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public BranchStoryPuzzleDetailItemUIController __Gen_Delegate_Imp4224(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4225(object p0, object p1, object p2, object p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public BranchStoryRealDetailItemUIController __Gen_Delegate_Imp4226(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4227(object p0, object p1, object p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public BranchStoryUITask __Gen_Delegate_Imp4228(bool p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4229(object p0, bool p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataConfigIDRangeInfo> __Gen_Delegate_Imp423(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4230(object p0, BranchStoryUITask.PipeLineMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4231(object p0, BranchStoryUITask.PipeLineMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public BranchStoryUITask.BranchStoryItemInfo __Gen_Delegate_Imp4232(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4233(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockBranchStoryClient __Gen_Delegate_Imp4234(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp4235(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4236(object p0, FactionCreditLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4237(object p0, FactionCreditLevel p1, FactionCreditLevel p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4238(object p0, FactionCreditLevel p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4239(object p0, FactionCreditLevel p1, int p2, object p3, float p4, long p5, object p6, object p7, int p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataConstellationFeatsPoolInfo __Gen_Delegate_Imp424(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditLevelInfo __Gen_Delegate_Imp4240(object p0, FactionCreditLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4241(object p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditLevel __Gen_Delegate_Imp4242(int p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4243(object p0, int p1, FactionCreditLevel p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4244(object p0, FactionCreditLevel p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataFactionCreditNpcInfo> __Gen_Delegate_Imp4245(object p0, int p1, FactionCreditLevel p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<FactionCreditLevel, List<ConfigDataFactionCreditNpcInfo>> __Gen_Delegate_Imp4246(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4247(object p0, bool p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataNpcShopItemInfo> __Gen_Delegate_Imp4248(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4249(GrandFaction p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataConstellationFeatsPoolInfo> __Gen_Delegate_Imp425(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4250(int p0, FactionCreditLevel p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4251(int p0, FactionCreditLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4252(int p0, FactionCreditLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditLevel __Gen_Delegate_Imp4253(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4254(object p0, FactionCreditLevel p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4255(object p0, int p1, int p2, GrandFaction p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4256(object p0, int p1, object p2, int p3, bool p4, bool p5, int p6, int p7, int p8, object p9, object p10)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditLevelInfo __Gen_Delegate_Imp4257(object p0, long p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4258(object p0, int p1, object p2, float p3, int p4, long p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4259(object p0, bool p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataConstellationInfo __Gen_Delegate_Imp426(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4260(object p0, int p1, FactionCreditLevel p2, FactionCreditLevel p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4261(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4262(int p0, FactionCreditLevel p1, FactionCreditLevel p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4263(object p0, object p1, bool p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4264(object p0, float p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4265(object p0, bool p1, float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditUITask __Gen_Delegate_Imp4266(bool p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp4267(int p0, FactionCreditLevel p1, object p2, out int p3, out int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4268(object p0, int p1, GrandFaction p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4269(object p0, int p1, int p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataConstellationInfo> __Gen_Delegate_Imp427(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<string> __Gen_Delegate_Imp4270(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo __Gen_Delegate_Imp4271(object p0, out bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4272(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public QuestAcceptDialogUITask __Gen_Delegate_Imp4273(object p0, object p1, object p2, object p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public QuestBackgroundUITask __Gen_Delegate_Imp4274()
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> __Gen_Delegate_Imp4275(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateController[] __Gen_Delegate_Imp4276(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4277(object p0, object p1, object p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4278(object p0, StoreItemType p1, int p2, bool p3, bool p4, long p5, int p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4279(object p0, QuestListCategoryUIController.CategoryQuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCrackBoxInfo __Gen_Delegate_Imp428(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListCategoryUIController.CategoryQuestType __Gen_Delegate_Imp4280(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4281(object p0, int p1, int p2, object p3, object p4, bool p5, bool p6, bool p7, bool p8, bool p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4282(object p0, object p1, int p2, object p3, object p4, bool p5, bool p6, bool p7, bool p8, bool p9)
        {
        }

        [MethodImpl(0x8000)]
        public UnAcceptedQuestUIInfo __Gen_Delegate_Imp4283(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4284(object p0, object p1, out QuestListCategoryUIController p2, QuestListCategoryUIController.CategoryQuestType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4285(object p0, object p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4286(object p0, object p1, object p2, object p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListCategoryUIController.CategoryQuestType __Gen_Delegate_Imp4287(object p0, QuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListCategoryUIController __Gen_Delegate_Imp4288(object p0, QuestListCategoryUIController.CategoryQuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestListItemUIController> __Gen_Delegate_Imp4289(object p0, QuestListCategoryUIController.CategoryQuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCrackBoxInfo> __Gen_Delegate_Imp429(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListItemUIController __Gen_Delegate_Imp4290(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListUITask __Gen_Delegate_Imp4291(object p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4292(object p0, bool p1, int p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4293(object p0, QuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo __Gen_Delegate_Imp4294(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4295(object p0, int p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4296(int p0, out NpcDNId p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4297(int p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualQuest __Gen_Delegate_Imp4298(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<bool, int> __Gen_Delegate_Imp4299()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp43(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCriminalLevelInfo __Gen_Delegate_Imp430(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4300(out bool p0, out bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<int, int> __Gen_Delegate_Imp4301()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4302(object p0, out List<QuestRewardInfo> p1, out List<QuestRewardInfo> p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4303(bool p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4304(QuestType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4305(object p0, out string p1, out float p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<VirtualBuffCategory, List<VirtualBuffDesc>> __Gen_Delegate_Imp4306(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool? __Gen_Delegate_Imp4307(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4308(int p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualQuest __Gen_Delegate_Imp4309(QuestType p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCriminalLevelInfo> __Gen_Delegate_Imp431(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4310(object p0, object p1, int p2, object p3, int p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public ChapterStorySubQuestUIController __Gen_Delegate_Imp4311(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4312(object p0, object p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ChapterStorySubQuestUIController __Gen_Delegate_Imp4313(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public StoryUITask __Gen_Delegate_Imp4314(bool p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4315(object p0, GotoType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4316(object p0, int p1, int p2, int p3, ulong p4, int p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4317(object p0, int p1, bool p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4318(object p0, object p1, int p2, int p3, ulong p4)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4319(object p0, bool p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCurrencyInfo __Gen_Delegate_Imp432(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp4320(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4321(object p0, NpcShopItemCategory p1, NpcShopItemType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4322(object p0, NpcShopItemCategory p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp4323(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4324(object p0, int p1, NpcShopItemCategory p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemCategoryToggleUIController __Gen_Delegate_Imp4325(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemCategoryToggleUIController __Gen_Delegate_Imp4326(object p0, NpcShopItemCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemCategoryToggleUIController __Gen_Delegate_Imp4327(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4328(object p0, Vector3 p1, AuctionSellItemInfoPanelUIController.PanelPosType p2)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4329(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCurrencyInfo> __Gen_Delegate_Imp433(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4330(object p0, object p1, bool p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo __Gen_Delegate_Imp4331(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4332(object p0, object p1, object p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4333(object p0, TimeSpan p1)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp4334(object p0, long p1, long p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4335(object p0, long p1, long p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4336(object p0, ItemStoreListUIController.ItemStoreTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4337(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4338(object p0, ulong p1, int p2, long p3, long p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4339(object p0, ulong p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCutInfo __Gen_Delegate_Imp434(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4340(object p0, AuctionBuyItemFilterType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4341(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4342(object p0, bool p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4343(object p0, object p1, NpcShopItemType p2, int p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4344(object p0, StoreItemType p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4345(object p0, AuctionUITask.PipeLineMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4346(object p0, AuctionUITask.PipeLineMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4347(object p0, NpcShopItemType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4348(object p0, int p1, Vector3 p2, ItemSimpleInfoUITask.PositionType p3)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionItemTypeListAck __Gen_Delegate_Imp4349(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCutInfo> __Gen_Delegate_Imp435(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionItemBuyAck __Gen_Delegate_Imp4350(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4351(object p0, int p1, int p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4352(AuctionBuyItemFilterType p0, AuctionBuyItemFilterType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4353(object p0, bool p1, bool p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionBuyItemFilterType __Gen_Delegate_Imp4354(RankType p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionBuyItemFilterType __Gen_Delegate_Imp4355(SubRankType p0)
        {
        }

        [MethodImpl(0x8000)]
        public AuctionBuyItemFilterType __Gen_Delegate_Imp4356(ShipSizeType p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4357(object p0, AuctionBuyItemFilterType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4358(object p0, AuctionBuyItemFilterType p1, AuctionBuyItemFilterType p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4359(object p0, AuctionBuyItemFilterType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCutTextInfo __Gen_Delegate_Imp436(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp4360(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4361(object p0, DateTime p1, DateTime p2, DateTime p3)
        {
        }

        [MethodImpl(0x8000)]
        public BaseRedeployUITask __Gen_Delegate_Imp4362(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public BaseRedeployUITask __Gen_Delegate_Imp4363(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem __Gen_Delegate_Imp4364(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4365(object p0, bool p1, int p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4366(object p0, ConfigDataBattlePassLevelInfo[] p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4367(object p0, object p1, object p2, int p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4368(object p0, object p1, object p2, int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4369(object p0, out int p1, out int p2, out int p3, out bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataCutTextInfo> __Gen_Delegate_Imp437(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestRewardInfo> __Gen_Delegate_Imp4370(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public TimeSpan __Gen_Delegate_Imp4371()
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4372(KeyValuePair<int, ConfigDataBattlePassChallangeQuestAccRewardInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController __Gen_Delegate_Imp4373(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem __Gen_Delegate_Imp4374(object p0, object p1, out ItemInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4375(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4376(object p0, object p1, bool p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4377(object p0, object p1, ResourceShopItemCategory p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4378(object p0, ulong p1, ulong p2, ulong p3, ulong p4)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateController __Gen_Delegate_Imp4379(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDailyLoginInfo __Gen_Delegate_Imp438(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4380(object p0, object p1, ResourceShopItemCategory p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4381(object p0, object p1, ResourceShopItemCategory p2, BlackMarketShopItemType p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4382(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4383(ResourceShopItemCategory p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4384(ResourceShopItemCategory p0, object p1, object p2, object p3, object p4, BlackMarketShopItemType p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataBlackMarketShopItemInfo> __Gen_Delegate_Imp4385(object p0, ResourceShopItemCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataBlackMarketShopItemInfo> __Gen_Delegate_Imp4386(object p0, ref List<ConfigDataBlackMarketShopItemInfo> p1, ResourceShopItemCategory p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockGuildClient __Gen_Delegate_Imp4387()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4388(object p0, object p1, object p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4389(object p0, ResourceShopItemCategory p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDailyLoginInfo> __Gen_Delegate_Imp439(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4390(ulong p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem __Gen_Delegate_Imp4391(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4392(bool p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeMonthlyCardClient __Gen_Delegate_Imp4393(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4394(PurchaseRechargeGoodsResult p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeItemClient __Gen_Delegate_Imp4395(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeOrderRewardViewUITask __Gen_Delegate_Imp4396(ulong p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeOrderClient __Gen_Delegate_Imp4397(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4398(object p0, object p1, object p2, Vector3 p3)
        {
        }

        [MethodImpl(0x8000)]
        public QuestRewardInfo __Gen_Delegate_Imp4399(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp44(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDamageResistInfo __Gen_Delegate_Imp440(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpecialGiftPackageRecommendUITask __Gen_Delegate_Imp4400(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4401(object p0, int p1, object p2, Vector3 p3)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRechargeGiftPackageClient __Gen_Delegate_Imp4402(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4403(object p0, int p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4404(object p0, object p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4405(object p0, CaptainFeatInfoPanelUIController.FeatInfoPanelState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4406(object p0, CaptainFeatInfoItemUIController.FeatItemState p1)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainFeatInfoItemUIController.FeatItemState __Gen_Delegate_Imp4407(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp4408(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4409(object p0, CaptainListUITask.CaptainSortType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDamageResistInfo> __Gen_Delegate_Imp441(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4410(object p0, int p1, ulong p2, long p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainManagerUITask __Gen_Delegate_Imp4411(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4412(object p0, object p1, Vector3 p2, object p3, ItemSimpleInfoUITask.PositionType p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4413(object p0, object p1, bool p2, bool p3, UIProcess.ProcessExecMode p4)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp4414(object p0, int p1, object p2, bool p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public ShipHangarBGUIBackgroundManager __Gen_Delegate_Imp4415(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4416(int p0, CaptainShipStateHelper.CaptainShipState p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4417(object p0, bool p1, bool p2, bool p3, UIProcess.ProcessExecMode p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4418(object p0, object p1, ulong p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4419(object p0, object p1, object p2, bool p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDelegateFightSignalPoolInfo __Gen_Delegate_Imp442(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainFeatInfoPanelUIController __Gen_Delegate_Imp4420(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainFeatInfoPanelUIController __Gen_Delegate_Imp4421(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4422(object p0, int p1, WingManItemButtonState p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4423(object p0, int p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4424(NpcCaptainState p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4425(object p0, object p1, int p2, int p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainLevelUpFeatItemUIController __Gen_Delegate_Imp4426(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainLevelUpReportUITask __Gen_Delegate_Imp4427(ulong p0, int p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public NewCaptainGetUITask __Gen_Delegate_Imp4428(ulong p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp4429(object p0, int p1, out ItemSimpleInfoUITask.PositionType p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDelegateFightSignalPoolInfo> __Gen_Delegate_Imp443(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainMemoirsBookUIController __Gen_Delegate_Imp4430(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainRetireReportUITask __Gen_Delegate_Imp4431(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4432(object p0, CaptainRetireUIController.ChooseFeatState p1)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainRetireUITask __Gen_Delegate_Imp4433(ulong p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<StoreItemUpdateInfo> __Gen_Delegate_Imp4434(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4435(object p0, object p1, long p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public CaptainTrainingUITask __Gen_Delegate_Imp4436(ulong p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4437(object p0, long p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4438(object p0, ulong p1, int p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public WingManItemButtonState __Gen_Delegate_Imp4439(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDelegateMineralSignalDifficultInfo __Gen_Delegate_Imp444(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4440(object p0, object p1, object p2, int p3, object p4, bool p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterChipSlotItemUIController[] __Gen_Delegate_Imp4441(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4442(object p0, int p1, object p2, int p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4443(object p0, int p1, object p2, bool p3, bool p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4444(object p0, int p1, ulong p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterChipSwitchOrUnlockSchemeConfirmUITask __Gen_Delegate_Imp4445(int p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterChipSwitchOrUnlockSchemeConfirmUITask __Gen_Delegate_Imp4446(int p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4447(object p0, CharacterInfoUITask.TabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterBasicInfoPanelUIController __Gen_Delegate_Imp4448(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterOverviewInfoUIController __Gen_Delegate_Imp4449(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDelegateMineralSignalDifficultInfo> __Gen_Delegate_Imp445(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterWalletUIController __Gen_Delegate_Imp4450(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4451(object p0, CharacterWalletUIController.WalletCurrencyType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCurrencyInfo __Gen_Delegate_Imp4452(CharacterWalletUIController.WalletCurrencyType p0)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterInfoUITask __Gen_Delegate_Imp4453(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4454(object p0, int p1, int p2, int p3, int p4, int p5, int p6, int p7, int p8, bool p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4455(object p0, ulong p1, ulong p2, ulong p3, ulong p4, ulong p5, ulong p6, CharacterWalletUIController.WalletCurrencyType p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4456(object p0, CharacterWalletUIController.WalletCurrencyType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4457(object p0, ref bool p1, ref bool p2, ref bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterChipClient __Gen_Delegate_Imp4458(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterKillRecordDetailInfoUITask __Gen_Delegate_Imp4459(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDelegateSignalLevelLimit __Gen_Delegate_Imp446(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4460(object p0, object p1, ItemCompensationStatus p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4461(object p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4462(object p0, PropertyCategory p1, int p2, int p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterClient __Gen_Delegate_Imp4463(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4464(object p0, float p1, float p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4465(object p0, object p1, CharacterSimpleInfoUITask.DiplomacyPostionType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4466(object p0, CharacterSimpleInfoUITask.DiplomacyPostionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4467(object p0, CharacterSimpleInfoUITask.ButtonType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSimpleInfoUITask __Gen_Delegate_Imp4468(object p0, object p1, Vector3 p2, Vector2 p3, float p4, CharacterSimpleInfoUITask.PostionType p5, CharacterSimpleInfoUITask.DiplomacyPostionType p6, object p7, object p8, bool p9, object p10)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4469(object p0, bool p1, DiplomacyState p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDelegateSignalLevelLimit> __Gen_Delegate_Imp447(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4470(object p0, PropertyCategory p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4471(object p0, bool p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillDetailInfoUITask __Gen_Delegate_Imp4472(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4473(ref ConfigDataPassiveSkillInfo p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCharacterSkillClient __Gen_Delegate_Imp4474(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4475(object p0, PropertyCategory p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4476(object p0, PropertyCategory p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4477(object p0, PropertyCategory p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4478(object p0, object p1, object p2, int p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillTypeInfo __Gen_Delegate_Imp4479(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDelegateTransportSignalDifficultInfo __Gen_Delegate_Imp448(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillCategoryToggleUIController __Gen_Delegate_Imp4480(object p0, PropertyCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CharacterSkillCategoryToggleUIController> __Gen_Delegate_Imp4481(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillTreeUIController __Gen_Delegate_Imp4482(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillOverViewUIController __Gen_Delegate_Imp4483(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess __Gen_Delegate_Imp4484(object p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4485(object p0, PropertyCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillUITask __Gen_Delegate_Imp4486(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp4487(object p0, PropertyCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSkillTreeNodeUIController __Gen_Delegate_Imp4488(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4489(int p0, object p1, QuestType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDelegateTransportSignalDifficultInfo> __Gen_Delegate_Imp449(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4490(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4491(PropertyCategory p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4492(object p0, float[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerVoiceHandleThread.VoicePacket __Gen_Delegate_Imp4493(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] __Gen_Delegate_Imp4494(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerVoiceHandleThread __Gen_Delegate_Imp4495()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4496(object p0, ChatChannel p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChatLinkSendUITask __Gen_Delegate_Imp4497(StoreItemType p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ChatLinkSendUITask __Gen_Delegate_Imp4498(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChatLinkSendUITask __Gen_Delegate_Imp4499(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp45(object p0, object p1, out string p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDevelopmentFilterInfo __Gen_Delegate_Imp450(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChatLinkSendUITask __Gen_Delegate_Imp4500(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ChatLinkSendUITask __Gen_Delegate_Imp4501(ulong p0, int p1, GuildBattleType p2, GuildBuildingType p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentItem __Gen_Delegate_Imp4502(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentKillRecord __Gen_Delegate_Imp4503(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentDelegateMission __Gen_Delegate_Imp4504(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentShip __Gen_Delegate_Imp4505(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentGuild __Gen_Delegate_Imp4506(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentPlayer __Gen_Delegate_Imp4507(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentSailReport __Gen_Delegate_Imp4508()
        {
        }

        [MethodImpl(0x8000)]
        public ChatContentGuildBattleReport __Gen_Delegate_Imp4509(ulong p0, int p1, GuildBattleType p2, GuildBuildingType p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDevelopmentFilterInfo> __Gen_Delegate_Imp451(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4510(object p0, ChatContentType p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4511(object p0, ChatChannel p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4512(object p0, ChatChannel p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4513(object p0, bool p1, ChatChannel p2, ChatLanguageChannel p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4514(object p0, bool p1, ChatLanguageChannel p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4515(object p0, ChatChannel p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ChatSendHelper __Gen_Delegate_Imp4516()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp4517(object p0, ChatContentType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4518(object p0, ChatContentType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp4519(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDevelopmentNodeDescInfo __Gen_Delegate_Imp452(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public EmojiParseController __Gen_Delegate_Imp4520(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4521(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4522(object p0, object p1, ChatLanguageChannel p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4523(object p0, object p1, ChatLanguageChannel p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4524(object p0, ChatLanguageChannel p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4525(int p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4526(int p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4527(ChatChannel p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChatUIInfo __Gen_Delegate_Imp4528()
        {
        }

        [MethodImpl(0x8000)]
        public ChatUITask __Gen_Delegate_Imp4529(object p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDevelopmentNodeDescInfo> __Gen_Delegate_Imp453(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ChatUITask __Gen_Delegate_Imp4530(ChatChannel p0, object p1, object p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4531(ChatChannel p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4532(ChatChannel p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ChatLanguageChannel __Gen_Delegate_Imp4533()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4534(object p0, ChatChannel p1, ChatLanguageChannel p2, object p3, ref bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4535(object p0, ChatChannel p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ChatContentVoice> __Gen_Delegate_Imp4536(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ChatContentVoice> __Gen_Delegate_Imp4537(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4538(object p0, object p1, object p2, Vector2 p3, float p4, float p5, float p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4539(object p0, ulong p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDevelopmentProjectInfo __Gen_Delegate_Imp454(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CGPlayerUITask __Gen_Delegate_Imp4540(object p0, object p1, CGPlayerUITask.CGLogicType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4541(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4542(object p0, CommonBackAndCloseButtonUIController.CommonBackAndCloseUIMode p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonBackAndCloseUITask __Gen_Delegate_Imp4543(object p0, object p1, object p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4544(object p0, object p1, bool p2, bool p3, object p4, int p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4545(object p0, object p1, bool p2, object p3, int p4, DiplomacyState p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4546(object p0, object p1, int p2, bool p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4547(object p0, int p1, ProfessionType p2, int p3, object p4, DiplomacyState p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4548(object p0, ProfessionType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4549(object p0, DiplomacyState p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDevelopmentProjectInfo> __Gen_Delegate_Imp455(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4550(object p0, uint p1, uint p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4551(object p0, DiplomacyState p1, DiplomacyState p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public CommonDiplomacySetWndUITask __Gen_Delegate_Imp4552(object p0, uint p1, uint p2, Vector3 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4553(object p0, bool p1, DiplomacyState p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4554(object p0, object p1, uint p2, uint p3, DiplomacyState p4)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyState __Gen_Delegate_Imp4555(object p0, object p1, uint p2, uint p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceSimplestInfo> __Gen_Delegate_Imp4556(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4557(object p0, object p1, uint p2, uint p3, DeplomacyOptType p4)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4558(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4559(object p0, object p1, object p2, object p3, object p4, object p5, bool p6, CommonItemDetailInfoUIController.DetailInfoTabType p7, bool p8)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDeviceSetting __Gen_Delegate_Imp456(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4560(object p0, object p1, object p2, object p3, object p4, object p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4561(object p0, CommonItemDetailInfoUIController.DetailInfoUIMode p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4562(object p0, object p1, float p2, float p3, bool p4, DigitFormatUtil.DigitFormatType p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4563(object p0, object p1, object p2, object p3, float p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4564(object p0, object p1, bool p2, bool p3, int p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4565(object p0, object p1, bool p2, bool p3, long p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4566(object p0, object p1, bool p2, bool p3, object p4, object p5, object p6, object p7, object p8, bool p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4567(object p0, CommonItemIconUIController.CommonItemIconDataType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController.CommonItemIconDataType __Gen_Delegate_Imp4568(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4569(object p0, CommonItemIconUIController.CommonItemIconDataType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDeviceSetting> __Gen_Delegate_Imp457(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4570(StoreItemType p0, object p1, bool p2, bool p3, object p4, object p5, long p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4571(StoreItemType p0, object p1, bool p2, bool p3, object p4, object p5, object p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4572(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4573(object p0, object p1, long p2, long p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemUseUITask __Gen_Delegate_Imp4574(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CurrencyUpdateInfo> __Gen_Delegate_Imp4575(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemUseUITask __Gen_Delegate_Imp4576(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<CommonMenuItemUIController> __Gen_Delegate_Imp4577(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonMenuItemUIController __Gen_Delegate_Imp4578(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonMenuItemUIController.CommonMenuTreeInfo __Gen_Delegate_Imp4579(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDeviceSystemInfo __Gen_Delegate_Imp458(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonMenuItemUIController.CommonMenuItemKey __Gen_Delegate_Imp4580(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<object> __Gen_Delegate_Imp4581(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ToggleGroup __Gen_Delegate_Imp4582(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4583(object p0, int p1, Color p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public CommonPopupWndManagerUITask __Gen_Delegate_Imp4584()
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4585(object p0, object p1, object p2, object p3, bool p4, object p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4586(object p0, object p1, object p2, object p3, object p4, bool p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp4587(object p0, DateTime p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4588(object p0, object p1, object p2, object p3, bool p4, bool p5, out bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4589(object p0, object p1, object p2, object p3, object p4, bool p5, bool p6, out bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDeviceSystemInfo> __Gen_Delegate_Imp459(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4590(object p0, object p1, object p2, ref int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo __Gen_Delegate_Imp4591(object p0, StoreItemType p1, int p2, int p3, QuestRewardType p4)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController __Gen_Delegate_Imp4592(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp4593(object p0, float p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4594(object p0, object p1, WeaponCategory p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4595(object p0, object p1, bool p2, object p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo __Gen_Delegate_Imp4596(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public CrimePromptUITask __Gen_Delegate_Imp4597(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CutScenePlayerUITask __Gen_Delegate_Imp4598(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ItemSimpleInfoUITask.PositionType __Gen_Delegate_Imp4599(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CRIDesc.SheetDesc __Gen_Delegate_Imp46(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDisplayEffectCreateRateTypeInfo __Gen_Delegate_Imp460(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4600(object p0, GuildAllianceLanguageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType __Gen_Delegate_Imp4601(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4602(KeyValuePair<int, ConfigDataGuildAllianceLanguageTypeInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4603(ulong p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4604(object p0, object p1, long p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4605(object p0, bool p1, StoreItemType p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4606(StoreItemType p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogOutBoxUITask __Gen_Delegate_Imp4607(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogOutBoxUITask __Gen_Delegate_Imp4608(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4609(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDisplayEffectCreateRateTypeInfo> __Gen_Delegate_Imp461(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4610(object p0, bool p1, bool p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public LossWarningWindowUITask __Gen_Delegate_Imp4611(bool p0, bool p1, bool p2, Vector3 p3, Vector2 p4, float p5, LossWarningWindowUITask.LossWarningWindowPos p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4612(object p0, object p1, int p2, int p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4613(bool p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4614(object p0, object p1, int p2, int p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4615(int p0, bool p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4616(ulong p0, bool p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4617(bool p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4618(object p0, RescueSignalType p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4619(object p0, RescueSignalType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDisplayObjectGroupInfo __Gen_Delegate_Imp462(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public RescueSignalType __Gen_Delegate_Imp4620(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4621(object p0, RescueSignalType p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4622(object p0, RescueSignalType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4623(object p0, RescueSignalType p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4624(object p0, RescueSignalType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4625(int p0, int p1, object p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public SailReportUITask __Gen_Delegate_Imp4626(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4627(object p0, SceneScreenEffectType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4628(object p0, SceneScreenEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4629(SceneScreenEffectType p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDisplayObjectGroupInfo> __Gen_Delegate_Imp463(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SceneScreenEffectType __Gen_Delegate_Imp4630(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4631(object p0, bool p1, bool p2, Color p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4632(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4633(object p0, object p1, bool p2, bool p3, bool p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp4634(object p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4635(object p0, bool p1, bool p2, UIProcess.ProcessExecMode p3)
        {
        }

        [MethodImpl(0x8000)]
        public ShipCustomTemplateUITask __Gen_Delegate_Imp4636(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ShipCustomTemplateUITask __Gen_Delegate_Imp4637(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4638(object p0, object p1, ShipEquipSlotType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4639(object p0, object p1, NpcShopItemType p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDrivingLicenseInfo __Gen_Delegate_Imp464(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4640(object p0, object p1, out ShipCustomTemplateInfo p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4641(object p0, object p1, object p2, out ShipCustomTemplateInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfHighSlotInfo __Gen_Delegate_Imp4642(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipCustomTemplateInfo> __Gen_Delegate_Imp4643(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4644(object p0, StoreItemType p1, int p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4645(object p0, bool p1, int p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4646(object p0, object p1, object p2, object p3, ShipEquipSlotType p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4647(object p0, object p1, double p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp4648(object p0, object p1, out ItemSimpleInfoUITask.PositionType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4649(object p0, ShipType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDrivingLicenseInfo> __Gen_Delegate_Imp465(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4650(object p0, ShipType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4651(object p0, object p1, int p2, int p3, bool p4, object p5, int p6, int p7, bool p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4652(object p0, object p1, NpcShopItemType p2)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController __Gen_Delegate_Imp4653(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4654(object p0, out float p1, out string p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4655(object p0, object p1, Rect p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp4656(object p0, object p1, Rect p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4657(object p0, Rect p1)
        {
        }

        [MethodImpl(0x8000)]
        public Bounds __Gen_Delegate_Imp4658(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<Text, CommonUIStateController> __Gen_Delegate_Imp4659(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDroneInfo __Gen_Delegate_Imp466(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4660(object p0, object p1, TipWindowUITask.TipType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<Text, TweenMain> __Gen_Delegate_Imp4661(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4662(StringTableId p0, bool p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4663(int p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4664(SystemFuncType p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4665(object p0, Vector3 p1, Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp4666(object p0, object p1, Vector3 p2, Vector2 p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4667(object p0, CrackBGController.PlatformAnimMode p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4668(object p0, CrackBGController.BoxAnimMode p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp4669(object p0, int p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDroneInfo> __Gen_Delegate_Imp467(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4670(object p0, object p1, CrackSlotUIController.SlotState p2)
        {
        }

        [MethodImpl(0x8000)]
        public Sprite __Gen_Delegate_Imp4671(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public CrackSlotUIController.SlotState __Gen_Delegate_Imp4672(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CrackRewardItemUIController __Gen_Delegate_Imp4673(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Task __Gen_Delegate_Imp4674(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4675(object p0, CrackStoreUIController.BoxSortState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4676(object p0, StoreItemUpdateInfo p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCrackClient __Gen_Delegate_Imp4677(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4678(object p0, StoreItemType p1, object p2, bool p3, object p4, long p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4679(object p0, CrackSlotUIController.SlotState p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDynamicSceneLocationInfo __Gen_Delegate_Imp468(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CrackSlotUIController.SlotState __Gen_Delegate_Imp4680(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4681(object p0, CrackStoreUIController.BoxSortState p1)
        {
        }

        [MethodImpl(0x8000)]
        public ChapterBeginUITask __Gen_Delegate_Imp4682(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDevelopmentProjectInfo __Gen_Delegate_Imp4683(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4684(object p0, DevelopmentBgUITask.BackGroundStateType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4685(object p0, DevelopmentBgUITask.BackGroundStateType p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4686(object p0, object p1, object p2, DevelopmentTransformationUITask.ItemDisplayInfo[] p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4687(object p0, DevelopmentTransformationUITask.ItemDisplayInfo[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4688(object p0, bool p1, SystemFuncDescType p2)
        {
        }

        [MethodImpl(0x8000)]
        public DevelopmentBGUIBackgroupManager __Gen_Delegate_Imp4689(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDynamicSceneLocationInfo> __Gen_Delegate_Imp469(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDevelopmentNodeDescInfo __Gen_Delegate_Imp4690(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4691(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4692(object p0, object p1, Vector2Int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2Int __Gen_Delegate_Imp4693(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4694(object p0, Vector2Int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4695(object p0, ConfigDataDevelopmentNodeDescInfo[] p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4696(object p0, object p1, int p2, DevelopmentTransformationUITask.ItemDisplayInfo[] p3)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem __Gen_Delegate_Imp4697(object p0, StoreItemUpdateInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4698(object p0, object p1, object p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4699(object p0, ref List<string> p1)
        {
        }

        [MethodImpl(0x8000)]
        public CRIDesc.SheetDesc __Gen_Delegate_Imp47(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataElecDamageResistInfo __Gen_Delegate_Imp470(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4700(object p0, object p1, Vector3 p2, ItemSimpleInfoUITask.PositionType p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4701(object p0, ref List<FakeLBStoreItem> p1)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem __Gen_Delegate_Imp4702(int p0, ref List<FakeLBStoreItem> p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4703(object p0, uint p1, uint p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4704(object p0, object p1, DiplomacyState p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4705(object p0, object p1, bool p2, object p3, uint p4, uint p5, bool p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4706(object p0, DiplomacyItemType p1, DiplomacyState p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4707(object p0, bool p1, object p2, uint p3, uint p4, uint p5, uint p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4708(object p0, GuildAllianceLanguageType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4709(object p0, bool p1, bool p2, DiplomacyState p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataElecDamageResistInfo> __Gen_Delegate_Imp471(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4710(object p0, DiplomacyManagementUITask.SearchType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4711(object p0, DiplomacyManagementUITask.SearchType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4712(object p0, DiplomacyItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4713(object p0, DiplomacyItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyItemType __Gen_Delegate_Imp4714(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4715(object p0, bool p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4716(object p0, DiplomacyState p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4717(object p0, DiplomacyState p1, object p2, object p3, DiplomacyState p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4718(object p0, object p1, object p2, object p3, DiplomacyState p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4719(object p0, object p1, DiplomacyState p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataEquipCategoryInfo __Gen_Delegate_Imp472(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4720(object p0, object p1, bool p2, DiplomacyState p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4721(object p0, object p1, DiplomacyState p2, DeplomacyOptType p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyState __Gen_Delegate_Imp4722(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4723(object p0, bool p1, DiplomacyItemType p2, DiplomacyState p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSimplestInfo> __Gen_Delegate_Imp4724(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceBasicInfo> __Gen_Delegate_Imp4725(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4726(object p0, int p1, out List<string> p2, out List<int> p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4727(object p0, int p1, out List<uint> p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4728(DiplomacyItemType p0, DiplomacyItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4729(object p0, DiplomacyItemType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataEquipCategoryInfo> __Gen_Delegate_Imp473(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DrivingLiscesceNodeListItemUIController __Gen_Delegate_Imp4730(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp4731(object p0, ShipType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Task __Gen_Delegate_Imp4732(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4733(object p0, DrivingLicenseInfoUITask.PipeLineMask p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4734(object p0, DrivingLicenseInfoUITask.PipeLineMask p1)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp4735(object p0, GrandFaction p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4736(object p0, GrandFaction p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockDrivingLicenseClient __Gen_Delegate_Imp4737(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DrivingLicenseInfoUITask __Gen_Delegate_Imp4738(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4739(object p0, object p1, int p2, int p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataEquipEffectVirtualBufInfo __Gen_Delegate_Imp474(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Task __Gen_Delegate_Imp4740(object p0, int p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4741(object p0, object p1, Vector3 p2, bool p3, ItemSimpleInfoUITask.PositionType p4)
        {
        }

        [MethodImpl(0x8000)]
        public DrivingLiscenseCheckUITask __Gen_Delegate_Imp4742(object p0, int p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4743(object p0, out int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4744(object p0, Vector3 p1, object p2, Vector3 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4745(object p0, GameFunctionLockEffectType p1, object p2, object p3, object p4, bool p5, SystemFuncType p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp4746(object p0, GameFunctionLockEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4747(object p0, GameFunctionLockEffectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4748(object p0, GameFunctionLockEffectType p1, object p2, object p3, object p4, object p5, object p6, bool p7, object p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public GameFunctionLockAndUnlockUITask __Gen_Delegate_Imp4749()
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataEquipEffectVirtualBufInfo> __Gen_Delegate_Imp475(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int[] __Gen_Delegate_Imp4750(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4751(object p0, GMPanelController.NPCState p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4752(object p0, int p1, object p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4753(object p0, object p1, bool p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4754(object p0, object p1, ulong p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4755(object p0, object p1, object p2, object p3, GuildActionUITask.Tab p4, int p5, int p6, ulong p7, int p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4756(object p0, UIProcess.ProcessExecMode p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4757(object p0, object p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public GuildActionUITask __Gen_Delegate_Imp4758(object p0, object p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4759(ref int p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataEquipLaunchLimitInfo __Gen_Delegate_Imp476(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4760(int p0, int p1, ref int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4761(object p0, int p1, out string p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildActionInfoAck __Gen_Delegate_Imp4762(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildActionCreateAck __Gen_Delegate_Imp4763(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildActionDeleteAck __Gen_Delegate_Imp4764(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4765(object p0, AllianceCreateUIController.Mode p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4766(object p0, object p1, AllianceCreateUIController.Mode p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4767(uint p0, ulong p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4768(object p0, uint p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4769(uint p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataEquipLaunchLimitInfo> __Gen_Delegate_Imp477(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4770(uint p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp4771()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4772(object p0, AllianceMemberUIController.Mode p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4773(object p0, object p1, object p2, object p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4774(uint p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompensationCreateAck __Gen_Delegate_Imp4775(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompensationCloseAck __Gen_Delegate_Imp4776(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompensationDonateAck __Gen_Delegate_Imp4777(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompensationListAck __Gen_Delegate_Imp4778(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompensationAck __Gen_Delegate_Imp4779(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataEquipLimitInfo __Gen_Delegate_Imp478(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCompensationConfirmAck __Gen_Delegate_Imp4780(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4781(object p0, ShipType p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4782(object p0, CurrencyType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4783(object p0, object p1, bool p2, int p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4784(object p0, bool p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4785(object p0, uint p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4786(object p0, GuildAllianceLanguageType p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildJoinAck __Gen_Delegate_Imp4787(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4788(object p0, object p1, object p2, int p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4789(object p0, long p1, float p2, object p3, object p4, object p5, object p6, TimeSpan p7)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataEquipLimitInfo> __Gen_Delegate_Imp479(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarBGUITask __Gen_Delegate_Imp4790(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticShip __Gen_Delegate_Imp4791(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<float, Bounds> __Gen_Delegate_Imp4792(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4793(object p0, object p1, object p2, ulong p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4794(object p0, object p1, object p2, int p3, int p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4795(object p0, object p1, int p2, int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4796(object p0, object p1, int p2, int p3, object p4, int p5, int p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4797(object p0, object p1, int p2, int p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4798(object p0, object p1, object p2, int p3, int p4, long p5, int p6)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarUIController.UIMode __Gen_Delegate_Imp4799(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SheetRef __Gen_Delegate_Imp48(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataEquipShipLimitInfo __Gen_Delegate_Imp480(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4800(object p0, GuildFlagShipHangarUIController.UIMode p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4801(object p0, object p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4802(object p0, HangarShipUniqueWeaponEquipTipWndUIController.UniqueWeaponEquipTipState p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4803(object p0, bool p1, bool p2, ulong p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4804(object p0, ulong p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4805(object p0, ulong p1, int p2, int p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4806(object p0, ulong p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4807(object p0, object p1, bool p2, bool p3, bool p4, UIProcess.ProcessExecMode p5)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4808(object p0, bool p1, UIProcess.ProcessExecMode p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup __Gen_Delegate_Imp4809(object p0, ShipEquipSlotType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataEquipShipLimitInfo> __Gen_Delegate_Imp481(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarBGUIBackgroundManager __Gen_Delegate_Imp4810(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildFlagShipHangarVersionInfo __Gen_Delegate_Imp4811(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4812(object p0, bool p1, bool p2, ulong p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4813(object p0, ulong p1, int p2, int p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4814(GuildFlagShipOptLogType p0)
        {
        }

        [MethodImpl(0x8000)]
        public FleetPosition __Gen_Delegate_Imp4815(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4816(object p0, GuildFleetDetailsUIController.GuildFleetDetailSortType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4817(object p0, int p1, int p2, int p3, bool p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4818(object p0, bool p1, bool p2, bool p3, bool p4, GuildFleetPersonalSetting p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4819(object p0, bool p1, GuildFleetPersonalSetting p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataErrorCodeTip __Gen_Delegate_Imp482(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4820(object p0, ulong p1, object p2, FleetPosition p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4821(object p0, ulong p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4822(object p0, GuildFleetPersonalSetting p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberDynamicInfo __Gen_Delegate_Imp4823(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4824(object p0, int p1, bool p2, int p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4825(object p0, FleetPosition p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4826(object p0, FleetPosition p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4827(object p0, ulong p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4828(object p0, ulong p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4829(object p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataErrorCodeTip> __Gen_Delegate_Imp483(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4830(object p0, object p1, object p2, object p3, bool p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetManagementUITask __Gen_Delegate_Imp4831(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4832(object p0, int p1, int p2, int p3, int p4, object p5, LogoHelper.LogoUsageType p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4833(object p0, int p1, int p2, int p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4834(object p0, object p1, LogoHelper.LogoUsageType p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildLogoEditorUIController.AssetWithId> __Gen_Delegate_Imp4835(object p0, LogoHelper.LogoUsageType p1, ref int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4836(object p0, LogoHelper.LogoUsageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4837(object p0, GuildInfoEditorController.GuildInfoEditAction p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4838(object p0, GuildInfoEditNetTask.GuildInfoEditAction p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4839(object p0, GuildInfoEditNetTask.GuildInfoEditAction p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataExploreTicketInfo __Gen_Delegate_Imp484(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4840(object p0, GuildJoinPolicy p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildInfoUITask __Gen_Delegate_Imp4841(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4842(object p0, uint p1, bool p2, bool p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4843(object p0, uint p1, ushort p2, ushort p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4844(object p0, GuildManagerTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4845(uint p0, object p1, object p2, object p3, object p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4846(object p0, GuildManagerTabType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4847(bool p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4848(GuildJobType p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp4849(int p0, int p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataExploreTicketInfo> __Gen_Delegate_Imp485(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4850(GuildBuildingType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4851(GuildBattleType p0, GuildBuildingType p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4852(GuildBuildingType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4853(GuildBuildingType p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4854(GuildBuildingType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4855(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4856(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4857(int p0, object p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4858(int p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4859(int p0, object p1, out string p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditLevelInfo __Gen_Delegate_Imp486(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4860(GuildProductionSpeedUpLevel p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<NpcShopItemCategory> __Gen_Delegate_Imp4861()
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4862(FleetPosition p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4863(double p0, double p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4864(int p0, LogoHelper.LogoPositionType p1, LogoHelper.LogoUsageType p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4865(LogoHelper.LogoPositionType p0, LogoHelper.LogoUsageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp4866(int p0, LogoHelper.LogoPositionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4867(LogoHelper.LogoPositionType p0, LogoHelper.LogoUsageType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStoreItemClient> __Gen_Delegate_Imp4868(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4869(object p0, int p1, out IStaticFlagShipDataContainer p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFactionCreditLevelInfo> __Gen_Delegate_Imp487(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType __Gen_Delegate_Imp4870(LanguageType p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogoHelper.LogoUsageType __Gen_Delegate_Imp4871(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIUtil.CostCurrencyType __Gen_Delegate_Imp4872(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4873(object p0, CommonUIUtil.CostCurrencyType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Sprite __Gen_Delegate_Imp4874(object p0, GuildBuildingType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildWarUITask __Gen_Delegate_Imp4875(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberApplyForListUITask __Gen_Delegate_Imp4876(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildJoinApplyListAck __Gen_Delegate_Imp4877(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4878(object p0, object p1, bool p2, GuildMemberInfo[] p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4879(object p0, object p1, GuildMemberInfo[] p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditLevelRewardInfo __Gen_Delegate_Imp488(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4880(object p0, LogicBlockGuildClient.MemberTitleType p1, LogicBlockGuildClient.MemberTitleArrowState p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberListUITask __Gen_Delegate_Imp4881(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4882(object p0, GuildJobType p1, GuildJobType p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberStaffingLogUITask __Gen_Delegate_Imp4883(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4884(object p0, GuildMemberUITask.SubSysType p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp4885(object p0, object p1, UIProcess.ProcessExecMode p2, bool p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<CharacterSimpleInfoUITask.ButtonType, CharacterSimpleInfoUITask.ButtonState> __Gen_Delegate_Imp4886(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public CharacterSimpleInfoUITask.PostionType __Gen_Delegate_Imp4887(object p0, object p1, Vector3 p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4888(object p0, CrossScrollRectType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4889(object p0, GuildJobType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFactionCreditLevelRewardInfo> __Gen_Delegate_Imp489(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4890(object p0, object p1, bool[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4891(object p0, object p1, LogicBlockGuildClient.MemberTitleArrowState p2, GuildMemberJobCtrlUITask.TabType p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4892(object p0, bool p1, GuildJobType p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4893(object p0, object p1, object p2, GuildMemberJobCtrlUITask.TabType p3, object p4, GuildJobType p5, LogicBlockGuildClient.MemberTitleArrowState p6, object p7, bool p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4894(object p0, GuildJobType p1, LogicBlockGuildClient.MemberTitleArrowState p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberJobCtrlUITask.JobInfo __Gen_Delegate_Imp4895(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4896(object p0, int p1, GuildJobType p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberJobCtrlUITask __Gen_Delegate_Imp4897(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4898(object p0, GuildJobType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4899(object p0, GuildMemberJobCtrlUITask.TabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp49(object p0, object p1, object p2, out CriAtomEx.CueInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditModifyRelativeInfo __Gen_Delegate_Imp490(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4900(object p0, object p1, bool p2, bool p3, object p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public GuildLostReportUITask __Gen_Delegate_Imp4901(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildLostReportUITask __Gen_Delegate_Imp4902(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4903(object p0, GuildProductionSpeedUpLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildProduceSpeedUpUITask __Gen_Delegate_Imp4904(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4905(object p0, GuildProductionSpeedUpLevel p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4906(StoreItemType p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4907(object p0, int p1, int p2, int p3, Vector3 p4, long p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4908(object p0, int p1, int p2, int p3, Vector3 p4, long p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4909(object p0, object p1, int p2, Vector3 p3, long p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFactionCreditModifyRelativeInfo> __Gen_Delegate_Imp491(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildProductionLineInfo> __Gen_Delegate_Imp4910()
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> __Gen_Delegate_Imp4911(object p0, int p1, int p2, out ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo __Gen_Delegate_Imp4912(object p0, bool p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4913(object p0, LostReportType p1, ulong p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4914(object p0, GuildBattleType p1, GuildBuildingType p2)
        {
        }

        [MethodImpl(0x8000)]
        public WarGuildKillLostUITask __Gen_Delegate_Imp4915(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4916(object p0, GuildBattleStatus p1, GuildBattleStatus p2, DateTime p3, DateTime p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4917(object p0, GuildBattleStatus p1)
        {
        }

        [MethodImpl(0x8000)]
        public WarOverviewUITask __Gen_Delegate_Imp4918(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public WarPersonalKillRankingListUITask __Gen_Delegate_Imp4919(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditNpcInfo __Gen_Delegate_Imp492(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4920(object p0, GuildBattleType p1, GuildBattleStatus p2, DateTime p3, DateTime p4, GuildBuildingType p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4921(object p0, WarReportManagerUITask.TabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4922(object p0, object p1, Vector3 p2, object p3, ItemSimpleInfoUITask.PositionType p4)
        {
        }

        [MethodImpl(0x8000)]
        public WarShipLossUITask __Gen_Delegate_Imp4923(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4924(object p0, object p1, bool p2, Vector3 p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4925(object p0, object p1, int p2, object p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4926(object p0, KeyValuePair<int, ConfigDataAuctionItemInfo> p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataAuctionItemInfo __Gen_Delegate_Imp4927(KeyValuePair<int, ConfigDataAuctionItemInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4928(object p0, bool p1, NpcShopItemCategory p2, object p3, object p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4929(object p0, long p1, long p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFactionCreditNpcInfo> __Gen_Delegate_Imp493(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4930(object p0, int p1, long p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4931(KeyValuePair<int, ConfigDataAuctionItemInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4932(int p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4933(ulong p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4934(ulong[] p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4935(object p0, ulong[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4936(ulong p0, int p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4937(object p0, ItemTypeId p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4938(object p0, object p1, object p2, object p3, Vector3 p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4939(object p0, StoreItemType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditQuestWeeklyRewardInfo __Gen_Delegate_Imp494(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4940(object p0, ulong p1, ulong p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4941(object p0, int p1, ulong p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4942(object p0, GuildItemListUIController.GuildStoreTabType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildItemCategoryMenuItemUIController __Gen_Delegate_Imp4943(object p0, GuildItemListUIController.GuildStoreTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4944(object p0, GuildItemListUIController.GuildStoreTabType p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4945(object p0, int p1, BlackJack.ConfigData.InputType p2, InputStringCheckFlag p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4946(object p0, GuildWalletLogUITask.LogTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4947(object p0, GuildCurrencyLogFilterType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4948(object p0, object p1, GuildCurrencyLogFilterType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4949(object p0, object p1, object p2, object p3, bool p4, int p5, ulong p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFactionCreditQuestWeeklyRewardInfo> __Gen_Delegate_Imp495(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp4950(object p0, SDKHelper.SharePlatform p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4951(object p0, RecordingStatus p1)
        {
        }

        [MethodImpl(0x8000)]
        public Bounds __Gen_Delegate_Imp4952(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4953(object p0, CommonItemDetailInfoUIController.DetailInfoTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ItemDetailInfoUITask __Gen_Delegate_Imp4954(object p0, object p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public ItemDetailInfoUITask __Gen_Delegate_Imp4955(object p0, object p1, object p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ItemObtainSourceListItemUIController __Gen_Delegate_Imp4956(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4957(object p0, ItemSimpleInfoUIController.ItemInfoUIMode p1, ItemSimpleInfoUIController.ItemInfoButtonPosition p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4958(object p0, ItemSimpleInfoUIController.ItemInfoUIMode p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4959(object p0, ItemSimpleInfoUIController.ItemInfoButtonPosition p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditQuestWeeklyRewardLevelInfo __Gen_Delegate_Imp496(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4960(object p0, object p1, object p2, bool p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4961(object p0, object p1, StoreItemType p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4962(object p0, long p1, long p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public ItemSimpleInfoUITask __Gen_Delegate_Imp4963(object p0, object p1, bool p2, Vector3 p3, object p4, ItemSimpleInfoUITask.PositionType p5, bool p6, object p7, object p8, bool p9, bool p10, bool p11)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4964(object p0, NpcShopFlag p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4965(object p0, ItemStoreListUIController.ItemStoreTabType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp4966(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp4967(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4968(object p0, ItemStoreListUIController.ItemStoreTabType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4969(object p0, NormalItemType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFactionCreditQuestWeeklyRewardLevelInfo> __Gen_Delegate_Imp497(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ItemStoreListUIController.ItemStoreTabType __Gen_Delegate_Imp4970(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4971(BlackMarketShopPayType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp4972(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp4973(StoreItemType p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp4974(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfIdLevelInfo> __Gen_Delegate_Imp4975(StoreItemType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4976(StoreItemType p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4977(object p0, object p1, GrandFaction p2, ProfessionType p3, int p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, List<int>> __Gen_Delegate_Imp4978()
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4979(GrandFaction p0, ProfessionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionInfo __Gen_Delegate_Imp498(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp4980(object p0, GrandFaction p1, GenderType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4981(object p0, object p1, GrandFaction p2, ProfessionType p3, int p4, GenderType p5)
        {
        }

        [MethodImpl(0x8000)]
        public LoginBGTask __Gen_Delegate_Imp4982(object p0, GrandFaction p1)
        {
        }

        [MethodImpl(0x8000)]
        public LoginUIController.LoginAnnouncement.AnnounceType __Gen_Delegate_Imp4983(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4984(object p0, LoginUIController.LoginAnnouncement.AnnounceType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4985(object p0, LoginUIController.LoginAnnouncement.AnnounceType p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public StringTableId __Gen_Delegate_Imp4986(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4987(object p0, object p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp4988(object p0, object p1, ref string p2, ref string p3, ref string p4, ref string p5, ref string p6)
        {
        }

        [MethodImpl(0x8000)]
        public ServerInfo __Gen_Delegate_Imp4989(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFactionInfo> __Gen_Delegate_Imp499(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ServerInfo __Gen_Delegate_Imp4990(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LoginUITask.SessionTokenCacheItem __Gen_Delegate_Imp4991(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public LoginUITask.SessionTokenCacheDataContainer __Gen_Delegate_Imp4992(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public WorldEnterReqNetTask.WorldEnterReqResultState __Gen_Delegate_Imp4993(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4994(object p0, WorldEnterReqNetTask.WorldEnterReqResultState p1)
        {
        }

        [MethodImpl(0x8000)]
        public LoginUITask.SessionTokenCacheItem __Gen_Delegate_Imp4995(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LoginTestSdk __Gen_Delegate_Imp4996()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4997(object p0, int p1, int p2, int p3, int p4, int p5, int p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4998(object p0, int p1, ProfessionType p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp4999(object p0, MailUITask.ModeCategory p1, object p2, bool p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp50(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFlagShipEquipRelativeInfo __Gen_Delegate_Imp500(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5000(object p0, MailUITask.ModeCategory p1)
        {
        }

        [MethodImpl(0x8000)]
        public MailUIController.MailListButtonState __Gen_Delegate_Imp5001(object p0, object p1, MailUITask.ModeCategory p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5002(object p0, MailUIController.MailListButtonState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5003(object p0, MailUITask.Mode p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5004(object p0, MailUITask.Mode p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockMailClient __Gen_Delegate_Imp5005(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MailUITask.MailComparer __Gen_Delegate_Imp5006()
        {
        }

        [MethodImpl(0x8000)]
        public MenuPanelInSpaceStationUITask __Gen_Delegate_Imp5007(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5008(object p0, MenuPanelUIController.MenuPanelUIMode p1)
        {
        }

        [MethodImpl(0x8000)]
        public MenuPanelUITask __Gen_Delegate_Imp5009(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFlagShipEquipRelativeInfo> __Gen_Delegate_Imp501(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5010(object p0, MenuPanelUITask.PipeLineStateMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5011(object p0, MenuPanelUITask.PipeLineStateMaskType p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonCaptainIconUIController __Gen_Delegate_Imp5012(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController __Gen_Delegate_Imp5013(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public CommonItemIconUIController __Gen_Delegate_Imp5014(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5015(object p0, object p1, DelegateMissionState p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5016(object p0, object p1, out string p2, out long p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5017(SignalType p0)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateQuestSimpleInfoListUITask __Gen_Delegate_Imp5018(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5019(object p0, int p1, int p2, bool p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFlagShipRelativeData __Gen_Delegate_Imp502(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public TimeSpan __Gen_Delegate_Imp5020(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5021(object p0, int p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public Turple<Text, Text, Image> __Gen_Delegate_Imp5022(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public MotherShipUITask __Gen_Delegate_Imp5023(bool p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5024(bool p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5025(int p0, out bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcEmotionType __Gen_Delegate_Imp5026(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo __Gen_Delegate_Imp5027(object p0, out bool p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo __Gen_Delegate_Imp5028(int p0, out bool p1, object p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5029(int p0, int p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFlagShipRelativeData> __Gen_Delegate_Imp503(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5030(int p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5031(object p0, NpcEmotionType p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5032(int p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5033(int p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5034(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5035(object p0, object p1, object p2, object p3, bool p4, object p5, bool p6, bool p7, bool p8, bool p9, object p10, float p11)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5036(object p0, object p1, object p2, object p3, bool p4, bool p5, object p6, float p7, bool p8, bool p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5037(object p0, CurrencyType p1, ulong p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5038(object p0, long p1, long p2, long p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5039(object p0, out NpcShopMainUITask.NpcShopItemUIInfo p1, out long p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFlagShipWeaponRelativeInfo __Gen_Delegate_Imp504(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockNpcShopClient __Gen_Delegate_Imp5040(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5041(object p0, NpcShopItemCategory p1, NpcShopItemType p2)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopItemTypeToggleUIController __Gen_Delegate_Imp5042(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5043(object p0, Vector3 p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5044(object p0, StoreItemType p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5045(object p0, object p1, NpcShopItemCategory p2, NpcShopItemType p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5046(NpcShopFlag p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5047(bool p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp5048(object p0, object p1, NpcShopItemCategory p2, NpcShopItemType p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp5049(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFlagShipWeaponRelativeInfo> __Gen_Delegate_Imp505(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp5050(object p0, bool p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5051(out GDBSpaceStationNpcTalkerInfo p0, int p1, bool p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<NpcShopItemCategory, List<int>> __Gen_Delegate_Imp5052(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public long __Gen_Delegate_Imp5053(object p0, StoreItemType p1, int p2, bool p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5054(object p0, NpcShopMainUITask.NpcShopSortType p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopMainUITask.NpcShopItemFilter __Gen_Delegate_Imp5055(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5056(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopMainUITask.NpcShopItemUIInfo __Gen_Delegate_Imp5057(object p0, StoreItemType p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5058(object p0, StoreItemType p1, int p2, object p3, object p4, out bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5059(object p0, StoreItemType p1, object p2, int p3, out float p4, bool p5, NpcShopItemType p6)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFleetFormationInfo __Gen_Delegate_Imp506(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopMainUITask.NpcShopItemUIInfo __Gen_Delegate_Imp5060(object p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public NpcShopMainUITask.NpcShopItemUIInfo __Gen_Delegate_Imp5061(object p0, int p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5062(NpcShopMainUITask.NpcShopItemFilter p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5063(object p0, long p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5064(object p0, bool[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5065(object p0, NpcShopMainUITask.NpcShopItemFilter p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5066(object p0, bool p1, NpcShopMainUITask.NpcShopSortType p2, bool[] p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5067(object p0, object p1, NpcDialogOptType p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public NpcListItemUIController __Gen_Delegate_Imp5068(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5069(SpaceStationRoomType p0, object p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFleetFormationInfo> __Gen_Delegate_Imp507(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess __Gen_Delegate_Imp5070(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcTalkerBase __Gen_Delegate_Imp5071(object p0, out int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5072(object p0, bool? p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5073(object p0, object p1, object p2, object p3, object p4, int p5, object p6, bool p7, bool p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5074(object p0, object p1, object p2, object p3, object p4, bool p5, bool p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public SimpleNpcDialogUITask __Gen_Delegate_Imp5075(int p0, int p1, object p2, int p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5076(NpcFunctionType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5077(NpcDialogOptType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5078(ProfessionType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5079(GenderType p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFleetPositionInfo __Gen_Delegate_Imp508(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5080(object p0, bool p1, DateTime p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5081(object p0, bool p1, int p2, int p3, int p4, int[] p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5082(object p0, out DateTime p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5083(object p0, object p1, bool p2, bool p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5084(object p0, bool p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDailyLoginInfo __Gen_Delegate_Imp5085(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerInfoInitReqNetTask.InitReqResultState __Gen_Delegate_Imp5086(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5087(object p0, PlayerInfoInitReqNetTask.InitReqResultState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5088(object p0, ProduceLineState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5089(object p0, object p1, float p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFleetPositionInfo> __Gen_Delegate_Imp509(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5090(object p0, object p1, bool p2, object p3, int p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5091(object p0, int p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5092(object p0, int p1, out bool p2, out bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5093(object p0, BlueprintCategory p1, BlueprintType p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5094(object p0, object p1, object p2, int p3, long p4)
        {
        }

        [MethodImpl(0x8000)]
        public ProduceItemStoreUITask __Gen_Delegate_Imp5095(object p0, object p1, int p2, bool p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5096(object p0, object p1, int p2, bool p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, long> __Gen_Delegate_Imp5097(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5098(object p0, object p1, Vector3 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProduceBGUIBackgroupManager __Gen_Delegate_Imp5099(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClassLoader __Gen_Delegate_Imp51()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFreeSignalLevelLimit __Gen_Delegate_Imp510(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ProduceItemStoreUITask __Gen_Delegate_Imp5100(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5101(object p0, ProduceLineState p1, object p2, int p3, object p4, object p5, int p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5102(object p0, int p1, int p2, ProduceLineState p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5103(object p0, object p1, object p2, object p3, object p4, int p5)
        {
        }

        [MethodImpl(0x8000)]
        public Sprite __Gen_Delegate_Imp5104(object p0, StoreItemType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5105(object p0, long p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBProductionLine __Gen_Delegate_Imp5106(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5107(object p0, ProduceLineState p1, object p2, int p3, bool p4, bool p5, int p6, object p7, object p8, int p9, int p10)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5108(object p0, ProduceLineState p1, object p2, int p3, int p4, object p5, object p6, int p7, int p8, ulong p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5109(object p0, ProduceLineState p1, int p2, int p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataFreeSignalLevelLimit> __Gen_Delegate_Imp511(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5110(object p0, int p1, int p2, int p3, int p4, bool p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5111(object p0, int p1, int p2, int p3, int p4, ulong p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5112(object p0, int p1, int p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5113(object p0, object p1, int p2, ulong p3, bool p4, int p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5114(object p0, ProduceLineState p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5115(object p0, ProduceLineState p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProduceThreeDBGUITask __Gen_Delegate_Imp5116(ProduceLineState p0, int p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5117(object p0, ProduceLineState p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ProduceThreeDBGUIController __Gen_Delegate_Imp5118(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIIntentCustom __Gen_Delegate_Imp5119(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEBrushInfo __Gen_Delegate_Imp512(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> __Gen_Delegate_Imp5120(object p0, int p1, ulong p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5121(object p0, int p1, ulong p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ProduceUITask __Gen_Delegate_Imp5122(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5123(object p0, int p1, int p2, int p3, ulong p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5124(object p0, object p1, ref string p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5125(object p0, bool p1, object p2, Vector3 p3, Vector3 p4)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5126(object p0, Vector3 p1)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject[] __Gen_Delegate_Imp5127(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<Animator>[] __Gen_Delegate_Imp5128(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LineRenderer>[] __Gen_Delegate_Imp5129(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEBrushInfo> __Gen_Delegate_Imp513(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<float>[] __Gen_Delegate_Imp5130(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp5131(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5132(object p0, Vector2 p1, Vector2 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5133(object p0, Vector2 p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform[] __Gen_Delegate_Imp5134(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5135(object p0, RankingListType p1, object p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5136(object p0, RankingListType p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5137(object p0, object p1, int p2, object p3, object p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5138(object p0, RankingListType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5139(object p0, object p1, RankingListType p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEConfigableConst __Gen_Delegate_Imp514(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockRankingListClient __Gen_Delegate_Imp5140(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5141(object p0, RankingListType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5142(object p0, object p1, object p2, long p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5143(object p0, object p1, object p2, int p3, long p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5144(object p0, object p1, int p2, int p3, long p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5145(object p0, bool p1, bool p2, bool p3, object p4, object p5, UIProcess.ProcessExecMode p6)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> __Gen_Delegate_Imp5146(object p0, out bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5147(object p0, float p1, float p2, float p3, object p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5148(object p0, object p1, bool p2, object p3, out bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public HangarShipListItemUIController.HangarShipListItemState __Gen_Delegate_Imp5149(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEConfigableConst> __Gen_Delegate_Imp515(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5150(object p0, object p1, object p2, object p3, RankType p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public HangarShipListItemUIController __Gen_Delegate_Imp5151(object p0, int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5152(object p0, object p1, HangarShipSalvageUIController.UIMode p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5153(object p0, object p1, HangarShipSalvageUIController.UIMode p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5154(object p0, HangarShipUniqueWeaponEquipTipWndUIController.UniqueWeaponEquipTipState p1, object p2, object p3, out bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5155(object p0, object p1, StoreItemType p2, object p3, bool p4, bool p5, object p6, bool p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5156(object p0, bool p1, bool p2, bool p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public NewShipGetUITask __Gen_Delegate_Imp5157(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NewShipGetUITask __Gen_Delegate_Imp5158(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public ShipHangarAssignUITask __Gen_Delegate_Imp5159(object p0, int p1, bool p2, ShipType p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEFaction_StarGateTemplateMapInfo __Gen_Delegate_Imp516(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase __Gen_Delegate_Imp5160(object p0, ulong p1, int p2, bool p3, RankType p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5161(object p0, object p1, ItemStoreListUIController.ItemStoreTabType p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticPlayerShipClient __Gen_Delegate_Imp5162(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5163(object p0, object p1, int p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public ShipHangarItemStoreUITaskPipeLineCtx __Gen_Delegate_Imp5164(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStoreItemClient __Gen_Delegate_Imp5165(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5166(object p0, ShipHangarUIController.UIMode p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ShipHangarUIController.UIMode __Gen_Delegate_Imp5167(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5168(object p0, bool p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5169(object p0, object p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEFaction_StarGateTemplateMapInfo> __Gen_Delegate_Imp517(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5170(object p0, out List<int> p1, int p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5171(object p0, int p1, PropertiesId p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5172(object p0, AuctionBuyItemFilterType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.ProjectX.Runtime.UI.ShipHangarUITaskPipeLineCtx __Gen_Delegate_Imp5173(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5174(object p0, out RectTransform p1, out Camera p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5175(object p0, ShipEquipSlotType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5176(object p0, ShipModelOverviewUIController.ShipModelOverviewMode p1, object p2, object p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp5177(object p0, ShipModelOverviewUIController.ShipModelOverviewMode p1, object p2, object p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5178(object p0, ShipModelOverviewUIController.ShipModelOverviewMode p1, bool p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<SlotGroupInfoUIController> __Gen_Delegate_Imp5179(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEFaction_StationTemplateMapInfo __Gen_Delegate_Imp518(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5180(object p0, object p1, object p2, object p3, int p4, int p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public ShipInfoItemUIController __Gen_Delegate_Imp5181(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5182(object p0, object p1, object p2, object p3, bool p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5183(object p0, int p1, long p2, long p3)
        {
        }

        [MethodImpl(0x8000)]
        public ShipStrikeUITask.CachedHireCaptain __Gen_Delegate_Imp5184(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5185(object p0, ulong p1, int p2, int p3, int p4, uint p5, ulong p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5186(object p0, ulong p1, ulong p2, int p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5187(object p0, ulong p1, int p2, ulong p3)
        {
        }

        [MethodImpl(0x8000)]
        public ShipStrikeUITask.CachedHireCaptain __Gen_Delegate_Imp5188(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5189(object p0, int p1, long p2, long p3, ulong p4, ulong p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEFaction_StationTemplateMapInfo> __Gen_Delegate_Imp519(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5190(object p0, long p1, out long p2, out long p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5191(object p0, StoreItemType p1, int p2, int p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp5192(object p0, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp5193(object p0, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5194(object p0, int p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<ButtonEx> __Gen_Delegate_Imp5195(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<SpaceStationUI3DSceneObjectDesc> __Gen_Delegate_Imp5196(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationUI3DSceneObjectDesc __Gen_Delegate_Imp5197(object p0, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5198(object p0, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationUICameraOperationController __Gen_Delegate_Imp5199(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public object __Gen_Delegate_Imp52(object p0, object p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEGrayColorToSolarSystemTemplateMapInfo __Gen_Delegate_Imp520(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp5200(object p0, object p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5201(object p0, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5202(object p0, ChatChannel p1, object p2, ChatLanguageChannel p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5203(object p0, ulong p1, int p2, int p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5204(object p0, StarMapCelestialBodyInfoUIController.CelestialInfoType p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapCommonData __Gen_Delegate_Imp5205()
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo[] __Gen_Delegate_Imp5206()
        {
        }

        [MethodImpl(0x8000)]
        public Rect __Gen_Delegate_Imp5207()
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ForcePosition> __Gen_Delegate_Imp5208()
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5209(float p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEGrayColorToSolarSystemTemplateMapInfo> __Gen_Delegate_Imp521(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ulong> __Gen_Delegate_Imp5210()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5211(object p0, int p1, object p2, float p3, float p4, float p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapForGalaxyUITask __Gen_Delegate_Imp5212(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5213(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5214(int p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5215(object p0, GuildSovereignBattleStartUIController.ButtonType p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5216(object p0, bool p1, object p2, object p3, long p4, ulong p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5217(object p0, GuildSovereignBattleStartUIController.ButtonType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5218(object p0, object p1, object p2, object p3, object p4, GuildSovereignBattleStartUIController.ButtonType p5)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSovereignBattleStartUIController.ButtonType __Gen_Delegate_Imp5219(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEMoonCountDataInfo __Gen_Delegate_Imp522(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildBenefitsConfigInfo __Gen_Delegate_Imp5220(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildBenefitsConfigInfo __Gen_Delegate_Imp5221(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataGuildBenefitsConfigInfo> __Gen_Delegate_Imp5222(object p0, GuildBenefitsType p1, GuildBenefitsSubType p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingItemUIController __Gen_Delegate_Imp5223(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5224(object p0, long p1, ulong p2, object p3, object p4, object p5, GuildBuildingType p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5225(object p0, bool p1, float p2, float p3, float p4, float p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5226(int p0, bool p1, float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5227(object p0, bool p1, GuildBuildingType p2)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5228(object p0, int p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<ulong, Vector2> __Gen_Delegate_Imp5229(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEMoonCountDataInfo> __Gen_Delegate_Imp523(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5230(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess.UIProcessExecutor __Gen_Delegate_Imp5231(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess.UIProcessExecutor __Gen_Delegate_Imp5232(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5233(object p0, int p1, int p2, int p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5234(object p0, int p1, ulong p2, GuildBattleType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5235(object p0, int p1, object p2, bool p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5236(object p0, float p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp5237(object p0, Vector2 p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5238(object p0, Vector2 p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapOrientedTargetSimpleInfoUIController __Gen_Delegate_Imp5239(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEMoonResInfo __Gen_Delegate_Imp524(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapItemLocationUIController __Gen_Delegate_Imp5240(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5241(object p0, int p1, object p2, float p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5242(object p0, int p1, bool p2, Vector3D p3, ref bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5243(object p0, GlobalSceneInfo p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockSignalClient __Gen_Delegate_Imp5244(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp5245(object p0, QuestType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5246(object p0, SignalType p1)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase __Gen_Delegate_Imp5247(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5248(object p0, object p1, QuestType p2)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp5249(object p0, SignalType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEMoonResInfo> __Gen_Delegate_Imp525(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5250(object p0, SignalType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5251(KeyValuePair<int, ConfigDataNormalItemInfo> p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5252(object p0, float p1, DateTime p2)
        {
        }

        [MethodImpl(0x8000)]
        public QuestMenuItemUIController __Gen_Delegate_Imp5253(object p0, QuestType p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public QuestSignalMenuItemUIController __Gen_Delegate_Imp5254(object p0, QuestType p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateSignalMenuItemUIController __Gen_Delegate_Imp5255(object p0, SignalType p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public PvpSignalMenuItemUIController __Gen_Delegate_Imp5256(object p0, out int p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSentrySignalMenuItemUIController __Gen_Delegate_Imp5257(object p0, ulong p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public QuestSignalMenuItemUIController __Gen_Delegate_Imp5258(object p0, out int p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSignalMenuItemUIController __Gen_Delegate_Imp5259(object p0, out int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGenderFeatsPoolInfo __Gen_Delegate_Imp526(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase __Gen_Delegate_Imp5260(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase __Gen_Delegate_Imp5261(object p0, MenuItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase __Gen_Delegate_Imp5262(object p0, ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5263(object p0, DateTime p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5264(object p0, int p1, object p2, object p3, object p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5265(object p0, int p1, object p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5266(object p0, float p1, DateTime p2, object p3, bool p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemUIControllerBase __Gen_Delegate_Imp5267(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationMenuItemUIController __Gen_Delegate_Imp5268(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ButtonEx[] __Gen_Delegate_Imp5269(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGenderFeatsPoolInfo> __Gen_Delegate_Imp527(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MenuItemType __Gen_Delegate_Imp5270(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSentryInterestScene __Gen_Delegate_Imp5271(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5272(object p0, float p1, DateTime p2, object p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public Sprite __Gen_Delegate_Imp5273(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPSignal __Gen_Delegate_Imp5274(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> __Gen_Delegate_Imp5275(object p0, object p1, SignalType p2)
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPInvadeRescue __Gen_Delegate_Imp5276(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholeStarGroupInfo __Gen_Delegate_Imp5277(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestMenuItemUIController __Gen_Delegate_Imp5278(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public QuestSignalMenuItemUIController __Gen_Delegate_Imp5279(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEOtherResPathDataInfo __Gen_Delegate_Imp528(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public DelegateSignalMenuItemUIController __Gen_Delegate_Imp5280(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PvpSignalMenuItemUIController __Gen_Delegate_Imp5281(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PvpSignalMenuItemRootUIController __Gen_Delegate_Imp5282(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSignalMenuItemUIController __Gen_Delegate_Imp5283(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CelestialPlanetMenuItemUIController __Gen_Delegate_Imp5284(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CelestialStarMenuItemUIController __Gen_Delegate_Imp5285(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StargateMenuItemUIController __Gen_Delegate_Imp5286(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public WormholeMenuItemUIController __Gen_Delegate_Imp5287(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UnknownWormholeMenuItemUIController __Gen_Delegate_Imp5288(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public InfectMenuItemUIController __Gen_Delegate_Imp5289(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEOtherResPathDataInfo> __Gen_Delegate_Imp529(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RescueMenuItemUIController __Gen_Delegate_Imp5290(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSentrySignalMenuItemUIController __Gen_Delegate_Imp5291(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5292(object p0, object p1, object p2, object p3, int p4, object p5, int p6)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5293(object p0, bool p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5294(object p0, object p1, float p2, Vector2 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5295(object p0, float p1, Vector2 p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5296(object p0, Vector3 p1, bool p2, GEPlanetType p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5297(object p0, float p1, Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5298(object p0, bool p1, GEPlanetType p2, out float p3, out float p4, out float p5)
        {
        }

        [MethodImpl(0x8000)]
        public List<float> __Gen_Delegate_Imp5299(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public System.Type __Gen_Delegate_Imp53(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEPlanetDataInfo __Gen_Delegate_Imp530(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<float> __Gen_Delegate_Imp5300(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5301(object p0, GEPlanetType p1)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp5302(object p0, GuildBuildingType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject __Gen_Delegate_Imp5303(object p0, GuildBuildingType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapForStarfieldSSPointUIController __Gen_Delegate_Imp5304(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5305(object p0, int p1, int p2, GEBrushType p3, object p4, object p5, float p6, float p7, float p8, object p9)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5306(object p0, Vector2 p1, float p2, object p3, int p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5307(object p0, GEBrushType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5308(object p0, int p1, ulong p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5309(object p0, ref Vector2 p1, ref Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEPlanetDataInfo> __Gen_Delegate_Imp531(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess.UIProcessExecutor __Gen_Delegate_Imp5310(object p0, Vector2 p1, float p2, Vector2 p3, float p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5311(object p0, Vector2 p1, float p2, Vector2 p3, float p4, object p5, bool p6)
        {
        }

        [MethodImpl(0x8000)]
        public List<Vector2> __Gen_Delegate_Imp5312(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<Color32> __Gen_Delegate_Imp5313(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<Color, Color> __Gen_Delegate_Imp5314(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5315(object p0, ref Vector2 p1, ref Vector2 p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5316(object p0, ulong p1, float p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5317(object p0, int p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5318(int p0, int p1, int p2, GEBrushType p3, int p4, float p5, bool p6, bool p7, object p8, int p9, ulong p10, object p11, object p12)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5319(int p0, ulong p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEResPathDataInfo __Gen_Delegate_Imp532(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5320(object p0, ulong p1, ulong p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public GEBrushType __Gen_Delegate_Imp5321(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5322(object p0, object p1, int p2, object p3, float p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo __Gen_Delegate_Imp5323(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo __Gen_Delegate_Imp5324(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp5325(object p0, GEBrushType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5326(object p0, object p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess.UIProcessExecutor __Gen_Delegate_Imp5327(object p0, object p1, int p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp5328(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5329(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEResPathDataInfo> __Gen_Delegate_Imp533(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp5330(object p0, GEBrushType p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5331(KeyValuePair<ulong, int> p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5332(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5333(ref int p0, ref int p1, ref int p2, ref int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5334(int p0, int p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5335(int p0, int p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5336(int p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5337(int p0, int p1, int p2, ref byte[] p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5338(object p0, GuildViewHelper.MapType p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5339(int p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGERockyPlanetTemperatureInfo __Gen_Delegate_Imp534(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5340(Rect p0)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp5341(int p0, GuildViewHelper.MapType p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5342(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp5343(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp5344(int p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Rect __Gen_Delegate_Imp5345(int p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp5346(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public byte __Gen_Delegate_Imp5347(byte p0, byte p1)
        {
        }

        [MethodImpl(0x8000)]
        public byte __Gen_Delegate_Imp5348(float p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildViewHelper.StarArea> __Gen_Delegate_Imp5349(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGERockyPlanetTemperatureInfo> __Gen_Delegate_Imp535(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5350(int p0, object p1, object p2, float p3)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 __Gen_Delegate_Imp5351()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5352(Vector2 p0)
        {
        }

        [MethodImpl(0x8000)]
        public TextureFormat __Gen_Delegate_Imp5353()
        {
        }

        [MethodImpl(0x8000)]
        public ushort __Gen_Delegate_Imp5354(KeyValuePair<int, ForcePosition> p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5355(KeyValuePair<int, ForcePosition> p0)
        {
        }

        [MethodImpl(0x8000)]
        public ByteArrayFactory __Gen_Delegate_Imp5356()
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5357(object p0, GEBrushType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<string, string> __Gen_Delegate_Imp5358(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5359(object p0, float p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGESolarSystemBGMusicInfo __Gen_Delegate_Imp536(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ViewBlock __Gen_Delegate_Imp5360(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ViewBlock __Gen_Delegate_Imp5361(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RawImage __Gen_Delegate_Imp5362(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Texture2D __Gen_Delegate_Imp5363(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5364(object p0, Color32 p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5365(object p0, int p1, float p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5366(object p0, int p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5367(object p0, bool p1, bool p2, int p3, uint p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5368(object p0, int p1, uint p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<int> __Gen_Delegate_Imp5369(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGESolarSystemBGMusicInfo> __Gen_Delegate_Imp537(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5370(object p0, bool p1, bool p2, object p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public ushort __Gen_Delegate_Imp5371(uint p0, ushort p1, int p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5372(int p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5373(object p0, int p1, object p2, object p3, object p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5374(object p0, int p1, int p2, int p3, GEBrushType p4, object p5, int p6, bool p7, object p8, bool p9, int p10, ulong p11, object p12)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5375(object p0, int p1, ulong p2, object p3, bool p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5376(object p0, int p1, bool p2, StarMapForGuildUITask.SubTaskPanel p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapUIBackgroundManager __Gen_Delegate_Imp5377(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5378(object p0, NavigationNodeType p1, int p2, int p3, uint p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5379(object p0, object p1, int p2, object p3, object p4, float p5, bool p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGESolarSystemNameInfo __Gen_Delegate_Imp538(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5380(SpaceObjectType p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp5381(SpaceObjectType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Color __Gen_Delegate_Imp5382(GEBrushType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5383(SolarSystemGuildBattleStatus p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildSentryTargetTrackingLevelInfo __Gen_Delegate_Imp5384(float p0)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipFightBuf __Gen_Delegate_Imp5385(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public EquipEffectVirtualBufInfo __Gen_Delegate_Imp5386(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5387(object p0, object p1, DateTime? p2, bool p3, DateTime? p4, DateTime? p5, bool p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public TeamMainUIController.TeamMemberUIItemInfo __Gen_Delegate_Imp5388(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TeamMainUIController.TeamMemberUIItemInfo __Gen_Delegate_Imp5389(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGESolarSystemNameInfo> __Gen_Delegate_Imp539(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5390(object p0, object p1, object p2, object p3, object p4, object p5, RankType p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5391(object p0, object p1, int p2, object p3, object p4, bool p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockTechClient __Gen_Delegate_Imp5392(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5393(int p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeCancelUITask __Gen_Delegate_Imp5394(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5395(object p0, object p1, int p2, object p3, object p4, object p5, int p6, int p7)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeChooseCaptainUITask __Gen_Delegate_Imp5396(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeChooseCaptainUITask __Gen_Delegate_Imp5397(int p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ulong __Gen_Delegate_Imp5398(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechLevelInfo __Gen_Delegate_Imp5399(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp54(object p0, object p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGESolarSystemStarGateCountWeight __Gen_Delegate_Imp540(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Turple<float, float, float> __Gen_Delegate_Imp5400(object p0, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public Turple<float, float, float> __Gen_Delegate_Imp5401(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5402(object p0, int p1, ulong p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5403(object p0, int p1, ulong p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<CommonPropertyInfo> __Gen_Delegate_Imp5404(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public TechCostItemUIController __Gen_Delegate_Imp5405(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonCaptainIconUIController __Gen_Delegate_Imp5406(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeInfoUITask __Gen_Delegate_Imp5407(object p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5408(object p0, TechType p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public TechCategoryToggleUIController __Gen_Delegate_Imp5409(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGESolarSystemStarGateCountWeight> __Gen_Delegate_Imp541(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp5410(object p0, TechType p1)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform __Gen_Delegate_Imp5411(object p0, TechType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeOverviewUIController __Gen_Delegate_Imp5412(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeTreeUIController __Gen_Delegate_Imp5413(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeManageUITask __Gen_Delegate_Imp5414(object p0, int p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5415(int p0, out int p1, out int p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataTechInfo> __Gen_Delegate_Imp5416(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5417(object p0, StoreItemType p1, int p2, Vector3 p3, Vector2 p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5418(object p0, TechUpgradeManageUITask.SimplePanelState p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5419(object p0, int p1, object p2, bool p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGESolarSystemTemplateInfo __Gen_Delegate_Imp542(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<CommonItemIconUIController> __Gen_Delegate_Imp5420(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeSpeedUpUITask __Gen_Delegate_Imp5421(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeSpeedUpWithRealMoneyUITask __Gen_Delegate_Imp5422(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5423(object p0, TechUpgradeTreeNodeUIController.TechTreeNodeState p1)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradeOrderEditUITask __Gen_Delegate_Imp5424(object p0, object p1, int p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradeOrderEditUITask __Gen_Delegate_Imp5425(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5426(object p0, long p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5427(object p0, GuildTradeOrderEditUITask.GuildTradeOrderEditResult p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5428(object p0, ulong p1, int p2, ushort p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5429(object p0, int p1, StoreItemType p2, int p3, int p4, long p5)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGESolarSystemTemplateInfo> __Gen_Delegate_Imp543(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5430(object p0, ulong p1, int p2, int p3, long p4, ushort p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5431(object p0, ulong p1, int p2, ushort p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5432(object p0, int p1, StoreItemType p2, int p3, int p4, long p5, int p6, ulong p7)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5433(object p0, int p1, object p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePortListItemUIController __Gen_Delegate_Imp5434(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5435(object p0, int p1, ushort p2, ushort p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataNormalItemInfo> __Gen_Delegate_Imp5436(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5437(object p0, ushort p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public TradeItemMenuItemData __Gen_Delegate_Imp5438(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonMenuItemUIController __Gen_Delegate_Imp5439(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGESpectrumDataInfo __Gen_Delegate_Imp544(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonMenuItemUIController __Gen_Delegate_Imp5440(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public TradeItemTypeMenuItemUIController __Gen_Delegate_Imp5441(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public TradeItemTypeMenuItemUIController __Gen_Delegate_Imp5442(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public TradeItemTypeMenuItemData __Gen_Delegate_Imp5443(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GuildLogoController __Gen_Delegate_Imp5444(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5445(object p0, TradeUITaskTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5446(object p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5447(int p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildTradeItemInfo __Gen_Delegate_Imp5448(StoreItemType p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Func<TradeListItemInfo, TradeListItemInfo, int> __Gen_Delegate_Imp5449(TradeItemSortType p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGESpectrumDataInfo> __Gen_Delegate_Imp545(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Func<TradeListItemSellInfo, TradeListItemSellInfo, int> __Gen_Delegate_Imp5450(TradeItemSortType p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5451(int p0, StoreItemType p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5452(float p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFactionCreditLevelInfo __Gen_Delegate_Imp5453(FactionCreditLevel p0)
        {
        }

        [MethodImpl(0x8000)]
        public TradeItemMenuItemData __Gen_Delegate_Imp5454(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Func<GuildTradePurchaseInfo, GuildTradePurchaseInfo, int> __Gen_Delegate_Imp5455(object p0, TradeItemSortType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public TradeUITask.TradePortJumNumInfo __Gen_Delegate_Imp5456(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5457(object p0, object p1, int p2, out ConfigDataNpcShopItemInfo p3)
        {
        }

        [MethodImpl(0x8000)]
        public List<TradeListItemSellInfo> __Gen_Delegate_Imp5458(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIVertex __Gen_Delegate_Imp5459(object p0, Vector2 p1, Color p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEStarfieldColorBlockTemplateInfo __Gen_Delegate_Imp546(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5460(KeyValuePair<float, Color> p0, KeyValuePair<float, Color> p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5461(object p0, object p1, AnimatorStateInfo p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public float[] __Gen_Delegate_Imp5462(byte[] p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public IAudioCompressor __Gen_Delegate_Imp5463()
        {
        }

        [MethodImpl(0x8000)]
        public IVideoPlayer __Gen_Delegate_Imp5464(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ISceneAnimationPlayerController __Gen_Delegate_Imp5465(object p0, SceneAnimationPlayerType p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5466(object p0, object p1, Vector2[] p2, int[] p3, Color[] p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5467(object p0, GuildItemListUIController.GuildStoreTabType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<Func<ILBStoreItemClient, bool>> __Gen_Delegate_Imp5468(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<string> __Gen_Delegate_Imp5469(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEStarfieldColorBlockTemplateInfo> __Gen_Delegate_Imp547(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<string> __Gen_Delegate_Imp5470(object p0, GuildItemListUIController.GuildStoreTabType p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public GuildItemListUIController.GuildStoreTabType __Gen_Delegate_Imp5471(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ItemStoreListUIController.ItemStoreTabType __Gen_Delegate_Imp5472(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<Func<ILBStoreItemClient, bool>> __Gen_Delegate_Imp5473(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5474(SystemFuncDescType p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5475(Vector2 p0, Vector2 p1, object p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5476(object p0, object p1, out Vector2 p2, out Vector2 p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5477(object p0, object p1, int p2, Vector2 p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5478(object p0, object p1, object p2, out Vector2 p3, out Vector2 p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5479(int p0, bool p1, bool p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEStarGroupNameInfo __Gen_Delegate_Imp548(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideUITask __Gen_Delegate_Imp5480(int p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockUserGuideClient __Gen_Delegate_Imp5481(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideUITask.UserGuidePrepare4StepResultType __Gen_Delegate_Imp5482(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideUITask.UserGuidePrepare4StepResultType __Gen_Delegate_Imp5483(object p0, object p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5484(object p0, object p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5485(int p0, object[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5486(int p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5487(object p0, UserGuideTriggerPoint p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5488(object p0, int p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5489(bool p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEStarGroupNameInfo> __Gen_Delegate_Imp549(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5490(object p0, object p1, int p2, out UserGuideUITask.UserGuidePrepare4StepResultType p3)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideUITask.UserGuidePrepare4StepResultType __Gen_Delegate_Imp5491(object p0, int p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5492(int p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideUITask __Gen_Delegate_Imp5493()
        {
        }

        [MethodImpl(0x8000)]
        public Func<UITaskBase, int, UserGuideUITask.UserGuidePrepare4StepResultType> __Gen_Delegate_Imp5494(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Action<UITaskBase, ConfigDataUserGuideStepInfo, UserGuideStepInfo> __Gen_Delegate_Imp5495(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Action<UITaskBase, UserGuideStepInfo> __Gen_Delegate_Imp5496(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideUITask.UserGuideStepFuncInfo __Gen_Delegate_Imp5497(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideStepInfo __Gen_Delegate_Imp5498(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataUserGuideStepInfo __Gen_Delegate_Imp5499(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp55(object p0, object p1, int p2, int p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEStarResInfo __Gen_Delegate_Imp550(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataUserGuideStepInfo __Gen_Delegate_Imp5500(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataUserGuideGroupInfo __Gen_Delegate_Imp5501(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UserGuideStepInfo __Gen_Delegate_Imp5502(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationUITask __Gen_Delegate_Imp5503(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcTalkerDialogUITask __Gen_Delegate_Imp5504(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public NpcListDialogUITask __Gen_Delegate_Imp5505(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemUITask __Gen_Delegate_Imp5506(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5507(object p0, SolarSystemUITask.TargetListTableState p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5508(object p0, SolarSystemUITask.TargetListTableState p1, SolarSystemUITask.TargetListSortState p2)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5509(object p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEStarResInfo> __Gen_Delegate_Imp551(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess __Gen_Delegate_Imp5510(object p0, uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public UIManager.UIActionQueueItem __Gen_Delegate_Imp5511(object p0, UserGuideTriggerPoint p1, object[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public UserSettingUITask __Gen_Delegate_Imp5512(object p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5513(int p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<PropertyValueStringPair> __Gen_Delegate_Imp5514(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5515(int p0, float p1, out string p2, out string p3)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5516(PropertiesId p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5517(BufType p0, PropertiesId p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public CommonStrikeUtil.StrikeResult __Gen_Delegate_Imp5518(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonStrikeUtil.StrikeResult __Gen_Delegate_Imp5519(ulong p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGEStringTable __Gen_Delegate_Imp552(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public CommonStrikeUtil.StrikeResult __Gen_Delegate_Imp5520(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CommonStrikeUtil.StrikeResult __Gen_Delegate_Imp5521(NavigationNodeType p0, int p1, int p2, uint p3)
        {
        }

        [MethodImpl(0x8000)]
        public CommonStrikeUtil.StrikeResult __Gen_Delegate_Imp5522(object p0, out Vector3D p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5523(object p0, int p1, bool p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5524(object p0, ulong p1, int p2, uint p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5525(object p0, NavigationNodeType p1, int p2, int p3, uint p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5526(object p0, int p1, ulong p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5527(NavigationNodeType p0, int p1, int p2, ulong p3, int p4, uint p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5528(object p0, int p1, bool p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5529(object p0, ulong p1, bool p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGEStringTable> __Gen_Delegate_Imp553(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5530(ulong p0, ulong p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5531(NavigationNodeType p0, int p1, int p2, uint p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5532(object p0, NavigationNodeType p1, int p2, int p3, uint p4, bool p5, object p6, int p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5533(ulong p0, NavigationNodeType p1, int p2, int p3, uint p4, ulong p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5534(object p0, int p1, object p2, bool p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5535(ulong p0, int p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5536(int p0, ulong p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5537(ulong p0, int p1, ulong p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5538(object p0, object p1, bool p2, object p3, object p4, bool p5, int p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> __Gen_Delegate_Imp5539(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGESubSpectrumDataInfo __Gen_Delegate_Imp554(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5540(CurrencyType p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 __Gen_Delegate_Imp5541(float p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public Sprite __Gen_Delegate_Imp5542(object p0, object p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5543(GrandFaction p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5544(DateTime p0, DateTime p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5545(GrandFaction p0, ShipType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5546(ulong p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5547(object p0, UserGuideTriggerPoint p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public UIManager.UIActionQueueItem __Gen_Delegate_Imp5548(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5549(char p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGESubSpectrumDataInfo> __Gen_Delegate_Imp555(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5550(Vector3 p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5551(CommonUIUtil.CostCurrencyType p0)
        {
        }

        [MethodImpl(0x8000)]
        public DeviceRatingLevel __Gen_Delegate_Imp5552()
        {
        }

        [MethodImpl(0x8000)]
        public DeviceRatingLevel __Gen_Delegate_Imp5553(int p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5554(DigitFormatUtil.DigitFormatType p0, ref int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5555(DigitFormatUtil.DigitFormatType p0, ref uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5556(DigitFormatUtil.DigitFormatType p0, ref short p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5557(DigitFormatUtil.DigitFormatType p0, ref ushort p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5558(DigitFormatUtil.DigitFormatType p0, ref long p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5559(DigitFormatUtil.DigitFormatType p0, ref ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGiftCDKeyInfo __Gen_Delegate_Imp556(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5560(DigitFormatUtil.DigitFormatType p0, ref float p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5561(DigitFormatUtil.DigitFormatType p0, ref double p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5562(object p0, ref int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5563(object p0, ref uint p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5564(object p0, ref short p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5565(object p0, ref ushort p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5566(object p0, ref long p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5567(object p0, ref ulong p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5568(object p0, ref float p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5569(object p0, ref double p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGiftCDKeyInfo> __Gen_Delegate_Imp557(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public DigitFormatUtil.DigitFormatType __Gen_Delegate_Imp5570(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5571(object p0, StoreItemType p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5572(object p0, object p1, StoreItemType p2, bool p3, bool p4, long p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5573(object p0, StoreItemType p1, int p2, bool p3, bool p4, long p5)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5574(SystemFuncType p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5575(SystemFuncType p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5576(SystemFuncType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5577(GraphicQualityLevel p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5578(GraphicQualityLevel p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDeviceSetting __Gen_Delegate_Imp5579()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGiftPackageFeatureInfo __Gen_Delegate_Imp558(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public InputStringCheckResult __Gen_Delegate_Imp5580(object p0, int p1, InputStringCheckFlag p2, out string p3, BlackJack.ConfigData.InputType p4, object p5, object p6)
        {
        }

        [MethodImpl(0x8000)]
        public InputStringCheckResult __Gen_Delegate_Imp5581(object p0, int p1, bool p2, out string p3)
        {
        }

        [MethodImpl(0x8000)]
        public InputStringCheckResult __Gen_Delegate_Imp5582(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public InputStringCheckResult __Gen_Delegate_Imp5583(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp5584(char p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5585(LogicalSoundResTableID p0, object p1, float p2)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5586(LogicalSoundResTableID p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5587(LogicalSoundResTableID p0, float p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5588(LogicalSoundResTableID p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5589(float p0, DigitFormatUtil.DigitFormatType p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGiftPackageFeatureInfo> __Gen_Delegate_Imp559(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5590(float p0, int p1, DigitFormatUtil.DigitFormatType p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5591(long p0, DigitFormatUtil.DigitFormatType p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5592(ulong p0, DigitFormatUtil.DigitFormatType p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5593(int p0, int p1, DigitFormatUtil.DigitFormatType p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5594(double p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5595(double p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public double __Gen_Delegate_Imp5596()
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp5597(char p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5598(object p0, int p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5599(int p0, long p1, DigitFormatUtil.DigitFormatType p2)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp56(object p0, int p1, int p2, object p3, int p4, object p5, object p6, object p7)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGiftPackageInfo __Gen_Delegate_Imp560(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5600(StringTableId p0, object[] p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5601(NpcShopItemCategory p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5602(ShipEquipSlotType p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5603(QuestType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5604(FactionCreditLevel p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5605(SubRankType p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5606(GuildPermission p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5607(GuildItemListUIController.GuildStoreTabType p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5608(DateTime p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5609(uint p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGiftPackageInfo> __Gen_Delegate_Imp561(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5610(TimeSpan p0, bool p1, bool p2, bool p3, bool p4)
        {
        }

        [MethodImpl(0x8000)]
        public List<FormatStringParamInfo> __Gen_Delegate_Imp5611(int p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<FormatStringParamInfo> __Gen_Delegate_Imp5612(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public FormatStringParamInfo __Gen_Delegate_Imp5613(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp5614(CommonFormatStringParamType p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5615(object p0, float p1, float p2, float p3, float p4, object p5, object p6, object p7, object p8)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5616(object p0, float p1, object p2, float p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5617(object p0, object p1, float p2, float p3, DigitFormatUtil.DigitFormatType p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5618(object p0, StoreItemType p1, int p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ILBItem __Gen_Delegate_Imp5619(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGiftPackageTriggerInfo __Gen_Delegate_Imp562(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5620(object p0, out NpcShopItemCategory p1, out NpcShopItemType p2)
        {
        }

        [MethodImpl(0x8000)]
        public List<Func<ILBStoreItemClient, bool>> __Gen_Delegate_Imp5621(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSizeType __Gen_Delegate_Imp5622(object p0, StoreItemType p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp5623(object p0, StoreItemType p1, int p2, bool p3)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp5624(object p0, ulong p1, int p2, int p3, int p4, int p5, int p6)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGiftPackageTriggerInfo> __Gen_Delegate_Imp563(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGMInfoList __Gen_Delegate_Imp564(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGMInfoList> __Gen_Delegate_Imp565(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGrandFactionFeatsPoolInfo __Gen_Delegate_Imp566(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGrandFactionFeatsPoolInfo> __Gen_Delegate_Imp567(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGrandFactionInfo __Gen_Delegate_Imp568(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGrandFactionInfo> __Gen_Delegate_Imp569(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp57(object p0, object p1, object p2, object p3, bool p4, bool p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildActionAdditionRewardInfo __Gen_Delegate_Imp570(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildActionAdditionRewardInfo> __Gen_Delegate_Imp571(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildActionAdditionRewardListInfo __Gen_Delegate_Imp572(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildActionAdditionRewardListInfo> __Gen_Delegate_Imp573(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildActionInfo __Gen_Delegate_Imp574(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildActionInfo> __Gen_Delegate_Imp575(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildAllianceLanguageTypeInfo __Gen_Delegate_Imp576(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildAllianceLanguageTypeInfo> __Gen_Delegate_Imp577(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildBenefitsConfigInfo __Gen_Delegate_Imp578(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildBenefitsConfigInfo> __Gen_Delegate_Imp579(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp58(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildBuildingInfo __Gen_Delegate_Imp580(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildBuildingInfo> __Gen_Delegate_Imp581(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildBuildingLevelDetailInfo __Gen_Delegate_Imp582(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildBuildingLevelDetailInfo> __Gen_Delegate_Imp583(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildCurrencyLogTypeInfo __Gen_Delegate_Imp584(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildCurrencyLogTypeInfo> __Gen_Delegate_Imp585(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildEvaluateScoreInfo __Gen_Delegate_Imp586(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildEvaluateScoreInfo> __Gen_Delegate_Imp587(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildFlagShipOptLogInfo __Gen_Delegate_Imp588(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildFlagShipOptLogInfo> __Gen_Delegate_Imp589(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<string> __Gen_Delegate_Imp59(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildIconBGColorList __Gen_Delegate_Imp590(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildIconBGColorList> __Gen_Delegate_Imp591(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildIconBGList __Gen_Delegate_Imp592(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildIconBGList> __Gen_Delegate_Imp593(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildIconPaintingColorList __Gen_Delegate_Imp594(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildIconPaintingColorList> __Gen_Delegate_Imp595(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildIconPaintingList __Gen_Delegate_Imp596(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildIconPaintingList> __Gen_Delegate_Imp597(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildJobInfo __Gen_Delegate_Imp598(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildJobInfo> __Gen_Delegate_Imp599(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerator __Gen_Delegate_Imp6(object p0, object p1, object p2, object p3, object p4)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<string> __Gen_Delegate_Imp60(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildMineralExtraInfo __Gen_Delegate_Imp600(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildMineralExtraInfo> __Gen_Delegate_Imp601(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildMiningAreaLevelDetailInfo __Gen_Delegate_Imp602(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildMiningAreaLevelDetailInfo> __Gen_Delegate_Imp603(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildMomentsSubType __Gen_Delegate_Imp604(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildMomentsSubType> __Gen_Delegate_Imp605(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildMomentsTypeInfo __Gen_Delegate_Imp606(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildMomentsTypeInfo> __Gen_Delegate_Imp607(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildPermissionInfo __Gen_Delegate_Imp608(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildPermissionInfo> __Gen_Delegate_Imp609(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp61(object p0, object p1, object p2, int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildProduceCategoryInfo __Gen_Delegate_Imp610(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildProduceCategoryInfo> __Gen_Delegate_Imp611(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildProduceTempleteInfo __Gen_Delegate_Imp612(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildProduceTempleteInfo> __Gen_Delegate_Imp613(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildProduceTypeInfo __Gen_Delegate_Imp614(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildProduceTypeInfo> __Gen_Delegate_Imp615(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildPropertiesInfo __Gen_Delegate_Imp616(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildPropertiesInfo> __Gen_Delegate_Imp617(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildSentryTargetTrackingLevelInfo __Gen_Delegate_Imp618(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildSentryTargetTrackingLevelInfo> __Gen_Delegate_Imp619(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<string> __Gen_Delegate_Imp62(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildSentryTrackingSceneQuantity __Gen_Delegate_Imp620(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildSentryTrackingSceneQuantity> __Gen_Delegate_Imp621(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildTradeItemInfo __Gen_Delegate_Imp622(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildTradeItemInfo> __Gen_Delegate_Imp623(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGuildTradePortLevelDetailInfo __Gen_Delegate_Imp624(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGuildTradePortLevelDetailInfo> __Gen_Delegate_Imp625(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataHangerUnlockInfo __Gen_Delegate_Imp626(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataHangerUnlockInfo> __Gen_Delegate_Imp627(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataHelloDailog4FuncNpc __Gen_Delegate_Imp628(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataHelloDailog4FuncNpc> __Gen_Delegate_Imp629(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Action<BytesScriptableObjectMD5, bool> __Gen_Delegate_Imp63(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataHelloDailog4Npc __Gen_Delegate_Imp630(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataHelloDailog4Npc> __Gen_Delegate_Imp631(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataHireCaptainHelloDailog __Gen_Delegate_Imp632(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataHireCaptainHelloDailog> __Gen_Delegate_Imp633(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataHiredCaptainSubRankInfo __Gen_Delegate_Imp634(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataHiredCaptainSubRankInfo> __Gen_Delegate_Imp635(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataImportantEffectInfo __Gen_Delegate_Imp636(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataImportantEffectInfo> __Gen_Delegate_Imp637(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataIncCriminalLevelInfo __Gen_Delegate_Imp638(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataIncCriminalLevelInfo> __Gen_Delegate_Imp639(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> __Gen_Delegate_Imp64(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataInfectInfo __Gen_Delegate_Imp640(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataInfectInfo> __Gen_Delegate_Imp641(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataInputTypeInfo __Gen_Delegate_Imp642(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataInputTypeInfo> __Gen_Delegate_Imp643(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataIslandStarGroupInfo __Gen_Delegate_Imp644(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataIslandStarGroupInfo> __Gen_Delegate_Imp645(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataIslandTypeInfo __Gen_Delegate_Imp646(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataIslandTypeInfo> __Gen_Delegate_Imp647(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataItemDropInfo __Gen_Delegate_Imp648(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataItemDropInfo> __Gen_Delegate_Imp649(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo __Gen_Delegate_Imp65(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataItemObtainSourceTypeInfo __Gen_Delegate_Imp650(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataItemObtainSourceTypeInfo> __Gen_Delegate_Imp651(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataLanguageInfo __Gen_Delegate_Imp652(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataLanguageInfo> __Gen_Delegate_Imp653(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataLaserDamageInfo __Gen_Delegate_Imp654(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataLaserDamageInfo> __Gen_Delegate_Imp655(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataLastNameFeatsPoolInfo __Gen_Delegate_Imp656(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataLastNameFeatsPoolInfo> __Gen_Delegate_Imp657(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataLogicalSoundResTable __Gen_Delegate_Imp658(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataLogicalSoundResTable> __Gen_Delegate_Imp659(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public List<DummyType> __Gen_Delegate_Imp66(object p0, object p1, object p2, object p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataMineralTypeInfo __Gen_Delegate_Imp660(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataMineralTypeInfo> __Gen_Delegate_Imp661(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataMissileInfo __Gen_Delegate_Imp662(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataMissileInfo> __Gen_Delegate_Imp663(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataMonthlyCardInfo __Gen_Delegate_Imp664(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataMonthlyCardInfo> __Gen_Delegate_Imp665(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataMonthlyCardPriviledgeTypeInfo __Gen_Delegate_Imp666(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataMonthlyCardPriviledgeTypeInfo> __Gen_Delegate_Imp667(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNormalItemInfo __Gen_Delegate_Imp668(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNormalItemInfo> __Gen_Delegate_Imp669(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MemoryStream __Gen_Delegate_Imp67(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainFeatsBookInfo __Gen_Delegate_Imp670(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainFeatsBookInfo> __Gen_Delegate_Imp671(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainFeatsInfo __Gen_Delegate_Imp672(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainFeatsInfo> __Gen_Delegate_Imp673(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainFeatsLevelInfo __Gen_Delegate_Imp674(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainFeatsLevelInfo> __Gen_Delegate_Imp675(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainGrowthInfo __Gen_Delegate_Imp676(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainGrowthInfo> __Gen_Delegate_Imp677(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainLevelInfo __Gen_Delegate_Imp678(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainLevelInfo> __Gen_Delegate_Imp679(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientConfigDataLoaderBase __Gen_Delegate_Imp68()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainShipCompInfo __Gen_Delegate_Imp680(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainShipCompInfo> __Gen_Delegate_Imp681(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainShipGrowthInfo __Gen_Delegate_Imp682(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainShipGrowthInfo> __Gen_Delegate_Imp683(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainShipGrowthLineInfo __Gen_Delegate_Imp684(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainShipGrowthLineInfo> __Gen_Delegate_Imp685(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainShipInfo __Gen_Delegate_Imp686(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcCaptainShipInfo> __Gen_Delegate_Imp687(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcChatGroupInfo __Gen_Delegate_Imp688(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcChatGroupInfo> __Gen_Delegate_Imp689(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<string, string> __Gen_Delegate_Imp69(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcFirstNamePool __Gen_Delegate_Imp690(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcFirstNamePool> __Gen_Delegate_Imp691(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcGuildInfo __Gen_Delegate_Imp692(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcGuildInfo> __Gen_Delegate_Imp693(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcInteractionTemplate __Gen_Delegate_Imp694(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcInteractionTemplate> __Gen_Delegate_Imp695(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcInvadeAllianceName __Gen_Delegate_Imp696(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcInvadeAllianceName> __Gen_Delegate_Imp697(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcInvadeTemplateInfo __Gen_Delegate_Imp698(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcInvadeTemplateInfo> __Gen_Delegate_Imp699(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp7(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp70(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcLastNamePool __Gen_Delegate_Imp700(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcLastNamePool> __Gen_Delegate_Imp701(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelBufListInfo __Gen_Delegate_Imp702(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcMonsterLevelBufListInfo> __Gen_Delegate_Imp703(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelDropInfo __Gen_Delegate_Imp704(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcMonsterLevelDropInfo> __Gen_Delegate_Imp705(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelDropListInfo __Gen_Delegate_Imp706(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcMonsterLevelDropListInfo> __Gen_Delegate_Imp707(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterTeamRandomPool __Gen_Delegate_Imp708(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcMonsterTeamRandomPool> __Gen_Delegate_Imp709(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public float __Gen_Delegate_Imp71()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcScriptDailogInfo __Gen_Delegate_Imp710(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcScriptDailogInfo> __Gen_Delegate_Imp711(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcScriptParamInfo __Gen_Delegate_Imp712(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcScriptParamInfo> __Gen_Delegate_Imp713(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipAIInfo __Gen_Delegate_Imp714(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShipAIInfo> __Gen_Delegate_Imp715(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShopItemCategoryInfo __Gen_Delegate_Imp716(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShopItemCategoryInfo> __Gen_Delegate_Imp717(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShopItemInfo __Gen_Delegate_Imp718(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShopItemInfo> __Gen_Delegate_Imp719(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Assembly __Gen_Delegate_Imp72(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShopItemListInfo __Gen_Delegate_Imp720(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShopItemListInfo> __Gen_Delegate_Imp721(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShopItemSaleCountInfo __Gen_Delegate_Imp722(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShopItemSaleCountInfo> __Gen_Delegate_Imp723(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShopItemTypeInfo __Gen_Delegate_Imp724(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShopItemTypeInfo> __Gen_Delegate_Imp725(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcSovereignBattleRecommendedConfigurationInfo __Gen_Delegate_Imp726(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcSovereignBattleRecommendedConfigurationInfo> __Gen_Delegate_Imp727(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo __Gen_Delegate_Imp728(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcTalkerInfo> __Gen_Delegate_Imp729(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IAudioManager __Gen_Delegate_Imp73(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerResInfo __Gen_Delegate_Imp730(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcTalkerResInfo> __Gen_Delegate_Imp731(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerTemplateInfo __Gen_Delegate_Imp732(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcTalkerTemplateInfo> __Gen_Delegate_Imp733(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillInfo __Gen_Delegate_Imp734(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPassiveSkillInfo> __Gen_Delegate_Imp735(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillLevelInfo __Gen_Delegate_Imp736(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPassiveSkillLevelInfo> __Gen_Delegate_Imp737(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillTypeInfo __Gen_Delegate_Imp738(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPassiveSkillTypeInfo> __Gen_Delegate_Imp739(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ServerSettingManager __Gen_Delegate_Imp74(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPersonalTradeItemTypeInfo __Gen_Delegate_Imp740(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPersonalTradeItemTypeInfo> __Gen_Delegate_Imp741(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPlanetLikeBuildingInfo __Gen_Delegate_Imp742(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPlanetLikeBuildingInfo> __Gen_Delegate_Imp743(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPlayerRegionInfo __Gen_Delegate_Imp744(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPlayerRegionInfo> __Gen_Delegate_Imp745(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPlayerSpaceShipListInfo __Gen_Delegate_Imp746(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPlayerSpaceShipListInfo> __Gen_Delegate_Imp747(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPlayerStoreItemListInfo __Gen_Delegate_Imp748(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPlayerStoreItemListInfo> __Gen_Delegate_Imp749(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp75(object p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPoliceMonterTeamInfo __Gen_Delegate_Imp750(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPoliceMonterTeamInfo> __Gen_Delegate_Imp751(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPredefineGuildBenefitsInfo __Gen_Delegate_Imp752(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPredefineGuildBenefitsInfo> __Gen_Delegate_Imp753(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPredefineGuildMomentsInfo __Gen_Delegate_Imp754(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPredefineGuildMomentsInfo> __Gen_Delegate_Imp755(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPredefineMailInfo __Gen_Delegate_Imp756(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPredefineMailInfo> __Gen_Delegate_Imp757(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPreDefineNpcCaptainStaticInfo __Gen_Delegate_Imp758(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPreDefineNpcCaptainStaticInfo> __Gen_Delegate_Imp759(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ClientConfigDataLoaderBase __Gen_Delegate_Imp76(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataProduceUnlockInfo __Gen_Delegate_Imp760(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataProduceUnlockInfo> __Gen_Delegate_Imp761(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataProfessionFeatsPoolInfo __Gen_Delegate_Imp762(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataProfessionFeatsPoolInfo> __Gen_Delegate_Imp763(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPropertiesInfo __Gen_Delegate_Imp764(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPropertiesInfo> __Gen_Delegate_Imp765(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPVPSignalRatioInfo __Gen_Delegate_Imp766(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataPVPSignalRatioInfo> __Gen_Delegate_Imp767(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestAdditionRewardInfo __Gen_Delegate_Imp768(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestAdditionRewardInfo> __Gen_Delegate_Imp769(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp77(object p0, bool p1, bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestAdditionRewardListInfo __Gen_Delegate_Imp770(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestAdditionRewardListInfo> __Gen_Delegate_Imp771(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestCompleteCondParamEx __Gen_Delegate_Imp772(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestCompleteCondParamEx> __Gen_Delegate_Imp773(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestEnvirmentCustomString __Gen_Delegate_Imp774(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestEnvirmentCustomString> __Gen_Delegate_Imp775(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestEnvirmentInstanceInfo __Gen_Delegate_Imp776(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestEnvirmentInstanceInfo> __Gen_Delegate_Imp777(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestGrandFactionRewardInfo __Gen_Delegate_Imp778(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestGrandFactionRewardInfo> __Gen_Delegate_Imp779(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp78(object p0, object p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestItemInfo __Gen_Delegate_Imp780(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestItemInfo> __Gen_Delegate_Imp781(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestLevelInfo __Gen_Delegate_Imp782(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestLevelInfo> __Gen_Delegate_Imp783(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataRandomQuestPoolInfo __Gen_Delegate_Imp784(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataRandomQuestPoolInfo> __Gen_Delegate_Imp785(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataRankInfo __Gen_Delegate_Imp786(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataRankInfo> __Gen_Delegate_Imp787(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataRareWormholeActiveInfo __Gen_Delegate_Imp788(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataRareWormholeActiveInfo> __Gen_Delegate_Imp789(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GameManager __Gen_Delegate_Imp79()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataRareWormholePoolInfo __Gen_Delegate_Imp790(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataRareWormholePoolInfo> __Gen_Delegate_Imp791(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataRechargeItemInfo __Gen_Delegate_Imp792(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataRechargeItemInfo> __Gen_Delegate_Imp793(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataRechargeRewardItemDesc __Gen_Delegate_Imp794(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataRechargeRewardItemDesc> __Gen_Delegate_Imp795(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataResourceShopItemCategoryInfo __Gen_Delegate_Imp796(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataResourceShopItemCategoryInfo> __Gen_Delegate_Imp797(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSailReportEventSetting __Gen_Delegate_Imp798(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSailReportEventSetting> __Gen_Delegate_Imp799(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public CRIProvider __Gen_Delegate_Imp8(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public GameClientSetting __Gen_Delegate_Imp80(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneCameraAnimationClipInfo __Gen_Delegate_Imp800(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneCameraAnimationClipInfo> __Gen_Delegate_Imp801(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneDropGroupInfo __Gen_Delegate_Imp802(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneDropGroupInfo> __Gen_Delegate_Imp803(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneInfo __Gen_Delegate_Imp804(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneInfo> __Gen_Delegate_Imp805(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneResInfo __Gen_Delegate_Imp806(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneResInfo> __Gen_Delegate_Imp807(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneScreenEffectInfo __Gen_Delegate_Imp808(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneScreenEffectInfo> __Gen_Delegate_Imp809(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Func<ResourceManager> __Gen_Delegate_Imp81()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneScriptParamInfo __Gen_Delegate_Imp810(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneScriptParamInfo> __Gen_Delegate_Imp811(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneTypeInfo __Gen_Delegate_Imp812(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneTypeInfo> __Gen_Delegate_Imp813(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneWeaponAmmoReloadInfo __Gen_Delegate_Imp814(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSceneWeaponAmmoReloadInfo> __Gen_Delegate_Imp815(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSecurityLevelInfo __Gen_Delegate_Imp816(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSecurityLevelInfo> __Gen_Delegate_Imp817(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSheetToFilePathMapTable __Gen_Delegate_Imp818(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSheetToFilePathMapTable> __Gen_Delegate_Imp819(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public StringTableManagerBase __Gen_Delegate_Imp82(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipAbilityLevelInfo __Gen_Delegate_Imp820(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipAbilityLevelInfo> __Gen_Delegate_Imp821(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipAssemblyOptimizingInfo __Gen_Delegate_Imp822(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipAssemblyOptimizingInfo> __Gen_Delegate_Imp823(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipEnergyRecoveryInfo __Gen_Delegate_Imp824(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipEnergyRecoveryInfo> __Gen_Delegate_Imp825(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipGrowthLineFeatsPoolInfo __Gen_Delegate_Imp826(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipGrowthLineFeatsPoolInfo> __Gen_Delegate_Imp827(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipNamedEffectTypeInfo __Gen_Delegate_Imp828(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipNamedEffectTypeInfo> __Gen_Delegate_Imp829(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public IPlayerContextNetworkClient __Gen_Delegate_Imp83(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipPreCustomTemplateInfo __Gen_Delegate_Imp830(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipPreCustomTemplateInfo> __Gen_Delegate_Imp831(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipInfo __Gen_Delegate_Imp832(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipSuperEquipInfo> __Gen_Delegate_Imp833(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipRunningInfo __Gen_Delegate_Imp834(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipSuperEquipRunningInfo> __Gen_Delegate_Imp835(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipTacticalEquipInfo __Gen_Delegate_Imp836(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipTacticalEquipInfo> __Gen_Delegate_Imp837(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipTypeInfo __Gen_Delegate_Imp838(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipTypeInfo> __Gen_Delegate_Imp839(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerContextBase __Gen_Delegate_Imp84(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSignalInfo __Gen_Delegate_Imp840(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSignalInfo> __Gen_Delegate_Imp841(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSimpleObjectInfo __Gen_Delegate_Imp842(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSimpleObjectInfo> __Gen_Delegate_Imp843(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSimpleProgramCamera __Gen_Delegate_Imp844(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSimpleProgramCamera> __Gen_Delegate_Imp845(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemDelegateMissionPoolCountInfo __Gen_Delegate_Imp846(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSolarSystemDelegateMissionPoolCountInfo> __Gen_Delegate_Imp847(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemFlourishLevelInfo __Gen_Delegate_Imp848(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSolarSystemFlourishLevelInfo> __Gen_Delegate_Imp849(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public HotfixManager __Gen_Delegate_Imp85()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemGuildMinrealOutputInfo __Gen_Delegate_Imp850(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSolarSystemGuildMinrealOutputInfo> __Gen_Delegate_Imp851(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemPoolCountInitInfo __Gen_Delegate_Imp852(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSolarSystemPoolCountInitInfo> __Gen_Delegate_Imp853(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemQuestPoolCountInfo __Gen_Delegate_Imp854(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSolarSystemQuestPoolCountInfo> __Gen_Delegate_Imp855(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSoundResTable __Gen_Delegate_Imp856(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSoundResTable> __Gen_Delegate_Imp857(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSovereignMaintenanceCostInfo __Gen_Delegate_Imp858(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSovereignMaintenanceCostInfo> __Gen_Delegate_Imp859(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MethodInfo __Gen_Delegate_Imp86(object p0, object p1, ParamType[] p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSovereignMaintenanceCostLevelInfo __Gen_Delegate_Imp860(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSovereignMaintenanceCostLevelInfo> __Gen_Delegate_Imp861(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo __Gen_Delegate_Imp862(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceShipInfo> __Gen_Delegate_Imp863(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceSignalDifficultyInfo __Gen_Delegate_Imp864(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceSignalDifficultyInfo> __Gen_Delegate_Imp865(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceSignalInfo __Gen_Delegate_Imp866(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceSignalInfo> __Gen_Delegate_Imp867(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceSignalPoolInfo __Gen_Delegate_Imp868(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceSignalPoolInfo> __Gen_Delegate_Imp869(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp87(object p0, bool p1, object p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceSignalRewardModifyInfo __Gen_Delegate_Imp870(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceSignalRewardModifyInfo> __Gen_Delegate_Imp871(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceStationNpcShopInfo __Gen_Delegate_Imp872(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceStationNpcShopInfo> __Gen_Delegate_Imp873(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceStationResInfo __Gen_Delegate_Imp874(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceStationResInfo> __Gen_Delegate_Imp875(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceStationSceneInfo __Gen_Delegate_Imp876(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceStationSceneInfo> __Gen_Delegate_Imp877(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceStationTypeInfo __Gen_Delegate_Imp878(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceStationTypeInfo> __Gen_Delegate_Imp879(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public Stream __Gen_Delegate_Imp88(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataStarGateInfo __Gen_Delegate_Imp880(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataStarGateInfo> __Gen_Delegate_Imp881(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataStarGateSceneInfo __Gen_Delegate_Imp882(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataStarGateSceneInfo> __Gen_Delegate_Imp883(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataStringTable __Gen_Delegate_Imp884(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataStringTable> __Gen_Delegate_Imp885(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSubRankInfo __Gen_Delegate_Imp886(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSubRankInfo> __Gen_Delegate_Imp887(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSuperWeaponInfo __Gen_Delegate_Imp888(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSuperWeaponInfo> __Gen_Delegate_Imp889(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public MemoryStream __Gen_Delegate_Imp89(object p0, object p1)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSystemFunctionDescription __Gen_Delegate_Imp890(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSystemFunctionDescription> __Gen_Delegate_Imp891(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSystemFuncTypeInfo __Gen_Delegate_Imp892(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSystemFuncTypeInfo> __Gen_Delegate_Imp893(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSystemMessage __Gen_Delegate_Imp894(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSystemMessage> __Gen_Delegate_Imp895(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSystemPushInfo __Gen_Delegate_Imp896(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSystemPushInfo> __Gen_Delegate_Imp897(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechInfo __Gen_Delegate_Imp898(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTechInfo> __Gen_Delegate_Imp899(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp9(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public uint __Gen_Delegate_Imp90()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechTypeInfo __Gen_Delegate_Imp900(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTechTypeInfo> __Gen_Delegate_Imp901(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechUITypeInfo __Gen_Delegate_Imp902(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTechUITypeInfo> __Gen_Delegate_Imp903(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTestBook __Gen_Delegate_Imp904(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTestBook> __Gen_Delegate_Imp905(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTestNPCShipListInfo __Gen_Delegate_Imp906(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTestNPCShipListInfo> __Gen_Delegate_Imp907(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTradeBuyNpcShopInfo __Gen_Delegate_Imp908(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTradeBuyNpcShopInfo> __Gen_Delegate_Imp909(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public int __Gen_Delegate_Imp91()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTradeItemInfo __Gen_Delegate_Imp910(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTradeItemInfo> __Gen_Delegate_Imp911(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTradeSaleNpcShopInfo __Gen_Delegate_Imp912(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTradeSaleNpcShopInfo> __Gen_Delegate_Imp913(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataUIText __Gen_Delegate_Imp914(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataUIText> __Gen_Delegate_Imp915(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataUserGuideGroupInfo __Gen_Delegate_Imp916(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataUserGuideGroupInfo> __Gen_Delegate_Imp917(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataUserGuideStepInfo __Gen_Delegate_Imp918(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataUserGuideStepInfo> __Gen_Delegate_Imp919(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public LogManager __Gen_Delegate_Imp92()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataUserGuideTriggerPointInfo __Gen_Delegate_Imp920(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataUserGuideTriggerPointInfo> __Gen_Delegate_Imp921(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataVideoSubPathToBundleTable __Gen_Delegate_Imp922(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataVideoSubPathToBundleTable> __Gen_Delegate_Imp923(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataVitalityRewardInfo __Gen_Delegate_Imp924(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataVitalityRewardInfo> __Gen_Delegate_Imp925(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWeaponInfo __Gen_Delegate_Imp926(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWeaponInfo> __Gen_Delegate_Imp927(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholePoolInfo __Gen_Delegate_Imp928(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholePoolInfo> __Gen_Delegate_Imp929(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp93(object p0, bool p1, bool p2, object p3, object p4, object p5)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholeSepcailSceneInfo __Gen_Delegate_Imp930(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholeSepcailSceneInfo> __Gen_Delegate_Imp931(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholeSolarSystemInfo __Gen_Delegate_Imp932(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholeSolarSystemInfo> __Gen_Delegate_Imp933(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholeStarGateSceneInfo __Gen_Delegate_Imp934(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholeStarGateSceneInfo> __Gen_Delegate_Imp935(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWormholeStarGroupInfo __Gen_Delegate_Imp936(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholeStarGroupInfo> __Gen_Delegate_Imp937(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceSimpleObject3DInfo __Gen_Delegate_Imp938(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceSimpleObject3DInfo> __Gen_Delegate_Imp939(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp94(object p0, object p1, object p2, LogType p3)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShip3DInfo __Gen_Delegate_Imp940(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceShip3DInfo> __Gen_Delegate_Imp941(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataStargate3DInfo __Gen_Delegate_Imp942(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataStargate3DInfo> __Gen_Delegate_Imp943(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceStation3DInfo __Gen_Delegate_Imp944(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSpaceStation3DInfo> __Gen_Delegate_Imp945(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataST_CN __Gen_Delegate_Imp946(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataST_CN> __Gen_Delegate_Imp947(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataST_EN __Gen_Delegate_Imp948(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataST_EN> __Gen_Delegate_Imp949(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public FileLogger __Gen_Delegate_Imp95(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataST_TW __Gen_Delegate_Imp950(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataST_TW> __Gen_Delegate_Imp951(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSST_CN __Gen_Delegate_Imp952(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSST_CN> __Gen_Delegate_Imp953(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSST_EN __Gen_Delegate_Imp954(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSST_EN> __Gen_Delegate_Imp955(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSST_TW __Gen_Delegate_Imp956(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSST_TW> __Gen_Delegate_Imp957(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGDBST_CN __Gen_Delegate_Imp958(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGDBST_CN> __Gen_Delegate_Imp959(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public string __Gen_Delegate_Imp96()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGDBST_EN __Gen_Delegate_Imp960(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGDBST_EN> __Gen_Delegate_Imp961(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataGDBST_TW __Gen_Delegate_Imp962(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataGDBST_TW> __Gen_Delegate_Imp963(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBufInfo __Gen_Delegate_Imp964(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataBufInfo> __Gen_Delegate_Imp965(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDialogInfo __Gen_Delegate_Imp966(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataDialogInfo> __Gen_Delegate_Imp967(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterInfo __Gen_Delegate_Imp968(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcMonsterInfo> __Gen_Delegate_Imp969(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerContextBase __Gen_Delegate_Imp97()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelBufInfo __Gen_Delegate_Imp970(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcMonsterLevelBufInfo> __Gen_Delegate_Imp971(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterTeamInfo __Gen_Delegate_Imp972(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcMonsterTeamInfo> __Gen_Delegate_Imp973(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipInfo __Gen_Delegate_Imp974(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShipInfo> __Gen_Delegate_Imp975(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipTemplateInfo __Gen_Delegate_Imp976(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataNpcShipTemplateInfo> __Gen_Delegate_Imp977(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataProduceBlueprintInfo __Gen_Delegate_Imp978(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataProduceBlueprintInfo> __Gen_Delegate_Imp979(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public bool __Gen_Delegate_Imp98(object p0, object p1, out bool p2)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestInfo __Gen_Delegate_Imp980(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataQuestInfo> __Gen_Delegate_Imp981(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSensitiveWords __Gen_Delegate_Imp982(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataSensitiveWords> __Gen_Delegate_Imp983(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipEquipInfo __Gen_Delegate_Imp984(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataShipEquipInfo> __Gen_Delegate_Imp985(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechLevelInfo __Gen_Delegate_Imp986(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataTechLevelInfo> __Gen_Delegate_Imp987(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataScene3DInfo __Gen_Delegate_Imp988(object p0, int p1)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataScene3DInfo> __Gen_Delegate_Imp989(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp99(object p0, DateTime p1)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp990(object p0, object p1, out string p2, out int p3)
        {
        }

        [MethodImpl(0x8000)]
        public ActivityType __Gen_Delegate_Imp991(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp992(object p0, ActivityType p1)
        {
        }

        [MethodImpl(0x8000)]
        public List<ActivityRewardInfo> __Gen_Delegate_Imp993(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public SystemFuncType __Gen_Delegate_Imp994(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp995(object p0, SystemFuncType p1)
        {
        }

        [MethodImpl(0x8000)]
        public IExtension __Gen_Delegate_Imp996(object p0, bool p1)
        {
        }

        [MethodImpl(0x8000)]
        public AgeType __Gen_Delegate_Imp997(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void __Gen_Delegate_Imp998(object p0, AgeType p1)
        {
        }

        [MethodImpl(0x8000)]
        public WeaponCategory __Gen_Delegate_Imp999(object p0)
        {
        }

        [MethodImpl(0x8000)]
        public void InParam<T>(T p)
        {
            this.paramList.Add(p);
        }

        public void InParams<T>(T[] ps)
        {
            this.paramList.Add(ps);
        }

        [MethodImpl(0x8000)]
        public void Invoke(int nRet)
        {
        }

        public void InvokeSessionEnd()
        {
            this.paramList = null;
            this.result = null;
        }

        public TResult InvokeSessionEndWithResult<TResult>() => 
            ((TResult) this.result);

        public void InvokeSessionStart()
        {
            this.paramList = new List<object>();
        }

        [MethodImpl(0x8000)]
        public void OutParam<TResult>(int pos, out TResult ret)
        {
        }
    }
}

