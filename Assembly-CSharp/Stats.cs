﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Stats : MonoBehaviour
{
    private int m_updateCount;
    private float m_fps;
    private float m_lastFpsResetTime;
    private int m_unit;
    private GUIStyle m_textStyle;
    public bool m_isShowStats = true;
    private DateTime m_showStatsByGMFinishTime = DateTime.MinValue;
    private bool m_isShowStatsByGM;

    [MethodImpl(0x8000)]
    private void ComputeFps()
    {
    }

    [MethodImpl(0x8000)]
    private void OnGUI()
    {
        if (this.m_isShowStats || this.m_isShowStatsByGM)
        {
            int num = Screen.width / 50;
            if (num != this.m_unit)
            {
                this.m_unit = num;
                this.m_textStyle = new GUIStyle(GUI.skin.label);
                this.m_textStyle.normal.textColor = Color.white;
                this.m_textStyle.fontSize = (this.m_unit * 7) / 8;
            }
            int unit = this.m_unit;
            GUI.Label(new Rect((float) this.m_unit, (float) unit, (float) (this.m_unit * 20), (float) (this.m_unit * 4)), $"FPS  {this.m_fps:F1} ({1000f / this.m_fps:F1})  Resolution:{Screen.currentResolution.width}X{Screen.currentResolution.height}", this.m_textStyle);
        }
    }

    [MethodImpl(0x8000)]
    public void ShowStatsByGm(float showTime)
    {
    }

    private void Start()
    {
    }

    [MethodImpl(0x8000)]
    private void Update()
    {
        if (this.m_isShowStatsByGM && (this.m_showStatsByGMFinishTime < DateTime.Now))
        {
            this.m_isShowStatsByGM = false;
        }
        if (this.m_isShowStats || this.m_isShowStatsByGM)
        {
            this.ComputeFps();
        }
    }
}

