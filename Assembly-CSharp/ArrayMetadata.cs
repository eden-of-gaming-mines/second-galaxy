﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
internal struct ArrayMetadata
{
    private System.Type element_type;
    private bool is_array;
    private bool is_list;
    public System.Type ElementType
    {
        [MethodImpl(0x8000)]
        get
        {
        }
        set => 
            (this.element_type = value);
    }
    public bool IsArray
    {
        get => 
            this.is_array;
        set => 
            (this.is_array = value);
    }
    public bool IsList
    {
        get => 
            this.is_list;
        set => 
            (this.is_list = value);
    }
}

