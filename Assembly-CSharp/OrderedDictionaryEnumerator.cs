﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal class OrderedDictionaryEnumerator : IDictionaryEnumerator, IEnumerator
{
    private IEnumerator<KeyValuePair<string, JsonData>> list_enumerator;

    public OrderedDictionaryEnumerator(IEnumerator<KeyValuePair<string, JsonData>> enumerator)
    {
        this.list_enumerator = enumerator;
    }

    public bool MoveNext() => 
        this.list_enumerator.MoveNext();

    public void Reset()
    {
        this.list_enumerator.Reset();
    }

    public object Current =>
        this.Entry;

    public DictionaryEntry Entry
    {
        [MethodImpl(0x8000)]
        get
        {
            KeyValuePair<string, JsonData> current = this.list_enumerator.Current;
            return new DictionaryEntry(current.Key, current.Value);
        }
    }

    public object Key
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    public object Value
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }
}

