﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(Camera)), ExecuteInEditMode]
public class Sexybloom : MonoBehaviour
{
    [Range(0f, 1f)]
    public float bloomIntensity;
    public Shader shader;
    private Material material;
    private bool isSupported;
    private float blurSize;
    public bool inputIsHDR;

    [MethodImpl(0x8000)]
    private void OnDisable()
    {
    }

    [MethodImpl(0x8000)]
    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
    }

    [MethodImpl(0x8000)]
    private void Start()
    {
    }
}

