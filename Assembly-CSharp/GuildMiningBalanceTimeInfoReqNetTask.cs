﻿using BlackJack.BJFramework.Runtime.TaskNs;
using System;
using System.Runtime.CompilerServices;

public class GuildMiningBalanceTimeInfoReqNetTask : NetWorkTransactionTask
{
    public int m_result;

    public GuildMiningBalanceTimeInfoReqNetTask() : base(10f, true, false, false)
    {
    }

    private void OnGuildMiningBalanceTimeInfoAck(int result)
    {
        this.m_result = result;
        base.Stop();
    }

    [MethodImpl(0x8000)]
    protected override void RegisterNetworkEvent()
    {
    }

    [MethodImpl(0x8000)]
    protected override bool StartNetWorking()
    {
    }

    [MethodImpl(0x8000)]
    protected override void UnregisterNetworkEvent()
    {
    }
}

