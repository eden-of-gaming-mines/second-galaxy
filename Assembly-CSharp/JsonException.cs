﻿using System;
using System.Runtime.CompilerServices;

public class JsonException : ApplicationException
{
    public JsonException()
    {
    }

    [MethodImpl(0x8000)]
    internal JsonException(ParserToken token)
    {
    }

    [MethodImpl(0x8000)]
    internal JsonException(int c)
    {
    }

    public JsonException(string message) : base(message)
    {
    }

    [MethodImpl(0x8000)]
    internal JsonException(ParserToken token, Exception inner_exception)
    {
    }

    [MethodImpl(0x8000)]
    internal JsonException(int c, Exception inner_exception)
    {
    }

    public JsonException(string message, Exception inner_exception) : base(message, inner_exception)
    {
    }
}

