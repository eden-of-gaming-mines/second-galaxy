﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

public class UIAnimationEventHandler : MonoBehaviour
{
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action EventOnAnimationFinished;

    public event Action EventOnAnimationFinished
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    [MethodImpl(0x8000)]
    public void OnAnimatonFinished()
    {
    }
}

