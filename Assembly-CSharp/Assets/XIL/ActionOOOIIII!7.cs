﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOOOIIII<T0, T1, T2, in T3, in T4, in T5, in T6>(ref T0 p0, ref T1 p1, ref T2 p2, T3 p3, T4 p4, T5 p5, T6 p6);
}

