﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionIO<in T0, T1, out TReturn>(T0 p0, ref T1 p1);
}

