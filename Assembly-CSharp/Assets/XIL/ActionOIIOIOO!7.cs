﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOIIOIOO<T0, in T1, in T2, T3, in T4, T5, T6>(ref T0 p0, T1 p1, T2 p2, ref T3 p3, T4 p4, ref T5 p5, ref T6 p6);
}

