﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionOIOO<T0, in T1, T2, T3, out TReturn>(ref T0 p0, T1 p1, ref T2 p2, ref T3 p3);
}

