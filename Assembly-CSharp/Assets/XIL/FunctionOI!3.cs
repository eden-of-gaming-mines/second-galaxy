﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionOI<T0, in T1, out TReturn>(ref T0 p0, T1 p1);
}

