﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionIOIIIO<in T0, T1, in T2, in T3, in T4, T5, out TReturn>(T0 p0, ref T1 p1, T2 p2, T3 p3, T4 p4, ref T5 p5);
}

