﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionIIOIIOI<in T0, in T1, T2, in T3, in T4, T5, in T6, out TReturn>(T0 p0, T1 p1, ref T2 p2, T3 p3, T4 p4, ref T5 p5, T6 p6);
}

