﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOOII<T0, T1, in T2, in T3>(ref T0 p0, ref T1 p1, T2 p2, T3 p3);
}

