﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionO<T0, out TReturn>(ref T0 p0);
}

