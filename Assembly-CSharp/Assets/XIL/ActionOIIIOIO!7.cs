﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOIIIOIO<T0, in T1, in T2, in T3, T4, in T5, T6>(ref T0 p0, T1 p1, T2 p2, T3 p3, ref T4 p4, T5 p5, ref T6 p6);
}

