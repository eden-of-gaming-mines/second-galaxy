﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionOOIOI<T0, T1, in T2, T3, in T4, out TReturn>(ref T0 p0, ref T1 p1, T2 p2, ref T3 p3, T4 p4);
}

