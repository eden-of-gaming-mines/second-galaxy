﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionIIOOIIO<in T0, in T1, T2, T3, in T4, in T5, T6>(T0 p0, T1 p1, ref T2 p2, ref T3 p3, T4 p4, T5 p5, ref T6 p6);
}

