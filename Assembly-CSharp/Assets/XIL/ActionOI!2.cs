﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOI<T0, in T1>(ref T0 p0, T1 p1);
}

