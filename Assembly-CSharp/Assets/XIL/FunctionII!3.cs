﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionII<in T0, in T1, out TReturn>(T0 p0, T1 p1);
}

