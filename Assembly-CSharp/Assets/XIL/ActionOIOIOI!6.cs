﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOIOIOI<T0, in T1, T2, in T3, T4, in T5>(ref T0 p0, T1 p1, ref T2 p2, T3 p3, ref T4 p4, T5 p5);
}

