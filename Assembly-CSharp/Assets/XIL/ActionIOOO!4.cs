﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionIOOO<in T0, T1, T2, T3>(T0 p0, ref T1 p1, ref T2 p2, ref T3 p3);
}

