﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOO<T0, T1>(ref T0 p0, ref T1 p1);
}

