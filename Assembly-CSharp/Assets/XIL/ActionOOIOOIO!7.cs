﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOOIOOIO<T0, T1, in T2, T3, T4, in T5, T6>(ref T0 p0, ref T1 p1, T2 p2, ref T3 p3, ref T4 p4, T5 p5, ref T6 p6);
}

