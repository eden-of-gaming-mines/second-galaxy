﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionIII<in T0, in T1, in T2, out TReturn>(T0 p0, T1 p1, T2 p2);
}

