﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionIIO<in T0, in T1, T2>(T0 p0, T1 p1, ref T2 p2);
}

