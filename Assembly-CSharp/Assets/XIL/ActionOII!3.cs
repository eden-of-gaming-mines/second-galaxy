﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOII<T0, in T1, in T2>(ref T0 p0, T1 p1, T2 p2);
}

