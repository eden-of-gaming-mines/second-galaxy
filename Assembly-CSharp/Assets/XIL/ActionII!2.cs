﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionII<in T0, in T1>(T0 p0, T1 p1);
}

