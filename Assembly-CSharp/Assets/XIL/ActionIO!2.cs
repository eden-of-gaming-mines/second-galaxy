﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionIO<in T0, T1>(T0 p0, ref T1 p1);
}

