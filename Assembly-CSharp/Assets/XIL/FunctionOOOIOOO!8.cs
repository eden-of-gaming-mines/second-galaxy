﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionOOOIOOO<T0, T1, T2, in T3, T4, T5, T6, out TReturn>(ref T0 p0, ref T1 p1, ref T2 p2, T3 p3, ref T4 p4, ref T5 p5, ref T6 p6);
}

