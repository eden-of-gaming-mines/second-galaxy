﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionIOI<in T0, T1, in T2>(T0 p0, ref T1 p1, T2 p2);
}

