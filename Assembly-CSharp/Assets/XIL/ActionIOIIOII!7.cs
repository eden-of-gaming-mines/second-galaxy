﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionIOIIOII<in T0, T1, in T2, in T3, T4, in T5, in T6>(T0 p0, ref T1 p1, T2 p2, T3 p3, ref T4 p4, T5 p5, T6 p6);
}

