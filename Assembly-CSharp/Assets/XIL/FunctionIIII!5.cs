﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionIIII<in T0, in T1, in T2, in T3, out TReturn>(T0 p0, T1 p1, T2 p2, T3 p3);
}

