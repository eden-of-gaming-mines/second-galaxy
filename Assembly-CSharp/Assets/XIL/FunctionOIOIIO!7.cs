﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate TReturn FunctionOIOIIO<T0, in T1, T2, in T3, in T4, T5, out TReturn>(ref T0 p0, T1 p1, ref T2 p2, T3 p3, T4 p4, ref T5 p5);
}

