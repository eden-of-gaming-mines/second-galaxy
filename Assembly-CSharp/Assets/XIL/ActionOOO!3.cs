﻿namespace Assets.XIL
{
    using System;
    using System.Runtime.CompilerServices;

    public delegate void ActionOOO<T0, T1, T2>(ref T0 p0, ref T1 p1, ref T2 p2);
}

