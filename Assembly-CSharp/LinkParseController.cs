﻿using BlackJack.BJFramework.Runtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LinkParseController : MonoBehaviour
{
    private List<GameObject> m_unUsedButtonItemList;
    private GameObject m_unUseItemRoot;
    private GameObject m_buttonAsset;
    private Camera m_camera;
    private static LinkParseController m_instance;

    [MethodImpl(0x8000)]
    private LinkParseController()
    {
    }

    [MethodImpl(0x8000)]
    private void FreeItemsFormParent(GameObject showText)
    {
    }

    [MethodImpl(0x8000)]
    private void FreeLinkButtonItem(GameObject buttonItem)
    {
    }

    [MethodImpl(0x8000)]
    private GameObject GetLinkButtonItem()
    {
    }

    [MethodImpl(0x8000)]
    public void Init(Camera camera)
    {
    }

    [MethodImpl(0x8000)]
    private Vector2 LocationPosToScreenPos(Transform transf, Vector3 orignalPos)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    public IEnumerator ParseString(Text showText, string showString, string specialString, Action clickEvent)
    {
    }

    public void ParseStringForLink(Text showText, string showString, List<string> specialStringList, Action clickEvent)
    {
        showText.text = showString;
    }

    public void ParseStringForLink(Text showText, string showString, string specialString, Action clickEvent)
    {
        showText.text = showString;
    }

    public static LinkParseController Instance
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    [CompilerGenerated]
    private sealed class <ParseString>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string showString;
        internal Text showText;
        internal string specialString;
        internal int <startIndex>__0;
        internal int <endIndex>__0;
        internal TextGenerator <textGen>__0;
        internal UIVertex <startPoint>__0;
        internal UIVertex <startPoint3>__0;
        internal UIVertex <endPoint>__0;
        internal float <checkSize>__0;
        internal Action clickEvent;
        internal LinkParseController $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;
        private <ParseString>c__AnonStorey1 $locvar0;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.$locvar0 = new <ParseString>c__AnonStorey1();
                    this.$locvar0.<>f__ref$0 = this;
                    this.$locvar0.clickEvent = this.clickEvent;
                    this.showText.text = this.showString;
                    this.$this.FreeItemsFormParent(this.showText.gameObject);
                    if (!string.IsNullOrEmpty(this.specialString))
                    {
                        this.<startIndex>__0 = this.showString.IndexOf(this.specialString);
                        if (this.<startIndex>__0 != -1)
                        {
                            this.<endIndex>__0 = (this.<startIndex>__0 + this.specialString.Length) - 1;
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            return true;
                        }
                    }
                    break;

                case 1:
                    this.<textGen>__0 = this.showText.get_cachedTextGeneratorForLayout();
                    if ((this.<textGen>__0 == null) || (this.<textGen>__0.vertexCount == 0))
                    {
                        this.<textGen>__0 = this.showText.get_cachedTextGenerator();
                    }
                    if ((this.<textGen>__0 == null) || (this.<textGen>__0.vertexCount == 0))
                    {
                        this.<textGen>__0 = this.showText.get_cachedTextGeneratorForLayout();
                        if ((this.<textGen>__0 == null) || (this.<textGen>__0.vertexCount == 0))
                        {
                            this.<textGen>__0 = this.showText.get_cachedTextGenerator();
                        }
                        Debug.LogWarning($"<color=red>No Verts Data , Name: {this.showText.transform.parent.gameObject.name} Ticks: {Timer.m_currTick}</color>");
                    }
                    else
                    {
                        this.<startPoint>__0 = this.<textGen>__0.verts[this.<startIndex>__0 * 4];
                        this.<startPoint3>__0 = this.<textGen>__0.verts[(this.<startIndex>__0 * 4) + 3];
                        this.<endPoint>__0 = this.<textGen>__0.verts[(this.<endIndex>__0 * 4) + 1];
                        this.<checkSize>__0 = Math.Abs((float) (this.<startPoint3>__0.position.y - this.<startPoint>__0.position.y));
                        if (Math.Abs((float) (this.<startPoint>__0.position.y - this.<endPoint>__0.position.y)) >= this.<checkSize>__0)
                        {
                            Debug.LogError("not a line");
                        }
                        else
                        {
                            GameObject linkButtonItem = this.$this.GetLinkButtonItem();
                            linkButtonItem.transform.SetParent(this.showText.transform, false);
                            linkButtonItem.gameObject.SetActive(true);
                            linkButtonItem.transform.localPosition = new Vector3(this.<startPoint>__0.position.x, this.<startPoint>__0.position.y, 0f);
                            Vector2 vector = this.$this.LocationPosToScreenPos(this.showText.transform, this.<startPoint>__0.position);
                            Vector2 vector2 = this.$this.LocationPosToScreenPos(this.showText.transform, this.<startPoint3>__0.position);
                            float x = Math.Abs((float) (this.$this.LocationPosToScreenPos(this.showText.transform, this.<endPoint>__0.position).x - vector.x));
                            linkButtonItem.GetComponent<RectTransform>().sizeDelta = new Vector2(x, Math.Abs((float) (vector2.y - vector.y)));
                            linkButtonItem.GetComponent<ButtonEx>().onClick.AddListener(new UnityAction(this.$locvar0.<>m__0));
                        }
                        this.$PC = -1;
                    }
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;

        private sealed class <ParseString>c__AnonStorey1
        {
            internal Action clickEvent;
            internal LinkParseController.<ParseString>c__Iterator0 <>f__ref$0;

            internal void <>m__0()
            {
                this.clickEvent();
            }
        }
    }

    public class LinkItemMark : MonoBehaviour
    {
    }
}

