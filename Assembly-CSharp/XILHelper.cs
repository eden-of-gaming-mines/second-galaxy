﻿using Assets.XIL;
using BlackJack.BJFramework.Runtime.Hotfix;
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

public static class XILHelper
{
    public static TDel CallBaseMethod<T, TDel>(this T obj, string methodName, ParamType[] types = null) where TDel: class => 
        HotfixManager.CallBaseMethod<T, TDel>(obj, methodName, types);

    public static MethodInfo GetMethod(this System.Type type, string methodName, ParamType[] types = null) => 
        HotfixManager.GetMethod(type, methodName, types);
}

