﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, AddComponentMenu("Image Effects/Color Adjustments/Contrast Enhance (Unsharp Mask)"), RequireComponent(typeof(Camera))]
    public class ContrastEnhance : PostEffectsBase
    {
        [Range(0f, 1f)]
        public float intensity;
        [Range(0f, 0.999f)]
        public float threshold;
        private Material separableBlurMaterial;
        private Material contrastCompositeMaterial;
        [Range(0f, 1f)]
        public float blurSpread;
        public Shader separableBlurShader;
        public Shader contrastCompositeShader;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

