﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Bloom and Glow/BloomAndFlares (3.5, Deprecated)"), RequireComponent(typeof(Camera)), ExecuteInEditMode]
    public class BloomAndFlares : PostEffectsBase
    {
        public TweakMode34 tweakMode;
        public UnityStandardAssets.ImageEffects.BloomScreenBlendMode screenBlendMode;
        public UnityStandardAssets.ImageEffects.HDRBloomMode hdr;
        private bool doHdr;
        public float sepBlurSpread;
        public float useSrcAlphaAsMask;
        public float bloomIntensity;
        public float bloomThreshold;
        public int bloomBlurIterations;
        public bool lensflares;
        public int hollywoodFlareBlurIterations;
        public LensflareStyle34 lensflareMode;
        public float hollyStretchWidth;
        public float lensflareIntensity;
        public float lensflareThreshold;
        public Color flareColorA;
        public Color flareColorB;
        public Color flareColorC;
        public Color flareColorD;
        public Texture2D lensFlareVignetteMask;
        public Shader lensFlareShader;
        private Material lensFlareMaterial;
        public Shader vignetteShader;
        private Material vignetteMaterial;
        public Shader separableBlurShader;
        private Material separableBlurMaterial;
        public Shader addBrightStuffOneOneShader;
        private Material addBrightStuffBlendOneOneMaterial;
        public Shader screenBlendShader;
        private Material screenBlend;
        public Shader hollywoodFlaresShader;
        private Material hollywoodFlaresMaterial;
        public Shader brightPassFilterShader;
        private Material brightPassFilterMaterial;

        [MethodImpl(0x8000)]
        private void AddTo(float intensity_, RenderTexture from, RenderTexture to)
        {
        }

        [MethodImpl(0x8000)]
        private void BlendFlares(RenderTexture from, RenderTexture to)
        {
        }

        [MethodImpl(0x8000)]
        private void BrightFilter(float thresh, float useAlphaAsMask, RenderTexture from, RenderTexture to)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void Vignette(float amount, RenderTexture from, RenderTexture to)
        {
        }
    }
}

