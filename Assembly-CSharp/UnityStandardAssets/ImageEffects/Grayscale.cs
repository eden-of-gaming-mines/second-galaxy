﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Color Adjustments/Grayscale"), ExecuteInEditMode]
    public class Grayscale : ImageEffectBase
    {
        public Texture textureRamp;
        [Range(-1f, 1f)]
        public float rampOffset;

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

