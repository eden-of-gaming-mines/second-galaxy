﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), ExecuteInEditMode, AddComponentMenu("Image Effects/Edge Detection/Edge Detection")]
    public class EdgeDetection : PostEffectsBase
    {
        public EdgeDetectMode mode;
        public float sensitivityDepth;
        public float sensitivityNormals;
        public float lumThreshold;
        public float edgeExp;
        public float sampleDist;
        public float edgesOnly;
        public Color edgesOnlyBgColor;
        public Shader edgeDetectShader;
        private Material edgeDetectMaterial;
        private EdgeDetectMode oldMode;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        private void OnEnable()
        {
            this.SetCameraFlag();
        }

        [MethodImpl(0x8000), ImageEffectOpaque]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCameraFlag()
        {
        }

        private void Start()
        {
            this.oldMode = this.mode;
        }

        public enum EdgeDetectMode
        {
            TriangleDepthNormals,
            RobertsCrossDepthNormals,
            SobelDepth,
            SobelDepthThin,
            TriangleLuminance
        }
    }
}

