﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Blur/Blur (Optimized)"), RequireComponent(typeof(Camera)), ExecuteInEditMode]
    public class BlurOptimized : PostEffectsBase
    {
        [Range(0f, 2f)]
        public int downsample;
        [Range(0f, 10f)]
        public float blurSize;
        [Range(1f, 4f)]
        public int blurIterations;
        public BlurType blurType;
        public Shader blurShader;
        private Material blurMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        public void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        public void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        public enum BlurType
        {
            StandardGauss,
            SgxGauss
        }
    }
}

