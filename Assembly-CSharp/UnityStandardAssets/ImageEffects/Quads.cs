﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class Quads
    {
        private static Mesh[] meshes;
        private static int currentQuads;

        [MethodImpl(0x8000)]
        public static void Cleanup()
        {
        }

        [MethodImpl(0x8000)]
        private static Mesh GetMesh(int triCount, int triOffset, int totalWidth, int totalHeight)
        {
        }

        [MethodImpl(0x8000)]
        public static Mesh[] GetMeshes(int totalWidth, int totalHeight)
        {
        }

        [MethodImpl(0x8000)]
        private static bool HasMeshes()
        {
        }
    }
}

