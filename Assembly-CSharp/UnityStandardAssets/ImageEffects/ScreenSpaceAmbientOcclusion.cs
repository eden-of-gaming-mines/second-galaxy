﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Rendering/Screen Space Ambient Occlusion"), ExecuteInEditMode, RequireComponent(typeof(Camera))]
    public class ScreenSpaceAmbientOcclusion : MonoBehaviour
    {
        [Range(0.05f, 1f)]
        public float m_Radius;
        public SSAOSamples m_SampleCount;
        [Range(0.5f, 4f)]
        public float m_OcclusionIntensity;
        [Range(0f, 4f)]
        public int m_Blur;
        [Range(1f, 6f)]
        public int m_Downsampling;
        [Range(0.2f, 2f)]
        public float m_OcclusionAttenuation;
        [Range(1E-05f, 0.5f)]
        public float m_MinZ;
        public Shader m_SSAOShader;
        private Material m_SSAOMaterial;
        public Texture2D m_RandomTexture;
        private bool m_Supported;

        [MethodImpl(0x8000)]
        private static Material CreateMaterial(Shader shader)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateMaterials()
        {
        }

        [MethodImpl(0x8000)]
        private static void DestroyMaterial(Material mat)
        {
        }

        private void OnDisable()
        {
            DestroyMaterial(this.m_SSAOMaterial);
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000), ImageEffectOpaque]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        public enum SSAOSamples
        {
            Low,
            Medium,
            High
        }
    }
}

