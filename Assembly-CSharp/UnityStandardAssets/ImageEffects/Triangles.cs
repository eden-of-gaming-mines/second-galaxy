﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class Triangles
    {
        private static Mesh[] meshes;
        private static int currentTris;

        [MethodImpl(0x8000)]
        private static void Cleanup()
        {
        }

        [MethodImpl(0x8000)]
        private static Mesh GetMesh(int triCount, int triOffset, int totalWidth, int totalHeight)
        {
        }

        [MethodImpl(0x8000)]
        private static Mesh[] GetMeshes(int totalWidth, int totalHeight)
        {
        }

        [MethodImpl(0x8000)]
        private static bool HasMeshes()
        {
        }
    }
}

