﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Camera/Depth of Field (Lens Blur, Scatter, DX11)"), ExecuteInEditMode]
    public class DepthOfField : PostEffectsBase
    {
        public bool visualizeFocus;
        public float focalLength;
        public float focalSize;
        public float aperture;
        public Transform focalTransform;
        public float maxBlurSize;
        public bool highResolution;
        public BlurType blurType;
        public BlurSampleCount blurSampleCount;
        public bool nearBlur;
        public float foregroundOverlap;
        public Shader dofHdrShader;
        private Material dofHdrMaterial;
        public Shader dx11BokehShader;
        private Material dx11bokehMaterial;
        public float dx11BokehThreshold;
        public float dx11SpawnHeuristic;
        public Texture2D dx11BokehTexture;
        public float dx11BokehScale;
        public float dx11BokehIntensity;
        private float focalDistance01;
        private ComputeBuffer cbDrawArgs;
        private ComputeBuffer cbPoints;
        private float internalBlurWidth;
        private Camera cachedCamera;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateComputeResources()
        {
        }

        [MethodImpl(0x8000)]
        private float FocalDistance01(float worldDist)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void ReleaseComputeResources()
        {
        }

        [MethodImpl(0x8000)]
        private void WriteCoc(RenderTexture fromTo, bool fgDilate)
        {
        }

        public enum BlurSampleCount
        {
            Low,
            Medium,
            High
        }

        public enum BlurType
        {
            DiscBlur,
            DX11
        }
    }
}

