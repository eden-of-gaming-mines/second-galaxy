﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Displacement/Twirl"), ExecuteInEditMode]
    public class Twirl : ImageEffectBase
    {
        public Vector2 radius;
        [Range(0f, 360f)]
        public float angle;
        public Vector2 center;

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

