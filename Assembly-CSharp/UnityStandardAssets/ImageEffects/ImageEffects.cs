﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("")]
    public class ImageEffects
    {
        [Obsolete("Use Graphics.Blit(source,dest) instead")]
        public static void Blit(RenderTexture source, RenderTexture dest)
        {
            Graphics.Blit(source, dest);
        }

        [Obsolete("Use Graphics.Blit(source, destination, material) instead")]
        public static void BlitWithMaterial(Material material, RenderTexture source, RenderTexture dest)
        {
            Graphics.Blit(source, dest, material);
        }

        [MethodImpl(0x8000)]
        public static void RenderDistortion(Material material, RenderTexture source, RenderTexture destination, float angle, Vector2 center, Vector2 radius)
        {
        }
    }
}

