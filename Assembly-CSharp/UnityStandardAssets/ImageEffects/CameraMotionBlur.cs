﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Camera/Camera Motion Blur")]
    public class CameraMotionBlur : PostEffectsBase
    {
        private static float MAX_RADIUS = 10f;
        public MotionBlurFilter filterType;
        public bool preview;
        public Vector3 previewScale;
        public float movementScale;
        public float rotationScale;
        public float maxVelocity;
        public float minVelocity;
        public float velocityScale;
        public float softZDistance;
        public int velocityDownsample;
        public LayerMask excludeLayers;
        private GameObject tmpCam;
        public Shader shader;
        public Shader dx11MotionBlurShader;
        public Shader replacementClear;
        private Material motionBlurMaterial;
        private Material dx11MotionBlurMaterial;
        public Texture2D noiseTexture;
        public float jitter;
        public bool showVelocity;
        public float showVelocityScale;
        private Matrix4x4 currentViewProjMat;
        private Matrix4x4[] currentStereoViewProjMat;
        private Matrix4x4 prevViewProjMat;
        private Matrix4x4[] prevStereoViewProjMat;
        private int prevFrameCount;
        private bool wasActive;
        private Vector3 prevFrameForward;
        private Vector3 prevFrameUp;
        private Vector3 prevFramePos;
        private Camera _camera;

        [MethodImpl(0x8000)]
        private void CalculateViewProjection()
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        private static int divRoundUp(int x, int d) => 
            (((x + d) - 1) / d);

        [MethodImpl(0x8000)]
        private Camera GetTmpCam()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void Remember()
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        private void StartFrame()
        {
        }

        public enum MotionBlurFilter
        {
            CameraMotion,
            LocalBlur,
            Reconstruction,
            ReconstructionDX11,
            ReconstructionDisc
        }
    }
}

