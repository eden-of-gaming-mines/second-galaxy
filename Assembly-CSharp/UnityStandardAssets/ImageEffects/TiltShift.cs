﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Camera/Tilt Shift (Lens Blur)"), RequireComponent(typeof(Camera))]
    internal class TiltShift : PostEffectsBase
    {
        public TiltShiftMode mode;
        public TiltShiftQuality quality;
        [Range(0f, 15f)]
        public float blurArea;
        [Range(0f, 25f)]
        public float maxBlurSize;
        [Range(0f, 1f)]
        public int downsample;
        public Shader tiltShiftShader;
        private Material tiltShiftMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        public enum TiltShiftMode
        {
            TiltShiftMode,
            IrisMode
        }

        public enum TiltShiftQuality
        {
            Preview,
            Normal,
            High
        }
    }
}

