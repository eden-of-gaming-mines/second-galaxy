﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), ExecuteInEditMode]
    public class PostEffectsBase : MonoBehaviour
    {
        protected bool supportHDRTextures;
        protected bool supportDX11;
        protected bool isSupported;
        private List<Material> createdMaterials;

        [MethodImpl(0x8000)]
        public virtual bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShader(Shader s)
        {
        }

        [MethodImpl(0x8000)]
        protected Material CheckShaderAndCreateMaterial(Shader s, Material m2Create)
        {
        }

        protected bool CheckSupport() => 
            this.CheckSupport(false);

        [MethodImpl(0x8000)]
        protected bool CheckSupport(bool needDepth)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckSupport(bool needDepth, bool needHdr)
        {
        }

        [MethodImpl(0x8000)]
        protected Material CreateMaterial(Shader s, Material m2Create)
        {
        }

        [MethodImpl(0x8000)]
        protected void DrawBorder(RenderTexture dest, Material material)
        {
        }

        public bool Dx11Support() => 
            this.supportDX11;

        protected void NotSupported()
        {
            base.enabled = false;
            this.isSupported = false;
        }

        private void OnDestroy()
        {
            this.RemoveCreatedMaterials();
        }

        private void OnEnable()
        {
            this.isSupported = true;
        }

        [MethodImpl(0x8000)]
        private void RemoveCreatedMaterials()
        {
        }

        [MethodImpl(0x8000)]
        protected void ReportAutoDisable()
        {
        }

        protected void Start()
        {
            this.CheckResources();
        }
    }
}

