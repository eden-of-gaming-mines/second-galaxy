﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, AddComponentMenu("Image Effects/Rendering/Global Fog"), RequireComponent(typeof(Camera))]
    internal class GlobalFog : PostEffectsBase
    {
        [Tooltip("Apply distance-based fog?")]
        public bool distanceFog;
        [Tooltip("Exclude far plane pixels from distance-based fog? (Skybox or clear color)")]
        public bool excludeFarPixels;
        [Tooltip("Distance fog is based on radial distance from camera when checked")]
        public bool useRadialDistance;
        [Tooltip("Apply height-based fog?")]
        public bool heightFog;
        [Tooltip("Fog top Y coordinate")]
        public float height;
        [Range(0.001f, 10f)]
        public float heightDensity;
        [Tooltip("Push fog away from the camera by this amount")]
        public float startDistance;
        public Shader fogShader;
        private Material fogMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private static void CustomGraphicsBlit(RenderTexture source, RenderTexture dest, Material fxMaterial, int passNr)
        {
        }

        [MethodImpl(0x8000), ImageEffectOpaque]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

