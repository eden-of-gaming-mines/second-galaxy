﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), ExecuteInEditMode]
    internal class PostEffectsHelper : MonoBehaviour
    {
        [MethodImpl(0x8000)]
        private static void DrawBorder(RenderTexture dest, Material material)
        {
        }

        [MethodImpl(0x8000)]
        private static void DrawLowLevelPlaneAlignedWithCamera(float dist, RenderTexture source, RenderTexture dest, Material material, Camera cameraForProjectionMatrix)
        {
        }

        [MethodImpl(0x8000)]
        private static void DrawLowLevelQuad(float x1, float x2, float y1, float y2, RenderTexture source, RenderTexture dest, Material material)
        {
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Debug.Log("OnRenderImage in Helper called ...");
        }
    }
}

