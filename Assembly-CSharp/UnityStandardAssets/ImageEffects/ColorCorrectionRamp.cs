﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Color Adjustments/Color Correction (Ramp)"), ExecuteInEditMode]
    public class ColorCorrectionRamp : ImageEffectBase
    {
        public Texture textureRamp;

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

