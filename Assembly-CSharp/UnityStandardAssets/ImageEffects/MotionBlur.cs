﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, AddComponentMenu("Image Effects/Blur/Motion Blur (Color Accumulation)"), RequireComponent(typeof(Camera))]
    public class MotionBlur : ImageEffectBase
    {
        [Range(0f, 0.92f)]
        public float blurAmount;
        public bool extraBlur;
        private RenderTexture accumTexture;

        [MethodImpl(0x8000)]
        protected override void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

