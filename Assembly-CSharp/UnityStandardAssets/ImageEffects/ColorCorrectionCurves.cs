﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, AddComponentMenu("Image Effects/Color Adjustments/Color Correction (Curves, Saturation)")]
    public class ColorCorrectionCurves : PostEffectsBase
    {
        public AnimationCurve redChannel;
        public AnimationCurve greenChannel;
        public AnimationCurve blueChannel;
        public bool useDepthCorrection;
        public AnimationCurve zCurve;
        public AnimationCurve depthRedChannel;
        public AnimationCurve depthGreenChannel;
        public AnimationCurve depthBlueChannel;
        private Material ccMaterial;
        private Material ccDepthMaterial;
        private Material selectiveCcMaterial;
        private Texture2D rgbChannelTex;
        private Texture2D rgbDepthChannelTex;
        private Texture2D zCurveTex;
        public float saturation;
        public bool selectiveCc;
        public Color selectiveFromColor;
        public Color selectiveToColor;
        public ColorCorrectionMode mode;
        public bool updateTextures;
        public Shader colorCorrectionCurvesShader;
        public Shader simpleColorCorrectionCurvesShader;
        public Shader colorCorrectionSelectiveShader;
        private bool updateTexturesOnStartup;

        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        private void Start()
        {
            base.Start();
            this.updateTexturesOnStartup = true;
        }

        [MethodImpl(0x8000)]
        public void UpdateParameters()
        {
        }

        private void UpdateTextures()
        {
            this.UpdateParameters();
        }

        public enum ColorCorrectionMode
        {
            Simple,
            Advanced
        }
    }
}

