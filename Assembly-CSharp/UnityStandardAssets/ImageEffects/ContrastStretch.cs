﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, AddComponentMenu("Image Effects/Color Adjustments/Contrast Stretch")]
    public class ContrastStretch : MonoBehaviour
    {
        [Range(0.0001f, 1f)]
        public float adaptationSpeed;
        [Range(0f, 1f)]
        public float limitMinimum;
        [Range(0f, 1f)]
        public float limitMaximum;
        private RenderTexture[] adaptRenderTex;
        private int curAdaptIndex;
        public Shader shaderLum;
        private Material m_materialLum;
        public Shader shaderReduce;
        private Material m_materialReduce;
        public Shader shaderAdapt;
        private Material m_materialAdapt;
        public Shader shaderApply;
        private Material m_materialApply;

        [MethodImpl(0x8000)]
        private void CalculateAdaptation(Texture curTexture)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        protected Material materialLum
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected Material materialReduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected Material materialAdapt
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected Material materialApply
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

