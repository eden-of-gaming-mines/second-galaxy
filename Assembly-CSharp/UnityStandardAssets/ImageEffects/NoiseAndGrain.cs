﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Noise/Noise And Grain (Filmic)"), ExecuteInEditMode]
    public class NoiseAndGrain : PostEffectsBase
    {
        public float intensityMultiplier;
        public float generalIntensity;
        public float blackIntensity;
        public float whiteIntensity;
        public float midGrey;
        public bool dx11Grain;
        public float softness;
        public bool monochrome;
        public Vector3 intensities;
        public Vector3 tiling;
        public float monochromeTiling;
        public FilterMode filterMode;
        public Texture2D noiseTexture;
        public Shader noiseShader;
        private Material noiseMaterial;
        public Shader dx11NoiseShader;
        private Material dx11NoiseMaterial;
        private static float TILE_AMOUNT = 64f;
        private Mesh mesh;

        private void Awake()
        {
            this.mesh = new Mesh();
        }

        [MethodImpl(0x8000)]
        private static void BuildMesh(Mesh mesh, RenderTexture source, Texture2D noise)
        {
        }

        [MethodImpl(0x8000)]
        private static void BuildMeshUV0(Mesh mesh, int width, int height, float noiseSize, int noiseWidth)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private static void DrawNoiseQuadGrid(RenderTexture source, RenderTexture dest, Material fxMaterial, Texture2D noise, Mesh mesh, int passNr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

