﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Rendering/Screen Space Ambient Obscurance"), ExecuteInEditMode, RequireComponent(typeof(Camera))]
    internal class ScreenSpaceAmbientObscurance : PostEffectsBase
    {
        [Range(0f, 3f)]
        public float intensity;
        [Range(0.1f, 3f)]
        public float radius;
        [Range(0f, 3f)]
        public int blurIterations;
        [Range(0f, 5f)]
        public float blurFilterDistance;
        [Range(0f, 1f)]
        public int downsample;
        public Texture2D rand;
        public Shader aoShader;
        private Material aoMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000), ImageEffectOpaque]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

