﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Edge Detection/Crease Shading"), ExecuteInEditMode]
    public class CreaseShading : PostEffectsBase
    {
        public float intensity;
        public int softness;
        public float spread;
        public Shader blurShader;
        private Material blurMaterial;
        public Shader depthFetchShader;
        private Material depthFetchMaterial;
        public Shader creaseApplyShader;
        private Material creaseApplyMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

