﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Blur/Blur"), ExecuteInEditMode]
    public class Blur : MonoBehaviour
    {
        [Range(0f, 10f)]
        public int iterations;
        [Range(0f, 1f)]
        public float blurSpread;
        public Shader blurShader;
        private static Material m_Material;

        [MethodImpl(0x8000)]
        private void DownSample4x(RenderTexture source, RenderTexture dest)
        {
        }

        [MethodImpl(0x8000)]
        public void FourTapCone(RenderTexture source, RenderTexture dest, int iteration)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        protected void Start()
        {
        }

        protected Material material
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

