﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Displacement/Fisheye"), ExecuteInEditMode, RequireComponent(typeof(Camera))]
    public class Fisheye : PostEffectsBase
    {
        [Range(0f, 1.5f)]
        public float strengthX;
        [Range(0f, 1.5f)]
        public float strengthY;
        public Shader fishEyeShader;
        private Material fisheyeMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }
    }
}

