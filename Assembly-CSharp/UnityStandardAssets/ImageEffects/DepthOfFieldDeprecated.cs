﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Camera/Depth of Field (deprecated)")]
    public class DepthOfFieldDeprecated : PostEffectsBase
    {
        private static int SMOOTH_DOWNSAMPLE_PASS;
        private static float BOKEH_EXTRA_BLUR;
        public Dof34QualitySetting quality;
        public DofResolution resolution;
        public bool simpleTweakMode;
        public float focalPoint;
        public float smoothness;
        public float focalZDistance;
        public float focalZStartCurve;
        public float focalZEndCurve;
        private float focalStartCurve;
        private float focalEndCurve;
        private float focalDistance01;
        public Transform objectFocus;
        public float focalSize;
        public DofBlurriness bluriness;
        public float maxBlurSpread;
        public float foregroundBlurExtrude;
        public Shader dofBlurShader;
        private Material dofBlurMaterial;
        public Shader dofShader;
        private Material dofMaterial;
        public bool visualize;
        public BokehDestination bokehDestination;
        private float widthOverHeight;
        private float oneOverBaseSize;
        public bool bokeh;
        public bool bokehSupport;
        public Shader bokehShader;
        public Texture2D bokehTexture;
        public float bokehScale;
        public float bokehIntensity;
        public float bokehThresholdContrast;
        public float bokehThresholdLuminance;
        public int bokehDownsample;
        private Material bokehMaterial;
        private Camera _camera;
        private RenderTexture foregroundTexture;
        private RenderTexture mediumRezWorkTexture;
        private RenderTexture finalDefocus;
        private RenderTexture lowRezWorkTexture;
        private RenderTexture bokehSource;
        private RenderTexture bokehSource2;

        [MethodImpl(0x8000)]
        private void AddBokeh(RenderTexture bokehInfo, RenderTexture tempTex, RenderTexture finalTarget)
        {
        }

        [MethodImpl(0x8000)]
        private void AllocateTextures(bool blurForeground, RenderTexture source, int divider, int lowTexDivider)
        {
        }

        [MethodImpl(0x8000)]
        private void Blur(RenderTexture from, RenderTexture to, DofBlurriness iterations, int blurPass, float spread)
        {
        }

        [MethodImpl(0x8000)]
        private void BlurFg(RenderTexture from, RenderTexture to, DofBlurriness iterations, int blurPass, float spread)
        {
        }

        [MethodImpl(0x8000)]
        private void BlurHex(RenderTexture from, RenderTexture to, int blurPass, float spread, RenderTexture tmp)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateMaterials()
        {
        }

        [MethodImpl(0x8000)]
        private void Downsample(RenderTexture from, RenderTexture to)
        {
        }

        [MethodImpl(0x8000)]
        private float FocalDistance01(float worldDist)
        {
        }

        [MethodImpl(0x8000)]
        private int GetDividerBasedOnQuality()
        {
        }

        [MethodImpl(0x8000)]
        private int GetLowResolutionDividerBasedOnQuality(int baseDivider)
        {
        }

        private void OnDisable()
        {
            Quads.Cleanup();
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void ReleaseTextures()
        {
        }

        public enum BokehDestination
        {
            Background = 1,
            Foreground = 2,
            BackgroundAndForeground = 3
        }

        public enum Dof34QualitySetting
        {
            OnlyBackground = 1,
            BackgroundAndForeground = 2
        }

        public enum DofBlurriness
        {
            Low = 1,
            High = 2,
            VeryHigh = 4
        }

        public enum DofResolution
        {
            High = 2,
            Medium = 3,
            Low = 4
        }
    }
}

