﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Noise/Noise and Scratches"), RequireComponent(typeof(Camera)), ExecuteInEditMode]
    public class NoiseAndScratches : MonoBehaviour
    {
        public bool monochrome;
        private bool rgbFallback;
        [Range(0f, 5f)]
        public float grainIntensityMin;
        [Range(0f, 5f)]
        public float grainIntensityMax;
        [Range(0.1f, 50f)]
        public float grainSize;
        [Range(0f, 5f)]
        public float scratchIntensityMin;
        [Range(0f, 5f)]
        public float scratchIntensityMax;
        [Range(1f, 30f)]
        public float scratchFPS;
        [Range(0f, 1f)]
        public float scratchJitter;
        public Texture grainTexture;
        public Texture scratchTexture;
        public Shader shaderRGB;
        public Shader shaderYUV;
        private Material m_MaterialRGB;
        private Material m_MaterialYUV;
        private float scratchTimeLeft;
        private float scratchX;
        private float scratchY;

        [MethodImpl(0x8000)]
        protected void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        private void SanitizeParameters()
        {
        }

        [MethodImpl(0x8000)]
        protected void Start()
        {
        }

        protected Material material
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

