﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), ExecuteInEditMode, AddComponentMenu("Image Effects/Bloom and Glow/Bloom (Optimized)")]
    public class BloomOptimized : PostEffectsBase
    {
        [Range(0f, 1.5f)]
        public float threshold;
        [Range(0f, 2.5f)]
        public float intensity;
        [Range(0.25f, 5.5f)]
        public float blurSize;
        private Resolution resolution;
        [Range(1f, 4f)]
        public int blurIterations;
        public BlurType blurType;
        public Shader fastBloomShader;
        private Material fastBloomMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        public enum BlurType
        {
            Standard,
            Sgx
        }

        public enum Resolution
        {
            Low,
            High
        }
    }
}

