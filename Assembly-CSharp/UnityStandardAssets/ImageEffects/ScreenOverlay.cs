﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, AddComponentMenu("Image Effects/Other/Screen Overlay"), RequireComponent(typeof(Camera))]
    public class ScreenOverlay : PostEffectsBase
    {
        public OverlayBlendMode blendMode;
        public float intensity;
        public Texture2D texture;
        public Shader overlayShader;
        private Material overlayMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        public enum OverlayBlendMode
        {
            Additive,
            ScreenBlend,
            Multiply,
            Overlay,
            AlphaBlend
        }
    }
}

