﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, ImageEffectAllowedInSceneView, AddComponentMenu("Image Effects/Color Adjustments/Tonemapping"), RequireComponent(typeof(Camera))]
    public class Tonemapping : PostEffectsBase
    {
        public TonemapperType type;
        public AdaptiveTexSize adaptiveTextureSize;
        public AnimationCurve remapCurve;
        private Texture2D curveTex;
        public float exposureAdjustment;
        public float middleGrey;
        public float white;
        public float adaptionSpeed;
        public Shader tonemapper;
        public bool validRenderTextureFormat;
        private Material tonemapMaterial;
        private RenderTexture rt;
        private RenderTextureFormat rtFormat;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private bool CreateInternalRenderTexture()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000), ImageEffectTransformsToLDR]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        public float UpdateCurve()
        {
        }

        public enum AdaptiveTexSize
        {
            Square16 = 0x10,
            Square32 = 0x20,
            Square64 = 0x40,
            Square128 = 0x80,
            Square256 = 0x100,
            Square512 = 0x200,
            Square1024 = 0x400
        }

        public enum TonemapperType
        {
            SimpleReinhard,
            UserCurve,
            Hable,
            Photographic,
            OptimizedHejiDawson,
            AdaptiveReinhard,
            AdaptiveReinhardAutoWhite
        }
    }
}

