﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu(""), RequireComponent(typeof(Camera))]
    public class ImageEffectBase : MonoBehaviour
    {
        public Shader shader;
        private Material m_Material;

        [MethodImpl(0x8000)]
        protected virtual void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void Start()
        {
        }

        protected Material material
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

