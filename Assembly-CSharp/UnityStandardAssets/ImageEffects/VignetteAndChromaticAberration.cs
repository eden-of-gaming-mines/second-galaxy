﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode, RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Camera/Vignette and Chromatic Aberration")]
    public class VignetteAndChromaticAberration : PostEffectsBase
    {
        public AberrationMode mode;
        public float intensity;
        public float chromaticAberration;
        public float axialAberration;
        public float blur;
        public float blurSpread;
        public float luminanceDependency;
        public float blurDistance;
        public Shader vignetteShader;
        public Shader separableBlurShader;
        public Shader chromAberrationShader;
        private Material m_VignetteMaterial;
        private Material m_SeparableBlurMaterial;
        private Material m_ChromAberrationMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        public enum AberrationMode
        {
            Simple,
            Advanced
        }
    }
}

