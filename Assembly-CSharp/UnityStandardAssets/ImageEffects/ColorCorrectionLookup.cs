﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Color Adjustments/Color Correction (3D Lookup Texture)"), ExecuteInEditMode]
    public class ColorCorrectionLookup : PostEffectsBase
    {
        public Shader shader;
        private Material material;
        public Texture3D converted3DLut;
        public string basedOnTempTex;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        public void Convert(Texture2D temp2DTex, string path)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDestroy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIdentityLut()
        {
        }

        [MethodImpl(0x8000)]
        public bool ValidDimensions(Texture2D tex2d)
        {
        }
    }
}

