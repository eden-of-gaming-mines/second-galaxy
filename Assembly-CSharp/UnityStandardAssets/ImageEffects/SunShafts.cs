﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("Image Effects/Rendering/Sun Shafts"), RequireComponent(typeof(Camera)), ExecuteInEditMode]
    public class SunShafts : PostEffectsBase
    {
        public SunShaftsResolution resolution;
        public ShaftsScreenBlendMode screenBlendMode;
        public Transform sunTransform;
        public int radialBlurIterations;
        public Color sunColor;
        public Color sunThreshold;
        public float sunShaftBlurRadius;
        public float sunShaftIntensity;
        public float maxRadius;
        public bool useDepthTexture;
        public Shader sunShaftsShader;
        private Material sunShaftsMaterial;
        public Shader simpleClearShader;
        private Material simpleClearMaterial;

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        public enum ShaftsScreenBlendMode
        {
            Screen,
            Add
        }

        public enum SunShaftsResolution
        {
            Low,
            Normal,
            High
        }
    }
}

