﻿namespace UnityStandardAssets.ImageEffects
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(Camera)), AddComponentMenu("Image Effects/Bloom and Glow/Bloom"), ExecuteInEditMode]
    public class Bloom : PostEffectsBase
    {
        public TweakMode tweakMode;
        public BloomScreenBlendMode screenBlendMode;
        public HDRBloomMode hdr;
        private bool doHdr;
        public float sepBlurSpread;
        public BloomQuality quality;
        public float bloomIntensity;
        public float bloomThreshold;
        public Color bloomThresholdColor;
        public int bloomBlurIterations;
        public Shader screenBlendShader;
        private Material screenBlend;
        public Shader blurAndFlaresShader;
        private Material blurAndFlaresMaterial;
        public Shader brightPassFilterShader;
        private Material brightPassFilterMaterial;

        [MethodImpl(0x8000)]
        private void AddTo(float intensity_, RenderTexture from, RenderTexture to)
        {
        }

        [MethodImpl(0x8000)]
        private void BrightFilter(float thresh, RenderTexture from, RenderTexture to)
        {
        }

        [MethodImpl(0x8000)]
        private void BrightFilter(Color threshColor, RenderTexture from, RenderTexture to)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckResources()
        {
        }

        [MethodImpl(0x8000)]
        public void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
        }

        public enum BloomQuality
        {
            Cheap,
            High
        }

        public enum BloomScreenBlendMode
        {
            Screen,
            Add
        }

        public enum HDRBloomMode
        {
            Auto,
            On,
            Off
        }

        public enum TweakMode
        {
            Basic,
            Complex
        }
    }
}

