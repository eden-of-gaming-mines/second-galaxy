﻿using System;

public static class ResourcesPath
{
    public static readonly string dataPath;
    public static readonly string streamingAssetsPath;
    public const string PlatformKey = "StandaloneWindows";
    public static readonly string WritePath;
    public static readonly string LocalBasePath;
    public static readonly string LocalDataPath;
    public static readonly string LocalTempPath;
}

