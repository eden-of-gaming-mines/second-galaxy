﻿using BlackJack.BJFramework.Runtime.Log;
using System;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

public static class Debug
{
    public static Thread m_mainThread;

    [MethodImpl(0x8000)]
    public static void Assert(bool value, string str)
    {
    }

    [MethodImpl(0x8000)]
    public static void Assert(bool value, params object[] paramList)
    {
    }

    [MethodImpl(0x8000)]
    public static void Break()
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawLine(Vector3 start, Vector3 end)
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawLine(Vector3 start, Vector3 end, Color color)
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration, bool depthTest)
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawRay(Vector3 start, Vector3 dir)
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawRay(Vector3 start, Vector3 dir, Color color)
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
    {
    }

    [MethodImpl(0x8000)]
    public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration, bool depthTest)
    {
    }

    [MethodImpl(0x8000)]
    public static void Log(string str)
    {
        if ((m_mainThread == null) || (LogManager.Instance == null))
        {
            UnityEngine.Debug.Log(str);
        }
        else
        {
            if (LogManager.Instance.NeedFileLog)
            {
                LogManager.Instance.FileLogger.WriteLog(str, "DEBUG");
            }
            if (ReferenceEquals(Thread.CurrentThread, m_mainThread) && LogManager.Instance.NeedEngineLog)
            {
                UnityEngine.Debug.Log(str);
            }
        }
    }

    [MethodImpl(0x8000)]
    public static void Log(params object[] paramList)
    {
    }

    [MethodImpl(0x8000)]
    public static void LogError(string str)
    {
        if ((m_mainThread == null) || (LogManager.Instance == null))
        {
            UnityEngine.Debug.LogError(str);
        }
        else
        {
            if (LogManager.Instance.NeedFileLog)
            {
                LogManager.Instance.FileLogger.WriteLog(str, "ERROR");
            }
            if (ReferenceEquals(Thread.CurrentThread, m_mainThread) && LogManager.Instance.NeedEngineLog)
            {
                UnityEngine.Debug.LogError(str);
            }
        }
    }

    [MethodImpl(0x8000)]
    public static void LogError(params object[] paramList)
    {
        LogError(ParamListToString(paramList));
    }

    [MethodImpl(0x8000)]
    public static void LogWarning(string str)
    {
        if ((m_mainThread == null) || (LogManager.Instance == null))
        {
            UnityEngine.Debug.LogWarning(str);
        }
        else
        {
            if (LogManager.Instance.NeedFileLog)
            {
                LogManager.Instance.FileLogger.WriteLog(str, "WARNING");
            }
            if (ReferenceEquals(Thread.CurrentThread, m_mainThread) && LogManager.Instance.NeedEngineLog)
            {
                UnityEngine.Debug.LogWarning(str);
            }
        }
    }

    [MethodImpl(0x8000)]
    public static void LogWarning(params object[] paramList)
    {
        LogWarning(ParamListToString(paramList));
    }

    [MethodImpl(0x8000)]
    private static string ParamListToString(object[] paramList)
    {
    }

    public static void SystemLogException(params object[] paramList)
    {
        LogError(paramList);
    }

    public static void WriteLine(string str)
    {
        Log(str);
    }

    public static void WriteLine(params object[] paramList)
    {
        Log(paramList);
    }
}

