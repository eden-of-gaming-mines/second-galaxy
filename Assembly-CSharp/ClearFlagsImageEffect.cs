﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

[ExecuteInEditMode]
public class ClearFlagsImageEffect : MonoBehaviour
{
    private static Material m_Material;
    private RenderTexture _pastFrame;
    [Range(0f, 1f), SerializeField]
    private float _maxTransparency;

    [MethodImpl(0x8000)]
    protected void OnDisable()
    {
    }

    [MethodImpl(0x8000)]
    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
    }

    [MethodImpl(0x8000)]
    private void Start()
    {
    }

    protected Material material
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }
}

