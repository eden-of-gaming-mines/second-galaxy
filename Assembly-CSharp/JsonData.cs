﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

public class JsonData : IJsonWrapper, IEquatable<JsonData>, IList, IOrderedDictionary, ICollection, IEnumerable, IDictionary
{
    private IList<JsonData> inst_array;
    private bool inst_boolean;
    private double inst_double;
    private int inst_int;
    private long inst_long;
    private IDictionary<string, JsonData> inst_object;
    private string inst_string;
    private string json;
    private JsonType type;
    private IList<KeyValuePair<string, JsonData>> object_list;

    public JsonData()
    {
    }

    [MethodImpl(0x8000)]
    public JsonData(bool boolean)
    {
    }

    [MethodImpl(0x8000)]
    public JsonData(double number)
    {
    }

    [MethodImpl(0x8000)]
    public JsonData(int number)
    {
    }

    [MethodImpl(0x8000)]
    public JsonData(long number)
    {
    }

    [MethodImpl(0x8000)]
    public JsonData(object obj)
    {
    }

    [MethodImpl(0x8000)]
    public JsonData(string str)
    {
        this.type = JsonType.String;
        this.inst_string = str;
    }

    [MethodImpl(0x8000)]
    public int Add(object value)
    {
    }

    [MethodImpl(0x8000)]
    public void Clear()
    {
    }

    [MethodImpl(0x8000)]
    private ICollection EnsureCollection()
    {
    }

    [MethodImpl(0x8000)]
    private IDictionary EnsureDictionary()
    {
        if (this.type != JsonType.Object)
        {
            if (this.type != JsonType.None)
            {
                throw new InvalidOperationException("Instance of JsonData is not a dictionary");
            }
            this.type = JsonType.Object;
            this.inst_object = new Dictionary<string, JsonData>();
            this.object_list = new List<KeyValuePair<string, JsonData>>();
        }
        return (IDictionary) this.inst_object;
    }

    [MethodImpl(0x8000)]
    private IList EnsureList()
    {
    }

    [MethodImpl(0x8000)]
    public bool Equals(JsonData x)
    {
    }

    public JsonType GetJsonType() => 
        this.type;

    [MethodImpl(0x8000)]
    bool IJsonWrapper.GetBoolean()
    {
    }

    [MethodImpl(0x8000)]
    double IJsonWrapper.GetDouble()
    {
    }

    [MethodImpl(0x8000)]
    int IJsonWrapper.GetInt()
    {
    }

    [MethodImpl(0x8000)]
    long IJsonWrapper.GetLong()
    {
    }

    [MethodImpl(0x8000)]
    string IJsonWrapper.GetString()
    {
        if (this.type != JsonType.String)
        {
            throw new InvalidOperationException("JsonData instance doesn't hold a string");
        }
        return this.inst_string;
    }

    [MethodImpl(0x8000)]
    void IJsonWrapper.SetBoolean(bool val)
    {
        this.type = JsonType.Boolean;
        this.inst_boolean = val;
        this.json = null;
    }

    [MethodImpl(0x8000)]
    void IJsonWrapper.SetDouble(double val)
    {
    }

    [MethodImpl(0x8000)]
    void IJsonWrapper.SetInt(int val)
    {
        this.type = JsonType.Int;
        this.inst_int = val;
        this.json = null;
    }

    [MethodImpl(0x8000)]
    void IJsonWrapper.SetLong(long val)
    {
    }

    [MethodImpl(0x8000)]
    void IJsonWrapper.SetString(string val)
    {
        this.type = JsonType.String;
        this.inst_string = val;
        this.json = null;
    }

    string IJsonWrapper.ToJson() => 
        this.ToJson();

    void IJsonWrapper.ToJson(JsonWriter writer)
    {
        this.ToJson(writer);
    }

    [MethodImpl(0x8000)]
    public static explicit operator bool(JsonData data)
    {
        if (data.type != JsonType.Boolean)
        {
            throw new InvalidCastException("Instance of JsonData doesn't hold a double");
        }
        return data.inst_boolean;
    }

    [MethodImpl(0x8000)]
    public static explicit operator double(JsonData data)
    {
    }

    [MethodImpl(0x8000)]
    public static explicit operator int(JsonData data)
    {
        if (data.type != JsonType.Int)
        {
            throw new InvalidCastException("Instance of JsonData doesn't hold an int");
        }
        return data.inst_int;
    }

    [MethodImpl(0x8000)]
    public static explicit operator long(JsonData data)
    {
    }

    [MethodImpl(0x8000)]
    public static explicit operator string(JsonData data)
    {
        if (data.type != JsonType.String)
        {
            throw new InvalidCastException("Instance of JsonData doesn't hold a string");
        }
        return data.inst_string;
    }

    public static implicit operator JsonData(bool data) => 
        new JsonData(data);

    public static implicit operator JsonData(double data) => 
        new JsonData(data);

    public static implicit operator JsonData(int data) => 
        new JsonData(data);

    public static implicit operator JsonData(long data) => 
        new JsonData(data);

    public static implicit operator JsonData(string data) => 
        new JsonData(data);

    [MethodImpl(0x8000)]
    public void SetJsonType(JsonType type)
    {
        if (this.type != type)
        {
            switch (type)
            {
                case JsonType.Object:
                    this.inst_object = new Dictionary<string, JsonData>();
                    this.object_list = new List<KeyValuePair<string, JsonData>>();
                    break;

                case JsonType.Array:
                    this.inst_array = new List<JsonData>();
                    break;

                case JsonType.String:
                    this.inst_string = null;
                    break;

                case JsonType.Int:
                    this.inst_int = 0;
                    break;

                case JsonType.Long:
                    this.inst_long = 0L;
                    break;

                case JsonType.Double:
                    this.inst_double = 0.0;
                    break;

                case JsonType.Boolean:
                    this.inst_boolean = false;
                    break;

                default:
                    break;
            }
            this.type = type;
        }
    }

    void ICollection.CopyTo(Array array, int index)
    {
        this.EnsureCollection().CopyTo(array, index);
    }

    [MethodImpl(0x8000)]
    void IDictionary.Add(object key, object value)
    {
    }

    [MethodImpl(0x8000)]
    void IDictionary.Clear()
    {
    }

    bool IDictionary.Contains(object key) => 
        this.EnsureDictionary().Contains(key);

    IDictionaryEnumerator IDictionary.GetEnumerator() => 
        ((IOrderedDictionary) this).GetEnumerator();

    [MethodImpl(0x8000)]
    void IDictionary.Remove(object key)
    {
    }

    IEnumerator IEnumerable.GetEnumerator() => 
        this.EnsureCollection().GetEnumerator();

    int IList.Add(object value) => 
        this.Add(value);

    [MethodImpl(0x8000)]
    void IList.Clear()
    {
    }

    bool IList.Contains(object value) => 
        this.EnsureList().Contains(value);

    int IList.IndexOf(object value) => 
        this.EnsureList().IndexOf(value);

    [MethodImpl(0x8000)]
    void IList.Insert(int index, object value)
    {
    }

    [MethodImpl(0x8000)]
    void IList.Remove(object value)
    {
    }

    [MethodImpl(0x8000)]
    void IList.RemoveAt(int index)
    {
    }

    [MethodImpl(0x8000)]
    IDictionaryEnumerator IOrderedDictionary.GetEnumerator()
    {
        this.EnsureDictionary();
        return new OrderedDictionaryEnumerator(this.object_list.GetEnumerator());
    }

    [MethodImpl(0x8000)]
    void IOrderedDictionary.Insert(int idx, object key, object value)
    {
    }

    [MethodImpl(0x8000)]
    void IOrderedDictionary.RemoveAt(int idx)
    {
    }

    [MethodImpl(0x8000)]
    public string ToJson()
    {
        if (this.json == null)
        {
            StringWriter writer = new StringWriter();
            JsonWriter writer2 = new JsonWriter(writer) {
                Validate = false
            };
            WriteJson(this, writer2);
            this.json = writer.ToString();
        }
        return this.json;
    }

    [MethodImpl(0x8000)]
    public void ToJson(JsonWriter writer)
    {
    }

    [MethodImpl(0x8000)]
    private JsonData ToJsonData(object obj) => 
        ((obj != null) ? (!(obj is JsonData) ? new JsonData(obj) : ((JsonData) obj)) : null);

    [MethodImpl(0x8000)]
    public override string ToString()
    {
    }

    [MethodImpl(0x8000)]
    private static void WriteJson(IJsonWrapper obj, JsonWriter writer)
    {
        if (obj.IsString)
        {
            writer.Write(obj.GetString());
        }
        else if (obj.IsBoolean)
        {
            writer.Write(obj.GetBoolean());
        }
        else if (obj.IsDouble)
        {
            writer.Write(obj.GetDouble());
        }
        else if (obj.IsInt)
        {
            writer.Write(obj.GetInt());
        }
        else if (obj.IsLong)
        {
            writer.Write(obj.GetLong());
        }
        else if (obj.IsArray)
        {
            writer.WriteArrayStart();
            IEnumerator enumerator = obj.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    object current = enumerator.Current;
                    WriteJson((JsonData) current, writer);
                }
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
            writer.WriteArrayEnd();
        }
        else if (obj.IsObject)
        {
            writer.WriteObjectStart();
            IDictionaryEnumerator enumerator = obj.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    DictionaryEntry current = (DictionaryEntry) enumerator.Current;
                    writer.WritePropertyName((string) current.Key);
                    WriteJson((JsonData) current.Value, writer);
                }
            }
            finally
            {
                IDisposable disposable2 = enumerator as IDisposable;
                if (disposable2 != null)
                {
                    disposable2.Dispose();
                }
            }
            writer.WriteObjectEnd();
        }
    }

    int ICollection.Count =>
        this.Count;

    bool ICollection.IsSynchronized =>
        this.EnsureCollection().IsSynchronized;

    object ICollection.SyncRoot =>
        this.EnsureCollection().SyncRoot;

    bool IDictionary.IsFixedSize =>
        this.EnsureDictionary().IsFixedSize;

    bool IDictionary.IsReadOnly =>
        this.EnsureDictionary().IsReadOnly;

    ICollection IDictionary.Keys
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    ICollection IDictionary.Values
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    bool IJsonWrapper.IsArray =>
        this.IsArray;

    bool IJsonWrapper.IsBoolean =>
        this.IsBoolean;

    bool IJsonWrapper.IsDouble =>
        this.IsDouble;

    bool IJsonWrapper.IsInt =>
        this.IsInt;

    bool IJsonWrapper.IsLong =>
        this.IsLong;

    bool IJsonWrapper.IsObject =>
        this.IsObject;

    bool IJsonWrapper.IsString =>
        this.IsString;

    bool IList.IsFixedSize =>
        this.EnsureList().IsFixedSize;

    bool IList.IsReadOnly =>
        this.EnsureList().IsReadOnly;

    object IDictionary.this[object key]
    {
        get => 
            this.EnsureDictionary()[key];
        [MethodImpl(0x8000)]
        set
        {
            if (!(key is string))
            {
                throw new ArgumentException("The key has to be a string");
            }
            JsonData data = this.ToJsonData(value);
            this[(string) key] = data;
        }
    }

    object IOrderedDictionary.this[int idx]
    {
        [MethodImpl(0x8000)]
        get
        {
        }
        [MethodImpl(0x8000)]
        set
        {
        }
    }

    object IList.this[int index]
    {
        get => 
            this.EnsureList()[index];
        [MethodImpl(0x8000)]
        set
        {
        }
    }

    public int Count =>
        this.EnsureCollection().Count;

    public bool IsArray =>
        (this.type == JsonType.Array);

    public bool IsBoolean =>
        (this.type == JsonType.Boolean);

    public bool IsDouble =>
        (this.type == JsonType.Double);

    public bool IsInt =>
        (this.type == JsonType.Int);

    public bool IsLong =>
        (this.type == JsonType.Long);

    public bool IsObject =>
        (this.type == JsonType.Object);

    public bool IsString =>
        (this.type == JsonType.String);

    public JsonData this[string prop_name]
    {
        [MethodImpl(0x8000)]
        get
        {
            this.EnsureDictionary();
            return this.inst_object[prop_name];
        }
        [MethodImpl(0x8000)]
        set
        {
            this.EnsureDictionary();
            KeyValuePair<string, JsonData> item = new KeyValuePair<string, JsonData>(prop_name, value);
            if (!this.inst_object.ContainsKey(prop_name))
            {
                this.object_list.Add(item);
            }
            else
            {
                for (int i = 0; i < this.object_list.Count; i++)
                {
                    KeyValuePair<string, JsonData> pair2 = this.object_list[i];
                    if (pair2.Key == prop_name)
                    {
                        this.object_list[i] = item;
                        break;
                    }
                }
            }
            this.inst_object[prop_name] = value;
            this.json = null;
        }
    }

    public JsonData this[int index]
    {
        [MethodImpl(0x8000)]
        get
        {
        }
        [MethodImpl(0x8000)]
        set
        {
        }
    }
}

