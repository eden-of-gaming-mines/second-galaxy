﻿using Steamworks;
using System;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
public class SteamManager : MonoBehaviour
{
    private static SteamManager s_instance;
    private static bool s_EverInitialized;
    private bool m_bInitialized;
    private SteamAPIWarningMessageHook_t m_SteamAPIWarningMessageHook;
    protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;
    protected Callback<MicroTxnAuthorizationResponse_t> m_MicroTxnAuthorizationResponse;

    [MethodImpl(0x8000)]
    private void Awake()
    {
        if (s_instance != null)
        {
            Destroy(base.gameObject);
        }
        else
        {
            s_instance = this;
            if (s_EverInitialized)
            {
                throw new Exception("Tried to Initialize the SteamAPI twice in one session!");
            }
            DontDestroyOnLoad(base.gameObject);
            if (!Packsize.Test())
            {
                object[] paramList = new object[] { "[Steamworks.NET] Packsize Test returned false, the wrong version of Steamworks.NET is being run in this platform.", this };
                Debug.LogError(paramList);
            }
            if (!DllCheck.Test())
            {
                object[] paramList = new object[] { "[Steamworks.NET] DllCheck Test returned false, One or more of the Steamworks binaries seems to be the wrong version.", this };
                Debug.LogError(paramList);
            }
            try
            {
                if (SteamAPI.RestartAppIfNecessary((AppId_t) 0x110dfa))
                {
                    Application.Quit();
                }
                else
                {
                    this.m_bInitialized = SteamAPI.Init();
                    if (!this.m_bInitialized)
                    {
                        object[] paramList = new object[] { "[Steamworks.NET] SteamAPI_Init() failed. Refer to Valve's documentation or the comment above this line for more information.", this };
                        Debug.LogError(paramList);
                    }
                    else
                    {
                        s_EverInitialized = true;
                    }
                }
            }
            catch (DllNotFoundException exception)
            {
                object[] paramList = new object[] { "[Steamworks.NET] Could not load [lib]steam_api.dll/so/dylib. It's likely not in the correct location. Refer to the README for more details.\n" + exception, this };
                Debug.LogError(paramList);
                Application.Quit();
            }
        }
    }

    [MethodImpl(0x8000)]
    private void OnDestroy()
    {
    }

    [MethodImpl(0x8000)]
    private void OnEnable()
    {
        if (s_instance == null)
        {
            s_instance = this;
        }
        if (this.m_bInitialized)
        {
            if (this.m_SteamAPIWarningMessageHook == null)
            {
                this.m_SteamAPIWarningMessageHook = new SteamAPIWarningMessageHook_t(SteamManager.SteamAPIDebugTextHook);
                SteamClient.SetWarningMessageHook(this.m_SteamAPIWarningMessageHook);
            }
            if (Initialized)
            {
                Debug.Log("SteamManager Initialized");
                this.m_MicroTxnAuthorizationResponse = Callback<MicroTxnAuthorizationResponse_t>.Create(new Callback<MicroTxnAuthorizationResponse_t>.DispatchDelegate(this.OnMicroTxnAuthorizationResponse));
            }
        }
    }

    [MethodImpl(0x8000)]
    private void OnGameOverlayActivated(GameOverlayActivated_t pCallback)
    {
    }

    [MethodImpl(0x8000)]
    private void OnMicroTxnAuthorizationResponse(MicroTxnAuthorizationResponse_t pCallback)
    {
    }

    [MethodImpl(0x8000)]
    private static void SteamAPIDebugTextHook(int nSeverity, StringBuilder pchDebugText)
    {
        object[] paramList = new object[] { pchDebugText };
        Debug.LogWarning(paramList);
    }

    [MethodImpl(0x8000)]
    private void Update()
    {
        if (this.m_bInitialized)
        {
            SteamAPI.RunCallbacks();
        }
    }

    private static SteamManager Instance =>
        ((s_instance != null) ? s_instance : new GameObject("SteamManager").AddComponent<SteamManager>());

    public static bool Initialized =>
        Instance.m_bInitialized;
}

