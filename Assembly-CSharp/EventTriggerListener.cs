﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerListener : EventTrigger
{
    public VoidDelegate onClick;
    public VoidDelegate onDown;
    public VoidDelegate onUp;
    public VoidDelegate onEnter;
    public VoidDelegate onExit;
    public VoidDelegate onSelect;
    public VoidDelegate onDeselect;
    public VoidDelegate onUpdateSelect;
    public VoidDelegate onBeginDrag;
    public VoidDelegate onEndDrag;
    private HashSet<int> m_pressedTouchIdSet;
    private HashSet<int> m_breakedPressedTouchIdSet;
    private const int LeftMouseId = 0;

    [MethodImpl(0x8000)]
    private void Awake()
    {
    }

    [MethodImpl(0x8000)]
    private void CopyPressedTouchIdSetToBreakSet()
    {
    }

    [MethodImpl(0x8000)]
    public static EventTriggerListener Get(GameObject go)
    {
    }

    [MethodImpl(0x8000)]
    public static EventTriggerListener Get(Transform transform)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnBeginDrag(PointerEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnDeselect(BaseEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    private void OnDestroy()
    {
    }

    [MethodImpl(0x8000)]
    public override void OnEndDrag(PointerEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    private void OnEventSystemEnable(bool enable)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnPointerClick(PointerEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnPointerDown(PointerEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnPointerEnter(PointerEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnPointerExit(PointerEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnPointerUp(PointerEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnSelect(BaseEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    public override void OnUpdateSelected(BaseEventData eventData)
    {
    }

    [MethodImpl(0x8000)]
    private void Update()
    {
    }

    [MethodImpl(0x8000)]
    private void WatchInputEvent()
    {
    }

    [MethodImpl(0x8000)]
    private void WatchMouseInputEvent()
    {
    }

    [MethodImpl(0x8000)]
    private void WatchTouchInputEvent()
    {
    }

    public delegate void VoidDelegate(GameObject go);
}

