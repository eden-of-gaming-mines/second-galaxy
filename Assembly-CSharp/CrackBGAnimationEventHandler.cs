﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CrackBGAnimationEventHandler : MonoBehaviour
{
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action EventOnCrackBoxOpenBegin;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnPlatformMoveEnd;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action EventOnPlatformStartEnd;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnPlatformLodeEnd;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnCrackBoxEndStart;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private Action EventOnPlatformStartStart;
    [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
    private Action EventOnPlatformLoadStart;

    public event Action EventOnCrackBoxEndStart
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnCrackBoxOpenBegin
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnPlatformLoadStart
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnPlatformLodeEnd
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnPlatformMoveEnd
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnPlatformStartEnd
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    public event Action EventOnPlatformStartStart
    {
        [MethodImpl(0x8000)] add
        {
        }
        [MethodImpl(0x8000)] remove
        {
        }
    }

    [MethodImpl(0x8000)]
    public void OnCrackBoxEndStart()
    {
    }

    [MethodImpl(0x8000)]
    public void OnCrackBoxOpenBegin()
    {
    }

    [MethodImpl(0x8000)]
    public void OnPlatformLoadEnd()
    {
    }

    [MethodImpl(0x8000)]
    public void OnPlatformLoadStart()
    {
    }

    [MethodImpl(0x8000)]
    public void OnPlatformMoveEnd()
    {
    }

    [MethodImpl(0x8000)]
    public void OnPlatformStartEnd()
    {
    }

    [MethodImpl(0x8000)]
    public void OnPlatformStartStart()
    {
    }
}

