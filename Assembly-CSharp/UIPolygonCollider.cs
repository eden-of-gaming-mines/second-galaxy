﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PolygonCollider2D))]
public class UIPolygonCollider : Image
{
    private PolygonCollider2D m_polygon;

    protected UIPolygonCollider()
    {
        base.useLegacyMeshGeneration = true;
    }

    [MethodImpl(0x8000)]
    public override bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
    {
    }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();
    }

    private PolygonCollider2D Polygon
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }
}

