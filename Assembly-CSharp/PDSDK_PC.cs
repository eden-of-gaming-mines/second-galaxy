﻿using Ionic.Zip;
using Steamworks;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class PDSDK_PC
{
    private static int webViewEnable = 1;
    private static string gameInfo = string.Empty;
    private static JsonData commonPropertyJson;
    private static string userToken = string.Empty;
    private static string loginType = string.Empty;
    private static string deviceId;
    public static string testUserId = string.Empty;
    private static string _goodsName;
    private static int _goodsNumber;
    private static string _goodsId;
    private static string _goodsRegisterId;
    private static double _goodsPrice;
    private static string _customparams;
    private static string _goodsDes;
    private static string _orderId;
    private static string qqhAppkey = "1448278612076";
    private static string hxsyAppkey = "1480042163929";
    private static string _login_customparams = string.Empty;
    private static string _webviewAction = string.Empty;
    private static string SEPARATOR;
    private static string INNER_SEPARATOR;
    private static string mbi_version;
    private static string mbi_active;
    private static string mbi_gameevent;
    private static string mbi_login;
    private static string mbi_online;
    private static string mbi_start;
    private static string mbi_path;
    private static string mbi_zip_path;
    private static string CMBI_SDK_INIT_SUCCESS_ID;
    private static string CMBI_SDK_LOGIN_START_ID;
    private static string CMBI_SDK_LOGIN_SUCCESS_ID;
    private static long lastSentTicks;
    private static string config_path;
    private static string config_path_d;
    [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
    private static string <SDKLanguage>k__BackingField;
    private const string kSDKLanguage = "kSDKLanguage";
    private const string m_SDKDefaultLanguage = "en";
    private static List<string> m_EUCpuntryCodeList;
    [CompilerGenerated]
    private static Dictionary<string, int> <>f__switch$map7;

    [MethodImpl(0x8000)]
    static PDSDK_PC()
    {
        byte[] bytes = new byte[] { 1 };
        SEPARATOR = Encoding.ASCII.GetString(bytes);
        byte[] buffer2 = new byte[] { 2 };
        INNER_SEPARATOR = Encoding.ASCII.GetString(buffer2);
        mbi_version = "1.0";
        mbi_active = "active.log";
        mbi_gameevent = "gameevent.log";
        mbi_login = "login.log";
        mbi_online = "online.log";
        mbi_start = "start.log";
        mbi_path = Application.dataPath + "/mbi/";
        mbi_zip_path = Application.dataPath + "/mbi_zip/";
        CMBI_SDK_INIT_SUCCESS_ID = "4";
        CMBI_SDK_LOGIN_START_ID = "5";
        CMBI_SDK_LOGIN_SUCCESS_ID = "6";
        lastSentTicks = DateTime.Now.Ticks;
        config_path = Application.dataPath + "/StreamingAssets/commonProperty";
        config_path_d = Application.dataPath + "/StreamingAssets/commonPropertyD";
        List<string> list = new List<string> { 
            "AT",
            "BE",
            "BG",
            "CH",
            "CZ",
            "DE",
            "DK",
            "EE",
            "ES",
            "FI",
            "FR",
            "GB",
            "GR",
            "HR",
            "HU",
            "IE",
            "IT",
            "IS",
            "LT",
            "LV",
            "MC",
            "MD",
            "MT",
            "NL",
            "NO",
            "PL",
            "PT",
            "RO",
            "RU",
            "SE",
            "SK",
            "SM",
            "UA",
            "UK",
            "VA",
            "YU"
        };
        m_EUCpuntryCodeList = list;
    }

    [MethodImpl(0x8000)]
    public static void callCustomerServiceWeb()
    {
    }

    [MethodImpl(0x8000)]
    public static void CallWebView(string title, int fullscreen_flag, int title_flag, string action, string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public static void CheckPayBack()
    {
    }

    [MethodImpl(0x8000)]
    private static string decode(string src, byte key)
    {
    }

    [MethodImpl(0x8000)]
    private static string DefaultCallbackMessage(string code, string message)
    {
        JsonData data = new JsonData {
            ["code"] = code,
            ["message"] = message
        };
        return data.ToJson();
    }

    [MethodImpl(0x8000)]
    public static void DoOpenFBFanPage()
    {
    }

    [MethodImpl(0x8000)]
    public static void DoPay(string goodsName, int goodsNumber, string goodsId, string goodsRegisterId, double goodsPrice, string customparams, string goodsDes)
    {
    }

    [MethodImpl(0x8000)]
    public static string Encode_DES(string str, string key)
    {
    }

    [MethodImpl(0x8000)]
    public static string Encrypt_MD5(string strPwd)
    {
        MD5 md = new MD5CryptoServiceProvider();
        byte[] buffer2 = md.ComputeHash(Encoding.Default.GetBytes(strPwd));
        md.Clear();
        string str = string.Empty;
        for (int i = 0; i < buffer2.Length; i++)
        {
            str = str + buffer2[i].ToString("x").PadLeft(2, '0');
        }
        return str;
    }

    [MethodImpl(0x8000)]
    public static void EncryptCommonProperty()
    {
    }

    [MethodImpl(0x8000)]
    public static void Exit()
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_CREATORDER(string url)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_GOODSLIST(string url)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_NEWVERSION(string url) => 
        new <GET_NEWVERSION>c__Iterator6 { url = url };

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_NOTICECENTER(string url, string nodeId)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_PAYBACK(string url)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_STEAMLOGIN(string steamTicket, string customparams)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_SUPERME(string url, string data, string opcode, string channelid)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_VERIFYORDER(string url, string steamOrderId)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator GET_WXORDER(string url)
    {
    }

    [MethodImpl(0x8000)]
    public static string getCommonProperty(string pKey, string defaultValue = "")
    {
        if ((commonPropertyJson == null) || !((IDictionary) commonPropertyJson).Contains(pKey))
        {
            return defaultValue;
        }
        return (string) commonPropertyJson[pKey];
    }

    [MethodImpl(0x8000)]
    public static string GetDeviceID()
    {
        if (deviceId == null)
        {
            deviceId = "UNITYPC" + INNER_SEPARATOR + SystemInfo.deviceUniqueIdentifier;
        }
        return deviceId;
    }

    [MethodImpl(0x8000)]
    public static string GetECID()
    {
    }

    [MethodImpl(0x8000)]
    public static void GetProductsList()
    {
    }

    [MethodImpl(0x8000)]
    public static string GetSysCountry()
    {
    }

    [MethodImpl(0x8000)]
    public static string GetSysLangue()
    {
    }

    [MethodImpl(0x8000)]
    public static unsafe void Init()
    {
        byte[] bytes = null;
        try
        {
            bytes = File.ReadAllBytes(config_path_d);
        }
        catch (FileNotFoundException)
        {
            try
            {
                bytes = File.ReadAllBytes(config_path);
                for (int i = 0; i <= (bytes.Length - 1); i++)
                {
                    if ((i % 2) == 0)
                    {
                        byte* numPtr1 = (byte*) ref bytes[i];
                        numPtr1[0] = (byte) (numPtr1[0] ^ 100);
                    }
                    else
                    {
                        byte* numPtr2 = (byte*) ref bytes[i];
                        numPtr2[0] = (byte) (numPtr2[0] ^ 0x37);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                bytes = null;
            }
        }
        if (bytes != null)
        {
            commonPropertyJson = JsonMapper.ToObject(Encoding.UTF8.GetString(bytes));
            PrintActiveLog();
            PDSDK.DoCoroutine(GET_NEWVERSION(GetNewVersionUrl), null);
            JsonData data = new JsonData {
                ["state_code"] = "400",
                ["message"] = "pcinitsuccess",
                ["data"] = string.Empty
            };
            SDKLanguage = !PlayerPrefs.HasKey("kSDKLanguage") ? "en" : ((PlayerPrefs.GetString("kSDKLanguage").Length <= 0) ? "en" : PlayerPrefs.GetString("kSDKLanguage"));
            PrintGameEventLog(CMBI_SDK_INIT_SUCCESS_ID, string.Empty);
            PDSDK.Instance.onInitSuccess(data.ToJson());
        }
    }

    [MethodImpl(0x8000)]
    public static bool IsEuCountry()
    {
    }

    public static void Login()
    {
        Login(_login_customparams);
    }

    [MethodImpl(0x8000)]
    public static void Login(string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public static void Logout(string customparams)
    {
    }

    [MethodImpl(0x8000)]
    public static bool MakeLogZipAndSend()
    {
        try
        {
            lastSentTicks = DateTime.Now.Ticks;
            Directory.CreateDirectory(mbi_zip_path);
            if (Directory.Exists(mbi_path))
            {
                string str = DateTime.Now.ToString("yyyyMMddHHmm");
                string[] textArray1 = new string[] { "MBI001_", getCommonProperty("cmbi_app_key", string.Empty), "_", str, "_v1.4.zip" };
                string str2 = string.Concat(textArray1);
                string path = mbi_zip_path + str2;
                if (!File.Exists(path))
                {
                    ZipFile file = new ZipFile(path);
                    file.AddDirectory(mbi_path, string.Empty);
                    file.Save();
                    Directory.Delete(mbi_path, true);
                }
            }
        }
        catch (ArgumentException)
        {
            Debug.Log("MakeLogZipAndSend ArgumentException: ");
        }
        catch (IOException)
        {
            Debug.Log("MakeLogZipAndSend IOException: ");
        }
        catch (NullReferenceException)
        {
            Debug.Log("MakeLogZipAndSend NullReferenceException: ");
        }
        finally
        {
            Debug.Log("MakeLogZipAndSend finally: ");
        }
        SendZipFile();
        return true;
    }

    [MethodImpl(0x8000)]
    public static void NoticeCenterState(int mode, string noticeID)
    {
    }

    [MethodImpl(0x8000)]
    public static void onCallWebviewCancel()
    {
    }

    [MethodImpl(0x8000)]
    public static void onCallWebviewSuccess(string msg)
    {
    }

    public static void OnLangueChangeFailed(string Langue)
    {
    }

    public static void OnLangueChangeSuccess(string Langue)
    {
    }

    [MethodImpl(0x8000)]
    public static void onLoginCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void onLoginFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void onLoginSuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void onNoticeCenterStateFailed(string nodeId, string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void onNoticeCenterStateSuccess(string nodeId, string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void onPcPayCancel(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void onPcPayFailed(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void onPcPaySuccess(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void OpenWebBrowser(string url, int w, int h)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private static IEnumerator POST_SENDZIPFILE(string url, string zipFileNameRaw) => 
        new <POST_SENDZIPFILE>c__Iterator0 { 
            url = url,
            zipFileNameRaw = zipFileNameRaw
        };

    [MethodImpl(0x8000)]
    public static void PrintActiveLog()
    {
        Debug.Log("pc PrintActiveLog ");
        string content = (((((((((((string.Empty + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + SEPARATOR) + GetDeviceID() + SEPARATOR) + getCommonProperty("cmbi_channel_id", string.Empty) + SEPARATOR) + mbi_version + SEPARATOR) + "0" + SEPARATOR) + "0" + SEPARATOR) + "0" + SEPARATOR) + "800*600" + SEPARATOR) + "LAN" + SEPARATOR) + "0" + SEPARATOR) + "1001" + SEPARATOR) + mbi_version;
        PrintLineToFile(mbi_active, content);
        MakeLogZipAndSend();
    }

    [MethodImpl(0x8000)]
    public static void PrintGameEventLog(string eventID, string remark)
    {
        Debug.Log("pc PrintGameEventLog ");
        JsonData data = JsonMapper.ToObject(gameInfo);
        string testUserId = string.Empty;
        if (((IDictionary) data).Contains("GameUid"))
        {
            testUserId = (string) data["GameUid"];
            if (PDSDK_PC.testUserId != string.Empty)
            {
                testUserId = PDSDK_PC.testUserId;
            }
        }
        string str2 = string.Empty;
        if (((IDictionary) data).Contains("ServerId"))
        {
            str2 = (string) data["ServerId"];
        }
        string content = (((((((((((string.Empty + getCommonProperty("app_key", string.Empty) + SEPARATOR) + mbi_version + SEPARATOR) + "win" + SEPARATOR) + str2 + SEPARATOR) + getCommonProperty("cmbi_channel_id", string.Empty) + SEPARATOR) + GetDeviceID() + SEPARATOR) + testUserId + SEPARATOR) + "V" + mbi_version + SEPARATOR) + DateTime.Now.ToString("yyyyMMddHHmmss") + SEPARATOR) + eventID + SEPARATOR) + "winpc" + SEPARATOR) + remark;
        if (getCommonProperty("platform", "Zlongame") != "Zlongame")
        {
            content = content + SEPARATOR + "0";
        }
        PrintLineToFile(mbi_gameevent, content);
        if (DateTime.Now.Ticks > (lastSentTicks + 0x23c34600L))
        {
            MakeLogZipAndSend();
        }
    }

    [MethodImpl(0x8000)]
    public static void PrintLineToFile(string fileName, string content)
    {
        try
        {
            Debug.Log("pc PrintLineToFile : " + fileName + " " + content);
            Directory.CreateDirectory(mbi_path);
            FileInfo info = new FileInfo(mbi_path + fileName);
            StreamWriter writer = info.Exists ? info.AppendText() : info.CreateText();
            writer.WriteLine(content);
            writer.Close();
            writer.Dispose();
        }
        catch (ArgumentException)
        {
            Debug.Log("PrintLineToFile ArgumentException: ");
        }
        catch (IOException)
        {
            Debug.Log("PrintLineToFile IOException: ");
        }
        catch (NullReferenceException)
        {
            Debug.Log("PrintLineToFile NullReferenceException: ");
        }
        finally
        {
            Debug.Log("PrintLineToFile finally: ");
        }
    }

    [MethodImpl(0x8000)]
    public static void PrintLoginLog()
    {
    }

    [MethodImpl(0x8000)]
    public static void PrintOnlineLog()
    {
    }

    [MethodImpl(0x8000)]
    public static void PrintStartLog()
    {
    }

    [MethodImpl(0x8000)]
    public static void SendZipFile()
    {
        try
        {
            int num = 3;
            foreach (string str in Directory.GetFileSystemEntries(mbi_zip_path))
            {
                if (num <= 0)
                {
                    break;
                }
                if (File.Exists(str) && (str.IndexOf("MBI001") >= 0))
                {
                    num--;
                    PDSDK.DoCoroutine(POST_SENDZIPFILE(getCommonProperty("platformBIBaseUrl", string.Empty) + "/pd-bi-log-service/UploadFileServlet", str.Substring(str.LastIndexOf("/") + 1, (str.Length - str.LastIndexOf("/")) - 1)), null);
                }
            }
        }
        catch (ArgumentException)
        {
            Debug.Log("SendZipFile ArgumentException: ");
        }
        catch (IOException)
        {
            Debug.Log("SendZipFile IOException: ");
        }
        catch (NullReferenceException)
        {
            Debug.Log("SendZipFile NullReferenceException: ");
        }
        finally
        {
            Debug.Log("SendZipFile finally: ");
        }
    }

    [MethodImpl(0x8000)]
    public static void setCommonProperty(string pKey, string pValue)
    {
    }

    [MethodImpl(0x8000)]
    public static void SetSDKLanguage(string languageCode, Action<string> callback = null)
    {
        SDKLanguage = languageCode;
        PlayerPrefs.SetString("kSDKLanguage", SDKLanguage);
        PlayerPrefs.Save();
        if (callback != null)
        {
            callback(DefaultCallbackMessage("1", "切换语言成功"));
        }
    }

    [MethodImpl(0x8000)]
    public static void StartGame(string gameparams)
    {
    }

    [MethodImpl(0x8000)]
    public static void UserCenter()
    {
    }

    [MethodImpl(0x8000)]
    public static string UserLoginType()
    {
    }

    [MethodImpl(0x8000)]
    public static void VerifyOrder(string steamOrderId)
    {
    }

    [MethodImpl(0x8000)]
    public static void verifyToken(string msg)
    {
    }

    [MethodImpl(0x8000)]
    public static void WebInvestigation()
    {
    }

    public static string SDKLanguage
    {
        [CompilerGenerated]
        get => 
            <SDKLanguage>k__BackingField;
        [CompilerGenerated]
        private set => 
            (<SDKLanguage>k__BackingField = value);
    }

    private static string Platform_account_url
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string LoginUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string UsercenterUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string AccountOpenLoginUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string Platform_webview_url
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string Platform_pay_forbidden_url
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string Platform_pay_payback_url
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string GetProductsListUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string BillingCreateOrderUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string BillingVerifyOrderUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string BillingPayBackUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string OrderPayUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string OrderPayGamebeansUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string OrderPayGlobalUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string GetNewVersionUrl
    {
        [MethodImpl(0x8000)]
        get
        {
            string str = string.Empty;
            if (getCommonProperty("app_key", string.Empty) != qqhAppkey)
            {
                str = "/" + getCommonProperty("app_key", string.Empty);
            }
            string str2 = "/sdk-service" + str + "/version/getNewVersion.json";
            return ((getCommonProperty("debug_mode", "0") != "1") ? (getCommonProperty("platform_billing_url", string.Empty) + str2) : (getCommonProperty("debug_billing_url", string.Empty) + str2));
        }
    }

    private static string WxCreateOrderUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private static string WxQrcodeUrl
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    [CompilerGenerated]
    private sealed class <GET_CREATORDER>c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal JsonData <json>__0;
        internal string <userId>__0;
        internal string <roleId>__0;
        internal string <roleName>__0;
        internal string <pushInfo>__0;
        internal string <serverId>__0;
        internal string <serverName>__0;
        internal JsonData <createOrderJson>__0;
        internal Dictionary<string, string> <myhear>__0;
        internal string <str_params>__0;
        internal byte[] <post_data>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<json>__0 = JsonMapper.ToObject(PDSDK_PC.gameInfo);
                    this.<userId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("GameUid"))
                    {
                        this.<userId>__0 = (string) this.<json>__0["GameUid"];
                        if (PDSDK_PC.testUserId != string.Empty)
                        {
                            this.<userId>__0 = PDSDK_PC.testUserId;
                        }
                    }
                    this.<roleId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("RoleId"))
                    {
                        this.<roleId>__0 = (string) this.<json>__0["RoleId"];
                    }
                    this.<roleName>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("GameName"))
                    {
                        this.<roleName>__0 = (string) this.<json>__0["GameName"];
                    }
                    this.<pushInfo>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("PushInfo"))
                    {
                        this.<pushInfo>__0 = (string) this.<json>__0["PushInfo"];
                    }
                    this.<serverId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("ServerId"))
                    {
                        this.<serverId>__0 = (string) this.<json>__0["ServerId"];
                    }
                    this.<serverName>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("ServerName"))
                    {
                        this.<serverName>__0 = (string) this.<json>__0["ServerName"];
                    }
                    this.<createOrderJson>__0 = new JsonData();
                    this.<createOrderJson>__0["account_id"] = this.<userId>__0;
                    this.<createOrderJson>__0["user_id"] = this.<userId>__0;
                    this.<createOrderJson>__0["goods_id"] = PDSDK_PC._goodsId;
                    this.<createOrderJson>__0["goods_register_id"] = PDSDK_PC._goodsRegisterId;
                    this.<createOrderJson>__0["goods_number"] = PDSDK_PC._goodsNumber.ToString();
                    this.<createOrderJson>__0["goods_price"] = PDSDK_PC._goodsPrice.ToString();
                    this.<createOrderJson>__0["push_info"] = this.<pushInfo>__0;
                    this.<createOrderJson>__0["game_channel"] = PDSDK_PC.getCommonProperty("cmbi_app_key", string.Empty);
                    this.<createOrderJson>__0["customparams"] = PDSDK_PC._customparams;
                    this.<createOrderJson>__0["role_id"] = this.<roleId>__0;
                    this.<createOrderJson>__0["group_id"] = this.<serverId>__0;
                    this.<createOrderJson>__0["role_name"] = this.<roleName>__0;
                    this.<createOrderJson>__0["group_name"] = this.<serverName>__0;
                    if (PDSDK_PC.getCommonProperty("pc_store", string.Empty) == "steam")
                    {
                        JsonData data = new JsonData {
                            ["steamId"] = SteamUser.GetSteamID().ToString(),
                            ["language"] = SteamApps.GetCurrentGameLanguage(),
                            ["country"] = SteamUtils.GetIPCountry()
                        };
                        this.<createOrderJson>__0["ext"] = data;
                    }
                    this.<myhear>__0 = new Dictionary<string, string>();
                    this.<myhear>__0.Add("Content-Type", "application/json;charset=utf-8");
                    this.<myhear>__0.Add("debug", PDSDK_PC.getCommonProperty("debug_mode", "0"));
                    this.<myhear>__0.Add("tag", "123456");
                    this.<myhear>__0.Add("app_key", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<myhear>__0.Add("media_channel_id", PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty));
                    this.<myhear>__0.Add("channel_id", PDSDK_PC.getCommonProperty("channel_id", string.Empty));
                    if (PDSDK_PC.getCommonProperty("channel_id", string.Empty) == "1000000002")
                    {
                        this.<myhear>__0.Add("ecid", PDSDK_PC.getCommonProperty("ecid", string.Empty));
                    }
                    this.<myhear>__0.Add("clientVersion", PDSDK_PC.getCommonProperty("sdk_version_name", "1.0"));
                    this.<myhear>__0.Add("sign", "b694fd55b8107149");
                    this.<str_params>__0 = this.<createOrderJson>__0.ToJson();
                    this.<post_data>__0 = Encoding.UTF8.GetBytes(this.<str_params>__0);
                    this.<www>__0 = new WWW(this.url, this.<post_data>__0, this.<myhear>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                        JsonData data2 = new JsonData {
                            ["message"] = "payFailed-createorder"
                        };
                        PDSDK.Instance.onGetPayHistoryFailed(data2.ToJson());
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        JsonData data3 = JsonMapper.ToObject(this.<www>__0.text);
                        PDSDK_PC._orderId = (string) data3["order_id"];
                        string str = (string) data3["orderSign"];
                        string str2 = string.Empty;
                        if (((IDictionary) data3).Contains("orderSignString"))
                        {
                            str2 = (string) data3["orderSignString"];
                        }
                        string[] textArray1 = new string[] { this.<userId>__0, "|", PDSDK_PC._orderId, "|", PDSDK_PC.getCommonProperty("channel_id", string.Empty) };
                        if (!str.Equals(PDSDK_PC.Encrypt_MD5(PDSDK_PC.Encrypt_MD5(string.Concat(textArray1)).Substring(0, 10))))
                        {
                            JsonData data8 = new JsonData {
                                ["message"] = "payFailed-createorder-sign"
                            };
                            PDSDK.Instance.onGetPayHistoryFailed(data8.ToJson());
                        }
                        else if (PDSDK_PC.getCommonProperty("pc_store", string.Empty) != "steam")
                        {
                            if (PDSDK_PC.getCommonProperty("pc_store", string.Empty) == "global")
                            {
                                JsonData data4 = new JsonData();
                                DateTime time = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(0x7b2, 1, 1));
                                long totalMilliseconds = (long) (DateTime.Now - time).TotalMilliseconds;
                                data4["timestamp"] = totalMilliseconds.ToString();
                                data4["appKey"] = PDSDK_PC.getCommonProperty("app_key", string.Empty);
                                data4["productId"] = PDSDK_PC._goodsRegisterId;
                                data4["transactionId"] = PDSDK_PC._orderId;
                                data4["promotionId"] = PDSDK_PC.getCommonProperty("channel_id", string.Empty);
                                data4["goodsId"] = PDSDK_PC._goodsId;
                                data4["propsName"] = PDSDK_PC._goodsName;
                                data4["price"] = PDSDK_PC._goodsPrice;
                                data4["customerId"] = this.<userId>__0;
                                data4["sign"] = str2;
                                data4["roleId"] = this.<roleId>__0;
                                data4["serverId"] = this.<serverId>__0;
                                data4["isdebug"] = PDSDK_PC.getCommonProperty("debug_mode", "0") != "0";
                                data4["ecid"] = PDSDK_PC.getCommonProperty("ecid", string.Empty);
                                data4["lang"] = PDSDK_PC.SDKLanguage;
                                string s = data4.ToJson();
                                string str6 = string.Empty;
                                byte[] bytes = Encoding.UTF8.GetBytes(s);
                                try
                                {
                                    str6 = Convert.ToBase64String(bytes);
                                }
                                catch
                                {
                                    str6 = s;
                                }
                                Debug.Log("encode :" + str6);
                                PDSDK_PC.OpenWebBrowser((PDSDK_PC.OrderPayGlobalUrl + "?data=" + WWW.EscapeURL(str6)) + "&r=" + totalMilliseconds.ToString(), 0x3e8, 500);
                            }
                            else if (PDSDK_PC.getCommonProperty("platform", "Zlongame") == "GameBeans")
                            {
                                string s = new JsonData { 
                                    ["order_id"] = PDSDK_PC._orderId,
                                    ["currency"] = string.Empty,
                                    ["goods_register_id"] = PDSDK_PC._goodsRegisterId,
                                    ["goods_price"] = PDSDK_PC._goodsPrice,
                                    ["user_id"] = this.<userId>__0,
                                    ["role_id"] = this.<roleId>__0,
                                    ["role_name"] = this.<roleName>__0,
                                    ["group_id"] = this.<serverId>__0,
                                    ["goods_id"] = PDSDK_PC._goodsId,
                                    ["order_sign"] = str,
                                    ["order_sign_string"] = str2
                                }.ToJson();
                                string str10 = string.Empty;
                                byte[] bytes = Encoding.UTF8.GetBytes(s);
                                try
                                {
                                    str10 = Convert.ToBase64String(bytes);
                                }
                                catch
                                {
                                    str10 = s;
                                }
                                Debug.Log("encode :" + str10);
                                PDSDK_PC.OpenWebBrowser(PDSDK_PC.OrderPayGamebeansUrl + "?data=" + WWW.EscapeURL(str10), 0x1c8, 0x1de);
                            }
                            else if (PDSDK_PC.getCommonProperty("platform", "Zlongame") != "Zlongame")
                            {
                                JsonData data7 = new JsonData {
                                    ["message"] = "payFailed-createorder-sign"
                                };
                                PDSDK.Instance.onGetPayHistoryFailed(data7.ToJson());
                            }
                            else
                            {
                                JsonData data6 = new JsonData();
                                DateTime time2 = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(0x7b2, 1, 1));
                                long totalMilliseconds = (long) (DateTime.Now - time2).TotalMilliseconds;
                                data6["timestamp"] = totalMilliseconds.ToString();
                                data6["appKey"] = PDSDK_PC.getCommonProperty("app_key", string.Empty);
                                data6["transactionId"] = PDSDK_PC._orderId;
                                data6["channelId"] = PDSDK_PC.getCommonProperty("channel_id", string.Empty);
                                if (PDSDK_PC.getCommonProperty("channel_id", string.Empty) == "1000000002")
                                {
                                    data6["ecid"] = PDSDK_PC.getCommonProperty("ecid", string.Empty);
                                }
                                data6["subject"] = PDSDK_PC._goodsName;
                                data6["body"] = PDSDK_PC._goodsName;
                                data6["pd_device_id"] = PDSDK_PC.GetDeviceID();
                                data6["total_fee"] = PDSDK_PC._goodsPrice;
                                data6["userId"] = this.<userId>__0;
                                data6["roleId"] = this.<roleId>__0;
                                data6["serverId"] = this.<serverId>__0;
                                data6["goodsId"] = PDSDK_PC._goodsId;
                                string[] textArray2 = new string[9];
                                textArray2[0] = totalMilliseconds.ToString();
                                textArray2[1] = PDSDK_PC.getCommonProperty("app_key", string.Empty);
                                textArray2[2] = PDSDK_PC._orderId;
                                textArray2[3] = PDSDK_PC.getCommonProperty("channel_id", string.Empty);
                                textArray2[4] = this.<userId>__0;
                                textArray2[5] = this.<serverId>__0;
                                textArray2[6] = this.<roleId>__0;
                                textArray2[7] = PDSDK_PC._goodsId;
                                textArray2[8] = PDSDK_PC._goodsPrice.ToString("0.00");
                                string str14 = PDSDK_PC.Encrypt_MD5(string.Concat(textArray2));
                                data6["sign"] = str14;
                                string s = data6.ToJson();
                                string str16 = string.Empty;
                                byte[] bytes = Encoding.UTF8.GetBytes(s);
                                try
                                {
                                    str16 = Convert.ToBase64String(bytes);
                                }
                                catch
                                {
                                    str16 = s;
                                }
                                PDSDK_PC.OpenWebBrowser(PDSDK_PC.OrderPayUrl + "?data=" + WWW.EscapeURL(str16), 0x1c8, 0x1de);
                            }
                        }
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_GOODSLIST>c__Iterator5 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal Dictionary<string, string> <headers>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<headers>__0 = new Dictionary<string, string>();
                    this.<headers>__0.Add("debug", PDSDK_PC.getCommonProperty("debug_mode", "0"));
                    this.<headers>__0.Add("tag", "123456");
                    this.<headers>__0.Add("app_key", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<headers>__0.Add("media_channel_id", PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty));
                    this.<headers>__0.Add("channel_id", PDSDK_PC.getCommonProperty("channel_id", string.Empty));
                    if (PDSDK_PC.getCommonProperty("channel_id", string.Empty) == "1000000002")
                    {
                        this.<headers>__0.Add("ecid", PDSDK_PC.getCommonProperty("ecid", string.Empty));
                    }
                    this.<headers>__0.Add("clientVersion", PDSDK_PC.getCommonProperty("sdk_version_name", "1.0"));
                    if (PDSDK_PC.getCommonProperty("pc_store", string.Empty) != "steam")
                    {
                        this.<headers>__0.Add("sign", PDSDK_PC.Encrypt_MD5(PDSDK_PC.getCommonProperty("app_key", string.Empty)).Substring(8, 0x10));
                    }
                    else
                    {
                        this.url = this.url + "?steamId=";
                        this.url = this.url + SteamUser.GetSteamID().ToString();
                        this.<headers>__0.Add("sign", PDSDK_PC.Encrypt_MD5(PDSDK_PC.getCommonProperty("app_key", string.Empty) + "steamId=" + SteamUser.GetSteamID().ToString()).Substring(8, 0x10));
                    }
                    this.<www>__0 = new WWW(this.url, null, this.<headers>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                        JsonData data = new JsonData {
                            ["message"] = "GetProductsListFailed"
                        };
                        PDSDK.Instance.onGetProductsListFailed(data.ToJson());
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        string msg = new JsonData { 
                            ["state_code"] = "800",
                            ["message"] = string.Empty,
                            ["data"] = this.<www>__0.text
                        }.ToJson();
                        PDSDK.Instance.onGetProductsListSuccess(msg);
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_NEWVERSION>c__Iterator6 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal Dictionary<string, string> <headers>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<headers>__0 = new Dictionary<string, string>();
                    this.<headers>__0.Add("debug", PDSDK_PC.getCommonProperty("debug_mode", "0"));
                    this.<headers>__0.Add("tag", "123456");
                    this.<headers>__0.Add("app_key", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<headers>__0.Add("media_channel_id", PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty));
                    this.<headers>__0.Add("channel_id", PDSDK_PC.getCommonProperty("channel_id", string.Empty));
                    if (PDSDK_PC.getCommonProperty("channel_id", string.Empty) == "1000000002")
                    {
                        this.<headers>__0.Add("ecid", PDSDK_PC.getCommonProperty("ecid", string.Empty));
                    }
                    this.<headers>__0.Add("clientVersion", PDSDK_PC.getCommonProperty("sdk_version_name", "1.0"));
                    this.<headers>__0.Add("sign", PDSDK_PC.Encrypt_MD5(PDSDK_PC.getCommonProperty("app_key", string.Empty) + "app_version=" + PDSDK_PC.getCommonProperty("version_name", "1.0.0")).Substring(8, 0x10));
                    this.<www>__0 = new WWW(this.url + "?app_version=" + PDSDK_PC.getCommonProperty("version_name", "1.0.0"), null, this.<headers>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        JsonData data = JsonMapper.ToObject(this.<www>__0.text);
                        if (((int) data["update_level"]) >= 1)
                        {
                            PDSDK_PC.OpenWebBrowser((string) data["download_url"], 0x470, 640);
                            PDSDK_PC.webViewEnable = 0;
                        }
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_NOTICECENTER>c__Iterator7 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal JsonData <json>__0;
        internal string <userId>__0;
        internal string <roleId>__0;
        internal string <roleLevel>__0;
        internal string <serverId>__0;
        internal int <vipLevel>__0;
        internal Dictionary<string, string> <headers>__0;
        internal JsonData <notifyJson>__0;
        internal string nodeId;
        internal string <str_params>__0;
        internal byte[] <post_data>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<json>__0 = JsonMapper.ToObject(PDSDK_PC.gameInfo);
                    this.<userId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("GameUid"))
                    {
                        this.<userId>__0 = (string) this.<json>__0["GameUid"];
                        if (PDSDK_PC.testUserId != string.Empty)
                        {
                            this.<userId>__0 = PDSDK_PC.testUserId;
                        }
                    }
                    this.<roleId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("RoleId"))
                    {
                        this.<roleId>__0 = (string) this.<json>__0["RoleId"];
                    }
                    this.<roleLevel>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("RoleLevel"))
                    {
                        this.<roleLevel>__0 = (string) this.<json>__0["RoleLevel"];
                    }
                    this.<serverId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("ServerId"))
                    {
                        this.<serverId>__0 = (string) this.<json>__0["ServerId"];
                    }
                    this.<vipLevel>__0 = 0;
                    if (((IDictionary) this.<json>__0).Contains("VipLevel"))
                    {
                        this.<vipLevel>__0 = (int) this.<json>__0["VipLevel"];
                    }
                    this.<headers>__0 = new Dictionary<string, string>();
                    this.<headers>__0.Add("Content-Type", "application/json;charset=utf-8");
                    this.<headers>__0.Add("debug", PDSDK_PC.getCommonProperty("debug_mode", "0"));
                    this.<headers>__0.Add("tag", "123456");
                    this.<headers>__0.Add("app_key", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<headers>__0.Add("media_channel_id", PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty));
                    this.<headers>__0.Add("channel_id", PDSDK_PC.getCommonProperty("channel_id", string.Empty));
                    if (PDSDK_PC.getCommonProperty("channel_id", string.Empty) == "1000000002")
                    {
                        this.<headers>__0.Add("ecid", PDSDK_PC.getCommonProperty("ecid", string.Empty));
                    }
                    this.<headers>__0.Add("clientVersion", PDSDK_PC.getCommonProperty("sdk_version_name", "1.0"));
                    this.<headers>__0.Add("sign", PDSDK_PC.Encrypt_MD5(PDSDK_PC.getCommonProperty("app_key", string.Empty) + "app_version=" + PDSDK_PC.getCommonProperty("version_name", "1.0.0")).Substring(8, 0x10));
                    this.<notifyJson>__0 = new JsonData();
                    this.<notifyJson>__0["appKey"] = PDSDK_PC.getCommonProperty("app_key", string.Empty);
                    this.<notifyJson>__0["userId"] = this.<userId>__0;
                    this.<notifyJson>__0["roleId"] = this.<roleId>__0;
                    this.<notifyJson>__0["serverId"] = this.<serverId>__0;
                    this.<notifyJson>__0["vip"] = this.<vipLevel>__0;
                    this.<notifyJson>__0["level"] = this.<roleLevel>__0;
                    this.<notifyJson>__0["nodeId"] = this.nodeId;
                    this.<str_params>__0 = this.<notifyJson>__0.ToJson();
                    this.<post_data>__0 = Encoding.UTF8.GetBytes(this.<str_params>__0);
                    this.<www>__0 = new WWW(this.url, this.<post_data>__0, this.<headers>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                        PDSDK_PC.onNoticeCenterStateFailed(this.nodeId, string.Empty);
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        JsonData data = JsonMapper.ToObject(this.<www>__0.text);
                        string str = (string) data["msg"];
                        string msg = data["data"].ToJson();
                        if (((int) data["code"]) == 0)
                        {
                            PDSDK_PC.onNoticeCenterStateSuccess(this.nodeId, msg);
                        }
                        else
                        {
                            PDSDK_PC.onNoticeCenterStateFailed(this.nodeId, string.Empty);
                        }
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_PAYBACK>c__Iterator4 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal JsonData <json>__0;
        internal string <userId>__0;
        internal JsonData <payBackJson>__0;
        internal Dictionary<string, string> <myhear>__0;
        internal string <str_params>__0;
        internal byte[] <post_data>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<json>__0 = JsonMapper.ToObject(PDSDK_PC.gameInfo);
                    this.<userId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("GameUid"))
                    {
                        this.<userId>__0 = (string) this.<json>__0["GameUid"];
                        if (PDSDK_PC.testUserId != string.Empty)
                        {
                            this.<userId>__0 = PDSDK_PC.testUserId;
                        }
                    }
                    this.<payBackJson>__0 = new JsonData();
                    this.<payBackJson>__0["user_id"] = this.<userId>__0;
                    this.<myhear>__0 = new Dictionary<string, string>();
                    this.<myhear>__0.Add("Content-Type", "application/json;charset=utf-8");
                    this.<myhear>__0.Add("debug", PDSDK_PC.getCommonProperty("debug_mode", "0"));
                    this.<myhear>__0.Add("tag", "123456");
                    this.<myhear>__0.Add("app_key", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<myhear>__0.Add("media_channel_id", PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty));
                    this.<myhear>__0.Add("channel_id", PDSDK_PC.getCommonProperty("channel_id", string.Empty));
                    if (PDSDK_PC.getCommonProperty("channel_id", string.Empty) == "1000000002")
                    {
                        this.<myhear>__0.Add("ecid", PDSDK_PC.getCommonProperty("ecid", string.Empty));
                    }
                    this.<myhear>__0.Add("clientVersion", PDSDK_PC.getCommonProperty("sdk_version_name", "1.0"));
                    this.<myhear>__0.Add("sign", "b694fd55b8107149");
                    this.<myhear>__0.Add("lang", PDSDK_PC.SDKLanguage);
                    this.<str_params>__0 = this.<payBackJson>__0.ToJson();
                    this.<post_data>__0 = Encoding.UTF8.GetBytes(this.<str_params>__0);
                    this.<www>__0 = new WWW(this.url, this.<post_data>__0, this.<myhear>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        if (((int) JsonMapper.ToObject(this.<www>__0.text)["total"]) > 0)
                        {
                            PDSDK_PC.OpenWebBrowser(PDSDK_PC.Platform_pay_payback_url, 0x396, 0x200);
                        }
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_STEAMLOGIN>c__Iterator8 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal WWWForm <form>__0;
        internal string steamTicket;
        internal WWW <www>__0;
        internal string customparams;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(PDSDK_PC.AccountOpenLoginUrl);
                    this.<form>__0 = new WWWForm();
                    this.<form>__0.AddField("openname", "steam");
                    this.<form>__0.AddField("openID", this.steamTicket);
                    this.<form>__0.AddField("device", PDSDK_PC.GetDeviceID());
                    this.<form>__0.AddField("appkey", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<www>__0 = new WWW(PDSDK_PC.AccountOpenLoginUrl, this.<form>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        JsonData data = JsonMapper.ToObject(this.<www>__0.text);
                        if (int.Parse(data["status"].ToJson()) != 1)
                        {
                            PDSDK_PC.onLoginFailed("pcloginfailed");
                        }
                        else
                        {
                            string str = "10001";
                            string str2 = PDSDK_PC.getCommonProperty("channel_id", string.Empty);
                            PDSDK_PC.userToken = data["data"]["token"].ToString();
                            string str3 = data["data"]["userid"].ToString();
                            PDSDK_PC.loginType = "true_steam";
                            JsonData data2 = new JsonData {
                                ["userip"] = "127.0.0.1",
                                ["deviceid"] = PDSDK_PC.GetDeviceID(),
                                ["isdebug"] = PDSDK_PC.getCommonProperty("debug_mode", "0"),
                                ["channel_id"] = str2,
                                ["opcode"] = str,
                                ["token"] = PDSDK_PC.userToken,
                                ["oid"] = str3,
                                ["sdkversion"] = PDSDK_PC.getCommonProperty("sdk_version_name", "1.0"),
                                ["system"] = "UNITYPC",
                                ["gamechannel"] = str2,
                                ["app_key"] = PDSDK_PC.getCommonProperty("app_key", string.Empty)
                            };
                            if (str2 == "1000000002")
                            {
                                data2["ecid"] = PDSDK_PC.getCommonProperty("ecid", "0");
                            }
                            string str4 = PDSDK_PC.Encode_DES(data2.ToJson(), "cyou-mrd");
                            JsonData data3 = new JsonData {
                                ["validateInfo"] = str4
                            };
                            JsonData data4 = new JsonData {
                                ["state_code"] = "300",
                                ["message"] = "pcloginsuccess",
                                ["channel_id"] = str2,
                                ["opcode"] = str,
                                ["data"] = data3.ToJson(),
                                ["customparams"] = this.customparams,
                                ["operators"] = PDSDK_PC.getCommonProperty("ecid", "0") + "_"
                            };
                            PDSDK.Instance.onLoginSuccess(data4.ToJson());
                            PDSDK_PC.PrintGameEventLog(PDSDK_PC.CMBI_SDK_LOGIN_SUCCESS_ID, string.Empty);
                            PDSDK_PC.PrintStartLog();
                        }
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_SUPERME>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal WWWForm <form>__0;
        internal string data;
        internal string opcode;
        internal string channelid;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<form>__0 = new WWWForm();
                    this.<form>__0.AddField("data", this.data);
                    this.<form>__0.AddField("opcode", this.opcode);
                    this.<form>__0.AddField("channelid", this.channelid);
                    this.<www>__0 = new WWW(this.url, this.<form>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        PDSDK_PC.testUserId = JsonMapper.ToObject(this.<www>__0.text)["data"]["userid"].ToString();
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_VERIFYORDER>c__Iterator9 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal JsonData <json>__0;
        internal string <userId>__0;
        internal string <roleId>__0;
        internal string <roleName>__0;
        internal string <pushInfo>__0;
        internal string <serverId>__0;
        internal string <serverName>__0;
        internal JsonData <verifyOrderJson>__0;
        internal string steamOrderId;
        internal Dictionary<string, string> <myhear>__0;
        internal string <str_params>__0;
        internal byte[] <post_data>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<json>__0 = JsonMapper.ToObject(PDSDK_PC.gameInfo);
                    this.<userId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("GameUid"))
                    {
                        this.<userId>__0 = (string) this.<json>__0["GameUid"];
                        if (PDSDK_PC.testUserId != string.Empty)
                        {
                            this.<userId>__0 = PDSDK_PC.testUserId;
                        }
                    }
                    this.<roleId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("RoleId"))
                    {
                        this.<roleId>__0 = (string) this.<json>__0["RoleId"];
                    }
                    this.<roleName>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("GameName"))
                    {
                        this.<roleName>__0 = (string) this.<json>__0["GameName"];
                    }
                    this.<pushInfo>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("PushInfo"))
                    {
                        this.<pushInfo>__0 = (string) this.<json>__0["PushInfo"];
                    }
                    this.<serverId>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("ServerId"))
                    {
                        this.<serverId>__0 = (string) this.<json>__0["ServerId"];
                    }
                    this.<serverName>__0 = string.Empty;
                    if (((IDictionary) this.<json>__0).Contains("ServerName"))
                    {
                        this.<serverName>__0 = (string) this.<json>__0["ServerName"];
                    }
                    this.<verifyOrderJson>__0 = new JsonData();
                    this.<verifyOrderJson>__0["account_id"] = this.<userId>__0;
                    this.<verifyOrderJson>__0["user_id"] = this.<userId>__0;
                    this.<verifyOrderJson>__0["goods_register_id"] = PDSDK_PC._goodsRegisterId;
                    this.<verifyOrderJson>__0["push_info"] = this.<pushInfo>__0;
                    this.<verifyOrderJson>__0["customparams"] = PDSDK_PC._customparams;
                    this.<verifyOrderJson>__0["role_id"] = this.<roleId>__0;
                    this.<verifyOrderJson>__0["group_id"] = this.<serverId>__0;
                    this.<verifyOrderJson>__0["role_name"] = this.<roleName>__0;
                    this.<verifyOrderJson>__0["group_name"] = this.<serverName>__0;
                    this.<verifyOrderJson>__0["receipt"] = this.steamOrderId;
                    this.<myhear>__0 = new Dictionary<string, string>();
                    this.<myhear>__0.Add("Content-Type", "application/json;charset=utf-8");
                    this.<myhear>__0.Add("debug", PDSDK_PC.getCommonProperty("debug_mode", "0"));
                    this.<myhear>__0.Add("tag", "123456");
                    this.<myhear>__0.Add("app_key", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<myhear>__0.Add("media_channel_id", PDSDK_PC.getCommonProperty("cmbi_channel_id", string.Empty));
                    this.<myhear>__0.Add("channel_id", PDSDK_PC.getCommonProperty("channel_id", string.Empty));
                    if (PDSDK_PC.getCommonProperty("channel_id", string.Empty) == "1000000002")
                    {
                        this.<myhear>__0.Add("ecid", PDSDK_PC.getCommonProperty("ecid", string.Empty));
                    }
                    this.<myhear>__0.Add("clientVersion", PDSDK_PC.getCommonProperty("sdk_version_name", "1.0"));
                    this.<myhear>__0.Add("sign", "b694fd55b8107149");
                    this.<str_params>__0 = this.<verifyOrderJson>__0.ToJson();
                    Debug.Log(this.<str_params>__0);
                    this.<post_data>__0 = Encoding.UTF8.GetBytes(this.<str_params>__0);
                    this.<www>__0 = new WWW(this.url, this.<post_data>__0, this.<myhear>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                        PDSDK_PC.onPcPayFailed(string.Empty);
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        PDSDK_PC.onPcPaySuccess(string.Empty);
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <GET_WXORDER>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal WWWForm <form>__0;
        internal int <priceFeng>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url);
                    this.<form>__0 = new WWWForm();
                    this.<form>__0.AddField("app_id", PDSDK_PC.getCommonProperty("app_key", string.Empty));
                    this.<form>__0.AddField("subject", PDSDK_PC._goodsName);
                    this.<form>__0.AddField("body", PDSDK_PC._goodsName);
                    this.<priceFeng>__0 = (int) (PDSDK_PC._goodsPrice * 100.0);
                    this.<form>__0.AddField("total_fee", this.<priceFeng>__0.ToString());
                    this.<form>__0.AddField("pd_device_id", PDSDK_PC.GetDeviceID());
                    this.<form>__0.AddField("extra", PDSDK_PC._orderId);
                    this.<www>__0 = new WWW(this.url, this.<form>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                        JsonData data = new JsonData {
                            ["message"] = "payFailed-wxorder"
                        };
                        PDSDK.Instance.onGetPayHistoryFailed(data.ToJson());
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        JsonData data2 = JsonMapper.ToObject(this.<www>__0.text);
                        int num2 = (int) data2["status"];
                        JsonData data3 = data2["data"];
                        if (num2 == 1)
                        {
                            PDSDK_PC.OpenWebBrowser((((PDSDK_PC.WxQrcodeUrl + "&orderId=" + ((string) data3["orderId"])) + "&subject=" + WWW.EscapeURL((string) data3["subject"])) + "&totalFee=" + ((string) data3["totalFee"])) + "&codeUrl=" + WWW.EscapeURL((string) data3["codeUrl"]), 0x1c8, 0x26f);
                        }
                        else
                        {
                            JsonData data4 = new JsonData {
                                ["message"] = "payFailed-wxorder-status " + num2.ToString()
                            };
                            PDSDK.Instance.onGetPayHistoryFailed(data4.ToJson());
                        }
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <POST_SENDZIPFILE>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string url;
        internal string zipFileNameRaw;
        internal string <zipFileName>__0;
        internal string <LINEND>__0;
        internal string <PREFIX>__0;
        internal string <BOUNDARY>__0;
        internal WWWForm <form>__0;
        internal FileStream <fs>__0;
        internal int <len>__0;
        internal byte[] <b>__0;
        internal WWW <www>__0;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        [MethodImpl(0x8000)]
        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    Debug.Log(this.url + "   " + this.zipFileNameRaw);
                    this.<zipFileName>__0 = PDSDK_PC.mbi_zip_path + this.zipFileNameRaw;
                    this.<LINEND>__0 = "\r\n";
                    this.<PREFIX>__0 = "--";
                    this.<BOUNDARY>__0 = "A52F733B0E51D9A568FABABBC8D3A518";
                    this.<form>__0 = new WWWForm();
                    this.<form>__0.AddField("Content-Type", "multipart/form-data;boundary=" + this.<BOUNDARY>__0);
                    this.<form>__0.AddField("connection", "keep-alive");
                    this.<form>__0.AddField("Charsert", "UTF-8");
                    this.<form>__0.AddField("project", "MBI001");
                    this.<fs>__0 = File.OpenRead(this.<zipFileName>__0);
                    this.<len>__0 = (int) this.<fs>__0.Length;
                    this.<b>__0 = new byte[this.<len>__0];
                    this.<fs>__0.Read(this.<b>__0, 0, this.<len>__0);
                    this.<fs>__0.Close();
                    this.<form>__0.AddBinaryData("file", this.<b>__0, this.zipFileNameRaw);
                    this.<www>__0 = new WWW(this.url, this.<form>__0);
                    this.$current = this.<www>__0;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;

                case 1:
                    if (this.<www>__0.error != null)
                    {
                        Debug.Log("error is :" + this.<www>__0.error);
                    }
                    else
                    {
                        Debug.Log("request ok : " + this.<www>__0.text);
                        if ((bool) JsonMapper.ToObject(this.<www>__0.text)["success"])
                        {
                            File.Delete(this.<zipFileName>__0);
                        }
                    }
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
            return false;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }
}

