﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;

    [Serializable]
    public class TransformTweenBehaviour : PlayableBehaviour
    {
        public Transform m_startLocation;
        public Transform m_endLocation;
        public bool m_tweenPosition;
        public bool m_tweenRotation;
        public bool m_tweenScale;
        public TweenType m_tweenType;
        public float m_customStartingSpeed;
        public float m_customEndingSpeed;
        public float m_inverseDuration;
        public Vector3 m_startingPosition;
        public Quaternion m_startingRotation;
        public Vector3 m_startingScale;
        public AnimationCurve m_currentCurve;
        private readonly AnimationCurve m_linearCurve;
        private readonly AnimationCurve m_decelerationCurve;
        private readonly AnimationCurve m_harmonicCurve;
        private AnimationCurve m_customCurve;
        private const float KRightAngleInRads = 1.570796f;

        [MethodImpl(0x8000)]
        public override void OnGraphStart(Playable playable)
        {
        }

        public enum TweenType
        {
            Linear,
            Deceleration,
            Harmonic,
            Custom
        }
    }
}

