﻿namespace UnityEngine.UI
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;

    [AddComponentMenu("UI/TextExpand", 10)]
    public class TextExpand : Text, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler, IEventSystemHandler
    {
        [TextArea(3, 10), SerializeField]
        protected string m_orignalText;
        [SerializeField]
        protected readonly List<LinkInfo> m_LinkInfos;
        protected static readonly StringBuilder s_TextBuilder;
        [SerializeField]
        private LinkClickEvent m_OnLinkClick;
        [SerializeField]
        private MeshPopulateEvent m_OnMeshPopulateEnd;
        private static readonly Regex s_LinkRegex;
        private static readonly Regex s_LinkRegexDetail;

        [MethodImpl(0x8000)]
        protected override void Awake()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetOutputText(string outputText)
        {
        }

        [MethodImpl(0x8000)]
        public string GetViewText(string outputText)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnPointerClick(PointerEventData eventData)
        {
        }

        public void OnPointerDown(PointerEventData eventData)
        {
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPopulateMesh(VertexHelper toFill)
        {
        }

        public override void SetVerticesDirty()
        {
            base.SetVerticesDirty();
            this.UpdateOutputText();
        }

        [MethodImpl(0x8000)]
        protected void UpdateOutputText()
        {
        }

        public override string text
        {
            get => 
                base.m_Text;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public LinkClickEvent onLinkClick
        {
            get => 
                this.m_OnLinkClick;
            set => 
                (this.m_OnLinkClick = value);
        }

        public MeshPopulateEvent onMeshPopulateEnd
        {
            get => 
                this.m_OnMeshPopulateEnd;
            set => 
                (this.m_OnMeshPopulateEnd = value);
        }

        [Serializable]
        public class LinkClickEvent : UnityEvent<string>
        {
        }

        public class LinkInfo : UnityEngine.Object
        {
            public int startIndex = -1;
            public int endIndex = -1;
            public string type = string.Empty;
            public bool isCareClick = true;
            public Color bgImageColor = new Color(0f, 0f, 0f, 0f);
            public readonly List<Rect> boxes = new List<Rect>();
        }

        [Serializable]
        public class MeshPopulateEvent : UnityEvent<List<TextExpand.LinkInfo>>
        {
        }
    }
}

