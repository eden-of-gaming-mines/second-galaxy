﻿namespace UnityEngine.UI
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("UI/BackgroupPrint", 11)]
    public class BackgroupPrint : MonoBehaviour
    {
        private bool m_isMeshDataChanged;
        private List<TextExpand.LinkInfo> m_linkInfoList;
        private Dictionary<Rect, Color> m_boxToColorDict;
        private List<GameObject> m_bgImageList;
        private GameObject m_bgImageItem;
        private TextExpand m_attachText;
        [SerializeField, Header("下划线:")]
        private bool m_underLine;

        [MethodImpl(0x8000)]
        private GameObject CreateBgImageItem()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDestroy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMeshChanged(List<Rect> rectList)
        {
        }

        public void OnMeshPopulateEnd(List<TextExpand.LinkInfo> linkInfos)
        {
            this.m_isMeshDataChanged = true;
            this.m_linkInfoList = linkInfos;
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        private void Update()
        {
            this.UpdateBgImage();
        }

        [MethodImpl(0x8000)]
        protected void UpdateBgImage()
        {
        }

        public TextExpand attachText
        {
            get => 
                this.m_attachText;
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool underLine
        {
            get => 
                this.m_underLine;
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

