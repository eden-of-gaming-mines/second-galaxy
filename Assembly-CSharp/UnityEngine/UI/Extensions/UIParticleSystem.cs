﻿namespace UnityEngine.UI.Extensions
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(CanvasRenderer)), ExecuteInEditMode, RequireComponent(typeof(ParticleSystem))]
    public class UIParticleSystem : MaskableGraphic
    {
        public Texture particleTexture;
        public Sprite particleSprite;
        private Transform _transform;
        private ParticleSystem _particleSystem;
        private ParticleSystem.Particle[] _particles;
        private UIVertex[] _quad;
        private Vector4 _uv;
        private ParticleSystem.TextureSheetAnimationModule _textureSheetAnimation;
        private int _textureSheetAnimationFrames;
        private Vector2 _textureSheedAnimationFrameSize;

        [MethodImpl(0x8000)]
        protected override void Awake()
        {
        }

        [MethodImpl(0x8000)]
        protected bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPopulateMesh(VertexHelper vh)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        public override Texture mainTexture
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

