﻿namespace UnityEngine
{
    using BlackJack.BJFramework.Runtime;
    using System;

    [Serializable]
    public class CriCueInfo4Timeline
    {
        [Header("废弃，使用m_cueInfos"), Obsolete("废弃，使用m_cueInfos")]
        public string m_cueName;
        public bool m_isLoop;
        public PlaySoundOption m_option;
    }
}

