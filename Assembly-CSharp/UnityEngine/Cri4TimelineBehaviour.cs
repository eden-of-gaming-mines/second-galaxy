﻿namespace UnityEngine
{
    using BlackJack.BJFramework.Runtime;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;

    [Serializable]
    public class Cri4TimelineBehaviour : PlayableBehaviour
    {
        [Header("废弃，使用m_cueInfos"), Obsolete("废弃，使用m_cueInfos")]
        public List<string> m_cueNames;
        public List<CriCueInfo4Timeline> m_cueInfos;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private CRIAudioSourceHelperImpl <AudioPlayer>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <EventTriggered>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private double <StartTime>k__BackingField;

        [MethodImpl(0x8000)]
        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
        }

        public override void OnGraphStart(Playable playable)
        {
            base.OnGraphStart(playable);
            this.EventTriggered = false;
        }

        public CRIAudioSourceHelperImpl AudioPlayer { get; set; }

        public bool EventTriggered { get; set; }

        public double StartTime { get; set; }
    }
}

