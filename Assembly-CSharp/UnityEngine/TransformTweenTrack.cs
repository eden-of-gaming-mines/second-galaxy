﻿namespace UnityEngine
{
    using System;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [TrackColor(0.855f, 0.8623f, 0.87f), TrackBindingType(typeof(Transform)), TrackClipType(typeof(TransformTweenClip))]
    public class TransformTweenTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount) => 
            ((Playable) ScriptPlayable<TransformTweenMixerBehaviour>.Create(graph, inputCount));

        public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
        {
            base.GatherProperties(director, driver);
        }
    }
}

