﻿namespace UnityEngine
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;

    public class CustomEventMixerBehaviour : PlayableBehaviour
    {
        private TimelineCustomEventHandler m_eventHandler;

        [MethodImpl(0x8000)]
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendMessage(List<string> events)
        {
        }
    }
}

