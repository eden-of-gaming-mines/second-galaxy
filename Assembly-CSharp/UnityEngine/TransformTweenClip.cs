﻿namespace UnityEngine
{
    using BlackJack.BJFramework.Runtime;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [Serializable]
    public class TransformTweenClip : PlayableAsset, ITimelineClipAsset
    {
        public TransformTweenBehaviour m_template;
        [TimelineAutoBind("")]
        public ExposedReference<Transform> m_startLocation;
        [TimelineAutoBind("")]
        public ExposedReference<Transform> m_endLocation;

        [MethodImpl(0x8000)]
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
        }

        public ClipCaps clipCaps =>
            ClipCaps.Blending;
    }
}

