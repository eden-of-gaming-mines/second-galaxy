﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [TrackColor(0.5808823f, 0.5808823f, 0.5808823f), TrackClipType(typeof(CustomEventClip)), TrackBindingType(typeof(TimelineCustomEventHandler))]
    public class CustomEventTrack : TrackAsset
    {
        [MethodImpl(0x8000)]
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
        }
    }
}

