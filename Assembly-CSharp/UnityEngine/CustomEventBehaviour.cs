﻿namespace UnityEngine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;

    [Serializable]
    public class CustomEventBehaviour : PlayableBehaviour
    {
        public List<string> m_events;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <EventTriggered>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private double <StartTime>k__BackingField;

        public override void OnGraphStart(Playable playable)
        {
            base.OnGraphStart(playable);
            this.EventTriggered = false;
        }

        public bool EventTriggered { get; set; }

        public double StartTime { get; set; }
    }
}

