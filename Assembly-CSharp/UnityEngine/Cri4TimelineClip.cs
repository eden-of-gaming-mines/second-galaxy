﻿namespace UnityEngine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [Serializable]
    public class Cri4TimelineClip : PlayableAsset, ITimelineClipAsset
    {
        public ExposedReference<CRIAudioSourceHelperImpl> m_audioPlayer;
        [HideInInspector]
        public Cri4TimelineBehaviour m_template;
        [Obsolete("废弃，请使用m_cueInfos"), Header("废弃，请使用m_cueInfos")]
        public List<string> m_cueNames;
        public List<CriCueInfo4Timeline> m_cueInfos;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private double <StartTime>k__BackingField;

        [MethodImpl(0x8000)]
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
        }

        public ClipCaps clipCaps =>
            ClipCaps.None;

        public double StartTime { get; set; }
    }
}

