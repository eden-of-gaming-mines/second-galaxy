﻿namespace UnityEngine
{
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;

    public class TransformTweenMixerBehaviour : PlayableBehaviour
    {
        private bool m_firstFrameHappened;

        [MethodImpl(0x8000)]
        private static Quaternion AddQuaternions(Quaternion first, Quaternion second)
        {
        }

        [MethodImpl(0x8000)]
        private static Quaternion NormalizeQuaternion(Quaternion rotation)
        {
        }

        [MethodImpl(0x8000)]
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
        }

        private static float QuaternionMagnitude(Quaternion rotation) => 
            Mathf.Sqrt(Quaternion.Dot(rotation, rotation));

        [MethodImpl(0x8000)]
        private static Quaternion ScaleQuaternion(Quaternion rotation, float multiplier)
        {
        }
    }
}

