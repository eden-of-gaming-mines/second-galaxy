﻿namespace UnityEngine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [Serializable]
    public class CustomEventClip : PlayableAsset, ITimelineClipAsset
    {
        [HideInInspector]
        public CustomEventBehaviour m_template;
        public List<string> m_events;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private double <StartTime>k__BackingField;

        [MethodImpl(0x8000)]
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
        }

        public ClipCaps clipCaps =>
            ClipCaps.None;

        public double StartTime { get; set; }
    }
}

