﻿namespace UnityEngine
{
    using BlackJack.BJFramework.Runtime;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [TrackClipType(typeof(Cri4TimelineClip)), TrackColor(0.5808823f, 0.5808823f, 0.5808823f), TrackBindingType(typeof(CRIAudioSourceHelperImpl))]
    public class Cri4TimelineTrack : TrackAsset
    {
        [MethodImpl(0x8000)]
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
        }
    }
}

