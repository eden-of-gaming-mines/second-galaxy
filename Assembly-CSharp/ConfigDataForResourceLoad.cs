﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ConfigDataForResourceLoad : MonoBehaviour
{
    public List<ViewItemInfoForOption> ViewItemListForOption;

    [Serializable]
    public class AssetandBundleName
    {
        public string assetName;
        public string bundleName;
    }

    [Serializable]
    public class ViewItemInfoForOption
    {
        public string optionName;
        public List<ConfigDataForResourceLoad.AssetandBundleName> viewItemList;
    }
}

