﻿using System;
using System.Runtime.CompilerServices;
using ZenFulcrum.EmbeddedBrowser;

public class PDWebCallBackHandler
{
    [MethodImpl(0x8000)]
    public void gotoUrl(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void hideCloseBtn(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void onActvCancel(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void onActvCommit(JSONNode args)
    {
    }

    public void onClose(JSONNode args)
    {
        Debug.Log("webview onClose ");
    }

    [MethodImpl(0x8000)]
    public void onLoginCancel(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void onLoginFailed(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void onLoginSuccess(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void onPcPayCancel(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void onPcPayFailed(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void onPcPaySuccess(JSONNode args)
    {
    }

    [MethodImpl(0x8000)]
    public void showCloseBtn(JSONNode args)
    {
    }
}

