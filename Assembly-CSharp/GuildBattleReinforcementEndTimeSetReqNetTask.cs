﻿using BlackJack.BJFramework.Runtime.TaskNs;
using System;
using System.Runtime.CompilerServices;

public class GuildBattleReinforcementEndTimeSetReqNetTask : NetWorkTransactionTask
{
    public int m_result;
    private readonly int m_endHour;
    private readonly int m_endMinuts;

    [MethodImpl(0x8000)]
    public GuildBattleReinforcementEndTimeSetReqNetTask(int endHour, int endMinuts)
    {
    }

    private void OnGuildBattleReinforcementEndTimeSetAck(int result)
    {
        this.m_result = result;
        base.Stop();
    }

    [MethodImpl(0x8000)]
    protected override void RegisterNetworkEvent()
    {
    }

    [MethodImpl(0x8000)]
    protected override bool StartNetWorking()
    {
    }

    [MethodImpl(0x8000)]
    protected override void UnregisterNetworkEvent()
    {
    }
}

