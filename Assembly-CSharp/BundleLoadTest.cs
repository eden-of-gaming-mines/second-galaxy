﻿using BlackJack.BJFramework.Runtime.Resource;
using BlackJack.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class BundleLoadTest : MonoBehaviour
{
    private bool initComplete;
    private bool isLoading;
    private StreamWriter streamWriter;
    private AssetBundleManifest m_manifest;
    protected TinyCorutineHelper m_corutineHelper;
    private StreamingAssetsFileList m_stramingfileList;
    private List<AssetBundle> assetBundles;
    private Dictionary<string, AssetBundle> commonBundle;
    public List<string> unLoadBundles;
    public Text Notice;
    public string bundleName;
    public string assetName;

    [MethodImpl(0x8000)]
    private int GetBundleVersion(string name, out string path)
    {
    }

    [MethodImpl(0x8000)]
    public void Load()
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    protected IEnumerator LoadAllBundle()
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    protected IEnumerator LoadAsset(string assetName, string bundleName)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    protected IEnumerator LoadBundle(string bundleName, Action<AssetBundle> onComplete = null, bool LoadFrist = true)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    protected IEnumerator LoadBundleDependence(string bundleName, Action<bool> onComplete)
    {
    }

    [MethodImpl(0x8000)]
    public void LogFile(int msg)
    {
    }

    [MethodImpl(0x8000)]
    public void LogFile(string msg)
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private IEnumerator Start()
    {
    }

    [MethodImpl(0x8000)]
    protected void UnLoadBundle()
    {
    }

    private void Update()
    {
        this.m_corutineHelper.Tick();
    }

    [CompilerGenerated]
    private sealed class <LoadAllBundle>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal List<StreamingAssetsFileList.ListItem>.Enumerator $locvar0;
        internal StreamingAssetsFileList.ListItem <item>__1;
        internal IEnumerator <iter>__2;
        internal BundleLoadTest $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;
        private <LoadAllBundle>c__AnonStorey6 $locvar1;

        [DebuggerHidden]
        public void Dispose()
        {
            uint num = (uint) this.$PC;
            this.$disposing = true;
            this.$PC = -1;
            switch (num)
            {
                case 1:
                    try
                    {
                    }
                    finally
                    {
                        this.$locvar0.Dispose();
                    }
                    break;

                default:
                    break;
            }
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            bool flag = false;
            switch (num)
            {
                case 0:
                    this.$locvar0 = this.$this.m_stramingfileList.m_fileList.GetEnumerator();
                    num = 0xfffffffd;
                    goto TR_0017;

                case 1:
                    goto TR_0017;

                case 2:
                    this.$PC = -1;
                    break;

                default:
                    break;
            }
        TR_0000:
            return false;
        TR_0001:
            return true;
        TR_0017:
            try
            {
                switch (num)
                {
                    case 1:
                        goto TR_0010;

                    default:
                        break;
                }
                goto TR_0015;
            TR_0010:
                if (this.<iter>__2.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    flag = true;
                    goto TR_0001;
                }
                else if (this.$locvar1.bundle != null)
                {
                    if ((this.$this.unLoadBundles == null) || !this.$this.unLoadBundles.Contains(this.<item>__1.m_bundleName))
                    {
                        this.$locvar1.bundle.Unload(true);
                    }
                    else
                    {
                        this.$this.commonBundle[this.<item>__1.m_bundleName] = this.$locvar1.bundle;
                    }
                }
                else
                {
                    goto TR_0000;
                }
            TR_0015:
                while (true)
                {
                    if (!this.$locvar0.MoveNext())
                    {
                        break;
                    }
                    this.<item>__1 = this.$locvar0.Current;
                    this.$locvar1 = new <LoadAllBundle>c__AnonStorey6();
                    this.$locvar1.<>f__ref$1 = this;
                    if (this.<item>__1.m_bundleName.EndsWith("abs"))
                    {
                        this.$locvar1.bundle = null;
                        this.<iter>__2 = this.$this.LoadBundle(this.<item>__1.m_bundleName, new Action<AssetBundle>(this.$locvar1.<>m__0), true);
                        goto TR_0010;
                    }
                }
            }
            finally
            {
                if (!flag)
                {
                    this.$locvar0.Dispose();
                }
            }
            this.$this.LogFile("初始化加载完成");
            this.$this.initComplete = true;
            this.$current = null;
            if (!this.$disposing)
            {
                this.$PC = 2;
            }
            goto TR_0001;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;

        private sealed class <LoadAllBundle>c__AnonStorey6
        {
            internal AssetBundle bundle;
            internal BundleLoadTest.<LoadAllBundle>c__Iterator1 <>f__ref$1;

            internal void <>m__0(AssetBundle lbundle)
            {
                this.bundle = lbundle;
            }
        }
    }

    [CompilerGenerated]
    private sealed class <LoadAsset>c__Iterator4 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal long <timebefore>__0;
        internal string assetName;
        internal string bundleName;
        internal IEnumerator <iter>__0;
        internal long <timeBunldeLoad>__0;
        internal AssetBundleRequest <assetReqIter>__0;
        internal UnityEngine.Object <asset>__0;
        internal long <timeAssetLoad>__0;
        internal UnityEngine.Object <obj>__0;
        internal long <timeInstantiate>__0;
        internal BundleLoadTest $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;
        private <LoadAsset>c__AnonStorey8 $locvar0;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.$locvar0 = new <LoadAsset>c__AnonStorey8();
                    this.$locvar0.<>f__ref$4 = this;
                    if (!this.$this.isLoading)
                    {
                        this.$this.isLoading = true;
                        this.$this.Notice.text = string.Empty;
                        this.$this.LogFile(string.Empty);
                        this.<timebefore>__0 = DateTime.Now.ToFileTime();
                        this.$this.LogFile($"BeginLoad {this.assetName} from {this.bundleName} atTime:{DateTime.FromFileTime(this.<timebefore>__0)}");
                        this.$this.LogFile(string.Empty);
                        this.$locvar0.complete = false;
                        this.<iter>__0 = this.$this.LoadBundleDependence(this.bundleName, new Action<bool>(this.$locvar0.<>m__0));
                    }
                    else
                    {
                        this.$this.LogFile("有正在加载的资源前等待");
                        goto TR_0000;
                    }
                    break;

                case 1:
                    break;

                case 2:
                    goto TR_0012;

                case 3:
                    goto TR_000C;

                case 4:
                    this.$PC = -1;
                    goto TR_0000;

                default:
                    goto TR_0000;
            }
            if (this.<iter>__0.MoveNext())
            {
                this.$current = null;
                if (!this.$disposing)
                {
                    this.$PC = 1;
                }
                goto TR_0003;
            }
            else if (this.$locvar0.complete)
            {
                this.<iter>__0 = null;
                this.$locvar0.bundle = null;
                this.<iter>__0 = this.$this.LoadBundle(this.bundleName, new Action<AssetBundle>(this.$locvar0.<>m__1), false);
            }
            else
            {
                goto TR_0000;
            }
            goto TR_0012;
        TR_0000:
            return false;
        TR_0003:
            return true;
        TR_000C:
            if (!this.<assetReqIter>__0.isDone)
            {
                this.$current = null;
                if (!this.$disposing)
                {
                    this.$PC = 3;
                }
                goto TR_0003;
            }
            else
            {
                this.<asset>__0 = this.<assetReqIter>__0.asset;
                if (this.<asset>__0 != null)
                {
                    this.<timeAssetLoad>__0 = DateTime.Now.ToFileTime();
                    this.$this.LogFile($"time for LoadAsset:{(this.<timeAssetLoad>__0 - this.<timeBunldeLoad>__0) / 0x2710L}ms");
                    this.<obj>__0 = UnityEngine.Object.Instantiate(this.<asset>__0);
                    this.<timeInstantiate>__0 = DateTime.Now.ToFileTime();
                    this.$this.LogFile($"time for Instantiate:{(this.<timeInstantiate>__0 - this.<timeAssetLoad>__0) / 0x2710L}ms");
                    this.$this.LogFile($"EndLoad TotalTime:{(this.<timeInstantiate>__0 - this.<timebefore>__0) / 0x2710L}ms");
                    this.$this.LogFile($"EndLoad atTime:{DateTime.FromFileTime(this.<timeInstantiate>__0)}");
                    UnityEngine.Object.DestroyImmediate(this.<obj>__0);
                    this.$this.UnLoadBundle();
                    UnityEngine.Resources.UnloadUnusedAssets();
                    GC.Collect();
                    this.$this.LogFile(string.Empty);
                    this.$this.isLoading = false;
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 4;
                    }
                    goto TR_0003;
                }
                else
                {
                    this.$this.LogFile(this.assetName + "资源加载失败");
                    this.$this.UnLoadBundle();
                    this.$this.isLoading = false;
                }
            }
            goto TR_0000;
        TR_0012:
            if (this.<iter>__0.MoveNext())
            {
                this.$current = null;
                if (!this.$disposing)
                {
                    this.$PC = 2;
                }
                goto TR_0003;
            }
            else if (this.$locvar0.bundle != null)
            {
                this.<timeBunldeLoad>__0 = DateTime.Now.ToFileTime();
                this.$this.LogFile($"time for LoadBundle:{(this.<timeBunldeLoad>__0 - this.<timebefore>__0) / 0x2710L}ms");
                this.<assetReqIter>__0 = this.$locvar0.bundle.LoadAssetAsync(this.assetName);
            }
            else
            {
                goto TR_0000;
            }
            goto TR_000C;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;

        private sealed class <LoadAsset>c__AnonStorey8
        {
            internal bool complete;
            internal AssetBundle bundle;
            internal BundleLoadTest.<LoadAsset>c__Iterator4 <>f__ref$4;

            internal void <>m__0(bool isComplete)
            {
                this.complete = isComplete;
            }

            internal void <>m__1(AssetBundle lbundle)
            {
                this.bundle = lbundle;
            }
        }
    }

    [CompilerGenerated]
    private sealed class <LoadBundle>c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal bool LoadFrist;
        internal string bundleName;
        internal Action<AssetBundle> onComplete;
        internal string <url>__0;
        internal int <version>__0;
        internal long <beginLoad>__0;
        internal WWW <www>__0;
        internal BundleLoadTest $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    if (this.LoadFrist)
                    {
                        goto TR_0017;
                    }
                    else if (!this.$this.unLoadBundles.Contains(this.bundleName))
                    {
                        goto TR_0017;
                    }
                    else
                    {
                        this.$this.LogFile($"Load {this.bundleName}:is unload bundle");
                        this.onComplete(this.$this.commonBundle[this.bundleName]);
                    }
                    break;

                case 1:
                    goto TR_0012;

                default:
                    break;
            }
        TR_0000:
            return false;
        TR_0002:
            this.$this.LogFile(this.bundleName + "不存在");
            goto TR_0000;
        TR_0012:
            if (!this.<www>__0.isDone && string.IsNullOrEmpty(this.<www>__0.error))
            {
                this.$current = null;
                if (!this.$disposing)
                {
                    this.$PC = 1;
                }
                return true;
            }
            if (!string.IsNullOrEmpty(this.<www>__0.error))
            {
                this.$this.LogFile(this.<www>__0.error);
            }
            else if (!this.<www>__0.isDone || (this.<www>__0.assetBundle == null))
            {
                Debug.LogError("AssetBundleUpdateingWorker download  fail url = " + this.<url>__0);
            }
            else
            {
                if (!this.LoadFrist)
                {
                    this.$this.LogFile($"load {this.bundleName}：{(DateTime.Now.ToFileTime() - this.<beginLoad>__0) / 0x2710L}ms");
                    this.$this.assetBundles.Add(this.<www>__0.assetBundle);
                }
                if (this.onComplete != null)
                {
                    this.onComplete(this.<www>__0.assetBundle);
                }
                this.$PC = -1;
            }
            goto TR_0000;
        TR_0017:
            this.<url>__0 = null;
            this.<version>__0 = -1;
            this.<beginLoad>__0 = 0L;
            this.<version>__0 = this.$this.GetBundleVersion(this.bundleName, out this.<url>__0);
            if (this.<url>__0 == null)
            {
                goto TR_0002;
            }
            else if (this.<version>__0 != -1)
            {
                if (!this.LoadFrist)
                {
                    this.<beginLoad>__0 = DateTime.Now.ToFileTime();
                }
                this.<www>__0 = WWW.LoadFromCacheOrDownload(this.<url>__0, this.<version>__0);
            }
            else
            {
                goto TR_0002;
            }
            goto TR_0012;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }

    [CompilerGenerated]
    private sealed class <LoadBundleDependence>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string bundleName;
        internal string[] <dependenceList>__0;
        internal string[] $locvar0;
        internal int $locvar1;
        internal string <dependence>__1;
        internal IEnumerator <iter>__2;
        internal Action<bool> onComplete;
        internal BundleLoadTest $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;
        private <LoadBundleDependence>c__AnonStorey7 $locvar2;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        public bool MoveNext()
        {
            uint num = (uint) this.$PC;
            this.$PC = -1;
            switch (num)
            {
                case 0:
                    this.<dependenceList>__0 = this.$this.m_manifest.GetAllDependencies(this.bundleName);
                    if (this.<dependenceList>__0 == null)
                    {
                        goto TR_0001;
                    }
                    else if (this.<dependenceList>__0.Length == 0)
                    {
                        goto TR_0001;
                    }
                    else
                    {
                        this.$this.LogFile($"Load dependence bundles :count={this.<dependenceList>__0.Length}");
                        this.$locvar0 = this.<dependenceList>__0;
                        this.$locvar1 = 0;
                    }
                    break;

                case 1:
                    goto TR_0009;

                default:
                    goto TR_0000;
            }
            goto TR_000C;
        TR_0000:
            return false;
        TR_0001:
            this.onComplete(true);
            this.$PC = -1;
            goto TR_0000;
        TR_0009:
            if (this.<iter>__2.MoveNext())
            {
                this.$current = null;
                if (!this.$disposing)
                {
                    this.$PC = 1;
                }
                return true;
            }
            if (this.$locvar2.bundle != null)
            {
                this.$locvar1++;
            }
            else
            {
                goto TR_0000;
            }
        TR_000C:
            while (true)
            {
                if (this.$locvar1 >= this.$locvar0.Length)
                {
                    this.$this.LogFile("Load dependence bundles Complete.");
                    this.$this.LogFile(string.Empty);
                    break;
                }
                this.<dependence>__1 = this.$locvar0[this.$locvar1];
                this.$locvar2 = new <LoadBundleDependence>c__AnonStorey7();
                this.$locvar2.<>f__ref$2 = this;
                this.$locvar2.bundle = null;
                this.<iter>__2 = this.$this.LoadBundle(this.<dependence>__1, new Action<AssetBundle>(this.$locvar2.<>m__0), false);
                goto TR_0009;
            }
            goto TR_0001;
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;

        private sealed class <LoadBundleDependence>c__AnonStorey7
        {
            internal AssetBundle bundle;
            internal BundleLoadTest.<LoadBundleDependence>c__Iterator2 <>f__ref$2;

            internal void <>m__0(AssetBundle lbundle)
            {
                this.bundle = lbundle;
            }
        }
    }

    [CompilerGenerated]
    private sealed class <Start>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal string <path>__0;
        internal IEnumerator <iter>__0;
        internal AssetBundleManifest[] <manifests>__0;
        internal BundleLoadTest $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;
        private <Start>c__AnonStorey5 $locvar0;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        [MethodImpl(0x8000)]
        public bool MoveNext()
        {
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;

        private sealed class <Start>c__AnonStorey5
        {
            internal AssetBundle manifestBundle;
            internal BundleLoadTest.<Start>c__Iterator0 <>f__ref$0;

            internal void <>m__0(AssetBundle bundle)
            {
                this.manifestBundle = bundle;
            }
        }
    }
}

