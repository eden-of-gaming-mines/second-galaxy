﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using ZenFulcrum.EmbeddedBrowser;

public class PDWebViewManager : MonoBehaviour
{
    public RawImage MaskGraphic;
    public static PDWebViewManager Manager;
    private PDWebCallBackHandler m_callbackHandler;
    private string m_CurrentUrl = string.Empty;
    private string m_PageType = string.Empty;
    private Browser m_WebBrowser;
    private GameObject m_WebviewCloseBtn;
    private GameObject m_WebviewCloseBg;

    [MethodImpl(0x8000)]
    private void Awake()
    {
        if (null == Manager)
        {
            UserAgent.SetUserAgent("untiypcsdk");
            BrowserNative.ProfilePath = Application.dataPath + "/BrowserProfile/";
            Manager = this;
            this.m_callbackHandler = new PDWebCallBackHandler();
        }
    }

    [MethodImpl(0x8000)]
    public void ClearCookie()
    {
    }

    [MethodImpl(0x8000)]
    public void CloseWebBrowser()
    {
    }

    [MethodImpl(0x8000), DebuggerHidden]
    private IEnumerator DelayShow()
    {
    }

    [MethodImpl(0x8000)]
    public void OpenWebBrowser(string url, float width = 0f, float height = 0f)
    {
    }

    [MethodImpl(0x8000)]
    private void RegisterJSFuncAll()
    {
    }

    public void ShowWebviewCloseBg(bool isShow)
    {
        this.WebviewCloseBg.SetActive(isShow);
    }

    [MethodImpl(0x8000)]
    public void ShowWebviewCloseBtn(bool isShow, string pageType = "")
    {
    }

    [MethodImpl(0x8000)]
    private void Start()
    {
        this.WebBrowser.onNavStateChange += delegate {
        };
    }

    private Browser WebBrowser
    {
        [MethodImpl(0x8000)]
        get
        {
            if (this.m_WebBrowser == null)
            {
                GameObject gameObject = base.transform.Find("BrowserGUI").gameObject;
                if (gameObject && gameObject.GetComponent<Browser>())
                {
                    this.m_WebBrowser = gameObject.GetComponent<Browser>();
                }
            }
            return this.m_WebBrowser;
        }
    }

    private GameObject WebviewCloseBtn
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    private GameObject WebviewCloseBg
    {
        [MethodImpl(0x8000)]
        get
        {
        }
    }

    [CompilerGenerated]
    private sealed class <DelayShow>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
    {
        internal PDWebViewManager $this;
        internal object $current;
        internal bool $disposing;
        internal int $PC;

        [DebuggerHidden]
        public void Dispose()
        {
            this.$disposing = true;
            this.$PC = -1;
        }

        [MethodImpl(0x8000)]
        public bool MoveNext()
        {
        }

        [DebuggerHidden]
        public void Reset()
        {
            throw new NotSupportedException();
        }

        object IEnumerator<object>.Current =>
            this.$current;

        object IEnumerator.Current =>
            this.$current;
    }
}

