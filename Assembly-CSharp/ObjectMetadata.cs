﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
internal struct ObjectMetadata
{
    private System.Type element_type;
    private bool is_dictionary;
    private IDictionary<string, PropertyMetadata> properties;
    public System.Type ElementType
    {
        [MethodImpl(0x8000)]
        get
        {
        }
        set => 
            (this.element_type = value);
    }
    public bool IsDictionary
    {
        get => 
            this.is_dictionary;
        set => 
            (this.is_dictionary = value);
    }
    public IDictionary<string, PropertyMetadata> Properties
    {
        get => 
            this.properties;
        set => 
            (this.properties = value);
    }
}

