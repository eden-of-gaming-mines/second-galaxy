﻿namespace BlackJack.ServerFramework.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="LoginByAuthTokenAck")]
    public class LoginByAuthTokenAck : IExtensible
    {
        private int _Result;
        private bool _NeedRedirect;
        private string _SessionToken;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_NeedRedirect;
        private static DelegateBridge __Hotfix_set_NeedRedirect;
        private static DelegateBridge __Hotfix_get_SessionToken;
        private static DelegateBridge __Hotfix_set_SessionToken;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="NeedRedirect", DataFormat=DataFormat.Default)]
        public bool NeedRedirect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="SessionToken", DataFormat=DataFormat.Default), DefaultValue("")]
        public string SessionToken
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

