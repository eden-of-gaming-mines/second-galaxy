﻿namespace BlackJack.ServerFramework.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="LoginBySessionTokenReq")]
    public class LoginBySessionTokenReq : IExtensible
    {
        private string _SessionToken;
        private string _ClientVersion;
        private string _Localization;
        private int _CurrChannelId;
        private int _BornChannelId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SessionToken;
        private static DelegateBridge __Hotfix_set_SessionToken;
        private static DelegateBridge __Hotfix_get_ClientVersion;
        private static DelegateBridge __Hotfix_set_ClientVersion;
        private static DelegateBridge __Hotfix_get_Localization;
        private static DelegateBridge __Hotfix_set_Localization;
        private static DelegateBridge __Hotfix_get_CurrChannelId;
        private static DelegateBridge __Hotfix_set_CurrChannelId;
        private static DelegateBridge __Hotfix_get_BornChannelId;
        private static DelegateBridge __Hotfix_set_BornChannelId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SessionToken", DataFormat=DataFormat.Default)]
        public string SessionToken
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ClientVersion", DataFormat=DataFormat.Default)]
        public string ClientVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Localization", DataFormat=DataFormat.Default)]
        public string Localization
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CurrChannelId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrChannelId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="BornChannelId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BornChannelId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

