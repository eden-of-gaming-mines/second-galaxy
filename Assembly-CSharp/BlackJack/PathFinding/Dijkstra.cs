﻿namespace BlackJack.PathFinding
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class Dijkstra
    {
        private DJNode[] m_allNode;
        private Queue<TouchedNode> m_touchedNodePool;
        private FindingCtx m_defaultFindingCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_AddNodes;
        private static DelegateBridge __Hotfix_MergeNode;
        private static DelegateBridge __Hotfix_FindPath_2;
        private static DelegateBridge __Hotfix_FindPath_0;
        private static DelegateBridge __Hotfix_FindPath_1;
        private static DelegateBridge __Hotfix_FindNodesWithinCost_0;
        private static DelegateBridge __Hotfix_FindNodesWithinCost_1;
        private static DelegateBridge __Hotfix_FindNodesWithinCost_2;
        private static DelegateBridge __Hotfix_FindNodeWithinCostWithExInfo;
        private static DelegateBridge __Hotfix_FindNodesEqualCost_0;
        private static DelegateBridge __Hotfix_FindNodesEqualCost_1;
        private static DelegateBridge __Hotfix_FindNodesWithinCostLimitGroupByCost;
        private static DelegateBridge __Hotfix_AllocTouchedNode;
        private static DelegateBridge __Hotfix_FreeTouchedNode;
        private static DelegateBridge __Hotfix_ProcessNodeLinks;
        private static DelegateBridge __Hotfix_AddNodeToOpenListSorted;
        private static DelegateBridge __Hotfix_GetTouchedNodePathAllCost;
        private static DelegateBridge __Hotfix_GetResultPath;
        private static DelegateBridge __Hotfix_GetDJNodeById;
        private static DelegateBridge __Hotfix_BeginPathFinding;
        private static DelegateBridge __Hotfix_StepPathFinding;
        private static DelegateBridge __Hotfix_EndPathFinding;
        private static DelegateBridge __Hotfix_IsFindingCtxAlreadyTouchedNode;

        [MethodImpl(0x8000)]
        public void AddNodes(IList<DJNode> nodes)
        {
        }

        [MethodImpl(0x8000)]
        private void AddNodeToOpenListSorted(FindingCtx findingCtx, TouchedNode node4Add)
        {
        }

        [MethodImpl(0x8000)]
        private TouchedNode AllocTouchedNode()
        {
        }

        [MethodImpl(0x8000)]
        private bool BeginPathFinding(FindingCtx findingCtx, DJNode fromNode, Func<DJNode, DJLink, int> costCalc, int costLimit, Action<DJNode, int, bool> onNodeTouched = null)
        {
        }

        [MethodImpl(0x8000)]
        public void EndPathFinding(object findingCtx)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindNodesEqualCost(DJNode from, int cost, Func<DJNode, DJLink, int> costCalc = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindNodesEqualCost(int from, int cost, Func<DJNode, DJLink, int> costCalc = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindNodesWithinCost(object findingCtx, int cost)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindNodesWithinCost(DJNode from, int cost, Func<DJNode, DJLink, int> costCalc = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindNodesWithinCost(int from, int cost, Func<DJNode, DJLink, int> costCalc = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode>[] FindNodesWithinCostLimitGroupByCost(int id, int costLimit, Func<DJNode, DJLink, int> costCalc = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<DJNode, int>> FindNodeWithinCostWithExInfo(object findingCtx, int cost)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindPath(DJNode from, DJNode to, Func<DJNode, DJLink, int> costCalc = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindPath(int from, int to, Func<DJNode, DJLink, int> costCalc = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<DJNode> FindPath(object findingCtx, DJNode to, out int allCost)
        {
        }

        [MethodImpl(0x8000)]
        private void FreeTouchedNode(TouchedNode node)
        {
        }

        [MethodImpl(0x8000)]
        private DJNode GetDJNodeById(int id)
        {
        }

        [MethodImpl(0x8000)]
        private List<DJNode> GetResultPath(FindingCtx findingCtx, DJNode end, out int allCost)
        {
        }

        [MethodImpl(0x8000)]
        private int GetTouchedNodePathAllCost(FindingCtx findingCtx, TouchedNode node)
        {
        }

        [MethodImpl(0x8000)]
        public void Initialize(IList<DJNode> nodes)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsFindingCtxAlreadyTouchedNode(FindingCtx findingCtx, DJNode to)
        {
        }

        [MethodImpl(0x8000)]
        private void MergeNode(DJNode targetNode, DJNode newNode)
        {
        }

        [MethodImpl(0x8000)]
        private bool ProcessNodeLinks(FindingCtx findingCtx, TouchedNode node, int toNodeId, Func<DJNode, DJLink, int> costCalc, int costLimit, Action<DJNode, int, bool> onNodeTouched = null)
        {
        }

        [MethodImpl(0x8000)]
        public int StepPathFinding(object findingCtx, DJNode toNode)
        {
        }

        private class FindingCtx
        {
            public int m_pathVersion;
            public Func<DJNode, DJLink, int> m_costCalc;
            public int m_costLimit;
            public Action<DJNode, int, bool> m_onNodeTouched;
            public LinkedList<Dijkstra.TouchedNode> m_openNodeQueue = new LinkedList<Dijkstra.TouchedNode>();
            public Dictionary<int, Dijkstra.TouchedNode> m_touchedNodes = new Dictionary<int, Dijkstra.TouchedNode>();
            private Dijkstra m_owner;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Clear;

            public FindingCtx(Dijkstra owner)
            {
                this.m_owner = owner;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, owner);
                }
            }

            public void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    foreach (KeyValuePair<int, Dijkstra.TouchedNode> pair in this.m_touchedNodes)
                    {
                        this.m_owner.FreeTouchedNode(pair.Value);
                    }
                    this.m_pathVersion = 0;
                    this.m_costCalc = null;
                    this.m_onNodeTouched = null;
                    this.m_openNodeQueue.Clear();
                    this.m_touchedNodes.Clear();
                }
            }
        }

        private class TouchedNode
        {
            public DJNode m_node;
            public Dijkstra.TouchedNode m_fromNode;
            public int m_cost;
            public int m_allCost;
            public int m_allCostPathVersion;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

