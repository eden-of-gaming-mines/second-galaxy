﻿namespace BlackJack.PropertiesCalculater
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class PropertiesCalculaterNoCache<T> : PropertiesCalculaterTiny<T> where T: class, IPropertiesInfo
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_CheckDirty;
        private static DelegateBridge __Hotfix_CheckAnyGroupDirty;

        [MethodImpl(0x8000)]
        protected PropertiesCalculaterNoCache()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcPropertiesById(int propertiesId, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override bool CheckAnyGroupDirty()
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override bool CheckDirty(int group)
        {
        }

        [MethodImpl(0x8000)]
        protected override void Initialize()
        {
        }
    }
}

