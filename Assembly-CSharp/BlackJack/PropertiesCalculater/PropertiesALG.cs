﻿namespace BlackJack.PropertiesCalculater
{
    using System;

    public enum PropertiesALG
    {
        PropertiesALG_Add = 1,
        PropertiesALG_Min = 2,
        PropertiesALG_Max = 3,
        PropertiesALG_OneAddMulti = 4,
        PropertiesALG_OneSubMulti = 5,
        PropertiesALG_Custom = 6
    }
}

