﻿namespace BlackJack.PropertiesCalculater
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class PropertiesCalculaterTiny<T> : BlackJack.PropertiesCalculater.PropertiesCalculater<T> where T: class, IPropertiesInfo
    {
        protected List<PropertiesCache<T>> m_propertiesCacheList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_CleanPropertiesCacheByGroup;

        [MethodImpl(0x8000)]
        protected PropertiesCalculaterTiny()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcPropertiesById(int propertiesId, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected override void CleanPropertiesCacheByGroup(int group)
        {
        }

        [MethodImpl(0x8000)]
        protected override void Initialize()
        {
        }

        [Serializable]
        protected class PropertiesCache
        {
            public Dictionary<int, float> m_id2ValueDict;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

