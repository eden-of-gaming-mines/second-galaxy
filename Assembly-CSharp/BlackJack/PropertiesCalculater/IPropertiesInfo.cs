﻿namespace BlackJack.PropertiesCalculater
{
    using System;

    public interface IPropertiesInfo
    {
        int PropertyId { get; }

        int PropertyGroup { get; }

        PropertiesALG CalcAlgorithm { get; }

        bool IsPropertyActive { get; }
    }
}

