﻿namespace BlackJack.PropertiesCalculater
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class PropertiesCalculater<T> : PropertiesCalculaterBase where T: class, IPropertiesInfo
    {
        protected static T[] s_propertiesConfInfoList;
        protected static T[][] s_propertiesListForGroups;
        protected static int s_groupMax;
        protected static int s_cachedGoupMax;
        protected static int s_propertiesCount;
        protected uint m_propertiesGroupDirtyMask;
        protected List<IPropertiesProvider> m_providerList;
        protected float?[] m_propertiesValueList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StaticInitlize;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_AddProvider;
        private static DelegateBridge __Hotfix_RemoveProvider;
        private static DelegateBridge __Hotfix_ClearAll;
        private static DelegateBridge __Hotfix_SetDirty;
        private static DelegateBridge __Hotfix_SetDirtyByGroupMask;
        private static DelegateBridge __Hotfix_MergePropertyValue;
        private static DelegateBridge __Hotfix_CheckDirty;
        private static DelegateBridge __Hotfix_CheckAnyGroupDirty;
        private static DelegateBridge __Hotfix_CleanPropertiesCacheByGroup;
        private static DelegateBridge __Hotfix_GetPropertiesInfoById;
        private static DelegateBridge __Hotfix_GetPropertiesGroupIdById;
        private static DelegateBridge __Hotfix_OnProviderPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMaskById;
        private static DelegateBridge __Hotfix_HasPropertiesGroupImpl;
        private static DelegateBridge __Hotfix_HasPropertyImpl;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMaskImpl;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAddImpl;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMinImpl;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMaxImpl;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneAddMultiImpl;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneSubMultiImpl;
        private static DelegateBridge __Hotfix_RegEventOnPropertiesGroupDirtyImpl;
        private static DelegateBridge __Hotfix_UnregEventOnPropertiesGroupDirtyImpl;

        [MethodImpl(0x8000)]
        public override void AddProvider(IPropertiesProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcPropertiesById(int propertiesId, object ctx)
        {
        }

        protected abstract float CalcPropertiesByIdCustom(int propertiesid, object ctx);
        [MethodImpl(0x8000)]
        protected virtual bool CheckAnyGroupDirty()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool CheckDirty(int group)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void CleanPropertiesCacheByGroup(int group)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearAll()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAddImpl(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdMaxImpl(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdMinImpl(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdOneAddMultiImpl(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdOneSubMultiImpl(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPropertiesGroupIdById(int id)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetPropertiesGroupMaskById(int id)
        {
        }

        [MethodImpl(0x8000)]
        protected override uint GetPropertiesGroupMaskImpl()
        {
        }

        [MethodImpl(0x8000)]
        public static T GetPropertiesInfoById(int id)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasPropertiesGroupImpl(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasPropertyImpl(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void Initialize()
        {
        }

        [MethodImpl(0x8000)]
        protected float MergePropertyValue(T confInfo, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnProviderPropertiesGroupDirty(int group)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegEventOnPropertiesGroupDirtyImpl(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public override void RemoveProvider(IPropertiesProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetDirty(int group = 0)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetDirtyByGroupMask(uint groupMask)
        {
        }

        [MethodImpl(0x8000)]
        public static void StaticInitlize(List<T> propertiesConfList, int cachedGoupMax)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregEventOnPropertiesGroupDirtyImpl(Action<int> action)
        {
        }
    }
}

