﻿namespace BlackJack.PropertiesCalculater
{
    using System;
    using System.Runtime.InteropServices;

    public interface IPropertiesProvider
    {
        bool GetPropertiesByIdAdd(int propertiesId, out float result, object ctx);
        bool GetPropertiesByIdMax(int propertiesId, out float result, object ctx);
        bool GetPropertiesByIdMin(int propertiesId, out float result, object ctx);
        bool GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx);
        bool GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx);
        uint GetPropertiesGroupMask();
        bool HasPropertiesGroup(int cacheGroup);
        bool HasProperty(int propertiesId);
        void RegEventOnPropertiesGroupDirty(Action<int> action);
        void UnregEventOnPropertiesGroupDirty(Action<int> action);
    }
}

