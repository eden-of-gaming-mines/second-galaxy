﻿namespace BlackJack.PropertiesCalculater
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class PropertiesCalculaterBase : IPropertiesProvider
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnPropertiesGroupDirty;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_add_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_remove_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;

        public event Action<int> EventOnPropertiesGroupDirty
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected PropertiesCalculaterBase()
        {
        }

        public abstract void AddProvider(IPropertiesProvider provider);
        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        public abstract float CalcPropertiesById(int propertiesId, object ctx);
        public abstract void ClearAll();
        protected abstract bool GetPropertiesByIdAddImpl(int propertiesId, out float result, object ctx);
        protected abstract bool GetPropertiesByIdMaxImpl(int propertiesId, out float result, object ctx);
        protected abstract bool GetPropertiesByIdMinImpl(int propertiesId, out float result, object ctx);
        protected abstract bool GetPropertiesByIdOneAddMultiImpl(int propertiesId, out float result, object ctx);
        protected abstract bool GetPropertiesByIdOneSubMultiImpl(int propertiesId, out float result, object ctx);
        protected abstract uint GetPropertiesGroupMaskImpl();
        protected abstract bool HasPropertiesGroupImpl(int cacheGroup);
        protected abstract bool HasPropertyImpl(int propertiesId);
        [MethodImpl(0x8000)]
        protected virtual void OnPropertiesGroupDirty(int group)
        {
        }

        protected abstract void RegEventOnPropertiesGroupDirtyImpl(Action<int> action);
        public abstract void RemoveProvider(IPropertiesProvider provider);
        public abstract void SetDirty(int group = 0);
        public abstract void SetDirtyByGroupMask(uint groupMask);
        protected abstract void UnregEventOnPropertiesGroupDirtyImpl(Action<int> action);
    }
}

