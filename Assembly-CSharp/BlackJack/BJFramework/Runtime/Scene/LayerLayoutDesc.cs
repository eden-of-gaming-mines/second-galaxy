﻿namespace BlackJack.BJFramework.Runtime.Scene
{
    using System;
    using UnityEngine;

    public class LayerLayoutDesc : MonoBehaviour
    {
        public bool Opaque;
        public bool FullScreen;
        public bool StayOnTop;
        [Header("layer优先度, 值越大越上层")]
        public int Priority;
    }
}

