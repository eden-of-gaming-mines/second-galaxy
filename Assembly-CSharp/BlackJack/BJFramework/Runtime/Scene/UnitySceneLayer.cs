﻿namespace BlackJack.BJFramework.Runtime.Scene
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class UnitySceneLayer : ThreeDSceneLayer
    {
        private Camera m_layerCamera;
        public UnityEngine.SceneManagement.Scene Scene;
        public List<GameObject> UnitySceneRootObjs;
        private static DelegateBridge __Hotfix_SetScene;
        private static DelegateBridge __Hotfix_get_LayerCamera;

        [MethodImpl(0x8000)]
        public void SetScene(UnityEngine.SceneManagement.Scene scene)
        {
        }

        public override Camera LayerCamera
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

