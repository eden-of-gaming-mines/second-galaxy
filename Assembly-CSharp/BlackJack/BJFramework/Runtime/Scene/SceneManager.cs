﻿namespace BlackJack.BJFramework.Runtime.Scene
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.Rendering;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;

    public class SceneManager : ITickable
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnEventSystemEnable;
        private static BlackJack.BJFramework.Runtime.Scene.SceneManager m_instance;
        private TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
        private bool m_useOrthographicForUILayer;
        private int m_designScreenWidth;
        private int m_designScreenHeight;
        private int m_trigerWidth2ShrinkScale;
        private int m_trigerHeight2ShrinkScale;
        private Vector3 m_layerOffset;
        private GameObject m_sceneRootGo;
        private GameObject m_unusedLayerRootGo;
        private GameObject m_loadingLayerRootGo;
        private GameObject m_3DSceneRootGo;
        private GameObject m_uiSceneGroup1RootCanvasGo;
        private GameObject m_uiSceneGroup2RootCanvasGo;
        private GameObject m_unusedLayerRootCanvasGo;
        private GameObject m_inputBlockRootGo;
        private EventSystem m_eventSystem;
        public Camera m_uiSceneGroup1Camera;
        public Camera m_uiSceneGroup2Camera;
        private Canvas m_uiSceneGroup1RootCanvas;
        private Canvas m_uiSceneGroup2RootCanvas;
        private Canvas m_unusedLayerRootCanvas;
        private CanvasScaler m_uiSceneGroup1RootCanvasScaler;
        private CanvasScaler m_uiSceneGroup2RootCanvasScaler;
        private CanvasScaler m_unusedLayerRootCanvasScaler;
        private GameObject m_3DLayerRootPrefab;
        private GameObject m_uiLayerRootPrefab;
        private Dictionary<string, SceneLayerBase> m_layerDict = new Dictionary<string, SceneLayerBase>();
        private readonly List<SceneLayerBase> m_unusedLayerList = new List<SceneLayerBase>();
        private readonly List<SceneLayerBase> m_loadingLayerList = new List<SceneLayerBase>();
        private readonly List<SceneLayerBase> m_layerStack = new List<SceneLayerBase>();
        private bool m_stackDirty;
        private bool m_is3DLayerBlurActive;
        private LayerRenderSettingDesc m_currRenderSetting;
        private LayerRenderSettingDesc m_defaultRenderSetting;
        private bool m_enableLayerReserve;
        private static string m_sceneRootAssetPath = "SceneRoot";
        private static string m_3DLayerRootAssetPath = "3DLayerRoot";
        private static string m_uiLayerRootAssetPath = "UILayerRoot";
        private static int m_cameraDepthMax = 80;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateSceneManager;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_CreateSceneRoot;
        private static DelegateBridge __Hotfix_CreateLayer;
        private static DelegateBridge __Hotfix_CreateUILayer;
        private static DelegateBridge __Hotfix_Create3DLayer;
        private static DelegateBridge __Hotfix_CreateUnitySceneLayer;
        private static DelegateBridge __Hotfix_FreeLayer;
        private static DelegateBridge __Hotfix_PushLayer;
        private static DelegateBridge __Hotfix_PopLayer;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_Enable3DLayerBlur;
        private static DelegateBridge __Hotfix_SetLayerTransparentMask;
        private static DelegateBridge __Hotfix_ClearLayerTransparentMask;
        private static DelegateBridge __Hotfix_AddLayerToLoading;
        private static DelegateBridge __Hotfix_AddLayerToUnused;
        private static DelegateBridge __Hotfix_OnLayerLoadAssetComplete;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_UpdateLayerStack;
        private static DelegateBridge __Hotfix_SortLayerStack;
        private static DelegateBridge __Hotfix_GetDefaultRenderSetting;
        private static DelegateBridge __Hotfix_ApplyRenderSettingAsDefault;
        private static DelegateBridge __Hotfix_ApplyDefaultRenderSetting;
        private static DelegateBridge __Hotfix_ApplyOverlayRenderSetting;
        private static DelegateBridge __Hotfix_GetCurrRenderSetting;
        private static DelegateBridge __Hotfix_ApplyRenderSettingImp;
        private static DelegateBridge __Hotfix_GetRootCanvas;
        private static DelegateBridge __Hotfix_FindLayerByName;
        private static DelegateBridge __Hotfix_FindUILayerUnderLayer;
        private static DelegateBridge __Hotfix_GetLayerCameraFromSceneByPath;
        private static DelegateBridge __Hotfix_GetRectTransformFromSceneByPath;
        private static DelegateBridge __Hotfix_GetObjectPath;
        private static DelegateBridge __Hotfix_EnableInputBlock;
        private static DelegateBridge __Hotfix_GetEventSystemActiveState;
        private static DelegateBridge __Hotfix_GetEventSystemEnableState;
        private static DelegateBridge __Hotfix_SetEventSystemDpi;
        private static DelegateBridge __Hotfix_GetCurrRenderSettingFogEnable;
        private static DelegateBridge __Hotfix_SetCurrRenderSettingFogEnable;
        private static DelegateBridge __Hotfix_add_EventOnEventSystemEnable;
        private static DelegateBridge __Hotfix_remove_EventOnEventSystemEnable;
        private static DelegateBridge __Hotfix_get_Instance;

        public event Action<bool> EventOnEventSystemEnable
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private SceneManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        private void AddLayerToLoading(SceneLayerBase layer)
        {
            DelegateBridge bridge = __Hotfix_AddLayerToLoading;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, layer);
            }
            else if (!ReferenceEquals(layer.GetType(), typeof(UnitySceneLayer)))
            {
                layer.gameObject.transform.SetParent(this.m_loadingLayerRootGo.transform, false);
                layer.gameObject.transform.localPosition = Vector3.zero;
                layer.gameObject.transform.localScale = Vector3.one;
                layer.gameObject.transform.localRotation = Quaternion.identity;
                layer.gameObject.SetActive(false);
                this.m_loadingLayerList.Add(layer);
                layer.State = SceneLayerBase.LayerState.Loading;
            }
        }

        [MethodImpl(0x8000)]
        private void AddLayerToUnused(SceneLayerBase layer)
        {
            DelegateBridge bridge = __Hotfix_AddLayerToUnused;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, layer);
            }
            else if (!ReferenceEquals(layer.GetType(), typeof(UnitySceneLayer)))
            {
                this.m_loadingLayerList.Remove(layer);
                this.m_layerStack.Remove(layer);
                layer.gameObject.transform.SetParent(this.m_unusedLayerRootCanvasGo.transform, false);
                layer.gameObject.transform.localPosition = Vector3.zero;
                layer.gameObject.transform.localScale = Vector3.one;
                layer.gameObject.transform.localRotation = Quaternion.identity;
                layer.gameObject.SetActive(false);
                this.m_unusedLayerList.Add(layer);
                layer.State = SceneLayerBase.LayerState.Unused;
            }
        }

        [MethodImpl(0x8000)]
        public void ApplyDefaultRenderSetting()
        {
        }

        [MethodImpl(0x8000)]
        public void ApplyOverlayRenderSetting(LayerRenderSettingDesc renderSetting)
        {
        }

        [MethodImpl(0x8000)]
        private void ApplyRenderSettingAsDefault(LayerRenderSettingDesc renderSetting)
        {
            DelegateBridge bridge = __Hotfix_ApplyRenderSettingAsDefault;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, renderSetting);
            }
            else if (this.m_currRenderSetting != renderSetting)
            {
                this.m_currRenderSetting = renderSetting;
                this.ApplyRenderSettingImp(this.m_currRenderSetting);
            }
        }

        [MethodImpl(0x8000)]
        private void ApplyRenderSettingImp(LayerRenderSettingDesc renderSetting)
        {
            DelegateBridge bridge = __Hotfix_ApplyRenderSettingImp;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, renderSetting);
            }
            else
            {
                RenderSettings.ambientMode = renderSetting.enviornmentLighting.AmbientSource;
                if (renderSetting.enviornmentLighting.AmbientSource == AmbientMode.Skybox)
                {
                    RenderSettings.skybox = renderSetting.enviornmentLighting.SkyBox;
                    RenderSettings.ambientLight = renderSetting.enviornmentLighting.AmbientColor;
                }
                else if (renderSetting.enviornmentLighting.AmbientSource == AmbientMode.Flat)
                {
                    RenderSettings.skybox = renderSetting.enviornmentLighting.SkyBox;
                    RenderSettings.ambientLight = renderSetting.enviornmentLighting.AmbientColor;
                }
                RenderSettings.ambientIntensity = renderSetting.enviornmentLighting.AmbientIntensity;
                RenderSettings.fog = renderSetting.fog.EnableFog;
                RenderSettings.fogMode = renderSetting.fog.FogMode;
                RenderSettings.fogColor = renderSetting.fog.FogColor;
                if (renderSetting.fog.FogMode != FogMode.Linear)
                {
                    RenderSettings.fogDensity = renderSetting.fog.FogDensity;
                }
                else
                {
                    RenderSettings.fogStartDistance = renderSetting.fog.FogStart;
                    RenderSettings.fogEndDistance = renderSetting.fog.FogEnd;
                }
                DynamicGI.UpdateEnvironment();
            }
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop(UISceneLayer layer)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearLayerTransparentMask()
        {
        }

        [MethodImpl(0x8000)]
        private void Create3DLayer(string name, string resPath, Action<SceneLayerBase> onComplete, bool enableReserve = false)
        {
        }

        [MethodImpl(0x8000)]
        public void CreateLayer(System.Type layerType, string name, string resPath, Action<SceneLayerBase> onComplete, bool enableReserve = false)
        {
            DelegateBridge bridge = __Hotfix_CreateLayer;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp156(this, layerType, name, resPath, onComplete, enableReserve);
            }
            else
            {
                if (string.IsNullOrEmpty(name))
                {
                    throw new Exception("CreateLayer need a name");
                }
                if (!this.m_enableLayerReserve)
                {
                    enableReserve = false;
                }
                SceneLayerBase base2 = this.FindLayerByName(name);
                if (base2 != null)
                {
                    if (base2.IsReserve() && (base2.State == SceneLayerBase.LayerState.Unused))
                    {
                        onComplete(base2);
                    }
                    else
                    {
                        object[] paramList = new object[] { "CreateLayer oldLayer state error. LayerName:{0},LayerType:{1},respath:{2},IsReserve:{3},LayerState:{4}", name, layerType, resPath, base2.IsReserve(), base2.State };
                        Debug.LogError(paramList);
                        onComplete(null);
                    }
                }
                else
                {
                    if (this.m_layerDict.ContainsKey(name))
                    {
                        throw new Exception($"CreateLayer name conflict {name}");
                    }
                    if (ReferenceEquals(layerType, typeof(UISceneLayer)))
                    {
                        this.CreateUILayer(name, resPath, onComplete, enableReserve);
                    }
                    else if (ReferenceEquals(layerType, typeof(ThreeDSceneLayer)))
                    {
                        this.Create3DLayer(name, resPath, onComplete, enableReserve);
                    }
                    else if (ReferenceEquals(layerType, typeof(UnitySceneLayer)))
                    {
                        this.CreateUnitySceneLayer(name, resPath, onComplete);
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public static BlackJack.BJFramework.Runtime.Scene.SceneManager CreateSceneManager()
        {
            DelegateBridge bridge = __Hotfix_CreateSceneManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp154();
            }
            if (m_instance == null)
            {
                m_instance = new BlackJack.BJFramework.Runtime.Scene.SceneManager();
            }
            return m_instance;
        }

        [MethodImpl(0x8000)]
        private bool CreateSceneRoot()
        {
            DelegateBridge bridge = __Hotfix_CreateSceneRoot;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            Debug.Log("SceneManager.CreateSceneRoot start");
            GameObject original = UnityEngine.Resources.Load<GameObject>(m_sceneRootAssetPath);
            if (original == null)
            {
                Debug.LogError("SceneManager.CreateSceneRoot fail sceneRootPrefab = null");
                return false;
            }
            this.m_sceneRootGo = UnityEngine.Object.Instantiate<GameObject>(original);
            if (this.m_sceneRootGo == null)
            {
                Debug.LogError("SceneManager.CreateSceneRoot fail sceneRootGo = null");
                return false;
            }
            Util.RemoveCloneFromGameObjectName(this.m_sceneRootGo);
            this.m_sceneRootGo.transform.localPosition = Vector3.zero;
            this.m_sceneRootGo.transform.localRotation = Quaternion.identity;
            this.m_sceneRootGo.transform.localScale = Vector3.one;
            this.m_eventSystem = this.m_sceneRootGo.GetComponentInChildren<EventSystem>(true);
            this.m_3DSceneRootGo = this.m_sceneRootGo.transform.Find("3DSceneRoot").gameObject;
            this.m_unusedLayerRootGo = this.m_sceneRootGo.transform.Find("UnusedLayerRoot").gameObject;
            this.m_loadingLayerRootGo = this.m_sceneRootGo.transform.Find("LoadingLayerRoot").gameObject;
            this.m_uiSceneGroup1RootCanvasGo = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot1/Canvas").gameObject;
            this.m_uiSceneGroup2RootCanvasGo = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot2/Canvas").gameObject;
            this.m_inputBlockRootGo = this.m_sceneRootGo.transform.Find("InputBlockRoot").gameObject;
            this.m_unusedLayerRootCanvasGo = this.m_sceneRootGo.transform.Find("UnusedLayerRoot/Canvas").gameObject;
            this.m_uiSceneGroup1Camera = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot1").gameObject.GetComponent<Camera>();
            this.m_uiSceneGroup2Camera = this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot2").gameObject.GetComponent<Camera>();
            this.m_uiSceneGroup1RootCanvas = this.m_uiSceneGroup1RootCanvasGo.GetComponent<Canvas>();
            this.m_uiSceneGroup2RootCanvas = this.m_uiSceneGroup2RootCanvasGo.GetComponent<Canvas>();
            this.m_unusedLayerRootCanvas = this.m_unusedLayerRootCanvasGo.GetComponent<Canvas>();
            this.m_uiSceneGroup1RootCanvasScaler = this.m_uiSceneGroup1RootCanvasGo.GetComponent<CanvasScaler>();
            this.m_uiSceneGroup2RootCanvasScaler = this.m_uiSceneGroup2RootCanvasGo.GetComponent<CanvasScaler>();
            this.m_unusedLayerRootCanvasScaler = this.m_unusedLayerRootCanvasGo.GetComponent<CanvasScaler>();
            this.m_uiSceneGroup1RootCanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            this.m_uiSceneGroup1RootCanvasScaler.set_referenceResolution(new Vector2((float) this.m_designScreenWidth, (float) this.m_designScreenHeight));
            if ((Screen.width < this.m_trigerWidth2ShrinkScale) || (Screen.height < this.m_trigerHeight2ShrinkScale))
            {
                this.m_uiSceneGroup1RootCanvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Shrink;
            }
            this.m_uiSceneGroup2RootCanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            this.m_uiSceneGroup2RootCanvasScaler.set_referenceResolution(new Vector2((float) this.m_designScreenWidth, (float) this.m_designScreenHeight));
            if ((Screen.width < this.m_trigerWidth2ShrinkScale) || (Screen.height < this.m_trigerHeight2ShrinkScale))
            {
                this.m_uiSceneGroup2RootCanvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Shrink;
            }
            this.m_unusedLayerRootCanvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            this.m_unusedLayerRootCanvasScaler.set_referenceResolution(new Vector2((float) this.m_designScreenWidth, (float) this.m_designScreenHeight));
            if ((Screen.width < this.m_trigerWidth2ShrinkScale) || (Screen.height < this.m_trigerHeight2ShrinkScale))
            {
                this.m_unusedLayerRootCanvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Shrink;
            }
            if (this.m_useOrthographicForUILayer)
            {
                this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot1").GetComponent<Camera>().orthographic = true;
                this.m_sceneRootGo.transform.Find("UISceneRoot/UILayerGroupRoot2").GetComponent<Camera>().orthographic = true;
            }
            return true;
        }

        [MethodImpl(0x8000)]
        private void CreateUILayer(string name, string resPath, Action<SceneLayerBase> onComplete, bool enableReserve = false)
        {
            DelegateBridge bridge = __Hotfix_CreateUILayer;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp131(this, name, resPath, onComplete, enableReserve);
            }
            else
            {
                <CreateUILayer>c__AnonStorey0 storey = new <CreateUILayer>c__AnonStorey0 {
                    onComplete = onComplete,
                    $this = this
                };
                GameObject go = UnityEngine.Object.Instantiate<GameObject>(this.m_uiLayerRootPrefab);
                Util.RemoveCloneFromGameObjectName(go);
                go.name = name + "_LayerRoot";
                storey.layer = go.GetComponent<UISceneLayer>();
                storey.layer.SetName(name);
                storey.layer.SetReserve(enableReserve);
                this.m_layerDict.Add(name, storey.layer);
                this.AddLayerToLoading(storey.layer);
                IEnumerator corutine = ResourceManager.Instance.LoadAsset<GameObject>(resPath, new Action<string, GameObject>(storey.<>m__0), false, false);
                this.m_corutineHelper.StartCorutine(corutine);
            }
        }

        [MethodImpl(0x8000)]
        private void CreateUnitySceneLayer(string name, string resPath, Action<SceneLayerBase> onComplete)
        {
        }

        [MethodImpl(0x8000)]
        public void Enable3DLayerBlur(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableInputBlock(bool enable)
        {
            DelegateBridge bridge = __Hotfix_EnableInputBlock;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, enable);
            }
            else if ((this.m_inputBlockRootGo != null) && (this.m_inputBlockRootGo.activeSelf != enable))
            {
                this.m_inputBlockRootGo.SetActive(enable);
            }
        }

        [MethodImpl(0x8000)]
        public SceneLayerBase FindLayerByName(string name)
        {
            SceneLayerBase base2;
            DelegateBridge bridge = __Hotfix_FindLayerByName;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp159(this, name);
            }
            this.m_layerDict.TryGetValue(name, out base2);
            return base2;
        }

        [MethodImpl(0x8000)]
        public SceneLayerBase FindUILayerUnderLayer(string name, Func<SceneLayerBase, bool> filter)
        {
        }

        [MethodImpl(0x8000)]
        public void FreeLayer(SceneLayerBase layer)
        {
        }

        [MethodImpl(0x8000)]
        public LayerRenderSettingDesc GetCurrRenderSetting()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetCurrRenderSettingFogEnable()
        {
        }

        [MethodImpl(0x8000)]
        private LayerRenderSettingDesc GetDefaultRenderSetting()
        {
            DelegateBridge bridge = __Hotfix_GetDefaultRenderSetting;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp153(this);
            }
            if (this.m_defaultRenderSetting == null)
            {
                LayerRenderSettingDesc componentInChildren = this.m_sceneRootGo.GetComponentInChildren<LayerRenderSettingDesc>(true);
                this.m_defaultRenderSetting = componentInChildren;
            }
            return this.m_defaultRenderSetting;
        }

        [MethodImpl(0x8000)]
        public bool GetEventSystemActiveState()
        {
            DelegateBridge bridge = __Hotfix_GetEventSystemActiveState;
            return ((bridge == null) ? this.m_eventSystem.gameObject.activeInHierarchy : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        public bool GetEventSystemEnableState()
        {
            DelegateBridge bridge = __Hotfix_GetEventSystemEnableState;
            return ((bridge == null) ? this.m_eventSystem.enabled : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCameraFromSceneByPath(string rectTransPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetObjectPath(GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetRectTransformFromSceneByPath(string rectTransPath)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetRootCanvas(int index = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initlize(int designScreenWidth, int designScreenHeight, int trigerWidth2ShrinkScale, int trigerHeight2ShrinkScale, Vector3 layerOffset, bool enableLayerReserve = false, bool useOrthographicForUILayer = false)
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp155(this, designScreenWidth, designScreenHeight, trigerWidth2ShrinkScale, trigerHeight2ShrinkScale, layerOffset, enableLayerReserve, useOrthographicForUILayer);
            }
            Debug.Log("SceneManager.Initlize start");
            this.m_designScreenWidth = designScreenWidth;
            this.m_designScreenHeight = designScreenHeight;
            this.m_trigerWidth2ShrinkScale = trigerWidth2ShrinkScale;
            this.m_trigerHeight2ShrinkScale = trigerHeight2ShrinkScale;
            this.m_layerOffset = layerOffset;
            this.m_useOrthographicForUILayer = useOrthographicForUILayer;
            this.m_enableLayerReserve = enableLayerReserve;
            if (!this.CreateSceneRoot())
            {
                Debug.LogError("SceneManager.Initlize CreateSceneRoot fail");
                return false;
            }
            this.m_uiLayerRootPrefab = UnityEngine.Resources.Load<GameObject>(m_uiLayerRootAssetPath);
            this.m_3DLayerRootPrefab = UnityEngine.Resources.Load<GameObject>(m_3DLayerRootAssetPath);
            if ((this.m_uiLayerRootPrefab == null) || (this.m_3DLayerRootPrefab == null))
            {
                Debug.LogError("SceneManager.Initlize m_uiLayerRootPrefab == null || m_3DLayerRootPrefab == null");
                return false;
            }
            LayerRenderSettingDesc defaultRenderSetting = this.GetDefaultRenderSetting();
            if (defaultRenderSetting == null)
            {
                Debug.LogError("SceneManager.Initlize GetDefaultRenderSetting fail");
                return false;
            }
            this.ApplyRenderSettingAsDefault(defaultRenderSetting);
            return true;
        }

        [MethodImpl(0x8000)]
        private void OnLayerLoadAssetComplete(SceneLayerBase layer, GameObject asset)
        {
            DelegateBridge bridge = __Hotfix_OnLayerLoadAssetComplete;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, layer, asset);
            }
            else if (!ReferenceEquals(layer.GetType(), typeof(UnitySceneLayer)))
            {
                if (asset == null)
                {
                    Debug.LogError($"CreateUILayer LoadAsset fail name={layer.LayerName}");
                    this.FreeLayer(layer);
                }
                else
                {
                    GameObject go = UnityEngine.Object.Instantiate<GameObject>(asset);
                    Util.RemoveCloneFromGameObjectName(go);
                    layer.AttachGameObject(go);
                    this.AddLayerToUnused(layer);
                }
            }
        }

        [MethodImpl(0x8000)]
        public void PopLayer(SceneLayerBase layer)
        {
        }

        [MethodImpl(0x8000)]
        public bool PushLayer(SceneLayerBase layer)
        {
            DelegateBridge bridge = __Hotfix_PushLayer;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp16(this, layer);
            }
            if (layer.State != SceneLayerBase.LayerState.Unused)
            {
                Debug.LogError($"PushLayer in wrong state layer={layer.name} state={layer.State}");
                return false;
            }
            this.m_unusedLayerList.Remove(layer);
            this.m_layerStack.Add(layer);
            layer.State = SceneLayerBase.LayerState.InStack;
            if (ReferenceEquals(layer.GetType(), typeof(UISceneLayer)))
            {
                layer.transform.SetParent(this.m_uiSceneGroup1RootCanvasGo.transform, false);
            }
            else if (ReferenceEquals(layer.GetType(), typeof(ThreeDSceneLayer)))
            {
                layer.transform.SetParent(this.m_3DSceneRootGo.transform, false);
            }
            else if (ReferenceEquals(layer.GetType(), typeof(UnitySceneLayer)))
            {
            }
            if (!ReferenceEquals(layer.GetType(), typeof(UnitySceneLayer)))
            {
                layer.gameObject.SetActive(true);
            }
            this.m_stackDirty = true;
            return true;
        }

        [MethodImpl(0x8000)]
        public void SetCurrRenderSettingFogEnable(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEventSystemDpi(int value)
        {
            DelegateBridge bridge = __Hotfix_SetEventSystemDpi;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp31(this, value);
            }
            else
            {
                this.m_eventSystem.pixelDragThreshold = value;
            }
        }

        [MethodImpl(0x8000)]
        public void SetLayerTransparentMask(List<string> layerNames, bool isNegativeMask, float alpha = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void SortLayerStack(List<SceneLayerBase> layerStack)
        {
            DelegateBridge bridge = __Hotfix_SortLayerStack;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, layerStack);
            }
            else
            {
                int num = 1;
                while (num < layerStack.Count)
                {
                    int num2 = num;
                    SceneLayerBase base2 = layerStack[num];
                    while (true)
                    {
                        if ((num2 <= 0) || !base2.IsLowPriorityThan(layerStack[num2 - 1]))
                        {
                            layerStack[num2] = base2;
                            num++;
                            break;
                        }
                        layerStack[num2] = layerStack[num2 - 1];
                        num2--;
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                try
                {
                    this.m_corutineHelper.Tick();
                    this.UpdateLayerStack();
                }
                catch (Exception exception)
                {
                    Debug.LogError($"{"SceneManager.Tick"} : {exception}");
                }
            }
        }

        [MethodImpl(0x8000)]
        public void Uninitlize()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLayerStack()
        {
            DelegateBridge bridge = __Hotfix_UpdateLayerStack;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.m_stackDirty)
            {
                this.m_stackDirty = false;
                this.SortLayerStack(this.m_layerStack);
                bool flag = false;
                int cameraDepthMax = m_cameraDepthMax;
                Vector3 zero = Vector3.zero;
                UnitySceneLayer layer = null;
                this.m_uiSceneGroup2RootCanvasGo.SetActive(false);
                for (int i = this.m_layerStack.Count - 1; i >= 0; i--)
                {
                    SceneLayerBase base2 = this.m_layerStack[i];
                    if (ReferenceEquals(base2.GetType(), typeof(UISceneLayer)))
                    {
                        if (!flag)
                        {
                            base2.gameObject.transform.SetParent(this.m_uiSceneGroup1RootCanvasGo.transform, false);
                            base2.gameObject.transform.localPosition = Vector3.zero;
                            base2.gameObject.transform.localScale = Vector3.one;
                            base2.gameObject.transform.SetAsFirstSibling();
                            base2.LayerCamera.depth = cameraDepthMax;
                        }
                        else
                        {
                            if (!this.m_uiSceneGroup2RootCanvasGo.activeSelf)
                            {
                                this.m_uiSceneGroup2RootCanvasGo.SetActive(true);
                            }
                            base2.gameObject.transform.SetParent(this.m_uiSceneGroup2RootCanvasGo.transform, false);
                            base2.gameObject.transform.localPosition = Vector3.zero;
                            base2.gameObject.transform.SetAsFirstSibling();
                            base2.LayerCamera.depth = cameraDepthMax;
                        }
                    }
                    else if (!ReferenceEquals(base2.GetType(), typeof(ThreeDSceneLayer)))
                    {
                        layer = (layer != null) ? layer : ((UnitySceneLayer) base2);
                        if (base2.LayerCamera != null)
                        {
                            base2.LayerCamera.depth = --cameraDepthMax;
                            cameraDepthMax--;
                        }
                        flag = true;
                    }
                    else
                    {
                        base2.gameObject.transform.SetParent(this.m_3DSceneRootGo.transform, false);
                        base2.gameObject.transform.localPosition = zero;
                        zero += this.m_layerOffset;
                        if (base2.LayerCamera != null)
                        {
                            base2.LayerCamera.depth = --cameraDepthMax;
                            cameraDepthMax--;
                            MonoBehaviour component = base2.LayerCamera.gameObject.GetComponent("UnityStandardAssets.ImageEffects.BlurOptimized") as MonoBehaviour;
                            if (component != null)
                            {
                                component.enabled = this.m_is3DLayerBlurActive;
                            }
                        }
                        flag = true;
                    }
                }
                bool flag2 = false;
                LayerRenderSettingDesc renderSetting = null;
                for (int j = this.m_layerStack.Count - 1; j >= 0; j--)
                {
                    SceneLayerBase base3 = this.m_layerStack[j];
                    renderSetting = (renderSetting != null) ? renderSetting : base3.GetRenderSetting();
                    if (!(base3 is UnitySceneLayer))
                    {
                        if (!flag2)
                        {
                            base3.gameObject.SetActive(true);
                        }
                        else
                        {
                            base3.gameObject.SetActive(false);
                        }
                    }
                    if (base3.IsOpaque() && base3.IsFullScreen())
                    {
                        flag2 = true;
                    }
                }
                if (renderSetting == null)
                {
                    renderSetting = this.GetDefaultRenderSetting();
                }
                if (layer == null)
                {
                    foreach (SceneLayerBase base4 in this.m_unusedLayerList)
                    {
                        if (ReferenceEquals(base4.GetType(), typeof(UnitySceneLayer)))
                        {
                            layer = (UnitySceneLayer) base4;
                            break;
                        }
                    }
                }
                if (layer == null)
                {
                    this.ApplyRenderSettingAsDefault(renderSetting);
                }
                else if (UnityEngine.SceneManagement.SceneManager.GetActiveScene() != layer.Scene)
                {
                    UnityEngine.SceneManagement.SceneManager.SetActiveScene(layer.Scene);
                }
            }
        }

        public static BlackJack.BJFramework.Runtime.Scene.SceneManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp154());
            }
        }

        [CompilerGenerated]
        private sealed class <Create3DLayer>c__AnonStorey1
        {
            internal ThreeDSceneLayer layer;
            internal Action<SceneLayerBase> onComplete;
            internal BlackJack.BJFramework.Runtime.Scene.SceneManager $this;

            internal void <>m__0(string lpath, GameObject lasset)
            {
                if (this.layer.State == SceneLayerBase.LayerState.WaitForFree)
                {
                    this.$this.FreeLayer(this.layer);
                }
                else
                {
                    this.$this.OnLayerLoadAssetComplete(this.layer, lasset);
                    this.onComplete((lasset != null) ? this.layer : null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CreateUILayer>c__AnonStorey0
        {
            internal UISceneLayer layer;
            internal Action<SceneLayerBase> onComplete;
            internal BlackJack.BJFramework.Runtime.Scene.SceneManager $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(string lpath, GameObject lasset)
            {
                if (this.layer.State == SceneLayerBase.LayerState.WaitForFree)
                {
                    this.$this.FreeLayer(this.layer);
                }
                else
                {
                    this.$this.OnLayerLoadAssetComplete(this.layer, lasset);
                    this.onComplete((lasset != null) ? this.layer : null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CreateUnitySceneLayer>c__AnonStorey2
        {
            internal Action<SceneLayerBase> onComplete;
            internal Scene? scene;
            internal string name;
            internal BlackJack.BJFramework.Runtime.Scene.SceneManager $this;

            internal void <>m__0(string lpath, Scene? lscene)
            {
                if (lscene == null)
                {
                    this.onComplete(null);
                }
                else
                {
                    this.scene = lscene;
                    GameObject[] rootGameObjects = this.scene.Value.GetRootGameObjects();
                    if ((rootGameObjects == null) || (rootGameObjects.Length == 0))
                    {
                        this.onComplete(null);
                    }
                    else
                    {
                        UnitySceneLayer layer = rootGameObjects[0].AddComponent<UnitySceneLayer>();
                        layer.SetName(this.name);
                        layer.SetScene(this.scene.Value);
                        this.$this.m_layerDict.Add(this.name, layer);
                        layer.State = SceneLayerBase.LayerState.Unused;
                        this.$this.m_unusedLayerList.Add(layer);
                        this.$this.PushLayer(layer);
                        this.onComplete(layer);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <FindUILayerUnderLayer>c__AnonStorey3
        {
            internal string name;

            internal bool <>m__0(SceneLayerBase layer) => 
                (layer.LayerName == this.name);
        }
    }
}

