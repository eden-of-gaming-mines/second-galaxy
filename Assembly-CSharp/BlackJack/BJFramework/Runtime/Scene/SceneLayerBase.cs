﻿namespace BlackJack.BJFramework.Runtime.Scene
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneLayerBase : MonoBehaviour
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <LayerName>k__BackingField;
        public LayerState State;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsResourceReady>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsInited>k__BackingField;
        private LayerLayoutDesc m_layerLayoutDesc;
        private LayerRenderSettingDesc m_renderSetting;
        private bool m_renderSettingSearched;
        private bool m_isReserve;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private GameObject <LayerPrefabRoot>k__BackingField;
        private static DelegateBridge __Hotfix_SetName;
        private static DelegateBridge __Hotfix_IsOpaque;
        private static DelegateBridge __Hotfix_IsFullScreen;
        private static DelegateBridge __Hotfix_IsStayOnTop;
        private static DelegateBridge __Hotfix_GetLayerPriority;
        private static DelegateBridge __Hotfix_SetLayerPriority;
        private static DelegateBridge __Hotfix_SetLayerOnTop;
        private static DelegateBridge __Hotfix_IsLowPriorityThan;
        private static DelegateBridge __Hotfix_GetDesc;
        private static DelegateBridge __Hotfix_AttachGameObject;
        private static DelegateBridge __Hotfix_GetRenderSetting;
        private static DelegateBridge __Hotfix_IsReserve;
        private static DelegateBridge __Hotfix_SetReserve;
        private static DelegateBridge __Hotfix_get_LayerName;
        private static DelegateBridge __Hotfix_set_LayerName;
        private static DelegateBridge __Hotfix_get_LayerCamera;
        private static DelegateBridge __Hotfix_get_IsResourceReady;
        private static DelegateBridge __Hotfix_set_IsResourceReady;
        private static DelegateBridge __Hotfix_get_IsInited;
        private static DelegateBridge __Hotfix_set_IsInited;
        private static DelegateBridge __Hotfix_get_LayerPrefabRoot;
        private static DelegateBridge __Hotfix_set_LayerPrefabRoot;

        [MethodImpl(0x8000)]
        public void AttachGameObject(GameObject go)
        {
            DelegateBridge bridge = __Hotfix_AttachGameObject;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, go);
            }
            else if (go != null)
            {
                Vector3 localPosition = go.transform.localPosition;
                Vector3 localScale = go.transform.localScale;
                Quaternion localRotation = go.transform.localRotation;
                go.transform.SetParent(base.transform);
                go.transform.localPosition = localPosition;
                go.transform.localScale = localScale;
                go.transform.localRotation = localRotation;
                this.LayerPrefabRoot = go;
            }
        }

        [MethodImpl(0x8000)]
        private LayerLayoutDesc GetDesc()
        {
            DelegateBridge bridge = __Hotfix_GetDesc;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp152(this);
            }
            if (this.m_layerLayoutDesc == null)
            {
                LayerLayoutDesc[] componentsInChildren = base.transform.GetComponentsInChildren<LayerLayoutDesc>(true);
                if (componentsInChildren.Length != 0)
                {
                    this.m_layerLayoutDesc = componentsInChildren[0];
                }
            }
            return this.m_layerLayoutDesc;
        }

        [MethodImpl(0x8000)]
        public int GetLayerPriority()
        {
        }

        [MethodImpl(0x8000)]
        public LayerRenderSettingDesc GetRenderSetting()
        {
            DelegateBridge bridge = __Hotfix_GetRenderSetting;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp153(this);
            }
            if (!this.m_renderSettingSearched)
            {
                this.m_renderSettingSearched = true;
                LayerRenderSettingDesc[] componentsInChildren = base.GetComponentsInChildren<LayerRenderSettingDesc>(true);
                if (componentsInChildren.Length != 0)
                {
                    this.m_renderSetting = componentsInChildren[0];
                }
            }
            return this.m_renderSetting;
        }

        [MethodImpl(0x8000)]
        public bool IsFullScreen()
        {
            DelegateBridge bridge = __Hotfix_IsFullScreen;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            LayerLayoutDesc desc = this.GetDesc();
            return ((desc != null) ? desc.FullScreen : true);
        }

        [MethodImpl(0x8000)]
        public bool IsLowPriorityThan(SceneLayerBase compareLayer)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOpaque()
        {
            DelegateBridge bridge = __Hotfix_IsOpaque;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            LayerLayoutDesc desc = this.GetDesc();
            return ((desc != null) ? desc.Opaque : true);
        }

        [MethodImpl(0x8000)]
        public bool IsReserve()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStayOnTop()
        {
        }

        [MethodImpl(0x8000)]
        public void SetLayerOnTop(bool isOntop)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLayerPriority(int priority)
        {
        }

        [MethodImpl(0x8000)]
        public void SetName(string name)
        {
            DelegateBridge bridge = __Hotfix_SetName;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, name);
            }
            else
            {
                if (!string.IsNullOrEmpty(this.LayerName))
                {
                    throw new Exception($"SceneLayerBase.SetName={name} to layer already have name={this.LayerName}");
                }
                this.LayerName = name;
            }
        }

        [MethodImpl(0x8000)]
        public void SetReserve(bool value)
        {
            DelegateBridge bridge = __Hotfix_SetReserve;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, value);
            }
            else
            {
                this.m_isReserve = value;
            }
        }

        public string LayerName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_LayerName;
                return ((bridge == null) ? this.<LayerName>k__BackingField : bridge.__Gen_Delegate_Imp33(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
                DelegateBridge bridge = __Hotfix_set_LayerName;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<LayerName>k__BackingField = value;
                }
            }
        }

        public virtual Camera LayerCamera
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsResourceReady
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_IsResourceReady;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, value);
                }
                else
                {
                    this.<IsResourceReady>k__BackingField = value;
                }
            }
        }

        public bool IsInited
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_IsInited;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, value);
                }
                else
                {
                    this.<IsInited>k__BackingField = value;
                }
            }
        }

        public GameObject LayerPrefabRoot
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_LayerPrefabRoot;
                return ((bridge == null) ? this.<LayerPrefabRoot>k__BackingField : bridge.__Gen_Delegate_Imp35(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
                DelegateBridge bridge = __Hotfix_set_LayerPrefabRoot;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<LayerPrefabRoot>k__BackingField = value;
                }
            }
        }

        public enum LayerState
        {
            None,
            Loading,
            Unused,
            InStack,
            WaitForFree
        }
    }
}

