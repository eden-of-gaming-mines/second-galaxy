﻿namespace BlackJack.BJFramework.Runtime.Scene
{
    using IL;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class UISceneLayer : SceneLayerBase
    {
        private Transform m_layerParent;
        private Camera m_layerCamera;
        private Canvas m_layerCanvas;
        private CanvasGroup m_layerCanvasGroup;
        private static DelegateBridge __Hotfix_get_LayerCamera;
        private static DelegateBridge __Hotfix_get_LayerCanvas;
        private static DelegateBridge __Hotfix_get_LayerCanvasGroup;

        public override Camera LayerCamera
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_LayerCamera;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp2(this);
                }
                if (base.State != SceneLayerBase.LayerState.InStack)
                {
                    this.m_layerCamera = null;
                }
                else
                {
                    if (this.m_layerParent != base.transform.parent)
                    {
                        this.m_layerParent = base.transform.parent;
                        this.m_layerCamera = null;
                    }
                    if (this.m_layerCamera == null)
                    {
                        Camera[] componentsInParent = base.GetComponentsInParent<Camera>(true);
                        if (componentsInParent.Length != 0)
                        {
                            this.m_layerCamera = componentsInParent[0];
                        }
                    }
                }
                return this.m_layerCamera;
            }
        }

        public Canvas LayerCanvas
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CanvasGroup LayerCanvasGroup
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

