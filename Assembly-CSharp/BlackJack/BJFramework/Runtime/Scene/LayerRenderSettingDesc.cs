﻿namespace BlackJack.BJFramework.Runtime.Scene
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Rendering;

    public class LayerRenderSettingDesc : MonoBehaviour
    {
        public EnviornmentLighting enviornmentLighting;
        public Fog fog;
        private static DelegateBridge __Hotfix_CopyFrom;

        [MethodImpl(0x8000)]
        public void CopyFrom(LayerRenderSettingDesc target)
        {
        }

        [Serializable]
        public class EnviornmentLighting
        {
            public Material SkyBox;
            public AmbientMode AmbientSource;
            public float AmbientIntensity;
            [Header("TrilightMode Colors")]
            public Color SkyColor;
            public Color EquatorColor;
            public Color GroundColor;
            [Header("FlatMode Color")]
            public Color AmbientColor;
            private static DelegateBridge _c__Hotfix_ctor;

            [MethodImpl(0x8000)]
            public EnviornmentLighting()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        [Serializable]
        public class Fog
        {
            public bool EnableFog;
            public Color FogColor;
            public UnityEngine.FogMode FogMode;
            [Header("LinearMode")]
            public float FogStart;
            public float FogEnd;
            [Header("ExponentialMode")]
            public float FogDensity;
            private static DelegateBridge _c__Hotfix_ctor;

            public Fog()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

