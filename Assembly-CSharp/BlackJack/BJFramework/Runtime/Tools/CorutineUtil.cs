﻿namespace BlackJack.BJFramework.Runtime.Tools
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CorutineUtil
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_WaitUntil;
        private static DelegateBridge __Hotfix_WaitForSeconds;

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerator WaitForSeconds(float second)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerator WaitUntil(Func<bool> predicate)
        {
            DelegateBridge bridge = __Hotfix_WaitUntil;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(predicate);
            }
            return new <WaitUntil>c__Iterator0 { predicate = predicate };
        }

        [CompilerGenerated]
        private sealed class <WaitForSeconds>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal float second;
            internal DateTime <outTime>__0;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<outTime>__0 = Timer.m_currTime.AddSeconds((double) this.second);
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<outTime>__0 < Timer.m_currTime)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <WaitUntil>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Func<bool> predicate;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (this.predicate())
                        {
                            this.$PC = -1;
                            break;
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

