﻿namespace BlackJack.BJFramework.Runtime.Tools
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class CLZF
    {
        private static readonly uint HLOG;
        private static readonly uint HSIZE;
        private static readonly uint MAX_LIT;
        private static readonly uint MAX_OFF;
        private static readonly uint MAX_REF;
        private static readonly long[] HashTable;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Compress;
        private static DelegateBridge __Hotfix_Decompress;
        private static DelegateBridge __Hotfix_lzf_compress;
        private static DelegateBridge __Hotfix_lzf_decompress;

        [MethodImpl(0x8000)]
        public static byte[] Compress(byte[] inputBytes)
        {
        }

        [MethodImpl(0x8000)]
        public static byte[] Decompress(byte[] inputBytes)
        {
        }

        [MethodImpl(0x8000)]
        public static int lzf_compress(byte[] input, ref byte[] output)
        {
        }

        [MethodImpl(0x8000)]
        public static int lzf_decompress(byte[] input, ref byte[] output)
        {
        }
    }
}

