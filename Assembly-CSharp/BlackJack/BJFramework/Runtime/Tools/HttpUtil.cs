﻿namespace BlackJack.BJFramework.Runtime.Tools
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class HttpUtil
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetUploadFileServerSaveName;
        private static DelegateBridge __Hotfix_DownloadFileFromHttp;
        private static DelegateBridge __Hotfix_UploadFileToHttp;
        private static DelegateBridge __Hotfix_Byte2HexByte;

        [MethodImpl(0x8000)]
        private static byte[] Byte2HexByte(byte[] bytes)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerator DownloadFileFromHttp(string url, Action<bool, WWW> onReceive, Action<WWW> onUpdate = null, int retryCount = 3, int singleTimeout = 0x3a98)
        {
            DelegateBridge bridge = __Hotfix_DownloadFileFromHttp;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp203(url, onReceive, onUpdate, retryCount, singleTimeout);
            }
            return new <DownloadFileFromHttp>c__Iterator0 { 
                url = url,
                onReceive = onReceive,
                retryCount = retryCount,
                onUpdate = onUpdate,
                singleTimeout = singleTimeout
            };
        }

        [MethodImpl(0x8000)]
        public static string GetUploadFileServerSaveName(RuntimePlatform platform, string appVersion, string clientBasicVersion, string clientCurrVersion, string serverId, string playerName, string playerId, string fileName)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerator UploadFileToHttp(string localFilepath, string uploadURL, string serverFilename, Action<bool> onEnd)
        {
        }

        [CompilerGenerated]
        private sealed class <DownloadFileFromHttp>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string url;
            internal Action<bool, WWW> onReceive;
            internal int retryCount;
            internal Action<WWW> onUpdate;
            internal int singleTimeout;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <DownloadFileFromHttp>c__AnonStorey2 $locvar0;
            private <DownloadFileFromHttp>c__AnonStorey3 $locvar1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
                int num2;
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <DownloadFileFromHttp>c__AnonStorey2();
                        this.$locvar0.<>f__ref$0 = this;
                        this.$locvar0.onUpdate = this.onUpdate;
                        this.$locvar0.singleTimeout = this.singleTimeout;
                        if (string.IsNullOrEmpty(this.url))
                        {
                            this.onReceive(false, null);
                            goto TR_0000;
                        }
                        goto TR_000D;

                    case 1:
                        if (this.$locvar1.isTimeOut)
                        {
                            break;
                        }
                        if (!this.$locvar1.www.isDone)
                        {
                            break;
                        }
                        if (!string.IsNullOrEmpty(this.$locvar1.www.error))
                        {
                            break;
                        }
                        if (this.onReceive != null)
                        {
                            this.onReceive(true, this.$locvar1.www);
                        }
                        goto TR_0000;

                    case 2:
                        goto TR_000D;

                    default:
                        goto TR_0000;
                }
                if (this.$locvar1.isTimeOut)
                {
                    Debug.LogError($"DownloadFileFromHttp: {this.url} Error: TimeOut: {this.$locvar0.singleTimeout}");
                }
                else
                {
                    Debug.LogError($"DownloadFileFromHttp: {this.url} Error: {this.$locvar1.www.error}");
                }
                if (this.retryCount > 0)
                {
                    Debug.Log("DownloadFileFromHttp:Wait for 2 seconds and retry to download...");
                    this.$current = CorutineUtil.WaitForSeconds(2f);
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                    goto TR_0006;
                }
                goto TR_000D;
            TR_0000:
                return false;
            TR_0006:
                return true;
            TR_000D:
                this.retryCount = (num2 = this.retryCount) - 1;
                if (num2 > 0)
                {
                    this.$locvar1 = new <DownloadFileFromHttp>c__AnonStorey3();
                    this.$locvar1.<>f__ref$0 = this;
                    this.$locvar1.<>f__ref$2 = this.$locvar0;
                    this.$locvar1.www = null;
                    try
                    {
                        this.$locvar1.www = new WWW(this.url);
                        this.$locvar1.wwwStartTime = DateTime.Now;
                        this.$locvar1.isTimeOut = false;
                        this.$current = CorutineUtil.WaitUntil(new Func<bool>(this.$locvar1.<>m__0));
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        goto TR_0006;
                    }
                    catch (Exception exception)
                    {
                        Debug.LogError($"{"HttpUtil.DownloadFileFromHttp"} : {exception}");
                        this.onReceive(false, null);
                    }
                }
                else
                {
                    if (this.onReceive != null)
                    {
                        this.onReceive(false, null);
                    }
                    this.$PC = -1;
                }
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <DownloadFileFromHttp>c__AnonStorey2
            {
                internal Action<WWW> onUpdate;
                internal int singleTimeout;
                internal HttpUtil.<DownloadFileFromHttp>c__Iterator0 <>f__ref$0;
            }

            private sealed class <DownloadFileFromHttp>c__AnonStorey3
            {
                internal WWW www;
                internal DateTime wwwStartTime;
                internal bool isTimeOut;
                internal HttpUtil.<DownloadFileFromHttp>c__Iterator0 <>f__ref$0;
                internal HttpUtil.<DownloadFileFromHttp>c__Iterator0.<DownloadFileFromHttp>c__AnonStorey2 <>f__ref$2;

                internal bool <>m__0()
                {
                    int num1;
                    if (this.<>f__ref$2.onUpdate != null)
                    {
                        this.<>f__ref$2.onUpdate(this.www);
                    }
                    if (!this.www.isDone)
                    {
                        num1 = (int) ((DateTime.Now - this.wwwStartTime).TotalMilliseconds >= this.<>f__ref$2.singleTimeout);
                    }
                    else
                    {
                        num1 = 0;
                    }
                    this.isTimeOut = (bool) num1;
                    return (this.www.isDone || this.isTimeOut);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UploadFileToHttp>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal byte[] <compressData>__0;
            internal string localFilepath;
            internal Action<bool> onEnd;
            internal WWWForm <form>__0;
            internal Dictionary<string, string> <headers>__0;
            internal string serverFilename;
            internal string uploadURL;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <UploadFileToHttp>c__AnonStorey4 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <UploadFileToHttp>c__AnonStorey4();
                        this.$locvar0.<>f__ref$1 = this;
                        this.<compressData>__0 = null;
                        try
                        {
                            this.<compressData>__0 = CLZF.Compress(File.ReadAllBytes(this.localFilepath));
                        }
                        catch (Exception exception)
                        {
                            Debug.LogError($"{"HttpUtil.UploadFileToHttp"} : {exception}");
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                            break;
                        }
                        if ((this.<compressData>__0 != null) && (this.<compressData>__0.Length != 0))
                        {
                            this.<form>__0 = new WWWForm();
                            this.<headers>__0 = this.<form>__0.headers;
                            this.<headers>__0.Add("FileName", WWW.EscapeURL(this.serverFilename));
                            this.<form>__0.AddBinaryData("CompressedFile", HttpUtil.Byte2HexByte(this.<compressData>__0));
                            Debug.Log("compressed file lengh = " + this.<compressData>__0.Length);
                            this.$locvar0.www = null;
                            try
                            {
                                this.$locvar0.www = new WWW(this.uploadURL, this.<form>__0.data, this.<headers>__0);
                                this.$current = CorutineUtil.WaitUntil(new Func<bool>(this.$locvar0.<>m__0));
                                if (!this.$disposing)
                                {
                                    this.$PC = 1;
                                }
                                return true;
                            }
                            catch (Exception exception2)
                            {
                                Debug.LogError($"{"HttpUtil.UploadFileToHttp"} : {exception2}");
                                if (this.onEnd != null)
                                {
                                    this.onEnd(false);
                                }
                            }
                        }
                        else if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                        break;

                    case 1:
                        if (!string.IsNullOrEmpty(this.$locvar0.www.error))
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                            Debug.LogWarning(string.Format("UploadFile {0} failed, error:{0}", this.$locvar0.www.error));
                        }
                        else
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(true);
                            }
                            Debug.Log($"Response: {this.$locvar0.www.text}");
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <UploadFileToHttp>c__AnonStorey4
            {
                internal WWW www;
                internal HttpUtil.<UploadFileToHttp>c__Iterator1 <>f__ref$1;

                internal bool <>m__0() => 
                    this.www.isDone;
            }
        }
    }
}

