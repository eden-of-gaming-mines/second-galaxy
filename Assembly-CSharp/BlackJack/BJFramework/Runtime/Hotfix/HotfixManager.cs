﻿namespace BlackJack.BJFramework.Runtime.Hotfix
{
    using Assets.XIL;
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using wxb;

    public class HotfixManager
    {
        private static HotfixManager m_instance;
        private HotfixResLoader m_hotfixResLoader;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateHotfixManager;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_CallBaseMethod;
        private static DelegateBridge __Hotfix_GetMethod;
        private static DelegateBridge __Hotfix_TypeWithoutRefEquals;
        private static DelegateBridge __Hotfix_CreateObject;

        [MethodImpl(0x8000)]
        private HotfixManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public static TDel CallBaseMethod<T, TDel>(T obj, string methodName, ParamType[] types = null) where TDel: class
        {
        }

        [MethodImpl(0x8000)]
        public static HotfixManager CreateHotfixManager()
        {
            DelegateBridge bridge = __Hotfix_CreateHotfixManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp85();
            }
            if (m_instance == null)
            {
                m_instance = new HotfixManager();
            }
            return m_instance;
        }

        [MethodImpl(0x8000)]
        public static T CreateObject<T>(params object[] p)
        {
        }

        [MethodImpl(0x8000)]
        public static MethodInfo GetMethod(System.Type type, string methodName, ParamType[] types = null)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator Init()
        {
        }

        [MethodImpl(0x8000)]
        private static bool TypeWithoutRefEquals(System.Type type, bool isRef, System.Type comparedType)
        {
        }

        public static HotfixManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <Init>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string <assemblyName>__0;
            internal HotfixManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$this.m_hotfixResLoader = new HotfixManager.HotfixResLoader();
                        this.$current = this.$this.m_hotfixResLoader.Init();
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        goto TR_0001;

                    case 1:
                        if (this.$this.m_hotfixResLoader.IsHotfixAssemblyLoadSuccessed())
                        {
                            this.<assemblyName>__0 = GameManager.Instance.GameClientSetting.HotfixSettings.HotfixAssemblyName;
                            hotMgr.Init(this.<assemblyName>__0, this.$this.m_hotfixResLoader);
                            this.$this.m_hotfixResLoader.Clear();
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 2;
                            }
                            goto TR_0001;
                        }
                        else
                        {
                            Debug.LogWarning("HotfixManager Init hotfixResLoader failed!");
                            this.$this.m_hotfixResLoader.Clear();
                        }
                        break;

                    case 2:
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            TR_0001:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public class HotfixResLoader : IResLoad
        {
            private MemoryStream m_hotfixAssemblyStream;
            private MemoryStream m_hotfixAssemblyPdbStream;
            private MemoryStream m_hotfixAssemblyMdbStream;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_GetStream;
            private static DelegateBridge __Hotfix_Init;
            private static DelegateBridge __Hotfix_IsHotfixAssemblyLoadSuccessed;
            private static DelegateBridge __Hotfix_Clear;
            private static DelegateBridge __Hotfix_LoadAssemblyFromAsset;
            private static DelegateBridge __Hotfix_LoadAssemblyFromFile;

            [MethodImpl(0x8000)]
            public void Clear()
            {
            }

            [MethodImpl(0x8000)]
            public Stream GetStream(string path)
            {
            }

            [MethodImpl(0x8000), DebuggerHidden]
            public IEnumerator Init()
            {
            }

            [MethodImpl(0x8000)]
            public bool IsHotfixAssemblyLoadSuccessed()
            {
            }

            [MethodImpl(0x8000), DebuggerHidden]
            private IEnumerator LoadAssemblyFromAsset(string assetName, Action<MemoryStream> onEnd)
            {
            }

            [MethodImpl(0x8000)]
            private MemoryStream LoadAssemblyFromFile(string path)
            {
            }

            [CompilerGenerated]
            private sealed class <Init>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal GameClientSetting <gameClientSetting>__0;
                internal string <dllAssetName>__0;
                internal HotfixManager.HotfixResLoader $this;
                internal object $current;
                internal bool $disposing;
                internal int $PC;

                internal void <>m__0(MemoryStream res)
                {
                    this.$this.m_hotfixAssemblyStream = res;
                }

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                [MethodImpl(0x8000)]
                public bool MoveNext()
                {
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;
            }

            [CompilerGenerated]
            private sealed class <LoadAssemblyFromAsset>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
            {
                internal string assetName;
                internal Action<MemoryStream> onEnd;
                internal object $current;
                internal bool $disposing;
                internal int $PC;
                private <LoadAssemblyFromAsset>c__AnonStorey2 $locvar0;

                [DebuggerHidden]
                public void Dispose()
                {
                    this.$disposing = true;
                    this.$PC = -1;
                }

                public bool MoveNext()
                {
                    uint num = (uint) this.$PC;
                    this.$PC = -1;
                    switch (num)
                    {
                        case 0:
                            this.$locvar0 = new <LoadAssemblyFromAsset>c__AnonStorey2();
                            this.$locvar0.<>f__ref$1 = this;
                            this.$locvar0.asset = null;
                            this.$current = ResourceManager.Instance.LoadAsset<BytesScripableObject>(this.assetName, new Action<string, BytesScripableObject>(this.$locvar0.<>m__0), false, false);
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            return true;

                        case 1:
                            if (this.$locvar0.asset == null)
                            {
                                Debug.LogError($"HotfixResLoader load asset fail assetName={this.assetName}");
                            }
                            else
                            {
                                if (this.onEnd != null)
                                {
                                    this.onEnd(new MemoryStream(this.$locvar0.asset.GetBytes()));
                                }
                                this.$PC = -1;
                            }
                            break;

                        default:
                            break;
                    }
                    return false;
                }

                [DebuggerHidden]
                public void Reset()
                {
                    throw new NotSupportedException();
                }

                object IEnumerator<object>.Current =>
                    this.$current;

                object IEnumerator.Current =>
                    this.$current;

                private sealed class <LoadAssemblyFromAsset>c__AnonStorey2
                {
                    internal BytesScripableObject asset;
                    internal HotfixManager.HotfixResLoader.<LoadAssemblyFromAsset>c__Iterator1 <>f__ref$1;

                    internal void <>m__0(string lpath, BytesScripableObject lasset)
                    {
                        this.asset = lasset;
                    }
                }
            }
        }
    }
}

