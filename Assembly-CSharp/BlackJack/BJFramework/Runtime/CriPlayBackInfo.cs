﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CriPlayBackInfo
    {
        protected string m_sheetName;
        protected string m_cueName;
        protected Mode m_mode;
        protected List<CriAtomExPlayback> m_playbackList;
        protected HashSet<uint> m_stoppedPlaybackIDSet;
        protected DateTime m_playBackInfoDeadOutTime;
        protected const float PlayBackInfoDeadDelayTime = 30f;
        protected bool m_isAllPlayBackStoped;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddPlayBack;
        private static DelegateBridge __Hotfix_StopPlayBack;
        private static DelegateBridge __Hotfix_IsPlayBackPlaying;
        private static DelegateBridge __Hotfix_CheckStatus;
        private static DelegateBridge __Hotfix_IsPlaybackStopped;
        private static DelegateBridge __Hotfix_get_SheetName;

        [MethodImpl(0x8000)]
        public CriPlayBackInfo(string sheetName, string cueName, CriAtomExPlayback playback, Mode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void AddPlayBack(CriAtomExPlayback playBack)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckStatus(List<CriAtomExPlayback> removePlayBackList)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPlayBackPlaying()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPlaybackStopped(CriAtomExPlayback playback)
        {
        }

        [MethodImpl(0x8000)]
        public void StopPlayBack(int stopCount = -1)
        {
        }

        public string SheetName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum Mode
        {
            SinglePlayBack,
            MultiPlayBack
        }
    }
}

