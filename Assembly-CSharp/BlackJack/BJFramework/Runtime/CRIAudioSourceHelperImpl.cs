﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [RequireComponent(typeof(CriAtomSource)), AddComponentMenu("CRIWARE/BlackJack/CRI Audio Helper Impl")]
    public class CRIAudioSourceHelperImpl : MonoBehaviour
    {
        private CriAtomSource m_criSoundSource;
        private BlackJack.BJFramework.Runtime.CRIProvider m_CRIProvider;
        [Header("播放延时")]
        public float m_delayTime;
        public bool m_playOnWake;
        public bool m_playOnEnable;
        public string m_criCueName;
        public string m_criEndCueName;
        public string m_criCueParamName;
        [Header("音效播放的选择-立即/冲突/替换")]
        public PlaySoundOption m_playSoundOption;
        [Header("是否循环")]
        public bool m_isLoop;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_OnDestroy;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_PlayEnd;
        private static DelegateBridge __Hotfix_PlaySoundByName;
        private static DelegateBridge __Hotfix_StopAll;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_SetSoundParameter;
        private static DelegateBridge __Hotfix_PlayWithDelay;
        private static DelegateBridge __Hotfix_IsSourceReady;
        private static DelegateBridge __Hotfix_get_CRIProvider;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSourceReady()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDestroy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void Play()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySoundByName(string soundName, PlaySoundOption playSoundOption, bool isLoop)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator PlayWithDelay()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundParameter(string paraName, float value)
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void Stop(string souneName)
        {
        }

        [MethodImpl(0x8000)]
        public void StopAll()
        {
        }

        private BlackJack.BJFramework.Runtime.CRIProvider CRIProvider
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PlayWithDelay>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal CRIAudioSourceHelperImpl $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

