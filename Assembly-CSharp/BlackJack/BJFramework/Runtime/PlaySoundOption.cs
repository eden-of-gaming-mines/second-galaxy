﻿namespace BlackJack.BJFramework.Runtime
{
    using System;

    public enum PlaySoundOption
    {
        Immediately,
        DodgeSameSound,
        ReplaceSameSound
    }
}

