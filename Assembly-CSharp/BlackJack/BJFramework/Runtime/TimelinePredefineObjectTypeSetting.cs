﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class TimelinePredefineObjectTypeSetting : ScriptableObject
    {
        public List<TimelinePredefineObjectTypeGroup> ObjectTypeGroupList;

        [Serializable]
        public class TimelinePredefineObjectTypeGroup
        {
            public string Name;
            public List<string> ObjectTypeList;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

