﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class FpsHelper
    {
        private static float m_averageFps = 30f;
        private static int m_averageTickCount;
        private static float m_lastAverageTime;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetAverageFps;
        private static DelegateBridge __Hotfix_CalcAverageFps;

        [MethodImpl(0x8000)]
        private static void CalcAverageFps()
        {
            DelegateBridge bridge = __Hotfix_CalcAverageFps;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp19();
            }
            else
            {
                m_averageTickCount++;
                float num = Time.realtimeSinceStartup - m_lastAverageTime;
                if (num > 1f)
                {
                    m_averageFps = ((float) m_averageTickCount) / num;
                    m_lastAverageTime = Time.realtimeSinceStartup;
                    m_averageTickCount = 0;
                }
            }
        }

        [MethodImpl(0x8000)]
        public static float GetAverageFps()
        {
        }

        [MethodImpl(0x8000)]
        public static void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp19();
            }
            else
            {
                CalcAverageFps();
            }
        }
    }
}

