﻿namespace BlackJack.BJFramework.Runtime
{
    using BlackJack.BJFramework.Runtime.Tools;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ServerSettingManager
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <VaildAssetBundleDownLoadUrl>k__BackingField;
        protected Dictionary<string, string> m_serverSettingInfoDict = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        private static ServerSettingManager m_instance;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateManager;
        private static DelegateBridge __Hotfix_InitServerSettingManager;
        private static DelegateBridge __Hotfix_UpdateAssetBundleDownLoadUrl;
        private static DelegateBridge __Hotfix_LoadGameServerConfig;
        private static DelegateBridge __Hotfix_ParseGameServerConfig;
        private static DelegateBridge __Hotfix_SetGameServerConfigExtraData;
        private static DelegateBridge __Hotfix_UpdateServerSetting;
        private static DelegateBridge __Hotfix_ParseServerSetting;
        private static DelegateBridge __Hotfix_SetServerSettingExtraData;
        private static DelegateBridge __Hotfix_UpdateVaildAssetBundleDownLoadUrl;
        private static DelegateBridge __Hotfix_ParseAssetBundleDownLoadList;
        private static DelegateBridge __Hotfix_SetServerSettingInfoValue;
        private static DelegateBridge __Hotfix_GetServerSettingInfoValue;
        private static DelegateBridge __Hotfix_CheckServerSettingInfo;
        private static DelegateBridge __Hotfix_DefaultParseFileStringContent;
        private static DelegateBridge __Hotfix_DefaultSplitStringContent;
        private static DelegateBridge __Hotfix_IsEnableBundleDownLoad;
        private static DelegateBridge __Hotfix_get_VaildAssetBundleDownLoadUrl;
        private static DelegateBridge __Hotfix_set_VaildAssetBundleDownLoadUrl;
        private static DelegateBridge __Hotfix_get_ServerSettingUrlKey;
        private static DelegateBridge __Hotfix_get_LoginAgentUrlKey;
        private static DelegateBridge __Hotfix_get_AssetBundleDownLoadServerListKey;
        private static DelegateBridge __Hotfix_get_LoginAnnouncementUrlKey;
        private static DelegateBridge __Hotfix_get_ServerListUrlKey;
        private static DelegateBridge __Hotfix_get_ServerInfoListKey;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        public ServerSettingManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public bool CheckServerSettingInfo(string key)
        {
            DelegateBridge bridge = __Hotfix_CheckServerSettingInfo;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp16(this, key);
            }
            if (!string.IsNullOrEmpty(this.GetServerSettingInfoValue(this.ServerSettingUrlKey)))
            {
                return true;
            }
            Debug.LogError($"ServerSettingManager::CheckServerSettingInfo error key {key} has no value");
            return false;
        }

        [MethodImpl(0x8000)]
        public static T CreateManager<T>() where T: ServerSettingManager, new()
        {
            DelegateBridge bridge = __Hotfix_CreateManager;
            if (bridge != null)
            {
                bridge.InvokeSessionStart();
                bridge.Invoke(1);
                return bridge.InvokeSessionEndWithResult<T>();
            }
            if (m_instance != null)
            {
                return (m_instance as T);
            }
            m_instance = Activator.CreateInstance<T>();
            return (T) m_instance;
        }

        [MethodImpl(0x8000)]
        protected Dictionary<string, string> DefaultParseFileStringContent(string content)
        {
            DelegateBridge bridge = __Hotfix_DefaultParseFileStringContent;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp166(this, content);
            }
            string[] separator = new string[] { "\n", "\r\n" };
            List<string> list = new List<string>(content.Split(separator, StringSplitOptions.RemoveEmptyEntries));
            int index = 0;
            while (index < list.Count)
            {
                if (list[index].StartsWith("//"))
                {
                    list.RemoveAt(index);
                    continue;
                }
                index++;
            }
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            foreach (string str in list)
            {
                string[] textArray2 = new string[] { "=" };
                string[] strArray = str.Split(textArray2, StringSplitOptions.RemoveEmptyEntries);
                if ((strArray == null) || (strArray.Length < 2))
                {
                    Debug.LogError("ServerSettingManager::DefaultParseFileStringContent::Content Error: " + str);
                    continue;
                }
                dictionary[strArray[0].Trim()] = strArray[1].Trim();
            }
            return dictionary;
        }

        [MethodImpl(0x8000)]
        protected List<string> DefaultSplitStringContent(string content)
        {
            DelegateBridge bridge = __Hotfix_DefaultSplitStringContent;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp167(this, content);
            }
            List<string> list = new List<string>();
            char[] separator = new char[] { ',' };
            string[] strArray = content.Trim().Split(separator);
            if (strArray != null)
            {
                foreach (string str in strArray)
                {
                    list.Add(str.Trim());
                }
            }
            return list;
        }

        [MethodImpl(0x8000)]
        public string GetServerSettingInfoValue(string key)
        {
            DelegateBridge bridge = __Hotfix_GetServerSettingInfoValue;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp44(this, key);
            }
            string str = string.Empty;
            if (this.m_serverSettingInfoDict.TryGetValue(key, out str))
            {
                return str;
            }
            Debug.LogError($"ServerSettingManager::GetServerSettingInfoValue error key:{key} not exist");
            return string.Empty;
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator InitServerSettingManager()
        {
            DelegateBridge bridge = __Hotfix_InitServerSettingManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <InitServerSettingManager>c__Iterator0 { $this = this };
        }

        [MethodImpl(0x8000)]
        protected bool IsEnableBundleDownLoad()
        {
            DelegateBridge bridge = __Hotfix_IsEnableBundleDownLoad;
            return ((bridge == null) ? !GameManager.Instance.GameClientSetting.ResourcesSetting.DisableAssetBundleDownload : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator LoadGameServerConfig()
        {
            DelegateBridge bridge = __Hotfix_LoadGameServerConfig;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <LoadGameServerConfig>c__Iterator2 { $this = this };
        }

        [MethodImpl(0x8000)]
        protected virtual List<string> ParseAssetBundleDownLoadList(string content)
        {
            DelegateBridge bridge = __Hotfix_ParseAssetBundleDownLoadList;
            return ((bridge == null) ? this.DefaultSplitStringContent(content) : bridge.__Gen_Delegate_Imp167(this, content));
        }

        [MethodImpl(0x8000)]
        protected virtual Dictionary<string, string> ParseGameServerConfig(string content)
        {
            DelegateBridge bridge = __Hotfix_ParseGameServerConfig;
            return ((bridge == null) ? this.DefaultParseFileStringContent(content) : bridge.__Gen_Delegate_Imp166(this, content));
        }

        [MethodImpl(0x8000)]
        protected virtual Dictionary<string, string> ParseServerSetting(string content)
        {
            DelegateBridge bridge = __Hotfix_ParseServerSetting;
            return ((bridge == null) ? this.DefaultParseFileStringContent(content) : bridge.__Gen_Delegate_Imp166(this, content));
        }

        [MethodImpl(0x8000)]
        protected virtual void SetGameServerConfigExtraData(Dictionary<string, string> contentDict)
        {
            DelegateBridge bridge = __Hotfix_SetGameServerConfigExtraData;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, contentDict);
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void SetServerSettingExtraData(Dictionary<string, string> contentDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetServerSettingInfoValue(string key, Dictionary<string, string> contextDict)
        {
            DelegateBridge bridge = __Hotfix_SetServerSettingInfoValue;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, key, contextDict);
            }
            else
            {
                string str;
                if (!contextDict.TryGetValue(key, out str))
                {
                    Debug.LogError($"ServerSettingManager::SetServerSettingInfoValue error key:{key} not in contextDict");
                }
                else
                {
                    this.m_serverSettingInfoDict[key] = str;
                    Debug.Log($"ServerSettingManager::SetServerSettingInfoValue  key:{key}, value:{str}");
                }
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator UpdateAssetBundleDownLoadUrl(Action onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator UpdateServerSetting()
        {
            DelegateBridge bridge = __Hotfix_UpdateServerSetting;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <UpdateServerSetting>c__Iterator3 { $this = this };
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator UpdateVaildAssetBundleDownLoadUrl()
        {
            DelegateBridge bridge = __Hotfix_UpdateVaildAssetBundleDownLoadUrl;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <UpdateVaildAssetBundleDownLoadUrl>c__Iterator4 { $this = this };
        }

        public string VaildAssetBundleDownLoadUrl
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_VaildAssetBundleDownLoadUrl;
                return ((bridge == null) ? this.<VaildAssetBundleDownLoadUrl>k__BackingField : bridge.__Gen_Delegate_Imp33(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_VaildAssetBundleDownLoadUrl;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<VaildAssetBundleDownLoadUrl>k__BackingField = value;
                }
            }
        }

        public virtual string ServerSettingUrlKey
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_ServerSettingUrlKey;
                return ((bridge == null) ? "ServerSettingUrl" : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        public virtual string LoginAgentUrlKey
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_LoginAgentUrlKey;
                return ((bridge == null) ? "LoginAgent" : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        public virtual string AssetBundleDownLoadServerListKey
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_AssetBundleDownLoadServerListKey;
                return ((bridge == null) ? "AssetBundleDownLoadServerList" : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        public virtual string LoginAnnouncementUrlKey
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_LoginAnnouncementUrlKey;
                return ((bridge == null) ? "LoginAnnouncementUrl" : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        public virtual string ServerListUrlKey
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_ServerListUrlKey;
                return ((bridge == null) ? "ServerListUrl" : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        public virtual string ServerInfoListKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static ServerSettingManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp168());
            }
        }

        [CompilerGenerated]
        private sealed class <InitServerSettingManager>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal ServerSettingManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.$this.LoadGameServerConfig();
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        break;

                    case 1:
                        this.$current = this.$this.UpdateServerSetting();
                        if (!this.$disposing)
                        {
                            this.$PC = 2;
                        }
                        break;

                    case 2:
                        this.$PC = -1;
                        goto TR_0000;

                    default:
                        goto TR_0000;
                }
                return true;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <LoadGameServerConfig>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal List<string> <gameServerConfigPathList>__0;
            internal string <content>__0;
            internal List<string>.Enumerator $locvar0;
            internal string <serverConfigPath>__1;
            internal WWW <www>__2;
            internal Dictionary<string, string> <contentDict>__0;
            internal ServerSettingManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                    {
                        Debug.Log("ServerSettingManager::LoadGameServerConfig");
                        List<string> list = new List<string> { 
                            "Config/GameConfig.txt",
                            "Config/GameConfig2.txt"
                        };
                        this.<gameServerConfigPathList>__0 = list;
                        this.<content>__0 = string.Empty;
                        this.$locvar0 = this.<gameServerConfigPathList>__0.GetEnumerator();
                        num = 0xfffffffd;
                        break;
                    }
                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        case 1:
                            goto TR_000F;

                        default:
                            break;
                    }
                    goto TR_0012;
                TR_000F:
                    if (!this.<www>__2.isDone)
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        flag = true;
                        return true;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(this.<www>__2.error) || string.IsNullOrEmpty(this.<www>__2.text))
                        {
                            Debug.Log($"ServerSettingManager::LoadGameServerConfig failed = {this.<www>__2.error} or www.text is empty");
                        }
                        else
                        {
                            this.<content>__0 = this.<www>__2.text;
                        }
                        if (string.IsNullOrEmpty(this.<content>__0))
                        {
                            goto TR_0012;
                        }
                        else
                        {
                            Debug.Log("ServerSettingManager::LoadGameServerConfig success from path:" + this.<serverConfigPath>__1);
                        }
                    }
                    goto TR_0001;
                TR_0012:
                    while (true)
                    {
                        if (this.$locvar0.MoveNext())
                        {
                            this.<serverConfigPath>__1 = this.$locvar0.Current;
                            this.<www>__2 = new WWW(Util.GetFileStreamingAssetsPath4WWW(this.<serverConfigPath>__1));
                        }
                        else
                        {
                            goto TR_0001;
                        }
                        break;
                    }
                    goto TR_000F;
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                goto TR_0001;
            TR_0000:
                return false;
            TR_0001:
                this.<contentDict>__0 = this.$this.ParseGameServerConfig(this.<content>__0);
                this.$this.SetServerSettingInfoValue(this.$this.ServerSettingUrlKey, this.<contentDict>__0);
                this.$this.SetGameServerConfigExtraData(this.<contentDict>__0);
                this.$PC = -1;
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <UpdateAssetBundleDownLoadUrl>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action onEnd;
            internal ServerSettingManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.$this.UpdateServerSetting();
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        if (this.onEnd != null)
                        {
                            this.onEnd();
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <UpdateServerSetting>c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string <url>__0;
            internal Dictionary<string, string> <contentDict>__0;
            internal ServerSettingManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <UpdateServerSetting>c__AnonStorey5 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <UpdateServerSetting>c__AnonStorey5();
                        this.$locvar0.<>f__ref$3 = this;
                        if (this.$this.CheckServerSettingInfo(this.$this.ServerSettingUrlKey))
                        {
                            this.$locvar0.serverSettingContent = string.Empty;
                            this.<url>__0 = this.$this.GetServerSettingInfoValue(this.$this.ServerSettingUrlKey);
                            Debug.Log("ServerSettingManager::Start Download ServerSetting from: " + this.<url>__0);
                            this.$current = HttpUtil.DownloadFileFromHttp(this.<url>__0, new Action<bool, WWW>(this.$locvar0.<>m__0), null, 3, 0x3a98);
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            goto TR_0001;
                        }
                        goto TR_0000;

                    case 1:
                        if (!string.IsNullOrEmpty(this.$locvar0.serverSettingContent))
                        {
                            Debug.Log($"ServerSettingManager::Download ServerSetting from: {this.<url>__0} success ");
                            this.<contentDict>__0 = this.$this.ParseServerSetting(this.$locvar0.serverSettingContent);
                            this.$this.SetServerSettingInfoValue(this.$this.LoginAgentUrlKey, this.<contentDict>__0);
                            this.$this.SetServerSettingInfoValue(this.$this.AssetBundleDownLoadServerListKey, this.<contentDict>__0);
                            this.$this.SetServerSettingInfoValue(this.$this.LoginAnnouncementUrlKey, this.<contentDict>__0);
                            this.$this.SetServerSettingInfoValue(this.$this.ServerListUrlKey, this.<contentDict>__0);
                            this.$this.SetServerSettingExtraData(this.<contentDict>__0);
                            if (this.$this.IsEnableBundleDownLoad())
                            {
                                this.$current = this.$this.UpdateVaildAssetBundleDownLoadUrl();
                                if (!this.$disposing)
                                {
                                    this.$PC = 2;
                                }
                                goto TR_0001;
                            }
                        }
                        else
                        {
                            Debug.LogError($"ServerSettingManager::Download ServerSetting from: {this.<url>__0} failed ");
                            goto TR_0000;
                        }
                        break;

                    case 2:
                        break;

                    default:
                        goto TR_0000;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            TR_0001:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <UpdateServerSetting>c__AnonStorey5
            {
                internal string serverSettingContent;
                internal ServerSettingManager.<UpdateServerSetting>c__Iterator3 <>f__ref$3;

                internal void <>m__0(bool res, WWW www)
                {
                    if (res)
                    {
                        this.serverSettingContent = www.text;
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateVaildAssetBundleDownLoadUrl>c__Iterator4 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal List<string> <bundleUrlList>__0;
            internal List<string>.Enumerator $locvar0;
            internal string <bundleUrl>__1;
            internal ServerSettingManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <UpdateVaildAssetBundleDownLoadUrl>c__AnonStorey6 $locvar1;
            private <UpdateVaildAssetBundleDownLoadUrl>c__AnonStorey7 $locvar2;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.$locvar1 = new <UpdateVaildAssetBundleDownLoadUrl>c__AnonStorey6();
                        this.$locvar1.<>f__ref$4 = this;
                        this.$this.VaildAssetBundleDownLoadUrl = string.Empty;
                        if (this.$this.CheckServerSettingInfo(this.$this.AssetBundleDownLoadServerListKey))
                        {
                            this.<bundleUrlList>__0 = this.$this.ParseAssetBundleDownLoadList(this.$this.GetServerSettingInfoValue(this.$this.AssetBundleDownLoadServerListKey));
                            if (this.<bundleUrlList>__0.Count != 0)
                            {
                                this.$locvar1.isConnectSuccess = false;
                                this.$locvar0 = this.<bundleUrlList>__0.GetEnumerator();
                                num = 0xfffffffd;
                                goto TR_0016;
                            }
                            else
                            {
                                Debug.LogError("ServerSettingManager::UpdateVaildAssetBundleDownLoadUrl error: bundleUrlList.Count == 0");
                            }
                        }
                        else
                        {
                            Debug.LogError("ServerSettingManager::UpdateVaildAssetBundleDownLoadUrl error: AssetBundleDownLoadServerList == null");
                        }
                        break;

                    case 1:
                        goto TR_0016;

                    default:
                        break;
                }
            TR_0000:
                return false;
            TR_0005:
                if (!this.$locvar1.isConnectSuccess)
                {
                    Debug.LogError("ServerSettingManager::UpdateVaildAssetBundleDownLoadUrl error: not reachable url");
                }
                this.$PC = -1;
                goto TR_0000;
            TR_0016:
                try
                {
                    switch (num)
                    {
                        case 1:
                            if (!this.$locvar1.isConnectSuccess)
                            {
                                break;
                            }
                            this.$this.VaildAssetBundleDownLoadUrl = this.<bundleUrl>__1;
                            goto TR_0005;

                        default:
                            break;
                    }
                    while (true)
                    {
                        if (!this.$locvar0.MoveNext())
                        {
                            break;
                        }
                        this.<bundleUrl>__1 = this.$locvar0.Current;
                        this.$locvar2 = new <UpdateVaildAssetBundleDownLoadUrl>c__AnonStorey7();
                        this.$locvar2.<>f__ref$4 = this;
                        this.$locvar2.<>f__ref$6 = this.$locvar1;
                        char[] trimChars = new char[] { '/' };
                        this.$locvar2.url = $"{this.<bundleUrl>__1.Trim(trimChars)}/{Util.GetCurrentTargetPlatform()}/{PathHelper.BundleDataVersionFileName}";
                        if (!GameManager.Instance.GameClientSetting.DisableUnimportantLog)
                        {
                            Debug.Log($"ServerSettingManager::UpdateVaildAssetBundleDownLoadUrl: try download from {this.$locvar2.url} ");
                        }
                        if (!string.IsNullOrEmpty(this.$locvar2.url))
                        {
                            this.$current = HttpUtil.DownloadFileFromHttp(this.$locvar2.url, new Action<bool, WWW>(this.$locvar2.<>m__0), null, 3, 0x3a98);
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            flag = true;
                            return true;
                        }
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                goto TR_0005;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <UpdateVaildAssetBundleDownLoadUrl>c__AnonStorey6
            {
                internal bool isConnectSuccess;
                internal ServerSettingManager.<UpdateVaildAssetBundleDownLoadUrl>c__Iterator4 <>f__ref$4;
            }

            private sealed class <UpdateVaildAssetBundleDownLoadUrl>c__AnonStorey7
            {
                internal string url;
                internal ServerSettingManager.<UpdateVaildAssetBundleDownLoadUrl>c__Iterator4 <>f__ref$4;
                internal ServerSettingManager.<UpdateVaildAssetBundleDownLoadUrl>c__Iterator4.<UpdateVaildAssetBundleDownLoadUrl>c__AnonStorey6 <>f__ref$6;

                internal void <>m__0(bool ret, WWW www)
                {
                    this.<>f__ref$6.isConnectSuccess = ret;
                    Debug.Log($"ServerSettingManager::UpdateVaildAssetBundleDownLoadUrl:  download from {this.url} {this.<>f__ref$6.isConnectSuccess}");
                }
            }
        }
    }
}

