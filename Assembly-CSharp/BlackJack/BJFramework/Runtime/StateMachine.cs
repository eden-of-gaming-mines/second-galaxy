﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StateMachine
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <State>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetStateCheck;
        private static DelegateBridge __Hotfix_SetStateWithoutCheck;
        private static DelegateBridge __Hotfix_EventCheck;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;

        [MethodImpl(0x8000)]
        public virtual bool EventCheck(int commingEvent)
        {
        }

        [MethodImpl(0x8000)]
        public virtual int SetStateCheck(int commingEvent, int newState = -1, bool testOnly = false)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetStateWithoutCheck(int newState)
        {
        }

        public int State
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }
    }
}

