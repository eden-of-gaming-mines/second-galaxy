﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class TimelineActorBindNodeBase : MonoBehaviour
    {
        [Header("General"), SerializeField]
        protected List<NodeStruct> m_nodes;
        private static DelegateBridge __Hotfix_GetNodeByName;
        private static DelegateBridge __Hotfix_GetNodeNameByNode;

        [MethodImpl(0x8000)]
        public virtual Transform GetNodeByName(string nodeName)
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetNodeNameByNode(Transform trans)
        {
        }

        [Serializable, StructLayout(LayoutKind.Sequential)]
        public struct NodeStruct
        {
            public string m_nodeName;
            public Transform m_node;
        }
    }
}

