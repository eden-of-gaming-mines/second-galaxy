﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class SceneSettings
    {
        public bool UseOrthographicForUILayer;
        public int DesignScreenWidthPercent = 80;
        public int DesignScreenHeightPercent = 80;
        public int DesignScreenWidth = 0x780;
        public int DesignScreenHeight = 0x438;
        public int UIDesignScreenWidth = 0x500;
        public int UIDesignScreenHeight = 720;
        public int TrigerWidth2ShrinkScale;
        public int TrigerHeight2ShrinkScale;
        public Vector3 SceneLayerOffset = new Vector3(1000f, 0f, 0f);
        public bool EnableLayerReserve;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public SceneSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

