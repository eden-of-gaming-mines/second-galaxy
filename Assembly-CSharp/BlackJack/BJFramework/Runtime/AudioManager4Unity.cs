﻿namespace BlackJack.BJFramework.Runtime
{
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Audio;

    public class AudioManager4Unity : IAudioManager, ITickable
    {
        private static AudioManager4Unity m_instance;
        private readonly TinyCorutineHelper m_corutineHelper;
        private GameObject m_audioRoot;
        private AudioSource m_backGroundAudioSource1;
        private AudioSource m_backGroundAudioSource2;
        private AudioSource m_currBackGroundAudioSource;
        private AudioSource m_oldBackGroundAudioSource;
        private DateTime? m_bgmSwapTime;
        private AudioSource m_soundAudioSource;
        private AudioSource m_playerVoiceAudioSource;
        private AudioSource m_speechAudioSource;
        private AudioMixer m_mainAudioMixer;
        private AudioListener m_mainAudioListener;
        private bool m_muteBackGroundMusic;
        private bool m_muteSound;
        private bool m_mutePlayerVoice;
        private bool m_muteSpeech;
        private float m_backGroundMusicVolume;
        private float m_soundVolume;
        private float m_playerVoiceVolume;
        private float m_speechVolume;
        private const float BGMSwapTime = 1f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateAudioManager;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_EnableDefaultAudioListener;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetRealSoundAudioSourceVolume;
        private static DelegateBridge __Hotfix_GetPlayerVoiceAudioSourceVolume;
        private static DelegateBridge __Hotfix_PlaySoundIndependently;
        private static DelegateBridge __Hotfix_PlaySoundDodgeSame;
        private static DelegateBridge __Hotfix_PlaySoundReplaceSame;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_StopAll;
        private static DelegateBridge __Hotfix_SetVolumeToZero;
        private static DelegateBridge __Hotfix_SetMuteState;
        private static DelegateBridge __Hotfix_SetVolume;
        private static DelegateBridge __Hotfix_PlayBackGroundMusic;
        private static DelegateBridge __Hotfix_PlaySound;
        private static DelegateBridge __Hotfix_StopAudio;
        private static DelegateBridge __Hotfix_PlaySpeech;
        private static DelegateBridge __Hotfix_PlayPlayerVoice;
        private static DelegateBridge __Hotfix_IsAudioPlaying;
        private static DelegateBridge __Hotfix_SetSoundParameter;
        private static DelegateBridge __Hotfix_ClearSpeechCache;
        private static DelegateBridge __Hotfix_SetLocalization;
        private static DelegateBridge __Hotfix_EnableAudioPlugin;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_get_MainAudioListener;
        private static DelegateBridge __Hotfix_CreateMainAudioMixer;
        private static DelegateBridge __Hotfix_StopAllByAudioMixer;
        private static DelegateBridge __Hotfix_SetMuteByAudioMixer;
        private static DelegateBridge __Hotfix_SetVolumeByAudioMixer;
        private static DelegateBridge __Hotfix_StopBackGroundMusicByAudioMixer;
        private static DelegateBridge __Hotfix_StopSoundByAudioMixer;
        private static DelegateBridge __Hotfix_StopSpeechByAudioMixer;
        private static DelegateBridge __Hotfix_StopPlayerVoiceByAudioMixer;
        private static DelegateBridge __Hotfix_TransformNormalizeVolumeToRealAudioMixerGroupVolume;
        private static DelegateBridge __Hotfix_GetAudioMixerGroupBySubPathName;
        private static DelegateBridge __Hotfix_StopAllByAudioSource;
        private static DelegateBridge __Hotfix_SetMuteByAudioSource;
        private static DelegateBridge __Hotfix_SetVolumeByAudioSource;
        private static DelegateBridge __Hotfix_StopBackGroundMusicByAudioSource;
        private static DelegateBridge __Hotfix_StopSoundByAudioSource;
        private static DelegateBridge __Hotfix_StopSpeechByAudioSource;
        private static DelegateBridge __Hotfix_StopPlayerVoiceByAudioSource;

        [MethodImpl(0x8000)]
        private AudioManager4Unity()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearSpeechCache()
        {
        }

        [MethodImpl(0x8000)]
        public static AudioManager4Unity CreateAudioManager()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator CreateMainAudioMixer(Action<bool> onEnd, string mixerAssetPath)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableAudioPlugin(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableDefaultAudioListener(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private AudioMixerGroup GetAudioMixerGroupBySubPathName(AudioMixer audioMixer, string subPathName)
        {
        }

        [MethodImpl(0x8000)]
        private float GetPlayerVoiceAudioSourceVolume(float soundVolume)
        {
        }

        [MethodImpl(0x8000)]
        private float GetRealSoundAudioSourceVolume(float soundVolume)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initlize()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAudioPlaying(BlackJack.BJFramework.Runtime.AudioType audioType, string name = null)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayBackGroundMusic(string music, bool isInterruptSame)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayPlayerVoice(AudioClip sound, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySound(string sound, object audioClip, PlaySoundOption playOption, bool loop = false, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        private void PlaySoundDodgeSame(string sound, AudioClip audioClip = null, bool loop = false, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        private void PlaySoundIndependently(string sound, AudioClip audioClip = null, bool loop = false, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        private void PlaySoundReplaceSame(string sound, AudioClip audioClip = null, bool loop = false, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySpeech(string sound, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocalization(string localizationStr)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMuteByAudioMixer()
        {
        }

        [MethodImpl(0x8000)]
        private void SetMuteByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMuteState(bool muteBackGroundMusic, bool muteSound, bool mutePlayerVoice, bool muteSpeech)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundParameter(string paraName, float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVolume(BlackJack.BJFramework.Runtime.AudioType audioType, float volume)
        {
        }

        [MethodImpl(0x8000)]
        private void SetVolumeByAudioMixer()
        {
        }

        [MethodImpl(0x8000)]
        private void SetVolumeByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        public void SetVolumeToZero(bool muteBackGroundMusic, bool muteSound, bool mutePlayerVoice, bool muteSpeech)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator Start(Action<bool> onEnd, string mixerAssetPath, string criFilePrefix, Dictionary<string, string> configDataDict)
        {
        }

        [MethodImpl(0x8000)]
        public void StopAll()
        {
        }

        [MethodImpl(0x8000)]
        private void StopAllByAudioMixer()
        {
        }

        [MethodImpl(0x8000)]
        private void StopAllByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        public void StopAudio(BlackJack.BJFramework.Runtime.AudioType audioType, string audio = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StopBackGroundMusicByAudioMixer()
        {
        }

        [MethodImpl(0x8000)]
        private void StopBackGroundMusicByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        private void StopPlayerVoiceByAudioMixer()
        {
        }

        [MethodImpl(0x8000)]
        private void StopPlayerVoiceByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        private void StopSoundByAudioMixer()
        {
        }

        [MethodImpl(0x8000)]
        private void StopSoundByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        private void StopSpeechByAudioMixer()
        {
        }

        [MethodImpl(0x8000)]
        private void StopSpeechByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        private float TransformNormalizeVolumeToRealAudioMixerGroupVolume(float normalizeVolume)
        {
        }

        [MethodImpl(0x8000)]
        public void Uninitlize()
        {
        }

        public static AudioManager4Unity Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private AudioListener MainAudioListener
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreateMainAudioMixer>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string mixerAssetPath;
            internal Action<bool> onEnd;
            internal IEnumerator <iter>__0;
            internal AudioManager4Unity $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <CreateMainAudioMixer>c__AnonStorey3 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <CreateMainAudioMixer>c__AnonStorey3();
                        this.$locvar0.<>f__ref$1 = this;
                        this.$locvar0.mixerAssetPath = this.mixerAssetPath;
                        this.$locvar0.onEnd = this.onEnd;
                        this.<iter>__0 = ResourceManager.Instance.LoadAsset<AudioMixer>(this.$locvar0.mixerAssetPath, new Action<string, AudioMixer>(this.$locvar0.<>m__0), false, false);
                        this.$current = this.<iter>__0;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        if (this.$this.m_mainAudioMixer != null)
                        {
                            GameClientSetting gameClientSetting = GameManager.Instance.GameClientSetting;
                            AudioMixerGroup audioMixerGroupBySubPathName = this.$this.GetAudioMixerGroupBySubPathName(this.$this.m_mainAudioMixer, gameClientSetting.AudioSetting.AudioMixerBGMGroupSubPath);
                            if (audioMixerGroupBySubPathName == null)
                            {
                                Debug.LogError($"AudioManager.Start find AudioMixerBGMGroup fail subpath = {gameClientSetting.AudioSetting.AudioMixerBGMGroupSubPath}");
                            }
                            this.$this.m_backGroundAudioSource1.outputAudioMixerGroup = audioMixerGroupBySubPathName;
                            this.$this.m_backGroundAudioSource2.outputAudioMixerGroup = audioMixerGroupBySubPathName;
                            AudioMixerGroup group2 = this.$this.GetAudioMixerGroupBySubPathName(this.$this.m_mainAudioMixer, gameClientSetting.AudioSetting.AudioMixerSoundEffectGroupSubPath);
                            if (group2 == null)
                            {
                                Debug.LogError($"AudioManager.Start find AudioMixerSoundEffectGroup fail subpath = {gameClientSetting.AudioSetting.AudioMixerBGMGroupSubPath}");
                            }
                            this.$this.m_soundAudioSource.outputAudioMixerGroup = group2;
                            AudioMixerGroup group3 = this.$this.GetAudioMixerGroupBySubPathName(this.$this.m_mainAudioMixer, gameClientSetting.AudioSetting.AudioMixerPlayerVoiceGroupSubPath);
                            if (group3 == null)
                            {
                                Debug.LogError($"AudioManager.Start find AudioMixerPlayerVoiceGroup fail subpath = {gameClientSetting.AudioSetting.AudioMixerPlayerVoiceGroupSubPath}");
                            }
                            this.$this.m_playerVoiceAudioSource.outputAudioMixerGroup = group3;
                            AudioMixerGroup group4 = this.$this.GetAudioMixerGroupBySubPathName(this.$this.m_mainAudioMixer, gameClientSetting.AudioSetting.AudioMixerSpeechGroupSubPath);
                            if (group4 == null)
                            {
                                Debug.LogError($"AudioManager.Start find AudioMixerSpeechGroup fail subpath = {gameClientSetting.AudioSetting.AudioMixerSpeechGroupSubPath}");
                            }
                            this.$this.m_speechAudioSource.outputAudioMixerGroup = group4;
                            this.$locvar0.onEnd(true);
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <CreateMainAudioMixer>c__AnonStorey3
            {
                internal string mixerAssetPath;
                internal Action<bool> onEnd;
                internal AudioManager4Unity.<CreateMainAudioMixer>c__Iterator1 <>f__ref$1;

                internal void <>m__0(string path, AudioMixer asset)
                {
                    AudioMixer mixer = asset;
                    if (mixer == null)
                    {
                        Debug.LogError($"AudioManager.Start load AudioMixer fail path={this.mixerAssetPath}");
                        this.onEnd(false);
                    }
                    this.<>f__ref$1.$this.m_mainAudioMixer = mixer;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PlaySoundIndependently>c__AnonStorey2
        {
            internal float volume;
            internal AudioManager4Unity $this;

            internal void <>m__0(string path, AudioClip asset)
            {
                this.$this.m_soundAudioSource.PlayOneShot(asset, this.$this.GetRealSoundAudioSourceVolume(this.volume));
            }
        }

        [CompilerGenerated]
        private sealed class <Start>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string mixerAssetPath;
            internal Action<bool> onEnd;
            internal AudioManager4Unity $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

