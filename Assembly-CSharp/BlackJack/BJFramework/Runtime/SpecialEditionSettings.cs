﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class SpecialEditionSettings
    {
        [Header("华为定制版")]
        public bool ForHuaWei;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public SpecialEditionSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

