﻿namespace BlackJack.BJFramework.Runtime
{
    using System;
    using UnityEngine;

    public class GameClientSetting : ScriptableObject
    {
        public bool DisableUnityEngineLog;
        public bool Log2Persistent;
        public bool DisableUnimportantLog = true;
        public ResourcesSettings ResourcesSetting;
        public ResolutionSettings ResolutionSetting;
        public SceneSettings SceneSetting;
        public DynamicAssemblySettings DynamicAssemblySetting;
        public BlackJack.BJFramework.Runtime.HotfixSettings HotfixSettings;
        public ConfigDataSettings ConfigDataSetting;
        public BlackJack.BJFramework.Runtime.AudioSettings AudioSetting;
        public StringTableSettings StringTableSetting;
        [Header("日志上传的设置:")]
        public LogUpLoadSetting UpLoadSetting;
        public string UITaskRegisterTypeDNName = "Assembly-CSharp@BlackJack.ProjectSample.SampleGameUITaskRegister";
        public LoginSettings LoginSetting;
        public MemorySettings MemorySetting;
        public NetworkSettings NetworkSetting;
        public BlackJack.BJFramework.Runtime.PublishSetting PublishSetting;
        public SpecialEditionSettings m_specialEditionSettings;
        public static string defaultClientPrefPath = "Assets/GameProject/Resources/GameClientSetting";
    }
}

