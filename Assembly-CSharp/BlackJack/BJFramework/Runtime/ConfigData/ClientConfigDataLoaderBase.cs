﻿namespace BlackJack.BJFramework.Runtime.ConfigData
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using IL;
    using ProtoBuf;
    using ProtoBuf.Meta;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;

    public abstract class ClientConfigDataLoaderBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <ConfigDataAssetAllowNull>k__BackingField;
        protected GameClientSetting m_gameClientSetting;
        protected Dictionary<string, Action<BytesScriptableObjectMD5, bool>> m_deserializFuncDict;
        protected Dictionary<string, string> m_assetMD5Dict;
        private Dictionary<string, List<LazyLoadConfigAssetInfo>> m_lazyLoadConfigDataAssetPathDict;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <InitLoadDataCount>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CurrLoadedDataCount>k__BackingField;
        protected const float LoadAssetSizeLimitBeforeGC = 2097152f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartInitializeFromAsset;
        private static DelegateBridge __Hotfix_InitLoadConfigData;
        private static DelegateBridge __Hotfix_InitLoadConfigDataWorker;
        private static DelegateBridge __Hotfix_OnLoadConfigDataAssetEnd;
        private static DelegateBridge __Hotfix_InitLoadConfigDataForLuaDummyType;
        private static DelegateBridge __Hotfix_AddConfigDataItemForLuaDummyType;
        private static DelegateBridge __Hotfix_GetAllInitLoadConfigDataTypeNameForLuaDummy;
        private static DelegateBridge __Hotfix_IsUseMulitThreadOnInitLoadFromAssetEnd;
        private static DelegateBridge __Hotfix_LoadLazyLoadConfigDataAsset;
        private static DelegateBridge __Hotfix_GetLazyLoadConfigAssetNameByKey;
        private static DelegateBridge __Hotfix_GetLazyLoadConfigAssetNameListByConfigDataName;
        private static DelegateBridge __Hotfix_GetConfigDataPath;
        private static DelegateBridge __Hotfix_GetDeserializFunc4ConfigData;
        private static DelegateBridge __Hotfix_GetAllLazyLoadConfigDataAssetPath;
        private static DelegateBridge __Hotfix_GetLazyLoadConfigAssetInfo;
        private static DelegateBridge __Hotfix_DeserializeExtensionTableOnLoadFromAssetEnd;
        private static DelegateBridge __Hotfix_AllocStream4Deserialize;
        private static DelegateBridge __Hotfix_get_ConfigDataAssetAllowNull;
        private static DelegateBridge __Hotfix_set_ConfigDataAssetAllowNull;
        private static DelegateBridge __Hotfix_get_ConfigDataMD5Dict;
        private static DelegateBridge __Hotfix_get_InitLoadDataCount;
        private static DelegateBridge __Hotfix_set_InitLoadDataCount;
        private static DelegateBridge __Hotfix_get_CurrLoadedDataCount;
        private static DelegateBridge __Hotfix_set_CurrLoadedDataCount;

        [MethodImpl(0x8000)]
        public ClientConfigDataLoaderBase(string luaModuleName)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
        {
        }

        [MethodImpl(0x8000)]
        protected MemoryStream AllocStream4Deserialize(int length)
        {
        }

        [MethodImpl(0x8000)]
        public List<DummyType> DeserializeExtensionTableOnLoadFromAssetEnd(BytesScriptableObjectMD5 dataObj, string assetPath, string typeName)
        {
        }

        protected abstract HashSet<string> GetAllInitLoadConfigDataAssetPath();
        [MethodImpl(0x8000)]
        protected virtual List<string> GetAllInitLoadConfigDataTypeNameForLuaDummy()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual Dictionary<string, List<LazyLoadConfigAssetInfo>> GetAllLazyLoadConfigDataAssetPath()
        {
        }

        [MethodImpl(0x8000)]
        public string GetConfigDataPath()
        {
        }

        [MethodImpl(0x8000)]
        protected Action<BytesScriptableObjectMD5, bool> GetDeserializFunc4ConfigData(string typeName)
        {
        }

        [MethodImpl(0x8000)]
        public LazyLoadConfigAssetInfo GetLazyLoadConfigAssetInfo(string configDataName, string configAssetName)
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetLazyLoadConfigAssetNameByKey(string exportSubPath, string configDataName, int key)
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<string> GetLazyLoadConfigAssetNameListByConfigDataName(string exportSubPath, string configDataName)
        {
        }

        protected abstract void InitDeserializFunc4ConfigData();
        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator InitLoadConfigData(HashSet<string> pathSet, int threadCount, int loadCountForSingleYield, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator InitLoadConfigDataForLuaDummyType(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator InitLoadConfigDataWorker(int workerCount, int workerId, HashSet<string> pathSet, int loadCountForSingleYield, Action<bool> onEnd, List<string> skipTypeList = null, List<string> filterTypeList = null)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsUseMulitThreadOnInitLoadFromAssetEnd()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool LoadLazyLoadConfigDataAsset(string configDataName, string configAssetName)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool OnLoadConfigDataAssetEnd(string configDataName, string path, BytesScriptableObjectMD5 configDataAsset, bool inThreading, bool isUnload = true)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool StartInitializeFromAsset(Action<bool> onEnd, out int initLoadDataCount)
        {
        }

        public bool ConfigDataAssetAllowNull
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public Dictionary<string, string> ConfigDataMD5Dict
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int InitLoadDataCount
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int CurrLoadedDataCount
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <InitLoadConfigData>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal List<IProtobufExtensionTypeInfo> <typeList>__0;
            internal List<IProtobufExtensionTypeInfo>.Enumerator $locvar0;
            internal int threadCount;
            internal List<Thread> <workers>__1;
            internal HashSet<string> pathSet;
            internal int loadCountForSingleYield;
            internal Action<bool> onEnd;
            internal ClientConfigDataLoaderBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <InitLoadConfigData>c__AnonStorey3 $locvar1;
            private static Predicate<Thread> <>f__am$cache0;

            private static bool <>m__0(Thread t) => 
                t.IsAlive;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                List<string> luaExtTypeList;
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar1 = new <InitLoadConfigData>c__AnonStorey3();
                        this.$locvar1.<>f__ref$0 = this;
                        this.$locvar1.threadCount = this.threadCount;
                        this.$locvar1.pathSet = this.pathSet;
                        this.$locvar1.loadCountForSingleYield = this.loadCountForSingleYield;
                        this.<typeList>__0 = ProtobufExensionHelper.Handler.GetExtTypeInfoList();
                        this.$locvar1.luaExtTypeList = new List<string>();
                        this.$locvar0 = this.<typeList>__0.GetEnumerator();
                        try
                        {
                            while (this.$locvar0.MoveNext())
                            {
                                IProtobufExtensionTypeInfo current = this.$locvar0.Current;
                                if (current.HasExtensionInHierarchy())
                                {
                                    this.$locvar1.luaExtTypeList.Add(current.GetTypeName());
                                }
                            }
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        this.$locvar1.workerRet = 0;
                        if (this.$locvar1.threadCount <= 1)
                        {
                            luaExtTypeList = this.$locvar1.luaExtTypeList;
                            this.$current = this.$this.InitLoadConfigDataWorker(1, 0, this.$locvar1.pathSet, this.$locvar1.loadCountForSingleYield, new Action<bool>(this.$locvar1.<>m__0), luaExtTypeList, null);
                            if (!this.$disposing)
                            {
                                this.$PC = 2;
                            }
                            goto TR_0001;
                        }
                        else
                        {
                            this.<workers>__1 = new List<Thread>(this.$locvar1.threadCount);
                            for (int i = 0; i < this.$locvar1.threadCount; i++)
                            {
                                <InitLoadConfigData>c__AnonStorey4 storey = new <InitLoadConfigData>c__AnonStorey4 {
                                    <>f__ref$0 = this,
                                    <>f__ref$3 = this.$locvar1,
                                    id = i
                                };
                                Thread item = new Thread(new ThreadStart(storey.<>m__0));
                                this.<workers>__1.Add(item);
                                item.Start();
                            }
                        }
                        break;

                    case 1:
                        break;

                    case 2:
                        goto TR_0003;

                    case 3:
                        this.$current = this.$this.InitLoadConfigDataForLuaDummyType(new Action<bool>(this.$locvar1.<>m__2));
                        if (!this.$disposing)
                        {
                            this.$PC = 4;
                        }
                        goto TR_0001;

                    case 4:
                        if (this.onEnd != null)
                        {
                            this.onEnd(this.$locvar1.workerRet == 0);
                        }
                        this.$PC = -1;
                        goto TR_0000;

                    default:
                        goto TR_0000;
                }
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = new Predicate<Thread>(ClientConfigDataLoaderBase.<InitLoadConfigData>c__Iterator0.<>m__0);
                }
                if (this.<workers>__1.Find(<>f__am$cache0) != null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    goto TR_0001;
                }
                goto TR_0003;
            TR_0000:
                return false;
            TR_0001:
                return true;
            TR_0003:
                luaExtTypeList = this.$locvar1.luaExtTypeList;
                this.$current = this.$this.InitLoadConfigDataWorker(1, 0, this.$locvar1.pathSet, this.$locvar1.loadCountForSingleYield, new Action<bool>(this.$locvar1.<>m__1), null, luaExtTypeList);
                if (!this.$disposing)
                {
                    this.$PC = 3;
                }
                goto TR_0001;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <InitLoadConfigData>c__AnonStorey3
            {
                internal int threadCount;
                internal HashSet<string> pathSet;
                internal int loadCountForSingleYield;
                internal List<string> luaExtTypeList;
                internal int workerRet;
                internal ClientConfigDataLoaderBase.<InitLoadConfigData>c__Iterator0 <>f__ref$0;

                internal void <>m__0(bool lret)
                {
                    if (!lret)
                    {
                        Interlocked.Exchange(ref this.workerRet, -1);
                    }
                }

                internal void <>m__1(bool lret)
                {
                    if (!lret)
                    {
                        Interlocked.Exchange(ref this.workerRet, -1);
                    }
                }

                internal void <>m__2(bool lret)
                {
                    if (!lret)
                    {
                        Interlocked.Exchange(ref this.workerRet, -1);
                    }
                }
            }

            private sealed class <InitLoadConfigData>c__AnonStorey4
            {
                internal int id;
                internal ClientConfigDataLoaderBase.<InitLoadConfigData>c__Iterator0 <>f__ref$0;
                internal ClientConfigDataLoaderBase.<InitLoadConfigData>c__Iterator0.<InitLoadConfigData>c__AnonStorey3 <>f__ref$3;

                internal void <>m__0()
                {
                    IEnumerator enumerator = this.<>f__ref$0.$this.InitLoadConfigDataWorker(this.<>f__ref$3.threadCount, this.id, this.<>f__ref$3.pathSet, this.<>f__ref$3.loadCountForSingleYield, delegate (bool lret) {
                        if (!lret)
                        {
                            Interlocked.Exchange(ref this.<>f__ref$3.workerRet, -1);
                        }
                    }, this.<>f__ref$3.luaExtTypeList, null);
                    while (enumerator.MoveNext())
                    {
                    }
                }

                internal void <>m__1(bool lret)
                {
                    if (!lret)
                    {
                        Interlocked.Exchange(ref this.<>f__ref$3.workerRet, -1);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <InitLoadConfigDataForLuaDummyType>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal List<string> <typeNameList>__0;
            internal Action<bool> onEnd;
            internal List<string>.Enumerator $locvar0;
            internal string <typeName>__1;
            internal string <path>__2;
            internal string <fileName>__2;
            internal ClientConfigDataLoaderBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <InitLoadConfigDataForLuaDummyType>c__AnonStorey5 $locvar2;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<typeNameList>__0 = this.$this.GetAllInitLoadConfigDataTypeNameForLuaDummy();
                        if (this.<typeNameList>__0 != null)
                        {
                            this.$locvar0 = this.<typeNameList>__0.GetEnumerator();
                            num = 0xfffffffd;
                        }
                        else
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(true);
                            }
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        case 1:
                            if (this.$locvar2.configDataAsset != null)
                            {
                                if (this.$locvar2.configDataAsset.GetBytes() == null)
                                {
                                    object[] paramList = new object[] { "InitLoadConfigDataForLuaDummyType fail {0}, asset file has no data", this.<typeName>__1 };
                                    Debug.LogError(paramList);
                                }
                                else
                                {
                                    this.<fileName>__2 = $"{Path.GetFileNameWithoutExtension(this.<path>__2)}.bin".ToLower();
                                    this.$this.m_assetMD5Dict[this.<fileName>__2] = this.$locvar2.configDataAsset.m_MD5;
                                    using (MemoryStream stream = new MemoryStream(this.$locvar2.configDataAsset.GetBytes()))
                                    {
                                        IProtobufExtensionTypeInfo extTypeInfo = ProtobufExensionHelper.Handler.GetExtTypeInfo(this.<typeName>__1);
                                        foreach (DummyType type in RuntimeTypeModel.Default.Deserialize(stream, null, typeof(List<DummyType>), extTypeInfo) as List<DummyType>)
                                        {
                                            this.$this.AddConfigDataItemForLuaDummyType(this.<typeName>__1, type);
                                        }
                                        object[] paramList = new object[] { "InitLoadConfigDataForLuaDummyType ok {0}", this.<typeName>__1 };
                                        Debug.Log(paramList);
                                    }
                                    ResourceManager.Instance.UnloadAsset(this.$locvar2.configDataAsset);
                                }
                                goto TR_000D;
                            }
                            else
                            {
                                Debug.LogError($"ClientConfigDataLoader InitLoadConfigDataForLuaDummyType fail {this.<path>__2} = null");
                                if (!this.$this.ConfigDataAssetAllowNull)
                                {
                                    if (this.onEnd != null)
                                    {
                                        this.onEnd(false);
                                    }
                                }
                                else
                                {
                                    goto TR_000D;
                                }
                            }
                            break;

                        default:
                            goto TR_000D;
                    }
                    goto TR_0000;
                TR_000D:
                    if (this.$locvar0.MoveNext())
                    {
                        this.<typeName>__1 = this.$locvar0.Current;
                        this.$locvar2 = new <InitLoadConfigDataForLuaDummyType>c__AnonStorey5();
                        this.$locvar2.<>f__ref$2 = this;
                        this.<path>__2 = $"{this.$this.m_gameClientSetting.ConfigDataSetting.GetConfigDataAssetTargetPath()}/{this.<typeName>__1}.asset";
                        this.$locvar2.configDataAsset = null;
                        this.$current = ResourceManager.Instance.LoadAsset<UnityEngine.Object>(this.<path>__2, new Action<string, UnityEngine.Object>(this.$locvar2.<>m__0), false, true);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        flag = true;
                        return true;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <InitLoadConfigDataForLuaDummyType>c__AnonStorey5
            {
                internal BytesScriptableObjectMD5 configDataAsset;
                internal ClientConfigDataLoaderBase.<InitLoadConfigDataForLuaDummyType>c__Iterator2 <>f__ref$2;

                internal void <>m__0(string lpath, UnityEngine.Object asset)
                {
                    this.configDataAsset = asset as BytesScriptableObjectMD5;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <InitLoadConfigDataWorker>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal int <assetCount>__0;
            internal int workerCount;
            internal bool <inThreading>__0;
            internal int <assetTotalSize>__0;
            internal DateTime <loadOutTime>__0;
            internal HashSet<string> pathSet;
            internal HashSet<string>.Enumerator $locvar0;
            internal string <path>__1;
            internal int workerId;
            internal string <configDataName>__2;
            internal List<string> skipTypeList;
            internal List<string> filterTypeList;
            internal BytesScriptableObjectMD5 <configDataAsset>__2;
            internal Action<bool> onEnd;
            internal ClientConfigDataLoaderBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<assetCount>__0 = 0;
                        this.<inThreading>__0 = this.workerCount > 1;
                        this.<assetTotalSize>__0 = 0;
                        this.<loadOutTime>__0 = DateTime.Now.AddSeconds(0.5);
                        this.$locvar0 = this.pathSet.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        default:
                            while (true)
                            {
                                if (!this.$locvar0.MoveNext())
                                {
                                    break;
                                }
                                this.<path>__1 = this.$locvar0.Current;
                                this.<assetCount>__0++;
                                if ((this.<assetCount>__0 % (this.workerId + 1)) == 0)
                                {
                                    this.<configDataName>__2 = Path.GetFileNameWithoutExtension(this.<path>__1);
                                    if (((this.skipTypeList == null) || !this.skipTypeList.Contains(this.<configDataName>__2)) && ((this.filterTypeList == null) || this.filterTypeList.Contains(this.<configDataName>__2)))
                                    {
                                        this.<configDataAsset>__2 = null;
                                        this.<configDataAsset>__2 = ResourceManager.Instance.LoadAssetSync<BytesScriptableObjectMD5>(this.<path>__1, false);
                                        if (this.<configDataAsset>__2 != null)
                                        {
                                            this.<assetTotalSize>__0 += this.<configDataAsset>__2.m_size;
                                        }
                                        if (this.$this.OnLoadConfigDataAssetEnd(this.<configDataName>__2, this.<path>__1, this.<configDataAsset>__2, this.<inThreading>__0, true))
                                        {
                                            if (this.<assetTotalSize>__0 > 2097152f)
                                            {
                                                this.<assetTotalSize>__0 = 0;
                                            }
                                            if (this.<loadOutTime>__0 < DateTime.Now)
                                            {
                                                this.<loadOutTime>__0 = DateTime.Now.AddSeconds(0.5);
                                                this.$current = null;
                                                if (!this.$disposing)
                                                {
                                                    this.$PC = 1;
                                                }
                                                flag = true;
                                                return true;
                                            }
                                        }
                                        else
                                        {
                                            if (this.onEnd != null)
                                            {
                                                this.onEnd(false);
                                            }
                                            goto TR_0000;
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public class LazyLoadConfigAssetInfo
        {
            public string m_configAssetName;
            public ClientConfigDataLoaderBase.LazyLoadConfigAsssetState m_state;
            private static DelegateBridge _c__Hotfix_ctor;

            public LazyLoadConfigAssetInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum LazyLoadConfigAsssetState
        {
            Unload,
            Loading,
            Loaded,
            Loadfailed
        }
    }
}

