﻿namespace BlackJack.BJFramework.Runtime.ConfigData
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public abstract class StringTableManagerBase
    {
        protected ClientConfigDataLoaderBase m_configLoader;
        protected string m_currLocalization;
        protected List<string> m_localizationList;
        protected bool m_isInitLoadDefaultStringTable;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitLocalization;
        private static DelegateBridge __Hotfix_SetLocalization;
        private static DelegateBridge __Hotfix_InitDefaultStringTable;
        private static DelegateBridge __Hotfix_ClearCurrentLocalizeion;
        private static DelegateBridge __Hotfix_GetStringInDefaultStringTable;

        [MethodImpl(0x8000)]
        public StringTableManagerBase(ClientConfigDataLoaderBase configLoader)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearCurrentLocalizeion()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetStringInDefaultStringTable(string key)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitDefaultStringTable()
        {
        }

        [MethodImpl(0x8000)]
        public void InitLocalization(string localization)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetLocalization(string localization)
        {
        }
    }
}

