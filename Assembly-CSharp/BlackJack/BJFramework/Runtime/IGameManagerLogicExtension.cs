﻿namespace BlackJack.BJFramework.Runtime
{
    using System;

    public interface IGameManagerLogicExtension
    {
        void OnClear4Relogin();
        void OnLoadConfigDataEnd(Action<bool> onEnd);
        void OnLoadDynamicAssemblyEnd();
        void Tick();
    }
}

