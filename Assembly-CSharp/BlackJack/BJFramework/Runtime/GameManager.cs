﻿namespace BlackJack.BJFramework.Runtime
{
    using BlackJack.BJFramework.Runtime.ConfigData;
    using BlackJack.BJFramework.Runtime.Hotfix;
    using BlackJack.BJFramework.Runtime.Log;
    using BlackJack.BJFramework.Runtime.PlayerContext;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.Tools;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime.SDK;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;

    public abstract class GameManager : ITickable
    {
        private static GameManager m_instance;
        protected GMState m_state = GMState.None;
        protected TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
        protected BlackJack.BJFramework.Runtime.GameClientSetting m_gameClientSetting;
        protected LogManager m_logManager;
        protected TaskManager m_taskManager;
        protected ResourceManager m_resourceManager;
        protected ServerSettingManager m_serverSettingManager;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Func<ResourceManager> <CreateResourceManagerHandler>k__BackingField;
        protected IAudioManager m_audioManager;
        protected SceneManager m_sceneManager;
        protected UIManager m_uiManager;
        protected ClassLoader m_classLoader;
        protected HotfixManager m_hotfixManager;
        protected ClientConfigDataLoaderBase m_configDataLoader;
        protected StringTableManagerBase m_stringTableManager;
        protected IPlayerContextNetworkClient m_networkClient;
        protected PlayerContextBase m_playerContext = null;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <LoginUserId>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <ServerId>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsLoginUserSettingChanged>k__BackingField;
        protected string m_currLocalization;
        protected Dictionary<string, object> m_gameManagerParamDict = new Dictionary<string, object>();
        private const string LocalizationPrefKey = "Localization";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateAndInitGameManager;
        private static DelegateBridge __Hotfix_OnAssemblyResolve;
        private static DelegateBridge __Hotfix_OnAssemblyLoad;
        private static DelegateBridge __Hotfix_OnUnhandledException;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_IsFullScreen;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_OnApplicationQuit;
        private static DelegateBridge __Hotfix_InitlizeGameSetting;
        private static DelegateBridge __Hotfix_CreateAudioManager;
        private static DelegateBridge __Hotfix_CreateServerSettingManager;
        private static DelegateBridge __Hotfix_StartLoadDynamicAssemblys;
        private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysCompleted;
        private static DelegateBridge __Hotfix_LoadDynamicAssemblysWorker;
        private static DelegateBridge __Hotfix_StartHotfixManager;
        private static DelegateBridge __Hotfix_StartHotfixManagerWorker;
        private static DelegateBridge __Hotfix_StartLoadConfigData;
        private static DelegateBridge __Hotfix_CreateConfigDataLoader;
        private static DelegateBridge __Hotfix_OnLoadConfigDataEnd;
        private static DelegateBridge __Hotfix_InitStringTableManager;
        private static DelegateBridge __Hotfix_GetConfigData;
        private static DelegateBridge __Hotfix_StartAudioManager;
        private static DelegateBridge __Hotfix_InitGraphSetting;
        private static DelegateBridge __Hotfix_StartInitGraphSetting;
        private static DelegateBridge __Hotfix_InitlizeBeforeGameAuthLogin;
        private static DelegateBridge __Hotfix_InitGameNetworkClient;
        private static DelegateBridge __Hotfix_InitPlayerContext;
        private static DelegateBridge __Hotfix_GetPlayerContext;
        private static DelegateBridge __Hotfix_GetGameManagerParam;
        private static DelegateBridge __Hotfix_SetGameManagerParam;
        private static DelegateBridge __Hotfix_Return2Login;
        private static DelegateBridge __Hotfix_SaveDataSection;
        private static DelegateBridge __Hotfix_Clear4Return2Login;
        private static DelegateBridge __Hotfix_Clear4Relogin;
        private static DelegateBridge __Hotfix_DeleteOverdueLogFile;
        private static DelegateBridge __Hotfix_UploadClientLogFile;
        private static DelegateBridge __Hotfix_FileInfoComparer;
        private static DelegateBridge __Hotfix_GetClientLogUploadUrl;
        private static DelegateBridge __Hotfix_GetFileLogPath;
        private static DelegateBridge __Hotfix_GetUploadFileServerSaveName;
        private static DelegateBridge __Hotfix_GetAudioManagerInitConfigData;
        private static DelegateBridge __Hotfix_StartCorutine_1;
        private static DelegateBridge __Hotfix_StartCorutine_0;
        private static DelegateBridge __Hotfix_IsMemoryRich;
        private static DelegateBridge __Hotfix_InitLocalizationInfo;
        private static DelegateBridge __Hotfix_GetDefaultLocalization;
        private static DelegateBridge __Hotfix_SetLocalization;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnGameManagerException;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_get_GameClientSetting;
        private static DelegateBridge __Hotfix_get_CreateResourceManagerHandler;
        private static DelegateBridge __Hotfix_set_CreateResourceManagerHandler;
        private static DelegateBridge __Hotfix_get_AudioManager;
        private static DelegateBridge __Hotfix_get_ConfigDataLoader;
        private static DelegateBridge __Hotfix_get_StringTableManager;
        private static DelegateBridge __Hotfix_get_NetworkClient;
        private static DelegateBridge __Hotfix_get_PlayerContext;
        private static DelegateBridge __Hotfix_get_LoginUserId;
        private static DelegateBridge __Hotfix_set_LoginUserId;
        private static DelegateBridge __Hotfix_get_ServerId;
        private static DelegateBridge __Hotfix_set_ServerId;
        private static DelegateBridge __Hotfix_get_IsLoginUserSettingChanged;
        private static DelegateBridge __Hotfix_set_IsLoginUserSettingChanged;
        private static DelegateBridge __Hotfix_get_CurrLocalization;

        [MethodImpl(0x8000)]
        protected GameManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public virtual void Clear4Relogin()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Clear4Return2Login(bool isCacheDataDirty)
        {
        }

        [MethodImpl(0x8000)]
        public static T CreateAndInitGameManager<T>() where T: GameManager, new()
        {
            DelegateBridge bridge = __Hotfix_CreateAndInitGameManager;
            if (bridge != null)
            {
                bridge.InvokeSessionStart();
                bridge.Invoke(1);
                return bridge.InvokeSessionEndWithResult<T>();
            }
            if (m_instance != null)
            {
                return (m_instance as T);
            }
            m_instance = Activator.CreateInstance<T>();
            if (m_instance.Initlize())
            {
                return (T) m_instance;
            }
            UnityEngine.Debug.LogError("CreateAndInitGameManager fail");
            return null;
        }

        [MethodImpl(0x8000)]
        protected IAudioManager CreateAudioManager()
        {
            DelegateBridge bridge = __Hotfix_CreateAudioManager;
            return ((bridge == null) ? (!this.m_gameClientSetting.AudioSetting.EnableCRI ? ((IAudioManager) AudioManager4Unity.CreateAudioManager()) : ((IAudioManager) AudioManager4CRI.CreateAudioManager())) : bridge.__Gen_Delegate_Imp73(this));
        }

        [MethodImpl(0x8000)]
        protected virtual ClientConfigDataLoaderBase CreateConfigDataLoader()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ServerSettingManager CreateServerSettingManager()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void DeleteOverdueLogFile()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int FileInfoComparer(FileInfo a, FileInfo b)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual Dictionary<string, string> GetAudioManagerInitConfigData()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetClientLogUploadUrl()
        {
        }

        [MethodImpl(0x8000)]
        public T GetConfigData<T>() where T: ClientConfigDataLoaderBase
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetDefaultLocalization()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetFileLogPath()
        {
            DelegateBridge bridge = __Hotfix_GetFileLogPath;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp33(this);
            }
            string str = !this.m_gameClientSetting.Log2Persistent ? Application.temporaryCachePath : Application.persistentDataPath;
            return $"{str}/Log/";
        }

        [MethodImpl(0x8000)]
        public T GetGameManagerParam<T>(string key) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public T GetPlayerContext<T>() where T: PlayerContextBase
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetUploadFileServerSaveName(string srcFileName)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool InitGameNetworkClient()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool InitGraphSetting(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initlize()
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            UnityEngine.Debug.Log("GameManager.Initlize start");
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(this.OnAssemblyResolve);
            AppDomain.CurrentDomain.AssemblyLoad += new AssemblyLoadEventHandler(this.OnAssemblyLoad);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(this.OnUnhandledException);
            if (!this.InitlizeGameSetting(null))
            {
                UnityEngine.Debug.LogError("GameManager.Initlize fail");
                return false;
            }
            if (this.m_gameClientSetting.ResolutionSetting.ResetResolution4PreDesign)
            {
                if (Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    Screen.SetResolution(this.m_gameClientSetting.SceneSetting.DesignScreenWidth, this.m_gameClientSetting.SceneSetting.DesignScreenHeight, this.IsFullScreen(), this.m_gameClientSetting.ResolutionSetting.RefreshRate);
                }
                else
                {
                    float num2 = (Screen.width * 1f) / ((float) Screen.height);
                    if (((this.m_gameClientSetting.SceneSetting.DesignScreenWidth * 1f) / ((float) this.m_gameClientSetting.SceneSetting.DesignScreenHeight)) <= num2)
                    {
                        Screen.SetResolution(Mathf.FloorToInt(this.m_gameClientSetting.SceneSetting.DesignScreenHeight * num2), this.m_gameClientSetting.SceneSetting.DesignScreenHeight, this.IsFullScreen(), this.m_gameClientSetting.ResolutionSetting.RefreshRate);
                    }
                    else
                    {
                        int height = Mathf.FloorToInt(((float) this.m_gameClientSetting.SceneSetting.DesignScreenWidth) / num2);
                        Screen.SetResolution(this.m_gameClientSetting.SceneSetting.DesignScreenWidth, height, this.IsFullScreen(), this.m_gameClientSetting.ResolutionSetting.RefreshRate);
                    }
                }
            }
            Application.targetFrameRate = this.m_gameClientSetting.ResolutionSetting.RefreshRate;
            Screen.sleepTimeout = -1;
            Debug.m_mainThread = Thread.CurrentThread;
            this.m_logManager = LogManager.CreateLogManager();
            if (!this.m_logManager.Initlize(!this.m_gameClientSetting.DisableUnityEngineLog, true, this.GetFileLogPath(), "Log", null))
            {
                UnityEngine.Debug.LogError("GameManager.Initlize fail for m_logManager.Initlize");
                return false;
            }
            this.m_taskManager = TaskManager.CreateTaskManager();
            if (!this.m_taskManager.Initlize())
            {
                UnityEngine.Debug.LogError("GameManager.Initlize fail for m_taskManager.Initlize()");
                return false;
            }
            this.InitLocalizationInfo();
            this.m_resourceManager = (CreateResourceManagerHandler == null) ? ResourceManager.CreateResourceManager() : CreateResourceManagerHandler();
            if (!this.m_resourceManager.Initlize(this.m_gameClientSetting.ResourcesSetting, this.m_currLocalization, this.m_gameClientSetting.PublishSetting.EnableAndroidObb))
            {
                Debug.LogError("GameManager.Initlize fail for m_resourceManager.Initlize()");
                return false;
            }
            this.m_serverSettingManager = this.CreateServerSettingManager();
            this.m_audioManager = this.CreateAudioManager();
            if (!this.m_audioManager.Initlize())
            {
                Debug.LogError("GameManager.Initlize fail for m_audioManager.Initlize()");
                return false;
            }
            this.m_audioManager.SetLocalization(this.m_currLocalization);
            this.m_sceneManager = SceneManager.CreateSceneManager();
            if (!this.m_sceneManager.Initlize(this.m_gameClientSetting.SceneSetting.UIDesignScreenWidth, this.m_gameClientSetting.SceneSetting.UIDesignScreenHeight, this.m_gameClientSetting.SceneSetting.TrigerWidth2ShrinkScale, this.m_gameClientSetting.SceneSetting.TrigerHeight2ShrinkScale, this.m_gameClientSetting.SceneSetting.SceneLayerOffset, this.m_gameClientSetting.SceneSetting.EnableLayerReserve, this.m_gameClientSetting.SceneSetting.UseOrthographicForUILayer))
            {
                Debug.LogError("GameManager.Initlize fail for m_sceneManager.Initlize()");
                return false;
            }
            this.m_uiManager = UIManager.CreateUIManager();
            if (!this.m_uiManager.Initlize())
            {
                Debug.LogError("GameManager.Initlize fail for m_uiManager.Initlize()");
                return false;
            }
            this.m_classLoader = ClassLoader.CreateClassLoader();
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                this.m_classLoader.AddAssembly(assembly);
            }
            foreach (string str in this.m_gameClientSetting.DynamicAssemblySetting.DynamicAssemblyList)
            {
                this.m_classLoader.AddAssembly(Assembly.Load(str));
            }
            SDKHelper.SetSpecialEditionMark(SDKHelper.SpecialEditionMark.HuaWei, this.m_gameClientSetting.m_specialEditionSettings.ForHuaWei);
            if (this.m_gameClientSetting.HotfixSettings.EnableHotfix)
            {
                this.m_hotfixManager = HotfixManager.CreateHotfixManager();
            }
            this.m_state = GMState.Inited;
            return true;
        }

        [MethodImpl(0x8000)]
        public virtual bool InitlizeBeforeGameAuthLogin(string loginUserId, string serverId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool InitlizeGameSetting(string gameSettingTypeName = null)
        {
            DelegateBridge bridge = __Hotfix_InitlizeGameSetting;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp16(this, gameSettingTypeName);
            }
            System.Type systemTypeInstance = !string.IsNullOrEmpty(gameSettingTypeName) ? System.Type.GetType(gameSettingTypeName) : typeof(BlackJack.BJFramework.Runtime.GameClientSetting);
            this.m_gameClientSetting = Resources.Load("GameClientSetting", systemTypeInstance) as BlackJack.BJFramework.Runtime.GameClientSetting;
            if (this.m_gameClientSetting != null)
            {
                return true;
            }
            UnityEngine.Debug.LogError("InitlizeGameSetting fail");
            return false;
        }

        [MethodImpl(0x8000)]
        public void InitLocalizationInfo()
        {
            DelegateBridge bridge = __Hotfix_InitLocalizationInfo;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (!PlayerPrefs.HasKey("Localization"))
            {
                this.m_currLocalization = this.GetDefaultLocalization();
                PlayerPrefs.SetString("Localization", this.m_currLocalization);
                PlayerPrefs.Save();
            }
            else
            {
                this.m_currLocalization = PlayerPrefs.GetString("Localization");
                if (!this.GameClientSetting.StringTableSetting.LocalizationList.Contains(this.m_currLocalization))
                {
                    this.m_currLocalization = this.GetDefaultLocalization();
                }
            }
        }

        [MethodImpl(0x8000)]
        public virtual bool InitPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool InitStringTableManager(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsFullScreen()
        {
            DelegateBridge bridge = __Hotfix_IsFullScreen;
            return ((bridge == null) ? ((Application.platform != RuntimePlatform.WindowsPlayer) || Screen.fullScreen) : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        public bool IsMemoryRich()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator LoadDynamicAssemblysWorker(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnApplicationQuit()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
        }

        [MethodImpl(0x8000)]
        private Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameManagerException()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnLoadConfigDataEnd(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadDynamicAssemblysCompleted(bool ret, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Return2Login(bool raiseCriticalDataCacheDirty, bool switchAccount = false)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SaveDataSection()
        {
        }

        [MethodImpl(0x8000)]
        public void SetGameManagerParam(string key, object value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocalization(string localization, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool StartAudioManager(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCorutine(IEnumerator corutine)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCorutine(Func<IEnumerator> corutine)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool StartHotfixManager(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator StartHotfixManagerWorker(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public virtual IEnumerator StartInitGraphSetting(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool StartLoadConfigData(Action<bool> onEnd, out int initLoadDataCount)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool StartLoadDynamicAssemblys(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                try
                {
                    this.m_corutineHelper.Tick();
                    BlackJack.BJFramework.Runtime.Timer.Tick();
                    FpsHelper.Tick();
                    if (this.m_playerContext != null)
                    {
                        this.m_playerContext.Tick();
                    }
                    this.m_taskManager.Tick();
                    this.m_resourceManager.Tick();
                    this.m_audioManager.Tick();
                    this.m_sceneManager.Tick();
                    this.m_uiManager.Tick();
                }
                catch (Exception exception)
                {
                    Debug.LogError($"{"GameManager.Tick"} : {exception}");
                    this.OnGameManagerException();
                    throw;
                }
            }
        }

        [MethodImpl(0x8000)]
        public virtual void Uninitlize()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public virtual IEnumerator UploadClientLogFile(Action<bool> onEnd, string bugDesc = "")
        {
        }

        public static GameManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp79());
            }
        }

        public BlackJack.BJFramework.Runtime.GameClientSetting GameClientSetting
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_GameClientSetting;
                return ((bridge == null) ? this.m_gameClientSetting : bridge.__Gen_Delegate_Imp80(this));
            }
        }

        public static Func<ResourceManager> CreateResourceManagerHandler
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_CreateResourceManagerHandler;
                return ((bridge == null) ? <CreateResourceManagerHandler>k__BackingField : bridge.__Gen_Delegate_Imp81());
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public IAudioManager AudioManager
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientConfigDataLoaderBase ConfigDataLoader
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public StringTableManagerBase StringTableManager
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_StringTableManager;
                return ((bridge == null) ? this.m_stringTableManager : bridge.__Gen_Delegate_Imp82(this));
            }
        }

        public IPlayerContextNetworkClient NetworkClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public PlayerContextBase PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string LoginUserId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public string ServerId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public bool IsLoginUserSettingChanged
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public string CurrLocalization
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_CurrLocalization;
                return ((bridge == null) ? this.m_currLocalization : bridge.__Gen_Delegate_Imp33(this));
            }
        }

        [CompilerGenerated]
        private sealed class <InitGraphSetting>c__AnonStorey9
        {
            internal Action<bool> onEnd;
            internal GameManager $this;

            internal void <>m__0(bool lret)
            {
                if (!lret)
                {
                    Debug.LogError("GameManager InitGraphSetting.Start fail");
                    this.onEnd(false);
                }
                else
                {
                    this.$this.m_state = GameManager.GMState.InitGraphSettingEnd;
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadDynamicAssemblysWorker>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal List<string>.Enumerator $locvar0;
            internal string <assemblyName>__1;
            internal string <dllAssetName>__2;
            internal IEnumerator <iter>__2;
            internal Action<bool> onEnd;
            internal Assembly <assembly>__2;
            internal GameManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadDynamicAssemblysWorker>c__AnonStorey5 $locvar1;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = this.$this.m_gameClientSetting.DynamicAssemblySetting.DynamicAssemblyList.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        case 1:
                            if (this.$locvar1.dllAsset != null)
                            {
                                Debug.LogWarning($"LoadDynamicAssemblysWorker Assembly.Load dllAsset.bytes={this.$locvar1.dllAsset.m_size}");
                                this.<assembly>__2 = AppDomain.CurrentDomain.Load(this.$locvar1.dllAsset.GetBytes());
                                this.$this.m_classLoader.AddAssembly(this.<assembly>__2);
                                goto TR_0009;
                            }
                            else
                            {
                                Debug.LogError($"LoadDynamicAssemblysWorker fail dllAssetName={this.<dllAssetName>__2}");
                                this.onEnd(false);
                            }
                            break;

                        default:
                            goto TR_0009;
                    }
                    goto TR_0000;
                TR_0009:
                    if (this.$locvar0.MoveNext())
                    {
                        this.<assemblyName>__1 = this.$locvar0.Current;
                        this.$locvar1 = new <LoadDynamicAssemblysWorker>c__AnonStorey5();
                        this.$locvar1.<>f__ref$0 = this;
                        this.<dllAssetName>__2 = $"{PathHelper.DynamicAssemblyAssetRoot}/{this.<assemblyName>__1}_ABS/{this.<assemblyName>__1}_dll.asset";
                        this.$locvar1.dllAsset = null;
                        this.<iter>__2 = this.$this.m_resourceManager.LoadAsset<BytesScripableObject>(this.<dllAssetName>__2, new Action<string, BytesScripableObject>(this.$locvar1.<>m__0), false, false);
                        this.$current = this.<iter>__2;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        flag = true;
                        return true;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                this.$this.m_state = GameManager.GMState.DynamicAssemblyLoadEnd;
                this.onEnd(true);
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadDynamicAssemblysWorker>c__AnonStorey5
            {
                internal BytesScripableObject dllAsset;
                internal GameManager.<LoadDynamicAssemblysWorker>c__Iterator0 <>f__ref$0;

                internal void <>m__0(string lpath, BytesScripableObject lasset)
                {
                    this.dllAsset = lasset;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartAudioManager>c__AnonStorey8
        {
            internal Action<bool> onEnd;
            internal GameManager $this;

            internal void <>m__0(bool lret)
            {
                if (!lret)
                {
                    Debug.LogError("StartAudioManager m_audioManager.Start fail");
                    this.onEnd(false);
                }
                else
                {
                    this.$this.m_state = GameManager.GMState.AudioManagerLoadEnd;
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartHotfixManagerWorker>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal GameManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = this.$this.m_hotfixManager.Init();
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.$this.m_state = GameManager.GMState.HotfixAssemblyLoadEnd;
                        this.onEnd(true);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <StartInitGraphSetting>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.onEnd(true);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <StartLoadConfigData>c__AnonStorey7
        {
            internal Action<bool> onEnd;
            internal GameManager $this;

            internal void <>m__0(bool resultForConfigDataLoaderInit)
            {
                if (resultForConfigDataLoaderInit)
                {
                    this.$this.InitStringTableManager(delegate (bool resultForStringTableManagerInit) {
                        if (!resultForStringTableManagerInit)
                        {
                            this.onEnd(false);
                        }
                        else
                        {
                            <StartLoadConfigData>c__AnonStorey6 storey = new <StartLoadConfigData>c__AnonStorey6 {
                                <>f__ref$7 = this,
                                hasExtensionProcess = false
                            };
                            storey.hasExtensionProcess = this.$this.OnLoadConfigDataEnd(new Action<bool>(storey.<>m__0));
                            if (!storey.hasExtensionProcess)
                            {
                                this.$this.m_state = GameManager.GMState.ConfigDataLoadEnd;
                                this.onEnd(true);
                            }
                        }
                    });
                }
                else
                {
                    this.onEnd(false);
                }
            }

            internal void <>m__1(bool resultForStringTableManagerInit)
            {
                if (!resultForStringTableManagerInit)
                {
                    this.onEnd(false);
                }
                else
                {
                    <StartLoadConfigData>c__AnonStorey6 storey = new <StartLoadConfigData>c__AnonStorey6 {
                        <>f__ref$7 = this,
                        hasExtensionProcess = false
                    };
                    storey.hasExtensionProcess = this.$this.OnLoadConfigDataEnd(new Action<bool>(storey.<>m__0));
                    if (!storey.hasExtensionProcess)
                    {
                        this.$this.m_state = GameManager.GMState.ConfigDataLoadEnd;
                        this.onEnd(true);
                    }
                }
            }

            private sealed class <StartLoadConfigData>c__AnonStorey6
            {
                internal bool hasExtensionProcess;
                internal GameManager.<StartLoadConfigData>c__AnonStorey7 <>f__ref$7;

                internal void <>m__0(bool lret)
                {
                    if (!this.hasExtensionProcess)
                    {
                        throw new ApplicationException("OnLoadConfigDataEnd !hasExtensionProcess but call OnEnd");
                    }
                    this.<>f__ref$7.$this.m_state = !lret ? this.<>f__ref$7.$this.m_state : GameManager.GMState.ConfigDataLoadEnd;
                    this.<>f__ref$7.onEnd(lret);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadDynamicAssemblys>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal GameManager $this;

            internal void <>m__0(bool lret)
            {
                this.$this.OnLoadDynamicAssemblysCompleted(lret, this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <UploadClientLogFile>c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal string <uploadUrl>__0;
            internal DateTime <currTime>__0;
            internal DateTime <minLogTime>__0;
            internal string <logPath>__0;
            internal string bugDesc;
            internal string <tempFilePath>__0;
            internal List<FileInfo> <uploadLogList>__0;
            internal string[] $locvar0;
            internal int $locvar1;
            internal List<FileInfo>.Enumerator $locvar2;
            internal FileInfo <item>__1;
            internal string <fileName>__3;
            internal string <bugDescFilePath>__3;
            internal GameManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <UploadClientLogFile>c__AnonStoreyA $locvar3;
            private <UploadClientLogFile>c__AnonStoreyB $locvar4;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar2.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        if (this.$this.m_gameClientSetting != null)
                        {
                            this.<uploadUrl>__0 = this.$this.GetClientLogUploadUrl();
                            if (!string.IsNullOrEmpty(this.<uploadUrl>__0))
                            {
                                this.<currTime>__0 = DateTime.Now;
                                this.<minLogTime>__0 = DateTime.MinValue;
                                if (this.$this.m_gameClientSetting.UpLoadSetting.LimintHours != 0f)
                                {
                                    this.<minLogTime>__0 = this.<currTime>__0.AddHours((double) -this.$this.m_gameClientSetting.UpLoadSetting.LimintHours);
                                }
                                this.<logPath>__0 = this.$this.GetFileLogPath();
                                object[] objArray1 = new object[4];
                                objArray1[0] = this.<logPath>__0;
                                objArray1[1] = this.<uploadUrl>__0;
                                objArray1[2] = (this.$this.m_gameClientSetting.UpLoadSetting.LimintHours != 0f) ? this.$this.m_gameClientSetting.UpLoadSetting.LimintHours.ToString() : "所有";
                                object[] local1 = objArray1;
                                object[] args = objArray1;
                                args[3] = !string.IsNullOrEmpty(this.bugDesc) ? this.bugDesc : "无";
                                Debug.Log(string.Format("上传日志: 日志路径-{0}，上传路径-{1}，上传时间段-{2}小时内,\n 额外上传描述-{3}", args));
                                this.<tempFilePath>__0 = Path.Combine(this.<logPath>__0, "temp");
                                if (File.Exists(this.<tempFilePath>__0))
                                {
                                    File.Delete(this.<tempFilePath>__0);
                                }
                                this.<uploadLogList>__0 = new List<FileInfo>();
                                this.$locvar0 = Directory.GetFiles(this.<logPath>__0, "Log*.txt");
                                this.$locvar1 = 0;
                                while (true)
                                {
                                    if (this.$locvar1 >= this.$locvar0.Length)
                                    {
                                        this.<uploadLogList>__0.Sort(new Comparison<FileInfo>(this.$this.FileInfoComparer));
                                        this.$locvar2 = this.<uploadLogList>__0.GetEnumerator();
                                        num = 0xfffffffd;
                                        break;
                                    }
                                    string fileName = this.$locvar0[this.$locvar1];
                                    FileInfo item = new FileInfo(fileName);
                                    if (item.LastWriteTime >= this.<minLogTime>__0)
                                    {
                                        this.<uploadLogList>__0.Add(item);
                                    }
                                    this.$locvar1++;
                                }
                                goto TR_001C;
                            }
                            else
                            {
                                Debug.LogError("UploadClientLogFile::uploadUrl == NullOrEmpty");
                                if (this.onEnd != null)
                                {
                                    this.onEnd(false);
                                }
                            }
                        }
                        else
                        {
                            Debug.LogError("UploadClientLogFile::m_gameClientSetting == null");
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                        }
                        break;

                    case 1:
                        goto TR_001C;

                    case 2:
                        File.Delete(this.<bugDescFilePath>__3);
                        goto TR_000D;

                    default:
                        break;
                }
            TR_0000:
                return false;
            TR_0007:
                return true;
            TR_000D:
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
                this.$PC = -1;
                goto TR_0000;
            TR_001C:
                try
                {
                    switch (num)
                    {
                        case 1:
                            File.Delete(this.<tempFilePath>__0);
                            if (this.$locvar3.result)
                            {
                                try
                                {
                                    File.Delete(this.<item>__1.FullName);
                                }
                                catch (Exception exception)
                                {
                                    Debug.LogError($"{"GameManager.UploadClientLogFile"} : {exception}");
                                }
                            }
                            break;

                        default:
                            break;
                    }
                    if (this.$locvar2.MoveNext())
                    {
                        this.<item>__1 = this.$locvar2.Current;
                        this.$locvar3 = new <UploadClientLogFile>c__AnonStoreyA();
                        this.$locvar3.<>f__ref$3 = this;
                        File.Copy(this.<item>__1.FullName, this.<tempFilePath>__0);
                        this.$locvar3.result = false;
                        this.$current = HttpUtil.UploadFileToHttp(this.<tempFilePath>__0, this.<uploadUrl>__0, this.$this.GetUploadFileServerSaveName(this.<item>__1.Name), new Action<bool>(this.$locvar3.<>m__0));
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        flag = true;
                        goto TR_0007;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar2.Dispose();
                    }
                }
                if (!string.IsNullOrEmpty(this.bugDesc))
                {
                    this.$locvar4 = new <UploadClientLogFile>c__AnonStoreyB();
                    this.$locvar4.<>f__ref$3 = this;
                    this.<fileName>__3 = $"BugDesc{this.<currTime>__0.ToString("yyyy_MMdd_HHmm_ss")}.txt";
                    this.<bugDescFilePath>__3 = Path.Combine(this.<logPath>__0, this.<fileName>__3);
                    File.WriteAllText(this.<bugDescFilePath>__3, this.bugDesc);
                    this.$current = HttpUtil.UploadFileToHttp(this.<bugDescFilePath>__3, this.<uploadUrl>__0, this.$this.GetUploadFileServerSaveName(this.<fileName>__3), new Action<bool>(this.$locvar4.<>m__0));
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                    goto TR_0007;
                }
                goto TR_000D;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <UploadClientLogFile>c__AnonStoreyA
            {
                internal bool result;
                internal GameManager.<UploadClientLogFile>c__Iterator3 <>f__ref$3;

                internal void <>m__0(bool ret)
                {
                    this.result = ret;
                }
            }

            private sealed class <UploadClientLogFile>c__AnonStoreyB
            {
                internal bool result;
                internal GameManager.<UploadClientLogFile>c__Iterator3 <>f__ref$3;

                internal void <>m__0(bool ret)
                {
                    this.result = ret;
                }
            }
        }

        public enum GMState
        {
            None = 0,
            Inited = 1,
            DynamicAssemblyLoading = 2,
            DynamicAssemblyLoadEnd = 3,
            HotfixAssemblyLoading = 4,
            HotfixAssemblyLoadEnd = 5,
            ConfigDataLoading = 6,
            ConfigDataLoadEnd = 7,
            AudioManagerLoading = 8,
            AudioManagerLoadEnd = 9,
            InitGraphSetting = 10,
            InitGraphSettingEnd = 11,
            Ready = 11
        }
    }
}

