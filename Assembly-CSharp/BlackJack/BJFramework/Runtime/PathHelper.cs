﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class PathHelper
    {
        public static string AssetBundlesBuildOutputPath = "AssetBundles";
        private static string m_assetBundlesBuildOutputPathWithPlatform;
        private static string m_assetBundlesDownloadPath4Editor;
        public static string BundleDataPath = "Assets/GameProject/RuntimeAssets/BundleData";
        private static string m_bundleDataAssetName;
        private static string m_bundleDataAssetPathNoPostfix;
        private static string m_bundleDataAssetPath;
        private static string m_streamAssetBundleDataAssetPathInEditor;
        private static string m_streamAssetBundleDataAssetPath;
        private static string m_bundleDataBundleName;
        public static string BundleDataVersionFileName = "BundleDataVersion.txt";
        private static string m_bundleDataVersionFilePath;
        public static string StreamingAssetsBundlePathDirName = "ExportAssetBundle";
        private static string m_streamingAssetsRootPath;
        private static string m_streamingAssetsBundlePath;
        private static string m_streamingAssetsFileListPath;
        private static string m_streamingAssetsFileListPathNoPostfix;
        private static string m_streamingAssetsFileListResourcesName;
        private static DelegateBridge __Hotfix_get_AssetBundlesBuildOutputPathWithPlatform;
        private static DelegateBridge __Hotfix_get_AssetBundlesDownloadPath4Editor;
        private static DelegateBridge __Hotfix_get_BundleDataAssetName;
        private static DelegateBridge __Hotfix_get_BundleDataAssetPathNoPostfix;
        private static DelegateBridge __Hotfix_get_BundleDataAssetPath;
        private static DelegateBridge __Hotfix_get_StreamAssetBundleDataAssetPathInEditor;
        private static DelegateBridge __Hotfix_get_StreamAssetBundleDataResourcesPath;
        private static DelegateBridge __Hotfix_get_BundleDataBundleName;
        private static DelegateBridge __Hotfix_get_BundleDataVersionFilePath;
        private static DelegateBridge __Hotfix_get_StreamingAssetsRootPath;
        private static DelegateBridge __Hotfix_get_StreamingAssetsBundlePath;
        private static DelegateBridge __Hotfix_get_StreamingAssetsFileListPath;
        private static DelegateBridge __Hotfix_get_StreamingAssetsFileListPathNoPostfix;
        private static DelegateBridge __Hotfix_get_StreamingAssetsFileListResourcesName;
        private static DelegateBridge __Hotfix_get_DynamicAssemblyRoot;
        private static DelegateBridge __Hotfix_get_DynamicAssemblyAssetRoot;

        public static string AssetBundlesBuildOutputPathWithPlatform
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string AssetBundlesDownloadPath4Editor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string BundleDataAssetName
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_BundleDataAssetName;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp96();
                }
                if (Application.isEditor)
                {
                    return $"BundleData{Util.GetCurrentTargetPlatform()}";
                }
                if (m_bundleDataAssetName == null)
                {
                    m_bundleDataAssetName = $"BundleData{Util.GetCurrentTargetPlatform()}";
                }
                return m_bundleDataAssetName;
            }
        }

        public static string BundleDataAssetPathNoPostfix
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string BundleDataAssetPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string StreamAssetBundleDataAssetPathInEditor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string StreamAssetBundleDataResourcesPath
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_StreamAssetBundleDataResourcesPath;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp96();
                }
                if (Application.isEditor)
                {
                    return $"StreamingAssetsRes/{BundleDataAssetName}";
                }
                if (m_streamAssetBundleDataAssetPath == null)
                {
                    m_streamAssetBundleDataAssetPath = $"StreamingAssetsRes/{BundleDataAssetName}";
                }
                return m_streamAssetBundleDataAssetPath;
            }
        }

        public static string BundleDataBundleName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string BundleDataVersionFilePath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string StreamingAssetsRootPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string StreamingAssetsBundlePath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string StreamingAssetsFileListPath
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string StreamingAssetsFileListPathNoPostfix
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string StreamingAssetsFileListResourcesName
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_StreamingAssetsFileListResourcesName;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp96();
                }
                if (Application.isEditor)
                {
                    return $"StreamingAssetsRes/StreamingAssetsFileList{Util.GetCurrentTargetPlatform()}";
                }
                if (m_streamingAssetsFileListResourcesName == null)
                {
                    m_streamingAssetsFileListResourcesName = $"StreamingAssetsRes/StreamingAssetsFileList{Util.GetCurrentTargetPlatform()}";
                }
                return m_streamingAssetsFileListResourcesName;
            }
        }

        public static string DynamicAssemblyRoot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static string DynamicAssemblyAssetRoot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

