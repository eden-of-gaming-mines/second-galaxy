﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public static class MathExtensions
    {
        private static DelegateBridge __Hotfix_AlmostEquals_3;
        private static DelegateBridge __Hotfix_AlmostEquals_2;
        private static DelegateBridge __Hotfix_AlmostEquals_1;
        private static DelegateBridge __Hotfix_AlmostEquals_0;

        [MethodImpl(0x8000)]
        public static bool AlmostEquals(this float target, float second, float floatDiff)
        {
        }

        [MethodImpl(0x8000)]
        public static bool AlmostEquals(this Quaternion target, Quaternion second, float maxAngle)
        {
        }

        [MethodImpl(0x8000)]
        public static bool AlmostEquals(this Vector2 target, Vector2 second, float sqrMagnitudePrecision)
        {
        }

        [MethodImpl(0x8000)]
        public static bool AlmostEquals(this Vector3 target, Vector3 second, float sqrMagnitudePrecision)
        {
        }
    }
}

