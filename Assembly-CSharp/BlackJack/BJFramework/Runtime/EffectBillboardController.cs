﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode]
    public class EffectBillboardController : MonoBehaviour
    {
        [Header("编辑状态下朝向的Camera")]
        public Camera FaceToCameraInEditor;
        [Header("运行状态下朝向的Camera")]
        public Camera FaceToCameraInRuntime;
        [Header("特效对象Camera朝向模式")]
        public FaceMode RotationFaceMode;
        public bool IsKeepSize;
        public float Width;
        public Renderer Render;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_GetLayerCameraFromParent;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_KeepSize;
        private static DelegateBridge __Hotfix_GetScreenPortion;

        [MethodImpl(0x8000)]
        protected Camera GetLayerCameraFromParent()
        {
        }

        [MethodImpl(0x8000)]
        private float GetScreenPortion()
        {
        }

        [MethodImpl(0x8000)]
        protected void KeepSize()
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        public enum FaceMode
        {
            ReverseCameraRotation,
            FaceToCamera
        }
    }
}

