﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Playables;

    [RequireComponent(typeof(PlayableDirector))]
    public class TimelineAutoBindGenerator : MonoBehaviour
    {
        public TimelinePredefineObjectTypeSetting PredefineObjTypeSetting;
        public string PreprocessBindDataListObjTypeGroupName;
        public List<TimelinePreprocessBindNode> PreprocessBindDataList;
        public List<TimelineBindNode> BindDataList;
        private BlackJack.BJFramework.Runtime.TimelineObjectStorage m_objectStorage;
        private PlayableDirector m_director;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_GetBindObjects;
        private static DelegateBridge __Hotfix_SetBindObjects;
        private static DelegateBridge __Hotfix_SetPreprocessBindObject;
        private static DelegateBridge __Hotfix_DoBind;
        private static DelegateBridge __Hotfix_BindTracks;
        private static DelegateBridge __Hotfix_BindClips;
        private static DelegateBridge __Hotfix_BindVariable_1;
        private static DelegateBridge __Hotfix_BindVariable_0;
        private static DelegateBridge __Hotfix_BindVariableInControlTrack;
        private static DelegateBridge __Hotfix_BindField;
        private static DelegateBridge __Hotfix_BindProperty;
        private static DelegateBridge __Hotfix_GetObjectByType;
        private static DelegateBridge __Hotfix_IsChild;
        private static DelegateBridge __Hotfix_get_TimelineObjectStorage;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private void BindClips(PlayableAsset playableAsset, object bindObj, string trackName, int clipIndex, string[] variableNameArray)
        {
        }

        [MethodImpl(0x8000)]
        private object BindField(object obj, object val, string variableName)
        {
        }

        [MethodImpl(0x8000)]
        private object BindProperty(object obj, object val, string variableName)
        {
        }

        [MethodImpl(0x8000)]
        private void BindTracks(GameObject bindObjGo, PlayableAsset playableAsset, string trackName)
        {
        }

        [MethodImpl(0x8000)]
        private object BindVariable(object obj, object val, string variableName)
        {
        }

        [MethodImpl(0x8000)]
        private void BindVariable(object obj, string[] variableNameArray, object val, bool isControlTrack = false)
        {
        }

        [MethodImpl(0x8000)]
        private void BindVariableInControlTrack(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public void DoBind(Dictionary<string, object> bindObjectDic)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<string, object> GetBindObjects()
        {
        }

        [MethodImpl(0x8000)]
        private object GetObjectByType(System.Type type, object val)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsChild(Transform parentTransform, Transform childTransform)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBindObjects(Dictionary<string, object> bindObjectDic)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPreprocessBindObject(string nodeType, object obj)
        {
        }

        public BlackJack.BJFramework.Runtime.TimelineObjectStorage TimelineObjectStorage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetPreprocessBindObject>c__AnonStorey0
        {
            internal string nodeType;

            internal bool <>m__0(TimelineAutoBindGenerator.TimelinePreprocessBindNode a) => 
                (a.BindNodeType == this.nodeType);
        }

        [Serializable]
        public class TimelineBindNode
        {
            public string BindNodeType;
            public string BindNodeParam;
            public string BindToPath;
            public bool BindNodeTypeIsValid;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        [Serializable]
        public class TimelinePreprocessBindNode
        {
            public string BindNodeType;
            public Component BindComponent;
            public string StringValue;
            private static DelegateBridge _c__Hotfix_ctor;

            public TimelinePreprocessBindNode(string bindNodeType, Component bindComponnet, string stringValue)
            {
                this.BindNodeType = bindNodeType;
                this.BindComponent = bindComponnet;
                this.StringValue = stringValue;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp37(this, bindNodeType, bindComponnet, stringValue);
                }
            }
        }
    }
}

