﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public static class Timer
    {
        public static DateTime m_currTime;
        public static ulong m_currTick;
        public static DateTime m_lastTickTime = DateTime.MaxValue;
        private static DelegateBridge __Hotfix_GetLastFrameDeltaTimeMs;
        private static DelegateBridge __Hotfix_GetCurrFrameCount;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public static int GetCurrFrameCount()
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetLastFrameDeltaTimeMs()
        {
        }

        [MethodImpl(0x8000)]
        public static void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp19();
            }
            else
            {
                m_lastTickTime = m_currTime;
                m_currTime = DateTime.Now;
                m_currTick += (ulong) 1L;
            }
        }
    }
}

