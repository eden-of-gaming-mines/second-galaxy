﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LoginSettings
    {
        public bool LoginUseSDK;
        public bool LoginUseSettings = true;
        public bool IsLoginByServerListInEditor;
        public string GameServerAddress = "192.168.1.111";
        public int GameServerPort = 0x3e81;
        public string LoginAccount = "testUser1";
        public string ClientVersion = string.Empty;
        public int BasicVersion = 1;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public LoginSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

