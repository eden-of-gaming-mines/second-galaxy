﻿namespace BlackJack.BJFramework.Runtime.TaskNs
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class MapSceneTaskBase : Task, IDynamicAssetProvider
    {
        protected MapSceneUpdatePipeLineCtxBase m_pipeCtxDefault;
        protected List<MapSceneUpdatePipeLineCtxBase> m_runingPipeLineCtxList;
        protected List<MapSceneUpdatePipeLineCtxBase> m_unusingPipeLineCtxList;
        protected List<MapSceneUpdatePipeLineCtxBase> m_pipeLineWait2Start;
        protected SceneLayerBase[] m_layerArray;
        protected Dictionary<string, DynamicResCacheItem> m_dynamicResCacheDict;
        protected List<string> m_wait2ReleaseResList;
        protected TinyCorutineHelper m_corutineHelper;
        private int m_unloadDynamicResTimeInterval;
        private DateTime m_nextUnLoadDynamicResTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitPipeCtxDefault;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_StartUpdatePipeLine;
        private static DelegateBridge __Hotfix_AllocPipeLineCtx;
        private static DelegateBridge __Hotfix_CreatePipeLineCtx;
        private static DelegateBridge __Hotfix_StartPipeLineCtx;
        private static DelegateBridge __Hotfix_ReleasePipeLineCtx;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_PreProcessBeforeResLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_StartLoadStaticRes;
        private static DelegateBridge __Hotfix_OnLoadStaticResCompleted;
        private static DelegateBridge __Hotfix_IsNeedCollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_CalculateDynamicResReallyNeedLoad;
        private static DelegateBridge __Hotfix_OnLoadDynamicResCompleted;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_StartUpdateView;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_HideAllView;
        private static DelegateBridge __Hotfix_ClearAllContextAndRes;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_ReleaseDynamicResCache;
        private static DelegateBridge __Hotfix_LockDynamicResList;
        private static DelegateBridge __Hotfix_UnlockDynamicResList;
        private static DelegateBridge __Hotfix_LockDynamicResCache;
        private static DelegateBridge __Hotfix_UnlockDynamicResCache;
        private static DelegateBridge __Hotfix_UnlockAllDynamicResCache;
        private static DelegateBridge __Hotfix_TickDynamicResCache;
        private static DelegateBridge __Hotfix_BlackJack.BJFramework.Runtime.TaskNs.IDynamicAssetProvider.AllocDynamicAsset_1;
        private static DelegateBridge __Hotfix_BlackJack.BJFramework.Runtime.TaskNs.IDynamicAssetProvider.AllocDynamicAsset_0;
        private static DelegateBridge __Hotfix_BlackJack.BJFramework.Runtime.TaskNs.IDynamicAssetProvider.ReleaseDynamicAsset;
        private static DelegateBridge __Hotfix_BlackJack.BJFramework.Runtime.TaskNs.IDynamicAssetProvider.CheckAsset;
        private static DelegateBridge __Hotfix_get_m_mainLayer;
        private static DelegateBridge __Hotfix_get_LayerDescArray;

        [MethodImpl(0x8000)]
        public MapSceneTaskBase(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual MapSceneUpdatePipeLineCtxBase AllocPipeLineCtx()
        {
        }

        [MethodImpl(0x8000)]
        UnityEngine.Object IDynamicAssetProvider.AllocDynamicAsset(string name)
        {
        }

        [MethodImpl(0x8000)]
        T IDynamicAssetProvider.AllocDynamicAsset<T>(string name) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000)]
        bool IDynamicAssetProvider.CheckAsset(string name)
        {
        }

        [MethodImpl(0x8000)]
        void IDynamicAssetProvider.ReleaseDynamicAsset(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearAllContextAndRes()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual List<string> CollectAllDynamicResForLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual MapSceneUpdatePipeLineCtxBase CreatePipeLineCtx()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void HideAllView()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitLayerStateOnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitPipeCtxDefault()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedCollectAllDynamicResForLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedLoadStaticRes()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedUpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void LockDynamicResCache(string path)
        {
        }

        [MethodImpl(0x8000)]
        protected void LockDynamicResList(MapSceneUpdatePipeLineCtxBase ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadAllResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadDynamicResCompleted(Dictionary<string, UnityEngine.Object> resDict, MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadStaticResCompleted(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(object param, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PostUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PreProcessBeforeResLoad(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void ReleaseDynamicResCache(string path)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ReleasePipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartLoadDynamicRes(List<string> resPathList, MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartLoadStaticRes(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool StartPipeLineCtx(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool StartUpdatePipeLine(MapSceneUpdatePipeLineCtxBase pipeCtx = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartUpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickDynamicResCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockAllDynamicResCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockDynamicResCache(string path)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockDynamicResList(MapSceneUpdatePipeLineCtxBase ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateDataCache(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateView(MapSceneUpdatePipeLineCtxBase pipeCtx)
        {
        }

        protected SceneLayerBase m_mainLayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected virtual LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadDynamicRes>c__AnonStorey2
        {
            internal MapSceneUpdatePipeLineCtxBase pipeCtx;
            internal Dictionary<string, UnityEngine.Object> resDict;
            internal MapSceneTaskBase $this;

            internal void <>m__0()
            {
                this.pipeCtx.m_loadingDynamicResCorutineCount--;
                this.$this.OnLoadDynamicResCompleted(this.resDict, this.pipeCtx);
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadStaticRes>c__AnonStorey0
        {
            internal string layerName;
            internal int index;
            internal MapSceneTaskBase.<StartLoadStaticRes>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0(SceneLayerBase layer)
            {
                if (layer == null)
                {
                    Debug.LogError($"Load Layer fail task={this.<>f__ref$1.$this.ToString()} layer={this.layerName}");
                }
                else
                {
                    this.<>f__ref$1.$this.m_layerArray[this.index] = layer;
                    this.<>f__ref$1.pipeCtx.m_loadingStaticResCorutineCount--;
                    this.<>f__ref$1.$this.OnLoadStaticResCompleted(this.<>f__ref$1.pipeCtx);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadStaticRes>c__AnonStorey1
        {
            internal MapSceneUpdatePipeLineCtxBase pipeCtx;
            internal MapSceneTaskBase $this;
        }

        public class DynamicResCacheItem
        {
            public UnityEngine.Object m_res;
            public uint m_ref;
            public bool m_waitForRelease;
            public DateTime m_releaseTime = DateTime.MaxValue;
            private float m_waitForReleaseTime = 10f;
            public int m_lockRef;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Lock;
            private static DelegateBridge __Hotfix_IsLocked;
            private static DelegateBridge __Hotfix_IsWaitForReleaseTime;
            private static DelegateBridge __Hotfix_RefreshReleaseTime;

            public DynamicResCacheItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public bool IsLocked()
            {
                DelegateBridge bridge = __Hotfix_IsLocked;
                return ((bridge == null) ? (this.m_lockRef > 0) : bridge.__Gen_Delegate_Imp9(this));
            }

            public bool IsWaitForReleaseTime()
            {
                DelegateBridge bridge = __Hotfix_IsWaitForReleaseTime;
                return ((bridge == null) ? (this.m_releaseTime > DateTime.Now) : bridge.__Gen_Delegate_Imp9(this));
            }

            public void Lock(bool isLock)
            {
                DelegateBridge bridge = __Hotfix_Lock;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, isLock);
                }
                else
                {
                    this.m_lockRef = !isLock ? (this.m_lockRef - 1) : (this.m_lockRef + 1);
                    this.m_lockRef = (this.m_lockRef >= 0) ? this.m_lockRef : 0;
                    if (this.m_lockRef > 0)
                    {
                        this.m_releaseTime = DateTime.MaxValue;
                    }
                }
            }

            public bool RefreshReleaseTime()
            {
                DelegateBridge bridge = __Hotfix_RefreshReleaseTime;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                if (!(this.m_releaseTime == DateTime.MaxValue))
                {
                    return false;
                }
                this.m_releaseTime = DateTime.Now.AddSeconds((double) this.m_waitForReleaseTime);
                return true;
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct LayerDesc
        {
            public string m_layerName;
            public string m_layerResPath;
            public bool m_isUILayer;
        }
    }
}

