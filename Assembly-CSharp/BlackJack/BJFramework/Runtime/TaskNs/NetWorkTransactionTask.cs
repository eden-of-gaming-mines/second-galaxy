﻿namespace BlackJack.BJFramework.Runtime.TaskNs
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class NetWorkTransactionTask : Task
    {
        protected bool m_blockedUI;
        protected bool m_autoRetry;
        protected bool m_skipRelogin;
        protected float m_timeout;
        protected bool m_isTimeOuted;
        protected DateTime? m_timeoutTime;
        protected DateTime m_showWaitingUITime;
        protected bool m_isUIWaitingShown;
        protected bool m_isReturnToLoginByDirtyData;
        protected bool m_isLaunchedReloginUITask;
        public static int m_delayTimeForUIWaiting = 1;
        public const float TimeOutForEditor = 60f;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Action<bool> EventShowUIWaiting;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Action<bool> EventReturnToLoginUI;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private static Action<Action> EventReLoginBySession;
        private bool m_isNetworkError;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnTransactionComplete;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnTimeOut;
        private static DelegateBridge __Hotfix_OnGameServerDataUnsyncNotify;
        private static DelegateBridge __Hotfix_OnGameServerNetworkError;
        private static DelegateBridge __Hotfix_OnGameServerDisconnected;
        private static DelegateBridge __Hotfix_OnNetworkError;
        private static DelegateBridge __Hotfix_OnReLoginSuccess;
        private static DelegateBridge __Hotfix_OnUIManagerReturnToLoginUI;
        private static DelegateBridge __Hotfix_add_EventShowUIWaiting;
        private static DelegateBridge __Hotfix_remove_EventShowUIWaiting;
        private static DelegateBridge __Hotfix_add_EventReturnToLoginUI;
        private static DelegateBridge __Hotfix_remove_EventReturnToLoginUI;
        private static DelegateBridge __Hotfix_add_EventReLoginBySession;
        private static DelegateBridge __Hotfix_remove_EventReLoginBySession;
        private static DelegateBridge __Hotfix_get_IsNetworkError;

        public static  event Action<Action> EventReLoginBySession
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public static  event Action<bool> EventReturnToLoginUI
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public static  event Action<bool> EventShowUIWaiting
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public NetWorkTransactionTask(float timeout = 10f, bool blockedUI = true, bool autoRetry = false, bool skipRelogin = false)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerDataUnsyncNotify()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerNetworkError()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNetworkError()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnReLoginSuccess()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(object param = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(object param, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTransactionComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnUIManagerReturnToLoginUI(bool obj, bool switchAccount)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UnregisterNetworkEvent()
        {
        }

        public bool IsNetworkError
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

