﻿namespace BlackJack.BJFramework.Runtime.TaskNs
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    public class Task : ITickable
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <Name>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private TaskState <State>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private DateTime <PauseStartTime>k__BackingField;
        private TaskManager m_taskManager;
        private ulong m_currTickCount;
        private LinkedList<DelayExecItem> m_delayExecList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Task> EventOnStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Task> EventOnStop;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Task> EventOnPause;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Task> EventOnResume;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_Pause;
        private static DelegateBridge __Hotfix_Resume;
        private static DelegateBridge __Hotfix_ClearOnStopEvent;
        private static DelegateBridge __Hotfix_BlackJack.BJFramework.Runtime.ITickable.Tick;
        private static DelegateBridge __Hotfix_ExecAfterTicks;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_ToString;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;
        private static DelegateBridge __Hotfix_get_PauseStartTime;
        private static DelegateBridge __Hotfix_set_PauseStartTime;
        private static DelegateBridge __Hotfix_add_EventOnStart;
        private static DelegateBridge __Hotfix_remove_EventOnStart;
        private static DelegateBridge __Hotfix_add_EventOnStop;
        private static DelegateBridge __Hotfix_remove_EventOnStop;
        private static DelegateBridge __Hotfix_add_EventOnPause;
        private static DelegateBridge __Hotfix_remove_EventOnPause;
        private static DelegateBridge __Hotfix_add_EventOnResume;
        private static DelegateBridge __Hotfix_remove_EventOnResume;

        public event Action<Task> EventOnPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Task> EventOnResume
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Task> EventOnStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Task> EventOnStop
        {
            [MethodImpl(0x8000)] add
            {
                DelegateBridge bridge = __Hotfix_add_EventOnStop;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    Action<Task> eventOnStop = this.EventOnStop;
                    while (true)
                    {
                        Action<Task> a = eventOnStop;
                        eventOnStop = Interlocked.CompareExchange<Action<Task>>(ref this.EventOnStop, (Action<Task>) System.Delegate.Combine(a, value), eventOnStop);
                        if (ReferenceEquals(eventOnStop, a))
                        {
                            return;
                        }
                    }
                }
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Task(string name)
        {
            this.Name = name;
            this.State = TaskState.Init;
            this.m_taskManager = TaskManager.Instance;
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, name);
            }
        }

        [MethodImpl(0x8000)]
        void ITickable.Tick()
        {
            DelegateBridge bridge = __Hotfix_BlackJack.BJFramework.Runtime.ITickable.Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.m_currTickCount += (ulong) 1L;
                if (this.State == TaskState.Stopped)
                {
                    if ((this.m_currTickCount % ((ulong) 200L)) == 0L)
                    {
                        Debug.Log("Still ticking an stopped task: " + this);
                    }
                }
                else if (this.State == TaskState.Running)
                {
                    if (this.m_delayExecList != null)
                    {
                        while (this.m_delayExecList.First != null)
                        {
                            DelayExecItem item = this.m_delayExecList.First.Value;
                            if (item.m_execTargetTick > this.m_currTickCount)
                            {
                                break;
                            }
                            item.m_action();
                            this.m_delayExecList.RemoveFirst();
                        }
                    }
                    this.OnTick();
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void ClearOnStopEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void ExecAfterTicks(Action action, ulong delayTickCount = 1UL)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnResume(object param = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnStart(object param, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void Pause()
        {
        }

        [MethodImpl(0x8000)]
        public bool Resume(object param = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool Start(object param = null, Action<bool> onPipelineEnd = null)
        {
            DelegateBridge bridge = __Hotfix_Start;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp43(this, param, onPipelineEnd);
            }
            if (this.State != TaskState.Init)
            {
                return false;
            }
            if (!this.m_taskManager.RegisterTask(this))
            {
                return false;
            }
            this.State = TaskState.Running;
            if (!this.OnStart(param, onPipelineEnd))
            {
                this.Stop();
                return false;
            }
            if (this.EventOnStart != null)
            {
                this.EventOnStart(this);
            }
            return true;
        }

        [MethodImpl(0x8000)]
        public void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
            DelegateBridge bridge = __Hotfix_ToString;
            return ((bridge == null) ? $"[Task: name={this.Name}, state={this.State}]" : bridge.__Gen_Delegate_Imp33(this));
        }

        public string Name
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Name;
                return ((bridge == null) ? this.<Name>k__BackingField : bridge.__Gen_Delegate_Imp33(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
                DelegateBridge bridge = __Hotfix_set_Name;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<Name>k__BackingField = value;
                }
            }
        }

        public TaskState State
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_State;
                return ((bridge == null) ? this.<State>k__BackingField : bridge.__Gen_Delegate_Imp176(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
                DelegateBridge bridge = __Hotfix_set_State;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp177(this, value);
                }
                else
                {
                    this.<State>k__BackingField = value;
                }
            }
        }

        public DateTime PauseStartTime
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public class DelayExecItem
        {
            public ulong m_execTargetTick;
            public Action m_action;
            private static DelegateBridge _c__Hotfix_ctor;

            public DelayExecItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum TaskState
        {
            Init,
            Running,
            Pausing,
            Paused,
            Stopped
        }
    }
}

