﻿namespace BlackJack.BJFramework.Runtime.TaskNs
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class MapSceneUpdatePipeLineCtxBase
    {
        protected bool m_runing;
        public int m_loadingStaticResCorutineCount;
        public int m_loadingDynamicResCorutineCount;
        public Action m_updateViewAction;
        public List<string> m_dynamicResList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_IsRunning;

        [MethodImpl(0x8000)]
        public virtual void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRunning()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Start()
        {
        }
    }
}

