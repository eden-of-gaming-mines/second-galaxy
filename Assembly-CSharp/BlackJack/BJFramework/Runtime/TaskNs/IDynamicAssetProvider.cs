﻿namespace BlackJack.BJFramework.Runtime.TaskNs
{
    using System;
    using UnityEngine;

    public interface IDynamicAssetProvider
    {
        UnityEngine.Object AllocDynamicAsset(string name);
        T AllocDynamicAsset<T>(string name) where T: UnityEngine.Object;
        bool CheckAsset(string resString);
        void ReleaseDynamicAsset(string name);
    }
}

