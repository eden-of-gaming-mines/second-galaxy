﻿namespace BlackJack.BJFramework.Runtime.TaskNs
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class TaskManager : ITickable
    {
        private static TaskManager m_instance;
        private List<Task> m_taskList = new List<Task>();
        private List<Task> m_taskList4TickLoop = new List<Task>();
        private Dictionary<string, Task> m_taskRegDict = new Dictionary<string, Task>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateTaskManager;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_RegisterTask;
        private static DelegateBridge __Hotfix_UnregisterTask;
        private static DelegateBridge __Hotfix_StopTaskWithTaskName;
        private static DelegateBridge __Hotfix_FindTaskByName;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        private TaskManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public static TaskManager CreateTaskManager()
        {
            DelegateBridge bridge = __Hotfix_CreateTaskManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp178();
            }
            if (m_instance == null)
            {
                m_instance = new TaskManager();
            }
            return m_instance;
        }

        [MethodImpl(0x8000)]
        public T FindTaskByName<T>(string taskName) where T: Task
        {
        }

        [MethodImpl(0x8000)]
        public bool Initlize()
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            return ((bridge == null) || bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        public bool RegisterTask(Task task)
        {
            DelegateBridge bridge = __Hotfix_RegisterTask;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp16(this, task);
            }
            if (!string.IsNullOrEmpty(task.Name) && this.m_taskRegDict.ContainsKey(task.Name))
            {
                if (this.m_taskRegDict[task.Name] != task)
                {
                    Debug.LogError($"Task name collision. Name: {task.Name}.");
                }
                else
                {
                    Debug.LogError($"Readding task. Name: {task.Name}.");
                }
                return false;
            }
            if (this.m_taskRegDict.ContainsValue(task))
            {
                Debug.LogError($"Re-adding same task with different name. Task name {task.Name}");
                return false;
            }
            this.m_taskList.Add(task);
            if (!string.IsNullOrEmpty(task.Name))
            {
                this.m_taskRegDict.Add(task.Name, task);
            }
            return true;
        }

        [MethodImpl(0x8000)]
        public void StopTaskWithTaskName(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                foreach (Task task in this.m_taskList)
                {
                    this.m_taskList4TickLoop.Add(task);
                }
                foreach (ITickable tickable in this.m_taskList4TickLoop)
                {
                    try
                    {
                        tickable.Tick();
                    }
                    catch (Exception exception)
                    {
                        Debug.LogError($"{"TaskManager.Tick"} : {exception}");
                    }
                }
                this.m_taskList4TickLoop.Clear();
            }
        }

        [MethodImpl(0x8000)]
        public void Uninitlize()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterTask(Task task)
        {
        }

        public static TaskManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp178());
            }
        }
    }
}

