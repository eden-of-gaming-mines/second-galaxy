﻿namespace BlackJack.BJFramework.Runtime.Log
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class FileLogger
    {
        private string _logFileRoot;
        private string _logName;
        private string _logFileFullPath;
        private StreamWriter _logStreamWriter;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<string, string> EventOnLog;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetNewFileFullPath;
        private static DelegateBridge __Hotfix_WriteLog;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_add_EventOnLog;
        private static DelegateBridge __Hotfix_remove_EventOnLog;

        public event Action<string, string> EventOnLog
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public FileLogger(string logFileRoot, string logName)
        {
            if (!Directory.Exists(logFileRoot))
            {
                try
                {
                    Directory.CreateDirectory(logFileRoot);
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.Log("Create log directory fail " + exception.ToString());
                    return;
                }
            }
            this._logFileRoot = logFileRoot;
            this._logName = logName;
            this._logFileFullPath = this.GetNewFileFullPath();
            this._logStreamWriter = new StreamWriter(this._logFileFullPath);
            UnityEngine.Debug.Log("Create Log File " + this._logFileFullPath);
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, logFileRoot, logName);
            }
        }

        [MethodImpl(0x8000)]
        public void Close()
        {
        }

        [MethodImpl(0x8000)]
        private string GetNewFileFullPath()
        {
            DelegateBridge bridge = __Hotfix_GetNewFileFullPath;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp33(this);
            }
            return (this._logFileRoot + this._logName + DateTime.Now.ToString("yyyy_MMdd_HHmm_ss") + ".txt");
        }

        [MethodImpl(0x8000)]
        public void WriteLog(string msg, string level)
        {
            DelegateBridge bridge = __Hotfix_WriteLog;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, msg, level);
            }
            else if (this._logStreamWriter != null)
            {
                lock (this._logStreamWriter)
                {
                    try
                    {
                        this._logStreamWriter.WriteLine($"[{level}][{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff")}] {msg}");
                        this._logStreamWriter.Flush();
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }
                if (this.EventOnLog != null)
                {
                    this.EventOnLog(level, msg);
                }
            }
        }
    }
}

