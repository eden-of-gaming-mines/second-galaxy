﻿namespace BlackJack.BJFramework.Runtime.Log
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LogManager
    {
        private static LogManager m_instance;
        private bool m_inited;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private BlackJack.BJFramework.Runtime.Log.FileLogger <FileLogger>k__BackingField;
        public bool NeedFileLog;
        public bool NeedEngineLog = true;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateLogManager;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_OnEngineLog;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_get_FileLogger;
        private static DelegateBridge __Hotfix_set_FileLogger;

        [MethodImpl(0x8000)]
        private LogManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public static LogManager CreateLogManager()
        {
            DelegateBridge bridge = __Hotfix_CreateLogManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp92();
            }
            if (m_instance == null)
            {
                m_instance = new LogManager();
            }
            return m_instance;
        }

        [MethodImpl(0x8000)]
        public bool Initlize(bool needEngineLog, bool needFileLog, string logFileRoot, string logName, Action<string, string> logCallBack = null)
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp93(this, needEngineLog, needFileLog, logFileRoot, logName, logCallBack);
            }
            if (!this.m_inited)
            {
                this.NeedEngineLog = needEngineLog;
                this.NeedFileLog = needFileLog;
                if (needFileLog)
                {
                    this.FileLogger = new BlackJack.BJFramework.Runtime.Log.FileLogger(logFileRoot, logName);
                    if (logCallBack != null)
                    {
                        this.FileLogger.EventOnLog += logCallBack;
                    }
                }
                if (!this.NeedEngineLog)
                {
                    Application.logMessageReceived += new Application.LogCallback(this.OnEngineLog);
                }
                this.m_inited = true;
            }
            return true;
        }

        [MethodImpl(0x8000)]
        private void OnEngineLog(string condition, string stackTrace, LogType type)
        {
            DelegateBridge bridge = __Hotfix_OnEngineLog;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp94(this, condition, stackTrace, type);
            }
            else
            {
                switch (type)
                {
                    case LogType.Error:
                    case LogType.Assert:
                    case LogType.Exception:
                        Debug.LogError(condition);
                        if (!string.IsNullOrEmpty(stackTrace))
                        {
                            Debug.LogError(stackTrace);
                        }
                        break;

                    case LogType.Warning:
                        Debug.LogWarning(condition);
                        if (!string.IsNullOrEmpty(stackTrace))
                        {
                            Debug.LogWarning(stackTrace);
                        }
                        break;

                    case LogType.Log:
                        Debug.Log(condition);
                        if (!string.IsNullOrEmpty(stackTrace))
                        {
                            Debug.Log(stackTrace);
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        [MethodImpl(0x8000)]
        public void Uninitlize()
        {
        }

        public static LogManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp92());
            }
        }

        public BlackJack.BJFramework.Runtime.Log.FileLogger FileLogger
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_FileLogger;
                return ((bridge == null) ? this.<FileLogger>k__BackingField : bridge.__Gen_Delegate_Imp95(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
                DelegateBridge bridge = __Hotfix_set_FileLogger;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<FileLogger>k__BackingField = value;
                }
            }
        }
    }
}

