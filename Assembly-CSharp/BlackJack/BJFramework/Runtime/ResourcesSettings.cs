﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class ResourcesSettings
    {
        public bool SkipStreamingAssetsFileProcessing = true;
        public bool DisableAssetBundle = true;
        public bool DisableAssetBundleDownload = true;
        public string AssetBundleDownloadUrlRoot;
        public bool SkipAssetBundlePreUpdateing = true;
        public int PreUpdateWorkerCount = 4;
        public bool LoadAssetFromBundleInEditor;
        public int UnloadUnusedAssetTimeInterval = 10;
        public bool AssetPathIgnoreCase;
        public List<string> ResaveFileDirRoots;
        public string ResaveAssetRoot = "GameProject/RuntimeAssets/ResaveFiles";
        public string ResaveFileDestDir = "Resave";
        public bool ClearUnusedAssetBundleName;
        [Header("是否开启资源管理器的详细log")]
        public bool EnableDetailResourceManagerLog = true;
        [Header("是否在游戏运行过程中检查资源更新")]
        public bool EnableCheckVersionUpdateInRuning;
        [Header("运行期的版本检查的目标URL，如果不设置就会默认使用下载地址")]
        public string CheckVersionUpdateInRuningUrl;
        [Header("运行期的版本检查的时间间隔,单位秒")]
        public int CheckVersionUpdateInRuningCD = 600;
        [Header("Bundle预更新重试次数")]
        public int BundlePreUpdateRetryCount = 10;
        [Header("Bundle预更新重试时间间隔(秒)")]
        public float BundlePreUpdateRetryTimeDelay = 1f;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public ResourcesSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

