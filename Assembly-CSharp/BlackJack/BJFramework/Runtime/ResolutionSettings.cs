﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class ResolutionSettings
    {
        [Header("刷新率")]
        public int RefreshRate;
        [Header("是否重新设置分辨率")]
        public bool ResetResolution4PreDesign;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public ResolutionSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

