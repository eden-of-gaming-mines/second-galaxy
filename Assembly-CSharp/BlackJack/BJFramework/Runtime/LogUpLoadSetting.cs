﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class LogUpLoadSetting
    {
        [Header("上传日志的时间段(单位: hour, 距现在多长时间内的日志上传):")]
        public float LimintHours = 3.5f;
        [Header("上传的http路径")]
        public string UploadUrl;
        [Header("有效日志的时间段(单位:hours, 距离现在多长时间的日志保留):")]
        public float ValidLogFileTime = 72f;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public LogUpLoadSetting()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

