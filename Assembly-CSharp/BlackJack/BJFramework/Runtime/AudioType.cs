﻿namespace BlackJack.BJFramework.Runtime
{
    using System;

    public enum AudioType
    {
        Sound,
        NpcSpeech,
        PlayerVoice,
        BGMusic
    }
}

