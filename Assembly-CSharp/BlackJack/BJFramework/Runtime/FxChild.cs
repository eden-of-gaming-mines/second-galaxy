﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class FxChild : MonoBehaviour
    {
        public GameObject m_prefab;
        public bool m_visible = true;
        private bool m_animationVisible;
        private GameObject m_childGameObject;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_OnDrawGizmosSelected;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_LateUpdate;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDrawGizmosSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

