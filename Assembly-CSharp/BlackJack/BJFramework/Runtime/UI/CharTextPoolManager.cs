﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharTextPoolManager
    {
        private static readonly CharTextPool m_charPool = new CharTextPool();
        public static GameObject m_charPoolRoot;
        public static Transform m_charPoolRootTrans;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Request;
        private static DelegateBridge __Hotfix_Recycle;
        private static DelegateBridge __Hotfix_SetRootTrans;

        [MethodImpl(0x8000)]
        public static void Recycle(Text txtObj)
        {
        }

        [MethodImpl(0x8000)]
        public static Text Request()
        {
        }

        [MethodImpl(0x8000)]
        private static void SetRootTrans()
        {
        }
    }
}

