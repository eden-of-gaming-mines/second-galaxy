﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public abstract class UserGuideUIControllerBase : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnNextPageClicked;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnOperationAreaClicked;
        protected UserGuideStepInfo m_stepInfo;
        protected bool m_isEnableOperationArea;
        protected bool m_isEndStepByOperatorAreaClick;
        protected bool m_isPassEventSetting;
        protected Rect m_operationAreaRect;
        protected Rect m_npcDialogRect;
        protected string m_operationAreaEffectMode;
        protected List<GameObject> m_npcDialogDummyPositionList;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateOpreationArea;
        private static DelegateBridge __Hotfix_UpdateMaskPanel;
        private static DelegateBridge __Hotfix_ShowMaskPanel;
        private static DelegateBridge __Hotfix_SetOperationAreaButtonEnable;
        private static DelegateBridge __Hotfix_SetMaskPanelEnable;
        private static DelegateBridge __Hotfix_ShowOperationAreaEffect;
        private static DelegateBridge __Hotfix_OnShowOperationAreaEffect;
        private static DelegateBridge __Hotfix_HideOperationAreaEffect;
        private static DelegateBridge __Hotfix_ShowOperationNotice;
        private static DelegateBridge __Hotfix_OnShowOpreationNotice;
        private static DelegateBridge __Hotfix_HideOperationNotice;
        private static DelegateBridge __Hotfix_OnMaskPanelClicked;
        private static DelegateBridge __Hotfix_MaskPanelClickedImp;
        private static DelegateBridge __Hotfix_OnOperationAreaClicked;
        private static DelegateBridge __Hotfix_OperationAreaClickedImp;
        private static DelegateBridge __Hotfix_SetRectTransformSize;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_CreateRect;
        private static DelegateBridge __Hotfix_GetOperationAreaEffectGo;
        private static DelegateBridge __Hotfix_add_EventOnNextPageClicked;
        private static DelegateBridge __Hotfix_remove_EventOnNextPageClicked;
        private static DelegateBridge __Hotfix_add_EventOnOperationAreaClicked;
        private static DelegateBridge __Hotfix_remove_EventOnOperationAreaClicked;

        public event Action EventOnNextPageClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnOperationAreaClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected UserGuideUIControllerBase()
        {
        }

        [MethodImpl(0x8000)]
        protected Rect CreateRect(Vector2 center, Vector2 size)
        {
        }

        protected abstract Vector2 GetNpcDialogSize();
        [MethodImpl(0x8000)]
        protected virtual GameObject GetOperationAreaEffectGo()
        {
        }

        protected abstract Vector2 GetOperationNoticeSize(GameObject go);
        [MethodImpl(0x8000)]
        protected void HideOperationAreaEffect()
        {
        }

        protected abstract void HideOperationAreaEffectImp();
        [MethodImpl(0x8000)]
        protected void HideOperationNotice()
        {
        }

        protected abstract void HideOperationNoticeImp();
        [MethodImpl(0x8000)]
        protected void MaskPanelClickedImp(bool isClickOperationArea)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaskPanelClicked()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOperationAreaClicked()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnShowOperationAreaEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnShowOpreationNotice()
        {
        }

        [MethodImpl(0x8000)]
        protected void OperationAreaClickedImp(bool isClickOperationArea)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Reset()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetMaskPanelEnable(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetOperationAreaButtonEnable(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetRectTransformSize(RectTransform trans, Vector2 newSize)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowMaskPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowOperationAreaEffect(Vector2 operationAreaCenter, Vector2 operationAreaSize)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowOperationNotice(Vector2 operationAreaCenter, Vector2 operationAreaSize)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateMaskPanel(Vector2 operationAreaCenter, Vector2 operationAreaSize, bool isShowMaskPanel)
        {
        }

        protected abstract void UpdateNpcDialog(UserGuideStepInfo.IPageInfo pageInfo, bool useNPCChat, int npcChatDummyIndex, Vector2 offset, Dictionary<string, UnityEngine.Object> dynamicResCacheDict);
        [MethodImpl(0x8000)]
        protected void UpdateOpreationArea(Vector2 operationAreaCenter, Vector2 operationAreaSize)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateView(UserGuideStepInfo stepInfo, int currPageIndex, bool isEnableOperationArea, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        protected abstract RectTransform OperationAreaButtonRect { get; }

        protected abstract RectTransform MaskPanelLeft { get; }

        protected abstract RectTransform MaskPanelRight { get; }

        protected abstract RectTransform MaskPanelTop { get; }

        protected abstract RectTransform MaskPanelBottom { get; }

        protected abstract RectTransform MaskPanelCenter { get; }

        protected abstract GameObject OperationNoticeGoCenter { get; }

        protected abstract GameObject OperationNoticeGoLeft { get; }

        protected abstract GameObject OperationNoticeGoRight { get; }

        protected abstract GameObject OperationNoticeGoTop { get; }

        protected abstract GameObject OperationNoticeGoBottom { get; }
    }
}

