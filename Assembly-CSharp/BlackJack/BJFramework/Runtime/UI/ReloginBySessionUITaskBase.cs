﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class ReloginBySessionUITaskBase : UITaskBase
    {
        public const string ReloginUITaskModeWaitForReloginConfirm = "WaitForReloginConfirm";
        public const string ReloginUITaskModeWaitForReloginProcessing = "WaitForReloginProcessing";
        public const string ReloginUITaskModeWaitReturnToLoginConfirm = "WaitReturnToLoginConfirm";
        public const string ReloginUITaskModePromptToLoginAgain = "PromptToLoginAgain";
        public const string ReloginUITaskModeBundleDataVersionMisMatch = "BundleDataVersionMisMatch";
        protected LoginState m_loginTaskState;
        protected bool m_waitForStartGameSessionLogin;
        protected bool m_raiseCriticalDataCacheDirty;
        public static Action EventOnReloginSuccess;
        private DateTime m_reloginCoolDownTime;
        private bool m_isNeedRetryReloginWhenCoolDown;
        private const float ReloginCoolDownTime = 5f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_StartRelogin;
        private static DelegateBridge __Hotfix_ShowErrorMsg;
        private static DelegateBridge __Hotfix_ShowWaitForReloginConfirmUI;
        private static DelegateBridge __Hotfix_ShowCloseUI;
        private static DelegateBridge __Hotfix_ShowWaitForReloginProcessingUI;
        private static DelegateBridge __Hotfix_ShowWaitReturnToLoginConfirmUI;
        private static DelegateBridge __Hotfix_ShowBundleDataVersionMisMatchUI;
        private static DelegateBridge __Hotfix_OnRetryLoginButtonClicked;
        private static DelegateBridge __Hotfix_OnReturnToLoginConfirmButtonClicked;
        private static DelegateBridge __Hotfix_IsNeedCheckLocalDataCache;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnLoginBySessionTokenAck;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitAck;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitEnd;
        private static DelegateBridge __Hotfix_OnGameServerDataUnsyncNotify;
        private static DelegateBridge __Hotfix_OnGameServerNetworkError;
        private static DelegateBridge __Hotfix_OnGameServerDisconnected;
        private static DelegateBridge __Hotfix_GetReloginCoolDownTime;

        [MethodImpl(0x8000)]
        public ReloginBySessionUITaskBase(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual float GetReloginCoolDownTime()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedCheckLocalDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerDataUnsyncNotify()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerNetworkError()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnLoginBySessionTokenAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnPlayerInfoInitAck(object msg)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnPlayerInfoInitEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnRetryLoginButtonClicked(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnReturnToLoginConfirmButtonClicked(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowBundleDataVersionMisMatchUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowCloseUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowErrorMsg(string key)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowWaitForReloginConfirmUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowWaitForReloginProcessingUI()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowWaitReturnToLoginConfirmUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartRelogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UnregisterNetworkEvent()
        {
        }

        protected enum LoginState
        {
            None,
            Inited,
            StartSessionLogin,
            StartPlayerInfoInit
        }
    }
}

