﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    [AddComponentMenu("UI/BlackJack/TextTypeWriterUIController")]
    public class TextTypeWriterUIController : MonoBehaviour
    {
        public int charsPerSecond;
        public float fadeInTime;
        public bool AutoStart;
        private Text mText;
        protected Color mTextColor;
        private string mFullText;
        private int mCurrentOffset;
        private float mNextChar;
        private bool mReset;
        private bool mActive;
        private List<FadeEntry> mFade;
        private static DelegateBridge __Hotfix_get_isActive;
        private static DelegateBridge __Hotfix_ResetToBeginning;
        private static DelegateBridge __Hotfix_Finish;
        private static DelegateBridge __Hotfix_StartTypeWriter;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_GetAlphaColor;
        private static DelegateBridge __Hotfix_Update;

        [MethodImpl(0x8000)]
        public void Finish()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetAlphaColor(float alpha)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetToBeginning()
        {
        }

        [MethodImpl(0x8000)]
        public void StartTypeWriter()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        public bool isActive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct FadeEntry
        {
            public int index;
            public string text;
            public float alpha;
        }
    }
}

