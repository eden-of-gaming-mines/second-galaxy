﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class CustomUIProcess : UIProcess
    {
        private readonly UIProcessExecutor m_processExecutor;
        [CompilerGenerated]
        private static Action<UIProcess> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnCancel;
        private static DelegateBridge __Hotfix_OnChildCancel;
        private static DelegateBridge __Hotfix_OnUIProcessExecutorFinished;

        [MethodImpl(0x8000)]
        public CustomUIProcess(UIProcessExecutor executor, UIProcess.ProcessExecMode execMode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnCancel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnChildCancel(UIProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStart()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUIProcessExecutorFinished(bool isCompleted)
        {
        }

        public delegate void UIProcessExecutor(Action<bool> onExecEnd);
    }
}

