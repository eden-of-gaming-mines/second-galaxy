﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using System;

    public enum ScrollSnapDirection
    {
        Vertical,
        Horizontal
    }
}

