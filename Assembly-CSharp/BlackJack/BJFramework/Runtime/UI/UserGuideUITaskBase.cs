﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class UserGuideUITaskBase : UITaskBase
    {
        protected UserGuideStepInfo m_currStepInfo;
        protected bool m_isNeedLoadDynamicRes;
        protected int m_currPageIndex;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsEnableOperationArea;
        private static DelegateBridge __Hotfix_OnNextPageClicked;
        private static DelegateBridge __Hotfix_OnOperationAreaClicked;

        [MethodImpl(0x8000)]
        public UserGuideUITaskBase(string name)
        {
        }

        protected abstract UserGuideUIControllerBase GetUIController();
        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        protected abstract bool InitDataFromUIIntent(UIIntent uiIntent);
        [MethodImpl(0x8000)]
        protected bool IsEnableOperationArea()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNextPageClicked()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnOperationAreaClicked(bool isClickOperationArea)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        protected abstract void OnStepEnd(bool isClickOperationArea);
        protected abstract void OnStepStart();
        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }
    }
}

