﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    [AddComponentMenu("UIExtend/ScrollViewEventHandler", 0x10)]
    public class ScrollViewEventHandler : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnScrollViewDragBegin;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnScrollViewDragEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnScrollViewClick;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_OnEndDrag;
        private static DelegateBridge __Hotfix_OnPointerClick;
        private static DelegateBridge __Hotfix_OnPointerDown;
        private static DelegateBridge __Hotfix_add_EventOnScrollViewDragBegin;
        private static DelegateBridge __Hotfix_remove_EventOnScrollViewDragBegin;
        private static DelegateBridge __Hotfix_add_EventOnScrollViewDragEnd;
        private static DelegateBridge __Hotfix_remove_EventOnScrollViewDragEnd;
        private static DelegateBridge __Hotfix_add_EventOnScrollViewClick;
        private static DelegateBridge __Hotfix_remove_EventOnScrollViewClick;

        public event Action EventOnScrollViewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnScrollViewDragBegin
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnScrollViewDragEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerClick(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerDown(PointerEventData eventData)
        {
        }
    }
}

