﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Utils;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class UIManager
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UITaskBase, string, string> EventOnUITaskNotify;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, bool> EventReturnToLoginUI;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventAndroidBackKeyUp;
        private static UIManager m_instance;
        protected Dictionary<string, int> m_globalUIInputBlockDict = new Dictionary<string, int>();
        private List<UIIntent> m_uiIntentStack = new List<UIIntent>();
        private Dictionary<string, UITaskBase> m_uiTaskDict = new Dictionary<string, UITaskBase>();
        private Dictionary<string, UITaskRegItem> m_uiTaskRegDict = new Dictionary<string, UITaskRegItem>();
        private List<UITaskBase> m_taskList4Stop = new List<UITaskBase>();
        private List<List<int>> m_uiTaskGroupConflictList = new List<List<int>>();
        protected DateTime m_lastTickPauseTimeOutTime = DateTime.MinValue;
        protected DelayExecHelper m_delayExecHelper = new DelayExecHelper();
        protected bool m_isTaskStopDuringPrepare;
        public const double UITaskPauseTimeOut = 120.0;
        private DateTime m_nextPrintBlockStateTime = DateTime.MinValue;
        private const double UIBlockStatePrintTime = 60000.0;
        protected bool m_inFullScreenAction;
        protected FullScreenActionPriority m_currFullScreenActionPriority;
        protected Action<Action> m_currFullScreenActionOnInterrupt;
        protected List<UIActionQueueItem> m_uiActionQueue = new List<UIActionQueueItem>();
        protected UIActionQueueItem m_currRuningUIAction;
        protected DateTime? m_currRuningUIActionTimeOut;
        protected List<Func<UIActionQueueItem, bool>> m_uiActionQueueBlockerList = new List<Func<UIActionQueueItem, bool>>();
        protected bool m_blockGlobalUIInputForWaitingItem;
        [CompilerGenerated]
        private static Comparison<UIActionQueueItem> <>f__mg$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateUIManager;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_StartUITask;
        private static DelegateBridge __Hotfix_StartUITaskWithPrepare;
        private static DelegateBridge __Hotfix_ReturnUITask;
        private static DelegateBridge __Hotfix_ReturnUITaskWithTaskName;
        private static DelegateBridge __Hotfix_ReturnUITaskToLast;
        private static DelegateBridge __Hotfix_ReturnUITaskByStackIndex;
        private static DelegateBridge __Hotfix_ReturnUITaskWithPrepare;
        private static DelegateBridge __Hotfix_PauseUITask;
        private static DelegateBridge __Hotfix_StopUITask;
        private static DelegateBridge __Hotfix_GetOrCreateUITask;
        private static DelegateBridge __Hotfix_FindUITaskWithName;
        private static DelegateBridge __Hotfix_StartUITaskInternal;
        private static DelegateBridge __Hotfix_StartOrResumeTask;
        private static DelegateBridge __Hotfix_PopIntentUntilReturnTarget;
        private static DelegateBridge __Hotfix_RemoveIntentFromStack;
        private static DelegateBridge __Hotfix_OnUITaskStop;
        private static DelegateBridge __Hotfix_OnUITaskStopDuringPrepare;
        private static DelegateBridge __Hotfix_OnUITaskNotify;
        private static DelegateBridge __Hotfix_CloseAllConflictUITask;
        private static DelegateBridge __Hotfix_StopUITaskByGroup;
        private static DelegateBridge __Hotfix_PauseUITaskByGroup;
        private static DelegateBridge __Hotfix_PauseUITaskByTag;
        private static DelegateBridge __Hotfix_StopUITaskByTag;
        private static DelegateBridge __Hotfix_RegisterUITaskWithGroup;
        private static DelegateBridge __Hotfix_RegisterUITaskTag;
        private static DelegateBridge __Hotfix_SetUITaskGroupConflict;
        private static DelegateBridge __Hotfix_ReturnToLoginUI;
        private static DelegateBridge __Hotfix_IsGlobalUIInputEnable;
        private static DelegateBridge __Hotfix_GlobalUIInputEnable;
        private static DelegateBridge __Hotfix_GlobalUIInputBlockClear;
        private static DelegateBridge __Hotfix_GlobalUIInputClearForReLogin;
        private static DelegateBridge __Hotfix_GlobalUIInputBlockForTicks;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_ProcessAndroidBackKey;
        private static DelegateBridge __Hotfix_TickPauseTimeOut;
        private static DelegateBridge __Hotfix_PrintUIBlockState;
        private static DelegateBridge __Hotfix_add_EventOnUITaskNotify;
        private static DelegateBridge __Hotfix_remove_EventOnUITaskNotify;
        private static DelegateBridge __Hotfix_add_EventReturnToLoginUI;
        private static DelegateBridge __Hotfix_remove_EventReturnToLoginUI;
        private static DelegateBridge __Hotfix_add_EventAndroidBackKeyUp;
        private static DelegateBridge __Hotfix_remove_EventAndroidBackKeyUp;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_StartFullScreenAction;
        private static DelegateBridge __Hotfix_OnFullScreenActionEnd;
        private static DelegateBridge __Hotfix_ClearFullScreenActionInfo;
        private static DelegateBridge __Hotfix_RegisterUIAction;
        private static DelegateBridge __Hotfix_UnregisterUIAction;
        private static DelegateBridge __Hotfix_ClearCurrRunningUIAction;
        private static DelegateBridge __Hotfix_ClearActionQueue;
        private static DelegateBridge __Hotfix_TickUIActionQueue;
        private static DelegateBridge __Hotfix_OnUIActionEnd;
        private static DelegateBridge __Hotfix_UpdateBlockGlobalUIInputForActionQueue;
        private static DelegateBridge __Hotfix_IsUIActionQueueItemExist;
        private static DelegateBridge __Hotfix_UIActionQueueComparsion;

        public event Action EventAndroidBackKeyUp
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UITaskBase, string, string> EventOnUITaskNotify
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, bool> EventReturnToLoginUI
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private UIManager()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void ClearActionQueue()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearCurrRunningUIAction()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearFullScreenActionInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseAllConflictUITask(string taskName)
        {
            DelegateBridge bridge = __Hotfix_CloseAllConflictUITask;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, taskName);
            }
            else
            {
                UITaskRegItem item;
                if (this.m_uiTaskRegDict.TryGetValue(taskName, out item))
                {
                    int taskGroup = item.m_taskGroup;
                    List<int> list = null;
                    if (this.m_uiTaskGroupConflictList.Count > taskGroup)
                    {
                        list = this.m_uiTaskGroupConflictList[taskGroup];
                    }
                    if ((list != null) && (list.Count != 0))
                    {
                        foreach (KeyValuePair<string, UITaskBase> pair in this.m_uiTaskDict)
                        {
                            UITaskRegItem item2;
                            if (!this.m_uiTaskRegDict.TryGetValue(pair.Value.Name, out item2))
                            {
                                continue;
                            }
                            if (list.Contains(item2.m_taskGroup))
                            {
                                this.m_taskList4Stop.Add(pair.Value);
                            }
                        }
                        if (this.m_taskList4Stop.Count != 0)
                        {
                            Debug.Log("UIManager::CloseAllConflictUITask::TaskName: " + taskName);
                            foreach (UITaskBase base2 in this.m_taskList4Stop)
                            {
                                base2.Stop();
                            }
                            this.m_taskList4Stop.Clear();
                        }
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public static UIManager CreateUIManager()
        {
            DelegateBridge bridge = __Hotfix_CreateUIManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp290();
            }
            if (m_instance == null)
            {
                m_instance = new UIManager();
            }
            return m_instance;
        }

        [MethodImpl(0x8000)]
        public UITaskBase FindUITaskWithName(string taskName, bool needFailLog = true)
        {
        }

        [MethodImpl(0x8000)]
        private UITaskBase GetOrCreateUITask(string taskName)
        {
            UITaskBase base2;
            DelegateBridge bridge = __Hotfix_GetOrCreateUITask;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp294(this, taskName);
            }
            if (!this.m_uiTaskDict.TryGetValue(taskName, out base2))
            {
                UITaskRegItem item;
                if (!this.m_uiTaskRegDict.TryGetValue(taskName, out item))
                {
                    Debug.LogError($"GetOrCreateUITask fail taskName={taskName} not registed");
                    return null;
                }
                object[] args = new object[] { taskName };
                base2 = ClassLoader.Instance.CreateInstance(item.m_taskTypeDnName, args) as UITaskBase;
                if (base2 == null)
                {
                    Debug.LogError($"GetOrCreateUITask CreateInstance fail taskName={taskName}");
                    return null;
                }
                if (item.m_tagList != null)
                {
                    foreach (string str in item.m_tagList)
                    {
                        base2.SetTag(str);
                    }
                }
                base2.InitlizeBeforeManagerStartIt();
                this.m_uiTaskDict[taskName] = base2;
            }
            return base2;
        }

        [MethodImpl(0x8000)]
        public void GlobalUIInputBlockClear(string srcName)
        {
        }

        [MethodImpl(0x8000)]
        public void GlobalUIInputBlockForTicks(int ticks)
        {
        }

        [MethodImpl(0x8000)]
        public void GlobalUIInputClearForReLogin()
        {
        }

        [MethodImpl(0x8000)]
        public void GlobalUIInputEnable(string srcName, bool enable)
        {
            DelegateBridge bridge = __Hotfix_GlobalUIInputEnable;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp12(this, srcName, enable);
            }
            else
            {
                if (!enable)
                {
                    int num3;
                    if (this.m_globalUIInputBlockDict.TryGetValue(srcName, out num3))
                    {
                        this.m_globalUIInputBlockDict[srcName] = num3 + 1;
                        Debug.Log($"UITaskBase::GlobalUIInputEnable, disableInput, srcName = {srcName}, count = {(num3 + 1).ToString()}");
                    }
                    else
                    {
                        this.m_globalUIInputBlockDict[srcName] = 1;
                        Debug.Log($"UITaskBase::GlobalUIInputEnable, disableInput, srcName = {srcName}, count = 1");
                    }
                }
                else
                {
                    int num;
                    if (this.m_globalUIInputBlockDict.TryGetValue(srcName, out num))
                    {
                        if (num == 1)
                        {
                            this.m_globalUIInputBlockDict.Remove(srcName);
                            Debug.Log($"UITaskBase::GlobalUIInputEnable, enableInput ,srcName = {srcName}, count = 0");
                        }
                        else
                        {
                            this.m_globalUIInputBlockDict[srcName] = num - 1;
                            Debug.Log($"UITaskBase::GlobalUIInputEnable, enableInput, srcName = {srcName}, count = {(num - 1).ToString()}");
                        }
                    }
                }
                SceneManager.Instance.EnableInputBlock(!this.IsGlobalUIInputEnable());
            }
        }

        [MethodImpl(0x8000)]
        public bool Initlize()
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            Debug.Log("UIManager.Initlize start");
            return true;
        }

        [MethodImpl(0x8000)]
        public bool IsGlobalUIInputEnable()
        {
            DelegateBridge bridge = __Hotfix_IsGlobalUIInputEnable;
            return ((bridge == null) ? (this.m_globalUIInputBlockDict.Count == 0) : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        public bool IsUIActionQueueItemExist(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFullScreenActionEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUIActionEnd(UIActionQueueItem item)
        {
        }

        [MethodImpl(0x8000)]
        public void OnUITaskNotify(UITaskBase task, UITaskNotifyType notifyType, string desc)
        {
            DelegateBridge bridge = __Hotfix_OnUITaskNotify;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp299(this, task, notifyType, desc);
            }
            else if (this.EventOnUITaskNotify != null)
            {
                this.EventOnUITaskNotify(task, notifyType.ToString(), desc);
            }
        }

        [MethodImpl(0x8000)]
        private void OnUITaskStop(Task task)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUITaskStopDuringPrepare(Task task)
        {
        }

        [MethodImpl(0x8000)]
        public void PauseUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        public void PauseUITaskByGroup(int group)
        {
        }

        [MethodImpl(0x8000)]
        public void PauseUITaskByTag(string tag, bool isExcept = false)
        {
        }

        [MethodImpl(0x8000)]
        private bool PopIntentUntilReturnTarget(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void PrintUIBlockState()
        {
            DelegateBridge bridge = __Hotfix_PrintUIBlockState;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.m_nextPrintBlockStateTime <= Timer.m_currTime)
            {
                if (this.m_globalUIInputBlockDict.Count == 0)
                {
                    Debug.Log("[UI锁]-界面解锁状态");
                }
                else
                {
                    Debug.Log("[UI锁]-界面锁定状态");
                    int num = 1;
                    foreach (KeyValuePair<string, int> pair in this.m_globalUIInputBlockDict)
                    {
                        Debug.Log($"[UI锁]-序号: {num}. 锁定名字: {pair.Key}, 锁定次数: {pair.Value}");
                        num++;
                    }
                }
                Debug.Log($"[UI锁]-EventSystem 状态: Active: {SceneManager.Instance.GetEventSystemActiveState()}, Enable: {SceneManager.Instance.GetEventSystemEnableState()}");
                this.m_nextPrintBlockStateTime = Timer.m_currTime.AddMilliseconds(60000.0);
            }
        }

        [MethodImpl(0x8000)]
        private void ProcessAndroidBackKey()
        {
            DelegateBridge bridge = __Hotfix_ProcessAndroidBackKey;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (((Application.platform == RuntimePlatform.Android) && Input.GetKeyUp(KeyCode.Escape)) && (this.EventAndroidBackKeyUp != null))
            {
                this.EventAndroidBackKeyUp();
            }
        }

        [MethodImpl(0x8000)]
        public void RegisterUIAction(UIActionQueueItem item)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterUITaskTag(string taskName, string TagName)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterUITaskWithGroup(string taskName, TypeDNName taskTypeDNName, int group, string luaModuleName = null)
        {
            DelegateBridge bridge = __Hotfix_RegisterUITaskWithGroup;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp300(this, taskName, taskTypeDNName, group, luaModuleName);
            }
            else
            {
                UITaskRegItem item;
                if (!this.m_uiTaskRegDict.TryGetValue(taskName, out item))
                {
                    item = new UITaskRegItem();
                    this.m_uiTaskRegDict.Add(taskName, item);
                }
                item.m_taskName = taskName;
                item.m_taskTypeDnName = taskTypeDNName;
                item.m_taskGroup = group;
                item.m_luaModuleName = luaModuleName;
            }
        }

        [MethodImpl(0x8000)]
        public void RemoveIntentFromStack(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        public void ReturnToLoginUI(bool switchAccount)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase ReturnUITask(UIIntent intent, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase ReturnUITaskByStackIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase ReturnUITaskToLast()
        {
        }

        [MethodImpl(0x8000)]
        public void ReturnUITaskWithPrepare(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public UITaskBase ReturnUITaskWithTaskName(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUITaskGroupConflict(uint group1, uint group2)
        {
        }

        [MethodImpl(0x8000)]
        public bool StartFullScreenAction(Action onStart, Action<Action> onInterrupt, FullScreenActionPriority priority)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartOrResumeTask(UITaskBase targetTask, UIIntent intent, Action<bool> onPipelineEnd)
        {
            DelegateBridge bridge = __Hotfix_StartOrResumeTask;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp298(this, targetTask, intent, onPipelineEnd);
            }
            switch (targetTask.State)
            {
                case Task.TaskState.Init:
                    targetTask.EventOnStop += new Action<Task>(this.OnUITaskStop);
                    return targetTask.Start(intent, onPipelineEnd);

                case Task.TaskState.Running:
                    return targetTask.OnNewIntent(intent, onPipelineEnd);

                case Task.TaskState.Paused:
                    return targetTask.Resume(intent, onPipelineEnd);

                case Task.TaskState.Stopped:
                    Debug.LogError($"StartOrResumeTask fail in TaskState.Stopped task={targetTask.Name}");
                    return false;
            }
            return false;
        }

        [MethodImpl(0x8000)]
        public UITaskBase StartUITask(UIIntent intent, bool pushIntentToStack = false, bool clearIntentStack = false, Action redirectPipLineOnLoadAllResCompleted = null, Action<bool> onPipelineEnd = null)
        {
            DelegateBridge bridge = __Hotfix_StartUITask;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp291(this, intent, pushIntentToStack, clearIntentStack, redirectPipLineOnLoadAllResCompleted, onPipelineEnd);
            }
            Debug.Log($"StartUITask task={intent.TargetTaskName}");
            if (!this.m_uiTaskRegDict.ContainsKey(intent.TargetTaskName))
            {
                Debug.LogError($"StartUITask fail for Unregisted TargetTaskName={intent.TargetTaskName}");
                return null;
            }
            UITaskBase orCreateUITask = this.GetOrCreateUITask(intent.TargetTaskName);
            if (orCreateUITask == null)
            {
                Debug.LogError($"StartUITask fail for GetOrCreateUITask null TargetTaskName={intent.TargetTaskName}");
                return null;
            }
            if (clearIntentStack)
            {
                this.m_uiIntentStack.Clear();
            }
            if (redirectPipLineOnLoadAllResCompleted != null)
            {
                orCreateUITask.RedirectPipLineOnAllResReady(redirectPipLineOnLoadAllResCompleted);
            }
            if (this.StartUITaskInternal<UITaskBase>(orCreateUITask, intent, pushIntentToStack, onPipelineEnd))
            {
                return orCreateUITask;
            }
            Debug.LogError($"StartUITask fail for StartUITaskInternal fail TargetTaskName={intent.TargetTaskName}");
            return null;
        }

        [MethodImpl(0x8000)]
        private bool StartUITaskInternal<TTaskType>(TTaskType targetTask, UIIntent intent, bool pushIntentToStack, Action<bool> onPipelineEnd = null) where TTaskType: UITaskBase
        {
            DelegateBridge bridge = __Hotfix_StartUITaskInternal;
            if (bridge != null)
            {
                bridge.InvokeSessionStart();
                bridge.InParam<UIManager>(this);
                bridge.InParam<TTaskType>(targetTask);
                bridge.InParam<UIIntent>(intent);
                bridge.InParam<bool>(pushIntentToStack);
                bridge.InParam<Action<bool>>(onPipelineEnd);
                bridge.Invoke(1);
                return bridge.InvokeSessionEndWithResult<bool>();
            }
            this.CloseAllConflictUITask(intent.TargetTaskName);
            if (pushIntentToStack)
            {
                this.m_uiIntentStack.Add(intent);
            }
            if (this.StartOrResumeTask(targetTask, intent, onPipelineEnd))
            {
                return true;
            }
            Debug.LogError($"StartUITask fail task={targetTask.Name}");
            return false;
        }

        [MethodImpl(0x8000)]
        public void StartUITaskWithPrepare(UIIntent intent, Action<bool> onPrepareEnd, bool pushIntentToStack = false, Action<bool> onPipelineEnd = null, Action redirectPipLineOnLoadAllResCompleted = null)
        {
        }

        [MethodImpl(0x8000)]
        public void StopUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        public void StopUITaskByGroup(int group)
        {
        }

        [MethodImpl(0x8000)]
        public void StopUITaskByTag(string tag, bool isExcept)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                try
                {
                    this.m_delayExecHelper.Tick((uint) Timer.m_currTick);
                    this.TickPauseTimeOut();
                    this.TickUIActionQueue();
                    this.ProcessAndroidBackKey();
                    this.PrintUIBlockState();
                }
                catch (Exception exception)
                {
                    Debug.LogError($"{"UIManager.Tick"} : {exception}");
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void TickPauseTimeOut()
        {
            DelegateBridge bridge = __Hotfix_TickPauseTimeOut;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if ((Timer.m_currTime - this.m_lastTickPauseTimeOutTime).TotalSeconds >= 5.0)
            {
                this.m_lastTickPauseTimeOutTime = Timer.m_currTime;
                DateTime time = Timer.m_currTime.AddSeconds(-120.0);
                foreach (KeyValuePair<string, UITaskBase> pair in this.m_uiTaskDict)
                {
                    <TickPauseTimeOut>c__AnonStorey2 storey = new <TickPauseTimeOut>c__AnonStorey2 {
                        task = pair.Value
                    };
                    if (storey.task.IsNeedPauseTimeOut && ((storey.task.State == Task.TaskState.Paused) && ((storey.task.PauseStartTime <= time) && (this.m_uiIntentStack.Find(new Predicate<UIIntent>(storey.<>m__0)) == null))))
                    {
                        this.m_taskList4Stop.Add(pair.Value);
                    }
                }
                if (this.m_taskList4Stop.Count != 0)
                {
                    foreach (UITaskBase base2 in this.m_taskList4Stop)
                    {
                        base2.Stop();
                    }
                    this.m_taskList4Stop.Clear();
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void TickUIActionQueue()
        {
            DelegateBridge bridge = __Hotfix_TickUIActionQueue;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
                return;
            }
            bool flag = false;
            DateTime currTime = Timer.m_currTime;
            if (this.m_currRuningUIAction != null)
            {
                if ((this.m_currRuningUIActionTimeOut != null) && (this.m_currRuningUIActionTimeOut.Value < currTime))
                {
                    this.m_currRuningUIAction.OnEnd();
                }
                return;
            }
            if (this.m_uiActionQueue.Count == 0)
            {
                return;
            }
            int index = this.m_uiActionQueue.Count - 1;
            goto TR_001E;
        TR_0008:
            if (flag)
            {
                this.UpdateBlockGlobalUIInputForActionQueue();
            }
            return;
        TR_001E:
            while (true)
            {
                if (index >= 0)
                {
                    this.m_currRuningUIAction = this.m_uiActionQueue[index];
                    bool onlyTryOnce = this.m_currRuningUIAction.m_onlyTryOnce;
                    if (this.m_currRuningUIAction.m_waitTimeOutTime != null)
                    {
                        DateTime? waitTimeOutTime = this.m_currRuningUIAction.m_waitTimeOutTime;
                        if ((waitTimeOutTime != null) && (waitTimeOutTime.GetValueOrDefault() < currTime))
                        {
                            Debug.Log($"  超时-注销UIActionQueue, itemName: {this.m_currRuningUIAction.m_name}");
                            this.m_currRuningUIAction = null;
                            this.m_uiActionQueue.RemoveAt(index);
                            flag = true;
                            break;
                        }
                    }
                    if (((this.m_currRuningUIAction == null) || (this.m_currRuningUIAction.m_cbCanStart == null)) || this.m_currRuningUIAction.m_cbCanStart())
                    {
                        Debug.Log($"   执行-注销UIActionQueue, itemName: {this.m_currRuningUIAction.m_name}");
                        this.m_uiActionQueue.RemoveAt(index);
                        flag = true;
                    }
                    else
                    {
                        Debug.Log($"  不能执行-注销UIActionQueue, itemName: {this.m_currRuningUIAction.m_name}");
                        this.m_currRuningUIAction = null;
                        if (onlyTryOnce)
                        {
                            this.m_uiActionQueue.RemoveAt(index);
                            flag = true;
                        }
                    }
                    if (this.m_currRuningUIAction != null)
                    {
                        if (this.m_currRuningUIAction.m_processTimeOut != 0)
                        {
                            this.m_currRuningUIActionTimeOut = new DateTime?(Timer.m_currTime.AddMilliseconds((double) this.m_currRuningUIAction.m_processTimeOut));
                        }
                        Debug.Log($"   执行UIActionQueue, itemName: {this.m_currRuningUIAction.m_name}");
                        this.m_currRuningUIAction.m_cbOnStart();
                        if ((this.m_currRuningUIAction != null) && this.m_currRuningUIAction.m_blockGlobalUIInputForProcessing)
                        {
                            this.GlobalUIInputEnable(this.m_currRuningUIAction.ToString(), false);
                            Debug.Log($"DisableGlobalUIInputForActionQueue, itemName: {this.m_currRuningUIAction.m_name}");
                        }
                        goto TR_0008;
                    }
                }
                else
                {
                    goto TR_0008;
                }
                break;
            }
            index--;
            goto TR_001E;
        }

        [MethodImpl(0x8000)]
        private static int UIActionQueueComparsion(UIActionQueueItem x, UIActionQueueItem y)
        {
        }

        [MethodImpl(0x8000)]
        public void Uninitlize()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterUIAction(UIActionQueueItem item)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBlockGlobalUIInputForActionQueue()
        {
        }

        public static UIManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp290());
            }
        }

        [CompilerGenerated]
        private sealed class <ReturnUITaskWithPrepare>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
            internal UIIntent intent;
            internal UITaskBase targetTask;
            internal UIManager $this;

            internal void <>m__0(bool lret)
            {
                if (!lret)
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(true);
                    }
                    if (!this.$this.PopIntentUntilReturnTarget(this.intent))
                    {
                        Debug.LogError($"ReturnUITask fail intent not in stack task={this.intent.TargetTaskName}");
                        this.onPrepareEnd(false);
                    }
                    else
                    {
                        this.$this.StartUITaskInternal<UITaskBase>(this.targetTask, this.intent, false, null);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartFullScreenAction>c__AnonStorey3
        {
            internal Action onStart;
            internal Action<Action> onInterrupt;
            internal UIManager.FullScreenActionPriority priority;
            internal UIManager $this;

            internal void <>m__0()
            {
                this.$this.StartFullScreenAction(this.onStart, this.onInterrupt, this.priority);
            }
        }

        [CompilerGenerated]
        private sealed class <StartUITaskWithPrepare>c__AnonStorey0
        {
            internal UITaskBase targetTask;
            internal Action<bool> onPrepareEnd;
            internal Action redirectPipLineOnLoadAllResCompleted;
            internal UIIntent intent;
            internal bool pushIntentToStack;
            internal Action<bool> onPipelineEnd;
            internal UIManager $this;

            internal void <>m__0(bool lret)
            {
                this.targetTask.EventOnStop -= new Action<Task>(this.$this.OnUITaskStopDuringPrepare);
                if (this.$this.m_isTaskStopDuringPrepare)
                {
                    lret = false;
                }
                if (!lret)
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                    if (this.targetTask.State == Task.TaskState.Init)
                    {
                        this.$this.m_uiTaskDict.Remove(this.targetTask.Name);
                    }
                }
                else
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(true);
                    }
                    if (this.redirectPipLineOnLoadAllResCompleted != null)
                    {
                        this.targetTask.RedirectPipLineOnAllResReady(this.redirectPipLineOnLoadAllResCompleted);
                    }
                    this.$this.StartUITaskInternal<UITaskBase>(this.targetTask, this.intent, this.pushIntentToStack, this.onPipelineEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <TickPauseTimeOut>c__AnonStorey2
        {
            internal UITaskBase task;

            internal bool <>m__0(UIIntent intent) => 
                (intent.TargetTaskName == this.task.Name);
        }

        public enum FullScreenActionPriority
        {
            SimpleNpcDialog,
            QuestAcceptDialog
        }

        public class UIActionQueueItem
        {
            public int m_priority;
            public int m_typeId;
            public string m_name;
            public object m_ctx;
            public Func<bool> m_cbCanStart;
            public Action m_cbOnStart;
            public Action m_cbOnEnd;
            public int m_waitTimeOut;
            public DateTime? m_waitTimeOutTime;
            public int m_processTimeOut;
            public bool m_onlyTryOnce = true;
            public bool m_blockGlobalUIInputForWaiting;
            public bool m_blockGlobalUIInputForProcessing;
            public bool m_ignoreBlockGlobalUIInputForWaitingInProcessing;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_OnEnd;

            public UIActionQueueItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void OnEnd()
            {
                DelegateBridge bridge = __Hotfix_OnEnd;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    UIManager.Instance.OnUIActionEnd(this);
                    if (this.m_cbOnEnd != null)
                    {
                        this.m_cbOnEnd();
                    }
                }
            }
        }

        public enum UITaskNotifyType
        {
            Custom,
            UpdateViewComplete
        }

        private class UITaskRegItem
        {
            public int m_taskGroup;
            public string m_taskName;
            public TypeDNName m_taskTypeDnName;
            public string m_luaModuleName;
            public List<string> m_tagList = new List<string>();
            private static DelegateBridge _c__Hotfix_ctor;

            public UITaskRegItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

