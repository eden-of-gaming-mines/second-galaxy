﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class UIStateDesc
    {
        public string StateName;
        public int StateColorSetIndex = -1;
        public List<GameObject> SetToShowGameObjectList = new List<GameObject>();
        public List<TweenMain> TweenAnimationList = new List<TweenMain>();
        public List<UIStateGradientColorDesc> GradientDescList = new List<UIStateGradientColorDesc>();
        public bool IsUnscaledTime = true;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public UIStateDesc()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

