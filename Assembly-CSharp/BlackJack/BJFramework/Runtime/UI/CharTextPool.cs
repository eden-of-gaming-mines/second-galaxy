﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharTextPool
    {
        public Stack<GameObject> m_unusedText;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Request;
        private static DelegateBridge __Hotfix_Recycle;
        private static DelegateBridge __Hotfix_Release;

        [MethodImpl(0x8000)]
        public void Recycle(GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public Text Request()
        {
        }
    }
}

