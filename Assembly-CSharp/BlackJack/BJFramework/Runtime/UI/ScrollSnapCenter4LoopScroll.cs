﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class ScrollSnapCenter4LoopScroll : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
    {
        public float m_itemSize;
        public ScrollSnapDirection m_scrollSnapDir;
        public ScrollSnapPageType m_scrollSnapPageType;
        public float m_dragPercentage;
        [Header("回弹的敏感度")]
        public float m_sensitivity;
        [Header("是否允许快速滑动")]
        public bool m_isEnableSwipe;
        [Header("快速滑动的阈值")]
        public float m_swipeVelocity;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCenterItemChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEndDrag;
        private int m_scrollStartIndex;
        private int m_scrollEndIndex;
        private bool m_isSnaping;
        private Vector2 m_snapPosition;
        private int m_snapIndex;
        private int m_itemCount;
        private LoopScrollRect m_scrollRect;
        private int m_currentCenterIndex;
        private int m_beginDragCenterIndex;
        private Vector2 m_beginDragPosition;
        private Vector2 m_endFragPosition;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_LateUpdate;
        private static DelegateBridge __Hotfix_SetItemCount;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_ComputeItemPosition;
        private static DelegateBridge __Hotfix_GetCenterItemIndexUnclamped;
        private static DelegateBridge __Hotfix_Snap_1;
        private static DelegateBridge __Hotfix_AutoSnapToNext;
        private static DelegateBridge __Hotfix_GetItemIndexByDragDistance;
        private static DelegateBridge __Hotfix_GetItemIndexBySwipeDistance;
        private static DelegateBridge __Hotfix_ComputeSnapCenterPosition;
        private static DelegateBridge __Hotfix_Snap_0;
        private static DelegateBridge __Hotfix_GetNextItemIndex;
        private static DelegateBridge __Hotfix_IsSwipe;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_OnEndDrag;
        private static DelegateBridge __Hotfix_RecordStartAndEndIndex;
        private static DelegateBridge __Hotfix_add_EventOnCenterItemChanged;
        private static DelegateBridge __Hotfix_remove_EventOnCenterItemChanged;
        private static DelegateBridge __Hotfix_add_EventOnEndDrag;
        private static DelegateBridge __Hotfix_remove_EventOnEndDrag;

        public event Action<int> EventOnCenterItemChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEndDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AutoSnapToNext(bool isNext)
        {
        }

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 ComputeItemPosition(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 ComputeSnapCenterPosition(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCenterItemIndexUnclamped()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemCount()
        {
        }

        [MethodImpl(0x8000)]
        private int GetItemIndexByDragDistance()
        {
        }

        [MethodImpl(0x8000)]
        private int GetItemIndexBySwipeDistance()
        {
        }

        [MethodImpl(0x8000)]
        private int GetNextItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSwipe()
        {
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void RecordStartAndEndIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        private void Snap(Vector2 p)
        {
        }

        [MethodImpl(0x8000)]
        public void Snap(int idx, bool smooth = true)
        {
        }
    }
}

