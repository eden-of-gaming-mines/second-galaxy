﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class UIIntent
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <TargetTaskName>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <TargetMode>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TargetTaskName;
        private static DelegateBridge __Hotfix_set_TargetTaskName;
        private static DelegateBridge __Hotfix_get_TargetMode;
        private static DelegateBridge __Hotfix_set_TargetMode;

        [MethodImpl(0x8000)]
        public UIIntent(string targetTaskName, string targetMode = null)
        {
            this.TargetTaskName = targetTaskName;
            this.TargetMode = targetMode;
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, targetTaskName, targetMode);
            }
        }

        public string TargetTaskName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_TargetTaskName;
                return ((bridge == null) ? this.<TargetTaskName>k__BackingField : bridge.__Gen_Delegate_Imp33(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
                DelegateBridge bridge = __Hotfix_set_TargetTaskName;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<TargetTaskName>k__BackingField = value;
                }
            }
        }

        public string TargetMode
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_TargetMode;
                return ((bridge == null) ? this.<TargetMode>k__BackingField : bridge.__Gen_Delegate_Imp33(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_TargetMode;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<TargetMode>k__BackingField = value;
                }
            }
        }
    }
}

