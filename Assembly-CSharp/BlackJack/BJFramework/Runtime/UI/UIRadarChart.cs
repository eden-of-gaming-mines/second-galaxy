﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    [AddComponentMenu("UIExtend/UIRadarChart", 0x10)]
    public class UIRadarChart : MaskableGraphic
    {
        public bool isFill;
        [Range(0f, 0.99f)]
        public float fillPercent;
        [Range(0f, 1f)]
        public float[] values;
        [Range(0f, 360f)]
        public float angleOffset;
        public bool useStateLine;
        public Color lineColor;
        public float lineWidth;
        [Range(0f, 1f)]
        public float lineLength;
        private static DelegateBridge __Hotfix_OnPopulateMesh;
        private static DelegateBridge __Hotfix_GetLine;
        private static DelegateBridge __Hotfix_GetPoint;
        private static DelegateBridge __Hotfix_GetQuad;

        [MethodImpl(0x8000)]
        private UIVertex[] GetLine(Vector2 start, Vector2 end)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 GetPoint(Vector2 size, int i)
        {
        }

        [MethodImpl(0x8000)]
        private UIVertex[] GetQuad(params Vector2[] vertPos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPopulateMesh(VertexHelper vh)
        {
        }
    }
}

