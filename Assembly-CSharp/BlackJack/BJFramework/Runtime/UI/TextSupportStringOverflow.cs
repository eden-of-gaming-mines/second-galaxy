﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    [AddComponentMenu("UI/TextSupportStringOverflow")]
    public class TextSupportStringOverflow : Text, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler, IEventSystemHandler
    {
        private bool m_isNeedRedrawTextStringMesh;
        private string m_displayText;
        private bool m_isTextOverflow;
        protected static GameObject m_overflowTextTipWindowGameObject;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Action<Text> EventOnOverflowTextClick;
        [SerializeField]
        private bool m_isSettingTextBoxHeightManualy;
        [SerializeField]
        private float m_heightMax;
        private static DelegateBridge __Hotfix_OnPopulateMesh;
        private static DelegateBridge __Hotfix_get_text;
        private static DelegateBridge __Hotfix_set_text;
        private static DelegateBridge __Hotfix_RefreshDisplayTextWithEllipsis;
        private static DelegateBridge __Hotfix_OnDisplayTextOverflow;
        private static DelegateBridge __Hotfix_GetCurrDisplayedCharCountFromTextGenerator;
        private static DelegateBridge __Hotfix_SearchLastUITextLineIndexLessThanTextBoxHeightMax;
        private static DelegateBridge __Hotfix_CalcUITextLineBottomY2RectTopBorder;
        private static DelegateBridge __Hotfix_UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick;
        private static DelegateBridge __Hotfix_UnityEngine.EventSystems.IPointerUpHandler.OnPointerUp;
        private static DelegateBridge __Hotfix_UnityEngine.EventSystems.IPointerDownHandler.OnPointerDown;
        private static DelegateBridge __Hotfix_get_preferredHeight;
        private static DelegateBridge __Hotfix_add_EventOnOverflowTextClick;
        private static DelegateBridge __Hotfix_remove_EventOnOverflowTextClick;
        private static DelegateBridge __Hotfix_get_IsSettingTextBoxHeightManualy;
        private static DelegateBridge __Hotfix_set_IsSettingTextBoxHeightManualy;
        private static DelegateBridge __Hotfix_get_HeightMax;
        private static DelegateBridge __Hotfix_set_HeightMax;

        public static  event Action<Text> EventOnOverflowTextClick
        {
            [MethodImpl(0x8000)] add
            {
                DelegateBridge bridge = __Hotfix_add_EventOnOverflowTextClick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(value);
                }
                else
                {
                    Action<Text> eventOnOverflowTextClick = EventOnOverflowTextClick;
                    while (true)
                    {
                        Action<Text> a = eventOnOverflowTextClick;
                        eventOnOverflowTextClick = Interlocked.CompareExchange<Action<Text>>(ref EventOnOverflowTextClick, (Action<Text>) Delegate.Combine(a, value), eventOnOverflowTextClick);
                        if (object.ReferenceEquals(eventOnOverflowTextClick, a))
                        {
                            return;
                        }
                    }
                }
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected float CalcUITextLineBottomY2RectTopBorder(UILineInfo lineInfo, RectTransform textTrans)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetCurrDisplayedCharCountFromTextGenerator(TextGenerator generator)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDisplayTextOverflow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPopulateMesh(VertexHelper toFill)
        {
        }

        [MethodImpl(0x8000)]
        protected void RefreshDisplayTextWithEllipsis(string srcStr)
        {
        }

        [MethodImpl(0x8000)]
        protected int SearchLastUITextLineIndexLessThanTextBoxHeightMax(TextGenerator generator)
        {
        }

        [MethodImpl(0x8000)]
        void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
        }

        public override string text
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public override float preferredHeight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [HideInInspector]
        public bool IsSettingTextBoxHeightManualy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [HideInInspector]
        public float HeightMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

