﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    [AddComponentMenu("UI/Effects/Gradient")]
    public class Gradient : BaseMeshEffect
    {
        public GradientDirection m_gradientDir;
        public Color m_color1 = Color.white;
        public Color m_color2 = Color.black;
        private static DelegateBridge __Hotfix_ModifyMesh;

        [MethodImpl(0x8000)]
        public override unsafe void ModifyMesh(VertexHelper vh)
        {
            DelegateBridge bridge = __Hotfix_ModifyMesh;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, vh);
            }
            else if (this.IsActive())
            {
                UIVertex simpleVert = UIVertex.simpleVert;
                float maxValue = float.MaxValue;
                float minValue = float.MinValue;
                for (int i = 0; i < vh.currentVertCount; i++)
                {
                    vh.PopulateUIVertex(ref simpleVert, i);
                    float b = (this.m_gradientDir != GradientDirection.Vertical) ? simpleVert.position.x : simpleVert.position.y;
                    maxValue = Mathf.Min(maxValue, b);
                    minValue = Mathf.Max(minValue, b);
                }
                float num5 = minValue - maxValue;
                if (num5 > 0f)
                {
                    for (int j = 0; j < vh.currentVertCount; j++)
                    {
                        vh.PopulateUIVertex(ref simpleVert, j);
                        UIVertex* vertexPtr1 = (UIVertex*) ref simpleVert;
                        vertexPtr1->color *= Color.Lerp(this.m_color2, this.m_color1, (((this.m_gradientDir != GradientDirection.Vertical) ? simpleVert.position.x : simpleVert.position.y) - maxValue) / num5);
                        vh.SetUIVertex(simpleVert, j);
                    }
                }
            }
        }
    }
}

