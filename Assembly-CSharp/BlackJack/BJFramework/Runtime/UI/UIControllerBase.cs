﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;

    public class UIControllerBase : PrefabControllerBase
    {
        public bool AutoInitLocalizedString;
        protected Dictionary<string, Action<UIControllerBase>> m_buttonClickListenerDict;
        protected Dictionary<string, Action<UIControllerBase>> m_buttonDoubleClickListenerDict;
        protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressStartListenerDict;
        protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressingListenerDict;
        protected Dictionary<string, Action<UIControllerBase>> m_buttonLongPressEndListenerDict;
        protected Dictionary<string, Action<UIControllerBase, bool>> m_toggleValueChangedListenerDict;
        private static int m_lastButtonClickFrameCount = -1;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_BindFields;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_BindFieldImpl;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnButtonClick;
        private static DelegateBridge __Hotfix_CheckAndUpdateCurrFrameButtonClickStateInfo;
        private static DelegateBridge __Hotfix_OnButtonDoubleClick;
        private static DelegateBridge __Hotfix_OnButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnButtonLongPressing;
        private static DelegateBridge __Hotfix_OnButtonLongPressEnd;
        private static DelegateBridge __Hotfix_SetButtonClickListener_0;
        private static DelegateBridge __Hotfix_SetButtonClickListener_1;
        private static DelegateBridge __Hotfix_SetButtonDoubleClickListener;
        private static DelegateBridge __Hotfix_SetButtonLongPressStartListener;
        private static DelegateBridge __Hotfix_SetButtonLongPressingListener;
        private static DelegateBridge __Hotfix_SetButtonLongPressEndListener;
        private static DelegateBridge __Hotfix_OnToggleValueChanged;
        private static DelegateBridge __Hotfix_SetToggleValueChangedListener_0;
        private static DelegateBridge __Hotfix_SetToggleValueChangedListener_1;
        private static DelegateBridge __Hotfix_OnDestroy;
        private static DelegateBridge __Hotfix_ToString;
        private static DelegateBridge __Hotfix_InitLocalizedString;

        [MethodImpl(0x8000)]
        protected override UnityEngine.Object BindFieldImpl(System.Type fieldType, string path, AutoBindAttribute.InitState initState, string fieldName, string ctrlName = null, bool optional = false)
        {
            UnityEngine.Object obj2;
            DelegateBridge bridge = __Hotfix_BindFieldImpl;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp115(this, fieldType, path, initState, fieldName, ctrlName, optional);
            }
            <BindFieldImpl>c__AnonStorey1 storey = new <BindFieldImpl>c__AnonStorey1 {
                fieldName = fieldName,
                $this = this
            };
            if (!fieldType.IsSubclassOf(typeof(Selectable)))
            {
                obj2 = base.BindFieldImpl(fieldType, path, initState, storey.fieldName, ctrlName, optional);
            }
            else
            {
                GameObject childByPath = base.GetChildByPath(path);
                if (childByPath == null)
                {
                    Debug.LogError($"BindFields fail can not found child {path}");
                    return null;
                }
                Component component = childByPath.GetComponent(fieldType);
                if (component == null)
                {
                    Debug.LogError($"BindFields fail can not found comp in child {path} {fieldType.Name}");
                    return null;
                }
                obj2 = component;
                string name = fieldType.Name;
                if (name != null)
                {
                    if (name == "Button")
                    {
                        <BindFieldImpl>c__AnonStorey0 storey2 = new <BindFieldImpl>c__AnonStorey0 {
                            <>f__ref$1 = storey,
                            button = component as Button
                        };
                        if (storey2.button != null)
                        {
                            storey2.button.onClick.AddListener(new UnityAction(storey2.<>m__0));
                        }
                    }
                    else if (name == "ButtonEx")
                    {
                        <BindFieldImpl>c__AnonStorey2 storey3 = new <BindFieldImpl>c__AnonStorey2 {
                            <>f__ref$1 = storey,
                            button = component as ButtonEx
                        };
                        if (storey3.button != null)
                        {
                            storey3.button.onClick.AddListener(new UnityAction(storey3.<>m__0));
                            storey3.button.onDoubleClick.AddListener(new UnityAction(storey3.<>m__1));
                            storey3.button.onLongPressStart.AddListener(new UnityAction(storey3.<>m__2));
                            storey3.button.onLongPressing.AddListener(new UnityAction(storey3.<>m__3));
                            storey3.button.onLongPressEnd.AddListener(new UnityAction(storey3.<>m__4));
                        }
                    }
                    else if (name == "Toggle")
                    {
                        <BindFieldImpl>c__AnonStorey3 storey4 = new <BindFieldImpl>c__AnonStorey3 {
                            <>f__ref$1 = storey,
                            toggle = component as Toggle
                        };
                        if (storey4.toggle != null)
                        {
                            storey4.toggle.onValueChanged.AddListener(new UnityAction<bool>(storey4.<>m__0));
                        }
                    }
                    else if (name == "ToggleEx")
                    {
                        <BindFieldImpl>c__AnonStorey4 storey5 = new <BindFieldImpl>c__AnonStorey4 {
                            <>f__ref$1 = storey,
                            toggle = component as ToggleEx
                        };
                        if (storey5.toggle != null)
                        {
                            storey5.toggle.onValueChanged.AddListener(new UnityAction<bool>(storey5.<>m__0));
                        }
                    }
                }
            }
            return obj2;
        }

        [MethodImpl(0x8000)]
        public override void BindFields()
        {
            DelegateBridge bridge = __Hotfix_BindFields;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                base.BindFields();
            }
        }

        [MethodImpl(0x8000)]
        public static bool CheckAndUpdateCurrFrameButtonClickStateInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public override void Initlize(string ctrlName, bool bindNow)
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp12(this, ctrlName, bindNow);
            }
            else
            {
                base.Initlize(ctrlName, bindNow);
            }
        }

        [MethodImpl(0x8000)]
        public static void InitLocalizedString(GameObject goRoot)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
            DelegateBridge bridge = __Hotfix_OnBindFiledsCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                base.OnBindFiledsCompleted();
            }
        }

        [MethodImpl(0x8000)]
        protected void OnButtonClick(Button button, string fieldName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonDoubleClick(Button button, string fieldName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonLongPressEnd(Button button, string fieldName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonLongPressing(Button button, string fieldName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonLongPressStart(Button button, string fieldName)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDestroy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleValueChanged(Toggle toggle, string fieldName, bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonClickListener(string fieldName, Action<UIControllerBase> action)
        {
            DelegateBridge bridge = __Hotfix_SetButtonClickListener_0;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, fieldName, action);
            }
            else
            {
                if (this.m_buttonClickListenerDict == null)
                {
                    this.m_buttonClickListenerDict = new Dictionary<string, Action<UIControllerBase>>();
                }
                this.m_buttonClickListenerDict[fieldName] = action;
            }
        }

        [MethodImpl(0x8000)]
        public void SetButtonClickListener(string[] fieldNames, Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonDoubleClickListener(string fieldName, Action<UIControllerBase> action)
        {
            DelegateBridge bridge = __Hotfix_SetButtonDoubleClickListener;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, fieldName, action);
            }
            else
            {
                if (this.m_buttonDoubleClickListenerDict == null)
                {
                    this.m_buttonDoubleClickListenerDict = new Dictionary<string, Action<UIControllerBase>>();
                }
                this.m_buttonDoubleClickListenerDict[fieldName] = action;
            }
        }

        [MethodImpl(0x8000)]
        public void SetButtonLongPressEndListener(string fieldName, Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonLongPressingListener(string fieldName, Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonLongPressStartListener(string fieldName, Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleValueChangedListener(string fieldName, Action<UIControllerBase, bool> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleValueChangedListener(string[] fieldNames, Action<UIControllerBase, bool> action)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }

        [CompilerGenerated]
        private sealed class <BindFieldImpl>c__AnonStorey0
        {
            internal Button button;
            internal UIControllerBase.<BindFieldImpl>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0()
            {
                this.<>f__ref$1.$this.OnButtonClick(this.button, this.<>f__ref$1.fieldName);
            }
        }

        [CompilerGenerated]
        private sealed class <BindFieldImpl>c__AnonStorey1
        {
            internal string fieldName;
            internal UIControllerBase $this;
        }

        [CompilerGenerated]
        private sealed class <BindFieldImpl>c__AnonStorey2
        {
            internal ButtonEx button;
            internal UIControllerBase.<BindFieldImpl>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0()
            {
                this.<>f__ref$1.$this.OnButtonClick(this.button, this.<>f__ref$1.fieldName);
            }

            internal void <>m__1()
            {
                this.<>f__ref$1.$this.OnButtonDoubleClick(this.button, this.<>f__ref$1.fieldName);
            }

            internal void <>m__2()
            {
                this.<>f__ref$1.$this.OnButtonLongPressStart(this.button, this.<>f__ref$1.fieldName);
            }

            internal void <>m__3()
            {
                this.<>f__ref$1.$this.OnButtonLongPressing(this.button, this.<>f__ref$1.fieldName);
            }

            internal void <>m__4()
            {
                this.<>f__ref$1.$this.OnButtonLongPressEnd(this.button, this.<>f__ref$1.fieldName);
            }
        }

        [CompilerGenerated]
        private sealed class <BindFieldImpl>c__AnonStorey3
        {
            internal Toggle toggle;
            internal UIControllerBase.<BindFieldImpl>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0(bool value)
            {
                this.<>f__ref$1.$this.OnToggleValueChanged(this.toggle, this.<>f__ref$1.fieldName, value);
            }
        }

        [CompilerGenerated]
        private sealed class <BindFieldImpl>c__AnonStorey4
        {
            internal ToggleEx toggle;
            internal UIControllerBase.<BindFieldImpl>c__AnonStorey1 <>f__ref$1;

            internal void <>m__0(bool value)
            {
                this.<>f__ref$1.$this.OnToggleValueChanged(this.toggle, this.<>f__ref$1.fieldName, value);
            }
        }
    }
}

