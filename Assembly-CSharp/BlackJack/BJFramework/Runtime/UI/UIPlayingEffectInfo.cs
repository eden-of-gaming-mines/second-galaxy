﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class UIPlayingEffectInfo
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private CommonUIStateController <UIStateCtrl>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <UIStateName>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsImmediateComplete>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <AllowToRefreshSameState>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_UIStateCtrl;
        private static DelegateBridge __Hotfix_set_UIStateCtrl;
        private static DelegateBridge __Hotfix_get_UIStateName;
        private static DelegateBridge __Hotfix_set_UIStateName;
        private static DelegateBridge __Hotfix_get_IsImmediateComplete;
        private static DelegateBridge __Hotfix_set_IsImmediateComplete;
        private static DelegateBridge __Hotfix_get_AllowToRefreshSameState;
        private static DelegateBridge __Hotfix_set_AllowToRefreshSameState;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo(CommonUIStateController uiStateCtrl, string uiStateName, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
            if ((uiStateCtrl == null) || string.IsNullOrEmpty(uiStateName))
            {
                throw new ApplicationException("UIPlayingEffectInfo.ctor faild!");
            }
            this.UIStateCtrl = uiStateCtrl;
            this.UIStateName = uiStateName;
            this.IsImmediateComplete = isImmediateComplete;
            this.AllowToRefreshSameState = allowToRefreshSameState;
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp207(this, uiStateCtrl, uiStateName, isImmediateComplete, allowToRefreshSameState);
            }
        }

        public CommonUIStateController UIStateCtrl
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_UIStateCtrl;
                return ((bridge == null) ? this.<UIStateCtrl>k__BackingField : bridge.__Gen_Delegate_Imp206(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_UIStateCtrl;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<UIStateCtrl>k__BackingField = value;
                }
            }
        }

        public string UIStateName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_UIStateName;
                return ((bridge == null) ? this.<UIStateName>k__BackingField : bridge.__Gen_Delegate_Imp33(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_UIStateName;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<UIStateName>k__BackingField = value;
                }
            }
        }

        public bool IsImmediateComplete
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_IsImmediateComplete;
                return ((bridge == null) ? this.<IsImmediateComplete>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_IsImmediateComplete;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, value);
                }
                else
                {
                    this.<IsImmediateComplete>k__BackingField = value;
                }
            }
        }

        public bool AllowToRefreshSameState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_AllowToRefreshSameState;
                return ((bridge == null) ? this.<AllowToRefreshSameState>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
                DelegateBridge bridge = __Hotfix_set_AllowToRefreshSameState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, value);
                }
                else
                {
                    this.<AllowToRefreshSameState>k__BackingField = value;
                }
            }
        }
    }
}

