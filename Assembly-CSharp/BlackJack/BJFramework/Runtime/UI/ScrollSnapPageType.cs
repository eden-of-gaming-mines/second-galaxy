﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using System;

    public enum ScrollSnapPageType
    {
        HalfOfItem,
        DragPercentage
    }
}

