﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class GameEntryUITaskBase : NetWorkCheckUITask
    {
        protected bool? m_startBundlePreDownloadByPlayer;
        protected int m_configDataInitLoadCount;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_StartInitSDK;
        private static DelegateBridge __Hotfix_OnSDKInitEnd;
        private static DelegateBridge __Hotfix_IsSDKLogin;
        private static DelegateBridge __Hotfix_EntryPipeLine;
        private static DelegateBridge __Hotfix_UpdateAssetBundleDownloadUrl;
        private static DelegateBridge __Hotfix_OnUpdateAssetBundleDownloadUrl;
        private static DelegateBridge __Hotfix_OnUpdateBundleDownloadUrlStart;
        private static DelegateBridge __Hotfix_OnUpdateBundleDownloadUrlEnd;
        private static DelegateBridge __Hotfix_OnStreamingAssetsFilesProcessingStart;
        private static DelegateBridge __Hotfix_OnStreamingAssetsFilesProcessingEnd;
        private static DelegateBridge __Hotfix_OnBundleDataLoadingStart;
        private static DelegateBridge __Hotfix_OnBundleDataLoadingEnd;
        private static DelegateBridge __Hotfix_OnBasicVersionUnmatch;
        private static DelegateBridge __Hotfix_NotifyUserDownloadAndWait;
        private static DelegateBridge __Hotfix_StartPreDownload;
        private static DelegateBridge __Hotfix_RefusePreDownload;
        private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingRefuse;
        private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingStart;
        private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingEnd;
        private static DelegateBridge __Hotfix_OnAssetBundleManifestLoadingStart;
        private static DelegateBridge __Hotfix_OnAssetBundleManifestLoadingEnd;
        private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysStart;
        private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysEnd;
        private static DelegateBridge __Hotfix_OnStartLuaManagerStart;
        private static DelegateBridge __Hotfix_OnStartLuaManagerEnd;
        private static DelegateBridge __Hotfix_OnStartHotfixManagerStart;
        private static DelegateBridge __Hotfix_OnStartHotfixManagerEnd;
        private static DelegateBridge __Hotfix_OnLoadConfigDataStart;
        private static DelegateBridge __Hotfix_OnLoadingConfigData;
        private static DelegateBridge __Hotfix_OnLoadConfigDataEnd;
        private static DelegateBridge __Hotfix_OnStartAudioManagerStart;
        private static DelegateBridge __Hotfix_OnStartAudioManagerEnd;
        private static DelegateBridge __Hotfix_InitUITaskRegister;
        private static DelegateBridge __Hotfix_LaunchLogin;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_UpdateView4StreamingAssetsFilesProcessing;
        private static DelegateBridge __Hotfix_UpdateView4BundleDataLoading;
        private static DelegateBridge __Hotfix_UpdateView4AssetBundlePreUpdateing;
        private static DelegateBridge __Hotfix_UpdateView4AssetBundleManifestLoading;

        [MethodImpl(0x8000)]
        public GameEntryUITaskBase(string name) : base(name)
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, name);
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected virtual IEnumerator EntryPipeLine()
        {
            DelegateBridge bridge = __Hotfix_EntryPipeLine;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <EntryPipeLine>c__Iterator0 { $this = this };
        }

        [MethodImpl(0x8000)]
        protected virtual bool InitUITaskRegister()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsSDKLogin()
        {
            DelegateBridge bridge = __Hotfix_IsSDKLogin;
            return ((bridge == null) ? GameManager.Instance.GameClientSetting.LoginSetting.LoginUseSDK : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        protected virtual void LaunchLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void NotifyUserDownloadAndWait(long totalDownloadByte)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAssetBundleManifestLoadingEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAssetBundleManifestLoadingStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAssetBundlePreUpdateingEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAssetBundlePreUpdateingRefuse()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAssetBundlePreUpdateingStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBasicVersionUnmatch()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBundleDataLoadingEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBundleDataLoadingStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadConfigDataEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadConfigDataStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadDynamicAssemblysEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadDynamicAssemblysStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadingConfigData(float processVal)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnSDKInitEnd(bool isSuccess, bool isDownloadServerLoginAnnouncement = false)
        {
            DelegateBridge bridge = __Hotfix_OnSDKInitEnd;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp77(this, isSuccess, isDownloadServerLoginAnnouncement);
            }
            else
            {
                this.EnableUIInput(true);
                if (isSuccess)
                {
                    base.m_corutineHelper.StartCorutine(new Func<IEnumerator>(this.EntryPipeLine));
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStartAudioManagerEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStartAudioManagerStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStartHotfixManagerEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStartHotfixManagerStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStartLuaManagerEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStartLuaManagerStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStreamingAssetsFilesProcessingEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStreamingAssetsFilesProcessingStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
            DelegateBridge bridge = __Hotfix_OnTick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                base.OnTick();
                switch (ResourceManager.Instance.State)
                {
                    case ResourceManager.RMState.StreamingAssetsFilesProcessing:
                        this.UpdateView4StreamingAssetsFilesProcessing();
                        break;

                    case ResourceManager.RMState.BundleDataLoading:
                        this.UpdateView4BundleDataLoading();
                        break;

                    case ResourceManager.RMState.AssetBundlePreUpdateing:
                        this.UpdateView4AssetBundlePreUpdateing();
                        break;

                    case ResourceManager.RMState.AssetBundleManifestLoading:
                        this.UpdateView4AssetBundleManifestLoading();
                        break;

                    default:
                        break;
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnUpdateAssetBundleDownloadUrl(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnUpdateBundleDownloadUrlEnd(bool ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnUpdateBundleDownloadUrlStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
            DelegateBridge bridge = __Hotfix_PostUpdateView;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (ResourceManager.Instance.State == ResourceManager.RMState.Inited)
            {
                if (this.IsSDKLogin())
                {
                    this.StartInitSDK();
                }
                else
                {
                    base.m_corutineHelper.StartCorutine(new Func<IEnumerator>(this.EntryPipeLine));
                }
            }
        }

        [MethodImpl(0x8000)]
        public void RefusePreDownload()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartInitSDK()
        {
            DelegateBridge bridge = __Hotfix_StartInitSDK;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.EnableUIInput(false);
            }
        }

        [MethodImpl(0x8000)]
        public void StartPreDownload()
        {
        }

        [MethodImpl(0x8000)]
        protected bool UpdateAssetBundleDownloadUrl()
        {
            DelegateBridge bridge = __Hotfix_UpdateAssetBundleDownloadUrl;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            if (GameManager.Instance.GameClientSetting.ResourcesSetting.DisableAssetBundleDownload)
            {
                return true;
            }
            string vaildAssetBundleDownLoadUrl = ServerSettingManager.Instance.VaildAssetBundleDownLoadUrl;
            if (string.IsNullOrEmpty(vaildAssetBundleDownLoadUrl))
            {
                Debug.Log("GameEntryUITaskBase:UpdateAssetBundleDownloadUrl string.IsNullOrEmpty(vaildBundleDownloadUrl)");
                return this.OnUpdateAssetBundleDownloadUrl(false);
            }
            ResourceManager.Instance.SetDownloadBundleUrl(vaildAssetBundleDownLoadUrl);
            return this.OnUpdateAssetBundleDownloadUrl(true);
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateView4AssetBundleManifestLoading()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateView4AssetBundlePreUpdateing()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateView4BundleDataLoading()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateView4StreamingAssetsFilesProcessing()
        {
        }

        [CompilerGenerated]
        private sealed class <EntryPipeLine>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal long <totalDownloadByte>__0;
            internal GameEntryUITaskBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <EntryPipeLine>c__AnonStorey1 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
                bool? nullable;
                bool? nullable2;
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <EntryPipeLine>c__AnonStorey1();
                        this.$locvar0.<>f__ref$0 = this;
                        nullable = null;
                        this.$locvar0.ret = nullable;
                        this.$current = ServerSettingManager.Instance.InitServerSettingManager();
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        goto TR_0001;

                    case 1:
                        if (this.$this.UpdateAssetBundleDownloadUrl())
                        {
                            ResourceManager.Instance.CheckBundleCache();
                            this.$this.OnStreamingAssetsFilesProcessingStart();
                            ResourceManager.Instance.StartStreamingAssetsFilesProcessing(new Action<bool>(this.$locvar0.<>m__0));
                        }
                        else
                        {
                            Debug.LogError("GameEntryUITaskBase::UpdateAssetBundleDownloadUrl failed! Skip EntryPipeLine");
                            goto TR_0000;
                        }
                        break;

                    case 2:
                        break;

                    case 3:
                        goto TR_004F;

                    case 4:
                        goto TR_0044;

                    case 5:
                        goto TR_003E;

                    case 6:
                        goto TR_0037;

                    case 7:
                        goto TR_0031;

                    case 8:
                        goto TR_002B;

                    case 9:
                        goto TR_0025;

                    case 10:
                        goto TR_001F;

                    case 11:
                        goto TR_0017;

                    default:
                        goto TR_0000;
                }
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable = null;
                    this.$locvar0.ret = nullable;
                    this.$this.OnBundleDataLoadingStart();
                    ResourceManager.Instance.StartBundleDataLoading(new Action<bool>(this.$locvar0.<>m__1));
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartStreamingAssetsFilesProcessing");
                    goto TR_0000;
                }
                goto TR_004F;
            TR_0000:
                return false;
            TR_0001:
                return true;
            TR_0017:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 11;
                    }
                    goto TR_0001;
                }
                else if (!this.$locvar0.ret.Value)
                {
                    Debug.LogError("EntryPipeLine fail in InitGraphSetting");
                }
                else
                {
                    nullable2 = null;
                    this.$locvar0.ret = nullable2;
                    this.$this.LaunchLogin();
                    this.$PC = -1;
                }
                goto TR_0000;
            TR_001F:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 10;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable2 = null;
                    this.$locvar0.ret = nullable2;
                    this.$locvar0.ret = new bool?(this.$this.InitUITaskRegister());
                    if (this.$locvar0.ret.Value)
                    {
                        nullable2 = null;
                        this.$locvar0.ret = nullable2;
                        GameManager.Instance.InitGraphSetting(new Action<bool>(this.$locvar0.<>m__8));
                    }
                    else
                    {
                        Debug.LogError("EntryPipeLine fail in InitUITaskRegister");
                        goto TR_0000;
                    }
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartAudioManager");
                    goto TR_0000;
                }
                goto TR_0017;
            TR_0025:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 9;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable2 = null;
                    this.$locvar0.ret = nullable2;
                    this.$this.OnStartAudioManagerStart();
                    GameManager.Instance.StartAudioManager(new Action<bool>(this.$locvar0.<>m__7));
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartLoadConfigData");
                    goto TR_0000;
                }
                goto TR_001F;
            TR_002B:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 8;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable2 = null;
                    this.$locvar0.ret = nullable2;
                    this.$this.OnLoadConfigDataStart();
                    GameManager.Instance.StartLoadConfigData(new Action<bool>(this.$locvar0.<>m__6), out this.$this.m_configDataInitLoadCount);
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartHotfixManager");
                    goto TR_0000;
                }
                goto TR_0025;
            TR_0031:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 7;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable2 = null;
                    this.$locvar0.ret = nullable2;
                    this.$this.OnStartHotfixManagerStart();
                    GameManager.Instance.StartHotfixManager(new Action<bool>(this.$locvar0.<>m__5));
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartLoadDynamicAssemblys");
                    goto TR_0000;
                }
                goto TR_002B;
            TR_0037:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 6;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable2 = null;
                    this.$locvar0.ret = nullable2;
                    this.$this.OnLoadDynamicAssemblysStart();
                    GameManager.Instance.StartLoadDynamicAssemblys(new Action<bool>(this.$locvar0.<>m__4));
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartAssetBundleManifestLoading");
                    goto TR_0000;
                }
                goto TR_0031;
            TR_0038:
                this.$this.OnAssetBundleManifestLoadingStart();
                ResourceManager.Instance.StartAssetBundleManifestLoading(new Action<bool>(this.$locvar0.<>m__3));
                goto TR_0037;
            TR_003E:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 5;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable2 = null;
                    this.$locvar0.ret = nullable2;
                    goto TR_0038;
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartAssetBundlePreUpdateing");
                }
                goto TR_0000;
            TR_0044:
                if (this.$this.m_startBundlePreDownloadByPlayer == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 4;
                    }
                    goto TR_0001;
                }
                else if (this.$this.m_startBundlePreDownloadByPlayer.Value)
                {
                    this.$this.OnAssetBundlePreUpdateingStart();
                    ResourceManager.Instance.StartAssetBundlePreUpdateing(new Action<bool>(this.$locvar0.<>m__2));
                }
                else
                {
                    Debug.Log("BundlePreUpdateing cancel by player");
                    this.$this.OnAssetBundlePreUpdateingRefuse();
                    goto TR_0000;
                }
                goto TR_003E;
            TR_0046:
                this.<totalDownloadByte>__0 = ResourceManager.Instance.GetTotalPreUpdateingDownloadBytes();
                if (this.<totalDownloadByte>__0 == 0L)
                {
                    goto TR_0038;
                }
                else
                {
                    Debug.Log($"NotifyUserDownloadAndWait bytes={this.<totalDownloadByte>__0}");
                    this.$this.NotifyUserDownloadAndWait(this.<totalDownloadByte>__0);
                }
                goto TR_0044;
            TR_004F:
                if (this.$locvar0.ret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 3;
                    }
                    goto TR_0001;
                }
                else if (this.$locvar0.ret.Value)
                {
                    nullable = null;
                    this.$locvar0.ret = nullable;
                    if (GameManager.Instance.GameClientSetting.ResourcesSetting.DisableAssetBundleDownload)
                    {
                        goto TR_0046;
                    }
                    else if (GameManager.Instance.GameClientSetting.LoginSetting.BasicVersion == ResourceManager.Instance.GetBundleDataBasicVersion())
                    {
                        goto TR_0046;
                    }
                    else
                    {
                        Debug.LogError($"EntryPipeLine fail BasicVersion Unmatch, basicVersion = {GameManager.Instance.GameClientSetting.LoginSetting.BasicVersion}, bundleVersion = {ResourceManager.Instance.GetBundleDataBasicVersion()}");
                        this.$this.OnBasicVersionUnmatch();
                    }
                }
                else
                {
                    Debug.LogError("EntryPipeLine fail after StartBundleDataLoading");
                }
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <EntryPipeLine>c__AnonStorey1
            {
                internal bool tret;
                internal bool? ret;
                internal GameEntryUITaskBase.<EntryPipeLine>c__Iterator0 <>f__ref$0;

                [MethodImpl(0x8000)]
                internal void <>m__0(bool lret)
                {
                    bool flag;
                    this.tret = flag = lret;
                    this.ret = new bool?(flag);
                    this.<>f__ref$0.$this.OnStreamingAssetsFilesProcessingEnd(lret);
                }

                [MethodImpl(0x8000)]
                internal void <>m__1(bool lret)
                {
                }

                [MethodImpl(0x8000)]
                internal void <>m__2(bool lret)
                {
                }

                [MethodImpl(0x8000)]
                internal void <>m__3(bool lret)
                {
                }

                [MethodImpl(0x8000)]
                internal void <>m__4(bool lret)
                {
                }

                [MethodImpl(0x8000)]
                internal void <>m__5(bool lret)
                {
                }

                [MethodImpl(0x8000)]
                internal void <>m__6(bool lret)
                {
                }

                [MethodImpl(0x8000)]
                internal void <>m__7(bool lret)
                {
                }

                [MethodImpl(0x8000)]
                internal void <>m__8(bool lret)
                {
                }
            }
        }
    }
}

