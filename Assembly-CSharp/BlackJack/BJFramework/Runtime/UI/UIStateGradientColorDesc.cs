﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class UIStateGradientColorDesc
    {
        public BlackJack.BJFramework.Runtime.UI.Gradient GradientComptent;
        public Color Color1;
        public Color Color2;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

