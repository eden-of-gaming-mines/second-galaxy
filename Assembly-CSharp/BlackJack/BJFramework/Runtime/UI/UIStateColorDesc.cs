﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class UIStateColorDesc
    {
        public Color TargetColor;
        public GameObject ChangeColorGo;
        [HideInInspector]
        public List<Image> ChangeColorImageList;
        [HideInInspector]
        public List<Text> ChangeColorTextList;
        [HideInInspector]
        public List<FrequentChangeText> ChangeColorFrenquentTextList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

