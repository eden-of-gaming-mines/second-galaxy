﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class UITaskPipeLineCtx
    {
        public bool m_isInitPipeLine = true;
        public bool m_isTaskResume;
        public bool m_isRuning;
        public bool m_layerLoadedInPipe;
        public bool m_blockGlobalUIInput = true;
        public Action m_redirectPipLineOnAllResReady;
        public string m_taskNotifyDesc;
        protected ulong m_updateMask;
        public Action<bool> m_onPipelineEnd;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetUpdateMask_1;
        private static DelegateBridge __Hotfix_SetUpdateMask_0;
        private static DelegateBridge __Hotfix_AddUpdateMask;
        private static DelegateBridge __Hotfix_ClearUpdateMask;
        private static DelegateBridge __Hotfix_IsNeedUpdate;
        private static DelegateBridge __Hotfix_IsUpdateMaskClear;

        [MethodImpl(0x8000)]
        public UITaskPipeLineCtx()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void AddUpdateMask(int index)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Clear()
        {
            DelegateBridge bridge = __Hotfix_Clear;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.m_isInitPipeLine = false;
                this.m_layerLoadedInPipe = false;
                this.m_isTaskResume = false;
                this.m_redirectPipLineOnAllResReady = null;
                this.m_updateMask = 0UL;
                this.m_isRuning = false;
                this.m_taskNotifyDesc = string.Empty;
                this.m_onPipelineEnd = null;
            }
        }

        [MethodImpl(0x8000)]
        public void ClearUpdateMask(int index)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedUpdate(int index)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsUpdateMaskClear()
        {
        }

        [MethodImpl(0x8000)]
        public void SetUpdateMask(ulong mask)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUpdateMask(params int[] indexs)
        {
        }
    }
}

