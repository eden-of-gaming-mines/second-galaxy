﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class UIIntentReturnable : UIIntentCustom
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private UIIntent <PrevTaskIntent>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_set_PrevTaskIntent;
        private static DelegateBridge __Hotfix_get_PrevTaskIntent;

        [MethodImpl(0x8000)]
        public UIIntentReturnable(UIIntent prevTaskIntent, string targetTaskName, string targetMode = null)
        {
        }

        public UIIntent PrevTaskIntent
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

