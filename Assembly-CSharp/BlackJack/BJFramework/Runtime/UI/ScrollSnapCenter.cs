﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class ScrollSnapCenter : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
    {
        public float m_itemSize;
        public ScrollSnapDirection m_scrollSnapDir;
        public ScrollSnapPageType m_scrollSnapPageType;
        public float m_dragPercentage;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCenterItemChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnEndDrag;
        private bool m_isSnaping;
        private Vector2 m_snapPosition;
        private int m_itemCount;
        private ScrollRect m_scrollRect;
        private int m_currentCenterIndex;
        private Vector2 m_beginDragPosition;
        private Vector2 m_endFragPosition;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_LateUpdate;
        private static DelegateBridge __Hotfix_SetItemCount;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_ComputeItemPosition;
        private static DelegateBridge __Hotfix_GetCenterItemIndexUnclamped;
        private static DelegateBridge __Hotfix_GetCenterItemIndex;
        private static DelegateBridge __Hotfix_Snap_1;
        private static DelegateBridge __Hotfix_GetItemIndexByDragDistance;
        private static DelegateBridge __Hotfix_ComputeSnapCenterPosition;
        private static DelegateBridge __Hotfix_Snap_0;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_OnEndDrag;
        private static DelegateBridge __Hotfix_add_EventOnCenterItemChanged;
        private static DelegateBridge __Hotfix_remove_EventOnCenterItemChanged;
        private static DelegateBridge __Hotfix_add_EventOnEndDrag;
        private static DelegateBridge __Hotfix_remove_EventOnEndDrag;

        public event Action<int> EventOnCenterItemChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEndDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 ComputeItemPosition(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 ComputeSnapCenterPosition(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCenterItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCenterItemIndexUnclamped()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemCount()
        {
        }

        [MethodImpl(0x8000)]
        private int GetItemIndexByDragDistance()
        {
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        private void Snap(Vector2 p)
        {
        }

        [MethodImpl(0x8000)]
        public void Snap(int idx, bool smooth = true)
        {
        }
    }
}

