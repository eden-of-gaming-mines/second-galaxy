﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Serialization;
    using UnityEngine.UI;

    [AddComponentMenu("UIExtend/ToggleEx", 0x10)]
    public class ToggleEx : Toggle
    {
        [FormerlySerializedAs("ButtonComponent"), Header("[按钮组件]"), SerializeField]
        protected List<GameObject> m_toggleComponent;
        [SerializeField, FormerlySerializedAs("NormalStateColorList")]
        protected List<Color> m_normalStateColorList;
        [SerializeField, FormerlySerializedAs("PressedStateColorList")]
        protected List<Color> m_pressedStateColorList;
        [FormerlySerializedAs("DisableStateColorList"), SerializeField]
        protected List<Color> m_disableStateColorList;
        protected Dictionary<GameObject, Color> m_baseColorList;
        [SerializeField, Header("[Unity按下音效]"), FormerlySerializedAs("PressedAudioClip")]
        protected AudioClip m_pressedAudioClip;
        [Header("[CRI按下音效]"), FormerlySerializedAs("PressedAudioCRI"), SerializeField]
        protected string m_pressedAudioCRI;
        [Header("[On状态需要隐藏对象列表]"), SerializeField, FormerlySerializedAs("OnStateHideList")]
        protected List<GameObject> m_onStateHideObjectList;
        [SerializeField, FormerlySerializedAs("OnStateColorList"), Header("[On状态颜色改变列表]")]
        protected List<ColorDesc> m_onStateColorList;
        [FormerlySerializedAs("OnStateTweenList"), SerializeField, Header("[On状态Tween列表]")]
        protected List<TweenMain> m_onStateTweenList;
        [Header("[Off状态需要隐藏对象列表]"), FormerlySerializedAs("OffStateHideList"), SerializeField]
        protected List<GameObject> m_offStateHideObjectList;
        [FormerlySerializedAs("OffStateColorList"), Header("[Off状态颜色改变列表]"), SerializeField]
        protected List<ColorDesc> m_offStateColorList;
        [SerializeField, Header("[Off状态Tween列表]"), FormerlySerializedAs("OffStateTweenList")]
        protected List<TweenMain> m_offStateTweenList;
        protected bool m_preIsOn;
        private const string UINormalToggleClickAudioResFullPath = "UI1_NormalClick";
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_SetNormalState;
        private static DelegateBridge __Hotfix_SetPressedState;
        private static DelegateBridge __Hotfix_SetDisableState;
        private static DelegateBridge __Hotfix_SetIsOn;
        private static DelegateBridge __Hotfix_SetBaseColorList;
        private static DelegateBridge __Hotfix_DoStateTransition;
        private static DelegateBridge __Hotfix_InstantClearState;
        private static DelegateBridge __Hotfix_SetToggleComponentColor;
        private static DelegateBridge __Hotfix_ResetToggleCompontentToBaseColor;
        private static DelegateBridge __Hotfix_GetBaseStateColor;
        private static DelegateBridge __Hotfix_ShowOrHideObjectWhenIsOnChanged;
        private static DelegateBridge __Hotfix_SetObjectColorWhenIsOnChanged;
        private static DelegateBridge __Hotfix_PlayTweenListWhenIsOnChanged;
        private static DelegateBridge __Hotfix_SetGameObjectColor;
        private static DelegateBridge __Hotfix_OnValueChanged;
        private static DelegateBridge __Hotfix_get_PressedAudioClip;
        private static DelegateBridge __Hotfix_get_PressedAudioCRI;

        [MethodImpl(0x8000)]
        protected override void Awake()
        {
        }

        [MethodImpl(0x8000)]
        protected override void DoStateTransition(Selectable.SelectionState state, bool instant)
        {
        }

        [MethodImpl(0x8000)]
        protected Color GetBaseStateColor(GameObject go, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InstantClearState()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnValueChanged(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlayTweenListWhenIsOnChanged(List<TweenMain> tweenList)
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetToggleCompontentToBaseColor()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetBaseColorList(Dictionary<GameObject, Color> baseColorList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDisableState()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetGameObjectColor(GameObject go, Color color)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsOn(bool ison)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNormalState()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetObjectColorWhenIsOnChanged(List<ColorDesc> stateColorList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPressedState()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetToggleComponentColor(List<Color> stateColorList)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowOrHideObjectWhenIsOnChanged(bool isOn)
        {
        }

        public AudioClip PressedAudioClip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string PressedAudioCRI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [Serializable]
        public class ColorDesc
        {
            public GameObject go;
            public Color color;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

