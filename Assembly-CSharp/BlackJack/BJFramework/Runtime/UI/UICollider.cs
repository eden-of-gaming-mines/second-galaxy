﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    [AddComponentMenu("UIExtend/UICollider", 0x10)]
    public class UICollider : Graphic
    {
        private static DelegateBridge __Hotfix_Raycast;
        private static DelegateBridge __Hotfix_OnPopulateMesh;

        [MethodImpl(0x8000)]
        protected override void OnPopulateMesh(VertexHelper vh)
        {
            DelegateBridge bridge = __Hotfix_OnPopulateMesh;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, vh);
            }
            else
            {
                vh.Clear();
            }
        }

        [MethodImpl(0x8000)]
        public override bool Raycast(Vector2 sp, Camera eventCamera)
        {
            DelegateBridge bridge = __Hotfix_Raycast;
            return ((bridge == null) || bridge.__Gen_Delegate_Imp252(this, sp, eventCamera));
        }
    }
}

