﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    [AddComponentMenu("UI/BlackJack/TextLocalizationHelper")]
    public class UITextLocalizationHelper : MonoBehaviour
    {
        private Text m_uiText;
        public string m_textLocalizationKey;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_LoadLocatizedString;
        private static DelegateBridge __Hotfix_GetTextOriginalContent;
        private static DelegateBridge __Hotfix_ReportNotTextError;

        [MethodImpl(0x8000)]
        private void Awake()
        {
            DelegateBridge bridge = __Hotfix_Awake;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (Application.isPlaying)
            {
                this.LoadLocatizedString();
            }
        }

        [MethodImpl(0x8000)]
        public string GetTextOriginalContent()
        {
        }

        [MethodImpl(0x8000)]
        public void LoadLocatizedString()
        {
            DelegateBridge bridge = __Hotfix_LoadLocatizedString;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if ((GameManager.Instance != null) && (GameManager.Instance.StringTableManager != null))
            {
                string stringInDefaultStringTable = GameManager.Instance.StringTableManager.GetStringInDefaultStringTable(this.m_textLocalizationKey);
                if (this.m_uiText == null)
                {
                    this.m_uiText = base.GetComponent<Text>();
                }
                if (this.m_uiText != null)
                {
                    this.m_uiText.text = stringInDefaultStringTable;
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void ReportNotTextError()
        {
        }
    }
}

