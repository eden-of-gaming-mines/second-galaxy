﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public static class UIProcessFactory
    {
        private static DelegateBridge __Hotfix_CreateCommonUIStateEffectProcess;
        private static DelegateBridge __Hotfix_CreateExecutorProcess;
        private static DelegateBridge __Hotfix_CreateTaskNofityProcess;

        [MethodImpl(0x8000)]
        public static CommonUIStateEffectProcess CreateCommonUIStateEffectProcess(UIProcess.ProcessExecMode execMode, params UIPlayingEffectInfo[] effectList)
        {
            DelegateBridge bridge = __Hotfix_CreateCommonUIStateEffectProcess;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp310(execMode, effectList);
            }
            if ((effectList == null) || (effectList.Length == 0))
            {
                return new CommonUIStateEffectProcess(null, execMode);
            }
            CommonUIStateEffectProcess process = new CommonUIStateEffectProcess(effectList[0], execMode);
            for (int i = 1; i < effectList.Length; i++)
            {
                process.AddChild(new CommonUIStateEffectProcess(effectList[i], UIProcess.ProcessExecMode.Parallel));
            }
            return process;
        }

        [MethodImpl(0x8000)]
        public static CustomUIProcess CreateExecutorProcess(UIProcess.ProcessExecMode execMode, params CustomUIProcess.UIProcessExecutor[] executorList)
        {
        }

        [MethodImpl(0x8000)]
        public static TaskNotifyUIProcess CreateTaskNofityProcess(UIProcess.ProcessExecMode execMode, string taskName, string notifyType, string notifyDesc)
        {
        }
    }
}

