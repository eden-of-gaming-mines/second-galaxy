﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;
    using UnityEngine.Serialization;
    using UnityEngine.UI;

    [AddComponentMenu("UIExtend/ButtonEx", 0x10)]
    public class ButtonEx : Button
    {
        [FormerlySerializedAs("onDoubleClick"), SerializeField]
        private ButtonDoubleClickedEvent m_OnDoubleClick = new ButtonDoubleClickedEvent();
        [SerializeField]
        private ButtonLongPressStartEvent m_OnLongPressStart = new ButtonLongPressStartEvent();
        [SerializeField]
        private ButtonLongPressingEvent m_OnLongPressing = new ButtonLongPressingEvent();
        [SerializeField]
        private ButtonLongPressEndEvent m_OnLongPressEnd = new ButtonLongPressEndEvent();
        private bool m_isPointerInside;
        private bool m_isPointerDown;
        private int m_continueClickCount;
        private float m_continueClickDelayLeftTime;
        public float m_doubleClickDelayTime = 0.3f;
        public float m_longPressThreshhold = 0.5f;
        public float m_longPressingTriggerTimeGap = -1f;
        private float m_timeFromPressStarted;
        private bool m_isLongPressTriggered;
        private float m_timeFromLastPressingTrigger;
        [SerializeField, FormerlySerializedAs("ButtonComponent"), Header("[按钮组件]")]
        protected List<GameObject> m_buttonComponent;
        [FormerlySerializedAs("NormalStateColorList"), SerializeField]
        protected List<Color> m_normalStateColorList;
        [SerializeField, FormerlySerializedAs("PressedStateColorList")]
        protected List<Color> m_pressedStateColorList;
        [SerializeField, FormerlySerializedAs("DisableStateColorList")]
        protected List<Color> m_disableStateColorList;
        protected Dictionary<GameObject, Color> m_baseColorList;
        [Header("[Unity按下音效]"), SerializeField, FormerlySerializedAs("PressedAudioClip")]
        protected AudioClip m_pressedAudioClip;
        [Header("[CRI按下音效]"), SerializeField, FormerlySerializedAs("PressedAudioCRI")]
        protected string m_pressedAudioCRI;
        [SerializeField, Header("[是否禁止音效]")]
        protected bool m_prohibitAudio;
        [SerializeField, FormerlySerializedAs("PointerDownTweenList"), Header("按下Tween列表")]
        protected List<TweenMain> m_pointerDownTweenList;
        private const string UINormalButtonClickAudioResFullPath = "UI1_NormalClick";
        private static DelegateBridge __Hotfix_get_onDoubleClick;
        private static DelegateBridge __Hotfix_set_onDoubleClick;
        private static DelegateBridge __Hotfix_get_onLongPressStart;
        private static DelegateBridge __Hotfix_set_onLongPressStart;
        private static DelegateBridge __Hotfix_get_onLongPressing;
        private static DelegateBridge __Hotfix_set_onLongPressing;
        private static DelegateBridge __Hotfix_get_onLongPressEnd;
        private static DelegateBridge __Hotfix_set_onLongPressEnd;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_SetNormalState;
        private static DelegateBridge __Hotfix_SetPressedState;
        private static DelegateBridge __Hotfix_SetDisableState;
        private static DelegateBridge __Hotfix_OnPointerDown;
        private static DelegateBridge __Hotfix_OnPointerUp;
        private static DelegateBridge __Hotfix_OnPointerEnter;
        private static DelegateBridge __Hotfix_OnPointerExit;
        private static DelegateBridge __Hotfix_SetBaseColorList;
        private static DelegateBridge __Hotfix_DoStateTransition;
        private static DelegateBridge __Hotfix_InstantClearState;
        private static DelegateBridge __Hotfix_SetButtonComponentColor;
        private static DelegateBridge __Hotfix_ResetButtonCompontentToBaseColor;
        private static DelegateBridge __Hotfix_GetBaseStateColor;
        private static DelegateBridge __Hotfix_PlayPointerDownTween;
        private static DelegateBridge __Hotfix_SetGameObjectColor;
        private static DelegateBridge __Hotfix_OnButtonClicked;
        private static DelegateBridge __Hotfix_ResetDoubleClickState;
        private static DelegateBridge __Hotfix_get_PressedAudioClip;
        private static DelegateBridge __Hotfix_get_PressedAudioCRI;
        private static DelegateBridge __Hotfix_get_ProhibitAudio;

        [MethodImpl(0x8000)]
        protected ButtonEx()
        {
            base.onClick.AddListener(new UnityAction(this.OnButtonClicked));
        }

        [MethodImpl(0x8000)]
        protected override void DoStateTransition(Selectable.SelectionState state, bool instant)
        {
            DelegateBridge bridge = __Hotfix_DoStateTransition;
            if (bridge != null)
            {
                bridge.InvokeSessionStart();
                bridge.InParam<ButtonEx>(this);
                bridge.InParam<Selectable.SelectionState>(state);
                bridge.InParam<bool>(instant);
                bridge.Invoke(0);
                bridge.InvokeSessionEnd();
            }
            else
            {
                base.DoStateTransition(state, instant);
                if (base.gameObject.activeInHierarchy)
                {
                    switch (state)
                    {
                        case Selectable.SelectionState.Normal:
                        case Selectable.SelectionState.Highlighted:
                            if (this.m_baseColorList == null)
                            {
                                this.SetButtonComponentColor(this.m_normalStateColorList);
                            }
                            else
                            {
                                this.ResetButtonCompontentToBaseColor();
                            }
                            break;

                        case Selectable.SelectionState.Pressed:
                            this.SetButtonComponentColor(this.m_pressedStateColorList);
                            break;

                        case Selectable.SelectionState.Disabled:
                            this.SetButtonComponentColor(this.m_disableStateColorList);
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        protected Color GetBaseStateColor(GameObject go, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InstantClearState()
        {
            DelegateBridge bridge = __Hotfix_InstantClearState;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                base.InstantClearState();
                this.SetButtonComponentColor(this.m_disableStateColorList);
            }
        }

        [MethodImpl(0x8000)]
        protected void OnButtonClicked()
        {
        }

        [MethodImpl(0x8000)]
        public override void OnPointerDown(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnPointerEnter(PointerEventData eventData)
        {
            DelegateBridge bridge = __Hotfix_OnPointerEnter;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, eventData);
            }
            else
            {
                this.m_isPointerInside = true;
                base.OnPointerEnter(eventData);
            }
        }

        [MethodImpl(0x8000)]
        public override void OnPointerExit(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnPointerUp(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlayPointerDownTween(bool isforward)
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetButtonCompontentToBaseColor()
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetDoubleClickState()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetBaseColorList(Dictionary<GameObject, Color> baseColorList)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetButtonComponentColor(List<Color> stateColorList)
        {
            DelegateBridge bridge = __Hotfix_SetButtonComponentColor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, stateColorList);
            }
            else if (stateColorList != null)
            {
                for (int i = 0; (i < this.m_buttonComponent.Count) && (i < stateColorList.Count); i++)
                {
                    GameObject go = this.m_buttonComponent[i];
                    if (go != null)
                    {
                        this.SetGameObjectColor(go, stateColorList[i]);
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public void SetDisableState()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetGameObjectColor(GameObject go, Color color)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNormalState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPressedState()
        {
        }

        [MethodImpl(0x8000)]
        protected void Update()
        {
            DelegateBridge bridge = __Hotfix_Update;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                if (this.m_continueClickDelayLeftTime > 0f)
                {
                    this.m_continueClickDelayLeftTime -= Time.deltaTime;
                    if (this.m_continueClickDelayLeftTime < 0f)
                    {
                        this.m_continueClickDelayLeftTime = 0f;
                        this.m_continueClickCount--;
                    }
                }
                if (this.m_isPointerDown)
                {
                    if (!this.m_isLongPressTriggered)
                    {
                        if ((Time.time - this.m_timeFromPressStarted) > this.m_longPressThreshhold)
                        {
                            this.m_isLongPressTriggered = true;
                            this.m_timeFromLastPressingTrigger = 0f;
                            this.m_OnLongPressStart.Invoke();
                        }
                    }
                    else if (this.m_longPressingTriggerTimeGap <= 0f)
                    {
                        this.m_OnLongPressing.Invoke();
                    }
                    else
                    {
                        this.m_timeFromLastPressingTrigger += Time.deltaTime;
                        if (this.m_timeFromLastPressingTrigger >= this.m_longPressingTriggerTimeGap)
                        {
                            this.m_OnLongPressing.Invoke();
                            this.m_timeFromLastPressingTrigger -= this.m_longPressingTriggerTimeGap;
                        }
                    }
                }
            }
        }

        public ButtonDoubleClickedEvent onDoubleClick
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_onDoubleClick;
                return ((bridge == null) ? this.m_OnDoubleClick : bridge.__Gen_Delegate_Imp208(this));
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public ButtonLongPressStartEvent onLongPressStart
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_onLongPressStart;
                return ((bridge == null) ? this.m_OnLongPressStart : bridge.__Gen_Delegate_Imp209(this));
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public ButtonLongPressingEvent onLongPressing
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_onLongPressing;
                return ((bridge == null) ? this.m_OnLongPressing : bridge.__Gen_Delegate_Imp210(this));
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public ButtonLongPressEndEvent onLongPressEnd
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_onLongPressEnd;
                return ((bridge == null) ? this.m_OnLongPressEnd : bridge.__Gen_Delegate_Imp211(this));
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public AudioClip PressedAudioClip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string PressedAudioCRI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool ProhibitAudio
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [Serializable]
        public class ButtonDoubleClickedEvent : UnityEvent
        {
            private static DelegateBridge _c__Hotfix_ctor;

            [MethodImpl(0x8000)]
            public ButtonDoubleClickedEvent()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        [Serializable]
        public class ButtonLongPressEndEvent : UnityEvent
        {
            private static DelegateBridge _c__Hotfix_ctor;

            public ButtonLongPressEndEvent()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        [Serializable]
        public class ButtonLongPressingEvent : UnityEvent
        {
            private static DelegateBridge _c__Hotfix_ctor;

            public ButtonLongPressingEvent()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        [Serializable]
        public class ButtonLongPressStartEvent : UnityEvent
        {
            private static DelegateBridge _c__Hotfix_ctor;

            public ButtonLongPressStartEvent()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

