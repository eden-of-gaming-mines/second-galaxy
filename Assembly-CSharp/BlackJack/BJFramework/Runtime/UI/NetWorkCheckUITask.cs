﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using UnityEngine;

    public abstract class NetWorkCheckUITask : UITaskBase
    {
        private float m_netLoseConnectFlagTime;
        private const float NetLoseConnectWaitInterval = 0.5f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ExecuteNetWorkOnNetReachable;
        private static DelegateBridge __Hotfix_ExecuteNetWorkUntilSucess;
        private static DelegateBridge __Hotfix_RetryExecuteNetWorkUntilSuccess;
        private static DelegateBridge __Hotfix_RetryExecuteNetWork;
        private static DelegateBridge __Hotfix_CheckNetwork;
        private static DelegateBridge __Hotfix_WaitUntil;

        [MethodImpl(0x8000)]
        public NetWorkCheckUITask(string name) : base(name)
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, name);
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator CheckNetwork(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void ExecuteNetWorkOnNetReachable(Action netAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void ExecuteNetWorkUntilSucess(Action<Action<bool>> netAction, Action onEnd)
        {
        }

        protected abstract string GetNetworkErrorMsg();
        protected abstract void OnStartRetryExecuteNetWork();
        [MethodImpl(0x8000)]
        protected void RetryExecuteNetWork(Action netAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void RetryExecuteNetWorkUntilSuccess(Action<Action<bool>> netAction, Action onEnd)
        {
        }

        protected abstract void ShowNetworkErrorMsgBox(string msg, Action onConfirm);
        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator WaitUntil(Func<bool> predicate)
        {
        }

        [CompilerGenerated]
        private sealed class <CheckNetwork>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal bool <isNeedNotify>__1;
            internal Action<bool> onEnd;
            internal NetWorkCheckUITask.Hostname2Ip[] $locvar0;
            internal int $locvar1;
            internal NetWorkCheckUITask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <CheckNetwork>c__AnonStorey5 $locvar2;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$this.m_netLoseConnectFlagTime = Time.unscaledTime;
                        if (SceneManager.Instance != null)
                        {
                            goto TR_000A;
                        }
                        else
                        {
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                        }
                        break;

                    case 1:
                    case 3:
                        goto TR_000A;

                    case 2:
                        this.$locvar0 = this.$locvar2.h2Is;
                        this.$locvar1 = 0;
                        while (true)
                        {
                            if (this.$locvar1 < this.$locvar0.Length)
                            {
                                this.$locvar0[this.$locvar1].Destroy();
                                this.$locvar1++;
                                continue;
                            }
                            if (Time.unscaledTime < this.$locvar2.t)
                            {
                                Debug.Log("CheckNetwork Connect Success");
                                this.onEnd(true);
                            }
                            else
                            {
                                if (!this.<isNeedNotify>__1)
                                {
                                    this.$current = null;
                                    if (!this.$disposing)
                                    {
                                        this.$PC = 3;
                                    }
                                    break;
                                }
                                Debug.LogError("CheckNetwork Connect Error: Out Time");
                                this.onEnd(false);
                            }
                            goto TR_0000;
                        }
                        break;

                    default:
                        goto TR_0000;
                }
                goto TR_0001;
            TR_0000:
                return false;
            TR_0001:
                return true;
            TR_0007:
                NetWorkCheckUITask.Hostname2Ip[] ipArray1 = new NetWorkCheckUITask.Hostname2Ip[] { new NetWorkCheckUITask.Hostname2Ip("www.baidu.com"), new NetWorkCheckUITask.Hostname2Ip("www.qq.com"), new NetWorkCheckUITask.Hostname2Ip("www.taobao.com"), new NetWorkCheckUITask.Hostname2Ip("www.google.com"), new NetWorkCheckUITask.Hostname2Ip("www.facebook.com") };
                this.$locvar2.h2Is = ipArray1;
                this.$locvar2.t = Time.unscaledTime + 5f;
                this.$current = this.$this.WaitUntil(new Func<bool>(this.$locvar2.<>m__0));
                if (!this.$disposing)
                {
                    this.$PC = 2;
                }
                goto TR_0001;
            TR_000A:
                this.$locvar2 = new <CheckNetwork>c__AnonStorey5();
                this.$locvar2.<>f__ref$0 = this;
                this.<isNeedNotify>__1 = (Time.unscaledTime - this.$this.m_netLoseConnectFlagTime) > 0.5f;
                this.$this.m_netLoseConnectFlagTime = !this.<isNeedNotify>__1 ? this.$this.m_netLoseConnectFlagTime : Time.unscaledTime;
                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    goto TR_0007;
                }
                else if (!this.<isNeedNotify>__1)
                {
                    goto TR_0007;
                }
                else
                {
                    Debug.LogError("CheckNetwork Connect Error: network Not Reachable");
                    this.onEnd(false);
                }
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <CheckNetwork>c__AnonStorey5
            {
                internal NetWorkCheckUITask.Hostname2Ip[] h2Is;
                internal float t;
                internal NetWorkCheckUITask.<CheckNetwork>c__Iterator0 <>f__ref$0;

                internal bool <>m__0()
                {
                    foreach (NetWorkCheckUITask.Hostname2Ip ip in this.h2Is)
                    {
                        if (ip.isDone && !string.IsNullOrEmpty(ip.ip))
                        {
                            return true;
                        }
                    }
                    return (Time.unscaledTime > this.t);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ExecuteNetWorkUntilSucess>c__AnonStorey2
        {
            internal Action<Action<bool>> netAction;
            internal Action onEnd;
            internal NetWorkCheckUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    this.$this.RetryExecuteNetWorkUntilSuccess(this.netAction, this.onEnd);
                }
                else if (this.netAction == null)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd();
                    }
                }
                else
                {
                    this.netAction(delegate (bool lres) {
                        if (!lres)
                        {
                            this.$this.RetryExecuteNetWorkUntilSuccess(this.netAction, this.onEnd);
                        }
                        else if (this.onEnd != null)
                        {
                            this.onEnd();
                        }
                    });
                }
            }

            internal void <>m__1(bool lres)
            {
                if (!lres)
                {
                    this.$this.RetryExecuteNetWorkUntilSuccess(this.netAction, this.onEnd);
                }
                else if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RetryExecuteNetWork>c__AnonStorey4
        {
            internal Action netAction;
            internal NetWorkCheckUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(false);
                this.$this.PostDelayTicksExecuteAction(this.netAction, 1UL);
            }
        }

        [CompilerGenerated]
        private sealed class <RetryExecuteNetWorkUntilSuccess>c__AnonStorey3
        {
            internal Action<Action<bool>> netAction;
            internal Action onEnd;
            internal NetWorkCheckUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(false);
                this.$this.PostDelayTicksExecuteAction(() => this.$this.ExecuteNetWorkUntilSucess(this.netAction, this.onEnd), 1UL);
            }

            internal void <>m__1()
            {
                this.$this.ExecuteNetWorkUntilSucess(this.netAction, this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <WaitUntil>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Func<bool> predicate;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    case 1:
                        if (this.predicate())
                        {
                            this.$PC = -1;
                            break;
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        private class Hostname2Ip
        {
            private Thread m_thread;
            private bool m_isThreadExit;
            private string m_ip;
            private readonly string m_hostname;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Destroy;
            private static DelegateBridge __Hotfix_get_isDone;
            private static DelegateBridge __Hotfix_get_ip;
            private static DelegateBridge __Hotfix_ThreadProc;
            private static DelegateBridge __Hotfix_Hostname2IP;

            [MethodImpl(0x8000)]
            public Hostname2Ip(string hostname)
            {
            }

            [MethodImpl(0x8000)]
            public void Destroy()
            {
            }

            [MethodImpl(0x8000)]
            private string Hostname2IP(string hostname)
            {
            }

            [MethodImpl(0x8000)]
            private void ThreadProc()
            {
            }

            public bool isDone
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }

            public string ip
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }
        }
    }
}

