﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CommonUIStateEffectProcess : UIProcess
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private BlackJack.BJFramework.Runtime.UI.UIPlayingEffectInfo <UIPlayingEffectInfo>k__BackingField;
        [CompilerGenerated]
        private static Action<UIProcess> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnCancel;
        private static DelegateBridge __Hotfix_OnChildCancel;
        private static DelegateBridge __Hotfix_OnUIPlayingEffectFinished;
        private static DelegateBridge __Hotfix_StartPlayingEffect;
        private static DelegateBridge __Hotfix_get_UIPlayingEffectInfo;
        private static DelegateBridge __Hotfix_set_UIPlayingEffectInfo;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess(BlackJack.BJFramework.Runtime.UI.UIPlayingEffectInfo effectInfo, UIProcess.ProcessExecMode execMode) : base(execMode)
        {
            this.UIPlayingEffectInfo = effectInfo;
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp205(this, effectInfo, execMode);
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnCancel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnChildCancel(UIProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStart()
        {
            DelegateBridge bridge = __Hotfix_OnStart;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                if (this.UIPlayingEffectInfo != null)
                {
                    this.StartPlayingEffect();
                }
                else
                {
                    this.OnUIPlayingEffectFinished(true);
                }
                base.OnStart();
            }
        }

        [MethodImpl(0x8000)]
        private void OnUIPlayingEffectFinished(bool isCompleted)
        {
            DelegateBridge bridge = __Hotfix_OnUIPlayingEffectFinished;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, isCompleted);
            }
            else
            {
                this.OnStop(isCompleted ? UIProcess.StopOption.Complete : UIProcess.StopOption.Cancel);
            }
        }

        [MethodImpl(0x8000)]
        public void StartPlayingEffect()
        {
            DelegateBridge bridge = __Hotfix_StartPlayingEffect;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.UIPlayingEffectInfo.UIStateCtrl.SetToUIStateExtra(this.UIPlayingEffectInfo.UIStateName, this.UIPlayingEffectInfo.IsImmediateComplete, this.UIPlayingEffectInfo.AllowToRefreshSameState, new Action<bool>(this.OnUIPlayingEffectFinished));
            }
        }

        public BlackJack.BJFramework.Runtime.UI.UIPlayingEffectInfo UIPlayingEffectInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_UIPlayingEffectInfo;
                return ((bridge == null) ? this.<UIPlayingEffectInfo>k__BackingField : bridge.__Gen_Delegate_Imp204(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
                DelegateBridge bridge = __Hotfix_set_UIPlayingEffectInfo;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    this.<UIPlayingEffectInfo>k__BackingField = value;
                }
            }
        }
    }
}

