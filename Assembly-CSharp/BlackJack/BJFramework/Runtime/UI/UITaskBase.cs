﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class UITaskBase : Task
    {
        protected List<string> m_modeDefine;
        protected UIIntent m_currIntent;
        protected string m_currMode;
        protected bool m_isUIInputEnable;
        protected bool m_enableUIInputLog;
        protected int m_loadingStaticResCorutineCount;
        protected int m_loadingDynamicResCorutineCount;
        protected List<PlayingUpdateViewEffectItem> m_playingUpdateViewEffectList;
        protected SceneLayerBase[] m_layerArray;
        protected UIControllerBase[] m_uiCtrlArray;
        protected UITaskPipeLineCtx m_currPipeLineCtx;
        protected Dictionary<string, Object> m_dynamicResCacheDict;
        protected TinyCorutineHelper m_corutineHelper;
        protected bool m_blockPipeLine;
        protected bool m_enableUpdateViewAsync;
        private ulong m_lastStartUpdatePipeLineTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsNeedPauseTimeOut>k__BackingField;
        private readonly LinkedList<DelayTimeExecItem> m_delayTimeExecList;
        protected List<PiplineQueueItem> m_piplineQueue;
        protected List<string> m_tagList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_IsAnyPipeLineRuning;
        private static DelegateBridge __Hotfix_OnStart_1;
        private static DelegateBridge __Hotfix_OnStart_0;
        private static DelegateBridge __Hotfix_GetPipeLineCtx;
        private static DelegateBridge __Hotfix_CreatePipeLineCtx;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume_1;
        private static DelegateBridge __Hotfix_OnResume_0;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_StartUpdatePipeLine;
        private static DelegateBridge __Hotfix_CheckStaticResInit;
        private static DelegateBridge __Hotfix_NeedSkipUpdatePipeLine;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_StartLoadStaticRes;
        private static DelegateBridge __Hotfix_CheckLayerDescArray;
        private static DelegateBridge __Hotfix_OnLoadStaticResCompleted;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_CalculateDynamicResReallyNeedLoad;
        private static DelegateBridge __Hotfix_OnLoadDynamicResCompleted;
        private static DelegateBridge __Hotfix_RedirectPipLineOnAllResReady;
        private static DelegateBridge __Hotfix_OnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_ReturnFromRedirectPipLineOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_StartUpdateView;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewAsync;
        private static DelegateBridge __Hotfix_EnableUpdateViewAsync;
        private static DelegateBridge __Hotfix_RegUpdateViewPlayingEffect;
        private static DelegateBridge __Hotfix_UnregUpdateViewPlayingEffect;
        private static DelegateBridge __Hotfix_PlayUIProcess;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_HideAllView;
        private static DelegateBridge __Hotfix_ClearAllContextAndRes;
        private static DelegateBridge __Hotfix_SaveContextInIntentOnPause;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_ClearContextOnIntentChange;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_EnableUIInput_0;
        private static DelegateBridge __Hotfix_EnableUIInput_1;
        private static DelegateBridge __Hotfix_GetLayerDescByName;
        private static DelegateBridge __Hotfix_GetLayerByName;
        private static DelegateBridge __Hotfix_RegisterModesDefine;
        private static DelegateBridge __Hotfix_SetCurrentMode;
        private static DelegateBridge __Hotfix_SetIsNeedPauseTimeOut;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_TickForDelayTimeExecuteActionList;
        private static DelegateBridge __Hotfix_PostDelayTimeExecuteAction;
        private static DelegateBridge __Hotfix_PostDelayTicksExecuteAction;
        private static DelegateBridge __Hotfix_SetTag;
        private static DelegateBridge __Hotfix_HasTag;
        private static DelegateBridge __Hotfix_RaiseNotify;
        private static DelegateBridge __Hotfix_get_CurrentIntent;
        private static DelegateBridge __Hotfix_get_IsUIInputEnable;
        private static DelegateBridge __Hotfix_get_IsGlobalUIInputEnable;
        private static DelegateBridge __Hotfix_get_MainLayer;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_IsNeedPauseTimeOut;
        private static DelegateBridge __Hotfix_set_IsNeedPauseTimeOut;

        [MethodImpl(0x8000)]
        public UITaskBase(string name) : base(name)
        {
            this.m_isUIInputEnable = true;
            this.m_enableUIInputLog = true;
            this.m_playingUpdateViewEffectList = new List<PlayingUpdateViewEffectItem>();
            this.m_dynamicResCacheDict = new Dictionary<string, Object>();
            this.m_corutineHelper = new TinyCorutineHelper();
            this.m_delayTimeExecList = new LinkedList<DelayTimeExecItem>();
            this.m_piplineQueue = new List<PiplineQueueItem>();
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, name);
            }
        }

        [MethodImpl(0x8000)]
        protected HashSet<string> CalculateDynamicResReallyNeedLoad(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void CheckLayerDescArray(List<LayerDesc> layerDescArray)
        {
            DelegateBridge bridge = __Hotfix_CheckLayerDescArray;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, layerDescArray);
            }
            else if ((layerDescArray == null) || (layerDescArray.Count <= 0))
            {
                Debug.LogError("StartLoadStaticRes fail LayerDescArray size error");
                throw new Exception("StartLoadStaticRes fail LayerDescArray size error");
            }
        }

        [MethodImpl(0x8000)]
        protected void CheckStaticResInit()
        {
            DelegateBridge bridge = __Hotfix_CheckStaticResInit;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                bool flag = false;
                SceneLayerBase[] layerArray = this.m_layerArray;
                int index = 0;
                while (true)
                {
                    if (index < layerArray.Length)
                    {
                        SceneLayerBase base2 = layerArray[index];
                        if (((base2 == null) || base2.IsInited) || !base2.IsResourceReady)
                        {
                            index++;
                            continue;
                        }
                        flag = true;
                    }
                    if (flag)
                    {
                        this.InitLayerStateOnLoadAllResCompleted();
                        this.InitAllUIControllers();
                    }
                    return;
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearAllContextAndRes()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearContextOnIntentChange(UIIntent newIntent)
        {
            DelegateBridge bridge = __Hotfix_ClearContextOnIntentChange;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, newIntent);
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearContextOnUpdateViewEnd()
        {
            DelegateBridge bridge = __Hotfix_ClearContextOnUpdateViewEnd;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.m_currPipeLineCtx.Clear();
            }
        }

        [MethodImpl(0x8000)]
        protected virtual List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual List<LayerDesc> CollectAllStaticResDescForLoad()
        {
            DelegateBridge bridge = __Hotfix_CollectAllStaticResDescForLoad;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp315(this);
            }
            if (this.MainLayer != null)
            {
                return null;
            }
            List<LayerDesc> list = new List<LayerDesc>();
            foreach (LayerDesc desc in this.LayerDescArray)
            {
                if (!desc.m_isLazyLoad)
                {
                    list.Add(desc);
                }
            }
            return list;
        }

        [MethodImpl(0x8000)]
        protected virtual UITaskPipeLineCtx CreatePipeLineCtx()
        {
            DelegateBridge bridge = __Hotfix_CreatePipeLineCtx;
            return ((bridge == null) ? new UITaskPipeLineCtx() : bridge.__Gen_Delegate_Imp313(this));
        }

        [MethodImpl(0x8000)]
        public virtual void EnableUIInput(bool isEnable)
        {
            DelegateBridge bridge = __Hotfix_EnableUIInput_0;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, isEnable);
            }
            else
            {
                this.m_isUIInputEnable = isEnable;
            }
        }

        [MethodImpl(0x8000)]
        public virtual void EnableUIInput(bool isEnable, bool isGlobalEnable)
        {
            DelegateBridge bridge = __Hotfix_EnableUIInput_1;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp77(this, isEnable, isGlobalEnable);
            }
            else
            {
                this.m_isUIInputEnable = isEnable;
                UIManager.Instance.GlobalUIInputEnable(base.Name, isGlobalEnable);
            }
        }

        [MethodImpl(0x8000)]
        protected void EnableUpdateViewAsync()
        {
        }

        [MethodImpl(0x8000)]
        protected SceneLayerBase GetLayerByName(string name)
        {
            DelegateBridge bridge = __Hotfix_GetLayerByName;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp159(this, name);
            }
            foreach (SceneLayerBase base2 in this.m_layerArray)
            {
                if ((base2 != null) && (base2.LayerName == name))
                {
                    return base2;
                }
            }
            return null;
        }

        [MethodImpl(0x8000)]
        protected LayerDesc GetLayerDescByName(string name)
        {
            DelegateBridge bridge = __Hotfix_GetLayerDescByName;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp318(this, name);
            }
            foreach (LayerDesc desc in this.LayerDescArray)
            {
                if (desc.m_layerName == name)
                {
                    return desc;
                }
            }
            return null;
        }

        [MethodImpl(0x8000)]
        protected UITaskPipeLineCtx GetPipeLineCtx()
        {
            DelegateBridge bridge = __Hotfix_GetPipeLineCtx;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp313(this);
            }
            if (this.m_currPipeLineCtx == null)
            {
                this.m_currPipeLineCtx = this.CreatePipeLineCtx();
            }
            return this.m_currPipeLineCtx;
        }

        [MethodImpl(0x8000)]
        public bool HasTag(string tag)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void HideAllView()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitAllUIControllers()
        {
            DelegateBridge bridge = __Hotfix_InitAllUIControllers;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                List<UIControllerBase> list;
                int num;
                if (this.UICtrlDescArray == null)
                {
                    return;
                }
                else if (this.UICtrlDescArray.Length != 0)
                {
                    if (this.m_uiCtrlArray == null)
                    {
                        this.m_uiCtrlArray = new UIControllerBase[this.UICtrlDescArray.Length];
                    }
                    list = new List<UIControllerBase>();
                    num = 0;
                }
                else
                {
                    return;
                }
                while (true)
                {
                    while (true)
                    {
                        if (num >= this.UICtrlDescArray.Length)
                        {
                            foreach (UIControllerBase base4 in list)
                            {
                                if (base4 != null)
                                {
                                    base4.BindFields();
                                }
                            }
                            return;
                        }
                        if (this.m_uiCtrlArray[num] == null)
                        {
                            UIControllerDesc desc = this.UICtrlDescArray[num];
                            LayerDesc layerDescByName = this.GetLayerDescByName(desc.m_attachLayerName);
                            SceneLayerBase layerByName = this.GetLayerByName(desc.m_attachLayerName);
                            if (layerByName == null)
                            {
                                if ((layerDescByName != null) && layerDescByName.m_isLazyLoad)
                                {
                                    break;
                                }
                                Debug.LogError($"InitAllUIControllers fail for ctrl={desc.m_ctrlName} can not find layer={desc.m_attachLayerName} in task={this.ToString()}");
                            }
                            if (layerByName != null)
                            {
                                PrefabControllerBase base3 = PrefabControllerBase.AddControllerToGameObject(layerByName.LayerPrefabRoot, desc.m_attachPath, desc.m_ctrlTypeDNName, desc.m_ctrlName, desc.m_luaModuleName, false);
                                if (base3 == null)
                                {
                                    Debug.LogError($"InitAllUIControllers AddControllerToGameObject fail for ctrl={desc.m_ctrlName} layer={desc.m_attachLayerName} in task={this.ToString()}");
                                }
                                this.m_uiCtrlArray[num] = base3 as UIControllerBase;
                                list.Add(base3 as UIControllerBase);
                                layerByName.IsInited = true;
                            }
                        }
                        break;
                    }
                    num++;
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void InitLayerStateOnLoadAllResCompleted()
        {
            DelegateBridge bridge = __Hotfix_InitLayerStateOnLoadAllResCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.MainLayer.State != SceneLayerBase.LayerState.InStack)
            {
                SceneManager.Instance.PushLayer(this.MainLayer);
            }
        }

        [MethodImpl(0x8000)]
        public virtual void InitlizeBeforeManagerStartIt()
        {
            DelegateBridge bridge = __Hotfix_InitlizeBeforeManagerStartIt;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.LayerDescArray != null)
            {
                for (int i = 0; i < this.LayerDescArray.Length; i++)
                {
                    this.LayerDescArray[i].m_index = i;
                }
                this.m_layerArray = new SceneLayerBase[this.LayerDescArray.Length];
            }
        }

        [MethodImpl(0x8000)]
        public bool IsAnyPipeLineRuning()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsLoadAllResCompleted()
        {
            DelegateBridge bridge = __Hotfix_IsLoadAllResCompleted;
            return ((bridge == null) ? ((this.m_loadingStaticResCorutineCount == 0) && (this.m_loadingDynamicResCorutineCount == 0)) : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedLoadDynamicRes()
        {
            DelegateBridge bridge = __Hotfix_IsNeedLoadDynamicRes;
            return ((bridge != null) && bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedLoadStaticRes()
        {
            DelegateBridge bridge = __Hotfix_IsNeedLoadStaticRes;
            return ((bridge == null) ? (this.MainLayer == null) : bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedUpdateDataCache()
        {
            DelegateBridge bridge = __Hotfix_IsNeedUpdateDataCache;
            return ((bridge != null) && bridge.__Gen_Delegate_Imp9(this));
        }

        [MethodImpl(0x8000)]
        protected virtual bool NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadAllResCompleted()
        {
            DelegateBridge bridge = __Hotfix_OnLoadAllResCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                Debug.Log($"OnLoadAllResCompleted task={this.ToString()}");
                if (this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady != null)
                {
                    this.m_blockPipeLine = true;
                    this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady();
                }
                else
                {
                    if (this.m_currPipeLineCtx.m_layerLoadedInPipe)
                    {
                        this.InitLayerStateOnLoadAllResCompleted();
                        this.InitAllUIControllers();
                    }
                    this.PostOnLoadAllResCompleted();
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadDynamicResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLoadStaticResCompleted()
        {
            DelegateBridge bridge = __Hotfix_OnLoadStaticResCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.IsLoadAllResCompleted())
            {
                if (base.State == Task.TaskState.Paused)
                {
                    Debug.Log("UITaskBase::StartUpdatePipeLine::OnLoadStaticResCompleted TaskState == Paused, Name: " + base.Name);
                    this.ClearContextOnUpdateViewEnd();
                    this.EnableUIInput(true, true);
                    if (this.m_currPipeLineCtx.m_onPipelineEnd != null)
                    {
                        this.m_currPipeLineCtx.m_onPipelineEnd(false);
                    }
                }
                else if (base.State == Task.TaskState.Stopped)
                {
                    Debug.Log("UITaskBase::StartUpdatePipeLine::OnLoadStaticResCompleted TaskState == Stopped, Name: " + base.Name);
                    this.ClearContextOnUpdateViewEnd();
                    this.EnableUIInput(true, true);
                    Action<bool> onPipelineEnd = this.m_currPipeLineCtx.m_onPipelineEnd;
                    this.ClearAllContextAndRes();
                    if (onPipelineEnd != null)
                    {
                        onPipelineEnd(false);
                    }
                }
                else
                {
                    this.OnLoadAllResCompleted();
                }
            }
            else if (base.State == Task.TaskState.Paused)
            {
                Debug.Log("UITaskBase::StartUpdatePipeLine::OnLoadStaticResCompleted TaskState == Paused, Name: " + base.Name);
                this.ClearContextOnUpdateViewEnd();
                this.EnableUIInput(true, true);
                if ((this.m_currPipeLineCtx != null) && (this.m_currPipeLineCtx.m_onPipelineEnd != null))
                {
                    this.m_currPipeLineCtx.m_onPipelineEnd(false);
                }
            }
            else if (base.State == Task.TaskState.Stopped)
            {
                Debug.Log("UITaskBase::StartUpdatePipeLine::OnLoadStaticResCompleted TaskState == Stopped, Name: " + base.Name);
                this.ClearContextOnUpdateViewEnd();
                this.EnableUIInput(true, true);
                Action<bool> onPipelineEnd = null;
                if (this.m_currPipeLineCtx != null)
                {
                    onPipelineEnd = this.m_currPipeLineCtx.m_onPipelineEnd;
                }
                this.ClearAllContextAndRes();
                if (onPipelineEnd != null)
                {
                    onPipelineEnd(false);
                }
            }
        }

        [MethodImpl(0x8000)]
        public virtual bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override bool OnResume(object param = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
            DelegateBridge bridge = __Hotfix_OnStart_0;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp43(this, intent, onPipelineEnd);
            }
            this.m_currPipeLineCtx = this.GetPipeLineCtx();
            return this.StartUpdatePipeLine(intent, false, false, true, onPipelineEnd);
        }

        [MethodImpl(0x8000)]
        protected sealed override bool OnStart(object param, Action<bool> onPipelineEnd)
        {
            DelegateBridge bridge = __Hotfix_OnStart_1;
            return ((bridge == null) ? this.OnStart(param as UIIntent, onPipelineEnd) : bridge.__Gen_Delegate_Imp43(this, param, onPipelineEnd));
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
            DelegateBridge bridge = __Hotfix_OnTick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.TickForDelayTimeExecuteActionList();
                if (this.m_corutineHelper != null)
                {
                    this.m_corutineHelper.Tick();
                }
                if (this.m_playingUpdateViewEffectList.Count != 0)
                {
                    foreach (PlayingUpdateViewEffectItem item in this.m_playingUpdateViewEffectList)
                    {
                        if (item == null)
                        {
                            continue;
                        }
                        if (item.m_timeOutTime != null)
                        {
                            DateTime? timeOutTime = item.m_timeOutTime;
                            if ((timeOutTime != null) && (timeOutTime.GetValueOrDefault() <= Timer.m_currTime))
                            {
                                this.UnregUpdateViewPlayingEffect(item.m_name, false);
                                break;
                            }
                        }
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public void PlayUIProcess(UIProcess process, bool blockui = true, Action<UIProcess, bool> onEnd = null, bool alwaysInvokeOnEnd = false)
        {
            DelegateBridge bridge = __Hotfix_PlayUIProcess;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp317(this, process, blockui, onEnd, alwaysInvokeOnEnd);
            }
            else
            {
                <PlayUIProcess>c__AnonStorey2 storey = new <PlayUIProcess>c__AnonStorey2 {
                    onEnd = onEnd,
                    alwaysInvokeOnEnd = alwaysInvokeOnEnd,
                    process = process,
                    blockui = blockui,
                    $this = this
                };
                if (this.m_currPipeLineCtx.m_isRuning)
                {
                    this.RegUpdateViewPlayingEffect(storey.process.ToString(), 0, null);
                    storey.process.Start(new UIProcess.OnEnd(storey.<>m__0));
                }
                else
                {
                    if (storey.blockui)
                    {
                        this.EnableUIInput(false, false);
                    }
                    storey.process.Start(new UIProcess.OnEnd(storey.<>m__1));
                }
            }
        }

        [MethodImpl(0x8000)]
        public void PostDelayTicksExecuteAction(Action action, ulong delayTickCount)
        {
        }

        [MethodImpl(0x8000)]
        public void PostDelayTimeExecuteAction(Action action, float delaySeconds)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PostOnLoadAllResCompleted()
        {
            DelegateBridge bridge = __Hotfix_PostOnLoadAllResCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.StartUpdateView();
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PostUpdateViewBeforeClearContext()
        {
            DelegateBridge bridge = __Hotfix_PostUpdateViewBeforeClearContext;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public virtual void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void RaiseNotify(UIManager.UITaskNotifyType notifyType, string desc)
        {
            DelegateBridge bridge = __Hotfix_RaiseNotify;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp320(this, notifyType, desc);
            }
            else
            {
                UIManager.Instance.OnUITaskNotify(this, notifyType, desc);
            }
        }

        [MethodImpl(0x8000)]
        public void RedirectPipLineOnAllResReady(Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterModesDefine(string defaultMode, params string[] modes)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegUpdateViewPlayingEffect(string name = null, int timeout = 0, Action<string> onTimeOut = null)
        {
            DelegateBridge bridge = __Hotfix_RegUpdateViewPlayingEffect;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp316(this, name, timeout, onTimeOut);
            }
            else if (name == null)
            {
                this.m_playingUpdateViewEffectList.Add(null);
            }
            else
            {
                PlayingUpdateViewEffectItem item = new PlayingUpdateViewEffectItem {
                    m_name = name
                };
                if (timeout != 0)
                {
                    item.m_timeOutTime = new DateTime?(Timer.m_currTime.AddMilliseconds((double) timeout));
                    item.m_onTimeOut = onTimeOut;
                }
                this.m_playingUpdateViewEffectList.Add(item);
            }
        }

        [MethodImpl(0x8000)]
        public void ReturnFromRedirectPipLineOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SaveContextInIntentOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool SetCurrentMode(string mode)
        {
            DelegateBridge bridge = __Hotfix_SetCurrentMode;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp16(this, mode);
            }
            if ((string.IsNullOrEmpty(this.m_currMode) || (this.m_currMode != mode)) && (this.m_modeDefine != null))
            {
                if (string.IsNullOrEmpty(mode))
                {
                    this.m_currMode = this.m_modeDefine[0];
                }
                else
                {
                    if (!this.m_modeDefine.Contains(mode))
                    {
                        Debug.LogError($"UITaskBase.SetCurrentMode fail error mode {mode}");
                        return false;
                    }
                    this.m_currMode = mode;
                }
            }
            return true;
        }

        [MethodImpl(0x8000)]
        protected void SetIsNeedPauseTimeOut(bool isNeed)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTag(string tag)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartLoadStaticRes()
        {
            DelegateBridge bridge = __Hotfix_StartLoadStaticRes;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                List<LayerDesc> layerDescArray = this.CollectAllStaticResDescForLoad();
                this.CheckLayerDescArray(layerDescArray);
                for (int i = layerDescArray.Count - 1; i >= 0; i--)
                {
                    if (this.m_layerArray[layerDescArray[i].m_index] != null)
                    {
                        layerDescArray.RemoveAt(i);
                    }
                    else
                    {
                        this.m_loadingStaticResCorutineCount++;
                    }
                }
                if (layerDescArray.Count == 0)
                {
                    this.OnLoadStaticResCompleted();
                }
                else
                {
                    foreach (LayerDesc desc in layerDescArray)
                    {
                        <StartLoadStaticRes>c__AnonStorey0 storey = new <StartLoadStaticRes>c__AnonStorey0 {
                            $this = this,
                            layerName = desc.m_layerName
                        };
                        string layerResPath = desc.m_layerResPath;
                        bool isUILayer = desc.m_isUILayer;
                        storey.index = desc.m_index;
                        if (isUILayer)
                        {
                            this.m_currPipeLineCtx.m_layerLoadedInPipe = true;
                            SceneManager.Instance.CreateLayer(typeof(UISceneLayer), storey.layerName, layerResPath, new Action<SceneLayerBase>(storey.<>m__0), false);
                            continue;
                        }
                        if (layerResPath.EndsWith(".unity"))
                        {
                            SceneManager.Instance.CreateLayer(typeof(UnitySceneLayer), storey.layerName, layerResPath, new Action<SceneLayerBase>(storey.<>m__1), false);
                            continue;
                        }
                        this.m_currPipeLineCtx.m_layerLoadedInPipe = true;
                        SceneManager.Instance.CreateLayer(typeof(ThreeDSceneLayer), storey.layerName, layerResPath, new Action<SceneLayerBase>(storey.<>m__2), false);
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        protected bool StartUpdatePipeLine(UIIntent intent = null, bool onlyUpdateView = false, bool canBeSkip = false, bool enableQueue = true, Action<bool> onPipelineEnd = null)
        {
            DelegateBridge bridge = __Hotfix_StartUpdatePipeLine;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp314(this, intent, onlyUpdateView, canBeSkip, enableQueue, onPipelineEnd);
            }
            if (base.State != Task.TaskState.Running)
            {
                Debug.LogError($"UITaskBase.StartUpdatePipeLine Error: State {base.State} != TaskState.Running");
                return false;
            }
            if (this.m_blockPipeLine)
            {
                return false;
            }
            if (this.m_currPipeLineCtx.m_isRuning)
            {
                if (enableQueue)
                {
                    PiplineQueueItem item = new PiplineQueueItem {
                        m_canBeSkip = canBeSkip,
                        m_intent = intent,
                        m_onlyUpdateView = onlyUpdateView,
                        m_onPipelineEnd = onPipelineEnd
                    };
                    this.m_piplineQueue.Add(item);
                }
                return false;
            }
            if (canBeSkip && this.NeedSkipUpdatePipeLine(intent, onlyUpdateView))
            {
                Debug.Log("UITaskBase.StartUpdatePipeLine Skiped");
                return true;
            }
            if ((intent != null) && (!ReferenceEquals(this.m_currIntent, intent) || (this.m_currMode != intent.TargetMode)))
            {
                if (!this.SetCurrentMode(intent.TargetMode))
                {
                    Debug.LogError($"UITaskBase.StartUpdatePipeLine fail error mode {intent.TargetMode}");
                    return false;
                }
                this.ClearContextOnIntentChange(intent);
                this.m_currIntent = intent;
            }
            this.m_currPipeLineCtx.m_isRuning = true;
            this.m_currPipeLineCtx.m_onPipelineEnd = onPipelineEnd;
            this.m_lastStartUpdatePipeLineTime = Timer.m_currTick;
            if (this.m_currPipeLineCtx.m_blockGlobalUIInput)
            {
                this.EnableUIInput(false, false);
            }
            if (onlyUpdateView)
            {
                this.StartUpdateView();
                return true;
            }
            if (this.IsNeedUpdateDataCache())
            {
                this.UpdateDataCache();
            }
            bool flag = this.IsNeedLoadStaticRes();
            bool flag2 = this.IsNeedLoadDynamicRes();
            this.CheckStaticResInit();
            if (!flag && !flag2)
            {
                if (this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady == null)
                {
                    this.StartUpdateView();
                    return true;
                }
                this.m_blockPipeLine = true;
                this.m_currPipeLineCtx.m_redirectPipLineOnAllResReady();
                return true;
            }
            if (flag)
            {
                this.StartLoadStaticRes();
            }
            if (flag2)
            {
                this.StartLoadDynamicRes();
            }
            return true;
        }

        [MethodImpl(0x8000)]
        protected virtual void StartUpdateView()
        {
            DelegateBridge bridge = __Hotfix_StartUpdateView;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                if (this.m_enableUpdateViewAsync)
                {
                    this.m_playingUpdateViewEffectList.Add(null);
                    this.m_corutineHelper.StartCorutine(this.UpdateViewAsync(() => this.UnregUpdateViewPlayingEffect(null, false)));
                }
                else
                {
                    this.m_playingUpdateViewEffectList.Add(null);
                    this.UpdateView();
                    this.m_playingUpdateViewEffectList.Remove(null);
                }
                if (this.m_playingUpdateViewEffectList.Count == 0)
                {
                    this.PostUpdateViewBeforeClearContext();
                    this.RaiseNotify(UIManager.UITaskNotifyType.UpdateViewComplete, this.m_currPipeLineCtx.m_taskNotifyDesc);
                    Action<bool> onPipelineEnd = this.m_currPipeLineCtx.m_onPipelineEnd;
                    this.ClearContextOnUpdateViewEnd();
                    this.EnableUIInput(true, true);
                    if (onPipelineEnd != null)
                    {
                        onPipelineEnd(true);
                    }
                    this.PostUpdateView();
                }
            }
        }

        [MethodImpl(0x8000)]
        private void TickForDelayTimeExecuteActionList()
        {
            DelegateBridge bridge = __Hotfix_TickForDelayTimeExecuteActionList;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if ((this.m_delayTimeExecList != null) && (this.m_delayTimeExecList.Count != 0))
            {
                while (this.m_delayTimeExecList.First != null)
                {
                    DelayTimeExecItem item = this.m_delayTimeExecList.First.Value;
                    if (item.m_execTargetTime > Timer.m_currTime)
                    {
                        break;
                    }
                    item.m_action();
                    this.m_delayTimeExecList.RemoveFirst();
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void UnregUpdateViewPlayingEffect(string name = null, bool isTimeOut = false)
        {
            DelegateBridge bridge = __Hotfix_UnregUpdateViewPlayingEffect;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp12(this, name, isTimeOut);
            }
            else
            {
                <UnregUpdateViewPlayingEffect>c__AnonStorey1 storey = new <UnregUpdateViewPlayingEffect>c__AnonStorey1 {
                    name = name
                };
                Action<string> onTimeOut = null;
                if (storey.name == null)
                {
                    this.m_playingUpdateViewEffectList.Remove(null);
                }
                else
                {
                    PlayingUpdateViewEffectItem item = this.m_playingUpdateViewEffectList.Find(new Predicate<PlayingUpdateViewEffectItem>(storey.<>m__0));
                    if (item != null)
                    {
                        onTimeOut = item.m_onTimeOut;
                        this.m_playingUpdateViewEffectList.Remove(item);
                    }
                }
                if (isTimeOut && (onTimeOut != null))
                {
                    onTimeOut(storey.name);
                }
                if (this.m_playingUpdateViewEffectList.Count == 0)
                {
                    if (base.State == Task.TaskState.Paused)
                    {
                        Debug.Log("UITaskBase::UnregUpdateViewPlayingEffect TaskState == Paused, Name: " + base.Name);
                        this.ClearContextOnUpdateViewEnd();
                        this.EnableUIInput(true, true);
                        if (this.m_currPipeLineCtx.m_onPipelineEnd != null)
                        {
                            this.m_currPipeLineCtx.m_onPipelineEnd(false);
                        }
                    }
                    else if (base.State == Task.TaskState.Stopped)
                    {
                        Debug.Log("UITaskBase::UnregUpdateViewPlayingEffect TaskState == Stopped, Name: " + base.Name);
                        this.ClearContextOnUpdateViewEnd();
                        this.EnableUIInput(true, true);
                        Action<bool> onPipelineEnd = this.m_currPipeLineCtx.m_onPipelineEnd;
                        this.ClearAllContextAndRes();
                        if (onPipelineEnd != null)
                        {
                            onPipelineEnd(false);
                        }
                    }
                    else
                    {
                        this.PostUpdateViewBeforeClearContext();
                        this.RaiseNotify(UIManager.UITaskNotifyType.UpdateViewComplete, this.m_currPipeLineCtx.m_taskNotifyDesc);
                        Action<bool> onPipelineEnd = this.m_currPipeLineCtx.m_onPipelineEnd;
                        this.ClearContextOnUpdateViewEnd();
                        this.EnableUIInput(true, true);
                        if (onPipelineEnd != null)
                        {
                            onPipelineEnd(true);
                        }
                        this.PostUpdateView();
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual IEnumerator UpdateViewAsync(Action onEnd)
        {
        }

        public UIIntent CurrentIntent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected bool IsUIInputEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected static bool IsGlobalUIInputEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected virtual SceneLayerBase MainLayer
        {
            [MethodImpl(0x8000)]
            get
            {
                SceneLayerBase base1;
                DelegateBridge bridge = __Hotfix_get_MainLayer;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp172(this);
                }
                if ((this.m_layerArray == null) || (this.m_layerArray.Length == 0))
                {
                    base1 = null;
                }
                else
                {
                    base1 = this.m_layerArray[0];
                }
                return base1;
            }
        }

        protected virtual LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected virtual UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsNeedPauseTimeOut
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_IsNeedPauseTimeOut;
                return ((bridge == null) ? this.<IsNeedPauseTimeOut>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PlayUIProcess>c__AnonStorey2
        {
            internal Action<UIProcess, bool> onEnd;
            internal bool alwaysInvokeOnEnd;
            internal UIProcess process;
            internal bool blockui;
            internal UITaskBase $this;

            internal void <>m__0(UIProcess lprocess, bool isComplete)
            {
                if ((this.onEnd != null) && (this.alwaysInvokeOnEnd || (this.$this.State == Task.TaskState.Running)))
                {
                    this.onEnd(lprocess, isComplete);
                }
                this.$this.UnregUpdateViewPlayingEffect(this.process.ToString(), false);
            }

            internal void <>m__1(UIProcess lprocess, bool isComplete)
            {
                if (this.blockui)
                {
                    this.$this.EnableUIInput(true, true);
                }
                if ((this.onEnd != null) && (this.alwaysInvokeOnEnd || (this.$this.State == Task.TaskState.Running)))
                {
                    this.onEnd(lprocess, isComplete);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadStaticRes>c__AnonStorey0
        {
            internal string layerName;
            internal int index;
            internal UITaskBase $this;

            internal void <>m__0(SceneLayerBase layer)
            {
                if (layer == null)
                {
                    Debug.LogError($"Load Layer fail task={this.$this.ToString()} layer={this.layerName}");
                }
                else
                {
                    layer.IsResourceReady = true;
                    this.$this.m_layerArray[this.index] = layer;
                    this.$this.m_loadingStaticResCorutineCount--;
                    this.$this.OnLoadStaticResCompleted();
                }
            }

            internal void <>m__1(SceneLayerBase layer)
            {
                if (layer == null)
                {
                    Debug.LogError($"Load Layer fail task={this.$this.ToString()} layer={this.layerName}");
                }
                else
                {
                    layer.IsResourceReady = true;
                    this.$this.m_layerArray[this.index] = layer;
                    this.$this.m_loadingStaticResCorutineCount--;
                    this.$this.OnLoadStaticResCompleted();
                }
            }

            internal void <>m__2(SceneLayerBase layer)
            {
                if (layer == null)
                {
                    Debug.LogError($"Load Layer fail task={this.$this.ToString()} layer={this.layerName}");
                }
                else
                {
                    this.$this.m_layerArray[this.index] = layer;
                    this.$this.m_loadingStaticResCorutineCount--;
                    this.$this.OnLoadStaticResCompleted();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnregUpdateViewPlayingEffect>c__AnonStorey1
        {
            internal string name;

            internal bool <>m__0(UITaskBase.PlayingUpdateViewEffectItem litem) => 
                ((litem != null) && (litem.m_name == this.name));
        }

        public class DelayTimeExecItem
        {
            public DateTime m_execTargetTime;
            public Action m_action;
            private static DelegateBridge _c__Hotfix_ctor;

            public DelayTimeExecItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class LayerDesc
        {
            public string m_layerName;
            public string m_layerResPath;
            public bool m_isUILayer;
            public bool m_isLazyLoad;
            public int m_index;
            public bool m_isReserve;
            private static DelegateBridge _c__Hotfix_ctor;

            public LayerDesc()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class PiplineQueueItem
        {
            public UIIntent m_intent;
            public bool m_onlyUpdateView;
            public bool m_canBeSkip;
            public Action<bool> m_onPipelineEnd;
            private static DelegateBridge _c__Hotfix_ctor;

            public PiplineQueueItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class PlayingUpdateViewEffectItem
        {
            public string m_name;
            public DateTime? m_timeOutTime;
            public Action<string> m_onTimeOut;
            private static DelegateBridge _c__Hotfix_ctor;

            [MethodImpl(0x8000)]
            public PlayingUpdateViewEffectItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class UIControllerDesc
        {
            public string m_ctrlName;
            public string m_luaModuleName;
            public TypeDNName m_ctrlTypeDNName;
            public string m_attachLayerName;
            public string m_attachPath;
            private static DelegateBridge _c__Hotfix_ctor;

            public UIControllerDesc()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

