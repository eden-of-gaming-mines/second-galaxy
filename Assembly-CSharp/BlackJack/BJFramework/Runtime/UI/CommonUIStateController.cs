﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonUIStateController : MonoBehaviour
    {
        public List<UIStateDesc> UIStateDescList = new List<UIStateDesc>();
        public List<ColorSetDesc> UIStateColorSetList = new List<ColorSetDesc>();
        public List<ButtonEx> UIRelateButtonList = new List<ButtonEx>();
        [HideInInspector]
        public List<GameObject> DisableGoList = new List<GameObject>();
        [HideInInspector]
        public int LastStateIndex;
        private bool isGathed;
        private UIStateDesc m_currUIState;
        private Dictionary<UIStateDesc, Action<bool>> m_uiState2OnTweensFinishedActionDict = new Dictionary<UIStateDesc, Action<bool>>();
        private List<TweenMain> m_allTweens = new List<TweenMain>();
        private Dictionary<UIStateDesc, List<GameObject>> m_disableGoListByTweenAlpha = new Dictionary<UIStateDesc, List<GameObject>>();
        private Dictionary<UIStateDesc, float> m_uiState2OnFinishedCallbackCountDownDict = new Dictionary<UIStateDesc, float>();
        private bool m_isDuringState;
        private bool m_isStateCancel;
        private float m_currUIStateSiwtchTimer;
        public bool m_needOnFinishedCallbackNextUpdate;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_OnDestroy;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_SetToUIStateExtra;
        private static DelegateBridge __Hotfix_StopStateChanging;
        private static DelegateBridge __Hotfix_ChangeToColor;
        private static DelegateBridge __Hotfix_NotifyColorChanged;
        private static DelegateBridge __Hotfix_ShowNextState;
        private static DelegateBridge __Hotfix_ReGatherDynamicGo;
        private static DelegateBridge __Hotfix_GetUIStateDescByName;
        private static DelegateBridge __Hotfix_GetCurrentUIState;
        private static DelegateBridge __Hotfix_CheckCurrentState;
        private static DelegateBridge __Hotfix_IsCurrentState;
        private static DelegateBridge __Hotfix_GetMaxTweenDurationByUIState;
        private static DelegateBridge __Hotfix_ReGatherColorChangeImageAndText;
        private static DelegateBridge __Hotfix_OnAllUIStateTweensFinished;
        private static DelegateBridge __Hotfix_CollectDisableObjByTweenAlpha;
        private static DelegateBridge __Hotfix_DisableAllOtherStateTweens;
        private static DelegateBridge __Hotfix_SetCurrStateColorChanging;
        private static DelegateBridge __Hotfix_SetToUIState;
        private static DelegateBridge __Hotfix_SetActionForUIStateFinshed;

        [MethodImpl(0x8000)]
        private void ChangeToColor(List<UIStateColorDesc> colorDescList)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCurrentState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDisableObjByTweenAlpha(TweenMain tweenAlpha, UIStateDesc stateDesc)
        {
            DelegateBridge bridge = __Hotfix_CollectDisableObjByTweenAlpha;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp26(this, tweenAlpha, stateDesc);
            }
            else
            {
                List<GameObject> list;
                if (!this.m_disableGoListByTweenAlpha.TryGetValue(stateDesc, out list))
                {
                    list = new List<GameObject>();
                    this.m_disableGoListByTweenAlpha.Add(stateDesc, list);
                }
                if (!list.Contains(tweenAlpha.gameObject))
                {
                    list.Add(tweenAlpha.gameObject);
                }
            }
        }

        [MethodImpl(0x8000)]
        private void DisableAllOtherStateTweens()
        {
            DelegateBridge bridge = __Hotfix_DisableAllOtherStateTweens;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.m_currUIState != null)
            {
                foreach (TweenMain main in this.m_allTweens)
                {
                    if (main == null)
                    {
                        continue;
                    }
                    if (!this.m_currUIState.TweenAnimationList.Contains(main))
                    {
                        main.enabled = false;
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public UIStateDesc GetCurrentUIState()
        {
        }

        [MethodImpl(0x8000)]
        public float GetMaxTweenDurationByUIState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public UIStateDesc GetUIStateDescByName(string name)
        {
            DelegateBridge bridge = __Hotfix_GetUIStateDescByName;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp216(this, name);
            }
            using (List<UIStateDesc>.Enumerator enumerator = this.UIStateDescList.GetEnumerator())
            {
                while (true)
                {
                    if (!enumerator.MoveNext())
                    {
                        break;
                    }
                    UIStateDesc current = enumerator.Current;
                    if (current.StateName == name)
                    {
                        return current;
                    }
                }
            }
            return null;
        }

        [MethodImpl(0x8000)]
        public bool IsCurrentState(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        private void NotifyColorChanged(List<UIStateColorDesc> colorDescList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllUIStateTweensFinished()
        {
            DelegateBridge bridge = __Hotfix_OnAllUIStateTweensFinished;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                List<GameObject> list;
                Action<bool> action;
                this.SetCurrStateColorChanging();
                this.m_isDuringState = false;
                bool isStateCancel = this.m_isStateCancel;
                this.m_isStateCancel = false;
                UIStateDesc currUIState = this.m_currUIState;
                if (this.m_disableGoListByTweenAlpha.TryGetValue(currUIState, out list))
                {
                    foreach (GameObject obj2 in list)
                    {
                        if (obj2.activeSelf)
                        {
                            obj2.SetActive(false);
                        }
                    }
                }
                if (this.m_uiState2OnTweensFinishedActionDict.TryGetValue(currUIState, out action))
                {
                    if (action == null)
                    {
                        this.m_uiState2OnTweensFinishedActionDict.Remove(currUIState);
                    }
                    else
                    {
                        action(!isStateCancel);
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        private void OnDestroy()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
            DelegateBridge bridge = __Hotfix_OnDisable;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.StopStateChanging(true);
            }
        }

        [MethodImpl(0x8000)]
        private void ReGatherColorChangeImageAndText()
        {
            DelegateBridge bridge = __Hotfix_ReGatherColorChangeImageAndText;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                foreach (ColorSetDesc desc in this.UIStateColorSetList)
                {
                    foreach (UIStateColorDesc desc2 in desc.UIStateColorDescList)
                    {
                        desc2.ChangeColorImageList.Clear();
                        desc2.ChangeColorTextList.Clear();
                        desc2.ChangeColorFrenquentTextList.Clear();
                        if (desc2.ChangeColorGo != null)
                        {
                            Image[] components = desc2.ChangeColorGo.GetComponents<Image>();
                            int index = 0;
                            while (true)
                            {
                                if (index >= components.Length)
                                {
                                    Text[] textArray2 = desc2.ChangeColorGo.GetComponents<Text>();
                                    int num2 = 0;
                                    while (true)
                                    {
                                        if (num2 >= textArray2.Length)
                                        {
                                            foreach (FrequentChangeText text2 in desc2.ChangeColorGo.GetComponents<FrequentChangeText>())
                                            {
                                                if ((text2 != null) && !desc2.ChangeColorFrenquentTextList.Contains(text2))
                                                {
                                                    desc2.ChangeColorFrenquentTextList.Add(text2);
                                                }
                                            }
                                            break;
                                        }
                                        Text text = textArray2[num2];
                                        if ((text != null) && !desc2.ChangeColorTextList.Contains(text))
                                        {
                                            desc2.ChangeColorTextList.Add(text);
                                        }
                                        num2++;
                                    }
                                    break;
                                }
                                Image item = components[index];
                                if ((item != null) && !desc2.ChangeColorImageList.Contains(item))
                                {
                                    desc2.ChangeColorImageList.Add(item);
                                }
                                index++;
                            }
                        }
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public void ReGatherDynamicGo()
        {
            DelegateBridge bridge = __Hotfix_ReGatherDynamicGo;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.DisableGoList.Clear();
                this.m_allTweens.Clear();
                this.m_disableGoListByTweenAlpha.Clear();
                foreach (UIStateDesc desc in this.UIStateDescList)
                {
                    foreach (GameObject obj2 in desc.SetToShowGameObjectList)
                    {
                        if (obj2 == null)
                        {
                            continue;
                        }
                        if (!this.DisableGoList.Contains(obj2))
                        {
                            this.DisableGoList.Add(obj2);
                        }
                    }
                    foreach (TweenMain main in desc.TweenAnimationList)
                    {
                        if (!this.m_allTweens.Contains(main))
                        {
                            this.m_allTweens.Add(main);
                        }
                    }
                }
                this.ReGatherColorChangeImageAndText();
                foreach (UIStateDesc desc2 in this.UIStateDescList)
                {
                    if (desc2.TweenAnimationList.Count != 0)
                    {
                        float num = 0f;
                        foreach (TweenMain main2 in desc2.TweenAnimationList)
                        {
                            if (main2 != null)
                            {
                                TweenCGAlpha tweenAlpha = main2 as TweenCGAlpha;
                                if (tweenAlpha != null)
                                {
                                    if ((tweenAlpha.style == TweenMain.Style.Once) && (tweenAlpha.to == 0f))
                                    {
                                        this.CollectDisableObjByTweenAlpha(tweenAlpha, desc2);
                                    }
                                }
                                else
                                {
                                    TweenAlpha alpha2 = main2 as TweenAlpha;
                                    if (((alpha2 != null) && (alpha2.style == TweenMain.Style.Once)) && (alpha2.to == 0f))
                                    {
                                        this.CollectDisableObjByTweenAlpha(alpha2, desc2);
                                    }
                                }
                                if ((main2.style == TweenMain.Style.Once) && ((main2.duration + main2.delay) > num))
                                {
                                    num = main2.duration + main2.delay;
                                }
                            }
                        }
                        this.m_uiState2OnFinishedCallbackCountDownDict.Add(desc2, num);
                    }
                }
                this.isGathed = true;
            }
        }

        [MethodImpl(0x8000), Obsolete("重构完成后不能再使用这个方法")]
        public bool SetActionForUIStateFinshed(string stateName, Action action)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrStateColorChanging()
        {
            DelegateBridge bridge = __Hotfix_SetCurrStateColorChanging;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.m_currUIState != null)
            {
                foreach (UIStateGradientColorDesc desc in this.m_currUIState.GradientDescList)
                {
                    if (desc.GradientComptent != null)
                    {
                        desc.GradientComptent.enabled = false;
                        desc.GradientComptent.m_color1 = desc.Color1;
                        desc.GradientComptent.m_color2 = desc.Color2;
                        desc.GradientComptent.enabled = true;
                    }
                }
            }
        }

        [MethodImpl(0x8000), Obsolete("重构完成后不能再使用这个方法")]
        public void SetToUIState(string stateName, bool notPlayTweens = false, bool allowToRefreshSameState = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToUIStateExtra(string stateName, bool immediateComplete = false, bool allowToRefreshSameState = true, Action<bool> onStateFinished = null)
        {
            DelegateBridge bridge = __Hotfix_SetToUIStateExtra;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp215(this, stateName, immediateComplete, allowToRefreshSameState, onStateFinished);
            }
            else
            {
                if (this.m_isDuringState)
                {
                    this.StopStateChanging(false);
                }
                if (string.IsNullOrEmpty(stateName))
                {
                    Debug.LogError("CommonUIStateController::SetToUIState stateName IsNullOrEmpty!");
                }
                else
                {
                    UIStateDesc uIStateDescByName = this.GetUIStateDescByName(stateName);
                    if (uIStateDescByName == null)
                    {
                        Debug.LogError($"CommonUIStateController::SetToUIState UIState Find Fail:  GoName:{base.gameObject.name}  stateName:{stateName}");
                    }
                    else
                    {
                        if (!this.isGathed)
                        {
                            this.ReGatherDynamicGo();
                        }
                        if (onStateFinished != null)
                        {
                            this.m_uiState2OnTweensFinishedActionDict[uIStateDescByName] = onStateFinished;
                        }
                        if (ReferenceEquals(uIStateDescByName, this.m_currUIState) && !allowToRefreshSameState)
                        {
                            this.OnAllUIStateTweensFinished();
                        }
                        else
                        {
                            if (!base.gameObject.activeSelf)
                            {
                                base.gameObject.SetActive(true);
                            }
                            if (((base.transform.parent != null) && (base.transform.parent.gameObject.activeSelf && (base.transform.parent.gameObject.activeInHierarchy && !base.gameObject.activeInHierarchy))) && base.gameObject.activeSelf)
                            {
                                base.gameObject.SetActive(true);
                            }
                            this.m_currUIState = uIStateDescByName;
                            this.m_currUIStateSiwtchTimer = 0f;
                            this.m_isDuringState = true;
                            foreach (GameObject obj2 in this.DisableGoList)
                            {
                                if (obj2.activeSelf)
                                {
                                    obj2.SetActive(false);
                                }
                            }
                            foreach (UIStateDesc desc2 in this.UIStateDescList)
                            {
                                if (desc2.StateName == stateName)
                                {
                                    foreach (GameObject obj3 in desc2.SetToShowGameObjectList)
                                    {
                                        if (obj3 == null)
                                        {
                                            continue;
                                        }
                                        if (!obj3.activeSelf)
                                        {
                                            obj3.SetActive(true);
                                        }
                                    }
                                    if (((desc2.StateColorSetIndex >= 0) && (this.UIStateColorSetList.Count > 0)) && (desc2.StateColorSetIndex < this.UIStateColorSetList.Count))
                                    {
                                        ColorSetDesc desc3 = this.UIStateColorSetList[desc2.StateColorSetIndex];
                                        this.ChangeToColor(desc3.UIStateColorDescList);
                                        this.NotifyColorChanged(desc3.UIStateColorDescList);
                                    }
                                    if (desc2.TweenAnimationList.Count == 0)
                                    {
                                        this.DisableAllOtherStateTweens();
                                        this.OnAllUIStateTweensFinished();
                                    }
                                    else if (!immediateComplete)
                                    {
                                        this.DisableAllOtherStateTweens();
                                        foreach (TweenMain main2 in desc2.TweenAnimationList)
                                        {
                                            if (main2 != null)
                                            {
                                                if (!main2.gameObject.activeSelf)
                                                {
                                                    main2.gameObject.SetActive(true);
                                                }
                                                main2.ResetToBeginning();
                                                main2.PlayForward();
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (TweenMain main in desc2.TweenAnimationList)
                                        {
                                            if (main == null)
                                            {
                                                continue;
                                            }
                                            if (main.style == TweenMain.Style.Once)
                                            {
                                                if (!main.gameObject.activeSelf)
                                                {
                                                    main.gameObject.SetActive(true);
                                                }
                                                main.SetToFinish();
                                            }
                                        }
                                        this.DisableAllOtherStateTweens();
                                        this.OnAllUIStateTweensFinished();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public void ShowNextState()
        {
        }

        [MethodImpl(0x8000)]
        public void StopStateChanging(bool isCancel)
        {
            DelegateBridge bridge = __Hotfix_StopStateChanging;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, isCancel);
            }
            else if (this.m_isDuringState)
            {
                this.m_isStateCancel = isCancel;
                foreach (TweenMain main in this.m_currUIState.TweenAnimationList)
                {
                    if (main == null)
                    {
                        continue;
                    }
                    if (main.style == TweenMain.Style.Once)
                    {
                        if (!main.gameObject.activeSelf)
                        {
                            main.gameObject.SetActive(true);
                        }
                        main.SetToFinish();
                    }
                }
                this.OnAllUIStateTweensFinished();
            }
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
            DelegateBridge bridge = __Hotfix_Update;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.m_isDuringState)
            {
                float num;
                if (!this.m_uiState2OnFinishedCallbackCountDownDict.TryGetValue(this.m_currUIState, out num))
                {
                    this.OnAllUIStateTweensFinished();
                }
                else
                {
                    this.m_currUIStateSiwtchTimer += !this.m_currUIState.IsUnscaledTime ? Time.deltaTime : UnScaledTime.deltaTime;
                    if (this.m_currUIStateSiwtchTimer > num)
                    {
                        if (!this.m_needOnFinishedCallbackNextUpdate)
                        {
                            this.m_needOnFinishedCallbackNextUpdate = true;
                        }
                        else
                        {
                            this.m_needOnFinishedCallbackNextUpdate = false;
                            this.OnAllUIStateTweensFinished();
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SetActionForUIStateFinshed>c__AnonStorey0
        {
            internal Action action;

            internal void <>m__0(bool a)
            {
                this.action();
            }
        }
    }
}

