﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class UIProcess
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private DebugOnEndFunc <DebugOnEnd>k__BackingField;
        protected UIProcessState m_state;
        protected OnEnd m_onEnd;
        protected List<UIProcess> m_childList;
        protected ProcessExecMode m_processExecMode;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddChild_0;
        private static DelegateBridge __Hotfix_AddChild_1;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_ToString;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnComplete;
        private static DelegateBridge __Hotfix_OnCompleteForSerialModeImpl;
        private static DelegateBridge __Hotfix_OnCompleteForParallelModeImpl;
        private static DelegateBridge __Hotfix_OnCancel;
        private static DelegateBridge __Hotfix_OnChildProcessEnd;
        private static DelegateBridge __Hotfix_OnChildCompleted;
        private static DelegateBridge __Hotfix_OnChildCompletedForSerialModeImpl;
        private static DelegateBridge __Hotfix_OnChildCompletedForParallelModeImpl;
        private static DelegateBridge __Hotfix_OnChildCancel;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_get_DebugOnEnd;
        private static DelegateBridge __Hotfix_set_DebugOnEnd;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;

        [MethodImpl(0x8000)]
        protected UIProcess(ProcessExecMode execMode)
        {
            this.m_processExecMode = execMode;
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp309(this, execMode);
            }
        }

        [MethodImpl(0x8000)]
        public void AddChild(UIProcess child)
        {
            DelegateBridge bridge = __Hotfix_AddChild_0;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, child);
            }
            else if ((child != null) && (this.m_state == UIProcessState.Init))
            {
                if (this.m_childList == null)
                {
                    this.m_childList = new List<UIProcess>();
                }
                child.m_onEnd = Delegate.Combine(child.m_onEnd, new OnEnd(this.OnChildProcessEnd)) as OnEnd;
                this.m_childList.Add(child);
            }
        }

        [MethodImpl(0x8000)]
        public void AddChild(params UIProcess[] children)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnCancel()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnChildCancel(UIProcess child)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnChildCompleted(UIProcess child)
        {
            DelegateBridge bridge = __Hotfix_OnChildCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, child);
            }
            else
            {
                ProcessExecMode processExecMode = this.m_processExecMode;
                if (processExecMode == ProcessExecMode.Serial)
                {
                    this.OnChildCompletedForSerialModeImpl(child);
                }
                else if (processExecMode == ProcessExecMode.Parallel)
                {
                    this.OnChildCompletedForParallelModeImpl(child);
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void OnChildCompletedForParallelModeImpl(UIProcess child)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChildCompletedForSerialModeImpl(UIProcess child)
        {
            DelegateBridge bridge = __Hotfix_OnChildCompletedForSerialModeImpl;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, child);
            }
            else if (((this.m_childList != null) && this.m_childList.Contains(child)) && (this.m_state == UIProcessState.WaitChildCompleted))
            {
                int index = this.m_childList.IndexOf(child);
                if (index != (this.m_childList.Count - 1))
                {
                    this.m_childList[index + 1].Start(new OnEnd(this.OnChildProcessEnd));
                }
                else
                {
                    this.m_state = UIProcessState.Completed;
                    this.OnProcessEnd(true);
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void OnChildProcessEnd(UIProcess child, bool isCompleted)
        {
            DelegateBridge bridge = __Hotfix_OnChildProcessEnd;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp12(this, child, isCompleted);
            }
            else if (isCompleted)
            {
                this.OnChildCompleted(child);
            }
            else
            {
                this.OnChildCancel(child);
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void OnComplete()
        {
            DelegateBridge bridge = __Hotfix_OnComplete;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                ProcessExecMode processExecMode = this.m_processExecMode;
                if (processExecMode == ProcessExecMode.Serial)
                {
                    this.OnCompleteForSerialModeImpl();
                }
                else if (processExecMode == ProcessExecMode.Parallel)
                {
                    this.OnCompleteForParallelModeImpl();
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void OnCompleteForParallelModeImpl()
        {
            DelegateBridge bridge = __Hotfix_OnCompleteForParallelModeImpl;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                bool flag = false;
                if ((this.m_childList != null) && (this.m_childList.Count != 0))
                {
                    foreach (UIProcess process in this.m_childList)
                    {
                        if (process.State != UIProcessState.Completed)
                        {
                            flag = true;
                            break;
                        }
                    }
                }
                if (flag)
                {
                    this.m_state = UIProcessState.WaitChildCompleted;
                }
                else
                {
                    this.m_state = UIProcessState.Completed;
                    this.OnProcessEnd(true);
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void OnCompleteForSerialModeImpl()
        {
            DelegateBridge bridge = __Hotfix_OnCompleteForSerialModeImpl;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if ((this.m_childList == null) || (this.m_childList.Count == 0))
            {
                this.m_state = UIProcessState.Completed;
                this.OnProcessEnd(true);
            }
            else
            {
                this.m_state = UIProcessState.WaitChildCompleted;
                this.m_childList[0].Start(new OnEnd(this.OnChildProcessEnd));
            }
        }

        [MethodImpl(0x8000)]
        protected void OnProcessEnd(bool isCompleted)
        {
            DelegateBridge bridge = __Hotfix_OnProcessEnd;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp18(this, isCompleted);
            }
            else
            {
                if (this.DebugOnEnd != null)
                {
                    this.DebugOnEnd();
                    this.DebugOnEnd = null;
                }
                if (this.m_onEnd != null)
                {
                    this.m_onEnd(this, isCompleted);
                    this.m_onEnd = null;
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStart()
        {
            DelegateBridge bridge = __Hotfix_OnStart;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if ((this.m_childList != null) && (this.m_processExecMode == ProcessExecMode.Parallel))
            {
                foreach (UIProcess process in this.m_childList)
                {
                    process.Start(new OnEnd(this.OnChildProcessEnd));
                }
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void OnStop(StopOption opt)
        {
            DelegateBridge bridge = __Hotfix_OnStop;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp305(this, opt);
            }
            else if (opt == StopOption.Complete)
            {
                this.OnComplete();
            }
            else
            {
                this.OnCancel();
            }
        }

        [MethodImpl(0x8000)]
        public void Start(OnEnd onEnd = null)
        {
            DelegateBridge bridge = __Hotfix_Start;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, onEnd);
            }
            else if (this.m_state == UIProcessState.Init)
            {
                this.m_state = UIProcessState.Started;
                this.m_onEnd = onEnd;
                this.OnStart();
            }
        }

        [MethodImpl(0x8000)]
        public void Stop(StopOption opt = 0, bool isRecursive = false)
        {
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
            DelegateBridge bridge = __Hotfix_ToString;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp33(this);
            }
            return this.GetHashCode().ToString();
        }

        public DebugOnEndFunc DebugOnEnd
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_DebugOnEnd;
                return ((bridge == null) ? this.<DebugOnEnd>k__BackingField : bridge.__Gen_Delegate_Imp306(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public UIProcessState State
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public delegate void DebugOnEndFunc();

        public delegate void OnEnd(UIProcess process, bool isCompleted);

        public enum ProcessExecMode
        {
            Serial,
            Parallel
        }

        public enum StopOption
        {
            Complete,
            Cancel
        }

        public enum UIProcessState
        {
            Init,
            Started,
            Canceled,
            WaitChildCompleted,
            Completed
        }
    }
}

