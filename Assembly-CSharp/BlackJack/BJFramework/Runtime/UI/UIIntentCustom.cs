﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class UIIntentCustom : UIIntent
    {
        private readonly Dictionary<string, object> m_params;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetParam_8;
        private static DelegateBridge __Hotfix_SetParam_6;
        private static DelegateBridge __Hotfix_SetParam_7;
        private static DelegateBridge __Hotfix_SetParam_13;
        private static DelegateBridge __Hotfix_SetParam_14;
        private static DelegateBridge __Hotfix_SetParam_5;
        private static DelegateBridge __Hotfix_SetParam_12;
        private static DelegateBridge __Hotfix_SetParam_1;
        private static DelegateBridge __Hotfix_SetParam_0;
        private static DelegateBridge __Hotfix_SetParam_9;
        private static DelegateBridge __Hotfix_SetParam_10;
        private static DelegateBridge __Hotfix_SetParam_4;
        private static DelegateBridge __Hotfix_SetParam_3;
        private static DelegateBridge __Hotfix_SetParam_2;
        private static DelegateBridge __Hotfix_SetParam_11;
        private static DelegateBridge __Hotfix_SetParam_17;
        private static DelegateBridge __Hotfix_SetParam_16;
        private static DelegateBridge __Hotfix_SetParam_15;
        private static DelegateBridge __Hotfix_TryGetParam;
        private static DelegateBridge __Hotfix_GetClassParam;
        private static DelegateBridge __Hotfix_GetStructParam;

        [MethodImpl(0x8000)]
        public UIIntentCustom(string targetTaskName, string targetMode = null)
        {
        }

        [MethodImpl(0x8000)]
        public T GetClassParam<T>(string key) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public T GetStructParam<T>(string key) where T: struct
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, byte value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, char value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, DateTime value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, decimal value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, double value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, short value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, int value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, long value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, object value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, sbyte value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, TimeSpan value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, ushort value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, uint value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, ulong value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, Vector2 value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, Vector3 value)
        {
        }

        [MethodImpl(0x8000)]
        private void SetParam<T>(string key, T value) where T: struct, IEquatable<T>
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetParam(string key, out object value)
        {
        }
    }
}

