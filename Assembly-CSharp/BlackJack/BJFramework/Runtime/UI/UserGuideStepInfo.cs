﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class UserGuideStepInfo
    {
        public int m_stepId;
        public bool isShowOperationAreaEffect;
        public string OpreationAreaEffectMode;
        public bool isShowOperationNotice;
        public bool isShowMaskPanel;
        public bool isStepEndByOpreatorAreaClick;
        public bool isPassEventOnStepEnd;
        public bool isQuitStepWhenClickMaskArea;
        public bool m_useNPCChat;
        public int m_npcChatDummyIndex;
        public Vector2 m_npcChatOffset;
        public UITaskBase m_targetTask;
        public int m_dragFlag;
        public float m_dragDistance;
        public string m_waitForTaskName;
        public List<IPageInfo> m_pageList;
        private Vector2 m_operationAreaCenter;
        private Vector2 m_operationAreaSize;
        public int m_dragFlagTypeOnStepEnd;
        private const float DefaultClickRadious = 100f;
        private const float MiniClickRadious = 15f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OperationAreaCenter;
        private static DelegateBridge __Hotfix_get_OperationAreaSize;
        private static DelegateBridge __Hotfix_SetOperationAreaInfo_1;
        private static DelegateBridge __Hotfix_SetOperationAreaInfo_0;
        private static DelegateBridge __Hotfix_SetOperationAreaInfo_2;
        private static DelegateBridge __Hotfix_SetOperationAreaSize;
        private static DelegateBridge __Hotfix_SetRectransform;

        [MethodImpl(0x8000)]
        public void SetOperationAreaInfo(Camera canvasCamera, RectTransform trans)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOperationAreaInfo(Vector2 center, Vector2 size)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOperationAreaInfo(Camera canvasCamera, Vector2 screenPos, Vector2 size)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOperationAreaSize(Vector2 size)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 SetRectransform(Vector2 size)
        {
        }

        public Vector2 OperationAreaCenter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Vector2 OperationAreaSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public interface IPageInfo
        {
            string GetDialogAudioPath();
            string GetDialogStr();
            string GetNPCIconPath();
            string GetNPCName();
            bool IsRemoteNPC();
            bool IsShowNPCDialog();
        }
    }
}

