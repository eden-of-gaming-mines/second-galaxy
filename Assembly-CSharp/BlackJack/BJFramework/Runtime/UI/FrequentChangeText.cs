﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using JetBrains.Annotations;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    [Serializable, RequireComponent(typeof(HorizontalLayoutGroup))]
    public class FrequentChangeText : MonoBehaviour
    {
        public UnityEngine.Font m_font;
        public UnityEngine.FontStyle m_fontStyle;
        public int m_fontSize;
        public TextAnchor m_alignment;
        public UnityEngine.Color m_color;
        public NumberFormatType m_formatType;
        public TimeFormat m_dateTimeFormat;
        public int m_decPlace;
        public double m_numberValue;
        public bool m_use64Bit;
        public string m_unitName;
        public PreMark m_premark;
        private HorizontalLayoutGroup m_horGroup;
        private readonly List<Text> m_charList;
        private static readonly string[] m_number;
        private static readonly DateTime m_zeroDate;
        private double m_lowestDeviation;
        private bool m_isDivNumber;
        private int m_upNumber;
        private int m_downNumber;
        private int m_extPlaceCount;
        private static DelegateBridge __Hotfix_GetHorizontalGroup;
        private static DelegateBridge __Hotfix_OnChanged;
        private static DelegateBridge __Hotfix_OnSetDivNumber;
        private static DelegateBridge __Hotfix_OnChangedTime_1;
        private static DelegateBridge __Hotfix_OnChangedTime_0;
        private static DelegateBridge __Hotfix_OnChangedTime_2;
        private static DelegateBridge __Hotfix_OperatIntegerValueAndDisplay_1;
        private static DelegateBridge __Hotfix_CalculateChars_0;
        private static DelegateBridge __Hotfix_RecycleUnused;
        private static DelegateBridge __Hotfix_CalculateChars_1;
        private static DelegateBridge __Hotfix_OperatIntegerValueAndDisplay_0;
        private static DelegateBridge __Hotfix_SetCharAt;
        private static DelegateBridge __Hotfix_SetUnitName;
        private static DelegateBridge __Hotfix_SetFont_1;
        private static DelegateBridge __Hotfix_SetFont_0;
        private static DelegateBridge __Hotfix_get_NumberValue;
        private static DelegateBridge __Hotfix_set_NumberValue;
        private static DelegateBridge __Hotfix_get_IntValue;
        private static DelegateBridge __Hotfix_set_IntValue;
        private static DelegateBridge __Hotfix_get_Font;
        private static DelegateBridge __Hotfix_set_Font;
        private static DelegateBridge __Hotfix_get_FontStyle;
        private static DelegateBridge __Hotfix_set_FontStyle;
        private static DelegateBridge __Hotfix_get_FontSize;
        private static DelegateBridge __Hotfix_set_FontSize;
        private static DelegateBridge __Hotfix_get_Alignment;
        private static DelegateBridge __Hotfix_set_Alignment;
        private static DelegateBridge __Hotfix_get_FormatType;
        private static DelegateBridge __Hotfix_set_FormatType;
        private static DelegateBridge __Hotfix_get_DecPlace;
        private static DelegateBridge __Hotfix_set_DecPlace;
        private static DelegateBridge __Hotfix_get_Color;
        private static DelegateBridge __Hotfix_set_Color;
        private static DelegateBridge __Hotfix_get_Use64Bit;
        private static DelegateBridge __Hotfix_set_Use64Bit;
        private static DelegateBridge __Hotfix_get_UnitName;
        private static DelegateBridge __Hotfix_set_UnitName;
        private static DelegateBridge __Hotfix_get_DateTimeFormat;
        private static DelegateBridge __Hotfix_set_DateTimeFormat;
        private static DelegateBridge __Hotfix_get_Premark;
        private static DelegateBridge __Hotfix_set_Premark;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnEditorEnable;
        private static DelegateBridge __Hotfix_Clean;
        private static DelegateBridge __Hotfix_SetNumber_0;
        private static DelegateBridge __Hotfix_SetNumber_1;
        private static DelegateBridge __Hotfix_SetDateTime_0;
        private static DelegateBridge __Hotfix_SetDateTime_1;
        private static DelegateBridge __Hotfix_SetDivNum;

        [MethodImpl(0x8000)]
        private void CalculateChars(double dValue, ref int index)
        {
        }

        [MethodImpl(0x8000)]
        private void CalculateChars(float fValue, ref int index)
        {
        }

        [MethodImpl(0x8000)]
        private void Clean()
        {
        }

        [MethodImpl(0x8000)]
        private HorizontalLayoutGroup GetHorizontalGroup()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChanged(double dValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangedTime(DateTime dateTime)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangedTime(long seconds)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangedTime(TimeSpan ts)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEditorEnable()
        {
        }

        [MethodImpl(0x8000), UsedImplicitly]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSetDivNumber(int upNum, int downNum)
        {
        }

        [MethodImpl(0x8000)]
        private bool OperatIntegerValueAndDisplay(int intValue, int intPlaceCount, ref int index)
        {
        }

        [MethodImpl(0x8000)]
        private bool OperatIntegerValueAndDisplay(long longValue, int intPlaceCount, ref int index)
        {
        }

        [MethodImpl(0x8000)]
        private void RecycleUnused(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCharAt(int charNumberIndex, int positionIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDateTime(DateTime time, TimeFormat format = 3)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDateTime(TimeSpan time, TimeFormat format = 8)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDivNum(int upNum, int downNum)
        {
        }

        [MethodImpl(0x8000)]
        private void SetFont()
        {
        }

        [MethodImpl(0x8000)]
        private void SetFont(Text text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNumber(long value, string ext = null, PreMark preMark = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNumber(double value, string ext = null, int decPlace = 2, PreMark preMark = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUnitName(string unitName, int positionIndex)
        {
        }

        public double NumberValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public long IntValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public UnityEngine.Font Font
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public UnityEngine.FontStyle FontStyle
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int FontSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public TextAnchor Alignment
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public NumberFormatType FormatType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int DecPlace
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public UnityEngine.Color Color
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool Use64Bit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public string UnitName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public TimeFormat DateTimeFormat
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public PreMark Premark
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public enum NumberFormatType
        {
            None,
            Seperated,
            WithExt
        }

        public enum PreMark
        {
            None = 0,
            LessThan = -1,
            MoreThan = 1,
            Plus = 2
        }

        [Flags]
        public enum TimeFormat
        {
            None = 0,
            Date = 1,
            Time = 2,
            DateAndTime = 3,
            ShortDate = 5,
            ShortDateTime = 7,
            TimeSpan = 8,
            MinuteSecond = 12,
            HHMMSS = 13
        }
    }
}

