﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class LoginUITaskBase : NetWorkCheckUITask
    {
        protected LoginState m_loginTaskState;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsNeedSdkRelogin>k__BackingField;
        protected static bool m_isDownloadServerLoginAnnouncement = true;
        protected bool m_isSwitchAccount;
        protected static bool s_sdkLogined;
        protected static DateTime s_sdkLoginTime;
        protected string m_authLoginServerAddress;
        protected int m_authLoginServerPort;
        protected string m_authtoken;
        protected string m_gameUserId;
        protected string m_serverId;
        protected int m_loginChannelId;
        protected int m_bornChannelId;
        protected string m_authLoginServerDomain;
        protected bool m_isWaitingForMsgAck;
        protected DateTime m_timeOutTime;
        protected float m_timeoutDelayTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_IsSDKLogin;
        private static DelegateBridge __Hotfix_IsEditorTestLogin;
        private static DelegateBridge __Hotfix_IsLoginByServerListInEditor;
        private static DelegateBridge __Hotfix_MakeSDKLogout;
        private static DelegateBridge __Hotfix_OnSDKLogout;
        private static DelegateBridge __Hotfix_DownloadGameServerLoginAnnouncementBeforeSDKLogin;
        private static DelegateBridge __Hotfix_OnDownloadGameServerLoginAnnouncementEndBeforeSDKLogin;
        private static DelegateBridge __Hotfix_StartSDKLogin;
        private static DelegateBridge __Hotfix_OnSDKLoginEnd;
        private static DelegateBridge __Hotfix_PostSDKLogin;
        private static DelegateBridge __Hotfix_IsNeedSelectServer;
        private static DelegateBridge __Hotfix_LaunchServerListUI;
        private static DelegateBridge __Hotfix_DownloadServerList;
        private static DelegateBridge __Hotfix_OnDownloadServerListEnd;
        private static DelegateBridge __Hotfix_ShowServerListUI;
        private static DelegateBridge __Hotfix_OnDownloadServerListFailed;
        private static DelegateBridge __Hotfix_OnGameServerConfirmed;
        private static DelegateBridge __Hotfix_InitServerCtxByServerId;
        private static DelegateBridge __Hotfix_GetLastLoginedServerId;
        private static DelegateBridge __Hotfix_LaunchEnterGameUI;
        private static DelegateBridge __Hotfix_LaunchEnterGameUIWithGameSettingTokenAndServer;
        private static DelegateBridge __Hotfix_LaunchEnterGameUIWithUIInputAccountAndServer;
        private static DelegateBridge __Hotfix_LaunchEnterGameUIWithServerListInEditor;
        private static DelegateBridge __Hotfix_LoadLastLoginWithUIInputInfo;
        private static DelegateBridge __Hotfix_DownloadGameServerLoginAnnouncement;
        private static DelegateBridge __Hotfix_OnDownloadGameServerLoginAnnouncementEnd;
        private static DelegateBridge __Hotfix_ShowGameServerLoginAnnouncementUI;
        private static DelegateBridge __Hotfix_ShowEnterGameUI;
        private static DelegateBridge __Hotfix_OnEnterGameConfirmed;
        private static DelegateBridge __Hotfix_StartLoginAgentLogin;
        private static DelegateBridge __Hotfix_LoginAgentLoginEnd;
        private static DelegateBridge __Hotfix_StartAuthLogin;
        private static DelegateBridge __Hotfix_StartGameAuthLogin;
        private static DelegateBridge __Hotfix_StartSessionLogin;
        private static DelegateBridge __Hotfix_StartGameSessionLogin;
        private static DelegateBridge __Hotfix_GetLastSessionToken;
        private static DelegateBridge __Hotfix_StartPlayerInfoInitReq;
        private static DelegateBridge __Hotfix_LauncheMainUI;
        private static DelegateBridge __Hotfix_OnWaitingMsgAckOutTime;
        private static DelegateBridge __Hotfix_StartWaitingMsgAck;
        private static DelegateBridge __Hotfix_StopWaitingMsgAck;
        private static DelegateBridge __Hotfix_ClearGameServerLoginState;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnGameServerConnected;
        private static DelegateBridge __Hotfix_OnGameServerDisconnected;
        private static DelegateBridge __Hotfix_OnGameServerNetworkError;
        private static DelegateBridge __Hotfix_OnLoginByAuthTokenAck;
        private static DelegateBridge __Hotfix_OnLoginBySessionTokenAck;
        private static DelegateBridge __Hotfix_OnConfigDataMd5InfoNtf;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitEnd;
        private static DelegateBridge __Hotfix_RegisterNetEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetEvent;
        private static DelegateBridge __Hotfix_get_IsNeedSdkRelogin;
        private static DelegateBridge __Hotfix_set_IsNeedSdkRelogin;

        [MethodImpl(0x8000)]
        public LoginUITaskBase(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearGameServerLoginState()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected virtual IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected virtual IEnumerator DownloadGameServerLoginAnnouncementBeforeSDKLogin(bool loginSDk, Action<bool, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual IEnumerator DownloadServerList(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetLastLoginedServerId()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetLastSessionToken()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitServerCtxByServerId(string serverId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsEditorTestLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsLoginByServerListInEditor()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedSelectServer()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsSDKLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void LauncheMainUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void LaunchEnterGameUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void LaunchEnterGameUIWithGameSettingTokenAndServer()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void LaunchEnterGameUIWithServerListInEditor()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void LaunchEnterGameUIWithUIInputAccountAndServer()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void LaunchServerListUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool LoadLastLoginWithUIInputInfo(out string loginUserId, out string authLoginServerAddress, out int authLoginServerPort)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void LoginAgentLoginEnd(int errCode, string loginUserId, string authToken)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void MakeSDKLogout()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnConfigDataMd5InfoNtf(string fileNameWithErrorMd5, string localMd5, string serverMd5)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDownloadGameServerLoginAnnouncementEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDownloadGameServerLoginAnnouncementEndBeforeSDKLogin(bool loginSDK, bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDownloadServerListEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDownloadServerListFailed()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnEnterGameConfirmed()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerConfirmed(string serverId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerConnected()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerNetworkError()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnLoginBySessionTokenAck(int ret)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnPlayerInfoInitEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnSDKLoginEnd(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnSDKLogout(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnWaitingMsgAckOutTime()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PostSDKLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterNetEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowEnterGameUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowGameServerLoginAnnouncementUI(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowServerListUI()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartAuthLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool StartGameAuthLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool StartGameSessionLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartPlayerInfoInitReq()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartSDKLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartSessionLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartWaitingMsgAck(float waitTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void StopWaitingMsgAck()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterNetEvent()
        {
        }

        protected virtual bool IsNeedSdkRelogin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DownloadGameServerLoginAnnouncement>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal LoginUITaskBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                this.$PC = -1;
                if ((this.$PC == 0) && this.$this.IsSDKLogin())
                {
                    throw new NotImplementedException();
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DownloadGameServerLoginAnnouncementBeforeSDKLogin>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal LoginUITaskBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                this.$PC = -1;
                if (this.$PC == 0)
                {
                    this.$this.EnableUIInput(false);
                    if (this.$this.IsSDKLogin())
                    {
                        throw new NotImplementedException();
                    }
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <OnDownloadGameServerLoginAnnouncementEndBeforeSDKLogin>c__AnonStorey2
        {
            internal bool loginSDK;
            internal LoginUITaskBase $this;

            internal void <>m__0()
            {
                if (!this.loginSDK)
                {
                    this.$this.ShowEnterGameUI();
                }
                else if (!LoginUITaskBase.s_sdkLogined)
                {
                    this.$this.StartSDKLogin();
                }
                else
                {
                    this.$this.OnSDKLoginEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PostSDKLogin>c__AnonStorey3
        {
            internal Action<bool> action;

            internal void <>m__0(int errCode, string loginUserId, string authToken)
            {
                bool flag = !string.IsNullOrEmpty(loginUserId);
                if (this.action != null)
                {
                    this.action(flag);
                }
            }
        }

        protected enum LoginState
        {
            None,
            Inited,
            StartSDKInit,
            SDKInitEnd,
            StartSDKLogin,
            SDKLoginEnd,
            ReadyForEnterGame,
            StartLoginAgentLogin,
            LoginAgentLoginEnd,
            StartAuthLogin,
            AuthLoginEnd,
            RedirectWaitDisconnect,
            StartSessionLogin,
            SessionLoginEnd,
            StartPlayerInfoInit,
            ReadyForCharacterUI
        }
    }
}

