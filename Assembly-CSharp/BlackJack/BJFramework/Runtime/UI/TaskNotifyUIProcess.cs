﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class TaskNotifyUIProcess : UIProcess
    {
        protected string m_taskName;
        protected string m_notifyType;
        protected string m_notifyDesc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnUITaskNotify;

        [MethodImpl(0x8000)]
        public TaskNotifyUIProcess(string taskName, string notifyType, string notifyDesc, UIProcess.ProcessExecMode execMode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStart()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUITaskNotify(UITaskBase task, string notifyType, string notifyDesc)
        {
        }
    }
}

