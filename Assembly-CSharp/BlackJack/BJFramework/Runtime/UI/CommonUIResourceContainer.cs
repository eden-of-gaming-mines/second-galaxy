﻿namespace BlackJack.BJFramework.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CommonUIResourceContainer : MonoBehaviour
    {
        public List<AssetCacheItem> AssetList;
        private static DelegateBridge __Hotfix_GetAsset;

        [MethodImpl(0x8000)]
        public T GetAsset<T>(CommonUIType type) where T: UnityEngine.Object
        {
        }

        [Serializable]
        public class AssetCacheItem
        {
            public CommonUIResourceContainer.CommonUIType UIType;
            public UnityEngine.Object Asset;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        [Serializable]
        public enum CommonUIType
        {
            Invalid,
            StatrGate,
            SpaceStateion,
            Planet,
            Star,
            Frigate,
            Destroyer,
            Cruiser,
            BattleCruiser,
            BattleShip
        }
    }
}

