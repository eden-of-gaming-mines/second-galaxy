﻿namespace BlackJack.BJFramework.Runtime
{
    using System;
    using System.Runtime.InteropServices;

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class TimelineAutoBindAttribute : Attribute
    {
        public readonly string TypeFieldName;

        public TimelineAutoBindAttribute(string typeFieldName = "")
        {
            this.TypeFieldName = typeFieldName;
        }
    }
}

