﻿namespace BlackJack.BJFramework.Runtime
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface IAudioManager : ITickable
    {
        void ClearSpeechCache();
        void EnableAudioPlugin(bool enable);
        bool Initlize();
        bool IsAudioPlaying(BlackJack.BJFramework.Runtime.AudioType audioType, string name = null);
        void PlayBackGroundMusic(string music, bool isInterruptSame = true);
        void PlayPlayerVoice(AudioClip sound, float volume = 1f);
        void PlaySound(string sound, object audioClip, PlaySoundOption playOption, bool loop = false, float volume = 1f);
        void PlaySpeech(string music, float volume = 1f);
        void SetLocalization(string localizationStr);
        void SetMuteState(bool muteBackGroundMusic, bool muteSound, bool mutePlayerVoice, bool muteSpeech);
        void SetSoundParameter(string paraName, float value);
        void SetVolume(BlackJack.BJFramework.Runtime.AudioType audioType, float volume);
        void SetVolumeToZero(bool muteBackGroundMusic, bool muteSound, bool mutePlayerVoice, bool muteSpeech);
        IEnumerator Start(Action<bool> onEnd, string initAssetPath, string criFilePrefix, Dictionary<string, string> configDataDict);
        void StopAll();
        void StopAudio(BlackJack.BJFramework.Runtime.AudioType audioType, string audio = null);
        void Uninitlize();
    }
}

