﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class AudioManager4CRI : IAudioManager, ITickable
    {
        private static AudioManager4CRI m_instance;
        private bool m_muteBackGroundMusic;
        private bool m_muteSound;
        private bool m_mutePlayerVoice;
        private bool m_muteSpeech;
        private float m_backGroundMusicVolume = 1f;
        private float m_soundVolume = 1f;
        private float m_playerVoiceVolume = 1f;
        private float m_speechVolume = 1f;
        private AudioSource m_playerVoiceAudioSource;
        private CRIProvider m_CRIProvider;
        private bool m_isAudioManagerInited;
        private string m_currLocalization;
        private static string m_CRIWAREAssetPath = "CRI/CRIWARE";
        private static string m_CriWareErrorHandlerAssetPath = "CRI/CriWareErrorHandler";
        private static string m_CriWareLibraryInitializerAssetPath = "CRI/CriWareLibraryInitializer";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateAudioManager;
        private static DelegateBridge __Hotfix_SetCRIProvider;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_GetCueLength;
        private static DelegateBridge __Hotfix_GetCRIProvider;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CreateCRI;
        private static DelegateBridge __Hotfix_CreatePlayerVoiceSource;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_StopAll;
        private static DelegateBridge __Hotfix_SetVolumeToZero;
        private static DelegateBridge __Hotfix_SetMuteState;
        private static DelegateBridge __Hotfix_SetVolume;
        private static DelegateBridge __Hotfix_PlayBackGroundMusic;
        private static DelegateBridge __Hotfix_StopBackGroundMusic;
        private static DelegateBridge __Hotfix_PlaySound;
        private static DelegateBridge __Hotfix_StopAudio;
        private static DelegateBridge __Hotfix_PlaySpeech;
        private static DelegateBridge __Hotfix_IsSpeechPlaying;
        private static DelegateBridge __Hotfix_PlayPlayerVoice;
        private static DelegateBridge __Hotfix_IsSoundPlaying;
        private static DelegateBridge __Hotfix_IsAnySoundPlaying;
        private static DelegateBridge __Hotfix_IsPlayerVoicePlaying;
        private static DelegateBridge __Hotfix_StopPlayerVoiceByAudioSource;
        private static DelegateBridge __Hotfix_SetSoundParameter;
        private static DelegateBridge __Hotfix_IsAudioPlaying;
        private static DelegateBridge __Hotfix_ClearSpeechCache;
        private static DelegateBridge __Hotfix_SetLocalization;
        private static DelegateBridge __Hotfix_EnableAudioPlugin;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        public AudioManager4CRI()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void ClearSpeechCache()
        {
        }

        [MethodImpl(0x8000)]
        public static AudioManager4CRI CreateAudioManager()
        {
            DelegateBridge bridge = __Hotfix_CreateAudioManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp4();
            }
            if (m_instance == null)
            {
                m_instance = new AudioManager4CRI();
            }
            return m_instance;
        }

        [MethodImpl(0x8000)]
        private void CreateCRI()
        {
        }

        [MethodImpl(0x8000)]
        private void CreatePlayerVoiceSource()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableAudioPlugin(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public CRIProvider GetCRIProvider()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCueLength(string cueName)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initlize()
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            this.m_muteBackGroundMusic = false;
            this.m_mutePlayerVoice = false;
            this.m_muteSound = false;
            this.m_muteSpeech = false;
            this.m_playerVoiceVolume = 1f;
            Debug.Log("AudioManager4CRI.Initlize start");
            return true;
        }

        [MethodImpl(0x8000)]
        public bool IsAnySoundPlaying()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAudioPlaying(BlackJack.BJFramework.Runtime.AudioType audioType, string name = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPlayerVoicePlaying()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSoundPlaying(string sound)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSpeechPlaying()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayBackGroundMusic(string music, bool isInterruptSame = true)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayPlayerVoice(AudioClip sound, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySound(string sound, object audioClip, PlaySoundOption playOption, bool loop = false, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySpeech(string sound, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCRIProvider(CRIProvider audioCRIImpl)
        {
            DelegateBridge bridge = __Hotfix_SetCRIProvider;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, audioCRIImpl);
            }
            else
            {
                this.m_CRIProvider = audioCRIImpl;
            }
        }

        [MethodImpl(0x8000)]
        public void SetLocalization(string localizationStr)
        {
            DelegateBridge bridge = __Hotfix_SetLocalization;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, localizationStr);
            }
            else
            {
                this.m_currLocalization = localizationStr;
            }
        }

        [MethodImpl(0x8000)]
        public void SetMuteState(bool muteBackGroundMusic, bool muteSound, bool mutePlayerVoice, bool muteSpeech)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundParameter(string paraName, float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVolume(BlackJack.BJFramework.Runtime.AudioType audioType, float volumn)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVolumeToZero(bool muteBackGroundMusic, bool muteSound, bool mutePlayerVoice, bool muteSpeech)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator Start(Action<bool> onEnd, string initAssetPath, string criFilePrefix, Dictionary<string, string> configDataDict)
        {
        }

        [MethodImpl(0x8000)]
        public void StopAll()
        {
        }

        [MethodImpl(0x8000)]
        public void StopAudio(BlackJack.BJFramework.Runtime.AudioType audioType, string audio = null)
        {
        }

        [MethodImpl(0x8000)]
        public void StopBackGroundMusic()
        {
        }

        [MethodImpl(0x8000)]
        private void StopPlayerVoiceByAudioSource()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.m_CRIProvider.Tick();
            }
        }

        [MethodImpl(0x8000)]
        public void Uninitlize()
        {
        }

        public static AudioManager4CRI Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp4());
            }
        }

        [CompilerGenerated]
        private sealed class <Start>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string criFilePrefix;
            internal Dictionary<string, string> configDataDict;
            internal Action<bool> onEnd;
            internal AudioManager4CRI $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

