﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class TimelineObjectStorage
    {
        private Dictionary<string, object> m_objectDic;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ObjectDic;
        private static DelegateBridge __Hotfix_GetObject;
        private static DelegateBridge __Hotfix_AddObject;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_CopyData_1;
        private static DelegateBridge __Hotfix_CopyData_0;
        private static DelegateBridge __Hotfix_Count;
        private static DelegateBridge __Hotfix_FindChild;
        private static DelegateBridge __Hotfix_FindChildPath;
        private static DelegateBridge __Hotfix_MakeChildPath;

        [MethodImpl(0x8000)]
        public virtual void AddObject(string objectType, object obj)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public virtual Dictionary<string, object> CopyData()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CopyData(TimelineObjectStorage storagte)
        {
        }

        [MethodImpl(0x8000)]
        public int Count()
        {
        }

        [MethodImpl(0x8000)]
        private static Transform FindChild(Transform parentTrans, string childName)
        {
        }

        [MethodImpl(0x8000)]
        public static Transform FindChildPath(Transform root, string childPath)
        {
        }

        [MethodImpl(0x8000)]
        public virtual object GetObject(string objectType, string objectParam)
        {
        }

        [MethodImpl(0x8000)]
        public static string MakeChildPath(Transform root, Transform child)
        {
        }

        public Dictionary<string, object> ObjectDic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

