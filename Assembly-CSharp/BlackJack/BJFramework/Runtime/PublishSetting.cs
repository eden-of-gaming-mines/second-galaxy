﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class PublishSetting
    {
        [Header("是否启用obb分包机制")]
        public bool EnableAndroidObb;
        [Header("安卓发布key")]
        public string AndroidPublicKey;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public PublishSetting()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

