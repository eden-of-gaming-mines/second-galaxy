﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class AudioSettings
    {
        public string AudioMixerAssetPath;
        public string AudioMixerBGMGroupSubPath;
        public string AudioMixerSoundEffectGroupSubPath;
        public string AudioMixerPlayerVoiceGroupSubPath;
        public string AudioMixerSpeechGroupSubPath;
        public string AudioMixerBGMVolumeParamName;
        public string AudioMixerMovieBGMVolumeParamName;
        public string AudioMixerSoundEffectParamName;
        public string AudioMixerPlayerVoiceVolumeParamName;
        public string AudioMixerSpeechVolumeParamName;
        public bool EnableCRI;
        public bool AudioMultiLanguage;
        public string AudioDefaultLanguage;
        public string CRIAudioACFAssetPrefixPath;
        public string CRIAudioPathPrefixInEditor;
        public string CRIVideoAssetPathRoot;
        public string CRIVideoAssetPathRootForEditor;
        public string CRIFilePrefix;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public AudioSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

