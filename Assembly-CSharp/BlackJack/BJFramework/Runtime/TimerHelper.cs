﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class TimerHelper
    {
        protected static Stack<DateTime> m_timeStack = new Stack<DateTime>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Stop;

        [MethodImpl(0x8000)]
        public static void Start(string msg = "", bool logStart = true)
        {
        }

        [MethodImpl(0x8000)]
        public static void Stop(string msg = "", bool isLargeThanFixedTime = false, double fixedTime = 300.0)
        {
        }
    }
}

