﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class TimelineClipStringValue
    {
        [Header("手动输入绑定类型")]
        public string BindType;
        [TimelineAutoBind("BindType")]
        public string Value;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

