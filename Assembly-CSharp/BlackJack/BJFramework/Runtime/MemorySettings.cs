﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class MemorySettings
    {
        [Header("低内存阀值(mb)")]
        public int LowMemorySize = 0x400;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public MemorySettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

