﻿namespace BlackJack.BJFramework.Runtime.PlayerContext
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class PlayerContextBase : ITickable, IPlayerContextNetworkEventHandler
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGameServerConnected;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGameServerNetworkError;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGameServerDisconnected;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Func<int, string, bool, bool> EventOnLoginByAuthTokenAck;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Func<int, bool> EventOnLoginBySessionTokenAck;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGameServerDataUnsyncNotify;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<object> EventOnPlayerInfoInitAck;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPlayerInfoInitEnd;
        protected IPlayerContextNetworkClient m_networkClient;
        protected TinyCorutineHelper m_tinyCorutineHelper;
        protected PlayerContextStateMachine m_fsm;
        protected string m_deviceId;
        protected string m_clientVersion;
        private int m_loginChannelId;
        private int m_bornChannelId;
        private string m_localization;
        protected string m_sessionToken;
        protected bool m_inited;
        private DateTime? m_serverTimeSynced;
        private DateTime m_localTimeAtServerTimeSynced;
        private DateTime m_currTickServerTime;
        private bool m_isBlockNetwork;
        [CompilerGenerated]
        private static Action<string> <>f__mg$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstance;
        private static DelegateBridge __Hotfix_InitWithNetworkClient;
        private static DelegateBridge __Hotfix_IsInited;
        private static DelegateBridge __Hotfix_BlockNetworkClient;
        private static DelegateBridge __Hotfix_IsBlockNetworkClient;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitEndNtf;
        private static DelegateBridge __Hotfix_SyncServerTime;
        private static DelegateBridge __Hotfix_GetCurrServerTime;
        private static DelegateBridge __Hotfix_GetCurrTickServerTime;
        private static DelegateBridge __Hotfix_Disconnect;
        private static DelegateBridge __Hotfix_StartGameAuthLogin;
        private static DelegateBridge __Hotfix_SetSessionToken;
        private static DelegateBridge __Hotfix_StartGameSessionLogin;
        private static DelegateBridge __Hotfix_SendPlayerInfoInitReq;
        private static DelegateBridge __Hotfix_SendPlayerInfoReqOnReloginBySession;
        private static DelegateBridge __Hotfix_SendWorldEnterReqOnReloginBySession;
        private static DelegateBridge __Hotfix_CheckForSessionLogin;
        private static DelegateBridge __Hotfix_CreateFakeAuthToken;
        private static DelegateBridge __Hotfix_GetDeviceId;
        private static DelegateBridge __Hotfix_GetClientVersion;
        private static DelegateBridge __Hotfix_OnGameServerConnected;
        private static DelegateBridge __Hotfix_OnGameServerDisconnected;
        private static DelegateBridge __Hotfix_OnGameServerError;
        private static DelegateBridge __Hotfix_OnLoginByAuthTokenAck;
        private static DelegateBridge __Hotfix_OnLoginBySessionTokenAck;
        private static DelegateBridge __Hotfix_OnGameServerMessage;
        private static DelegateBridge __Hotfix_OnGameServerMessageExtend;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnTimeJumped;
        private static DelegateBridge __Hotfix_add_EventOnGameServerConnected;
        private static DelegateBridge __Hotfix_remove_EventOnGameServerConnected;
        private static DelegateBridge __Hotfix_add_EventOnGameServerNetworkError;
        private static DelegateBridge __Hotfix_remove_EventOnGameServerNetworkError;
        private static DelegateBridge __Hotfix_add_EventOnGameServerDisconnected;
        private static DelegateBridge __Hotfix_remove_EventOnGameServerDisconnected;
        private static DelegateBridge __Hotfix_add_EventOnLoginByAuthTokenAck;
        private static DelegateBridge __Hotfix_remove_EventOnLoginByAuthTokenAck;
        private static DelegateBridge __Hotfix_add_EventOnLoginBySessionTokenAck;
        private static DelegateBridge __Hotfix_remove_EventOnLoginBySessionTokenAck;
        private static DelegateBridge __Hotfix_add_EventOnGameServerDataUnsyncNotify;
        private static DelegateBridge __Hotfix_remove_EventOnGameServerDataUnsyncNotify;
        private static DelegateBridge __Hotfix_add_EventOnPlayerInfoInitAck;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerInfoInitAck;
        private static DelegateBridge __Hotfix_add_EventOnPlayerInfoInitEnd;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerInfoInitEnd;

        public event Action EventOnGameServerConnected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGameServerDataUnsyncNotify
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGameServerDisconnected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGameServerNetworkError
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Func<int, string, bool, bool> EventOnLoginByAuthTokenAck
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Func<int, bool> EventOnLoginBySessionTokenAck
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<object> EventOnPlayerInfoInitAck
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPlayerInfoInitEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected PlayerContextBase()
        {
        }

        [MethodImpl(0x8000)]
        public void BlockNetworkClient(bool isBlock)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForSessionLogin()
        {
        }

        [MethodImpl(0x8000)]
        public static string CreateFakeAuthToken(string platformUserId, string password)
        {
        }

        [MethodImpl(0x8000)]
        public bool Disconnect()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetClientVersion()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetCurrServerTime()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetCurrTickServerTime()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetDeviceId()
        {
        }

        [MethodImpl(0x8000)]
        public static PlayerContextBase GetInstance()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void InitWithNetworkClient(IPlayerContextNetworkClient networkClient)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBlockNetworkClient()
        {
        }

        public abstract bool IsDataCacheDirtyByPlayerInfoInitAck(object msg, out bool raiseCriticalDataCacheDirty);
        [MethodImpl(0x8000)]
        public bool IsInited()
        {
        }

        public abstract bool IsPlayerInfoInitAck4CheckOnly(object msg);
        [MethodImpl(0x8000)]
        public virtual void OnGameServerConnected()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnGameServerDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnGameServerError(int err, string excepionInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnGameServerMessage(object msg, int msgId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnGameServerMessageExtend(object msg, int msgId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnLoginBySessionTokenAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerInfoInitEndNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTimeJumped()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool SendPlayerInfoInitReq()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool SendPlayerInfoReqOnReloginBySession(bool checkOnly)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool SendWorldEnterReqOnReloginBySession()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSessionToken(string sessionToken, int channelId, int bornChannelId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool StartGameAuthLogin(string serverAddress, int serverPort, string authToken, string localization, int loginChannelId, int bornChannelId, string serverDomain)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool StartGameSessionLogin()
        {
        }

        [MethodImpl(0x8000)]
        protected void SyncServerTime(DateTime serverTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick()
        {
        }
    }
}

