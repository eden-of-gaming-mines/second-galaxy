﻿namespace BlackJack.BJFramework.Runtime.PlayerContext
{
    using System;
    using System.Runtime.InteropServices;

    public interface IPlayerContextNetworkEventHandler
    {
        void OnGameServerConnected();
        void OnGameServerDisconnected();
        void OnGameServerError(int err, string excepionInfo = null);
        void OnGameServerMessage(object msg, int msgId);
        void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken);
        void OnLoginBySessionTokenAck(int result);
    }
}

