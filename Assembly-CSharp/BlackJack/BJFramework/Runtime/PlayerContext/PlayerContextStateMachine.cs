﻿namespace BlackJack.BJFramework.Runtime.PlayerContext
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class PlayerContextStateMachine : StateMachine
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetStateCheck_1;
        private static DelegateBridge __Hotfix_SetStateCheck_0;
        private static DelegateBridge __Hotfix_get_StateEnumValue;

        [MethodImpl(0x8000)]
        public StateCode SetStateCheck(EventCode commingEvent, StateCode newState = -1, bool testOnly = false)
        {
        }

        [MethodImpl(0x8000)]
        public override int SetStateCheck(int commingEvent, int newState = -1, bool testOnly = false)
        {
        }

        public StateCode StateEnumValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum EventCode
        {
            OnConnected = 1,
            OnDisconnected = 2,
            OnAuthLoginStart = 3,
            OnAuthLoginAckOk = 4,
            OnAuthLoginAckOkRedirect = 5,
            OnAuthLoginAckFail = 6,
            OnSessionLoginStart = 7,
            OnSessionLoginAckOk = 8,
            OnSessionLoginAckFail = 9,
            OnGameLogicMsgSend = 10,
            OnPlayerInitEnd = 11
        }

        public enum StateCode
        {
            Invalid = -1,
            None = 0,
            Disconnected = 1,
            AuthLoginStarted = 2,
            RedirectWaitDisconnect = 3,
            SessionLoginStarted = 4,
            GameLoginDone = 5,
            PlayerInitEnd = 6
        }
    }
}

