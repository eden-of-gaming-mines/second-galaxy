﻿namespace BlackJack.BJFramework.Runtime.PlayerContext
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [Serializable]
    public class DataSection
    {
        [NonSerialized]
        protected ushort m_version;
        protected ushort m_persistentVersion;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitWithPersistentVersion;
        private static DelegateBridge __Hotfix_SetVersion;
        private static DelegateBridge __Hotfix_SetDirty;
        private static DelegateBridge __Hotfix_NeedFlushPersistent;
        private static DelegateBridge __Hotfix_OnFlushed;
        private static DelegateBridge __Hotfix_ClearPersistentVersion;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_get_PersistentVersion;
        private static DelegateBridge __Hotfix_OnDeserialized;

        [MethodImpl(0x8000)]
        public virtual void ClearPersistentVersion()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void InitWithPersistentVersion(ushort version)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool NeedFlushPersistent()
        {
        }

        [MethodImpl(0x8000), OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnFlushed()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDirty()
        {
        }

        [MethodImpl(0x8000)]
        public void SetVersion(ushort version)
        {
        }

        public ushort Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ushort PersistentVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

