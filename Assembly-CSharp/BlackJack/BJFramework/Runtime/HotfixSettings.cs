﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class HotfixSettings
    {
        [Header("是否启用hotfix")]
        public bool EnableHotfix;
        [Header("hotfix程序集名称")]
        public string HotfixAssemblyName = "DyncDll";
        public string HotfixAssemblyRoot = "Assets/HotfixAssembly";
        public string HotfixAssemblyAssetRoot = "Assets/GameProject/RuntimeAssets/HotfixAssembly";
        public string[] HotFixableTypePrefix;
        public string[] ExceptedTypePrefix;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public HotfixSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

