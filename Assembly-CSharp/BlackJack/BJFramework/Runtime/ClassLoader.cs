﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    public class ClassLoader
    {
        private static ClassLoader m_instance;
        private Dictionary<string, Assembly> m_assembleDict = new Dictionary<string, Assembly>();
        private Dictionary<string, System.Type> m_typeDict = new Dictionary<string, System.Type>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateClassLoader;
        private static DelegateBridge __Hotfix_CreateInstance;
        private static DelegateBridge __Hotfix_LoadType;
        private static DelegateBridge __Hotfix_AddAssembly;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        private ClassLoader()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void AddAssembly(Assembly assembly)
        {
            DelegateBridge bridge = __Hotfix_AddAssembly;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, assembly);
            }
            else
            {
                this.m_assembleDict[assembly.GetName().Name] = assembly;
            }
        }

        [MethodImpl(0x8000)]
        public static ClassLoader CreateClassLoader()
        {
            DelegateBridge bridge = __Hotfix_CreateClassLoader;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp51();
            }
            if (m_instance == null)
            {
                m_instance = new ClassLoader();
            }
            return m_instance;
        }

        [MethodImpl(0x8000)]
        public object CreateInstance(TypeDNName typeDNName, params object[] args)
        {
            DelegateBridge bridge = __Hotfix_CreateInstance;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp52(this, typeDNName, args);
            }
            System.Type type = this.LoadType(typeDNName);
            if (type == null)
            {
                return null;
            }
            return Activator.CreateInstance(type, args);
        }

        [MethodImpl(0x8000)]
        public System.Type LoadType(TypeDNName typeDNName)
        {
            System.Type type;
            DelegateBridge bridge = __Hotfix_LoadType;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp53(this, typeDNName);
            }
            if (!this.m_typeDict.TryGetValue(typeDNName.m_typeFullName, out type))
            {
                Assembly assembly;
                if (string.IsNullOrEmpty(typeDNName.m_assemblyName))
                {
                    assembly = this.m_assembleDict["Assembly-CSharp"];
                }
                else if (!this.m_assembleDict.TryGetValue(typeDNName.m_assemblyName, out assembly))
                {
                    return null;
                }
                type = assembly.GetType(typeDNName.m_typeFullName);
                if (type == null)
                {
                    Debug.LogError($"Can not find type {typeDNName.m_typeFullName}");
                    return null;
                }
                this.m_typeDict[typeDNName.m_typeFullName] = type;
            }
            return type;
        }

        public static ClassLoader Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp51());
            }
        }
    }
}

