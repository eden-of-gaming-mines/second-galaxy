﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Runtime.CompilerServices;

    internal static class DictionaryExtensions
    {
        private static DelegateBridge __Hotfix_Merge;
        private static DelegateBridge __Hotfix_MergeStringKeys;
        private static DelegateBridge __Hotfix_ToStringFull;
        private static DelegateBridge __Hotfix_StripToStringKeys;
        private static DelegateBridge __Hotfix_StripKeysWithNullValues;

        [MethodImpl(0x8000)]
        public static void Merge(this IDictionary target, IDictionary addHash)
        {
        }

        [MethodImpl(0x8000)]
        public static void MergeStringKeys(this IDictionary target, IDictionary addHash)
        {
        }

        [MethodImpl(0x8000)]
        public static void StripKeysWithNullValues(this IDictionary original)
        {
        }

        [MethodImpl(0x8000)]
        public static Hashtable StripToStringKeys(this IDictionary original)
        {
        }

        [MethodImpl(0x8000)]
        public static string ToStringFull(this IDictionary dict)
        {
        }
    }
}

