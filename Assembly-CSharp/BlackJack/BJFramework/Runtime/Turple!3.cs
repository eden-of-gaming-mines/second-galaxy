﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class Turple<T1, T2, T3>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private T1 <Elem1>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private T2 <Elem2>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private T3 <Elem3>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Elem1;
        private static DelegateBridge __Hotfix_set_Elem1;
        private static DelegateBridge __Hotfix_get_Elem2;
        private static DelegateBridge __Hotfix_set_Elem2;
        private static DelegateBridge __Hotfix_get_Elem3;
        private static DelegateBridge __Hotfix_set_Elem3;

        [MethodImpl(0x8000)]
        public Turple(T1 elem1, T2 elem2, T3 elem3)
        {
        }

        public T1 Elem1
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public T2 Elem2
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public T3 Elem3
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

