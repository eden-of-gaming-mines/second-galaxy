﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DynamicAssemblySettings
    {
        public bool EnableDynamicAssembly;
        public List<string> DynamicAssemblyList = new List<string>();
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public DynamicAssemblySettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

