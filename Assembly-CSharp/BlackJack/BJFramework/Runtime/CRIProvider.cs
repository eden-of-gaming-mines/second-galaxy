﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CRIProvider
    {
        private CriAtomSource m_criBackGroundSource;
        private CriAtomSource m_criSoundSource;
        private CriAtomSource m_criSpeechSource;
        public string m_criFilePrefix;
        private Dictionary<string, string> m_sheetName2PathPrefixDict = new Dictionary<string, string>();
        private List<SheetRef> m_removeSheetRefList = new List<SheetRef>();
        private List<CRIDesc.SheetDesc> m_removeSheetDescList = new List<CRIDesc.SheetDesc>();
        private readonly CRIDesc m_criDesc = new CRIDesc();
        private readonly List<SheetRef> m_sheetRefList = new List<SheetRef>();
        private readonly List<SheetRef> m_deadSheetRefList = new List<SheetRef>();
        private readonly Dictionary<CriAtomSource, Dictionary<string, CriPlayBackInfo>> m_soundPlaybackDict = new Dictionary<CriAtomSource, Dictionary<string, CriPlayBackInfo>>();
        private readonly Dictionary<string, CriPlayBackInfo> m_speechPlaybackDict = new Dictionary<string, CriPlayBackInfo>();
        private readonly Dictionary<string, string> m_cueNameSheetNameMappingDict = new Dictionary<string, string>();
        private readonly List<string> m_removePlayBackInfoNameList = new List<string>();
        private readonly List<CriAtomSource> m_removePlayBackInfoList = new List<CriAtomSource>();
        protected List<CriAtomExPlayback> m_removePlaybackList = new List<CriAtomExPlayback>();
        protected DateTime m_removeStopPlayBackOutTime = DateTime.MinValue;
        protected const float RemoveStopPlayBackDelayTime = 5f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetCRIManagerObject;
        private static DelegateBridge __Hotfix_InitAssetPath;
        private static DelegateBridge __Hotfix_CriRegisterAcf;
        private static DelegateBridge __Hotfix_AddCRIComponentSources;
        private static DelegateBridge __Hotfix_AllocSheetByCueName;
        private static DelegateBridge __Hotfix_CRIRemoveCueSheet;
        private static DelegateBridge __Hotfix_CRIAddCueSheet;
        private static DelegateBridge __Hotfix_GetCueLength;
        private static DelegateBridge __Hotfix_CRISetCategoryVolume;
        private static DelegateBridge __Hotfix_CRISetCategoryMuteState;
        private static DelegateBridge __Hotfix_GetBackGroundCueName;
        private static DelegateBridge __Hotfix_SetBackGroundCueName;
        private static DelegateBridge __Hotfix_PlayBackGroundMusic;
        private static DelegateBridge __Hotfix_StopBackGroundMusic;
        private static DelegateBridge __Hotfix_PlaySound;
        private static DelegateBridge __Hotfix_PlaySoundIndependently;
        private static DelegateBridge __Hotfix_PlaySoundDodgeSame;
        private static DelegateBridge __Hotfix_PlaySoundReplaceSame;
        private static DelegateBridge __Hotfix_PlaySoundImp;
        private static DelegateBridge __Hotfix_StopSound_1;
        private static DelegateBridge __Hotfix_StopSound_0;
        private static DelegateBridge __Hotfix_IsSoundPlaying;
        private static DelegateBridge __Hotfix_IsAnySoundPlaying;
        private static DelegateBridge __Hotfix_IsBackGroundMusicPlaying;
        private static DelegateBridge __Hotfix_PlaySpeech;
        private static DelegateBridge __Hotfix_StopSpeech;
        private static DelegateBridge __Hotfix_IsSpeechPlaying;
        private static DelegateBridge __Hotfix_IsAnySpeechPlaying;
        private static DelegateBridge __Hotfix_IsSheetStopped;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_SetSoundParameter;
        private static DelegateBridge __Hotfix_GetOrCreateSheetName;
        private static DelegateBridge __Hotfix_ClearMultiLanuageSheetDesc;
        private static DelegateBridge __Hotfix_GetCRIResourcePath;
        private static DelegateBridge __Hotfix_GetSheetName;
        private static DelegateBridge __Hotfix_GetSheetDesc;
        private static DelegateBridge __Hotfix_GetSheetDescImpl;
        private static DelegateBridge __Hotfix_GetSheetRef;
        private static DelegateBridge __Hotfix_TickSheetRef;
        private static DelegateBridge __Hotfix_FreeSheetByCueName;
        private static DelegateBridge __Hotfix_GetCueInfo;
        private static DelegateBridge __Hotfix_GetPlaybackCacheName;
        private static DelegateBridge __Hotfix_CheckPlayBackInfoDictStatus_1;
        private static DelegateBridge __Hotfix_CheckPlayBackInfoDictStatus_0;

        [MethodImpl(0x8000)]
        public CRIProvider()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void AddCRIComponentSources()
        {
        }

        [MethodImpl(0x8000)]
        public bool AllocSheetByCueName(string cueName, bool isShortSound, string customPathParam = null)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckPlayBackInfoDictStatus(Dictionary<CriAtomSource, Dictionary<string, CriPlayBackInfo>> playBackInfos)
        {
            DelegateBridge bridge = __Hotfix_CheckPlayBackInfoDictStatus_0;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, playBackInfos);
            }
            else
            {
                this.m_removePlayBackInfoList.Clear();
                foreach (KeyValuePair<CriAtomSource, Dictionary<string, CriPlayBackInfo>> pair in playBackInfos)
                {
                    this.m_removePlayBackInfoNameList.Clear();
                    foreach (KeyValuePair<string, CriPlayBackInfo> pair2 in pair.Value)
                    {
                        if (!pair2.Value.CheckStatus(this.m_removePlaybackList))
                        {
                            this.m_removePlayBackInfoNameList.Add(pair2.Key);
                        }
                    }
                    foreach (string str in this.m_removePlayBackInfoNameList)
                    {
                        pair.Value.Remove(str);
                    }
                    if (pair.Value.Count == 0)
                    {
                        this.m_removePlayBackInfoList.Add(pair.Key);
                    }
                }
                foreach (CriAtomSource source in this.m_removePlayBackInfoList)
                {
                    playBackInfos.Remove(source);
                }
                this.m_removePlayBackInfoNameList.Clear();
                this.m_removePlayBackInfoList.Clear();
            }
        }

        [MethodImpl(0x8000)]
        private void CheckPlayBackInfoDictStatus(Dictionary<string, CriPlayBackInfo> playBackInfos)
        {
            DelegateBridge bridge = __Hotfix_CheckPlayBackInfoDictStatus_1;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, playBackInfos);
            }
            else
            {
                this.m_removePlayBackInfoNameList.Clear();
                foreach (KeyValuePair<string, CriPlayBackInfo> pair in playBackInfos)
                {
                    if (!pair.Value.CheckStatus(this.m_removePlaybackList))
                    {
                        this.m_removePlayBackInfoNameList.Add(pair.Key);
                    }
                }
                foreach (string str in this.m_removePlayBackInfoNameList)
                {
                    playBackInfos.Remove(str);
                }
                this.m_removePlayBackInfoNameList.Clear();
            }
        }

        [MethodImpl(0x8000)]
        public void ClearMultiLanuageSheetDesc()
        {
        }

        [MethodImpl(0x8000)]
        public void CRIAddCueSheet(string sheetName, string acbFullPath, string awbFullPath)
        {
        }

        [MethodImpl(0x8000)]
        public void CriRegisterAcf(string acfFullPath)
        {
        }

        [MethodImpl(0x8000)]
        public void CRIRemoveCueSheet(string sheetName)
        {
        }

        [MethodImpl(0x8000)]
        public void CRISetCategoryMuteState(string category, bool isMute)
        {
        }

        [MethodImpl(0x8000)]
        public void CRISetCategoryVolume(string categroy, float volume)
        {
        }

        [MethodImpl(0x8000)]
        private void FreeSheetByCueName(string cueName, bool isShortSound)
        {
        }

        [MethodImpl(0x8000)]
        public string GetBackGroundCueName()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetCRIManagerObject()
        {
        }

        [MethodImpl(0x8000)]
        private string GetCRIResourcePath(string basePath, out string realFilePath)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetCueInfo(string sheetName, string cueName, out CriAtomEx.CueInfo cueInfo)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCueLength(string sheetName, string cueName)
        {
        }

        [MethodImpl(0x8000)]
        public string GetOrCreateSheetName(string cueName)
        {
        }

        [MethodImpl(0x8000)]
        private string GetPlaybackCacheName(string cueName, CriAtomSource source)
        {
        }

        [MethodImpl(0x8000)]
        private CRIDesc.SheetDesc GetSheetDesc(string sheetName, string customPathParam = null)
        {
        }

        [MethodImpl(0x8000)]
        private CRIDesc.SheetDesc GetSheetDescImpl(string sheetName)
        {
        }

        [MethodImpl(0x8000)]
        private string GetSheetName(string cueName)
        {
        }

        [MethodImpl(0x8000)]
        private SheetRef GetSheetRef(string sheetName)
        {
        }

        [MethodImpl(0x8000)]
        public void InitAssetPath(string criFilePrefix, Dictionary<string, string> configDataDict)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnySoundPlaying(CriAtomSource criSoundSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnySpeechPlaying()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBackGroundMusicPlaying()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSheetStopped(string sheetName)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSoundPlaying(string sound, CriAtomSource criSoundSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSpeechPlaying(string sound)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayBackGroundMusic(string music)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySound(string sound, PlaySoundOption playOption, bool loop = false, CriAtomSource criSoundSource = null, bool isMultiLanguage = true)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlaySoundDodgeSame(string sound, bool loop = false, CriAtomSource criSoundSource = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlaySoundImp(string music, CriAtomSource criSoundSource, bool needCache = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlaySoundIndependently(string sound, bool loop = false, CriAtomSource criSoundSource = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlaySoundReplaceSame(string sound, bool loop = false, CriAtomSource criSoundSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySpeech(string music, string customPathParam = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackGroundCueName(string cueName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundParameter(string paraName, float value)
        {
        }

        [MethodImpl(0x8000)]
        public void StopBackGroundMusic()
        {
        }

        [MethodImpl(0x8000)]
        public void StopSound(string music)
        {
        }

        [MethodImpl(0x8000)]
        public void StopSound(string music, CriAtomSource criSoundSource, int stopCount = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void StopSpeech(string music)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                if (this.m_removeStopPlayBackOutTime < Timer.m_currTime)
                {
                    this.m_removeStopPlayBackOutTime = Timer.m_currTime.AddSeconds(5.0);
                    this.CheckPlayBackInfoDictStatus(this.m_soundPlaybackDict);
                    this.CheckPlayBackInfoDictStatus(this.m_speechPlaybackDict);
                }
                this.TickSheetRef();
            }
        }

        [MethodImpl(0x8000)]
        private void TickSheetRef()
        {
            DelegateBridge bridge = __Hotfix_TickSheetRef;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                foreach (SheetRef ref2 in this.m_sheetRefList)
                {
                    if (ref2.IsDead() && this.IsSheetStopped(ref2.m_sheetName))
                    {
                        this.m_deadSheetRefList.Add(ref2);
                        continue;
                    }
                    if (ref2.m_liveTime > 0f)
                    {
                        ref2.DecreaseTime(Time.deltaTime);
                    }
                }
                foreach (SheetRef ref3 in this.m_deadSheetRefList)
                {
                    this.m_sheetRefList.Remove(ref3);
                    this.CRIRemoveCueSheet(ref3.m_sheetName);
                }
                this.m_deadSheetRefList.Clear();
            }
        }
    }
}

