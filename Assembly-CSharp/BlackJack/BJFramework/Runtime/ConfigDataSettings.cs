﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class ConfigDataSettings
    {
        public string ConfigDataLoaderTypeDNName = "Assembly-CSharp@BlackJack.ConfigData.ClientConfigDataLoader";
        public bool ConfigDataAssetAllowNullSetting = true;
        public string StringTableManagerTypeDNName;
        public bool ConfigDataAllowMD5NotMach;
        public string ConfigDataAssetTargeRoot = "Assets/GameProject/RuntimeAssets/ConfigData";
        [Header("初始化的线程个数")]
        public int InitThreadCount = 4;
        [Header("初始化时加载多少个yeildreturn一次")]
        public int InitloadCountForSingleYield = 20;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetConfigDataAssetTargetPath;
        private static DelegateBridge __Hotfix_GetConfigDataAssetPathNoPostfix;
        private static DelegateBridge __Hotfix_GetConfigDataAssetPath;

        [MethodImpl(0x8000)]
        public ConfigDataSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public string GetConfigDataAssetPath(string directoryName, string fileName)
        {
        }

        [MethodImpl(0x8000)]
        public string GetConfigDataAssetPathNoPostfix(string directoryName, string fileName)
        {
        }

        [MethodImpl(0x8000)]
        public string GetConfigDataAssetTargetPath()
        {
        }
    }
}

