﻿namespace BlackJack.BJFramework.Runtime
{
    using System;

    public interface ITickable
    {
        void Tick();
    }
}

