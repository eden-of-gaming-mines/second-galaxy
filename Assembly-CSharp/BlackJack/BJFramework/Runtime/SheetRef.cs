﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SheetRef
    {
        public string m_sheetName;
        public uint m_refNum;
        public float m_liveTime;
        private const float LiveTimePeriod = 30f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsDead;
        private static DelegateBridge __Hotfix_AddTime;
        private static DelegateBridge __Hotfix_DecreaseTime;
        private static DelegateBridge __Hotfix_AddRef;
        private static DelegateBridge __Hotfix_ReleaseRef;
        private static DelegateBridge __Hotfix_AddRefOrTime;

        [MethodImpl(0x8000)]
        public SheetRef(string sheetName)
        {
        }

        [MethodImpl(0x8000)]
        public void AddRef()
        {
        }

        [MethodImpl(0x8000)]
        public void AddRefOrTime(bool isShortSound)
        {
        }

        [MethodImpl(0x8000)]
        public void AddTime()
        {
        }

        [MethodImpl(0x8000)]
        public void DecreaseTime(float deltaTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDead()
        {
        }

        [MethodImpl(0x8000)]
        public void ReleaseRef()
        {
        }
    }
}

