﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class PrefabControllerBase : MonoBehaviour
    {
        protected bool m_inited;
        protected string m_ctrlName;
        public PrefabResourceContainer m_resContainer;
        protected PrefabControllerNextUpdateExecutor m_nextUpdateExecutor = new PrefabControllerNextUpdateExecutor();
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_GetAssetInContainer_0;
        private static DelegateBridge __Hotfix_GetAssetInContainer_1;
        private static DelegateBridge __Hotfix_BindResContainer;
        private static DelegateBridge __Hotfix_BindFields;
        private static DelegateBridge __Hotfix_GetNextUpdateExecutor;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_BindFieldImpl;
        private static DelegateBridge __Hotfix_GetChildByPath;
        private static DelegateBridge __Hotfix_AddControllerToGameObject;
        private static DelegateBridge __Hotfix_get_CtrlName;

        [MethodImpl(0x8000)]
        public static PrefabControllerBase AddControllerToGameObject(GameObject root, string path, TypeDNName ctrlTypeDNName, string ctrlName, string luaModuleName = null, bool autoBind = false)
        {
            PrefabControllerBase component;
            DelegateBridge bridge = __Hotfix_AddControllerToGameObject;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp117(root, path, ctrlTypeDNName, ctrlName, luaModuleName, autoBind);
            }
            System.Type type = ClassLoader.Instance.LoadType(ctrlTypeDNName);
            if (type == null)
            {
                Debug.LogError($"AddControllerToGameObject fail for {ctrlTypeDNName.m_typeFullName}");
                return null;
            }
            int index = path.IndexOf('/');
            if (index == -1)
            {
                if (!root.name.Contains(path))
                {
                    Debug.LogError($"AddControllerToGameObject fail path error {path}");
                    return null;
                }
                component = root.GetComponent(type) as PrefabControllerBase;
                if (component == null)
                {
                    component = root.AddComponent(type) as PrefabControllerBase;
                    if (component != null)
                    {
                        component.Initlize(ctrlName, autoBind);
                    }
                }
                return component;
            }
            string name = path.Substring(index + 1);
            Transform transform = root.transform.Find(name);
            if (transform == null)
            {
                Debug.LogError($"AddControllerToGameObject fail path error {path}");
                return null;
            }
            GameObject gameObject = transform.gameObject;
            component = gameObject.GetComponent(type) as PrefabControllerBase;
            if (component == null)
            {
                component = gameObject.AddComponent(type) as PrefabControllerBase;
                if (component != null)
                {
                    component.Initlize(ctrlName, autoBind);
                }
            }
            return component;
        }

        [MethodImpl(0x8000)]
        protected virtual UnityEngine.Object BindFieldImpl(System.Type fieldType, string path, AutoBindAttribute.InitState initState, string fieldName, string ctrlName = null, bool optional = false)
        {
            DelegateBridge bridge = __Hotfix_BindFieldImpl;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp115(this, fieldType, path, initState, fieldName, ctrlName, optional);
            }
            UnityEngine.Object obj2 = null;
            GameObject childByPath = this.GetChildByPath(path);
            if (childByPath == null)
            {
                if (!optional)
                {
                    Debug.LogError($"BindFields fail can not found child {path} in {base.gameObject} {base.GetType().Name}");
                }
                return null;
            }
            if (ReferenceEquals(fieldType, typeof(GameObject)))
            {
                obj2 = childByPath;
            }
            else if (fieldType.IsSubclassOf(typeof(PrefabControllerBase)))
            {
                PrefabControllerBase component = childByPath.GetComponent(fieldType) as PrefabControllerBase;
                if (component == null)
                {
                    if (!optional)
                    {
                        Debug.LogError($"BindFields fail can not found comp in child {path} {fieldType.Name}");
                    }
                    return null;
                }
                obj2 = component;
            }
            else if (fieldType.IsSubclassOf(typeof(Component)))
            {
                Component component = childByPath.GetComponent(fieldType);
                if (component == null)
                {
                    if (!optional)
                    {
                        Debug.LogError($"BindFields fail can not found comp in child {path} {fieldType.Name}");
                    }
                    return null;
                }
                obj2 = component;
            }
            if (initState == AutoBindAttribute.InitState.Active)
            {
                childByPath.SetActive(true);
            }
            else if (initState == AutoBindAttribute.InitState.Inactive)
            {
                childByPath.SetActive(false);
            }
            return obj2;
        }

        [MethodImpl(0x8000)]
        public virtual void BindFields()
        {
            DelegateBridge bridge = __Hotfix_BindFields;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                foreach (FieldInfo info in base.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                {
                    object[] customAttributes = info.GetCustomAttributes(typeof(AutoBindAttribute), false);
                    if (customAttributes.Length != 0)
                    {
                        AutoBindAttribute attribute = null;
                        object[] objArray2 = customAttributes;
                        int index = 0;
                        while (true)
                        {
                            if (index < objArray2.Length)
                            {
                                object obj2 = objArray2[index];
                                if (!(obj2 is AutoBindAttribute))
                                {
                                    index++;
                                    continue;
                                }
                                attribute = obj2 as AutoBindAttribute;
                            }
                            if (attribute != null)
                            {
                                bool optional = attribute.m_optional;
                                System.Type fieldType = info.FieldType;
                                UnityEngine.Object obj3 = this.BindFieldImpl(fieldType, attribute.m_path, attribute.m_initState, info.Name, fieldType.Name, optional);
                                if (obj3 != null)
                                {
                                    info.SetValue(this, obj3);
                                }
                            }
                            break;
                        }
                    }
                }
                this.OnBindFiledsCompleted();
            }
        }

        [MethodImpl(0x8000)]
        public void BindResContainer()
        {
            DelegateBridge bridge = __Hotfix_BindResContainer;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                this.m_resContainer = base.GetComponent<PrefabResourceContainer>();
            }
        }

        [MethodImpl(0x8000)]
        public UnityEngine.Object GetAssetInContainer(string resName)
        {
        }

        [MethodImpl(0x8000)]
        public T GetAssetInContainer<T>(string resName) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetChildByPath(string path)
        {
            DelegateBridge bridge = __Hotfix_GetChildByPath;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp116(this, path);
            }
            int index = path.LastIndexOf("../");
            Transform transform = null;
            if (index == -1)
            {
                index = path.IndexOf('/');
                if (index == -1)
                {
                    return base.gameObject;
                }
                string name = path.Substring(index + 1);
                transform = base.transform.Find(name);
            }
            else
            {
                Transform parent = base.transform;
                int num2 = index / 3;
                while (true)
                {
                    if (num2 < 0)
                    {
                        transform = parent.Find(path.Substring(index + 3));
                        break;
                    }
                    num2--;
                    parent = parent.parent;
                }
            }
            return transform?.gameObject;
        }

        [MethodImpl(0x8000)]
        public PrefabControllerNextUpdateExecutor GetNextUpdateExecutor()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Initlize(string ctrlName, bool bindNow)
        {
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp12(this, ctrlName, bindNow);
            }
            else if (!this.m_inited)
            {
                this.m_inited = true;
                this.m_ctrlName = ctrlName;
                if (bindNow)
                {
                    this.BindFields();
                }
                this.BindResContainer();
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBindFiledsCompleted()
        {
            DelegateBridge bridge = __Hotfix_OnBindFiledsCompleted;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        public string CtrlName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

