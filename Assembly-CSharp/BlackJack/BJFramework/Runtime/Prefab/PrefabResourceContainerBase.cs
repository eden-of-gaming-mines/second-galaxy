﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;

    public abstract class PrefabResourceContainerBase : MonoBehaviour
    {
        public List<AssetCacheItem> AssetList;
        public List<AssetCacheItem> m_assetCacheList;
        protected Dictionary<string, UnityEngine.Object> m_rawAssetCacheDict;
        protected bool m_isRecachedAllAssets;
        private static DelegateBridge __Hotfix_TryGetAsset;
        private static DelegateBridge __Hotfix_GetAsset_1;
        private static DelegateBridge __Hotfix_GetAsset_0;
        private static DelegateBridge __Hotfix_GetAssetAsync;
        private static DelegateBridge __Hotfix_StartLazyLoadBackground_0;
        private static DelegateBridge __Hotfix_StartLazyLoadBackground_1;
        private static DelegateBridge __Hotfix_RecacheAllAssetsFromResourceManager;
        private static DelegateBridge __Hotfix_LoadAndCacheAsset;
        private static DelegateBridge __Hotfix_GetCacheItemFromList;

        [MethodImpl(0x8000)]
        protected PrefabResourceContainerBase()
        {
        }

        [MethodImpl(0x8000)]
        public UnityEngine.Object GetAsset(string assetName)
        {
        }

        [MethodImpl(0x8000)]
        public T GetAsset<T>(string assetName) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable GetAssetAsync(string assetName, Action<UnityEngine.Object> onEnd, bool isLoadAssetAsync = false)
        {
        }

        [MethodImpl(0x8000)]
        private AssetCacheItem GetCacheItemFromList(string assetName)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadAndCacheAsset(HashSet<string> assetPathSet, bool isLoadAssetAsync)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator RecacheAllAssetsFromResourceManager()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable StartLazyLoadBackground(bool isLoadAssetAsync = true)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable StartLazyLoadBackground(List<string> assetNames, bool isLoadAssetAsync = true)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetAsset(string assetName, out UnityEngine.Object asset)
        {
        }

        [CompilerGenerated]
        private sealed class <GetAssetAsync>c__Iterator0 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string assetName;
            internal Action<UnityEngine.Object> onEnd;
            internal PrefabResourceContainerBase.AssetCacheItem <item>__0;
            internal bool isLoadAssetAsync;
            internal IEnumerator <iter>__1;
            internal PrefabResourceContainerBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <GetAssetAsync>c__AnonStorey5 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <GetAssetAsync>c__AnonStorey5();
                        this.$locvar0.<>f__ref$0 = this;
                        if (!this.$this.TryGetAsset(this.assetName, out this.$locvar0.asset))
                        {
                            this.<item>__0 = this.$this.GetCacheItemFromList(this.assetName);
                            if (this.<item>__0 != null)
                            {
                                if (!Application.isEditor || GameManager.Instance.GameClientSetting.ResourcesSetting.LoadAssetFromBundleInEditor)
                                {
                                    this.<iter>__1 = ResourceManager.Instance.LoadAsset<UnityEngine.Object>(this.<item>__0.AssetPath, new Action<string, UnityEngine.Object>(this.$locvar0.<>m__0), false, this.isLoadAssetAsync);
                                    break;
                                }
                                this.onEnd(this.<item>__0.Asset);
                                goto TR_0003;
                            }
                            else
                            {
                                this.onEnd(null);
                            }
                        }
                        else
                        {
                            this.onEnd(this.$locvar0.asset);
                        }
                        goto TR_0000;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__1.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.<item>__0.RuntimeAssetCache = this.$locvar0.asset;
                this.$this.m_assetCacheList.Add(this.<item>__0);
                this.$this.m_rawAssetCacheDict[this.<item>__0.AssetPath] = this.$locvar0.asset;
                this.onEnd(this.$locvar0.asset);
                goto TR_0003;
            TR_0000:
                return false;
            TR_0003:
                this.$PC = -1;
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<object> IEnumerable<object>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new PrefabResourceContainerBase.<GetAssetAsync>c__Iterator0 { 
                    $this = this.$this,
                    assetName = this.assetName,
                    onEnd = this.onEnd,
                    isLoadAssetAsync = this.isLoadAssetAsync
                };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <GetAssetAsync>c__AnonStorey5
            {
                internal UnityEngine.Object asset;
                internal PrefabResourceContainerBase.<GetAssetAsync>c__Iterator0 <>f__ref$0;

                internal void <>m__0(string lPath, UnityEngine.Object lasset)
                {
                    this.asset = lasset;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAndCacheAsset>c__Iterator4 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal HashSet<string> assetPathSet;
            internal bool isLoadAssetAsync;
            internal List<PrefabResourceContainerBase.AssetCacheItem>.Enumerator $locvar0;
            internal PrefabResourceContainerBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadAndCacheAsset>c__AnonStorey6 $locvar1;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar1 = new <LoadAndCacheAsset>c__AnonStorey6();
                        this.$locvar1.<>f__ref$4 = this;
                        if (this.assetPathSet == null)
                        {
                            goto TR_0000;
                        }
                        else if (this.assetPathSet.Count != 0)
                        {
                            this.$locvar1.isLoadAssetsEnd = false;
                            ResourceManager.Instance.StartLoadAssetsCorutine(this.assetPathSet, this.$this.m_rawAssetCacheDict, new Action(this.$locvar1.<>m__0), this.isLoadAssetAsync);
                        }
                        else
                        {
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (!this.$locvar1.isLoadAssetsEnd)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$locvar0 = this.$this.AssetList.GetEnumerator();
                try
                {
                    while (this.$locvar0.MoveNext())
                    {
                        UnityEngine.Object obj2;
                        PrefabResourceContainerBase.AssetCacheItem current = this.$locvar0.Current;
                        if (!string.IsNullOrEmpty(current.AssetPath) && (!current.LazyLoad && this.$this.m_rawAssetCacheDict.TryGetValue(current.AssetPath, out obj2)))
                        {
                            current.RuntimeAssetCache = obj2;
                            this.$this.m_assetCacheList.Add(current);
                            if (current.IsAssetBundleNotUnload)
                            {
                                ResourceManager.Instance.MakeAssetBundleDontUnload(current.AssetPath);
                            }
                        }
                    }
                }
                finally
                {
                    this.$locvar0.Dispose();
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadAndCacheAsset>c__AnonStorey6
            {
                internal bool isLoadAssetsEnd;
                internal PrefabResourceContainerBase.<LoadAndCacheAsset>c__Iterator4 <>f__ref$4;

                internal void <>m__0()
                {
                    this.isLoadAssetsEnd = true;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RecacheAllAssetsFromResourceManager>c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal HashSet<string> <assetPathSet>__0;
            internal List<PrefabResourceContainerBase.AssetCacheItem>.Enumerator $locvar0;
            internal IEnumerator <iter>__0;
            internal PrefabResourceContainerBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (!this.$this.m_isRecachedAllAssets)
                        {
                            this.$this.m_isRecachedAllAssets = true;
                            this.$this.m_assetCacheList = new List<PrefabResourceContainerBase.AssetCacheItem>();
                            this.$this.m_rawAssetCacheDict = new Dictionary<string, UnityEngine.Object>();
                            this.<assetPathSet>__0 = new HashSet<string>();
                            this.$locvar0 = this.$this.AssetList.GetEnumerator();
                            try
                            {
                                while (this.$locvar0.MoveNext())
                                {
                                    PrefabResourceContainerBase.AssetCacheItem current = this.$locvar0.Current;
                                    if (!string.IsNullOrEmpty(current.AssetPath) && !current.LazyLoad)
                                    {
                                        this.<assetPathSet>__0.Add(current.AssetPath);
                                    }
                                }
                            }
                            finally
                            {
                                this.$locvar0.Dispose();
                            }
                            this.<iter>__0 = this.$this.LoadAndCacheAsset(this.<assetPathSet>__0, false);
                        }
                        else
                        {
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__0.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <StartLazyLoadBackground>c__Iterator1 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
        {
            internal HashSet<string> <assetPathSet>__0;
            internal List<PrefabResourceContainerBase.AssetCacheItem>.Enumerator $locvar0;
            internal bool isLoadAssetAsync;
            internal IEnumerator <iter>__0;
            internal PrefabResourceContainerBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<assetPathSet>__0 = new HashSet<string>();
                        this.$locvar0 = this.$this.AssetList.GetEnumerator();
                        try
                        {
                            while (this.$locvar0.MoveNext())
                            {
                                PrefabResourceContainerBase.AssetCacheItem current = this.$locvar0.Current;
                                if (current.LazyLoad && ((current.RuntimeAssetCache == null) && !string.IsNullOrEmpty(current.AssetPath)))
                                {
                                    this.<assetPathSet>__0.Add(current.AssetPath);
                                }
                            }
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        this.<iter>__0 = this.$this.LoadAndCacheAsset(this.<assetPathSet>__0, this.isLoadAssetAsync);
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__0.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<object> IEnumerable<object>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new PrefabResourceContainerBase.<StartLazyLoadBackground>c__Iterator1 { 
                    $this = this.$this,
                    isLoadAssetAsync = this.isLoadAssetAsync
                };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <StartLazyLoadBackground>c__Iterator2 : IEnumerable, IEnumerable<object>, IEnumerator, IDisposable, IEnumerator<object>
        {
            internal HashSet<string> <assetPathSet>__0;
            internal List<PrefabResourceContainerBase.AssetCacheItem>.Enumerator $locvar0;
            internal List<string> assetNames;
            internal bool isLoadAssetAsync;
            internal IEnumerator <iter>__0;
            internal PrefabResourceContainerBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<assetPathSet>__0 = new HashSet<string>();
                        this.$locvar0 = this.$this.AssetList.GetEnumerator();
                        try
                        {
                            while (this.$locvar0.MoveNext())
                            {
                                PrefabResourceContainerBase.AssetCacheItem current = this.$locvar0.Current;
                                if (current.LazyLoad && ((current.RuntimeAssetCache == null) && (!string.IsNullOrEmpty(current.AssetPath) && this.assetNames.Contains(current.Name))))
                                {
                                    this.<assetPathSet>__0.Add(current.AssetPath);
                                }
                            }
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        this.<iter>__0 = this.$this.LoadAndCacheAsset(this.<assetPathSet>__0, this.isLoadAssetAsync);
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__0.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<object> IEnumerable<object>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new PrefabResourceContainerBase.<StartLazyLoadBackground>c__Iterator2 { 
                    $this = this.$this,
                    assetNames = this.assetNames,
                    isLoadAssetAsync = this.isLoadAssetAsync
                };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<object>.GetEnumerator();

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [Serializable]
        public class AssetCacheItem
        {
            public string Name;
            [Header("[不要手动设置!]")]
            public string AssetPath;
            public UnityEngine.Object Asset;
            [Header("[是否延时加载!]")]
            public bool LazyLoad;
            [Header("[不要手动设置!]")]
            public bool IsAssetBundleNotUnload;
            [Header("[不要手动设置!]")]
            public UnityEngine.Object RuntimeAssetCache;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

