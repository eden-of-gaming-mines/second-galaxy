﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [AttributeUsage(AttributeTargets.Field)]
    public class AutoBindAttribute : Attribute
    {
        public readonly string m_path;
        public readonly InitState m_initState;
        public readonly bool m_optional;

        [MethodImpl(0x8000)]
        public AutoBindAttribute(string path, InitState initState = 0, bool optional = false)
        {
            this.m_path = path;
            this.m_initState = initState;
            this.m_optional = optional;
            if (path.Contains("../"))
            {
                throw new Exception($"AutoBind不能使用../方式，如果必须请用AutoBindWithParent path:{path}");
            }
        }

        [MethodImpl(0x8000)]
        protected AutoBindAttribute(string path, bool withParent, InitState initState = 0, bool optional = false)
        {
        }

        public enum InitState
        {
            NotInit,
            Active,
            Inactive
        }
    }
}

