﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using BlackJack.BJFramework.Runtime;
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct ControllerDesc
    {
        public string m_ctrlName;
        public TypeDNName m_ctrlTypeDNName;
        public string m_luaModuleName;
        public string m_attachPath;
    }
}

