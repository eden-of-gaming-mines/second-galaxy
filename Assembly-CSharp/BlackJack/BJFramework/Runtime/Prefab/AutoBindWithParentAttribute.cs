﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using System;
    using System.Runtime.InteropServices;

    [AttributeUsage(AttributeTargets.Field)]
    public class AutoBindWithParentAttribute : AutoBindAttribute
    {
        public AutoBindWithParentAttribute(string path, AutoBindAttribute.InitState initState = 0, bool optional = false) : base(path, true, initState, optional)
        {
        }
    }
}

