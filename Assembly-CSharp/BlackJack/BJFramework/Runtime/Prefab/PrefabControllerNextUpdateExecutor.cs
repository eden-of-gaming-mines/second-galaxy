﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class PrefabControllerNextUpdateExecutor
    {
        private List<Action> m_nextUpdateExecutionList = new List<Action>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddNextUpdateExecution;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public PrefabControllerNextUpdateExecutor()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public void AddNextUpdateExecution(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }
    }
}

