﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using System;

    [AttributeUsage(AttributeTargets.Field)]
    public class CacheAssetAttribute : Attribute
    {
    }
}

