﻿namespace BlackJack.BJFramework.Runtime.Prefab
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class PrefabControllerCreater : MonoBehaviour
    {
        public List<ControllerDesc> CtrlDescList;
        public List<PrefabControllerBase> CtrlList;
        private static DelegateBridge __Hotfix_CreateAllControllers_1;
        private static DelegateBridge __Hotfix_CreateAllControllers_0;
        private static DelegateBridge __Hotfix_GetControllerByName;

        [MethodImpl(0x8000)]
        public bool CreateAllControllers(bool autoBind = true)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CreateAllControllers(GameObject root)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabControllerBase GetControllerByName(string name)
        {
        }
    }
}

