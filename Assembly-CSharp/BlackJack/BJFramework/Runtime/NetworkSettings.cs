﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class NetworkSettings
    {
        [Header("是否在主线程tick中解析协议包")]
        public bool DecodeMessageOnTick = true;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public NetworkSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

