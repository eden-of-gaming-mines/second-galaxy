﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class Util
    {
        private static string m_platform;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private static Func<string> <GetCurrentTargetPlatformInEditor>k__BackingField;
        private static string m_persistentPath;
        private static string m_streamingAssetsPath;
        private static Dictionary<string, string> m_dnNameDict = new Dictionary<string, string>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetCurrentTargetPlatform;
        private static DelegateBridge __Hotfix_get_GetCurrentTargetPlatformInEditor;
        private static DelegateBridge __Hotfix_set_GetCurrentTargetPlatformInEditor;
        private static DelegateBridge __Hotfix_GetFilePersistentPath;
        private static DelegateBridge __Hotfix_GetFileStreamingAssetsPath;
        private static DelegateBridge __Hotfix_GetFileStreamingAssetsPath4WWW;
        private static DelegateBridge __Hotfix_ReformPathString;
        private static DelegateBridge __Hotfix_ReformPathStrings;
        private static DelegateBridge __Hotfix_GetAssetDNNameFromPath;
        private static DelegateBridge __Hotfix_ReplaceLastFolderName;
        private static DelegateBridge __Hotfix_GetBundleNameByAssetPath;
        private static DelegateBridge __Hotfix_GetFileSize;
        private static DelegateBridge __Hotfix_RemoveCloneFromGameObjectName;
        private static DelegateBridge __Hotfix_SetTransformParent;
        private static DelegateBridge __Hotfix_CreateMd5FromFileList;
        private static DelegateBridge __Hotfix_CreateMd5FromFile;
        private static DelegateBridge __Hotfix_CreateMd5FromBytes;
        private static DelegateBridge __Hotfix_TimeSpanToString;
        private static DelegateBridge __Hotfix_GetTypeDefaultValueString;
        private static DelegateBridge __Hotfix_FindType;
        private static DelegateBridge __Hotfix_GetTypeDefinitionString;
        private static DelegateBridge __Hotfix_GetGenericTypeFriendlyName;

        [MethodImpl(0x8000)]
        public static string CreateMd5FromBytes(byte[] bytes)
        {
        }

        [MethodImpl(0x8000)]
        public static string CreateMd5FromFile(string filePath)
        {
        }

        [MethodImpl(0x8000)]
        public static List<string> CreateMd5FromFileList(List<string> fileList)
        {
        }

        [MethodImpl(0x8000)]
        public static System.Type FindType(string typeName)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetAssetDNNameFromPath(string path, string replaceLastFolderNameStr = "", bool autoExtension = true)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetBundleNameByAssetPath(string path, string replaceLastFolderNameStr = "", bool autoExtension = true)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCurrentTargetPlatform()
        {
            DelegateBridge bridge = __Hotfix_GetCurrentTargetPlatform;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp96();
            }
            if (string.IsNullOrEmpty(m_platform))
            {
                if (Application.isEditor)
                {
                    m_platform = (GetCurrentTargetPlatformInEditor == null) ? "Android" : GetCurrentTargetPlatformInEditor();
                }
                else
                {
                    RuntimePlatform platform = Application.platform;
                    if (platform == RuntimePlatform.OSXPlayer)
                    {
                        m_platform = "StandaloneOSXIntel";
                    }
                    else if (platform == RuntimePlatform.WindowsPlayer)
                    {
                        m_platform = (IntPtr.Size != 8) ? "StandaloneWindows" : "StandaloneWindows64";
                    }
                    else
                    {
                        switch (platform)
                        {
                            case RuntimePlatform.IPhonePlayer:
                                m_platform = "iOS";
                                break;

                            case RuntimePlatform.Android:
                                m_platform = "Android";
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
            return m_platform;
        }

        [MethodImpl(0x8000)]
        public static string GetFilePersistentPath(string assetPath)
        {
            DelegateBridge bridge = __Hotfix_GetFilePersistentPath;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp33(assetPath);
            }
            if (m_persistentPath == null)
            {
                m_persistentPath = Application.persistentDataPath + "/";
            }
            return (m_persistentPath + assetPath);
        }

        [MethodImpl(0x8000)]
        public static long GetFileSize(string path)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFileStreamingAssetsPath(string assetPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFileStreamingAssetsPath4WWW(string assetPath)
        {
            DelegateBridge bridge = __Hotfix_GetFileStreamingAssetsPath4WWW;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp33(assetPath);
            }
            if (m_streamingAssetsPath == null)
            {
                m_streamingAssetsPath = Application.streamingAssetsPath + "/";
            }
            return (m_streamingAssetsPath.Contains("://") ? $"{m_streamingAssetsPath}{assetPath}" : $"file://{m_streamingAssetsPath}{assetPath}");
        }

        [MethodImpl(0x8000)]
        public static string GetGenericTypeFriendlyName(System.Type type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTypeDefaultValueString(System.Type type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTypeDefinitionString(System.Type type)
        {
        }

        [MethodImpl(0x8000)]
        public static string ReformPathString(string path)
        {
        }

        [MethodImpl(0x8000)]
        public static List<string> ReformPathStrings(IEnumerable paths)
        {
        }

        [MethodImpl(0x8000)]
        public static void RemoveCloneFromGameObjectName(GameObject go)
        {
            DelegateBridge bridge = __Hotfix_RemoveCloneFromGameObjectName;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(go);
            }
            else if (go.name.EndsWith("(Clone)"))
            {
                go.name = go.name.Replace("(Clone)", string.Empty);
            }
        }

        [MethodImpl(0x8000)]
        private static string ReplaceLastFolderName(string path, string replaceStr)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetTransformParent(Transform child, Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public static string TimeSpanToString(TimeSpan timeSpan)
        {
        }

        public static Func<string> GetCurrentTargetPlatformInEditor
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

