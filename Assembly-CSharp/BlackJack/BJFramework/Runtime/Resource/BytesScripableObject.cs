﻿namespace BlackJack.BJFramework.Runtime.Resource
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BytesScripableObject : ScriptableObject
    {
        public int m_size;
        [SerializeField]
        protected byte[] m_bytes;
        private static DelegateBridge __Hotfix_SetBytes;
        private static DelegateBridge __Hotfix_GetBytes;

        [MethodImpl(0x8000)]
        public byte[] GetBytes()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetBytes(byte[] bytes)
        {
        }
    }
}

