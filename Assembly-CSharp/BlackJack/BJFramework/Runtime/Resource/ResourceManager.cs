﻿namespace BlackJack.BJFramework.Runtime.Resource
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.Utils;
    using Boo.Lang;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Threading;
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class ResourceManager : ITickable
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAssetLoaded;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBundleDataVersionMisMatch;
        protected static BlackJack.BJFramework.Runtime.Resource.ResourceManager m_instance;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private RMState <State>k__BackingField;
        protected TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
        protected string m_currLocalization;
        protected bool m_needGC;
        protected int m_unloadUnusedAssetTimeInterval;
        protected DateTime m_nextUnloadUnusedAssetTime;
        protected DateTime m_nextCheckMemUseageTime;
        protected bool m_assetPathIgnoreCase;
        protected bool m_enableDetailResourceManagerLog;
        protected AsyncOperation m_currUnloadUnusedAssetsOperation;
        protected bool m_disableAssetBundle;
        protected bool m_loadAssetFromBundleInEditor;
        protected bool m_useObb;
        protected Dictionary<string, AssetCacheItem> m_assetsCacheDict = new Dictionary<string, AssetCacheItem>();
        protected System.Collections.Generic.List<AssetCacheItem> m_assetsCacheToRemove = new System.Collections.Generic.List<AssetCacheItem>();
        protected string m_bundleDownloadUrlRoot;
        protected string m_downloadUrlRootWithPlatform;
        protected string m_downloadBundleUrlPrefix;
        protected bool m_needUnloadAllUnusedBundles;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action m_eventOnUnloadUnusedResourceAllCompleted;
        protected bool m_needUnloadUnusedAssets;
        protected int m_loadingOpCount;
        protected HashSet<string> m_loadedAssetPathSet = new HashSet<string>();
        protected DateTime m_nextTickVersionUpdateTime = DateTime.MinValue;
        protected bool m_enableCheckVersionUpdateInRuning;
        protected string m_checkVersionUpdateInRuningUrl;
        protected int m_checkVersionUpdateInRuningCD;
        protected const string StreamingAssetsFileListVersionKey = "StreamingAssetsFileListVersion";
        protected const string BundleDataVersionKey = "BundleDataVersion";
        protected const string PreUpdateVersion4BundleDataKey = "PreUpdateVersion4BundleData";
        protected const string TouchedBundleSetFileName = "TouchedBundleSet";
        protected BundleData m_bundleData;
        protected BundleDataHelper m_bundleDataHelper;
        protected AssetBundleManifest m_assetBundleManifest;
        protected DateTime m_nextTickBundleTime = DateTime.MinValue;
        protected Dictionary<string, BundleCacheItem> m_bundleCacheDict = new Dictionary<string, BundleCacheItem>();
        protected System.Collections.Generic.List<BundleCacheItem> m_bundleCachetoUnload = new System.Collections.Generic.List<BundleCacheItem>();
        protected Dictionary<string, BundleLoadingCtx> m_bundleLoadingCtxDict = new Dictionary<string, BundleLoadingCtx>();
        protected HashSet<string> m_touchedBundleSet;
        protected bool m_touchedBundleSetDirty;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> m_eventAssetBundleManifestLoading;
        protected bool m_skipAssetBundlePreUpdateing;
        protected int m_updateingWorkerCount;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> m_eventOnAssetBundleUpdateingEnd;
        public Func<System.Collections.Generic.List<string>> GetCustomPreUpdateBundleListFunc;
        public Func<System.Collections.Generic.List<string>> GetCustomSkipPreUpdateBundleListFunc;
        protected System.Collections.Generic.List<BundleData.SingleBundleData> m_updateingBundleList;
        protected System.Collections.Generic.List<string> m_customSkipPreUpdateBundleList;
        protected System.Collections.Generic.List<string> m_customPreUpdateBundleList;
        protected int m_updateWorkerAliveCount;
        protected bool m_isAllUpdateWorkerSuccess;
        protected long m_assetBundleUpdateingTotalByte;
        protected long m_assetBundleUpdateingDownloadedByte;
        private bool m_disableAssetBundleDownload;
        private int m_currBundleDataVersion;
        private BundleData m_lastPreUpdateBundleData;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnBundleDataLoadingEnd;
        private int m_retryDownloadBundleDataVersionLeftCount;
        private const int RetryDownloadBundleDataVesionMaxCount = 5;
        public const int CheckBundleVersion_NetworkError = -1;
        private const string m_signalFileName = "dontmove.tmp";
        protected Dictionary<string, ReserveItem> m_assetReserveDict = new Dictionary<string, ReserveItem>();
        protected Dictionary<string, ReserveItem> m_assetReserveDict4Keep = new Dictionary<string, ReserveItem>();
        protected System.Collections.Generic.List<string> m_assetReserve4Remove = new System.Collections.Generic.List<string>();
        protected DateTime m_reserveTickDelayOutTime;
        protected const float ReserveTickDelayTime = 5f;
        private bool m_skipStreamingAssetsFileProcessing;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> m_eventOnStreamingAssetsFilesProcessingEnd;
        private int m_currStreamingAssetsFileListVersion;
        private StreamingAssetsFileList m_streamingAssetsFileList;
        private BundleData m_streamingAssetsBundleData;
        private BundleDataHelper m_streamingAssetsBundleDataHelper;
        private Boo.Lang.List<BundleData.SingleBundleData> m_streamingAssetsMissingBundles = new Boo.Lang.List<BundleData.SingleBundleData>();
        private Boo.Lang.List<BundleData.SingleBundleData> m_lazyUpdateBundles = new Boo.Lang.List<BundleData.SingleBundleData>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateResourceManager;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_Uninitlize;
        private static DelegateBridge __Hotfix_LoadAsset;
        private static DelegateBridge __Hotfix_LoadAssetSync;
        private static DelegateBridge __Hotfix_IsNeedLoadAllForAssetPath;
        private static DelegateBridge __Hotfix_StartLoadAssetsCorutine;
        private static DelegateBridge __Hotfix_StartLoadAssetCorutine;
        private static DelegateBridge __Hotfix_LoadAssetsCorutine;
        private static DelegateBridge __Hotfix_LoadAssetByResourcesLoad;
        private static DelegateBridge __Hotfix_LoadAssetByAssetDatabase;
        private static DelegateBridge __Hotfix_GetAssetFromCache;
        private static DelegateBridge __Hotfix_PushAssetToCache;
        private static DelegateBridge __Hotfix_SetLocalization;
        private static DelegateBridge __Hotfix_SetDisableAssetBundle;
        private static DelegateBridge __Hotfix_SetSkipStreamingAssetsFileProcessing;
        private static DelegateBridge __Hotfix_SetDisableAssetBundleDownload;
        private static DelegateBridge __Hotfix_SetSkipAssetBundlePreUpdateing;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickAsset;
        private static DelegateBridge __Hotfix_TickGC;
        private static DelegateBridge __Hotfix_TickVersionUpdate;
        private static DelegateBridge __Hotfix_StartCheckBundleDataVersion;
        private static DelegateBridge __Hotfix_SetCheckVersionUpdateInRuningUrl;
        private static DelegateBridge __Hotfix_TickMemUseage;
        private static DelegateBridge __Hotfix_OutputMemUseage;
        private static DelegateBridge __Hotfix_UnloadUnusedAssets;
        private static DelegateBridge __Hotfix_UnloadUnusedResourceAll;
        private static DelegateBridge __Hotfix_UnloadAsset;
        private static DelegateBridge __Hotfix_UnloadAllBundle;
        private static DelegateBridge __Hotfix_CleanUnusedAssetsCache;
        private static DelegateBridge __Hotfix_GetDownloadBundleUrl;
        private static DelegateBridge __Hotfix_GetStreamingAssetBundleUrl;
        private static DelegateBridge __Hotfix_GetStreamingAssetBundlePath;
        private static DelegateBridge __Hotfix_GetBundleLoadingUrl;
        private static DelegateBridge __Hotfix_IsBundleNeedLoadFromStreamingAssetFile;
        private static DelegateBridge __Hotfix_LoadBundleFromCacheOrDownload;
        private static DelegateBridge __Hotfix_GetLoadedAssetPathSet;
        private static DelegateBridge __Hotfix_EnableDetailResourceManagerLog;
        private static DelegateBridge __Hotfix_add_EventOnAssetLoaded;
        private static DelegateBridge __Hotfix_remove_EventOnAssetLoaded;
        private static DelegateBridge __Hotfix_add_EventOnBundleDataVersionMisMatch;
        private static DelegateBridge __Hotfix_remove_EventOnBundleDataVersionMisMatch;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;
        private static DelegateBridge __Hotfix_add_m_eventOnUnloadUnusedResourceAllCompleted;
        private static DelegateBridge __Hotfix_remove_m_eventOnUnloadUnusedResourceAllCompleted;
        private static DelegateBridge __Hotfix_SetDownloadBundleUrlPrefix;
        private static DelegateBridge __Hotfix_SetDownloadBundleUrl;
        private static DelegateBridge __Hotfix_LoadAssetFromBundle;
        private static DelegateBridge __Hotfix_LoadBundle4UnityScene;
        private static DelegateBridge __Hotfix_LoadBundle_0;
        private static DelegateBridge __Hotfix_LoadBundle_1;
        private static DelegateBridge __Hotfix_LoadBundleFromWWWOrStreamingImpl;
        private static DelegateBridge __Hotfix_GetBundleNameByCurrLocalization;
        private static DelegateBridge __Hotfix_MakeAssetBundleDontUnload;
        private static DelegateBridge __Hotfix_GetAssetBundleFromCache;
        private static DelegateBridge __Hotfix_PushAssetBundleToCache;
        private static DelegateBridge __Hotfix_OnBundleCacheHit;
        private static DelegateBridge __Hotfix_RegBundleLoadingCtx;
        private static DelegateBridge __Hotfix_UnregBundleLoadingCtx;
        private static DelegateBridge __Hotfix_GetBundleDataByAssetPath;
        private static DelegateBridge __Hotfix_GetResaveFileBundleDataByPath;
        private static DelegateBridge __Hotfix_GetAssetNameByPath;
        private static DelegateBridge __Hotfix_TickBundle;
        private static DelegateBridge __Hotfix_UnloadAllUnusedBundles;
        private static DelegateBridge __Hotfix_DealWithResaveFileInBundle;
        private static DelegateBridge __Hotfix_ConvertRelativePathFile2OrginalPathFile;
        private static DelegateBridge __Hotfix_GetResaveRuntimePath;
        private static DelegateBridge __Hotfix_TryGetResaveFile;
        private static DelegateBridge __Hotfix_DownloadResaveFile;
        private static DelegateBridge __Hotfix_StartDownloadResaveFile;
        private static DelegateBridge __Hotfix_LoadTouchedBundleSet;
        private static DelegateBridge __Hotfix_SaveTouchedBundleSet;
        private static DelegateBridge __Hotfix_AddBundle2TouchedBundleSet;
        private static DelegateBridge __Hotfix_IsAssetInTouchedBundleSet;
        private static DelegateBridge __Hotfix_StartAssetBundleManifestLoading;
        private static DelegateBridge __Hotfix_AssetBundleManifestLoadingWorker;
        private static DelegateBridge __Hotfix_OnAssetBundleManifestLoadingWorkerEnd;
        private static DelegateBridge __Hotfix_GetAssetBundleManifestBundleName;
        private static DelegateBridge __Hotfix_add_m_eventAssetBundleManifestLoading;
        private static DelegateBridge __Hotfix_remove_m_eventAssetBundleManifestLoading;
        private static DelegateBridge __Hotfix_StartAssetBundlePreUpdateing;
        private static DelegateBridge __Hotfix_StartAssetBundleUpdateingWorkers;
        private static DelegateBridge __Hotfix_AssetBundleUpdateingWorker;
        private static DelegateBridge __Hotfix_WaitForTimeSecond;
        private static DelegateBridge __Hotfix_CalcPreUpdateBundleListByBundleData;
        private static DelegateBridge __Hotfix_GetSingleBundleData4PreUpdate;
        private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingWorkerEnd;
        private static DelegateBridge __Hotfix_GetAssetBundleUpdateingPercent;
        private static DelegateBridge __Hotfix_GetPreUpdateingDownloadedBytes;
        private static DelegateBridge __Hotfix_GetTotalPreUpdateingDownloadBytes;
        private static DelegateBridge __Hotfix_StartDownloadUntouchedAssets;
        private static DelegateBridge __Hotfix_OnUntouchedAssetBundleUpdateingWorkerEnd;
        private static DelegateBridge __Hotfix_GetTotalUntouchedAssetBundleDownloadBytes;
        private static DelegateBridge __Hotfix_IsForceCustomPreUpdate;
        private static DelegateBridge __Hotfix_IsBundleInCustomPreUpdateList;
        private static DelegateBridge __Hotfix_IsBundleInSkipPreUpdateList;
        private static DelegateBridge __Hotfix_Log;
        private static DelegateBridge __Hotfix_add_m_eventOnAssetBundleUpdateingEnd;
        private static DelegateBridge __Hotfix_remove_m_eventOnAssetBundleUpdateingEnd;
        private static DelegateBridge __Hotfix_StartBundleDataLoading;
        private static DelegateBridge __Hotfix_BundleDataLoadingWorker;
        private static DelegateBridge __Hotfix_CheckBundleDataVersionFromUrl;
        private static DelegateBridge __Hotfix_OnBundleDataLoadingWorkerEnd;
        private static DelegateBridge __Hotfix_GetBundleData;
        private static DelegateBridge __Hotfix_GetBundleDataBasicVersion;
        private static DelegateBridge __Hotfix_GetBundleDataVersion;
        private static DelegateBridge __Hotfix_add_EventOnBundleDataLoadingEnd;
        private static DelegateBridge __Hotfix_remove_EventOnBundleDataLoadingEnd;
        private static DelegateBridge __Hotfix_CheckBundleCache;
        private static DelegateBridge __Hotfix_IsBundleCacheSignalFileExist;
        private static DelegateBridge __Hotfix_CreateBundleCacheSignalFile;
        private static DelegateBridge __Hotfix_ClearForCacheLost;
        private static DelegateBridge __Hotfix_ReserveAsset;
        private static DelegateBridge __Hotfix_StartReserveAssetsCorutine;
        private static DelegateBridge __Hotfix_AddAsset2Reserve;
        private static DelegateBridge __Hotfix_SetupReserveTimeOut;
        private static DelegateBridge __Hotfix_RemoveReserve;
        private static DelegateBridge __Hotfix_RemoveReserve4CacheHit;
        private static DelegateBridge __Hotfix_ClearAllReserve;
        private static DelegateBridge __Hotfix_TickReserve;
        private static DelegateBridge __Hotfix_StartStreamingAssetsFilesProcessing;
        private static DelegateBridge __Hotfix_IsBundleInStreamingAssets;
        private static DelegateBridge __Hotfix_StreamingAssetsFileProcessor;
        private static DelegateBridge __Hotfix_DealWithResaveFileInBundleAfterStreamingAssetProcessing;
        private static DelegateBridge __Hotfix_OnStreamingAssetsFileProcessorEnd;
        private static DelegateBridge __Hotfix_add_m_eventOnStreamingAssetsFilesProcessingEnd;
        private static DelegateBridge __Hotfix_remove_m_eventOnStreamingAssetsFilesProcessingEnd;
        private static DelegateBridge __Hotfix_LoadUnityScene;

        public event Action EventOnAssetLoaded
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        private event Action<bool> EventOnBundleDataLoadingEnd
        {
            [MethodImpl(0x8000)] add
            {
                DelegateBridge bridge = __Hotfix_add_EventOnBundleDataLoadingEnd;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, value);
                }
                else
                {
                    Action<bool> eventOnBundleDataLoadingEnd = this.EventOnBundleDataLoadingEnd;
                    while (true)
                    {
                        Action<bool> a = eventOnBundleDataLoadingEnd;
                        eventOnBundleDataLoadingEnd = Interlocked.CompareExchange<Action<bool>>(ref this.EventOnBundleDataLoadingEnd, (Action<bool>) System.Delegate.Combine(a, value), eventOnBundleDataLoadingEnd);
                        if (ReferenceEquals(eventOnBundleDataLoadingEnd, a))
                        {
                            return;
                        }
                    }
                }
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBundleDataVersionMisMatch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        protected event Action<bool> m_eventAssetBundleManifestLoading
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        protected event Action<bool> m_eventOnAssetBundleUpdateingEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        private event Action<bool> m_eventOnStreamingAssetsFilesProcessingEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        protected event Action m_eventOnUnloadUnusedResourceAllCompleted
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected ResourceManager()
        {
            this.State = RMState.None;
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        private ReserveItem AddAsset2Reserve(string path, int reserveTime, UnityEngine.Object asset)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddBundle2TouchedBundleSet(string bundleName, bool checkBundleData = true)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator AssetBundleManifestLoadingWorker()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator AssetBundleUpdateingWorker(Action<bool> onEnd, Action<BundleData.SingleBundleData> onSingleBundleDownloadSuccess = null)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator BundleDataLoadingWorker()
        {
            DelegateBridge bridge = __Hotfix_BundleDataLoadingWorker;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp28(this);
            }
            return new <BundleDataLoadingWorker>c__IteratorD { $this = this };
        }

        [MethodImpl(0x8000)]
        protected void CalcPreUpdateBundleListByBundleData()
        {
        }

        [MethodImpl(0x8000)]
        public void CheckBundleCache()
        {
            DelegateBridge bridge = __Hotfix_CheckBundleCache;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (!this.IsBundleCacheSignalFileExist())
            {
                this.ClearForCacheLost();
                this.CreateBundleCacheSignalFile();
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator CheckBundleDataVersionFromUrl(Action<bool, int, bool, int> onEnd, string independentUrl = null, int timeout = 0x1388)
        {
            DelegateBridge bridge = __Hotfix_CheckBundleDataVersionFromUrl;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp147(this, onEnd, independentUrl, timeout);
            }
            return new <CheckBundleDataVersionFromUrl>c__IteratorE { 
                onEnd = onEnd,
                independentUrl = independentUrl,
                timeout = timeout,
                $this = this
            };
        }

        [MethodImpl(0x8000)]
        protected void CleanUnusedAssetsCache()
        {
            DelegateBridge bridge = __Hotfix_CleanUnusedAssetsCache;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                foreach (AssetCacheItem item in this.m_assetsCacheDict.Values)
                {
                    if (!item.m_weakRefrence.IsAlive)
                    {
                        this.m_assetsCacheToRemove.Add(item);
                    }
                }
                foreach (AssetCacheItem item2 in this.m_assetsCacheToRemove)
                {
                    this.m_assetsCacheDict.Remove(item2.m_cacheKey);
                }
                this.m_assetsCacheToRemove.Clear();
            }
        }

        [MethodImpl(0x8000)]
        protected void ClearAllReserve(HashSet<string> keepReserve = null)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearForCacheLost()
        {
        }

        [MethodImpl(0x8000)]
        private string ConvertRelativePathFile2OrginalPathFile(string relativePathFile)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateBundleCacheSignalFile()
        {
        }

        [MethodImpl(0x8000)]
        public static BlackJack.BJFramework.Runtime.Resource.ResourceManager CreateResourceManager()
        {
            DelegateBridge bridge = __Hotfix_CreateResourceManager;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp129();
            }
            if (m_instance == null)
            {
                m_instance = new BlackJack.BJFramework.Runtime.Resource.ResourceManager();
            }
            return m_instance;
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DealWithResaveFileInBundle(AssetBundle assetBundle, BundleDataHelper bundleHelper)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator DealWithResaveFileInBundleAfterStreamingAssetProcessing()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator DownloadResaveFile(string srcDir, HashSet<string> relativePaths, Action<string, bool> onComplete)
        {
        }

        [MethodImpl(0x8000)]
        public bool EnableDetailResourceManagerLog()
        {
        }

        [MethodImpl(0x8000)]
        protected BundleCacheItem GetAssetBundleFromCache(string bundleName)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetAssetBundleManifestBundleName()
        {
        }

        [MethodImpl(0x8000)]
        public float GetAssetBundleUpdateingPercent()
        {
        }

        [MethodImpl(0x8000)]
        protected UnityEngine.Object GetAssetFromCache(string key)
        {
            AssetCacheItem item;
            DelegateBridge bridge = __Hotfix_GetAssetFromCache;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp113(this, key);
            }
            if (!this.m_assetsCacheDict.TryGetValue(key, out item))
            {
                if (this.m_enableDetailResourceManagerLog)
                {
                    Debug.Log("GetAssetFromCache NULL " + key);
                }
            }
            else
            {
                WeakReference weakRefrence = item.m_weakRefrence;
                if (weakRefrence.IsAlive)
                {
                    if (this.m_enableDetailResourceManagerLog)
                    {
                        Debug.Log("GetAssetFromCache OK " + key);
                    }
                    if (item.m_removeReserveOnHit)
                    {
                        this.RemoveReserve4CacheHit(key);
                    }
                    return (weakRefrence.Target as UnityEngine.Object);
                }
                if (this.m_enableDetailResourceManagerLog)
                {
                    Debug.Log("GetAssetFromCache Dead " + key);
                }
                this.m_assetsCacheDict.Remove(key);
            }
            return null;
        }

        [MethodImpl(0x8000)]
        protected string GetAssetNameByPath(string path)
        {
        }

        [MethodImpl(0x8000)]
        public BundleData GetBundleData()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBundleDataBasicVersion()
        {
        }

        [MethodImpl(0x8000)]
        protected BundleData.SingleBundleData GetBundleDataByAssetPath(string path)
        {
        }

        [MethodImpl(0x8000)]
        public int GetBundleDataVersion()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetBundleLoadingUrl(string bundleName)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetBundleNameByCurrLocalization(BundleData.SingleBundleData bundleData)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetDownloadBundleUrl(string path)
        {
            DelegateBridge bridge = __Hotfix_GetDownloadBundleUrl;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp44(this, path);
            }
            if (this.m_downloadUrlRootWithPlatform == null)
            {
                this.m_downloadUrlRootWithPlatform = !string.IsNullOrEmpty(this.m_downloadBundleUrlPrefix) ? $"{this.m_bundleDownloadUrlRoot}/{this.m_downloadBundleUrlPrefix}/{Util.GetCurrentTargetPlatform()}" : $"{this.m_bundleDownloadUrlRoot}/{Util.GetCurrentTargetPlatform()}";
            }
            return $"{this.m_downloadUrlRootWithPlatform}/{path}";
        }

        [MethodImpl(0x8000)]
        public HashSet<string> GetLoadedAssetPathSet()
        {
        }

        [MethodImpl(0x8000)]
        public long GetPreUpdateingDownloadedBytes()
        {
        }

        [MethodImpl(0x8000)]
        protected BundleData.SingleBundleData GetResaveFileBundleDataByPath(string relativePath)
        {
        }

        [MethodImpl(0x8000)]
        public string GetResaveRuntimePath(string srcDir, string relativePath)
        {
        }

        [MethodImpl(0x8000)]
        protected BundleData.SingleBundleData GetSingleBundleData4PreUpdate()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetStreamingAssetBundlePath(string bundleName)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetStreamingAssetBundleUrl(string bundleName)
        {
        }

        [MethodImpl(0x8000)]
        public long GetTotalPreUpdateingDownloadBytes()
        {
        }

        [MethodImpl(0x8000)]
        public long GetTotalUntouchedAssetBundleDownloadBytes()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initlize(ResourcesSettings settings, string currLocalization, bool useObb)
        {
            int skipAssetBundlePreUpdateing;
            DelegateBridge bridge = __Hotfix_Initlize;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp130(this, settings, currLocalization, useObb);
            }
            Debug.Log("ResourceManager.Initlize start");
            this.State = RMState.Inited;
            this.m_disableAssetBundle = settings.DisableAssetBundle;
            this.m_skipStreamingAssetsFileProcessing = this.m_disableAssetBundle && settings.SkipStreamingAssetsFileProcessing;
            this.m_disableAssetBundleDownload = !this.m_disableAssetBundle ? settings.DisableAssetBundleDownload : true;
            if (this.m_disableAssetBundle || this.m_disableAssetBundleDownload)
            {
                skipAssetBundlePreUpdateing = 1;
            }
            else
            {
                skipAssetBundlePreUpdateing = (int) settings.SkipAssetBundlePreUpdateing;
            }
            this.m_skipAssetBundlePreUpdateing = (bool) skipAssetBundlePreUpdateing;
            this.m_bundleDownloadUrlRoot = settings.AssetBundleDownloadUrlRoot;
            this.m_updateingWorkerCount = (settings.PreUpdateWorkerCount >= 1) ? settings.PreUpdateWorkerCount : 1;
            this.m_loadAssetFromBundleInEditor = !settings.DisableAssetBundle ? settings.LoadAssetFromBundleInEditor : false;
            this.m_unloadUnusedAssetTimeInterval = settings.UnloadUnusedAssetTimeInterval;
            this.m_assetPathIgnoreCase = settings.AssetPathIgnoreCase;
            this.m_enableDetailResourceManagerLog = settings.EnableDetailResourceManagerLog;
            this.m_enableCheckVersionUpdateInRuning = settings.EnableCheckVersionUpdateInRuning;
            this.m_checkVersionUpdateInRuningUrl = settings.CheckVersionUpdateInRuningUrl;
            this.m_checkVersionUpdateInRuningCD = settings.CheckVersionUpdateInRuningCD;
            this.m_currLocalization = currLocalization;
            this.m_useObb = useObb;
            return this.LoadTouchedBundleSet();
        }

        [MethodImpl(0x8000)]
        public bool IsAssetInTouchedBundleSet(string assetPath)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsBundleCacheSignalFileExist()
        {
            DelegateBridge bridge = __Hotfix_IsBundleCacheSignalFileExist;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            return File.Exists($"{Application.persistentDataPath}/{"dontmove.tmp"}".Replace('\\', '/'));
        }

        [MethodImpl(0x8000)]
        protected bool IsBundleInCustomPreUpdateList(string bundleName)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsBundleInSkipPreUpdateList(string bundleName)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsBundleInStreamingAssets(BundleData.SingleBundleData singleBundleData)
        {
            DelegateBridge bridge = __Hotfix_IsBundleInStreamingAssets;
            return ((bridge == null) ? (!singleBundleData.m_isInStreamingAssets ? ((Application.platform == RuntimePlatform.Android) && (this.m_useObb && singleBundleData.m_isInStreamingAssetsOnObb)) : true) : bridge.__Gen_Delegate_Imp16(this, singleBundleData));
        }

        [MethodImpl(0x8000)]
        protected bool IsBundleNeedLoadFromStreamingAssetFile(string bundleName, int version)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsForceCustomPreUpdate()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsNeedLoadAllForAssetPath(string assetPath)
        {
            DelegateBridge bridge = __Hotfix_IsNeedLoadAllForAssetPath;
            return ((bridge == null) ? (assetPath.EndsWith(".png", StringComparison.OrdinalIgnoreCase) || assetPath.EndsWith(".fbx", StringComparison.OrdinalIgnoreCase)) : bridge.__Gen_Delegate_Imp16(this, assetPath));
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator LoadAsset<T>(string path, Action<string, T> onCompleted, bool noErrlog = false, bool loadAsync = false) where T: UnityEngine.Object
        {
            DelegateBridge bridge = __Hotfix_LoadAsset;
            if (bridge == null)
            {
                return new <LoadAsset>c__Iterator0<T> { 
                    path = path,
                    onCompleted = onCompleted,
                    noErrlog = noErrlog,
                    loadAsync = loadAsync,
                    $this = this
                };
            }
            bridge.InvokeSessionStart();
            bridge.InParam<BlackJack.BJFramework.Runtime.Resource.ResourceManager>(this);
            bridge.InParam<string>(path);
            bridge.InParam<Action<string, T>>(onCompleted);
            bridge.InParam<bool>(noErrlog);
            bridge.InParam<bool>(loadAsync);
            bridge.Invoke(1);
            return bridge.InvokeSessionEndWithResult<IEnumerator>();
        }

        [MethodImpl(0x8000)]
        protected virtual void LoadAssetByAssetDatabase<T>(string path, Action<string, UnityEngine.Object[]> onCompleted, bool noErrlog = false, bool isNeedLoadAllRes = false) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000)]
        protected void LoadAssetByResourcesLoad<T>(string path, Action<string, UnityEngine.Object[]> onCompleted, bool noErrlog = false, bool isNeedLoadAllRes = false) where T: UnityEngine.Object
        {
            DelegateBridge bridge = __Hotfix_LoadAssetByResourcesLoad;
            if (bridge != null)
            {
                bridge.InvokeSessionStart();
                bridge.InParam<BlackJack.BJFramework.Runtime.Resource.ResourceManager>(this);
                bridge.InParam<string>(path);
                bridge.InParam<Action<string, UnityEngine.Object[]>>(onCompleted);
                bridge.InParam<bool>(noErrlog);
                bridge.InParam<bool>(isNeedLoadAllRes);
                bridge.Invoke(0);
                bridge.InvokeSessionEnd();
            }
            else
            {
                int length = path.LastIndexOf(".", StringComparison.Ordinal);
                string str = (length != -1) ? path.Substring(0, length) : path;
                int index = str.IndexOf("Resources/", StringComparison.Ordinal);
                str = (index != -1) ? str.Substring(index + "Resources/".Length) : str;
                UnityEngine.Object[] objArray = !isNeedLoadAllRes ? new UnityEngine.Object[] { Resources.Load<T>(str) } : Resources.LoadAll(str);
                if ((objArray != null) && (objArray.Length != 0))
                {
                    if (this.m_enableDetailResourceManagerLog)
                    {
                        Debug.Log($"LoadAssetByResourcesLoad ok {str}");
                    }
                }
                else if (!noErrlog)
                {
                    Debug.LogError($"LoadAssetByResourcesLoad fail {str}");
                }
                else
                {
                    Debug.Log($"LoadAssetByResourcesLoad fail {str}");
                }
                onCompleted(str, objArray);
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadAssetFromBundle(string path, Action<string, UnityEngine.Object[], BundleCacheItem> onCompleted, bool noErrlog = false, bool loadAync = false)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadAssetsCorutine(HashSet<string> pathList, int corutineId, IDictionary<string, UnityEngine.Object> assetDict, Action onComplete, bool loadAsync)
        {
        }

        [MethodImpl(0x8000)]
        public T LoadAssetSync<T>(string path, bool noErrlog = false) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadBundle(BundleData.SingleBundleData bundleData, bool loadAync, Action<AssetBundle, BundleCacheItem> onComplete)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadBundle(string bundleName, bool loadAync, Action<AssetBundle, BundleCacheItem> onComplete, bool noErrlog = false)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadBundle4UnityScene(string scenePath, Action<string, BundleCacheItem> onComplete, bool noErrlog, bool loadAync)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadBundleFromCacheOrDownload(string url, int bundleVersion, uint bundleCRC, Action<AssetBundle> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator LoadBundleFromWWWOrStreamingImpl(BundleData.SingleBundleData bundleData, bool loadAync, bool ignoreCRC, Action<AssetBundle> onComplete)
        {
        }

        [MethodImpl(0x8000)]
        protected bool LoadTouchedBundleSet()
        {
            DelegateBridge bridge = __Hotfix_LoadTouchedBundleSet;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            string filePersistentPath = Util.GetFilePersistentPath("TouchedBundleSet");
            if (!File.Exists(filePersistentPath))
            {
                this.m_touchedBundleSet = new HashSet<string>();
                this.SaveTouchedBundleSet(true);
                return true;
            }
            try
            {
                FileStream serializationStream = null;
                System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string>();
                this.m_touchedBundleSet = new HashSet<string>();
                using (serializationStream = File.OpenRead(filePersistentPath))
                {
                    list = new BinaryFormatter().Deserialize(serializationStream) as System.Collections.Generic.List<string>;
                }
                foreach (string str2 in list)
                {
                    this.m_touchedBundleSet.Add(str2);
                }
            }
            catch (Exception exception)
            {
                Debug.LogError($"{"ResourceManager.LoadTouchedBundleSet"} : {exception}");
                this.m_touchedBundleSet = new HashSet<string>();
                this.SaveTouchedBundleSet(true);
                return false;
            }
            return true;
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator LoadUnityScene(string path, Action<string, Scene?> onCompleted, bool noErrlog = false, bool loadAync = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void Log(string msg)
        {
        }

        [MethodImpl(0x8000)]
        public bool MakeAssetBundleDontUnload(string assetPath)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAssetBundleManifestLoadingWorkerEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAssetBundlePreUpdateingWorkerEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBundleCacheHit(BundleCacheItem item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBundleDataLoadingWorkerEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStreamingAssetsFileProcessorEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUntouchedAssetBundleUpdateingWorkerEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        public void OutputMemUseage()
        {
        }

        [MethodImpl(0x8000)]
        protected BundleCacheItem PushAssetBundleToCache(string bundleName, AssetBundle bundle)
        {
        }

        [MethodImpl(0x8000)]
        protected void PushAssetToCache(string key, UnityEngine.Object asset, BundleCacheItem bundleCacheItem = null)
        {
            DelegateBridge bridge = __Hotfix_PushAssetToCache;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp37(this, key, asset, bundleCacheItem);
            }
            else
            {
                AssetCacheItem item;
                if (!this.m_assetsCacheDict.TryGetValue(key, out item) || !ReferenceEquals(item.m_weakRefrence.Target, asset))
                {
                    item = new AssetCacheItem {
                        m_weakRefrence = new WeakReference(asset),
                        m_cacheKey = key
                    };
                    this.m_assetsCacheDict[key] = item;
                    if (bundleCacheItem != null)
                    {
                        this.OnBundleCacheHit(bundleCacheItem);
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        protected bool RegBundleLoadingCtx(string bundleName, out BundleLoadingCtx ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveReserve(string path)
        {
        }

        [MethodImpl(0x8000)]
        private void RemoveReserve4CacheHit(string path)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator ReserveAsset(string path, Action<string, UnityEngine.Object> onCompleted, bool noErrlog = false, int reserveTime = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SaveTouchedBundleSet(bool force = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCheckVersionUpdateInRuningUrl(string url)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDisableAssetBundle(bool disableAssetBundle)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDisableAssetBundleDownload(bool disableAssetBundleDownload)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDownloadBundleUrl(string downloadUrlStr)
        {
            DelegateBridge bridge = __Hotfix_SetDownloadBundleUrl;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, downloadUrlStr);
            }
            else if (!string.IsNullOrEmpty(downloadUrlStr) && (this.m_bundleDownloadUrlRoot != downloadUrlStr))
            {
                this.m_bundleDownloadUrlRoot = downloadUrlStr;
                this.m_nextTickVersionUpdateTime = DateTime.Now;
                this.m_downloadUrlRootWithPlatform = null;
            }
        }

        [MethodImpl(0x8000)]
        public void SetDownloadBundleUrlPrefix(string prefix)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocalization(string localization, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkipAssetBundlePreUpdateing(bool skipAssetBundlePreUpdateing)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkipStreamingAssetsFileProcessing(bool skipStreamingAssetsFileProcessing)
        {
        }

        [MethodImpl(0x8000)]
        private void SetupReserveTimeOut(ReserveItem reserveItem, int ReserveTime)
        {
        }

        [MethodImpl(0x8000)]
        public void StartAssetBundleManifestLoading(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StartAssetBundlePreUpdateing(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartAssetBundleUpdateingWorkers(Action<bool> onEnd, Action<BundleData.SingleBundleData> onSingleBundleDownloadSuccess = null)
        {
        }

        [MethodImpl(0x8000)]
        public void StartBundleDataLoading(Action<bool> onEnd)
        {
            DelegateBridge bridge = __Hotfix_StartBundleDataLoading;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, onEnd);
            }
            else
            {
                Debug.Log("ResourceManager.StartBundleDataLoading");
                if (this.State != RMState.StreamingAssetsFilesProcessEnd)
                {
                    Debug.LogError("StartBundleDataLoading in wrong state");
                    onEnd(false);
                }
                else
                {
                    this.State = RMState.BundleDataLoading;
                    if (!this.m_disableAssetBundle)
                    {
                        this.EventOnBundleDataLoadingEnd += onEnd;
                        this.m_corutineHelper.StartCorutine(new Func<IEnumerator>(this.BundleDataLoadingWorker));
                    }
                    else
                    {
                        Debug.Log("ResourceManager.StartBundleDataLoading skip");
                        this.State = RMState.BundleDataLoadEnd;
                        onEnd(true);
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        public void StartCheckBundleDataVersion(Action<bool, int> onCheckEnd, bool ignoreOnUnreachable)
        {
            DelegateBridge bridge = __Hotfix_StartCheckBundleDataVersion;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp12(this, onCheckEnd, ignoreOnUnreachable);
            }
            else
            {
                <StartCheckBundleDataVersion>c__AnonStorey18 storey = new <StartCheckBundleDataVersion>c__AnonStorey18 {
                    onCheckEnd = onCheckEnd,
                    ignoreOnUnreachable = ignoreOnUnreachable
                };
                this.m_corutineHelper.StartCorutine(this.CheckBundleDataVersionFromUrl(new Action<bool, int, bool, int>(storey.<>m__0), this.m_checkVersionUpdateInRuningUrl, 0x1388));
            }
        }

        [MethodImpl(0x8000)]
        public void StartDownloadResaveFile(string srcDir, HashSet<string> relativePaths, Action<string, bool> onComplete)
        {
        }

        [MethodImpl(0x8000)]
        public void StartDownloadUntouchedAssets(HashSet<string> assets, Action<bool> OnEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StartLoadAssetCorutine(string path, Action<string, UnityEngine.Object> onComplete)
        {
        }

        [MethodImpl(0x8000)]
        public void StartLoadAssetsCorutine(HashSet<string> pathList, IDictionary<string, UnityEngine.Object> assetDict, Action onComplete, bool loadAsync = false)
        {
        }

        [MethodImpl(0x8000)]
        public void StartReserveAssetsCorutine(HashSet<string> pathList, IDictionary<string, UnityEngine.Object> assetDict, Action onComplete, bool loadAsync = false, int reserveTime = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void StartStreamingAssetsFilesProcessing(Action<bool> onEnd)
        {
            DelegateBridge bridge = __Hotfix_StartStreamingAssetsFilesProcessing;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp5(this, onEnd);
            }
            else
            {
                Debug.Log("ResourceManager.StartStreamingAssetsFilesProcessing");
                if (this.State != RMState.Inited)
                {
                    Debug.LogError("StartProcessStreamingAssetsFiles in wrong state");
                    onEnd(false);
                }
                else
                {
                    this.State = RMState.StreamingAssetsFilesProcessing;
                    if (this.m_skipStreamingAssetsFileProcessing)
                    {
                        Debug.LogWarning("ResourceManager.StartStreamingAssetsFilesProcessing skip processing");
                        this.State = RMState.StreamingAssetsFilesProcessEnd;
                        onEnd(true);
                    }
                    else
                    {
                        int @int = PlayerPrefs.GetInt("StreamingAssetsFileListVersion");
                        Debug.Log(" StartStreamingAssetsFilesProcessing oldVersion:" + @int);
                        this.m_streamingAssetsBundleData = Resources.Load<BundleData>(PathHelper.StreamAssetBundleDataResourcesPath);
                        if (this.m_streamingAssetsBundleData == null)
                        {
                            Debug.LogError("Load streamingAssetsBundleData fail");
                            onEnd(false);
                        }
                        else
                        {
                            this.m_streamingAssetsBundleDataHelper = new BundleDataHelper(this.m_streamingAssetsBundleData, this.m_assetPathIgnoreCase);
                            this.m_streamingAssetsFileList = Resources.Load<StreamingAssetsFileList>(PathHelper.StreamingAssetsFileListResourcesName);
                            if (this.m_streamingAssetsFileList == null)
                            {
                                Debug.LogError("Load StreamingAssetsFileList fail");
                                onEnd(false);
                            }
                            else
                            {
                                using (System.Collections.Generic.List<BundleData.SingleBundleData>.Enumerator enumerator = this.m_streamingAssetsBundleData.m_bundleList.GetEnumerator())
                                {
                                    while (enumerator.MoveNext())
                                    {
                                        <StartStreamingAssetsFilesProcessing>c__AnonStorey26 storey = new <StartStreamingAssetsFilesProcessing>c__AnonStorey26();
                                        if (storey.singleBundleData.m_isLazyUpdate)
                                        {
                                            this.m_lazyUpdateBundles.Add(storey.singleBundleData);
                                        }
                                        if (this.IsBundleInStreamingAssets(storey.singleBundleData) && (this.m_streamingAssetsFileList.m_fileList.Find(new Predicate<StreamingAssetsFileList.ListItem>(storey.<>m__0)) == null))
                                        {
                                            this.Log($"StartStreamingAssetsFilesProcessing  bundle.Name: {storey.singleBundleData.m_bundleName} is in m_streamingAssetsMissingBundles");
                                            this.m_streamingAssetsMissingBundles.Add(storey.singleBundleData);
                                        }
                                    }
                                }
                                Debug.Log(" StartStreamingAssetsFilesProcessing m_streamingAssetsFileList.m_version:" + this.m_streamingAssetsFileList.m_version);
                                if (this.m_streamingAssetsFileList.m_version > @int)
                                {
                                    this.m_currStreamingAssetsFileListVersion = this.m_streamingAssetsFileList.m_version;
                                    this.m_eventOnStreamingAssetsFilesProcessingEnd += onEnd;
                                    this.m_corutineHelper.StartCorutine(new Func<IEnumerator>(this.StreamingAssetsFileProcessor));
                                }
                                else
                                {
                                    this.m_currStreamingAssetsFileListVersion = @int;
                                    this.State = RMState.StreamingAssetsFilesProcessEnd;
                                    Debug.LogWarning($"ResourceManager.StartStreamingAssetsFilesProcessing m_streamingAssetsFileList.m_version:{this.m_streamingAssetsFileList.m_version} <= oldVersion:{@int}, may skip processing");
                                    onEnd(true);
                                }
                            }
                        }
                    }
                }
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator StreamingAssetsFileProcessor()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                try
                {
                    this.m_corutineHelper.Tick();
                    this.TickReserve();
                    this.TickAsset();
                    this.TickBundle();
                    this.TickVersionUpdate();
                }
                catch (Exception exception)
                {
                    Debug.LogError($"{"ResourceManager.Tick"} : {exception}");
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void TickAsset()
        {
            DelegateBridge bridge = __Hotfix_TickAsset;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                if (this.m_needUnloadUnusedAssets && (this.m_loadingOpCount == 0))
                {
                    this.m_needUnloadUnusedAssets = false;
                    this.UnloadUnusedAssets();
                }
                if ((this.m_currUnloadUnusedAssetsOperation != null) && this.m_currUnloadUnusedAssetsOperation.isDone)
                {
                    this.m_currUnloadUnusedAssetsOperation = null;
                    GC.Collect();
                    this.CleanUnusedAssetsCache();
                    if (this.m_needUnloadAllUnusedBundles)
                    {
                        if (this.UnloadAllUnusedBundles(false))
                        {
                            this.m_needUnloadAllUnusedBundles = false;
                            this.m_needGC = true;
                        }
                        if (this.m_eventOnUnloadUnusedResourceAllCompleted != null)
                        {
                            this.m_eventOnUnloadUnusedResourceAllCompleted();
                            this.m_eventOnUnloadUnusedResourceAllCompleted = null;
                        }
                    }
                }
                if (((this.m_nextUnloadUnusedAssetTime <= BlackJack.BJFramework.Runtime.Timer.m_currTime) && (this.m_loadingOpCount == 0)) && (this.m_currUnloadUnusedAssetsOperation == null))
                {
                    this.m_nextUnloadUnusedAssetTime = BlackJack.BJFramework.Runtime.Timer.m_currTime.AddSeconds((double) this.m_unloadUnusedAssetTimeInterval);
                    this.UnloadUnusedAssets();
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void TickBundle()
        {
            DelegateBridge bridge = __Hotfix_TickBundle;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                if (this.m_touchedBundleSetDirty)
                {
                    this.SaveTouchedBundleSet(false);
                }
                if (this.m_nextTickBundleTime <= BlackJack.BJFramework.Runtime.Timer.m_currTime)
                {
                    this.m_nextTickBundleTime = BlackJack.BJFramework.Runtime.Timer.m_currTime.AddSeconds(15.0);
                    this.UnloadAllUnusedBundles(true);
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void TickGC()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickMemUseage()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickReserve()
        {
            DelegateBridge bridge = __Hotfix_TickReserve;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else
            {
                DateTime currTime = BlackJack.BJFramework.Runtime.Timer.m_currTime;
                if (this.m_reserveTickDelayOutTime < currTime)
                {
                    this.m_reserveTickDelayOutTime = currTime.AddSeconds(5.0);
                    foreach (KeyValuePair<string, ReserveItem> pair in this.m_assetReserveDict)
                    {
                        if (!(pair.Value.m_timeOut != DateTime.MinValue))
                        {
                            continue;
                        }
                        if (pair.Value.m_timeOut < currTime)
                        {
                            this.m_assetReserve4Remove.Add(pair.Key);
                        }
                    }
                    if (this.m_assetReserve4Remove.Count != 0)
                    {
                        foreach (string str in this.m_assetReserve4Remove)
                        {
                            this.RemoveReserve(str);
                        }
                        this.m_assetReserve4Remove.Clear();
                    }
                }
            }
        }

        [MethodImpl(0x8000)]
        protected void TickVersionUpdate()
        {
            DelegateBridge bridge = __Hotfix_TickVersionUpdate;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
            else if (this.m_enableCheckVersionUpdateInRuning)
            {
                if (this.m_nextTickVersionUpdateTime == DateTime.MinValue)
                {
                    this.m_nextTickVersionUpdateTime = DateTime.Now.AddSeconds((double) this.m_checkVersionUpdateInRuningCD);
                }
                else if (this.m_nextTickVersionUpdateTime <= DateTime.Now)
                {
                    this.m_nextTickVersionUpdateTime = DateTime.Now.AddSeconds((double) this.m_checkVersionUpdateInRuningCD);
                    this.StartCheckBundleDataVersion(delegate (bool checkResult, int errCode) {
                    }, true);
                }
            }
        }

        [MethodImpl(0x8000)]
        public string TryGetResaveFile(string srcDir, string relativePath)
        {
        }

        [MethodImpl(0x8000)]
        public void Uninitlize()
        {
        }

        [MethodImpl(0x8000)]
        public void UnloadAllBundle()
        {
        }

        [MethodImpl(0x8000)]
        protected bool UnloadAllUnusedBundles(bool onlyTimeOut = false)
        {
            DelegateBridge bridge = __Hotfix_UnloadAllUnusedBundles;
            return ((bridge == null) || bridge.__Gen_Delegate_Imp103(this, onlyTimeOut));
        }

        [MethodImpl(0x8000)]
        public void UnloadAsset(UnityEngine.Object asset)
        {
        }

        [MethodImpl(0x8000)]
        public bool UnloadUnusedAssets()
        {
            DelegateBridge bridge = __Hotfix_UnloadUnusedAssets;
            if (bridge != null)
            {
                return bridge.__Gen_Delegate_Imp9(this);
            }
            if (this.m_loadingOpCount != 0)
            {
                Debug.LogError("UnloadUnusedAssets fail, m_loadingOpCount != 0" + this.m_loadingOpCount.ToString());
                return false;
            }
            if (this.m_currUnloadUnusedAssetsOperation == null)
            {
                this.m_currUnloadUnusedAssetsOperation = Resources.UnloadUnusedAssets();
            }
            return true;
        }

        [MethodImpl(0x8000)]
        public bool UnloadUnusedResourceAll(Action onComplete = null, HashSet<string> keepReserve = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregBundleLoadingCtx(BundleLoadingCtx bundleLoadingCtx)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator WaitForTimeSecond(float time)
        {
        }

        public static BlackJack.BJFramework.Runtime.Resource.ResourceManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
                DelegateBridge bridge = __Hotfix_get_Instance;
                return ((bridge == null) ? m_instance : bridge.__Gen_Delegate_Imp129());
            }
        }

        public RMState State
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
                DelegateBridge bridge = __Hotfix_get_State;
                return ((bridge == null) ? this.<State>k__BackingField : bridge.__Gen_Delegate_Imp135(this));
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
                DelegateBridge bridge = __Hotfix_set_State;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp136(this, value);
                }
                else
                {
                    this.<State>k__BackingField = value;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <AssetBundleManifestLoadingWorker>c__IteratorA : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal BundleData.SingleBundleData <singleBundleData>__0;
            internal IEnumerator <lbIter>__0;
            internal AssetBundleManifest[] <assets>__0;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <AssetBundleManifestLoadingWorker>c__AnonStorey1F $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <AssetBundleManifestLoadingWorker>c__AnonStorey1F();
                        this.$locvar0.<>f__ref$10 = this;
                        this.<singleBundleData>__0 = this.$this.m_bundleDataHelper.GetBundleDataByName(this.$this.GetAssetBundleManifestBundleName());
                        this.$locvar0.manifestBundle = null;
                        this.<lbIter>__0 = this.$this.LoadBundleFromWWWOrStreamingImpl(this.<singleBundleData>__0, false, true, new Action<AssetBundle>(this.$locvar0.<>m__0));
                        this.$current = this.<lbIter>__0;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        if (this.$locvar0.manifestBundle == null)
                        {
                            this.$this.OnAssetBundleManifestLoadingWorkerEnd(false);
                        }
                        else
                        {
                            Debug.Log("AssetBundleManifestLoadingWorker download AssetBundleManifest ok");
                            this.<assets>__0 = this.$locvar0.manifestBundle.LoadAllAssets<AssetBundleManifest>();
                            if (this.<assets>__0.Length <= 0)
                            {
                                Debug.LogError("BundleDataLoadingWorker can not find AssetBundleManifest in bundle ");
                                this.$this.OnAssetBundleManifestLoadingWorkerEnd(false);
                            }
                            else
                            {
                                this.$this.m_assetBundleManifest = this.<assets>__0[0];
                                Debug.Log("AssetBundleManifestLoadingWorker m_assetBundleManifest load ok");
                                this.$locvar0.manifestBundle.Unload(false);
                                this.$this.OnAssetBundleManifestLoadingWorkerEnd(true);
                                this.$PC = -1;
                            }
                        }
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <AssetBundleManifestLoadingWorker>c__AnonStorey1F
            {
                internal AssetBundle manifestBundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<AssetBundleManifestLoadingWorker>c__IteratorA <>f__ref$10;

                internal void <>m__0(AssetBundle lbundle)
                {
                    this.manifestBundle = lbundle;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <AssetBundleUpdateingWorker>c__IteratorB : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal BundleData.SingleBundleData <singleBundleData>__1;
            internal Action<bool> onEnd;
            internal string <url>__1;
            internal bool <isDownloadSuccess>__1;
            internal int <retryCount>__1;
            internal float <retryTimeDelay>__1;
            internal long <currDownloadByte>__2;
            internal WWW <www>__2;
            internal long <wwwBytesDownloaded>__3;
            internal Action<BundleData.SingleBundleData> onSingleBundleDownloadSuccess;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        break;

                    case 1:
                        goto TR_001B;

                    case 2:
                    case 3:
                        goto TR_001D;

                    case 4:
                        goto TR_0010;

                    default:
                        goto TR_0000;
                }
                goto TR_0021;
            TR_0000:
                return false;
            TR_0003:
                if (this.<isDownloadSuccess>__1)
                {
                    goto TR_0021;
                }
                else
                {
                    this.onEnd(false);
                }
                goto TR_0000;
            TR_0004:
                return true;
            TR_0009:
                Debug.LogError("AssetBundleUpdateingWorker download  fail url = " + this.<url>__1);
                this.<retryCount>__1--;
                this.$this.m_assetBundleUpdateingDownloadedByte -= this.<currDownloadByte>__2;
                this.$current = this.$this.WaitForTimeSecond(this.<retryTimeDelay>__1);
                if (!this.$disposing)
                {
                    this.$PC = 3;
                }
                goto TR_0004;
            TR_0010:
                this.<www>__2.assetBundle.Unload(true);
                Debug.Log("AssetBundleUpdateingWorker download ok " + this.<url>__1);
                if (this.onSingleBundleDownloadSuccess != null)
                {
                    this.onSingleBundleDownloadSuccess(this.<singleBundleData>__1);
                }
                this.<isDownloadSuccess>__1 = true;
                goto TR_0003;
            TR_001B:
                if (!this.<www>__2.isDone && string.IsNullOrEmpty(this.<www>__2.error))
                {
                    this.$this.m_assetBundleUpdateingDownloadedByte -= this.<currDownloadByte>__2;
                    this.<wwwBytesDownloaded>__3 = (long) (this.<singleBundleData>__1.m_size * this.<www>__2.progress);
                    this.$this.m_assetBundleUpdateingDownloadedByte += this.<wwwBytesDownloaded>__3;
                    this.<currDownloadByte>__2 = this.<wwwBytesDownloaded>__3;
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                }
                else if (string.IsNullOrEmpty(this.<www>__2.error))
                {
                    if (!this.<www>__2.isDone)
                    {
                        goto TR_0009;
                    }
                    else if (this.<www>__2.assetBundle != null)
                    {
                        this.$this.m_assetBundleUpdateingDownloadedByte -= this.<currDownloadByte>__2;
                        this.$this.m_assetBundleUpdateingDownloadedByte += this.<singleBundleData>__1.m_size;
                        if (!this.<singleBundleData>__1.m_isResaveFileBundle)
                        {
                            goto TR_0010;
                        }
                        else
                        {
                            this.$current = this.$this.DealWithResaveFileInBundle(this.<www>__2.assetBundle, this.$this.m_bundleDataHelper);
                            if (!this.$disposing)
                            {
                                this.$PC = 4;
                            }
                        }
                    }
                    else
                    {
                        goto TR_0009;
                    }
                }
                else
                {
                    Debug.LogError(this.<www>__2.error);
                    this.<retryCount>__1--;
                    this.$this.m_assetBundleUpdateingDownloadedByte -= this.<currDownloadByte>__2;
                    this.$current = this.$this.WaitForTimeSecond(this.<retryTimeDelay>__1);
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                }
                goto TR_0004;
            TR_001D:
                if (this.<retryCount>__1 > 0)
                {
                    Debug.Log($"Start download bundle:{this.<singleBundleData>__1.m_bundleName}, retryCount = {this.<retryCount>__1}, version = {this.<singleBundleData>__1.m_version}");
                    this.<currDownloadByte>__2 = 0L;
                    this.<www>__2 = WWW.LoadFromCacheOrDownload(this.<url>__1, this.<singleBundleData>__1.m_version, this.<singleBundleData>__1.m_bundleCRC);
                }
                else
                {
                    goto TR_0003;
                }
                goto TR_001B;
            TR_0021:
                while (true)
                {
                    this.<singleBundleData>__1 = this.$this.GetSingleBundleData4PreUpdate();
                    if (this.<singleBundleData>__1 != null)
                    {
                        this.<url>__1 = this.$this.GetDownloadBundleUrl(this.<singleBundleData>__1.m_bundleName);
                        Debug.Log("AssetBundleUpdateingWorker download start " + this.<url>__1);
                        this.<isDownloadSuccess>__1 = false;
                        this.<retryCount>__1 = GameManager.Instance.GameClientSetting.ResourcesSetting.BundlePreUpdateRetryCount;
                        this.<retryTimeDelay>__1 = GameManager.Instance.GameClientSetting.ResourcesSetting.BundlePreUpdateRetryTimeDelay;
                    }
                    else
                    {
                        this.onEnd(true);
                        goto TR_0000;
                    }
                    break;
                }
                goto TR_001D;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <BundleDataLoadingWorker>c__IteratorD : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal uint <urlBundleDataCRC>__0;
            internal int <lastPreUpdateBundleDataVersion>__0;
            internal int <streamingAssetsBundleDataVersion>__0;
            internal int <bundleDataVersion4Loading>__1;
            internal IEnumerator <iter>__2;
            internal string <bundleDataBundleUrl>__3;
            internal IEnumerator <iter>__4;
            internal IEnumerator <iter>__5;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <BundleDataLoadingWorker>c__AnonStorey21 $locvar0;
            private <BundleDataLoadingWorker>c__AnonStorey22 $locvar1;
            private <BundleDataLoadingWorker>c__AnonStorey23 $locvar2;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<urlBundleDataCRC>__0 = 0;
                        this.<lastPreUpdateBundleDataVersion>__0 = PlayerPrefs.GetInt("PreUpdateVersion4BundleData");
                        this.<streamingAssetsBundleDataVersion>__0 = this.$this.m_streamingAssetsBundleData.m_version;
                        this.$this.m_currBundleDataVersion = PlayerPrefs.GetInt("BundleDataVersion");
                        this.$this.m_currBundleDataVersion = (this.$this.m_currBundleDataVersion != 0) ? this.$this.m_currBundleDataVersion : this.<streamingAssetsBundleDataVersion>__0;
                        if (!this.$this.m_disableAssetBundleDownload)
                        {
                            this.$locvar0 = new <BundleDataLoadingWorker>c__AnonStorey21();
                            this.$locvar0.<>f__ref$13 = this;
                            this.$locvar0.urlBundleDataVersion = 0;
                            this.$locvar0.checkComplete = false;
                            this.<iter>__2 = this.$this.CheckBundleDataVersionFromUrl(new Action<bool, int, bool, int>(this.$locvar0.<>m__0), null, 0x1388);
                        }
                        else
                        {
                            this.<bundleDataVersion4Loading>__1 = this.$this.m_currBundleDataVersion;
                            this.<bundleDataVersion4Loading>__1 = (this.<bundleDataVersion4Loading>__1 <= this.<streamingAssetsBundleDataVersion>__0) ? this.<streamingAssetsBundleDataVersion>__0 : this.<bundleDataVersion4Loading>__1;
                            this.<bundleDataVersion4Loading>__1 = (this.<bundleDataVersion4Loading>__1 <= this.<lastPreUpdateBundleDataVersion>__0) ? this.<lastPreUpdateBundleDataVersion>__0 : this.<bundleDataVersion4Loading>__1;
                            this.$this.m_bundleData = this.$this.m_streamingAssetsBundleData;
                            Debug.Log($"ResourceManager.StartBundleDataLoading download skip load from streamingassets url bundleDataVersion4Loading = m_currBundleDataVersion = {this.<bundleDataVersion4Loading>__1}");
                            goto TR_0001;
                        }
                        goto TR_0014;

                    case 1:
                        goto TR_0014;

                    case 2:
                        if (this.$locvar1.assetBundle != null)
                        {
                            Debug.Log("BundleDataLoadingWorker download m_lastPreUpdateBundleData bundle ok");
                            this.$this.m_lastPreUpdateBundleData = this.$locvar1.assetBundle.LoadAsset<BundleData>(PathHelper.BundleDataAssetName);
                            if (this.$this.m_lastPreUpdateBundleData != null)
                            {
                                Debug.Log($"BundleDataLoadingWorker load m_lastPreUpdateBundleData from {PathHelper.BundleDataAssetName} ok, Ver={this.$this.m_lastPreUpdateBundleData.m_version}.");
                                this.$locvar1.assetBundle.Unload(false);
                                if (this.$this.m_lastPreUpdateBundleData.m_version != this.<lastPreUpdateBundleDataVersion>__0)
                                {
                                    this.$this.m_lastPreUpdateBundleData = null;
                                }
                                goto TR_0008;
                            }
                            else
                            {
                                Debug.LogError("BundleDataLoadingWorker LoadAsset for old m_bundleData fail");
                                this.$this.OnBundleDataLoadingWorkerEnd(false);
                            }
                        }
                        else
                        {
                            Debug.LogError("BundleDataLoadingWorker download old BundleDataBundle  fail url = " + this.<bundleDataBundleUrl>__3);
                            this.$this.OnBundleDataLoadingWorkerEnd(false);
                        }
                        break;

                    case 3:
                        if (this.$locvar2.assetBundle == null)
                        {
                            Debug.LogError("BundleDataLoadingWorker download newest BundleDataBundle  fail url = " + this.<bundleDataBundleUrl>__3);
                            this.$this.OnBundleDataLoadingWorkerEnd(false);
                            break;
                        }
                        Debug.Log("BundleDataLoadingWorker download newest BundleDataBundle ok");
                        this.$this.m_bundleData = this.$locvar2.assetBundle.LoadAsset<BundleData>(PathHelper.BundleDataAssetName);
                        if (this.$this.m_bundleData == null)
                        {
                            Debug.LogError("BundleDataLoadingWorker LoadAsset for m_bundleData fail");
                            this.$this.OnBundleDataLoadingWorkerEnd(false);
                            break;
                        }
                        Debug.Log("BundleDataLoadingWorker load m_bundleData ok");
                        this.$locvar2.assetBundle.Unload(false);
                        goto TR_0001;

                    default:
                        break;
                }
            TR_0000:
                return false;
            TR_0001:
                this.$this.m_currBundleDataVersion = this.$this.m_bundleData.m_version;
                PlayerPrefs.SetInt("BundleDataVersion", this.$this.m_bundleData.m_version);
                PlayerPrefs.Save();
                Debug.Log($"Set PlayerPrefs {"BundleDataVersion"}={this.$this.m_bundleData.m_version}");
                this.$this.m_bundleDataHelper = new BundleDataHelper(this.$this.m_bundleData, this.$this.m_assetPathIgnoreCase);
                this.$this.OnBundleDataLoadingWorkerEnd(true);
                this.$PC = -1;
                goto TR_0000;
            TR_0003:
                return true;
            TR_0008:
                this.$locvar2 = new <BundleDataLoadingWorker>c__AnonStorey23();
                this.$locvar2.<>f__ref$13 = this;
                Debug.Log($"BundleDataLoadingWorker LoadFromCacheOrDownload for new m_bundleData {this.<bundleDataBundleUrl>__3} {this.<bundleDataVersion4Loading>__1}");
                this.$locvar2.assetBundle = null;
                this.<iter>__5 = this.$this.LoadBundleFromCacheOrDownload(this.<bundleDataBundleUrl>__3, this.<bundleDataVersion4Loading>__1, this.<urlBundleDataCRC>__0, new Action<AssetBundle>(this.$locvar2.<>m__0));
                this.$current = this.<iter>__5;
                if (!this.$disposing)
                {
                    this.$PC = 3;
                }
                goto TR_0003;
            TR_0014:
                if (this.<iter>__2.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    goto TR_0003;
                }
                else
                {
                    if (!this.$locvar0.checkComplete)
                    {
                        this.$this.OnBundleDataLoadingWorkerEnd(false);
                    }
                    if (this.$locvar0.urlBundleDataVersion == this.$this.m_currBundleDataVersion)
                    {
                        Debug.Log("BundleDataLoadingWorker urlBundleDataVersion = m_currBundleDataVersion = " + this.$this.m_currBundleDataVersion.ToString());
                    }
                    this.<bundleDataVersion4Loading>__1 = this.$locvar0.urlBundleDataVersion;
                    this.<bundleDataBundleUrl>__3 = this.$this.GetDownloadBundleUrl(PathHelper.BundleDataBundleName);
                    if ((this.<lastPreUpdateBundleDataVersion>__0 != 0) && (this.<lastPreUpdateBundleDataVersion>__0 != this.<bundleDataVersion4Loading>__1))
                    {
                        this.$locvar1 = new <BundleDataLoadingWorker>c__AnonStorey22();
                        this.$locvar1.<>f__ref$13 = this;
                        Debug.Log($"BundleDataLoadingWorker LoadFromCacheOrDownload for m_oldBundleData {this.<bundleDataBundleUrl>__3} {this.<lastPreUpdateBundleDataVersion>__0}");
                        this.$locvar1.assetBundle = null;
                        this.<iter>__4 = this.$this.LoadBundleFromCacheOrDownload(this.<bundleDataBundleUrl>__3, this.<lastPreUpdateBundleDataVersion>__0, 0, new Action<AssetBundle>(this.$locvar1.<>m__0));
                        this.$current = this.<iter>__4;
                        if (!this.$disposing)
                        {
                            this.$PC = 2;
                        }
                        goto TR_0003;
                    }
                }
                goto TR_0008;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <BundleDataLoadingWorker>c__AnonStorey21
            {
                internal bool checkComplete;
                internal int urlBundleDataVersion;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<BundleDataLoadingWorker>c__IteratorD <>f__ref$13;

                internal void <>m__0(bool lret, int lversion, bool lCheckRet, int lerrCode)
                {
                    this.checkComplete = lret;
                    this.urlBundleDataVersion = lversion;
                }
            }

            private sealed class <BundleDataLoadingWorker>c__AnonStorey22
            {
                internal AssetBundle assetBundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<BundleDataLoadingWorker>c__IteratorD <>f__ref$13;

                internal void <>m__0(AssetBundle lbundle)
                {
                    this.assetBundle = lbundle;
                }
            }

            private sealed class <BundleDataLoadingWorker>c__AnonStorey23
            {
                internal AssetBundle assetBundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<BundleDataLoadingWorker>c__IteratorD <>f__ref$13;

                internal void <>m__0(AssetBundle lbundle)
                {
                    this.assetBundle = lbundle;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CalcPreUpdateBundleListByBundleData>c__AnonStorey20
        {
            internal BundleData.SingleBundleData newBundleData;

            internal bool <>m__0(BundleData.SingleBundleData ldata) => 
                (ldata.m_bundleName == this.newBundleData.m_bundleName);
        }

        [CompilerGenerated]
        private sealed class <CheckBundleDataVersionFromUrl>c__IteratorE : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool, int, bool, int> onEnd;
            internal int <streamingAssetsBundleDataVersion>__0;
            internal string independentUrl;
            internal string <bundleDataVersionUrl>__0;
            internal DateTime <wwwStartTime>__1;
            internal bool <isTimeOut>__1;
            internal WWW <wwwBundleDataVersion>__1;
            internal int timeout;
            internal string[] <strVals>__1;
            internal int <urlBundleDataVersion>__1;
            internal uint <urlBundleDataCRC>__1;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.$this.m_disableAssetBundle)
                        {
                            goto TR_0001;
                        }
                        else if (!this.$this.m_disableAssetBundleDownload)
                        {
                            if (this.$this.m_streamingAssetsBundleData != null)
                            {
                                this.<streamingAssetsBundleDataVersion>__0 = this.$this.m_streamingAssetsBundleData.m_version;
                                this.<bundleDataVersionUrl>__0 = !string.IsNullOrEmpty(this.independentUrl) ? this.independentUrl : this.$this.GetDownloadBundleUrl(PathHelper.BundleDataVersionFileName);
                                Debug.Log("CheckBundleDataVersionFromUrl start download BundleDataVersion url = " + this.<bundleDataVersionUrl>__0);
                                if (!string.IsNullOrEmpty(this.<bundleDataVersionUrl>__0))
                                {
                                    this.$this.m_retryDownloadBundleDataVersionLeftCount = 5;
                                }
                                else
                                {
                                    this.onEnd(false, 0, false, -1);
                                    goto TR_0000;
                                }
                            }
                            else
                            {
                                this.onEnd(true, 0, true, 0);
                                goto TR_0000;
                            }
                        }
                        else
                        {
                            goto TR_0001;
                        }
                        goto TR_0021;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                goto TR_001C;
            TR_0000:
                return false;
            TR_0001:
                this.onEnd(true, 0, true, 0);
                goto TR_0000;
            TR_0004:
                this.$PC = -1;
                goto TR_0000;
            TR_000C:
                if (this.<isTimeOut>__1)
                {
                    Debug.LogError("CheckBundleDataVersionFromUrl::TimeOut " + this.timeout);
                }
                else
                {
                    Debug.LogError("CheckBundleDataVersionFromUrl::Error " + this.<wwwBundleDataVersion>__1.error);
                }
                this.$this.m_retryDownloadBundleDataVersionLeftCount--;
                if (this.$this.m_retryDownloadBundleDataVersionLeftCount <= 0)
                {
                    this.onEnd(false, 0, false, -1);
                }
                else
                {
                    this.<wwwBundleDataVersion>__1.Dispose();
                    Thread.Sleep(0x3e8);
                    goto TR_0021;
                }
                goto TR_0000;
            TR_0011:
                char[] chArray1 = new char[] { ',' };
                this.<strVals>__1 = this.<wwwBundleDataVersion>__1.text.Split(chArray1);
                this.<urlBundleDataVersion>__1 = int.Parse((this.<strVals>__1.Length <= 0) ? string.Empty : this.<strVals>__1[0]);
                this.<urlBundleDataCRC>__1 = uint.Parse((this.<strVals>__1.Length <= 1) ? string.Empty : this.<strVals>__1[1]);
                Debug.Log($"CheckBundleDataVersionFromUrl BundleDataVersion: New = {this.<urlBundleDataVersion>__1}; Last = {this.$this.m_currBundleDataVersion}, Install ={this.<streamingAssetsBundleDataVersion>__0}, NewCRC = {this.<urlBundleDataCRC>__1}");
                this.onEnd(true, this.<urlBundleDataVersion>__1, this.<urlBundleDataVersion>__1 == this.$this.m_currBundleDataVersion, 0);
                goto TR_0004;
            TR_001C:
                if ((!this.<wwwBundleDataVersion>__1.isDone && !this.<isTimeOut>__1) && string.IsNullOrEmpty(this.<wwwBundleDataVersion>__1.error))
                {
                    this.<isTimeOut>__1 = (DateTime.Now - this.<wwwStartTime>__1).TotalMilliseconds >= this.timeout;
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                if (!string.IsNullOrEmpty(this.<wwwBundleDataVersion>__1.error))
                {
                    goto TR_000C;
                }
                else if (!this.<isTimeOut>__1)
                {
                    if (!this.<wwwBundleDataVersion>__1.isDone)
                    {
                        goto TR_0011;
                    }
                    else if (!string.IsNullOrEmpty(this.<wwwBundleDataVersion>__1.text))
                    {
                        goto TR_0011;
                    }
                    else
                    {
                        Debug.LogError("CheckBundleDataVersionFromUrl download BundleDataVersion  fail url = " + this.<bundleDataVersionUrl>__0);
                        this.$this.m_retryDownloadBundleDataVersionLeftCount--;
                        if (this.$this.m_retryDownloadBundleDataVersionLeftCount <= 0)
                        {
                            this.onEnd(false, 0, false, -1);
                        }
                        else
                        {
                            this.<wwwBundleDataVersion>__1.Dispose();
                            Thread.Sleep(0x3e8);
                            goto TR_0021;
                        }
                    }
                }
                else
                {
                    goto TR_000C;
                }
                goto TR_0000;
            TR_0021:
                while (true)
                {
                    if (this.$this.m_retryDownloadBundleDataVersionLeftCount >= 0)
                    {
                        this.<wwwStartTime>__1 = DateTime.Now;
                        this.<isTimeOut>__1 = false;
                        this.<wwwBundleDataVersion>__1 = null;
                        try
                        {
                            this.<wwwBundleDataVersion>__1 = new WWW(this.<bundleDataVersionUrl>__0);
                        }
                        catch (Exception exception)
                        {
                            this.onEnd(false, 0, false, -1);
                            Debug.LogError($"{"ResourceManager.CheckBundleDataVersionFromUrl"} : {exception}");
                            goto TR_0000;
                        }
                    }
                    else
                    {
                        goto TR_0004;
                    }
                    break;
                }
                goto TR_001C;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DealWithResaveFileInBundle>c__Iterator8 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal AssetBundle assetBundle;
            internal BundleDataHelper bundleHelper;
            internal BundleData.SingleBundleData <bundleData>__0;
            internal int <dealedAssetSize>__0;
            internal System.Collections.Generic.List<string>.Enumerator $locvar0;
            internal string <assetName>__1;
            internal ResaveFileSctipableObject <asset>__2;
            internal string <destPathName>__2;
            internal string <destPath>__2;
            internal byte[] <bytes>__2;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        Debug.Log($"DealWithResaveFileInBundle bundle.name = {this.assetBundle.name}");
                        this.<bundleData>__0 = this.bundleHelper.GetBundleDataByName(this.assetBundle.name);
                        this.<dealedAssetSize>__0 = 0;
                        if (this.$this.m_enableDetailResourceManagerLog)
                        {
                            Debug.Log("DealWithResaveFileInBundle start resave file");
                        }
                        this.$locvar0 = this.<bundleData>__0.m_assetList.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        default:
                            if (!this.$locvar0.MoveNext())
                            {
                                break;
                            }
                            this.<assetName>__1 = this.$locvar0.Current;
                            if (this.$this.m_enableDetailResourceManagerLog)
                            {
                                Debug.Log($"DealWithResaveFileInBundle assetName: {this.<assetName>__1} start");
                            }
                            this.<asset>__2 = this.assetBundle.LoadAsset<ResaveFileSctipableObject>(this.<assetName>__1);
                            if (this.$this.m_enableDetailResourceManagerLog)
                            {
                                Debug.Log($"DealWithResaveFileInBundle assetName: {this.<assetName>__1} end");
                            }
                            this.<destPathName>__2 = $"{Application.persistentDataPath}/{GameManager.Instance.GameClientSetting.ResourcesSetting.ResaveFileDestDir}/{this.<asset>__2.m_fileSrcPath}";
                            this.<destPathName>__2 = this.<destPathName>__2.Replace('\\', '/');
                            if (this.$this.m_enableDetailResourceManagerLog)
                            {
                                Debug.Log($"DealWithResaveFileInBundle fileName: {this.<destPathName>__2}");
                            }
                            this.<destPath>__2 = Path.GetDirectoryName(this.<destPathName>__2);
                            if (!Directory.Exists(this.<destPath>__2))
                            {
                                Directory.CreateDirectory(this.<destPath>__2);
                            }
                            this.<bytes>__2 = this.<asset>__2.GetBytes();
                            File.WriteAllBytes(this.<destPathName>__2, this.<bytes>__2);
                            this.<dealedAssetSize>__0 += this.<bytes>__2.Length;
                            Resources.UnloadAsset(this.<asset>__2);
                            if (this.<dealedAssetSize>__0 > 0x989680)
                            {
                                Debug.Log("DealWithResaveFileInBundle GC.Collect();");
                                this.<dealedAssetSize>__0 = 0;
                            }
                            Debug.Log($"DealWithResaveFileInBundle fileName: {this.<destPathName>__2} success");
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            flag = true;
                            return true;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                Debug.Log("DealWithResaveFileInBundle end GC.Collect();");
                GC.Collect();
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DealWithResaveFileInBundleAfterStreamingAssetProcessing>c__Iterator11 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal System.Collections.Generic.List<StreamingAssetsFileList.ListItem>.Enumerator $locvar0;
            internal StreamingAssetsFileList.ListItem <fileItem>__1;
            internal BundleData.SingleBundleData <singleBundleData>__2;
            internal AssetBundle <assetBundle>__2;
            internal string <srcPath>__2;
            internal AssetBundleCreateRequest <req>__2;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                    case 2:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = this.$this.m_streamingAssetsFileList.m_fileList.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                    case 2:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        case 1:
                            goto TR_0011;

                        case 2:
                            this.<assetBundle>__2.Unload(false);
                            break;

                        default:
                            break;
                    }
                    goto TR_001A;
                TR_0011:
                    if (!this.<req>__2.isDone)
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        flag = true;
                        goto TR_0006;
                    }
                    else
                    {
                        this.<assetBundle>__2 = this.<req>__2.assetBundle;
                        if (this.<assetBundle>__2 != null)
                        {
                            object[] paramList = new object[] { "DealWithResaveFileInBundle start, bundleName = {0}", this.<fileItem>__1.m_bundleName };
                            Debug.LogWarning(paramList);
                            this.$current = this.$this.DealWithResaveFileInBundle(this.<assetBundle>__2, this.$this.m_streamingAssetsBundleDataHelper);
                            if (!this.$disposing)
                            {
                                this.$PC = 2;
                            }
                            flag = true;
                            goto TR_0006;
                        }
                        else
                        {
                            Debug.Log($"Cannot Load Resave Bundle File:{this.<srcPath>__2}");
                        }
                    }
                TR_001A:
                    while (true)
                    {
                        if (!this.$locvar0.MoveNext())
                        {
                            break;
                        }
                        this.<fileItem>__1 = this.$locvar0.Current;
                        if ((this.<fileItem>__1.m_bundleName != PathHelper.BundleDataBundleName) && (this.<fileItem>__1.m_filePath.IndexOf(PathHelper.StreamingAssetsBundlePathDirName) != -1))
                        {
                            this.<singleBundleData>__2 = this.$this.m_streamingAssetsBundleDataHelper.GetBundleDataByName(this.<fileItem>__1.m_bundleName);
                            if (this.<singleBundleData>__2 == null)
                            {
                                UnityEngine.Debug.LogError($"singleBundleData is null, bundleName = {this.<fileItem>__1.m_bundleName}");
                                continue;
                            }
                            if (this.<singleBundleData>__2.m_isResaveFileBundle)
                            {
                                this.<assetBundle>__2 = null;
                                this.<srcPath>__2 = this.$this.GetStreamingAssetBundlePath(this.<fileItem>__1.m_bundleName);
                                this.<req>__2 = AssetBundle.LoadFromFileAsync(this.<srcPath>__2);
                                goto TR_0011;
                            }
                        }
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                this.$PC = -1;
            TR_0000:
                return false;
            TR_0006:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DownloadResaveFile>c__Iterator9 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal bool <ret>__0;
            internal HashSet<string> relativePaths;
            internal HashSet<string>.Enumerator $locvar0;
            internal string <relativePath>__1;
            internal string srcDir;
            internal string <path>__2;
            internal BundleData.SingleBundleData <singleBundleData>__2;
            internal IEnumerator <iter>__2;
            internal Action<string, bool> onComplete;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <DownloadResaveFile>c__AnonStorey1E $locvar1;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                    case 2:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<ret>__0 = true;
                        this.$locvar0 = this.relativePaths.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                    case 2:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        case 1:
                            goto TR_0013;

                        default:
                            break;
                    }
                    goto TR_001A;
                TR_0013:
                    if (this.<iter>__2.MoveNext())
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        flag = true;
                        goto TR_0009;
                    }
                    else if (this.$locvar1.bundle != null)
                    {
                        this.$current = this.$this.DealWithResaveFileInBundle(this.$locvar1.bundle, this.$this.m_bundleDataHelper);
                        if (!this.$disposing)
                        {
                            this.$PC = 2;
                        }
                        flag = true;
                        goto TR_0009;
                    }
                    else
                    {
                        Debug.LogError($"Load Resave Bundle {this.<relativePath>__1} failed");
                        this.<ret>__0 = false;
                    }
                TR_001A:
                    while (true)
                    {
                        if (!this.$locvar0.MoveNext())
                        {
                            break;
                        }
                        this.<relativePath>__1 = this.$locvar0.Current;
                        this.$locvar1 = new <DownloadResaveFile>c__AnonStorey1E();
                        this.$locvar1.<>f__ref$9 = this;
                        this.<path>__2 = this.$this.TryGetResaveFile(this.srcDir, this.<relativePath>__1);
                        if (this.<path>__2 == null)
                        {
                            this.<singleBundleData>__2 = this.$this.GetResaveFileBundleDataByPath(this.<relativePath>__1);
                            if (this.<singleBundleData>__2 == null)
                            {
                                Debug.LogError($"Load Resave BundleData {this.<relativePath>__1} failed");
                                this.<ret>__0 = false;
                                continue;
                            }
                            this.$locvar1.bundle = null;
                            this.<iter>__2 = this.$this.LoadBundle(this.<singleBundleData>__2, false, new Action<AssetBundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem>(this.$locvar1.<>m__0));
                            goto TR_0013;
                        }
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                this.$this.UnloadAllUnusedBundles(false);
                if (this.onComplete != null)
                {
                    this.onComplete(this.srcDir, this.<ret>__0);
                }
                this.$PC = -1;
            TR_0000:
                return false;
            TR_0009:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <DownloadResaveFile>c__AnonStorey1E
            {
                internal AssetBundle bundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<DownloadResaveFile>c__Iterator9 <>f__ref$9;

                internal void <>m__0(AssetBundle lbundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem lcacheItem)
                {
                    this.bundle = lbundle;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <IsBundleNeedLoadFromStreamingAssetFile>c__AnonStorey19
        {
            internal BundleData.SingleBundleData streamingAssetsBundleDataItem;

            internal bool <>m__0(StreamingAssetsFileList.ListItem item) => 
                (item.m_bundleName == this.streamingAssetsBundleDataItem.m_bundleName);
        }

        [CompilerGenerated]
        private sealed class <LoadAsset>c__Iterator0<T> : IEnumerator, IDisposable, IEnumerator<object> where T: UnityEngine.Object
        {
            internal string path;
            internal T <asset>__0;
            internal Action<string, T> onCompleted;
            internal int <atIndexInPath>__0;
            internal string <assetPath>__1;
            internal string <subAssetName>__1;
            internal bool <hasSubAssetName>__0;
            internal bool <isNeedLoadAllRes>__0;
            internal bool noErrlog;
            internal bool loadAsync;
            internal IEnumerator <iter>__2;
            internal UnityEngine.Object <mainAsset>__3;
            internal GameObject <prefab>__4;
            internal PrefabResourceContainerBase[] <resourceContanierList>__5;
            internal PrefabResourceContainerBase[] $locvar2;
            internal int $locvar3;
            internal PrefabResourceContainerBase <resContainer>__6;
            internal IEnumerator <iter>__7;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadAsset>c__AnonStorey13<T> $locvar4;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar4 = new <LoadAsset>c__AnonStorey13<T>();
                        this.$locvar4.<>f__ref$0 = (BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadAsset>c__Iterator0<T>) this;
                        if (this.$this.m_enableDetailResourceManagerLog)
                        {
                            Debug.Log($"LoadAsset {this.path}");
                        }
                        if (Application.isEditor)
                        {
                            this.$this.m_loadedAssetPathSet.Add(this.path);
                        }
                        this.$this.m_loadingOpCount++;
                        this.<asset>__0 = this.$this.GetAssetFromCache(this.path) as T;
                        if (this.<asset>__0 == null)
                        {
                            this.<atIndexInPath>__0 = this.path.IndexOf('@');
                            if (this.<atIndexInPath>__0 == -1)
                            {
                                this.<assetPath>__1 = this.path;
                                this.<subAssetName>__1 = null;
                            }
                            else
                            {
                                this.<assetPath>__1 = this.path.Substring(0, this.<atIndexInPath>__0);
                                this.<subAssetName>__1 = this.path.Substring(this.<atIndexInPath>__0 + 1);
                            }
                            this.<hasSubAssetName>__0 = !string.IsNullOrEmpty(this.<subAssetName>__1);
                            this.<isNeedLoadAllRes>__0 = this.$this.IsNeedLoadAllForAssetPath(this.<assetPath>__1);
                            if (this.<hasSubAssetName>__0 && !this.<isNeedLoadAllRes>__0)
                            {
                                this.$this.m_loadingOpCount--;
                                throw new ApplicationException($"[{this.path}] hasSubAssetName but not in fbx");
                            }
                            this.$locvar4.bundleCacheItem = null;
                            this.$locvar4.allAsset = null;
                            if (this.$this.State == BlackJack.BJFramework.Runtime.Resource.ResourceManager.RMState.AssetBundleManifestLoadEnd)
                            {
                                if (Application.isEditor && !this.$this.m_loadAssetFromBundleInEditor)
                                {
                                    goto TR_0042;
                                }
                                this.<iter>__2 = this.$this.LoadAssetFromBundle(this.<assetPath>__1, new Action<string, UnityEngine.Object[], BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem>(this.$locvar4.<>m__1), this.noErrlog, this.loadAsync);
                            }
                            else
                            {
                                this.$this.LoadAssetByResourcesLoad<T>(this.<assetPath>__1, new Action<string, UnityEngine.Object[]>(this.$locvar4.<>m__0), this.noErrlog, this.<isNeedLoadAllRes>__0);
                                goto TR_003A;
                            }
                            goto TR_0047;
                        }
                        else
                        {
                            this.$this.m_loadingOpCount--;
                            this.onCompleted(this.path, this.<asset>__0);
                            if (this.$this.EventOnAssetLoaded != null)
                            {
                                this.$this.EventOnAssetLoaded();
                            }
                        }
                        break;

                    case 1:
                        goto TR_000E;

                    case 2:
                        goto TR_0047;

                    default:
                        break;
                }
            TR_0000:
                return false;
            TR_0007:
                this.onCompleted(this.path, this.<asset>__0);
                if (this.$this.EventOnAssetLoaded != null)
                {
                    this.$this.EventOnAssetLoaded();
                }
                this.$PC = -1;
                goto TR_0000;
            TR_000A:
                return true;
            TR_000E:
                if (this.<iter>__7.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    goto TR_000A;
                }
                else
                {
                    this.$locvar3++;
                }
            TR_0011:
                while (true)
                {
                    if (this.$locvar3 >= this.$locvar2.Length)
                    {
                        break;
                    }
                    this.<resContainer>__6 = this.$locvar2[this.$locvar3];
                    this.<iter>__7 = this.<resContainer>__6.RecacheAllAssetsFromResourceManager();
                    goto TR_000E;
                }
                goto TR_0007;
            TR_003A:
                if ((this.$locvar4.allAsset != null) && (this.$locvar4.allAsset.Length != 0))
                {
                    if (!this.<hasSubAssetName>__0)
                    {
                        this.<asset>__0 = this.$locvar4.allAsset[0] as T;
                    }
                    else
                    {
                        if ((this.$locvar4.allAsset.Length == 2) && ((this.$locvar4.allAsset[1] is Texture2D) || (this.$locvar4.allAsset[0] is Sprite)))
                        {
                            UnityEngine.Object obj2 = this.$locvar4.allAsset[0];
                            this.$locvar4.allAsset[0] = this.$locvar4.allAsset[1];
                            this.$locvar4.allAsset[1] = obj2;
                        }
                        this.<mainAsset>__3 = this.$locvar4.allAsset[0];
                        UnityEngine.Object[] allAsset = this.$locvar4.allAsset;
                        int index = 0;
                        while (true)
                        {
                            if (index < allAsset.Length)
                            {
                                UnityEngine.Object obj3 = allAsset[index];
                                if (((obj3 == null) || (obj3 == this.<mainAsset>__3)) || (obj3.name != this.<subAssetName>__1))
                                {
                                    index++;
                                    continue;
                                }
                                this.<asset>__0 = obj3 as T;
                            }
                            this.<asset>__0 = (this.<asset>__0 == null) ? (this.<mainAsset>__3 as T) : this.<asset>__0;
                            break;
                        }
                    }
                    if (this.<asset>__0 == null)
                    {
                        if (!this.noErrlog)
                        {
                            Debug.LogError($"LoadAsset fail for {this.path}");
                        }
                        else
                        {
                            Debug.Log($"LoadAsset fail for {this.path}");
                        }
                    }
                    for (int i = 0; i < this.$locvar4.allAsset.Length; i++)
                    {
                        UnityEngine.Object asset = this.$locvar4.allAsset[i];
                        if (asset != null)
                        {
                            if (asset is Sprite)
                            {
                                this.$this.PushAssetToCache($"{this.<assetPath>__1}@{asset.name}", asset, this.$locvar4.bundleCacheItem);
                            }
                            else if (i == 0)
                            {
                                this.$this.PushAssetToCache(this.<assetPath>__1, asset, this.$locvar4.bundleCacheItem);
                            }
                            else if (asset != null)
                            {
                                this.$this.PushAssetToCache($"{this.<assetPath>__1}@{asset.name}", asset, this.$locvar4.bundleCacheItem);
                            }
                        }
                    }
                }
                this.$this.m_loadingOpCount--;
                if (Application.isEditor && ((GameManager.Instance == null) || !GameManager.Instance.GameClientSetting.ResourcesSetting.LoadAssetFromBundleInEditor))
                {
                    goto TR_0007;
                }
                this.<prefab>__4 = this.<asset>__0 as GameObject;
                if (this.<prefab>__4 == null)
                {
                    goto TR_0007;
                }
                else
                {
                    this.<resourceContanierList>__5 = this.<prefab>__4.GetComponentsInChildren<PrefabResourceContainerBase>(true);
                    this.$locvar2 = this.<resourceContanierList>__5;
                    this.$locvar3 = 0;
                }
                goto TR_0011;
            TR_0042:
                if (Application.isEditor && ((this.$locvar4.allAsset == null) || (this.$locvar4.allAsset.Length == 0)))
                {
                    this.$this.LoadAssetByAssetDatabase<T>(this.<assetPath>__1, new Action<string, UnityEngine.Object[]>(this.$locvar4.<>m__2), this.noErrlog, this.<isNeedLoadAllRes>__0);
                }
                if ((this.$locvar4.allAsset == null) || (this.$locvar4.allAsset.Length == 0))
                {
                    this.$this.LoadAssetByResourcesLoad<T>(this.<assetPath>__1, new Action<string, UnityEngine.Object[]>(this.$locvar4.<>m__3), this.noErrlog, this.<isNeedLoadAllRes>__0);
                }
                goto TR_003A;
            TR_0047:
                if (this.<iter>__2.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                    goto TR_000A;
                }
                else
                {
                    this.$locvar4.bundleCacheItem = (this.$locvar4.allAsset == null) ? null : this.$locvar4.bundleCacheItem;
                }
                goto TR_0042;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadAsset>c__AnonStorey13
            {
                internal UnityEngine.Object[] allAsset;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem bundleCacheItem;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadAsset>c__Iterator0<T> <>f__ref$0;

                internal void <>m__0(string lPath, UnityEngine.Object[] lAllAsset)
                {
                    this.allAsset = lAllAsset;
                }

                internal void <>m__1(string lPath, UnityEngine.Object[] lAllAsset, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem lBundleCacheItem)
                {
                    this.allAsset = lAllAsset;
                    this.bundleCacheItem = lBundleCacheItem;
                }

                internal void <>m__2(string lPath, UnityEngine.Object[] lAllAsset)
                {
                    this.allAsset = lAllAsset;
                }

                internal void <>m__3(string lPath, UnityEngine.Object[] lAllAsset)
                {
                    this.allAsset = lAllAsset;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetFromBundle>c__Iterator3 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string path;
            internal BundleData.SingleBundleData <bundleData>__0;
            internal Action<string, UnityEngine.Object[], BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem> onCompleted;
            internal bool loadAync;
            internal IEnumerator <iter>__1;
            internal bool noErrlog;
            internal string <assetName>__0;
            internal AssetBundleRequest <assetReqIter>__2;
            internal UnityEngine.Object[] <allAssets>__2;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadAssetFromBundle>c__AnonStorey1A $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <LoadAssetFromBundle>c__AnonStorey1A();
                        this.$locvar0.<>f__ref$3 = this;
                        this.<bundleData>__0 = this.$this.GetBundleDataByAssetPath(this.path);
                        if (this.<bundleData>__0 != null)
                        {
                            if (this.$this.m_enableDetailResourceManagerLog)
                            {
                                Debug.Log($"LoadAssetFromBundle  start bundlename={this.<bundleData>__0.m_bundleName} path = {this.path}");
                            }
                            this.$locvar0.bundle = null;
                            this.$locvar0.bundleCacheItem = this.$this.GetAssetBundleFromCache(this.<bundleData>__0.m_bundleName);
                            if (this.$locvar0.bundleCacheItem != null)
                            {
                                this.$locvar0.bundle = this.$locvar0.bundleCacheItem.m_bundle;
                                goto TR_0017;
                            }
                            else
                            {
                                this.<iter>__1 = this.$this.LoadBundle(this.<bundleData>__0, this.loadAync, new Action<AssetBundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem>(this.$locvar0.<>m__0));
                            }
                        }
                        else
                        {
                            if (this.$this.m_enableDetailResourceManagerLog)
                            {
                                Debug.Log("LoadAssetFromBundle::bundleData == null, 堆栈: " + StackTraceUtility.ExtractStackTrace());
                            }
                            Debug.LogError($"LoadAssetFromBundle bundleData=null {this.path}");
                            this.onCompleted(this.path, null, null);
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        break;

                    case 2:
                        goto TR_0014;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__1.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    goto TR_0010;
                }
                else if (this.$locvar0.bundle != null)
                {
                    goto TR_0017;
                }
                else
                {
                    if (!this.noErrlog)
                    {
                        Debug.LogError($"LoadAssetFromBundle LoadBundle fail {this.<bundleData>__0.m_bundleName}");
                    }
                    else
                    {
                        Debug.Log($"LoadAssetFromBundle LoadBundle fail {this.<bundleData>__0.m_bundleName}");
                    }
                    this.onCompleted(this.path, null, null);
                }
            TR_0000:
                return false;
            TR_000E:
                if ((this.<allAssets>__2 != null) && (this.<allAssets>__2.Length > 0))
                {
                    this.onCompleted(this.path, this.<allAssets>__2, this.$locvar0.bundleCacheItem);
                    this.$PC = -1;
                }
                else
                {
                    if (!this.noErrlog)
                    {
                        Debug.LogError($"LoadAssetFromBundle bundle.LoadAsset fail {this.<assetName>__0} {this.<bundleData>__0.m_bundleName}");
                    }
                    else
                    {
                        Debug.Log($"LoadAssetFromBundle bundle.LoadAsset fail {this.<assetName>__0} {this.<bundleData>__0.m_bundleName}");
                    }
                    this.onCompleted(this.path, null, null);
                }
                goto TR_0000;
            TR_0010:
                return true;
            TR_0014:
                if (!this.<assetReqIter>__2.isDone)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                }
                else
                {
                    this.<allAssets>__2 = this.<assetReqIter>__2.allAssets;
                    goto TR_000E;
                }
                goto TR_0010;
            TR_0017:
                this.<assetName>__0 = this.$this.GetAssetNameByPath(this.path);
                if (!this.loadAync)
                {
                    this.<allAssets>__2 = this.$locvar0.bundle.LoadAssetWithSubAssets(this.<assetName>__0);
                    goto TR_000E;
                }
                else
                {
                    this.<assetReqIter>__2 = this.$locvar0.bundle.LoadAssetWithSubAssetsAsync(this.<assetName>__0);
                }
                goto TR_0014;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadAssetFromBundle>c__AnonStorey1A
            {
                internal AssetBundle bundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem bundleCacheItem;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadAssetFromBundle>c__Iterator3 <>f__ref$3;

                internal void <>m__0(AssetBundle lbundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem lbundleCacheItem)
                {
                    this.bundle = lbundle;
                    this.bundleCacheItem = lbundleCacheItem;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetsCorutine>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal int <i>__0;
            internal DateTime <startTime>__0;
            internal HashSet<string> pathList;
            internal HashSet<string>.Enumerator $locvar0;
            internal string <path>__1;
            internal int corutineId;
            internal bool loadAsync;
            internal IEnumerator <iter>__2;
            internal IDictionary<string, UnityEngine.Object> assetDict;
            internal TimeSpan <timeSpan>__2;
            internal Action onComplete;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadAssetsCorutine>c__AnonStorey17 $locvar1;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                    case 2:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.<i>__0 = -1;
                        this.<startTime>__0 = DateTime.Now;
                        this.$this.m_loadingOpCount++;
                        this.$locvar0 = this.pathList.GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                    case 2:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        case 1:
                            goto TR_0011;

                        default:
                            break;
                    }
                    goto TR_0017;
                TR_0011:
                    if (this.<iter>__2.MoveNext())
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        flag = true;
                        goto TR_0004;
                    }
                    else
                    {
                        if (this.assetDict != null)
                        {
                            this.assetDict[this.<path>__1] = this.$locvar1.asset;
                        }
                        this.<timeSpan>__2 = DateTime.Now - this.<startTime>__0;
                        if (this.<timeSpan>__2.TotalSeconds > 0.33000001311302185)
                        {
                            this.<startTime>__0 = DateTime.Now;
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 2;
                            }
                            flag = true;
                            goto TR_0004;
                        }
                    }
                TR_0017:
                    while (true)
                    {
                        if (!this.$locvar0.MoveNext())
                        {
                            break;
                        }
                        this.<path>__1 = this.$locvar0.Current;
                        this.$locvar1 = new <LoadAssetsCorutine>c__AnonStorey17();
                        this.$locvar1.<>f__ref$1 = this;
                        this.<i>__0++;
                        if ((this.corutineId == 0x7fffffff) || ((this.<i>__0 % 2) == this.corutineId))
                        {
                            this.$locvar1.asset = null;
                            this.<iter>__2 = this.$this.LoadAsset<UnityEngine.Object>(this.<path>__1, new Action<string, UnityEngine.Object>(this.$locvar1.<>m__0), false, this.loadAsync);
                            goto TR_0011;
                        }
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                this.onComplete();
                this.$this.m_loadingOpCount--;
                this.$PC = -1;
            TR_0000:
                return false;
            TR_0004:
                return true;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadAssetsCorutine>c__AnonStorey17
            {
                internal UnityEngine.Object asset;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadAssetsCorutine>c__Iterator1 <>f__ref$1;

                internal void <>m__0(string lpath, UnityEngine.Object lasset)
                {
                    this.asset = lasset;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadAssetSync>c__AnonStorey14<T> where T: UnityEngine.Object
        {
            internal T asset;

            internal void <>m__0(string p, T a)
            {
                this.asset = a;
            }
        }

        [CompilerGenerated]
        private sealed class <LoadBundle>c__Iterator5 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal BundleData.SingleBundleData bundleData;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem <bundleCatchItem>__0;
            internal Action<AssetBundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem> onComplete;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleLoadingCtx <bundleLoadingCtx>__0;
            internal bool <alreadyInLoading>__0;
            internal float <startWaitTime>__1;
            internal string[] <dependenceList>__0;
            internal System.Collections.Generic.List<BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem> <dependBundleCacheList>__0;
            internal string[] $locvar0;
            internal int $locvar1;
            internal string <dependence>__2;
            internal bool loadAync;
            internal IEnumerator <iter>__3;
            internal bool <isNewTouchedBundle>__0;
            internal IEnumerator <lbIter>__0;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadBundle>c__AnonStorey1D $locvar3;
            private <LoadBundle>c__AnonStorey1C $locvar4;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar3 = new <LoadBundle>c__AnonStorey1D();
                        this.$locvar3.<>f__ref$5 = this;
                        this.<bundleCatchItem>__0 = this.$this.GetAssetBundleFromCache(this.bundleData.m_bundleName);
                        if (this.<bundleCatchItem>__0 == null)
                        {
                            if (this.$this.m_enableDetailResourceManagerLog)
                            {
                                Debug.Log($"LoadBundle start {this.bundleData.m_bundleName} {this.bundleData.m_version}");
                            }
                            this.<alreadyInLoading>__0 = this.$this.RegBundleLoadingCtx(this.bundleData.m_bundleName, out this.<bundleLoadingCtx>__0);
                            if (!this.<alreadyInLoading>__0)
                            {
                                this.<dependenceList>__0 = this.$this.m_assetBundleManifest.GetDirectDependencies(this.bundleData.m_bundleName);
                                this.<dependBundleCacheList>__0 = null;
                                if (this.<dependenceList>__0 == null)
                                {
                                    goto TR_001F;
                                }
                                else if (this.<dependenceList>__0.Length == 0)
                                {
                                    goto TR_001F;
                                }
                                else
                                {
                                    if (this.$this.m_enableDetailResourceManagerLog)
                                    {
                                        Debug.LogWarning("LoadBundle load dependence bundles for " + this.bundleData.m_bundleName);
                                    }
                                    this.$locvar0 = this.<dependenceList>__0;
                                    this.$locvar1 = 0;
                                }
                            }
                            else
                            {
                                Debug.Log($"LoadBundle start wait for LoadingCtx {this.bundleData.m_bundleName}");
                                this.<startWaitTime>__1 = Time.time;
                                goto TR_0009;
                            }
                            goto TR_002B;
                        }
                        else
                        {
                            this.onComplete(this.<bundleCatchItem>__0.m_bundle, this.<bundleCatchItem>__0);
                        }
                        goto TR_0000;

                    case 1:
                        goto TR_0009;

                    case 2:
                        goto TR_0028;

                    case 3:
                        goto TR_001E;

                    case 4:
                        break;

                    default:
                        goto TR_0000;
                }
                goto TR_000F;
            TR_0000:
                return false;
            TR_0004:
                return true;
            TR_0009:
                if (!this.<bundleLoadingCtx>__0.m_isEnd)
                {
                    if (Time.time <= (this.<startWaitTime>__1 + 20f))
                    {
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        goto TR_0004;
                    }
                    else
                    {
                        object[] paramList = new object[] { "LoadBundle Waiting LoadingCtx {0} time out! May exist bundle circulate reference!", this.<bundleLoadingCtx>__0.m_bundleName };
                        Debug.LogError(paramList);
                        this.onComplete(null, null);
                    }
                }
                else
                {
                    this.onComplete(this.<bundleLoadingCtx>__0.m_bundle, this.$this.GetAssetBundleFromCache(this.bundleData.m_bundleName));
                    this.$this.UnregBundleLoadingCtx(this.<bundleLoadingCtx>__0);
                }
                goto TR_0000;
            TR_000F:
                this.$this.UnregBundleLoadingCtx(this.<bundleLoadingCtx>__0);
                this.onComplete(this.$locvar3.loadedBundle, this.<bundleCatchItem>__0);
                this.$PC = -1;
                goto TR_0000;
            TR_001E:
                if (this.<lbIter>__0.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 3;
                    }
                }
                else
                {
                    if (this.$locvar3.loadedBundle != null)
                    {
                        this.<bundleLoadingCtx>__0.m_isEnd = true;
                        this.<bundleLoadingCtx>__0.m_bundle = this.$locvar3.loadedBundle;
                        this.<bundleCatchItem>__0 = this.$this.PushAssetBundleToCache(this.bundleData.m_bundleName, this.$locvar3.loadedBundle);
                        this.<bundleCatchItem>__0.m_dependBundleCacheList = this.<dependBundleCacheList>__0;
                        if (this.<dependBundleCacheList>__0 != null)
                        {
                            foreach (BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem item in this.<dependBundleCacheList>__0)
                            {
                                item.AddRefrence();
                            }
                        }
                        if (!this.<isNewTouchedBundle>__0)
                        {
                            goto TR_000F;
                        }
                        else if (!this.bundleData.m_isResaveFileBundle)
                        {
                            goto TR_000F;
                        }
                        else
                        {
                            this.$current = this.$this.DealWithResaveFileInBundle(this.$locvar3.loadedBundle, this.$this.m_bundleDataHelper);
                            if (!this.$disposing)
                            {
                                this.$PC = 4;
                            }
                        }
                        goto TR_0004;
                    }
                    else
                    {
                        this.<bundleLoadingCtx>__0.m_isEnd = true;
                        this.onComplete(null, null);
                        this.$this.UnregBundleLoadingCtx(this.<bundleLoadingCtx>__0);
                    }
                    goto TR_0000;
                }
                goto TR_0004;
            TR_001F:
                this.<isNewTouchedBundle>__0 = this.$this.AddBundle2TouchedBundleSet(this.bundleData.m_bundleName, true);
                this.$locvar3.loadedBundle = null;
                this.<lbIter>__0 = this.$this.LoadBundleFromWWWOrStreamingImpl(this.bundleData, this.loadAync, true, new Action<AssetBundle>(this.$locvar3.<>m__0));
                goto TR_001E;
            TR_0028:
                if (this.<iter>__3.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                    goto TR_0004;
                }
                else if (this.$locvar4.dependBundle != null)
                {
                    if (this.<dependBundleCacheList>__0 == null)
                    {
                        this.<dependBundleCacheList>__0 = new System.Collections.Generic.List<BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem>();
                    }
                    this.<dependBundleCacheList>__0.Add(this.$locvar4.dependCacheItem);
                    this.$locvar1++;
                    goto TR_002B;
                }
                else
                {
                    Debug.LogError($"LoadBundle fail by load dependence fail {this.bundleData.m_bundleName} {this.bundleData.m_version}");
                    this.onComplete(null, null);
                }
                goto TR_0000;
            TR_002B:
                while (true)
                {
                    if (this.$locvar1 >= this.$locvar0.Length)
                    {
                        break;
                    }
                    this.<dependence>__2 = this.$locvar0[this.$locvar1];
                    this.$locvar4 = new <LoadBundle>c__AnonStorey1C();
                    this.$locvar4.<>f__ref$5 = this;
                    this.$locvar4.dependBundle = null;
                    this.$locvar4.dependCacheItem = null;
                    this.<iter>__3 = this.$this.LoadBundle(this.<dependence>__2, this.loadAync, new Action<AssetBundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem>(this.$locvar4.<>m__0), false);
                    goto TR_0028;
                }
                goto TR_001F;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadBundle>c__AnonStorey1C
            {
                internal AssetBundle dependBundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem dependCacheItem;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadBundle>c__Iterator5 <>f__ref$5;

                internal void <>m__0(AssetBundle lbundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem lbundleCache)
                {
                    this.dependBundle = lbundle;
                    this.dependCacheItem = lbundleCache;
                }
            }

            private sealed class <LoadBundle>c__AnonStorey1D
            {
                internal AssetBundle loadedBundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadBundle>c__Iterator5 <>f__ref$5;

                internal void <>m__0(AssetBundle lbundle)
                {
                    this.loadedBundle = lbundle;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadBundle>c__Iterator6 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string bundleName;
            internal BundleData.SingleBundleData <bundleData>__0;
            internal bool noErrlog;
            internal Action<AssetBundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem> onComplete;
            internal bool loadAync;
            internal IEnumerator <iter>__0;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<bundleData>__0 = this.$this.m_bundleDataHelper.GetBundleDataByName(this.bundleName);
                        if (this.<bundleData>__0 != null)
                        {
                            this.<iter>__0 = this.$this.LoadBundle(this.<bundleData>__0, this.loadAync, this.onComplete);
                        }
                        else
                        {
                            if (!this.noErrlog)
                            {
                                Debug.LogError($"LoadBundle fail {this.bundleName}");
                            }
                            else
                            {
                                Debug.Log($"LoadBundle fail {this.bundleName}");
                            }
                            this.onComplete(null, null);
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__0.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <LoadBundle4UnityScene>c__Iterator4 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string scenePath;
            internal BundleData.SingleBundleData <bundleData>__0;
            internal Action<string, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem> onComplete;
            internal bool loadAync;
            internal IEnumerator <iter>__1;
            internal bool noErrlog;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadBundle4UnityScene>c__AnonStorey1B $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <LoadBundle4UnityScene>c__AnonStorey1B();
                        this.$locvar0.<>f__ref$4 = this;
                        Debug.Log($"LoadBundle4UnityScene start {this.scenePath}");
                        this.<bundleData>__0 = this.$this.GetBundleDataByAssetPath(this.scenePath);
                        if (this.<bundleData>__0 != null)
                        {
                            Debug.Log($"LoadBundle4UnityScene bundlename={this.<bundleData>__0.m_bundleName} {this.scenePath}");
                            this.$locvar0.bundle = null;
                            this.$locvar0.bundleCacheItem = this.$this.GetAssetBundleFromCache(this.<bundleData>__0.m_bundleName);
                            if (this.$locvar0.bundleCacheItem != null)
                            {
                                goto TR_0006;
                            }
                            else
                            {
                                this.<iter>__1 = this.$this.LoadBundle(this.<bundleData>__0, this.loadAync, new Action<AssetBundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem>(this.$locvar0.<>m__0));
                            }
                        }
                        else
                        {
                            Debug.LogError($"LoadBundle4UnityScene bundleData=null {this.scenePath}");
                            this.onComplete(this.scenePath, null);
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__1.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                if (this.$locvar0.bundle == null)
                {
                    if (!this.noErrlog)
                    {
                        Debug.LogError($"LoadBundle4UnityScene LoadBundle fail {this.<bundleData>__0.m_bundleName}");
                    }
                    else
                    {
                        Debug.Log($"LoadBundle4UnityScene LoadBundle fail {this.<bundleData>__0.m_bundleName}");
                    }
                    this.onComplete(this.scenePath, null);
                }
                else
                {
                    goto TR_0006;
                }
            TR_0000:
                return false;
            TR_0006:
                this.onComplete(this.scenePath, this.$locvar0.bundleCacheItem);
                this.$PC = -1;
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadBundle4UnityScene>c__AnonStorey1B
            {
                internal AssetBundle bundle;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem bundleCacheItem;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadBundle4UnityScene>c__Iterator4 <>f__ref$4;

                internal void <>m__0(AssetBundle lbundle, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem lbundleCacheItem)
                {
                    this.bundle = lbundle;
                    this.bundleCacheItem = lbundleCacheItem;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LoadBundleFromCacheOrDownload>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string url;
            internal int bundleVersion;
            internal uint bundleCRC;
            internal WWW <www>__0;
            internal Action<AssetBundle> onEnd;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<www>__0 = WWW.LoadFromCacheOrDownload(this.url, this.bundleVersion, this.bundleCRC);
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (!this.<www>__0.isDone && string.IsNullOrEmpty(this.<www>__0.error))
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                if (!string.IsNullOrEmpty(this.<www>__0.error))
                {
                    Debug.LogError(this.<www>__0.error);
                    this.onEnd(null);
                }
                else
                {
                    this.onEnd(this.<www>__0.assetBundle);
                    this.$PC = -1;
                }
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <LoadBundleFromWWWOrStreamingImpl>c__Iterator7 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal BundleData.SingleBundleData bundleData;
            internal string <bundleName>__0;
            internal BundleData.SingleBundleData <realBundleData>__0;
            internal AssetBundle <loadedBundle>__0;
            internal bool ignoreCRC;
            internal uint <crc>__0;
            internal string <path>__1;
            internal bool loadAync;
            internal AssetBundleCreateRequest <req>__2;
            internal Action<AssetBundle> onComplete;
            internal string <url>__0;
            internal WWW <www>__0;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<bundleName>__0 = this.$this.GetBundleNameByCurrLocalization(this.bundleData);
                        this.<realBundleData>__0 = this.$this.m_bundleDataHelper.GetBundleDataByName(this.<bundleName>__0);
                        this.<loadedBundle>__0 = null;
                        this.<crc>__0 = !this.ignoreCRC ? this.<realBundleData>__0.m_bundleCRC : 0;
                        if (!this.$this.IsBundleNeedLoadFromStreamingAssetFile(this.<realBundleData>__0.m_bundleName, this.<realBundleData>__0.m_version))
                        {
                            goto TR_0019;
                        }
                        else
                        {
                            this.<path>__1 = this.$this.GetStreamingAssetBundlePath(this.<realBundleData>__0.m_bundleName);
                            if (this.$this.m_enableDetailResourceManagerLog)
                            {
                                Debug.Log($"LoadBundleFromWWWOrStreamingImpl LoadFromFileAsync {this.<path>__1} {this.<crc>__0}");
                            }
                            if (!this.loadAync)
                            {
                                this.<loadedBundle>__0 = AssetBundle.LoadFromFile(this.<path>__1);
                                goto TR_001B;
                            }
                            else
                            {
                                this.<req>__2 = AssetBundle.LoadFromFileAsync(this.<path>__1);
                            }
                        }
                        break;

                    case 1:
                        break;

                    case 2:
                        goto TR_0016;

                    default:
                        goto TR_0000;
                }
                if (!this.<req>__2.isDone)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    goto TR_000E;
                }
                else if (this.<req>__2.assetBundle != null)
                {
                    this.<loadedBundle>__0 = this.<req>__2.assetBundle;
                }
                else
                {
                    object[] paramList = new object[] { "LoadBundleFromWWWOrStreamingImpl LoadFromFileAsync  fail {0} {1}", this.<path>__1, this.<crc>__0 };
                    Debug.LogError(paramList);
                    this.onComplete(null);
                    goto TR_0000;
                }
                goto TR_001B;
            TR_0000:
                return false;
            TR_000D:
                if (!string.IsNullOrEmpty(this.<www>__0.error))
                {
                    Debug.LogError(this.<www>__0.error);
                    this.onComplete(null);
                }
                else if (!this.<www>__0.isDone || (this.<www>__0.assetBundle == null))
                {
                    Debug.LogError($"LoadBundleFromWWWOrStreamingImpl: error={this.<www>__0.error}, url={this.<url>__0}, ver={this.<realBundleData>__0.m_version}");
                    this.onComplete(null);
                }
                else
                {
                    this.<loadedBundle>__0 = this.<www>__0.assetBundle;
                    if (this.$this.m_enableDetailResourceManagerLog)
                    {
                        Debug.Log($"LoadBundleFromWWWOrStreamingImpl LoadFromCacheOrDownload success {this.<url>__0} {this.<realBundleData>__0.m_version}");
                    }
                    this.onComplete(this.<loadedBundle>__0);
                    this.$PC = -1;
                }
                goto TR_0000;
            TR_000E:
                return true;
            TR_0016:
                while (true)
                {
                    if (this.<www>__0.isDone)
                    {
                        goto TR_000D;
                    }
                    else if (string.IsNullOrEmpty(this.<www>__0.error))
                    {
                        if (!this.loadAync)
                        {
                            Thread.Sleep(1);
                            continue;
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 2;
                        }
                    }
                    else
                    {
                        goto TR_000D;
                    }
                    break;
                }
                goto TR_000E;
            TR_0019:
                this.<url>__0 = this.$this.GetBundleLoadingUrl(this.<realBundleData>__0.m_bundleName);
                if (this.$this.m_enableDetailResourceManagerLog)
                {
                    Debug.Log($"LoadBundleFromWWWOrStreamingImpl LoadFromCacheOrDownload {this.<url>__0} {this.<crc>__0}");
                }
                this.<www>__0 = WWW.LoadFromCacheOrDownload(this.<url>__0, this.<realBundleData>__0.m_version, this.<crc>__0);
                goto TR_0016;
            TR_001B:
                if (this.<loadedBundle>__0 == null)
                {
                    goto TR_0019;
                }
                else
                {
                    if (this.$this.m_enableDetailResourceManagerLog)
                    {
                        Debug.Log($"LoadBundleFromWWWOrStreamingImpl LoadFromFileAsync Success {this.<path>__1} {this.<crc>__0}");
                    }
                    this.onComplete(this.<loadedBundle>__0);
                }
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <LoadUnityScene>c__Iterator12 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string path;
            internal string <sceneName>__0;
            internal Action<string, Scene?> onCompleted;
            internal AsyncOperation <asyncOp>__0;
            internal bool noErrlog;
            internal bool loadAync;
            internal IEnumerator <iter>__1;
            internal Scene <scene>__0;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <LoadUnityScene>c__AnonStorey27 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                Scene? nullable;
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <LoadUnityScene>c__AnonStorey27();
                        this.$locvar0.<>f__ref$18 = this;
                        Debug.Log($"LoadUnityScene {this.path}");
                        if (Application.isEditor)
                        {
                            this.$this.m_loadedAssetPathSet.Add(this.path);
                        }
                        this.$this.m_loadingOpCount++;
                        this.<sceneName>__0 = Path.GetFileNameWithoutExtension(this.path);
                        if (this.$this.State == BlackJack.BJFramework.Runtime.Resource.ResourceManager.RMState.AssetBundleManifestLoadEnd)
                        {
                            this.$locvar0.ret = false;
                            this.<asyncOp>__0 = null;
                            if (Application.isEditor && !this.$this.m_loadAssetFromBundleInEditor)
                            {
                                goto TR_0005;
                            }
                            this.<iter>__1 = this.$this.LoadBundle4UnityScene(this.path, new Action<string, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem>(this.$locvar0.<>m__0), this.noErrlog, this.loadAync);
                        }
                        else
                        {
                            Debug.LogError("LoadUnityScene but  State != RMState.Ready");
                            nullable = null;
                            this.onCompleted(this.path, nullable);
                            goto TR_0000;
                        }
                        break;

                    case 1:
                        break;

                    case 2:
                        goto TR_000B;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__1.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    goto TR_0007;
                }
                else if (this.$locvar0.ret)
                {
                    this.<asyncOp>__0 = SceneManager.LoadSceneAsync(this.<sceneName>__0, LoadSceneMode.Additive);
                    goto TR_000B;
                }
                else
                {
                    nullable = null;
                    this.onCompleted(this.path, nullable);
                }
            TR_0000:
                return false;
            TR_0005:
                this.<scene>__0 = SceneManager.GetSceneByName(this.<sceneName>__0);
                if (this.<scene>__0.IsValid() && this.<scene>__0.isLoaded)
                {
                }
                this.$this.m_loadingOpCount--;
                this.onCompleted(this.path, new Scene?(this.<scene>__0));
                this.$PC = -1;
                goto TR_0000;
            TR_0007:
                return true;
            TR_000B:
                if (!this.<asyncOp>__0.isDone)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 2;
                    }
                }
                else
                {
                    goto TR_0005;
                }
                goto TR_0007;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <LoadUnityScene>c__AnonStorey27
            {
                internal bool ret;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<LoadUnityScene>c__Iterator12 <>f__ref$18;

                internal void <>m__0(string lpath, BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem bundleCacheItem)
                {
                    this.ret = !ReferenceEquals(bundleCacheItem, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ReserveAsset>c__IteratorF : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string path;
            internal bool noErrlog;
            internal int reserveTime;
            internal IEnumerator <iter>__0;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <ReserveAsset>c__AnonStorey24 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                    {
                        this.$locvar0 = new <ReserveAsset>c__AnonStorey24();
                        this.$locvar0.<>f__ref$15 = this;
                        this.$locvar0.path = this.path;
                        this.$locvar0.reserveTime = this.reserveTime;
                        this.$this.m_loadingOpCount++;
                        bool noErrlog = this.noErrlog;
                        this.<iter>__0 = this.$this.LoadAsset<UnityEngine.Object>(this.$locvar0.path, new Action<string, UnityEngine.Object>(this.$locvar0.<>m__0), noErrlog, false);
                        break;
                    }
                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<iter>__0.MoveNext())
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$this.m_loadingOpCount--;
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <ReserveAsset>c__AnonStorey24
            {
                internal string path;
                internal int reserveTime;
                internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<ReserveAsset>c__IteratorF <>f__ref$15;

                internal void <>m__0(string lpath, UnityEngine.Object lasset)
                {
                    this.<>f__ref$15.$this.AddAsset2Reserve(this.path, this.reserveTime, lasset);
                    if (this.<>f__ref$15.$this.EventOnAssetLoaded != null)
                    {
                        this.<>f__ref$15.$this.EventOnAssetLoaded();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartCheckBundleDataVersion>c__AnonStorey18
        {
            internal Action<bool, int> onCheckEnd;
            internal bool ignoreOnUnreachable;

            internal void <>m__0(bool isComplete, int version, bool isVersionMatch, int errCode)
            {
                if (this.onCheckEnd != null)
                {
                    this.onCheckEnd(!isComplete ? this.ignoreOnUnreachable : isVersionMatch, errCode);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadAssetsCorutine>c__AnonStorey15
        {
            internal int endCout;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager.<StartLoadAssetsCorutine>c__AnonStorey16 <>f__ref$22;

            internal void <>m__0()
            {
                this.endCout++;
                if (this.endCout == 2)
                {
                    this.<>f__ref$22.onComplete();
                }
            }

            internal void <>m__1()
            {
                this.endCout++;
                if (this.endCout == 2)
                {
                    this.<>f__ref$22.onComplete();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadAssetsCorutine>c__AnonStorey16
        {
            internal Action onComplete;
        }

        [CompilerGenerated]
        private sealed class <StartReserveAssetsCorutine>c__AnonStorey25
        {
            internal IDictionary<string, UnityEngine.Object> assetDict;
            internal int reserveTime;
            internal Action onComplete;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;

            internal void <>m__0()
            {
                foreach (KeyValuePair<string, UnityEngine.Object> pair in this.assetDict)
                {
                    this.$this.AddAsset2Reserve(pair.Key, this.reserveTime, pair.Value);
                }
                if (this.onComplete != null)
                {
                    this.onComplete();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartStreamingAssetsFilesProcessing>c__AnonStorey26
        {
            internal BundleData.SingleBundleData singleBundleData;

            internal bool <>m__0(StreamingAssetsFileList.ListItem item) => 
                (item.m_bundleName == this.singleBundleData.m_bundleName);
        }

        [CompilerGenerated]
        private sealed class <StreamingAssetsFileProcessor>c__Iterator10 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal bool <ret>__0;
            internal IEnumerator <iter>__1;
            internal BlackJack.BJFramework.Runtime.Resource.ResourceManager $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<ret>__0 = true;
                        this.<iter>__1 = this.$this.DealWithResaveFileInBundleAfterStreamingAssetProcessing();
                        this.$current = this.<iter>__1;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.$this.OnStreamingAssetsFileProcessorEnd(this.<ret>__0);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <WaitForTimeSecond>c__IteratorC : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal float time;
            internal DateTime <outTime>__0;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        Debug.Log("WaitForTimeSecond: " + this.time);
                        this.<outTime>__0 = DateTime.Now.AddSeconds((double) this.time);
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<outTime>__0 > DateTime.Now)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        protected class AssetCacheItem
        {
            public string m_cacheKey;
            public WeakReference m_weakRefrence;
            public bool m_removeReserveOnHit;
            private static DelegateBridge _c__Hotfix_ctor;

            public AssetCacheItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        protected class BundleCacheItem
        {
            public string m_bundleName;
            public DateTime m_lastTouchTime;
            public DateTime m_timeOutTime;
            public int m_hitCount;
            public int m_refCount;
            public AssetBundle m_bundle;
            public bool m_dontUnload;
            public System.Collections.Generic.List<BlackJack.BJFramework.Runtime.Resource.ResourceManager.BundleCacheItem> m_dependBundleCacheList;
            public static float s_validedHitInterval = 1f;
            public static int m_firstTimeOut = 600;
            public static int m_OnHitTimeOutDelay = 60;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_AddRefrence;
            private static DelegateBridge __Hotfix_RemoveRefrence;

            public BundleCacheItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void AddRefrence()
            {
                DelegateBridge bridge = __Hotfix_AddRefrence;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_refCount++;
                }
            }

            public void RemoveRefrence()
            {
                DelegateBridge bridge = __Hotfix_RemoveRefrence;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_refCount--;
                }
            }
        }

        public class BundleLoadingCtx
        {
            public string m_bundleName;
            public AssetBundle m_bundle;
            public bool m_isEnd;
            public int m_ref;
            private static DelegateBridge _c__Hotfix_ctor;

            public BundleLoadingCtx()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        protected class ReserveItem
        {
            public UnityEngine.Object m_asset;
            public DateTime m_timeOut = DateTime.MinValue;
            private static DelegateBridge _c__Hotfix_ctor;

            public ReserveItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum RMState
        {
            None = 0,
            Inited = 1,
            StreamingAssetsFilesProcessing = 2,
            StreamingAssetsFilesProcessEnd = 3,
            BundleDataLoading = 4,
            BundleDataLoadEnd = 5,
            AssetBundlePreUpdateing = 6,
            AssetBundlePreUpdateEnd = 7,
            AssetBundleManifestLoading = 8,
            AssetBundleManifestLoadEnd = 9,
            Ready = 9
        }
    }
}

