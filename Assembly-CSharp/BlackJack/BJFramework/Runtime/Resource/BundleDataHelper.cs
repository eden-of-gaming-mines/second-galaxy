﻿namespace BlackJack.BJFramework.Runtime.Resource
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class BundleDataHelper
    {
        private BundleData m_internalBundleData;
        private bool m_assetPathIgnoreCase;
        private Dictionary<string, BundleData.SingleBundleData> m_bundleDataDict = new Dictionary<string, BundleData.SingleBundleData>();
        private Dictionary<string, BundleData.SingleBundleData> m_assetPath2BundleDataDict = new Dictionary<string, BundleData.SingleBundleData>(StringComparer.OrdinalIgnoreCase);
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetBundleVersionByName;
        private static DelegateBridge __Hotfix_GetBundleDataByName;
        private static DelegateBridge __Hotfix_GetBundleDataByAssetPath;
        private static DelegateBridge __Hotfix_GetResaveFileBundleDataByPath;

        [MethodImpl(0x8000)]
        public BundleDataHelper(BundleData bundleData, bool assetPathIgnoreCase)
        {
            this.m_internalBundleData = bundleData;
            this.m_assetPathIgnoreCase = assetPathIgnoreCase;
            this.m_internalBundleData.m_bundleList.ForEach(new Action<BundleData.SingleBundleData>(this.<BundleDataHelper>m__0));
            this.m_internalBundleData.m_bundleList.ForEach(new Action<BundleData.SingleBundleData>(this.<BundleDataHelper>m__1));
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp12(this, bundleData, assetPathIgnoreCase);
            }
        }

        [MethodImpl(0x8000), CompilerGenerated]
        private void <BundleDataHelper>m__0(BundleData.SingleBundleData elem)
        {
            this.m_bundleDataDict.Add(elem.m_bundleName, elem);
        }

        [MethodImpl(0x8000), CompilerGenerated]
        private void <BundleDataHelper>m__1(BundleData.SingleBundleData elem)
        {
            <BundleDataHelper>c__AnonStorey0 storey = new <BundleDataHelper>c__AnonStorey0 {
                elem = elem,
                $this = this
            };
            storey.elem.m_assetList.ForEach(new Action<string>(storey.<>m__0));
        }

        [MethodImpl(0x8000)]
        public BundleData.SingleBundleData GetBundleDataByAssetPath(string assetPath)
        {
        }

        [MethodImpl(0x8000)]
        public BundleData.SingleBundleData GetBundleDataByName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public int GetBundleVersionByName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public BundleData.SingleBundleData GetResaveFileBundleDataByPath(string relativePath)
        {
        }

        [CompilerGenerated]
        private sealed class <BundleDataHelper>c__AnonStorey0
        {
            internal BundleData.SingleBundleData elem;
            internal BundleDataHelper $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(string assetPath)
            {
                this.$this.m_assetPath2BundleDataDict.Add(!this.$this.m_assetPathIgnoreCase ? assetPath : assetPath.ToLower(), this.elem);
            }
        }
    }
}

