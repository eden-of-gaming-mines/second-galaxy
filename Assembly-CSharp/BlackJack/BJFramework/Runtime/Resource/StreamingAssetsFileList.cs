﻿namespace BlackJack.BJFramework.Runtime.Resource
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class StreamingAssetsFileList : ScriptableObject
    {
        public int m_version;
        public List<ListItem> m_fileList = new List<ListItem>();

        [Serializable]
        public class ListItem
        {
            public string m_bundleName;
            public int m_bundleVersion;
            public string m_filePath;
            private static DelegateBridge _c__Hotfix_ctor;

            [MethodImpl(0x8000)]
            public ListItem(string path, int version)
            {
            }
        }
    }
}

