﻿namespace BlackJack.BJFramework.Runtime.Resource
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BundleData : ScriptableObject
    {
        public int m_version = 1;
        public int m_basicVersion = 1;
        public List<SingleBundleData> m_bundleList = new List<SingleBundleData>();

        [Serializable]
        public class SingleBundleData
        {
            public string m_bundleName;
            public int m_version = 1;
            public string m_bundleHash;
            public uint m_bundleCRC;
            public long m_size;
            public bool m_isInStreamingAssets;
            public bool m_isInStreamingAssetsOnObb;
            public bool m_isNeedPreUpdateByDefault;
            public bool m_isResaveFileBundle;
            public bool m_isLazyUpdate;
            public string m_localizationKey;
            public List<string> m_assetList = new List<string>();
            private static DelegateBridge _c__Hotfix_ctor;

            [MethodImpl(0x8000)]
            public SingleBundleData()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

