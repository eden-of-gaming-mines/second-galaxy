﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CRIDesc
    {
        public string m_acfFullPath;
        public List<SheetDesc> m_sheetList = new List<SheetDesc>();
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public CRIDesc()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        public class SheetDesc
        {
            public string m_sheetName;
            public string m_acbFullPath;
            public string m_awbFullPath;
            public bool m_multiLanguage;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

