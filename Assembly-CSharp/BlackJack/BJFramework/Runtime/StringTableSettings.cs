﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StringTableSettings
    {
        public List<string> LocalizationList = new List<string>();
        public string LocalizationDefault;
        public bool InitLoadDefaultStringTable = true;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public StringTableSettings()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }
    }
}

