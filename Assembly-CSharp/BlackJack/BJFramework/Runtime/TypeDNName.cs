﻿namespace BlackJack.BJFramework.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class TypeDNName
    {
        public string m_assemblyName;
        public string m_typeFullName;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ToString;

        [MethodImpl(0x8000)]
        public TypeDNName(string typeDNName)
        {
            int index = typeDNName.IndexOf('@');
            if (index == -1)
            {
                this.m_assemblyName = "Assembly-CSharp";
                this.m_typeFullName = typeDNName;
            }
            else
            {
                this.m_assemblyName = typeDNName.Substring(0, index);
                this.m_typeFullName = typeDNName.Substring(index + 1);
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, typeDNName);
                }
            }
        }

        [MethodImpl(0x8000)]
        public override string ToString()
        {
        }
    }
}

