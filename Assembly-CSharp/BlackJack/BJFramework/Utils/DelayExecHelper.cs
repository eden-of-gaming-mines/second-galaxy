﻿namespace BlackJack.BJFramework.Utils
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class DelayExecHelper
    {
        private List<DelayExecItem> m_delayExecList = new List<DelayExecItem>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_DelayExec_1;
        private static DelegateBridge __Hotfix_DelayExec_0;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public DelayExecHelper()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        private void DelayExec(uint execTime, DelayExecItem item)
        {
        }

        [MethodImpl(0x8000)]
        public IDelayExecItem DelayExec(Action action, int delayTime, uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(uint currTime)
        {
            DelegateBridge bridge = __Hotfix_Tick;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp197(this, currTime);
            }
            else
            {
                int num = 0;
                while (true)
                {
                    if (num < this.m_delayExecList.Count)
                    {
                        DelayExecItem item = this.m_delayExecList[num];
                        if (item.m_execTime < currTime)
                        {
                            this.m_delayExecList.Remove(item);
                            num--;
                            item.m_action();
                            if (item.m_continueItem != null)
                            {
                                this.DelayExec(item.m_continueItem.m_execTime, item.m_continueItem);
                            }
                            num++;
                            continue;
                        }
                    }
                    return;
                }
            }
        }

        [Serializable]
        private class DelayExecItem : DelayExecHelper.IDelayExecItem
        {
            public Action m_action;
            public uint m_execTime;
            public DelayExecHelper.DelayExecItem m_continueItem;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_BlackJack.BJFramework.Utils.DelayExecHelper.IDelayExecItem.ContinueWith;

            public DelayExecItem()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            DelayExecHelper.IDelayExecItem DelayExecHelper.IDelayExecItem.ContinueWith(Action action, int delayTime)
            {
                DelegateBridge bridge = __Hotfix_BlackJack.BJFramework.Utils.DelayExecHelper.IDelayExecItem.ContinueWith;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp198(this, action, delayTime);
                }
                this.m_continueItem = new DelayExecHelper.DelayExecItem();
                this.m_continueItem.m_action = action;
                this.m_continueItem.m_execTime = this.m_execTime + ((uint) delayTime);
                return this.m_continueItem;
            }
        }

        public interface IDelayExecItem
        {
            DelayExecHelper.IDelayExecItem ContinueWith(Action action, int delayTime);
        }
    }
}

