﻿namespace BlackJack.SensitiveWords
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class SensitiveWords
    {
        private static Dictionary<char, LinkedList<string>> m_initialChar2Word;
        private static HashSet<string> m_writeListA;
        private static HashSet<string> m_writeListB;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitSensitiveWord;
        private static DelegateBridge __Hotfix_InsertWordByLength;
        private static DelegateBridge __Hotfix_IsSensitiveWord_0;
        private static DelegateBridge __Hotfix_IsSensitiveWord_1;
        private static DelegateBridge __Hotfix_IsSensitiveWord4DomesticPackage_0;
        private static DelegateBridge __Hotfix_IsSensitiveWord4DomesticPackage_1;
        private static DelegateBridge __Hotfix_ReplaceSensitiveWord;
        private static DelegateBridge __Hotfix_ReplaceSensitiveWord4DomesticPackage;
        private static DelegateBridge __Hotfix_CombineReplaceChar;
        private static DelegateBridge __Hotfix_IsSensitiveWordInWriteListA;
        private static DelegateBridge __Hotfix_IsSensitiveWordInWriteListB;

        [MethodImpl(0x8000)]
        private static string CombineReplaceChar(char replaceChar, int nums)
        {
        }

        [MethodImpl(0x8000)]
        public static void InitSensitiveWord(string word, bool isInWriteListA, bool isInWriteListB)
        {
        }

        [MethodImpl(0x8000)]
        private static void InsertWordByLength(LinkedList<string> words, string word)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSensitiveWord(string sentence)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSensitiveWord(string sentence, out string sensitivePart)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSensitiveWord4DomesticPackage(string sentence)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSensitiveWord4DomesticPackage(string sentence, out string sensitivePart)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsSensitiveWordInWriteListA(string sentence, string sentenceWord)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsSensitiveWordInWriteListB(string sentenceWord)
        {
        }

        [MethodImpl(0x8000)]
        public static string ReplaceSensitiveWord(string sentence, char replaceChar = '*')
        {
        }

        [MethodImpl(0x8000)]
        public static string ReplaceSensitiveWord4DomesticPackage(string sentence, char replaceChar = '*')
        {
        }
    }
}

