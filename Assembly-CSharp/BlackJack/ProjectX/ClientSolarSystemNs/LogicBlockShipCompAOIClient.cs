﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompAOIClient : LogicBlockShipCompAOIBase, IClientSpaceAOIWatcher
    {
        protected HashSet<ClientSpaceObject> m_interestList;
        protected HashSet<ILBSpaceTarget> m_currInterestTargetList;
        protected HashSet<ILBSpaceDropBox> m_currInterestDropBoxList;
        protected ClientSelfPlayerSpaceShip m_selfOwnerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_IsObjectInView;
        private static DelegateBridge __Hotfix_GetPlayerShipInView;
        private static DelegateBridge __Hotfix_GetCurrInterestTargetList;
        private static DelegateBridge __Hotfix_GetCurrInterestDropBoxList;
        private static DelegateBridge __Hotfix_EnableAccurateAOI;
        private static DelegateBridge __Hotfix_OnObjectEnterView;
        private static DelegateBridge __Hotfix_OnObjLeaveView;
        private static DelegateBridge __Hotfix_OnTickOver;

        [MethodImpl(0x8000)]
        public override void EnableAccurateAOI(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public override HashSet<ILBSpaceDropBox> GetCurrInterestDropBoxList()
        {
        }

        [MethodImpl(0x8000)]
        public override HashSet<ILBSpaceTarget> GetCurrInterestTargetList()
        {
        }

        [MethodImpl(0x8000)]
        public override ILBInSpacePlayerShip GetPlayerShipInView(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsObjectInView(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnObjectEnterView(ClientSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void OnObjLeaveView(ClientSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTickOver()
        {
        }
    }
}

