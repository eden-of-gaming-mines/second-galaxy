﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ClientStar : ClientSpaceObject
    {
        protected int m_confId;
        protected GDBStarInfo m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetStarResPath;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_get_SimObject;
        private static DelegateBridge __Hotfix_set_SimObject;

        [MethodImpl(0x8000)]
        public ClientStar(uint objId, GDBStarInfo info, IClientSolarSytemSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        public string GetStarResPath()
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GDBStarInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimStar SimObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

