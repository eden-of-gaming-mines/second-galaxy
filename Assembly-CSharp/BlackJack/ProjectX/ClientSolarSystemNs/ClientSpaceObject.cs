﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class ClientSpaceObject : ILBSpaceObject
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private uint <ObjId>k__BackingField;
        protected IClientSolarSytemSpaceContext m_spaceContext;
        protected bool m_waitForRemove;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private SimSpaceObject <SimObject>k__BackingField;
        protected List<IClientSpaceObjectEventListener> m_eventListenerList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddEventListener;
        private static DelegateBridge __Hotfix_AttachSimObject;
        private static DelegateBridge __Hotfix_PrepareRemoveFromSolarSystem;
        private static DelegateBridge __Hotfix_IsWaitForRemove;
        private static DelegateBridge __Hotfix_OnServerSyncEvent;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_CalcDistanceToTarget;
        private static DelegateBridge __Hotfix_CalcDistanceToLocation;
        private static DelegateBridge __Hotfix_GetLocation;
        private static DelegateBridge __Hotfix_GetRotation;
        private static DelegateBridge __Hotfix_get_ObjId;
        private static DelegateBridge __Hotfix_set_ObjId;
        private static DelegateBridge __Hotfix_get_SimObject;
        private static DelegateBridge __Hotfix_set_SimObject;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_get_SmoothLocation;
        private static DelegateBridge __Hotfix_get_Rotation;
        private static DelegateBridge __Hotfix_get_SmoothRotation;
        private static DelegateBridge __Hotfix_get_EventListenerList;

        [MethodImpl(0x8000)]
        public ClientSpaceObject(uint objId, IClientSolarSytemSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        public void AddEventListener(IClientSpaceObjectEventListener eventListener)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void AttachSimObject(SimSpaceObject simObj)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToLocation(Vector3D location, bool considerSpaceObjectCollider = false)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToTarget(ILBSpaceObject target, bool considerSpaceObjectCollider = false)
        {
        }

        public abstract void CollectAllResources(List<string> resList);
        public abstract SpaceObjectType GetInstanceType();
        [MethodImpl(0x8000)]
        public Vector3D GetLocation()
        {
        }

        public abstract string GetName();
        [MethodImpl(0x8000)]
        public Vector3D GetRotation()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWaitForRemove()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnServerSyncEvent(uint eventTime, SyncEventFlag eventFlag)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PrepareRemoveFromSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint deltaMillisecond)
        {
        }

        public uint ObjId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public SimSpaceObject SimObject
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public Vector3D Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Vector3D SmoothLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Vector3D Rotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Vector3D SmoothRotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<IClientSpaceObjectEventListener> EventListenerList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

