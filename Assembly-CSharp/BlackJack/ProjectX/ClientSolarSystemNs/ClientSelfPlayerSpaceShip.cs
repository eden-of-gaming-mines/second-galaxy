﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ClientSelfPlayerSpaceShip : ClientPlayerSpaceShip, IClientSpaceAOIWatcher
    {
        protected LogicBlockShipCompAOIHackerClient m_lbAOIHacker;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitializeAllComp;
        private static DelegateBridge __Hotfix_CheckCmdMove;
        private static DelegateBridge __Hotfix_PushCmdOperation;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_GetJumpingTunnelAssetPath;
        private static DelegateBridge __Hotfix_GetSpaceDustAssetPath;
        private static DelegateBridge __Hotfix_GetShipAudioAssetPath;
        private static DelegateBridge __Hotfix_GetLBAOIHacker;
        private static DelegateBridge __Hotfix_OnObjectEnterView;
        private static DelegateBridge __Hotfix_OnObjLeaveView;
        private static DelegateBridge __Hotfix_OnTickOver;
        private static DelegateBridge __Hotfix_get_InstanceId;

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip(uint objId, int confId, IInSpacePlayerShipDataContainer dc, ILBInSpacePlayerShipCaptain captain, IClientSolarSytemSpaceContext spaceContext, IPlayerInfoProvider playerInfoProvider, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCmdMove(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public string GetJumpingTunnelAssetPath()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompAOIHackerClient GetLBAOIHacker()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipAudioAssetPath()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSpaceDustAssetPath()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        public void OnObjectEnterView(ClientSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void OnObjLeaveView(ClientSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTickOver()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool PostInitializeAllComp()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PushCmdOperation()
        {
        }

        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

