﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class ClientSpaceShip : ClientSpaceObject, ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        protected IInSpaceShipDataContainer m_dataContainer;
        protected ConfigDataSpaceShipInfo m_confInfo;
        protected ConfigDataSpaceShip3DInfo m_conf3DInfo;
        protected ILBInSpaceShipCaptain m_captain;
        protected ShipGuildInfo m_guildInfo;
        protected LogicBlockShipCompInteractionClient m_lbInteraction;
        protected LogicBlockShipCompDelayExec m_lbDelayExec;
        protected LogicBlockShipCompInSpaceBasicCtxClient m_lbInSpaceBasicCtx;
        protected LogicBlockShipCompInSpaceCaptain m_lbInSpaceCaptain;
        protected LogicBlockShipCompSpaceResource m_lbInSpaceResource;
        protected LogicBlockShipCompSpaceTargetClient m_lbSpaceTarget;
        protected LogicBlockShipCompInSpaceWeaponEquipClient m_lbWeaponEquip;
        protected LogicBlockShipCompInSpaceEquipEffectVirtualBufContainerClient m_lbEquipVirtualBufContainer;
        protected LogicBlockShipCompInSpacePropertiesCalc m_lbPropertiesCalc;
        protected LogicBlockShipCompFireControllClient m_lbFireControll;
        protected LogicBlockShipCompDroneFireControllClient m_lbDroneFireControll;
        protected LogicBlockShipCompIFFBase m_lbIFF;
        protected LogicBlockShipCompTargetSelectBase m_lbTargetSelect;
        protected LogicBlockShipCompProcessContainerClient m_lbProcessContainer;
        protected LogicBlockShipCompSyncEventClient m_lbSyncEvent;
        protected LogicBlockShipCompSpaceObjectClient m_lbSpaceObject;
        protected LogicBlockShipCompAOIClient m_lbAOI;
        protected LogicBlockShipCompSceneBase m_lbScene;
        protected LogicBlockShipCompItemStoreClient m_lbItemStore;
        protected LogicBlockShipCompBufContainerBase m_lbBufContainer;
        protected LogicBlockShipCompEventDispatcher m_lbEventDispatcher;
        protected LogicBlockShipCompGuildInfo m_lbGuildInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_InitializeAllComp;
        private static DelegateBridge __Hotfix_PostInitializeAllComp;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_AttachSimObject;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnServerSyncMoveState;
        private static DelegateBridge __Hotfix_OnServerSyncEvent;
        private static DelegateBridge __Hotfix_PostServerSyncEvent;
        private static DelegateBridge __Hotfix_RemoveSelfFromSpace;
        private static DelegateBridge __Hotfix_GetLBEquipVirtualBufContainer;
        private static DelegateBridge __Hotfix_GetObjectId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceTarget.IsWaitForRemove;
        private static DelegateBridge __Hotfix_GetSpaceTargetName;
        private static DelegateBridge __Hotfix_GetMinAroundRadius;
        private static DelegateBridge __Hotfix_IsGuildFlagShip;
        private static DelegateBridge __Hotfix_IsNpcPolice;
        private static DelegateBridge __Hotfix_GetGuildId;
        private static DelegateBridge __Hotfix_GetAllienceId;
        private static DelegateBridge __Hotfix_IsExistFightBuf;
        private static DelegateBridge __Hotfix_GetSpaceTargetOwner;
        private static DelegateBridge __Hotfix_GetLBPVP;
        private static DelegateBridge __Hotfix_GetLocation;
        private static DelegateBridge __Hotfix_GetRotation;
        private static DelegateBridge __Hotfix_GetRotationQuaternion;
        private static DelegateBridge __Hotfix_GetTargetVelocity;
        private static DelegateBridge __Hotfix_GetTargetArmor;
        private static DelegateBridge __Hotfix_GetTargetShield;
        private static DelegateBridge __Hotfix_GetTargetEnergy;
        private static DelegateBridge __Hotfix_IsDead;
        private static DelegateBridge __Hotfix_IsStateFreeze;
        private static DelegateBridge __Hotfix_IsInJumping;
        private static DelegateBridge __Hotfix_IsInJumpPrepare;
        private static DelegateBridge __Hotfix_IsPreparingForUsingStarGate;
        private static DelegateBridge __Hotfix_IsPreparingForEnterStation;
        private static DelegateBridge __Hotfix_IsHoldMoveState;
        private static DelegateBridge __Hotfix_IsWithNotAttackableFlag;
        private static DelegateBridge __Hotfix_IsWithShieldRepairerDisableFlag;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_GetProcessByInstanceId;
        private static DelegateBridge __Hotfix_OnHitByBullet;
        private static DelegateBridge __Hotfix_OnHitByLaser;
        private static DelegateBridge __Hotfix_OnHitByDrone;
        private static DelegateBridge __Hotfix_OnHitByEquip;
        private static DelegateBridge __Hotfix_OnMissileInComming;
        private static DelegateBridge __Hotfix_OnEnemyDroneFighterAttach;
        private static DelegateBridge __Hotfix_OnAttachBufByEquip;
        private static DelegateBridge __Hotfix_OnOtherTargetBlink;
        private static DelegateBridge __Hotfix_IsInvisible;
        private static DelegateBridge __Hotfix_IsAvailable4WeaponEquipTarget;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisibleStart;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisibleEnd;
        private static DelegateBridge __Hotfix_OnOtherTargetJump;
        private static DelegateBridge __Hotfix_OnClearInvisibleByTarget;
        private static DelegateBridge __Hotfix_OnKillOtherTarget;
        private static DelegateBridge __Hotfix_OnLockedByOtherTarget;
        private static DelegateBridge __Hotfix_OnClearLockedByOtherTarget;
        private static DelegateBridge __Hotfix_GetTargetPriority;
        private static DelegateBridge __Hotfix_OnDetachBufByEquip;
        private static DelegateBridge __Hotfix_HasBuf;
        private static DelegateBridge __Hotfix_IsBufLifeEnd;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_CalcDistanceToTarget_1;
        private static DelegateBridge __Hotfix_CalcDistanceToTarget_0;
        private static DelegateBridge __Hotfix_CalcDistanceToLocation;
        private static DelegateBridge __Hotfix_GetNpcTargetCountInHateList;
        private static DelegateBridge __Hotfix_GetLockedMeNpcTargetCount;
        private static DelegateBridge __Hotfix_GetSpaceCtx;
        private static DelegateBridge __Hotfix_GetLBBasicCtx;
        private static DelegateBridge __Hotfix_GetLBCaptain;
        private static DelegateBridge __Hotfix_GetLBResource;
        private static DelegateBridge __Hotfix_GetLBSpaceTarget;
        private static DelegateBridge __Hotfix_GetLBInSpaceWeaponEquip;
        private static DelegateBridge __Hotfix_GetLBPropertiesCalc;
        private static DelegateBridge __Hotfix_GetLBFireControll;
        private static DelegateBridge __Hotfix_GetLBDroneFireControll;
        private static DelegateBridge __Hotfix_GetLBIFF;
        private static DelegateBridge __Hotfix_GetLBTargetSelect;
        private static DelegateBridge __Hotfix_GetLBProcessContainer;
        private static DelegateBridge __Hotfix_GetLBSyncEvent;
        private static DelegateBridge __Hotfix_GetLBBufContainer;
        private static DelegateBridge __Hotfix_GetLBAOI;
        private static DelegateBridge __Hotfix_GetLBSpaceObject;
        private static DelegateBridge __Hotfix_GetLBScene;
        private static DelegateBridge __Hotfix_GetLBDelayExec;
        private static DelegateBridge __Hotfix_GetLBKillRecord;
        private static DelegateBridge __Hotfix_GetLBGuildInfo;
        private static DelegateBridge __Hotfix_GetLBInteraction;
        private static DelegateBridge __Hotfix_GetShipDataContainer;
        private static DelegateBridge __Hotfix_GetShipConfInfo;
        private static DelegateBridge __Hotfix_GetDefaultAroundRadius;
        private static DelegateBridge __Hotfix_GetBufFactory;
        private static DelegateBridge __Hotfix_GetLBStaticBasicCtx;
        private static DelegateBridge __Hotfix_GetShipItemStoreDataContainer;
        private static DelegateBridge __Hotfix_GetLBShipItemStore;
        private static DelegateBridge __Hotfix_get_DataContainer;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_get_Conf3DInfo;
        private static DelegateBridge __Hotfix_get_SimObject;
        private static DelegateBridge __Hotfix_set_SimObject;

        [MethodImpl(0x8000)]
        public ClientSpaceShip(uint objId, int confId, IInSpaceShipDataContainer dc, ILBInSpaceShipCaptain captain, IClientSolarSytemSpaceContext spaceContext, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void AttachSimObject(SimSpaceObject simObj)
        {
        }

        [MethodImpl(0x8000)]
        bool ILBSpaceTarget.IsWaitForRemove()
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToLocation(Vector3D location, bool considerSpaceObjectCollider = false)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToTarget(Vector3D position)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToTarget(ILBSpaceTarget target, bool considerSpaceObjectCollider = false)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcPropertiesById(PropertiesId propertiesId)
        {
        }

        protected abstract LogicBlockShipCompIFFBase CreateShipCompIFF();
        protected abstract LogicBlockShipCompItemStoreClient CreateShipCompItemStore();
        [MethodImpl(0x8000)]
        public uint GetAllienceId()
        {
        }

        [MethodImpl(0x8000)]
        public ILBBuffFactory GetBufFactory()
        {
        }

        [MethodImpl(0x8000)]
        public float GetDefaultAroundRadius(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildId()
        {
        }

        [MethodImpl(0x8000)]
        public ILogicBlockShipCompAOI GetLBAOI()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceBasicCtxBase GetLBBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompBufContainerBase GetLBBufContainer()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceCaptain GetLBCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompDelayExec GetLBDelayExec()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompDroneFireControllBase GetLBDroneFireControll()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceEquipEffectVirtualBufContainerClient GetLBEquipVirtualBufContainer()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompFireControllBase GetLBFireControll()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompGuildInfo GetLBGuildInfo()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompIFFBase GetLBIFF()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInSpaceWeaponEquipBase GetLBInSpaceWeaponEquip()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInteractionBase GetLBInteraction()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompKillRecordBase GetLBKillRecord()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompProcessContainer GetLBProcessContainer()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCompPropertiesCalc GetLBPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        public virtual LogicBlockShipCompPVPBase GetLBPVP()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSpaceResource GetLBResource()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSceneBase GetLBScene()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompItemStoreBase GetLBShipItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public ILogicBlockShipCompSpaceObject GetLBSpaceObject()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSpaceTargetBase GetLBSpaceTarget()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtx GetLBStaticBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompSyncEventBase GetLBSyncEvent()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompTargetSelectBase GetLBTargetSelect()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetLockedMeNpcTargetCount()
        {
        }

        [MethodImpl(0x8000)]
        public float GetMinAroundRadius()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcTargetCountInHateList()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetObjectId()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcess GetProcessByInstanceId(uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetRotation()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetRotationQuaternion()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetShipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public IShipDataContainer GetShipDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public IShipItemStoreDataContainer GetShipItemStoreDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceContext GetSpaceCtx()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSpaceTargetName()
        {
        }

        [MethodImpl(0x8000)]
        public object GetSpaceTargetOwner()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetArmor()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTargetPriority()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetShield()
        {
        }

        [MethodImpl(0x8000)]
        public Vector4D GetTargetVelocity()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasBuf(int bufId, ulong instanceid)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool InitializeAllComp()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvailable4WeaponEquipTarget(bool isHostile)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBufLifeEnd(ulong instanceid)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDead()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsExistFightBuf(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildFlagShip()
        {
        }

        public abstract bool IsHiredCaptainShip();
        [MethodImpl(0x8000)]
        public bool IsHoldMoveState()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInJumping()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInJumpPrepare()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInvisible()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNpcPolice()
        {
        }

        public abstract bool IsNpcShip();
        public abstract bool IsPlayerShip();
        [MethodImpl(0x8000)]
        public bool IsPreparingForEnterStation()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPreparingForUsingStarGate()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStateFreeze()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWithNotAttackableFlag()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWithShieldRepairerDisableFlag()
        {
        }

        [MethodImpl(0x8000)]
        public ulong OnAttachBufByEquip(int bufId, uint bufLifeTime, float bufInstanceParam1, ILBSpaceTarget srcTarget, EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnClearInvisibleByTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnClearLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDetachBufByEquip(int bufId, ulong instanceid)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEnemyDroneFighterAttach(LBSpaceProcessDroneFighterLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByBullet(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess bulletFlyProcess)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByDrone(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByEquip(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByLaser(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isCritical, float damageMulti, LBBulletDamageInfo damageInfo, LBSpaceProcess singleLaserFireProcess)
        {
        }

        [MethodImpl(0x8000)]
        public void OnKillOtherTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void OnMissileInComming(LBSpaceProcessMissileLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetBlink(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetInvisibleEnd(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetInvisibleStart(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetJump(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnServerSyncEvent(uint eventTime, SyncEventFlag eventFlag, List<LBSyncEventOnHitInfo> onHitList, List<LBSyncEvent> eventList)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnServerSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint snapshotTime)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool PostInitializeAllComp()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PostServerSyncEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSelfFromSpace()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }

        protected IInSpaceShipDataContainer DataContainer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataSpaceShipInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataSpaceShip3DInfo Conf3DInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimSpaceShip SimObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

