﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ClientOtherPlayerInfo : IPlayerInfoProvider
    {
        protected string m_playerGameUserId;
        protected string m_playerName;
        protected int m_playerLevel;
        protected int m_avartarId;
        protected ProfessionType m_profession;
        private ILBSpaceContext m_spaceContext;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerGameUserId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerName;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerLevel;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetSessionId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetTeamInstanceId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerAvatarId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerProfession;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetGrandFaction;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetGenderType;
        private static DelegateBridge __Hotfix_GetCustomizedParameterList;
        private static DelegateBridge __Hotfix_GetCustomizedParameterValueAsInt;
        private static DelegateBridge __Hotfix_GetCustomizedParameterValueAsString;
        private static DelegateBridge __Hotfix_GetCustomizedParameterValueAsBool;
        private static DelegateBridge __Hotfix_GetExtraDailyWormholeEntryCount;

        [MethodImpl(0x8000)]
        public ClientOtherPlayerInfo(string playerGameUserId, string playerName, int playerLevel, int avartarId, ProfessionType profession, ILBSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        GenderType IPlayerInfoProvider.GetGenderType()
        {
        }

        [MethodImpl(0x8000)]
        GrandFaction IPlayerInfoProvider.GetGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        int IPlayerInfoProvider.GetPlayerAvatarId()
        {
        }

        [MethodImpl(0x8000)]
        string IPlayerInfoProvider.GetPlayerGameUserId()
        {
        }

        [MethodImpl(0x8000)]
        int IPlayerInfoProvider.GetPlayerLevel()
        {
        }

        [MethodImpl(0x8000)]
        string IPlayerInfoProvider.GetPlayerName()
        {
        }

        [MethodImpl(0x8000)]
        ProfessionType IPlayerInfoProvider.GetPlayerProfession()
        {
        }

        [MethodImpl(0x8000)]
        ulong IPlayerInfoProvider.GetSessionId()
        {
        }

        [MethodImpl(0x8000)]
        uint IPlayerInfoProvider.GetTeamInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public List<CustomizedParameterInfo> GetCustomizedParameterList()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetCustomizedParameterValueAsBool(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCustomizedParameterValueAsInt(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetCustomizedParameterValueAsString(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetExtraDailyWormholeEntryCount()
        {
        }
    }
}

