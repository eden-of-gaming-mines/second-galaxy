﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ClientSpaceDropBox : ClientSpaceObject, ILBSpaceDropBox
    {
        protected LogicBlockObjCompDropBoxClient m_lbDropBox;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetSpaceDropBoxArtResource;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_get_SimObject;
        private static DelegateBridge __Hotfix_set_SimObject;
        private static DelegateBridge __Hotfix_GetSpaceCtx;
        private static DelegateBridge __Hotfix_GetObjId;
        private static DelegateBridge __Hotfix_IsAvailable4Player;
        private static DelegateBridge __Hotfix_GetDropItemList4Player;
        private static DelegateBridge __Hotfix_GetLocation;
        private static DelegateBridge __Hotfix_RemoveSelfOnTickEnd;
        private static DelegateBridge __Hotfix_GetLBDropBox;

        [MethodImpl(0x8000)]
        public ClientSpaceDropBox(uint objId, IClientSolarSytemSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetDropItemList4Player(string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockObjCompDropBoxBase GetLBDropBox()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetObjId()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceContext GetSpaceCtx()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSpaceDropBoxArtResource()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(DropInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvailable4Player(string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSelfOnTickEnd()
        {
        }

        public SimSpaceSimpleObject SimObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

