﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompAOIHackerClient
    {
        protected ILBInSpaceShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnObjEnterView;
        private static DelegateBridge __Hotfix_OnObjLeaveView;
        private static DelegateBridge __Hotfix_OnAOIShipInvisbleStart;
        private static DelegateBridge __Hotfix_OnAOIShipInvisibleEnd;
        private static DelegateBridge __Hotfix_OnAOIShipEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_OnAOIShipBlinkEnd;

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAOIShipBlinkEnd(ILBInSpaceShip blinkShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAOIShipEnterHighSpeedJumping(ILBInSpaceShip jumpingShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAOIShipInvisbleStart(ILBInSpaceShip invisibleShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAOIShipInvisibleEnd(ILBInSpaceShip invisibleShip)
        {
        }

        [MethodImpl(0x8000)]
        public void OnObjEnterView(ClientSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void OnObjLeaveView(ClientSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }
    }
}

