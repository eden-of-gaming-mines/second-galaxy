﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ClientNpcSpaceShip : ClientSpaceShip, ILBInSpaceNpcShip, ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        protected LogicBlockShipCompWingShipCtrlStubClient m_lbWingShipCtrlStub;
        protected ConfigDataSpaceShipInfo m_shipConfInfo;
        protected ConfigDataNpcShipInfo m_npcShipConfInfo;
        protected ConfigDataNpcMonsterInfo m_npcMonsterConfInfo;
        protected ConfigDataNpcShipTemplateInfo m_npcShipTemplateConfInfo;
        protected ConfigDataNpcShipAIInfo m_npcShipAIInfo;
        protected NpcShipAnimEffectType m_npcCreateAnimEffectType;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitializeAllComp;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_CreateShipCompIFF;
        private static DelegateBridge __Hotfix_CreateShipCompItemStore;
        private static DelegateBridge __Hotfix_IsPlayerShip;
        private static DelegateBridge __Hotfix_IsHiredCaptainShip;
        private static DelegateBridge __Hotfix_IsNpcShip;
        private static DelegateBridge __Hotfix_GetNpcMonsterLevel;
        private static DelegateBridge __Hotfix_GetNpcShipConfInfo;
        private static DelegateBridge __Hotfix_GetNpcMonsterConfInfo;
        private static DelegateBridge __Hotfix_GetNpcShipTemplateConfInfo;
        private static DelegateBridge __Hotfix_GetNpcShipTypeLevelDropInfo;
        private static DelegateBridge __Hotfix_GetLBWingShipCtrlStub;
        private static DelegateBridge __Hotfix_GetNpcShipAIConfInfo;
        private static DelegateBridge __Hotfix_get_ShipConfInfo;
        private static DelegateBridge __Hotfix_get_NpcShipConfInfo;
        private static DelegateBridge __Hotfix_get_NpcMonsterConfInfo;
        private static DelegateBridge __Hotfix_get_NpcShipTemplateConfInfo;
        private static DelegateBridge __Hotfix_get_NpcShipAIInfo;
        private static DelegateBridge __Hotfix_get_CreateAnimEffect;

        [MethodImpl(0x8000)]
        public ClientNpcSpaceShip(uint objId, int confId, IInSpaceNpcShipDataContainer dc, ILBInSpaceShipCaptain captain, NpcShipAnimEffectType createEffect, IClientSolarSytemSpaceContext spaceContext, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompIFFBase CreateShipCompIFF()
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompItemStoreClient CreateShipCompItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWingShipCtrlStubBase GetLBWingShipCtrlStub()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterInfo GetNpcMonsterConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcMonsterLevel()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipAIInfo GetNpcShipAIConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipInfo GetNpcShipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcShipTemplateInfo GetNpcShipTemplateConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcMonsterLevelDropInfo GetNpcShipTypeLevelDropInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHiredCaptainShip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNpcShip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsPlayerShip()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool PostInitializeAllComp()
        {
        }

        public ConfigDataSpaceShipInfo ShipConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataNpcShipInfo NpcShipConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataNpcMonsterInfo NpcMonsterConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataNpcShipTemplateInfo NpcShipTemplateConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataNpcShipAIInfo NpcShipAIInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public NpcShipAnimEffectType CreateAnimEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

