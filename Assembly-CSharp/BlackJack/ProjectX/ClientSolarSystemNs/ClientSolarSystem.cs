﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using BlackJack.ToolUtil;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ClientSolarSystem : IClientSolarSytemSpaceContext, ILBSpaceContext, ILBSpaceProcessContainer
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private GDBSolarSystemInfo <ConfInfo>k__BackingField;
        protected IConfigDataLoader m_configDataLoader;
        protected ILBPlayerContextClient m_lbPlayerCtx;
        protected LogicBlockTeamClient m_lbTeamClient;
        protected bool m_isInfect;
        protected uint m_currTime;
        protected uint m_lastTickTime;
        protected uint m_tickDalte;
        protected DateTime m_nextGetCurrObjCountTime;
        protected DateTime m_lastGetCurrObjCountTime;
        private int m_objCount;
        protected Random m_rand;
        private LBSpaceProcessContainerClient m_spaceProcessContainer;
        protected MapObjInstanceIdFactory m_idFactory;
        protected ObjId2ObjMap m_objId2ClientSpaceObjMap;
        protected SimSolarSystem m_simSolarSystem;
        protected HashSet<ClientSpaceObject> m_needRemoveObjectSet;
        protected Action<ClientSpaceObject> m_tickSingleObjectAction;
        protected const uint ClientSimFrameSize = 20;
        protected ClientStar m_star;
        protected List<ClientPlanet> m_planetList;
        protected List<ClientPlanetLikeBuilding> m_planetLikeBuildingList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_InitlizeSolarSystemByConfData;
        private static DelegateBridge __Hotfix_GetObjectByObjId;
        private static DelegateBridge __Hotfix_RemoveObjectOnTickEnd_0;
        private static DelegateBridge __Hotfix_GetGDBStarGateByGDBConfId;
        private static DelegateBridge __Hotfix_RemoveObjectOnTickEnd_1;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_TickSingleObject;
        private static DelegateBridge __Hotfix_TickRemove;
        private static DelegateBridge __Hotfix_CalcSimTickFrameSize;
        private static DelegateBridge __Hotfix_TickObjCountLog;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.ClientSolarSystemNs.IClientSolarSytemSpaceContext.GetObjectByObjId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.ClientSolarSystemNs.IClientSolarSytemSpaceContext.GetTickDelta;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.ClientSolarSystemNs.IClientSolarSytemSpaceContext.GetGDBSolarSystemConf;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.ClientSolarSystemNs.IClientSolarSytemSpaceContext.RemoveObjectOnTickEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetTickSeq;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetCurrTime;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.NextRandom;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetRandom;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetSpaceTargetById;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetSpaceDropBoxById;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetConfigDataLoader;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetPlayerTeamMambersByTeamId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetPlayerTeamIdByGameUserId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.RemoveShipOnTickEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.GetCurrentSolarSystemId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.EnableAccurateAOI4Ship;
        private static DelegateBridge __Hotfix_GetCurrServerTime;
        private static DelegateBridge __Hotfix_GetSecurityLevel;
        private static DelegateBridge __Hotfix_IsPVPEnable;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceContext.IsAntiTeleportEffectActivated;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessContainer.GetProcessByInstanceId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessContainer.Tick;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessContainer.RegistProcess;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessContainer.CancelProcess;
        private static DelegateBridge __Hotfix_CancelAllProcessByDestTarget;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_set_ConfInfo;
        private static DelegateBridge __Hotfix_get_IsInfect;
        private static DelegateBridge __Hotfix_set_IsInfect;
        private static DelegateBridge __Hotfix_CreateStar;
        private static DelegateBridge __Hotfix_CreatePlanet;
        private static DelegateBridge __Hotfix_CreatePlanetLikeBuilding;
        private static DelegateBridge __Hotfix_CreateMoon_0;
        private static DelegateBridge __Hotfix_CreateMoon_1;
        private static DelegateBridge __Hotfix_IsMoonNeedAOI;
        private static DelegateBridge __Hotfix_GetNoneAOICelestialsInRange;
        private static DelegateBridge __Hotfix_GetStar;
        private static DelegateBridge __Hotfix_GetPlanetList;
        private static DelegateBridge __Hotfix_GetPlanetById;
        private static DelegateBridge __Hotfix_GetPlanetLikeBuildingList;
        private static DelegateBridge __Hotfix_GetPlanetLikeBuildingById;
        private static DelegateBridge __Hotfix_GetClientPlanetByGDBSolarSystemObjectId;
        private static DelegateBridge __Hotfix_GetClientCelestialObjectByGDBSolarSystemObjectId;
        private static DelegateBridge __Hotfix_GetStarObject;
        private static DelegateBridge __Hotfix_CreateSelfPlayerSpaceShip;
        private static DelegateBridge __Hotfix_CreateSelfPlayerSpaceShip4FlagShip;
        private static DelegateBridge __Hotfix_CreateOtherPlayerSpaceShip;
        private static DelegateBridge __Hotfix_CreateOtherPlayerSpaceShip4FlagShip;
        private static DelegateBridge __Hotfix_CreateNpcSpaceShip;
        private static DelegateBridge __Hotfix_CreateHiredCaptainSpaceShip;
        private static DelegateBridge __Hotfix_CreateSimpleObject;
        private static DelegateBridge __Hotfix_CreateSceneObject;
        private static DelegateBridge __Hotfix_CreateDropBox;
        private static DelegateBridge __Hotfix_CreateSpaceStation;
        private static DelegateBridge __Hotfix_GetSpaceStationById;
        private static DelegateBridge __Hotfix_GetSpaceStationList;
        private static DelegateBridge __Hotfix_GetClientSpaceStationByGDBSolarSystemObjectId;
        private static DelegateBridge __Hotfix_CreateStarGate;
        private static DelegateBridge __Hotfix_GetStarGateById;
        private static DelegateBridge __Hotfix_GetStarGateList;
        private static DelegateBridge __Hotfix_GetClientStarGateByGDBSolarSystemObjectId;

        [MethodImpl(0x8000)]
        GDBSolarSystemInfo IClientSolarSytemSpaceContext.GetGDBSolarSystemConf()
        {
        }

        [MethodImpl(0x8000)]
        T IClientSolarSytemSpaceContext.GetObjectByObjId<T>(uint id) where T: ClientSpaceObject
        {
        }

        [MethodImpl(0x8000)]
        uint IClientSolarSytemSpaceContext.GetTickDelta()
        {
        }

        [MethodImpl(0x8000)]
        void IClientSolarSytemSpaceContext.RemoveObjectOnTickEnd(ClientSpaceObject spaceObj)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceContext.EnableAccurateAOI4Ship(ILBInSpaceShip ship, bool enable)
        {
        }

        [MethodImpl(0x8000)]
        IConfigDataLoader ILBSpaceContext.GetConfigDataLoader()
        {
        }

        [MethodImpl(0x8000)]
        int ILBSpaceContext.GetCurrentSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        uint ILBSpaceContext.GetCurrTime()
        {
        }

        [MethodImpl(0x8000)]
        uint ILBSpaceContext.GetPlayerTeamIdByGameUserId(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        IEnumerable<KeyValuePair<ulong, ILBInSpacePlayerShip>> ILBSpaceContext.GetPlayerTeamMambersByTeamId(uint teamInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        Random ILBSpaceContext.GetRandom()
        {
        }

        [MethodImpl(0x8000)]
        ILBSpaceDropBox ILBSpaceContext.GetSpaceDropBoxById(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        ILBSpaceTarget ILBSpaceContext.GetSpaceTargetById(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        uint ILBSpaceContext.GetTickSeq()
        {
        }

        [MethodImpl(0x8000)]
        bool ILBSpaceContext.IsAntiTeleportEffectActivated()
        {
        }

        [MethodImpl(0x8000)]
        float ILBSpaceContext.NextRandom()
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceContext.RemoveShipOnTickEnd(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessContainer.CancelProcess(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        LBSpaceProcess ILBSpaceProcessContainer.GetProcessByInstanceId(uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessContainer.RegistProcess(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessContainer.Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected uint CalcSimTickFrameSize(uint tickDalte)
        {
        }

        [MethodImpl(0x8000)]
        public void CancelAllProcessByDestTarget(ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceDropBox CreateDropBox(uint objId, DropInfo dropInfo, Vector3D location, Vector3D rotation)
        {
        }

        [MethodImpl(0x8000)]
        public ClientHiredCaptainSpaceShip CreateHiredCaptainSpaceShip(uint objId, int confId, IInSpaceNpcShipDataContainer dc, SpaceShipPositionInfo posInfo, ILBInSpaceShipCaptain captain, NpcShipAnimEffectType createEffect = 0, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        protected ClientMoon CreateMoon(GDBMoonInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public ClientMoon CreateMoon(GDBMoonInfo gdbInfo, uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public ClientNpcSpaceShip CreateNpcSpaceShip(uint objId, int confId, IInSpaceNpcShipDataContainer dc, SpaceShipPositionInfo posInfo, ILBInSpaceShipCaptain captain, NpcShipAnimEffectType createEffect = 0, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public ClientOtherPlayerSpaceShip CreateOtherPlayerSpaceShip(uint objId, int confId, IInSpacePlayerShipDataContainer dc, SpaceShipPositionInfo posInfo, ILBInSpacePlayerShipCaptain captain, IPlayerInfoProvider playerInfoProvider, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public ClientOtherPlayerSpaceShip4FlagShip CreateOtherPlayerSpaceShip4FlagShip(uint objId, int confId, IInSpaceFlagShipDataContainer dc, SpaceShipPositionInfo posInfo, ILBInSpacePlayerShipCaptain captain, IPlayerInfoProvider playerInfoProvider, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        protected ClientPlanet CreatePlanet(GDBPlanetInfo gdbInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected ClientPlanetLikeBuilding CreatePlanetLikeBuilding(GDBPlanetLikeBuildingInfo gdbInfo)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceSceneObject CreateSceneObject(uint objId, int sceneConfId, int index, Vector3D location, Vector3D rotation)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip CreateSelfPlayerSpaceShip(uint objId, int confId, IInSpacePlayerShipDataContainer dc, SpaceShipPositionInfo posInfo, ILBInSpacePlayerShipCaptain captain, IPlayerInfoProvider playerInfoProvider, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip4FlagShip CreateSelfPlayerSpaceShip4FlagShip(uint objId, int confId, IInSpaceFlagShipDataContainer dc, SpaceShipPositionInfo posInfo, ILBInSpacePlayerShipCaptain captain, IPlayerInfoProvider playerInfoProvider, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceSimpleObject CreateSimpleObject(uint objId, int configId, Vector3D location, Vector3D rotation, float scale = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceStation CreateSpaceStation(GDBSpaceStationInfo gdbInfo, uint objId)
        {
        }

        [MethodImpl(0x8000)]
        protected ClientStar CreateStar(GDBStarInfo gdbInfo)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStarGate CreateStarGate(GDBStargateInfo gdbInfo, uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceObject GetClientCelestialObjectByGDBSolarSystemObjectId(int gdbSolarSystemObjectId)
        {
        }

        [MethodImpl(0x8000)]
        public ClientPlanet GetClientPlanetByGDBSolarSystemObjectId(int gdbSolarSystemObjectId)
        {
        }

        [MethodImpl(0x8000)]
        public ClientSpaceStation GetClientSpaceStationByGDBSolarSystemObjectId(int gdbSolarSystemObjectId)
        {
        }

        [MethodImpl(0x8000)]
        public ClientStarGate GetClientStarGateByGDBSolarSystemObjectId(int gdbSolarSystemObjectId)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetCurrServerTime()
        {
        }

        [MethodImpl(0x8000)]
        public GDBStargateInfo GetGDBStarGateByGDBConfId(int gdbConfId)
        {
        }

        [MethodImpl(0x8000)]
        public List<ClientSpaceObject> GetNoneAOICelestialsInRange(double range, Vector3D center)
        {
        }

        [MethodImpl(0x8000)]
        public T GetObjectByObjId<T>(uint id) where T: ClientSpaceObject
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetInfo GetPlanetById(int planetId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetLikeBuildingInfo GetPlanetLikeBuildingById(int planetLikeBuildingId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBPlanetLikeBuildingInfo> GetPlanetLikeBuildingList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBPlanetInfo> GetPlanetList()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSecurityLevel()
        {
        }

        [MethodImpl(0x8000)]
        public GDBSpaceStationInfo GetSpaceStationById(int spaceStationId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSpaceStationInfo> GetSpaceStationList()
        {
        }

        [MethodImpl(0x8000)]
        public GDBStarInfo GetStar()
        {
        }

        [MethodImpl(0x8000)]
        public GDBStargateInfo GetStarGateById(int starGateId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBStargateInfo> GetStarGateList()
        {
        }

        [MethodImpl(0x8000)]
        public ClientStar GetStarObject()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initlize(GDBSolarSystemInfo confDataInfo, uint currSolarSystemTime, IConfigDataLoader configDataLoader, ILBPlayerContextClient playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        private bool InitlizeSolarSystemByConfData(GDBSolarSystemInfo confDataInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsMoonNeedAOI(GDBMoonInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPVPEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveObjectOnTickEnd(ClientSpaceObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveObjectOnTickEnd(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        private void TickObjCountLog()
        {
        }

        [MethodImpl(0x8000)]
        private void TickRemove()
        {
        }

        [MethodImpl(0x8000)]
        private void TickSingleObject(ClientSpaceObject obj)
        {
        }

        public GDBSolarSystemInfo ConfInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public bool IsInfect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetClientPlanetByGDBSolarSystemObjectId>c__AnonStorey0
        {
            internal int gdbSolarSystemObjectId;

            [MethodImpl(0x8000)]
            internal bool <>m__0(ClientPlanet planet)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetClientSpaceStationByGDBSolarSystemObjectId>c__AnonStorey1
        {
            internal int gdbSolarSystemObjectId;

            internal bool <>m__0(ClientSpaceStation station) => 
                (station.ConfId == this.gdbSolarSystemObjectId);
        }

        [CompilerGenerated]
        private sealed class <GetClientStarGateByGDBSolarSystemObjectId>c__AnonStorey2
        {
            internal int gdbSolarSystemObjectId;

            internal bool <>m__0(ClientStarGate starGate) => 
                (starGate.ConfId == this.gdbSolarSystemObjectId);
        }
    }
}

