﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ClientSpaceSimpleObject : ClientSpaceObject
    {
        protected ConfigDataSimpleObjectInfo m_confInfo;
        private ConfigDataSpaceSimpleObject3DInfo m_conf3DInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetSpaceSimpleObjectArtResource;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_get_Conf3DInfo;
        private static DelegateBridge __Hotfix_get_SimObject;
        private static DelegateBridge __Hotfix_set_SimObject;
        private static DelegateBridge __Hotfix_get_Scale;

        [MethodImpl(0x8000)]
        public ClientSpaceSimpleObject(uint objId, int confId, IClientSolarSytemSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSpaceSimpleObjectArtResource()
        {
        }

        public ConfigDataSimpleObjectInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataSpaceSimpleObject3DInfo Conf3DInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimSpaceSimpleObject SimObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float Scale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

