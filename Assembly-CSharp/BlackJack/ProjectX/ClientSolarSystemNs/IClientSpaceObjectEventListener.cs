﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using System;

    public interface IClientSpaceObjectEventListener
    {
        void OnRemovedFromSpace(ClientSpaceObject obj);
    }
}

