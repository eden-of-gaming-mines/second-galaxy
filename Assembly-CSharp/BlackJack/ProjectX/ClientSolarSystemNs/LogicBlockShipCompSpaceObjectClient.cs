﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using Dest.Math;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompSpaceObjectClient : ILogicBlockShipCompSpaceObjectClient, ISimSpaceShipEventListener, ISimMovemnetParamProvider, ILogicBlockShipCompSpaceObject, ILogicBlockObjCompSpaceObject, ISimSpaceObjectEventListener
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<SimSpaceShip.MoveCmd> EventOnSyncMoveState;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnJumpPrepare;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnJumpPrepareEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBInSpaceShip, Vector3D> EventOnStartJumping;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBInSpaceShip> EventOnEnterHighSpeedJumping;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLeaveHighSpeedJumping;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBInSpaceShip> EventOnJumpToLocationEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<SimSpaceShip.MoveCmd> EventOnMovementCompleted;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<double, uint> EventOnArroundTargetMovementModuleStartToCloseToTarget;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<double, uint> EventOnArroundTargetMovementModuleStartToArroundToTarget;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBlinkStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBInSpaceShip> EventOnBlinkEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBInSpaceShip> EventOnInvisibleStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBInSpaceShip> EventOnInvisibleEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnClearInvisibleByTarget;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnBlinkInterrupted;
        protected ClientSpaceShip m_ownerShip;
        protected IClientSolarSytemSpaceContext m_spaceContext;
        protected SimSpaceShip m_simObject;
        protected ConfigDataSpaceShipInfo m_confInfo;
        protected bool m_isInBlink;
        protected bool m_isInvisible;
        protected uint m_maxCruiseSpeedValidSeq;
        protected double m_maxCruiseSpeed;
        protected uint m_defaultMaxCruiseSpeedValidSeq;
        protected double m_defaultMaxCruiseSpeed;
        protected uint m_inertiaParamValidSeq;
        protected double m_inertiaParam;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_GetShipConfigName;
        private static DelegateBridge __Hotfix_GetOwnerName;
        private static DelegateBridge __Hotfix_OnDead;
        private static DelegateBridge __Hotfix_AttachSimObject;
        private static DelegateBridge __Hotfix_OnSyncMoveState;
        private static DelegateBridge __Hotfix_CheckPushCmdMove;
        private static DelegateBridge __Hotfix_ClearProcessContext4Blink;
        private static DelegateBridge __Hotfix_ClearProcessContext4InvisibleStart;
        private static DelegateBridge __Hotfix_ClearProcessContext4InvisibleEnd;
        private static DelegateBridge __Hotfix_ClearProcessContext4Jump;
        private static DelegateBridge __Hotfix_GetObjectId;
        private static DelegateBridge __Hotfix_IsWaitForRemove;
        private static DelegateBridge __Hotfix_GetPose;
        private static DelegateBridge __Hotfix_SetPose;
        private static DelegateBridge __Hotfix_SetLocation;
        private static DelegateBridge __Hotfix_GetLocation;
        private static DelegateBridge __Hotfix_GetRotationEuler;
        private static DelegateBridge __Hotfix_GetRotationQuaternion;
        private static DelegateBridge __Hotfix_CheckDistanceToTarget;
        private static DelegateBridge __Hotfix_GetDistanceToTarget;
        private static DelegateBridge __Hotfix_DoArroundTargetInFightRange;
        private static DelegateBridge __Hotfix_StopShip;
        private static DelegateBridge __Hotfix_JumpToLocation;
        private static DelegateBridge __Hotfix_JumpToRandomPlanet;
        private static DelegateBridge __Hotfix_PushCmdMoveToLocation;
        private static DelegateBridge __Hotfix_PushCmdMoveToTarget;
        private static DelegateBridge __Hotfix_CheckDistanceToLocation;
        private static DelegateBridge __Hotfix_IsInJumping;
        private static DelegateBridge __Hotfix_IsInJumpPrepare;
        private static DelegateBridge __Hotfix_IsPreparingForUsingStarGate;
        private static DelegateBridge __Hotfix_IsPreparingForEnterStation;
        private static DelegateBridge __Hotfix_IsHoldMoveState;
        private static DelegateBridge __Hotfix_OnBlinkStart;
        private static DelegateBridge __Hotfix_OnBlinkEnd;
        private static DelegateBridge __Hotfix_OnBlinkCancel;
        private static DelegateBridge __Hotfix_IsInBlinking;
        private static DelegateBridge __Hotfix_OnInvisibleStart;
        private static DelegateBridge __Hotfix_OnInvisibleEnd;
        private static DelegateBridge __Hotfix_IsInvisible;
        private static DelegateBridge __Hotfix_SetKeepingInvisibleForWeaponEquipLaunch;
        private static DelegateBridge __Hotfix_GetKeepingInvisibleForWeaponEquipLaunch;
        private static DelegateBridge __Hotfix_OnClearInvisibleByTarget;
        private static DelegateBridge __Hotfix_GetMovementRelativeObjId;
        private static DelegateBridge __Hotfix_RegEventOnEnterSpaceStation;
        private static DelegateBridge __Hotfix_UnregEventOnEnterSpaceStation;
        private static DelegateBridge __Hotfix_RegEventOnJumpingStart;
        private static DelegateBridge __Hotfix_UnregEventOnJumpingStart;
        private static DelegateBridge __Hotfix_RegEventOnJumpingEnd;
        private static DelegateBridge __Hotfix_UnregEventOnJumpingEnd;
        private static DelegateBridge __Hotfix_RegMovementRelativeTargetLost;
        private static DelegateBridge __Hotfix_RegEventOnBlinkStart;
        private static DelegateBridge __Hotfix_UnregEventOnBlinkStart;
        private static DelegateBridge __Hotfix_RegEventOnBlinkEnd;
        private static DelegateBridge __Hotfix_UnregEventOnBlinkEnd;
        private static DelegateBridge __Hotfix_RegEventOnInvsibleStart;
        private static DelegateBridge __Hotfix_UnregEventInvsibleStart;
        private static DelegateBridge __Hotfix_RegEventOnInvsibleEnd;
        private static DelegateBridge __Hotfix_UnregEventInvsibleEnd;
        private static DelegateBridge __Hotfix_RegEventOnClearInvisibleByTarget;
        private static DelegateBridge __Hotfix_UnregEventOnClearInvisibleByTarget;
        private static DelegateBridge __Hotfix_RegEventOnStartSolarSystemTeleport;
        private static DelegateBridge __Hotfix_UnregEventOnStartSolarSystemTeleport;
        private static DelegateBridge __Hotfix_RegEventOnStartSolarSystemTeleport2Scene;
        private static DelegateBridge __Hotfix_UnregEventOnStartSolarSystemTeleport2Scene;
        private static DelegateBridge __Hotfix_RemoveSelfFromSpace;
        private static DelegateBridge __Hotfix_RegEventOnRemoveFromSolarSystem;
        private static DelegateBridge __Hotfix_UnregEventOnRemoveFromSolarSystem;
        private static DelegateBridge __Hotfix_RegEventOnPrepare4Jump;
        private static DelegateBridge __Hotfix_UnregEventOnPrepare4Jump;
        private static DelegateBridge __Hotfix_RegEventOnEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_UnregEventOnEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_RegEventOnJumpPrepareEnd;
        private static DelegateBridge __Hotfix_UnregEventOnJumpPrepareEnd;
        private static DelegateBridge __Hotfix_OnSyncEventStartJumping;
        private static DelegateBridge __Hotfix_IsJumpDisturbTriggered;
        private static DelegateBridge __Hotfix_IsJumpDisturbedBySpecialBuf;
        private static DelegateBridge __Hotfix_OnSyncEventBlinkInterrupted;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnMoveCmdComplete;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnMoveCmdComplete;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnStartJumpPrepare;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnStartJumpPrepare;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnJumpPrepareEnd;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnJumpPrepareEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnStartJumping;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnStartJumping;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnLeaveHighSpeedJumping;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnLeaveHighSpeedJumping;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnHoldMoveStateEnd;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnHoldMoveStateEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnArroundTargetMovementModuleStartToCloseToTarget;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnArroundTargetMovementModuleStartToCloseToTarget;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimSpaceShipEventListener.OnArroundTargetMovementModuleStartToArroundToTarget;
        private static DelegateBridge __Hotfix_ISimSpaceShipEventListener_OnArroundTargetMovementModuleStartToArroundToTarget;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovemnetParamProvider.GetInertiaParam;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovemnetParamProvider.GetDefaultMaxCruiseSpeed;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovemnetParamProvider.GetMaxCruiseSpeed;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovemnetParamProvider.GetJumpAccelerationParam;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovemnetParamProvider.GetJumpMaxSpeed;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.SimSolarSystemNs.ISimMovemnetParamProvider.IsShipMoveDisabled;
        private static DelegateBridge __Hotfix_add_EventOnSyncMoveState;
        private static DelegateBridge __Hotfix_remove_EventOnSyncMoveState;
        private static DelegateBridge __Hotfix_add_EventOnJumpPrepare;
        private static DelegateBridge __Hotfix_remove_EventOnJumpPrepare;
        private static DelegateBridge __Hotfix_add_EventOnJumpPrepareEnd;
        private static DelegateBridge __Hotfix_remove_EventOnJumpPrepareEnd;
        private static DelegateBridge __Hotfix_add_EventOnStartJumping;
        private static DelegateBridge __Hotfix_remove_EventOnStartJumping;
        private static DelegateBridge __Hotfix_add_EventOnEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_remove_EventOnEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_add_EventOnLeaveHighSpeedJumping;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveHighSpeedJumping;
        private static DelegateBridge __Hotfix_add_EventOnJumpToLocationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToLocationEnd;
        private static DelegateBridge __Hotfix_add_EventOnMovementCompleted;
        private static DelegateBridge __Hotfix_remove_EventOnMovementCompleted;
        private static DelegateBridge __Hotfix_add_EventOnArroundTargetMovementModuleStartToCloseToTarget;
        private static DelegateBridge __Hotfix_remove_EventOnArroundTargetMovementModuleStartToCloseToTarget;
        private static DelegateBridge __Hotfix_add_EventOnArroundTargetMovementModuleStartToArroundToTarget;
        private static DelegateBridge __Hotfix_remove_EventOnArroundTargetMovementModuleStartToArroundToTarget;
        private static DelegateBridge __Hotfix_add_EventOnBlinkStart;
        private static DelegateBridge __Hotfix_remove_EventOnBlinkStart;
        private static DelegateBridge __Hotfix_add_EventOnBlinkEnd;
        private static DelegateBridge __Hotfix_remove_EventOnBlinkEnd;
        private static DelegateBridge __Hotfix_add_EventOnInvisibleStart;
        private static DelegateBridge __Hotfix_remove_EventOnInvisibleStart;
        private static DelegateBridge __Hotfix_add_EventOnInvisibleEnd;
        private static DelegateBridge __Hotfix_remove_EventOnInvisibleEnd;
        private static DelegateBridge __Hotfix_add_EventOnClearInvisibleByTarget;
        private static DelegateBridge __Hotfix_remove_EventOnClearInvisibleByTarget;
        private static DelegateBridge __Hotfix_add_EventOnBlinkInterrupted;
        private static DelegateBridge __Hotfix_remove_EventOnBlinkInterrupted;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_get_CurrMoveCmd;

        public event Action<double, uint> EventOnArroundTargetMovementModuleStartToArroundToTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<double, uint> EventOnArroundTargetMovementModuleStartToCloseToTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceShip> EventOnBlinkEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnBlinkInterrupted
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBlinkStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnClearInvisibleByTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceShip> EventOnEnterHighSpeedJumping
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceShip> EventOnInvisibleEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceShip> EventOnInvisibleStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnJumpPrepare
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnJumpPrepareEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceShip> EventOnJumpToLocationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLeaveHighSpeedJumping
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SimSpaceShip.MoveCmd> EventOnMovementCompleted
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceShip, Vector3D> EventOnStartJumping
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SimSpaceShip.MoveCmd> EventOnSyncMoveState
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public virtual void AttachSimObject(SimSpaceObject simObj)
        {
        }

        [MethodImpl(0x8000)]
        double ISimMovemnetParamProvider.GetDefaultMaxCruiseSpeed()
        {
        }

        [MethodImpl(0x8000)]
        double ISimMovemnetParamProvider.GetInertiaParam()
        {
        }

        [MethodImpl(0x8000)]
        double ISimMovemnetParamProvider.GetJumpAccelerationParam()
        {
        }

        [MethodImpl(0x8000)]
        double ISimMovemnetParamProvider.GetJumpMaxSpeed()
        {
        }

        [MethodImpl(0x8000)]
        double ISimMovemnetParamProvider.GetMaxCruiseSpeed()
        {
        }

        [MethodImpl(0x8000)]
        bool ISimMovemnetParamProvider.IsShipMoveDisabled()
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnEnterHighSpeedJumping()
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnHoldMoveStateEnd()
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnJumpPrepareEnd()
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnLeaveHighSpeedJumping()
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnMoveCmdComplete(SimSpaceShip.MoveCmd.CmdType cmdType, SimSpaceShip.MoveCmd moveCmd)
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnStartJumping(Vector3D dest)
        {
        }

        [MethodImpl(0x8000)]
        void ISimSpaceShipEventListener.OnStartJumpPrepare()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDistanceToLocation(Vector3D location, float maxDistance)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDistanceToTarget(ILBSpaceTarget target, float maxDistance)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckPushCmdMove(SimSpaceShip.MoveCmd cmd, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearProcessContext4Blink()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearProcessContext4InvisibleEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearProcessContext4InvisibleStart()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearProcessContext4Jump()
        {
        }

        [MethodImpl(0x8000)]
        public void DoArroundTargetInFightRange(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public float GetDistanceToTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetKeepingInvisibleForWeaponEquipLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetMovementRelativeObjId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetObjectId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetOwnerName()
        {
        }

        [MethodImpl(0x8000)]
        public void GetPose(out Vector3D location, out Quaternion rotation, out Vector4D velocity)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetRotationEuler()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetRotationQuaternion()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetShipConfigName()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetShipName()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ClientSpaceShip owner, IClientSolarSytemSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHoldMoveState()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnEnterHighSpeedJumping()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnHoldMoveStateEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnJumpPrepareEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnLeaveHighSpeedJumping()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnMoveCmdComplete(SimSpaceShip.MoveCmd.CmdType cmdType, SimSpaceShip.MoveCmd moveCmd)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnStartJumping(Vector3D dest)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ISimSpaceShipEventListener_OnStartJumpPrepare()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInBlinking()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInJumping()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInJumpPrepare()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInvisible()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsJumpDisturbedBySpecialBuf()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsJumpDisturbTriggered()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPreparingForEnterStation()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPreparingForUsingStarGate()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWaitForRemove()
        {
        }

        [MethodImpl(0x8000)]
        public void JumpToLocation(Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        public void JumpToRandomPlanet()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBlinkCancel()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBlinkEnd(float distance)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBlinkStart()
        {
        }

        [MethodImpl(0x8000)]
        public void OnClearInvisibleByTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDead(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInvisibleEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnInvisibleStart()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventBlinkInterrupted(int reason)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventStartJumping(bool isJumpingDisturbed)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncMoveState(SimSpaceShipStateSnapshot snapshot, uint snapshotTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void PushCmdMoveToLocation(Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        public void PushCmdMoveToTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnBlinkEnd(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnBlinkStart(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnClearInvisibleByTarget(Action<ILBSpaceTarget> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnEnterHighSpeedJumping(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnEnterSpaceStation(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnInvsibleEnd(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnInvsibleStart(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnJumpingEnd(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnJumpingStart(Action<ILBInSpaceShip, Vector3D> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnJumpPrepareEnd(Action<bool> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnPrepare4Jump(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnRemoveFromSolarSystem(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnStartSolarSystemTeleport(Action<int, uint, TeleportEffect> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnStartSolarSystemTeleport2Scene(Action<int, int, SceneType, TeleportEffect> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegMovementRelativeTargetLost(Func<bool> func)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSelfFromSpace()
        {
        }

        [MethodImpl(0x8000)]
        public void SetKeepingInvisibleForWeaponEquipLaunch(bool isKeeping)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocation(Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPose(Vector3D location, Quaternion rotation, Vector4D velocity)
        {
        }

        [MethodImpl(0x8000)]
        public void StopShip()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventInvsibleEnd(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventInvsibleStart(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnBlinkEnd(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnBlinkStart(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnClearInvisibleByTarget(Action<ILBSpaceTarget> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnEnterHighSpeedJumping(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnEnterSpaceStation(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnJumpingEnd(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnJumpingStart(Action<ILBInSpaceShip, Vector3D> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnJumpPrepareEnd(Action<bool> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnPrepare4Jump(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnRemoveFromSolarSystem(Action<ILBInSpaceShip> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnStartSolarSystemTeleport(Action<int, uint, TeleportEffect> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnStartSolarSystemTeleport2Scene(Action<int, int, SceneType, TeleportEffect> action)
        {
        }

        public ConfigDataSpaceShipInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimSpaceShip.MoveCmd CurrMoveCmd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

