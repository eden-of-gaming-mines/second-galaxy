﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IClientSolarSytemSpaceContext : ILBSpaceContext, ILBSpaceProcessContainer
    {
        GDBSolarSystemInfo GetGDBSolarSystemConf();
        GDBStargateInfo GetGDBStarGateByGDBConfId(int gdbConfId);
        T GetObjectByObjId<T>(uint id) where T: ClientSpaceObject;
        uint GetTickDelta();
        void RemoveObjectOnTickEnd(ClientSpaceObject spaceObj);
    }
}

