﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ClientSpaceSceneObject : ClientSpaceObject
    {
        protected SceneObject3DInfo m_conf3DInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_get_Conf3DInfo;
        private static DelegateBridge __Hotfix_get_SimObject;
        private static DelegateBridge __Hotfix_set_SimObject;
        private static DelegateBridge __Hotfix_get_Scale;

        [MethodImpl(0x8000)]
        public ClientSpaceSceneObject(uint objId, int sceneConfId, int index, IClientSolarSytemSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetName()
        {
        }

        public SceneObject3DInfo Conf3DInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimSpaceSimpleObject SimObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float Scale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

