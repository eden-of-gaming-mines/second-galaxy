﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompEventDispatcher
    {
        protected LogicBlockShipCompInSpaceBasicCtxClient m_lbInSpaceBasicCtx;
        protected LogicBlockShipCompFireControllClient m_lbFireControll;
        protected LogicBlockShipCompInSpaceWeaponEquipClient m_lbInSpaceWeaponEquip;
        protected LogicBlockShipCompSpaceTargetClient m_lbInSpaceTarget;
        protected LogicBlockShipCompDroneFireControllClient m_lbDroneFireControll;
        protected LogicBlockShipCompSpaceObjectClient m_lbSpaceObject;
        protected LogicBlockShipCompAOIClient m_lbAOI;
        protected LogicBlockShipCompBufContainerClient m_lbBuffContainer;
        protected LogicBlockShipCompPickDropBoxClient m_lbPickDropBox;
        protected LogicBlockShipCompWingShipCtrlClient m_lbWingShip;
        protected LogicBlockShipCompIFFBase m_lbIFF;
        protected LogicBlockShipCompInteractionMakerClient m_lbInteractionMaker;
        protected LogicBlockShipCompItemStoreClient m_lbShipItemStore;
        protected LogicBlockShipCompSceneBase m_lbShipScene;
        protected ClientSpaceShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_RegEventsForShip;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnInvisibleStart;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnInvisibleEnd;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnBlinkInterrupted;
        private static DelegateBridge __Hotfix_CallbackForShipEnterScene;
        private static DelegateBridge __Hotfix_CallbackForShipLeaveScene;
        private static DelegateBridge __Hotfix_CallbackForTargetEnterIFF;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnLockTarget;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnLockedTargetLost;
        private static DelegateBridge __Hotfix_CallbackForTargetAdd2HateList;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnShieldBroken;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnDead;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnSyncEventHit;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnSyncEventBufDotDamage;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnEnterFight;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnLeaveFight;
        private static DelegateBridge __Hotfix_CallbackForDefenderAttackMissile;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnHitByBullet;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnHitByLaser;
        private static DelegateBridge __Hotfix_CallbackForShipEventOnHitByDrone;
        private static DelegateBridge __Hotfix_CallbackForShipTargetNoticeChanged;
        private static DelegateBridge __Hotfix_CallbackForShipHideInTargetListChanged;
        private static DelegateBridge __Hotfix_CallbackForSuperGroupEnergyFull;
        private static DelegateBridge __Hotfix_CallbackForSuperGroupEnergyChangeToEmpty;
        private static DelegateBridge __Hotfix_CallbackForResetWeaponEquipGroupCD;
        private static DelegateBridge __Hotfix_CallbackForWeaponAmmoReloadByPercent;
        private static DelegateBridge __Hotfix_CallbackForSyncMoveState;
        private static DelegateBridge __Hotfix_CallbackForJumpPrepare;
        private static DelegateBridge __Hotfix_CallbackForStartJumping;
        private static DelegateBridge __Hotfix_CallbackForJumpPrepareEnd;
        private static DelegateBridge __Hotfix_CallbackForJumpEnd;
        private static DelegateBridge __Hotfix_CallbackForEnterHighSpeedJumping;
        private static DelegateBridge __Hotfix_CallbackForLeaveHighSpeedJumping;
        private static DelegateBridge __Hotfix_CallbackForMovementCompleted;
        private static DelegateBridge __Hotfix_CallbackForArroundTargetMovementModuleStartToCloseToTarget;
        private static DelegateBridge __Hotfix_CallbackForArroundTargetMovementModuleStartToArroundToTarget;
        private static DelegateBridge __Hotfix_CallbackForNPCShipAnimEffect;
        private static DelegateBridge __Hotfix_RegEventsForWeaponGroups;
        private static DelegateBridge __Hotfix_DispatchEventsForNormalAttackWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForNormalAttackEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForNormalAttackEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForNormalAttackEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForNormalAttackReloadAmmoStart;
        private static DelegateBridge __Hotfix_CallbackForNormalAttackReloadAmmoEnd;
        private static DelegateBridge __Hotfix_DispatchEventsForRailgunWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForRailgunEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForRailgunEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForRailgunEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForRailgunReloadAmmoStart;
        private static DelegateBridge __Hotfix_CallbackForRailgunReloadAmmoEnd;
        private static DelegateBridge __Hotfix_DispatchEventsForPlasmaWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForPlasmaEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForPlasmaEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForPlasmaEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForPlasmaReloadAmmoStart;
        private static DelegateBridge __Hotfix_CallbackForPlasmaReloadAmmoEnd;
        private static DelegateBridge __Hotfix_DispatchEventsForCannonWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForCannonEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForCannonEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForCannonEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForCannonReloadAmmoStart;
        private static DelegateBridge __Hotfix_CallbackForCannonReloadAmmoEnd;
        private static DelegateBridge __Hotfix_DispatchEventsForMissileWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForMissileEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForMissileEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForMissileEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForMissileReloadAmmoStart;
        private static DelegateBridge __Hotfix_CallbackForMissileReloadAmmoEnd;
        private static DelegateBridge __Hotfix_DispatchEventsForLaserWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForLaserEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForLaserEventOnSingleFireCancel;
        private static DelegateBridge __Hotfix_CallbackForLaserEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForLaserEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForLaserEventOnReloadAmmoStart;
        private static DelegateBridge __Hotfix_CallbackForLaserEventOnReloadAmmoEnd;
        private static DelegateBridge __Hotfix_DispatchEventsForDroneWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForDroneEventOnFighterFire;
        private static DelegateBridge __Hotfix_CallbackForDroneDefenderLaunch;
        private static DelegateBridge __Hotfix_CallbackForDroneDestoryedByDefender;
        private static DelegateBridge __Hotfix_CallbackForDroneFighterLaunch;
        private static DelegateBridge __Hotfix_CallbackForDroneReturnFinished;
        private static DelegateBridge __Hotfix_CallbackForDroneHitByDefender;
        private static DelegateBridge __Hotfix_CallbackForDroneSelfExplode;
        private static DelegateBridge __Hotfix_CallbackForDroneSniperLaunch;
        private static DelegateBridge __Hotfix_CallbackForDroneStateChange;
        private static DelegateBridge __Hotfix_CallbackForDroneUnitFire;
        private static DelegateBridge __Hotfix_CallbackForDroneStartCharge;
        private static DelegateBridge __Hotfix_CallBackForHighSlotWeaponLaunchCycleStart;
        private static DelegateBridge __Hotfix_CallbackForWeaponLaunch;
        private static DelegateBridge __Hotfix_RegEventsForSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForSuperWeaponEquipLaunchable;
        private static DelegateBridge __Hotfix_DispatchEventsForRailgunSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForSuperRailgunEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForSuperRailgunEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForSuperRailgunEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_DispatchEventsForPlasmaSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForSuperPlasmaEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForSuperPlasmaEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForSuperPlasmaEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_DispatchEventsForMissileSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForSuperMissileEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForSuperMissileEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForSuperMissileEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_DispatchEventsForLaserSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForSuperLaserEventOnLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForSuperLaserEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForSuperLaserEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForSuperLaserEventOnSingleFireCancel;
        private static DelegateBridge __Hotfix_DispatchEventsForDroneSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CallbackForSuperDroneEventOnUnitFire;
        private static DelegateBridge __Hotfix_CallbackForSuperDroneEventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_CallbackForSuperDroneEventOnDroneStateChange;
        private static DelegateBridge __Hotfix_CallbackForSuperDroneEventOnDroneSelfExplode;
        private static DelegateBridge __Hotfix_CallbackForSuperDroneEventOnDroneFlyReturnFinished;
        private static DelegateBridge __Hotfix_CallbackForSuperDroneEventOnDroneFighterLaunch;
        private static DelegateBridge __Hotfix_CallbackForSuperDroneEventOnDroneFighterFire;
        private static DelegateBridge __Hotfix_RegEventsForEquipGroups;
        private static DelegateBridge __Hotfix_CallbackForEquipLaunch;
        private static DelegateBridge __Hotfix_CallbackForEquipCharge;
        private static DelegateBridge __Hotfix_CallbackForEquipFire;
        private static DelegateBridge __Hotfix_CallbackForEquipLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForEquipLaunchEnd;
        private static DelegateBridge __Hotfix_CallbackForEquipEffect;
        private static DelegateBridge __Hotfix_RegEventsForTacticalEquipGroup;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquip_PropertiesProviderByTargetShieldPercent;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquip_KeepingInvisibleForWeaponLaunch;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquip_AttachBufByShieldPercent;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquip_AttachBufByShieldPercent;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquip_DetachBufByShieldPercent;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquip_AttachBuf2SelfByEnergyAndShieldPercent;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquip_AttachBuf2SelfByEnergyAndShieldPercent;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquip_AttachBufByEnergyPercent;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquip_AttachBufByEnergyPercent;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipIncByNearByDead;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipIncBuff2AttachBuffByWeaponEquipLaunch;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipIncByNearByDead_TargetDead;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipIncByNearByDead_BuffAttack;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipIncByNearByDead_IncBuffDetach;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipGlobalDynamicPropertiesProvider;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGlobalDynamicPropertiesProviderActivate;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipAttachBufAndInvisibleByArmorPercent;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipEffectStart;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipEffectEnd;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipRecoverySuperEnergyByKillingHit;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipRecoverySuperEnergyByKillingHit;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipRecoveryEnergyByKillingHit;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipRecoveryEnergyByKillingHit;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipInvisibleAndCleanPlasmaCDAndAttachBufByArmorPercent;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipLaunch;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipCharge;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipFire;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipLaunchEnd;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipEffect;
        private static DelegateBridge __Hotfix_DispatchEventsForTacticalEquipAttachBufBySupperEnergyFull;
        private static DelegateBridge __Hotfix_CallbackForEquipAttachBufBySupperEnergyFullEffectStart;
        private static DelegateBridge __Hotfix_CallbackForEquipAttachBufBySupperEnergyFullEffectEnd;
        private static DelegateBridge __Hotfix_DispatchEventForAttackBuff2SelfByCritical;
        private static DelegateBridge __Hotfix_CallbackForAttackBuff2SelfByCriticalEffectStart;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipIncBufRecoveryShieldByShiledRepairEquip;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipRecoveryShield;
        private static DelegateBridge __Hotfix_CallbackForRecoveryShield;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipIncBufRecovergyEnergyByMakeDamage;
        private static DelegateBridge __Hotfix_CallbackForTacTicalEquipGroupIncBufRecoveryEnergyByMakeDamage;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipGroupReduceCDByKillingHit;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupReduceCDByKillingHit_EquipCDComplete;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupReduceCDByKillingHit_EquipEnterCD;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupReduceCDByKillingHit_ReduceCD;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipGroupReduceCDByWeaponEquipLaunch;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupReduceCDByWeaponEquipLaunch_ReduceCD;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipAttachBuf2SelfByDodge;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipAttachBuf2SelfByDodge;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipAttachBufByKillingHit;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupAttachBufByKillingHit;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupAttachBufByKillingHitEffectEnd;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalAttachBuffByNotBeHitForAWhile;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupAttachBuffByNotBeHitForAWhile;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalAddShiledExBufByCostEnergy;
        private static DelegateBridge __Hotfix_CallbackForTackticalAddShiledExBufByCostEnergy;
        private static DelegateBridge __Hotfix_CallbackForTackticalAddShiledExBufByCostEnergyEffectEnd;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalAddShiledAndBuffByArmor;
        private static DelegateBridge __Hotfix_CallbackForTacticalAddShiledAndBuffByArmor;
        private static DelegateBridge __Hotfix_DispatchEventForTacticalEquipGroupIncBufByWeaponLaunch;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipGroupLastIncBufDetach;
        private static DelegateBridge __Hotfix_CallbackForTacticalEquipPropertyProviderByTargetShieldPercent_Active;
        private static DelegateBridge __Hotfix_RegEventsForSuperEquipGroup;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAddShieldAndAttachBuf;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipBlinkAndAttachBuf;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAttachBuf2Self;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAddShieldWhenEnergyCosting;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAddExtraShieldByCurrEnergy;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAttachBuf2Enemy;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquiChannelMakeDamageAndAttachBuf;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipTranslateEnergyFromDamageOfTarget;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipPeriodicDamageAndRecoveryEnergy;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAddEnergyAndAttachBuf;
        private static DelegateBridge __Hotfix_DispatchEventForSuperEquipShipShield2ShieldExAndAttachBuff2Self;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAoeAndAttachBuf;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipClearCDAndAttachBuff2Self;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipDamageRebound;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipBlinkAndAoeAttacBuf2Self;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipAntiJumpingForceShield;
        private static DelegateBridge __Hotfix_DispatchEventsForSuperEquipTransformToTeleportTunnel;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipBlinkAndAoeAttachBuf_ProcessFire;
        private static DelegateBridge __Hotfix_CalbackForSuperEquipRecoverEnergy;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipAoeAndAttachBuf_ProcessFire;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipDamageRebound_ProcessFireForAoe;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipLaunch;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipCharge;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipFire;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipLaunchCancel;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipLaunchEnd;
        private static DelegateBridge __Hotfix_CallbackForSuperEquipEffect;
        private static DelegateBridge __Hotfix_RegEventsForBuffContainer;
        private static DelegateBridge __Hotfix_CallbackForAttachBuf;
        private static DelegateBridge __Hotfix_CallbackForDetachBuf;
        private static DelegateBridge __Hotfix_CallbackForAttachBufDuplicate;
        private static DelegateBridge __Hotfix_CallbackForAttachBufFromWormhole;
        private static DelegateBridge __Hotfix_RegEventsForPickDropBox;
        private static DelegateBridge __Hotfix_CallbackForPickDropBoxStart;
        private static DelegateBridge __Hotfix_CallbackForPickDropBoxEnd;
        private static DelegateBridge __Hotfix_RegEventsForAOI;
        private static DelegateBridge __Hotfix_CallbackForTargetEnterView;
        private static DelegateBridge __Hotfix_CallbackForTargetLeaveView;
        private static DelegateBridge __Hotfix_CallbackForDropBoxEnterView;
        private static DelegateBridge __Hotfix_CallbackForDropBoxLeaveView;
        private static DelegateBridge __Hotfix_RegEventsForWingShip;
        private static DelegateBridge __Hotfix_CallbackForWingShipSet;
        private static DelegateBridge __Hotfix_CallbackForWingShipUnSet;
        private static DelegateBridge __Hotfix_CallbackForWingShipLeaveView;
        private static DelegateBridge __Hotfix_CallbackForSceneWingShipSettingChanged;
        private static DelegateBridge __Hotfix_RegEventForIFF;
        private static DelegateBridge __Hotfix_CallbackForNeutralTargetChangeToEnemy;
        private static DelegateBridge __Hotfix_RegEventForInteractionMaker;
        private static DelegateBridge __Hotfix_CallbackForStartInteraction;
        private static DelegateBridge __Hotfix_CallbackForInteractionEnd;
        private static DelegateBridge __Hotfix_RegEventsForShipItemStore;
        private static DelegateBridge __Hotfix_CallbackForShipItemStoreAddItem;
        private static DelegateBridge __Hotfix_CallbackForShipItemStoreRemoveItem;
        private static DelegateBridge __Hotfix_RegEventsForFlagShip;
        private static DelegateBridge __Hotfix_CallbackForFlagShipTeleportStartCharging;
        private static DelegateBridge __Hotfix_CallbackForFlagShipTeleportChargingFinish;
        private static DelegateBridge __Hotfix_CallbackForFlagShipTeleportChargingFail;
        private static DelegateBridge __Hotfix_CallbackForFlagShipTeleportProcessingFail;

        [MethodImpl(0x8000)]
        protected void CalbackForSuperEquipRecoverEnergy(LBSuperEquipGroupBase equipGroup, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForArroundTargetMovementModuleStartToArroundToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForArroundTargetMovementModuleStartToCloseToTarget(double radius, uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForAttachBuf(LBBufBase buf, ILBBufSource bufSource)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForAttachBufDuplicate(LBBufBase buf, List<uint> sourceTargetIdList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForAttachBufFromWormhole(LBBufBase buf, bool isContinueBuf)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForAttackBuff2SelfByCriticalEffectStart(float durTime, LBTacticalEquipGroupBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForCannonEventOnLaunchCancel(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForCannonEventOnUnitFire(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, LBSpaceProcessBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForCannonEventOnUnitStartCharge(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForCannonReloadAmmoEnd(LBWeaponGroupBase weaponGroup, ItemInfo reloadAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForCannonReloadAmmoStart(LBWeaponGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDefenderAttackMissile(LBSpaceProcessDroneDefenderLaunch.AttackChance attackChance)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDetachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneDefenderLaunch(LBWeaponGroupDroneClient weaponGroup, LBSpaceProcessDroneDefenderLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneDestoryedByDefender(LBSpaceProcessDroneFighterLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneEventOnFighterFire(LBSpaceProcessDroneLaunchBase launchProcess, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneFighterLaunch(LBWeaponGroupDroneClient weaponGroup, LBSpaceProcessDroneFighterLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneHitByDefender(LBSpaceProcessDroneFighterLaunch fighterProcess, LBSpaceProcessDroneDefenderLaunch defenderProcess, LBSpaceProcessDroneDefenderLaunch.AttackChance attackChange)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneReturnFinished(LBSpaceProcessDroneLaunchBase launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneSelfExplode(LBSpaceProcessDroneLaunchBase launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneSniperLaunch(LBWeaponGroupDroneClient weaponGroup, LBSpaceProcessDroneSniperLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneStartCharge(LBWeaponGroupDroneClient weaponGroup, LBSpaceProcessDroneLaunchBase launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneStateChange(LBSpaceProcessDroneLaunchBase launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDroneUnitFire(LBWeaponGroupDroneClient weaponGroup, LBSpaceProcessDroneLaunchBase launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDropBoxEnterView(ILBSpaceDropBox dropBox)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForDropBoxLeaveView(ILBSpaceDropBox dropBox)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEnterHighSpeedJumping(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipAttachBufBySupperEnergyFullEffectEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipAttachBufBySupperEnergyFullEffectStart()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipCharge(LBEquipGroupBase equip, StructProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipEffect(LBEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process, string effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipFire(LBEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipLaunch(LBEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipLaunchCancel(LBEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForEquipLaunchEnd(LBEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        private void CallbackForFlagShipTeleportChargingFail()
        {
        }

        [MethodImpl(0x8000)]
        private void CallbackForFlagShipTeleportChargingFinish()
        {
        }

        [MethodImpl(0x8000)]
        private void CallbackForFlagShipTeleportProcessingFail()
        {
        }

        [MethodImpl(0x8000)]
        private void CallbackForFlagShipTeleportStartCharging()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallBackForHighSlotWeaponLaunchCycleStart(LBInSpaceWeaponEquipGroupBase weapon)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForInteractionEnd(uint npcObjId, int stopReason)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForJumpEnd(ILBInSpaceShip srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForJumpPrepare()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForJumpPrepareEnd(bool isJumpingEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForLaserEventOnLaunchCancel(LBWeaponGroupLaserClient weaponGroup, LBSpaceProcessLaserLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForLaserEventOnReloadAmmoEnd(LBWeaponGroupBase weaponGroup, ItemInfo reloadAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForLaserEventOnReloadAmmoStart(LBWeaponGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForLaserEventOnSingleFireCancel(LBWeaponGroupLaserClient weaponGroup, LBSpaceProcessLaserSingleFire singleFireProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForLaserEventOnUnitFire(LBWeaponGroupLaserClient weaponGroup, LBSpaceProcessLaserLaunch launchProcess, LBSpaceProcessLaserSingleFire singleFireProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForLaserEventOnUnitStartCharge(LBWeaponGroupLaserClient weaponGroup, LBSpaceProcessLaserLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForLeaveHighSpeedJumping()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForMissileEventOnLaunchCancel(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForMissileEventOnUnitFire(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, LBSpaceProcessBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForMissileEventOnUnitStartCharge(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForMissileReloadAmmoEnd(LBWeaponGroupBase weaponGroup, ItemInfo reloadAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForMissileReloadAmmoStart(LBWeaponGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForMovementCompleted(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForNeutralTargetChangeToEnemy(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForNormalAttackEventOnLaunchCancel(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForNormalAttackEventOnUnitFire(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, LBSpaceProcessBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForNormalAttackEventOnUnitStartCharge(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForNormalAttackReloadAmmoEnd(LBWeaponGroupBase weaponGroup, ItemInfo reloadAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForNormalAttackReloadAmmoStart(LBWeaponGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForNPCShipAnimEffect(NpcShipAnimEffectType effect, uint startTime, uint endTime, int openParam)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForPickDropBoxEnd(ILBSpaceDropBox dropBox, List<ShipStoreItemInfo> itemList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForPickDropBoxStart(ILBSpaceDropBox dropBox, float time)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForPlasmaEventOnLaunchCancel(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForPlasmaEventOnUnitFire(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, LBSpaceProcessBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForPlasmaEventOnUnitStartCharge(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForPlasmaReloadAmmoEnd(LBWeaponGroupBase weaponGroup, ItemInfo reloadAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForPlasmaReloadAmmoStart(LBWeaponGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForRailgunEventOnLaunchCancel(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForRailgunEventOnUnitFire(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, LBSpaceProcessBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForRailgunEventOnUnitStartCharge(LBWeaponGroupBase weaponGroup, LBSpaceProcessBulletGunLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForRailgunReloadAmmoEnd(LBWeaponGroupBase weaponGroup, ItemInfo reloadAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForRailgunReloadAmmoStart(LBWeaponGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForRecoveryShield(LBTacticalEquipGroupBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForResetWeaponEquipGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSceneWingShipSettingChanged(SceneWingShipSetting setting, bool ignoreCD)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEnterScene()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnBlinkInterrupted(int reason)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnDead(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnHitByBullet(LBSpaceProcess bulletFlyProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnHitByDrone(LBSpaceProcess launchProcess, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnHitByLaser(LBSpaceProcess laserSingleFireProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnInvisibleEnd(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnInvisibleStart(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnLeaveFight()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnLockedTargetLost()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnLockTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnShieldBroken()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnSyncEventBufDotDamage(uint bufSrcObjectId, ulong bufInstanceId, float value, BufType bufType)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipEventOnSyncEventHit(LBSyncEventOnHitInfo onHitInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipHideInTargetListChanged(bool isHide)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipItemStoreAddItem(List<ShipStoreItemInfo> itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipItemStoreRemoveItem(List<ShipStoreItemInfo> itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipLeaveScene()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForShipTargetNoticeChanged(TargetNoticeType targetNotice)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForStartInteraction(uint npcObjId, uint endSolarSystemTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForStartJumping(ILBInSpaceShip ship, Vector3D dest)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperDroneEventOnDroneFighterFire(LBSpaceProcessDroneFighterLaunch launchProcess, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperDroneEventOnDroneFighterLaunch(List<LBSpaceProcessDroneFighterLaunch> launchProcessList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperDroneEventOnDroneFlyReturnFinished(LBSpaceProcessDroneFighterLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperDroneEventOnDroneSelfExplode(LBSpaceProcessDroneFighterLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperDroneEventOnDroneStateChange(LBSpaceProcessDroneFighterLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperDroneEventOnUnitFire(LBSuperWeaponGroupDroneClient weaponGroup, LBSpaceProcessDroneFighterLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperDroneEventOnUnitStartCharge(LBSuperWeaponGroupDroneClient weaponGroup, LBSpaceProcessDroneFighterLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipAoeAndAttachBuf_ProcessFire(LBSuperEquipGroupBase equipGroup, StructProcessSuperEquipAoeLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipBlinkAndAoeAttachBuf_ProcessFire(LBSuperEquipGroupBase equipGroup, StructProcessSuperEquipAoeLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipCharge(LBSuperEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipDamageRebound_ProcessFireForAoe(LBSuperEquipGroupBase equipGroup, StructProcessSuperEquipAoeLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipEffect(LBSuperEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process, string effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipFire(LBSuperEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipLaunch(LBSuperEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipLaunchCancel(LBSuperEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperEquipLaunchEnd(LBSuperEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperGroupEnergyChangeToEmpty()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperGroupEnergyFull()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperLaserEventOnLaunchCancel(LBSuperWeaponGroupLaserClient weaponGroup, LBSpaceProcessSuperLaserLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperLaserEventOnSingleFireCancel(LBSuperWeaponGroupLaserClient weaponGroup, LBSpaceProcessSuperLaserSingleFire laserSingleFireProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperLaserEventOnUnitFire(LBSuperWeaponGroupLaserClient weaponGroup, LBSpaceProcessSuperLaserLaunch launchProcess, LBSpaceProcessSuperLaserSingleFire laserSingleFireProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperLaserEventOnUnitStartCharge(LBSuperWeaponGroupLaserClient weaponGroup, LBSpaceProcessSuperLaserLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperMissileEventOnLaunchCancel(LBSuperWeaponGroupMissileClient weaponGroup, LBSpaceProcessSuperMissileLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperMissileEventOnUnitFire(LBSuperWeaponGroupMissileClient weaponGroup, LBSpaceProcessSuperMissileLaunch launchProcess, LBSpaceProcessBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperMissileEventOnUnitStartCharge(LBSuperWeaponGroupMissileClient weaponGroup, LBSpaceProcessSuperMissileLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperPlasmaEventOnLaunchCancel(LBSuperWeaponGroupPlasmaClient weaponGroup, LBSpaceProcessSuperWeaponLaunchBase launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperPlasmaEventOnUnitFire(LBSuperWeaponGroupPlasmaClient weaponGroup, LBSpaceProcessSuperWeaponLaunchBase launchProcess, LBSpaceProcessBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperPlasmaEventOnUnitStartCharge(LBSuperWeaponGroupPlasmaClient weaponGroup, LBSpaceProcessSuperWeaponLaunchBase launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperRailgunEventOnLaunchCancel(LBSuperWeaponGroupRailgunClient weaponGroup, LBSpaceProcessSuperRailgunLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperRailgunEventOnUnitFire(LBSuperWeaponGroupRailgunClient weaponGroup, LBSpaceProcessSuperRailgunLaunch launchProcess, LBSpaceProcessSuperRailgunBulletFly bulletFlyProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperRailgunEventOnUnitStartCharge(LBSuperWeaponGroupRailgunClient weaponGroup, LBSpaceProcessSuperRailgunLaunch launchProcess, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSuperWeaponEquipLaunchable(bool hasTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForSyncMoveState(SimSpaceShip.MoveCmd cmd)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTackticalAddShiledExBufByCostEnergy(float buf, float shield, LBTacticalEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTackticalAddShiledExBufByCostEnergyEffectEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalAddShiledAndBuffByArmor(uint endBuffTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquip_AttachBuf2SelfByEnergyAndShieldPercent(bool shield, bool energy)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquip_AttachBufByEnergyPercent(bool isShield, float value, int pid)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquip_AttachBufByShieldPercent(bool isTrigger)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquip_DetachBufByShieldPercent(bool type)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipAttachBuf2SelfByDodge(uint bufEndTime, LBTacticalEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipCharge(LBTacticalEquipGroupBase equip, StructProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipEffect(LBTacticalEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process, string effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipEffectEnd(uint endTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipEffectStart(uint endTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipFire(LBTacticalEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGlobalDynamicPropertiesProviderActivate(LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupAttachBufByKillingHit(float durTime, LBTacticalEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupAttachBufByKillingHitEffectEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupAttachBuffByNotBeHitForAWhile(float durTime, float endTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacTicalEquipGroupIncBufRecoveryEnergyByMakeDamage(LBTacticalEquipGroupBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupLastIncBufDetach(LBTacticalEquipGroupBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupReduceCDByKillingHit_EquipCDComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupReduceCDByKillingHit_EquipEnterCD(LBInSpaceWeaponEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupReduceCDByKillingHit_ReduceCD(LBTacticalEquipGroupBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipGroupReduceCDByWeaponEquipLaunch_ReduceCD(LBTacticalEquipGroupBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipIncByNearByDead_BuffAttack(int plies, int maxLevel, bool isFullPlies, uint bufEndTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipIncByNearByDead_IncBuffDetach(LBTacticalEquipGroupBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipIncByNearByDead_TargetDead(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipLaunch(LBTacticalEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipLaunchCancel(LBTacticalEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipLaunchEnd(LBTacticalEquipGroupBase equip, LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipPropertyProviderByTargetShieldPercent_Active(LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipRecoveryEnergyByKillingHit(uint cdEndTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTacticalEquipRecoverySuperEnergyByKillingHit()
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTargetAdd2HateList(ILBSpaceTarget target, ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        private void CallbackForTargetEnterIFF(ILBSpaceTarget target, IFFState state)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTargetEnterView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForTargetLeaveView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForWeaponAmmoReloadByPercent(List<ItemInfo> ammoReloadInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForWeaponLaunch(LBInSpaceWeaponEquipGroupBase weapon, LBSpaceProcessWeaponLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForWingShipLeaveView(ILBInSpaceNpcShip wingShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForWingShipSet(ILBInSpaceNpcShip wingShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CallbackForWingShipUnSet(ILBInSpaceNpcShip wingShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForAttackBuff2SelfByCritical(LBTacticalEquipGroupAttachBufByCriticalClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForSuperEquipShipShield2ShieldExAndAttachBuff2Self(LBSuperEquipGroupShipShield2ExtraShieldAndAttachBuff2SelfClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalAddShiledAndBuffByArmor(LBTacticalEquipGroupRecoveryShieldAndAttachBuffByArmorClient equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalAddShiledExBufByCostEnergy(LBTacticalEquipGroupAttachShieldExBufByCostEnergyClient groupEquip)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalAttachBuffByNotBeHitForAWhile(LBTacticalEquipGroupAttachBuffByNotBeHitForAWhileClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventForTacticalEquip_AttachBuf2SelfByEnergyAndShieldPercent(LBTacticalEquipGroupAttachBuf2SelfByEnergyAndShieldPercentClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventForTacticalEquip_AttachBufByEnergyPercent(LBTacticalEquipGroupAttachBuf2SelfByEnergyPercentClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventForTacticalEquip_AttachBufByShieldPercent(LBTacticalEquipGroupAttachBuf2SelfByShieldPercentClient equip)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventForTacticalEquip_KeepingInvisibleForWeaponLaunch(LBTacticalEquipGroupKeepingInvisibleForWeaponLaunchClient equip)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventForTacticalEquip_PropertiesProviderByTargetShieldPercent(LBTacticalEquipGroupPropertiesProviderByTargetShieldPercent equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipAttachBuf2SelfByDodge(LBTacticalEquipGroupAttachBuf2SelfByDodgeClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipAttachBufByKillingHit(LBTacticalEquipGroupAttachBuffByKillingHitClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipGroupIncBufByWeaponLaunch(LBTacticalEquipGroupIncBufByWeaponLaunchClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipGroupReduceCDByKillingHit(LBTacticalEquipGroupReduceCDByKillingHitClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipGroupReduceCDByWeaponEquipLaunch(LBTacticalEquipGroupReduceCDByWeaponEquipLaunchClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipIncBufRecovergyEnergyByMakeDamage(LBTacticalEquipGroupIncBufRecoveryEnergyOnNextMakeDamageByWeaponEquipLaunchClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipIncBufRecoveryShieldByShiledRepairEquip(LBTacticalEquipGroupIncBufRecoveryShiledOnDetachByShiledRepairEquiplaunchClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventForTacticalEquipRecoveryShield(LBTacticalEquipGroupIncBufRecoveryShieldOnDetachByWeaponEquipLaunchClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForCannonWeaponGroup(LBWeaponGroupCannonClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForDroneSuperWeaponGroup(LBSuperWeaponGroupDroneClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForDroneWeaponGroup(LBWeaponGroupDroneClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForLaserSuperWeaponGroup(LBSuperWeaponGroupLaserClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForLaserWeaponGroup(LBWeaponGroupLaserClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForMissileSuperWeaponGroup(LBSuperWeaponGroupMissileClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForMissileWeaponGroup(LBWeaponGroupMissileClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForNormalAttackWeaponGroup(LBWeaponGroupNormalAttackClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForPlasmaSuperWeaponGroup(LBSuperWeaponGroupPlasmaClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForPlasmaWeaponGroup(LBWeaponGroupPlasmaClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForRailgunSuperWeaponGroup(LBSuperWeaponGroupRailgunClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForRailgunWeaponGroup(LBWeaponGroupRailgunClient weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquiChannelMakeDamageAndAttachBuf(LBSuperEquipGroupChannelClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAddEnergyAndAttachBuf(LBSuperEquipGroupAddEnergyAndAttachBufClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAddExtraShieldByCurrEnergy(LBSuperEquipGroupAddExtraShieldByCurrEnergyClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAddShieldAndAttachBuf(LBSuperEquipGroupAddShieldAndAttachBufClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAddShieldWhenEnergyCosting(LBSuperEquipGroupAddShieldWhenEnergyCostingClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAntiJumpingForceShield(LBSuperEquipGroupAntiJumpingForceShieldAndAttachBufClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAoeAndAttachBuf(LBSuperEquipGroupAoeAndAttachBufClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAttachBuf2Enemy(LBSuperEquipGroupAttachBuf2EnemyClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipAttachBuf2Self(LBSuperEquipGroupAttachBuf2SelfClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipBlinkAndAoeAttacBuf2Self(LBSuperEquipGroupBlinkAndAoeAttachBuf2TargetClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipBlinkAndAttachBuf(LBSuperEquipGroupBlinkAndAttachBufClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipClearCDAndAttachBuff2Self(LBSuperEquipGroupClearCDAndAttachBuffClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipDamageRebound(LBSuperEquipGroupDamageReboundClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipPeriodicDamageAndRecoveryEnergy(LBSuperEquipGroupPeriodicMakeDamageAndAddEnergyClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipTransformToTeleportTunnel(LBSuperEquipGroupFlagShipTeleportTunnelClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForSuperEquipTranslateEnergyFromDamageOfTarget(LBSuperEquipGroupTranslateEnergyFromDamageOfTargetClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForTacticalEquipAttachBufAndInvisibleByArmorPercent(LBTacticalEquipGroupAttachBufAndInvisibleByArmorPercentClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForTacticalEquipAttachBufBySupperEnergyFull(LBTacticalEquipGroupAttachBufBySupperEnergyFullClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventsForTacticalEquipGlobalDynamicPropertiesProvider(LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventsForTacticalEquipIncBuff2AttachBuffByWeaponEquipLaunch(LBTacticalEquipGroupIncBufByWeaponEquipLaunchClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventsForTacticalEquipIncByNearByDead(LBTacticalEquipGroupIncBufByNeadbyDeadClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void DispatchEventsForTacticalEquipInvisibleAndCleanPlasmaCDAndAttachBufByArmorPercent(LBTacticalEquipGroupInvisibleAndCleanPlasmaCDAndAttachBufByArmorPercentClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventsForTacticalEquipRecoveryEnergyByKillingHit(LBTacticalEquipGroupRecoveryEnergyByKillingHitClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool DispatchEventsForTacticalEquipRecoverySuperEnergyByKillingHit(LBTacticalEquipGroupRecoverySuperEnergyByKillingHitClient equipGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ClientSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventForIFF()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventForInteractionMaker()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForAOI()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForBuffContainer()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForEquipGroups()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForFlagShip()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForPickDropBox()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForShip()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForShipItemStore()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForSuperEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForSuperWeaponGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForTacticalEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForWeaponGroups()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RegEventsForWingShip()
        {
        }
    }
}

