﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ClientStarGate : ClientSpaceObject
    {
        protected int m_confId;
        protected GDBStargateInfo m_confInfo;
        protected ConfigDataStargate3DInfo m_conf3DInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetStarGateResPath;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_GetShipArroundDistance;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_get_Conf3DInfo;
        private static DelegateBridge __Hotfix_get_DestationSolarSystemId;
        private static DelegateBridge __Hotfix_get_SimObject;
        private static DelegateBridge __Hotfix_set_SimObject;

        [MethodImpl(0x8000)]
        public ClientStarGate(uint objId, GDBStargateInfo confInfo, IClientSolarSytemSpaceContext spaceContext)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipArroundDistance()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        public string GetStarGateResPath()
        {
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GDBStargateInfo ConfInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataStargate3DInfo Conf3DInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int DestationSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SimStarGate SimObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

