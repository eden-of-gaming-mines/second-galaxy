﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ClientSelfPlayerSpaceShip4FlagShip : ClientSelfPlayerSpaceShip, ILBInSpaceFlagShip, ILBInSpacePlayerShip, ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        protected LogicBlockFlagShipCompInSpaceBasicInfoClient m_lbFlagShipBasicInfo;
        protected LogicBlockFlagShipCompTeleportClient m_lbFlagShipTeleport;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitializeAllComp;
        private static DelegateBridge __Hotfix_CreateShipCompItemStore;
        private static DelegateBridge __Hotfix_GetLBFlagShipBasicInfo;
        private static DelegateBridge __Hotfix_GetLBFlagShipTeleport;

        [MethodImpl(0x8000)]
        public ClientSelfPlayerSpaceShip4FlagShip(uint objId, int confId, IInSpaceFlagShipDataContainer dc, ILBInSpacePlayerShipCaptain captain, IClientSolarSytemSpaceContext spaceContext, IPlayerInfoProvider playerInfoProvider, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompItemStoreClient CreateShipCompItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFlagShipCompInSpaceBasicInfoBase GetLBFlagShipBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFlagShipCompTeleportBase GetLBFlagShipTeleport()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool PostInitializeAllComp()
        {
        }
    }
}

