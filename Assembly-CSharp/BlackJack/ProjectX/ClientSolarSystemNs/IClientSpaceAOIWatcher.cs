﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using System;

    public interface IClientSpaceAOIWatcher
    {
        void OnObjectEnterView(ClientSpaceObject obj);
        void OnObjLeaveView(ClientSpaceObject obj);
        void OnTickOver();
    }
}

