﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ClientHiredCaptainSpaceShip : ClientNpcSpaceShip
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetCaptainFirstNameId;
        private static DelegateBridge __Hotfix_GetCaptainLastNameId;
        private static DelegateBridge __Hotfix_GetCaptainLevel;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_CreateShipCompIFF;
        private static DelegateBridge __Hotfix_IsNpcShip;
        private static DelegateBridge __Hotfix_IsHiredCaptainShip;
        private static DelegateBridge __Hotfix_IsOwnerPlayerNullOrEmpty;

        [MethodImpl(0x8000)]
        public ClientHiredCaptainSpaceShip(uint objId, int confId, IInSpaceNpcShipDataContainer dc, ILBInSpaceShipCaptain captain, NpcShipAnimEffectType createEffect, IClientSolarSytemSpaceContext spaceContext, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompIFFBase CreateShipCompIFF()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCaptainFirstNameId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCaptainLastNameId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCaptainLevel()
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHiredCaptainShip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNpcShip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOwnerPlayerNullOrEmpty()
        {
        }
    }
}

