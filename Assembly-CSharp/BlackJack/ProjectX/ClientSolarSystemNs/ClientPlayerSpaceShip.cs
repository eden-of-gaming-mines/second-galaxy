﻿namespace BlackJack.ProjectX.ClientSolarSystemNs
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class ClientPlayerSpaceShip : ClientSpaceShip, ILBInSpacePlayerShip, ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        protected LogicBlockShipCompPlayerInfoClient m_lbPlayerInfo;
        protected LogicBlockShipCompPickDropBoxClient m_lbPickDropBox;
        protected IPlayerInfoProvider m_playerInfoProvider;
        protected LogicBlockShipCompWingShipCtrlClient m_lbWingShipCtrl;
        protected LogicBlockShipCompPVPClient m_lbPVP;
        protected LogicBlockShipCompInteractionMakerClient m_lbInteractionMaker;
        protected LogicBlockShipCompGuildAllianceTeleportClient m_lbGuildAllianceTeleport;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitializeAllComp;
        private static DelegateBridge __Hotfix_GetInstanceType;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_CreateShipCompIFF;
        private static DelegateBridge __Hotfix_CreateShipCompItemStore;
        private static DelegateBridge __Hotfix_IsPlayerShip;
        private static DelegateBridge __Hotfix_IsHiredCaptainShip;
        private static DelegateBridge __Hotfix_IsNpcShip;
        private static DelegateBridge __Hotfix_GetLBPlayerInfo;
        private static DelegateBridge __Hotfix_GetLBPickDropBox;
        private static DelegateBridge __Hotfix_GetLBWingShipCtrl;
        private static DelegateBridge __Hotfix_GetLBInteractionMaker;
        private static DelegateBridge __Hotfix_GetLBGuildAllianceTeleport;
        private static DelegateBridge __Hotfix_GetLBPVP;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_get_Captain;
        private static DelegateBridge __Hotfix_get_DataContainer;

        [MethodImpl(0x8000)]
        public ClientPlayerSpaceShip(uint objId, int confId, IInSpacePlayerShipDataContainer dc, ILBInSpacePlayerShipCaptain captain, IClientSolarSytemSpaceContext spaceContext, IPlayerInfoProvider playerInfoProvider, ShipGuildInfo guildInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompIFFBase CreateShipCompIFF()
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompItemStoreClient CreateShipCompItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public override SpaceObjectType GetInstanceType()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompGuildAllianceTeleportBase GetLBGuildAllianceTeleport()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompInteractionMakerBase GetLBInteractionMaker()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompPickDropBoxBase GetLBPickDropBox()
        {
        }

        [MethodImpl(0x8000)]
        public ILogicBlockShipCompPlayerInfo GetLBPlayerInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override LogicBlockShipCompPVPBase GetLBPVP()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWingShipCtrlBase GetLBWingShipCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHiredCaptainShip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNpcShip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsPlayerShip()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool PostInitializeAllComp()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint deltaMillisecond)
        {
        }

        protected ILBInSpacePlayerShipCaptain Captain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected IInSpacePlayerShipDataContainer DataContainer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

