﻿namespace BlackJack.ProjectX
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class EditorDummyObjectBase : MonoBehaviour
    {
        public int ConfigId;
        public int m_lastReloadConfigId;
        public object m_loadedConfigInfo;
        public string m_dummyOjbectName;
        public bool m_isCreateOnSceneCreate;
        public GameObject m_loadedGameObject;
        protected DateTime m_nextCheckReloadTime;
        private static DelegateBridge __Hotfix_GenerateUniqueName;
        private static DelegateBridge __Hotfix_UpdateName;
        private static DelegateBridge __Hotfix_TickReload;
        private static DelegateBridge __Hotfix_TryReloadDataByConfigId;
        private static DelegateBridge __Hotfix_IsNeedReloadData;
        private static DelegateBridge __Hotfix_GetConfigIdByConfInfo;
        private static DelegateBridge __Hotfix_ReloadData;
        private static DelegateBridge __Hotfix_ClearOldData;
        private static DelegateBridge __Hotfix_ClearGameObjectInDummyObject;
        private static DelegateBridge __Hotfix_GetNoneDummyObject;

        [MethodImpl(0x8000)]
        protected virtual void ClearGameObjectInDummyObject()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearOldData()
        {
        }

        [MethodImpl(0x8000)]
        protected void GenerateUniqueName()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int GetConfigIdByConfInfo(object m_loadedConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual GameObject GetNoneDummyObject()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsNeedReloadData()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ReloadData()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickReload()
        {
        }

        [MethodImpl(0x8000)]
        protected bool TryReloadDataByConfigId()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateName()
        {
        }
    }
}

