﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SpaceStationDesc : GalaxyObjectDescBase
    {
        public double PosX;
        public double PosY;
        public double PosZ;
        public double OrbitRadius;
        public double AzimuthAngle;
        public double ElevationAngle;
        public float RotationAngleY;
        public int ConfTempleteID;
        public List<NPCInfo> NPCList;
        public string WithPlanetId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <NeedReloadSpaceStationRes>k__BackingField;
        private SolarSystemDesc m_ownerSolarSystem;
        public bool isManualChanged;
        public bool isNPCListChanged;
        public bool isFactionChanged;
        public bool isGrandFactionChanged;
        public bool isStationTempleteIdWithGrandFaction;
        public int stationType;
        public int factionId;
        public int grandFactionType;
        public string desc;
        public bool isDescTextManualChanged;
        private static DelegateBridge __Hotfix_get_NeedReloadSpaceStationRes;
        private static DelegateBridge __Hotfix_set_NeedReloadSpaceStationRes;
        private static DelegateBridge __Hotfix_get_OwnerSolarSystem;

        public bool NeedReloadSpaceStationRes
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public SolarSystemDesc OwnerSolarSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [Serializable]
        public class NPCInfo
        {
            public int ID;
            public bool isGlobalNpc;
            public int FirstNameId;
            public int LastNameId;
            public int FunctionType;
            public int RoomType;
            public int Gender;
            public int PersonalityType;
            public int ResId;
            public int ScriptType;
            public int ScriptParamId;
            public int HelloDialogId;
            public bool IsHelloDialogIdManualChanged;
            public bool IsStatic;
            public List<int> AcceptableQuestIdList;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

