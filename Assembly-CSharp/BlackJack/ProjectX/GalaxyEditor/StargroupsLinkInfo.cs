﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StargroupsLinkInfo
    {
        public string m_firstStargroupGuid;
        public string m_firstSolarSystemGuid;
        public string m_firstStarGateGuid;
        public int m_fristIslandIndex;
        public string m_secondStargroupGuid;
        public string m_secondSolarSystemGuid;
        public string m_secondStarGateGuid;
        public int m_secondIslandIndex;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public StargroupsLinkInfo(string firstStargroupGuid, string firstSolarSystemGuid, string firstStarGateGuid, int fristIslandIndex, string secondStargroupGuid, string secondSolarSystemGuid, string secondStarGateGuid, int secondIslandIndex)
        {
        }
    }
}

