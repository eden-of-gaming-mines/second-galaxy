﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using System;
    using UnityEngine;

    public class GalaxyObjectDescBase : MonoBehaviour
    {
        public string m_guid;
        public int m_exportID;
        public string m_name;
        public bool m_needLocalization;
    }
}

