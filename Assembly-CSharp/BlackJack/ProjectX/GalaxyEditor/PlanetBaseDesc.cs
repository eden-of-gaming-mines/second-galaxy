﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class PlanetBaseDesc : GalaxyObjectDescBase
    {
        public double PosX;
        public double PosY;
        public double PosZ;
        public double Radius;
        public double Mass;
        public int ResourceId;
        protected SolarSystemDesc m_ownerSolarSystem;
        private static DelegateBridge __Hotfix_get_OwnerSolarSystem;

        protected PlanetBaseDesc()
        {
        }

        public abstract bool IsSameHorizont();

        public SolarSystemDesc OwnerSolarSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

