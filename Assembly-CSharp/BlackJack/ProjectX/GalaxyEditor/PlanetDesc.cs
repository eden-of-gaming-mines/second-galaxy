﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class PlanetDesc : PlanetBaseDesc
    {
        public double RevolutionPeriod;
        public double GravitationaAcceleration;
        public double EscapeVelocity;
        public int MaxMoonCount;
        public GEAtmosphereDensity AtmosphereDensity;
        public bool IsHaloExist;
        public GEPlanetType PlanetType;
        public double OrbitRadius;
        public double PlanetDepressionAngle;
        public double PlanetElevationAngle;
        public double Eccentricity;
        public double Temperature;
        public int HaloResourceId;
        public int StarGateCount;
        public List<MoonDesc> m_moons;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <NeedReloadPlanetRes>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <NeedReloadHaloRes>k__BackingField;
        private static DelegateBridge __Hotfix_get_NeedReloadPlanetRes;
        private static DelegateBridge __Hotfix_set_NeedReloadPlanetRes;
        private static DelegateBridge __Hotfix_get_NeedReloadHaloRes;
        private static DelegateBridge __Hotfix_set_NeedReloadHaloRes;
        private static DelegateBridge __Hotfix_IsSameHorizont;

        [MethodImpl(0x8000)]
        public override bool IsSameHorizont()
        {
        }

        public bool NeedReloadPlanetRes
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool NeedReloadHaloRes
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

