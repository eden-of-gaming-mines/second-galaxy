﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SolarSystemsLinkInfo
    {
        public string m_firstSolarSystemGuid;
        public string m_firstStarGateGuid;
        public int m_fristIslandIndex;
        public string m_secondSolarSystemGuid;
        public string m_secondStarGateGuid;
        public int m_sencondIslandIndex;
        public bool m_isWormLink;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public SolarSystemsLinkInfo(string firstSolarSystemGuid, string firstStarGateGuid, int fristIslandIndex, string secondSolarSystemGuid, string secondStarGateGuid, int sencondIslandIndex, bool isWormLink = false)
        {
        }
    }
}

