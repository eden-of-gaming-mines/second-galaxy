﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using UnityEngine;

    public class GalaxyGameObjectDescInSceneView : MonoBehaviour
    {
        public GalaxyObjectDescBase m_realtiveGalaxyObject;
        public GameObject m_realtiveGalaxyDummyObject;
        public Vector3 m_lastPosition;
    }
}

