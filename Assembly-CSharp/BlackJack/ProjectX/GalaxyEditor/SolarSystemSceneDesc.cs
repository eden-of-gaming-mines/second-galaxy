﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using System;

    public class SolarSystemSceneDesc : GalaxyObjectDescBase
    {
        public int m_sceneConfigId;
        public double PosX;
        public double PosY;
        public double PosZ;
        public float m_scale;
        public GalaxyObjectDescBase m_referenceObj;
        public float m_rotationAngleX;
        public float m_rotationAngleY;
        public float m_rotationAngleZ;
    }
}

