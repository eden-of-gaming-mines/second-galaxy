﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StargroupDesc : GalaxyObjectDescBase
    {
        public int m_starGroupType;
        public string m_OwnerStarfieldID;
        public List<SolarSystemsLinkInfo> m_linkInfoForSolarSystems;
        public List<StarGroupIslandInfo> m_islandInfoInStarGroup;
        public uint m_changedSolarSystemCnt;
        public uint m_changedSignalDiffcultCnt;
        public uint m_changedSecurityLevelCnt;
        public bool m_isHide;
        public bool m_isBackToMotherShip;
        public int m_solarsystemType;
        public int m_islandStarGroupId;
        public List<string> m_changedSolarSystemIdList;
        [HideInInspector]
        public int m_nameId;
        public List<Vector2> m_limitShapeVertexes;
        private static DelegateBridge __Hotfix_ContainsSolarSystem;
        private static DelegateBridge __Hotfix_FindIndexForSolarSystem;

        [MethodImpl(0x8000)]
        public bool ContainsSolarSystem(string solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public int FindIndexForSolarSystem(string solarSystemId, out int islandIndex)
        {
        }
    }
}

