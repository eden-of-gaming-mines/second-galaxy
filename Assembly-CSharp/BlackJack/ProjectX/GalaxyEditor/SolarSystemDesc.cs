﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class SolarSystemDesc : GalaxyObjectDescBase
    {
        public StarDesc m_starDesc;
        public int TemplateID;
        public int m_currChildObjId;
        public List<StargateDesc> m_stargateDescList;
        public List<PlanetDesc> m_planetDescs;
        public List<PlanetLikeBuildingDesc> m_planetLikeBuildingDescs;
        public List<SpaceStationDesc> m_spaceStationDescs;
        public AsteroidBeltOrbitDesc m_asteroidBeltDesc;
        public List<SolarSystemSceneDesc> m_solarSystemScenes;
        public List<GuildBuildingDesc> m_guildBuildingDescs;
        public string m_ownerStarGroupID;
        public uint m_stationChangedCnt;
        public bool m_isHide;
        public bool m_isBackToMotherShip;
        public bool m_changeBackToMotherShipOptition;
        public bool m_isCurrOccupyByNpcFollowCulture;
        public bool m_isCurrOccupyByNpc;
        public int m_solarsystemType;
        public bool m_isInfectable;
        [HideInInspector]
        public int m_nameId;
    }
}

