﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class StarGroupIslandInfo
    {
        public List<string> m_solarSystemIDList;
        public List<GameObject> m_solarSystemDummys;
        public int m_islandTypeId;
        public bool m_setGuildMinrealOutputManual;
        [Header("显示用,不允许修改")]
        public string m_guildMinrealOutputIdStr;
        [Header("显示用,不允许修改")]
        public string m_guildMinrealOutputValueStr;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

