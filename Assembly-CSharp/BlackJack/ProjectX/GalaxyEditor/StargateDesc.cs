﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class StargateDesc : GalaxyObjectDescBase
    {
        public double PosX;
        public double PosY;
        public double PosZ;
        public int TemplateID;
        public string DestSolarSystemID;
        public string ReferencePlanetID;
        public double OrbitRadius;
        public double AzimuthAngle;
        public double ElevationAngle;
        public float RotationAngleX;
        public float RotationAngleY;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <NeedReloadSpaceStationRes>k__BackingField;
        [HideInInspector]
        public bool isWormholeLink;
        private SolarSystemDesc m_ownerSolarSystem;
        private static DelegateBridge __Hotfix_get_NeedReloadSpaceStationRes;
        private static DelegateBridge __Hotfix_set_NeedReloadSpaceStationRes;
        private static DelegateBridge __Hotfix_get_OwnerSolarSystem;

        public bool NeedReloadSpaceStationRes
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public SolarSystemDesc OwnerSolarSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

