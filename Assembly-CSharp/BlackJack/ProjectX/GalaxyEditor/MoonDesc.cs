﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class MoonDesc : GalaxyObjectDescBase
    {
        public double OrbitRadius;
        public double PosX;
        public double PosZ;
        public double RevolutionPeriod;
        public double Mass;
        public double GravitationaAcceleration;
        public double EscapeVelocity;
        public double Temperature;
        public double MoonPositionAngle;
        public double Eccentricity;
        public double Radius;
        public double DistanceToPlanetGround;
        public int ResourceId;
        private PlanetDesc m_ownerPlanet;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <NeedReloadMoonRes>k__BackingField;
        private static DelegateBridge __Hotfix_get_OwnerPlanet;
        private static DelegateBridge __Hotfix_get_NeedReloadMoonRes;
        private static DelegateBridge __Hotfix_set_NeedReloadMoonRes;

        public PlanetDesc OwnerPlanet
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool NeedReloadMoonRes
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

