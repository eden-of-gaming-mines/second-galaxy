﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using System;
    using System.Collections.Generic;

    public class AsteroidBeltOrbitDesc : GalaxyObjectDescBase
    {
        public List<AsteroidBeltOrbitPosition> AsteroidGatherPointPositionList;
        public double OrbitRadius;
        public double MinOrbitRadius;
        public double MaxOrbitRadius;
        public double[] AsteroidGatherPointAngles;
    }
}

