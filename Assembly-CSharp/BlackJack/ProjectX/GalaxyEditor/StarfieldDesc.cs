﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class StarfieldDesc : GalaxyObjectDescBase
    {
        public List<string> m_stargroupIDList;
        public string m_OwnerGalaxyID;
        public Color m_color;
        public List<GameObject> m_stargroupDummys;
        public List<StargroupsLinkInfo> m_linkInfoForStargroups;
        public uint m_changedStargroupCnt;
        public uint m_changedSignalDiffcultStargroupCnt;
        public uint m_changedSecurityLevelStargroupCnt;
        public bool m_isHide;
        public bool m_isBackToMotherShip;
        public int m_solarsystemType;
    }
}

