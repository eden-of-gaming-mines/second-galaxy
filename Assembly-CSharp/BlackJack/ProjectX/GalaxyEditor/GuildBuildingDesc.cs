﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using BlackJack.ConfigData;
    using System;

    [Serializable]
    public class GuildBuildingDesc : GalaxyObjectDescBase
    {
        public double m_posX;
        public double m_posY;
        public double m_posZ;
        public GuildBuildingType m_guildBuildingType;
        public string m_planetId;
        public bool m_isInPlanetOrbit;
        public float m_offsetDepressionAngele;
        public double m_orbitRadius;
        public float m_elevationAngle;
        public SolarSystemDesc m_ownerSolarSystem;
    }
}

