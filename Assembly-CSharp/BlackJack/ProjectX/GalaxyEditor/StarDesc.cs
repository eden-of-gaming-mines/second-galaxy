﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class StarDesc : GalaxyObjectDescBase
    {
        public int StarAge;
        public int SpectrumID;
        public int SubSpectrumID;
        public double Brightness;
        public double Radius;
        public double Temperature;
        public double Mass;
        public int ResourceId;
        public int TemplateID;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <NeedReloadStarRes>k__BackingField;
        private SolarSystemDesc m_ownerSolarSystem;
        private static DelegateBridge __Hotfix_get_NeedReloadStarRes;
        private static DelegateBridge __Hotfix_set_NeedReloadStarRes;
        private static DelegateBridge __Hotfix_get_OwnerSolarSystem;

        public bool NeedReloadStarRes
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public SolarSystemDesc OwnerSolarSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

