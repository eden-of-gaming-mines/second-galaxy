﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class DummyDesc : MonoBehaviour
    {
        public string m_guid;
        public int grandFactionId;
        public int factionId;
        public List<int> npcMonsterCollectionIdlist;
        public float pirateSignalWeight;
        public List<int> mineralIdList;
        public float mineralSignalWeight;
        public float signalDifficult;
        public bool changeSignalDifficultManual;
        public float securityLevel;
        public bool changeSecurityLevelManual;
        public bool m_isMarkedAsFactionNpc;
        public float ssTemplateDetermine;
        public float delegateTransportWeight;
        [SerializeField]
        public bool isKeepGreyScaleValue;
        public bool EnablePlayerSovereignty;
        [SerializeField]
        public bool PlayerSovereigntyManualChanged;
        public List<int> m_guildMinrealList;
        public int m_guildMinrealOutputId;
        [Header("显示用,不允许修改")]
        public string m_guildMinrealOutputIdStr;
        [Header("显示用,不允许修改")]
        public string m_guildMinrealOutputValueStr;
    }
}

