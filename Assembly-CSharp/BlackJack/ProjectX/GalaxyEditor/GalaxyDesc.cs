﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using System;
    using System.Collections.Generic;

    public class GalaxyDesc : GalaxyObjectDescBase
    {
        public List<GameObject> m_starfieldDummys;
        public List<string> m_starfieldIDList;
        public List<StarfieldsLinkInfo> m_linkInfoListForStarfields;
        public float m_galaxyRadius;
        public int m_colorBlockTempleteID;
        public float m_colorBlockScale;
        public float m_colorBlockRotation;
        public float m_colorBlockLocationX;
        public float m_colorBlockLocationZ;
        public float m_colorBlockAlpha;
        public float m_colorBlockAlphaForView;
    }
}

