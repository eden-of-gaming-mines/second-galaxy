﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StarfieldsLinkInfo
    {
        public string m_firstStarfieldGuid;
        public string m_firstStargroupGuid;
        public string m_firstSolarSystemGuid;
        public string m_firstStarGateGuid;
        public string m_secondStarfieldGuid;
        public string m_secondStargroupGuid;
        public string m_secondSolarSystemGuid;
        public string m_secondStarGateGuid;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public StarfieldsLinkInfo(string firstStarfieldGuid, string firstStargroupGuid, string firstSolarSystemGuid, string firstStarGateGuid, string secondStarfieldGuid, string secondStargroupGuid, string secondSolarSystemGuid, string secondStarGateGuid)
        {
        }
    }
}

