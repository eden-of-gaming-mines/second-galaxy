﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class PlanetLikeBuildingDesc : PlanetBaseDesc
    {
        public float RotationAngleY;
        public double OrbitRadius;
        public double PlanetDepressionAngle;
        public double PlanetElevationAngle;
        public SpaceStationDesc m_portalStation;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <NeedReloadPlanetRes>k__BackingField;
        private static DelegateBridge __Hotfix_get_NeedReloadPlanetRes;
        private static DelegateBridge __Hotfix_set_NeedReloadPlanetRes;
        private static DelegateBridge __Hotfix_IsSameHorizont;

        [MethodImpl(0x8000)]
        public override bool IsSameHorizont()
        {
        }

        public bool NeedReloadPlanetRes
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

