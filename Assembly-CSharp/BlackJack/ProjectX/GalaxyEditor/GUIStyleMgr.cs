﻿namespace BlackJack.ProjectX.GalaxyEditor
{
    using UnityEngine;

    public class GUIStyleMgr : MonoBehaviour
    {
        public GUIStyle FoldOutStyle;
        public GUIStyle SelectStyle;
        public GUIStyle DisabledStyle;
        public GUIStyle IndentBlockStyle;
    }
}

