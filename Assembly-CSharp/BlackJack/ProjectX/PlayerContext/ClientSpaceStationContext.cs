﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ClientSpaceStationContext
    {
        private int m_spaceStationId;
        private ConfigDataSpaceStationResInfo m_resInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SpaceStationID;
        private static DelegateBridge __Hotfix_set_SpaceStationID;
        private static DelegateBridge __Hotfix_get_ResInfo;
        private static DelegateBridge __Hotfix_set_ResInfo;

        public int SpaceStationID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public ConfigDataSpaceStationResInfo ResInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

