﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ClientCommonLog : ICommonLog
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_DebugLog_0;
        private static DelegateBridge __Hotfix_ErrorLog_0;
        private static DelegateBridge __Hotfix_DebugLog_1;
        private static DelegateBridge __Hotfix_ErrorLog_1;
        private static DelegateBridge __Hotfix_ScriptDebugLog;
        private static DelegateBridge __Hotfix_WarnLog;

        [MethodImpl(0x8000)]
        public void DebugLog(string format, params object[] args)
        {
        }

        [MethodImpl(0x8000)]
        public void DebugLog(bool needInRelease, string format, params object[] args)
        {
        }

        [MethodImpl(0x8000)]
        public void ErrorLog(string format, params object[] args)
        {
        }

        [MethodImpl(0x8000)]
        public void ErrorLog(bool needInRelease, string format, params object[] args)
        {
        }

        [MethodImpl(0x8000)]
        public void ScriptDebugLog(string format, params object[] args)
        {
        }

        [MethodImpl(0x8000)]
        public void WarnLog(bool needInRelease, string format, params object[] args)
        {
        }
    }
}

