﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    internal class DFRDataFilterCurrencyLog : IDFRDataFilter
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <Filter>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private long <GuildId>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetStringKey;
        private static DelegateBridge __Hotfix_GetFilterAsLogTypes;
        private static DelegateBridge __Hotfix_get_Filter;
        private static DelegateBridge __Hotfix_set_Filter;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;

        [MethodImpl(0x8000)]
        public DFRDataFilterCurrencyLog(long guildId, int filterFlag)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildCurrencyLogType> GetFilterAsLogTypes()
        {
        }

        [MethodImpl(0x8000)]
        public string GetStringKey()
        {
        }

        public int Filter
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public long GuildId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

