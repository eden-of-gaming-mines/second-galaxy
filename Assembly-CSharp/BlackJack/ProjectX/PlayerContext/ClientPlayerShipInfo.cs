﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ClientPlayerShipInfo
    {
        private int m_configId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ConfigDataSpaceShipInfo <ConfInfo>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ConfigDataSpaceShip3DInfo <Conf3dInfo>k__BackingField;
        private ulong m_instanceId;
        private string m_name;
        private int m_solarSystemId;
        private int m_spaceStationId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_get_ConfInfo;
        private static DelegateBridge __Hotfix_set_ConfInfo;
        private static DelegateBridge __Hotfix_get_Conf3dInfo;
        private static DelegateBridge __Hotfix_set_Conf3dInfo;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_get_Name;

        [MethodImpl(0x8000)]
        public ClientPlayerShipInfo(ProtoTypeShipListAck.ProShipInstanceData proData)
        {
        }

        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataSpaceShipInfo ConfInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public ConfigDataSpaceShip3DInfo Conf3dInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

