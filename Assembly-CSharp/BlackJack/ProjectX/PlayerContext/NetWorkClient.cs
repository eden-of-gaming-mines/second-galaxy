﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.BJFramework.Runtime.PlayerContext;
    using BlackJack.ProjectX.LibClient;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class NetWorkClient : IPlayerContextNetworkClient
    {
        private IClient m_client;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_LoginByAuthToken;
        private static DelegateBridge __Hotfix_LoginBySessionToken;
        private static DelegateBridge __Hotfix_Disconnect;
        private static DelegateBridge __Hotfix_SendMessage;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_BlockProcessMsg;
        private static DelegateBridge __Hotfix_RegConnectionLogEvent;

        [MethodImpl(0x8000)]
        public NetWorkClient(IClient realClient)
        {
        }

        [MethodImpl(0x8000)]
        public void BlockProcessMsg(bool isBlock)
        {
        }

        [MethodImpl(0x8000)]
        public void Close()
        {
        }

        [MethodImpl(0x8000)]
        public bool Disconnect()
        {
        }

        [MethodImpl(0x8000)]
        public bool LoginByAuthToken(string serverAddress, int serverPort, string authToken, string clientVersion, string clientDeviceId, string localization, int loginChannelId, int bornChannelId, string serverDomain)
        {
        }

        [MethodImpl(0x8000)]
        public bool LoginBySessionToken(string sessionToken, string clientVersion, string localization, int loginChannelId, int bornChannelId)
        {
        }

        [MethodImpl(0x8000)]
        public void RegConnectionLogEvent(Action<string> logAction)
        {
        }

        [MethodImpl(0x8000)]
        public bool SendMessage(object msg)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }
    }
}

