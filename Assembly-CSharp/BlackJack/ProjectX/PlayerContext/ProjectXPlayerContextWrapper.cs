﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.BJFramework.Runtime.PlayerContext;
    using BlackJack.ProjectX.LibClient;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ProjectXPlayerContextWrapper : IClientEventHandler
    {
        private IPlayerContextNetworkEventHandler m_playerContext;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnConnected;
        private static DelegateBridge __Hotfix_OnDisconnected;
        private static DelegateBridge __Hotfix_OnError;
        private static DelegateBridge __Hotfix_OnLoginByAuthTokenAck;
        private static DelegateBridge __Hotfix_OnLoginBySessionTokenAck;
        private static DelegateBridge __Hotfix_OnMessage;

        [MethodImpl(0x8000)]
        public ProjectXPlayerContextWrapper(IPlayerContextNetworkEventHandler playerContext)
        {
        }

        [MethodImpl(0x8000)]
        public void OnConnected()
        {
        }

        [MethodImpl(0x8000)]
        public void OnDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        public void OnError(int err, string excepionInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLoginBySessionTokenAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        public void OnMessage(object msg, int msgId)
        {
        }
    }
}

