﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;

    public class RechargeGoodsInfo
    {
        public string m_id;
        public string m_registerId;
        public string m_name;
        public double m_price;
        public RechargeGoodsType m_type;
        public int m_sdkType;
        public string m_desc;
        public double m_displayPrice;
        public string m_displayCurrency;
        public int m_configId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

