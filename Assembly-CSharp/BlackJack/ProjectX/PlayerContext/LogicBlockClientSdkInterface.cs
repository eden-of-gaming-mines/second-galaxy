﻿namespace BlackJack.ProjectX.PlayerContext
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockClientSdkInterface
    {
        private readonly List<RechargeGoodsInfo> m_rechargeGoodsInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_RequestSdkRechargeGoodsInfoList;
        private static DelegateBridge __Hotfix_GetSdkRechargeGoodsInfoList;
        private static DelegateBridge __Hotfix_GetRechargeGoodsInfo_1;
        private static DelegateBridge __Hotfix_GetRechargeGoodsInfo_0;
        private static DelegateBridge __Hotfix_GetGoodsTypeByGoodsId;
        private static DelegateBridge __Hotfix_CheckPdSdkState;

        [MethodImpl(0x8000)]
        private bool CheckPdSdkState()
        {
        }

        [MethodImpl(0x8000)]
        public void GetGoodsTypeByGoodsId(string goodsIdStr, out RechargeGoodsType goodsType, out int goodId)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeGoodsInfo GetRechargeGoodsInfo(Predicate<RechargeGoodsInfo> filter)
        {
        }

        [MethodImpl(0x8000)]
        public RechargeGoodsInfo GetRechargeGoodsInfo(RechargeGoodsType goodsType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeGoodsInfo> GetSdkRechargeGoodsInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void RequestSdkRechargeGoodsInfoList(Action<bool> onEnd)
        {
        }

        [CompilerGenerated]
        private sealed class <GetRechargeGoodsInfo>c__AnonStorey1
        {
            internal int configId;
            internal RechargeGoodsType goodsType;

            internal bool <>m__0(RechargeGoodsInfo item) => 
                ((item.m_configId == this.configId) && (item.m_type == this.goodsType));
        }

        [CompilerGenerated]
        private sealed class <RequestSdkRechargeGoodsInfoList>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal LogicBlockClientSdkInterface $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool isSuccess)
            {
            }
        }
    }
}

