﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct ShipStoreItemUpdateInfo
    {
        public ShipStoreItemInfo m_shipItem;
        public long m_changeValue;
        public ShipStoreItemInfo m_shipFreezingItem;
        public long m_freezingItemChangeValue;
        public ShipStoreItemInfo m_shipBindItem;
        public long m_bindItemChangeValue;
        [MethodImpl(0x8000)]
        public ShipStoreItemUpdateInfo(ShipStoreItemInfo itemInfo, long changeValue)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEmptyInfo()
        {
        }
    }
}

