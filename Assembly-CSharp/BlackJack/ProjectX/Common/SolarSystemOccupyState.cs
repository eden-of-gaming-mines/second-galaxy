﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum SolarSystemOccupyState
    {
        OccupyByNpcRejectPlayer = 1,
        OccupyByNpcAcceptPlayer = 2,
        OccupybyNoOne = 3,
        OccupybyPlayer = 4
    }
}

