﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class SceneInteractionInfo
    {
        public SceneInteractionType m_type;
        public uint m_optTime;
        public uint m_range;
        public int m_npcTalkerId;
        public int m_message;
        public SceneEventInfo m_trigerSceneEvent;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

