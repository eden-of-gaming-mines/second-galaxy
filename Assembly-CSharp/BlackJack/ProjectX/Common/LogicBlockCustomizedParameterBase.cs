﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockCustomizedParameterBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected ICustomizedParameterDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetIntValue;
        private static DelegateBridge __Hotfix_GetStringValue;
        private static DelegateBridge __Hotfix_GetBoolValue;
        private static DelegateBridge __Hotfix_PutKeyValue;
        private static DelegateBridge __Hotfix_GetCustomizedParameterList;

        [MethodImpl(0x8000)]
        public bool GetBoolValue(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public List<CustomizedParameterInfo> GetCustomizedParameterList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetIntValue(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetStringValue(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void PutKeyValue(CustomizedParameter parameterId, string value)
        {
        }
    }
}

