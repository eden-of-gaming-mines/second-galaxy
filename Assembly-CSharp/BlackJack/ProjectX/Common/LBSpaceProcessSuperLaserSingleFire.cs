﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessSuperLaserSingleFire : LBSpaceProcessLaserSingleFire
    {
        protected List<ILBSpaceTarget> m_extraHitTargetList;
        protected List<LBBulletDamageInfo> m_damageInfoForExtraTargets;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetExtraBulletHitTarget;
        private static DelegateBridge __Hotfix_GetExtraBulletHitTarget;
        private static DelegateBridge __Hotfix_GetDamageInfoForExtraTargets;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperLaserSingleFire()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperLaserSingleFire(ushort bulletIndex, uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBulletDamageInfo> GetDamageInfoForExtraTargets()
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> GetExtraBulletHitTarget()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(ushort bulletIndex, uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExtraBulletHitTarget(List<ILBSpaceTarget> targetList, List<LBBulletDamageInfo> damageInfoForExtraTargets)
        {
        }
    }
}

