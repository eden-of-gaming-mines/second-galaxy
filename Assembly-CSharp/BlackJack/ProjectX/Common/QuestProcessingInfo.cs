﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class QuestProcessingInfo
    {
        public int m_questId;
        public int m_level;
        public int m_instanceId;
        public int m_questFactionId;
        public DateTime m_acceptTime;
        public bool m_completedWaitConfirm;
        public List<int> m_completeCondParamList;
        public bool m_isFail;
        public int m_sceneSolarSystemId;
        public Vector3D m_sceneLocation;
        public float m_rewardBonusMulti;
        public QuestSrcType m_questSrcType;
        public int m_currQuestPoolCount;
        public float m_guildRewardBonusMulti;
        public uint m_sceneInstanceId;
        public int m_sceneId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

