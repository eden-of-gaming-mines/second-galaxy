﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildSolarSystemCtxDataContainer
    {
        void AddOccupiedSolarSystem(GuildSolarSystemInfo info);
        List<GuildSolarSystemInfo> GetGuildSolarSystemInfoList();
        void RemoveOccupiedSolarSystem(int solarSystemId);
        void RemoveSolarSystemBuilding(int solarSystemId, ulong instanceId);
        void UpdateSolarSystemBuilding(GuildBuildingInfo info);
        void UpdateSolarSystemFlourishValue(int solarSystemId, int value);
    }
}

