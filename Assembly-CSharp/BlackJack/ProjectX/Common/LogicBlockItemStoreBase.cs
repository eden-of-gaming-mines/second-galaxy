﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockItemStoreBase
    {
        protected ILBPlayerContext m_playerContext;
        protected IItemStoreDataContainer m_itemStoreDC;
        protected Func<ulong> m_instanceIdAllocator;
        protected ulong m_itemStoreCurrUsedSize;
        protected bool m_itemStoreCurrUsedSizeDirty;
        protected List<LBStoreItem> m_itemList;
        [CompilerGenerated]
        private static Predicate<LBStoreItem> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetStoreSpaceMax;
        private static DelegateBridge __Hotfix_GetStoreSpaceUsed;
        private static DelegateBridge __Hotfix_HasSpace_1;
        private static DelegateBridge __Hotfix_HasSpace_0;
        private static DelegateBridge __Hotfix_GetStoreSpaceFree;
        private static DelegateBridge __Hotfix_GetItemByInstanceId;
        private static DelegateBridge __Hotfix_GetItemByIndex;
        private static DelegateBridge __Hotfix_GetMSStoreItemById;
        private static DelegateBridge __Hotfix_GetMSStoreItemCountById;
        private static DelegateBridge __Hotfix_GetMSStoreItemListByItemType;
        private static DelegateBridge __Hotfix_GetMSStoreItemListByNormalItemType;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_LogAllItemStoreItems;
        private static DelegateBridge __Hotfix_ForeachItems;
        private static DelegateBridge __Hotfix_RemoveMSStoreItem;
        private static DelegateBridge __Hotfix_RemoveMSStoreItemByIndex;
        private static DelegateBridge __Hotfix_CheckAddItem_1;
        private static DelegateBridge __Hotfix_CheckAddItem_0;
        private static DelegateBridge __Hotfix_CheckRemoveMSStoreItem;
        private static DelegateBridge __Hotfix_CheckUseMSStoreItemByIndex;
        private static DelegateBridge __Hotfix_CheckSuspendItem;
        private static DelegateBridge __Hotfix_CheckResumeItem;
        private static DelegateBridge __Hotfix_AddNewItemDirectly;
        private static DelegateBridge __Hotfix_UpdateItemDirectly;
        private static DelegateBridge __Hotfix_RemoveItemDirectly;
        private static DelegateBridge __Hotfix_FindFreeItemOrCreateEmpty;
        private static DelegateBridge __Hotfix_CreateEmptyItemForIndex;
        private static DelegateBridge __Hotfix_FireEventItemChange;
        private static DelegateBridge __Hotfix_UpdateItemFreezingTime;
        private static DelegateBridge __Hotfix_FreezingItemTimeOutConfirm;
        private static DelegateBridge __Hotfix_FreezingItemTimeoutImpl;
        private static DelegateBridge __Hotfix_MergeFreezingItemInMsStore;
        private static DelegateBridge __Hotfix_GetItemFreezingTime;
        private static DelegateBridge __Hotfix_UpdateStoreItemFlag;
        private static DelegateBridge __Hotfix_InitStoreItemList;
        private static DelegateBridge __Hotfix_CreateLBStoreItemByItemInfo;
        private static DelegateBridge __Hotfix_CalcTotalSizeForItem;
        private static DelegateBridge __Hotfix_GenerateStoreItemSuspendFlag;
        private static DelegateBridge __Hotfix_CalcItemStoreCurrUsedSize;

        [MethodImpl(0x8000)]
        public virtual LBStoreItem AddNewItemDirectly(StoreItemType itemType, int itemConfigId, long count, bool isBind = true, ItemSuspendReason suspendReason = 0, int index = -1, ulong instanceId = 0UL, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcItemStoreCurrUsedSize()
        {
        }

        [MethodImpl(0x8000)]
        protected ulong CalcTotalSizeForItem(int configId, StoreItemType itemType, long count)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckAddItem(List<ItemInfo> items, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckAddItem(StoreItemType itemType, int itemConfigId, long count, bool needCheckStoreSpace = true)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckRemoveMSStoreItem(int index, long count)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckResumeItem(LBStoreItem from, long count, bool needCheckStoreSpace = true)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckSuspendItem(LBStoreItem from, long count, ItemSuspendReason reason, LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckUseMSStoreItemByIndex(int index, long count, out LBStoreItem item, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem CreateEmptyItemForIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStoreItem CreateLBStoreItemByItemInfo(StoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem FindFreeItemOrCreateEmpty()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void FireEventItemChange(ItemInfo destItem, long changeCount, bool isFreezing = false, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachItems(Func<LBStoreItem, bool> fun)
        {
        }

        [MethodImpl(0x8000)]
        public bool FreezingItemTimeOutConfirm(int itemType, int itemId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public void FreezingItemTimeoutImpl(int itemId, StoreItemType storeItemType)
        {
        }

        [MethodImpl(0x8000)]
        protected uint GenerateStoreItemSuspendFlag(ItemSuspendReason reason)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemCount()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetItemFreezingTime(StoreItemType storeItemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetMSStoreItemById(StoreItemType itemType, int itemConfigId, bool isBind, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        public long GetMSStoreItemCountById(StoreItemType itemType, int itemConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> GetMSStoreItemListByItemType(StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> GetMSStoreItemListByNormalItemType(NormalItemType normalItemType)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetStoreSpaceFree()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetStoreSpaceMax()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetStoreSpaceUsed()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasSpace(float size)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasSpace(ulong size)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public void LogAllItemStoreItems()
        {
        }

        [MethodImpl(0x8000)]
        private void MergeFreezingItemInMsStore(StoreItemType storeItemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void RemoveItemDirectly(LBStoreItem item, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool RemoveMSStoreItem(StoreItemType itemType, int itemId, long itemCount, out int errCode, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool RemoveMSStoreItemByIndex(int index, long count, out int errCode, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateItemDirectly(LBStoreItem item, long count, long changeCount = 0L, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemFreezingTime(StoreItemType itemType, int itemId, DateTime itemFreezingTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStoreItemFlag(int index, ItemSuspendReason flag)
        {
        }
    }
}

