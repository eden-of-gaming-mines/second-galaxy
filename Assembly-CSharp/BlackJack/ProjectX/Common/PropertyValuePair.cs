﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct PropertyValuePair
    {
        public string PropertyName;
        public float Value;
        public PropertyValuePair(string name, float val)
        {
            this.PropertyName = name;
            this.Value = val;
        }
    }
}

