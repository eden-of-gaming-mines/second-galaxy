﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class Formula
    {
        private const double DelegateFightTimeMultiMin = 0.5;
        private const double DelegateFightTimeMultiMax = 5.0;
        private static DelegateBridge __Hotfix_CalcCommonFormulaForAddAndMulti_0;
        private static DelegateBridge __Hotfix_CalcCommonFormulaForAddAndMulti_1;
        private static DelegateBridge __Hotfix_CalcCommonFormulaForMultiAndAdd_0;
        private static DelegateBridge __Hotfix_CalcCommonFormulaForMultiAndAdd_1;
        private static DelegateBridge __Hotfix_CalcShipShieldOrArmorFinal;
        private static DelegateBridge __Hotfix_CalcCharactorPropertyGain;
        private static DelegateBridge __Hotfix_CalcWeaponCD4NextGroup;
        private static DelegateBridge __Hotfix_CalcNerfParam;
        private static DelegateBridge __Hotfix_CalcVolumnRatioFinal;
        private static DelegateBridge __Hotfix_CalcVolumnFinal;
        private static DelegateBridge __Hotfix_CalcVolumnFinalWithoutSpeed;
        private static DelegateBridge __Hotfix_CalcVolumnSpeedEffectFactor;
        private static DelegateBridge __Hotfix_AddAndMulti;
        private static DelegateBridge __Hotfix_MultiAndAdd;
        private static DelegateBridge __Hotfix_CalcElementalDamageFinal;
        private static DelegateBridge __Hotfix_CalcReceivedDamageFinalWithTarget;
        private static DelegateBridge __Hotfix_CalcSourceDamageFinal;
        private static DelegateBridge __Hotfix_CalcSourceDamageFinalNoCache;
        private static DelegateBridge __Hotfix_CalcSourceDamageFinalFrameCache;
        private static DelegateBridge __Hotfix_CalcSourceDamageBaseValue;
        private static DelegateBridge __Hotfix_CalcElementalRealDamageFinalWithTarget;
        private static DelegateBridge __Hotfix_CalcSourceRealDamageFinal;
        private static DelegateBridge __Hotfix_CalcSourceDamageIncludeTargetShieldRatioDmgMulti;
        private static DelegateBridge __Hotfix_CalcFireCtrlAccuracyFinal;
        private static DelegateBridge __Hotfix_CalcCriticalFinalWithTarget;
        private static DelegateBridge __Hotfix_CalcCriticalFinal;
        private static DelegateBridge __Hotfix_CalcCriticalDamageRatioFinalWithTarget;
        private static DelegateBridge __Hotfix_CalcCriticalDamageRatioFinal;
        private static DelegateBridge __Hotfix_CalcDamageResistFinal;
        private static DelegateBridge __Hotfix_CalcDamageResistFinalFramCache;
        private static DelegateBridge __Hotfix_CalcRealDamageReduceFinal;
        private static DelegateBridge __Hotfix_CalcDelegateFightTimeMulti;
        private static DelegateBridge __Hotfix_CalcDelegateFightNeedRepairPercent;
        private static DelegateBridge __Hotfix_CalcDelegateMineralTime;
        private static DelegateBridge __Hotfix_CalcDelegateTransportTime;
        private static DelegateBridge __Hotfix_CalcECMEquipStrengthFinal;
        private static DelegateBridge __Hotfix_CalcShipMaxSpeedFinal;
        private static DelegateBridge __Hotfix_CalcShipMassFinal;
        private static DelegateBridge __Hotfix_CalcShipMaxSpeedNoBoosterFinal;
        private static DelegateBridge __Hotfix_CalcShipJumpMaxSpeedFinal;
        private static DelegateBridge __Hotfix_CalcShipJumpSteadyFinal;
        private static DelegateBridge __Hotfix_CalcuSuperWeaponBurstTriggerIntervalFinal;
        private static DelegateBridge __Hotfix_CalcuSuperWeaponChargeSpeedFinal;
        private static DelegateBridge __Hotfix_CalcRailgunPlasmaCannonHitWithTarget;
        private static DelegateBridge __Hotfix_CalcRailgunPlasmaHitFinal;
        private static DelegateBridge __Hotfix_CalcDroneHitFinalWithTarget;
        private static DelegateBridge __Hotfix_CalcDroneHitFinal;
        private static DelegateBridge __Hotfix_CalcMissileHitFinal;
        private static DelegateBridge __Hotfix_CalcOptimalFireRangeFinal;
        private static DelegateBridge __Hotfix_CalcMaxFireRangeFinal;
        private static DelegateBridge __Hotfix_CalcMissileFlySpeedFinal;
        private static DelegateBridge __Hotfix_CalcDroneFightDurationFinal;
        private static DelegateBridge __Hotfix_CalcDroneTransverseVelocityFinal;
        private static DelegateBridge __Hotfix_CalcLaserTransverseVelocityFinal;
        private static DelegateBridge __Hotfix_CalcRailgunTransverseVelocityFinal;
        private static DelegateBridge __Hotfix_CalcPlasmaTransverseVelocityFinal;
        private static DelegateBridge __Hotfix_CalcMissileTransverseVelocityFinal;
        private static DelegateBridge __Hotfix_CalcNormalAttackTransverseVelocityFinal;

        [MethodImpl(0x8000)]
        private static float AddAndMulti(float baseValue, float addFactor, float multiFactor)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCharactorPropertyGain(float charactorProperty, float gainMultiValue)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCommonFormulaForAddAndMulti(float baseValue, float addEnhanceFactor, float multiEnhanceFactor, float nerfFactor, bool hasPunish)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCommonFormulaForAddAndMulti(float baseValue, float addESFactor, float addEDFactor, float multiESFactor, float multiEDFactor, float NSFactor, float NDFactor, bool hasPunish = true)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCommonFormulaForMultiAndAdd(float baseValue, float addEnhanceFactor, float multiEnhanceFactor, float nerfFactor, bool hasPunish)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCommonFormulaForMultiAndAdd(float baseValue, float addESFactor, float addEDFactor, float multiESFactor, float multiEDFactor, float NSFactor, float NDFactor, bool hasPunish = true)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCriticalDamageRatioFinal(float criticalDamageRatioBaseValue, float criticalDamageRatioAdd_ES, float criticalDamageRatioAdd_ED, float criticalDamageRatioMulti_ES, float criticalDamageRatioMulti_ED, float criticalDamageRatio_NS, float criticalDamageRatio_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCriticalDamageRatioFinalWithTarget(float sourceCriticalDamageRatioFinal, float beCriticalDamageRatioMulti_ED)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCriticalFinal(float criticalBaseValue, float criticalAdd_ES, float criticalAdd_ED, float criticalMulti_ES, float criticalMulti_ED, float critical_NS, float critical_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcCriticalFinalWithTarget(float sourceCriticalFinal, float beCriticalMulti_ED)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDamageResistFinal(float damageResistBase, float damageResist_ES, float damageResist_ED, float damageResist_NS, float damageResist_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDamageResistFinalFramCache(float damageResistFinal, float damageResistFinalModifyFrameCache)
        {
        }

        [MethodImpl(0x8000)]
        public static double CalcDelegateFightNeedRepairPercent(float survivalNeedValue, float survivalCapabilityValue, int constRepairTimeMult, float delegateFightSpeedMulti_ES)
        {
        }

        [MethodImpl(0x8000)]
        public static double CalcDelegateFightTimeMulti(float damageNeedValue, float damageCapabilityValue, int constFightTimeMult, float delegateFightSpeedMulti_ES)
        {
        }

        [MethodImpl(0x8000)]
        public static double CalcDelegateMineralTime(int mineralCount, float mineralPackedSize, double miningEfficiency, double itemStoreSpace)
        {
        }

        [MethodImpl(0x8000)]
        public static double CalcDelegateTransportTime(int goodsCount, int jumpDistanceCount, double itemStoreSpace, int oneJumpTime, float delegateTransportSpeedMulti_ES)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDroneFightDurationFinal(float droneFightDurationBaseValue, float droneFightDurationModify, float droneFightDurationAdd_ES, float droneFightDurationAdd_ED, float droneFightDurationMulti_ES, float droneFightDurationMulti_ED, float droneFightDuration_NS, float droneFightDuration_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDroneHitFinal(float hitBaseValue, float hitMulti_ES, float hitMulti_ED, float hit_NS, float hit_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDroneHitFinalWithTarget(float sourceHitFinal, float volumnRatioFinal)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDroneTransverseVelocityFinal(float droneTransverseVelocityBaseValue, float droneTransverseVelocityAdd_ES, float droneTransverseVelocityAdd_ED, float droneTransverseVelocityMulti_ES, float droneTransverseVelocityMulti_ED, float droneTransverseVelocity_NS, float droneTransverseVelocity_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcECMEquipStrengthFinal(float strengthBaseValue, float strengthAdd_ES, float strengthAdd_ED, float strengthMulti_ES, float strengthMulti_ED, float strength_NS, float strength_ND, float charElectricGainFinal)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcElementalDamageFinal(float sourceDamageFinal, float ammoElementalDamagePercent)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcElementalRealDamageFinalWithTarget(float sourceRealDamageFinal, float realDamageReduceFinal)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcFireCtrlAccuracyFinal(float fireCtrlAccuracyBaseValue, float fireCtrlAccuracyAdd_ES, float fireCtrlAccuracyAdd_ED, float fireCtrlAccuracyMulti_ES, float fireCtrlAccuracyMulti_ED, float fireCtrlAccuracy_NS, float fireCtrlAccuracy_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcLaserTransverseVelocityFinal(float LaserTransverseVelocityBaseValue, float LaserTransverseVelocityAdd_ES, float LaserTransverseVelocityAdd_ED, float LaserTransverseVelocityMulti_ES, float LaserTransverseVelocityMulti_ED, float LaserTransverseVelocity_NS, float LaserTransverseVelocity_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcMaxFireRangeFinal(float maxFireRangeBaseValue, float ammoMaxFireRangeModify, float maxFireRangeAdd_ES, float maxFireRangeAdd_ED, float maxFireRangeMulti_ES, float maxFireRangeMulti_ED, float maxFireRange_NS, float maxFireRange_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcMissileFlySpeedFinal(float missileFlySpeedBaseValue, float missileFlySpeedAdd_ES, float missileFlySpeedAdd_ED, float missileFlySpeedMulti_ES, float missileFlySpeedMulti_ED, float missileFlySpeed_NS, float missileFlySpeed_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcMissileHitFinal(float hitBaseValue, float hitMulti_ES, float hitMulti_ED, float hit_NS, float hit_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcMissileTransverseVelocityFinal(float MissileTransverseVelocityBaseValue, float MissileTransverseVelocityAdd_ES, float MissileTransverseVelocityAdd_ED, float MissileTransverseVelocityMulti_ES, float MissileTransverseVelocityMulti_ED, float MissileTransverseVelocity_NS, float MissileTransverseVelocity_ND)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcNerfParam(float nerfFactor, bool hasPunish = true)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcNormalAttackTransverseVelocityFinal(float NormalAttackTransverseVelocityBaseValue, float NormalAttackTransverseVelocityAdd_ES, float NormalAttackTransverseVelocityAdd_ED, float NormalAttackTransverseVelocityMulti_ES, float NormalAttackTransverseVelocityMulti_ED, float NormalAttackTransverseVelocity_NS, float NormalAttackTransverseVelocity_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcOptimalFireRangeFinal(float optimalFireRangeBaseValue, float ammoOptimalFireRangeModify, float optimalFireRangeAdd_ES, float optimalFireRangeAdd_ED, float optimalFireRangeMulti_ES, float optimalFireRangeMulti_ED, float optimalFireRange_NS, float optimalFireRange_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcPlasmaTransverseVelocityFinal(float PlasmaTransverseVelocityBaseValue, float PlasmaTransverseVelocityAdd_ES, float PlasmaTransverseVelocityAdd_ED, float PlasmaTransverseVelocityMulti_ES, float PlasmaTransverseVelocityMulti_ED, float PlasmaTransverseVelocity_NS, float PlasmaTransverseVelocity_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcRailgunPlasmaCannonHitWithTarget(float sourceHitFinal, float volumnRatioFinal)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcRailgunPlasmaHitFinal(float hitBaseValue, float hitMulti_ES, float hitMulti_ED, float hit_NS, float hit_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcRailgunTransverseVelocityFinal(float RailgunTransverseVelocityBaseValue, float RailgunTransverseVelocityAdd_ES, float RailgunTransverseVelocityAdd_ED, float RailgunTransverseVelocityMulti_ES, float RailgunTransverseVelocityMulti_ED, float RailgunTransverseVelocity_NS, float RailgunTransverseVelocity_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcRealDamageReduceFinal(float realDamageReduce_ES, float realDamageReduce_ED, float realDamage_ES, float realDamage_ED)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcReceivedDamageFinalWithTarget(float sourceDamageFinal, float realDamageReduceFinal, float srcAllDamageMulti, float volumnRatio)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcShipJumpMaxSpeedFinal(float shipJumpMaxSpeedBaseValue, float shipJumpMaxSpeedAdd_ES, float shipJumpMaxSpeedAdd_ED, float shipJumpMaxSpeedMulti_ES, float shipJumpMaxSpeedMulti_ED, float shipJumpMaxSpeed_NS, float shipJumpMaxSpeed_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcShipJumpSteadyFinal(float shipJumpSteadyBaseValue, float shipJumpSteadyAdd_ES, float shipJumpSteadyAdd_ED, float shipJumpSteadyDisturb_ES, float shipJumpSteadyDisturb_ED)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcShipMassFinal(float shipMassBaseValue, float shipMassAdd_ES, float shipMassAdd_ED)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcShipMaxSpeedFinal(float shipMassFinal, float shipMaxSpeedNoBoosterFinal, float boosterMultiParam, float modifyConstParam)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcShipMaxSpeedNoBoosterFinal(float shipMaxSpeedBaseValue, float shipMaxSpeedAdd_ES, float shipMaxSpeedAdd_ED, float shipMaxSpeedMulti_ES, float shipMaxSpeedMulti_ED, float shipMaxSpeed_NS, float shipMaxSpeed_ND, float charactorDriveGain)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcShipShieldOrArmorFinal(float baseValue, float npcGainBase, float addESFactor, float addEDFactor, float multiESFactor, float multiEDFactor, float NSFactor, float NDFactor, float charDefGainFinal)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSourceDamageBaseValue(float weaponDamageFinal, float ammoDamage, float ammoDamageModify)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSourceDamageFinal(float sourceDamageBaseValue, float charAttGainFinal, float npcAttGainBase, float sourceDamageAdd_ES, float sourceDamageAdd_ED, float sourceDamageMulti_ES, float sourceDamageMulti_ED, float sourceDamage_NS, float sourceDamage_ND, float flagShipDamagMulti_ED)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSourceDamageFinalFrameCache(float damageFinal, float damageFinalModifyFrameCache)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSourceDamageFinalNoCache(float damageFinal, float damageFinalModifyFrameCache, float damageFinalModifyNoCache)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSourceDamageIncludeTargetShieldRatioDmgMulti(float sourceRealDamageFinal, float targetShieldPercent, float targetShieldRatioDmg_multi)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSourceRealDamageFinal(float sourceRealDamageAdd_ES, float sourceRealDamageAdd_ED)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcuSuperWeaponBurstTriggerIntervalFinal(float burstTriggerIntervalBaseValue, float burstTriggerIntervalMulti_ES, float burstTriggerIntervalMulti_ED, float burstTriggerInterval_NS, float burstTriggerInterval_ND, float burstTriggerIntervalRandomFactor)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcuSuperWeaponChargeSpeedFinal(float chargeSpeedBaseValue, float chargeSpeedMulti_ES, float chargeSpeedMulti_ED, float chargeSpeed_NS, float chargeSpeed_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcVolumnFinal(float volumnFinalWithoutSpeed, float volumnSpeedEffectFactor)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcVolumnFinalWithoutSpeed(float volumnBaseValue, float volumnAdd_ES, float volumnAdd_ED, float volumnMulti_ES, float volumnMulti_ED, float volumn_NS, float volumn_ND)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcVolumnRatioFinal(float targetVolumnFinal, float fireCtrlAccuracyFinal)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcVolumnSpeedEffectFactor(float selfVolumn, float attackerVolumn, float attackerWeaponTransverseVelocity, float onhitTransverseVelocity)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcWeaponCD4NextGroup(float baseValue, float addESFactor, float addEDFactor, float multiESFactor, float multiEDFactor, float NSFactor, float NDFactor, float NSFactorForWeaponType = 1f)
        {
        }

        [MethodImpl(0x8000)]
        private static float MultiAndAdd(float baseValue, float addFactor, float multiFactor)
        {
        }
    }
}

