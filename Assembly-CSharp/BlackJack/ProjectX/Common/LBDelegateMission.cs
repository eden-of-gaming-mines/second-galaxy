﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LBDelegateMission
    {
        protected IDelegateMissionListDataContainer m_dc;
        protected LogicBlockHiredCaptainManagementBase m_lbHiredCaptain;
        protected DelegateMissionInfo m_readOnlyMissionInfo;
        protected LBSignalDelegateBase m_sourceSignal;
        protected List<LBStaticHiredCaptain> m_captainList;
        protected Random m_rand;
        private const double DelegateFightTimeMultiMin = 0.5;
        private const double DelegateFightTimeMultiMax = 5.0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckCancel;
        private static DelegateBridge __Hotfix_SetCanceled;
        private static DelegateBridge __Hotfix_OnInvadeStart;
        private static DelegateBridge __Hotfix_OnInvadeEnd;
        private static DelegateBridge __Hotfix_OnComplete;
        private static DelegateBridge __Hotfix_SetReward;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetSourceSignal;
        private static DelegateBridge __Hotfix_GetState;
        private static DelegateBridge __Hotfix_GetStartTime;
        private static DelegateBridge __Hotfix_GetCompeletime;
        private static DelegateBridge __Hotfix_GetCaptainList;
        private static DelegateBridge __Hotfix_GetDestroyCaptainList;
        private static DelegateBridge __Hotfix_GetSourceSignalType;
        private static DelegateBridge __Hotfix_GetAllRewardList;
        private static DelegateBridge __Hotfix_GetRewardMultiES;
        private static DelegateBridge __Hotfix_GetCurrDelegateMissionPoolCount;
        private static DelegateBridge __Hotfix_GetCurrGuildRewardMulti;
        private static DelegateBridge __Hotfix_GetStoreSpace4Reward;
        private static DelegateBridge __Hotfix_GetInvadeInfoList;
        private static DelegateBridge __Hotfix_GetReadOnlyDelegateMissionInfo;
        private static DelegateBridge __Hotfix_GetMissionCompelteExp;
        private static DelegateBridge __Hotfix_CalcCompleteTime;
        private static DelegateBridge __Hotfix_CalcCompleteTime4DelegateFight;
        private static DelegateBridge __Hotfix_CalcRawCompleteTime4DelegateFight;
        private static DelegateBridge __Hotfix_CalcCompleteTime4DelegateMineral;
        private static DelegateBridge __Hotfix_CalcRawCompleteTime4DelegateMineral;
        private static DelegateBridge __Hotfix_CalcCompleteTime4DelegateTransport;
        private static DelegateBridge __Hotfix_CalcRawCompleteTime4DelegateTransport;
        private static DelegateBridge __Hotfix_CalcDelegateMissionCompleteTimeMin;
        private static DelegateBridge __Hotfix_CalcDelegateMissionCompleteTimeMax;
        private static DelegateBridge __Hotfix_CalcDelegateTransportOneJumpTime;
        private static DelegateBridge __Hotfix_InitCaptains;
        private static DelegateBridge __Hotfix_DismissAllCaptain;

        [MethodImpl(0x8000)]
        public LBDelegateMission(DelegateMissionInfo delegateMission, IDelegateMissionListDataContainer dc, LogicBlockHiredCaptainManagementBase lbHiredCaptain, Random rand)
        {
        }

        [MethodImpl(0x8000)]
        public static double CalcCompleteTime(List<LBStaticHiredCaptain> captainList, LBSignalBase signal)
        {
        }

        [MethodImpl(0x8000)]
        private static double CalcCompleteTime4DelegateFight(List<LBStaticHiredCaptain> captainList, LBSignalDelegateFight signal)
        {
        }

        [MethodImpl(0x8000)]
        private static double CalcCompleteTime4DelegateMineral(List<LBStaticHiredCaptain> captainList, LBSignalDelegateMineral signal)
        {
        }

        [MethodImpl(0x8000)]
        private static double CalcCompleteTime4DelegateTransport(List<LBStaticHiredCaptain> captainList, LBSignalDelegateTransport signal)
        {
        }

        [MethodImpl(0x8000)]
        public static int CalcDelegateMissionCompleteTimeMax()
        {
        }

        [MethodImpl(0x8000)]
        public static int CalcDelegateMissionCompleteTimeMin()
        {
        }

        [MethodImpl(0x8000)]
        public static int CalcDelegateTransportOneJumpTime()
        {
        }

        [MethodImpl(0x8000)]
        private static double CalcRawCompleteTime4DelegateFight(List<LBStaticHiredCaptain> captainList, LBSignalDelegateFight signal)
        {
        }

        [MethodImpl(0x8000)]
        private static double CalcRawCompleteTime4DelegateMineral(List<LBStaticHiredCaptain> captainList, LBSignalDelegateMineral signal)
        {
        }

        [MethodImpl(0x8000)]
        private static double CalcRawCompleteTime4DelegateTransport(List<LBStaticHiredCaptain> captainList, LBSignalDelegateTransport signal)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCancel()
        {
        }

        [MethodImpl(0x8000)]
        public void DismissAllCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetAllRewardList()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticHiredCaptain> GetCaptainList()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetCompeletime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrDelegateMissionPoolCount()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrGuildRewardMulti()
        {
        }

        [MethodImpl(0x8000)]
        public List<ulong> GetDestroyCaptainList()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public List<DelegateMissionInvadeInfo> GetInvadeInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetMissionCompelteExp()
        {
        }

        [MethodImpl(0x8000)]
        public DelegateMissionInfo GetReadOnlyDelegateMissionInfo()
        {
        }

        [MethodImpl(0x8000)]
        public float GetRewardMultiES()
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalDelegateBase GetSourceSignal()
        {
        }

        [MethodImpl(0x8000)]
        public SignalType GetSourceSignalType()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetStartTime()
        {
        }

        [MethodImpl(0x8000)]
        public DelegateMissionState GetState()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetStoreSpace4Reward()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitCaptains()
        {
        }

        [MethodImpl(0x8000)]
        public void OnComplete(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInvadeEnd(DelegateMissionInvadeInfo invadeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInvadeStart(PlayerSimpleInfo invadingPlayeInfo, DateTime invadingStartTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCanceled(bool isByPlayer, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetReward(List<ItemInfo> completeRewardList, List<ItemInfo> processRewardList)
        {
        }
    }
}

