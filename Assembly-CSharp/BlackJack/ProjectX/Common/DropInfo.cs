﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class DropInfo
    {
        public uint m_srcObjId;
        public string m_srcTargetName;
        public int m_srcNPCMonsterConfId;
        public int m_srcHiredCaptainFirstNameId;
        public int m_srcHiredCaptainLastNameId;
        public GrandFaction m_srcHiredCaptainGrandFaction;
        public List<DropInfo4SinglePlayer> m_dorpList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

