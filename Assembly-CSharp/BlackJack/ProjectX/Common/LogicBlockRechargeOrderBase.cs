﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockRechargeOrderBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IRechargeOrderDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetRechargeOrder;
        private static DelegateBridge __Hotfix_AddRechargeOrder;
        private static DelegateBridge __Hotfix_RemoveRechargeOrder;
        private static DelegateBridge __Hotfix_IsOngoingOrderExistsForGoods;
        private static DelegateBridge __Hotfix_GetOngoingOrderList;
        private static DelegateBridge __Hotfix_GetUnacknowledgedOrderList;

        [MethodImpl(0x8000)]
        public void AddRechargeOrder(RechargeOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeOrderInfo> GetOngoingOrderList()
        {
        }

        [MethodImpl(0x8000)]
        public RechargeOrderInfo GetRechargeOrder(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<RechargeOrderInfo> GetUnacknowledgedOrderList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOngoingOrderExistsForGoods(RechargeGoodsType goodsType, int goodsId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveRechargeOrder(ulong orderInstanceId)
        {
        }
    }
}

