﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildBattleBase
    {
        int GetReinforcementEndHour();
        int GetReinforcementEndMinuts();
        bool HasReinforcementEndTimeSetted();
    }
}

