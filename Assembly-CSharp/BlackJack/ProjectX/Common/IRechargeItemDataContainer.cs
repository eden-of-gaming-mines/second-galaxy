﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IRechargeItemDataContainer
    {
        void AddRechargeItem(RechargeItemInfo item);
        RechargeItemInfo GetRechargeItem(int itemId);
        List<RechargeItemInfo> GetRechargeItemList();
        int GetRechargeItemListVersion();
        void SetRechargeItemBought(int itemId, bool isBought = true);
    }
}

