﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class AmmoInfo
    {
        public int m_ammoConfigId;
        public StoreItemType m_ammoType;
        public int m_ammoCount;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public AmmoInfo()
        {
        }

        [MethodImpl(0x8000)]
        public AmmoInfo(AmmoInfo from)
        {
        }
    }
}

