﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum ClientOSType
    {
        IOS,
        Android,
        PC,
        Other,
        Max
    }
}

