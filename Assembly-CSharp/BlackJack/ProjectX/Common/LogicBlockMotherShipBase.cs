﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockMotherShipBase
    {
        protected int m_motherShipSingleSolarSystemSpainTime;
        protected int m_motherShipPerRealMonyCostTime;
        private const int MotherShipRedployTimeMin = 2;
        protected ILBPlayerContext m_playerCtx;
        protected IMotherShipBasicDataContainer m_dc;
        protected LogicBlockItemStoreBase m_lbItemStore;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CheckRedeployStart;
        private static DelegateBridge __Hotfix_CheckRedployDestValid;
        private static DelegateBridge __Hotfix_CheckRedeploySpeedUp;
        private static DelegateBridge __Hotfix_CheckRedeploySpeedUp2End;
        private static DelegateBridge __Hotfix_CheckRedeployConfirm;
        private static DelegateBridge __Hotfix_CheckRedeployCancle;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_GetSpaceStationId;
        private static DelegateBridge __Hotfix_GetRedeployInfo;
        private static DelegateBridge __Hotfix_GetRedeployStartTime;
        private static DelegateBridge __Hotfix_GetRedeployChargingEndTime;
        private static DelegateBridge __Hotfix_UpdateSolarSystemId;
        private static DelegateBridge __Hotfix_UpdateSpaceStationId;
        private static DelegateBridge __Hotfix_UpdateRedeployInfo;
        private static DelegateBridge __Hotfix_CalcRedeploySpeedUp2EndCost;
        private static DelegateBridge __Hotfix_CalcRedploySpeedUpTime;
        private static DelegateBridge __Hotfix_CalcRedployChargingTime;
        private static DelegateBridge __Hotfix_IsRedeploing;

        [MethodImpl(0x8000)]
        public int CalcRedeploySpeedUp2EndCost()
        {
        }

        [MethodImpl(0x8000)]
        public int CalcRedployChargingTime(int jumpDistance)
        {
        }

        [MethodImpl(0x8000)]
        protected int CalcRedploySpeedUpTime(int itemId, int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRedeployCancle(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRedeployConfirm(out bool isNeedRemoteCheck, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRedeploySpeedUp(int itemId, int count, out DateTime newEndTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRedeploySpeedUp2End(out int realMoneyCost, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRedeployStart(int solarSystemId, int spaceStationId, out bool isNeedRemoteCheck, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckRedployDestValid(int solarSystemId, int spaceStationId, ref bool isNeedRemoteCheck, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetRedeployChargingEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public MSRedeployInfo GetRedeployInfo()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetRedeployStartTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSpaceStationId()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedeploing()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRedeployInfo(MSRedeployInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpaceStationId(int spaceStationId)
        {
        }
    }
}

