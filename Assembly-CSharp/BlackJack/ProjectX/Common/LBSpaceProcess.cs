﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSpaceProcess
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcess> EventOnProcessCancel;
        private static int s_instanceIdSeed;
        protected uint m_startTime;
        protected bool m_isEnd;
        protected bool m_isCancel;
        protected bool m_cancelable;
        protected uint m_lastTickTime;
        protected uint m_instanceId;
        protected LBSpaceProcessType m_type;
        protected ILBSpaceTarget m_srcTarget;
        protected ILBSpaceTarget m_destTarget;
        protected ILBSpaceProcessSource m_processSource;
        protected uint m_processForceRemoveTime;
        private const int m_processLifeTimeMax = 0x927c0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AllocInstanceId;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetProcessType;
        private static DelegateBridge __Hotfix_IsEnd;
        private static DelegateBridge __Hotfix_IsCancel;
        private static DelegateBridge __Hotfix_IsCancelable;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_GetSrcTarget;
        private static DelegateBridge __Hotfix_GetDestTarget;
        private static DelegateBridge __Hotfix_GetStartTime;
        private static DelegateBridge __Hotfix_SetProcessSource;
        private static DelegateBridge __Hotfix_SetCancelable;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_add_EventOnProcessCancel;
        private static DelegateBridge __Hotfix_remove_EventOnProcessCancel;

        public event Action<LBSpaceProcess> EventOnProcessCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcess(uint startTime, uint instanceId, LBSpaceProcessType type, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ILBSpaceProcessSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public static uint AllocInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetDestTarget()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessType GetProcessType()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetSrcTarget()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetStartTime()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, LBSpaceProcessType type, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ILBSpaceProcessSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsCancel()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCancelable()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsEnd()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnProcessCancel()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnProcessEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCancelable(bool cancelable)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetProcessSource(ILBSpaceProcessSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint currTime)
        {
        }
    }
}

