﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBFakeInspaceWeaponEquipGroup : LBWeaponEquipGroupBase
    {
        protected ILBStaticShip m_ownerShip;
        protected LBStaticWeaponEquipSlotGroup m_staticGroup;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_IsWeapon;
        private static DelegateBridge __Hotfix_IsEquip;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetGlobalLogicBlockCompPropertiesCalc;
        private static DelegateBridge __Hotfix_InitPropertiesCalculater;
        private static DelegateBridge __Hotfix_SetPropertiesDirty;
        private static DelegateBridge __Hotfix_CalcActiveEquipGroupCostEnergy;
        private static DelegateBridge __Hotfix_CalcActiveEquipGroupRoundCD;
        private static DelegateBridge __Hotfix_CalcWeaponGroupCostEnergy;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_GetWeaponGroupAmmoDamageCompose;
        private static DelegateBridge __Hotfix_CalcWeaponGroupDPS;
        private static DelegateBridge __Hotfix_CalcWeaponGroupCriticalRate;
        private static DelegateBridge __Hotfix_CalcWeaponGroupHitRate;
        private static DelegateBridge __Hotfix_CalcWeaponGroupAmmoReloadTime;
        private static DelegateBridge __Hotfix_CalcWeaponGroupCD4NextWave;
        private static DelegateBridge __Hotfix_CalcShieldRepairerEfficiency;
        private static DelegateBridge __Hotfix_CalcArmorRepairerEfficiency;
        private static DelegateBridge __Hotfix_CalcTotalCPUCost;
        private static DelegateBridge __Hotfix_CalcTotalPowerCost;
        private static DelegateBridge __Hotfix_CalcShieldRepairSpeed;
        private static DelegateBridge __Hotfix_CalcEffectPreviewPropertiesById_1;
        private static DelegateBridge __Hotfix_CalcEffectPreviewPropertiesById_0;
        private static DelegateBridge __Hotfix_CalcEffectPreviewBufParamByIndex_1;
        private static DelegateBridge __Hotfix_CalcEffectPreviewBufParamByIndex_0;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewPropertiesById_1;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewPropertiesById_0;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewBufParamByIndex_1;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewBufParamByIndex_0;
        private static DelegateBridge __Hotfix_CalcDestEffectPreviewPropertiesById;
        private static DelegateBridge __Hotfix_CalcDestEffectPreviewBufParamByIndex;
        private static DelegateBridge __Hotfix_GetEquipPreviewPropertiesBaseByPropertiesFinal;
        private static DelegateBridge __Hotfix_CalcWeaponFireRangeMax;
        private static DelegateBridge __Hotfix_CalcEquipFireRangeMax;
        private static DelegateBridge __Hotfix_CalcDamageComposeHeat;
        private static DelegateBridge __Hotfix_CalcDamageComposeKinetic;
        private static DelegateBridge __Hotfix_CalcCriticalRate;
        private static DelegateBridge __Hotfix_CalcHitRateFinal;
        private static DelegateBridge __Hotfix_CalcReloadAmmoCD;
        private static DelegateBridge __Hotfix_CalcWaveCD;
        private static DelegateBridge __Hotfix_CalcChargeTime;
        private static DelegateBridge __Hotfix_CalcGroupCD;
        private static DelegateBridge __Hotfix_CalcSingleWeaponEquipCPUCost;
        private static DelegateBridge __Hotfix_CalcSingleWeaponEquipPowerCost;
        private static DelegateBridge __Hotfix_CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_CalcLaunchEnergyPercentCost;
        private static DelegateBridge __Hotfix_CalcBufInstanceParam;
        private static DelegateBridge __Hotfix_GetWaveCountForOneShoot;
        private static DelegateBridge __Hotfix_GetTotalUnitCountInWeaponGroup;
        private static DelegateBridge __Hotfix_CalcWaveGroupDamage;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4WeaponEquip;
        private static DelegateBridge __Hotfix_GetPropertyFromConfigData4Ammo;

        [MethodImpl(0x8000)]
        public LBFakeInspaceWeaponEquipGroup(ILBStaticShip ownerShip, LBStaticWeaponEquipSlotGroup staticGroup)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcActiveEquipGroupCostEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcActiveEquipGroupRoundCD()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcArmorRepairerEfficiency()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcBufInstanceParam(int bufId, out float bufInstanceParam, out PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcCriticalRate()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcDamageComposeHeat()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcDamageComposeKinetic()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcDestEffectPreviewBufParamByIndex(int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcDestEffectPreviewPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcEffectPreviewBufParamByIndex(int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcEffectPreviewBufParamByIndex(ConfigDataShipTacticalEquipInfo confInfo, int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcEffectPreviewPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcEffectPreviewPropertiesById(LBTacticalEquipGroupBase tacticalEquipGroup, ConfigDataShipTacticalEquipInfo confInfo, PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEquipFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public override uint CalcGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcHitRateFinal()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcLaunchEnergyPercentCost()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcReloadAmmoCD()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcSelfEffectPreviewBufParamByIndex(int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSelfEffectPreviewBufParamByIndex(ConfigDataShipEquipInfo confInfo, int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcSelfEffectPreviewPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSelfEffectPreviewPropertiesById(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcShieldRepairerEfficiency()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcShieldRepairSpeed()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcSingleWeaponEquipCPUCost()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcSingleWeaponEquipPowerCost()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcTotalCPUCost()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcTotalPowerCost()
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcWaveCD()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWaveGroupDamage()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWeaponFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWeaponGroupAmmoReloadTime()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWeaponGroupCD4NextWave()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWeaponGroupCostEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWeaponGroupCriticalRate()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWeaponGroupDPS()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcWeaponGroupHitRate()
        {
        }

        [MethodImpl(0x8000)]
        public T GetConfInfo<T>() where T: class
        {
        }

        [MethodImpl(0x8000)]
        private static PropertiesId GetEquipPreviewPropertiesBaseByPropertiesFinal(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockCompPropertiesCalc GetGlobalLogicBlockCompPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetTotalUnitCountInWeaponGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetWaveCountForOneShoot()
        {
        }

        [MethodImpl(0x8000)]
        public void GetWeaponGroupAmmoDamageCompose(out float heatDamageCompose, out float kiniticDamageCompose, out float electricDamageCompose)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitPropertiesCalculater()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEquip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertiesDirty()
        {
        }
    }
}

