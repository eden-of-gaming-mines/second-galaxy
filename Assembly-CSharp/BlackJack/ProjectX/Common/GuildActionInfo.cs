﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildActionInfo
    {
        public int m_configId;
        public ulong m_instanceId;
        public int m_solarSystemId;
        public DateTime m_expiryTime;
        public bool m_isFail;
        public float m_process;
        public DateTime m_completedTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

