﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GuildJoinPolicy
    {
        AutoJoin,
        NeedConfirm,
        Max
    }
}

