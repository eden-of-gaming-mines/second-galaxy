﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBBulletDamageInfo
    {
        public float BulletDamageHeatFinal;
        public float BulletDamageKineticFinal;
        public float BulletDamageElectricFinal;
        public float BulletCriticalDamageHeatFinal;
        public float BulletCriticalDamageKineticFinal;
        public float BulletCriticalDamageElectricFinal;
        public float BulletRealDamageHeatFinal;
        public float BulletRealDamageKineticFinal;
        public float BulletRealDamageElectricFinal;
        public float BulletPercentageDamage;
        public WeaponCategory AttackerWeaponType;
        public float AttackerWeaponTransverseVelocity;
        public float TargetTransverseVelocity;
        public float WeaponFireCtrlAccuracy;
        public float ChargeAndChannelBreakFactor;
        public float AllDamageMulti;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetValueTo;
        private static DelegateBridge __Hotfix_GetTotalDamge;
        private static DelegateBridge __Hotfix_HasDamage;

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTotalDamge()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasDamage()
        {
        }

        [MethodImpl(0x8000)]
        public void SetValueTo(LBBulletDamageInfo damageInfo)
        {
        }
    }
}

