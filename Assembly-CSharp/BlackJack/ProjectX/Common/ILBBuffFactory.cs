﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    public interface ILBBuffFactory
    {
        LBBufBase CreateBuf(int bufId, Func<uint> instanceIdAllocater);
        LBBufBase CreateBuf(int bufId, uint instanceId = 0);
    }
}

