﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AllianceCompInviteBase : AllianceCompBase, IAllianceCompInviteBase, IAllianceInviteBase
    {
        protected IAllianceDCInvite m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetDC;
        private static DelegateBridge __Hotfix_GetAllianceInviteList;

        [MethodImpl(0x8000)]
        public AllianceCompInviteBase(IAllianceCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceInviteInfo> GetAllianceInviteList()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDC(IAllianceDCInvite dc)
        {
        }
    }
}

