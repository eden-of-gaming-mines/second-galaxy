﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct LBSyncEventOnHitInfo
    {
        public uint m_srcObjId;
        public float m_damage;
        public SyncEventOnHitFlag m_flag;
        [MethodImpl(0x8000)]
        public bool IsHit()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCritical()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOnShield()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOnArmor()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOnShieldEx()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOnArmorEx()
        {
        }

        public float GetDamage() => 
            this.m_damage;

        public SyncEventOnHitFlag GetFlag() => 
            this.m_flag;
    }
}

