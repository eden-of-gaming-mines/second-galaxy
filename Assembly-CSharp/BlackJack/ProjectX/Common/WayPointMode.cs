﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum WayPointMode
    {
        Loop,
        PingPong,
        OneWay
    }
}

