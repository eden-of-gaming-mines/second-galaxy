﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBStargateInfo")]
    public class GDBStargateInfo : IExtensible
    {
        private int _Id;
        private int _ConfTempleteId;
        private double _locationX;
        private double _locationY;
        private double _locationZ;
        private uint _rotationX;
        private uint _rotationY;
        private int _destSolarSystemId;
        private string _name;
        private string _destSolarSystemLocalizationKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_ConfTempleteId;
        private static DelegateBridge __Hotfix_set_ConfTempleteId;
        private static DelegateBridge __Hotfix_get_LocationX;
        private static DelegateBridge __Hotfix_set_LocationX;
        private static DelegateBridge __Hotfix_get_LocationY;
        private static DelegateBridge __Hotfix_set_LocationY;
        private static DelegateBridge __Hotfix_get_LocationZ;
        private static DelegateBridge __Hotfix_set_LocationZ;
        private static DelegateBridge __Hotfix_get_RotationX;
        private static DelegateBridge __Hotfix_set_RotationX;
        private static DelegateBridge __Hotfix_get_RotationY;
        private static DelegateBridge __Hotfix_set_RotationY;
        private static DelegateBridge __Hotfix_get_DestSolarSystemId;
        private static DelegateBridge __Hotfix_set_DestSolarSystemId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_DestSolarSystemLocalizationKey;
        private static DelegateBridge __Hotfix_set_DestSolarSystemLocalizationKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ConfTempleteId", DataFormat=DataFormat.TwosComplement)]
        public int ConfTempleteId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="locationX", DataFormat=DataFormat.TwosComplement)]
        public double LocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="locationY", DataFormat=DataFormat.TwosComplement)]
        public double LocationY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="locationZ", DataFormat=DataFormat.TwosComplement)]
        public double LocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="rotationX", DataFormat=DataFormat.TwosComplement)]
        public uint RotationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="rotationY", DataFormat=DataFormat.TwosComplement)]
        public uint RotationY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="destSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int DestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="name", DataFormat=DataFormat.Default), DefaultValue("")]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="destSolarSystemLocalizationKey", DataFormat=DataFormat.Default), DefaultValue("")]
        public string DestSolarSystemLocalizationKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

