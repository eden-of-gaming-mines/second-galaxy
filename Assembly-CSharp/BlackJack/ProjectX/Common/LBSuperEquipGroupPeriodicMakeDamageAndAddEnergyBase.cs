﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSuperEquipGroupPeriodicMakeDamageAndAddEnergyBase : LBSuperEquipGroupBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;

        [MethodImpl(0x8000)]
        protected LBSuperEquipGroupPeriodicMakeDamageAndAddEnergyBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }
    }
}

