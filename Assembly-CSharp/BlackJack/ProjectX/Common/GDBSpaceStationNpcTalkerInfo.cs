﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBSpaceStationNpcTalkerInfo")]
    public class GDBSpaceStationNpcTalkerInfo : IExtensible
    {
        private int _ID;
        private bool _IsGlobalNpc;
        private int _FirstNameId;
        private int _LastNameId;
        private int _FunctionType;
        private int _RoomType;
        private int _Gender;
        private int _PersonalityType;
        private int _ResId;
        private int _ScriptType;
        private bool _IsStatic;
        private readonly List<int> _AcceptableQuestIdList;
        private readonly List<int> _ParamList;
        private int _HelloDialogId;
        private int _ScriptParam;
        private readonly List<HelloDialogIdToCompletedMainQuestInfo> _HelloDialogIdToCompletedMainQuestInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_IsGlobalNpc;
        private static DelegateBridge __Hotfix_set_IsGlobalNpc;
        private static DelegateBridge __Hotfix_get_FirstNameId;
        private static DelegateBridge __Hotfix_set_FirstNameId;
        private static DelegateBridge __Hotfix_get_LastNameId;
        private static DelegateBridge __Hotfix_set_LastNameId;
        private static DelegateBridge __Hotfix_get_FunctionType;
        private static DelegateBridge __Hotfix_set_FunctionType;
        private static DelegateBridge __Hotfix_get_RoomType;
        private static DelegateBridge __Hotfix_set_RoomType;
        private static DelegateBridge __Hotfix_get_Gender;
        private static DelegateBridge __Hotfix_set_Gender;
        private static DelegateBridge __Hotfix_get_PersonalityType;
        private static DelegateBridge __Hotfix_set_PersonalityType;
        private static DelegateBridge __Hotfix_get_ResId;
        private static DelegateBridge __Hotfix_set_ResId;
        private static DelegateBridge __Hotfix_get_ScriptType;
        private static DelegateBridge __Hotfix_set_ScriptType;
        private static DelegateBridge __Hotfix_get_IsStatic;
        private static DelegateBridge __Hotfix_set_IsStatic;
        private static DelegateBridge __Hotfix_get_AcceptableQuestIdList;
        private static DelegateBridge __Hotfix_get_ParamList;
        private static DelegateBridge __Hotfix_get_HelloDialogId;
        private static DelegateBridge __Hotfix_set_HelloDialogId;
        private static DelegateBridge __Hotfix_get_ScriptParam;
        private static DelegateBridge __Hotfix_set_ScriptParam;
        private static DelegateBridge __Hotfix_get_HelloDialogIdToCompletedMainQuestInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="ID", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="IsGlobalNpc", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsGlobalNpc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="FirstNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="LastNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int LastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="FunctionType", DataFormat=DataFormat.TwosComplement)]
        public int FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="RoomType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int RoomType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="Gender", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Gender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="PersonalityType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int PersonalityType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="ResId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ResId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="ScriptType", DataFormat=DataFormat.TwosComplement)]
        public int ScriptType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(10, IsRequired=false, Name="IsStatic", DataFormat=DataFormat.Default)]
        public bool IsStatic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="AcceptableQuestIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> AcceptableQuestIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, Name="ParamList", DataFormat=DataFormat.TwosComplement)]
        public List<int> ParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(14, IsRequired=false, Name="HelloDialogId", DataFormat=DataFormat.TwosComplement)]
        public int HelloDialogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=false, Name="ScriptParam", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ScriptParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, Name="HelloDialogIdToCompletedMainQuestInfoList", DataFormat=DataFormat.Default)]
        public List<HelloDialogIdToCompletedMainQuestInfo> HelloDialogIdToCompletedMainQuestInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

