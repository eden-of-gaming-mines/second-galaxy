﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBPVPSignal : LBSignalBase
    {
        protected PVPSignalInfo m_readOnlyPVPSignalInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetSourceSignalInstanceId;
        private static DelegateBridge __Hotfix_GetDynamicSceneInstanceId;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetSignalType;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_GetLocation;
        private static DelegateBridge __Hotfix_IsValid;
        private static DelegateBridge __Hotfix_GetExpiryTime;
        private static DelegateBridge __Hotfix_GetPVPSignalInfo;
        private static DelegateBridge __Hotfix_GetPVPSignalShipList;
        private static DelegateBridge __Hotfix_GetValidShipTypeList;
        private static DelegateBridge __Hotfix_GetSrcSignalPlayerUserId;
        private static DelegateBridge __Hotfix_IsRealPlayer;
        private static DelegateBridge __Hotfix_GetSrcSignalPlayerGuildId;
        private static DelegateBridge __Hotfix_GetSrcSignalPlayerAllianceId;
        private static DelegateBridge __Hotfix_GetSrcSignalPlayerLevel;
        private static DelegateBridge __Hotfix_GetSrcSignalPlayerAvartarId;
        private static DelegateBridge __Hotfix_GetSrcSignalPlayerProfession;
        private static DelegateBridge __Hotfix_GetSrcSignalPlayerName;
        private static DelegateBridge __Hotfix_GetNpcInvadeGuildName;
        private static DelegateBridge __Hotfix_IsNpcInvadeSignal;
        private static DelegateBridge __Hotfix_GetPVPDelegateSignalCaptainList;
        private static DelegateBridge __Hotfix_GetPVPDelegateSignalRewardList;
        private static DelegateBridge __Hotfix_GetCurrMonsterTeamId;

        [MethodImpl(0x8000)]
        public LBPVPSignal(PVPSignalInfo signal)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrMonsterTeamId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetDynamicSceneInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public override DateTime GetExpiryTime()
        {
        }

        [MethodImpl(0x8000)]
        public override ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3D GetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public string GetNpcInvadeGuildName()
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcCaptainInfo> GetPVPDelegateSignalCaptainList()
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetPVPDelegateSignalRewardList()
        {
        }

        [MethodImpl(0x8000)]
        public PVPSignalInfo GetPVPSignalInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetPVPSignalShipList()
        {
        }

        [MethodImpl(0x8000)]
        public override SignalType GetSignalType()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetSourceSignalInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetSrcSignalPlayerAllianceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSrcSignalPlayerAvartarId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetSrcSignalPlayerGuildId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSrcSignalPlayerLevel()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSrcSignalPlayerName()
        {
        }

        [MethodImpl(0x8000)]
        public ProfessionType GetSrcSignalPlayerProfession()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSrcSignalPlayerUserId()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> GetValidShipTypeList()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNpcInvadeSignal()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRealPlayer()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsValid(DateTime now)
        {
        }
    }
}

