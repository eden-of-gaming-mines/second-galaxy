﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Serializable]
    public enum DelegateMissionInvadeEventType
    {
        DefensePlayerEnter = 1,
        DefensePlayerLeave = 2,
        DefensePlayerDead = 3,
        DefenseCaptainDead = 4,
        InvadePlayerEnter = 5,
        InvadePlayerLeave = 6,
        InvadePlayerDead = 7,
        InvadeCaptainDead = 8
    }
}

