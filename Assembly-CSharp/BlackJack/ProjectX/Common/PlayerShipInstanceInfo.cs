﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class PlayerShipInstanceInfo
    {
        public ulong m_instanceId;
        public int m_shipConfId;
        public string m_shipName;
        public ShipState m_state;
        public ShipDestroyState m_destroyState;
        public int[] m_highSlotList;
        public AmmoInfo[] m_highSlotAmmoList;
        public List<int> m_middleSlotList;
        public List<int> m_lowSlotList;
        public List<int> m_storeItemList;
        public uint m_armorCurr;
        public uint m_shieldCurr;
        public uint m_energyCurr;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

