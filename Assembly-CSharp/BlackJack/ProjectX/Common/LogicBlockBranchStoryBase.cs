﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockBranchStoryBase
    {
        protected LogicBlockQuestBase m_lbQuest;
        protected ILBPlayerContext m_lbPlayerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CheckBranchStoryQuestListStartable;
        private static DelegateBridge __Hotfix_GetBranchStoryCurrentQuestId;
        private static DelegateBridge __Hotfix_CheckConf;

        [MethodImpl(0x8000)]
        public bool CheckBranchStoryQuestListStartable(int branchStoryQuestListId, out ConfigDataBranchStoryQuestListInfo conf, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckConf(ConfigDataBranchStoryQuestListInfo conf, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int? GetBranchStoryCurrentQuestId(int branchStoryQuestListId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

