﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompActionBase : GuildCompBase, IGuildCompActionBase, IGuildActionBase
    {
        protected IGuildCompSolarSystemCtxBase m_compSolarSystem;
        protected IGuildCompBasicLogicBase m_compBasic;
        protected IGuildCompCurrencyBase m_compCurrency;
        protected const float SolarSystemSecurityLevelMax = 5f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_CalcGuildMinJumpDistance2TargetSolarSystem;
        private static DelegateBridge __Hotfix_CalcGuildActionInfomationPointCost;
        private static DelegateBridge __Hotfix_CalcInfoPointJumpMulti;
        private static DelegateBridge __Hotfix_CheckSecurityLevel;
        private static DelegateBridge __Hotfix_IsSolarSystemSovereignLevelFit;
        private static DelegateBridge __Hotfix_IsSovereignFit;
        private static DelegateBridge __Hotfix_IsEnoughInfoPoint;
        private static DelegateBridge __Hotfix_CheckGuildActionCreate;
        private static DelegateBridge __Hotfix_CalcInfoPointDiffcultMulti;
        private static DelegateBridge __Hotfix_HasGuildActionCreatePermission;
        private static DelegateBridge __Hotfix_HasSovereignAndFitLevel;

        [MethodImpl(0x8000)]
        public GuildCompActionBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public ulong CalcGuildActionInfomationPointCost(ConfigDataGuildActionInfo guildActionConfig, int solarSystemId, int minJumpDistance)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcGuildMinJumpDistance2TargetSolarSystem(GDBHelper gdbHelper, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private float CalcInfoPointDiffcultMulti(ConfigDataGuildActionInfo guildActionConfig, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcInfoPointJumpMulti(ConfigDataGuildActionInfo guildActionConfig, int minJumpDistance)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildActionCreate(string gameUserId, int guildConfigId, int solarSystemId, GDBHelper gdbHelper, out ulong informationPointCost, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSecurityLevel(int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        private bool HasGuildActionCreatePermission(string gameUserId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        private bool HasSovereignAndFitLevel(int guildConfigId, int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEnoughInfoPoint(int guildConfigId, int solarSysId, int minJumpDis, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemSovereignLevelFit(int guildConfigId, int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSovereignFit(int guildConfigId, int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitlialize()
        {
        }
    }
}

