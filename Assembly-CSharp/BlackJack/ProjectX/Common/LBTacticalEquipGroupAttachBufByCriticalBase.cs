﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupAttachBufByCriticalBase : LBTacticalEquipGroupBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_GetAttachBuffId;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupAttachBufByCriticalBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetAttachBuffId()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }
    }
}

