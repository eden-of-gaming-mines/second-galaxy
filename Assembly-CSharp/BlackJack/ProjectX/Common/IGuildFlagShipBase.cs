﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildFlagShipBase
    {
        bool CheckForGuildFlagShipAddModuleEquip(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, int equipItemStoreIndex, DateTime currTime, out int errCode);
        bool CheckForGuildFlagShipCaptainRegister(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, List<int> drivingLicenseIdList, out int errCode);
        bool CheckForGuildFlagShipCaptainUnregister(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode);
        bool CheckForGuildFlagShipHangarInfoReq(string gameUserId, int solarSystemId, out int errCode);
        bool CheckForGuildFlagShipHangarLocking(string gameUserId, out int errCode);
        bool CheckForGuildFlagShipHangarScanning(string gameUserId, out int errCode);
        bool CheckForGuildFlagShipHangarUnlocking(string gameUserId, out int errCode);
        bool CheckForGuildFlagShipPack(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode);
        bool CheckForGuildFlagShipRemoveModuleEquip(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, DateTime currTime, out int errCode);
        bool CheckForGuildFlagShipRename(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode);
        bool CheckForGuildFlagShipRename4DomesticPackage(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode);
        bool CheckForGuildFlagShipSupplementFuel(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int supplementCount, int applySupplementCount, DateTime currTime, out int errCode);
        bool CheckForGuildFlagShipUnpack(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, ulong shipItemStoreInsId, out int errCode);
        void ClearShipHangarLockMaster();
        List<IGuildFlagShipHangarDataContainer> GetAllFlagShipHangarList();
        PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, int slotIndex);
        PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, ulong shipInstanceId);
        List<int> GetFlagShipEquipModuleConfIdList(IStaticFlagShipDataContainer ship);
        IStaticFlagShipDataContainer GetFlagShipInfoByDrivingInfo(int shipHangarSolarSystemId, string drivingGameUserId);
        IStaticFlagShipDataContainer GetFlagShipInfoByInsId(int shipHangarSolarSystemId, ulong shipInsId);
        IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarById(ulong instanceId);
        IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarBySolarSystemId(int solarSystemId);
        List<GuildFlagShipOptLogInfo> GetGuildFlagShipOptLogInfoList();
        RankType GetHangarSlotRankType(IGuildFlagShipHangarDataContainer shipHangarDC, int index);
        int GetHangarUnlockSlotCount(IGuildFlagShipHangarDataContainer shipHangarDC);
        List<IStaticFlagShipDataContainer> GetShipHangarFlagShipInstanceInfoList(ulong shipHangarInsId);
        string GetShipHangarLockMaster();
        ILBStaticFlagShip GetStaticFlagShip(ulong shipHangarInsId, int slotIndex);
        bool IsFlagShipUnsetFinished(DateTime currTime, ulong shipHangarInsId, int slotIndex);
        bool IsHangarSlotUnlock(IGuildFlagShipHangarDataContainer shipHangarDC, int index);
        void RegisterFlagShipDriver(ulong shipHangarInsId, int slotIndex, PlayerSimplestInfo driverInfo);
        void SetFlagShipCustomName(ulong shipHangarInsId, int slotIndex, string name);
        void UnregisterFlagShipDriver(ulong shipHangarInsId, int slotIndex, string optGameUserId);
        void UpdateShipHangarLockMaster(string masterId);
    }
}

