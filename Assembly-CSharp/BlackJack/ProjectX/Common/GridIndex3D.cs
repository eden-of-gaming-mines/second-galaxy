﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct GridIndex3D
    {
        public int X;
        public int Y;
        public int Z;
        [MethodImpl(0x8000)]
        public override bool Equals(object other)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGridInRangeApproximate(ref GridIndex3D grid, int gridRange)
        {
        }
    }
}

