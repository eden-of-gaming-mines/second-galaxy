﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildCompSolarSystemCtxBase : IGuildSolarSystemCtxBase
    {
        int CalcDailyIncomeInformationPoint();
        int GetSolarSystemFlourishValue(int solarSystemId);
        void ReCalcGuildBuildingSolarSystemEffect(int solarSystemId);
    }
}

