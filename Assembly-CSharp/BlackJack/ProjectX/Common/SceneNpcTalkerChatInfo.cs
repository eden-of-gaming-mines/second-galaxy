﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SceneNpcTalkerChatInfo
    {
        public List<int> m_dialogIdList;
        public NpcDNId m_replaceNpc;
        public SceneMulticastType m_multicastType;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

