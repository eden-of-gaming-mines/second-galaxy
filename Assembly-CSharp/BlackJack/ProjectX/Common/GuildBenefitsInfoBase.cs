﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildBenefitsInfoBase
    {
        public ulong m_instanceId;
        public uint m_guildId;
        public GuildBenefitsType m_type;
        public GuildBenefitsSubType m_subType;
        public DateTime m_createTime;
        public DateTime m_expireTime;
        public List<SimpleItemInfoWithCount> m_itemList;
        public List<string> m_receivedGameUserIdList;
        public int m_configId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

