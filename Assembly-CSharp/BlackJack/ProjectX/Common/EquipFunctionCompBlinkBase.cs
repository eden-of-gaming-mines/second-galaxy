﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class EquipFunctionCompBlinkBase : EquipFunctionCompBase
    {
        protected PropertiesCalculaterBase m_propertiesCalc;
        protected List<LBSpaceProcess> m_processingList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_CalcBlinkDistance;

        [MethodImpl(0x8000)]
        protected EquipFunctionCompBlinkBase(IEquipFunctionCompBlinkOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcBlinkDistance()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsReadyForLaunch(out int errCode)
        {
        }
    }
}

