﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class DiplomacyInfo
    {
        public List<string> m_friendlyPlayer;
        public List<string> m_enemyPlayer;
        public List<uint> m_friendlyGuild;
        public List<uint> m_enemyGuild;
        public List<uint> m_friendlyAllience;
        public List<uint> m_enemyAllience;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

