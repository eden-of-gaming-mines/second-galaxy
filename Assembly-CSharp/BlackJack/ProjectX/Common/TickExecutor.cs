﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class TickExecutor
    {
        private uint m_currTickTime;
        private Action m_executeAction;
        private uint m_nextExecuteTime;
        private uint m_timeInterval;
        private bool m_isTickPause;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_ResumeTick;
        private static DelegateBridge __Hotfix_PauseTick;

        [MethodImpl(0x8000)]
        public TickExecutor(Action executeAction, uint timeInterval)
        {
        }

        [MethodImpl(0x8000)]
        public void PauseTick()
        {
        }

        [MethodImpl(0x8000)]
        public void ResumeTick()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(uint currTime)
        {
        }
    }
}

