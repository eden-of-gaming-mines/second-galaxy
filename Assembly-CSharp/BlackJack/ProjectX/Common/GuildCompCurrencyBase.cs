﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompCurrencyBase : GuildCompBase, IGuildCompCurrencyBase, IGuildCurrencyBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildCurrencyLogType, string, long, ulong, int, string> EventOnCurrencyChange;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetCurrencyInfo;
        private static DelegateBridge __Hotfix_GetTradeMoney;
        private static DelegateBridge __Hotfix_GetInformationPoint;
        private static DelegateBridge __Hotfix_GetTacticalPoint;
        private static DelegateBridge __Hotfix_ModifyGuildTradeMoney;
        private static DelegateBridge __Hotfix_ModifyGuildInformationPoint;
        private static DelegateBridge __Hotfix_ModifyGuildTacticalPoint;
        private static DelegateBridge __Hotfix_FireEventOnCurrencyChange;
        private static DelegateBridge __Hotfix_ModifyCurrencyInternal;
        private static DelegateBridge __Hotfix_ModifyWalletAnnouncement;
        private static DelegateBridge __Hotfix_add_EventOnCurrencyChange;
        private static DelegateBridge __Hotfix_remove_EventOnCurrencyChange;
        private static DelegateBridge __Hotfix_get_DC;

        public event Action<GuildCurrencyLogType, string, long, ulong, int, string> EventOnCurrencyChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildCompCurrencyBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnCurrencyChange(GuildCurrencyLogType type, string playerName, long addValue, ulong finalValue, int param, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildCurrencyInfo GetCurrencyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInformationPoint()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetTacticalPoint()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetTradeMoney()
        {
        }

        [MethodImpl(0x8000)]
        private static ulong ModifyCurrencyInternal(long addValue, ulong orgValue)
        {
        }

        [MethodImpl(0x8000)]
        public void ModifyGuildInformationPoint(long addValue, GuildCurrencyLogType type = 0, string playerName = "", int param = 0, string gameUserId = "")
        {
        }

        [MethodImpl(0x8000)]
        public void ModifyGuildTacticalPoint(long addValue, GuildCurrencyLogType type = 0, string playerName = "", int param = 0, string gameUserId = "")
        {
        }

        [MethodImpl(0x8000)]
        public void ModifyGuildTradeMoney(long addValue, GuildCurrencyLogType type = 0, string playerName = "", int param = 0, string gameUserId = "")
        {
        }

        [MethodImpl(0x8000)]
        protected void ModifyWalletAnnouncement(string newAnnouncement)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

