﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockTechBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, int, bool> EventOnTechUpgrade;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TechUpgradeInfo> EventOnTeachUp4OperateLog;
        protected ILBPlayerContext m_playerCtx;
        protected LogicBlockHiredCaptainManagementBase m_lbHiredCaptain;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected ITechDataContainer m_dc;
        protected List<LBTech> m_techList;
        protected PropertiesCalculater m_propertiesCalculater;
        protected PropertiesCalculaterTiny m_tempPropertiesCalculater;
        protected LBBuffContainer m_bufContainerForPlayer;
        protected LBBuffContainer m_bufContainerForHiredCaptain;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesCalculater;
        private static DelegateBridge __Hotfix_GetBufContainerForPlayer;
        private static DelegateBridge __Hotfix_GetBufContainerForHiredCaptain;
        private static DelegateBridge __Hotfix_GetTechList;
        private static DelegateBridge __Hotfix_GetTechById;
        private static DelegateBridge __Hotfix_GetTechConf;
        private static DelegateBridge __Hotfix_GetTechLevelConf;
        private static DelegateBridge __Hotfix_GetTechUpgradeList;
        private static DelegateBridge __Hotfix_GetUpgradeInfo;
        private static DelegateBridge __Hotfix_AllocTempPropertiesCalc4InStation;
        private static DelegateBridge __Hotfix_FreeTempPropertiesCalc4InStation;
        private static DelegateBridge __Hotfix_CheckTechUpgradeStart;
        private static DelegateBridge __Hotfix_CheckTechUpgradeValid;
        private static DelegateBridge __Hotfix_CheckTechUpgradeByRealMoney;
        private static DelegateBridge __Hotfix_CheckTechUpgradeSpeedUp;
        private static DelegateBridge __Hotfix_CheckTechUpgradeSpeedUpByRealMoney;
        private static DelegateBridge __Hotfix_CheckTechUpgradeCancel;
        private static DelegateBridge __Hotfix_CalcTechUpgradeRealCost;
        private static DelegateBridge __Hotfix_CalcTechUpgradeRealCostCount;
        private static DelegateBridge __Hotfix_CalcTechUpgradeTime;
        private static DelegateBridge __Hotfix_CalcTechUpgradeByRealMoneyCost;
        private static DelegateBridge __Hotfix_CalcTechUpgradeSpeedUpByRealMoneyCost;
        private static DelegateBridge __Hotfix_CalcRealMoneyCostByTechUpgradeTime;
        private static DelegateBridge __Hotfix_GetTechUpgradeCostMultiPropertiesId;
        private static DelegateBridge __Hotfix_GetTechUpgradeTimeMultiPropertiesId;
        private static DelegateBridge __Hotfix_AddTech;
        private static DelegateBridge __Hotfix_InitTech;
        private static DelegateBridge __Hotfix_UpdateTechLevel;
        private static DelegateBridge __Hotfix_OnTechUpgradeComplete;
        private static DelegateBridge __Hotfix_FireEventOnTechUpgrade;
        private static DelegateBridge __Hotfix_FireEventOnTeachUp4OperateLog;
        private static DelegateBridge __Hotfix_GetCharacterEvaluateScore;
        private static DelegateBridge __Hotfix_add_EventOnTechUpgrade;
        private static DelegateBridge __Hotfix_remove_EventOnTechUpgrade;
        private static DelegateBridge __Hotfix_add_EventOnTeachUp4OperateLog;
        private static DelegateBridge __Hotfix_remove_EventOnTeachUp4OperateLog;

        public event Action<TechUpgradeInfo> EventOnTeachUp4OperateLog
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, int, bool> EventOnTechUpgrade
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected bool AddTech(int techId, int level, bool isExecuteForInit = false)
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesCalculaterTiny AllocTempPropertiesCalc4InStation(bool calcWithHiredCaption = true)
        {
        }

        [MethodImpl(0x8000)]
        protected int CalcRealMoneyCostByTechUpgradeTime(double seconds)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcTechUpgradeByRealMoneyCost(int techId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> CalcTechUpgradeRealCost(int techId, int level, LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        private static int CalcTechUpgradeRealCostCount(float propertiesSumValue, int propertiesMulti, int costCount, float costMulti)
        {
        }

        [MethodImpl(0x8000)]
        protected int CalcTechUpgradeSpeedUpByRealMoneyCost(TechUpgradeInfo upgradeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcTechUpgradeTime(ConfigDataTechLevelInfo techLevelConf, ConfigDataTechInfo techConf, LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTechUpgradeByRealMoney(int techId, int level, out int realMoneyCost, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTechUpgradeCancel(int techId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTechUpgradeSpeedUp(int techId, int itemId, int count, out DateTime newEndTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTechUpgradeSpeedUpByRealMoney(int techId, out int realMoneyCost, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTechUpgradeStart(int techId, int level, ulong captainInstanceId, out List<CostInfo> realCostList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTechUpgradeValid(int techId, int level, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnTeachUp4OperateLog(TechUpgradeInfo upgradeInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void FireEventOnTechUpgrade(int techId, int level, bool bNotifyByTime = false)
        {
        }

        [MethodImpl(0x8000)]
        public void FreeTempPropertiesCalc4InStation()
        {
        }

        [MethodImpl(0x8000)]
        public LBBuffContainer GetBufContainerForHiredCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public LBBuffContainer GetBufContainerForPlayer()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCharacterEvaluateScore()
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesCalculater GetPropertiesCalculater()
        {
        }

        [MethodImpl(0x8000)]
        public LBTech GetTechById(int id)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechInfo GetTechConf(int techId)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechLevelInfo GetTechLevelConf(int techId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBTech> GetTechList()
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetTechUpgradeCostMultiPropertiesId(TechType techType, CostType costType)
        {
        }

        [MethodImpl(0x8000)]
        public List<TechUpgradeInfo> GetTechUpgradeList()
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetTechUpgradeTimeMultiPropertiesId(TechType techType)
        {
        }

        [MethodImpl(0x8000)]
        public TechUpgradeInfo GetUpgradeInfo(int techId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitTech(int techId, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTechUpgradeComplete(TechUpgradeInfo upgradeInfo, bool bNotifyByTime = false)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateTechLevel(LBTech tech, int level, bool isExecuteForInit = false)
        {
        }
    }
}

