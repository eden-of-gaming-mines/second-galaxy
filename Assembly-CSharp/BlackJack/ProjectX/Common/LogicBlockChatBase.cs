﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockChatBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected DateTime m_localLastSendTime;
        protected DateTime m_solarSystemEnglishLastSendTime;
        protected DateTime m_solarSystemChineseLastSendTime;
        protected DateTime m_teamLastSendTime;
        protected DateTime m_wisperLastSendTime;
        protected DateTime m_starFieldSendTime;
        protected DateTime m_guildSendTime;
        protected DateTime m_allianceSendTime;
        protected int m_starfieldChatPropId;
        protected IChatDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_IsBanned;
        private static DelegateBridge __Hotfix_GetBannedTime;
        private static DelegateBridge __Hotfix_Ban;
        private static DelegateBridge __Hotfix_CheckChatInLocalChannel;
        private static DelegateBridge __Hotfix_CheckChatInSolarSystemChannel;
        private static DelegateBridge __Hotfix_CheckChatInTeamChannel;
        private static DelegateBridge __Hotfix_CheckChatInWisperChannel;
        private static DelegateBridge __Hotfix_CheckChatInStarFieldChannel;
        private static DelegateBridge __Hotfix_CheckChatInGuildChannel;
        private static DelegateBridge __Hotfix_CheckChatInAllianceChannel;

        [MethodImpl(0x8000)]
        public void Ban(DateTime bannedTime, string bannedReason)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChatInAllianceChannel(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChatInGuildChannel(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChatInLocalChannel(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChatInSolarSystemChannel(out int errCode, ChatLanguageChannel chatLanguageChannel = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChatInStarFieldChannel(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChatInTeamChannel(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChatInWisperChannel(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetBannedTime()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBanned()
        {
        }
    }
}

