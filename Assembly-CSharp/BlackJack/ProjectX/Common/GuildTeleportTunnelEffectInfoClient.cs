﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildTeleportTunnelEffectInfoClient : GuildTeleportTunnelEffectInfo
    {
        public bool m_isLimitedByAntiTeleport;
        public GuildFlagShipTeleportEffectExtraInfo m_flagShipExtraInfo;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

