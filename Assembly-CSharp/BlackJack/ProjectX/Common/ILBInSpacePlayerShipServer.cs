﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBInSpacePlayerShipServer : ILBInSpacePlayerShip, ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        ILogicBlockShipCompAI GetLBAI();
        LogicBlockShipCompFormationMaker GetLBFormationMaker();
        ILogicBlockShipCompSolarSystemGuildFleet GetLBGuildFleet();
        ILBInSpaceShipScriptInterface GetLBScriptInterface();
        void OnShipRemove4SolarSystem();
    }
}

