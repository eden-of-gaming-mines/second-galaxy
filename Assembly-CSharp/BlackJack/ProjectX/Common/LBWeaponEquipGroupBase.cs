﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBWeaponEquipGroupBase : IPropertiesProvider
    {
        protected LogicBlockCompPropertiesCalc m_lbShipCompPropertiesCalc;
        protected PropertiesCalculaterBase m_propertiesCalc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_Prepare4Remove;
        private static DelegateBridge __Hotfix_InitPropertiesCalculater;
        private static DelegateBridge __Hotfix_GetChargeTime;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_GetFireRangeMax;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_HasPropertiesGroup_;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMin_;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMax_;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneAddMulti_;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneSubMulti_;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;

        [MethodImpl(0x8000)]
        protected LBWeaponEquipGroupBase()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        protected abstract uint CalcChargeTime();
        [MethodImpl(0x8000)]
        protected virtual float CalcFireRangeMax()
        {
        }

        public abstract uint CalcGroupCD();
        public abstract float CalcLaunchEnergyCost();
        public abstract float CalcLaunchFuelCost();
        [MethodImpl(0x8000)]
        public float CalcPropertiesById(PropertiesId propertiesId, object ctx = null)
        {
        }

        protected abstract uint CalcWaveCD();
        [MethodImpl(0x8000)]
        public uint GetChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        public float GetFireRangeMax()
        {
        }

        protected abstract LogicBlockCompPropertiesCalc GetGlobalLogicBlockCompPropertiesCalc();
        protected abstract bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result);
        [MethodImpl(0x8000)]
        protected virtual bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool GetPropertiesByIdMax_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool GetPropertiesByIdMin_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool GetPropertiesByIdOneAddMulti_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool GetPropertiesByIdOneSubMulti_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        protected abstract bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result);
        [MethodImpl(0x8000)]
        protected virtual bool HasPropertiesGroup_(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Initialize()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitPropertiesCalculater()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Prepare4Remove()
        {
        }
    }
}

