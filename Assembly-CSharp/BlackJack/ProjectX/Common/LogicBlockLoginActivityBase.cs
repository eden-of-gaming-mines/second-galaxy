﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class LogicBlockLoginActivityBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IDailyLoginDataContainer m_dailyLoginDC;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetConfigDataDailyLoginInfo;
        private static DelegateBridge __Hotfix_CheckDayIndexCanTakeReward;
        private static DelegateBridge __Hotfix_AddNewDailyReward;
        private static DelegateBridge __Hotfix_GetDailyLoginRewardList;
        private static DelegateBridge __Hotfix_GetDailyLoginRewardByIndex;
        private static DelegateBridge __Hotfix_GetDailySignAccumulateRewardCfgIndex;

        [MethodImpl(0x8000)]
        protected LogicBlockLoginActivityBase()
        {
        }

        [MethodImpl(0x8000)]
        public bool AddNewDailyReward(DailyLoginRewardInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDayIndexCanTakeReward(int dayIndex, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDailyLoginInfo GetConfigDataDailyLoginInfo(int dayIndex)
        {
        }

        [MethodImpl(0x8000)]
        public DailyLoginRewardInfo GetDailyLoginRewardByIndex(int dayIndex)
        {
        }

        [MethodImpl(0x8000)]
        public List<DailyLoginRewardInfo> GetDailyLoginRewardList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetDailySignAccumulateRewardCfgIndex(uint totalSignCount)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [CompilerGenerated]
        private sealed class <GetDailyLoginRewardByIndex>c__AnonStorey0
        {
            internal int dayIndex;

            internal bool <>m__0(DailyLoginRewardInfo c) => 
                (c.m_dayIndex == this.dayIndex);
        }
    }
}

