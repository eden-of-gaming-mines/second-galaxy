﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class MailInfo
    {
        public ulong m_instanceId;
        public MailType m_mailType;
        public bool m_hasAttachment;
        public bool m_isReaded;
        public int m_predefineMailId;
        public MailDeleteType m_deleteType;
        public string m_senderGameUserId;
        public string m_senderPlayerName;
        public int m_senderAvartarId;
        public ProfessionType m_profession;
        public string m_senderGuildCodeName;
        public string m_receiverGameUserId;
        public string m_title;
        public List<FormatStringParamInfo> m_titleFormatStrParamList;
        public string m_content;
        public List<FormatStringParamInfo> m_contentFormatStrParamList;
        public byte[] m_binContent;
        public MailBinContentType m_binContentType;
        public List<ItemInfo> m_attachmentList;
        public DateTime m_sendTime;
        public DateTime m_timeoutTime;
        public DateTime m_displayTime;
        public int m_readedExpiredTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

