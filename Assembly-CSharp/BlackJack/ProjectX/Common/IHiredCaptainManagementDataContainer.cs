﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IHiredCaptainManagementDataContainer
    {
        void AddCaptain(NpcCaptainInfo info);
        void AddCaptainFeats(ulong instanceId, int featsId, int featsLevel);
        void AddCaptainInitFeats(ulong instanceId, int featsId, int featsLevel);
        void AddCaptainShip(ulong instanceId, NpcCaptainShipInfo shipInfo);
        List<NpcCaptainInfo> GetCaptainList();
        ulong[] GetWingManList();
        void RemoveCaptain(ulong instanceId);
        void RemoveCaptainFeats(ulong instanceId, int featsId);
        void RemoveCaptainInitFeats(ulong instanceId, int featsId);
        void UpdateCaptain(ulong instanceId, long exp, int level, NpcCaptainState m_state);
        void UpdateCaptainCurrShip(ulong instanceId, int npcCaptainShipId);
        void UpdateCaptainShip(ulong instanceId, NpcCaptainShipInfo shipInfo);
        void UpdateWingMan(int index, ulong instanceId);
    }
}

