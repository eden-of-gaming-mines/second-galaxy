﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockDelegateMissionBase
    {
        protected IDelegateMissionListDataContainer m_dc;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected LogicBlockSignalBase m_lbSignal;
        protected LogicBlockHiredCaptainManagementBase m_lbHiredCaptain;
        protected LogicBlockTechBase m_lbTech;
        protected ILBPlayerContext m_playerCtx;
        protected List<LBDelegateMission> m_delegateMissionList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CheckDelegateMissionStart;
        private static DelegateBridge __Hotfix_CheckDelegateMissionCancelByPlayer;
        private static DelegateBridge __Hotfix_CheckDelegateMissionBalance;
        private static DelegateBridge __Hotfix_OnDelegateMissionStart;
        private static DelegateBridge __Hotfix_OnDelegateMissionInvadeStart;
        private static DelegateBridge __Hotfix_OnDelegateMissionInvadeEnd;
        private static DelegateBridge __Hotfix_DelegateMissionBalance;
        private static DelegateBridge __Hotfix_CalcDelegateMissionCompleteTime;
        private static DelegateBridge __Hotfix_CalcDelegateMissionCountMax;
        private static DelegateBridge __Hotfix_AddExp2Captain;
        private static DelegateBridge __Hotfix_GetMissionList;
        private static DelegateBridge __Hotfix_GetMissionByInstanceId;
        private static DelegateBridge __Hotfix_GetHistoryDelegateMissionList;
        private static DelegateBridge __Hotfix_AddDelegateMission;
        private static DelegateBridge __Hotfix_RemoveDelegateMission;

        [MethodImpl(0x8000)]
        protected LBDelegateMission AddDelegateMission(DelegateMissionInfo missionInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void AddExp2Captain(LBDelegateMission mission)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDelegateMissionCompleteTime(List<LBStaticHiredCaptain> captainList, LBSignalBase signal)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcDelegateMissionCountMax()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDelegateMissionBalance(ulong signalInstanceId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDelegateMissionCancelByPlayer(ulong instanceId, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDelegateMissionStart(List<LBStaticHiredCaptain> captainList, LBSignalBase signal, out double completeTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool DelegateMissionBalance(ulong signalInstanceId, out List<ItemInfo> rewardList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public List<DelegateMissionInfo> GetHistoryDelegateMissionList()
        {
        }

        [MethodImpl(0x8000)]
        public LBDelegateMission GetMissionByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBDelegateMission> GetMissionList()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDelegateMissionInvadeEnd(ulong signalInstanceId, DelegateMissionInvadeInfo invadeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDelegateMissionInvadeStart(ulong signalInstanceId, PlayerSimpleInfo invadingPlayeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDelegateMissionStart(DelegateMissionInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveDelegateMission(ulong signalInstanceId)
        {
        }
    }
}

