﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IGuildBasicDataContainer
    {
        GuildBasicInfo GetGuildBasicInfo();
        uint GetGuildId();
        void UpdateCodeName(string codeName);
        void UpdateGuildBasicInfo(DateTime dismissEnTime);
        void UpdateGuildBasicInfo(GuildAllianceLanguageType languageType, GuildJoinPolicy policy);
        void UpdateGuildBasicInfo(int baseSolarSystem, int baseStation);
        void UpdateGuildBasicInfo(string guildManifesto, string announcement, string walletAnnouncement);
        void UpdateGuildBasicInfo(uint allianceId, string allianceName, AllianceLogoInfo allianceLogo);
        void UpdateGuildBattleSetting(int reinforcementEndHour, int reinforcementEndMinuts);
        void UpdateGuildLeader(string leaderUserId);
        void UpdateGuildLeaderTransferEndTime(DateTime leaderTransferEndTime, string transferTargetUserId);
        void UpdateGuildLogoInfo(GuildLogoInfo logo);
        void UpdateName(string name);
    }
}

