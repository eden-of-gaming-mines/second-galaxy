﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface INpcShopDataContainer
    {
        void ClearSaleCountLimitRecord();
        List<KeyValuePair<int, long>> GetSaleCountLimitRecord();
        long GetSaleCountLimitRecordByShopItemId(int shopItemId);
        void UpdateSaleCountLimitRecord(int shopItemId, long count);
    }
}

