﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockRechargeGiftPackageBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IRechargeGiftPackageDataContainer m_dc;
        protected readonly List<LBRechargeGiftPackage> m_lbRechargeGiftPackageList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetRechargeGiftPackageList;
        private static DelegateBridge __Hotfix_GetFollowingRechargeGiftPackage;
        private static DelegateBridge __Hotfix_GetRechargeGiftPackage;
        private static DelegateBridge __Hotfix_RechargeGiftPackageAdd;
        private static DelegateBridge __Hotfix_GetRechargeGiftPackageListVersion;
        private static DelegateBridge __Hotfix_IsEligibleToBuy;
        private static DelegateBridge __Hotfix_InitLBRechargeGiftPackageList;
        private static DelegateBridge __Hotfix_IsGiftPackagePeriodAndLastAndBuyLimited;

        [MethodImpl(0x8000)]
        public LBRechargeGiftPackage GetFollowingRechargeGiftPackage(int giftPackageId)
        {
        }

        [MethodImpl(0x8000)]
        public LBRechargeGiftPackage GetRechargeGiftPackage(int giftPackageId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeGiftPackage> GetRechargeGiftPackageList(GiftPackageCategory category = 0)
        {
        }

        [MethodImpl(0x8000)]
        public int GetRechargeGiftPackageListVersion()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLBRechargeGiftPackageList()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEligibleToBuy(LBRechargeGiftPackage lbGiftPackage)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsGiftPackagePeriodAndLastAndBuyLimited(LBRechargeGiftPackage lbGiftPackage)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void RechargeGiftPackageAdd(RechargeGiftPackageInfo giftPackage)
        {
        }
    }
}

