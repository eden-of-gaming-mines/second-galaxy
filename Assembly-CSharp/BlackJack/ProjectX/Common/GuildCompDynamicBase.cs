﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompDynamicBase : GuildCompBase, IGuildCompDynamicBase, IGuildDynamicBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_SetGuildDynamicInfo;
        private static DelegateBridge __Hotfix_GetGuildDynamicInfo;

        [MethodImpl(0x8000)]
        public GuildCompDynamicBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public GuildDynamicInfo GetGuildDynamicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildDynamicInfo(GuildDynamicInfo info)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

