﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IInSpacePlayerShipDataContainer : IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        uint GetCurrWingShipObjId();
        List<uint> GetEquipGroupReadyForLaunchTime();
        ulong GetGuildFleetId();
        IEnumerable<KeyValuePair<uint, uint>> GetHateTagetInitInfoList();
        List<uint> GetHigSlotWeaponReadyForLaunchTime();
        List<ShipStoreItemInfo> GetInSpaceShipStoreItemList();
        ulong GetInstanceId();
        uint GetLastWingShipOpTime();
        float GetSuperWeaponEnergy();
        bool IsAutoFight();
        bool IsWingShipEnable();
    }
}

