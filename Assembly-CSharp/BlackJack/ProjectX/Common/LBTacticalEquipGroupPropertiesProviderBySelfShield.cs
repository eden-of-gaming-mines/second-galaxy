﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderBySelfShield : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <ShieldPercentMax>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <ShieldPercentMin>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <FinalModifyFactor4ShieldMax>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <FinalModifyFactor4ShieldMin>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <FinalModifyFactor4ThanShieldMax>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <FinalModifyFactor4LessShieldMin>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnSelfShield;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnSelfVelocity;
        private static DelegateBridge __Hotfix_get_ShieldPercentMax;
        private static DelegateBridge __Hotfix_set_ShieldPercentMax;
        private static DelegateBridge __Hotfix_get_ShieldPercentMin;
        private static DelegateBridge __Hotfix_set_ShieldPercentMin;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4ShieldMax;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4ShieldMax;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4ShieldMin;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4ShieldMin;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4ThanShieldMax;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4ThanShieldMax;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4LessShieldMin;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4LessShieldMin;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderBySelfShield(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnSelfVelocity(ref float propertyValue)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnSelfShield(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float ShieldPercentMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float ShieldPercentMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4ShieldMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4ShieldMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4ThanShieldMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4LessShieldMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

