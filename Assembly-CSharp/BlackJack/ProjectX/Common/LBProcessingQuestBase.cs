﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBProcessingQuestBase
    {
        protected ConfigDataQuestInfo m_confInfo;
        protected QuestProcessingInfo m_readOnlyInfo;
        protected LBQuestEnv m_questEnv;
        protected ILBPlayerContext m_lbPlayerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetQuestEnv;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetQuestReadOnlyInfo;
        private static DelegateBridge __Hotfix_GetQuestId;
        private static DelegateBridge __Hotfix_GetQuestFactionId;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetValidShipTypeList_0;
        private static DelegateBridge __Hotfix_GetValidShipGrandFactionList;
        private static DelegateBridge __Hotfix_GetValidShipTypeList_1;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetShowLevel;
        private static DelegateBridge __Hotfix_GetSceneSolarSystemId;
        private static DelegateBridge __Hotfix_GetCompleteSolarSystemId;
        private static DelegateBridge __Hotfix_GetSceneLocation;
        private static DelegateBridge __Hotfix_GetRewardBonusMulti;
        private static DelegateBridge __Hotfix_GetCurrQuestPoolCount;
        private static DelegateBridge __Hotfix_GetGuildRewardBonusMulti;
        private static DelegateBridge __Hotfix_GetQuestSrcType;
        private static DelegateBridge __Hotfix_GetCompleteCondList;
        private static DelegateBridge __Hotfix_GetQuestFlag;
        private static DelegateBridge __Hotfix_CheckQuestFlag_0;
        private static DelegateBridge __Hotfix_CheckQuestFlag_1;
        private static DelegateBridge __Hotfix_IsFreeSignalQuest_0;
        private static DelegateBridge __Hotfix_IsFreeSignalQuest_1;
        private static DelegateBridge __Hotfix_IsChapterGoalQuest;
        private static DelegateBridge __Hotfix_IsChapterSubItemQuest;
        private static DelegateBridge __Hotfix_IsStoryQuest;
        private static DelegateBridge __Hotfix_IsPuzzleGameQuest;
        private static DelegateBridge __Hotfix_IsFactionGameQuest;
        private static DelegateBridge __Hotfix_IsFactionQuest;
        private static DelegateBridge __Hotfix_IsBattlePassQuest;
        private static DelegateBridge __Hotfix_IsScienceExploreQuest;
        private static DelegateBridge __Hotfix_GetCompleteCondNpcDNId;
        private static DelegateBridge __Hotfix_CheckComplete;
        private static DelegateBridge __Hotfix_CheckCompleteInFuture;
        private static DelegateBridge __Hotfix_CheckNeedAutoLaunchNpcDialogToComplete;
        private static DelegateBridge __Hotfix_IsCompleteWaitConfirm;
        private static DelegateBridge __Hotfix_IsQuestFail;
        private static DelegateBridge __Hotfix_GetCompleteCondParamList;
        private static DelegateBridge __Hotfix_GetReward;
        private static DelegateBridge __Hotfix_GetRandomDropId;
        private static DelegateBridge __Hotfix_GetQuestAdditionRewardConf;
        private static DelegateBridge __Hotfix_CombineQuestRewardInfoList_0;
        private static DelegateBridge __Hotfix_CombineQuestRewardInfoList_1;
        private static DelegateBridge __Hotfix_GetAcceptTime;
        private static DelegateBridge __Hotfix_PostQuestComplete;
        private static DelegateBridge __Hotfix_PostQuestCompleteSingleEvent;
        private static DelegateBridge __Hotfix_CheckQuestSingleCompleteCond;
        private static DelegateBridge __Hotfix_CheckSingleCompleteCondInFuture;
        private static DelegateBridge __Hotfix_GetUncomplelteCondDialog;

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase(QuestProcessingInfo info, LBQuestEnv env, ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckComplete()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCompleteInFuture()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckNeedAutoLaunchNpcDialogToComplete()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckQuestFlag(QuestFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckQuestFlag(ConfigDataQuestInfo conf, QuestFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckQuestSingleCompleteCond(QuestCompleteCondInfo cond, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckSingleCompleteCondInFuture(QuestCompleteCondInfo cond, int index, QuestProcessingInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static List<QuestRewardInfo> CombineQuestRewardInfoList(List<QuestRewardInfo> listOne, List<QuestRewardInfo> listTwo)
        {
        }

        [MethodImpl(0x8000)]
        protected static List<QuestRewardInfo> CombineQuestRewardInfoList(List<QuestRewardInfo> listQuestReward, List<ItemInfo> listItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetAcceptTime()
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestCompleteCondInfo> GetCompleteCondList()
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId GetCompleteCondNpcDNId()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetCompleteCondParamList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCompleteSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrQuestPoolCount()
        {
        }

        [MethodImpl(0x8000)]
        public float GetGuildRewardBonusMulti()
        {
        }

        [MethodImpl(0x8000)]
        public int GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataQuestAdditionRewardInfo GetQuestAdditionRewardConf(ConfigDataQuestInfo questConf, int questLevel)
        {
        }

        [MethodImpl(0x8000)]
        public LBQuestEnv GetQuestEnv()
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestFactionId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestFlag()
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestId()
        {
        }

        [MethodImpl(0x8000)]
        public QuestProcessingInfo GetQuestReadOnlyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public QuestSrcType GetQuestSrcType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRandomDropId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<QuestRewardInfo> GetReward()
        {
        }

        [MethodImpl(0x8000)]
        public float GetRewardBonusMulti()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetSceneLocation()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSceneSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShowLevel()
        {
        }

        [MethodImpl(0x8000)]
        public void GetUncomplelteCondDialog(List<KeyValuePair<int, int>> resultList)
        {
        }

        [MethodImpl(0x8000)]
        public List<GrandFaction> GetValidShipGrandFactionList()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> GetValidShipTypeList()
        {
        }

        [MethodImpl(0x8000)]
        public static List<ShipType> GetValidShipTypeList(int questId, ConfigDataQuestInfo questConf = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBattlePassQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsChapterGoalQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsChapterSubItemQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCompleteWaitConfirm()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFactionGameQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFactionQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFreeSignalQuest()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFreeSignalQuest(ConfigDataQuestInfo conf)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPuzzleGameQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestFail()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsScienceExploreQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStoryQuest()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PostQuestComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PostQuestCompleteSingleEvent(QuestPostEventInfo postEvent)
        {
        }
    }
}

