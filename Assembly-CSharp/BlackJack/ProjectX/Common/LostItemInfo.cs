﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LostItemInfo
    {
        public bool m_isShipStoreItem;
        public double m_lostBindMoney;
        public SimpleItemInfoWithCount m_item;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public LostItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public LostItemInfo(SimpleItemInfoWithCount o, bool isShipStoreItem = false, double lostBindMoney = 0.0)
        {
        }
    }
}

