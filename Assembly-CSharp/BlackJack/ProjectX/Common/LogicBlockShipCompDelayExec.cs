﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompDelayExec
    {
        protected ILBSpaceContext m_spaceCtx;
        private List<KeyValuePair<Action, uint>> m_delayExecList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_DelayExec;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public void DelayExec(Action action, uint delayTime = 600)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(uint currTime)
        {
        }
    }
}

