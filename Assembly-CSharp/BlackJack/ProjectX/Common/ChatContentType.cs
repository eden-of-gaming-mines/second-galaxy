﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum ChatContentType
    {
        Text,
        Voice,
        Player,
        Location,
        Item,
        Ship,
        TeamInvite,
        KillRecord,
        DelegateMission,
        Guild,
        SailReport,
        GuildBattleReport,
        Max
    }
}

