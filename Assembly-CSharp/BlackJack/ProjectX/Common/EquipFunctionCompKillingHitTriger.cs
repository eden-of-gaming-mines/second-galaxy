﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class EquipFunctionCompKillingHitTriger : EquipFunctionCompTrigerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnKillingHitToTarget;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnKillOtherTarget;
        private static DelegateBridge __Hotfix_add_EventOnKillingHitToTarget;
        private static DelegateBridge __Hotfix_remove_EventOnKillingHitToTarget;

        public event Action<ILBSpaceTarget> EventOnKillingHitToTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public EquipFunctionCompKillingHitTriger(IEquipFunctionCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnKillOtherTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

