﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PathFinding;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GDBHelper
    {
        private IGDBDataLoader m_gdbDataLoader;
        private Dijkstra m_dijkstra;
        private Dijkstra m_dijkstraStarField;
        private Dictionary<int, Dictionary<int, DJNode>> m_starFiledIdSolarSystemDJNodeDict;
        private Dictionary<int, DJNode> m_starFieldDJNodeDict;
        private Dictionary<int, Dictionary<int, int>> m_solarSystemDistanceDict;
        private bool m_dijkstraInitlizeFlag;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitContext;
        private static DelegateBridge __Hotfix_ClearContext;
        private static DelegateBridge __Hotfix_GetGDBGalaxyInfo;
        private static DelegateBridge __Hotfix_GetGDBSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetGDBSolarSystemSimpleInfo;
        private static DelegateBridge __Hotfix_GetGDBSpaceStationInfo;
        private static DelegateBridge __Hotfix_GetOwnerStargroupBySolarSystemId;
        private static DelegateBridge __Hotfix_GetOwnerStarfieldBySolarSystemId;
        private static DelegateBridge __Hotfix_IsInSameStarField;
        private static DelegateBridge __Hotfix_GetSolarSystemInfoInStarField;
        private static DelegateBridge __Hotfix_GetRandomSolarSystemInfoInStarField;
        private static DelegateBridge __Hotfix_FindSolarSystemInJumps;
        private static DelegateBridge __Hotfix_FindSolarSystemAtJumps;
        private static DelegateBridge __Hotfix_FindSolarSystemsWithinJumpsGroupByJumps;
        private static DelegateBridge __Hotfix_CalcSolarSystemJumpDistance;
        private static DelegateBridge __Hotfix_CalcSolarSystemJumpDistanceWithCache;
        private static DelegateBridge __Hotfix_FindPath;
        private static DelegateBridge __Hotfix_FindPathInternal;
        private static DelegateBridge __Hotfix_TryAddSolarSystemDJNodeToDijkstra;
        private static DelegateBridge __Hotfix_GetDJNodeListByStarFieldInfo;
        private static DelegateBridge __Hotfix_AddStarFieldLinksToStarFieldDijkstra;
        private static DelegateBridge __Hotfix_AddStarFieldLinkToSolarSystemDijkstra;
        private static DelegateBridge __Hotfix_GetDJNodeBySolarsystemId;
        private static DelegateBridge __Hotfix_GetStarFieldDJNode;

        [MethodImpl(0x8000)]
        public GDBHelper(IGDBDataLoader gdbDataLoader)
        {
        }

        [MethodImpl(0x8000)]
        private void AddStarFieldLinksToStarFieldDijkstra()
        {
        }

        [MethodImpl(0x8000)]
        private void AddStarFieldLinkToSolarSystemDijkstra()
        {
        }

        [MethodImpl(0x8000)]
        public int CalcSolarSystemJumpDistance(int srcSolarsystemId, int destSolarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcSolarSystemJumpDistanceWithCache(int srcSolarSystemId, int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> FindPath(int srcSolarsystemId, int destSolarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected List<DJNode> FindPathInternal(int srcSolarsystemId, int destSolarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> FindSolarSystemAtJumps(int solarsystemId, int jumps)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> FindSolarSystemInJumps(int solarsystemId, int jumps)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo>[] FindSolarSystemsWithinJumpsGroupByJumps(int solarsystemId, int maxJumps)
        {
        }

        [MethodImpl(0x8000)]
        private DJNode GetDJNodeBySolarsystemId(int solarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        private List<DJNode> GetDJNodeListByStarFieldInfo(GDBStarfieldsInfo starFieldInfo)
        {
        }

        [MethodImpl(0x8000)]
        public GDBGalaxyInfo GetGDBGalaxyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemInfo GetGDBSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo GetGDBSolarSystemSimpleInfo(int solarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSpaceStationInfo GetGDBSpaceStationInfo(GDBSolarSystemInfo solarSystemInfo, int spaceStationId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStarfieldsInfo GetOwnerStarfieldBySolarSystemId(int solarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBStargroupInfo GetOwnerStargroupBySolarSystemId(int solarsystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemSimpleInfo GetRandomSolarSystemInfoInStarField(int currSolarSystemId, Random rand)
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> GetSolarSystemInfoInStarField(int currSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private DJNode GetStarFieldDJNode(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public void InitContext(bool initAllGalaxy)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInSameStarField(int srcSolarSystemId, int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void TryAddSolarSystemDJNodeToDijkstra(GDBStarfieldsInfo starFieldInfo)
        {
        }
    }
}

