﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildFlagShipHangarListDataContainer
    {
        bool AddNewFlagShipHangarInfo(GuildFlagShipHangarBasicInfo basicInfo);
        List<IGuildFlagShipHangarDataContainer> GetAllFlagShipHangarDS();
        IGuildFlagShipHangarDataContainer GetFlagShipHangarDSByInsId(ulong instanceId);
        IGuildFlagShipHangarDataContainer GetFlagShipHangarDSBySolarSystemId(int solarSystemId);
    }
}

