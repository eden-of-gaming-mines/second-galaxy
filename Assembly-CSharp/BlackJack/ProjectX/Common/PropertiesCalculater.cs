﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class PropertiesCalculater : BlackJack.PropertiesCalculater.PropertiesCalculater<ConfigDataPropertiesInfo>
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsBaseOrMultiPropertiesId;
        private static DelegateBridge __Hotfix_CalcPropertiesByIdCustom;
        private static DelegateBridge __Hotfix_CalcPropertiesByIdCustomImpl;

        [MethodImpl(0x8000)]
        protected override float CalcPropertiesByIdCustom(int propertiesId, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcPropertiesByIdCustomImpl<T>(T calculater, int propertiesId, object ctx) where T: PropertiesCalculaterBase
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsBaseOrMultiPropertiesId(PropertiesId id)
        {
        }
    }
}

