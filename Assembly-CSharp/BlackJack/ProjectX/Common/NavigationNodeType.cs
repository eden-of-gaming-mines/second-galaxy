﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum NavigationNodeType
    {
        None,
        SpaceStation,
        EnterSpaceStation,
        StarGate,
        UseStarGate,
        Celestial,
        StaticScene,
        PVPSignal,
        QuestScene,
        SolarSystem,
        TeamMember,
        GlobalScene,
        WormholeBackPoint,
        PVPInvadeRescue,
        WormholeGate,
        RareWormholeGate,
        InfectFinalBattle,
        GuildActionScene,
        GuildSentrySignal,
        GuildMember,
        SpaceSignal,
        End,
        Max
    }
}

