﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class AllianceInfo
    {
        public uint m_allianceId;
        public AllianceBasicInfo m_basicInfo;
        public List<AllianceMemberInfo> m_memberList;
        public List<AllianceInviteInfo> m_inviteList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

