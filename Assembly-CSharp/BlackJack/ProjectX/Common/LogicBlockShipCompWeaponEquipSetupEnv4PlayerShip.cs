﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompWeaponEquipSetupEnv4PlayerShip : LogicBlockShipCompWeaponEquipSetupEnv
    {
        protected float m_allItemStoreSize;
        protected bool m_allItemStoreSizeDirty;
        protected float m_allAmmoStoreSize;
        protected bool m_allAmmoStoreSizeDirty;
        protected uint m_allItemCostCPU;
        protected bool m_allItemCostCPUDirty;
        protected uint m_allItemCostPower;
        protected bool m_allItemCostPowerDirty;
        protected ILBStaticPlayerShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsWeaponEquipSlotGroupOptAvaiable;
        private static DelegateBridge __Hotfix_IsHighSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsMiddleSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsLowSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsDependTechReadyForWeaponEquip;
        private static DelegateBridge __Hotfix_CheckItemStoreCapacityForSlotItem;
        private static DelegateBridge __Hotfix_CheckItemStoreCapacityForAllSlotItems;
        private static DelegateBridge __Hotfix_GetItemEquipedCount;
        private static DelegateBridge __Hotfix_GetAllItemStoreSize;
        private static DelegateBridge __Hotfix_GetAllAmmoStoreSize;
        private static DelegateBridge __Hotfix_GetAllItemCostCPU;
        private static DelegateBridge __Hotfix_GetAllItemCostPower;
        private static DelegateBridge __Hotfix_FindAvailableAmmoItemInfoForWeapon;
        private static DelegateBridge __Hotfix_IsAmmoMatchWithWeapon;
        private static DelegateBridge __Hotfix_CheckUnloadAmmoFromSlotGroup;
        private static DelegateBridge __Hotfix_CheckReloadAmmoUseBindMoney;
        private static DelegateBridge __Hotfix_CalcAutoReloadAmmoUseBindMoney;
        private static DelegateBridge __Hotfix_CalcLoadableAmmoCount;
        private static DelegateBridge __Hotfix_OnShipWeaponEquipUpdating;
        private static DelegateBridge __Hotfix_OnShipWeaponAmmoUpdating;
        private static DelegateBridge __Hotfix_CalcAllItemStoreSize;
        private static DelegateBridge __Hotfix_CalcAllAmmoStoreSize;
        private static DelegateBridge __Hotfix_CalcAllItemCostCPU;
        private static DelegateBridge __Hotfix_CalcAllItemCostPower;
        private static DelegateBridge __Hotfix_CalcItemCostCPUAfterEquipped;
        private static DelegateBridge __Hotfix_CalcItemCostPowerAfterEquipped;
        private static DelegateBridge __Hotfix_CompareConfigDataAmmoInfoForWeapon;

        [MethodImpl(0x8000)]
        protected void CalcAllAmmoStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAllItemCostCPU()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAllItemCostPower()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAllItemStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        public long CalcAutoReloadAmmoUseBindMoney(out List<AmmoInfo> reloadAmmoInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcItemCostCPUAfterEquipped(LBStoreItem srcMSStoreItem)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcItemCostPowerAfterEquipped(LBStoreItem srcMSStoreItem)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcLoadableAmmoCount(LBStaticWeaponEquipSlotGroup group, LBStoreItem srcMSStoreItem, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckItemStoreCapacityForAllSlotItems()
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckItemStoreCapacityForSlotItem(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckReloadAmmoUseBindMoney(LBStaticWeaponEquipSlotGroup group, out AmmoInfo reloadAmmoInfo, out long needBindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckUnloadAmmoFromSlotGroup(LBStaticWeaponEquipSlotGroup group, int unloadAmmoCount)
        {
        }

        [MethodImpl(0x8000)]
        protected int CompareConfigDataAmmoInfoForWeapon(RankType rank1, SubRankType subRank1, long count1, RankType rank2, SubRankType subRank2, long count2)
        {
        }

        [MethodImpl(0x8000)]
        public bool FindAvailableAmmoItemInfoForWeapon(int weaponConfigId, int weaponMaxAmmoClipSize, out ItemInfo ammoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetAllAmmoStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAllItemCostCPU()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAllItemCostPower()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetAllItemStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemEquipedCount(StoreItemType itemType, int itemConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAmmoMatchWithWeapon(ConfigDataWeaponInfo weaponConf, StoreItemType ammoType, int ammoConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsDependTechReadyForWeaponEquip(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHighSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsLowSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsMiddleSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsWeaponEquipSlotGroupOptAvaiable(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipWeaponAmmoUpdating()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipWeaponEquipUpdating()
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }
    }
}

