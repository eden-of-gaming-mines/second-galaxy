﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IUserSettingDataContainer
    {
        bool GetAutoFightNotControlPlayerShipMoveState();
        void SetAutoFightNotControlPlayerShipMoveState(bool state);
    }
}

