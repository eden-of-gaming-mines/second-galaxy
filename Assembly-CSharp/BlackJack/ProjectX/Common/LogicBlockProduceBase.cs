﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockProduceBase
    {
        protected ILBPlayerContext m_playerCtx;
        protected LogicBlockHiredCaptainManagementBase m_lbHiredCaptain;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected LogicBlockTechBase m_lbTech;
        protected IProduceDataContainer m_dc;
        protected List<LBProductionLine> m_productionLineList;
        protected List<int> m_productunlockLevelList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnProductionLineAdd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBProductionLine> EventOnProductionLineComplete;
        [CompilerGenerated]
        private static Comparison<int> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_InitLBProductionLine;
        private static DelegateBridge __Hotfix_OnPlayerLevelUp;
        private static DelegateBridge __Hotfix_GetProductionLineList;
        private static DelegateBridge __Hotfix_GetProductionLineByIndex;
        private static DelegateBridge __Hotfix_UpdateProductionLineEndTime;
        private static DelegateBridge __Hotfix_CheckStartProductionLine;
        private static DelegateBridge __Hotfix_CheckProductionLineCancel;
        private static DelegateBridge __Hotfix_CheckProductionLineSpeedUp;
        private static DelegateBridge __Hotfix_CheckProductionLineSpeedUpByRealMoney;
        private static DelegateBridge __Hotfix_CheckProductionLineCompleteConfirm;
        private static DelegateBridge __Hotfix_CalcProductionRealCost;
        private static DelegateBridge __Hotfix_CalcProductionTimeCost;
        private static DelegateBridge __Hotfix_CalcProductionLineSpeedUpByRealMoneyCost;
        private static DelegateBridge __Hotfix_CalcRealMoneyCostByProductionTime;
        private static DelegateBridge __Hotfix_CalcProductionRealCostCount;
        private static DelegateBridge __Hotfix_CalcCurrentProductionLineCount;
        private static DelegateBridge __Hotfix_GetProductionCostMultiPropertiesId;
        private static DelegateBridge __Hotfix_GetProductionTimeMultiPropertiesId;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_add_EventOnProductionLineAdd;
        private static DelegateBridge __Hotfix_remove_EventOnProductionLineAdd;
        private static DelegateBridge __Hotfix_add_EventOnProductionLineComplete;
        private static DelegateBridge __Hotfix_remove_EventOnProductionLineComplete;

        public event Action<int> EventOnProductionLineAdd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProductionLine> EventOnProductionLineComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected int CalcCurrentProductionLineCount(int level)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcProductionLineSpeedUpByRealMoneyCost(LBProductionLine lbLineInfo)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> CalcProductionRealCost(ConfigDataProduceBlueprintInfo confInfo, int count, LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        private static int CalcProductionRealCostCount(float propertiesValue, int propertiesMulti, long costCount, float costMulti)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcProductionTimeCost(ConfigDataProduceBlueprintInfo confInfo, int count, LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcRealMoneyCostByProductionTime(double seconds)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineCancel(int lineIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineCompleteConfirm(int lineIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineSpeedUp(int lineIndex, int itemId, int count, out DateTime newEndTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineSpeedUpByRealMoney(int lineIndex, out int realMoneyCost, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckStartProductionLine(int blueprintIndexId, int count, int lineIndex, ulong captainInstanceId, out ProductionLineInfo productionLineInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetProductionCostMultiPropertiesId(StoreItemType productType, CostType costType)
        {
        }

        [MethodImpl(0x8000)]
        public LBProductionLine GetProductionLineByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBProductionLine> GetProductionLineList()
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetProductionTimeMultiPropertiesId(StoreItemType productType)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        private void InitLBProductionLine(int level)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerLevelUp(int level)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProductionLineEndTime(int index, DateTime dateTime)
        {
        }
    }
}

