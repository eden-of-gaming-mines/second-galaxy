﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildJoinApplyListInfo
    {
        public List<GuildJoinApplyBasicInfo> m_joinApplyList;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

