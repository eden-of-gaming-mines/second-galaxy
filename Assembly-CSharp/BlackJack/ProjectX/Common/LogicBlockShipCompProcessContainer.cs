﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompProcessContainer : ILBSpaceProcessContainer
    {
        protected List<LBSpaceProcess> m_toBeAddProcessList;
        protected List<LBSpaceProcess> m_processList;
        protected HashSet<LBSpaceProcess> m_toBeRemoveProcessList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetProcessByInstanceId;
        private static DelegateBridge __Hotfix_RegistProcess;
        private static DelegateBridge __Hotfix_CancelProcess;
        private static DelegateBridge __Hotfix_CancelAllProcessByDestTarget;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickAdd;
        private static DelegateBridge __Hotfix_TickRemove;
        private static DelegateBridge __Hotfix_AddProcess;
        private static DelegateBridge __Hotfix_ReomveProcess;

        [MethodImpl(0x8000)]
        protected void AddProcess(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void CancelAllProcessByDestTarget(ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void CancelProcess(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcess GetProcessByInstanceId(uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void RegistProcess(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ReomveProcess(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickAdd()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickRemove()
        {
        }
    }
}

