﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GuildBuildingStatus
    {
        Deploying,
        Normal,
        Recycling,
        Offline
    }
}

