﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompInSpacePropertiesCalc : LogicBlockCompPropertiesCalc
    {
        protected ILBInSpaceShip m_ownerShip;
        protected LBShipConfigPropertiesProvider m_configPropertiesProvider;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_TickOnFrameEnd;
        private static DelegateBridge __Hotfix_CollectProviderList;

        [MethodImpl(0x8000)]
        protected override void CollectProviderList()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void TickOnFrameEnd()
        {
        }
    }
}

