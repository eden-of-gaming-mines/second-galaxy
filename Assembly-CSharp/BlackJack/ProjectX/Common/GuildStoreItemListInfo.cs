﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildStoreItemListInfo
    {
        public List<GuildStoreItemInfo> m_itemList;
        public int m_startIndex;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

