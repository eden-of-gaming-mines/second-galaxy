﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockSailReportClient
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected uint m_invadeInstanceId;
        protected int m_relativeSignalId;
        protected float m_totalCauseDamage;
        protected float m_totalSufferDamage;
        protected double m_totalFlyDistance;
        protected int m_totalJumpCount;
        protected int m_destroyShipCount;
        protected int m_totalEnterFightCount;
        protected DateTime m_sailStartTime;
        protected float m_sailDurationTime;
        protected float m_sailGatherExp;
        protected float m_sailGatherCredit;
        protected float m_sailGatherMoney;
        protected List<ItemInfo> m_sailGatherItems;
        protected List<SailReportSolarSystemInfo> m_sailReportSolarSystemInfos;
        protected const int MaxSolarSystemInfoCount = 50;
        protected Stack<int> m_wormHoleSolarSysStack;
        protected bool m_isNewReport;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_IsAnySolarSystemSailReportEventExist;
        private static DelegateBridge __Hotfix_IsNew;
        private static DelegateBridge __Hotfix_Read;
        private static DelegateBridge __Hotfix_GetTotalCauseDamage;
        private static DelegateBridge __Hotfix_GetTotalSufferDamage;
        private static DelegateBridge __Hotfix_GetTotalFlyDistance;
        private static DelegateBridge __Hotfix_GetTotalJumpCount;
        private static DelegateBridge __Hotfix_GetDestroyShipCount;
        private static DelegateBridge __Hotfix_GetTotalEnterFightCount;
        private static DelegateBridge __Hotfix_GetSailStartTime;
        private static DelegateBridge __Hotfix_GetSailDurationTime;
        private static DelegateBridge __Hotfix_GetSailGatherExp;
        private static DelegateBridge __Hotfix_GetSailGatherCredit;
        private static DelegateBridge __Hotfix_GetSailGatherMoney;
        private static DelegateBridge __Hotfix_GetSailGatherItems;
        private static DelegateBridge __Hotfix_GetSailReportSolarSystemInfos;
        private static DelegateBridge __Hotfix_AddTotalCasueDamage;
        private static DelegateBridge __Hotfix_AddTotalSufferDamage;
        private static DelegateBridge __Hotfix_AddTotalFlyDistance;
        private static DelegateBridge __Hotfix_AddTotalJumpCount;
        private static DelegateBridge __Hotfix_AddDestroyShipCount;
        private static DelegateBridge __Hotfix_AddTotalEnterFightCount;
        private static DelegateBridge __Hotfix_AddSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetWormHoleEntranceSolarId;
        private static DelegateBridge __Hotfix_StartRecordSailReportEvent;
        private static DelegateBridge __Hotfix_StopRecordSailReportEvent;
        private static DelegateBridge __Hotfix_AddSailGainExp;
        private static DelegateBridge __Hotfix_AddSailGainCredit;
        private static DelegateBridge __Hotfix_AddSailGainMoney;
        private static DelegateBridge __Hotfix_RecordSailStartTime;
        private static DelegateBridge __Hotfix_RecordSailDurationTime;
        private static DelegateBridge __Hotfix_AddSailGatherItem;
        private static DelegateBridge __Hotfix_GetSailGatherItemsSimple;
        private static DelegateBridge __Hotfix_CreateSailReportSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetSailReportSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetSailReportEvent;
        private static DelegateBridge __Hotfix_RemoveSailReportEvent;
        private static DelegateBridge __Hotfix_SetInvadeId;
        private static DelegateBridge __Hotfix_GetInvadeId;
        private static DelegateBridge __Hotfix_GetSignalId;
        private static DelegateBridge __Hotfix_ClearInvadeId;
        private static DelegateBridge __Hotfix_get_CurrentTime;

        [MethodImpl(0x8000)]
        public void AddDestroyShipCount()
        {
        }

        [MethodImpl(0x8000)]
        public void AddSailGainCredit(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSailGainExp(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSailGainMoney(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSailGatherItem(List<ItemInfo> itemInfos)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void AddTotalCasueDamage(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void AddTotalEnterFightCount()
        {
        }

        [MethodImpl(0x8000)]
        public void AddTotalFlyDistance(double distance)
        {
        }

        [MethodImpl(0x8000)]
        public void AddTotalJumpCount()
        {
        }

        [MethodImpl(0x8000)]
        public void AddTotalSufferDamage(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearInvadeId()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateSailReportSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetDestroyShipCount()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetInvadeId()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSailDurationTime()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSailGatherCredit()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSailGatherExp()
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetSailGatherItems()
        {
        }

        [MethodImpl(0x8000)]
        public List<SimpleItemInfoWithCount> GetSailGatherItemsSimple()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSailGatherMoney()
        {
        }

        [MethodImpl(0x8000)]
        protected SailReportSolarSystemInfo.SailReportEvent GetSailReportEvent(int solarSystemId, SailReportEventType eventType)
        {
        }

        [MethodImpl(0x8000)]
        protected SailReportSolarSystemInfo GetSailReportSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<SailReportSolarSystemInfo> GetSailReportSolarSystemInfos()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetSailStartTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSignalId()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTotalCauseDamage()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTotalEnterFightCount()
        {
        }

        [MethodImpl(0x8000)]
        public double GetTotalFlyDistance()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTotalJumpCount()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTotalSufferDamage()
        {
        }

        [MethodImpl(0x8000)]
        public int GetWormHoleEntranceSolarId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnySolarSystemSailReportEventExist()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNew()
        {
        }

        [MethodImpl(0x8000)]
        public void Read()
        {
        }

        [MethodImpl(0x8000)]
        public void RecordSailDurationTime()
        {
        }

        [MethodImpl(0x8000)]
        public void RecordSailStartTime()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveSailReportEvent(int solarSystemId, SailReportEventType eventType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInvadeId(uint insId, int sigId)
        {
        }

        [MethodImpl(0x8000)]
        public void StartRecordSailReportEvent(int solarSystemId, SailReportEventType eventType, string eventName = "")
        {
        }

        [MethodImpl(0x8000)]
        public void StopRecordSailReportEvent(int solarSystemId, SailReportEventType eventType, bool isRemove = false)
        {
        }

        private DateTime CurrentTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

