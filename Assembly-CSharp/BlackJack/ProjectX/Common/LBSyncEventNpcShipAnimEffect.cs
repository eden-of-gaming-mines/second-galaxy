﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSyncEventNpcShipAnimEffect : LBSyncEvent
    {
        public NpcShipAnimEffectType m_effect;
        public uint m_startTime;
        public uint m_endTime;
        public int m_openParam;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

