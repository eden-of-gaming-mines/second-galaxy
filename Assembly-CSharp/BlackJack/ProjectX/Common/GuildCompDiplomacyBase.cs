﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompDiplomacyBase : GuildCompBase, IGuildCompDiplomacyBase, IGuildDiplomacyBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetDiplomacyInfo;
        private static DelegateBridge __Hotfix_GuildDiplomacyUpdateImpl;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompDiplomacyBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyInfo GetDiplomacyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void GuildDiplomacyUpdateImpl(DeplomacyOptType optType, string playerGameUserId, uint guildId, uint allienceId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

