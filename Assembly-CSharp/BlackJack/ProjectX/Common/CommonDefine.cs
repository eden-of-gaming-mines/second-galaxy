﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public static class CommonDefine
    {
        public const int ItemStoreIndexRangeInOneDataSection = 10;
        public const int ItemStoreSpaceSizeMax = 0xaae60;
        public const int MailStoreIndexRangeInOneDataSection = 5;
        public const int MailStoreIndexMax = 200;
        public const int KillRecordIndexRangeInOneDataSection = 5;
        public const int QuestCompletionSetMaxSize = 0x2710;
        public const int UserGuideGroupSetMaxSize = 0x3e8;
        public const int FunctionOpenStateSetMaxSize = 200;
        public const int SolarSystemPlayerListCountMax = 40;
        public const int NpcMonsterDifficultLevelMax = 20;
        public const int MessageSendDelayInSceneScriptForSummonNpcShipTeam = 0x5dc;
        public const string StrKeyForNpcDialogFormatStrParam = "NpcDailogFormatStr";
        public const int BattlePassLevelMax = 500;
        public const int BattlePassChallangeQuestCountMax = 100;
        public const int GuildFlagShipHangarLockTimeOutTime = 120;
        public const int MultiCastRpcSessionIdCountMax = 0x1388;
        public const int MomentsInfoGetOnceMaxCount = 20;
        public const int MomentsBriefInfoMaxCount = 10;

        public static class AI
        {
            public const float ChaseDistanceDefault = 50000f;
            public const float ChaseDistanceMin = 5000f;
            public const float Distance4LaunchBoosterDefault = 15000f;
            public const float ChaseDistanceIgnoreConflictParam = 1.5f;
            public const float NpcAroundRadiusMin = 500f;
            public const float ShieldArmorPercent4LaunchRepairer = 0.6f;
            public const float EnergyPercent4LaunchRepairer = 0.3f;
            public const float SelfEnergyPercent4RemoteShieldRepairer = 0.2f;
            public const float SelfEnergyPercent4RemoteEnergyRepairer = 0.2f;
            public const float TargetShieldPercent4RemoteShieldRepairer = 0.6f;
            public const float TargetEnergyPercent4RemoteEnergyRepairer = 0.3f;
            public const float TargetShieldPercent4RemoteDefAsst = 0.9f;
            public const float EnergyPercent4SelfEnhance = 0.6f;
            public const float EnergyPercent4RemoteEnhance = 0.2f;
            public const float EnergyPercent4EnemyDebuff = 0.2f;
            public const float EnergyPercent4Blink = 0.2f;
            public const float EnergyPercent4TeamAsset = 0.2f;
            public const float TeamMemberNoBufPercent4TeamAsset = 0.5f;
            public const float TargetEnergyPercent4EnergyDec = 0.1f;
            public const uint EquipUseAITickPeriod = 0x7d0;
            public const uint WeaponAutoLaunchCD = 0x3e8;
            public const int NpcRandPickLockTargetSelectCount = 10;
            public const double KeepFormationDistanceMax = 200000.0;
        }

        public static class Character
        {
            public const int CharChipSlotCount = 8;
            public const int CharChipSchemeCount = 3;
            public const int CharLeveUpSkillFree4Add = 3;
            public const int CharChipRandomBufRetryCountMax = 10;
            public const int CharacterNameLenMax = 100;
            public const int PlayerDailyFreshTimeHour = 0x10;
            public const int PlayerWeeklyFreshTimeHour = 0x10;
        }

        public static class Chat
        {
            public const uint LocolChatChannelCD = 1;
            public const uint SolarSystemChatChannelCD = 10;
            public const uint WisperChatChannelCD = 1;
            public const uint TeamChatChannelCD = 1;
            public const uint StarFieldChatChannelCD = 30;
            public const int StarFieldChatChannelSendLeveMin = 10;
            public const int ChatVoiceMinLength = 1;
            public const int ChatVoiceMaxLength = 20;
            public const int ChatRecordFrequency = 0x3e80;
            public const int ChatEncodeAndDecodeSampleSize = 320;
            public const int ChatVoiceTimeoutInLocal = 0x6ddd00;
            public const int ChatVoiceTimeoutInSolarSystem = 0x6ddd00;
            public const int ChatVoiceTimeoutInTeam = 0x6ddd00;
            public const int ChatVoiceTimeoutInStarfield = 0x6ddd00;
            public const int ChatVoiceTimeoutInWisper = 0x6ddd00;
            public const int ChatVoiceCacheCountMaxInLocal = 0x2710;
            public const int ChatVoiceCacheCountMaxInSolarSystem = 0x2710;
            public const int ChatVoiceCacheCountMaxInTeam = 0x2710;
            public const int ChatVoiceCacheCountMaxInStarfield = 0x2710;
            public const int ChatVoiceCacheCountMaxInWisper = 0x2710;
            public const uint GuildChatChannelCD = 2;
            public const int ChatVoiceTimeoutInGuild = 0x6ddd00;
            public const int ChatVoiceCacheCountMaxInGuild = 0x2710;
            public const uint AllianceChatChannelCD = 2;
            public const int ChatVoiceTimeoutInAlliance = 0x6ddd00;
            public const int ChatVoiceCacheCountMaxInAlliance = 0x2710;
            public const int ChatMsgCacheCountMaxInLocal = 0x7530;
            public const int ChatCachedMsgOutputCDMinInLocal = 500;
            public const int ChatCachedMsgOutputCDMaxInLocal = 0x2710;
            public const float ServerWorkload4ChatCachedMsgOutputCDMinInLocal = 0f;
            public const float ServerWorkload4ChatCachedMsgOutputCDMaxInLocal = 1f;
            public const int ServerWorkloadSamplingIntervalInLocal = 0x7d0;
            public const int ChatMsgCacheCountMaxInSolarSystem = 0x7530;
            public const int ChatCachedMsgOutputCDMinInSolarSystem = 500;
            public const int ChatCachedMsgOutputCDMaxInSolarSystem = 0x2710;
            public const float ServerWorkload4ChatCachedMsgOutputCDMinInSolarSystem = 0f;
            public const float ServerWorkload4ChatCachedMsgOutputCDMaxInSolarSystem = 1f;
            public const int ServerWorkloadSamplingIntervalInSolarSystem = 0x7d0;
            public const int ChatMsgCacheCountMaxInGuild = 0x7530;
            public const int ChatCachedMsgOutputCDMinInGuild = 500;
            public const int ChatCachedMsgOutputCDMaxInGuild = 0x2710;
            public const float ServerWorkload4ChatCachedMsgOutputCDMinInGuild = 0f;
            public const float ServerWorkload4ChatCachedMsgOutputCDMaxInGuild = 1f;
            public const int ServerWorkloadSamplingIntervalInGuild = 0x7d0;
            public const int ChatMsgCacheCountMaxInStarfield = 0x7530;
            public const int ChatCachedMsgOutputCDMinInStarfield = 500;
            public const int ChatCachedMsgOutputCDMaxInStarfield = 0x2710;
            public const float ServerWorkload4ChatCachedMsgOutputCDMinInStarfield = 0f;
            public const float ServerWorkload4ChatCachedMsgOutputCDMaxInStarfield = 1f;
            public const int ServerWorkloadSamplingIntervalInStarfield = 0x7d0;
            public const int ChatMsgCacheCountMaxInAlliance = 0x7530;
            public const int ChatCachedMsgOutputCDMinInAlliance = 500;
            public const int ChatCachedMsgOutputCDMaxInAlliance = 0x2710;
            public const float ServerWorkload4ChatCachedMsgOutputCDMinInAlliance = 0f;
            public const float ServerWorkload4ChatCachedMsgOutputCDMaxInAlliance = 1f;
            public const int ServerWorkloadSamplingIntervalInAlliance = 0x7d0;
        }

        public static class Crack
        {
            public const int CrackSlotInitCount = 4;
            public const int CrackSlotExtendCount = 2;
        }

        public static class DelayTicks
        {
            public const int TipQueueItemDelayTicks = 10;
        }

        public static class DelegateMission
        {
            public const int MissionSuspendTime4PVPScan = 600;
            public const int MissionNearCompleteProtectTime4PVPScan = 600;
            public const int TransportMissionUpdateSolarSystemInervalTime = 60;
            public const int MissionSuspendTime4InvadeShip = 0x5460;
            public const int MissionInvadeProtectTime4PVPScan = 0x5460;
            public const double MissionStartProtectTime4PVPScan = 0.1;
            public const double MissionStartProtectTime4NpcInvadeScan = 0.5;
            public const int MissionQuestSceneInvadablePlayerLevelMin = 10;
            public const int MissionShipCountMax = 5;
        }

        public static class Development
        {
            public const float DevelopmentProjectPerResearchValue = 10f;
            public const float DevelopmentHighSpeedResearchExtraValuePersent = 0.5f;
            public const float DevelopmentBurstResearchExtraValuePersent = 1f;
        }

        public static class Faction
        {
            public const int FactionIDFriendly2All = 1;
            public const int FactionIDNeutral2All = 2;
            public const int FactionIDEnemyly2All = 3;
            public const int FactionIDNeutral2PlayerEnemyly2Npc = 4;
            public const int FactionIDFriendly2PlayerEnemyly2Npc = 5;
            public const int FactionIDEnemyly2PlayerFriendly2Npc = 6;
        }

        public static class Guild
        {
            public const int GuildStaffingLogOnePageCount = 20;
            public const int GuildMessageOnePageCount = 20;
            public const int GuildItemStoreIndexRangeInOneDataSection = 10;
            public const int GuildCurrencyLogOnePageCount = 10;
            public const int GuildBuilingSpyTimeout = 600;
            public const int GuildFleetCountMax = 10;
            public const int GuildFleetMemberCountMax = 50;
            public const int GuildFleetFireControllerSubstituteCountMax = 3;
            public const int GuildFleetNavigatorSubstituteCountMax = 3;
            public const int GuildFlagShipHangarT1SlotCountMax = 3;
            public const int GuildFlagShipHangarT2SlotCountMax = 2;
            public const int GuildFlagShipHangarSlotCountMax = 5;
            public const int GuildFlagShipTeleportChargingTime = 5;
            public const double GuildTeleportTunnelBuildingEffectRange = 20000.0;
            public const double GuildTeleportTunnelFlagShipEffectRange = 20000.0;
            public const int GuildTeleportDestPosShiftingMinToTunnel4Ship = 0x1388;
            public const int GuildTeleportDestPosShiftingMaxToTunnel4Ship = 0x2710;
            public const int GuildTeleportDestPosShiftingMinToTunnel4FlagShip = 0x2710;
            public const int GuildTeleportDestPosShiftingMaxToTunnel4FlagShip = 0x3a98;
        }

        public static class GuildMomentsSubType
        {
            public const int GuildMomentsSubType_Create = 1;
            public const int GuildMomentsSubType_SetBase = 2;
            public const int GuildMomentsSubType_JoinAllience = 3;
            public const int GuildMomentsSubType_LeaveAllience = 4;
            public const int GuildMomentsSubType_LeaderTransfer = 5;
            public const int GuildMomentsSubType_ManagerTransfer = 6;
            public const int GuildMomentsSubType_DonateRankFirst = 7;
            public const int GuildMomentsSubType_DonateRankSecond = 8;
            public const int GuildMomentsSubType_DonateRankThird = 9;
            public const int GuildMomentsSubType_BuyGiftPackage = 10;
            public const int GuildMomentsSubType_ProduceComplete = 11;
            public const int GuildMomentsSubType_ProduceCancel = 12;
            public const int GuildMomentsSubType_ProduceSpeedUp = 13;
            public const int GuildMomentsSubType_DevelopLevelUp = 14;
            public const int GuildMomentsSubType_BuildingDeploy = 15;
            public const int GuildMomentsSubType_BuildingRecycle = 0x10;
            public const int GuildMomentsSubType_MiningBalanceSucceedOneMineral = 0x11;
            public const int GuildMomentsSubType_MiningBalanceFailOneMineral = 0x12;
            public const int GuildMomentsSubType_MiningBalanceSucceedTwoMineral = 0x13;
            public const int GuildMomentsSubType_MiningBalanceFailTwoMineral = 20;
            public const int GuildMomentsSubType_SovereignBattleAttackCreate = 0x15;
            public const int GuildMomentsSubType_SovereignBattleAttackWin = 0x16;
            public const int GuildMomentsSubType_SovereignBattleAttackLose = 0x17;
            public const int GuildMomentsSubType_SovereignBattleDefendCreate = 0x18;
            public const int GuildMomentsSubType_SovereignBattleDefendWin = 0x19;
            public const int GuildMomentsSubType_SovereignBattleDefendLose = 0x1a;
            public const int GuildMomentsSubType_BuildingBattleAttackCreate = 0x1b;
            public const int GuildMomentsSubType_BuildingBattleAttackWin = 0x1c;
            public const int GuildMomentsSubType_BuildingBattleAttackLose = 0x1d;
            public const int GuildMomentsSubType_BuildingBattleDefendCreate = 30;
            public const int GuildMomentsSubType_BuildingBattleDefendWin = 0x1f;
            public const int GuildMomentsSubType_BuildingBattleDefendLose = 0x20;
            public const int GuildMomentsSubType_ActionCreate = 0x21;
            public const int GuildMomentsSubType_ActionComplete = 0x22;
            public const int GuildMomentsSubType_TradeSaleSucceed = 0x23;
            public const int GuildMomentsSubType_TradeSaleFail = 0x24;
            public const int GuildMomentsSubType_TradePurchaseComplete = 0x25;
        }

        public static class HiredCaptain
        {
            public const int InitFeatsCountMax = 4;
            public const int AdditionFeatsCountMax = 4;
            public const int ShipFeatsCountMax = 2;
            public const int CaptainBirthDayRefenceYear = 0x424;
            public const int CaptainInitFeatsCreateTryCount = 20;
            public const int CaptainFeatsLearnTryCount = 10;
            public const int CaptainManagementCountMax = 100;
            public const int WingShipOpCD = 0xea60;
            public const int WingManCountMax = 3;
        }

        public static class Infect
        {
            public const int InfectRefreshHour = 0;
            public const int InfectSolarSystemNpcMonsterCollectionId = 0x3e8;
            public const uint InfectFinalBattleSceneEmptyWaitTime = 0x5265c00;
        }

        public static class Language
        {
            public const string ChineseShortStr = "CN";
            public const string EnglishShortStr = "EN";
        }

        public static class LoginActivity
        {
            public const int DailyLoginTakeMaxCount = 7;
        }

        public static class MotherShip
        {
        }

        public static class NpcShop
        {
            public const int GalaxyNpcShopItemListId = 1;
            public const int GuildGalaNpcShopItemListId = 100;
        }

        public static class OperationLogShopConst
        {
            public const int NPCShop = 0;
            public const int BlackMarket = 1;
        }

        public static class PredefineMail
        {
            public const int QuestRewardPredefineMailId = 1;
            public const int InfectCompleteRewardPredefineMailId = 2;
            public const int LosserPlayerKillRecordPredefineMailId = 3;
            public const int LosserCaptainDeadKillRecordPredefineMailId = 4;
            public const int ContinousCriminalSetPredefineMailId = 5;
            public const int ContinousCriminalUnSetPredefineMailId = 6;
            public const int DrivingLicensePredefineMailId = 8;
            public const int FullMatchOrderMailIdForSeller = 10;
            public const int PartialMatchOrderMailIdForSeller = 11;
            public const int ExpireOrderMailIdForSeller = 12;
            public const int MailIdForBuyer = 13;
            public const int TradeMoneyMailIdForSeller = 14;
            public const int ItemFreezingTimeNotifyMailId = 15;
            public const int GuildApplyConfirmAgreeMailId = 0x10;
            public const int GuildApplyConfirmDisagreeMailId = 0x11;
            public const int GuildDismissStartMailId = 0x12;
            public const int GuildDismissAbortMailId = 0x13;
            public const int GuildNameChangeMailId = 20;
            public const int GuildCodeChangeMailId = 0x15;
            public const int GuildBaseSolarSystemSetChangeMailId = 0x16;
            public const int GuildRemoveByOtherMailId = 0x17;
            public const int DelegateMissionConcelPredefineMailId = 0x1b;
            public const int GiftCDKeyPredefineMailid = 0x1c;
            public const int GuildJobAppointMailId = 0x1d;
            public const int GuildJobFireMailId = 30;
            public const int GuildLeaderTransferMailId = 0x1f;
            public const int GuildActionCompleteMailId = 0x20;
            public const int GuildCompensationByPlayerMailId = 0x21;
            public const int GuildCompensationCloseMailId = 0x22;
            public const int GuildCompensationFinishMailId = 0x23;
            public const int GuildCompensationTimeoutMailId = 0x24;
            public const int GuildCompensationLeaveGuildMailId = 0x25;
            public const int GuildCompensationCancelMailId = 0x26;
            public const int GuildBuildingBattleStartForAttackerGuildMailId = 0x27;
            public const int GuildBuildingBattleStartForDefenderGuildMailId = 40;
            public const int GuildBuildingBattleFailForAttackerGuildMailId = 0x29;
            public const int GuildBuildingBattleFailForDefenderGuildMailId = 0x2a;
            public const int GuildBuildingBattleSuccessForAttackerGuildMailId = 0x2b;
            public const int GuildBuildingBattleSuccessForDefenderGuildMailId = 0x2c;
            public const int GuildBuildingBattleEndBecauseSovereignChangeForAttackerGuildMailId = 0x2d;
            public const int GuildBuildingBattleEndBecauseSovereignChangeForDefenderGuildMailId = 0x2e;
            public const int GuildSovereignBattleStartForAttackerGuildMailId = 0x2f;
            public const int GuildSovereignBattleStartForDefenderGuildMailId = 0x30;
            public const int GuildSovereignBattleFailMailId = 0x31;
            public const int GuildSovereignBattlePlayerDeclarationMailId = 50;
            public const int GuildSovereignBattleAutoDeclarationMailId = 0x33;
            public const int GuildCompensationByGuildMailId = 0x34;
            public const int GuildMiningBalanceTimeResetMailId = 0x35;
            public const int GuildMiningBalanceShipDestroyedAndLostOneTypeMineralMailId = 0x36;
            public const int GuildMiningBalanceShipDestroyedAndLostTwoTypeMineralMailId = 0x37;
            public const int GuildActionCreateMailId = 0x38;
            public const int AllianceCreateMailId = 0x39;
            public const int AllianceMemberJoinMailId = 0x3a;
            public const int AllianceMemberLeaveMailId = 0x3b;
            public const int AllianceNameChangeMailId = 60;
            public const int AllianceLeaderTransferMailId = 0x3d;
            public const int AllianceDismissMailId = 0x3e;
            public const int GuildTradeTransportSucceedMailId = 0x41;
            public const int GuildTradeTransportFailMailId = 0x3f;
            public const int GuildTradePurchaseTransportSucceedMailId = 0x40;
            public const int RechargeGiftPackageSellOut = 0x44;
            public const int RechargeMonthlyCardExpireNotification = 0x45;
            public const int RechargeGiftPackageNotInGuildCompensation = 70;
            public const int BattlePassPeriodChangeMailId = 80;
        }

        public static class Produce
        {
            public const int ProduceLineCountMax = 5;
        }

        public static class PVP
        {
            public const float SecurityLevel4HighSafety = 5f;
            public const float SecurityLevel4Dangerous = 0f;
            public const float CriminalLevel4ContinuousFlag = 5f;
            public const float CriminalLevelMax = 10f;
            public const uint PoliceLeaveFightTimeOut = 0x2710;
            public const float NoCrimeStateMaxCriminalLevel = 1f;
            public const float SuspectStateMaxCriminalLevel = 5f;
            public const float CrimeStateMaxCriminalLevel = 10f;
            public const int ExtendDisconnectedTiemOutTimeInPVP = 0x1388;
        }

        public static class Ranking
        {
            public const int RankingPlayerLevelMin = 10;
            public const int ContinueKillRankingMin = 10;
            public const int QueryTopNumMax = 100;
            public const int RankingPlayerLevelLimit = 10;
            public const int RankingNumVisibleRange = 0x3e8;
        }

        public static class Script
        {
            public const int NpcInteractionTemplateTractionEffects = 1;
            public const int NpcInteractionTemplateCollectEffects = 2;
            public const int NpcInteractionTemplateScanEffects = 3;
            public const int NpcInteractionTemplateNpEffects = 7;
        }

        public static class Ship
        {
            public const int ShipHangarSizeMax = 20;
            public const int HighSlotGroupSlotCountMax = 3;
            public const uint ShipDeadRemoveDelay = 0x1194;
            public const int NpcShipAnimEffectDelay = 0x1388;
            public const int StationLeaveHoldMoveStateTime = 0x1388;
            public const float StationLeaveVelocityOutput = 0.5f;
            public const int StarGateLeaveHoldMoveStateTime = 0x1388;
            public const double ShipEnterJumpTunnelSpeed = 5000.0;
            public const double ShipLeaveJumpTunnelSpeed = 15000000000;
            public const int FleetShipCountMax = 5;
            public const int FleetCountMax = 10;
            public const int FleetNameByteMax = 50;
            public const int TickableBufPeriod = 0x3e8;
            public const int ShipFightBufCountMaxInSingleType = 30;
            public const float TraceSpeedModifyParam = 2f;
            public const float DropDistanceMax = 300000f;
            public const float PVPDropRate = 0.5f;
            public const uint DropBoxLifeTime = 0x927c0;
            public const uint DropPickTime = 0x7d0;
            public const double DropPickDistance = 50000.0;
            public const double DropPickDistancePow = 2500000000;
            public const int ShipNameLenMax = 50;
            public const float ShipScaleMin = 1f;
            public const float ShipScaleMax = 7f;
            public const uint ShieldRecoveryInterval = 0xbb8;
            public const int ShieldMaxValueFactorToShieldEx = 1;
            public const float JumpDisturbActiveTimeLength = 1f;
            public const float WeaponFireRangeMultiEDMax = 0.6f;
            public const float WeaponEquipDamageMultiEDMax = 0.7f;
            public const float NoBoosterSpeedMultiEDMax = 1f;
            public const uint WeaponEquipCDMinValue = 0x3e8;
            public const float ShipSpeedPercent4JumpPrepareEnd = 0.75f;
            public const float ShipDirShiftingAngleMax4JumpPrepareEnd = 5f;
        }

        public static class Signal
        {
            public const int SignalDifficultMax = 20;
            public const int DelegateTransportSignalJumpDistanceMin = 3;
            public const int DelegateTransportSignalJumpDistanceMax = 5;
            public const int PVPSignalExpireTimeForDelegateMission = 600;
            public const int PVPSignalExpireTimeForManualQuest = 200;
            public const int PVPSignalExpireTimeForPVPInvade = 0xe10;
            public const int HistoryDelegateMissionCountMax = 20;
        }

        public static class SimplestInfo
        {
            public const int PlayerSimplestInfoGetMaxCount = 100;
            public const int GuildAllianceSimplestInfoGetMaxCount = 50;
            public const int AllianceSimpleInfoGetMaxCount = 20;
        }

        public static class SolarSystem
        {
            public const float Tolerant4LoactionEqual = 100f;
            public const double AU = 15000000000;
            public const double SolarSystemRadiusAU = 60.0;
            public const double SolarSystemGridSize = 1000000.0;
            public const double SolarSystemSceneGridSize = 3000000.0;
            public const double RangeForLoadCelestial = 10000000000;
            public const double ChangeGridBufferSize = 100000.0;
            public const float UnityUnitLengthToLightYear = 0.1f;
            public const float MeterPerUnityUnitInShipLayer = 100f;
            public const float MeterPerUnityUnitInCelestialLayer = 1000000f;
            public const float AOIRadius4PlayerMax = 300000f;
            public const float AOIRadius4NPCMax = 100000f;
            public const float MoonNeedAOIRadiusMax = 30000f;
            public const int SolarSystemObjectCountMax = 0x100000;
            public const float CelestialSpaceConflictRange = 1.5f;
            public const float CelestialCloseToRange = 1.8f;
            public const float CelestialNearRangeMax = 10f;
            public const float MoonNearRangeMax = 3f;
            public const double SceneRadiusMax = 1000000.0;
            public const double SceneAOIRadius = 1000000.0;
            public const uint SceneEmptyWaitTimeOut = 0x30d40;
            public const uint SceneShutdownEmptyWaitTimeOut = 0xea60;
            public const uint DelegateInvadeSceneEmptyWaitTimeOut = 0x7530;
            public const int DynamicSceneGridYRange = 5;
            public const int DynamicSceneAllocateTryCountMax = 10;
            public const int DynamicSceneLooseAllocateTryCountMax = 5;
            public const int DynamicScenePlayerEnterMax = 200;
            public const double AOIRadiusMax = 1000000.0;
            public const float LightYearToStarMapUnityPosRatio = 0.1f;
            public const float EarthGravitationalAcceleration = 9.80665f;
            public const float SyncViewEnterListMaxLength = 20f;
            public const float SyncViewSyncEventListMaxLength = 200f;
            public const float SyncViewMoveStateListMaxLength = 100f;
            public const int SyncEventImportantTargetCountMax = 20;
            public const uint MaxTickSeqDistance4Sync2Client = 20;
            public const double JumpLowSpeedMax = 1000000.0;
            public const double JumpModeChangedSpeed = 800000.0;
            public const float SolarSystemChannelDataBaseNumber = 255f;
            public const int Navagation2PlayerRoundRaduis = 0xbb8;
            public const int RemoveDelayAfterFakeJumpOut = 0x7d0;
            public const int FakeJumpInDelay = 100;
            public const int ResummonStarGateGuardPoliceDelay = 0xea60;
            public const int WormholeGateRefreshHour = 12;
            public const int AOIAccurateShiftRadius = 0x3e8;
            public const int WormholeSorlarSystemSceneCountFullTime = 0x1c20;
            public const int TargetListGlobalSceneCountMax = 5;
            public const int PlayerCountForSceneEnterCrowdState = 100;
            public const int JumpLocationRadius = 0x1388;
        }

        public static class SolarSystemPoolCount
        {
            public static int[] RefreshQuestPoolCountHour = new int[] { 2, 10, 0x12, 0x1a };
            public static int[] RefreshDelegateMissionPoolCountHour = new int[] { 2, 10, 0x12, 0x1a };
        }

        public static class Team
        {
            public const int TeamMemberCountMax = 3;
            public const int TeamInviteSendCD = 0x1388;
        }

        public static class Tech
        {
            public const int TechUpgradeQueueSizeMax = 1;
        }

        public static class WeaponEquip
        {
            public const int WeaponEquipMaxTargetCount = 30;
        }
    }
}

