﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompWeaponAmmoSetup4PlayerShip
    {
        protected ILBStaticPlayerShip m_ownerShip;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWeaponAmmoUpdating;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_AutoReloadAmmoForAllWeaponGroups;
        private static DelegateBridge __Hotfix_ReloadWeaponGroupAmmoToCount;
        private static DelegateBridge __Hotfix_ReloadAmmoFromMSStoreImpl_1;
        private static DelegateBridge __Hotfix_AutoReloadAmmoImpl;
        private static DelegateBridge __Hotfix_ApplyDefaultAmmo;
        private static DelegateBridge __Hotfix_ReloadAmmoFromMSStoreImpl_0;
        private static DelegateBridge __Hotfix_ReloadAmmoFromShipStoreImpl;
        private static DelegateBridge __Hotfix_ReloadAmmoImplForWeaponGroup;
        private static DelegateBridge __Hotfix_UnloadAmmoImpl;
        private static DelegateBridge __Hotfix_OutPutAmmoChangeOperationLog;
        private static DelegateBridge __Hotfix_get_LbWeaponEquip;
        private static DelegateBridge __Hotfix_add_EventOnWeaponAmmoUpdating;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponAmmoUpdating;

        public event Action EventOnWeaponAmmoUpdating
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ApplyDefaultAmmo(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public bool AutoReloadAmmoForAllWeaponGroups(bool needReloadFromMSStore, bool changeAmmoAllowable, List<int> ammoCountList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool AutoReloadAmmoImpl(LBStaticWeaponEquipSlotGroup group, out int ammoAddCount, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBStaticPlayerShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OutPutAmmoChangeOperationLog(StoreItemType type, int configId, int changeValue, OperateLogItemChangeType causeId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool ReloadAmmoFromMSStoreImpl(LBStaticWeaponEquipSlotGroup group, LBStoreItem srcMSStoreItem, int count, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool ReloadAmmoFromMSStoreImpl(LBStaticWeaponEquipSlotGroup group, StoreItemType itemType, int itemConfigId, int count, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool ReloadAmmoFromShipStoreImpl(LBStaticWeaponEquipSlotGroup group, int itemConfigId, StoreItemType itemType, int count, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void ReloadAmmoImplForWeaponGroup(LBStaticWeaponEquipSlotGroup group, StoreItemType itemType, int itemConfigId, int count, OperateLogItemChangeType causeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool ReloadWeaponGroupAmmoToCount(LBStaticWeaponEquipSlotGroup group, ConfigDataWeaponInfo groupConfigInfo, int ammoId, int toLoadCount, bool bCanOperateMSItemStore, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnloadAmmoImpl(LBStaticWeaponEquipSlotGroup group, int unloadAmmoCount)
        {
        }

        protected LogicBlockShipCompStaticWeaponEquipBase LbWeaponEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

