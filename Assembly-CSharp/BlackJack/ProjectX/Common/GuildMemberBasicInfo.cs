﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildMemberBasicInfo
    {
        public string m_gameUserId;
        public string m_platformAuthId;
        public ushort m_seq;
        public List<GuildJobType> m_jobs;
        public DateTime m_joinTime;
        public ulong m_guildFleetId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

