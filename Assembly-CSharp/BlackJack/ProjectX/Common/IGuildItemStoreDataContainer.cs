﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildItemStoreDataContainer
    {
        GuildStoreItemInfo CreateEmptyGuildStoreItemInfo();
        GuildStoreItemInfo GetGuildStoreItemInfoByIndex(int index);
        List<GuildStoreItemListInfo> GetGuildStoreItemInfoList();
        void UpdateGuildStoreItemInfo(int itemIndex, bool isDeleted);
        void UpdateGuildStoreItemInfo(int itemIndex, long count);
        void UpdateGuildStoreItemInfo(int itemIndex, ItemInfo itemInfo, bool isDeleted, uint flag);
    }
}

