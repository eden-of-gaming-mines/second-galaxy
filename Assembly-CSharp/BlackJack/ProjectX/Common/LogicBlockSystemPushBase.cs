﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockSystemPushBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected ISystemPushDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_UpdatePlayerSystemPushInfo;

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerSystemPushInfo(ClientOSType osType, string pushToken, LanguageType languageType)
        {
        }
    }
}

