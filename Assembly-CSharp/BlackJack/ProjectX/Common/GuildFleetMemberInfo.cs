﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildFleetMemberInfo
    {
        public string m_memberGameUserId;
        public uint m_memberPosition;
        public int m_navigatorSubstituteSeq;
        public int m_fireControllerSubstituteSeq;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckFleetPosition;
        private static DelegateBridge __Hotfix_SetFleetPosition;
        private static DelegateBridge __Hotfix_UnsetFleetPosition;
        private static DelegateBridge __Hotfix_ClearFleetPosition;
        private static DelegateBridge __Hotfix_GetAllFleetPositions;

        [MethodImpl(0x8000)]
        public bool CheckFleetPosition(FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearFleetPosition()
        {
        }

        [MethodImpl(0x8000)]
        public List<FleetPosition> GetAllFleetPositions()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFleetPosition(FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        public void UnsetFleetPosition(FleetPosition position)
        {
        }
    }
}

