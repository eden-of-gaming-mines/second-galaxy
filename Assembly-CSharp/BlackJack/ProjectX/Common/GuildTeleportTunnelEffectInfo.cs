﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildTeleportTunnelEffectInfo
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public float m_teleportDistance;
        public bool m_isCreatedByFlagShip;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildTeleportTunnelEffectInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildTeleportTunnelEffectInfo(GuildTeleportTunnelEffectInfo info)
        {
        }
    }
}

