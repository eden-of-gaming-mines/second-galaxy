﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GuildPurchaseLogType
    {
        Create,
        Match,
        Modify
    }
}

