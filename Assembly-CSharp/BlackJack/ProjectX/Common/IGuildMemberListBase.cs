﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildMemberListBase
    {
        GuildMemberBase GetMember(string gameUserId);
        int GetMemberCount();
        List<GuildMemberBase> GetMemberList();
        GuildMemberListInfo GetMemberListInfo();
    }
}

