﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class SailReportSolarSystemInfo
    {
        public int m_solarSystemId;
        public string m_solarSystemName;
        public List<SailReportEvent> m_sailReportEvents;
        private static DelegateBridge _c__Hotfix_ctor;

        [Serializable]
        public class SailReportEvent
        {
            public SailReportEventType m_eventType;
            public string m_eventName;
            public float m_duration;
            public DateTime m_eventStartTime;
            protected bool m_isRecording;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_EventStart;
            private static DelegateBridge __Hotfix_EventStop;
            private static DelegateBridge __Hotfix_IsEventStoped;

            [MethodImpl(0x8000)]
            public void EventStart(DateTime dt)
            {
            }

            [MethodImpl(0x8000)]
            public void EventStop(DateTime dt)
            {
            }

            [MethodImpl(0x8000)]
            public bool IsEventStoped()
            {
            }
        }
    }
}

