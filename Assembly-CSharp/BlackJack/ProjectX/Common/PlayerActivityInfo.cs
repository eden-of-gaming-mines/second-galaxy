﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct PlayerActivityInfo
    {
        public ActivityType m_type;
        public int m_playedCount;
    }
}

