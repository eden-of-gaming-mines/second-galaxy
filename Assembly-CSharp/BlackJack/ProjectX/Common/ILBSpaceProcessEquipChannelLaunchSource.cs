﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessEquipChannelLaunchSource : ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        void OnProcessLoop(LBSpaceProcessEquipChannelLaunch process);
    }
}

