﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildTradePurchaseOrderTransportLogInfo
    {
        public GuildTradePurchaseOrderTransportLogType m_logType;
        public StoreItemType m_itemType;
        public int m_itemConfigId;
        public int m_itemCount;
        public int m_solarSystemId;
        public DateTime m_time;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

