﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class DelegateMissionInfo
    {
        public DelegateMissionState m_state;
        public SignalInfo m_sourceSignal;
        public List<NpcCaptainInfo> m_captainList;
        public DateTime m_startTime;
        public DateTime m_completeTime;
        public List<ItemInfo> m_completeRewardList;
        public List<ItemInfo> m_processRewardList;
        public float m_rewardMulti_ES;
        public int m_currDelegateMissionPoolCount;
        public float m_currGuildRewardMulti;
        public List<DelegateMissionInvadeInfo> m_invadeInfoList;
        public List<ulong> m_destroyCaptainInstanceIdList;
        [NonSerialized]
        public PlayerSimpleInfo m_currInvadingPlayerInfo;
        [NonSerialized]
        public DateTime m_invadeStartTime;
        [NonSerialized]
        public bool m_needCalcReward;
        public List<int> m_hiredCaptainStaticBufList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

