﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public abstract class AllianceBase : IAllianceCompOwnerBase, IAllianceBasicBase, IAllianceChatBase, IAllianceInviteBase, IAllianceMailBase, IAllianceMembersBase
    {
        protected List<AllianceCompBase> m_compList;
        protected AllianceCompBasicBase m_compBasic;
        protected AllianceCompChatBase m_compChat;
        protected AllianceCompInviteBase m_compInvite;
        protected AllianceCompMailBase m_compMail;
        protected AllianceCompMembersBase m_compMembers;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IAllianceCompOwnerBase.GetBasicComp;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IAllianceCompOwnerBase.GetChatComp;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IAllianceCompOwnerBase.GetInviteComp;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IAllianceCompOwnerBase.GetMailComp;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IAllianceCompOwnerBase.GetMembersComp;
        private static DelegateBridge __Hotfix_InitAllComps;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetAllianceBasicInfo;
        private static DelegateBridge __Hotfix_GetAllianceInviteList;
        private static DelegateBridge __Hotfix_GetAllianceMemberList;

        [MethodImpl(0x8000)]
        protected AllianceBase()
        {
        }

        [MethodImpl(0x8000)]
        IAllianceCompBasicBase IAllianceCompOwnerBase.GetBasicComp()
        {
        }

        [MethodImpl(0x8000)]
        IAllianceCompChatBase IAllianceCompOwnerBase.GetChatComp()
        {
        }

        [MethodImpl(0x8000)]
        IAllianceCompInviteBase IAllianceCompOwnerBase.GetInviteComp()
        {
        }

        [MethodImpl(0x8000)]
        IAllianceCompMailBase IAllianceCompOwnerBase.GetMailComp()
        {
        }

        [MethodImpl(0x8000)]
        IAllianceCompMembersBase IAllianceCompOwnerBase.GetMembersComp()
        {
        }

        [MethodImpl(0x8000)]
        public AllianceBasicInfo GetAllianceBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceInviteInfo> GetAllianceInviteList()
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceMemberInfo> GetAllianceMemberList()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool InitAllComps()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Initlize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PostInitlialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(DateTime currTime)
        {
        }
    }
}

