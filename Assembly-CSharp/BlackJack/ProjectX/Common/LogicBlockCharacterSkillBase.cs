﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCharacterSkillBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected ICharacterSkillDataContainer m_dc;
        protected LogicBlockCharacterBase m_lbCharacterBase;
        protected LogicBlockCharacterBasicPropertiesBase m_lbBasicProperties;
        protected List<LBPassiveSkill> m_passiveSkillList;
        protected LBBuffContainer m_bufContainer;
        [CompilerGenerated]
        private static Predicate<LBPassiveSkill> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_RemoveActiveBuf4Skill;
        private static DelegateBridge __Hotfix_ActiveBuf4Skill;
        private static DelegateBridge __Hotfix_GetFreeSkillPoint;
        private static DelegateBridge __Hotfix_GetPassiveSkillList;
        private static DelegateBridge __Hotfix_GetPassiveSkillCountWithoutDrivingLicense;
        private static DelegateBridge __Hotfix_GetPassiveSkill_1;
        private static DelegateBridge __Hotfix_GetPassiveSkill_0;
        private static DelegateBridge __Hotfix_CheckLearnSkill;
        private static DelegateBridge __Hotfix_LearnSkillImpl;
        private static DelegateBridge __Hotfix_LearnSkillForDrivingLicense;
        private static DelegateBridge __Hotfix_CheckForgetSkill;
        private static DelegateBridge __Hotfix_ForgetSkill;
        private static DelegateBridge __Hotfix_ClearAllPassiveSkill;
        private static DelegateBridge __Hotfix_RemoveLostDependentPassiveSkill;
        private static DelegateBridge __Hotfix_GetBufContainer;
        private static DelegateBridge __Hotfix_GetTotalBindMoneyCost;
        private static DelegateBridge __Hotfix_ClearAllSkillBuf;
        private static DelegateBridge __Hotfix_IsSkillLostDepend;
        private static DelegateBridge __Hotfix_RemoveSkillorLevel;
        private static DelegateBridge __Hotfix_AddNewSkillToPassiveSkillList;
        private static DelegateBridge __Hotfix_OnAddNewSkillToPassiveSkillList;
        private static DelegateBridge __Hotfix_RemoveSkillFromPassiveSkillList;
        private static DelegateBridge __Hotfix_OnRemoveSkillFromPassiveSkillList;
        private static DelegateBridge __Hotfix_GetCharacterEvaluateScore;

        [MethodImpl(0x8000)]
        protected void ActiveBuf4Skill(LBPassiveSkill lbPassiveSkill)
        {
        }

        [MethodImpl(0x8000)]
        protected void AddNewSkillToPassiveSkillList(LBPassiveSkill newSkill)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForgetSkill(int skillId, out LBPassiveSkill passiveSkill, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckLearnSkill(int skillId, int level, out int bindMoneyCost, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearAllPassiveSkill()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearAllSkillBuf()
        {
        }

        [MethodImpl(0x8000)]
        public bool ForgetSkill(int skillId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public LBBuffContainer GetBufContainer()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCharacterEvaluateScore()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFreeSkillPoint()
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill GetPassiveSkill(int skillId)
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill GetPassiveSkill(int skillId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public int GetPassiveSkillCountWithoutDrivingLicense()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPassiveSkill> GetPassiveSkillList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTotalBindMoneyCost()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSkillLostDepend(LBPassiveSkill skill)
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill LearnSkillForDrivingLicense(int skillId, int level, bool isExecuteForInit = false)
        {
        }

        [MethodImpl(0x8000)]
        protected LBPassiveSkill LearnSkillImpl(int skillId, int level, bool isExecuteForInit = false)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAddNewSkillToPassiveSkillList(LBPassiveSkill newSkill)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnRemoveSkillFromPassiveSkillList(LBPassiveSkill skill)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveActiveBuf4Skill(LBPassiveSkill lbPassiveSkill)
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveLostDependentPassiveSkill()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveSkillFromPassiveSkillList(LBPassiveSkill skill)
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveSkillorLevel(LBPassiveSkill skill)
        {
        }
    }
}

