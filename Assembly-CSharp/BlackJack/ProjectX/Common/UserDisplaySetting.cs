﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class UserDisplaySetting
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <LowFrameRateLimit>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <ShipDisplayState>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <SpecialEffectState>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <ShadowDisplayState>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <DamageTextDisplayState>k__BackingField;
        private BlackJack.ProjectX.Common.GraphicQualityLevel m_graphicQualityLevel;
        private BlackJack.ProjectX.Common.GraphicEffectLevel m_graphicEffectLevel;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_get_LowFrameRateLimit;
        private static DelegateBridge __Hotfix_set_LowFrameRateLimit;
        private static DelegateBridge __Hotfix_get_ShipDisplayState;
        private static DelegateBridge __Hotfix_set_ShipDisplayState;
        private static DelegateBridge __Hotfix_get_SpecialEffectState;
        private static DelegateBridge __Hotfix_set_SpecialEffectState;
        private static DelegateBridge __Hotfix_get_ShadowDisplayState;
        private static DelegateBridge __Hotfix_set_ShadowDisplayState;
        private static DelegateBridge __Hotfix_get_DamageTextDisplayState;
        private static DelegateBridge __Hotfix_set_DamageTextDisplayState;
        private static DelegateBridge __Hotfix_get_GraphicQualityLevel;
        private static DelegateBridge __Hotfix_set_GraphicQualityLevel;
        private static DelegateBridge __Hotfix_get_GraphicEffectLevel;
        private static DelegateBridge __Hotfix_set_GraphicEffectLevel;

        [MethodImpl(0x8000)]
        public void Reset()
        {
        }

        public bool LowFrameRateLimit
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool ShipDisplayState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool SpecialEffectState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool ShadowDisplayState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool DamageTextDisplayState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public BlackJack.ProjectX.Common.GraphicQualityLevel GraphicQualityLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public BlackJack.ProjectX.Common.GraphicEffectLevel GraphicEffectLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

