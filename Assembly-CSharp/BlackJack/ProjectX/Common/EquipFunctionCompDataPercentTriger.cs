﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class EquipFunctionCompDataPercentTriger : EquipFunctionCompTrigerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEnterTriger;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLeaveTriger;
        protected readonly float m_percent;
        protected readonly bool m_lessEqual;
        protected bool m_isInTriger;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_EnterTriger;
        private static DelegateBridge __Hotfix_LeaveTriger;
        private static DelegateBridge __Hotfix_add_EventOnEnterTriger;
        private static DelegateBridge __Hotfix_remove_EventOnEnterTriger;
        private static DelegateBridge __Hotfix_add_EventOnLeaveTriger;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveTriger;

        public event Action EventOnEnterTriger
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLeaveTriger
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected EquipFunctionCompDataPercentTriger(IEquipFunctionCompOwner owner, float percent, bool lessEqual = true)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnterTriger()
        {
        }

        protected abstract float GetCurrPercent();
        [MethodImpl(0x8000)]
        protected void LeaveTriger()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint currTime)
        {
        }
    }
}

