﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildBattleShipLostStatInfo
    {
        public GuildBattleGroupType m_groupType;
        public int m_shipConfigId;
        public int m_selfLostCount;
        public double m_selfLostBindMoney;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

