﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompStaticWeaponEquipBase
    {
        protected LBStaticWeaponEquipSlotGroup m_normalAttackGroup;
        protected LBStaticWeaponEquipSlotGroup m_staticDefaultBoosterGroup;
        protected LBStaticWeaponEquipSlotGroup m_staticDefaultRepairerGroup;
        protected List<LBStaticWeaponEquipSlotGroup> m_highSlotGroupList;
        protected List<LBStaticWeaponEquipSlotGroup> m_middleSlotGroupList;
        protected List<LBStaticWeaponEquipSlotGroup> m_lowSlotGroupList;
        protected List<LBStaticWeaponEquipSlotGroup> m_allSlotGroupList;
        protected ConfigDataSuperWeaponInfo m_superWeaponConf;
        protected ConfigDataShipSuperEquipInfo m_superEquipConf;
        protected ConfigDataShipTacticalEquipInfo m_tacticalEquipConf;
        protected ILBStaticShip m_ownerShip;
        protected IShipDataContainer m_shipDC;
        protected LogicBlockShipCompItemStoreBase m_lbShipStore;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_DestoryAllShipEquip;
        private static DelegateBridge __Hotfix_CalcAllWeaponFireRangeMax;
        private static DelegateBridge __Hotfix_CalcAllWeaponFireAverageRange;
        private static DelegateBridge __Hotfix_CalcAllWeaponGroupDPS;
        private static DelegateBridge __Hotfix_CalcAllRestoreShieldFromActiveRepairerEfficiency;
        private static DelegateBridge __Hotfix_CalcAllRestoreArmorFromActiveRepairerEfficiency;
        private static DelegateBridge __Hotfix_CalcAllEnergyCostEfficiency;
        private static DelegateBridge __Hotfix_CalcShieldRepairSpeed;
        private static DelegateBridge __Hotfix_GetAllWeaponEquipSlotGroups;
        private static DelegateBridge __Hotfix_GetNormalAttackGroup;
        private static DelegateBridge __Hotfix_GetHighSlotGroupCount;
        private static DelegateBridge __Hotfix_GetHighSlotGroup;
        private static DelegateBridge __Hotfix_GetMiddleSlotGroupCount;
        private static DelegateBridge __Hotfix_GetMiddleSlotGroup;
        private static DelegateBridge __Hotfix_GetLowSlotGroupCount;
        private static DelegateBridge __Hotfix_GetLowSlotGroup;
        private static DelegateBridge __Hotfix_GetStaticDefaultRepairerGroup;
        private static DelegateBridge __Hotfix_GetStaticDefaultBoosterGroup;
        private static DelegateBridge __Hotfix_GetSuperWeaponConf;
        private static DelegateBridge __Hotfix_GetSuperEquipConf;
        private static DelegateBridge __Hotfix_GetShipTacticalEquipConf;
        private static DelegateBridge __Hotfix_GetHighSlotGroups;
        private static DelegateBridge __Hotfix_GetMiddleSlotGroups;
        private static DelegateBridge __Hotfix_GetLowSlotGroups;
        private static DelegateBridge __Hotfix_InitStaticWeaponEquipSlotGroups;
        private static DelegateBridge __Hotfix_InitFakeInSpaceWeaponEquipGroups;
        private static DelegateBridge __Hotfix_InitAllSlotGroupCache;

        [MethodImpl(0x8000)]
        public float CalcAllEnergyCostEfficiency()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcAllRestoreArmorFromActiveRepairerEfficiency(float armorMulit)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcAllRestoreShieldFromActiveRepairerEfficiency(float shiledMulit)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcAllWeaponFireAverageRange()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcAllWeaponFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcAllWeaponGroupDPS()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcShieldRepairSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void DestoryAllShipEquip(OperateLogItemChangeType operateLogCauseId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> GetAllWeaponEquipSlotGroups()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetHighSlotGroup(int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHighSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> GetHighSlotGroups()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetLowSlotGroup(int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        public int GetLowSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> GetLowSlotGroups()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetMiddleSlotGroup(int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        public int GetMiddleSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> GetMiddleSlotGroups()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetNormalAttackGroup()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipTacticalEquipInfo GetShipTacticalEquipConf()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetStaticDefaultBoosterGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetStaticDefaultRepairerGroup()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipInfo GetSuperEquipConf()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSuperWeaponInfo GetSuperWeaponConf()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitAllSlotGroupCache()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitFakeInSpaceWeaponEquipGroups()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBStaticShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitStaticWeaponEquipSlotGroups()
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }
    }
}

