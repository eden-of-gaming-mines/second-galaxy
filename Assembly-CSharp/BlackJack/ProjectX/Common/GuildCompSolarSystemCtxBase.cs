﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompSolarSystemCtxBase : GuildCompBase, IGuildCompSolarSystemCtxBase, IGuildSolarSystemCtxBase
    {
        protected Dictionary<int, GuildBuildingSolarSystemEffectInfo> m_guildBuildingEffectDic;
        protected List<GuildSolarSystemInfo> m_solarSystemCtxList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initlialize;
        private static DelegateBridge __Hotfix_GetOccupiedSolarSystemList;
        private static DelegateBridge __Hotfix_GetGuildBuildingInfoList;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetGuildBuildingInfo_0;
        private static DelegateBridge __Hotfix_GetFlourishLevel;
        private static DelegateBridge __Hotfix_CheckBuildingSentryWorking;
        private static DelegateBridge __Hotfix_GetGuildBuildingSolarSystemEffectInfo;
        private static DelegateBridge __Hotfix_CheckGuildBuildingDeployUpgradeWithoutPermission;
        private static DelegateBridge __Hotfix_CheckGuildBuildingRecycleWithoutPermission;
        private static DelegateBridge __Hotfix_IsSolarSystemOccupied;
        private static DelegateBridge __Hotfix_GetSolarSystemFlourishValue;
        private static DelegateBridge __Hotfix_CalcDailyIncomeInformationPoint;
        private static DelegateBridge __Hotfix_ReCalcGuildBuildingSolarSystemEffect;
        private static DelegateBridge __Hotfix_GetGuildBuildingInfo_1;
        private static DelegateBridge __Hotfix_GetGuildBuildingInfo_2;
        private static DelegateBridge __Hotfix_CheckMemberPermission;
        private static DelegateBridge __Hotfix_CheckBuildingOperationQueueCount;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompSolarSystemCtxBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcDailyIncomeInformationPoint()
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckBuildingOperationQueueCount()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckBuildingSentryWorking(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildBuildingDeployUpgradeWithoutPermission(GuildBuildingDeployUpgradeReq req, out int errCode, out GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildBuildingRecycleWithoutPermission(GuildBuildingRecycleReq req, out int errCode, out GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool CheckMemberPermission(string gameUserId, GuildPermission permission, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFlourishLevel(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo GetGuildBuildingInfo(ulong instanceId, bool containsLost)
        {
        }

        [MethodImpl(0x8000)]
        protected GuildBuildingInfo GetGuildBuildingInfo(int solarSystemId, GuildBuildingType type, bool containsLost)
        {
        }

        [MethodImpl(0x8000)]
        protected GuildBuildingInfo GetGuildBuildingInfo(int solarSystemId, ulong instanceId, bool containsLost)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBuildingInfo> GetGuildBuildingInfoList(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingSolarSystemEffectInfo GetGuildBuildingSolarSystemEffectInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfo GetGuildSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemInfo> GetOccupiedSolarSystemList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemFlourishValue(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initlialize()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemOccupied(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void ReCalcGuildBuildingSolarSystemEffect(int solarSystemId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

