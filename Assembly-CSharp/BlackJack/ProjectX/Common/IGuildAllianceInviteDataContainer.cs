﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildAllianceInviteDataContainer
    {
        void AddAllianceInvite(GuildAllianceInviteInfo invite);
        GuildAllianceInviteInfo GetAllianceInvite(ulong instanceId);
        List<GuildAllianceInviteInfo> GetAllianceInviteList();
        void RemoveAllianceInvite(ulong instanceId);
    }
}

