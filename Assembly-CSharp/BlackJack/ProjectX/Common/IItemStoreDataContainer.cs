﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface IItemStoreDataContainer
    {
        StoreItemInfo CreateEmptyStoreItemInfo();
        Dictionary<StoreItemType, Dictionary<int, DateTime>> GetAllFreezingItem();
        DateTime GetItemFreezingTime(StoreItemType itemType, int itemId);
        StoreItemInfo GetStoreItemInfoByIndex(int index);
        List<StoreItemInfo> GetStoreItemInfoList();
        void RemoveItemFreezingTime(StoreItemType itemType, int itemId);
        void UpdateItemFreezingTime(StoreItemType itemType, int itemId, DateTime itemFreezingTime);
        void UpdateStoreItemInfo(int itemIndex, bool isDeleted);
        void UpdateStoreItemInfo(int itemIndex, long count);
        void UpdateStoreItemInfo(int itemIndex, uint flag);
        void UpdateStoreItemInfo(int itemIndex, ItemInfo itemInfo, bool isDeleted, uint flag);
    }
}

