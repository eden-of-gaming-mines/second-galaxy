﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockRechargeMonthlyCardClient : LogicBlockRechargeMonthlyCardBase
    {
        private Func<int, bool> m_rechargeMonthlyCardFilter;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnRechargeMonthlyCardDeliver;
        private static DelegateBridge __Hotfix_OnRechargeMonthlyCardBuy;
        private static DelegateBridge __Hotfix_OnRechargeMonthlyCardBuyCancel;
        private static DelegateBridge __Hotfix_OnRechargeMonthlyCardDailyRewardReceive;
        private static DelegateBridge __Hotfix_RefreshAllRechargeMonthlyCards;
        private static DelegateBridge __Hotfix_RechargeMonthlyCardSetExpired;
        private static DelegateBridge __Hotfix_GetRechargeMonthlyCardList;
        private static DelegateBridge __Hotfix_IsRechargeMonthlyCardBought;
        private static DelegateBridge __Hotfix_IsRechargeMonthlyCardNearlyExpireTime;
        private static DelegateBridge __Hotfix_UpdateMonthlyCard;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public List<LBRechargeMonthlyCard> GetRechargeMonthlyCardList(Func<LBRechargeMonthlyCard, bool> filter = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx, Func<int, bool> rechargeMonthlyCardFilter)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRechargeMonthlyCardBought(LBRechargeMonthlyCard monthCard)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRechargeMonthlyCardNearlyExpireTime(LBRechargeMonthlyCard monthCard)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeMonthlyCardBuy(RechargeOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeMonthlyCardBuyCancel(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeMonthlyCardDailyRewardReceive(int monthlyCardId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeMonthlyCardDeliver(int monthlyCardId, DateTime newExpireTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RechargeMonthlyCardSetExpired(int monthlyCardId)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllRechargeMonthlyCards(List<RechargeMonthlyCardInfo> monthlyCardList, int version)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMonthlyCard(RechargeMonthlyCardInfo monthlyCard)
        {
        }

        private IRechargeMonthlyCardDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

