﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBLinkInfo")]
    public class GDBLinkInfo : IExtensible
    {
        private int _FromStarfieldID;
        private int _FromStargroupID;
        private int _FromSolarSystemID;
        private int _FromStargateID;
        private int _ToStarfieldID;
        private int _ToStargroupID;
        private int _ToSolarSystemID;
        private int _ToStargateID;
        private bool _isWormLink;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FromStarfieldID;
        private static DelegateBridge __Hotfix_set_FromStarfieldID;
        private static DelegateBridge __Hotfix_get_FromStargroupID;
        private static DelegateBridge __Hotfix_set_FromStargroupID;
        private static DelegateBridge __Hotfix_get_FromSolarSystemID;
        private static DelegateBridge __Hotfix_set_FromSolarSystemID;
        private static DelegateBridge __Hotfix_get_FromStargateID;
        private static DelegateBridge __Hotfix_set_FromStargateID;
        private static DelegateBridge __Hotfix_get_ToStarfieldID;
        private static DelegateBridge __Hotfix_set_ToStarfieldID;
        private static DelegateBridge __Hotfix_get_ToStargroupID;
        private static DelegateBridge __Hotfix_set_ToStargroupID;
        private static DelegateBridge __Hotfix_get_ToSolarSystemID;
        private static DelegateBridge __Hotfix_set_ToSolarSystemID;
        private static DelegateBridge __Hotfix_get_ToStargateID;
        private static DelegateBridge __Hotfix_set_ToStargateID;
        private static DelegateBridge __Hotfix_get_IsWormLink;
        private static DelegateBridge __Hotfix_set_IsWormLink;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="FromStarfieldID", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FromStarfieldID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="FromStargroupID", DataFormat=DataFormat.TwosComplement)]
        public int FromStargroupID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="FromSolarSystemID", DataFormat=DataFormat.TwosComplement)]
        public int FromSolarSystemID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FromStargateID", DataFormat=DataFormat.TwosComplement)]
        public int FromStargateID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ToStarfieldID", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ToStarfieldID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="ToStargroupID", DataFormat=DataFormat.TwosComplement)]
        public int ToStargroupID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="ToSolarSystemID", DataFormat=DataFormat.TwosComplement)]
        public int ToSolarSystemID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ToStargateID", DataFormat=DataFormat.TwosComplement)]
        public int ToStargateID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="isWormLink", DataFormat=DataFormat.Default)]
        public bool IsWormLink
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

