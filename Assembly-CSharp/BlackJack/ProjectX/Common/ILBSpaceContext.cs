﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface ILBSpaceContext : ILBSpaceProcessContainer
    {
        void EnableAccurateAOI4Ship(ILBInSpaceShip m_ownerShip, bool enable);
        IConfigDataLoader GetConfigDataLoader();
        int GetCurrentSolarSystemId();
        DateTime GetCurrServerTime();
        uint GetCurrTime();
        uint GetPlayerTeamIdByGameUserId(string gameUserId);
        IEnumerable<KeyValuePair<ulong, ILBInSpacePlayerShip>> GetPlayerTeamMambersByTeamId(uint teamInstanceId);
        Random GetRandom();
        float GetSecurityLevel();
        ILBSpaceDropBox GetSpaceDropBoxById(uint objId);
        ILBSpaceTarget GetSpaceTargetById(uint objId);
        uint GetTickSeq();
        bool IsAntiTeleportEffectActivated();
        bool IsPVPEnable();
        float NextRandom();
        void RemoveShipOnTickEnd(ILBInSpaceShip ship);
    }
}

