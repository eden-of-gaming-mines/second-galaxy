﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBMoonInfo")]
    public class GDBMoonInfo : IExtensible
    {
        private int _Id;
        private string _name;
        private double _locationX;
        private double _locationY;
        private double _locationZ;
        private uint _radius;
        private ulong _orbitRadius;
        private double _orbitalPeriod;
        private float _eccentricity;
        private double _mass;
        private float _gravity;
        private float _escapeVelocity;
        private uint _temperature;
        private uint _artResourcesId;
        private bool _isNeedLocalization;
        private string _localizationKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_LocationX;
        private static DelegateBridge __Hotfix_set_LocationX;
        private static DelegateBridge __Hotfix_get_LocationY;
        private static DelegateBridge __Hotfix_set_LocationY;
        private static DelegateBridge __Hotfix_get_LocationZ;
        private static DelegateBridge __Hotfix_set_LocationZ;
        private static DelegateBridge __Hotfix_get_Radius;
        private static DelegateBridge __Hotfix_set_Radius;
        private static DelegateBridge __Hotfix_get_OrbitRadius;
        private static DelegateBridge __Hotfix_set_OrbitRadius;
        private static DelegateBridge __Hotfix_get_OrbitalPeriod;
        private static DelegateBridge __Hotfix_set_OrbitalPeriod;
        private static DelegateBridge __Hotfix_get_Eccentricity;
        private static DelegateBridge __Hotfix_set_Eccentricity;
        private static DelegateBridge __Hotfix_get_Mass;
        private static DelegateBridge __Hotfix_set_Mass;
        private static DelegateBridge __Hotfix_get_Gravity;
        private static DelegateBridge __Hotfix_set_Gravity;
        private static DelegateBridge __Hotfix_get_EscapeVelocity;
        private static DelegateBridge __Hotfix_set_EscapeVelocity;
        private static DelegateBridge __Hotfix_get_Temperature;
        private static DelegateBridge __Hotfix_set_Temperature;
        private static DelegateBridge __Hotfix_get_ArtResourcesId;
        private static DelegateBridge __Hotfix_set_ArtResourcesId;
        private static DelegateBridge __Hotfix_get_IsNeedLocalization;
        private static DelegateBridge __Hotfix_set_IsNeedLocalization;
        private static DelegateBridge __Hotfix_get_LocalizationKey;
        private static DelegateBridge __Hotfix_set_LocalizationKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="name", DataFormat=DataFormat.Default), DefaultValue("")]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="locationX", DataFormat=DataFormat.TwosComplement)]
        public double LocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="locationY", DataFormat=DataFormat.TwosComplement)]
        public double LocationY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="locationZ", DataFormat=DataFormat.TwosComplement)]
        public double LocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="radius", DataFormat=DataFormat.TwosComplement)]
        public uint Radius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="orbitRadius", DataFormat=DataFormat.TwosComplement)]
        public ulong OrbitRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="orbitalPeriod", DataFormat=DataFormat.TwosComplement)]
        public double OrbitalPeriod
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="eccentricity", DataFormat=DataFormat.FixedSize)]
        public float Eccentricity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="mass", DataFormat=DataFormat.TwosComplement)]
        public double Mass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="gravity", DataFormat=DataFormat.FixedSize)]
        public float Gravity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="escapeVelocity", DataFormat=DataFormat.FixedSize)]
        public float EscapeVelocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="temperature", DataFormat=DataFormat.TwosComplement)]
        public uint Temperature
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="artResourcesId", DataFormat=DataFormat.TwosComplement)]
        public uint ArtResourcesId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="isNeedLocalization", DataFormat=DataFormat.Default)]
        public bool IsNeedLocalization
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="localizationKey", DataFormat=DataFormat.Default), DefaultValue("")]
        public string LocalizationKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

