﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILogicBlockShipCompSpaceObjectClient : ILogicBlockShipCompSpaceObject, ILogicBlockObjCompSpaceObject
    {
        void OnSyncEventBlinkInterrupted(int reason);
        void OnSyncEventStartJumping(bool isJumpingDisturbed);
    }
}

