﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompWeaponEquipSetupEnv4FlagShip : LogicBlockShipCompWeaponEquipSetupEnv
    {
        protected ILBStaticFlagShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsWeaponEquipSlotGroupOptAvaiable;
        private static DelegateBridge __Hotfix_IsHighSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsMiddleSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsLowSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsDependTechReadyForWeaponEquip;
        private static DelegateBridge __Hotfix_CheckItemStoreCapacityForSlotItem;
        private static DelegateBridge __Hotfix_CheckItemStoreCapacityForAllSlotItems;

        [MethodImpl(0x8000)]
        public override bool CheckItemStoreCapacityForAllSlotItems()
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckItemStoreCapacityForSlotItem(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsDependTechReadyForWeaponEquip(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHighSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsLowSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsMiddleSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsWeaponEquipSlotGroupOptAvaiable(LBStaticWeaponEquipSlotGroup group)
        {
        }
    }
}

