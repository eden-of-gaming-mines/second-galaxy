﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSyncEventLaserLaunch : LBSyncEvent
    {
        public uint m_destObjectId;
        public LBSpaceProcessLaserLaunch m_process;
        public uint m_weaponGroupCDEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

