﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GuildBattleStatus
    {
        Prepare,
        FirstHalf,
        Reinforcement,
        SecondHalf,
        Declaration,
        BattleEnd
    }
}

