﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IFleetListDataContainer
    {
        void CreateFleet(string name);
        void DeleteFleet(int fleetIndex);
        FleetInfo GetFleetByIndex(int fleetIndex);
        List<FleetInfo> GetFleetList();
        void ModifyFleet(int fleetIndex, List<ulong> shipInstanceIdList);
    }
}

