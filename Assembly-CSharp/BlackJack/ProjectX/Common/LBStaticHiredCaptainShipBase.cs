﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBStaticHiredCaptainShipBase : ILBStaticHiredCaptainShip, ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        protected ILBPlayerContext m_playerCtx;
        protected ILBShipCaptain m_caption;
        protected IStaticHiredCaptainShipDataContainer m_hiredCaptainShipDC;
        protected ConfigDataNpcShipInfo m_npcShipConf;
        private ConfigDataSpaceShipInfo m_spaceShipConf;
        protected LogicBlockShipCompStaticWeaponEquipBase m_lbCompWeaponEquip;
        protected LogicBlockShipCompStaticPropertiesCalc m_lbCompPropertiesCalc;
        protected LogicBlockShipCompItemStoreBase m_lbCompItemStore;
        protected LogicBlockShipCompStaticBasicCtx m_lbCompBasicCtx;
        protected LogicBlockShipCompStaticCaptain m_lbCaptain;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UnInitialize;
        private static DelegateBridge __Hotfix_GetLBStaticBasicCtx;
        private static DelegateBridge __Hotfix_GetLBCaptain;
        private static DelegateBridge __Hotfix_GetLBWeaponEquip;
        private static DelegateBridge __Hotfix_GetLBPropertiesCalc;
        private static DelegateBridge __Hotfix_GetLBPlayerContext;
        private static DelegateBridge __Hotfix_GetShipDataContainer;
        private static DelegateBridge __Hotfix_GetShipConfInfo;
        private static DelegateBridge __Hotfix_GetDefaultAroundRadius;
        private static DelegateBridge __Hotfix_GetBufFactory;
        private static DelegateBridge __Hotfix_GetShipItemStoreDataContainer;
        private static DelegateBridge __Hotfix_GetLBShipItemStore;

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptainShipBase(IStaticHiredCaptainShipDataContainer dc, ILBShipCaptain captain, ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public ILBBuffFactory GetBufFactory()
        {
        }

        [MethodImpl(0x8000)]
        public float GetDefaultAroundRadius(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticCaptain GetLBCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public ILBPlayerContext GetLBPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCompPropertiesCalc GetLBPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompItemStoreBase GetLBShipItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtx GetLBStaticBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticWeaponEquipBase GetLBWeaponEquip()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetShipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public IShipDataContainer GetShipDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public IShipItemStoreDataContainer GetShipItemStoreDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public void UnInitialize()
        {
        }
    }
}

