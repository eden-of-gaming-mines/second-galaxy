﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct SolarSystemInfectInfo
    {
        public int m_solarSystemId;
        public float m_progress;
        public int m_finalBattleSceneId;
    }
}

