﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ServerStringTableProviderDefault : IStringTableProvider
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IStringTableProvider.GetStringInDefaultStringTable;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IStringTableProvider.GetStringInGDBStringTable;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IStringTableProvider.GetStringInStoryStringTable;

        [MethodImpl(0x8000)]
        string IStringTableProvider.GetStringInDefaultStringTable(string key)
        {
        }

        [MethodImpl(0x8000)]
        string IStringTableProvider.GetStringInGDBStringTable(string key)
        {
        }

        [MethodImpl(0x8000)]
        string IStringTableProvider.GetStringInStoryStringTable(string key)
        {
        }
    }
}

