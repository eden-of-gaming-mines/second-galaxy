﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IUserGuideDataContainer
    {
        bool IsStepGroupAlreadyCompleted(int groupId);
        void ResetGroup2NotComplete(int groupId);
        void SaveGroup4Completed(int currGroup);
    }
}

