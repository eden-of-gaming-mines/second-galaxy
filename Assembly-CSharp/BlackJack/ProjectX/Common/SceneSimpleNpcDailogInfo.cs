﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class SceneSimpleNpcDailogInfo
    {
        public int m_startDialogId;
        public int m_endDialogId;
        public SceneMulticastType m_multicastType;
        public uint m_watchTargetObjId;
        public NpcDNId m_replaceNpc;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

