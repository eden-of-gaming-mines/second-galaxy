﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBPlayerContextLBProvider
    {
        LogicBlockActivityBase GetLBActivity();
        LogicBlockAuctionBase GetLBAuction();
        LogicBlockBattlePassBase GetLBBattlePass();
        LogicBlockBlackMarketBase GetLBBlackMarket();
        LogicBlockBranchStoryBase GetLBBranchStory();
        LogicBlockCelestialScanBase GetLBCelestialScan();
        LogicBlockCharacterBase GetLBCharacter();
        LogicBlockCharacterBasicPropertiesBase GetLBCharacterBasicProperties();
        LogicBlockCharacterChipBase GetLBCharacterChip();
        LogicBlockCharacterFactionBase GetLBCharacterFaction();
        LogicBlockCharacterPVPBase GetLBCharacterPVP();
        LogicBlockCharacterSkillBase GetLBCharacterSkill();
        LogicBlockChatBase GetLBChat();
        LogicBlockCrackBase GetLBCrack();
        LogicBlockCustomizedParameterBase GetLBCustomizedParameter();
        LogicBlockDelegateMissionBase GetLBDelegateMission();
        LogicBlockDevelopmentBase GetLBDevelopment();
        LogicBlockDiplomacyBase GetLBDiplomacy();
        LogicBlockDrivingLicenseBase GetLBDrivingLicense();
        LogicBlockFactionCreditQuestBase GetLBFactionCreditQuest();
        LogicBlockFleetList GetLBFleet();
        LogicBlockFunctionOpenStateBase GetLBFunctionOpenState();
        LogicBlockGuildBase GetLBGuild();
        LogicBlockHiredCaptainManagementBase GetLBHiredCaptainManagement();
        LogicBlockItemStoreBase GetLBItemStore();
        LogicBlockKillRecordBase GetLBKillRecord();
        LogicBlockLoginActivityBase GetLBLoginActivity();
        LogicBlockMailBase GetLBMail();
        LogicBlockMotherShipBase GetLBMotherShipBasic();
        LogicBlockNavigationBase GetLBNavigation();
        LogicBlockNpcShopBase GetLBNpcShop();
        LogicBlockProduceBase GetLBProduce();
        LogicBlockPVPInvadeRescueBase GetLBPVPInvadeRescue();
        LogicBlockQuestBase GetLBQuest();
        LogicBlockRechargeGiftPackageBase GetLBRechargeGiftPackage();
        LogicBlockRechargeItemBase GetLBRechargeItem();
        LogicBlockRechargeMonthlyCardBase GetLBRechargeMonthlyCard();
        LogicBlockRechargeOrderBase GetLBRechargeOrder();
        LogicBlockShipCustomTemplateBase GetLBShipCustomTemplate();
        LogicBlockShipHangarsBase GetLBShipHangars();
        LogicBlockShipSalvageBase GetLBShipSalvage();
        LogicBlockSignalBase GetLBSignal();
        LogicBlockSolarSystemInfoBase GetLBSolarSystemInfoBase();
        LogicBlockSpaceSignalBase GetLBSpaceSignal();
        LogicBlockStationContextBase GetLBStationContext();
        LogicBlockTeamBase GetLBTeam();
        LogicBlockTechBase GetLBTech();
        LogicBlockTradeBase GetLBTrade();
        LogicBlockUserGuideBase GetLBUserGuide();
    }
}

