﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBShipStoreItemBase
    {
        protected ShipStoreItemInfo m_shipStoreItemInfo;
        protected object m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetStoreIndex;
        private static DelegateBridge __Hotfix_GetShipStoreItemInfo;
        private static DelegateBridge __Hotfix_GetItemType;
        private static DelegateBridge __Hotfix_GetConfigId;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_IsBind;
        private static DelegateBridge __Hotfix_IsFreezing;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetItemInfo;
        private static DelegateBridge __Hotfix_AddItemCount;
        private static DelegateBridge __Hotfix_ReduceItemCount;
        private static DelegateBridge __Hotfix_SetItemCount;
        private static DelegateBridge __Hotfix_FreezingItemTimeout;
        private static DelegateBridge __Hotfix_GetItemConf;

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase(ShipStoreItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AddItemCount(long addCount)
        {
        }

        [MethodImpl(0x8000)]
        public void FreezingItemTimeout()
        {
        }

        [MethodImpl(0x8000)]
        public int GetConfigId()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public T GetItemConf<T>() where T: class
        {
        }

        [MethodImpl(0x8000)]
        public long GetItemCount()
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo GetItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemType GetItemType()
        {
        }

        [MethodImpl(0x8000)]
        public ShipStoreItemInfo GetShipStoreItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetStoreIndex()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBind()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFreezing()
        {
        }

        [MethodImpl(0x8000)]
        public void ReduceItemCount(long reduceCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemCount(long count)
        {
        }
    }
}

