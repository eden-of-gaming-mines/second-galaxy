﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ToolUtil;
    using System;

    public interface IDrivingLicenseDataContainer
    {
        BitArrayEx GetRewardState();
        DrivingLicenseUpgradeInfo GetUpgradeInfo();
        void UpdateRewardState(int id);
        void UpdateUpgradeInfo(DrivingLicenseUpgradeInfo info);
    }
}

