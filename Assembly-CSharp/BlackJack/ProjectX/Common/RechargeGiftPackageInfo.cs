﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class RechargeGiftPackageInfo
    {
        public int m_id;
        public GiftPackageCategory m_category;
        public GiftPackageBuyCountLimitType m_buyCountLimitType;
        public int m_currCycleBuyCountLimit;
        public int m_currCycleBuyCount;
        public DateTime m_nextRefreshTime;
        public DateTime m_expireTime;
        public bool m_isTriggered;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

