﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Flags]
    public enum SceneInteractionFlag
    {
        None = 0,
        DisableInFight = 1,
        OnlySceneOwner = 2,
        AutoClear = 4,
        AutoStopShip = 8,
        Default = 15,
        TalkDefault = 11
    }
}

