﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildTradeTransportInfo
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public StarMapGuildSimpleInfo m_guildInfo;
        public int m_tradePortLevel;
        public int m_configId;
        public StoreItemType m_itemType;
        public int m_count;
        public long m_price;
        public ulong m_purchaseInstanceId;
        public int m_purchaseSolarSystemId;
        public StarMapGuildSimpleInfo m_purchaseGuildInfo;
        public DateTime m_startTime;
        public int m_currJumpDist;
        public int m_totalJumpDist;
        public int m_currSolarSystem;
        public float m_currShipShieldPercent;
        public float m_currShipArmorPercent;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

