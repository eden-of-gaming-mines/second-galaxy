﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IGuildBasicLogicBase
    {
        GuildBasicInfo GetBasicInfo();
        string GetGuildCodeName();
        uint GetGuildId();
        GuildAllianceLanguageType GetGuildLanguageType();
        string GetGuildName();
        StarMapAllianceSimpleInfo GetStarMapAllianceSimpleInfo();
        StarMapGuildSimpleInfo GetStarMapGuildSimpleInfo();
    }
}

