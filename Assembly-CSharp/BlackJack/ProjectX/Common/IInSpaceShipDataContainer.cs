﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IInSpaceShipDataContainer : IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        void AddGuildSolarSystemBuf(List<int> bufList);
        List<object> GetDynamicBufList();
        List<int> GetStaticBufList();
    }
}

