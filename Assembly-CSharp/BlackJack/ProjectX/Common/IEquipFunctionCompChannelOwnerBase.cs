﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IEquipFunctionCompChannelOwnerBase : IEquipFunctionCompOwner
    {
        float CalcFireRange();
        ushort CalcLoopCD();
        ushort CalcLoopCount();
        float CalcLoopDamageComposeHeat();
        float CalcLoopDamageComposeKinetic();
        float CalcLoopDamageTotal();
        ushort CalcLoopEnergyCost();
        float CalcLoopFireCtrlAccuracy();
        float GetBreakResistFactor();
        int GetChannelingSelfBufId();
        int GetChannelingTargetBufId(int bufSeq);
        int GetChargeBufId();
        float GetEquipTransverseVelocity();
        bool IsChannelBreakable();
    }
}

