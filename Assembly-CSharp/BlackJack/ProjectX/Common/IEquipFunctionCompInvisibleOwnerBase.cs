﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IEquipFunctionCompInvisibleOwnerBase : IEquipFunctionCompOwner
    {
        int GetChargeBufId();
        int GetLoopBufId();
    }
}

