﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ToolUtil;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockDrivingLicenseBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, int, int> EventOnDrivingLicenseUpgradeComplete;
        protected ILBPlayerContext m_playerCtx;
        protected IDrivingLicenseDataContainer m_dc;
        protected LogicBlockCharacterSkillBase m_lbSkill;
        protected LogicBlockCharacterBasicPropertiesBase m_lbBasicProperties;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected LogicBlockCharacterBase m_lbCharacterBase;
        protected List<LBDrivingLicense> m_drivingLicenseList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_ForEachALLDrivingLicenseSkillConf;
        private static DelegateBridge __Hotfix_InitDrivingLicense;
        private static DelegateBridge __Hotfix_CheckDrivingAssessmentStart;
        private static DelegateBridge __Hotfix_UpdateDrivingLicense;
        private static DelegateBridge __Hotfix_UpdateUpgradeInfo;
        private static DelegateBridge __Hotfix_UpdateRewardState;
        private static DelegateBridge __Hotfix_OnDrivingLicenseUpgradeComplete;
        private static DelegateBridge __Hotfix_OnDrivingLicenseLearnComplete;
        private static DelegateBridge __Hotfix_GetDrivingLicenseList;
        private static DelegateBridge __Hotfix_GetDrivingLicense;
        private static DelegateBridge __Hotfix_GetRewardState_1;
        private static DelegateBridge __Hotfix_GetRewardState_0;
        private static DelegateBridge __Hotfix_GetUpgradeInfo;
        private static DelegateBridge __Hotfix_CalcRealMoneyCostByDrivingLicenseUpgradeTime;
        private static DelegateBridge __Hotfix_CalcDrivingLicenseUpgradeSpeedUpByRealMoneyCost;
        private static DelegateBridge __Hotfix_CalcUpgradeCancelReturnBindMoney;
        private static DelegateBridge __Hotfix_add_EventOnDrivingLicenseUpgradeComplete;
        private static DelegateBridge __Hotfix_remove_EventOnDrivingLicenseUpgradeComplete;

        public event Action<int, int, int> EventOnDrivingLicenseUpgradeComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int CalcDrivingLicenseUpgradeSpeedUpByRealMoneyCost(DrivingLicenseUpgradeInfo upgradeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcRealMoneyCostByDrivingLicenseUpgradeTime(double seconds)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcUpgradeCancelReturnBindMoney(DrivingLicenseUpgradeInfo upgradeInfo, int upgradeNeedBindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDrivingAssessmentStart(int skillId, int level, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        private void ForEachALLDrivingLicenseSkillConf(Action<LBPassiveSkill> InitDrivingLicense)
        {
        }

        [MethodImpl(0x8000)]
        public LBDrivingLicense GetDrivingLicense(int drivingLicenseId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBDrivingLicense> GetDrivingLicenseList()
        {
        }

        [MethodImpl(0x8000)]
        public BitArrayEx GetRewardState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetRewardState(int drivingLicenseId)
        {
        }

        [MethodImpl(0x8000)]
        public DrivingLicenseUpgradeInfo GetUpgradeInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDrivingLicense(LBPassiveSkill lbSkill)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDrivingLicenseLearnComplete(int skillId, int level, int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDrivingLicenseUpgradeComplete(int skillId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected bool UpdateDrivingLicense(int skillId, int level, int questId = 0, bool isExecuteForInit = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateRewardState(int drivingLicenseId)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateUpgradeInfo(DrivingLicenseUpgradeInfo info)
        {
        }
    }
}

