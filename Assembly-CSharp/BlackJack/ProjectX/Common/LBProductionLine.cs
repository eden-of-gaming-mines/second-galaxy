﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LBProductionLine
    {
        private IProduceDataContainer m_dc;
        private ProductionLineInfo m_readOnlyInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsIdle;
        private static DelegateBridge __Hotfix_IsProducting;
        private static DelegateBridge __Hotfix_IsProductionComplete;
        private static DelegateBridge __Hotfix_IsCompletedNotified;
        private static DelegateBridge __Hotfix_GetIndex;
        private static DelegateBridge __Hotfix_GetWorkingCaptainInstanceId;
        private static DelegateBridge __Hotfix_GetCostInfoList;
        private static DelegateBridge __Hotfix_GetStartTime;
        private static DelegateBridge __Hotfix_GetEndTime;
        private static DelegateBridge __Hotfix_GetBlueprintId;
        private static DelegateBridge __Hotfix_GetBlueprintCount;
        private static DelegateBridge __Hotfix_GetBlueprintBind;
        private static DelegateBridge __Hotfix_GetReadOnlyProductionLineInfo;
        private static DelegateBridge __Hotfix_Update;

        [MethodImpl(0x8000)]
        public LBProductionLine(ProductionLineInfo info, IProduceDataContainer dc)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetBlueprintBind()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBlueprintCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBlueprintId()
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> GetCostInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetIndex()
        {
        }

        [MethodImpl(0x8000)]
        public ProductionLineInfo GetReadOnlyProductionLineInfo()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetStartTime()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetWorkingCaptainInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCompletedNotified()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsIdle()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsProducting(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsProductionComplete(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void Update(ProductionLineInfo info)
        {
        }
    }
}

