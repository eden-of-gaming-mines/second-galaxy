﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildProductionLineInfo
    {
        public ulong m_productionLineId;
        public int m_productionTempleteId;
        public int m_productionCount;
        public int m_solarSystemId;
        public ulong m_factoryInstanceId;
        public int m_lineLevel;
        public DateTime m_startTime;
        public DateTime m_endTime;
        public int m_speedUpReduceTime;
        public List<CostInfo> m_costList;
        public ulong m_tradeMoneyCost;
        public ulong m_reportId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsIdle;
        private static DelegateBridge __Hotfix_IsInProduce;
        private static DelegateBridge __Hotfix_IsProduceCompleted;
        private static DelegateBridge __Hotfix_GetProductionRestTime;

        [MethodImpl(0x8000)]
        public TimeSpan GetProductionRestTime(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsIdle()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInProduce(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsProduceCompleted(DateTime currTime)
        {
        }
    }
}

