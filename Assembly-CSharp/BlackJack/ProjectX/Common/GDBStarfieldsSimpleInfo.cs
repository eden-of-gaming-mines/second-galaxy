﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBStarfieldsSimpleInfo")]
    public class GDBStarfieldsSimpleInfo : IExtensible
    {
        private int _Id;
        private string _name;
        private float _gLocationX;
        private float _gLocationZ;
        private bool _isNeedLocalization;
        private string _localizationKey;
        private bool _isHideInStarMap;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_GLocationX;
        private static DelegateBridge __Hotfix_set_GLocationX;
        private static DelegateBridge __Hotfix_get_GLocationZ;
        private static DelegateBridge __Hotfix_set_GLocationZ;
        private static DelegateBridge __Hotfix_get_IsNeedLocalization;
        private static DelegateBridge __Hotfix_set_IsNeedLocalization;
        private static DelegateBridge __Hotfix_get_LocalizationKey;
        private static DelegateBridge __Hotfix_set_LocalizationKey;
        private static DelegateBridge __Hotfix_get_IsHideInStarMap;
        private static DelegateBridge __Hotfix_set_IsHideInStarMap;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="gLocationX", DataFormat=DataFormat.FixedSize)]
        public float GLocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="gLocationZ", DataFormat=DataFormat.FixedSize)]
        public float GLocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="isNeedLocalization", DataFormat=DataFormat.Default)]
        public bool IsNeedLocalization
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(6, IsRequired=false, Name="localizationKey", DataFormat=DataFormat.Default)]
        public string LocalizationKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="isHideInStarMap", DataFormat=DataFormat.Default)]
        public bool IsHideInStarMap
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

