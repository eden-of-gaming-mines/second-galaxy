﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IProduceDataContainer
    {
        ProductionLineInfo AddProductionLine();
        List<ProductionLineInfo> GetProductionLineList();
        void ProductionLineClear(int lineIndex);
        void UpdateProductionLineCompletedNotified(int lineIndex);
        void UpdateProductionLineEndTime(int lineIndex, DateTime endTime);
        void UpdateProductionLineInfo(ProductionLineInfo info);
    }
}

