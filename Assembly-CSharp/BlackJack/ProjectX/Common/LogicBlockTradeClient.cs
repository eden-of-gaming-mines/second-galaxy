﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockTradeClient : LogicBlockTradeBase
    {
        private readonly Dictionary<int, List<TradeListItemInfo>> m_tradeLegalListItemInfosDict;
        private uint m_legalVersion;
        private DateTime m_legalNextRefreshTime;
        private readonly List<TradeListItemInfo> m_tradeIllegalListItemInfos;
        private uint m_illegalVersion;
        private DateTime m_illegalNextRefreshTime;
        private readonly List<TradeNpcShopItemInfo> m_currTradeNpcShopItemInfos;
        private uint m_shopVersion;
        private DateTime m_shopNextRefreshTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SetupTradeContext;
        private static DelegateBridge __Hotfix_TradeLegalListItemInfosDictUpdate;
        private static DelegateBridge __Hotfix_TradeIllegalListItemInfosUpdate;
        private static DelegateBridge __Hotfix_TradeNpcShopItemInfosUpdate;
        private static DelegateBridge __Hotfix_GetTradeLegalListItemInfosByItemId;
        private static DelegateBridge __Hotfix_GetAllTradeIllegalListItemInfos;
        private static DelegateBridge __Hotfix_GetCurrTradeNpcShopItemInfos;
        private static DelegateBridge __Hotfix_GetTradeLegalListVersion;
        private static DelegateBridge __Hotfix_GetTradeIllegalListVersion;
        private static DelegateBridge __Hotfix_GetCurrTradeNpcShopVersion;
        private static DelegateBridge __Hotfix_GetTradeLegalListNextRefreshTime;
        private static DelegateBridge __Hotfix_GetTradeIlegalListNextRefreshTime;
        private static DelegateBridge __Hotfix_GetCurrTradeNpcShopNextRefreshTime;

        [MethodImpl(0x8000)]
        public List<TradeListItemInfo> GetAllTradeIllegalListItemInfos()
        {
        }

        [MethodImpl(0x8000)]
        public List<TradeNpcShopItemInfo> GetCurrTradeNpcShopItemInfos()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetCurrTradeNpcShopNextRefreshTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetCurrTradeNpcShopVersion()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetTradeIlegalListNextRefreshTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetTradeIllegalListVersion()
        {
        }

        [MethodImpl(0x8000)]
        public List<TradeListItemInfo> GetTradeLegalListItemInfosByItemId(int id)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetTradeLegalListNextRefreshTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetTradeLegalListVersion()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetupTradeContext(NpcDNId npcId)
        {
        }

        [MethodImpl(0x8000)]
        public void TradeIllegalListItemInfosUpdate(uint version, List<TradeListItemInfo> tradeIllegalListItemInfos, DateTime nextRefreshTime)
        {
        }

        [MethodImpl(0x8000)]
        public void TradeLegalListItemInfosDictUpdate(uint version, int tradeItemId, List<TradeListItemInfo> tradeLegaListItemInfos, DateTime nextRefreshTime)
        {
        }

        [MethodImpl(0x8000)]
        public void TradeNpcShopItemInfosUpdate(uint version, List<TradeNpcShopItemInfo> tradeNpcShopItemInfos, DateTime nextRefreshTime)
        {
        }
    }
}

