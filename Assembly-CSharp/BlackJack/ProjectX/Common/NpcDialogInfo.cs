﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    public class NpcDialogInfo
    {
        public int DialogId;
        public List<NpcDialogOptInfo> OptList;
        public NpcDNId ReplaceNpc;
        public List<FormatStringParamInfo> FormatStrParamList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

