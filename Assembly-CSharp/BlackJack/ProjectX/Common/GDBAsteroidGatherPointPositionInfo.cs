﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBAsteroidGatherPointPositionInfo")]
    public class GDBAsteroidGatherPointPositionInfo : IExtensible
    {
        private double _locationX;
        private double _locationY;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LocationX;
        private static DelegateBridge __Hotfix_set_LocationX;
        private static DelegateBridge __Hotfix_get_LocationY;
        private static DelegateBridge __Hotfix_set_LocationY;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="locationX", DataFormat=DataFormat.TwosComplement)]
        public double LocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="locationY", DataFormat=DataFormat.TwosComplement)]
        public double LocationY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

