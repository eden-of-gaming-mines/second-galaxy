﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GuildTradePurchaseOrderTransportLogType
    {
        TradeTransportStart,
        TradeTransportSucceed,
        TradeTransportFail
    }
}

