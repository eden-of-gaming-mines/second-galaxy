﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IDFRDataSource
    {
        List<IDFRFilterResultItem> GetFilterResult(IDFRDataFilter filter, out int version4Result);
        bool IsFilterResultValid(IDFRDataFilter filter, int resultVersion, DateTime resultCreateTime);
    }
}

