﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class LogicBlockShipCompWeaponEquipSetupBase
    {
        protected ILBStaticShip m_ownerShip;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWeaponEquipUpdating;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CheckItemEquipable;
        private static DelegateBridge __Hotfix_IsWeaponEquipSlotGroupOptAvailable;
        private static DelegateBridge __Hotfix_IsHighSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsMiddleSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsLowSlotGroupAllowedToSet;
        private static DelegateBridge __Hotfix_IsDependTechReadyForWeaponEquip;
        private static DelegateBridge __Hotfix_CheckEquipSetCountLimited;
        private static DelegateBridge __Hotfix_CheckWeaponEquipSetSlotType;
        private static DelegateBridge __Hotfix_CheckRemoveItemFromSlotGroup;
        private static DelegateBridge __Hotfix_CheckItemStoreCapacityForSlotItem;
        private static DelegateBridge __Hotfix_CheckRemoveAllItemFromSlotGroup;
        private static DelegateBridge __Hotfix_CheckItemStoreCapacityForAllSlotItems;
        private static DelegateBridge __Hotfix_get_LbWeaponEquip;
        private static DelegateBridge __Hotfix_add_EventOnWeaponEquipUpdating;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponEquipUpdating;

        public event Action EventOnWeaponEquipUpdating
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompWeaponEquipSetupBase()
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckEquipSetCountLimited(LBStaticWeaponEquipSlotGroup group, LBStoreItem item, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckItemEquipable(LBStaticWeaponEquipSlotGroup group, LBStoreItem srcMSStoreItem, out int errCode, bool isCheckCpu = true, bool isCheckPower = true)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckItemStoreCapacityForAllSlotItems()
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckItemStoreCapacityForSlotItem(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckRemoveAllItemFromSlotGroup()
        {
        }

        [MethodImpl(0x8000)]
        public int CheckRemoveItemFromSlotGroup(LBStaticWeaponEquipSlotGroup group)
        {
        }

        protected abstract bool CheckWeaponEquipSetCpuCost(LBStoreItem srcMSStoreItem, out int errCode);
        protected abstract bool CheckWeaponEquipSetPowerCost(LBStoreItem srcMSStoreItem, out int errCode);
        [MethodImpl(0x8000)]
        protected bool CheckWeaponEquipSetSlotType(LBStaticWeaponEquipSlotGroup group, LBStoreItem srcMSStoreItem, out int errCode)
        {
        }

        protected abstract LogicBlockShipCompWeaponEquipSetupEnv GetLBWeaponEquipSetupEnv();
        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBStaticShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDependTechReadyForWeaponEquip(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsHighSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsLowSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsMiddleSlotGroupAllowedToSet()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsWeaponEquipSlotGroupOptAvailable(LBStaticWeaponEquipSlotGroup group)
        {
        }

        protected LogicBlockShipCompStaticWeaponEquipBase LbWeaponEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

