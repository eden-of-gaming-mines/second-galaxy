﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface IGuildItemStoreItemOperationBase
    {
        List<LBStoreItem> GetGuildStoreItemInfoList();
        LBStoreItem GetItemInfoByIndex(int index);
        LBStoreItem GetItemInfoByItemId(StoreItemType type, int configId);
        LBStoreItem GetItemInfoByItemInsId(ulong instanceId);
    }
}

