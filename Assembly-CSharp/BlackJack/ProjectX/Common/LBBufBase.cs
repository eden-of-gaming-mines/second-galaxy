﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBBufBase : IPropertiesProvider
    {
        private static int m_instanceIdSeed;
        protected uint m_instanceId;
        protected float m_bufInstanceParam1;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBBufBase> EventOnDetach;
        protected ConfigDataBufInfo m_confInfo;
        protected ILBBufOwner m_owner;
        protected ILBInSpaceShip m_ownerShip;
        protected LBBuffContainer m_container;
        protected uint m_attachTime;
        protected uint m_lifeEndTimeAbs;
        protected uint m_propertiesGroupMask;
        protected int m_propertiesGroupIdMin;
        protected int m_attachCount;
        protected uint m_creatingTime;
        protected bool m_needSync2Client;
        protected EquipType m_srcEquipType;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitBufPropertiesGroupMask;
        private static DelegateBridge __Hotfix_InitBufPropertiesGroupIdMin;
        private static DelegateBridge __Hotfix_OnAttach;
        private static DelegateBridge __Hotfix_OnDetach;
        private static DelegateBridge __Hotfix_OnAttachDuplicate;
        private static DelegateBridge __Hotfix_OnSyncAttachDuplicateInfo;
        private static DelegateBridge __Hotfix_IsLifeEnd;
        private static DelegateBridge __Hotfix_GetBufInstanceParam1;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetBufType;
        private static DelegateBridge __Hotfix_GetBufPower;
        private static DelegateBridge __Hotfix_GetBufGroupId;
        private static DelegateBridge __Hotfix_GetAttachTime;
        private static DelegateBridge __Hotfix_GetLifeEndTimeAbs;
        private static DelegateBridge __Hotfix_SetLifeEndTimeAbs;
        private static DelegateBridge __Hotfix_GetLeftTime;
        private static DelegateBridge __Hotfix_IsStatic;
        private static DelegateBridge __Hotfix_IsEnhance;
        private static DelegateBridge __Hotfix_IsDebuf_0;
        private static DelegateBridge __Hotfix_IsDebuf_1;
        private static DelegateBridge __Hotfix_IsNeedReplaceSameId;
        private static DelegateBridge __Hotfix_IsNeedSyncInSpace;
        private static DelegateBridge __Hotfix_IsShipFightBuf;
        private static DelegateBridge __Hotfix_TestBufGamePlayFlag;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMin;
        private static DelegateBridge __Hotfix_GetContinueBuf;
        private static DelegateBridge __Hotfix_GetAttachedBufCount;
        private static DelegateBridge __Hotfix_IsNeedSync2Client;
        private static DelegateBridge __Hotfix_GetOwnerShip;
        private static DelegateBridge __Hotfix_GetOwnerShipLBBasicCtx;
        private static DelegateBridge __Hotfix_GetProBufInfo;
        private static DelegateBridge __Hotfix_GetSourceEquipType;
        private static DelegateBridge __Hotfix_CombineBufInstanceParam1;
        private static DelegateBridge __Hotfix_HasPropertiesGroup;
        private static DelegateBridge __Hotfix_HasProperty;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_AllocInstanceId;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_get_GroupID;
        private static DelegateBridge __Hotfix_get_GroupLevel;
        private static DelegateBridge __Hotfix_get_InstanceParam1;
        private static DelegateBridge __Hotfix_add_EventOnDetach;
        private static DelegateBridge __Hotfix_remove_EventOnDetach;

        public event Action<LBBufBase> EventOnDetach
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBBufBase(ConfigDataBufInfo confInfo, uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public static uint AllocInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual float CombineBufInstanceParam1(ref float oldBufInstanceParam, float newBufInstanceParam)
        {
        }

        [MethodImpl(0x8000)]
        public int GetAttachedBufCount()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAttachTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBufGroupId()
        {
        }

        [MethodImpl(0x8000)]
        public float GetBufInstanceParam1()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetBufPower()
        {
        }

        [MethodImpl(0x8000)]
        public BufType GetBufType()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBufInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetContinueBuf()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetLeftTime(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetLifeEndTimeAbs()
        {
        }

        [MethodImpl(0x8000)]
        protected ILBInSpaceShip GetOwnerShip()
        {
        }

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompInSpaceBasicCtxBase GetOwnerShipLBBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ProBufInfo GetProBufInfo(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        public int GetPropertiesGroupMin()
        {
        }

        [MethodImpl(0x8000)]
        public EquipType GetSourceEquipType()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        private void InitBufPropertiesGroupIdMin()
        {
        }

        [MethodImpl(0x8000)]
        private void InitBufPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDebuf()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsDebuf(ConfigDataBufInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEnhance()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsLifeEnd(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsNeedReplaceSameId(ConfigDataBufInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedSync2Client()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedSyncInSpace()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShipFightBuf()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStatic()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnAttach(ILBBufSource source, ILBBufOwner owner, uint currTime, uint lifeEndTime, LBBuffContainer container, object param = null, float bufInstanceParam1 = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnAttachDuplicate(ILBBufSource source, float additionalBufInstanceParam1 = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnDetach(bool isLifeEnd, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnSyncAttachDuplicateInfo(List<uint> sourceTargetList, ILBBufOwner owner, uint currTime, uint lifeEndTime, LBBuffContainer container, int attachCount, object param = null, float bufInstanceParam = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLifeEndTimeAbs(uint lifeEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool TestBufGamePlayFlag(BufGamePlayFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int GroupID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int GroupLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float InstanceParam1
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

