﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IShipCustomTemplateDataContainer
    {
        ShipCustomTemplateInfo AddTemplate(ShipCustomTemplateInfo info);
        ShipCustomTemplateInfo GetTemplateByIndex(int index);
        List<ShipCustomTemplateInfo> GetTemplateList();
        void RemoveTemplate(int index);
        void UpdateTemplateName(int index, string name);
    }
}

