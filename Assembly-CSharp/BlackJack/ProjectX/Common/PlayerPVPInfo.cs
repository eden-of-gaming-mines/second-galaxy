﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class PlayerPVPInfo
    {
        public bool m_isJustCrime;
        public DateTime? m_justCrimeTimeOut;
        public float m_criminalLevel;
        public bool m_isCriminalHunter;
        public DateTime? m_criminalHunterTimeOut;
        [NonSerialized]
        public uint m_lastAidLeaveSolarSystemLimitTargetTime;
        [NonSerialized]
        public uint m_lastPVPFireTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

