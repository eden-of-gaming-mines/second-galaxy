﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct PropertyValueStringPair
    {
        public string PropertyName;
        public string Value;
        public PropertyValueStringPair(string name, string val)
        {
            this.PropertyName = name;
            this.Value = val;
        }
    }
}

