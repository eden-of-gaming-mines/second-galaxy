﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class DataFilterResultManagerWithCache : DataFilterResultManagerBase
    {
        public Dictionary<ulong, IDFRDataItem> m_dataDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddOrUpdateFilterResult;
        private static DelegateBridge __Hotfix_GetResultVersionByFilter;
        private static DelegateBridge __Hotfix_GetDataById;
        private static DelegateBridge __Hotfix_ClearAllResult;

        [MethodImpl(0x8000)]
        public void AddOrUpdateFilterResult(IDFRDataFilter filter, int version, IEnumerable<IDFRDataItem> result)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearAllResult()
        {
        }

        [MethodImpl(0x8000)]
        public IDFRDataItem GetDataById(ulong id)
        {
        }

        [MethodImpl(0x8000)]
        public int GetResultVersionByFilter(IDFRDataFilter filter)
        {
        }
    }
}

