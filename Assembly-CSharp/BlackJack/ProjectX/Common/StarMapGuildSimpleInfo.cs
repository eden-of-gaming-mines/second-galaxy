﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StarMapGuildSimpleInfo
    {
        public uint m_guildId;
        public string m_name;
        public string m_codeName;
        public GuildLogoInfo m_logoInfo;
        public uint m_allianceId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CopyForm;

        [MethodImpl(0x8000)]
        public void CopyForm(StarMapGuildSimpleInfo info)
        {
        }
    }
}

