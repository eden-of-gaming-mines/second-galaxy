﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildSimplestInfo
    {
        public uint m_guild;
        public string m_name;
        public string m_codeName;
        public GuildAllianceLanguageType m_languageType;
        public GuildJoinPolicy m_joinType;
        public int m_memberCount;
        public GuildLogoInfo m_logoInfo;
        public int m_baseSolarSystem;
        public int m_baseStation;
        public uint m_allianceId;
        public string m_allianceName;
        public string m_leaderGameUserId;
        public string m_leaderGameUserName;
        [NonSerialized]
        public DateTime m_CachedTime;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildSimplestInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimplestInfo(GuildSimplestInfo info)
        {
        }
    }
}

