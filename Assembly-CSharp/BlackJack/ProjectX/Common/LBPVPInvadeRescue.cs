﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LBPVPInvadeRescue
    {
        protected PVPInvadeRescueInfo m_readOnlyRescueInfo;
        protected LBSignalDelegateBase m_sourceSignal;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetManualQuestSolarSystemId;
        private static DelegateBridge __Hotfix_GetManualQuestSceneInstanceId;
        private static DelegateBridge __Hotfix_GetSourceSignal;
        private static DelegateBridge __Hotfix_GetInvadeStartTime;
        private static DelegateBridge __Hotfix_GetCaptainList;
        private static DelegateBridge __Hotfix_GetDestroyCaptainList;
        private static DelegateBridge __Hotfix_GetSourceSignalType;
        private static DelegateBridge __Hotfix_GetDestSolarSystemId;
        private static DelegateBridge __Hotfix_GetValidShipTypeList;
        private static DelegateBridge __Hotfix_GetCaptainDynamicList;
        private static DelegateBridge __Hotfix_GetInvadePlayerInfo;
        private static DelegateBridge __Hotfix_GetSourcePlayerInfo;

        [MethodImpl(0x8000)]
        public LBPVPInvadeRescue(PVPInvadeRescueInfo pvpInvadeRescue)
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcCaptainSimpleDynamicInfo> GetCaptainDynamicList()
        {
        }

        [MethodImpl(0x8000)]
        public List<NpcCaptainStaticInfo> GetCaptainList()
        {
        }

        [MethodImpl(0x8000)]
        public List<ulong> GetDestroyCaptainList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetDestSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimpleInfo GetInvadePlayerInfo()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetInvadeStartTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetManualQuestSceneInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetManualQuestSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimpleInfo GetSourcePlayerInfo()
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalDelegateBase GetSourceSignal()
        {
        }

        [MethodImpl(0x8000)]
        public SignalType GetSourceSignalType()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> GetValidShipTypeList()
        {
        }
    }
}

