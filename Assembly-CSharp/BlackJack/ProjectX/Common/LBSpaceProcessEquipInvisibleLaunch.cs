﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBSpaceProcessEquipInvisibleLaunch : LBSpaceProcessEquipLaunchBase
    {
        protected uint? m_nextLoopTimeAbs;
        public uint m_loopEnergyCost;
        public int m_chargingBufId;
        public int m_loopingBufId;
        protected const uint ProcessEquipInvisibleLoopCD = 0x3e8;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetLoopCostEnergy;
        private static DelegateBridge __Hotfix_SetChargeBuf;
        private static DelegateBridge __Hotfix_SetLoopBuf;
        private static DelegateBridge __Hotfix_IsLooping;
        private static DelegateBridge __Hotfix_OnInvisibleEnd;
        private static DelegateBridge __Hotfix_ProcessFireImmediate;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnEquipFire;
        private static DelegateBridge __Hotfix_get_ProcessSource;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipInvisibleLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipInvisibleLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int groupIndex, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int groupIndex, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsLooping()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEquipFire(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInvisibleEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ProcessFireImmediate(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChargeBuf(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLoopBuf(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLoopCostEnergy(ushort loopCostEnergy)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        protected ILBSpaceProcessEquipInvisibleLaunchSource ProcessSource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

