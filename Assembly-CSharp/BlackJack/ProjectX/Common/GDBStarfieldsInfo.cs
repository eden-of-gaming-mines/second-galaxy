﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBStarfieldsInfo")]
    public class GDBStarfieldsInfo : IExtensible
    {
        private int _Id;
        private string _name;
        private float _gLocationX;
        private float _gLocationZ;
        private PVector3D _color;
        private readonly List<GDBStargroupInfo> _stargroups;
        private readonly List<GDBLinkInfo> _LinkList;
        private bool _isNeedLocalization;
        private string _localizationKey;
        private bool _isHideInStarMap;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_GLocationX;
        private static DelegateBridge __Hotfix_set_GLocationX;
        private static DelegateBridge __Hotfix_get_GLocationZ;
        private static DelegateBridge __Hotfix_set_GLocationZ;
        private static DelegateBridge __Hotfix_get_Color;
        private static DelegateBridge __Hotfix_set_Color;
        private static DelegateBridge __Hotfix_get_Stargroups;
        private static DelegateBridge __Hotfix_get_LinkList;
        private static DelegateBridge __Hotfix_get_IsNeedLocalization;
        private static DelegateBridge __Hotfix_set_IsNeedLocalization;
        private static DelegateBridge __Hotfix_get_LocalizationKey;
        private static DelegateBridge __Hotfix_set_LocalizationKey;
        private static DelegateBridge __Hotfix_get_IsHideInStarMap;
        private static DelegateBridge __Hotfix_set_IsHideInStarMap;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="gLocationX", DataFormat=DataFormat.FixedSize)]
        public float GLocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="gLocationZ", DataFormat=DataFormat.FixedSize)]
        public float GLocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="color", DataFormat=DataFormat.Default)]
        public PVector3D Color
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="stargroups", DataFormat=DataFormat.Default)]
        public List<GDBStargroupInfo> Stargroups
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="LinkList", DataFormat=DataFormat.Default)]
        public List<GDBLinkInfo> LinkList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="isNeedLocalization", DataFormat=DataFormat.Default)]
        public bool IsNeedLocalization
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(9, IsRequired=false, Name="localizationKey", DataFormat=DataFormat.Default)]
        public string LocalizationKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="isHideInStarMap", DataFormat=DataFormat.Default)]
        public bool IsHideInStarMap
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

