﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class ProductionLineInfo
    {
        public int m_index;
        public int m_blueprintId;
        public bool m_blueprintBind;
        public int m_blueprintCount;
        public DateTime m_startTime;
        public DateTime m_endTime;
        public ulong m_workingCaptain;
        public List<CostInfo> m_costList;
        public bool m_isCompleteNotified;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

