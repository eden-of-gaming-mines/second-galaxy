﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBInSpaceShipPlayerCaptain : LBInSpaceShipCaptainBase, ILBInSpacePlayerShipCaptain, ILBInSpaceShipCaptain, ILBShipCaptain, IPropertiesProvider
    {
        private IPlayerInfoProvider m_playerInfoProvider;
        private PlayerPVPInfo m_playerPVPInfo;
        private Dictionary<int, float> m_playerFactionCreditMap;
        private int m_playerActivityPoint;
        private Dictionary<ActivityType, int> m_playerActivityCompleteInfo;
        private int m_wormholeGateSolarSystemId;
        private Dictionary<CurrencyType, ulong> m_playerCurrencyInfo;
        private UserSettingInServer m_userSetting;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsPlayerCaptain;
        private static DelegateBridge __Hotfix_IsHiredCaptain;
        private static DelegateBridge __Hotfix_GetPlayerGameUserId;
        private static DelegateBridge __Hotfix_GetHiredCaptainInstanceId;
        private static DelegateBridge __Hotfix_GetCaptainName;
        private static DelegateBridge __Hotfix_GetCaptainFirstNameId;
        private static DelegateBridge __Hotfix_GetCaptainLastNameId;
        private static DelegateBridge __Hotfix_GetPlayerPVPInfo;
        private static DelegateBridge __Hotfix_GetPlayerFactionCreditMap;
        private static DelegateBridge __Hotfix_GetWormholeGateSolarSystemId;
        private static DelegateBridge __Hotfix_GetPlayerActivityPoint;
        private static DelegateBridge __Hotfix_GetPlayerActivityCompleteInfo;
        private static DelegateBridge __Hotfix_GetPlayerCurrencyInfo;
        private static DelegateBridge __Hotfix_GetPlayerUserSettingInfo;

        [MethodImpl(0x8000)]
        public LBInSpaceShipPlayerCaptain(BasicPropertiesInfo basicPropertiesInfo, int drivingLicenseId, int drivingLicenseLevel, IPlayerInfoProvider playerInfoProvider, PlayerPVPInfo playerPVPInfo, Dictionary<int, float> playerFactionCreditMap, int wormholeGateSolarSystemId, int playerActivityPoint, Dictionary<ActivityType, int> playerActivityCompleteInfo, Dictionary<CurrencyType, ulong> playerCurrencyInfo, UserSettingInServer userSetting)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetCaptainFirstNameId()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetCaptainLastNameId()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetCaptainName()
        {
        }

        [MethodImpl(0x8000)]
        public override ulong GetHiredCaptainInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<ActivityType, int> GetPlayerActivityCompleteInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetPlayerActivityPoint()
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<CurrencyType, ulong> GetPlayerCurrencyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, float> GetPlayerFactionCreditMap()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetPlayerGameUserId()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerPVPInfo GetPlayerPVPInfo()
        {
        }

        [MethodImpl(0x8000)]
        public UserSettingInServer GetPlayerUserSettingInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetWormholeGateSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHiredCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsPlayerCaptain()
        {
        }
    }
}

