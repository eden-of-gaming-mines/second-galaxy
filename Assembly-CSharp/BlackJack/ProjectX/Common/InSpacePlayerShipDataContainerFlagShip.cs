﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class InSpacePlayerShipDataContainerFlagShip : InSpacePlayerShipDataContainerDefault, IInSpaceFlagShipDataContainer, IInSpacePlayerShipDataContainer, IFlagShipDataContainer, IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        protected SolarSystemFlagShipInstanceInfo m_shipInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_UpdateCurrShipFuel;
        private static DelegateBridge __Hotfix_GetCurrShipFuel;
        private static DelegateBridge __Hotfix_GetConfRelativeData;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_SetSolarSystemId;
        private static DelegateBridge __Hotfix_GetHighSoltGroupItemList;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemList;
        private static DelegateBridge __Hotfix_GetHighSoltAmmoList;
        private static DelegateBridge __Hotfix_UpdateSlotGroupAmmoInfo;

        [MethodImpl(0x8000)]
        public ConfigDataFlagShipRelativeData GetConfRelativeData()
        {
        }

        [MethodImpl(0x8000)]
        public double GetCurrShipFuel()
        {
        }

        [MethodImpl(0x8000)]
        public override AmmoInfo[] GetHighSoltAmmoList()
        {
        }

        [MethodImpl(0x8000)]
        public override ShipSlotGroupInfo[] GetHighSoltGroupItemList()
        {
        }

        [MethodImpl(0x8000)]
        public override List<ShipSlotGroupInfo> GetMiddleSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(SolarSystemFlagShipInstanceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShipFuel(double fuel)
        {
        }

        [MethodImpl(0x8000)]
        public override void UpdateSlotGroupAmmoInfo(int groupIndex, StoreItemType ammoType, int ammoConfigId, int ammoCount)
        {
        }
    }
}

