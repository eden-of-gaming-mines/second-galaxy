﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LBCharChipActiveSuit
    {
        private List<LBBufBase> m_activeBufList;
        private ConfigDataCharChipSuitInfo m_chipSuitConf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetActiveBufCount;
        private static DelegateBridge __Hotfix_AddActiveBuf;
        private static DelegateBridge __Hotfix_GetActiveBufList;
        private static DelegateBridge __Hotfix_RemoveLastActiveBuf;

        [MethodImpl(0x8000)]
        public LBCharChipActiveSuit(int suitConfId)
        {
        }

        [MethodImpl(0x8000)]
        public void AddActiveBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public int GetActiveBufCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBufBase> GetActiveBufList()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSuitInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase RemoveLastActiveBuf()
        {
        }
    }
}

