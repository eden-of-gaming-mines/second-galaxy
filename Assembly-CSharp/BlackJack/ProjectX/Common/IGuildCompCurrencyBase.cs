﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Runtime.InteropServices;

    public interface IGuildCompCurrencyBase : IGuildCurrencyBase
    {
        event Action<GuildCurrencyLogType, string, long, ulong, int, string> EventOnCurrencyChange;

        ulong GetInformationPoint();
        ulong GetTacticalPoint();
        ulong GetTradeMoney();
        void ModifyGuildInformationPoint(long addValue, GuildCurrencyLogType type = 0, string playerName = "", int param = 0, string gameUserId = "");
        void ModifyGuildTacticalPoint(long addValue, GuildCurrencyLogType type = 0, string playerName = "", int param = 0, string gameUserId = "");
        void ModifyGuildTradeMoney(long addValue, GuildCurrencyLogType type = 0, string playerName = "", int param = 0, string gameUserId = "");
    }
}

