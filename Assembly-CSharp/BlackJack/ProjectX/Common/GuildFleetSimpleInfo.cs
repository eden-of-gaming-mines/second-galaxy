﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildFleetSimpleInfo
    {
        public ulong m_fleetInstanceId;
        public int m_fleetSeqId;
        public string m_fleetCustomName;
        public int m_fleetMemberCount;
        public string m_fleetCommanderName;
        public int m_fleetCommanderAvatarId;
        public string m_fleetCommanderGameUserId;
        public string m_fleetFireControllerGameUserId;
        public string m_fleetNavigatorGameUserId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

