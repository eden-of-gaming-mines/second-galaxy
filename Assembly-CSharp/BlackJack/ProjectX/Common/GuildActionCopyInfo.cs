﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct GuildActionCopyInfo
    {
        public int m_configId;
        public ulong m_instanceId;
        public int m_solarSystemId;
    }
}

