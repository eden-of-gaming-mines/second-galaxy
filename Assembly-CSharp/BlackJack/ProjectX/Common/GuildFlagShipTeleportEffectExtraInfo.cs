﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildFlagShipTeleportEffectExtraInfo
    {
        public int m_flagShipSceneConfId;
        public string m_flagShipGuildCodeName;
        public string m_flagShipDriverName;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

