﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public abstract class LogicBlockBattlePassBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPeriodChange;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, int, DateTime> EventOnChallangeQuestGroupChange;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnAddExp;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnUpgrade;
        protected ILBPlayerContext m_lbPlayerCtx;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected LogicBlockQuestBase m_lbQuest;
        protected IBattlePassDataContainer m_dc;
        protected ConfigDataBattlePassPeriodInfo m_currPeriodConf;
        protected ConfigDataBattlePassChallangeQuestGroupInfo[] m_challengeQuestGroupConfList;
        protected ConfigDataBattlePassLevelInfo[] m_periodLevelRewardConfList;
        protected ConfigDataBattlePassChallangeQuestAccRewardInfo[] m_challengeQuestAccRewardConfList;
        protected static int m_levelExp;
        protected static int m_levelPrice;
        protected static int m_upgradePrice;
        protected static int m_upgradePackagePrice;
        protected static int m_upgradedExpBonus;
        protected static int m_periodDuration;
        protected static int m_challageGroupDuration;
        protected static int m_upgradePackageAddLevel;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassLevelInfo>, int> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassLevelInfo>, ConfigDataBattlePassLevelInfo> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo>, int> <>f__am$cache2;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassChallangeQuestGroupInfo>, ConfigDataBattlePassChallangeQuestGroupInfo> <>f__am$cache3;
        [CompilerGenerated]
        private static Func<KeyValuePair<int, ConfigDataBattlePassChallangeQuestAccRewardInfo>, ConfigDataBattlePassChallangeQuestAccRewardInfo> <>f__am$cache4;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetExp;
        private static DelegateBridge __Hotfix_IsUpgraded;
        private static DelegateBridge __Hotfix_SetUpgrade;
        private static DelegateBridge __Hotfix_IsLevelRewardBalanced;
        private static DelegateBridge __Hotfix_IsChallangeQuestAccRewardBalanced;
        private static DelegateBridge __Hotfix_GetChallangeQuestCompletedCount;
        private static DelegateBridge __Hotfix_IsChallangeQuestCompleted;
        private static DelegateBridge __Hotfix_OnAddExp;
        private static DelegateBridge __Hotfix_OnQuestCompleteWait;
        private static DelegateBridge __Hotfix_OnQuestCompleteAndBalanceReward;
        private static DelegateBridge __Hotfix_SetRewardBalanced;
        private static DelegateBridge __Hotfix_SetChallangeQuestAccRewardBalanced;
        private static DelegateBridge __Hotfix_IsLevelRewardExist;
        private static DelegateBridge __Hotfix_FireEventOnPeriodChange;
        private static DelegateBridge __Hotfix_FireEventOnChallangeQuestGroupChange;
        private static DelegateBridge __Hotfix_FireEventOnUpgrade;
        private static DelegateBridge __Hotfix_add_EventOnPeriodChange;
        private static DelegateBridge __Hotfix_remove_EventOnPeriodChange;
        private static DelegateBridge __Hotfix_add_EventOnChallangeQuestGroupChange;
        private static DelegateBridge __Hotfix_remove_EventOnChallangeQuestGroupChange;
        private static DelegateBridge __Hotfix_add_EventOnAddExp;
        private static DelegateBridge __Hotfix_remove_EventOnAddExp;
        private static DelegateBridge __Hotfix_add_EventOnUpgrade;
        private static DelegateBridge __Hotfix_remove_EventOnUpgrade;
        private static DelegateBridge __Hotfix_get_GlobalPeriodId;
        private static DelegateBridge __Hotfix_get_GlobalPeriodInfo;
        private static DelegateBridge __Hotfix_get_CurrChallengeQuestGroupIndex;
        private static DelegateBridge __Hotfix_GetCurrPeriodStartTime;
        private static DelegateBridge __Hotfix_GetBattlePassExpBonus;
        private static DelegateBridge __Hotfix_InitConfigableConst;
        private static DelegateBridge __Hotfix_SetGlobalPeriod;
        private static DelegateBridge __Hotfix_CalcChallengeQuestIndexInCurrPeriod;
        private static DelegateBridge __Hotfix_ClearProcessingChallangeQuestGroup;
        private static DelegateBridge __Hotfix_get_CurrPeriodBattlePassLevelMax;

        public event Action<int> EventOnAddExp
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, int, DateTime> EventOnChallangeQuestGroupChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPeriodChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnUpgrade
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LogicBlockBattlePassBase()
        {
        }

        [MethodImpl(0x8000)]
        public int CalcChallengeQuestIndexInCurrPeriod(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearProcessingChallangeQuestGroup(bool periodChange)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnChallangeQuestGroupChange(int currPeriodId, int currChallangeQuestGroupIndex, DateTime currPeriodStartTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnPeriodChange()
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnUpgrade(bool isPackage)
        {
        }

        [MethodImpl(0x8000)]
        public int GetBattlePassExpBonus()
        {
        }

        [MethodImpl(0x8000)]
        public int GetChallangeQuestCompletedCount()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetCurrPeriodStartTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetExp()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public void InitConfigableConst()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsChallangeQuestAccRewardBalanced(int accRewardIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsChallangeQuestCompleted(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLevelRewardBalanced(int level, bool isUpgrade)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLevelRewardExist(int level, bool isUpgrade)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsUpgraded()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int OnAddExp(int addExp)
        {
        }

        [MethodImpl(0x8000)]
        public virtual int OnQuestCompleteAndBalanceReward(LBProcessingQuestBase quest, int confAddExp)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnQuestCompleteWait(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetChallangeQuestAccRewardBalanced(int accRewardIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGlobalPeriod(ConfigDataBattlePassPeriodInfo periodConf)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRewardBalanced(int level, bool isUpgrade)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUpgrade(bool isPackage)
        {
        }

        public int GlobalPeriodId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataBattlePassPeriodInfo GlobalPeriodInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int CurrChallengeQuestGroupIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected int CurrPeriodBattlePassLevelMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

