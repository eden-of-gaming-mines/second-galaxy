﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class StoredMailInfo
    {
        public int m_index;
        public bool m_deleted;
        public uint m_version;
        public MailInfo m_mailInfo;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

