﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildStaffingLogListInfo
    {
        public List<GuildStaffingLogInfo> m_logList;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

