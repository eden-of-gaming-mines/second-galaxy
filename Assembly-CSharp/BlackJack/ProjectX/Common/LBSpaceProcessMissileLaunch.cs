﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessMissileLaunch : LBSpaceProcessBulletGunLaunch, ILBSpaceProcessSource
    {
        protected float m_flyDistance;
        protected ConfigDataMissileInfo m_missileConfInfo;
        protected LBSpaceProcessBulletFly[] m_missileFlyProcessList;
        protected int m_missileFlyProcessListCurrCount;
        protected int[] m_preCalcMissileArmor;
        protected int m_preCalcDestroyCount;
        protected int m_onHitCount;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_InitForPreCalc;
        private static DelegateBridge __Hotfix_SetFlyDistance;
        private static DelegateBridge __Hotfix_IsNeedPreCalc4Defender;
        private static DelegateBridge __Hotfix_GetPreCalcAttackableMissileIndex;
        private static DelegateBridge __Hotfix_GetAttackablePeriodAbs;
        private static DelegateBridge __Hotfix_OnPreCalcHit;
        private static DelegateBridge __Hotfix_RegisterMissileFlyProcess;
        private static DelegateBridge __Hotfix_GetMissileFlyProcess;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessCancel;

        [MethodImpl(0x8000)]
        public LBSpaceProcessMissileLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessMissileLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ConfigDataMissileInfo missileConfInfo, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void GetAttackablePeriodAbs(float attackRange, int missileindex, out uint startTime, out uint endTime)
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletFly GetMissileFlyProcess(byte index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetPreCalcAttackableMissileIndex(int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void InitForPreCalc()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ConfigDataMissileInfo missileConfInfo, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedPreCalc4Defender()
        {
        }

        [MethodImpl(0x8000)]
        public bool OnPreCalcHit(int missileindex, uint damage)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterMissileFlyProcess(LBSpaceProcessBulletFly bulletFlyProcess)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlyDistance(float distance)
        {
        }
    }
}

