﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockSolarSystemInfoBase
    {
        protected ILBPlayerContext m_playerContext;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetNearestCelestial;

        [MethodImpl(0x8000)]
        public CelestialResult GetNearestCelestial(Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct CelestialResult
        {
            public GDBStarInfo m_starInfo;
            public GDBPlanetInfo m_planetInfo;
            public GDBMoonInfo m_moonInfo;
        }
    }
}

