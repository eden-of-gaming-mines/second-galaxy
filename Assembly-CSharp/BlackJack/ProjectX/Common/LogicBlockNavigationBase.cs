﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockNavigationBase
    {
        protected ILBPlayerContext m_playerContext;
        protected GDBHelper m_gdbHelper;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected int m_startSolarSystemId;
        protected int m_endSolarSystemId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsNavigationInProcess;
        private static DelegateBridge __Hotfix_get_StartSolarSystemId;
        private static DelegateBridge __Hotfix_set_StartSolarSystemId;
        private static DelegateBridge __Hotfix_get_EndSolarSystemId;
        private static DelegateBridge __Hotfix_set_EndSolarSystemId;

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNavigationInProcess()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        public int StartSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int EndSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

