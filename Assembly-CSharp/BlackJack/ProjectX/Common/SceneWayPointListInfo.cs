﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SceneWayPointListInfo")]
    public class SceneWayPointListInfo : IExtensible
    {
        private string _ScriptRefName;
        private readonly List<PVector3D> _PointList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ScriptRefName;
        private static DelegateBridge __Hotfix_set_ScriptRefName;
        private static DelegateBridge __Hotfix_get_PointList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ScriptRefName", DataFormat=DataFormat.Default)]
        public string ScriptRefName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="PointList", DataFormat=DataFormat.Default)]
        public List<PVector3D> PointList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

