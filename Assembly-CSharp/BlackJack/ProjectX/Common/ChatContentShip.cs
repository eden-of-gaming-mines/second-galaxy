﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    public class ChatContentShip : ChatInfo
    {
        public int m_shipConfId;
        public string m_shipName;
        public ShipSlotGroupInfo[] m_highSlotList;
        public AmmoInfo[] m_highSlotAmmoList;
        public List<ShipSlotGroupInfo> m_middleSlotList;
        public List<ShipSlotGroupInfo> m_lowSlotList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

