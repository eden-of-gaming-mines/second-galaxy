﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;
    using System.Runtime.InteropServices;

    public interface ILBSpaceObject
    {
        double CalcDistanceToLocation(Vector3D location, bool considerSpaceObjectCollider = false);
        double CalcDistanceToTarget(ILBSpaceObject target, bool considerSpaceObjectCollider = false);
        Vector3D GetLocation();
        Vector3D GetRotation();
        float GetSpaceObjectRadiusMax();
    }
}

