﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventAttachBuf : LBSyncEvent
    {
        public int m_configId;
        public uint m_instanceId;
        public uint m_lifeEndTime;
        public uint m_srcTargetId;
        public float m_instanceParam;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

