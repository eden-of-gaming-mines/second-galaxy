﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompSimpleRuntimeBase : GuildCompBase, IGuildCompSimpleRuntimeBase, IGuildSimpleRuntimeBase
    {
        protected readonly GuildSimplestInfo m_simplestInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildSimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_GetSimplestInfo;
        private static DelegateBridge __Hotfix_UpdateSimplestInfo;
        private static DelegateBridge __Hotfix_UpdateGuildSimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompSimpleRuntimeBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimpleRuntimeInfo GetGuildSimpleRuntimeInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimplestInfo GetSimplestInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildSimpleRuntimeInfo(GuildSimpleRuntimeInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateSimplestInfo()
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

