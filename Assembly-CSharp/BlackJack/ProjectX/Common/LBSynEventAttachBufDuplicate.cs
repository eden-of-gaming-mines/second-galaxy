﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSynEventAttachBufDuplicate : LBSyncEvent
    {
        public int m_configId;
        public uint m_instanceId;
        public int m_attachCount;
        public float m_additionalBufInstanceParam;
        public uint m_sourceTargetId;
        public uint m_lifeEndTime;
        public List<uint> m_sourceTargetList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CombineSourceTarget;

        [MethodImpl(0x8000)]
        public void CombineSourceTarget(uint srcTargetId)
        {
        }
    }
}

