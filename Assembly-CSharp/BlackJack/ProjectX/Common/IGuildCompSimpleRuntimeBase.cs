﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildCompSimpleRuntimeBase : IGuildSimpleRuntimeBase
    {
        void UpdateGuildSimpleRuntimeInfo(GuildSimpleRuntimeInfo info);
    }
}

