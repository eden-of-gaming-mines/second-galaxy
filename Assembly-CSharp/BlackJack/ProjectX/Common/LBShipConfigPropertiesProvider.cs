﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBShipConfigPropertiesProvider : IPropertiesProvider
    {
        protected ILBSpaceShipBasic m_ownerShip;
        protected ConfigDataSpaceShipInfo m_shipConfigInfo;
        protected HashSet<int> m_shipPropertiesSet;
        protected HashSet<int> m_shipDrivingLicensePropertiesSet;
        protected uint m_shipPropertiesMask;
        protected uint m_shipDrivingLicensePropertiesMask;
        private ILBShipCaptain m_captain;
        private int m_drivingLicenseId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetCurrCaptainDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_HasPropertiesGroup;
        private static DelegateBridge __Hotfix_HasProperty;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_CalcDrivingLicenseProperties;
        private static DelegateBridge __Hotfix_FindPropertiesBaseValueFromConfig;
        private static DelegateBridge __Hotfix_InitShipPropertiesCache;
        private static DelegateBridge __Hotfix_InitShipDrivingLicensePropertiesCache;

        [MethodImpl(0x8000)]
        public LBShipConfigPropertiesProvider(ILBSpaceShipBasic ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcDrivingLicenseProperties(float confValue, int drivingLevel, float propertiesLevelMulti)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool FindPropertiesBaseValueFromConfig(int propertiesId, out float baseValue)
        {
        }

        [MethodImpl(0x8000)]
        protected int? GetCurrCaptainDrivingLicenseLevel()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitShipDrivingLicensePropertiesCache()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitShipPropertiesCache()
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }
    }
}

