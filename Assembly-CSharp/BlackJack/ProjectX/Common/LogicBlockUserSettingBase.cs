﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockUserSettingBase
    {
        private IUserSettingDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetAutoFightNotControlPlayerShipMoveState;
        private static DelegateBridge __Hotfix_SetAutoFightNotControlPlayerShipMoveState;

        [MethodImpl(0x8000)]
        public bool GetAutoFightNotControlPlayerShipMoveState()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAutoFightNotControlPlayerShipMoveState(bool state)
        {
        }
    }
}

