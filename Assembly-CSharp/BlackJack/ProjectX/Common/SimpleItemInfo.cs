﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct SimpleItemInfo
    {
        public StoreItemType m_itemType;
        public int m_id;
    }
}

