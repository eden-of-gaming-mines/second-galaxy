﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperWeaponGroupDroneBase : LBSuperWeaponGroupBase
    {
        protected ConfigDataDroneInfo m_droneAmmoInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_GetAmmoType;
        private static DelegateBridge __Hotfix_CalcBulletFlyTime;
        private static DelegateBridge __Hotfix_CalcDroneFightTime;
        private static DelegateBridge __Hotfix_CalcDroneFormupTime;
        private static DelegateBridge __Hotfix_CalcGroupCD;
        private static DelegateBridge __Hotfix_CalcWaveCD;
        private static DelegateBridge __Hotfix_CalcHitRateFinal;
        private static DelegateBridge __Hotfix_CalcCriticalRate;
        private static DelegateBridge __Hotfix_CalcDamageTotal;
        private static DelegateBridge __Hotfix_CalcCriticalDamageTotal;
        private static DelegateBridge __Hotfix_CalcDamageComposeHeat;
        private static DelegateBridge __Hotfix_CalcDamageComposeKinetic;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_CalcFireCtrlAccuracy;
        private static DelegateBridge __Hotfix_CalcCD4NextDroneAttack;
        private static DelegateBridge __Hotfix_CalcCD4DroneNextAttack;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4WeaponEquip;
        private static DelegateBridge __Hotfix_GetPropertyFromConfigData4Ammo;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_IsTargetValidInWave;

        [MethodImpl(0x8000)]
        public LBSuperWeaponGroupDroneBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ushort CalcBulletFlyTime(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected ushort CalcCD4DroneNextAttack()
        {
        }

        [MethodImpl(0x8000)]
        protected ushort CalcCD4NextDroneAttack()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcCriticalDamageTotal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcCriticalRate(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageComposeHeat()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageComposeKinetic()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageTotal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected ushort CalcDroneFightTime()
        {
        }

        [MethodImpl(0x8000)]
        protected ushort CalcDroneFormupTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireCtrlAccuracy()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public override uint CalcGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcHitRateFinal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcWaveCD()
        {
        }

        [MethodImpl(0x8000)]
        public override StoreItemType GetAmmoType()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsTargetValidInWave(ILBSpaceTarget iLBSpaceTargetBase)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnProcessUnitFire(LBSpaceProcessDroneLaunchBase process, ushort unitIndex)
        {
        }
    }
}

