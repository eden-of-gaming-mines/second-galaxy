﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSyncEventDroneDefenderPreCalcAttackMissile : LBSyncEvent
    {
        public uint m_processDroneDefenderLaunchId;
        public uint m_destObjectId;
        public uint m_processMissileLaunchId;
        public List<AttackChance> m_attackChanceList;
        private static DelegateBridge _c__Hotfix_ctor;

        [Serializable]
        public class AttackChance
        {
            public byte DroneIndex;
            public byte ChanceIndex;
            public byte TargetIndex;
            public bool IsDestroyTarget;
            private static DelegateBridge _c__Hotfix_ctor_0;
            private static DelegateBridge _c__Hotfix_ctor_1;
            private static DelegateBridge __Hotfix_Convert2AttackChanceInt;

            [MethodImpl(0x8000)]
            public AttackChance()
            {
            }

            [MethodImpl(0x8000)]
            public AttackChance(uint chance)
            {
            }

            [MethodImpl(0x8000)]
            public uint Convert2AttackChanceInt()
            {
            }
        }
    }
}

