﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using System;
    using System.Collections.Generic;

    public interface ILBInSpacePlayerShipCaptain : ILBInSpaceShipCaptain, ILBShipCaptain, IPropertiesProvider
    {
        Dictionary<ActivityType, int> GetPlayerActivityCompleteInfo();
        int GetPlayerActivityPoint();
        Dictionary<CurrencyType, ulong> GetPlayerCurrencyInfo();
        Dictionary<int, float> GetPlayerFactionCreditMap();
        PlayerPVPInfo GetPlayerPVPInfo();
        UserSettingInServer GetPlayerUserSettingInfo();
        int GetWormholeGateSolarSystemId();
    }
}

