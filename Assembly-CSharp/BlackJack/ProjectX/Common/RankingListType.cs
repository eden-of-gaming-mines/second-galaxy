﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum RankingListType
    {
        KillRecordCount,
        KillRecordBindMoneyValue,
        ContinueKillCount,
        CharacterEvaluateScore,
        GuildEvaluateScore,
        AllianceEvaluateScore,
        Max
    }
}

