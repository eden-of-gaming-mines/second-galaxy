﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBWeaponGroupBase : LBInSpaceWeaponEquipGroupBase
    {
        protected ConfigDataWeaponInfo m_confInfo;
        protected uint? m_reloadAmmoEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SetPropertiesCacheDirty;
        private static DelegateBridge __Hotfix_CalcGroupCD;
        private static DelegateBridge __Hotfix_IsOrdinaryWeapon;
        private static DelegateBridge __Hotfix_IsSuperWeapon;
        private static DelegateBridge __Hotfix_IsOrdinaryEquip;
        private static DelegateBridge __Hotfix_IsSuperEquip;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_GetWeaponCategory;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetGroupIndex;
        private static DelegateBridge __Hotfix_GetTotalUnitCount;
        private static DelegateBridge __Hotfix_GetTotalFireCount;
        private static DelegateBridge __Hotfix_GetAmmoConfId;
        private static DelegateBridge __Hotfix_GetAmmoType;
        private static DelegateBridge __Hotfix_GetAmmoCount;
        private static DelegateBridge __Hotfix_GetAmmoClipSize;
        private static DelegateBridge __Hotfix_GetReloadAmmoEndTime;
        private static DelegateBridge __Hotfix_CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_UnloadAmmo;
        private static DelegateBridge __Hotfix_ReloadAmmo;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_IsReadyForLaunchAmmoCheck;
        private static DelegateBridge __Hotfix_IsTargetValidInWave;
        private static DelegateBridge __Hotfix_HasWeaponAmmoLogic;
        private static DelegateBridge __Hotfix_TryStartReloadAmmo;
        private static DelegateBridge __Hotfix_OnReloadAmmoCDEnd;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CalcPercentageDamge;
        private static DelegateBridge __Hotfix_CalcChargeTime;
        private static DelegateBridge __Hotfix_CalcCD4NextUnit;
        private static DelegateBridge __Hotfix_CalcCD4NextTurret;
        private static DelegateBridge __Hotfix_CalcRealDamageElectric;
        private static DelegateBridge __Hotfix_CalcRealDamageHeat;
        private static DelegateBridge __Hotfix_CalcRealDamageKinetic;
        private static DelegateBridge __Hotfix_CalcIsHit;
        private static DelegateBridge __Hotfix_CalcIsCritical;
        private static DelegateBridge __Hotfix_CalcDamageIncludeTargetShieldRatioDmgMulti;
        private static DelegateBridge __Hotfix_GetChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_GetWeaponTransverseVelocity;
        private static DelegateBridge __Hotfix_CalcAllDamageMulti;
        private static DelegateBridge __Hotfix_CalcWeaponGroupDPS;

        [MethodImpl(0x8000)]
        public LBWeaponGroupBase(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcAllDamageMulti()
        {
        }

        [MethodImpl(0x8000)]
        protected ushort CalcCD4NextTurret()
        {
        }

        [MethodImpl(0x8000)]
        protected ushort CalcCD4NextUnit()
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcChargeTime()
        {
        }

        protected abstract float CalcCriticalDamageTotal(ILBSpaceTarget target);
        protected abstract float CalcCriticalRate(ILBSpaceTarget target);
        protected abstract float CalcDamageComposeHeat();
        protected abstract float CalcDamageComposeKinetic();
        [MethodImpl(0x8000)]
        protected float CalcDamageIncludeTargetShieldRatioDmgMulti(ILBSpaceTarget target, float damageTotal)
        {
        }

        protected abstract float CalcDamageTotal(ILBSpaceTarget target);
        protected abstract float CalcFireCtrlAccuracy();
        [MethodImpl(0x8000)]
        public override uint CalcGroupCD()
        {
        }

        protected abstract float CalcHitRateFinal(ILBSpaceTarget target);
        [MethodImpl(0x8000)]
        protected bool CalcIsCritical(float propertyCriticalRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CalcIsHit(float propertyHitRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcPercentageDamge()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcRealDamageElectric(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcRealDamageHeat(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcRealDamageKinetic(ILBSpaceTarget target)
        {
        }

        public abstract float CalcReloadAmmoCD();
        public abstract float CalcWaveGroupDamage();
        [MethodImpl(0x8000)]
        public float CalcWeaponGroupDPS()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoClipSize()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoConfId()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoCount()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override StoreItemType GetAmmoType()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetChargeAndChannelBreakFactor()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataWeaponInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGroupIndex()
        {
        }

        [MethodImpl(0x8000)]
        public uint? GetReloadAmmoEndTime()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetTotalFireCount()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetTotalUnitCount()
        {
        }

        [MethodImpl(0x8000)]
        public WeaponCategory GetWeaponCategory()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetWeaponTransverseVelocity()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool HasWeaponAmmoLogic()
        {
        }

        [MethodImpl(0x8000)]
        public override void Initialize()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsOrdinaryEquip()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsOrdinaryWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsReadyForLaunchAmmoCheck(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsSuperEquip()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsSuperWeapon()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsTargetValidInWave(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool OnReloadAmmoCDEnd()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool ReloadAmmo(int itemConfigId, StoreItemType itemType, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertiesCacheDirty()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool TryStartReloadAmmo(uint startTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool UnloadAmmo()
        {
        }
    }
}

