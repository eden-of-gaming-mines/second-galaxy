﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IShipSalvageDataContainer
    {
        void AddUsedDailyShipSalvageCount();
        void ClearUsedDailyShipSalvageCount();
        int GetUsedDailyShipSalvageCount();
    }
}

