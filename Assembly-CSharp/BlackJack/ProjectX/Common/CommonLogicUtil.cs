﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using BlackJack.ProjectX.SimSolarSystemNs;
    using BlackJack.ToolUtil;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class CommonLogicUtil
    {
        [CompilerGenerated]
        private static Func<GDBPlanetInfo, bool> <>f__am$cache0;
        private static DelegateBridge __Hotfix_Convert2SimpleItemInfoWithCount_0;
        private static DelegateBridge __Hotfix_GetStoreItemSalePrice;
        private static DelegateBridge __Hotfix_GetStoreItemConfigData;
        private static DelegateBridge __Hotfix_GetRankAndSubRank;
        private static DelegateBridge __Hotfix_GetRandomItemInWeightPool_0;
        private static DelegateBridge __Hotfix_GetRandomItemInWeightPool_1;
        private static DelegateBridge __Hotfix_IsGlobalSceneAlwaysVisible;
        private static DelegateBridge __Hotfix_Convert2MathLibVector_1;
        private static DelegateBridge __Hotfix_Convert2MathLibVector_2;
        private static DelegateBridge __Hotfix_Convert2ProVectorDouble;
        private static DelegateBridge __Hotfix_Convert2PVector3D;
        private static DelegateBridge __Hotfix_Convert2MathLibVector_0;
        private static DelegateBridge __Hotfix_GetRotationUInt16;
        private static DelegateBridge __Hotfix_GetRotationDouble;
        private static DelegateBridge __Hotfix_Convert2ProVectorUInt32;
        private static DelegateBridge __Hotfix_Convert2ShipPositionInfo;
        private static DelegateBridge __Hotfix_Convert2SimMoveableSpaceObjectStateSnapshot;
        private static DelegateBridge __Hotfix_Convert2SimSpaceShipStateSnapshot;
        private static DelegateBridge __Hotfix_Convert2ProBaseMoveStateSnapshot;
        private static DelegateBridge __Hotfix_Convert2ProShipMoveStateSnapshot;
        private static DelegateBridge __Hotfix_Copy3DCollisionInfo;
        private static DelegateBridge __Hotfix_GetCollisionAfterRotation;
        private static DelegateBridge __Hotfix_GetIconSpriteResFullPath;
        private static DelegateBridge __Hotfix_GetSquareIconSpriteResFullPath;
        private static DelegateBridge __Hotfix_GetCircularIconSpriteResFullPath;
        private static DelegateBridge __Hotfix_GetNpcBodySpriteResFullPath;
        private static DelegateBridge __Hotfix_GetShipArtResFullPath;
        private static DelegateBridge __Hotfix_GetShipArtLOD0ResFullPath;
        private static DelegateBridge __Hotfix_GetWeaponArtResFullPath;
        private static DelegateBridge __Hotfix_GetSpaceStationArtResFullPath;
        private static DelegateBridge __Hotfix_GetPrdouce3DBgArtResFullPath;
        private static DelegateBridge __Hotfix_GetStarGateArtResFullPath;
        private static DelegateBridge __Hotfix_GetSimpleObjectArtResFullPath;
        private static DelegateBridge __Hotfix_GetEditorSceneArtResFullPath;
        private static DelegateBridge __Hotfix_GetPlanetArtResFullPath;
        private static DelegateBridge __Hotfix_GetSkillTreeArtResFullPath;
        private static DelegateBridge __Hotfix_GetCrackBoxArtResFullPath;
        private static DelegateBridge __Hotfix_GetOtherArtResFullPath;
        private static DelegateBridge __Hotfix_GetGrandFactionNameForShipWeapon;
        private static DelegateBridge __Hotfix_GetAmmoArtResFullPath;
        private static DelegateBridge __Hotfix_GetEquipArtResFullPath;
        private static DelegateBridge __Hotfix_GetAmmoTypeByWeaponConfInfo;
        private static DelegateBridge __Hotfix_GetNormalAmmoTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetStringKeyInStringTableWithId;
        private static DelegateBridge __Hotfix_GetShipLevelDesc;
        private static DelegateBridge __Hotfix_GetShipEffectArtResFullPath;
        private static DelegateBridge __Hotfix_GetCutSceneArtResFullPath;
        private static DelegateBridge __Hotfix_Convert2SignalInfo;
        private static DelegateBridge __Hotfix_Convert2ProSignalInfo;
        private static DelegateBridge __Hotfix_Convert2PlayerSimpleInfoList;
        private static DelegateBridge __Hotfix_Convert2ProPlayerSimpleInfoList;
        private static DelegateBridge __Hotfix_Convert2PlayerSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2ProPlayerSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2ProPVPSignalInfo;
        private static DelegateBridge __Hotfix_Convert2PVPSignalInfo;
        private static DelegateBridge __Hotfix_Convert2DelegateMissionInvadeInfo;
        private static DelegateBridge __Hotfix_Convert2ProDelegateMissionInvadeInfo;
        private static DelegateBridge __Hotfix_Convert2DelegateMissionInvadeEventInfo;
        private static DelegateBridge __Hotfix_Convert2ProDelegateMissionInvadeEventInfo;
        private static DelegateBridge __Hotfix_Convert2DelegateMissionInvadeEventInfoList;
        private static DelegateBridge __Hotfix_Convert2ProDelegateMissionInvadeEventInfoList;
        private static DelegateBridge __Hotfix_Convert2DelegateMissionInfo;
        private static DelegateBridge __Hotfix_Convert2ProDelegateMissionInfo;
        private static DelegateBridge __Hotfix_Convert2ProIdLevelInfo;
        private static DelegateBridge __Hotfix_Convert2IdLevelInfo;
        private static DelegateBridge __Hotfix_Convert2NpcCaptainSimpleDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2ProHiredCaptainSimpleDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2NpcCaptainStaticInfo;
        private static DelegateBridge __Hotfix_Convert2ProHiredCaptainStaticInfo;
        private static DelegateBridge __Hotfix_Convert2NpcCaptainInfo;
        private static DelegateBridge __Hotfix_Convert2ProHiredCaptainInfo;
        private static DelegateBridge __Hotfix_Convert2MSRedeployInfo;
        private static DelegateBridge __Hotfix_Convert2ProMSRedeployInfo;
        private static DelegateBridge __Hotfix_Convert2ItemInfo;
        private static DelegateBridge __Hotfix_Convert2ProItemInfo;
        private static DelegateBridge __Hotfix_Convert2ItemInfoList;
        private static DelegateBridge __Hotfix_Convert2MailInfo;
        private static DelegateBridge __Hotfix_Convert2ProMailInfo;
        private static DelegateBridge __Hotfix_Convert2ProStoreItemInfo;
        private static DelegateBridge __Hotfix_Convert2StoreItemInfo;
        private static DelegateBridge __Hotfix_Convert2ProShipStoreItemInfo;
        private static DelegateBridge __Hotfix_Convert2ShipStoreItemInfo;
        private static DelegateBridge __Hotfix_Convert2ShipStoreItemInfoList;
        private static DelegateBridge __Hotfix_Convert2ProStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2StoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2ProShipStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2ShipStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2GuildStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2ProCustomizedParameterInfo;
        private static DelegateBridge __Hotfix_Convert2CustomizedParameterInfo;
        private static DelegateBridge __Hotfix_Convert2ProDailyLoginRewardInfo;
        private static DelegateBridge __Hotfix_Convert2DailyLoginRewardInfo;
        private static DelegateBridge __Hotfix_IsTimeOnPlayerDailyRefreshCycle;
        private static DelegateBridge __Hotfix_Convert2ProCMDailySignInfo;
        private static DelegateBridge __Hotfix_Convert2ProCommanderAuthInfo;
        private static DelegateBridge __Hotfix_Convert2ProShipSlotGroupInfo;
        private static DelegateBridge __Hotfix_Convert2ShipSlotGroupInfo;
        private static DelegateBridge __Hotfix_Convert2ProShipSlotAmmoInfo;
        private static DelegateBridge __Hotfix_Convert2ProCostInfo;
        private static DelegateBridge __Hotfix_Convert2CostInfo;
        private static DelegateBridge __Hotfix_Convert2ProTechUpgradeInfo;
        private static DelegateBridge __Hotfix_Convert2TechUpgradeInfo;
        private static DelegateBridge __Hotfix_Convert2ProCurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_Convert2ProProductionLineInfo;
        private static DelegateBridge __Hotfix_Convert2ProductionLineInfo;
        private static DelegateBridge __Hotfix_Convert2ProShipCustomTemplateInfo;
        private static DelegateBridge __Hotfix_Convert2ShipCustomTemplateInfo;
        private static DelegateBridge __Hotfix_Convert2ProStoreItemTransformInfo;
        private static DelegateBridge __Hotfix_Convert2StoreItemTransformInfo;
        private static DelegateBridge __Hotfix_Convert2ProQuestProcessingInfo;
        private static DelegateBridge __Hotfix_Convert2QuestProcessingInfo;
        private static DelegateBridge __Hotfix_Convert2ProQuestWaitForAcceptInfo;
        private static DelegateBridge __Hotfix_Convert2QuestWaitForAcceptInfo;
        private static DelegateBridge __Hotfix_Convert2ProQuestRewardInfo;
        private static DelegateBridge __Hotfix_Convert2QuestRewardInfo;
        private static DelegateBridge __Hotfix_Convert2ProNpcDNId;
        private static DelegateBridge __Hotfix_Convert2NpcDNId;
        private static DelegateBridge __Hotfix_AddItemInfo2ItemList;
        private static DelegateBridge __Hotfix_AddCurrency2CurrencyList;
        private static DelegateBridge __Hotfix_GetNpcNameByNameId;
        private static DelegateBridge __Hotfix_GetNpcNameByNpcDNId;
        private static DelegateBridge __Hotfix_Convert2ProKillRecordInfo;
        private static DelegateBridge __Hotfix_Convert2ProKillRecordOpponentInfo;
        private static DelegateBridge __Hotfix_Convert2ProSimpleItemInfoWithCountList;
        private static DelegateBridge __Hotfix_Convert2ProLostItemInfo;
        private static DelegateBridge __Hotfix_Convert2LostItemInfo;
        private static DelegateBridge __Hotfix_Convert2ProSimpleItemInfoWithCount;
        private static DelegateBridge __Hotfix_Convert2KillRecordInfo;
        private static DelegateBridge __Hotfix_Convert2KillRecordOpponentInfo;
        private static DelegateBridge __Hotfix_Convert2SimpleItemInfoWithCountList;
        private static DelegateBridge __Hotfix_Convert2SimpleItemInfoWithCount_1;
        private static DelegateBridge __Hotfix_Convert2ProPlayerPVPInfo;
        private static DelegateBridge __Hotfix_Convert2PlayerPVPInfo;
        private static DelegateBridge __Hotfix_Convert2ProDrivingLicenseUpgradeInfo;
        private static DelegateBridge __Hotfix_Convert2DrivingLicenseUpgradeInfo;
        private static DelegateBridge __Hotfix_Convert2ProBitArrayExInfo;
        private static DelegateBridge __Hotfix_Convert2BitArrayEx;
        private static DelegateBridge __Hotfix_Convert2ProActivityInfo;
        private static DelegateBridge __Hotfix_Convert2ActivityInfo;
        private static DelegateBridge __Hotfix_Convert2ProPlayerActivityInfo;
        private static DelegateBridge __Hotfix_Convert2PlayerActivityInfo;
        private static DelegateBridge __Hotfix_Convert2ProGlobalSceneInfo;
        private static DelegateBridge __Hotfix_Convert2GlobalSceneInfo;
        private static DelegateBridge __Hotfix_Convert2ProRankingPlayerInfo;
        private static DelegateBridge __Hotfix_Convert2RankingPlayerBasicInfo_1;
        private static DelegateBridge __Hotfix_Convert2ProRankingGuildInfo;
        private static DelegateBridge __Hotfix_Convert2RankingGuildBasicInfo;
        private static DelegateBridge __Hotfix_Convert2ProRankingAllianceInfo;
        private static DelegateBridge __Hotfix_Convert2RankingAllianceBasicInfo;
        private static DelegateBridge __Hotfix_Convert2ProRankingListInfo;
        private static DelegateBridge __Hotfix_Convert2RankingListInfo;
        private static DelegateBridge __Hotfix_Convert2ProNpcDialogInfo;
        private static DelegateBridge __Hotfix_Convert2NpcDialogInfo;
        private static DelegateBridge __Hotfix_Convert2ProFormatStringParamInfo;
        private static DelegateBridge __Hotfix_Convert2FormatStringParamInfo;
        private static DelegateBridge __Hotfix_Convert2NpcCaptainSimpleInfoList;
        private static DelegateBridge __Hotfix_Convert2ProNpcCaptainSimpleInfoList;
        private static DelegateBridge __Hotfix_Convert2ProNpcCaptainSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2NpcCaptainSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2ProAuctionOrderBriefInfo;
        private static DelegateBridge __Hotfix_Convert2RankingPlayerBasicInfo_0;
        private static DelegateBridge __Hotfix_Convert2ProAuctionItemDetailInfo;
        private static DelegateBridge __Hotfix_Convert2AuctionItemDetailInfo;
        private static DelegateBridge __Hotfix_Convert2ProTradeListItemInfo;
        private static DelegateBridge __Hotfix_Convert2TradeListItemInfo;
        private static DelegateBridge __Hotfix_Convert2ProTradeNpcShopItemInfo;
        private static DelegateBridge __Hotfix_Convert2TradeNpcShopItemInfo;
        private static DelegateBridge __Hotfix_Convert2ProTradeNpcShopSaleItemInfo;
        private static DelegateBridge __Hotfix_Convert2TradeNpcShopSaleItemInfo;
        private static DelegateBridge __Hotfix_Convert2ProPVPInvadeRescueInfo;
        private static DelegateBridge __Hotfix_Convert2PVPInvadeRescueInfo;
        private static DelegateBridge __Hotfix_GetAudioResFullPath;
        private static DelegateBridge __Hotfix_GetSceneAnimationResFullPath;
        private static DelegateBridge __Hotfix_CalcInsertPosForSortedList;
        private static DelegateBridge __Hotfix_GetRandomItemIndexByWeightList;
        private static DelegateBridge __Hotfix_GetAgeType;
        private static DelegateBridge __Hotfix_GetConstellation;
        private static DelegateBridge __Hotfix_GetCharBasePropertyIdByPropertyCategory;
        private static DelegateBridge __Hotfix_IsPoliceNpcShipType;
        private static DelegateBridge __Hotfix_GetSolarSystemName_2;
        private static DelegateBridge __Hotfix_GetSolarSystemName_0;
        private static DelegateBridge __Hotfix_GetSolarSystemName_1;
        private static DelegateBridge __Hotfix_GetSpaceStationName;
        private static DelegateBridge __Hotfix_GetStargateName;
        private static DelegateBridge __Hotfix_GetPlanetName;
        private static DelegateBridge __Hotfix_GetRomanCharacterStringByInt;
        private static DelegateBridge __Hotfix_GetDefaultNpcGuildIdByCulture;
        private static DelegateBridge __Hotfix_Convert2GuildBasicInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBasicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildLogoInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildLogoInfo;
        private static DelegateBridge __Hotfix_Convert2GuildMemberListInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildMemberListInfo;
        private static DelegateBridge __Hotfix_Convert2GuildSimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildSimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_Convert2DiplomacyInfo;
        private static DelegateBridge __Hotfix_Convert2ProDiplomacyInfo;
        private static DelegateBridge __Hotfix_Convert2GuildMemberInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildMemberInfo;
        private static DelegateBridge __Hotfix_Convert2GuildMemberBasicInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildMemberBasicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildMemberDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildMemberDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildPurchaseLogInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildPurchaseOrderInfo;
        private static DelegateBridge __Hotfix_Convert2GuildPurchaseOrderInfo;
        private static DelegateBridge __Hotfix_Convert2GuildMemberRuntimeInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildMemberRuntimeInfo;
        private static DelegateBridge __Hotfix_Convert2PlayerSimplestInfo;
        private static DelegateBridge __Hotfix_Convert2ProPlayerSimplestInfo_1;
        private static DelegateBridge __Hotfix_Convert2GuildSimplestInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildSimplestInfo;
        private static DelegateBridge __Hotfix_Convert2GuildJoinApplyViewInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildJoinApplyViewInfo;
        private static DelegateBridge __Hotfix_Convert2GuildJoinApplyBasicInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildJoinApplyBasicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildStaffingLogInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildStoreItemInfo;
        private static DelegateBridge __Hotfix_Convert2GuildStoreItemInfo;
        private static DelegateBridge __Hotfix_Convert2GuildCurrencyInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildCurrencyInfo;
        private static DelegateBridge __Hotfix_Convert2GuildSolarSystemInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildSolarSystemInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildCurrencyLogInfo;
        private static DelegateBridge __Hotfix_Convert2GuildCurrencyLogInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBuildingInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBuildingInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBuildingBattleInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBuildingBattleInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildFleetMemberBasicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFleetMemberInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildFleetMemberDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFleetMemberDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildFleetSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFleetSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildFleetDetailInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFleetInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFleetMemberDynamicInfoList;
        private static DelegateBridge __Hotfix_Convert2ProGuildFleetBasicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFlagShipHangarBasicInfo;
        private static DelegateBridge __Hotfix_Convert2GuildStaticFlagShipListInfo;
        private static DelegateBridge __Hotfix_Convert2GuildStaticFlagShipDetailInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildFlagShipDetailInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildFlagShipHangarDetailInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFlagShipHangarDetailInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildAntiTeleportInfo;
        private static DelegateBridge __Hotfix_Convert2GuildAntiTeleportEffectInfo;
        private static DelegateBridge __Hotfix_Convert2GuildTeleportTunnelEffectInfoClient;
        private static DelegateBridge __Hotfix_Convert2ProGuildTeleportTunnelInfo;
        private static DelegateBridge __Hotfix_IsWormholeSolarSystem;
        private static DelegateBridge __Hotfix_IsHighSafetySolarSystem;
        private static DelegateBridge __Hotfix_IsMatchShipTypeLimitForPlayerShip;
        private static DelegateBridge __Hotfix_IsMatchShipGrandFactionLimitForPlayerShip;
        private static DelegateBridge __Hotfix_IsMatchShipTypeLimitForHiredCaptainShip;
        private static DelegateBridge __Hotfix_IsMatchShipTypeLimitForWingManShip;
        private static DelegateBridge __Hotfix_GetSolarSystemDifficultLevel;
        private static DelegateBridge __Hotfix_GetSolarSystemSecurityLevel_0;
        private static DelegateBridge __Hotfix_GetSolarSystemSecurityLevel_1;
        private static DelegateBridge __Hotfix_CheckSolarSystemEnablePlayerSovereignty;
        private static DelegateBridge __Hotfix_GetSolarSystemQuestPoolInfoByPoolCount;
        private static DelegateBridge __Hotfix_GetSolarSystemDelegateMissionPoolInfoByPoolCount;
        private static DelegateBridge __Hotfix_GetQuestLevelBySignalDifficult;
        private static DelegateBridge __Hotfix_GetSignalDifficultByQuestLevel;
        private static DelegateBridge __Hotfix_CalcLaserDamageMultiForWaveGroupDamage;
        private static DelegateBridge __Hotfix_AscendTimeCallBack;
        private static DelegateBridge __Hotfix_DescendTimeCallBack;
        private static DelegateBridge __Hotfix_GetTradeMoneyCostByGuildProductionSpeedUpLevel;
        private static DelegateBridge __Hotfix_GetTimeReduceByGuildProductionSpeedUpLevel;
        private static DelegateBridge __Hotfix_Convert2ProGuildProductionLineInfo;
        private static DelegateBridge __Hotfix_Convert2GuildProductionLineInfo;
        private static DelegateBridge __Hotfix_Convert2ProBuildingLostReport;
        private static DelegateBridge __Hotfix_Convert2BuildingLostReport;
        private static DelegateBridge __Hotfix_Convert2ProSovereignLostReport;
        private static DelegateBridge __Hotfix_Convert2SovereignLostReport;
        private static DelegateBridge __Hotfix_Convert2ProLossCompensation;
        private static DelegateBridge __Hotfix_Convert2LossCompensation;
        private static DelegateBridge __Hotfix_Convert2ProLostItem;
        private static DelegateBridge __Hotfix_Convert2LostItem;
        private static DelegateBridge __Hotfix_GetGuildBuildingLevelDetailConf;
        private static DelegateBridge __Hotfix_IsGuildBuildingLevelMax;
        private static DelegateBridge __Hotfix_Convert2GuildActionInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildActionInfo;
        private static DelegateBridge __Hotfix_GetFlourishLevelByValue;
        private static DelegateBridge __Hotfix_GetFlourishMaxValue;
        private static DelegateBridge __Hotfix_GetFlourishNeedValueByLevel;
        private static DelegateBridge __Hotfix_GetFlourishCurrValueByLevel;
        private static DelegateBridge __Hotfix_GetValueFromListByIndex;
        private static DelegateBridge __Hotfix_Convert2ProGuildBattleSimpleReportInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBattleSimpleReportInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBattleReportInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBattleReportInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBattlePlayerKillLostStatInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBattlePlayerKillLostStatInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBattleGuildKillLostStatInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBattleGuildKillLostStatInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBattleShipLostStatInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBattleShipLostStatInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildBattleGuildBuildingSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2GuildBattleGuildBuildingSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2GuildMomentsInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildMomentsInfo;
        private static DelegateBridge __Hotfix_CheckNpcGuildId;
        private static DelegateBridge __Hotfix_GetDiplomacyType2Player;
        private static DelegateBridge __Hotfix_Convert2StarMapGuildOccupyStaticInfo;
        private static DelegateBridge __Hotfix_Convert2ProStarMapGuildOccupyStaticInfo;
        private static DelegateBridge __Hotfix_Convert2StarMapGuildOccupyDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2ProStarMapGuildOccupyDynamicInfo;
        private static DelegateBridge __Hotfix_Convert2StarMapGuildSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2ProStarMapGuildSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2StarMapAllianceSimpleInfo;
        private static DelegateBridge __Hotfix_Convert2ProStarMapAllianceSimpleInfo;
        private static DelegateBridge __Hotfix_GetGuildFleetPositionListByFlag;
        private static DelegateBridge __Hotfix_CreateFormationInfoByGuildFleetFormationConfId;
        private static DelegateBridge __Hotfix_GetRandomPostionInSphere;
        private static DelegateBridge __Hotfix_GetRandomDisruptionList;
        private static DelegateBridge __Hotfix_Convert2SceneLevelByQuestLevel;
        private static DelegateBridge __Hotfix_Convert2SceneLevelBySignalLevel;
        private static DelegateBridge __Hotfix_Convert2SceneLevelBySolarSystemSignalDifficult;
        private static DelegateBridge __Hotfix_GetRandomPlanetFromSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetUniversePosForPlanetByNearCelestialType;
        private static DelegateBridge __Hotfix_GetRandomLocationNearPlanet;
        private static DelegateBridge __Hotfix_GetRandomStandaloneLocation;
        private static DelegateBridge __Hotfix_GetGuildBuildingLocation;
        private static DelegateBridge __Hotfix_GetRandomLocationForDynamicScene;
        private static DelegateBridge __Hotfix_GetLooseRandomLocationForDynamicScene;
        private static DelegateBridge __Hotfix_TestBufGamePlayFlag;
        private static DelegateBridge __Hotfix_IsFightBuf_1;
        private static DelegateBridge __Hotfix_IsFightBuf_0;
        private static DelegateBridge __Hotfix_Convert2ProGuildSovereigntyBenefits;
        private static DelegateBridge __Hotfix_Convert2ProGuildInformationPointBenefits;
        private static DelegateBridge __Hotfix_Convert2ProGuildCommonBenefits;
        private static DelegateBridge __Hotfix_Convert2GuildCommonBenefits;
        private static DelegateBridge __Hotfix_Convert2GuildInformationPointBenefits;
        private static DelegateBridge __Hotfix_Convert2GuildSovereigntyBenefits;
        private static DelegateBridge __Hotfix_ConvertMineral2ZipMineralForDrop;
        private static DelegateBridge __Hotfix_Convert2ProGuildTradePurchaseInfo;
        private static DelegateBridge __Hotfix_Convert2GuildTradePurchaseInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildTradeTransportInfo;
        private static DelegateBridge __Hotfix_Convert2GuildTradeTransportInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildTradePortExtraInfo;
        private static DelegateBridge __Hotfix_Convert2GuildTradePortExtraInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildTradePurchaseOrderTransportLogInfo;
        private static DelegateBridge __Hotfix_Convert2GuildTradePurchaseOrderTransportLogInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildFlagShipOptLogInfo;
        private static DelegateBridge __Hotfix_Convert2GuildFlagShipOptLogInfo;
        private static DelegateBridge __Hotfix_Convert2ProAllianceBasicInfo;
        private static DelegateBridge __Hotfix_Convert2AllianceBasicInfo;
        private static DelegateBridge __Hotfix_Convert2AllianceLogoInfo;
        private static DelegateBridge __Hotfix_Convert2ProAllianceLogoInfo;
        private static DelegateBridge __Hotfix_Convert2ProAllianceInfo;
        private static DelegateBridge __Hotfix_Convert2AllianceInfo;
        private static DelegateBridge __Hotfix_Convert2ProAllianceMemberInfo;
        private static DelegateBridge __Hotfix_Convert2AllianceMemberInfo;
        private static DelegateBridge __Hotfix_Convert2ProAllianceInviteInfo;
        private static DelegateBridge __Hotfix_Convert2AllianceInviteInfo;
        private static DelegateBridge __Hotfix_Convert2GuildAllianceInviteInfo;
        private static DelegateBridge __Hotfix_Convert2ProGuildAllianceInviteInfo;
        private static DelegateBridge __Hotfix_Convert2ProAllianceSimplestInfo;
        private static DelegateBridge __Hotfix_Convert2AllianceSimplestInfo;
        private static DelegateBridge __Hotfix_Convert2ProRechargeOrder;
        private static DelegateBridge __Hotfix_Convert2ProRechargeGiftPackage;
        private static DelegateBridge __Hotfix_Convert2ProRechargeMonthlyCard;
        private static DelegateBridge __Hotfix_Convert2ProRechargeItem;
        private static DelegateBridge __Hotfix_Convert2RechargeOrder;
        private static DelegateBridge __Hotfix_Convert2RechargeGiftPackage;
        private static DelegateBridge __Hotfix_Convert2RechargeMonthlyCard;
        private static DelegateBridge __Hotfix_Convert2RechargeItem;
        private static DelegateBridge __Hotfix_Convert2ProPlayerSimplestInfo_0;
        private static DelegateBridge __Hotfix_ReplaceSensitiveWord;
        private static DelegateBridge __Hotfix_IsSensitiveWord;
        private static DelegateBridge __Hotfix_ConvertDoubleToInt32;
        private static DelegateBridge __Hotfix_ConvertInt32ToDouble;
        private static DelegateBridge __Hotfix_SolarSystemIdListInRange;

        [MethodImpl(0x8000)]
        public static void AddCurrency2CurrencyList(List<KeyValuePair<CurrencyType, int>> currencyList, CurrencyType type, int count)
        {
        }

        [MethodImpl(0x8000)]
        public static void AddItemInfo2ItemList(List<ItemInfo> itemList, ItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public static int AscendTimeCallBack(DateTime lhs, DateTime rhs)
        {
        }

        [MethodImpl(0x8000)]
        public static int CalcInsertPosForSortedList<T>(List<T> list, T item, Comparison<T> comparison)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcLaserDamageMultiForWaveGroupDamage(ConfigDataLaserDamageInfo conf)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckNpcGuildId(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckSolarSystemEnablePlayerSovereignty(int solarSystemId, GDBSolarSystemInfo solarSystemConf = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ActivityInfo Convert2ActivityInfo(ProActivityInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static AllianceBasicInfo Convert2AllianceBasicInfo(ProAllianceBasicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static AllianceInfo Convert2AllianceInfo(ProAllianceInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static AllianceInviteInfo Convert2AllianceInviteInfo(ProAllianceInviteInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static AllianceLogoInfo Convert2AllianceLogoInfo(ProAllianceLogoInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static AllianceMemberInfo Convert2AllianceMemberInfo(ProAllianceMemberInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static AllianceSimplestInfo Convert2AllianceSimplestInfo(ProAllianceSimplestInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static AuctionItemDetailInfo Convert2AuctionItemDetailInfo(ProAuctionItemDetailInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static BitArrayEx Convert2BitArrayEx(ProBitArrayExInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static BuildingLostReport Convert2BuildingLostReport(ProBuildingLostReport proReport)
        {
        }

        [MethodImpl(0x8000)]
        public static CostInfo Convert2CostInfo(ProCostInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static CurrencyUpdateInfo Convert2CurrencyUpdateInfo(ProCurrencyUpdateInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static CustomizedParameterInfo Convert2CustomizedParameterInfo(ProCustomizedParameterInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static DailyLoginRewardInfo Convert2DailyLoginRewardInfo(this ProDailyLoginRewardInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static DelegateMissionInfo Convert2DelegateMissionInfo(ProDelegateMissionInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static DelegateMissionInvadeEventInfo Convert2DelegateMissionInvadeEventInfo(ProDelegateMissionInvadeEventInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<DelegateMissionInvadeEventInfo> Convert2DelegateMissionInvadeEventInfoList(List<ProDelegateMissionInvadeEventInfo> proInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public static DelegateMissionInvadeInfo Convert2DelegateMissionInvadeInfo(ProDelegateMissionInvadeInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static DiplomacyInfo Convert2DiplomacyInfo(ProDiplomacyInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static DrivingLicenseUpgradeInfo Convert2DrivingLicenseUpgradeInfo(ProDrivingLicenseUpgradeInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static FormatStringParamInfo Convert2FormatStringParamInfo(ProFormatStringParamInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GlobalSceneInfo Convert2GlobalSceneInfo(ProGlobalSceneInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildActionInfo Convert2GuildActionInfo(ProGuildActionInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildAllianceInviteInfo Convert2GuildAllianceInviteInfo(ProGuildAllianceInviteInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildAntiTeleportEffectInfo Convert2GuildAntiTeleportEffectInfo(ProGuildAntiTeleportInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBasicInfo Convert2GuildBasicInfo(ProGuildBasicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBattleGuildBuildingSimpleInfo Convert2GuildBattleGuildBuildingSimpleInfo(ProGuildBattleGuildBuildingSimpleInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBattleGuildKillLostStatInfo Convert2GuildBattleGuildKillLostStatInfo(ProGuildBattleGuildKillLostStatInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBattlePlayerKillLostStatInfo Convert2GuildBattlePlayerKillLostStatInfo(ProGuildBattlePlayerKillLostStatInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBattleReportInfo Convert2GuildBattleReportInfo(ProGuildBattleReportInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBattleShipLostStatInfo Convert2GuildBattleShipLostStatInfo(ProGuildBattleShipLostStatInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBattleSimpleReportInfo Convert2GuildBattleSimpleReportInfo(ProGuildBattleSimpleReportInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBuildingBattleInfo Convert2GuildBuildingBattleInfo(ProGuildBuildingBattleInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildBuildingInfo Convert2GuildBuildingInfo(ProGuildBuildingInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildCommonBenefitsInfo Convert2GuildCommonBenefits(ProGuildBenefitsCommon proBenefits)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildCurrencyInfo Convert2GuildCurrencyInfo(ProGuildCurrencyInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildCurrencyLogInfo Convert2GuildCurrencyLogInfo(ProGuildCurrencyLogInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildDynamicInfo Convert2GuildDynamicInfo(ProGuildDynamicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFlagShipHangarBasicInfo Convert2GuildFlagShipHangarBasicInfo(ProGuildFlagShipHangarDetailInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFlagShipHangarDetailInfo Convert2GuildFlagShipHangarDetailInfo(ProGuildFlagShipHangarDetailInfo proDetailInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFlagShipOptLogInfo Convert2GuildFlagShipOptLogInfo(ProGuildFlagShipOptLogInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFleetInfo Convert2GuildFleetInfo(ProGuildFleetDetailInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFleetMemberDynamicInfo Convert2GuildFleetMemberDynamicInfo(ProGuildFleetMemberDynamicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<GuildFleetMemberDynamicInfo> Convert2GuildFleetMemberDynamicInfoList(ProGuildFleetDetailInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFleetMemberInfo Convert2GuildFleetMemberInfo(ProGuildFleetMemberBasicInfo proMemberInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFleetSimpleInfo Convert2GuildFleetSimpleInfo(ProGuildFleetSimpleInfo proSimpleInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildInformationPointBenefitsInfo Convert2GuildInformationPointBenefits(ProGuildBenefitsInformationPoint proBenefits)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildJoinApplyBasicInfo Convert2GuildJoinApplyBasicInfo(ProGuildJoinApplyBasicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildJoinApplyViewInfo Convert2GuildJoinApplyViewInfo(ProGuildJoinApplyViewInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildLogoInfo Convert2GuildLogoInfo(ProGuildLogoInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberBasicInfo Convert2GuildMemberBasicInfo(ProGuildMemberBasicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberDynamicInfo Convert2GuildMemberDynamicInfo(ProGuildMemberDynamicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberInfo Convert2GuildMemberInfo(ProGuildMemberInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberListInfo Convert2GuildMemberListInfo(ProGuildMemberListInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberRuntimeInfo Convert2GuildMemberRuntimeInfo(ProGuildMemberRuntimeInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMomentsInfo Convert2GuildMomentsInfo(ProGuildMomentsInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildProductionLineInfo Convert2GuildProductionLineInfo(ProGuildProductionLineInfo proLineInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildPurchaseOrderInfo Convert2GuildPurchaseOrderInfo(ProGuildPurchaseOrderInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildSimpleRuntimeInfo Convert2GuildSimpleRuntimeInfo(ProGuildSimpleRuntimeInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildSimplestInfo Convert2GuildSimplestInfo(ProGuildSimplestInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildSolarSystemInfo Convert2GuildSolarSystemInfo(ProGuildSolarSystemInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildSovereigntyBenefitsInfo Convert2GuildSovereigntyBenefits(ProGuildBenefitsSovereignty proBenefits)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildStaffingLogInfo Convert2GuildStaffingLogInfo(ProGuildStaffingLogInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildStaticFlagShipInstanceInfo Convert2GuildStaticFlagShipDetailInfo(ProGuildFlagShipDetailInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<GuildStaticFlagShipInstanceInfo> Convert2GuildStaticFlagShipListInfo(ProGuildFlagShipHangarDetailInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildStoreItemInfo Convert2GuildStoreItemInfo(ProGuildStoreItemInfo proItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildStoreItemUpdateInfo Convert2GuildStoreItemUpdateInfo(ProGuildStoreItemUpdateInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildTeleportTunnelEffectInfoClient Convert2GuildTeleportTunnelEffectInfoClient(ProGuildTeleportTunnelInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildTradePortExtraInfo Convert2GuildTradePortExtraInfo(ProGuildTradePortExtraInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildTradePurchaseInfo Convert2GuildTradePurchaseInfo(ProGuildTradePurchaseInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildTradePurchaseOrderTransportLogInfo Convert2GuildTradePurchaseOrderTransportLogInfo(ProGuildTradePurchaseOrderTransportLogInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildTradeTransportInfo Convert2GuildTradeTransportInfo(ProGuildTradeTransportInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static IdLevelInfo Convert2IdLevelInfo(ProIdLevelInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ItemInfo Convert2ItemInfo(ProItemInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ItemInfo> Convert2ItemInfoList(List<ProItemInfo> proItemList)
        {
        }

        [MethodImpl(0x8000)]
        public static KillRecordInfo Convert2KillRecordInfo(ProKillRecordInfo prokillRecordInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static KillRecordOpponentInfo Convert2KillRecordOpponentInfo(ProKillRecordOpponentInfo proOpponentInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static LossCompensation Convert2LossCompensation(ProLossCompensation input)
        {
        }

        [MethodImpl(0x8000)]
        public static LostItem Convert2LostItem(ProLostItem input)
        {
        }

        [MethodImpl(0x8000)]
        public static LostItemInfo Convert2LostItemInfo(ProLostItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static MailInfo Convert2MailInfo(ProMailInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D Convert2MathLibVector(PVector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D Convert2MathLibVector(ProVectorDouble vector)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D Convert2MathLibVector(ProVectorUInt32 vector)
        {
        }

        [MethodImpl(0x8000)]
        public static MSRedeployInfo Convert2MSRedeployInfo(ProMSRedeployInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcCaptainInfo Convert2NpcCaptainInfo(ProHiredCaptainInfo proCaptainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcCaptainSimpleDynamicInfo Convert2NpcCaptainSimpleDynamicInfo(ProHiredCaptainSimpleDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcCaptainSimpleInfo Convert2NpcCaptainSimpleInfo(ProNpcCaptainSimpleInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<NpcCaptainSimpleInfo> Convert2NpcCaptainSimpleInfoList(List<ProNpcCaptainSimpleInfo> proInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcCaptainStaticInfo Convert2NpcCaptainStaticInfo(ProHiredCaptainStaticInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcDialogInfo Convert2NpcDialogInfo(ProNpcDialogInfo proDialogInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcDNId Convert2NpcDNId(ProNpcDNId proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static PlayerActivityInfo Convert2PlayerActivityInfo(ProPlayerActivityInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static PlayerPVPInfo Convert2PlayerPVPInfo(ProPlayerPVPInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static PlayerSimpleInfo Convert2PlayerSimpleInfo(ProPlayerSimpleInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<PlayerSimpleInfo> Convert2PlayerSimpleInfoList(List<ProPlayerSimpleInfo> proInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public static PlayerSimplestInfo Convert2PlayerSimplestInfo(ProPlayerSimplestInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProActivityInfo Convert2ProActivityInfo(ActivityInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAllianceBasicInfo Convert2ProAllianceBasicInfo(AllianceBasicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAllianceInfo Convert2ProAllianceInfo(AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAllianceInviteInfo Convert2ProAllianceInviteInfo(AllianceInviteInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAllianceLogoInfo Convert2ProAllianceLogoInfo(AllianceLogoInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAllianceMemberInfo Convert2ProAllianceMemberInfo(AllianceMemberInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAllianceSimplestInfo Convert2ProAllianceSimplestInfo(AllianceSimplestInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAuctionItemDetailInfo Convert2ProAuctionItemDetailInfo(AuctionItemDetailInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProAuctionOrderBriefInfo Convert2ProAuctionOrderBriefInfo(AuctionOrderBriefInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProBaseMoveStateSnapshot Convert2ProBaseMoveStateSnapshot(SimMoveableSpaceObjectStateSnapshot simMoveableSpaceObjectStateSnapshot)
        {
        }

        [MethodImpl(0x8000)]
        public static ProBitArrayExInfo Convert2ProBitArrayExInfo(BitArrayEx info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProBuildingLostReport Convert2ProBuildingLostReport(BuildingLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public static ProCMDailySignInfo Convert2ProCMDailySignInfo(this CMDailySignInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProCommanderAuthInfo Convert2ProCommanderAuthInfo(this CommanderAuthInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProCostInfo Convert2ProCostInfo(CostInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProCurrencyUpdateInfo Convert2ProCurrencyUpdateInfo(CurrencyUpdateInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProCustomizedParameterInfo Convert2ProCustomizedParameterInfo(CustomizedParameterInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProDailyLoginRewardInfo Convert2ProDailyLoginRewardInfo(this DailyLoginRewardInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProDelegateMissionInfo Convert2ProDelegateMissionInfo(DelegateMissionInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProDelegateMissionInvadeEventInfo Convert2ProDelegateMissionInvadeEventInfo(DelegateMissionInvadeEventInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ProDelegateMissionInvadeEventInfo> Convert2ProDelegateMissionInvadeEventInfoList(List<DelegateMissionInvadeEventInfo> infoList)
        {
        }

        [MethodImpl(0x8000)]
        public static ProDelegateMissionInvadeInfo Convert2ProDelegateMissionInvadeInfo(DelegateMissionInvadeInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProDiplomacyInfo Convert2ProDiplomacyInfo(DiplomacyInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProDrivingLicenseUpgradeInfo Convert2ProDrivingLicenseUpgradeInfo(DrivingLicenseUpgradeInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProductionLineInfo Convert2ProductionLineInfo(ProProductionLineInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProFormatStringParamInfo Convert2ProFormatStringParamInfo(FormatStringParamInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGlobalSceneInfo Convert2ProGlobalSceneInfo(GlobalSceneInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildActionInfo Convert2ProGuildActionInfo(GuildActionInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildAllianceInviteInfo Convert2ProGuildAllianceInviteInfo(GuildAllianceInviteInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildAntiTeleportInfo Convert2ProGuildAntiTeleportInfo(GuildAntiTeleportEffectInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBasicInfo Convert2ProGuildBasicInfo(GuildBasicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBattleGuildBuildingSimpleInfo Convert2ProGuildBattleGuildBuildingSimpleInfo(GuildBattleGuildBuildingSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBattleGuildKillLostStatInfo Convert2ProGuildBattleGuildKillLostStatInfo(GuildBattleGuildKillLostStatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBattlePlayerKillLostStatInfo Convert2ProGuildBattlePlayerKillLostStatInfo(GuildBattlePlayerKillLostStatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBattleReportInfo Convert2ProGuildBattleReportInfo(GuildBattleReportInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBattleShipLostStatInfo Convert2ProGuildBattleShipLostStatInfo(GuildBattleShipLostStatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBattleSimpleReportInfo Convert2ProGuildBattleSimpleReportInfo(GuildBattleSimpleReportInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBuildingBattleInfo Convert2ProGuildBuildingBattleInfo(GuildBuildingBattleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBuildingInfo Convert2ProGuildBuildingInfo(GuildBuildingInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBenefitsCommon Convert2ProGuildCommonBenefits(GuildCommonBenefitsInfo benefits)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildCurrencyInfo Convert2ProGuildCurrencyInfo(GuildCurrencyInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildCurrencyLogInfo Convert2ProGuildCurrencyLogInfo(GuildCurrencyLogInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildDynamicInfo Convert2ProGuildDynamicInfo(GuildDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFlagShipDetailInfo Convert2ProGuildFlagShipDetailInfo(GuildStaticFlagShipInstanceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFlagShipHangarDetailInfo Convert2ProGuildFlagShipHangarDetailInfo(GuildFlagShipHangarDetailInfo detailInfo, List<PlayerSimplestInfo> extraShipDriverList = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFlagShipOptLogInfo Convert2ProGuildFlagShipOptLogInfo(GuildFlagShipOptLogInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFleetBasicInfo Convert2ProGuildFleetBasicInfo(GuildFleetInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFleetDetailInfo Convert2ProGuildFleetDetailInfo(GuildFleetInfo guildFleetInfo, List<GuildFleetMemberDynamicInfo> dynamicInfoList, bool needFillBasicInfo, bool needFillMemberBasicInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFleetMemberBasicInfo Convert2ProGuildFleetMemberBasicInfo(GuildFleetMemberInfo memberInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFleetMemberDynamicInfo Convert2ProGuildFleetMemberDynamicInfo(GuildFleetMemberDynamicInfo dynamicInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildFleetSimpleInfo Convert2ProGuildFleetSimpleInfo(GuildFleetSimpleInfo fleetInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBenefitsInformationPoint Convert2ProGuildInformationPointBenefits(GuildInformationPointBenefitsInfo benefits)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildJoinApplyBasicInfo Convert2ProGuildJoinApplyBasicInfo(GuildJoinApplyBasicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildJoinApplyViewInfo Convert2ProGuildJoinApplyViewInfo(GuildJoinApplyViewInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildLogoInfo Convert2ProGuildLogoInfo(GuildLogoInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildMemberBasicInfo Convert2ProGuildMemberBasicInfo(GuildMemberBasicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildMemberDynamicInfo Convert2ProGuildMemberDynamicInfo(GuildMemberDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildMemberInfo Convert2ProGuildMemberInfo(GuildMemberInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildMemberListInfo Convert2ProGuildMemberListInfo(GuildMemberListInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildMemberRuntimeInfo Convert2ProGuildMemberRuntimeInfo(GuildMemberRuntimeInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildMomentsInfo Convert2ProGuildMomentsInfo(GuildMomentsInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildProductionLineInfo Convert2ProGuildProductionLineInfo(GuildProductionLineInfo lineInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildPurchaseLogInfo Convert2ProGuildPurchaseLogInfo(GuildPurchaseLogInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildPurchaseOrderInfo Convert2ProGuildPurchaseOrderInfo(GuildPurchaseOrderInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildSimpleRuntimeInfo Convert2ProGuildSimpleRuntimeInfo(GuildSimpleRuntimeInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildSimplestInfo Convert2ProGuildSimplestInfo(GuildSimplestInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildSolarSystemInfo Convert2ProGuildSolarSystemInfo(GuildSolarSystemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildBenefitsSovereignty Convert2ProGuildSovereigntyBenefits(GuildSovereigntyBenefitsInfo benefits)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildStoreItemInfo Convert2ProGuildStoreItemInfo(GuildStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildStoreItemUpdateInfo Convert2ProGuildStoreItemUpdateInfo(GuildStoreItemUpdateInfo updateInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildTeleportTunnelInfo Convert2ProGuildTeleportTunnelInfo(GuildTeleportTunnelEffectInfo effectInfo, GuildFlagShipTeleportEffectExtraInfo flagShipExtraInfo, bool isLimitedByAntiTeleport)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildTradePortExtraInfo Convert2ProGuildTradePortExtraInfo(GuildTradePortExtraInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildTradePurchaseInfo Convert2ProGuildTradePurchaseInfo(GuildTradePurchaseInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildTradePurchaseOrderTransportLogInfo Convert2ProGuildTradePurchaseOrderTransportLogInfo(GuildTradePurchaseOrderTransportLogInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProGuildTradeTransportInfo Convert2ProGuildTradeTransportInfo(GuildTradeTransportInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProHiredCaptainInfo Convert2ProHiredCaptainInfo(NpcCaptainInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProHiredCaptainSimpleDynamicInfo Convert2ProHiredCaptainSimpleDynamicInfo(NpcCaptainSimpleDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProHiredCaptainStaticInfo Convert2ProHiredCaptainStaticInfo(NpcCaptainStaticInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProIdLevelInfo Convert2ProIdLevelInfo(IdLevelInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProItemInfo Convert2ProItemInfo(ItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public static ProKillRecordInfo Convert2ProKillRecordInfo(KillRecordInfo killRecordInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProKillRecordOpponentInfo Convert2ProKillRecordOpponentInfo(KillRecordOpponentInfo opponentInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProLossCompensation Convert2ProLossCompensation(LossCompensation input)
        {
        }

        [MethodImpl(0x8000)]
        public static ProLostItem Convert2ProLostItem(LostItem input)
        {
        }

        [MethodImpl(0x8000)]
        public static ProLostItemInfo Convert2ProLostItemInfo(LostItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProMailInfo Convert2ProMailInfo(MailInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProMSRedeployInfo Convert2ProMSRedeployInfo(MSRedeployInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProNpcCaptainSimpleInfo Convert2ProNpcCaptainSimpleInfo(NpcCaptainSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ProNpcCaptainSimpleInfo> Convert2ProNpcCaptainSimpleInfoList(List<NpcCaptainSimpleInfo> infoList)
        {
        }

        [MethodImpl(0x8000)]
        public static ProNpcDialogInfo Convert2ProNpcDialogInfo(NpcDialogInfo returnDialog)
        {
        }

        [MethodImpl(0x8000)]
        public static ProNpcDNId Convert2ProNpcDNId(NpcDNId info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProPlayerActivityInfo Convert2ProPlayerActivityInfo(PlayerActivityInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProPlayerPVPInfo Convert2ProPlayerPVPInfo(PlayerPVPInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProPlayerSimpleInfo Convert2ProPlayerSimpleInfo(PlayerSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ProPlayerSimpleInfo> Convert2ProPlayerSimpleInfoList(List<PlayerSimpleInfo> infoList)
        {
        }

        [MethodImpl(0x8000)]
        public static ProPlayerSimplestInfo Convert2ProPlayerSimplestInfo(PlayerSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProPlayerSimplestInfo Convert2ProPlayerSimplestInfo(PlayerSimplestInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProProductionLineInfo Convert2ProProductionLineInfo(ProductionLineInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProPVPInvadeRescueInfo Convert2ProPVPInvadeRescueInfo(PVPInvadeRescueInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProPVPSignalInfo Convert2ProPVPSignalInfo(PVPSignalInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProQuestProcessingInfo Convert2ProQuestProcessingInfo(QuestProcessingInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProQuestRewardInfo Convert2ProQuestRewardInfo(this QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProQuestWaitForAcceptInfo Convert2ProQuestWaitForAcceptInfo(QuestWaitForAcceptInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRankingAllianceInfo Convert2ProRankingAllianceInfo(RankingAllianceBasicInfo allianceInfo, int score)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRankingGuildInfo Convert2ProRankingGuildInfo(RankingGuildBasicInfo guildInfo, int score)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRankingListInfo Convert2ProRankingListInfo(RankingListInfo rankingList)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRankingPlayerInfo Convert2ProRankingPlayerInfo(RankingPlayerBasicInfo playerInfo, int score)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRechargeGiftPackage Convert2ProRechargeGiftPackage(RechargeGiftPackageInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRechargeItem Convert2ProRechargeItem(RechargeItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRechargeMonthlyCard Convert2ProRechargeMonthlyCard(RechargeMonthlyCardInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProRechargeOrder Convert2ProRechargeOrder(RechargeOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProShipCustomTemplateInfo Convert2ProShipCustomTemplateInfo(ShipCustomTemplateInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProShipMoveStateSnapshot Convert2ProShipMoveStateSnapshot(SimSpaceShipStateSnapshot simSpaceShipStateSnapshot)
        {
        }

        [MethodImpl(0x8000)]
        public static ProShipSlotAmmoInfo Convert2ProShipSlotAmmoInfo(AmmoInfo ammo, int highSlotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public static ProShipSlotGroupInfo Convert2ProShipSlotGroupInfo(ShipSlotGroupInfo slotGroupInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProShipStoreItemInfo Convert2ProShipStoreItemInfo(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProShipStoreItemUpdateInfo Convert2ProShipStoreItemUpdateInfo(ShipStoreItemUpdateInfo updateInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProSignalInfo Convert2ProSignalInfo(SignalInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProSimpleItemInfoWithCount Convert2ProSimpleItemInfoWithCount(SimpleItemInfoWithCount itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ProSimpleItemInfoWithCount> Convert2ProSimpleItemInfoWithCountList(List<SimpleItemInfoWithCount> itemInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public static ProSovereignLostReport Convert2ProSovereignLostReport(SovereignLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public static ProStarMapAllianceSimpleInfo Convert2ProStarMapAllianceSimpleInfo(StarMapAllianceSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProStarMapGuildOccupyDynamicInfo Convert2ProStarMapGuildOccupyDynamicInfo(StarMapGuildOccupyDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProStarMapGuildOccupyStaticInfo Convert2ProStarMapGuildOccupyStaticInfo(StarMapGuildOccupyStaticInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProStarMapGuildSimpleInfo Convert2ProStarMapGuildSimpleInfo(StarMapGuildSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProStoreItemInfo Convert2ProStoreItemInfo(StoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProStoreItemTransformInfo Convert2ProStoreItemTransformInfo(StoreItemTransformInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProStoreItemUpdateInfo Convert2ProStoreItemUpdateInfo(this StoreItemUpdateInfo updateInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ProTechUpgradeInfo Convert2ProTechUpgradeInfo(TechUpgradeInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProTradeListItemInfo Convert2ProTradeListItemInfo(TradeListItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProTradeNpcShopItemInfo Convert2ProTradeNpcShopItemInfo(TradeNpcShopItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProTradeNpcShopSaleItemInfo Convert2ProTradeNpcShopSaleItemInfo(TradeNpcShopSaleItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ProVectorDouble Convert2ProVectorDouble(Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static ProVectorUInt32 Convert2ProVectorUInt32(Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static PVector3D Convert2PVector3D(Vector3D vector)
        {
        }

        [MethodImpl(0x8000)]
        public static PVPInvadeRescueInfo Convert2PVPInvadeRescueInfo(ProPVPInvadeRescueInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static PVPSignalInfo Convert2PVPSignalInfo(ProPVPSignalInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static QuestProcessingInfo Convert2QuestProcessingInfo(ProQuestProcessingInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static QuestRewardInfo Convert2QuestRewardInfo(this ProQuestRewardInfo pro)
        {
        }

        [MethodImpl(0x8000)]
        public static QuestWaitForAcceptInfo Convert2QuestWaitForAcceptInfo(ProQuestWaitForAcceptInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RankingAllianceBasicInfo Convert2RankingAllianceBasicInfo(ProRankingAllianceInfo proAllianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RankingGuildBasicInfo Convert2RankingGuildBasicInfo(ProRankingGuildInfo proGuildInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RankingListInfo Convert2RankingListInfo(ProRankingListInfo proRankingList)
        {
        }

        [MethodImpl(0x8000)]
        public static AuctionOrderBriefInfo Convert2RankingPlayerBasicInfo(ProAuctionOrderBriefInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RankingPlayerBasicInfo Convert2RankingPlayerBasicInfo(ProRankingPlayerInfo proPlayerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RechargeGiftPackageInfo Convert2RechargeGiftPackage(ProRechargeGiftPackage proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RechargeItemInfo Convert2RechargeItem(ProRechargeItem proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RechargeMonthlyCardInfo Convert2RechargeMonthlyCard(ProRechargeMonthlyCard proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static RechargeOrderInfo Convert2RechargeOrder(ProRechargeOrder proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int Convert2SceneLevelByQuestLevel(int questLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static int Convert2SceneLevelBySignalLevel(int signalLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static int Convert2SceneLevelBySolarSystemSignalDifficult(int difficultLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipCustomTemplateInfo Convert2ShipCustomTemplateInfo(ProShipCustomTemplateInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static SpaceShipPositionInfo Convert2ShipPositionInfo(ProVectorDouble location, ProVectorDouble rotation, ShipPoseRequest poseReq)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipSlotGroupInfo Convert2ShipSlotGroupInfo(ProShipSlotGroupInfo proGroupInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipStoreItemInfo Convert2ShipStoreItemInfo(ProShipStoreItemInfo proItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ShipStoreItemInfo> Convert2ShipStoreItemInfoList(List<ProShipStoreItemInfo> proItemList)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipStoreItemUpdateInfo Convert2ShipStoreItemUpdateInfo(ProShipStoreItemUpdateInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static SignalInfo Convert2SignalInfo(ProSignalInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static SimMoveableSpaceObjectStateSnapshot Convert2SimMoveableSpaceObjectStateSnapshot(ProBaseMoveStateSnapshot proBaseMoveStateSnapshot)
        {
        }

        [MethodImpl(0x8000)]
        public static SimpleItemInfoWithCount Convert2SimpleItemInfoWithCount(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static SimpleItemInfoWithCount Convert2SimpleItemInfoWithCount(ProSimpleItemInfoWithCount proItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<SimpleItemInfoWithCount> Convert2SimpleItemInfoWithCountList(List<ProSimpleItemInfoWithCount> proItemInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public static SimSpaceShipStateSnapshot Convert2SimSpaceShipStateSnapshot(ProShipMoveStateSnapshot proShipMoveStateSnapshot)
        {
        }

        [MethodImpl(0x8000)]
        public static SovereignLostReport Convert2SovereignLostReport(ProSovereignLostReport proReport)
        {
        }

        [MethodImpl(0x8000)]
        public static StarMapAllianceSimpleInfo Convert2StarMapAllianceSimpleInfo(ProStarMapAllianceSimpleInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static StarMapGuildOccupyDynamicInfo Convert2StarMapGuildOccupyDynamicInfo(ProStarMapGuildOccupyDynamicInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static StarMapGuildOccupyStaticInfo Convert2StarMapGuildOccupyStaticInfo(ProStarMapGuildOccupyStaticInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static StarMapGuildSimpleInfo Convert2StarMapGuildSimpleInfo(ProStarMapGuildSimpleInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static StoreItemInfo Convert2StoreItemInfo(ProStoreItemInfo proItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static StoreItemTransformInfo Convert2StoreItemTransformInfo(ProStoreItemTransformInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static StoreItemUpdateInfo Convert2StoreItemUpdateInfo(ProStoreItemUpdateInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static TechUpgradeInfo Convert2TechUpgradeInfo(ProTechUpgradeInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static TradeListItemInfo Convert2TradeListItemInfo(ProTradeListItemInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static TradeNpcShopItemInfo Convert2TradeNpcShopItemInfo(ProTradeNpcShopItemInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static TradeNpcShopSaleItemInfo Convert2TradeNpcShopSaleItemInfo(ProTradeNpcShopSaleItemInfo proInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void ConvertDoubleToInt32(double src, out int fractionPart, out int expPart)
        {
        }

        [MethodImpl(0x8000)]
        public static double ConvertInt32ToDouble(int fractionPart, int expPart)
        {
        }

        [MethodImpl(0x8000)]
        public static void ConvertMineral2ZipMineralForDrop(ItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public static CollisionInfo Copy3DCollisionInfo(CollisionInfo collisionInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CreateFormationInfoByGuildFleetFormationConfId(int confId, out FormationType formationType, out FormationCreatingInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static int DescendTimeCallBack(DateTime lhs, DateTime rhs)
        {
        }

        [MethodImpl(0x8000)]
        public static AgeType GetAgeType(int age)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetAmmoArtResFullPath(string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static StoreItemType GetAmmoTypeByWeaponConfInfo(ConfigDataWeaponInfo weaponInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetAudioResFullPath(string audioResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetCharBasePropertyIdByPropertyCategory(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCircularIconSpriteResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static CollisionInfo GetCollisionAfterRotation(CollisionInfo collisionInfo, Vector3D rotation, float scale)
        {
        }

        [MethodImpl(0x8000)]
        public static ConstellationType GetConstellation(DateTime birthDay)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCrackBoxArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCutSceneArtResFullPath(string resPath)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetDefaultNpcGuildIdByCulture(int cultureId)
        {
        }

        [MethodImpl(0x8000)]
        public static DiplomacyType GetDiplomacyType2Player(DiplomacyInfo diplomacyInfo, string gameUserId, uint guildId = 0, uint allienceId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEditorSceneArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipArtResFullPath(string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFlourishCurrValueByLevel(int currTotalValue, int flourishLevel, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFlourishLevelByValue(int value, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFlourishMaxValue(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFlourishNeedValueByLevel(int flourishLevel, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetGrandFactionNameForShipWeapon(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataGuildBuildingLevelDetailInfo GetGuildBuildingLevelDetailConf(GuildBuildingType buildingType, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D GetGuildBuildingLocation(Random rand, GDBSolarSystemInfo solarSystemInfo, GuildBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public static List<FleetPosition> GetGuildFleetPositionListByFlag(uint fleetPositionFlag)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetIconSpriteResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D GetLooseRandomLocationForDynamicScene(GDBSolarSystemInfo solarSystemInfo, IConfigDataLoader configDataLoader, ref DynamicSceneLocationType locationType, Random rand, ref DynamicSceneNearCelestialType nearCelestialType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNormalAmmoTypeDescStringKey(WeaponCategory weaponCat)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcBodySpriteResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcNameByNameId(int firstNameId, int lastNameId, GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcNameByNpcDNId(NpcDNId npc)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetOtherArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPlanetArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPlanetName(GDBSolarSystemInfo solarSystemInfo, int planetId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPrdouce3DBgArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetQuestLevelBySignalDifficult(int signalDifficult)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetRandomDisruptionList<T>(ref List<T> list, Random random)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetRandomItemIndexByWeightList(Random random, List<int> weightList)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetRandomItemInWeightPool(List<IntWeightListItem> weightList, Random random)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetRandomItemInWeightPool(List<IdWeightInfo> weightList, Random random)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D GetRandomLocationForDynamicScene(GDBSolarSystemInfo solarSystemInfo, IConfigDataLoader configDataLoader, DynamicSceneLocationType locationType, Random rand, DynamicSceneNearCelestialType nearCelestialType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D GetRandomLocationNearPlanet(Random rand, GDBSolarSystemInfo solarSystemInfo, DynamicSceneNearCelestialType nearType)
        {
        }

        [MethodImpl(0x8000)]
        private static GDBPlanetInfo GetRandomPlanetFromSolarSystemInfo(Random rand, GDBSolarSystemInfo solarSystemInfo, Func<GDBPlanetInfo, bool> condition)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D GetRandomPostionInSphere(Vector3D srcPostion, int radius, Random random)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3D GetRandomStandaloneLocation(Random rand)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetRankAndSubRank(StoreItemType itemType, int itemId, out RankType rankType, out SubRankType subRankType)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetRomanCharacterStringByInt(int number)
        {
        }

        [MethodImpl(0x8000)]
        public static double GetRotationDouble(uint rotationInt)
        {
        }

        [MethodImpl(0x8000)]
        public static ushort GetRotationUInt16(double rotationValue)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSceneAnimationResFullPath(string resPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipArtLOD0ResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipEffectArtResFullPath(string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipLevelDesc()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetSignalDifficultByQuestLevel(int questLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSimpleObjectArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSkillTreeArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataSolarSystemDelegateMissionPoolCountInfo GetSolarSystemDelegateMissionPoolInfoByPoolCount(int poolCount, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetSolarSystemDifficultLevel(int solarSystemId, GDBSolarSystemInfo solarSystemConf = null)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemName(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemName(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemName(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataSolarSystemQuestPoolCountInfo GetSolarSystemQuestPoolInfoByPoolCount(int poolCount, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetSolarSystemSecurityLevel(int solarSystemId, GDBSolarSystemInfo solarSystemConf = null)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetSolarSystemSecurityLevel(int solarSystemId, GDBSolarSystemSimpleInfo solarSystemConf)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSpaceStationArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSpaceStationName(GDBSolarSystemInfo solarSystemInfo, int spaceStationId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSquareIconSpriteResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStarGateArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStargateName(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static object GetStoreItemConfigData(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetStoreItemSalePrice(StoreItemType itemType, int itemId, long count = 1L)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringKeyInStringTableWithId(StringTableId id)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetTimeReduceByGuildProductionSpeedUpLevel(int level)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetTradeMoneyCostByGuildProductionSpeedUpLevel(int level)
        {
        }

        [MethodImpl(0x8000)]
        private static Vector3D GetUniversePosForPlanetByNearCelestialType(Random rand, GDBPlanetInfo planetInfo, DynamicSceneNearCelestialType nearCelestialType)
        {
        }

        [MethodImpl(0x8000)]
        public static T GetValueFromListByIndex<T>(List<T> list, int index)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWeaponArtResFullPath(string resPrefix, GrandFaction grandFaction, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFightBuf(ConfigDataBufInfo bufInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFightBuf(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsGlobalSceneAlwaysVisible(GlobalSceneInfo scene)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsGuildBuildingLevelMax(GuildBuildingType buildingType, int level)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsHighSafetySolarSystem(int solarSystemId, GDBSolarSystemSimpleInfo conf = null)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsMatchShipGrandFactionLimitForPlayerShip(List<GrandFaction> shipGrandFactionLimit, GrandFaction currGrandFaction)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsMatchShipTypeLimitForHiredCaptainShip(List<ShipType> shipTypeLimit, ShipType currShipType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsMatchShipTypeLimitForPlayerShip(List<ShipType> shipTypeLimit, ShipType currShipType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsMatchShipTypeLimitForWingManShip(List<ShipType> shipTypeLimit, ShipType wingManShipType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPoliceNpcShipType(ConfigDataNpcMonsterInfo npcMonsterConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSensitiveWord(string sentence, bool isForExternal)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsTimeOnPlayerDailyRefreshCycle(DateTime firstTime, DateTime secondTime)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsWormholeSolarSystem(int solarSystemId, GDBSolarSystemSimpleInfo conf = null)
        {
        }

        [MethodImpl(0x8000)]
        public static string ReplaceSensitiveWord(string sentence, bool isForExternal, char replaceChar = '*')
        {
        }

        [MethodImpl(0x8000)]
        public static IEnumerable<T> SolarSystemIdListInRange<T>(int centerSolaySystemId, float Range, bool includeSelf, Func<GDBSolarSystemSimpleInfo, T> selector)
        {
        }

        [MethodImpl(0x8000)]
        public static bool TestBufGamePlayFlag(ConfigDataBufInfo bufConfInfo, BufGamePlayFlag flag)
        {
        }

        [CompilerGenerated]
        private sealed class <GetRandomLocationForDynamicScene>c__AnonStorey3
        {
            internal double minRadius;
            internal double maxRadius;

            internal bool <>m__0(GDBPlanetInfo planet) => 
                ((planet.OrbitRadius >= this.minRadius) && (planet.OrbitRadius <= this.maxRadius));
        }

        [CompilerGenerated]
        private sealed class <IsMatchShipGrandFactionLimitForPlayerShip>c__AnonStorey1
        {
            internal GrandFaction currGrandFaction;

            internal bool <>m__0(GrandFaction e) => 
                (e == this.currGrandFaction);
        }

        [CompilerGenerated]
        private sealed class <IsMatchShipTypeLimitForHiredCaptainShip>c__AnonStorey2
        {
            internal ShipType currShipType;

            internal bool <>m__0(ShipType e) => 
                (e == this.currShipType);
        }

        [CompilerGenerated]
        private sealed class <IsMatchShipTypeLimitForPlayerShip>c__AnonStorey0
        {
            internal ShipType currShipType;

            internal bool <>m__0(ShipType e) => 
                (e == this.currShipType);
        }

        [CompilerGenerated]
        private sealed class <SolarSystemIdListInRange>c__AnonStorey4<T>
        {
            internal GDBSolarSystemSimpleInfo centerSolarSystem;
            internal float sqrRange;
            internal int centerSolaySystemId;
            internal bool includeSelf;
            internal Func<GDBSolarSystemSimpleInfo, T> selector;

            internal bool <>m__0(KeyValuePair<int, GDBSolarSystemSimpleInfo> kv)
            {
                int num1;
                if (kv.Value.IsHide || ((System.Math.Pow((double) (kv.Value.GLocationX - this.centerSolarSystem.GLocationX), 2.0) + System.Math.Pow((double) (kv.Value.GLocationZ - this.centerSolarSystem.GLocationZ), 2.0)) > this.sqrRange))
                {
                    num1 = 0;
                }
                else
                {
                    num1 = (kv.Key != this.centerSolaySystemId) ? 1 : ((int) this.includeSelf);
                }
                return (bool) num1;
            }

            internal T <>m__1(KeyValuePair<int, GDBSolarSystemSimpleInfo> kv) => 
                this.selector(kv.Value);
        }
    }
}

