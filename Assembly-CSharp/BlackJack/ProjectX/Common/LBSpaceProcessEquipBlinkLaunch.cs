﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessEquipBlinkLaunch : LBSpaceProcessEquipLaunchBase
    {
        protected float m_blinkDistance;
        protected uint m_blinkBeginEffectTime;
        protected const string BlinkBeginEffectName = "BlinkBegin";
        protected const uint BlinkBeginEffectLifeTime = 0x3e8;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetBlinkDistance;
        private static DelegateBridge __Hotfix_GetBlinkDistance;
        private static DelegateBridge __Hotfix_OnEquipFire;
        private static DelegateBridge __Hotfix_SetChargeTime;
        private static DelegateBridge __Hotfix_OnBlinkEnd;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetProcessSource;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipBlinkLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipBlinkLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int groupIndex, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public float GetBlinkDistance()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceProcessEquipLaunchSource GetProcessSource()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int groupIndex, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBlinkEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEquipFire(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBlinkDistance(float blinkDistance)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetChargeTime(ushort chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

