﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompIFF4Player : LogicBlockShipCompIFFBase
    {
        protected Dictionary<int, float> m_initFactionCreditMap;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_InitFactionCredit;
        private static DelegateBridge __Hotfix_GetFactionId;
        private static DelegateBridge __Hotfix_GetParentFactionId;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4Player;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4HiredCaptain;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4NpcShip;
        private static DelegateBridge __Hotfix_OnKillNpcShip;
        private static DelegateBridge __Hotfix_OnFactionCreditLevelChanged;

        [MethodImpl(0x8000)]
        public override int GetFactionId()
        {
        }

        [MethodImpl(0x8000)]
        protected override IFFState GetIFFStateForTargetInternal4HiredCaptain(ILBInSpaceNpcShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public override IFFState GetIFFStateForTargetInternal4NpcShip(ILBInSpaceNpcShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override IFFState GetIFFStateForTargetInternal4Player(ILBInSpacePlayerShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetParentFactionId()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitFactionCredit()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBInSpaceShip ownerShip, Dictionary<int, float> factionCreditMap)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFactionCreditLevelChanged(int factionId, float credit)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillNpcShip(ILBInSpaceNpcShip ship)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }
    }
}

