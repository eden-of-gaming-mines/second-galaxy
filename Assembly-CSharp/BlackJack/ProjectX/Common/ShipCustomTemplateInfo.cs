﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class ShipCustomTemplateInfo
    {
        public int m_index;
        public int m_shipConfId;
        public string m_name;
        public bool m_canBeDelete;
        public List<ConfHighSlotInfo> m_highSlot;
        public List<int> m_middleSlot;
        public List<int> m_lowSlot;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

