﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LogicBlockObjCompDropBoxBase
    {
        protected string m_name;
        protected RankType m_rank;
        protected SubRankType m_subRank;
        protected ILBSpaceDropBox m_ownerBox;
        protected DropInfo m_info;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_InitBoxRank;
        private static DelegateBridge __Hotfix_IsAvailable4Player;
        private static DelegateBridge __Hotfix_GetDropItemList4Player;
        private static DelegateBridge __Hotfix_GetDropInfo;
        private static DelegateBridge __Hotfix_ReInitDropInfoByAttenuationFactor;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetSubRank;
        private static DelegateBridge __Hotfix_PickItem;
        private static DelegateBridge __Hotfix_ClearDropItem4Player;

        [MethodImpl(0x8000)]
        protected LogicBlockObjCompDropBoxBase()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearDropItem4Player(string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public DropInfo GetDropInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetDropItemList4Player(string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public SubRankType GetSubRank()
        {
        }

        protected abstract void InitBoxName();
        [MethodImpl(0x8000)]
        private void InitBoxRank()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(DropInfo info, ILBSpaceDropBox owner)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvailable4Player(string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PickItem(StoreItemType itemType, int itemId, long count, bool isBind, string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void ReInitDropInfoByAttenuationFactor(float attenuationFactor)
        {
        }
    }
}

