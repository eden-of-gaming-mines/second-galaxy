﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class PlayerSimplestInfo
    {
        public string m_gameUserId;
        public string m_name;
        public int m_level;
        public int m_avatarId;
        public ProfessionType m_profession;
        public int m_evaluateScore;
        public uint m_guildId;
        public string m_guildName;
        public string m_guildCodeName;
        public uint m_allianceId;
        public string m_allianceName;
        [NonSerialized]
        public DateTime m_CachedTime;
        [NonSerialized]
        public int m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

