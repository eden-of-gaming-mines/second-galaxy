﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildSimpleRuntimeBase
    {
        GuildSimpleRuntimeInfo GetGuildSimpleRuntimeInfo();
        GuildSimplestInfo GetSimplestInfo();
        void UpdateSimplestInfo();
    }
}

