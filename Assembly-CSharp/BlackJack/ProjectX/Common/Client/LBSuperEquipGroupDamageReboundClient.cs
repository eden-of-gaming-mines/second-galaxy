﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperEquipGroupDamageReboundClient : LBSuperEquipGroupDamageReboundBase, ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch4OpenShield;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunchForAoe;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartCharge4OpenShield;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartChargeForAoe;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire4OpenShield;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, StructProcessSuperEquipAoeLaunch> EventOnProcessFireForAoe;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel4OpenShield;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancelForAoe;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd4OpenShield;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEndForAoe;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch4OpenShield;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch4ReboundDamage;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessFire;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessEffect;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessCancel;
        private static DelegateBridge __Hotfix_OnReboundShieldTimeOut;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunch4OpenShield;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunch4OpenShield;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunchForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunchForAoe;
        private static DelegateBridge __Hotfix_add_EventOnStartCharge4OpenShield;
        private static DelegateBridge __Hotfix_remove_EventOnStartCharge4OpenShield;
        private static DelegateBridge __Hotfix_add_EventOnStartChargeForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnStartChargeForAoe;
        private static DelegateBridge __Hotfix_add_EventOnProcessFire4OpenShield;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFire4OpenShield;
        private static DelegateBridge __Hotfix_add_EventOnProcessFireForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFireForAoe;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel4OpenShield;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel4OpenShield;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancelForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancelForAoe;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEnd4OpenShield;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEnd4OpenShield;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEndForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEndForAoe;

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch4OpenShield
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunchForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel4OpenShield
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancelForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd4OpenShield
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEndForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire4OpenShield
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, StructProcessSuperEquipAoeLaunch> EventOnProcessFireForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartCharge4OpenShield
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartChargeForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupDamageReboundClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAttachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnReboundShieldTimeOut(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch4OpenShield(LBSpaceProcessEquipAttachBufLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch4ReboundDamage(LBSpaceProcessSuperEquipAoeLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

