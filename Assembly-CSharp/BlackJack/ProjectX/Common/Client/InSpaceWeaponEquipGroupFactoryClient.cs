﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public static class InSpaceWeaponEquipGroupFactoryClient
    {
        private static DelegateBridge __Hotfix_CreateInSpaceGroup;
        private static DelegateBridge __Hotfix_CreateInSpaceWeaponGroup;
        private static DelegateBridge __Hotfix_CreateInSpaceEquipGroup;
        private static DelegateBridge __Hotfix_CreateSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CreateSuperEquipGroup;
        private static DelegateBridge __Hotfix_CreateNormalAttackGroup;
        private static DelegateBridge __Hotfix_CreateTacticalEquipGroup;

        [MethodImpl(0x8000)]
        private static LBEquipGroupBase CreateInSpaceEquipGroup(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticGroup)
        {
        }

        [MethodImpl(0x8000)]
        public static LBInSpaceWeaponEquipGroupBase CreateInSpaceGroup(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticGroup)
        {
        }

        [MethodImpl(0x8000)]
        private static LBWeaponGroupBase CreateInSpaceWeaponGroup(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticGroup)
        {
        }

        [MethodImpl(0x8000)]
        public static LBWeaponGroupNormalAttackClient CreateNormalAttackGroup(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup normalAttackStaticGroup)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSuperEquipGroupBase CreateSuperEquipGroup(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSuperWeaponGroupBase CreateSuperWeaponGroup(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public static LBTacticalEquipGroupBase CreateTacticalEquipGroup(ILBInSpaceShip ownerShip)
        {
        }
    }
}

