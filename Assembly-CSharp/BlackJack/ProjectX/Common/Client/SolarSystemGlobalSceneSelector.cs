﻿namespace BlackJack.ProjectX.Common.Client
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public abstract class SolarSystemGlobalSceneSelector
    {
        protected LinkedList<GlobalSceneInfo> m_selectedGlobalSceneList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetSelectedGlobalSceneList;
        private static DelegateBridge __Hotfix_ClearSelectedGlobalSceneList;

        [MethodImpl(0x8000)]
        protected SolarSystemGlobalSceneSelector()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearSelectedGlobalSceneList()
        {
        }

        [MethodImpl(0x8000)]
        public LinkedList<GlobalSceneInfo> GetSelectedGlobalSceneList()
        {
        }

        public abstract bool IsPermissiveToSelectGlobalScenes();
        public abstract void OnGlobalSceneRemove(uint sceneObjId);
        public abstract void TryToSelectGlobalScenes(LinkedList<GlobalSceneInfo> srcGlobalSceneList);
    }
}

