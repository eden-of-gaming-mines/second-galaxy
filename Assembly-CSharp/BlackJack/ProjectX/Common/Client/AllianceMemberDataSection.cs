﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AllianceMemberDataSection
    {
        public AllianceMemberInfo m_member;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateMemberGuildSimplestInfo;
        private static DelegateBridge __Hotfix_UpdateLeaveTimeAndReason;
        private static DelegateBridge __Hotfix_EraseAllianceId;
        private static DelegateBridge __Hotfix_UpdateMemberEvaluateScore;

        [MethodImpl(0x8000)]
        public AllianceMemberDataSection(AllianceMemberInfo member)
        {
        }

        [MethodImpl(0x8000)]
        public void EraseAllianceId()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLeaveTimeAndReason(GuildAllianceLeaveReason reason)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemberEvaluateScore(int value)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemberGuildSimplestInfo(GuildSimplestInfo info)
        {
        }
    }
}

