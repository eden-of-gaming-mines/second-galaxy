﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class LBStaticShipClientBase : LBStaticShipBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetDefaultAroundRadius;
        private static DelegateBridge __Hotfix_GetBufFactory;
        private static DelegateBridge __Hotfix_GetLBResouce;

        [MethodImpl(0x8000)]
        protected LBStaticShipClientBase(IShipDataContainer shipDC, int hangarIndex, ILBShipCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public override ILBBuffFactory GetBufFactory()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetDefaultAroundRadius(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual LogicBlockShipCompStaticResource GetLBResouce()
        {
        }
    }
}

