﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBWeaponGroupRailgunClient : LBWeaponGroupRailgunBase, ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBWeaponGroupBase, LBSpaceProcessBulletGunLaunch> EventOnLaunchCancel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBWeaponGroupBase, LBSpaceProcessBulletGunLaunch, ushort> EventOnUnitStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBWeaponGroupBase, LBSpaceProcessBulletGunLaunch, LBSpaceProcessBulletFly, ushort> EventOnUnitFire;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupBase> EventOnReloadAmmoStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupBase, ItemInfo> EventOnReloadAmmoEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventReloadAmmoStart;
        private static DelegateBridge __Hotfix_OnSyncEventReloadAmmoEnd;
        private static DelegateBridge __Hotfix_GetWeaponTransverseVelocity;
        private static DelegateBridge __Hotfix_TryStartReloadAmmo;
        private static DelegateBridge __Hotfix_OnReloadAmmoCDEnd;
        private static DelegateBridge __Hotfix_GetGroupCD;
        private static DelegateBridge __Hotfix_OnProcessUnitStartCharge;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_CreateBulletFlyProcess;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnUnitFire;
        private static DelegateBridge __Hotfix_remove_EventOnUnitFire;
        private static DelegateBridge __Hotfix_add_EventOnReloadAmmoStart;
        private static DelegateBridge __Hotfix_remove_EventOnReloadAmmoStart;
        private static DelegateBridge __Hotfix_add_EventOnReloadAmmoEnd;
        private static DelegateBridge __Hotfix_remove_EventOnReloadAmmoEnd;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunch;

        public event Action<LBWeaponGroupBase, LBSpaceProcessBulletGunLaunch> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupBase, ItemInfo> EventOnReloadAmmoEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupBase> EventOnReloadAmmoStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupBase, LBSpaceProcessBulletGunLaunch, LBSpaceProcessBulletFly, ushort> EventOnUnitFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupBase, LBSpaceProcessBulletGunLaunch, ushort> EventOnUnitStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupRailgunClient(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBSpaceProcessBulletFly CreateBulletFlyProcess(LBSpaceProcessBulletGunLaunch bulletGunLaunchProcess, LBProcessSingleUnitLaunchInfo unitLaunchInfo)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetWeaponTransverseVelocity()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort bulletIndex)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnProcessUnitStartCharge(LBSpaceProcessWeaponLaunchBase process, ushort bulletIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnReloadAmmoCDEnd()
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessBulletGunLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventReloadAmmoEnd(ItemInfo reloadAmmoItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventReloadAmmoStart(uint startTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool TryStartReloadAmmo(uint startTime)
        {
        }
    }
}

