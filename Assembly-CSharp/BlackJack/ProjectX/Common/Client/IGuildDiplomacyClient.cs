﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using System;

    public interface IGuildDiplomacyClient : IGuildDiplomacyBase
    {
        void OnGuildDiplomacyUpdateNtf(GuildDiplomacyUpdateNtf ntf);
    }
}

