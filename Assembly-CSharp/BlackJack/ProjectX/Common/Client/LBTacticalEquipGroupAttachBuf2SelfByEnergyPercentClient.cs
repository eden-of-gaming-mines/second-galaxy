﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBTacticalEquipGroupAttachBuf2SelfByEnergyPercentClient : LBTacticalEquipGroupAttachBuf2SelfByEnergyPercentBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, float, int> EventEffectChange;
        protected LBBufBase m_currBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnDetachBuf;
        private static DelegateBridge __Hotfix_GetBufFirstModifyPropertyValue;
        private static DelegateBridge __Hotfix_add_EventEffectChange;
        private static DelegateBridge __Hotfix_remove_EventEffectChange;

        public event Action<bool, float, int> EventEffectChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupAttachBuf2SelfByEnergyPercentClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public float GetBufFirstModifyPropertyValue(int bufConfId, out int pid)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDetachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

