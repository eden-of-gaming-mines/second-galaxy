﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompItemStoreClient : LogicBlockShipCompItemStoreBase
    {
        public DateTime LastItemStoreUpdateTime;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<List<ShipStoreItemInfo>> EventOnSyncEventShipItemStoreAddItem;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<List<ShipStoreItemInfo>> EventOnSyncEventShipItemStoreRemoveItem;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitializeShipStoreItem;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_TransformItemFromMSStore2Ship;
        private static DelegateBridge __Hotfix_TransformAllItemFromShip2MSStore;
        private static DelegateBridge __Hotfix_TransformItemFromShip2MSStore;
        private static DelegateBridge __Hotfix_TransformItemFromShip2MSStoreImplement;
        private static DelegateBridge __Hotfix_AddItemAutoOverlay;
        private static DelegateBridge __Hotfix_UpdateItemByStoreItemInfo_1;
        private static DelegateBridge __Hotfix_UpdateItemByStoreItemInfo_0;
        private static DelegateBridge __Hotfix_RemoveItem_2;
        private static DelegateBridge __Hotfix_RemoveItem_1;
        private static DelegateBridge __Hotfix_RemoveItem_0;
        private static DelegateBridge __Hotfix_HangarShipStoreItemRemove;
        private static DelegateBridge __Hotfix_HangarShipStoreItemListRemove;
        private static DelegateBridge __Hotfix_ShipStoreItemSale;
        private static DelegateBridge __Hotfix_InspaceShipStoreItemRemove;
        private static DelegateBridge __Hotfix_RemoveItemImpl;
        private static DelegateBridge __Hotfix_CreateLBShipStoreItemByShipStoreItemInfo;
        private static DelegateBridge __Hotfix_FixStoreItemIndex;
        private static DelegateBridge __Hotfix_OnSyncEventShipItemStoreAddItem;
        private static DelegateBridge __Hotfix_OnSyncEventShipItemStoreRemoveItem;
        private static DelegateBridge __Hotfix_RefreshLastItemStoreUpdateTime;
        private static DelegateBridge __Hotfix_get_m_lbItemStore;
        private static DelegateBridge __Hotfix_GetFilteredItemList;
        private static DelegateBridge __Hotfix_add_EventOnSyncEventShipItemStoreAddItem;
        private static DelegateBridge __Hotfix_remove_EventOnSyncEventShipItemStoreAddItem;
        private static DelegateBridge __Hotfix_add_EventOnSyncEventShipItemStoreRemoveItem;
        private static DelegateBridge __Hotfix_remove_EventOnSyncEventShipItemStoreRemoveItem;

        public event Action<List<ShipStoreItemInfo>> EventOnSyncEventShipItemStoreAddItem
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<ShipStoreItemInfo>> EventOnSyncEventShipItemStoreRemoveItem
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool AddItemAutoOverlay(StoreItemType itemType, int configId, long count, int storeItemIndex, bool isBind, ulong instanceId, out LBShipStoreItemClient destShipItem)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBShipStoreItemBase CreateLBShipStoreItemByShipStoreItemInfo(ShipStoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void FixStoreItemIndex(LBShipStoreItemClient destItem, int storeItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStoreItemClient> GetFilteredItemList(List<Func<ILBStoreItemClient, bool>> filterlist)
        {
        }

        [MethodImpl(0x8000)]
        public bool HangarShipStoreItemListRemove(List<int> storeItemsIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool HangarShipStoreItemRemove(int storeItemIndex, long storeItemCount)
        {
        }

        [MethodImpl(0x8000)]
        public bool InitializeShipStoreItem(List<ShipStoreItemInfo> shipStoreItemList)
        {
        }

        [MethodImpl(0x8000)]
        public bool InspaceShipStoreItemRemove(StoreItemType storeItemType, int storeItemId, bool isBind, bool isFreezing, long storeItemCount, out bool isDeleteSrcItem)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventShipItemStoreAddItem(List<ShipStoreItemInfo> shipItemList)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventShipItemStoreRemoveItem(List<ShipStoreItemInfo> shipItemList)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void RefreshLastItemStoreUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveItem(ItemInfo itemInfo, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveItem(StoreItemType itemType, int configId, long count)
        {
        }

        [MethodImpl(0x8000)]
        public override bool RemoveItem(StoreItemType itemType, int configId, long count, out int errCode, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RemoveItemImpl(LBShipStoreItemBase destItem, long count, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool ShipStoreItemSale(List<ItemInfo> itemList, out double price)
        {
        }

        [MethodImpl(0x8000)]
        public bool TransformAllItemFromShip2MSStore(List<StoreItemTransformInfo> resumedItemInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public bool TransformItemFromMSStore2Ship(LBStoreItem srcMSStoreItem, long count, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBShipStoreItemClient destShipStoreItem, out bool isDeleteSrcItem)
        {
        }

        [MethodImpl(0x8000)]
        public bool TransformItemFromShip2MSStore(LBShipStoreItemClient srcShipStoreItem, long count, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem destMSStoreItem, out bool isDeleteSrcItem)
        {
        }

        [MethodImpl(0x8000)]
        protected void TransformItemFromShip2MSStoreImplement(LBShipStoreItemClient srcShipStoreItem, long count, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem destMSStoreItem, out bool isDeleteSrcItem)
        {
        }

        [MethodImpl(0x8000)]
        protected bool UpdateItemByStoreItemInfo(ShipStoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool UpdateItemByStoreItemInfo(ShipStoreItemUpdateInfo itemUpdateInfo)
        {
        }

        protected LogicBlockItemStoreClient m_lbItemStore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

