﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompBasicLogicClient : GuildCompBasicLogicBase, IGuildCompBasicLogicClient, IGuildCompBasicLogicBase, IGuildBasicLogicClient, IGuildBasicLogicBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnGuildChangeBasicInfoNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeManifestoNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeAnnouncementNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeNameAndCodeNtf;
        private static DelegateBridge __Hotfix_OnGuildBaseSolarSystemSetAck;
        private static DelegateBridge __Hotfix_OnGuildLogoInfoNtf;
        private static DelegateBridge __Hotfix_OnSetDismissEndTime;
        private static DelegateBridge __Hotfix_UpdateGuildBasicInfo;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompBasicLogicClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBaseSolarSystemSetAck(int solarSystemId, int stationId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeAnnouncementNtf(string announcement, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeBasicInfoNtf(GuildAllianceLanguageType languageType, GuildJoinPolicy joinPolicy, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeManifestoNtf(string manifesto, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeNameAndCodeNtf(string name, string code)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildLogoInfoNtf(GuildLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSetDismissEndTime(long dismissEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBasicInfo(GuildBasicInfo info)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

