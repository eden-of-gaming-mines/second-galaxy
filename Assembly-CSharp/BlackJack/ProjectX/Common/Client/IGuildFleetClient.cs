﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildFleetClient : IGuildFleetBase
    {
        GuildFleetMemberInfo GetGuildFleetMemberInfo(ulong fleetInsId, string gameUserId);
    }
}

