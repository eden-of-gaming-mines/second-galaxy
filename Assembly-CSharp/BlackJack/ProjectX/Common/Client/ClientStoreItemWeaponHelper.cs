﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    internal class ClientStoreItemWeaponHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetWeaponCategory_0;
        private static DelegateBridge __Hotfix_GetWeaponCategory_1;
        private static DelegateBridge __Hotfix_GetAmmoDescStrKey;
        private static DelegateBridge __Hotfix_GetAssembleTechRequirement_0;
        private static DelegateBridge __Hotfix_GetAssembleTechRequirement_1;
        private static DelegateBridge __Hotfix_GetAssembleCPUCost_0;
        private static DelegateBridge __Hotfix_GetAssembleCPUCost_1;
        private static DelegateBridge __Hotfix_GetAssemblePowerCost_0;
        private static DelegateBridge __Hotfix_GetAssemblePowerCost_1;
        private static DelegateBridge __Hotfix_GetSlotTypeStringKey_0;
        private static DelegateBridge __Hotfix_GetSlotTypeStringKey_1;
        private static DelegateBridge __Hotfix_GetUnitCount_0;
        private static DelegateBridge __Hotfix_GetUnitCount_1;
        private static DelegateBridge __Hotfix_GetWaveGroupDamage_0;
        private static DelegateBridge __Hotfix_GetWaveGroupDamage_1;

        [MethodImpl(0x8000)]
        public static string GetAmmoDescStrKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetAssembleCPUCost(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetAssembleCPUCost(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetAssemblePowerCost(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetAssemblePowerCost(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetAssembleTechRequirement(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetAssembleTechRequirement(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSlotTypeStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSlotTypeStringKey(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetUnitCount(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetUnitCount(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetWaveGroupDamage(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetWaveGroupDamage(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static WeaponCategory GetWeaponCategory(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static WeaponCategory GetWeaponCategory(StoreItemType itemType, object confInfo)
        {
        }
    }
}

