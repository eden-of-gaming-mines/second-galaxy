﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockDelegateMissionClient : LogicBlockDelegateMissionBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetHistoryDelegateMissionInfoList;
        private static DelegateBridge __Hotfix_OnDelegateMissionCancel;
        private static DelegateBridge __Hotfix_OnDelegateMissionComplete;
        private static DelegateBridge __Hotfix_IsAnyFleetFightingOnDelegateMission;
        private static DelegateBridge __Hotfix_AddExp2Captain;

        [MethodImpl(0x8000)]
        protected override void AddExp2Captain(LBDelegateMission mission)
        {
        }

        [MethodImpl(0x8000)]
        public List<DelegateMissionInfo> GetHistoryDelegateMissionInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnyFleetFightingOnDelegateMission()
        {
        }

        [MethodImpl(0x8000)]
        public void OnDelegateMissionCancel(bool isByPlayer, ulong signalInstanceId, List<ItemInfo> completeRewardList, List<ItemInfo> processRewardList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDelegateMissionComplete(ulong signalInstanceId, List<ItemInfo> completeRewardList, List<ItemInfo> processRewardList)
        {
        }
    }
}

