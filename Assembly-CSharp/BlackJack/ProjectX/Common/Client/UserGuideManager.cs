﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ToolUtil;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class UserGuideManager : UserGuideManagerBase
    {
        private IUserGuideDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetStepInfo;
        private static DelegateBridge __Hotfix_GetGroupInfo;
        private static DelegateBridge __Hotfix_IsStepGroupAlreadyCompleted;
        private static DelegateBridge __Hotfix_IsGroupHaveSavePoint;
        private static DelegateBridge __Hotfix_ResetGroup2NotComplete;
        private static DelegateBridge __Hotfix_SaveGroup4Completed;
        private static DelegateBridge __Hotfix_CheckStep;
        private static DelegateBridge __Hotfix_get_DisableUserGuide;

        [MethodImpl(0x8000)]
        public UserGuideManager(IUserGuideDataContainer dc)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckStep(int stepIdout, out bool isGroupSaved, out int savedGroupId)
        {
        }

        [MethodImpl(0x8000)]
        public override IUserGuideGroupInfo GetGroupInfo(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        public override IUserGuideStepInfo GetStepInfo(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsGroupHaveSavePoint(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsStepGroupAlreadyCompleted(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetGroup2NotComplete(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SaveGroup4Completed(int groupId)
        {
        }

        private bool DisableUserGuide
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

