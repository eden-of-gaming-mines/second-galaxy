﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class ClientStoreItemShipHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetDriveingLicenseInfo_0;
        private static DelegateBridge __Hotfix_GetDriveingLicenseInfo_1;
        private static DelegateBridge __Hotfix_GetShipDrivingLicensePropertyList_1;
        private static DelegateBridge __Hotfix_GetShipDrivingLicensePropertyList_2;
        private static DelegateBridge __Hotfix_GetShipDrivingLicensePropertyList_0;
        private static DelegateBridge __Hotfix_GetShipSpecialBufPropertyList_1;
        private static DelegateBridge __Hotfix_GetShipSpecialBufPropertyList_2;
        private static DelegateBridge __Hotfix_GetShipSpecialBufPropertyList_0;
        private static DelegateBridge __Hotfix_GetSuperWeaponInfo_0;
        private static DelegateBridge __Hotfix_GetSuperWeaponInfo_1;
        private static DelegateBridge __Hotfix_GetSuperEquipInfo_0;
        private static DelegateBridge __Hotfix_GetSuperEquipInfo_1;
        private static DelegateBridge __Hotfix_GetTacticalEquipInfo_0;
        private static DelegateBridge __Hotfix_GetTacticalEquipInfo_1;
        private static DelegateBridge __Hotfix_GetShipHighSlotDesc_1;
        private static DelegateBridge __Hotfix_GetShipHighSlotDesc_2;
        private static DelegateBridge __Hotfix_GetShipHighSlotDesc_0;

        [MethodImpl(0x8000)]
        public static ConfigDataDrivingLicenseInfo GetDriveingLicenseInfo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataDrivingLicenseInfo GetDriveingLicenseInfo(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetShipDrivingLicensePropertyList(ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetShipDrivingLicensePropertyList(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetShipDrivingLicensePropertyList(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipHighSlotDesc(ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipHighSlotDesc(ILBStoreItemClient shipItem)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipHighSlotDesc(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetShipSpecialBufPropertyList(ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetShipSpecialBufPropertyList(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetShipSpecialBufPropertyList(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataShipSuperEquipInfo GetSuperEquipInfo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataShipSuperEquipInfo GetSuperEquipInfo(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataSuperWeaponInfo GetSuperWeaponInfo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataSuperWeaponInfo GetSuperWeaponInfo(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataShipTacticalEquipInfo GetTacticalEquipInfo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataShipTacticalEquipInfo GetTacticalEquipInfo(StoreItemType itemType, object confInfo)
        {
        }
    }
}

