﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildBasicLogicClient : IGuildBasicLogicBase
    {
        void OnGuildBaseSolarSystemSetAck(int solarSystemId, int stationId);
        void OnGuildChangeAnnouncementNtf(string announcement, DateTime currTime);
        void OnGuildChangeBasicInfoNtf(GuildAllianceLanguageType languageType, GuildJoinPolicy joinPolicy, DateTime currTime);
        void OnGuildChangeManifestoNtf(string manifesto, DateTime currTime);
        void OnGuildChangeNameAndCodeNtf(string name, string code);
        void OnGuildLogoInfoNtf(GuildLogoInfo logo);
        void OnSetDismissEndTime(long dismissEndTime);
    }
}

