﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompSyncEventClient : LogicBlockShipCompSyncEventBase
    {
        protected LogicBlockShipCompInSpaceBasicCtxClient m_lbInSpaceBasicCtx;
        protected LogicBlockShipCompInSpaceWeaponEquipClient m_lbWeaponEquip;
        protected LogicBlockShipCompFireControllClient m_lbFireControll;
        protected LogicBlockShipCompDroneFireControllClient m_lbDroneFireControll;
        private LogicBlockShipCompBufContainerClient m_lbBufContainer;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPostServerSyncEvent;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnServerSyncEvent;
        private static DelegateBridge __Hotfix_PostServerSyncEvent;
        private static DelegateBridge __Hotfix_add_EventOnPostServerSyncEvent;
        private static DelegateBridge __Hotfix_remove_EventOnPostServerSyncEvent;

        public event Action EventOnPostServerSyncEvent
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnServerSyncEvent(uint eventTime, SyncEventFlag eventFlag, List<LBSyncEventOnHitInfo> onHitList, List<LBSyncEvent> eventList)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void PostServerSyncEvent()
        {
        }
    }
}

