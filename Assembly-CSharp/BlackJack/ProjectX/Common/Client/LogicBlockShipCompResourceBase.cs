﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompResourceBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectAllResources;
        private static DelegateBridge __Hotfix_CollectShipLargeIconResourcePath;
        private static DelegateBridge __Hotfix_CollectAllIconResources;
        private static DelegateBridge __Hotfix_GetArtResourcePath;
        private static DelegateBridge __Hotfix_GetArtLOD0ResourcePath;
        private static DelegateBridge __Hotfix_GetWeaponEquipArtResPath;
        private static DelegateBridge __Hotfix_GetShipGrandFaction;
        private static DelegateBridge __Hotfix_GetWeaponEquipIconResPath;
        private static DelegateBridge __Hotfix_GetStaticDefaultBoosterIconResPath;
        private static DelegateBridge __Hotfix_GetStaticDefaultRepairerIconResPath;
        private static DelegateBridge __Hotfix_GetTacticalEquipIconResPath;
        private static DelegateBridge __Hotfix_GetSuperWeaponIconResPath;
        private static DelegateBridge __Hotfix_GetSuperEquipIconResPath;
        private static DelegateBridge __Hotfix_GetNormalWeaponIconResPath;
        private static DelegateBridge __Hotfix_GetAmmoIconResPath;
        private static DelegateBridge __Hotfix_GetIconArtResourcePath;
        private static DelegateBridge __Hotfix_GetShipLargeIconArtResourcePath;
        private static DelegateBridge __Hotfix_GetShipTypeIconArtResourcePath;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetHighSlotGroupCount;
        private static DelegateBridge __Hotfix_GetMiddleSlotGroupCount;
        private static DelegateBridge __Hotfix_GetStaticDefaultBoosterGroup;
        private static DelegateBridge __Hotfix_GetStaticDefaultRepairerGroup;
        private static DelegateBridge __Hotfix_GetSuperWeaponConfigInfo;
        private static DelegateBridge __Hotfix_GetNormalWeaponGroup;
        private static DelegateBridge __Hotfix_GetSuperEquipConfigInfo;
        private static DelegateBridge __Hotfix_GetTacticalEquipConfInfo;
        private static DelegateBridge __Hotfix_GetLowSlotGroupCount;
        private static DelegateBridge __Hotfix_GetHighSlotGroupByIndex;
        private static DelegateBridge __Hotfix_GetMiddleSlotGroupByIndex;
        private static DelegateBridge __Hotfix_GetLowSlotGroupByIndex;

        [MethodImpl(0x8000)]
        public virtual void CollectAllIconResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CollectAllResources(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void CollectShipLargeIconResourcePath(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public string GetAmmoIconResPath(LBStaticWeaponEquipSlotGroup weaponSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        public string GetArtLOD0ResourcePath()
        {
        }

        [MethodImpl(0x8000)]
        public string GetArtResourcePath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ConfigDataSpaceShipInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticWeaponEquipSlotGroup GetHighSlotGroupByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int GetHighSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        public string GetIconArtResourcePath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticWeaponEquipSlotGroup GetLowSlotGroupByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int GetLowSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticWeaponEquipSlotGroup GetMiddleSlotGroupByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int GetMiddleSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticWeaponEquipSlotGroup GetNormalWeaponGroup()
        {
        }

        [MethodImpl(0x8000)]
        public string GetNormalWeaponIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual GrandFaction GetShipGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipLargeIconArtResourcePath()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipTypeIconArtResourcePath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticWeaponEquipSlotGroup GetStaticDefaultBoosterGroup()
        {
        }

        [MethodImpl(0x8000)]
        public string GetStaticDefaultBoosterIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticWeaponEquipSlotGroup GetStaticDefaultRepairerGroup()
        {
        }

        [MethodImpl(0x8000)]
        public string GetStaticDefaultRepairerIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ConfigDataShipSuperEquipInfo GetSuperEquipConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetSuperEquipIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ConfigDataSuperWeaponInfo GetSuperWeaponConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetSuperWeaponIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ConfigDataShipTacticalEquipInfo GetTacticalEquipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<string> GetTacticalEquipIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        public string GetWeaponEquipArtResPath(LBStaticWeaponEquipSlotGroup weaponSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        public string GetWeaponEquipIconResPath(LBStaticWeaponEquipSlotGroup weaponSlotGroup)
        {
        }
    }
}

