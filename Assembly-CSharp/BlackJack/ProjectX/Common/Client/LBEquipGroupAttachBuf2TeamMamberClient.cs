﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBEquipGroupAttachBuf2TeamMamberClient : LBEquipGroupAttachBuf2TeamMamberBase, ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBEquipGroupBase, StructProcessEquipLaunchBase> EventOnStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnProcessStartCharge;
        private static DelegateBridge __Hotfix_OnProcessFire;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_OnProcessEffect;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_add_EventOnStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnProcessFire;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFire;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, StructProcessEquipLaunchBase> EventOnStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupAttachBuf2TeamMamberClient(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessEquipAttachBufLaunch process, uint groupCDEndTime)
        {
        }
    }
}

