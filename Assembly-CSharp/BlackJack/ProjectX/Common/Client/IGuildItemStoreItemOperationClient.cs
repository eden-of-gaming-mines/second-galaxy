﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildItemStoreItemOperationClient : IGuildItemStoreItemOperationBase
    {
        bool AddItemInMSStoreAutoOverlay(int itemIndex, ulong instanceId, StoreItemType itemType, int itemConfigId, int count);
        bool RemoveGuildStoreItemById(StoreItemType itemType, int itemId, int count);
        bool RemoveGuildStoreItemByIndex(int index, int count);
        bool ResumeItem(LBStoreItem from, int count, int resumedItemIndex, ulong instanceId, out LBStoreItem to);
        bool SuspendItem(LBStoreItem from, int count, ItemSuspendReason reason, int suspendedItemIndex, ulong instanceId, out LBStoreItem to);
        void UpdateGuildItemStoreInfo(List<GuildStoreItemListInfo> itemList);
    }
}

