﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockGuildClient : LogicBlockGuildBase
    {
        private List<ProGuildSimplestInfo> m_GuildList;
        private List<uint> m_SelfApplyList;
        private Dictionary<int, List<GuildActionInfo>> m_actionInfos;
        private List<GuildBattleSimpleReportInfo> m_guildBattleSimpleReportInfos;
        private List<GuildBattleSimpleReportInfo> m_guildBattleCompletedSimpleReportInfos;
        private Dictionary<ulong, GuildBattleReportInfo> m_guildBattleReportInfoDict;
        private Dictionary<MemberTitleType, List<Func<MemberTitleArrowState, GuildMemberInfo, GuildMemberInfo, int>>> m_memberSortFuncsDic;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <SortByMemberList>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <SelfAtMemberListFirst>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private MemberTitleArrowState <MemberListMainArrowState>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private MemberTitleType <MemberListMainTitleType>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <GuildJobCtrlEditUseId>k__BackingField;
        public GuildJobType JobCtrlMainJobType;
        public MemberTitleArrowState JobCtrlMainArrowState;
        public int GuildMemberFirstActiveIndex;
        protected IGuildDataContainerClient m_guildDC;
        private GuildClient m_guild;
        private float m_updateIntervalTime;
        private Dictionary<uint, OtherGuildMainPanelInfo> m_guildId2GuildMainPanelInfoDic;
        private readonly Dictionary<uint, GuildSimplestInfo> m_guildSimpleInfosCache;
        private readonly Dictionary<uint, AllianceBasicInfo> m_allianceBasicInfosCache;
        private BlackJack.ProjectX.Common.GuildDiplomacyFriendlyViewInfo m_guildDiplomacyFriendlyViewInfo;
        private float m_updateFriendViewInfoIntervalTime;
        private DateTime m_nextUpdateFriendViewInfoTime;
        private BlackJack.ProjectX.Common.GuildDiplomacyEnemyViewInfo m_guildDiplomacyEnemyViewInfo;
        private float m_updateEnemyViewInfoIntervalTime;
        private DateTime m_nextUpdateEnenmyViewInfoTime;
        private GuildStaffingLogFullInfo m_staffingLogInfo;
        private GuildMessageCacheInfo m_guildMessageCacheInfo;
        private float m_nextSendMessageIntervalTime;
        private DateTime m_nextSendGuildMessageDateTime;
        private uint m_currGuildMessageVersion;
        private GuildApplyListInfo m_applyForListInfo;
        private Dictionary<int, GuildSolarSystemInfoClient> m_guildSolarSystemCachedInfos;
        private const int GuildSolarSystemInfoClientValidTime = 60;
        private readonly Dictionary<int, Dictionary<int, ProSolarSystemGuildBattleStatusInfo>> m_solarSystemGuildBattleStatusDict;
        private readonly Dictionary<int, uint> m_solarSytemGuildBattleStatusDataVersionDict;
        private readonly Dictionary<int, float> m_solarSystemDelegateMissionRewardMultiDict;
        private ILBPlayerContextClient m_playerCtxClient;
        private const int GuildSimpleInfoValidTime = 60;
        private readonly List<AllianceMemberInfo> m_emptyAllianceMemberList;
        private readonly List<GuildAllianceInviteInfo> m_emptyGuildAllianceInviteList;
        private readonly List<AllianceInviteInfo> m_emptyAllianceInviteList;
        private AllianceClient m_alliance;
        private List<GuildBattleSimpleReportInfo> m_allianceProcessingGuildBattleSimpleReportInfos;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<List<FleetPosition>, List<FleetPosition>> EventOnSelfFleetPositionChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnSetFormationActive;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string, int> EventOnGuildFleetDismissed;
        private HashSet<string> m_ownFleetMemberDict;
        private uint m_fireFoucsTargetId;
        private uint m_protectTargetId;
        private List<FleetPosition> m_newFleetPositions;
        private List<FleetPosition> m_removeFleetPositions;
        private List<GuildFleetSimpleInfo> m_guildFleetSimpleInfoList;
        private Dictionary<ulong, List<GuildFleetMemberDynamicInfo>> m_guildFleetId2MemberDynamicInfoDict;
        private bool m_guildFleetDSInitialized;
        private Dictionary<int, GuildFleetOperationCDInfo> m_fleetOperationCdInfoDic;
        private readonly Dictionary<int, long> m_priceDict;
        public DateTime LastPurchaseOrderListUpdateTime;
        private GuildRedPointInfo m_guildRedPointInfo;
        private const string RedPointGuildMessageVersion = "RedPointGuildMessageVersion";
        private readonly Dictionary<int, GuildTradePurchaseInfo> m_selfGuildTradePurchaseInfoDict;
        private readonly Dictionary<int, GuildTradeTransportInfo> m_selfGuildTradeTransportInfoDict;
        private readonly List<GuildTradeTransportInfo> m_guildTradeTransportInfoList2Self;
        private readonly Dictionary<ulong, GuildTradePurchaseInfo> m_billboardGuildTradePurchaseInfoDict;
        [CompilerGenerated]
        private static Comparison<GuildStaffingLogInfo> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<GuildMessageInfo> <>f__am$cache1;
        [CompilerGenerated]
        private static Comparison<GuildBattleSimpleReportInfo> <>f__am$cache2;
        [CompilerGenerated]
        private static Comparison<GuildBattleSimpleReportInfo> <>f__am$cache3;
        [CompilerGenerated]
        private static Comparison<GuildBattlePlayerKillLostStatInfo> <>f__am$cache4;
        [CompilerGenerated]
        private static Comparison<GuildBattleSimpleReportInfo> <>f__am$cache5;
        [CompilerGenerated]
        private static Comparison<GuildFleetSimpleInfo> <>f__am$cache6;
        [CompilerGenerated]
        private static Comparison<GuildFleetSimpleInfo> <>f__am$cache7;
        [CompilerGenerated]
        private static Predicate<GuildFleetMemberInfo> <>f__am$cache8;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnGuildSearch4JoinAck;
        private static DelegateBridge __Hotfix_get_GuildList;
        private static DelegateBridge __Hotfix_get_SelfApplyList;
        private static DelegateBridge __Hotfix_OnGuildCreateAck;
        private static DelegateBridge __Hotfix_OnGuildJoinNtf;
        private static DelegateBridge __Hotfix_OnSelfLeaveGuild;
        private static DelegateBridge __Hotfix_ClearGuildInfo;
        private static DelegateBridge __Hotfix_RemoveGuildMember;
        private static DelegateBridge __Hotfix_UpdateGuildDynamicInfo;
        private static DelegateBridge __Hotfix_UpdateSelfMemberDynamicInfo;
        private static DelegateBridge __Hotfix_GetSelfMemberDynamicInfo;
        private static DelegateBridge __Hotfix_GetSelfMemberBasicInfo;
        private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateAck;
        private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateNtf;
        private static DelegateBridge __Hotfix_OnGuildMainPanelInfoAck;
        private static DelegateBridge __Hotfix_OnTargetGuildMainPanelInfoAck;
        private static DelegateBridge __Hotfix_OnGuildMemberListInfoAck;
        private static DelegateBridge __Hotfix_OnGuildChangeBasicInfoNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeManifestoNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeAnnouncementNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeNameAndCodeNtf;
        private static DelegateBridge __Hotfix_OnGuildLogoInfoNtf;
        private static DelegateBridge __Hotfix_OnGuildSelfInfoNtf;
        private static DelegateBridge __Hotfix_OnPlayerGuildInfoInitAck;
        private static DelegateBridge __Hotfix_OnGuildBaseSolarSystemSetAck;
        private static DelegateBridge __Hotfix_OnSetDismissEndTime;
        private static DelegateBridge __Hotfix_OnStaffingLogListAck;
        private static DelegateBridge __Hotfix_OnGuildLeaderTransferAbortAck;
        private static DelegateBridge __Hotfix_OnGuildApplyForListAck;
        private static DelegateBridge __Hotfix_GuildBattleReinforcementEndTimeSet;
        private static DelegateBridge __Hotfix_OnGuildCompensationListAck;
        private static DelegateBridge __Hotfix_GetSelfGuild;
        private static DelegateBridge __Hotfix_GetNpcGuildInfo;
        private static DelegateBridge __Hotfix_NeedUpdateGuildInfo;
        private static DelegateBridge __Hotfix_GetGuildGeneralInfoByGuildId;
        private static DelegateBridge __Hotfix_GetMemberListInfo;
        private static DelegateBridge __Hotfix_GetMemberInfo;
        private static DelegateBridge __Hotfix_GetGuildJob;
        private static DelegateBridge __Hotfix_CanSendGuildMessage;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyUpdateNtf;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyDetailInfoAck;
        private static DelegateBridge __Hotfix_GetGuildDiplomacyInfo;
        private static DelegateBridge __Hotfix_IsFriend;
        private static DelegateBridge __Hotfix_IsEnemy;
        private static DelegateBridge __Hotfix_GetDiplomacyDetailInfoVersion;
        private static DelegateBridge __Hotfix_NeedUpdateDiplomacyInfo;
        private static DelegateBridge __Hotfix_GetGuildDiplomacyState;
        private static DelegateBridge __Hotfix_GetStaffingLogListInfo;
        private static DelegateBridge __Hotfix_GetGuildBaseInfo;
        private static DelegateBridge __Hotfix_GetGuildDynamicInfo;
        private static DelegateBridge __Hotfix_GetApplyListInfo;
        private static DelegateBridge __Hotfix_RemoveItemFromFriendViewInfo;
        private static DelegateBridge __Hotfix_RemoveItemFromEnemyViewInfo;
        private static DelegateBridge __Hotfix_AddItemToFriendViewInfo;
        private static DelegateBridge __Hotfix_AddItemToEnemyViewInfo;
        private static DelegateBridge __Hotfix_OnGuildMessageGetAck;
        private static DelegateBridge __Hotfix_OnGuildMessageWriteAck;
        private static DelegateBridge __Hotfix_OnGuildMessageDeleteAck;
        private static DelegateBridge __Hotfix_GetGuildMessageInfo;
        private static DelegateBridge __Hotfix_UpdateMessageVersion;
        private static DelegateBridge __Hotfix_ExistNewMessage;
        private static DelegateBridge __Hotfix_GetGuildMessageVersion;
        private static DelegateBridge __Hotfix_UpdateGuildItemStoreInfo;
        private static DelegateBridge __Hotfix_GetGuildItemStoreInfo;
        private static DelegateBridge __Hotfix_GetGuildItemCountByItemId;
        private static DelegateBridge __Hotfix_GetGuildItemByStoreItemIndex;
        private static DelegateBridge __Hotfix_UpdateGuildCurrency;
        private static DelegateBridge __Hotfix_GetGuildCurrency;
        private static DelegateBridge __Hotfix_GetGuildCurrencyLogList;
        private static DelegateBridge __Hotfix_RefreshGuildCurrencyLogList;
        private static DelegateBridge __Hotfix_OnGuildGameGuildDonateAck;
        private static DelegateBridge __Hotfix_OnGuildWalletInfoAck;
        private static DelegateBridge __Hotfix_GetSelfDonateTradeMoneyToday;
        private static DelegateBridge __Hotfix_OnGuildActionInfoAck;
        private static DelegateBridge __Hotfix_OnGuildActionInfoUpdateAck;
        private static DelegateBridge __Hotfix_OnGuildActionCreateAck;
        private static DelegateBridge __Hotfix_OnGuildActionDeleteAck;
        private static DelegateBridge __Hotfix_GetGuildActionInfos;
        private static DelegateBridge __Hotfix_GetAcionInfoByInstanceId;
        private static DelegateBridge __Hotfix_RemoveGuildActionByInstanceId;
        private static DelegateBridge __Hotfix_RefreshProductionLineList;
        private static DelegateBridge __Hotfix_GetGuildProductionLineList;
        private static DelegateBridge __Hotfix_GetGuildProductionLineById;
        private static DelegateBridge __Hotfix_AddGuildProductionLine;
        private static DelegateBridge __Hotfix_RemoveGuildProductionLine;
        private static DelegateBridge __Hotfix_UpdateGuildProductionLine;
        private static DelegateBridge __Hotfix_ClearGuildProductionLineData;
        private static DelegateBridge __Hotfix_UpdateProductionLineSpeedUpTime;
        private static DelegateBridge __Hotfix_CheckGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_GetGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_SetGuldProductionDataVersion;
        private static DelegateBridge __Hotfix_CalcGuildProductionItemRealCost;
        private static DelegateBridge __Hotfix_CalcGuildProductionTradeMoneyRealCost;
        private static DelegateBridge __Hotfix_CalcGuildProductionTimeRealCost;
        private static DelegateBridge __Hotfix_CheckProductionLineStart;
        private static DelegateBridge __Hotfix_CheckProductionLineCancel;
        private static DelegateBridge __Hotfix_CheckProductionLineSpeedUpByTradeMoney;
        private static DelegateBridge __Hotfix_CheckProductionLineComplete;
        private static DelegateBridge __Hotfix_CheckProduceTempleteUnlockCondition_1;
        private static DelegateBridge __Hotfix_CheckProduceTempleteUnlockCondition_0;
        private static DelegateBridge __Hotfix_CheckPlayerGuildInfo4Production;
        private static DelegateBridge __Hotfix_RefleshSovereignLostReportList;
        private static DelegateBridge __Hotfix_GetSovereignLostReportList;
        private static DelegateBridge __Hotfix_AddSovereignLostReport;
        private static DelegateBridge __Hotfix_AddBuildingLostReport;
        private static DelegateBridge __Hotfix_GetBuildingLostReport;
        private static DelegateBridge __Hotfix_OnGuildOccupiedSolarSystemInfoAck;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemBasicInfoAck;
        private static DelegateBridge __Hotfix_GuildSolarSystemBasicCaChedInfoUpdate;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemBuildingInfoAck;
        private static DelegateBridge __Hotfix_GuildSolarSystemBuildingCaChedInfoUpdate;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemBattleInfoAck;
        private static DelegateBridge __Hotfix_GuildSolarSystemBattleCaChedInfoUpdate;
        private static DelegateBridge __Hotfix_IsSolarSystemOccupiedByNpc;
        private static DelegateBridge __Hotfix_OnGuildBuildingDeployUpgradeAck;
        private static DelegateBridge __Hotfix_OnGuildBuildingRecycleAck;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemInfo;
        private static DelegateBridge __Hotfix_CheckGuildSolarSystemBasicInfoValid;
        private static DelegateBridge __Hotfix_CheckGuildSolarSystemBuildingInfoValid;
        private static DelegateBridge __Hotfix_CheckGuildSolarSystemBattleInfoValid;
        private static DelegateBridge __Hotfix_RefreshGuildBuildingInfo;
        private static DelegateBridge __Hotfix_GetStarfieldGuildBattleStatusDataVersion;
        private static DelegateBridge __Hotfix_GetStarfieldGuildBattleStatus;
        private static DelegateBridge __Hotfix_UpdateGuildCurrStarfieldGuildBattleInfo;
        private static DelegateBridge __Hotfix_GetSolarSystemGuildBattleStatus;
        private static DelegateBridge __Hotfix_UpdateGuildSolarSystemDelegateMissionRewardMulti;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemDelegateMissionRewardMulti;
        private static DelegateBridge __Hotfix_UpdateCompensationDonateToday;
        private static DelegateBridge __Hotfix_DonateCompensation;
        private static DelegateBridge __Hotfix_RefreshCompensationList;
        private static DelegateBridge __Hotfix_GetCompensationListVersion;
        private static DelegateBridge __Hotfix_UpdateCompensation;
        private static DelegateBridge __Hotfix_AddCompensation;
        private static DelegateBridge __Hotfix_SetAutoCompensation;
        private static DelegateBridge __Hotfix_GetCompensationListByShipType;
        private static DelegateBridge __Hotfix_CloseCompensation;
        private static DelegateBridge __Hotfix_GetDonateTimesToday;
        private static DelegateBridge __Hotfix_AddBenefits;
        private static DelegateBridge __Hotfix_RemoveBenefits;
        private static DelegateBridge __Hotfix_GetBenefits;
        private static DelegateBridge __Hotfix_RefreshBenefitsList;
        private static DelegateBridge __Hotfix_ReceiveBenefits;
        private static DelegateBridge __Hotfix_GetFinalBenefitsListVersion;
        private static DelegateBridge __Hotfix_CaculateOriginalBenefitsListVersion;
        private static DelegateBridge __Hotfix_CaculateFinalBenefitsListVersion;
        private static DelegateBridge __Hotfix_OnGuildBattleSimpleReportInfoAck;
        private static DelegateBridge __Hotfix_GetGuildBattleSimpleReportInfos;
        private static DelegateBridge __Hotfix_GetGuildBattleCompletedSimpleReportInfos;
        private static DelegateBridge __Hotfix_OnGuildBattleReportInfoAck;
        private static DelegateBridge __Hotfix_GetGuildBattleReportInfo;
        private static DelegateBridge __Hotfix_CheckForGuildBattleSimpleReportReq;
        private static DelegateBridge __Hotfix_OnGuildSentryProbeUseAck;
        private static DelegateBridge __Hotfix_IsGuildSentryProbeAvailable;
        private static DelegateBridge __Hotfix_IsGuildSentrySignalInCountDown;
        private static DelegateBridge __Hotfix_ClearGuildSentryInterestScene;
        private static DelegateBridge __Hotfix_CheckMiningBalanceTimeRefreashCD;
        private static DelegateBridge __Hotfix_UpdateLastMiningBalanceTimeRefreashTime;
        private static DelegateBridge __Hotfix_GuildMiningBalanceTimeReset;
        private static DelegateBridge __Hotfix_GetGuildMiningBalanceTimeHour;
        private static DelegateBridge __Hotfix_GetGuildMiningBalanceTimeMinute;
        private static DelegateBridge __Hotfix_SortMemberList;
        private static DelegateBridge __Hotfix_SortMemberWithJobCtrl;
        private static DelegateBridge __Hotfix_CompareMember_0;
        private static DelegateBridge __Hotfix_MemberCompareByJobCtrl;
        private static DelegateBridge __Hotfix_InitMemberSotrFunc;
        private static DelegateBridge __Hotfix_SortMemberWithMemberList;
        private static DelegateBridge __Hotfix_SetSelfAtMemberListFirst;
        private static DelegateBridge __Hotfix_CompareMember_1;
        private static DelegateBridge __Hotfix_MemberCompareByJob;
        private static DelegateBridge __Hotfix_MemberCompareByGrade;
        private static DelegateBridge __Hotfix_MemberCompareByGalaxy;
        private static DelegateBridge __Hotfix_MemberCompareByContribution;
        private static DelegateBridge __Hotfix_MemberCompareByLastOnline;
        private static DelegateBridge __Hotfix_MemberCompareByLevel;
        private static DelegateBridge __Hotfix_set_SortByMemberList;
        private static DelegateBridge __Hotfix_get_SortByMemberList;
        private static DelegateBridge __Hotfix_set_SelfAtMemberListFirst;
        private static DelegateBridge __Hotfix_get_SelfAtMemberListFirst;
        private static DelegateBridge __Hotfix_set_MemberListMainArrowState;
        private static DelegateBridge __Hotfix_get_MemberListMainArrowState;
        private static DelegateBridge __Hotfix_set_MemberListMainTitleType;
        private static DelegateBridge __Hotfix_get_MemberListMainTitleType;
        private static DelegateBridge __Hotfix_set_GuildJobCtrlEditUseId;
        private static DelegateBridge __Hotfix_get_GuildJobCtrlEditUseId;
        private static DelegateBridge __Hotfix_GetOtherGuildMomentsBriefInfoListByGuildId;
        private static DelegateBridge __Hotfix_GetSelfGuildMomentsBriefInfoList;
        private static DelegateBridge __Hotfix_GetGuildMomentsInfoList;
        private static DelegateBridge __Hotfix_GetCurrMomentsBriefInfoVersion;
        private static DelegateBridge __Hotfix_UpdateGuildMomentsInfoList;
        private static DelegateBridge __Hotfix_TryGetGuildSimpleInfoList_1;
        private static DelegateBridge __Hotfix_GetGuildSimpleInfoById;
        private static DelegateBridge __Hotfix_UpdateGuildSimpleInfoCache;
        private static DelegateBridge __Hotfix_TryGetGuildSimpleInfoList_0;
        private static DelegateBridge __Hotfix_GetAllianceBasicInfoById;
        private static DelegateBridge __Hotfix_UpdateAllianceBasicInfoCache;
        private static DelegateBridge __Hotfix_UpdateCurrAvailableTeleportTunnelInfo;
        private static DelegateBridge __Hotfix_GetCurrAvailableTeleportTunnelInfo;
        private static DelegateBridge __Hotfix_UpdateGuildAntiTeleportInfoList;
        private static DelegateBridge __Hotfix_GetAntiTeleportEffectInfoVersion;
        private static DelegateBridge __Hotfix_GetAllGuildAntiTeleportEffectList;
        private static DelegateBridge __Hotfix_GetGuildAntiTeleportEffectBySolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildAntiTeleportEffectById;
        private static DelegateBridge __Hotfix_get_GuildDiplomacyFriendlyViewInfo;
        private static DelegateBridge __Hotfix_get_GuildDiplomacyEnemyViewInfo;
        private static DelegateBridge __Hotfix_get_PlayerCtxClient;
        private static DelegateBridge __Hotfix_RefreshAllianceMemberList;
        private static DelegateBridge __Hotfix_RefreshAllianceInfo;
        private static DelegateBridge __Hotfix_RefreshGuildAllianceInviteList;
        private static DelegateBridge __Hotfix_OnAllianceLeaderTransfer;
        private static DelegateBridge __Hotfix_OnAllianceInviteAccept;
        private static DelegateBridge __Hotfix_OnAllianceInviteRemove;
        private static DelegateBridge __Hotfix_OnAllianceInviteSend;
        private static DelegateBridge __Hotfix_OnAllianceMemberLeave;
        private static DelegateBridge __Hotfix_OnAllianceDismiss;
        private static DelegateBridge __Hotfix_OnAllianceMemberRemove;
        private static DelegateBridge __Hotfix_OnAllianceCreate;
        private static DelegateBridge __Hotfix_OnAllianceNameSet;
        private static DelegateBridge __Hotfix_OnAllianceLogoSet;
        private static DelegateBridge __Hotfix_OnAllianceRegionAndAnnouncementSet;
        private static DelegateBridge __Hotfix_GetAllianceInfo;
        private static DelegateBridge __Hotfix_GetAllianceBasicInfo;
        private static DelegateBridge __Hotfix_GetAllianceMemberList;
        private static DelegateBridge __Hotfix_GetGuildAllianceInviteList;
        private static DelegateBridge __Hotfix_GetAllianceInviteList;
        private static DelegateBridge __Hotfix_InitAlliance;
        private static DelegateBridge __Hotfix_OnGuildAllianceInfoChange;
        private static DelegateBridge __Hotfix_OnGuildAllianceInviteUnread;
        private static DelegateBridge __Hotfix_OnAllianceGuildBattleSimpleReportInfoAck;
        private static DelegateBridge __Hotfix_GetAllianceGuildBattleSimpleReportInfoList;
        private static DelegateBridge __Hotfix_CheckForAntiTeleportInfoReq;
        private static DelegateBridge __Hotfix_CheckForActivateAntiTeleportEffect;
        private static DelegateBridge __Hotfix_AddOrUpdateGuildFlagShipHangarInfo;
        private static DelegateBridge __Hotfix_UpdateFlagShipHangarLockMaster;
        private static DelegateBridge __Hotfix_UnpackFlagShipFromItemStore;
        private static DelegateBridge __Hotfix_PackFlagShipToItemStore;
        private static DelegateBridge __Hotfix_SetFlagShipModuleEquipToShip;
        private static DelegateBridge __Hotfix_RemoveFlagShipModuleEquipFromShip;
        private static DelegateBridge __Hotfix_ChangeFlagShipFuel;
        private static DelegateBridge __Hotfix_RegisterFlagShipDriver;
        private static DelegateBridge __Hotfix_UnregisterFlagShipDriver;
        private static DelegateBridge __Hotfix_UpdateFlagShipName;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipOptLogInfoList;
        private static DelegateBridge __Hotfix_IsHangarSlotUnlock;
        private static DelegateBridge __Hotfix_GetHangarUnlockSlotCount;
        private static DelegateBridge __Hotfix_GetHangarSlotRankType;
        private static DelegateBridge __Hotfix_GetStaticFlagShip;
        private static DelegateBridge __Hotfix_GetStaticFlagShipList;
        private static DelegateBridge __Hotfix_GetShipHangarFlagShipInstanceInfoList;
        private static DelegateBridge __Hotfix_SetFlagShipCustomName;
        private static DelegateBridge __Hotfix_GetAllFlagShipHangarList;
        private static DelegateBridge __Hotfix_GetGuildFlagShipHangarById;
        private static DelegateBridge __Hotfix_GetShipHangarLockMaster;
        private static DelegateBridge __Hotfix_GetGuildFlagShipHangarBySolarSystemId;
        private static DelegateBridge __Hotfix_GetFlagShipDriverInfo;
        private static DelegateBridge __Hotfix_GetGuildFlagShipOptLogInfoList;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarInfoReq;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarScanning;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarLocking;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarUnlocking;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipUnpack;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipPack;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipAddModuleEquip;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRemoveModuleEquip;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipSupplementFuel;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipCaptainRegister;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipCaptainUnregister;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRename;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRename4DomesticPackage;
        private static DelegateBridge __Hotfix_GuildFlagShipSupplementFuelInfoComparer;
        private static DelegateBridge __Hotfix_Initialize_GuildFleetOperationCdInfo;
        private static DelegateBridge __Hotfix_IsGuildFleetDSInitialized;
        private static DelegateBridge __Hotfix_InitGuildFleetDS;
        private static DelegateBridge __Hotfix_get_GetGuildFleetSimpleInfoList;
        private static DelegateBridge __Hotfix_IsGuildFleetAllowToJoinFleetManual;
        private static DelegateBridge __Hotfix_GetGuildFleetFormationType;
        private static DelegateBridge __Hotfix_GetGuildFleetIsFormationActive;
        private static DelegateBridge __Hotfix_GetGuildFleetDataVersion;
        private static DelegateBridge __Hotfix_GetGuildFleetInfo;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberDynamicInfoList;
        private static DelegateBridge __Hotfix_UpdateGuildFleetListSimpleInfoList;
        private static DelegateBridge __Hotfix_UpdateGuildFleetSimpleInfo;
        private static DelegateBridge __Hotfix_SetGuildFleetFormationType;
        private static DelegateBridge __Hotfix_SetGuildFleetAllowToJoinManual;
        private static DelegateBridge __Hotfix_SetGuildFleetFormationActive;
        private static DelegateBridge __Hotfix_AddGuildFleetMember;
        private static DelegateBridge __Hotfix_RemoveGuildFleetMember;
        private static DelegateBridge __Hotfix_AddOrUpdateGuildFleetDetailInfo;
        private static DelegateBridge __Hotfix_UpdateGuildFleetMemberBasicInfoList;
        private static DelegateBridge __Hotfix_RefreshGuildFleetMemberList;
        private static DelegateBridge __Hotfix_RemoveGuildFleetInfo;
        private static DelegateBridge __Hotfix_UpdateGuildFleetName;
        private static DelegateBridge __Hotfix_SetFleetCommander;
        private static DelegateBridge __Hotfix_RemoveFleetMemberPosition;
        private static DelegateBridge __Hotfix_TransferFleetMemberPosition;
        private static DelegateBridge __Hotfix_CheckForFleetListReq;
        private static DelegateBridge __Hotfix_CheckForFleetCreating;
        private static DelegateBridge __Hotfix_CheckForFleetSetFormationType;
        private static DelegateBridge __Hotfix_CheckForFleetSetFormationActive;
        private static DelegateBridge __Hotfix_CheckForFleetSetAllowToJoinFleetManual;
        private static DelegateBridge __Hotfix_CheckForFleetMemberJoin;
        private static DelegateBridge __Hotfix_CheckForFleetMemberLeave;
        private static DelegateBridge __Hotfix_CheckForFleetKickOut;
        private static DelegateBridge __Hotfix_CheckForFleetDismiss;
        private static DelegateBridge __Hotfix_CheckForFleetRename;
        private static DelegateBridge __Hotfix_CheckForFleetRename4DomesticPackage;
        private static DelegateBridge __Hotfix_CheckForFleetDetailInfoReq;
        private static DelegateBridge __Hotfix_CheckForGuildFleetSetCommanderReq;
        private static DelegateBridge __Hotfix_CheckForGuildFleetSetInternalPositions;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMemberRetire;
        private static DelegateBridge __Hotfix_CheckForGuildFleetTransferPosition;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMembersJumping;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMembersUseStargate;
        private static DelegateBridge __Hotfix_CheckForGuildFleetFireFocusTargetReq;
        private static DelegateBridge __Hotfix_CheckForGuildFleetProtectTargetReq;
        private static DelegateBridge __Hotfix_CheckPlayerGuildFleetSettingUpdate;
        private static DelegateBridge __Hotfix_SetFleetFireFocusTargetId;
        private static DelegateBridge __Hotfix_SetFleetProtectTargetId;
        private static DelegateBridge __Hotfix_GetFleetFireFocusTargetId;
        private static DelegateBridge __Hotfix_GetFleetProtectTargetId;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberByPosition;
        private static DelegateBridge __Hotfix_IsSelfGuildFleetMember;
        private static DelegateBridge __Hotfix_CheckFleetOperationCdTime;
        private static DelegateBridge __Hotfix_UpdateFleetOperationCdTime;
        private static DelegateBridge __Hotfix_GetSelfChangedPositionList;
        private static DelegateBridge __Hotfix_add_EventOnSelfFleetPositionChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSelfFleetPositionChanged;
        private static DelegateBridge __Hotfix_add_EventOnSetFormationActive;
        private static DelegateBridge __Hotfix_remove_EventOnSetFormationActive;
        private static DelegateBridge __Hotfix_add_EventOnGuildFleetDismissed;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFleetDismissed;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderListUpdate;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderCreate;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderModify;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderCancel;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderCommitForSale;
        private static DelegateBridge __Hotfix_GetGuildPurchaseOrderListInfo;
        private static DelegateBridge __Hotfix_UpdateGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_AddGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_CancelGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_SetGuildPrice;
        private static DelegateBridge __Hotfix_GetGuildPrice;
        private static DelegateBridge __Hotfix_GuildPurchaseAuctionIdAndPriceListSet;
        private static DelegateBridge __Hotfix_OnGuildRedPointVersionAck;
        private static DelegateBridge __Hotfix_set_RedPointGuildJoinApplyCount;
        private static DelegateBridge __Hotfix_set_RedPointGuildBenefits;
        private static DelegateBridge __Hotfix_set_RedPointGuildAllianceInviteCount;
        private static DelegateBridge __Hotfix_HasAnyGuildJoinApply;
        private static DelegateBridge __Hotfix_HasAnyAllianceInvite;
        private static DelegateBridge __Hotfix_HasNewGuildMessage;
        private static DelegateBridge __Hotfix_HasAnyBenefits2Balance;
        private static DelegateBridge __Hotfix_SetGuildMessageVersionData;
        private static DelegateBridge __Hotfix_OnGuildTradePurchaseOrderCreate;
        private static DelegateBridge __Hotfix_OnGuildTradePurchaseOrderRemove;
        private static DelegateBridge __Hotfix_OnGuildTradeTransportPlanCreate;
        private static DelegateBridge __Hotfix_OnGuildTradeTransportPlanComplete;
        private static DelegateBridge __Hotfix_GetGuildTradePortExtraInfoBySolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildTradePortExtraInfoList;
        private static DelegateBridge __Hotfix_GetSpaceShipConfigInfoByTradePortSolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildTradePortDataVersion;
        private static DelegateBridge __Hotfix_GuildTradePortExtraInfoUpdate;
        private static DelegateBridge __Hotfix_GuildTradePortExtraInfoListUpdate;
        private static DelegateBridge __Hotfix_GetGuildTradePurchaseInfoListByItem;
        private static DelegateBridge __Hotfix_GetSelfGuildTradePurchaseInfoBySolarSystemId;
        private static DelegateBridge __Hotfix_GetSelfGuildTradeTransportInfoBySolarSystemId;
        private static DelegateBridge __Hotfix_GetSelfGuildTradeTransportInfoDict;
        private static DelegateBridge __Hotfix_GetGuildTradeTransportInfoList2Self;
        private static DelegateBridge __Hotfix_GuildTradePurchaseInfoListUpdate;
        private static DelegateBridge __Hotfix_SelfGuildTradePurchaseInfoUpdateOrAdd;
        private static DelegateBridge __Hotfix_SelfGuildTradePurchaseInfoRemove;
        private static DelegateBridge __Hotfix_SelfGuildTradeTransportInfoUpdateOrAdd;
        private static DelegateBridge __Hotfix_SelfGuildTradeTransportInfoRemove_1;
        private static DelegateBridge __Hotfix_SelfGuildTradeTransportInfoRemove_0;
        private static DelegateBridge __Hotfix_GuildTradeTransportInfoList2SelfUpdate;

        public event Action<string, int> EventOnGuildFleetDismissed
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<FleetPosition>, List<FleetPosition>> EventOnSelfFleetPositionChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnSetFormationActive
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AddBenefits(GuildBenefitsInfoBase benefits)
        {
        }

        [MethodImpl(0x8000)]
        public void AddBuildingLostReport(BuildingLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public void AddCompensation(LossCompensation compensation)
        {
        }

        [MethodImpl(0x8000)]
        public void AddGuildFleetMember(ulong fleetId, GuildFleetMemberInfo newMember)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddGuildProductionLine(GuildProductionLineInfo lineInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void AddGuildPurchaseOrder(GuildPurchaseOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private void AddItemToEnemyViewInfo(PlayerSimplestInfo player, GuildSimplestInfo guild, AllianceBasicInfo alliance)
        {
        }

        [MethodImpl(0x8000)]
        private void AddItemToFriendViewInfo(PlayerSimplestInfo player, GuildSimplestInfo guild, AllianceBasicInfo alliance)
        {
        }

        [MethodImpl(0x8000)]
        public void AddOrUpdateGuildFlagShipHangarInfo(ProGuildFlagShipHangarDetailInfo proShipHangarDetailInfo, string hangarLocker)
        {
        }

        [MethodImpl(0x8000)]
        public void AddOrUpdateGuildFleetDetailInfo(uint basicInfoVersion, uint memberInfoVersion, GuildFleetInfo fleetInfo, List<GuildFleetMemberDynamicInfo> fleetMemberDynamicInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSovereignLostReport(SovereignLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public int CaculateFinalBenefitsListVersion(int originalListVersion, List<GuildBenefitsInfoBase> benefitsList, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public int CaculateOriginalBenefitsListVersion(int finalListVersion, List<GuildBenefitsInfoBase> benefitsList, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> CalcGuildProductionItemRealCost(int solarSystemId, GuildProduceCategory category, List<CostInfo> configCostList, int count)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcGuildProductionTimeRealCost(int solarSystemId, GuildProduceCategory category, int configTime, int count)
        {
        }

        [MethodImpl(0x8000)]
        public ulong CalcGuildProductionTradeMoneyRealCost(int solarSystemId, GuildProduceCategory category, ulong configMoneyCost, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void CancelGuildPurchaseOrder(ulong OrderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool CanSendGuildMessage()
        {
        }

        [MethodImpl(0x8000)]
        public bool ChangeFlagShipFuel(ulong shipHangarInsId, int slotIndex, ulong fuelInstanceId, int fuelIndex, int fuelSupplementCount, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckFleetOperationCdTime(GuildFleetOperationType operation)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForActivateAntiTeleportEffect(string gameUserId, ulong buildingInsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForAntiTeleportInfoReq(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetCreating(string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetDetailInfoReq(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetDismiss(ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetKickOut(ulong fleetId, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetListReq(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetMemberJoin(ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetMemberLeave(ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetRename(ulong fleetId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetRename4DomesticPackage(ulong fleetId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetSetAllowToJoinFleetManual(ulong fleetId, bool allowToJoinFleetManual, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetSetFormationActive(ulong fleetId, bool isActive, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetSetFormationType(ulong fleetId, int formationType, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildBattleSimpleReportReq(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipAddModuleEquip(ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, int equipItemStoreIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipCaptainRegister(ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, List<int> drivingLicenseIdList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipCaptainUnregister(ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarInfoReq(string gameUserId, int solarSystemId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarLocking(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarScanning(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarUnlocking(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipPack(ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipRemoveModuleEquip(ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipRename(ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipRename4DomesticPackage(ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipSupplementFuel(ulong shipHangarInsId, List<ProGuildFlagShipHangarShipSupplementFuelInfo> shipSupplementFuelInfoList, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipUnpack(ulong shipHangarInsId, int hangarSlotIndex, ulong shipItemStoreInsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetFireFocusTargetReq(ulong fleetId, ILBInSpacePlayerShip selfShip, ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetMemberRetire(ulong fleetId, string destGameUserId, List<uint> positionList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetMembersJumping(ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetMembersUseStargate(ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetProtectTargetReq(ulong fleetId, ILBInSpacePlayerShip selfShip, ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetSetCommanderReq(ulong fleetId, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetSetInternalPositions(ulong fleetId, GuildFleetMemberInfo member, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetTransferPosition(ulong fleetId, string destGameUserId, FleetPosition position, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildProductionDataVersion(uint version)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildSolarSystemBasicInfoValid(int solarSystemId, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildSolarSystemBattleInfoValid(int solarSystemId, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildSolarSystemBuildingInfoValid(int solarSystemId, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckMiningBalanceTimeRefreashCD(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckPlayerGuildFleetSettingUpdate(ILBInSpacePlayerShip selfShip, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckPlayerGuildInfo4Production(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> CheckProduceTempleteUnlockCondition(ConfigDataGuildProduceTempleteInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProduceTempleteUnlockCondition(ConfigDataGuildProduceTempleteInfo confInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineCancel(ulong lineId, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineComplete(ulong lineId, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineSpeedUpByTradeMoney(ulong lineId, int speedUpLevel, ulong currTradeMoney, DateTime currTime, out int errCode, out int costTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineStart(ulong lineId, int templeteId, int produceCount, out int errCode, out ulong moneyCost, out int timeCost, out List<CostInfo> costList)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearGuildInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearGuildProductionLineData(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearGuildSentryInterestScene()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseCompensation(ulong instanceId, CompensationCloseReason reason)
        {
        }

        [MethodImpl(0x8000)]
        private int CompareMember(GuildMemberInfo lhs, GuildMemberInfo rhs, MemberTitleArrowState mainSortJobState, GuildJobType mainJobType)
        {
        }

        [MethodImpl(0x8000)]
        public int CompareMember(GuildMemberInfo lhs, GuildMemberInfo rhs, MemberTitleType mainSortType, MemberTitleArrowState memberTitleArrowState)
        {
        }

        [MethodImpl(0x8000)]
        public void DonateCompensation(LossCompensation compensation, string playerUserId, long contributionAdd)
        {
        }

        [MethodImpl(0x8000)]
        public bool ExistNewMessage(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildActionInfo GetAcionInfoByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<IGuildFlagShipHangarDataContainer> GetAllFlagShipHangarList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAntiTeleportEffectInfo> GetAllGuildAntiTeleportEffectList()
        {
        }

        [MethodImpl(0x8000)]
        public AllianceBasicInfo GetAllianceBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public AllianceBasicInfo GetAllianceBasicInfoById(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBattleSimpleReportInfo> GetAllianceGuildBattleSimpleReportInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public AllianceInfo GetAllianceInfo()
        {
        }

        [MethodImpl(0x8000)]
        private List<AllianceInviteInfo> GetAllianceInviteList()
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceMemberInfo> GetAllianceMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAntiTeleportEffectInfoVersion()
        {
        }

        [MethodImpl(0x8000)]
        public GuildApplyListInfo GetApplyListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBenefitsInfoBase GetBenefits(ulong benefitsInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public BuildingLostReport GetBuildingLostReport(ulong reportId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LossCompensation> GetCompensationListByShipType(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCompensationListVersion()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfoClient> GetCurrAvailableTeleportTunnelInfo()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetCurrMomentsBriefInfoVersion()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetDiplomacyDetailInfoVersion(bool friendList, bool enemyList)
        {
        }

        [MethodImpl(0x8000)]
        public int GetDonateTimesToday()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFinalBenefitsListVersion(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetFleetFireFocusTargetId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetFleetProtectTargetId()
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, List<GuildActionInfo>> GetGuildActionInfos()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAllianceInviteInfo> GetGuildAllianceInviteList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectById(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectBySolarSystemId(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBasicInfo GetGuildBaseInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBattleSimpleReportInfo> GetGuildBattleCompletedSimpleReportInfos()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleReportInfo GetGuildBattleReportInfo(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBattleSimpleReportInfo> GetGuildBattleSimpleReportInfos()
        {
        }

        [MethodImpl(0x8000)]
        public GuildCurrencyInfo GetGuildCurrency()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildCurrencyLogInfo> GetGuildCurrencyLogList()
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyInfo GetGuildDiplomacyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyState GetGuildDiplomacyState(string playerId, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildDynamicInfo GetGuildDynamicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarById(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFlagShipOptLogInfo> GetGuildFlagShipOptLogInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public void GetGuildFleetDataVersion(ulong fleetId, out uint basicInfoVersion, out uint memberListInfoVersion)
        {
        }

        [MethodImpl(0x8000)]
        public int GetGuildFleetFormationType(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetInfo GetGuildFleetInfo(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetGuildFleetIsFormationActive(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo GetGuildFleetMemberByPosition(FleetPosition pos)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFleetMemberDynamicInfo> GetGuildFleetMemberDynamicInfoList(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public void GetGuildGeneralInfoByGuildId(uint guildId, out GuildBasicInfo basicInfo, out GuildSimpleRuntimeInfo simpleInfo, out GuildDynamicInfo dynamicInfo, out List<GuildMomentsInfo> momentsBriefInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetGuildItemByStoreItemIndex(int storeItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public long GetGuildItemCountByItemId(StoreItemType type, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> GetGuildItemStoreInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override List<GuildJobType> GetGuildJob()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMessageCacheInfo GetGuildMessageInfo(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildMessageVersion()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGuildMiningBalanceTimeHour()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGuildMiningBalanceTimeMinute()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMomentsInfo> GetGuildMomentsInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public long GetGuildPrice(int auctionId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildProductionDataVersion()
        {
        }

        [MethodImpl(0x8000)]
        public GuildProductionLineInfo GetGuildProductionLineById(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildProductionLineInfo> GetGuildProductionLineList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildPurchaseOrderInfo> GetGuildPurchaseOrderListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimplestInfo GetGuildSimpleInfoById(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public float GetGuildSolarSystemDelegateMissionRewardMulti(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfoClient GetGuildSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetGuildTradePortDataVersion()
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePortExtraInfo GetGuildTradePortExtraInfoBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradePortExtraInfo> GetGuildTradePortExtraInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradePurchaseInfo> GetGuildTradePurchaseInfoListByItem(int itemConfigId, StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradeTransportInfo> GetGuildTradeTransportInfoList2Self()
        {
        }

        [MethodImpl(0x8000)]
        public RankType GetHangarSlotRankType(IGuildFlagShipHangarDataContainer shipHangarDC, int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHangarUnlockSlotCount(IGuildFlagShipHangarDataContainer shipHangarDC)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberInfo GetMemberInfo(string userId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberListInfo GetMemberListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcGuildInfo GetNpcGuildInfo(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetOtherGuildMomentsBriefInfoListByGuildId(uint guildId, out List<GuildMomentsInfo> momentsBriefInfoList)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetSelfChangedPositionList(ulong fleetId, List<GuildFleetMemberInfo> memberList)
        {
        }

        [MethodImpl(0x8000)]
        public long GetSelfDonateTradeMoneyToday()
        {
        }

        [MethodImpl(0x8000)]
        public GuildClient GetSelfGuild()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMomentsInfo> GetSelfGuildMomentsBriefInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePurchaseInfo GetSelfGuildTradePurchaseInfoBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradeTransportInfo GetSelfGuildTradeTransportInfoBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, GuildTradeTransportInfo> GetSelfGuildTradeTransportInfoDict()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBasicInfo GetSelfMemberBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberDynamicInfo GetSelfMemberDynamicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<IStaticFlagShipDataContainer> GetShipHangarFlagShipInstanceInfoList(ulong shipHangarInsId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipHangarLockMaster()
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemGuildBattleStatus GetSolarSystemGuildBattleStatus(int starfieldId, int solarSyatemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<SovereignLostReport> GetSovereignLostReportList()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetSpaceShipConfigInfoByTradePortSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildStaffingLogFullInfo GetStaffingLogListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ProSolarSystemGuildBattleStatusInfo> GetStarfieldGuildBattleStatus(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetStarfieldGuildBattleStatusDataVersion(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticFlagShip GetStaticFlagShip(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticFlagShip> GetStaticFlagShipList(ulong shipHangarInsId)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildBattleReinforcementEndTimeSet(int endHour, int endMinuts)
        {
        }

        [MethodImpl(0x8000)]
        private int GuildFlagShipSupplementFuelInfoComparer(ProGuildFlagShipHangarShipSupplementFuelInfo fuelInfoA, ProGuildFlagShipHangarShipSupplementFuelInfo fuelInfoB)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildMiningBalanceTimeReset(int hour, int minute)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseAuctionIdAndPriceListSet(List<ProIdValueInfo> info)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderCancel()
        {
        }

        [MethodImpl(0x8000)]
        public bool GuildPurchaseOrderCommitForSale(GuildPurchaseOrderInfo info, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderCreate(GuildPurchaseOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderListUpdate(List<ProGuildPurchaseOrderListItemInfo> orderList)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderModify(bool isCancel, ulong instanceId, GuildPurchaseOrderInfo modifyOrder)
        {
        }

        [MethodImpl(0x8000)]
        private GuildSolarSystemInfoClient GuildSolarSystemBasicCaChedInfoUpdate(GuildSolarSystemBasicInfoAck ack, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        private GuildSolarSystemInfoClient GuildSolarSystemBattleCaChedInfoUpdate(GuildSolarSystemBattleInfoAck ack, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        private GuildSolarSystemInfoClient GuildSolarSystemBuildingCaChedInfoUpdate(GuildSolarSystemBuildingInfoAck ack, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradePortExtraInfoListUpdate(List<GuildTradePortExtraInfo> guildTradePortExtraInfoList, ushort dataVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradePortExtraInfoUpdate(GuildTradePortExtraInfo guildTradePortExtraInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradePurchaseInfoListUpdate(int itemConfigId, StoreItemType itemType, List<GuildTradePurchaseInfo> guildTradePurchaseInfoList, List<ulong> removeList, bool isRefreshList, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradeTransportInfoList2SelfUpdate(List<GuildTradeTransportInfo> guildTradeTransportInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyAllianceInvite()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyBenefits2Balance()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyGuildJoinApply()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasNewGuildMessage()
        {
        }

        [MethodImpl(0x8000)]
        private void InitAlliance(AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void InitGuildFleetDS(ulong fleetId, uint basicInfoVersion, uint memberInfoVersion, GuildFleetInfo fleetInfo, List<GuildFleetMemberDynamicInfo> fleetMemberDynamicInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void Initialize_GuildFleetOperationCdInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void InitMemberSotrFunc()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEnemy(string playerUserId, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFriend(string playerUserId, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildFleetAllowToJoinFleetManual(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildFleetDSInitialized()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildSentryProbeAvailable(int solarSyetmId, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildSentrySignalInCountDown()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHangarSlotUnlock(IGuildFlagShipHangarDataContainer shipHangarDC, int index)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSelfGuildFleetMember(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSolarSystemOccupiedByNpc(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        private int MemberCompareByContribution(MemberTitleArrowState state, GuildMemberInfo lhs, GuildMemberInfo rhs)
        {
        }

        [MethodImpl(0x8000)]
        private int MemberCompareByGalaxy(MemberTitleArrowState state, GuildMemberInfo lhs, GuildMemberInfo rhs)
        {
        }

        [MethodImpl(0x8000)]
        private int MemberCompareByGrade(MemberTitleArrowState state, GuildMemberInfo lhs, GuildMemberInfo rhs)
        {
        }

        [MethodImpl(0x8000)]
        private int MemberCompareByJob(MemberTitleArrowState state, GuildMemberInfo lhs, GuildMemberInfo rhs)
        {
        }

        [MethodImpl(0x8000)]
        private int MemberCompareByJobCtrl(GuildMemberInfo lhs, GuildMemberInfo rhs, GuildJobType job, MemberTitleArrowState state)
        {
        }

        [MethodImpl(0x8000)]
        private int MemberCompareByLastOnline(MemberTitleArrowState state, GuildMemberInfo lhs, GuildMemberInfo rhs)
        {
        }

        [MethodImpl(0x8000)]
        private int MemberCompareByLevel(MemberTitleArrowState state, GuildMemberInfo lhs, GuildMemberInfo rhs)
        {
        }

        [MethodImpl(0x8000)]
        public bool NeedUpdateDiplomacyInfo(bool friendList, bool enemyList)
        {
        }

        [MethodImpl(0x8000)]
        public bool NeedUpdateGuildInfo(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceCreate(AllianceInfo alliance, int guildTradeMoneyCost = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceDismiss()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceGuildBattleSimpleReportInfoAck(AllianceGuildBattleSimpleReportInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceInviteAccept(AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceInviteRemove(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceInviteSend(AllianceInviteInfo invite)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceLeaderTransfer(uint newLeaderGuildId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceLogoSet(AllianceLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceMemberLeave()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceMemberRemove(uint removedGuildId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceNameSet(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceRegionAndAnnouncementSet(int languageType, string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildActionCreateAck(GuildActionCreateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildActionDeleteAck(GuildActionDeleteAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildActionInfoAck(GuildActionInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildActionInfoUpdateAck(GuildActionInfoUpdateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildAllianceInfoChange(uint allianceId, string allianceName, AllianceLogoInfo allianceLogo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildAllianceInviteUnread(GuildAllianceInviteInfo invite)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildApplyForListAck(GuildJoinApplyListAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBaseSolarSystemSetAck()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBattleReportInfoAck(GuildBattleReportInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBattleSimpleReportInfoAck(GuildBattleSimpleReportInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBuildingDeployUpgradeAck(GuildBuildingDeployUpgradeAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBuildingRecycleAck(GuildBuildingRecycleAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeAnnouncementNtf(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeBasicInfoNtf(GuildAllianceLanguageType languageType, GuildJoinPolicy joinPolicy)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeManifestoNtf(string manifesto)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeNameAndCodeNtf(string name, string code)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCompensationListAck()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildCreateAck(GuildCreateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildDiplomacyDetailInfoAck(GuildDiplomacyDetailInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildDiplomacyUpdateNtf(GuildDiplomacyUpdateNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildGameGuildDonateAck(long donateTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildJoinNtf(GuildJoinNtf msg)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildLeaderTransferAbortAck(GuildLeaderTransferAbortAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildLogoInfoNtf(GuildLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMainPanelInfoAck(GuildBasicInfo basicInfo, GuildSimpleRuntimeInfo simpleRuntimeInfo, GuildDynamicInfo dynamicInfo, uint messageVersion, List<GuildMomentsInfo> momentsBriefInfoList, uint momentsBriefInfoVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberJobUpdateAck(GuildMemberJobUpdateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberJobUpdateNtf(GuildMemberJobUpdateNtf msg)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberListInfoAck(GuildMemberListInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMessageDeleteAck(GuildMessageDeleteAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMessageGetAck(GuildMessageGetAck ack, bool isForExternal)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMessageWriteAck(GuildMessageWriteAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildOccupiedSolarSystemInfoAck(GuildOccupiedSolarSystemInfoAck ack, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildRedPointVersionAck(GuildRedPointVersionAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSearch4JoinAck(GuildSearch4JoinAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSelfInfoNtf(GuildSelfInfoNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSentryProbeUseAck(List<ProGuildSentrySceneInfo> sceneInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSolarSystemBasicInfoAck(GuildSolarSystemBasicInfoAck ack, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSolarSystemBattleInfoAck(GuildSolarSystemBattleInfoAck ack, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSolarSystemBuildingInfoAck(GuildSolarSystemBuildingInfoAck ack, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradePurchaseOrderCreate(int solarSystemId, ulong purchaseInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradePurchaseOrderRemove(int solarSystemId, ulong purchaseInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradeTransportPlanComplete(int solarSystemId, ulong transportInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradeTransportPlanCreate(int solarSystemId, ulong transportInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildWalletInfoAck(long DonateTradeMoneyToday, long DonateTradeMoneyWeek)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlayerGuildInfoInitAck(PlayerGuildInfoInitAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSelfLeaveGuild(string selfGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSetDismissEndTime(long dismissEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStaffingLogListAck(GuildStaffingLogListAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTargetGuildMainPanelInfoAck(GuildBasicInfo basicInfo, GuildSimpleRuntimeInfo simpleRuntimeInfo, GuildDynamicInfo dynamicInfo, List<GuildMomentsInfo> momentsBriefInfos)
        {
        }

        [MethodImpl(0x8000)]
        public bool PackFlagShipToItemStore(ulong shipHangarInsId, int hangarIndex, int resumeStoreItemIndex, ulong resumeItemInsId, List<ProStoreItemTransformInfo> resumedShipEquipInfo, out LBStoreItem toItem)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void ReceiveBenefits(ulong instanceId, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void RefleshSovereignLostReportList(List<SovereignLostReport> sovereignLostReportList)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllianceInfo(AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllianceMemberList(List<AllianceMemberInfo> memberList)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshBenefitsList(List<GuildBenefitsInfoBase> benefitsList, int finalListVersion, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshCompensationList(List<LossCompensation> compensations, int version)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshGuildAllianceInviteList(List<GuildAllianceInviteInfo> inviteList)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshGuildBuildingInfo(GuildBuildingInfo buildInfo, ProGuildBuildingInfo proBuildInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshGuildCurrencyLogList(List<GuildCurrencyLogInfo> list)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshGuildFleetMemberList(ulong fleetId, List<GuildFleetMemberInfo> memberList)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshProductionLineList(List<GuildProductionLineInfo> lineList)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFlagShipDriver(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveBenefits(ulong benefitsInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveFlagShipModuleEquipFromShip(GuildFlagShipHangarShipRemoveModuleEquip2SlotAck msg, out bool isItemStoreContextChanged)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveFleetMemberPosition(ulong fleetId, string gameUserId, List<uint> positionList)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveGuildActionByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveGuildFleetInfo(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveGuildFleetMember(ulong fleetId, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveGuildMember(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveGuildProductionLine(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        private void RemoveItemFromEnemyViewInfo(PlayerSimplestInfo player, GuildSimplestInfo guild, AllianceBasicInfo alliance)
        {
        }

        [MethodImpl(0x8000)]
        private void RemoveItemFromFriendViewInfo(PlayerSimplestInfo player, GuildSimplestInfo guild, AllianceBasicInfo alliance)
        {
        }

        [MethodImpl(0x8000)]
        public void SelfGuildTradePurchaseInfoRemove(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void SelfGuildTradePurchaseInfoUpdateOrAdd(GuildTradePurchaseInfo guildTradePurchaseInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SelfGuildTradeTransportInfoRemove(List<ulong> transportInstanceIdList)
        {
        }

        [MethodImpl(0x8000)]
        public void SelfGuildTradeTransportInfoRemove(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void SelfGuildTradeTransportInfoUpdateOrAdd(GuildTradeTransportInfo guildTradeTransportInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAutoCompensation(bool isAuto)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlagShipCustomName(ulong shipHangarInsId, int slotIndex, string name)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetFlagShipModuleEquipToShip(GuildFlagShipHangarShipAddModuleEquip2SlotAck msg, out bool isItemStoreContextChanged)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFleetCommander(ulong fleetId, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFleetFireFocusTargetId(uint targetId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFleetProtectTargetId(uint targetId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildFleetAllowToJoinManual(ulong fleetId, bool isAllow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildFleetFormationActive(ulong fleetId, bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildFleetFormationType(ulong fleetId, int formationType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildMessageVersionData()
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildPrice(List<ProIdValueInfo> priceList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuldProductionDataVersion(uint version)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSelfAtMemberListFirst(List<GuildMemberInfo> members)
        {
        }

        [MethodImpl(0x8000)]
        public void SortMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public void SortMemberWithJobCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public void SortMemberWithMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void TransferFleetMemberPosition(ulong fleetId, string destGameUserId, FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetGuildSimpleInfoList(List<uint> allianceIdList, out List<AllianceBasicInfo> allianceBasicInfoList, ref List<uint> needReqAllianceIdList)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetGuildSimpleInfoList(List<uint> guildIdList, out List<GuildSimplestInfo> guildSimpleInfoList, ref List<uint> needReqGuildIdList)
        {
        }

        [MethodImpl(0x8000)]
        public bool UnpackFlagShipFromItemStore(ulong shipHangarInsId, ulong shipPackedItemInsId, int hangarIndex, ulong shipUnpackedItemInsId, int unpackedItemIndex, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterFlagShipDriver(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllianceBasicInfoCache(List<AllianceBasicInfo> newInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCompensation(LossCompensation compensation)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCompensationDonateToday(string playerUserId, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrAvailableTeleportTunnelInfo(List<GuildTeleportTunnelEffectInfoClient> list)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFlagShipHangarLockMaster(string masterId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFlagShipName(ulong shipHangarInsId, int slotIndex, string name)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFleetOperationCdTime(GuildFleetOperationType operation)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildAntiTeleportInfoList(List<GuildAntiTeleportEffectInfo> list, int dataVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildCurrency(GuildCurrencyInfo currency)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildCurrStarfieldGuildBattleInfo(int starfieldId, uint dataVersion, List<ProSolarSystemGuildBattleStatusInfo> stateInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildDynamicInfo(GuildDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFlagShipOptLogInfoList(List<GuildFlagShipOptLogInfo> logInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetListSimpleInfoList(List<GuildFleetSimpleInfo> simpleInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetMemberBasicInfoList(ulong fleetId, List<GuildFleetMemberInfo> memberList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetName(ulong fleetId, string name)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetSimpleInfo(GuildFleetSimpleInfo simpleInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildItemStoreInfo(List<GuildStoreItemListInfo> itemList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildMomentsInfoList(List<GuildMomentsInfo> momentsInfoList, bool isOlder = true)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildProductionLine(GuildProductionLineInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool UpdateGuildPurchaseOrder(GuildPurchaseOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildSimpleInfoCache(List<GuildSimplestInfo> newInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildSolarSystemDelegateMissionRewardMulti(int solarSystemId, float rewardMulti)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLastMiningBalanceTimeRefreashTime(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMessageVersion()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProductionLineSpeedUpTime(ulong lineId, int reduceTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelfMemberDynamicInfo(GuildMemberDynamicInfo info)
        {
        }

        public List<ProGuildSimplestInfo> GuildList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<uint> SelfApplyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool SortByMemberList
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool SelfAtMemberListFirst
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public MemberTitleArrowState MemberListMainArrowState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public MemberTitleType MemberListMainTitleType
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public string GuildJobCtrlEditUseId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public BlackJack.ProjectX.Common.GuildDiplomacyFriendlyViewInfo GuildDiplomacyFriendlyViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public BlackJack.ProjectX.Common.GuildDiplomacyEnemyViewInfo GuildDiplomacyEnemyViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected ILBPlayerContextClient PlayerCtxClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<GuildFleetSimpleInfo> GetGuildFleetSimpleInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int RedPointGuildJoinApplyCount
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool RedPointGuildBenefits
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int RedPointGuildAllianceInviteCount
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildFleetMemberByPosition>c__AnonStorey4
        {
            internal FleetPosition pos;

            internal bool <>m__0(GuildFleetMemberInfo member) => 
                member.CheckFleetPosition(this.pos);
        }

        [CompilerGenerated]
        private sealed class <RemoveItemFromEnemyViewInfo>c__AnonStorey1
        {
            internal PlayerSimplestInfo player;
            internal GuildSimplestInfo guild;
            internal AllianceBasicInfo alliance;

            internal bool <>m__0(PlayerSimplestInfo item) => 
                (item.m_gameUserId == this.player.m_gameUserId);

            internal bool <>m__1(GuildSimplestInfo item) => 
                (item.m_guild == this.guild.m_guild);

            internal bool <>m__2(AllianceBasicInfo item) => 
                (item.m_allianceId == this.alliance.m_allianceId);
        }

        [CompilerGenerated]
        private sealed class <RemoveItemFromFriendViewInfo>c__AnonStorey0
        {
            internal PlayerSimplestInfo player;
            internal GuildSimplestInfo guild;
            internal AllianceBasicInfo alliance;

            internal bool <>m__0(PlayerSimplestInfo item) => 
                (item.m_gameUserId == this.player.m_gameUserId);

            internal bool <>m__1(GuildSimplestInfo item) => 
                (item.m_guild == this.guild.m_guild);

            internal bool <>m__2(AllianceBasicInfo item) => 
                (item.m_allianceId == this.alliance.m_allianceId);
        }

        [CompilerGenerated]
        private sealed class <SetSelfAtMemberListFirst>c__AnonStorey2
        {
            internal PlayerSimpleInfo simpleInfo;

            internal bool <>m__0(GuildMemberInfo member) => 
                (member.m_basicInfo.m_gameUserId == this.simpleInfo.m_playerGameUserId);
        }

        [CompilerGenerated]
        private sealed class <UpdateGuildFleetSimpleInfo>c__AnonStorey3
        {
            internal GuildFleetSimpleInfo simpleInfo;

            internal bool <>m__0(GuildFleetSimpleInfo elem) => 
                (elem.m_fleetInstanceId == this.simpleInfo.m_fleetInstanceId);
        }

        public class GuildApplyListInfo
        {
            public uint version;
            public List<GuildJoinApplyViewInfo> applyList = new List<GuildJoinApplyViewInfo>();
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Clear;

            public GuildApplyListInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.applyList.Clear();
                    this.version = 0;
                }
            }
        }

        private class GuildFleetOperationCDInfo
        {
            private GuildFleetOperationType m_operationType;
            private float m_intervalTime;
            private DateTime m_nextOperationTime;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_UpdateNextOperationTime;
            private static DelegateBridge __Hotfix_CheckOperationCdTime;
            private static DelegateBridge __Hotfix_get_OperationType;

            public GuildFleetOperationCDInfo(GuildFleetOperationType operationType, float intervalTime)
            {
                this.m_operationType = operationType;
                this.m_intervalTime = intervalTime;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3143(this, operationType, intervalTime);
                }
            }

            public bool CheckOperationCdTime()
            {
                DelegateBridge bridge = __Hotfix_CheckOperationCdTime;
                return ((bridge == null) ? (this.m_nextOperationTime < DateTime.Now) : bridge.__Gen_Delegate_Imp9(this));
            }

            public void UpdateNextOperationTime()
            {
                DelegateBridge bridge = __Hotfix_UpdateNextOperationTime;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_nextOperationTime = DateTime.Now.AddSeconds((double) this.m_intervalTime);
                }
            }

            public GuildFleetOperationType OperationType
            {
                get
                {
                    DelegateBridge bridge = __Hotfix_get_OperationType;
                    return ((bridge == null) ? this.m_operationType : bridge.__Gen_Delegate_Imp3142(this));
                }
            }
        }

        public class GuildMessageCacheInfo
        {
            public uint guildId;
            public GuildMessageBoardInfo listInfo = new GuildMessageBoardInfo();
            public DateTime earliestGuildMessageTime;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_HasMore;
            private static DelegateBridge __Hotfix_Clear;
            private static DelegateBridge __Hotfix_Remove;
            private static DelegateBridge __Hotfix_GetCacheLastGuildMessageTime;

            public GuildMessageCacheInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.guildId = 0;
                    this.listInfo.m_messageList.Clear();
                    this.listInfo.m_version = 0;
                    this.earliestGuildMessageTime = DateTime.MinValue;
                }
            }

            public DateTime GetCacheLastGuildMessageTime()
            {
                DelegateBridge bridge = __Hotfix_GetCacheLastGuildMessageTime;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp100(this);
                }
                if (this.listInfo.m_messageList.Count <= 0)
                {
                    return DateTime.MinValue;
                }
                int num = this.listInfo.m_messageList.Count - 1;
                return this.listInfo.m_messageList[num].m_time;
            }

            public bool HasMore()
            {
                DelegateBridge bridge = __Hotfix_HasMore;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                if (this.listInfo.m_messageList.Count <= 0)
                {
                    return true;
                }
                int num = this.listInfo.m_messageList.Count - 1;
                return (this.earliestGuildMessageTime < this.listInfo.m_messageList[num].m_time);
            }

            public void Remove(ulong instanceId, string adminUserId, string adminName)
            {
                DelegateBridge bridge = __Hotfix_Remove;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3018(this, instanceId, adminUserId, adminName);
                }
                else
                {
                    foreach (GuildMessageInfo info in this.listInfo.m_messageList)
                    {
                        if (info.m_instanceId == instanceId)
                        {
                            info.m_delete = true;
                            info.m_deleteAdminGameUserId = adminUserId;
                            info.m_deleteAdminName = adminName;
                            this.listInfo.m_version = (ushort) (this.listInfo.m_version + 1);
                            break;
                        }
                    }
                }
            }
        }

        public class GuildStaffingLogFullInfo
        {
            public GuildStaffingLogListInfo listInfo = new GuildStaffingLogListInfo();
            public DateTime earliestLogTime;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_HasMore;
            private static DelegateBridge __Hotfix_GetCacheLastLogTime;
            private static DelegateBridge __Hotfix_Clear;

            public GuildStaffingLogFullInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.listInfo.m_logList.Clear();
                }
            }

            public DateTime GetCacheLastLogTime()
            {
                DelegateBridge bridge = __Hotfix_GetCacheLastLogTime;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp100(this);
                }
                if (this.listInfo.m_logList.Count <= 0)
                {
                    return DateTime.MinValue;
                }
                int num = this.listInfo.m_logList.Count - 1;
                return this.listInfo.m_logList[num].m_time;
            }

            public bool HasMore()
            {
                DelegateBridge bridge = __Hotfix_HasMore;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                if (this.listInfo.m_logList.Count <= 0)
                {
                    return true;
                }
                int num = this.listInfo.m_logList.Count - 1;
                return (this.earliestLogTime < this.listInfo.m_logList[num].m_time);
            }
        }

        public enum MemberTitleArrowState
        {
            Down = -1,
            Empty = 0,
            Up = 1
        }

        public enum MemberTitleType
        {
            LastOnLine,
            Job,
            Level,
            StationGalaxy,
            Contribution,
            Grade
        }

        private class OtherGuildMainPanelInfo
        {
            public GuildBasicInfo m_basicInfo;
            public GuildSimpleRuntimeInfo m_simpleRuntimeInfo;
            public GuildDynamicInfo m_dynamicInfo;
            public List<GuildMomentsInfo> m_momentsBriefInfoList = new List<GuildMomentsInfo>();
            public DateTime m_nextUpdateTime;
            private static DelegateBridge _c__Hotfix_ctor;

            public OtherGuildMainPanelInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

