﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AllianceInfoDataDict
    {
        private static AllianceInfoDataDict m_instance;
        private readonly Dictionary<uint, AllianceInfoReference> m_allianceTable;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_SetAllianceInfo;
        private static DelegateBridge __Hotfix_UpdateAllianceInfo;
        private static DelegateBridge __Hotfix_DeleteAllianceInfo;
        private static DelegateBridge __Hotfix_ClearAllianceInfo;
        private static DelegateBridge __Hotfix_GetAllianceInfoById;
        private static DelegateBridge __Hotfix_ReleaseAllianceInfoById;

        [MethodImpl(0x8000)]
        public static void ClearAllianceInfo()
        {
        }

        [MethodImpl(0x8000)]
        public static void DeleteAllianceInfo(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public static AllianceInfo GetAllianceInfoById(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public static void ReleaseAllianceInfoById(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetAllianceInfo(AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static bool UpdateAllianceInfo(AllianceInfo info)
        {
        }

        private static AllianceInfoDataDict Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private class AllianceInfoReference
        {
            public AllianceInfo m_allianceInfo;
            public uint m_reference;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

