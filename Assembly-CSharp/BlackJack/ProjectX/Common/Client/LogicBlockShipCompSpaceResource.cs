﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompSpaceResource : LogicBlockShipCompResourceBase
    {
        protected ILBInSpaceShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetHighSlotGroupCount;
        private static DelegateBridge __Hotfix_GetMiddleSlotGroupCount;
        private static DelegateBridge __Hotfix_GetLowSlotGroupCount;
        private static DelegateBridge __Hotfix_GetSuperWeaponConfigInfo;
        private static DelegateBridge __Hotfix_GetSuperEquipConfigInfo;
        private static DelegateBridge __Hotfix_GetTacticalEquipConfInfo;
        private static DelegateBridge __Hotfix_GetHighSlotGroupByIndex;
        private static DelegateBridge __Hotfix_GetMiddleSlotGroupByIndex;
        private static DelegateBridge __Hotfix_GetLowSlotGroupByIndex;
        private static DelegateBridge __Hotfix_GetStaticDefaultBoosterGroup;
        private static DelegateBridge __Hotfix_GetStaticDefaultRepairerGroup;
        private static DelegateBridge __Hotfix_GetShipGrandFaction;
        private static DelegateBridge __Hotfix_GetTacticalEquipIconResPath;
        private static DelegateBridge __Hotfix_GetSuperWeaponIconResPath;
        private static DelegateBridge __Hotfix_GetSuperEquipIconResPath;

        [MethodImpl(0x8000)]
        protected override ConfigDataSpaceShipInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStaticWeaponEquipSlotGroup GetHighSlotGroupByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override int GetHighSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStaticWeaponEquipSlotGroup GetLowSlotGroupByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override int GetLowSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStaticWeaponEquipSlotGroup GetMiddleSlotGroupByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override int GetMiddleSlotGroupCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override GrandFaction GetShipGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStaticWeaponEquipSlotGroup GetStaticDefaultBoosterGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStaticWeaponEquipSlotGroup GetStaticDefaultRepairerGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected override ConfigDataShipSuperEquipInfo GetSuperEquipConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetSuperEquipIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected override ConfigDataSuperWeaponInfo GetSuperWeaponConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetSuperWeaponIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected override ConfigDataShipTacticalEquipInfo GetTacticalEquipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override List<string> GetTacticalEquipIconResPath()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip playerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }
    }
}

