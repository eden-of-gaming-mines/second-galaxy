﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipHangarsClient : LogicBlockShipHangarsBase
    {
        public DateTime LastShipHangerListUpdateTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_UnpackShipFromItemStore;
        private static DelegateBridge __Hotfix_CreateLBStaticPlayerShip;
        private static DelegateBridge __Hotfix_RenewShipHangerInfo;
        private static DelegateBridge __Hotfix_PackShipToItemStore;
        private static DelegateBridge __Hotfix_GetShipList4Strike;
        private static DelegateBridge __Hotfix_UnloadAllShipStoreItemsFromHangarShip;
        private static DelegateBridge __Hotfix_UnloadAllShipEquipsFromHangarShip;
        private static DelegateBridge __Hotfix_DestoryAllShipEquip;
        private static DelegateBridge __Hotfix_ShipDestroyStateSyncNtf;
        private static DelegateBridge __Hotfix_get_m_lbItemStore;

        [MethodImpl(0x8000)]
        protected override ILBStaticPlayerShip CreateLBStaticPlayerShip(IStaticPlayerShipDataContainer shipDC, ILBPlayerContext playerCtx, int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override void DestoryAllShipEquip(ILBStaticPlayerShip playerShip)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticPlayerShip> GetShipList4Strike()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool PackShipToItemStore(int hangarIndex, int resumeStoreItemIndex, ulong resumeItemInsId, List<StoreItemTransformInfo> resumedShipStoreItemInfoList, List<StoreItemTransformInfo> resumedShipEquipInfoList, List<AmmoStoreItemInfo> itemStoreAmmoInfoList, out LBStoreItem toItem)
        {
        }

        [MethodImpl(0x8000)]
        public void RenewShipHangerInfo(int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void ShipDestroyStateSyncNtf(ulong shipInstanceId, ShipDestroyState destroyState)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnloadAllShipEquipsFromHangarShip(int hangarIndex, List<StoreItemTransformInfo> resumedItemInfoList, List<AmmoStoreItemInfo> ammoInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnloadAllShipStoreItemsFromHangarShip(int hangarIndex, List<StoreItemTransformInfo> resumedItemInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public bool UnpackShipFromItemStore(ulong shipPackedItemInsId, int hangarIndex, ulong shipUnpackedItemInsId, int unpackedItemIndex)
        {
        }

        protected LogicBlockItemStoreClient m_lbItemStore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

