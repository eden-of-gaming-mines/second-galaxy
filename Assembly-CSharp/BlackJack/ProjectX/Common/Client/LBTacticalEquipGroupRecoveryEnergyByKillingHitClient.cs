﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupRecoveryEnergyByKillingHitClient : LBTacticalEquipGroupRecoveryEnergyByKillingHitBase
    {
        public Action<uint> EventOnEnergyRecovery;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventGroupCD;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupRecoveryEnergyByKillingHitClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventGroupCD(uint groupCDEndTime)
        {
        }
    }
}

