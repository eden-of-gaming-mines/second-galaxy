﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    public class LogicBlockShipCompBufContainerClient : LogicBlockShipCompBufContainerBase, ILBBufOwner
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBBufBase, ILBBufSource> EventOnAttachBuf;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBBufBase, List<uint>> EventOnAttachDuplicateBuf;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBBufBase> EventOnDetachBuf;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBBufBase, bool> EventOnAttachBufFromWormhole;
        protected List<List<LBShipFightBuf>> m_shipFightBufViewList;
        protected bool m_isPostInitializeEnd;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetAllFightBufByType;
        private static DelegateBridge __Hotfix_GetFightBufViewList;
        private static DelegateBridge __Hotfix_IsAnyFightBufExist2View;
        private static DelegateBridge __Hotfix_GetAllFightBufViewList;
        private static DelegateBridge __Hotfix_GetBufListByBufType;
        private static DelegateBridge __Hotfix_OnSyncEventAttachBufInSpace;
        private static DelegateBridge __Hotfix_OnSyncEventDetachBuf;
        private static DelegateBridge __Hotfix_OnSyncEventAttachBufDuplicate;
        private static DelegateBridge __Hotfix_GetLBBuffByInstanceId;
        private static DelegateBridge __Hotfix_HasAnyBoosterBuff;
        private static DelegateBridge __Hotfix_GetBufOwner;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBBufOwner.OnAttachBuf;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBBufOwner.OnDetachBuf;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBBufOwner.OnAttachDuplicate;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBBufOwner.OnSyncAttachDuplicateInfo;
        private static DelegateBridge __Hotfix_add_EventOnAttachBuf;
        private static DelegateBridge __Hotfix_remove_EventOnAttachBuf;
        private static DelegateBridge __Hotfix_add_EventOnAttachDuplicateBuf;
        private static DelegateBridge __Hotfix_remove_EventOnAttachDuplicateBuf;
        private static DelegateBridge __Hotfix_add_EventOnDetachBuf;
        private static DelegateBridge __Hotfix_remove_EventOnDetachBuf;
        private static DelegateBridge __Hotfix_add_EventOnAttachBufFromWormhole;
        private static DelegateBridge __Hotfix_remove_EventOnAttachBufFromWormhole;

        public event Action<LBBufBase, ILBBufSource> EventOnAttachBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBBufBase, bool> EventOnAttachBufFromWormhole
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBBufBase, List<uint>> EventOnAttachDuplicateBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBBufBase> EventOnDetachBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        void ILBBufOwner.OnAttachBuf(LBBufBase buf, ILBBufSource source, object param)
        {
        }

        [MethodImpl(0x8000)]
        void ILBBufOwner.OnAttachDuplicate(LBBufBase buf, ILBBufSource bufSource, float bufInstanceParam)
        {
        }

        [MethodImpl(0x8000)]
        void ILBBufOwner.OnDetachBuf(LBBufBase buf, bool isLifeEnd, object param)
        {
        }

        [MethodImpl(0x8000)]
        void ILBBufOwner.OnSyncAttachDuplicateInfo(LBBufBase buf, List<uint> sourceTargetList, int attachCount, float bufInstanceParam)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBShipFightBuf> GetAllFightBufByType(ShipFightBufType fightBufType, bool isEnhance, BufType bufType)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable<LBShipFightBuf> GetAllFightBufViewList()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable<LBBufBase> GetBufListByBufType(BufType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override ILBBufOwner GetBufOwner()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBShipFightBuf> GetFightBufViewList(ShipFightBufType fightBufType, bool isEnhance)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetLBBuffByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyBoosterBuff()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBInSpaceShip ownerShip, List<int> staticBufList = null, List<object> fightBufList = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnyFightBufExist2View()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventAttachBufDuplicate(int configId, uint bufInstanceId, int attachCount, uint lifeEndTime, List<uint> srcTargetIdList, float bufInstanceParam)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventAttachBufInSpace(LBBufBase buf, ILBSpaceTarget bufSrc, uint lifeEndTime = 0, float bufInstanceParam = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventDetachBuf(ulong bufId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [CompilerGenerated]
        private sealed class <GetAllFightBufViewList>c__Iterator0 : IEnumerable, IEnumerable<LBShipFightBuf>, IEnumerator, IDisposable, IEnumerator<LBShipFightBuf>
        {
            internal List<LBBufBase>.Enumerator $locvar0;
            internal LBBufBase <it>__1;
            internal LogicBlockShipCompBufContainerClient $this;
            internal LBShipFightBuf $current;
            internal bool $disposing;
            internal int $PC;

            [MethodImpl(0x8000), DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [MethodImpl(0x8000), DebuggerHidden]
            IEnumerator<LBShipFightBuf> IEnumerable<LBShipFightBuf>.GetEnumerator()
            {
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<BlackJack.ProjectX.Common.LBShipFightBuf>.GetEnumerator();

            LBShipFightBuf IEnumerator<LBShipFightBuf>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <GetBufListByBufType>c__Iterator1 : IEnumerable, IEnumerable<LBBufBase>, IEnumerator, IDisposable, IEnumerator<LBBufBase>
        {
            internal List<LBBufBase>.Enumerator $locvar0;
            internal LBBufBase <it>__1;
            internal BufType type;
            internal LogicBlockShipCompBufContainerClient $this;
            internal LBBufBase $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = this.$this.m_bufContainer.GetBufList().GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        default:
                            while (true)
                            {
                                if (!this.$locvar0.MoveNext())
                                {
                                    break;
                                }
                                this.<it>__1 = this.$locvar0.Current;
                                if (this.<it>__1.GetBufType() == this.type)
                                {
                                    this.$current = this.<it>__1;
                                    if (!this.$disposing)
                                    {
                                        this.$PC = 1;
                                    }
                                    flag = true;
                                    return true;
                                }
                            }
                            break;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<LBBufBase> IEnumerable<LBBufBase>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new LogicBlockShipCompBufContainerClient.<GetBufListByBufType>c__Iterator1 { 
                    $this = this.$this,
                    type = this.type
                };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<BlackJack.ProjectX.Common.LBBufBase>.GetEnumerator();

            LBBufBase IEnumerator<LBBufBase>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

