﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBFakeStaticPlayerShipClient : ILBFakeStaticShipClient, ILBStaticPlayerShip, ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        protected ILBStaticPlayerShip m_sourceShip;
        protected LogicBlockShipCompFakeStaticWeaponEquipClient m_lbCompFakeWeaponEquip;
        protected LogicBlockShipCompFakeWeaponEquipSetupClient4PlayerShip m_lbCompFakeWeaponEquipSetup;
        protected LogicBlockShipCompFakeWeaponAmmoSetup4PlayerShipClient m_lbCompFakeWeaponAmmoSetup;
        protected LogicBlockShipCompFakeWeaponEquipSetupEnv4PlayerShip m_lbCompFakeWeaponEquipSetupEnv;
        protected LogicBlockShipCompFakeStaticPropertiesCalcClient m_lbCompFakePropertiesCalc;
        protected LogicBlockShipCompStaticBasicCtxClient m_lbCompBasicCtx;
        protected LogicBlockShipCompStaticResource m_lbCompResouce;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetLBResouce;
        private static DelegateBridge __Hotfix_GetSourceRealStaticShip;
        private static DelegateBridge __Hotfix_GetLBWeaponEquip;
        private static DelegateBridge __Hotfix_GetLBPropertiesCalc;
        private static DelegateBridge __Hotfix_GetLBStaticBasicCtx;
        private static DelegateBridge __Hotfix_GetLBCaptain;
        private static DelegateBridge __Hotfix_GetLBPlayerContext;
        private static DelegateBridge __Hotfix_GetShipDataContainer;
        private static DelegateBridge __Hotfix_GetShipConfInfo;
        private static DelegateBridge __Hotfix_GetDefaultAroundRadius;
        private static DelegateBridge __Hotfix_GetBufFactory;
        private static DelegateBridge __Hotfix_GetShipItemStoreDataContainer;
        private static DelegateBridge __Hotfix_GetLBShipItemStore;
        private static DelegateBridge __Hotfix_GetSourceShipWeaponEquip;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetup;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetupEnv;
        private static DelegateBridge __Hotfix_GetLBWeaponAmmoSetup;
        private static DelegateBridge __Hotfix_GetHangarIndex;

        [MethodImpl(0x8000)]
        public LBFakeStaticPlayerShipClient(ILBStaticPlayerShip ship, LBBuffContainer techBufContainer)
        {
        }

        [MethodImpl(0x8000)]
        public ILBBuffFactory GetBufFactory()
        {
        }

        [MethodImpl(0x8000)]
        public float GetDefaultAroundRadius(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHangarIndex()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticCaptain GetLBCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public ILBPlayerContext GetLBPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCompPropertiesCalc GetLBPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticResource GetLBResouce()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompItemStoreBase GetLBShipItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtx GetLBStaticBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponAmmoSetup4PlayerShip GetLBWeaponAmmoSetup()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticWeaponEquipBase GetLBWeaponEquip()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupBase GetLBWeaponEquipSetup()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupEnv4PlayerShip GetLBWeaponEquipSetupEnv()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetShipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public IShipDataContainer GetShipDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public IShipItemStoreDataContainer GetShipItemStoreDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip GetSourceRealStaticShip()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticWeaponEquipBase GetSourceShipWeaponEquip()
        {
        }
    }
}

