﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompFakeWeaponEquipSetupClient4PlayerShip : LogicBlockShipCompWeaponEquipSetupClient4PlayerShip
    {
        protected ILBFakeStaticShipClient m_ownerShip;
        public LogicBlockShipCompStaticWeaponEquipBase m_sourceShipWeapEquip;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnWeaponEquipUpdating;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SetItem2SlotGroup;
        private static DelegateBridge __Hotfix_RemoveItemFromSlotGroup;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetupEnv;
        private static DelegateBridge __Hotfix_GetMatchSourceStaticWeaponEquipSlotGroup;
        private static DelegateBridge __Hotfix_CheckForSetItem2SlotGroup;
        private static DelegateBridge __Hotfix_CreateFakeLBStoreItem;
        private static DelegateBridge __Hotfix_add_EventOnWeaponEquipUpdating;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponEquipUpdating;

        public event Action EventOnWeaponEquipUpdating
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected int CheckForSetItem2SlotGroup(LBStaticWeaponEquipSlotGroup group, StoreItemType itemType, int itemConfigId)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItemClient CreateFakeLBStoreItem(StoreItemType itemType, int itemConfigId, int num)
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompWeaponEquipSetupEnv GetLBWeaponEquipSetupEnv()
        {
        }

        [MethodImpl(0x8000)]
        protected LBStaticWeaponEquipSlotGroup GetMatchSourceStaticWeaponEquipSlotGroup(LBStaticWeaponEquipSlotGroup fakeGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBFakeStaticShipClient ownerShip, LogicBlockShipCompStaticWeaponEquipBase sourceShipWeapEquip)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveItemFromSlotGroup(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetItem2SlotGroup(LBStaticWeaponEquipSlotGroup group, StoreItemType itemType, int itemConfigId, bool needDefaultAmmo = true)
        {
        }
    }
}

