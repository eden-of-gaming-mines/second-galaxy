﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildAllianceClient : IGuildAllianceBase
    {
        void AllianceInviteRemove(ulong instanceId);
        void OnAllianceInfoChange(uint allianceId, string allianceName, AllianceLogoInfo allianceInfo);
        void RefreshAllianceInviteList(List<GuildAllianceInviteInfo> inviteList);
    }
}

