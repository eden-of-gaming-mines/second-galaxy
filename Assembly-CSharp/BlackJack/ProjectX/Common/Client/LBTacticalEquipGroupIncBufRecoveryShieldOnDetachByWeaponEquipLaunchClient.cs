﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupIncBufRecoveryShieldOnDetachByWeaponEquipLaunchClient : LBTacticalEquipGroupIncBuffClientBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBTacticalEquipGroupBase> EventOnRecoveryShipShield;
        protected int m_triggerCount;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnTargetDead;
        private static DelegateBridge __Hotfix_add_EventOnRecoveryShipShield;
        private static DelegateBridge __Hotfix_remove_EventOnRecoveryShipShield;

        public event Action<LBTacticalEquipGroupBase> EventOnRecoveryShipShield
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupIncBufRecoveryShieldOnDetachByWeaponEquipLaunchClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnAttachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetDead(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

