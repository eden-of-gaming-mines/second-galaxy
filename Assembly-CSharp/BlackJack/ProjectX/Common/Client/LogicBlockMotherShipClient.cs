﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockMotherShipClient : LogicBlockMotherShipBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RedployStart;
        private static DelegateBridge __Hotfix_RedeploySpeedUp;
        private static DelegateBridge __Hotfix_RedeploySpeedUpByRealMoney;
        private static DelegateBridge __Hotfix_RedeployConfirm;
        private static DelegateBridge __Hotfix_RedeployCancle;

        [MethodImpl(0x8000)]
        public bool RedeployCancle()
        {
        }

        [MethodImpl(0x8000)]
        public bool RedeployConfirm()
        {
        }

        [MethodImpl(0x8000)]
        public bool RedeploySpeedUp(int itemId, int count, MSRedeployInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool RedeploySpeedUpByRealMoney(int realMoneyCost)
        {
        }

        [MethodImpl(0x8000)]
        public void RedployStart(MSRedeployInfo info)
        {
        }
    }
}

