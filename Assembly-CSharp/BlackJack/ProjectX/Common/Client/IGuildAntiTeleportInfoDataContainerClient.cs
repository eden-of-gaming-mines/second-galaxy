﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildAntiTeleportInfoDataContainerClient : IGuildAntiTeleportInfoDataContainer
    {
        void UpdateGuildAntiTeleportInfoList(List<GuildAntiTeleportEffectInfo> list, int dataVersion);
    }
}

