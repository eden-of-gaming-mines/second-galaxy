﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompBenefitsClient : GuildCompBenefitsBase, IGuildCompBenefitsClient, IGuildCompBenefitsBase, IGuildBenefitsClient, IGuildBenefitsBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetFinalBenefitsListVersion;
        private static DelegateBridge __Hotfix_RefreshBenefitsList;
        private static DelegateBridge __Hotfix_BenefitsReceive;
        private static DelegateBridge __Hotfix_GetBenefitsList;
        private static DelegateBridge __Hotfix_HasBenefitsAvailable;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompBenefitsClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void BenefitsReceive(ulong benefitsInstanceId, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void GetBenefitsList(out List<GuildCommonBenefitsInfo> commonBenefitsList, out GuildSovereigntyBenefitsInfo sovereigntyBenefits, out List<GuildInformationPointBenefitsInfo> informationPointBenefitsList, DateTime selfJoinTime, out int benefitsListVersion)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFinalBenefitsListVersion(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasBenefitsAvailable(DateTime currServerTime, DateTime selfJoinTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshBenefitsList(List<GuildBenefitsInfoBase> benefitsList, int listVersion)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

