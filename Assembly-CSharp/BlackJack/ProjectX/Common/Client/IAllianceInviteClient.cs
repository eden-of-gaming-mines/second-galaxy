﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IAllianceInviteClient : IAllianceInviteBase
    {
        void AllianceInvite(AllianceInviteInfo invite);
    }
}

