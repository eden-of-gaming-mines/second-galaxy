﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockSolarSystemPoolCountInfoClient
    {
        private ILBPlayerContext m_lbPlayerCtx;
        private readonly Dictionary<int, Dictionary<int, int>> m_solarSystemQuestPoolCountDict;
        private readonly Dictionary<int, Dictionary<int, int>> m_solarSystemDelegateMissionPoolCountDict;
        private readonly Dictionary<int, uint> m_solarSytemPoolCountDataVersionDict;
        private DateTime m_lastStarfieldPoolCountRefreshTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_TryGetStarfieldPoolCountInfo;
        private static DelegateBridge __Hotfix_TryGetSolarSystemPoolCountInfo;
        private static DelegateBridge __Hotfix_GetSolarSystemPoolCountInitInfo;
        private static DelegateBridge __Hotfix_TryGetSolarSystemQuestPoolCountInfo;
        private static DelegateBridge __Hotfix_TryGetSolarSystemDelegateMissionPoolCountInfo;
        private static DelegateBridge __Hotfix_GetStarfieldPoolInfoDataVersion;
        private static DelegateBridge __Hotfix_GetStarfieldPoolInfo;
        private static DelegateBridge __Hotfix_UpdateSingleSolarSystemPoolInfo;
        private static DelegateBridge __Hotfix_UpdateStarfieldPoolInfo;

        [MethodImpl(0x8000)]
        public ConfigDataSolarSystemPoolCountInitInfo GetSolarSystemPoolCountInitInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, int> GetStarfieldPoolInfo(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetStarfieldPoolInfoDataVersion(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetSolarSystemDelegateMissionPoolCountInfo(int solarSystemId, out ConfigDataSolarSystemDelegateMissionPoolCountInfo poolCountInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetSolarSystemPoolCountInfo(int starfieldId, int solarSystemId, out int solarSystemPoolCount)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetSolarSystemQuestPoolCountInfo(int solarSystemId, out ConfigDataSolarSystemQuestPoolCountInfo poolCountInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetStarfieldPoolCountInfo(int starfieldId, out Dictionary<int, int> solarSystemPoolCountInfoDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleSolarSystemPoolInfo(int solarSystemId, int questPoolCount, int delegateMissionPoolCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarfieldPoolInfo(uint dataVersion, int starfieldId, List<ProSolarSystemPoolCountInfo> poolCountInfoList)
        {
        }
    }
}

