﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface ILBPlayerContextDCProviderClient
    {
        ICMDailySignDataContainerClient GetCMDailySignDC();
        ICommanderAuthDataContainerClient GetCommanderAuthDC();
        IGuildDataContainerClient GetGuildDC();
        IItemStoreRecommendMarkDataContainer GetItemStoreRecommendMarkDC();
    }
}

