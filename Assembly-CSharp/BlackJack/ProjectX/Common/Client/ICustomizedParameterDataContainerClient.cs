﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface ICustomizedParameterDataContainerClient : ICustomizedParameterDataContainer
    {
        void RefreshAllData(List<CustomizedParameterInfo> parameterList, int version);
    }
}

