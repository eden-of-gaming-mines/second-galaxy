﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildMiningClient : IGuildMiningBase
    {
        bool CheckMiningBalanceTimeRefreashCD(DateTime currTime);
        void GuildMiningBalanceTimeReset(int hour, int minute);
        void UpdateLastMiningBalanceTimeRefreashTime(DateTime currTime);
    }
}

