﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IEquipFunctionCompChannelOwnerClient : IEquipFunctionCompChannelOwnerBase, ILBSpaceProcessEquipChannelLaunchSource, IEquipFunctionCompOwner, ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
    }
}

