﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildCompAllianceClient : GuildCompAllianceBase, IGuildCompAllianceClient, IGuildCompAllianceBase, IGuildAllianceClient, IGuildAllianceBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnAllianceInfoChange;
        private static DelegateBridge __Hotfix_RefreshAllianceInviteList;
        private static DelegateBridge __Hotfix_AllianceInviteRemove;

        [MethodImpl(0x8000)]
        public GuildCompAllianceClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceInviteRemove(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceInfoChange(uint allianceId, string allianceName, AllianceLogoInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllianceInviteList(List<GuildAllianceInviteInfo> inviteList)
        {
        }
    }
}

