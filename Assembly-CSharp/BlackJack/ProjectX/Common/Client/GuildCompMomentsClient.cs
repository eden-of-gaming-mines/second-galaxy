﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompMomentsClient : GuildCompMomentsBase, IGuildCompMomentsClient, IGuildCompMomentsBase, IGuildMomentsClient, IGuildMomentsBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateGuildMomentsInfoList;
        private static DelegateBridge __Hotfix_UpdateGuildMomentsBriefInfoList;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompMomentsClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildMomentsBriefInfoList(List<GuildMomentsInfo> momentsBriefInfoList, uint momentsBriefInfoVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildMomentsInfoList(List<GuildMomentsInfo> momentsInfoList, bool isOlder)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

