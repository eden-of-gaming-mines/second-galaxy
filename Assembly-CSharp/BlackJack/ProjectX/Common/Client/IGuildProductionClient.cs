﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildProductionClient : IGuildProductionBase
    {
        void RefreshProductionLineList(List<GuildProductionLineInfo> lineList);
        void SetGuildProductionDataVersion(uint version);
    }
}

