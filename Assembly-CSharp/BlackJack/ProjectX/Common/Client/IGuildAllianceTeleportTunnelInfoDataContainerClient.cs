﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;
    using System.Collections.Generic;

    public interface IGuildAllianceTeleportTunnelInfoDataContainerClient
    {
        List<GuildTeleportTunnelEffectInfoClient> GetCurrAvailableTeleportTunnelInfo();
        void UpdateCurrAvailableTeleportTunnelInfo(List<GuildTeleportTunnelEffectInfoClient> list);
    }
}

