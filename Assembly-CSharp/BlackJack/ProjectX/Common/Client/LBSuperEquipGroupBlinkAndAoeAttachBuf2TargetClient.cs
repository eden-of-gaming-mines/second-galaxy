﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperEquipGroupBlinkAndAoeAttachBuf2TargetClient : LBSuperEquipGroupBlinkAndAoeAttachBuf2TargetBase, IEquipFunctionCompBlinkOwnerClient, IEquipFunctionCompBlinkOwnerBase, ILBSpaceProcessEquipLaunchSource, IEquipFunctionCompOwner, ILBSpaceProcessSource
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunchForBlink;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunchForAoe;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartChargeForBlink;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartChargeForAoe;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFireForBlink;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, StructProcessSuperEquipAoeLaunch> EventOnProcessFireForAoe;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancelForBlink;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancelForAoe;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEndForBlink;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEndForAoe;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffectForBlink;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnSyncEventLaunchForBlink;
        private static DelegateBridge __Hotfix_OnSyncEventLaunchForAoe;
        private static DelegateBridge __Hotfix_OnSyncEventOnBlinkComplete;
        private static DelegateBridge __Hotfix_CreateFuncComp;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessFire;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessCancel;
        private static DelegateBridge __Hotfix_OnProcessEffect;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunchForBlink;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunchForBlink;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunchForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunchForAoe;
        private static DelegateBridge __Hotfix_add_EventOnStartChargeForBlink;
        private static DelegateBridge __Hotfix_remove_EventOnStartChargeForBlink;
        private static DelegateBridge __Hotfix_add_EventOnStartChargeForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnStartChargeForAoe;
        private static DelegateBridge __Hotfix_add_EventOnProcessFireForBlink;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFireForBlink;
        private static DelegateBridge __Hotfix_add_EventOnProcessFireForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFireForAoe;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancelForBlink;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancelForBlink;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancelForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancelForAoe;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEndForBlink;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEndForBlink;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEndForAoe;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEndForAoe;
        private static DelegateBridge __Hotfix_add_EventOnProcessEffectForBlink;
        private static DelegateBridge __Hotfix_remove_EventOnProcessEffectForBlink;

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunchForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunchForBlink
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancelForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancelForBlink
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEndForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEndForBlink
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffectForBlink
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, StructProcessSuperEquipAoeLaunch> EventOnProcessFireForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFireForBlink
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartChargeForAoe
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartChargeForBlink
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupBlinkAndAoeAttachBuf2TargetClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected override EquipFunctionCompBlinkBase CreateFuncComp()
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunchForAoe(LBSpaceProcessSuperEquipAoeLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunchForBlink(LBSpaceProcessEquipBlinkLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventOnBlinkComplete(bool blinkSuccessful)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

