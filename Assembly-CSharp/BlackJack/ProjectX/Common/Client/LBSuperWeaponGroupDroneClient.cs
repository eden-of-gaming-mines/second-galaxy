﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperWeaponGroupDroneClient : LBSuperWeaponGroupDroneBase, ILBSpaceProcessDroneLaunchSource, ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<List<LBSpaceProcessDroneFighterLaunch>> EventOnDroneFighterLaunch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperWeaponGroupDroneClient, LBSpaceProcessDroneFighterLaunch, ushort> EventOnUnitStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperWeaponGroupDroneClient, LBSpaceProcessDroneFighterLaunch, ushort> EventOnUnitFire;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneSelfExplode;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneStateChange;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneFlyReturnFinished;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneFighterLaunch, int> EventOnDroneFighterFire;
        public List<LBSpaceProcessDroneFighterLaunch> m_waitForLaunchProcessList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventFighterLaunch;
        private static DelegateBridge __Hotfix_OnProcessDroneSelfExplode;
        private static DelegateBridge __Hotfix_OnProcessDroneStateChange;
        private static DelegateBridge __Hotfix_OnProcessDroneReturn;
        private static DelegateBridge __Hotfix_OnProcessDroneFighterFire;
        private static DelegateBridge __Hotfix_OnProcessDroneHitByDefender;
        private static DelegateBridge __Hotfix_OnProcessUnitStartCharge;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_add_EventOnDroneFighterLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnDroneFighterLaunch;
        private static DelegateBridge __Hotfix_add_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnUnitFire;
        private static DelegateBridge __Hotfix_remove_EventOnUnitFire;
        private static DelegateBridge __Hotfix_add_EventOnDroneSelfExplode;
        private static DelegateBridge __Hotfix_remove_EventOnDroneSelfExplode;
        private static DelegateBridge __Hotfix_add_EventOnDroneStateChange;
        private static DelegateBridge __Hotfix_remove_EventOnDroneStateChange;
        private static DelegateBridge __Hotfix_add_EventOnDroneFlyReturnFinished;
        private static DelegateBridge __Hotfix_remove_EventOnDroneFlyReturnFinished;
        private static DelegateBridge __Hotfix_add_EventOnDroneFighterFire;
        private static DelegateBridge __Hotfix_remove_EventOnDroneFighterFire;

        public event Action<LBSpaceProcessDroneFighterLaunch, int> EventOnDroneFighterFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<LBSpaceProcessDroneFighterLaunch>> EventOnDroneFighterLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneFlyReturnFinished
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneSelfExplode
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneStateChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperWeaponGroupDroneClient, LBSpaceProcessDroneFighterLaunch, ushort> EventOnUnitFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperWeaponGroupDroneClient, LBSpaceProcessDroneFighterLaunch, ushort> EventOnUnitStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperWeaponGroupDroneClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneFighterFire(LBSpaceProcessDroneLaunchBase process, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnProcessDroneHitByDefender(LBSpaceProcessDroneLaunchBase process, LBSpaceProcessDroneDefenderLaunch defenderProcess, LBSpaceProcessDroneDefenderLaunch.AttackChance chance)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneReturn(LBSpaceProcessDroneLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneSelfExplode(LBSpaceProcessDroneLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneStateChange(LBSpaceProcessDroneLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitStartCharge(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventFighterLaunch(List<LBSpaceProcessDroneFighterLaunch> processList)
        {
        }
    }
}

