﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IEquipFunctionCompInvisibleOwnerClient : IEquipFunctionCompInvisibleOwnerBase, ILBSpaceProcessEquipInvisibleLaunchSource, IEquipFunctionCompOwner, ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
    }
}

