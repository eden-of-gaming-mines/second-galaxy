﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompMemberListClient : GuildCompMemberListBase, IGuildCompMemberListClient, IGuildCompMemberListBase, IGuildMemberListClient, IGuildMemberListBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnGuildMemberListInfoAck;
        private static DelegateBridge __Hotfix_OnSelfLeaveGuild;
        private static DelegateBridge __Hotfix_OnRemoveMember;
        private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateAck;
        private static DelegateBridge __Hotfix_GetMemberInfo;
        private static DelegateBridge __Hotfix_OnGuildLeaderTransferAbortAck;
        private static DelegateBridge __Hotfix_CreateMemberCtx;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompMemberListClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        protected override GuildMemberBase CreateMemberCtx(GuildMemberInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberInfo GetMemberInfo(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildLeaderTransferAbortAck()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberJobUpdateAck(GuildJobType job, string playerGameUserId, bool isAppoint, DateTime serverTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberListInfoAck(GuildMemberListInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRemoveMember(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSelfLeaveGuild(string gameUserId)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

