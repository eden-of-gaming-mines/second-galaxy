﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBBufDOTShieldClient : LBBufDotBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsPercentDOT>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnPeriod;
        private static DelegateBridge __Hotfix_get_IsPercentDOT;
        private static DelegateBridge __Hotfix_set_IsPercentDOT;

        [MethodImpl(0x8000)]
        public LBBufDOTShieldClient(ConfigDataBufInfo confInfo, uint instanceId, bool isPercentDOT)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPeriod(uint tickTime)
        {
        }

        public bool IsPercentDOT
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

