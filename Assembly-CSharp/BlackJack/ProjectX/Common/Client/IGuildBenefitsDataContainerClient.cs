﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;
    using System.Collections.Generic;

    public interface IGuildBenefitsDataContainerClient
    {
        void RefreshBenefitsList(List<GuildBenefitsInfoBase> benefitsList, int listVersion);
    }
}

