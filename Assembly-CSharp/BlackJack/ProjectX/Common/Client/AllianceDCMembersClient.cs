﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AllianceDCMembersClient : IAllianceDCMembersClient, IAllianceDCMembers
    {
        private readonly AllianceMemberListDataSection m_ds;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RemoveMember;
        private static DelegateBridge __Hotfix_GetMemberList;

        [MethodImpl(0x8000)]
        public List<AllianceMemberInfo> GetMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(List<AllianceMemberInfo> memberList)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveMember(uint guildId, GuildAllianceLeaveReason reason)
        {
        }
    }
}

