﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildCompLostReportClient : GuildCompLostReportBase, IGuildCompLostReportClient, IGuildCompLostReportBase, IGuildLostReportClient, IGuildLostReportBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RefleshSovereignLostReportList;
        private static DelegateBridge __Hotfix_GetSovereignLostReportList;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompLostReportClient(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public List<SovereignLostReport> GetSovereignLostReportList()
        {
        }

        [MethodImpl(0x8000)]
        public void RefleshSovereignLostReportList(List<SovereignLostReport> sovereignLostReportList)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

