﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;

    public interface ILBStoreItemClient : ILBItem
    {
        ulong GetInstanceId();
        long GetItemCount();
        int GetStoreItemIndex();
        bool IsBind();
        bool IsFreezing();
    }
}

