﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBWeaponGroupDroneClient : LBWeaponGroupDroneBase, ILBSpaceProcessDroneLaunchSource, ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneFighterLaunch> EventOnDroneFighterLaunch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneDefenderLaunch> EventOnDroneDefenderLaunch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneSniperLaunch> EventOnDroneSniperLaunch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneLaunchBase, ushort> EventOnUnitStartCharge;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneLaunchBase, ushort> EventOnUnitFire;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneDestoryedByDefender;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcessDroneLaunchBase> EventOnDroneSelfExplode;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneLaunchBase> EventOnDroneStateChange;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneLaunchBase> EventOnDroneFlyReturnFinished;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneLaunchBase, int> EventOnDroneFighterFire;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcessDroneFighterLaunch, LBSpaceProcessDroneDefenderLaunch, LBSpaceProcessDroneDefenderLaunch.AttackChance> EventOnDroneHitByDefender;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetWeaponTransverseVelocity;
        private static DelegateBridge __Hotfix_OnSyncEventFighterLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventDefenderLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSniperLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventDroneDestroyByDefender;
        private static DelegateBridge __Hotfix_GetGroupCD;
        private static DelegateBridge __Hotfix_OnProcessDroneSelfExplode;
        private static DelegateBridge __Hotfix_OnProcessDroneStateChange;
        private static DelegateBridge __Hotfix_OnProcessDroneReturn;
        private static DelegateBridge __Hotfix_OnProcessDroneFighterFire;
        private static DelegateBridge __Hotfix_OnProcessDroneHitByDefender;
        private static DelegateBridge __Hotfix_OnProcessUnitStartCharge;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_add_EventOnDroneFighterLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnDroneFighterLaunch;
        private static DelegateBridge __Hotfix_add_EventOnDroneDefenderLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnDroneDefenderLaunch;
        private static DelegateBridge __Hotfix_add_EventOnDroneSniperLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnDroneSniperLaunch;
        private static DelegateBridge __Hotfix_add_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnUnitFire;
        private static DelegateBridge __Hotfix_remove_EventOnUnitFire;
        private static DelegateBridge __Hotfix_add_EventOnDroneDestoryedByDefender;
        private static DelegateBridge __Hotfix_remove_EventOnDroneDestoryedByDefender;
        private static DelegateBridge __Hotfix_add_EventOnDroneSelfExplode;
        private static DelegateBridge __Hotfix_remove_EventOnDroneSelfExplode;
        private static DelegateBridge __Hotfix_add_EventOnDroneStateChange;
        private static DelegateBridge __Hotfix_remove_EventOnDroneStateChange;
        private static DelegateBridge __Hotfix_add_EventOnDroneFlyReturnFinished;
        private static DelegateBridge __Hotfix_remove_EventOnDroneFlyReturnFinished;
        private static DelegateBridge __Hotfix_add_EventOnDroneFighterFire;
        private static DelegateBridge __Hotfix_remove_EventOnDroneFighterFire;
        private static DelegateBridge __Hotfix_add_EventOnDroneHitByDefender;
        private static DelegateBridge __Hotfix_remove_EventOnDroneHitByDefender;

        public event Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneDefenderLaunch> EventOnDroneDefenderLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneFighterLaunch> EventOnDroneDestoryedByDefender
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneLaunchBase, int> EventOnDroneFighterFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneFighterLaunch> EventOnDroneFighterLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneLaunchBase> EventOnDroneFlyReturnFinished
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneFighterLaunch, LBSpaceProcessDroneDefenderLaunch, LBSpaceProcessDroneDefenderLaunch.AttackChance> EventOnDroneHitByDefender
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneLaunchBase> EventOnDroneSelfExplode
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneSniperLaunch> EventOnDroneSniperLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneLaunchBase> EventOnDroneStateChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneLaunchBase, ushort> EventOnUnitFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupDroneClient, LBSpaceProcessDroneLaunchBase, ushort> EventOnUnitStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupDroneClient(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetWeaponTransverseVelocity()
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneFighterFire(LBSpaceProcessDroneLaunchBase process, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnProcessDroneHitByDefender(LBSpaceProcessDroneLaunchBase process, LBSpaceProcessDroneDefenderLaunch defenderProcess, LBSpaceProcessDroneDefenderLaunch.AttackChance chance)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneReturn(LBSpaceProcessDroneLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneSelfExplode(LBSpaceProcessDroneLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessDroneStateChange(LBSpaceProcessDroneLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitStartCharge(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventDefenderLaunch(LBSpaceProcessDroneDefenderLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventDroneDestroyByDefender(uint processInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventFighterLaunch(LBSpaceProcessDroneFighterLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventSniperLaunch(LBSpaceProcessDroneSniperLaunch process, uint groupCDEndTime)
        {
        }
    }
}

