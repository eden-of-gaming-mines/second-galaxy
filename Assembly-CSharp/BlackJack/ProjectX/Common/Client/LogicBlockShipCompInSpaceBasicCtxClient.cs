﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompInSpaceBasicCtxClient : LogicBlockShipCompInSpaceBasicCtxBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float> EventOnSetEnergy;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float> EventOnSetShipShield;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSyncEventOnHitInfo> EventOnSyncEventHit;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<uint, ulong, float, BufType> EventOnSyncEventOnDotDamage;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong, float, BufType> EventOnSyncEventOnHOTHeal;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<NpcShipAnimEffectType, uint, uint, int> EventOnLBSyncEventNpcShipAnimEffect;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnShieldBroken;
        protected LogicBlockShipCompBufContainerClient m_lbBufContainer;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnSyncEventOnHitList;
        private static DelegateBridge __Hotfix_OnSyncEventOnHit;
        private static DelegateBridge __Hotfix_OnSyncEventOnDotDamage;
        private static DelegateBridge __Hotfix_OnSyncEventOnHotHeal;
        private static DelegateBridge __Hotfix_OnSyncEventSheldBroken;
        private static DelegateBridge __Hotfix_OnSyncEventDead;
        private static DelegateBridge __Hotfix_OnSyncEventSetEnergy;
        private static DelegateBridge __Hotfix_OnSyncEventSetShipShield;
        private static DelegateBridge __Hotfix_OnLBSyncEventNpcShipAnimEffect;
        private static DelegateBridge __Hotfix_OnHitByBullet;
        private static DelegateBridge __Hotfix_OnHitByLaser;
        private static DelegateBridge __Hotfix_OnHitByDrone;
        private static DelegateBridge __Hotfix_OnHitByEquip;
        private static DelegateBridge __Hotfix_add_EventOnSetEnergy;
        private static DelegateBridge __Hotfix_remove_EventOnSetEnergy;
        private static DelegateBridge __Hotfix_add_EventOnSetShipShield;
        private static DelegateBridge __Hotfix_remove_EventOnSetShipShield;
        private static DelegateBridge __Hotfix_add_EventOnSyncEventHit;
        private static DelegateBridge __Hotfix_remove_EventOnSyncEventHit;
        private static DelegateBridge __Hotfix_add_EventOnSyncEventOnDotDamage;
        private static DelegateBridge __Hotfix_remove_EventOnSyncEventOnDotDamage;
        private static DelegateBridge __Hotfix_add_EventOnSyncEventOnHOTHeal;
        private static DelegateBridge __Hotfix_remove_EventOnSyncEventOnHOTHeal;
        private static DelegateBridge __Hotfix_add_EventOnLBSyncEventNpcShipAnimEffect;
        private static DelegateBridge __Hotfix_remove_EventOnLBSyncEventNpcShipAnimEffect;
        private static DelegateBridge __Hotfix_add_EventOnShieldBroken;
        private static DelegateBridge __Hotfix_remove_EventOnShieldBroken;

        public event Action<NpcShipAnimEffectType, uint, uint, int> EventOnLBSyncEventNpcShipAnimEffect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float> EventOnSetEnergy
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float> EventOnSetShipShield
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnShieldBroken
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSyncEventOnHitInfo> EventOnSyncEventHit
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<uint, ulong, float, BufType> EventOnSyncEventOnDotDamage
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong, float, BufType> EventOnSyncEventOnHOTHeal
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByBullet(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess bulletFlyProcess)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByDrone(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByEquip(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitByLaser(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isCritical, float damageMulti, LBBulletDamageInfo damageInfo, LBSpaceProcess singleLaserFireProcess)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLBSyncEventNpcShipAnimEffect(NpcShipAnimEffectType effect, uint startTime, uint endTime, int openParam)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventDead()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventOnDotDamage(ulong bufInstanceId, float damage, BufType bufType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSyncEventOnHit(LBSyncEventOnHitInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventOnHitList(List<LBSyncEventOnHitInfo> onHitList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventOnHotHeal(ulong bufInstanceId, float healValue, BufType bufType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventSetEnergy(float energyValue)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventSetShipShield(float shipShield)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventSheldBroken()
        {
        }
    }
}

