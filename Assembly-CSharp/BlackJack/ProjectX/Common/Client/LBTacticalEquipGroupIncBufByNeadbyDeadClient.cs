﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBTacticalEquipGroupIncBufByNeadbyDeadClient : LBTacticalEquipGroupIncBuffClientBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventOnIncByTargetDead;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsTargetValid;
        private static DelegateBridge __Hotfix_OnTargetDead;
        private static DelegateBridge __Hotfix_add_EventOnIncByTargetDead;
        private static DelegateBridge __Hotfix_remove_EventOnIncByTargetDead;

        public event Action<ILBSpaceTarget> EventOnIncByTargetDead
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupIncBufByNeadbyDeadClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetDead(ILBSpaceTarget target, IFFState iffState)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

