﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBStaticPlayerShipClient : LBStaticShipClientBase, ILBStaticPlayerShip, ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        protected LogicBlockShipCompItemStoreClient m_lbCompItemStore;
        protected LogicBlockShipCompWeaponEquipSetupClient4PlayerShip m_lbCompWeaponEquipSetup;
        protected LogicBlockShipCompWeaponEquipSetupEnv4PlayerShip m_lbCompWeaponEquipSetupEnv;
        protected LogicBlockShipCompWeaponAmmoSetup4PlayerShip m_lbCompWeaponAmmoSetup;
        protected LogicBlockShipCompStaticResource m_lbCompResouce;
        protected IStaticPlayerShipDataContainer m_shipDC;
        protected ILBPlayerContext m_playerContext;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetBufFactory;
        private static DelegateBridge __Hotfix_GetLBStaticBasicCtx;
        private static DelegateBridge __Hotfix_GetLBResouce;
        private static DelegateBridge __Hotfix_CreateFakeStaticShip;
        private static DelegateBridge __Hotfix_GetUnpackTime;
        private static DelegateBridge __Hotfix_GetDefaultAroundRadius;
        private static DelegateBridge __Hotfix_GetLBShipItemStore;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetup;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetupEnv;
        private static DelegateBridge __Hotfix_GetLBWeaponAmmoSetup;
        private static DelegateBridge __Hotfix_GetLBPlayerContext;

        [MethodImpl(0x8000)]
        public LBStaticPlayerShipClient(IStaticPlayerShipDataContainer playerShipDC, ILBPlayerContext playerCtx, int hangarIndex, ILBShipCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public ILBFakeStaticShipClient CreateFakeStaticShip()
        {
        }

        [MethodImpl(0x8000)]
        public override ILBBuffFactory GetBufFactory()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetDefaultAroundRadius(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public ILBPlayerContext GetLBPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        public override LogicBlockShipCompStaticResource GetLBResouce()
        {
        }

        [MethodImpl(0x8000)]
        public override LogicBlockShipCompItemStoreBase GetLBShipItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtxClient GetLBStaticBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponAmmoSetup4PlayerShip GetLBWeaponAmmoSetup()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupBase GetLBWeaponEquipSetup()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupEnv4PlayerShip GetLBWeaponEquipSetupEnv()
        {
        }

        [MethodImpl(0x8000)]
        public override DateTime GetUnpackTime()
        {
        }
    }
}

