﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBStoredMailClient : LBStoredMail
    {
        private ILBPlayerContext m_playerContext;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetMailInfo;
        private static DelegateBridge __Hotfix_IsReaded;
        private static DelegateBridge __Hotfix_GetMailSendTime;

        [MethodImpl(0x8000)]
        public LBStoredMailClient(StoredMailInfo info, ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public MailInfo GetMailInfo()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetMailSendTime()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsReaded()
        {
        }
    }
}

