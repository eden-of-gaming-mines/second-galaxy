﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AllianceCompBasicClient : AllianceCompBasicBase, IAllianceCompBasicClient, IAllianceCompBasicBase, IAllianceBasicClient, IAllianceBasicBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_AllianceNameSet;
        private static DelegateBridge __Hotfix_AllianceLogoSet;
        private static DelegateBridge __Hotfix_AllianceAnnouncementSet;
        private static DelegateBridge __Hotfix_AllianceRegionSet;

        [MethodImpl(0x8000)]
        public AllianceCompBasicClient(IAllianceCompOwnerClient owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceAnnouncementSet(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceLogoSet(AllianceLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceNameSet(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceRegionSet(int languageType)
        {
        }

        private IAllianceDCBasicClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

