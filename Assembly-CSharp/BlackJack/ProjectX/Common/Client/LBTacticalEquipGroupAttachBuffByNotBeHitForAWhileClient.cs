﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupAttachBuffByNotBeHitForAWhileClient : LBTacticalEquipGroupAttachBuffByNotBeHitForAWhileBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<float, float> EventOnResetEquipState;
        private uint m_notHitTriggerTime;
        private bool m_isFirstSyncNtf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnSyncEventTimeInfo;
        private static DelegateBridge __Hotfix_GetInitEffectTriggerTime;
        private static DelegateBridge __Hotfix_GetTakeEffectIntervalTime;
        private static DelegateBridge __Hotfix_OnHitByTarget;
        private static DelegateBridge __Hotfix_add_EventOnResetEquipState;
        private static DelegateBridge __Hotfix_remove_EventOnResetEquipState;

        public event Action<float, float> EventOnResetEquipState
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupAttachBuffByNotBeHitForAWhileClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetInitEffectTriggerTime()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTakeEffectIntervalTime()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHitByTarget(ILBSpaceTarget target, bool isCritical, LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventTimeInfo(uint endTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

