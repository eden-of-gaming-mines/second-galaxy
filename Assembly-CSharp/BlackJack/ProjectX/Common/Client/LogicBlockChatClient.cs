﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockChatClient : LogicBlockChatBase
    {
        public string m_wisperPlayerGameUserId;
        public string m_wisperPlayerName;
        private List<ChatInfo> m_wisperChatInfoList;
        private uint m_wisperChatInfoSeq;
        private uint m_lastWisperChatInfoSeq;
        private List<ChatInfo> m_localChatInfoList;
        private uint m_localChatInfoSeq;
        private uint m_lastLocalChatInfoSeq;
        private List<ChatInfo> m_teamChatInfoList;
        private uint m_teamChatInfoSeq;
        private uint m_lastTeamChatInfoSeq;
        private List<ChatInfo> m_solarSystemChatInfoList;
        private uint m_solarSystemEnglishChatInfoSeq;
        private uint m_lastSoalarSystemEnglishChatInfoSeq;
        private uint m_solarSystemChineseChatInfoSeq;
        private uint m_lastSolarSystemChineseChatInfoSeq;
        private List<ChatInfo> m_starFieldChatInfoList;
        private uint m_starFieldChatInfoSeq;
        private uint m_lastStarFieldChatInfoSeq;
        private List<ChatInfo> m_guildChatInfoList;
        private uint m_guildChatInfoSeq;
        private uint m_lastGuildChatInfoSeq;
        private List<ChatInfo> m_allianceChatInfoList;
        private uint m_allianceChatInfoSeq;
        private uint m_lastAllianceChatInfoSeq;
        private List<ChatInfo> m_sysChatInfoList;
        private uint m_sysChatInfoSeq;
        private uint m_lastSysChatInfoSeq;
        private ChatInfo m_lastMsg;
        private ChatChannel m_lastMsgChannel;
        private const int ChatMsgClearThresholdValue = 200;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnWisperChatNtf;
        private static DelegateBridge __Hotfix_OnLocalChatNtf;
        private static DelegateBridge __Hotfix_OnTeamChatNtf;
        private static DelegateBridge __Hotfix_OnSolarSystemChatNtf;
        private static DelegateBridge __Hotfix_OnStarFieldChatNtf;
        private static DelegateBridge __Hotfix_OnGuildChatLogAck;
        private static DelegateBridge __Hotfix_OnGuildChatNtf;
        private static DelegateBridge __Hotfix_OnAllianceChatNtf_0;
        private static DelegateBridge __Hotfix_OnAllianceChatLogAck;
        private static DelegateBridge __Hotfix_OnAllianceChatNtf_1;
        private static DelegateBridge __Hotfix_ClearChatCacheList;
        private static DelegateBridge __Hotfix_IsSystemChannelChatInfo;
        private static DelegateBridge __Hotfix_GetChatInfoSeq;
        private static DelegateBridge __Hotfix_OnStarFieldChatAck;
        private static DelegateBridge __Hotfix_OnSolarSystemChatAck;
        private static DelegateBridge __Hotfix_OnLocalChatAck;
        private static DelegateBridge __Hotfix_OnTeamChatAck;
        private static DelegateBridge __Hotfix_OnWisperChatAck;
        private static DelegateBridge __Hotfix_OnGuildChatAck;
        private static DelegateBridge __Hotfix_OnAllianceChatAck;
        private static DelegateBridge __Hotfix_AddSysMsgToPointChannel;
        private static DelegateBridge __Hotfix_GetChatInfoList;
        private static DelegateBridge __Hotfix_UpdateChatSeqValue;
        private static DelegateBridge __Hotfix_UpdateAllChatSeqValue;
        private static DelegateBridge __Hotfix_GetUnReadChatMsgCnt;
        private static DelegateBridge __Hotfix_IsCacheChange;
        private static DelegateBridge __Hotfix_AddMsgToSysInfoList;
        private static DelegateBridge __Hotfix_get_LastMsg;
        private static DelegateBridge __Hotfix_get_LastMsgChannel;

        [MethodImpl(0x8000)]
        private void AddMsgToSysInfoList(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSysMsgToPointChannel(ChatChannel channel, ChatInfo info, bool isChangeSeq)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearChatCacheList()
        {
        }

        [MethodImpl(0x8000)]
        public List<ChatInfo> GetChatInfoList(ChatChannel channelType)
        {
        }

        [MethodImpl(0x8000)]
        public int GetChatInfoSeq(ChatChannel channelType, ChatLanguageChannel languageChannelType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public int GetUnReadChatMsgCnt(ChatChannel chatChannel, string selfUserId, ChatLanguageChannel chatLanguageChannel = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCacheChange(ChatChannel chatChannel, ChatLanguageChannel chatLanguageChannel = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSystemChannelChatInfo(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceChatAck(ChatInAllianceAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceChatLogAck(List<ChatInfo> infoList, uint seq)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceChatNtf(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceChatNtf(ChatInfo info, uint seq)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChatAck(ChatInGuildAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChatLogAck(List<ChatInfo> infoList, uint seq)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChatNtf(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLocalChatAck(ChatInLocalAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLocalChatNtf(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSolarSystemChatAck(ChatInSolarSystemAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSolarSystemChatNtf(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStarFieldChatAck(ChatInStarFieldAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStarFieldChatNtf(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamChatAck(ChatInTeamAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamChatNtf(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnWisperChatAck(ChatInWisperAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnWisperChatNtf(ChatInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllChatSeqValue()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChatSeqValue(ChatChannel chatChannel, ChatLanguageChannel chatLanguageChannel = 0)
        {
        }

        public ChatInfo LastMsg
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ChatChannel LastMsgChannel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

