﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildLostReportClient : IGuildLostReportBase
    {
        List<SovereignLostReport> GetSovereignLostReportList();
        void RefleshSovereignLostReportList(List<SovereignLostReport> sovereignLostReportList);
    }
}

