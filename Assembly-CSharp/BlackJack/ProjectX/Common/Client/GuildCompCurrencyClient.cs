﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompCurrencyClient : GuildCompCurrencyBase, IGuildCompCurrencyClient, IGuildCompCurrencyBase, IGuildCurrencyClient, IGuildCurrencyBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateGuildCurrencyInfo;
        private static DelegateBridge __Hotfix_OnGuildWalletAnnouncementSetAck;
        private static DelegateBridge __Hotfix_GuildTradeMoneyModify;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompCurrencyClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradeMoneyModify(long changeValue, GuildCurrencyLogType logType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildWalletAnnouncementSetAck(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildCurrencyInfo(GuildCurrencyInfo currency)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

