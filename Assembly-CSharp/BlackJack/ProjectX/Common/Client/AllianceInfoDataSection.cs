﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AllianceInfoDataSection
    {
        public AllianceBasicInfo m_info;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetName;
        private static DelegateBridge __Hotfix_SetRegion;
        private static DelegateBridge __Hotfix_SetAnnouncement;
        private static DelegateBridge __Hotfix_SetLogo;
        private static DelegateBridge __Hotfix_SetLeaderGuild;

        [MethodImpl(0x8000)]
        public void Init(AllianceBasicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncement(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLeaderGuild(uint leaderGuildId, string guildCode, string guildName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLogo(AllianceLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRegion(GuildAllianceLanguageType languageType)
        {
        }
    }
}

