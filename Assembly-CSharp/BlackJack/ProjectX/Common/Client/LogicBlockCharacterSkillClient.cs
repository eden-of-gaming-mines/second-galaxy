﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockCharacterSkillClient : LogicBlockCharacterSkillBase
    {
        private Dictionary<int, List<LBPassiveSkill>> m_category2LearntSkillDict;
        private Dictionary<int, List<LBPassiveSkill>> m_skillType2LearntSkillDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_LearnSkill;
        private static DelegateBridge __Hotfix_GetLearntPassiveSkillsByCategory;
        private static DelegateBridge __Hotfix_GetLearntPassiveSkillsBySkillType;
        private static DelegateBridge __Hotfix_GetAllPassiveSkillBySkillType;
        private static DelegateBridge __Hotfix_OnAddNewSkillToPassiveSkillList;
        private static DelegateBridge __Hotfix_OnRemoveSkillFromPassiveSkillList;

        [MethodImpl(0x8000)]
        public List<ConfigDataPassiveSkillInfo> GetAllPassiveSkillBySkillType(int skillType)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPassiveSkill> GetLearntPassiveSkillsByCategory(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPassiveSkill> GetLearntPassiveSkillsBySkillType(int skillType)
        {
        }

        [MethodImpl(0x8000)]
        public void LearnSkill(int skillId, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnAddNewSkillToPassiveSkillList(LBPassiveSkill newSkill)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnRemoveSkillFromPassiveSkillList(LBPassiveSkill skill)
        {
        }
    }
}

