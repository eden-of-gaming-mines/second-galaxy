﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface ILBPlayerContextClient : ILBPlayerContext, ILBPlayerContextLBProviderClient, ILBPlayerContextDCProviderClient, ILBPlayerContextDCProvider, ILBPlayerContextLBProvider
    {
    }
}

