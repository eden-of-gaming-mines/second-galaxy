﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockFlagShipCompTeleportClient : LogicBlockFlagShipCompTeleportBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnFlagShipTeleportStartCharging;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnFlagShipTeleportChargingFail;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnFlagShipTeleportChargingFinish;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnFlagShipTeleportProcessingFail;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventFlagShipTeleportStartCharging;
        private static DelegateBridge __Hotfix_OnSyncEventFlagShipTeleportChargingFail;
        private static DelegateBridge __Hotfix_OnSyncEventFlagShipTeleportChargingFinish;
        private static DelegateBridge __Hotfix_OnSyncEventFlagShipTeleportProcessingFail;
        private static DelegateBridge __Hotfix_add_EventOnFlagShipTeleportStartCharging;
        private static DelegateBridge __Hotfix_remove_EventOnFlagShipTeleportStartCharging;
        private static DelegateBridge __Hotfix_add_EventOnFlagShipTeleportChargingFail;
        private static DelegateBridge __Hotfix_remove_EventOnFlagShipTeleportChargingFail;
        private static DelegateBridge __Hotfix_add_EventOnFlagShipTeleportChargingFinish;
        private static DelegateBridge __Hotfix_remove_EventOnFlagShipTeleportChargingFinish;
        private static DelegateBridge __Hotfix_add_EventOnFlagShipTeleportProcessingFail;
        private static DelegateBridge __Hotfix_remove_EventOnFlagShipTeleportProcessingFail;

        public event Action EventOnFlagShipTeleportChargingFail
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFlagShipTeleportChargingFinish
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFlagShipTeleportProcessingFail
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFlagShipTeleportStartCharging
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventFlagShipTeleportChargingFail()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventFlagShipTeleportChargingFinish()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventFlagShipTeleportProcessingFail()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventFlagShipTeleportStartCharging()
        {
        }
    }
}

