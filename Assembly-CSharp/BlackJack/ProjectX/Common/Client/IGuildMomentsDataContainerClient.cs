﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;
    using System.Collections.Generic;

    public interface IGuildMomentsDataContainerClient
    {
        void UpdateGuildMomentsBriefInfoList(List<GuildMomentsInfo> momentsBriefInfoList, uint momentsBriefInfoVersion);
        void UpdateGuildMomentsInfoList(List<GuildMomentsInfo> momentsInfoList, bool isOlder);
    }
}

