﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockNpcShopClient : LogicBlockNpcShopBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_NpcShopBuyItem;
        private static DelegateBridge __Hotfix_ClearSaleCountLimitRecord;
        private static DelegateBridge __Hotfix_GetCurrSaleItemList;
        private static DelegateBridge __Hotfix_GetAllCurrSaleItemList;
        private static DelegateBridge __Hotfix_GetItemSaleCount;
        private static DelegateBridge __Hotfix_GetItemBuyCountMax;
        private static DelegateBridge __Hotfix_GetCurrNpcShopCurrencyType;
        private static DelegateBridge __Hotfix_GetCurrNpcShopFactionId;

        [MethodImpl(0x8000)]
        public void ClearSaleCountLimitRecord()
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<int, long>> GetAllCurrSaleItemList()
        {
        }

        [MethodImpl(0x8000)]
        public CurrencyType GetCurrNpcShopCurrencyType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrNpcShopFactionId()
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<int, long>> GetCurrSaleItemList(int shopItemType)
        {
        }

        [MethodImpl(0x8000)]
        public long GetItemBuyCountMax(ConfigDataNpcShopItemInfo shopItemInfo, ulong currCostMoney, ulong currCostSpace, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public long GetItemSaleCount(ConfigDataNpcShopItemInfo itemConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool NpcShopBuyItem(List<KeyValuePair<int, long>> shopItemList, out int errCode)
        {
        }
    }
}

