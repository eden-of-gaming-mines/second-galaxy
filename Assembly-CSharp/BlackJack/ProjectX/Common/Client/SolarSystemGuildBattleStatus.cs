﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;

    public enum SolarSystemGuildBattleStatus
    {
        DisablePlayerSovereignty,
        EnablePlayerSovereignty,
        InBattleStatus,
        WaitForBattle
    }
}

