﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildFlagShipHangarDataContainerClient : IGuildFlagShipHangarDataContainer
    {
        void SetBasicInfoVersion(int version);
        void SetShipListVersion(int version);
        void UpdateGuildFlagShipHangarBasicInfo(GuildFlagShipHangarBasicInfo basicInfo);
        void UpdateGuildFlagShipHangarShipList(List<GuildStaticFlagShipInstanceInfo> shipListInfo);
    }
}

