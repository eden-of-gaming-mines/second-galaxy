﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;

    public interface ILBItem
    {
        T GetConfigInfo<T>() where T: class;
        StoreItemType GetItemType();
    }
}

