﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperWeaponGroupLaserClient : LBSuperWeaponGroupLaserBase, ILBSpaceProcessLaserLaunchSource, ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserLaunch> EventOnLaunchCancel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserSingleFire> EventOnSingleFireCancel;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserLaunch, ushort> EventOnUnitStartCharge;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserLaunch, LBSpaceProcessSuperLaserSingleFire, ushort> EventOnUnitFire;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_CreateLaserSingleFireProcess;
        private static DelegateBridge __Hotfix_OnProcessUnitFireOnHit;
        private static DelegateBridge __Hotfix_OnProcessUnitStartCharge;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnSingleFireCancel;
        private static DelegateBridge __Hotfix_remove_EventOnSingleFireCancel;
        private static DelegateBridge __Hotfix_add_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnUnitFire;
        private static DelegateBridge __Hotfix_remove_EventOnUnitFire;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunch;

        public event Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserLaunch> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserSingleFire> EventOnSingleFireCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserLaunch, LBSpaceProcessSuperLaserSingleFire, ushort> EventOnUnitFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperWeaponGroupLaserClient, LBSpaceProcessSuperLaserLaunch, ushort> EventOnUnitStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperWeaponGroupLaserClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBSpaceProcessSuperLaserSingleFire CreateLaserSingleFireProcess(LBSpaceProcessSuperLaserLaunch bulletGunLaunchProcess, LBProcessSingleUnitLaunchInfo bullet)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitFireOnHit(LBSpaceProcessLaserSingleFire process, uint processTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitStartCharge(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessSuperLaserLaunch process)
        {
        }
    }
}

