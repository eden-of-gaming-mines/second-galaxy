﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompPickDropBoxClient : LogicBlockShipCompPickDropBoxBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceDropBox, float> EventOnPickDropBoxStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceDropBox, List<ShipStoreItemInfo>> EventOnPickDropBoxEnd;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventPickDropBoxStart;
        private static DelegateBridge __Hotfix_OnSyncEventPickDropBoxEnd;
        private static DelegateBridge __Hotfix_GetLBSpaceDropBoxByObjId;
        private static DelegateBridge __Hotfix_add_EventOnPickDropBoxStart;
        private static DelegateBridge __Hotfix_remove_EventOnPickDropBoxStart;
        private static DelegateBridge __Hotfix_add_EventOnPickDropBoxEnd;
        private static DelegateBridge __Hotfix_remove_EventOnPickDropBoxEnd;

        public event Action<ILBSpaceDropBox, List<ShipStoreItemInfo>> EventOnPickDropBoxEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceDropBox, float> EventOnPickDropBoxStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public ILBSpaceDropBox GetLBSpaceDropBoxByObjId(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnSyncEventPickDropBoxEnd(List<ShipStoreItemInfo> items)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventPickDropBoxStart(ILBSpaceDropBox dropBox, uint time)
        {
        }
    }
}

