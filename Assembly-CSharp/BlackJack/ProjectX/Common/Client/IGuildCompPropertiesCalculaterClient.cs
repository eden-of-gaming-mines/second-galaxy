﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildCompPropertiesCalculaterClient : IGuildCompPropertiesCalculaterBase, IGuildPropertiesCalculaterClient, IGuildPropertiesCalculaterBase
    {
        void UpdateSolarSystemPropertiesCalc(int solarSystemId, GuildSolarSystemInfo info);
    }
}

