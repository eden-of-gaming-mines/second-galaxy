﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupRecoverySuperEnergyByKillingHitClient : LBTacticalEquipGroupRecoverySuperEnergyByKillingHitBase
    {
        public Action EventOnSuperEnergyModify;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEnergyModify;
        private static DelegateBridge __Hotfix_OnKillingHitToTarget;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupRecoverySuperEnergyByKillingHitClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnKillingHitToTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventSuperEnergyModify()
        {
        }
    }
}

