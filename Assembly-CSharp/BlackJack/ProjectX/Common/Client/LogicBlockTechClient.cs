﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockTechClient : LogicBlockTechBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_TechUpgradeStart;
        private static DelegateBridge __Hotfix_TechUpgradeByRealMoney;
        private static DelegateBridge __Hotfix_TechUpgradeSpeedUp;
        private static DelegateBridge __Hotfix_TechUpgradeSpeedUpByRealMoney;
        private static DelegateBridge __Hotfix_TechUpgradeCancel;
        private static DelegateBridge __Hotfix_TechUpgradeNtf;

        [MethodImpl(0x8000)]
        public bool TechUpgradeByRealMoney(int techId, int level, int realMoneyCost)
        {
        }

        [MethodImpl(0x8000)]
        public bool TechUpgradeCancel(int techId)
        {
        }

        [MethodImpl(0x8000)]
        public bool TechUpgradeNtf(int techId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public bool TechUpgradeSpeedUp(int itemId, int count, TechUpgradeInfo upgradeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool TechUpgradeSpeedUpByRealMoney(int techId, int realMoneyCost)
        {
        }

        [MethodImpl(0x8000)]
        public bool TechUpgradeStart(TechUpgradeInfo upgradeInfo)
        {
        }
    }
}

