﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using System;

    public interface IGuildSolarSystemCtxClient : IGuildSolarSystemCtxBase
    {
        void OnGuildBuildingDeployUpgradeAck(GuildBuildingDeployUpgradeAck ack);
        void OnGuildBuildingRecycleAck(GuildBuildingRecycleAck ack);
        void OnGuildOccupiedSolarSystemInfoAck(GuildOccupiedSolarSystemInfoAck ack);
        void OnGuildSolarSystemInfoAck(GuildSolarSystemInfo info);
    }
}

