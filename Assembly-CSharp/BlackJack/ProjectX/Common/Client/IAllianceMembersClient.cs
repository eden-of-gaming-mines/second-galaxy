﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IAllianceMembersClient : IAllianceMembersBase
    {
        void AllianceLeaderTransfer(uint srcGuildId, uint destGuildId);
        void AllianceMemberRemove(uint removedGuildId);
    }
}

