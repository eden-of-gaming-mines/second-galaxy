﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AllianceInviteListDataSection
    {
        private readonly List<AllianceInviteInfo> m_inviteList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_AddInvite;
        private static DelegateBridge __Hotfix_RemoveInvite;
        private static DelegateBridge __Hotfix_GetInviteList;
        private static DelegateBridge __Hotfix_GetInvite;

        [MethodImpl(0x8000)]
        public void AddInvite(AllianceInviteInfo invite)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceInviteInfo GetInvite(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceInviteInfo> GetInviteList()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(List<AllianceInviteInfo> inviteList)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveInvite(ulong inviteInstanceId)
        {
        }
    }
}

