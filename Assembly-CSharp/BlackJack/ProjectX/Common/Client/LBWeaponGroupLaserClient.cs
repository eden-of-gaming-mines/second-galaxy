﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBWeaponGroupLaserClient : LBWeaponGroupLaserBase, ILBSpaceProcessLaserLaunchSource, ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserLaunch> EventOnLaunchCancel;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserSingleFire> EventOnSingleFireCancel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserLaunch, ushort> EventOnUnitStartCharge;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserLaunch, LBSpaceProcessLaserSingleFire, ushort> EventOnUnitFire;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBWeaponGroupBase> EventOnReloadAmmoStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupBase, ItemInfo> EventOnReloadAmmoEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventReloadAmmoStart;
        private static DelegateBridge __Hotfix_OnSyncEventReloadAmmoEnd;
        private static DelegateBridge __Hotfix_GetWeaponTransverseVelocity;
        private static DelegateBridge __Hotfix_TryStartReloadAmmo;
        private static DelegateBridge __Hotfix_OnReloadAmmoCDEnd;
        private static DelegateBridge __Hotfix_CreateProcess;
        private static DelegateBridge __Hotfix_GetGroupCD;
        private static DelegateBridge __Hotfix_OnProcessUnitFireOnHit;
        private static DelegateBridge __Hotfix_OnProcessUnitStartCharge;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnSingleFireCancel;
        private static DelegateBridge __Hotfix_remove_EventOnSingleFireCancel;
        private static DelegateBridge __Hotfix_add_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnUnitFire;
        private static DelegateBridge __Hotfix_remove_EventOnUnitFire;
        private static DelegateBridge __Hotfix_add_EventOnReloadAmmoStart;
        private static DelegateBridge __Hotfix_remove_EventOnReloadAmmoStart;
        private static DelegateBridge __Hotfix_add_EventOnReloadAmmoEnd;
        private static DelegateBridge __Hotfix_remove_EventOnReloadAmmoEnd;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunch;

        public event Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserLaunch> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupBase, ItemInfo> EventOnReloadAmmoEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupBase> EventOnReloadAmmoStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserSingleFire> EventOnSingleFireCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserLaunch, LBSpaceProcessLaserSingleFire, ushort> EventOnUnitFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBWeaponGroupLaserClient, LBSpaceProcessLaserLaunch, ushort> EventOnUnitStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupLaserClient(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBSpaceProcessLaserSingleFire CreateProcess(ushort unitIndex, LBSpaceProcessLaserLaunch laserLaunchProcess, uint unitLaunchTime)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetWeaponTransverseVelocity()
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitFireOnHit(LBSpaceProcessLaserSingleFire process, uint processTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitStartCharge(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnReloadAmmoCDEnd()
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessLaserLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventReloadAmmoEnd(ItemInfo reloadAmmoItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventReloadAmmoStart(uint startTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool TryStartReloadAmmo(uint startTime)
        {
        }
    }
}

