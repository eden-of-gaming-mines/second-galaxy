﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildBenefitsClient : IGuildBenefitsBase
    {
        void BenefitsReceive(ulong benefitsInstanceId, string gameUserId);
        void GetBenefitsList(out List<GuildCommonBenefitsInfo> commonBenefitsList, out GuildSovereigntyBenefitsInfo sovereigntyBenefits, out List<GuildInformationPointBenefitsInfo> informationPointBenefitsList, DateTime selfJoinTime, out int benefitsListVersion);
        int GetFinalBenefitsListVersion(string gameUserId);
        bool HasBenefitsAvailable(DateTime currServerTime, DateTime selfJoinTime);
        void RefreshBenefitsList(List<GuildBenefitsInfoBase> benefitsList, int listVersion);
    }
}

