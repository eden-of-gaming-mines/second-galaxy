﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AllianceDCBasicClient : IAllianceDCBasicClient, IAllianceDCBasic
    {
        private readonly AllianceInfoDataSection m_ds;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetAllianceName;
        private static DelegateBridge __Hotfix_SetAllianceAnnouncement;
        private static DelegateBridge __Hotfix_SetAllianceLogo;
        private static DelegateBridge __Hotfix_SetAllianceLanguageType;
        private static DelegateBridge __Hotfix_SetAllianceLeader;
        private static DelegateBridge __Hotfix_GetAllianceBasicInfo;

        [MethodImpl(0x8000)]
        public AllianceBasicInfo GetAllianceBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(AllianceBasicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllianceAnnouncement(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllianceLanguageType(GuildAllianceLanguageType languageType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllianceLeader(uint leaderGuildId, string guildCode, string guildName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllianceLogo(AllianceLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllianceName(string name)
        {
        }
    }
}

