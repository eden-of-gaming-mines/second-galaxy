﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompInteractionClient : LogicBlockShipCompInteractionBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SceneInteractionType, uint, uint, int, SceneInteractionFlag, int, uint> EventOnSetInteraction;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnClearInteraction;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetInteraction;
        private static DelegateBridge __Hotfix_ClearInteraction;
        private static DelegateBridge __Hotfix_GetSceneInteractionMsg;
        private static DelegateBridge __Hotfix_add_EventOnSetInteraction;
        private static DelegateBridge __Hotfix_remove_EventOnSetInteraction;
        private static DelegateBridge __Hotfix_add_EventOnClearInteraction;
        private static DelegateBridge __Hotfix_remove_EventOnClearInteraction;

        public event Action EventOnClearInteraction
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SceneInteractionType, uint, uint, int, SceneInteractionFlag, int, uint> EventOnSetInteraction
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void ClearInteraction()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSceneInteractionMsg()
        {
        }

        [MethodImpl(0x8000)]
        public override void SetInteraction(SceneInteractionType type, uint optTime, uint range, int message, SceneInteractionFlag flag, int interactionTemplateId = 0, uint singlePlayerInteractionCountMax = 0)
        {
        }
    }
}

