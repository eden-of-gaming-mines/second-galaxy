﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompProductionClient : GuildCompProductionBase, IGuildCompProductionClient, IGuildCompProductionBase, IGuildProductionClient, IGuildProductionBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RefreshProductionLineList;
        private static DelegateBridge __Hotfix_SetGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompProductionClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshProductionLineList(List<GuildProductionLineInfo> lineList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildProductionDataVersion(uint version)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

