﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBStaticFlagShipClient : LBStaticShipClientBase, ILBStaticFlagShip, ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        protected LogicBlockShipCompWeaponEquipSetupClient4FlagShip m_lbCompWeaponEquipSetup;
        protected LogicBlockShipCompWeaponEquipSetupEnv4FlagShip m_lbCompWeaponEquipSetupEnv;
        protected LogicBlockShipCompStaticResource m_lbCompResouce;
        protected LogicBlockFlagShipCompStaticBasicInfo m_lbCompFlagShipBasicInfo;
        protected IStaticFlagShipDataContainer m_shipDC;
        protected IGuildItemStoreItemOperationClient m_guildItemStore;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetBufFactory;
        private static DelegateBridge __Hotfix_GetLBStaticBasicCtx;
        private static DelegateBridge __Hotfix_GetLBResouce;
        private static DelegateBridge __Hotfix_GetUnpackTime;
        private static DelegateBridge __Hotfix_GetDefaultAroundRadius;
        private static DelegateBridge __Hotfix_GetLBFlagShipBasicInfo;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetup;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetupEnv;
        private static DelegateBridge __Hotfix_GetGuildItemStore;
        private static DelegateBridge __Hotfix_GetLBPlayerContext;

        [MethodImpl(0x8000)]
        public LBStaticFlagShipClient(IStaticFlagShipDataContainer flagShipDC, IGuildItemStoreItemOperationClient guildItemStore, int hangarIndex, ILBShipCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public override ILBBuffFactory GetBufFactory()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetDefaultAroundRadius(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildItemStoreItemOperationBase GetGuildItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockFlagShipCompStaticBasicInfo GetLBFlagShipBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ILBPlayerContext GetLBPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        public override LogicBlockShipCompStaticResource GetLBResouce()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtxClient GetLBStaticBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupBase GetLBWeaponEquipSetup()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompWeaponEquipSetupEnv4FlagShip GetLBWeaponEquipSetupEnv()
        {
        }

        [MethodImpl(0x8000)]
        public override DateTime GetUnpackTime()
        {
        }
    }
}

