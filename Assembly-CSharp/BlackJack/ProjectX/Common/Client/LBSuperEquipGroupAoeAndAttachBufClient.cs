﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperEquipGroupAoeAndAttachBufClient : LBSuperEquipGroupAoeAndAttachBufBase, ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupAoeAndAttachBufClient, LBSpaceProcessSuperEquipAoeLaunch> EventOnSuperEquipLaunchCancel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupAoeAndAttachBufClient, LBSpaceProcessSuperEquipAoeLaunch> EventOnSuperEquipStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnSuperEquipLaunch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupAoeAndAttachBufClient, StructProcessSuperEquipAoeLaunch> EventOnSuperEquipFire;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_OnProcessStartCharge;
        private static DelegateBridge __Hotfix_OnProcessFire;
        private static DelegateBridge __Hotfix_OnProcessEffect;
        private static DelegateBridge __Hotfix_add_EventOnSuperEquipLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnSuperEquipLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnSuperEquipStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnSuperEquipStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnSuperEquipLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnSuperEquipLaunch;
        private static DelegateBridge __Hotfix_add_EventOnSuperEquipFire;
        private static DelegateBridge __Hotfix_remove_EventOnSuperEquipFire;

        public event Action<LBSuperEquipGroupAoeAndAttachBufClient, StructProcessSuperEquipAoeLaunch> EventOnSuperEquipFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnSuperEquipLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupAoeAndAttachBufClient, LBSpaceProcessSuperEquipAoeLaunch> EventOnSuperEquipLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupAoeAndAttachBufClient, LBSpaceProcessSuperEquipAoeLaunch> EventOnSuperEquipStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupAoeAndAttachBufClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessSuperEquipAoeLaunch process)
        {
        }
    }
}

