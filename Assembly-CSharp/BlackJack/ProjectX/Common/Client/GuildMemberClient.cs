﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildMemberClient : GuildMemberBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateBasicInfo;
        private static DelegateBridge __Hotfix_UpdateDynamicInfo;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildMemberClient(IGuildDataContainerBase dc, GuildMemberInfo readOnlyInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBasicInfo(GuildMemberBasicInfo basicInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDynamicInfo(GuildMemberDynamicInfo dynamicInfo)
        {
        }

        private IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

