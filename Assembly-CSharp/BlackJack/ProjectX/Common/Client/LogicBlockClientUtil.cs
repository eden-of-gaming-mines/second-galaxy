﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockClientUtil
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Convert2LBProcessSingleUnitLaunchInfo;
        private static DelegateBridge __Hotfix_Convert2LBProcessSingleMissileUnitLaunchInfo;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessBulletGunLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperRailgunLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperPlasmaLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessMissileLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperMissileLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessLaserLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperLaserLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessDroneFighterLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperDroneFighterLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperEquipAoeLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessDroneDefenderLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessDroneSniperLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessEquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperEquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_Conver2LBSynEventSuperEquipAntiJumpingForceSieldLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessEquipTransformToTeleportTunnelLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperEquipAddEnergyAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperEquipShipShield2ExtraShieldAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessEquipChannelLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessEquipPeriodicDamageLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessSuperEquipAddShieldAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessEquipBlinkLaunch;
        private static DelegateBridge __Hotfix_Convert2LBSpaceProcessEquipInvisibleLaunch;
        private static DelegateBridge __Hotfix_GetLBSyncEventOnHitInfoList;
        private static DelegateBridge __Hotfix_GetLBSyncEventList;

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipAntiJumpingForceShieldAndAttachBufLaunch Conver2LBSynEventSuperEquipAntiJumpingForceSieldLaunch(ProSpaceProcessEquipAntiJumpingForceShieldLaunch proAntiJumpingForceShieldLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        public static LBProcessSingleMissileUnitLaunchInfo Convert2LBProcessSingleMissileUnitLaunchInfo(ProLBProcessSingleSuperMissileUnitLaunchInfo proUnitLaunch, uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public static LBProcessSingleUnitLaunchInfo Convert2LBProcessSingleUnitLaunchInfo(ProLBProcessSingleUnitLaunchInfo proUnitLaunch, uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSpaceProcessBulletGunLaunch Convert2LBSpaceProcessBulletGunLaunch(ProSpaceProcessBulletGunLaunch proRailgunLaunch, LBWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected static LBSpaceProcessDroneDefenderLaunch Convert2LBSpaceProcessDroneDefenderLaunch(ProLBSpaceProcessDroneLaunchInfo proDroneLaunch, LBWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, double attackRange)
        {
        }

        [MethodImpl(0x8000)]
        protected static LBSpaceProcessDroneFighterLaunch Convert2LBSpaceProcessDroneFighterLaunch(ProLBSpaceProcessDroneLaunchInfo proDroneLaunch, LBWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected static LBSpaceProcessDroneSniperLaunch Convert2LBSpaceProcessDroneSniperLaunch(ProLBSpaceProcessDroneLaunchInfo proDroneLaunch, LBWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipAttachBufLaunch Convert2LBSpaceProcessEquipAttachBufLaunch(ProSpaceProcessEquipAttachBufLaunch proEquipAttachBufLaunch, ShipEquipSlotType slotType, int slotIndex, EquipType equipType, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipBlinkLaunch Convert2LBSpaceProcessEquipBlinkLaunch(ProSpaceProcessEquipBlinkLaunch proEquipBlinkLaunch, ShipEquipSlotType slotType, int slotIndex, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipChannelLaunch Convert2LBSpaceProcessEquipChannelLaunch(ProLBSpaceProcessEquipChannelLaunch proLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipInvisibleLaunch Convert2LBSpaceProcessEquipInvisibleLaunch(ProSpaceProcessEquipInvisibleLaunch proEquipInvisibleLaunch, ShipEquipSlotType slotType, int slotIndex, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipPeriodicDamageLaunch Convert2LBSpaceProcessEquipPeriodicDamageLaunch(ProLBSpaceProcessEquipPeriodicDamageLaunch proLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipTransformToTeleportTunnelLaunch Convert2LBSpaceProcessEquipTransformToTeleportTunnelLaunch(ProSpaceProcessSuperEquipTransformToTeleportTunnelLaunch proTranformToTeleportTunnelLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSpaceProcessLaserLaunch Convert2LBSpaceProcessLaserLaunch(ProSpaceProcessLaserLaunch proLaserLaunch, LBWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSpaceProcessBulletGunLaunch Convert2LBSpaceProcessMissileLaunch(ProSpaceProcessBulletGunLaunch proRailgunLaunch, LBWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected static LBSpaceProcessDroneFighterLaunch Convert2LBSpaceProcessSuperDroneFighterLaunch(ProLBSpaceProcessDroneLaunchInfo proDroneLaunch, LBSuperWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipAddEnergyAndAttachBufLaunch Convert2LBSpaceProcessSuperEquipAddEnergyAndAttachBufLaunch(ProSpaceProcessEquipAddEnergyAndAttachBufLaunch proEquipAttachBufLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipAddShieldAndAttachBufLaunch Convert2LBSpaceProcessSuperEquipAddShieldAndAttachBufLaunch(ProSpaceProcessEquipAddShieldAndAttachBufLaunch proEquipAttachBufLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected static LBSpaceProcessSuperEquipAoeLaunch Convert2LBSpaceProcessSuperEquipAoeLaunch(ProLBSpaceProcessSuperEquipAoeLaunch proAoeLaunch, ILBSpaceTarget srcTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipAttachBufLaunch Convert2LBSpaceProcessSuperEquipAttachBufLaunch(ProSpaceProcessEquipAttachBufLaunch proEquipAttachBufLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch Convert2LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch(ProLBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch proEquipAttachBufLaunch, LBSuperEquipGroupBase equipGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget)
        {
        }

        [MethodImpl(0x8000)]
        private static LBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuffLaunch Convert2LBSpaceProcessSuperEquipShipShield2ExtraShieldAndAttachBuffLaunch(ProLBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch proEquipAttachBufLaunch, LBSuperEquipGroupBase equpGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSpaceProcessSuperLaserLaunch Convert2LBSpaceProcessSuperLaserLaunch(ProLBSpaceProcessSuperLaserLaunch proSuperLaserLaunch, LBSuperWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx, int waveToLaunch)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSpaceProcessSuperMissileLaunch Convert2LBSpaceProcessSuperMissileLaunch(ProLBSpaceProcessSuperMissileLaunch proSuperMisslieLaunch, LBSuperWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceContext spaceCtx, int waveLaunchCount)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSpaceProcessSuperPlasmaLaunch Convert2LBSpaceProcessSuperPlasmaLaunch(ProLBSpaceProcessSuperPlasmaLaunch proSuperPlasmaLaunch, LBSuperWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx, int waveLaunchCount)
        {
        }

        [MethodImpl(0x8000)]
        public static LBSpaceProcessSuperRailgunLaunch Convert2LBSpaceProcessSuperRailgunLaunch(ProLBSpaceProcessSuperRailgunLaunch proSuperCannonLaunch, LBSuperWeaponGroupBase weaponGroup, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceContext spaceCtx, int waveLaunchCount)
        {
        }

        [MethodImpl(0x8000)]
        public static List<LBSyncEvent> GetLBSyncEventList(ProSynEventList4ObjInfo proList4Obj, ILBInSpaceShip srcSpaceShip, ILBSpaceContext spaceCtx)
        {
        }

        [MethodImpl(0x8000)]
        public static List<LBSyncEventOnHitInfo> GetLBSyncEventOnHitInfoList(ProSynEventList4ObjInfo proList4Obj)
        {
        }
    }
}

