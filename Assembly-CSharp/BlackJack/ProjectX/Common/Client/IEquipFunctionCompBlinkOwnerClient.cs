﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IEquipFunctionCompBlinkOwnerClient : IEquipFunctionCompBlinkOwnerBase, ILBSpaceProcessEquipLaunchSource, IEquipFunctionCompOwner, ILBSpaceProcessSource
    {
    }
}

