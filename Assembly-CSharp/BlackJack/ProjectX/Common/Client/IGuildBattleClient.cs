﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildBattleClient : IGuildBattleBase
    {
        void GuildBattleReinforcementEndTimeSet(int endHour, int endMinuts);
    }
}

