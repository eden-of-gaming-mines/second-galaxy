﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockDevelopmentClient : LogicBlockDevelopmentBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ProjectIDCanUse;

        [MethodImpl(0x8000)]
        public bool ProjectIDCanUse(int projectId, out int errCode)
        {
        }

        [CompilerGenerated]
        private sealed class <ProjectIDCanUse>c__AnonStorey0
        {
            internal int filterId;
            internal LogicBlockDevelopmentClient $this;
        }

        [CompilerGenerated]
        private sealed class <ProjectIDCanUse>c__AnonStorey1
        {
            internal DevelopmentCostItemList itemFilter;
        }

        [CompilerGenerated]
        private sealed class <ProjectIDCanUse>c__AnonStorey2
        {
            internal long countNeed;
            internal bool bFind;
            internal LogicBlockDevelopmentClient.<ProjectIDCanUse>c__AnonStorey0 <>f__ref$0;
            internal LogicBlockDevelopmentClient.<ProjectIDCanUse>c__AnonStorey1 <>f__ref$1;

            internal bool <>m__0(LBStoreItem item)
            {
                if (!item.IsInBaseStore())
                {
                    return false;
                }
                int errCode = 0;
                if (!this.<>f__ref$0.$this.CheckItemByFilter(item.GetInternalStoreItemInfo().m_itemInfo, this.<>f__ref$0.filterId, this.<>f__ref$1.itemFilter.Rank, this.<>f__ref$1.itemFilter.SubRank, out errCode))
                {
                    return false;
                }
                ItemInfo itemInfoCopy = item.GetItemInfoCopy();
                itemInfoCopy.m_count = (item.GetItemCount() < this.countNeed) ? item.GetItemCount() : this.countNeed;
                this.countNeed -= itemInfoCopy.m_count;
                if (this.countNeed > 0L)
                {
                    return false;
                }
                this.bFind = true;
                return true;
            }
        }
    }
}

