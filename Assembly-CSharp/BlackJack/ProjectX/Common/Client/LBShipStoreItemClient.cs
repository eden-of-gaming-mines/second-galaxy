﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBShipStoreItemClient : LBShipStoreItemBase, ILBStoreItemClient, ILBItem
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.Client.ILBItem.GetItemType;
        private static DelegateBridge __Hotfix_SetStoreIndex;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.Client.ILBItem.GetConfigInfo;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.Client.ILBStoreItemClient.GetInstanceId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.Client.ILBStoreItemClient.GetStoreItemIndex;

        [MethodImpl(0x8000)]
        public LBShipStoreItemClient(ShipStoreItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        T ILBItem.GetConfigInfo<T>() where T: class
        {
        }

        [MethodImpl(0x8000)]
        StoreItemType ILBItem.GetItemType()
        {
        }

        [MethodImpl(0x8000)]
        ulong ILBStoreItemClient.GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        int ILBStoreItemClient.GetStoreItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void SetStoreIndex(int index)
        {
        }
    }
}

