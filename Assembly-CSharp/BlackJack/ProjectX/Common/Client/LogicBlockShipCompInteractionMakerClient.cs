﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompInteractionMakerClient : LogicBlockShipCompInteractionMakerBase
    {
        protected uint m_lastTickTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<uint, uint> EventOnStartInteraction;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<uint, int> EventOnInteractionEnd;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InteractionStart;
        private static DelegateBridge __Hotfix_InteractionEnd;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_add_EventOnStartInteraction;
        private static DelegateBridge __Hotfix_remove_EventOnStartInteraction;
        private static DelegateBridge __Hotfix_add_EventOnInteractionEnd;
        private static DelegateBridge __Hotfix_remove_EventOnInteractionEnd;

        public event Action<uint, int> EventOnInteractionEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<uint, uint> EventOnStartInteraction
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void InteractionEnd(int stopReason, float shipShield, float shipArmor, float shipEnergy)
        {
        }

        [MethodImpl(0x8000)]
        public void InteractionStart(uint targetObjId, uint endSolarSystemTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

