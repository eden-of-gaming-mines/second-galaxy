﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockSpaceSceneClient
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, uint, int> EventOnEnterScene;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, uint> EventOnLeaveScene;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSceneBindQuest;
        protected ILBPlayerContext m_playerCtx;
        protected LogicBlockQuestBase m_lbQuest;
        protected uint? m_spaceSceneInstanceId;
        protected int? m_spaceSceneId;
        protected int m_relativeQuestId;
        protected int m_relativeQuestInstanceId;
        protected int m_solarSystemId;
        protected int m_relativeSignalId;
        private List<int> m_relativeCtxParamList;
        private bool m_pvpEnableSetByServer;
        protected ConfigDataSceneInfo m_spaceSceneConf;
        protected SpaceSceneExtraDataNtf m_spaceSceneExtraData;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnPlayerEnterScene;
        private static DelegateBridge __Hotfix_OnPlayerLeaveScene;
        private static DelegateBridge __Hotfix_ClearPlayerSceneInfo;
        private static DelegateBridge __Hotfix_OnSceneBindQuest;
        private static DelegateBridge __Hotfix_OnSpaceSceneExtraDataNtf;
        private static DelegateBridge __Hotfix_SetScenePVPEnable;
        private static DelegateBridge __Hotfix_GetSpaceSceneExtraData;
        private static DelegateBridge __Hotfix_IsInScene;
        private static DelegateBridge __Hotfix_GetSpaceSceneId;
        private static DelegateBridge __Hotfix_GetSpaceSceneConfInfo;
        private static DelegateBridge __Hotfix_GetSpaceSceneInstanceId;
        private static DelegateBridge __Hotfix_GetRelativeQuestId;
        private static DelegateBridge __Hotfix_GetRelativeQuestInstanceId;
        private static DelegateBridge __Hotfix_GetCurrSceneShipTypeLimit;
        private static DelegateBridge __Hotfix_GetRelativeQuest;
        private static DelegateBridge __Hotfix_IsPVPEnable;
        private static DelegateBridge __Hotfix_GetRelativeCtxParamList;
        private static DelegateBridge __Hotfix_add_EventOnEnterScene;
        private static DelegateBridge __Hotfix_remove_EventOnEnterScene;
        private static DelegateBridge __Hotfix_add_EventOnLeaveScene;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveScene;
        private static DelegateBridge __Hotfix_add_EventOnSceneBindQuest;
        private static DelegateBridge __Hotfix_remove_EventOnSceneBindQuest;

        public event Action<int, uint, int> EventOnEnterScene
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, uint> EventOnLeaveScene
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSceneBindQuest
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearPlayerSceneInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> GetCurrSceneShipTypeLimit()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetRelativeCtxParamList()
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase GetRelativeQuest()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRelativeQuestId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRelativeQuestInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSceneInfo GetSpaceSceneConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSceneExtraDataNtf GetSpaceSceneExtraData()
        {
        }

        [MethodImpl(0x8000)]
        public int? GetSpaceSceneId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetSpaceSceneInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInScene()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPVPEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlayerEnterScene(uint spaceSceneInstanceId, int spaceSceneId, int relativeQuestInstanceId, int relativeQuestId, int solarSystemId, int relativeSignalId, bool pvpEnable = false, List<int> relativeCtxParamList = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlayerLeaveScene(int solarSystemId, uint sceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSceneBindQuest(int relativeQuestInstanceId, int relativeQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSpaceSceneExtraDataNtf(SpaceSceneExtraDataNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void SetScenePVPEnable(uint sceneInsId, bool isPVPEnable)
        {
        }
    }
}

