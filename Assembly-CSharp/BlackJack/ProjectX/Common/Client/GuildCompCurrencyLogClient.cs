﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompCurrencyLogClient : GuildCompCurrencyLogBase, IGuildCompCurrencyLogClient, IGuildCompCurrencyLogBase, IGuildCurrencyLogClient, IGuildCurrencyLogBase
    {
        private readonly List<GuildCurrencyLogInfo> m_GuildCurrencyLogInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildCurrencyLogList;
        private static DelegateBridge __Hotfix_RefreshGuildCurrencyLogList;

        [MethodImpl(0x8000)]
        public GuildCompCurrencyLogClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildCurrencyLogInfo> GetGuildCurrencyLogList()
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshGuildCurrencyLogList(List<GuildCurrencyLogInfo> list)
        {
        }
    }
}

