﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Runtime.UI;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonStrikeUtil
    {
        [CompilerGenerated]
        private static Comparison<LBStaticHiredCaptain.ShipInfo> <>f__mg$cache0;
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetStrikeResultForQuest;
        private static DelegateBridge __Hotfix_GetStrikeResultForPvpSignal;
        private static DelegateBridge __Hotfix_GetStrikeResultForRescue;
        private static DelegateBridge __Hotfix_GetStrikeResultForCommon;
        private static DelegateBridge __Hotfix_GetStrikeResultForTeamMember;
        private static DelegateBridge __Hotfix_GetStrikeResultForCelestial;
        private static DelegateBridge __Hotfix_GetStrikeResultForGuildAction;
        private static DelegateBridge __Hotfix_GetStrikeResultForGuildSentrySignal;
        private static DelegateBridge __Hotfix_GetStrikrResultForGuildMember;
        private static DelegateBridge __Hotfix_GetStrikrResultForSpaceSignal;
        private static DelegateBridge __Hotfix_StrikeForQuest;
        private static DelegateBridge __Hotfix_StrikeForPvpSignal;
        private static DelegateBridge __Hotfix_StrikeForResuce;
        private static DelegateBridge __Hotfix_StrikeForTeamMember;
        private static DelegateBridge __Hotfix_StrikeForCelestial;
        private static DelegateBridge __Hotfix_StrikeForWormholeGateInOtherSolarSystem;
        private static DelegateBridge __Hotfix_StrikeForGuildAction;
        private static DelegateBridge __Hotfix_StrikeForGuildSentrySignal;
        private static DelegateBridge __Hotfix_StrikeForGuildMember;
        private static DelegateBridge __Hotfix_StrikeForSpaceSignal;
        private static DelegateBridge __Hotfix_ReturnToSolarSystemUITask;
        private static DelegateBridge __Hotfix_IsCaptainAbleToStrike;
        private static DelegateBridge __Hotfix_GetMostSuitableShipForCaptainStrike;
        private static DelegateBridge __Hotfix_IsHireCaptainAbleToStrike;
        private static DelegateBridge __Hotfix_ShipSizeSorter;
        private static DelegateBridge __Hotfix_StrikeInSpace;
        private static DelegateBridge __Hotfix_StrikeForLeaveStation;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForQuest;
        private static DelegateBridge __Hotfix_LeaveStationForQuest;
        private static DelegateBridge __Hotfix_LeaveStationToSceneForQuest;
        private static DelegateBridge __Hotfix_LeaveStationToStationForQuest;
        private static DelegateBridge __Hotfix_OpenSpaceStationUITaskForQuest;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForQuest;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForPvpSignal;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForPvpSignal;
        private static DelegateBridge __Hotfix_LeaveStationForPvpSignal;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForRescue;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForRescue;
        private static DelegateBridge __Hotfix_LeaveStationForRescue;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForTeamMember;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForGuildMember;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForTeamMember;
        private static DelegateBridge __Hotfix_LeaveStationForTeamMember;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForGuildMember;
        private static DelegateBridge __Hotfix_LeaveStationForGuildMember;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForCelestial;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForCelestial;
        private static DelegateBridge __Hotfix_LeaveStationForCelestial;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForWormholeGateInOtherSolarSystem;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForWormholeGateInOtherSolarSystem;
        private static DelegateBridge __Hotfix_LeaveStationForWormholeGateInOtherSolarSystem;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForGuildAction;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForGuildAction;
        private static DelegateBridge __Hotfix_LeaveStationForGuildAction;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForGuildSentrySingal;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForGuildSentrySignal;
        private static DelegateBridge __Hotfix_LeaveStationForGuildSentrySignal;
        private static DelegateBridge __Hotfix_StartShipNavigationInSpaceForSpaceSignal;
        private static DelegateBridge __Hotfix_ChooseShipStrikeForSpaceSignalSignal;
        private static DelegateBridge __Hotfix_LeaveStationForSpaceSignal;
        private static DelegateBridge __Hotfix_GetCurrShipInstanceId;
        private static DelegateBridge __Hotfix_ChooseShipForStrike;
        private static DelegateBridge __Hotfix_OnLeaveStationComplete;
        private static DelegateBridge __Hotfix_Clear4EnterSpace;
        private static DelegateBridge __Hotfix_OnHideSolarySystem;
        private static DelegateBridge __Hotfix_OnSolarSystemUnreachable;
        private static DelegateBridge __Hotfix_GetAllowInShipTypesForCelestial;
        private static DelegateBridge __Hotfix_IsableToNavigationInSpace;
        private static DelegateBridge __Hotfix_IsCharacterInSpace;
        private static DelegateBridge __Hotfix_IsPlayerInHideSolarySystem;
        private static DelegateBridge __Hotfix_IsCharacterStayWithMotherShip;
        private static DelegateBridge __Hotfix_IsNeedToLeaveCurrentStationForQuest;
        private static DelegateBridge __Hotfix_IsQuestInSpaceScene;
        private static DelegateBridge __Hotfix_IsCurrentShipTypeSuitableForQuest;
        private static DelegateBridge __Hotfix_IsCurrentShipGrandFactionSuitableForQuest;
        private static DelegateBridge __Hotfix_IsNeedToLeaveCurrentStation;
        private static DelegateBridge __Hotfix_IsCurrentShipSuitableForSignal;
        private static DelegateBridge __Hotfix_IsCurrentShipSuitableForRescue;
        private static DelegateBridge __Hotfix_IsCurrentShipSuitableForGuildAction;
        private static DelegateBridge __Hotfix_IsCurrentShipSuitableForGuildSentrySingal;
        private static DelegateBridge __Hotfix_IsInTheSameStation;
        private static DelegateBridge __Hotfix_IsInTheSameSolarSystem;
        private static DelegateBridge __Hotfix_IsFarEnoughForJumpToCelestial;
        private static DelegateBridge __Hotfix_IsFarEnoughForJumpToStation;
        private static DelegateBridge __Hotfix_IsFarEnoughForJumpToStargate;
        private static DelegateBridge __Hotfix_IsInTheSameScene;
        private static DelegateBridge __Hotfix_IsSceneAllowHireCaptain;
        private static DelegateBridge __Hotfix_IsCharacterDriveFlagShip;
        private static DelegateBridge __Hotfix_CanSolarSystemArrived;
        private static DelegateBridge __Hotfix_SendShipNavigationCmdReq;

        [MethodImpl(0x8000)]
        protected static bool CanSolarSystemArrived(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected static void ChooseShipForStrike(UIIntent preIntent, List<ShipType> allowInShipTypes, bool useDefaultBgTask, Action<ulong, Action<bool>> onSelectShip4Strike, List<GrandFaction> validGrandFaction = null, bool allowHireCaptian = true, int shipHangarIndex = -1, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void ChooseShipStrikeForCelestial(UIIntent preIntent, NavigationNodeType navigationType, int destId, int solarSystemId, uint destSceneInstanceId, bool useDefaultBgTask, Action<bool> onEnd, int shipHangarIndex = -1, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private static void ChooseShipStrikeForGuildAction(UIIntent preIntent, ulong guildActionInstanceId, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void ChooseShipStrikeForGuildMember(UIIntent preIntent, string guildMemberId, bool useDefaultBgTask, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void ChooseShipStrikeForGuildSentrySignal(UIIntent preIntent, GuildSentryInterestScene scene, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private static void ChooseShipStrikeForPvpSignal(UIIntent preIntent, ulong pvpSignalInstanceId, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void ChooseShipStrikeForQuest(UIIntent preIntent, int questInstanceId, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void ChooseShipStrikeForRescue(UIIntent preIntent, LBPVPInvadeRescue rescueInfo, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private static void ChooseShipStrikeForSpaceSignalSignal(UIIntent preIntent, SignalInfo spaceSignal, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void ChooseShipStrikeForTeamMember(UIIntent preIntent, TeamMemberInfo teamMemberInfo, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void ChooseShipStrikeForWormholeGateInOtherSolarSystem(UIIntent preIntent, int solarSystemId, ConfigDataWormholeStarGroupInfo wormholeConfigInfo, bool useDefaultBgTask, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected static void Clear4EnterSpace()
        {
        }

        [MethodImpl(0x8000)]
        protected static List<ShipType> GetAllowInShipTypesForCelestial(uint destSceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected static ulong GetCurrShipInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetMostSuitableShipForCaptainStrike(List<ShipType> allowInShipTypes, LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForCelestial(NavigationNodeType navigationType, int destId, int solarSystemId, uint destSceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForCommon(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForGuildAction(ulong guildActionInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForGuildSentrySignal(GuildSentryInterestScene sceneInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForPvpSignal(ulong pvpSignalInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForQuest(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForRescue(LBPVPInvadeRescue rescueInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikeResultForTeamMember(TeamMemberInfo teamMemberInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikrResultForGuildMember(string guildMemberId, out Vector3D location)
        {
        }

        [MethodImpl(0x8000)]
        protected static StrikeResult GetStrikrResultForSpaceSignal(SignalInfo signal)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsableToNavigationInSpace()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsCaptainAbleToStrike(List<ShipType> allowInShipTypes, LBStaticHiredCaptain captain, LBStaticHiredCaptain.ShipInfo shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCharacterDriveFlagShip()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCharacterInSpace()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCharacterStayWithMotherShip()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCurrentShipGrandFactionSuitableForQuest(LBProcessingQuestBase selQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCurrentShipSuitableForGuildAction(GuildActionInfo guildAction)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCurrentShipSuitableForGuildSentrySingal(GuildSentryInterestScene scene)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCurrentShipSuitableForRescue(LBPVPInvadeRescue rescueInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCurrentShipSuitableForSignal(LBPVPSignal signal)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsCurrentShipTypeSuitableForQuest(LBProcessingQuestBase selQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsFarEnoughForJumpToCelestial(int solarSystemId, int gdbCelestialId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsFarEnoughForJumpToStargate(int solarSystemId, int gdbStargateId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsFarEnoughForJumpToStation(int solarSystemId, int gdbStationId)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsHireCaptainAbleToStrike(List<ShipType> allowInShipTypes, LBStaticHiredCaptain captain, LBStaticHiredCaptain.ShipInfo shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsInTheSameScene(int solarSystemId, uint sceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsInTheSameSolarSystem(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsInTheSameStation(int solarSystemId, int stationId)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsNeedToLeaveCurrentStation(LBProcessingQuestBase selQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsNeedToLeaveCurrentStationForQuest(LBProcessingQuestBase selQuest)
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsPlayerInHideSolarySystem()
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsQuestInSpaceScene(LBProcessingQuestBase selQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected static bool IsSceneAllowHireCaptain(int sceneId)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationForCelestial(ulong shipInstanceId, NavigationNodeType navigationType, int destId, int solarSystemId, uint destSceneInstanceId, ulong targetInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void LeaveStationForGuildAction(ulong shipInstanceId, int desSolarSystemId, ulong guildActionInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationForGuildMember(ulong shipInstanceId, string guildMemberId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void LeaveStationForGuildSentrySignal(ulong shipInstanceId, GuildSentryInterestScene scene, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void LeaveStationForPvpSignal(ulong shipInstanceId, ulong pvpSignalInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationForQuest(ulong shipInstanceId, int questInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationForRescue(ulong shipInstanceId, LBPVPInvadeRescue rescueInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void LeaveStationForSpaceSignal(ulong shipInstanceId, SignalInfo spaceSignal, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationForTeamMember(ulong shipInstanceId, TeamMemberInfo teamMemberInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationForWormholeGateInOtherSolarSystem(ulong shipInstanceId, int solarSystemId, ConfigDataWormholeStarGroupInfo wormholeConfigInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationToSceneForQuest(ulong shipInstanceId, int questInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void LeaveStationToStationForQuest(ulong shipInstanceId, int questInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void OnHideSolarySystem(IStrikeSrcUITask srcTas)
        {
        }

        [MethodImpl(0x8000)]
        protected static void OnLeaveStationComplete()
        {
        }

        [MethodImpl(0x8000)]
        private static void OnSolarSystemUnreachable(IStrikeSrcUITask srcTas)
        {
        }

        [MethodImpl(0x8000)]
        protected static void OpenSpaceStationUITaskForQuest(int questInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void ReturnToSolarSystemUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected static void SendShipNavigationCmdReq(NavigationNodeType destType, int destSolarSystemId, int destId, ulong targetInstanceId, int questInstanceId, uint globalSceneInstanceid, string teamMemberGameUserId = null, string guildMemberGameUserId = null, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static int ShipSizeSorter(LBStaticHiredCaptain.ShipInfo shipAInfo, LBStaticHiredCaptain.ShipInfo shipBInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartShipNavigationInSpaceForCelestial(NavigationNodeType navigationType, int destId, int solarSystemId, uint destSceneInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void StartShipNavigationInSpaceForGuildAction(int destSolarSystemId, ulong guildActionInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartShipNavigationInSpaceForGuildMember(string destGuildMemberGameUserIdd, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void StartShipNavigationInSpaceForGuildSentrySingal(GuildSentryInterestScene scene, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void StartShipNavigationInSpaceForPvpSignal(ulong signalInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartShipNavigationInSpaceForQuest(int questInstanceId, bool navToScene, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartShipNavigationInSpaceForRescue(LBPVPInvadeRescue rescueInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private static void StartShipNavigationInSpaceForSpaceSignal(SignalInfo spaceSignal, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartShipNavigationInSpaceForTeamMember(TeamMemberInfo teamMemberInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartShipNavigationInSpaceForWormholeGateInOtherSolarSystem(int solarSystemId, ConfigDataWormholeStarGroupInfo wormholeConfigInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForCelestial(IStrikeSrcUITask srcTask, NavigationNodeType navigationType, int destId, int solarSystemId, uint destSceneInstanceId, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForGuildAction(IStrikeSrcUITask srcTask, int destSolarSystemId, ulong guildActionInstanceId, IUIBackgroundManager backgroundManager = null, Action onMsgBoxConfirm = null, Action onMsgBoxCancel = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForGuildMember(IStrikeSrcUITask srcTask, string destMemberId)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForGuildSentrySignal(IStrikeSrcUITask srcTask, GuildSentryInterestScene sceneInfo, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForLeaveStation(IStrikeSrcUITask srcTask, int hangarIndex, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForPvpSignal(IStrikeSrcUITask srcTask, ulong pvpSignalInstanceId, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForQuest(IStrikeSrcUITask srcTask, int questInstanceId, bool isStayWithMotherShip = false, Action onMsgBoxConfirm = null, Action onMsgBoxCancel = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForResuce(IStrikeSrcUITask srcTask, ulong resuceSignalInstanceId, int solarSystemId = 0, uint sceneInstanceId = 0, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForSpaceSignal(IStrikeSrcUITask srcTask, SignalInfo spaceSignal, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForTeamMember(IStrikeSrcUITask srcTask, string teamMemberUserId, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeForWormholeGateInOtherSolarSystem(IStrikeSrcUITask srcTask, int solarSystemId, ConfigDataWormholeStarGroupInfo wormholeConfigInfo, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StrikeInSpace(NavigationNodeType destType, int destSolarSystemId, int destId, ulong targetInstanceId, int questInstanceId, uint globalSceneInstanceid, string teamMemberGameUserId = null, string guildMemberGameUserId = null, Action<bool> onEnd = null)
        {
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForCelestial>c__AnonStorey1B
        {
            internal NavigationNodeType navigationType;
            internal int destId;
            internal int solarSystemId;
            internal uint destSceneInstanceId;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForCelestial>c__AnonStorey1C storeyc = new <ChooseShipStrikeForCelestial>c__AnonStorey1C {
                    <>f__ref$27 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForCelestial(shipInstanceId, this.navigationType, this.destId, this.solarSystemId, this.destSceneInstanceId, 0UL, new Action<bool>(storeyc.<>m__0));
            }

            private sealed class <ChooseShipStrikeForCelestial>c__AnonStorey1C
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForCelestial>c__AnonStorey1B <>f__ref$27;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$27.onEnd != null))
                    {
                        this.<>f__ref$27.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForGuildAction>c__AnonStorey20
        {
            internal GuildActionInfo actionInfo;
            internal ulong guildActionInstanceId;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForGuildAction>c__AnonStorey21 storey = new <ChooseShipStrikeForGuildAction>c__AnonStorey21 {
                    <>f__ref$32 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForGuildAction(shipInstanceId, this.actionInfo.m_solarSystemId, this.guildActionInstanceId, new Action<bool>(storey.<>m__0));
            }

            private sealed class <ChooseShipStrikeForGuildAction>c__AnonStorey21
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForGuildAction>c__AnonStorey20 <>f__ref$32;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$32.onEnd != null))
                    {
                        this.<>f__ref$32.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForGuildMember>c__AnonStorey18
        {
            internal string guildMemberId;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForGuildMember>c__AnonStorey19 storey = new <ChooseShipStrikeForGuildMember>c__AnonStorey19 {
                    <>f__ref$24 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForGuildMember(shipInstanceId, this.guildMemberId, new Action<bool>(storey.<>m__0));
            }

            private sealed class <ChooseShipStrikeForGuildMember>c__AnonStorey19
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForGuildMember>c__AnonStorey18 <>f__ref$24;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$24.onEnd != null))
                    {
                        this.<>f__ref$24.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForGuildSentrySignal>c__AnonStorey23
        {
            internal GuildSentryInterestScene scene;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForGuildSentrySignal>c__AnonStorey24 storey = new <ChooseShipStrikeForGuildSentrySignal>c__AnonStorey24 {
                    <>f__ref$35 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForGuildSentrySignal(shipInstanceId, this.scene, new Action<bool>(storey.<>m__0));
            }

            private sealed class <ChooseShipStrikeForGuildSentrySignal>c__AnonStorey24
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForGuildSentrySignal>c__AnonStorey23 <>f__ref$35;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$35.onEnd != null))
                    {
                        this.<>f__ref$35.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForPvpSignal>c__AnonStoreyF
        {
            internal ulong pvpSignalInstanceId;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForPvpSignal>c__AnonStorey10 storey = new <ChooseShipStrikeForPvpSignal>c__AnonStorey10 {
                    <>f__ref$15 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForPvpSignal(shipInstanceId, this.pvpSignalInstanceId, new Action<bool>(storey.<>m__0));
            }

            private sealed class <ChooseShipStrikeForPvpSignal>c__AnonStorey10
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForPvpSignal>c__AnonStoreyF <>f__ref$15;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$15.onEnd != null))
                    {
                        this.<>f__ref$15.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForQuest>c__AnonStoreyD
        {
            internal int questInstanceId;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForQuest>c__AnonStoreyE ye = new <ChooseShipStrikeForQuest>c__AnonStoreyE {
                    <>f__ref$13 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForQuest(shipInstanceId, this.questInstanceId, new Action<bool>(ye.<>m__0));
            }

            private sealed class <ChooseShipStrikeForQuest>c__AnonStoreyE
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForQuest>c__AnonStoreyD <>f__ref$13;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (this.<>f__ref$13.onEnd != null)
                    {
                        this.<>f__ref$13.onEnd(result);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForRescue>c__AnonStorey12
        {
            internal LBPVPInvadeRescue rescueInfo;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForRescue>c__AnonStorey13 storey = new <ChooseShipStrikeForRescue>c__AnonStorey13 {
                    <>f__ref$18 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForRescue(shipInstanceId, this.rescueInfo, new Action<bool>(storey.<>m__0));
            }

            private sealed class <ChooseShipStrikeForRescue>c__AnonStorey13
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForRescue>c__AnonStorey12 <>f__ref$18;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$18.onEnd != null))
                    {
                        this.<>f__ref$18.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForSpaceSignalSignal>c__AnonStorey26
        {
            internal SignalInfo spaceSignal;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForSpaceSignalSignal>c__AnonStorey27 storey = new <ChooseShipStrikeForSpaceSignalSignal>c__AnonStorey27 {
                    <>f__ref$38 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForSpaceSignal(shipInstanceId, this.spaceSignal, new Action<bool>(storey.<>m__0));
            }

            private sealed class <ChooseShipStrikeForSpaceSignalSignal>c__AnonStorey27
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForSpaceSignalSignal>c__AnonStorey26 <>f__ref$38;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$38.onEnd != null))
                    {
                        this.<>f__ref$38.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForTeamMember>c__AnonStorey15
        {
            internal TeamMemberInfo teamMemberInfo;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForTeamMember>c__AnonStorey16 storey = new <ChooseShipStrikeForTeamMember>c__AnonStorey16 {
                    <>f__ref$21 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForTeamMember(shipInstanceId, this.teamMemberInfo, new Action<bool>(storey.<>m__0));
            }

            private sealed class <ChooseShipStrikeForTeamMember>c__AnonStorey16
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForTeamMember>c__AnonStorey15 <>f__ref$21;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$21.onEnd != null))
                    {
                        this.<>f__ref$21.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChooseShipStrikeForWormholeGateInOtherSolarSystem>c__AnonStorey1E
        {
            internal int solarSystemId;
            internal ConfigDataWormholeStarGroupInfo wormholeConfigInfo;
            internal Action<bool> onEnd;

            internal void <>m__0(ulong shipInstanceId, Action<bool> onSendMsgEnd)
            {
                <ChooseShipStrikeForWormholeGateInOtherSolarSystem>c__AnonStorey1F storeyf = new <ChooseShipStrikeForWormholeGateInOtherSolarSystem>c__AnonStorey1F {
                    <>f__ref$30 = this,
                    onSendMsgEnd = onSendMsgEnd
                };
                CommonStrikeUtil.LeaveStationForWormholeGateInOtherSolarSystem(shipInstanceId, this.solarSystemId, this.wormholeConfigInfo, new Action<bool>(storeyf.<>m__0));
            }

            private sealed class <ChooseShipStrikeForWormholeGateInOtherSolarSystem>c__AnonStorey1F
            {
                internal Action<bool> onSendMsgEnd;
                internal CommonStrikeUtil.<ChooseShipStrikeForWormholeGateInOtherSolarSystem>c__AnonStorey1E <>f__ref$30;

                internal void <>m__0(bool result)
                {
                    this.onSendMsgEnd(result);
                    if (result && (this.<>f__ref$30.onEnd != null))
                    {
                        this.<>f__ref$30.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <IsFarEnoughForJumpToCelestial>c__AnonStorey29
        {
            internal int gdbCelestialId;

            internal bool <>m__0(GDBPlanetInfo p) => 
                (p.Id == this.gdbCelestialId);
        }

        [CompilerGenerated]
        private sealed class <IsFarEnoughForJumpToStargate>c__AnonStorey2B
        {
            internal int gdbStargateId;

            internal bool <>m__0(GDBStargateInfo stargate) => 
                (stargate.Id == this.gdbStargateId);
        }

        [CompilerGenerated]
        private sealed class <IsFarEnoughForJumpToStation>c__AnonStorey2A
        {
            internal int gdbStationId;

            internal bool <>m__0(GDBSpaceStationInfo station) => 
                (station.Id == this.gdbStationId);
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForCelestial>c__AnonStorey1D
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                HangarShipLeaveStationForCelestialNetTask task2 = task as HangarShipLeaveStationForCelestialNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_leaveStationForCelestialAckResult != 0)
                    {
                        Debug.LogError("HangarShipStrikeReqNetTask error:  returnTask.m_ackResult != 0");
                        this.onEnd(false);
                    }
                    else
                    {
                        this.onEnd(true);
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForGuildAction>c__AnonStorey22
        {
            internal Action<bool> onEnd;

            internal void <>m__0(bool result)
            {
                if (result && (this.onEnd != null))
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForGuildMember>c__AnonStorey1A
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                SpaceStationLeaveForGuildMemberNetTask task2 = task as SpaceStationLeaveForGuildMemberNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                        this.onEnd(false);
                    }
                    else
                    {
                        this.onEnd(true);
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForGuildSentrySignal>c__AnonStorey25
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                SpaceStationLeaveForGuildSentrySingalNetTask task2 = task as SpaceStationLeaveForGuildSentrySingalNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_result == 0)
                    {
                        this.onEnd(true);
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                    else
                    {
                        Debug.LogError("HangarShipStrikeForQuestNetTask error:  returnTask.m_ackResult != 0");
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForPvpSignal>c__AnonStorey11
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                SpaceStationLeaveForPVPSignalNetTask task2 = task as SpaceStationLeaveForPVPSignalNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_strikeForPVPSignalAckResult == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                    else
                    {
                        Debug.LogError("SpaceStationLeaveForPVPSignalNetTask error:  returnTask.m_ackResult != 0");
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_strikeForPVPSignalAckResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForRescue>c__AnonStorey14
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                SpaceStationLeaveForRescueNetTask task2 = task as SpaceStationLeaveForRescueNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_result != 0)
                    {
                        Debug.LogError("SpaceStationLeaveForRescueNetTask error:  returnTask.m_ackResult != 0");
                        this.onEnd(false);
                    }
                    else
                    {
                        this.onEnd(true);
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForSpaceSignal>c__AnonStorey28
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                SpaceStationLeaveForSpaceSignalNetTask task2 = task as SpaceStationLeaveForSpaceSignalNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.m_strikeForSpaceSignalAckResult == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                    else
                    {
                        Debug.LogError("SpaceStationLeaveForSpaceSignalNetTask error:  returnTask.m_ackResult != 0");
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_strikeForSpaceSignalAckResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationForTeamMember>c__AnonStorey17
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                SpaceStationLeaveForTeamMemberNetTask task2 = task as SpaceStationLeaveForTeamMemberNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                        this.onEnd(false);
                    }
                    else
                    {
                        this.onEnd(true);
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationToSceneForQuest>c__AnonStoreyB
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                HangarShipStrikeForQuestNetTask task2 = task as HangarShipStrikeForQuestNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_strikeForQuestAckResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_strikeForQuestAckResult, true, false);
                        this.onEnd(false);
                    }
                    else
                    {
                        this.onEnd(true);
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <LeaveStationToStationForQuest>c__AnonStoreyC
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                HangarShipLeaveStationForCelestialNetTask task2 = task as HangarShipLeaveStationForCelestialNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.m_leaveStationForCelestialAckResult != 0)
                    {
                        Debug.LogError("HangarShipStrikeReqNetTask error:  returnTask.m_ackResult != 0");
                        this.onEnd(false);
                    }
                    else
                    {
                        this.onEnd(true);
                        CommonStrikeUtil.OnLeaveStationComplete();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForCelestial>c__AnonStorey4
        {
            internal NavigationNodeType navigationType;
            internal int destId;
            internal int solarSystemId;
            internal uint destSceneInstanceId;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForCelestial(this.navigationType, this.destId, this.solarSystemId, this.destSceneInstanceId, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__1()
            {
                this.srcTask.OnStrikeEnd(false);
            }

            internal void <>m__2(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForCelestial(intent, this.navigationType, this.destId, this.solarSystemId, this.destSceneInstanceId, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), -1, this.backgroundManager);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForGuildAction>c__AnonStorey6
        {
            internal ulong guildActionInstanceId;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;
            internal int destSolarSystemId;

            internal void <>m__0(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForGuildAction(intent, this.guildActionInstanceId, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }

            internal void <>m__1()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForGuildAction(this.destSolarSystemId, this.guildActionInstanceId, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__2()
            {
                this.srcTask.OnStrikeEnd(false);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForGuildMember>c__AnonStorey8
        {
            internal string destMemberId;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;

            internal void <>m__0(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForGuildMember(intent, this.destMemberId, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__1()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForGuildMember(this.destMemberId, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__2()
            {
                this.srcTask.OnStrikeEnd(false);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForGuildSentrySignal>c__AnonStorey7
        {
            internal GuildSentryInterestScene sceneInfo;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForGuildSentrySignal(intent, this.sceneInfo, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }

            internal void <>m__1()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForGuildSentrySingal(this.sceneInfo, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__2()
            {
                this.srcTask.OnStrikeEnd(false);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForLeaveStation>c__AnonStoreyA
        {
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal int hangarIndex;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForCelestial(intent, NavigationNodeType.SolarSystem, 0, 0, 0, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.hangarIndex, this.backgroundManager);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForPvpSignal>c__AnonStorey1
        {
            internal ulong pvpSignalInstanceId;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForPvpSignal(this.pvpSignalInstanceId, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__1()
            {
                this.srcTask.OnStrikeEnd(false);
            }

            internal void <>m__2(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForPvpSignal(intent, this.pvpSignalInstanceId, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForQuest>c__AnonStorey0
        {
            internal int questInstanceId;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForQuest(this.questInstanceId, true, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__1()
            {
                this.srcTask.OnStrikeEnd(false);
            }

            internal void <>m__2()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForQuest(this.questInstanceId, false, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__3()
            {
                this.srcTask.OnStrikeEnd(false);
            }

            internal void <>m__4(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForQuest(intent, this.questInstanceId, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }

            internal void <>m__5()
            {
                CommonStrikeUtil.OpenSpaceStationUITaskForQuest(this.questInstanceId, new Action<bool>(this.srcTask.OnStrikeEnd));
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForResuce>c__AnonStorey2
        {
            internal LBPVPInvadeRescue lbRescueInfo;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForRescue(this.lbRescueInfo, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__1()
            {
                this.srcTask.OnStrikeEnd(false);
            }

            internal void <>m__2(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForRescue(intent, this.lbRescueInfo, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForSpaceSignal>c__AnonStorey9
        {
            internal SignalInfo spaceSignal;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForSpaceSignalSignal(intent, this.spaceSignal, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }

            internal void <>m__1()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForSpaceSignal(this.spaceSignal, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__2()
            {
                this.srcTask.OnStrikeEnd(false);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForTeamMember>c__AnonStorey3
        {
            internal TeamMemberInfo teamMemberInfo;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForTeamMember(this.teamMemberInfo, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__1()
            {
                this.srcTask.OnStrikeEnd(false);
            }

            internal void <>m__2(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForTeamMember(intent, this.teamMemberInfo, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }
        }

        [CompilerGenerated]
        private sealed class <StrikeForWormholeGateInOtherSolarSystem>c__AnonStorey5
        {
            internal int solarSystemId;
            internal ConfigDataWormholeStarGroupInfo wormholeConfigInfo;
            internal CommonStrikeUtil.IStrikeSrcUITask srcTask;
            internal IUIBackgroundManager backgroundManager;

            internal void <>m__0()
            {
                CommonStrikeUtil.StartShipNavigationInSpaceForWormholeGateInOtherSolarSystem(this.solarSystemId, this.wormholeConfigInfo, new Action<bool>(this.srcTask.OnStrikeEnd));
            }

            internal void <>m__1()
            {
                this.srcTask.OnStrikeEnd(false);
            }

            internal void <>m__2(UIIntent intent, bool useDefaultBgTask)
            {
                CommonStrikeUtil.ChooseShipStrikeForWormholeGateInOtherSolarSystem(intent, this.solarSystemId, this.wormholeConfigInfo, useDefaultBgTask, new Action<bool>(this.srcTask.OnStrikeEnd), this.backgroundManager);
            }
        }

        public interface IStrikeSrcUITask
        {
            void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd);
            void BeforeEnterSpaceStationUITask(Action onEnd);
            void OnStrikeEnd(bool success);
        }

        public enum StrikeResult
        {
            Invalid,
            CurrentShipTypeNotSuitable,
            CurrentShipGrandFactionNotSuitable,
            AlreadyInQuestScene,
            AlreadInTheSameStation,
            AlreadInTheSameScene,
            AlreadyInTheSameSolarSystem,
            MoveToLocation,
            MoveToEnterStation,
            NotFarEnoughForJump,
            ShipNavigationInSpace,
            ShipNavigationToQuestScene,
            ShipNavigationToQuestStation,
            ShipNavigationForPvp,
            ChooseShipForStrike,
            OpenSpaceStation,
            LeaveStation,
            LeaveStationForQuestScene,
            LeaveStationForQuestStation,
            LeaveStationForPvp,
            ShipNavigationForGuildAction,
            LeaveStationForGuildAction,
            HideSolarySystem,
            SolarSystemUnreachable,
            ShipMoveDisableByGuildFleet
        }
    }
}

