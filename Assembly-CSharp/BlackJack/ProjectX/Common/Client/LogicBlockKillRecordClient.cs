﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockKillRecordClient : LogicBlockKillRecordBase
    {
        protected DateTime m_lastUpdateKillRecordTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_AddKillRecordInfo;
        private static DelegateBridge __Hotfix_UpdateKillRecord_0;
        private static DelegateBridge __Hotfix_UpdateKillRecord_1;
        private static DelegateBridge __Hotfix_GetLastUpdateKillRecordTime;

        [MethodImpl(0x8000)]
        public override void AddKillRecordInfo(KillRecordInfo killRecordInfo)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetLastUpdateKillRecordTime()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateKillRecord(KillRecordInfo killRecordInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateKillRecord(LossCompensation compensation)
        {
        }
    }
}

