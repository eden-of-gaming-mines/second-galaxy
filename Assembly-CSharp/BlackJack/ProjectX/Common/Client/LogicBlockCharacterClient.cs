﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockCharacterClient : LogicBlockCharacterBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSolarSystemChanged;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateSolarSystemId;
        private static DelegateBridge __Hotfix_OnDailyRefresh;
        private static DelegateBridge __Hotfix_OnUpdateCurrencyInfo;
        private static DelegateBridge __Hotfix_GetCharactorAttackGain_Final;
        private static DelegateBridge __Hotfix_GetCharactorDefenceGain_Final;
        private static DelegateBridge __Hotfix_GetCharactorDriveGain_Final;
        private static DelegateBridge __Hotfix_GetCharactorElectronGain_Final;
        private static DelegateBridge __Hotfix_add_EventOnSolarSystemChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSolarSystemChanged;

        public event Action<int> EventOnSolarSystemChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public float GetCharactorAttackGain_Final(int charactorAttackBase)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCharactorDefenceGain_Final(int charactorDefenceBase)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCharactorDriveGain_Final(int charactorDriveBase)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCharactorElectronGain_Final(int charactorElectronBase)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDailyRefresh(long afkExp)
        {
        }

        [MethodImpl(0x8000)]
        public void OnUpdateCurrencyInfo(CurrencyUpdateInfo updateInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override void UpdateSolarSystemId(int solarSystemId)
        {
        }
    }
}

