﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;
    using System.Collections.Generic;

    public interface IGuildLostReportDataContainerClient
    {
        void RefleshSovereignLostReportList(List<SovereignLostReport> sovereignLostReportList);
    }
}

