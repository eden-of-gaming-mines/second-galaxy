﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompWeaponEquipSetupClient4PlayerShip : LogicBlockShipCompWeaponEquipSetupClientBase
    {
        protected ILBStaticPlayerShip m_ownerShip;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnWeaponEquipUpdating;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SuspendItemForSlotGroupSet;
        private static DelegateBridge __Hotfix_UpdateCacheInfoForSetSlotGroup;
        private static DelegateBridge __Hotfix_AutoLoadAmmoForSlotGroupSet;
        private static DelegateBridge __Hotfix_PostSetItem2SlotGroup;
        private static DelegateBridge __Hotfix_CheckWeaponEquipSetCpuCost;
        private static DelegateBridge __Hotfix_CheckWeaponEquipSetPowerCost;
        private static DelegateBridge __Hotfix_PreProcessingForUnsetWeaponGroup;
        private static DelegateBridge __Hotfix_ResumeItemFromSlotGroup;
        private static DelegateBridge __Hotfix_UpdateCacheInfoForUnsetSlotGroup;
        private static DelegateBridge __Hotfix_PostRemoveItemFromSlotGroup;
        private static DelegateBridge __Hotfix_CollectAllRemoveWeaponEquipGroups;
        private static DelegateBridge __Hotfix_GetLBWeaponEquipSetupEnv;
        private static DelegateBridge __Hotfix_GetAllItemStoreSize;
        private static DelegateBridge __Hotfix_GetAllAmmoStoreSize;
        private static DelegateBridge __Hotfix_GetAllItemCostCPU;
        private static DelegateBridge __Hotfix_GetAllItemCostPower;
        private static DelegateBridge __Hotfix_add_EventOnWeaponEquipUpdating;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponEquipUpdating;

        public event Action EventOnWeaponEquipUpdating
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void AutoLoadAmmoForSlotGroupSet(SimpleItemInfo ammoRequire, LBStaticWeaponEquipSlotGroup group, AmmoStoreItemInfo removedAmmo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool CheckWeaponEquipSetCpuCost(LBStoreItem srcMSStoreItem, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool CheckWeaponEquipSetPowerCost(LBStoreItem srcMSStoreItem, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<LBStaticWeaponEquipSlotGroup> CollectAllRemoveWeaponEquipGroups()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetAllAmmoStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAllItemCostCPU()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAllItemCostPower()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetAllItemStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompWeaponEquipSetupEnv GetLBWeaponEquipSetupEnv()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBStaticShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostRemoveItemFromSlotGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostSetItem2SlotGroup(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool PreProcessingForUnsetWeaponGroup(LBStaticWeaponEquipSlotGroup group, AmmoStoreItemInfo ammoInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool ResumeItemFromSlotGroup(LBStaticWeaponEquipSlotGroup group, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool SuspendItemForSlotGroupSet(LBStoreItem srcItem, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem destStoreItem)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateCacheInfoForSetSlotGroup(LBStaticWeaponEquipSlotGroup group, LBStoreItem destItem)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateCacheInfoForUnsetSlotGroup(LBStaticWeaponEquipSlotGroup group)
        {
        }
    }
}

