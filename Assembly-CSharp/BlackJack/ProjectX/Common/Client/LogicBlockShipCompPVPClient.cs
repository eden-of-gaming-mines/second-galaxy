﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompPVPClient : LogicBlockShipCompPVPBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DateTime> EventOnResetJustCrime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DateTime> EventOnResetCriminalHunter;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<float> EventOnIncCriminalLevel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnJustCrimeTimeOut;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCriminalHunterTimeOut;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_SyncResetJustCrime;
        private static DelegateBridge __Hotfix_SyncResetCriminalHunter;
        private static DelegateBridge __Hotfix_SyncIncCriminalLevel;
        private static DelegateBridge __Hotfix_SyncJustCrimeTimeOut;
        private static DelegateBridge __Hotfix_SyncCriminalHunterTimeOut;
        private static DelegateBridge __Hotfix_add_EventOnResetJustCrime;
        private static DelegateBridge __Hotfix_remove_EventOnResetJustCrime;
        private static DelegateBridge __Hotfix_add_EventOnResetCriminalHunter;
        private static DelegateBridge __Hotfix_remove_EventOnResetCriminalHunter;
        private static DelegateBridge __Hotfix_add_EventOnIncCriminalLevel;
        private static DelegateBridge __Hotfix_remove_EventOnIncCriminalLevel;
        private static DelegateBridge __Hotfix_add_EventOnJustCrimeTimeOut;
        private static DelegateBridge __Hotfix_remove_EventOnJustCrimeTimeOut;
        private static DelegateBridge __Hotfix_add_EventOnCriminalHunterTimeOut;
        private static DelegateBridge __Hotfix_remove_EventOnCriminalHunterTimeOut;

        public event Action EventOnCriminalHunterTimeOut
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float> EventOnIncCriminalLevel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnJustCrimeTimeOut
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DateTime> EventOnResetCriminalHunter
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DateTime> EventOnResetJustCrime
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SyncCriminalHunterTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void SyncIncCriminalLevel(float incValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncJustCrimeTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void SyncResetCriminalHunter(DateTime endTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncResetJustCrime(DateTime endTime)
        {
        }
    }
}

