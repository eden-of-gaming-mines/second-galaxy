﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompPurchaseOrderClient : GuildCompPurchaseOrderBase, IGuildCompPurchaseOrderClient, IGuildCompPurchaseOrderBase, IGuildPurchaseOrderClient, IGuildPurchaseOrderBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderListUpdate;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderCreate;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderModify;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderCommitForSale;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompPurchaseOrderClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderCommitForSale(GuildPurchaseOrderInfo matchInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderCreate(GuildPurchaseOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderListUpdate(List<ProGuildPurchaseOrderListItemInfo> orderList)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderModify(bool isCancel, ulong instanceId, GuildPurchaseOrderInfo modifyOrder)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GuildPurchaseOrderListUpdate>c__AnonStorey0
        {
            internal ProGuildPurchaseOrderListItemInfo item;

            [MethodImpl(0x8000)]
            internal bool <>m__0(GuildPurchaseOrderInfo order)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GuildPurchaseOrderListUpdate>c__AnonStorey1
        {
            internal GuildPurchaseOrderInfo item;

            internal bool <>m__0(ProGuildPurchaseOrderListItemInfo order) => 
                (order.OrderInstanceId == this.item.m_instanceId);
        }
    }
}

