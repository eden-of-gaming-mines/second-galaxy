﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface ILBPlayerContextLBProviderClient : ILBPlayerContextLBProvider
    {
        LogicBlockClientCustomData GetLBClientCustomData();
        LogicBlockSolarSystemPoolCountInfoClient GetLBSolarSystemPoolInfo();
        LogicBlockSpaceSceneClient GetLBSpaceScene();
        LogicBlockCompWormholeClient GetLBWormhole();
    }
}

