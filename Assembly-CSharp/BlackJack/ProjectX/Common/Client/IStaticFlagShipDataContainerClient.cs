﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IStaticFlagShipDataContainerClient : IStaticFlagShipDataContainer, IStaticPlayerShipDataContainer, IFlagShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        void UpdateFlagShipInstanceInfo(GuildStaticFlagShipInstanceInfo info);
    }
}

