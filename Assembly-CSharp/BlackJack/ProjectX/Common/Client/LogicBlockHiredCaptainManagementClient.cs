﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockHiredCaptainManagementClient : LogicBlockHiredCaptainManagementBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_TryCreateStaticShip4Feats;
        private static DelegateBridge __Hotfix_AddExp2Captain;
        private static DelegateBridge __Hotfix_OnSyncCaptainFeats;
        private static DelegateBridge __Hotfix_CaptainInitAndAdditionFeatsUpdate;
        private static DelegateBridge __Hotfix_AddExp2CaptainByItem;
        private static DelegateBridge __Hotfix_GetOneCaptainForTraining;
        private static DelegateBridge __Hotfix_LearnFeatsBook;
        private static DelegateBridge __Hotfix_CaptainRetire;
        private static DelegateBridge __Hotfix_SetCurrShip;
        private static DelegateBridge __Hotfix_UnlockShip;
        private static DelegateBridge __Hotfix_RepairShip;
        private static DelegateBridge __Hotfix_CaptainWingManSet;
        private static DelegateBridge __Hotfix_CaptainWingShipReturn;
        private static DelegateBridge __Hotfix_HiredCaptinAdd;

        [MethodImpl(0x8000)]
        public int AddExp2Captain(LBStaticHiredCaptain captain, long exp)
        {
        }

        [MethodImpl(0x8000)]
        public int AddExp2CaptainByItem(LBStaticHiredCaptain captain, int itemId, long itemCount)
        {
        }

        [MethodImpl(0x8000)]
        public void CaptainInitAndAdditionFeatsUpdate(ulong captainInstanceId, List<IdLevelInfo> additionFeats, List<IdLevelInfo> initFeats)
        {
        }

        [MethodImpl(0x8000)]
        public void CaptainRetire(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public bool CaptainWingManSet(int srcIndex, int destIndex, ulong captainInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool CaptainWingShipReturn(ulong captainInstanceId, bool isDestroyed)
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain GetOneCaptainForTraining()
        {
        }

        [MethodImpl(0x8000)]
        public void HiredCaptinAdd(NpcCaptainInfo captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool LearnFeatsBook(LBStaticHiredCaptain captain, int featsBookItemIndex, List<IdLevelInfo> additionFeats, out List<LBNpcCaptainFeats> newFeatsList, out List<LBNpcCaptainFeats> upgradeFeatsList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncCaptainFeats(ulong captainInstanceId, List<IdLevelInfo> additionFeats, out List<LBNpcCaptainFeats> newFeatsList, out List<LBNpcCaptainFeats> upgradeFeatsList)
        {
        }

        [MethodImpl(0x8000)]
        public void RepairShip(ulong captainInstanceId, int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrShip(ulong captainInstanceId, int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStaticHiredCaptainShipBase TryCreateStaticShip4Feats(int npcCaptainShipId, bool needRepire, LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public void UnlockShip(ulong captainInstanceId, int npcCaptainShipId)
        {
        }
    }
}

