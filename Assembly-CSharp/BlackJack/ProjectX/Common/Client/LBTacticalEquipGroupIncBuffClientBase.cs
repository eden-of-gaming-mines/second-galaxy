﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public abstract class LBTacticalEquipGroupIncBuffClientBase : LBTacticalEquipGroupIncBuffBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, int, bool, uint> EventOnIncBuffAttach;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBTacticalEquipGroupBase> EventOnLastIncBufDetach;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBTacticalEquipGroupBase> EventOnOtherLevelIncBufDetach;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnBufDetach;
        private static DelegateBridge __Hotfix_GetLastIncBufId;
        private static DelegateBridge __Hotfix_add_EventOnIncBuffAttach;
        private static DelegateBridge __Hotfix_remove_EventOnIncBuffAttach;
        private static DelegateBridge __Hotfix_add_EventOnLastIncBufDetach;
        private static DelegateBridge __Hotfix_remove_EventOnLastIncBufDetach;
        private static DelegateBridge __Hotfix_add_EventOnOtherLevelIncBufDetach;
        private static DelegateBridge __Hotfix_remove_EventOnOtherLevelIncBufDetach;

        public event Action<int, int, bool, uint> EventOnIncBuffAttach
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBTacticalEquipGroupBase> EventOnLastIncBufDetach
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBTacticalEquipGroupBase> EventOnOtherLevelIncBufDetach
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupIncBuffClientBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetLastIncBufId()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAttachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBufDetach(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

