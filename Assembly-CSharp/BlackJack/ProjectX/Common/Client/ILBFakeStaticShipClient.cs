﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface ILBFakeStaticShipClient : ILBStaticPlayerShip, ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        ILBStaticPlayerShip GetSourceRealStaticShip();
    }
}

