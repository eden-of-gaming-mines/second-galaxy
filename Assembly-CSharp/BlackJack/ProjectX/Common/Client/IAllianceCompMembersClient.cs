﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IAllianceCompMembersClient : IAllianceCompMembersBase, IAllianceMembersClient, IAllianceMembersBase
    {
    }
}

