﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IGuildItemStoreClient : IGuildItemStoreBase, IGuildItemStoreItemOperationClient, IGuildItemStoreItemOperationBase
    {
    }
}

