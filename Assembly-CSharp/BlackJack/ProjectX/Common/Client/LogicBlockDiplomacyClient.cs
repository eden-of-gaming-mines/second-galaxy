﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockDiplomacyClient : LogicBlockDiplomacyBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget, IFFState> EventIFFSetTarget2Enemy;
        public Dictionary<uint, TargetListViewState> m_targetListViewStateDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PlayerDiplomacyUpdate;
        private static DelegateBridge __Hotfix_GetPlayerDiplomacyFriendPlayerIdList;
        private static DelegateBridge __Hotfix_GetPlayerDiplomacyFriendGuildIdList;
        private static DelegateBridge __Hotfix_GetPlayerDiplomacyFriendAllienceIdList;
        private static DelegateBridge __Hotfix_GetPlayerDiplomacyEnemyPlayerIdList;
        private static DelegateBridge __Hotfix_GetPlayerDiplomacyEnemyGuildIdList;
        private static DelegateBridge __Hotfix_GetPlayerDiplomacyEnemyAllienceIdList;
        private static DelegateBridge __Hotfix_GetTargetListViewStateForTargetInView;
        private static DelegateBridge __Hotfix_UpdateTargetDiplomacyIFFState;
        private static DelegateBridge __Hotfix_RemoveDiplomacyIFFStateByTargetId;
        private static DelegateBridge __Hotfix_CalcTargetListViewState;
        private static DelegateBridge __Hotfix_CalcDiplomacyState;
        private static DelegateBridge __Hotfix_CalcPersonalDiplomacyState;
        private static DelegateBridge __Hotfix_RegEventOnTargetEnterIFF;
        private static DelegateBridge __Hotfix_RegEventOnNeutralTargetChangeToEnemy;
        private static DelegateBridge __Hotfix_UnRegEventOnTargetEnterIFF;
        private static DelegateBridge __Hotfix_UnRegEventOnNeutralTargetChangeToEnemy;
        private static DelegateBridge __Hotfix_OnNeutralTargetChangeToEnemy;
        private static DelegateBridge __Hotfix_OnEventOnTargetEnterIFF;
        private static DelegateBridge __Hotfix_add_EventIFFSetTarget2Enemy;
        private static DelegateBridge __Hotfix_remove_EventIFFSetTarget2Enemy;

        public event Action<ILBSpaceTarget, IFFState> EventIFFSetTarget2Enemy
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public DiplomacyState CalcDiplomacyState(string playerId, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyState CalcPersonalDiplomacyState(string playerUserId, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public TargetListViewState CalcTargetListViewState(ILBSpaceTarget target, IFFState IFFState)
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetPlayerDiplomacyEnemyAllienceIdList()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetPlayerDiplomacyEnemyGuildIdList()
        {
        }

        [MethodImpl(0x8000)]
        public List<string> GetPlayerDiplomacyEnemyPlayerIdList()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetPlayerDiplomacyFriendAllienceIdList()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetPlayerDiplomacyFriendGuildIdList()
        {
        }

        [MethodImpl(0x8000)]
        public List<string> GetPlayerDiplomacyFriendPlayerIdList()
        {
        }

        [MethodImpl(0x8000)]
        public TargetListViewState GetTargetListViewStateForTargetInView(uint targetOjbId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEventOnTargetEnterIFF(ILBSpaceTarget target, IFFState IFFState)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNeutralTargetChangeToEnemy(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayerDiplomacyUpdate(DeplomacyOptType optType, string playerGameUserId, uint guildId, uint allienceId, ushort version)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnNeutralTargetChangeToEnemy(ILBInSpacePlayerShip selfPlayerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnTargetEnterIFF(ILBInSpacePlayerShip selfPlayerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveDiplomacyIFFStateByTargetId(uint targetObjId)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegEventOnNeutralTargetChangeToEnemy(ILBInSpacePlayerShip selfPlayerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegEventOnTargetEnterIFF(ILBInSpacePlayerShip selfPlayerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTargetDiplomacyIFFState(ILBSpaceTarget target, IFFState IFFState)
        {
        }
    }
}

