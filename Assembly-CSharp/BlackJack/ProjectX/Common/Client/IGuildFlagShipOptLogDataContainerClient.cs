﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildFlagShipOptLogDataContainerClient : IGuildFlagShipOptLogDataContainer
    {
        void UpdateGuildFlagShipOptLogInfoList(List<GuildFlagShipOptLogInfo> logInfoList);
    }
}

