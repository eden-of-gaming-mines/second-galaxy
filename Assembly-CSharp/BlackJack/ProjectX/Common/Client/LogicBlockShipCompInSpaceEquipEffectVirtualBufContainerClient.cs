﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompInSpaceEquipEffectVirtualBufContainerClient
    {
        public Action<EquipEffectVirtualBufInfo> EventOnFakeBufAttach;
        public Action<EquipEffectVirtualBufInfo> EventOnFakeBufDetach;
        protected List<EquipEffectVirtualBufInfo> m_removeList;
        protected HashSet<EquipEffectVirtualBufInfo> m_fakeViewBufList;
        protected List<EquipEffectVirtualBufInfo>[] m_bufType2ListArray;
        protected ILBInSpaceShip m_ownerShip;
        private const int ShipFightBufTypeCount = 4;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetEquipEffectVirtualBufList;
        private static DelegateBridge __Hotfix_IsAnyEquipEffectVirtualBufExist;
        private static DelegateBridge __Hotfix_IsExsitVirtualBufForType;
        private static DelegateBridge __Hotfix_IsTargetValied;
        private static DelegateBridge __Hotfix_OnDynamicPropProviderActive;
        private static DelegateBridge __Hotfix_OnInvisiableStart;
        private static DelegateBridge __Hotfix_OnInvisiableEnd;
        private static DelegateBridge __Hotfix_AttachFakeBuf;
        private static DelegateBridge __Hotfix_GetFakeBufLifeType;
        private static DelegateBridge __Hotfix_DetachFakeBuf;

        [MethodImpl(0x8000)]
        protected EquipEffectVirtualBufInfo AttachFakeBuf(ConfigDataEquipEffectVirtualBufInfo configInfo, FakeViewBufLifeType type, TacticalEquipFunctionType functionType, bool isEasyChange = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void DetachFakeBuf(EquipEffectVirtualBufInfo fakeBufInfo)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<EquipEffectVirtualBufInfo> GetEquipEffectVirtualBufList()
        {
        }

        [MethodImpl(0x8000)]
        protected FakeViewBufLifeType GetFakeBufLifeType(TacticalEquipFunctionType type)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip onwerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnyEquipEffectVirtualBufExist(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsExsitVirtualBufForType(ShipFightBufType type, bool isEnhance, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTargetValied(EquipEffectVirtualBufInfo bufInfo, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDynamicPropProviderActive(LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnInvisiableEnd(LBTacticalEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnInvisiableStart(LBTacticalEquipGroupBase equip)
        {
        }
    }
}

