﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ProcessFactory
    {
        private static ProcessFactory m_instance;
        private Dictionary<Type, Stack<LBSpaceProcess>> m_processPoolDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetOrCreateProcess;
        private static DelegateBridge __Hotfix_FreeProcess_0;
        private static DelegateBridge __Hotfix_FreeProcess_1;
        private static DelegateBridge __Hotfix_ClearProcessPool;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        private ProcessFactory()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearProcessPool()
        {
        }

        [MethodImpl(0x8000)]
        public void FreeProcess(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected void FreeProcess<T>(T process) where T: LBSpaceProcess, new()
        {
        }

        [MethodImpl(0x8000)]
        public T GetOrCreateProcess<T>() where T: LBSpaceProcess, new()
        {
        }

        public static ProcessFactory Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

