﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupRecoveryShieldAndAttachBuffByArmorClient : LBTacticalEquipGroupRecoveryShieldAndAttachBuffByArmorBase
    {
        protected LBBufBase m_currBuf;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<uint> EventOnHOTShieldBufAttached;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<uint> EventOnHOTShieldBufDetach;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnEnterTrigerArmorPercent;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_DetachBuf;
        private static DelegateBridge __Hotfix_GetBufIdList4Attach;
        private static DelegateBridge __Hotfix_add_EventOnHOTShieldBufAttached;
        private static DelegateBridge __Hotfix_remove_EventOnHOTShieldBufAttached;
        private static DelegateBridge __Hotfix_add_EventOnHOTShieldBufDetach;
        private static DelegateBridge __Hotfix_remove_EventOnHOTShieldBufDetach;

        public event Action<uint> EventOnHOTShieldBufAttached
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<uint> EventOnHOTShieldBufDetach
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupRecoveryShieldAndAttachBuffByArmorClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void DetachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetBufIdList4Attach()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEnterTrigerArmorPercent()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

