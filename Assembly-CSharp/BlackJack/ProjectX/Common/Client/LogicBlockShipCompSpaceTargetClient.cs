﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompSpaceTargetClient : LogicBlockShipCompSpaceTargetBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcess> EventOnHitByEquip;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcess> EventOnHitByBullet;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcess> EventOnHitByLaser;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcess, int> EventOnHitByDrone;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<TargetNoticeType> EventOnTargetNoticeChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnIsHideInTargetListChanged;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnEnemyDroneFighterAttach;
        private static DelegateBridge __Hotfix_OnHitByBullet;
        private static DelegateBridge __Hotfix_OnHitByLaser;
        private static DelegateBridge __Hotfix_OnHitByDrone;
        private static DelegateBridge __Hotfix_OnHitByEquip;
        private static DelegateBridge __Hotfix_OnMissileInComming;
        private static DelegateBridge __Hotfix_OnAttachBufByEquip;
        private static DelegateBridge __Hotfix_OnDetachBufByEquip;
        private static DelegateBridge __Hotfix_get_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_set_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_IsVisableInTargetListByDefault;
        private static DelegateBridge __Hotfix_get_IsHideInTargetList;
        private static DelegateBridge __Hotfix_set_IsHideInTargetList;
        private static DelegateBridge __Hotfix_add_EventOnHitByEquip;
        private static DelegateBridge __Hotfix_remove_EventOnHitByEquip;
        private static DelegateBridge __Hotfix_add_EventOnHitByBullet;
        private static DelegateBridge __Hotfix_remove_EventOnHitByBullet;
        private static DelegateBridge __Hotfix_add_EventOnHitByLaser;
        private static DelegateBridge __Hotfix_remove_EventOnHitByLaser;
        private static DelegateBridge __Hotfix_add_EventOnHitByDrone;
        private static DelegateBridge __Hotfix_remove_EventOnHitByDrone;
        private static DelegateBridge __Hotfix_add_EventOnTargetNoticeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnTargetNoticeChanged;
        private static DelegateBridge __Hotfix_add_EventOnIsHideInTargetListChanged;
        private static DelegateBridge __Hotfix_remove_EventOnIsHideInTargetListChanged;

        public event Action<LBSpaceProcess> EventOnHitByBullet
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcess, int> EventOnHitByDrone
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcess> EventOnHitByEquip
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcess> EventOnHitByLaser
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnIsHideInTargetListChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TargetNoticeType> EventOnTargetNoticeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool IsVisableInTargetListByDefault()
        {
        }

        [MethodImpl(0x8000)]
        public override ulong OnAttachBufByEquip(int bufId, uint bufLifeTime, float bufInstanceParam1, ILBSpaceTarget srcTarget, EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnDetachBufByEquip(int bufId, ulong instanceid)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnEnemyDroneFighterAttach(LBSpaceProcessDroneFighterLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnHitByBullet(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess bulletFlyProcess)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnHitByDrone(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess, int droneIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnHitByEquip(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnHitByLaser(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isCritical, float damageMulti, LBBulletDamageInfo damageInfo, LBSpaceProcess singleLaserFireProcess)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnMissileInComming(LBSpaceProcessMissileLaunch process)
        {
        }

        public override TargetNoticeType TargetNoticeFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public override bool? IsHideInTargetList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

