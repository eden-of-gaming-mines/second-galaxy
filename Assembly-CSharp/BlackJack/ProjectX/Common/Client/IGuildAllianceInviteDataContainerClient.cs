﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;
    using System.Collections.Generic;

    public interface IGuildAllianceInviteDataContainerClient
    {
        void RefreshAllianceInviteList(List<GuildAllianceInviteInfo> inviteList);
    }
}

