﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupReduceCDByWeaponEquipLaunchClient : LBTacticalEquipGroupReduceCDByWeaponEquipLaunchBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBTacticalEquipGroupBase> EventOnReduceCD;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventGroupCD;
        private static DelegateBridge __Hotfix_add_EventOnReduceCD;
        private static DelegateBridge __Hotfix_remove_EventOnReduceCD;

        public event Action<LBTacticalEquipGroupBase> EventOnReduceCD
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupReduceCDByWeaponEquipLaunchClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventGroupCD(uint groupCDEndTime)
        {
        }
    }
}

