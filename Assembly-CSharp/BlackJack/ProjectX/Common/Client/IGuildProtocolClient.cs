﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildProtocolClient
    {
        void OnGuildMainPanelInfoAck(GuildBasicInfo basicInfo, GuildSimpleRuntimeInfo simpleRuntimeInfo, GuildDynamicInfo dynamicInfo, List<GuildMomentsInfo> momentsBriefInfos, uint momentsBriefInfoVersion);
    }
}

