﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;

    public enum GuildFleetOperationType
    {
        SetFormationType,
        SetFormationActive,
        SetAllowToJoinFleetManual,
        KickOut,
        SetCommander,
        SetInternalPosition,
        Retire,
        TransferPosition,
        MembersJumping,
        MembersUseStargate,
        FireFocusTarget,
        ProtectedTarget
    }
}

