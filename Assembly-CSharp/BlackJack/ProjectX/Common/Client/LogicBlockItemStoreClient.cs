﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockItemStoreClient : LogicBlockItemStoreBase
    {
        protected IItemStoreRecommendMarkDataContainer m_itemStoreRecommendMarkDC;
        protected readonly List<StoreItemRecommendMarkInfo> m_removeItemRecommedMarkInfoList;
        protected readonly List<ItemInfo> m_newGetItemCacheList;
        public DateTime LastItemStoreUpdateTime;
        public DateTime m_lastTickTimeForFreezingItem;
        public const int TickPeriodForFreezingItem = 0x1388;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<StoreItemType, int> EventOnFreezingItemTimeout;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_AddItemInMSStoreAutoOverlay;
        private static DelegateBridge __Hotfix_UpdateItemByStoreItemInfo_1;
        private static DelegateBridge __Hotfix_UpdateItemByStoreItemInfo_0;
        private static DelegateBridge __Hotfix_RemoveMSStoreItemByIndex_1;
        private static DelegateBridge __Hotfix_RemoveMSStoreItemByIndex_0;
        private static DelegateBridge __Hotfix_RemoveMSStoreItem_1;
        private static DelegateBridge __Hotfix_RemoveMSStoreItemById;
        private static DelegateBridge __Hotfix_RemoveMSStoreItem_0;
        private static DelegateBridge __Hotfix_SaleMSStoreItemByIndex;
        private static DelegateBridge __Hotfix_SaleMSStoreItemListByIndex;
        private static DelegateBridge __Hotfix_SuspendItem;
        private static DelegateBridge __Hotfix_ResumeItem;
        private static DelegateBridge __Hotfix_GetFilteredItemList;
        private static DelegateBridge __Hotfix_GetItemList;
        private static DelegateBridge __Hotfix_GetFristExploreTicket;
        private static DelegateBridge __Hotfix_TickForFreezingItem;
        private static DelegateBridge __Hotfix_OnGetNewItem;
        private static DelegateBridge __Hotfix_GetImportantWeaponEquipRecommendMarkInfo;
        private static DelegateBridge __Hotfix_GetImportantChipRecommendMarkInfo;
        private static DelegateBridge __Hotfix_HasAnyRecommendStoreItemForCertainItem;
        private static DelegateBridge __Hotfix_IsStoreItemAlreadyHaveRecommendMark;
        private static DelegateBridge __Hotfix_AddStoreItemRecommendMark;
        private static DelegateBridge __Hotfix_SetStoreItemRecommendMarkVisited;
        private static DelegateBridge __Hotfix_SetAllRecommendMarkVisitedForCertainItem;
        private static DelegateBridge __Hotfix_RefreshRecommendMarkInfo;
        private static DelegateBridge __Hotfix_GetNewGetItemInfos;
        private static DelegateBridge __Hotfix_ClearNewGetItemInfos;
        private static DelegateBridge __Hotfix_CreateLBStoreItemByItemInfo;
        private static DelegateBridge __Hotfix_AddStoreItemDirectly;
        private static DelegateBridge __Hotfix_ReInitStoreItem_1;
        private static DelegateBridge __Hotfix_ReInitStoreItem_0;
        private static DelegateBridge __Hotfix_add_EventOnFreezingItemTimeout;
        private static DelegateBridge __Hotfix_remove_EventOnFreezingItemTimeout;

        public event Action<StoreItemType, int> EventOnFreezingItemTimeout
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool AddItemInMSStoreAutoOverlay(int itemIndex, ulong instanceId, StoreItemType itemType, int itemConfigId, bool isBind, long count, ItemUpdateReason updateReason = 0)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem AddStoreItemDirectly(StoreItemType itemType, int itemConfigId, long count, bool isBind = true, ItemSuspendReason suspendReason = 0, int index = -1, ulong instanceId = 0UL)
        {
        }

        [MethodImpl(0x8000)]
        public void AddStoreItemRecommendMark(StoreItemType itemType, int configId, bool isBind)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearNewGetItemInfos()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStoreItem CreateLBStoreItemByItemInfo(StoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStoreItemClient> GetFilteredItemList(List<Func<ILBStoreItemClient, bool>> filterlist)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStoreItemClient GetFristExploreTicket()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemRecommendMarkInfo GetImportantChipRecommendMarkInfo()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemRecommendMarkInfo GetImportantWeaponEquipRecommendMarkInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItemClient> GetItemList()
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetNewGetItemInfos()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyRecommendStoreItemForCertainItem(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStoreItemAlreadyHaveRecommendMark(StoreItemType itemType, int configId, bool isBind, bool ingoreCheckVisit = false)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGetNewItem(ItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshRecommendMarkInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem ReInitStoreItem(int index, ItemInfo itemInfo, bool isDelete, uint flag)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem ReInitStoreItem(int index, StoreItemType itemType, int itemConfigId, long count, bool isBind, bool isFreezing, ulong instanceId, uint flag)
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveMSStoreItem(LBStoreItem item, long count)
        {
        }

        [MethodImpl(0x8000)]
        public override bool RemoveMSStoreItem(StoreItemType itemType, int itemId, long itemCount, out int errCode, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveMSStoreItemById(StoreItemType itemType, int itemId, long count)
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveMSStoreItemByIndex(int index, long count)
        {
        }

        [MethodImpl(0x8000)]
        public override bool RemoveMSStoreItemByIndex(int index, long count, out int errCode, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool ResumeItem(LBStoreItem from, long count, int resumedItemIndex, ulong instanceId, out LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        public bool SaleMSStoreItemByIndex(int index, long count, out double price)
        {
        }

        [MethodImpl(0x8000)]
        public bool SaleMSStoreItemListByIndex(List<KeyValuePair<int, long>> storeItemIndexCountList, out double price)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllRecommendMarkVisitedForCertainItem(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStoreItemRecommendMarkVisited(StoreItemType itemType, int configId, bool isBind)
        {
        }

        [MethodImpl(0x8000)]
        public bool SuspendItem(LBStoreItem from, long count, ItemSuspendReason reason, int suspendedItemIndex, ulong instanceId, out LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        public void TickForFreezingItem(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected bool UpdateItemByStoreItemInfo(StoreItemInfo itemInfo, out LBStoreItem destItem)
        {
        }

        [MethodImpl(0x8000)]
        public bool UpdateItemByStoreItemInfo(StoreItemUpdateInfo itemUpdateInfo, ItemUpdateReason updateReason = 0)
        {
        }

        [Flags]
        public enum ItemUpdateReason
        {
            Default,
            GetNewItem
        }
    }
}

