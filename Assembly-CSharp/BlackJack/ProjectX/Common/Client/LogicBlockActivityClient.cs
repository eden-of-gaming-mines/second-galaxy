﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockActivityClient : LogicBlockActivityBase
    {
        public List<ActivityInfo> m_activityList;
        private List<int> m_activityRewardStateList;
        private List<int> m_activityRewardNeedVitalityList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_VitalityGetReward;
        private static DelegateBridge __Hotfix_OnActivityInfoNtf;
        private static DelegateBridge __Hotfix_OnPlayerActivityInfoNtf;
        private static DelegateBridge __Hotfix_ExistUnreceivedReward;
        private static DelegateBridge __Hotfix_SetActivityRewardStateList;

        [MethodImpl(0x8000)]
        public bool ExistUnreceivedReward()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void OnActivityInfoNtf(List<ActivityInfo> activityList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlayerActivityInfoNtf(List<PlayerActivityInfo> playerActivityList, int activityPoint, int vitality)
        {
        }

        [MethodImpl(0x8000)]
        private void SetActivityRewardStateList(int vitality)
        {
        }

        [MethodImpl(0x8000)]
        public void VitalityGetReward(int index)
        {
        }
    }
}

