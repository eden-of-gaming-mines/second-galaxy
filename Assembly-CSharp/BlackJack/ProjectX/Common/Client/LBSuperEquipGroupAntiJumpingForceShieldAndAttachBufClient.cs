﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperEquipGroupAntiJumpingForceShieldAndAttachBufClient : LBSuperEquipGroupAntiJumplingForceShieldAndAttachBufBase, IEquipFunctionCompInOutRangeTriggerOwnerClient, IEquipFunctionCompInOutRangeTriggerOwnerBase, ILBSpaceProcessEquipLaunchSource, IEquipFunctionCompOwner, ILBSpaceProcessSource
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffect;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessFire;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessCancel;
        private static DelegateBridge __Hotfix_OnProcessEffect;
        private static DelegateBridge __Hotfix_CreateFuncComp;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_add_EventOnStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnProcessFire;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFire;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_add_EventOnProcessEffect;
        private static DelegateBridge __Hotfix_remove_EventOnProcessEffect;

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupAntiJumpingForceShieldAndAttachBufClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected override EquipFunctionCompInOutRangeTriggerBase CreateFuncComp()
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessEquipAntiJumpingForceShieldAndAttachBufLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventSuperEquipEnd()
        {
        }
    }
}

