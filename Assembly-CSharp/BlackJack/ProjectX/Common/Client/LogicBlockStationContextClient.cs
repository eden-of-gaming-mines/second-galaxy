﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockStationContextClient : LogicBlockStationContextBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateNpcTalker;
        private static DelegateBridge __Hotfix_OnStationDynamicNpcTalkerStateUpdate;
        private static DelegateBridge __Hotfix_NpcDialogStart;
        private static DelegateBridge __Hotfix_NpcDialogNext;

        [MethodImpl(0x8000)]
        protected override LBNpcTalkerBase CreateNpcTalker(ConfigDataNpcTalkerInfo npcTalkerInfo, bool isGlobalNpc)
        {
        }

        [MethodImpl(0x8000)]
        public void NpcDialogNext(int npcId, NpcDialogInfo dialog)
        {
        }

        [MethodImpl(0x8000)]
        public void NpcDialogStart(int npcId, NpcDialogInfo dialog)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStationDynamicNpcTalkerStateUpdate(List<StationDynamicNpcTalkerStateInfo> stateList)
        {
        }
    }
}

