﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockProduceClient : LogicBlockProduceBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ProductionLineStart;
        private static DelegateBridge __Hotfix_ProductionLineCancel;
        private static DelegateBridge __Hotfix_ProductionLineSpeedUp;
        private static DelegateBridge __Hotfix_ProductionLineSpeedUpByRealMoney;
        private static DelegateBridge __Hotfix_ProductionLineCompleteConfirm;

        [MethodImpl(0x8000)]
        public bool ProductionLineCancel(int lineIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool ProductionLineCompleteConfirm(int lineIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool ProductionLineSpeedUp(int lineIndex, int itemId, int count, DateTime newEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool ProductionLineSpeedUpByRealMoney(int lineIndex, int realMoneyCost)
        {
        }

        [MethodImpl(0x8000)]
        public bool ProductionLineStart(int blueprintIndexId, ProductionLineInfo info)
        {
        }
    }
}

