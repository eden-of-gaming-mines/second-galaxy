﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildCompensationClient : IGuildCompensationBase
    {
        void RefreshCompensationList(List<LossCompensation> compensations, int version);
    }
}

