﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildSolarSystemInfoClient : GuildSolarSystemInfo
    {
        public DateTime m_basicStaticInfoLastUpdateTime;
        public OccupyGuildInfo m_occupyGuildInfo;
        public DateTime m_buildingInfoLastUpdateTime;
        public List<ProGuildBuildingExInfo> m_buildingExInfos;
        public DateTime m_battleInfoLastUpdateTime;
        public List<ProGuildBattleClientSyncInfo> m_battleInfos;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetBuildingExInfoByBuildingInstanceId;
        private static DelegateBridge __Hotfix_GetBattleInfoByBattleInstanceId;
        private static DelegateBridge __Hotfix_GetSovereignBattleSimpleInfo;
        private static DelegateBridge __Hotfix_GetBuildingBattleInfoByBuildingInstanceId;
        private static DelegateBridge __Hotfix_IsInSovereignBattle;

        [MethodImpl(0x8000)]
        public ProGuildBattleClientSyncInfo GetBattleInfoByBattleInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBuildingBattleClientSyncInfo GetBuildingBattleInfoByBuildingInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public ProGuildBuildingExInfo GetBuildingExInfoByBuildingInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public SovereignBattleSimpleInfo GetSovereignBattleSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInSovereignBattle()
        {
        }

        public class OccupyGuildInfo
        {
            public uint m_guildId;
            public string m_guildCodeName;
            public string m_guildName;
            public string m_occupiedGuildManifesto;
            public GuildLogoInfo m_guildLogoInfo;
            public bool m_isOccupiedByNpc;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

