﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompSolarSystemCtxClient : GuildCompSolarSystemCtxBase, IGuildCompSolarSystemCtxClient, IGuildCompSolarSystemCtxBase, IGuildSolarSystemCtxClient, IGuildSolarSystemCtxBase
    {
        protected IGuildClientCompOwner m_owner;
        private IGuildCompPropertiesCalculaterClient m_compPropertiesCalculater;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_OnGuildOccupiedSolarSystemInfoAck;
        private static DelegateBridge __Hotfix_OnGuildBuildingDeployUpgradeAck;
        private static DelegateBridge __Hotfix_OnGuildBuildingRecycleAck;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemInfoAck;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompSolarSystemCtxClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBuildingDeployUpgradeAck(GuildBuildingDeployUpgradeAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBuildingRecycleAck(GuildBuildingRecycleAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildOccupiedSolarSystemInfoAck(GuildOccupiedSolarSystemInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSolarSystemInfoAck(GuildSolarSystemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitlialize()
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

