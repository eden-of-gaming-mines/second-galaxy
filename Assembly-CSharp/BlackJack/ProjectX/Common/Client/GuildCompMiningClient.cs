﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompMiningClient : GuildCompMiningBase, IGuildCompMiningClient, IGuildCompMiningBase, IGuildMiningClient, IGuildMiningBase
    {
        protected IGuildClientCompOwner m_owner;
        private DateTime m_lastMiningBalanceTimeRefreashTime;
        private const int MiningBalanceTimeRefreashCD = 60;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckMiningBalanceTimeRefreashCD;
        private static DelegateBridge __Hotfix_UpdateLastMiningBalanceTimeRefreashTime;
        private static DelegateBridge __Hotfix_GuildMiningBalanceTimeReset;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompMiningClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckMiningBalanceTimeRefreashCD(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildMiningBalanceTimeReset(int hour, int minute)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLastMiningBalanceTimeRefreashTime(DateTime currTime)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

