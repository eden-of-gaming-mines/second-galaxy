﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildCompMomentsClient : IGuildCompMomentsBase, IGuildMomentsClient, IGuildMomentsBase
    {
        void UpdateGuildMomentsBriefInfoList(List<GuildMomentsInfo> momentsBriefInfoList, uint momentsBriefInfoVersion);
    }
}

