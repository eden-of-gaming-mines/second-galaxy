﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompStaticWeaponEquipClient4FlagShip : LogicBlockShipCompStaticWeaponEquipClient
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_DestoryAllShipEquip;
        private static DelegateBridge __Hotfix_InitFakeInSpaceWeaponEquipGroups;

        [MethodImpl(0x8000)]
        public override void DestoryAllShipEquip(OperateLogItemChangeType operateLogCauseId = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitFakeInSpaceWeaponEquipGroups()
        {
        }
    }
}

