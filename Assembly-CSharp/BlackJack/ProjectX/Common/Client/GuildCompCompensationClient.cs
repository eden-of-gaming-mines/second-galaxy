﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompCompensationClient : GuildCompCompensationBase, IGuildCompCompensationClient, IGuildCompCompensationBase, IGuildCompensationClient, IGuildCompensationBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RefreshCompensationList;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompCompensationClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshCompensationList(List<LossCompensation> compensations, int version)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

