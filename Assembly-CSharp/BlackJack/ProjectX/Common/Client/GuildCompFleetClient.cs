﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompFleetClient : GuildCompFleetBase, IGuildCompFleetClient, IGuildCompFleetBase, IGuildFleetClient, IGuildFleetBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberInfo;
        private static DelegateBridge __Hotfix_CheckForFleetCreating;
        private static DelegateBridge __Hotfix_CheckForFleetKickOut;
        private static DelegateBridge __Hotfix_CheckForGuildFleetSetInternalPositions;

        [MethodImpl(0x8000)]
        public GuildCompFleetClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckForFleetCreating(string gameUserId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckForFleetKickOut(string srcGameUserId, ulong fleetId, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckForGuildFleetSetInternalPositions(string gameUserId, ulong fleetId, GuildFleetMemberInfo member, bool needCheckPermission, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo GetGuildFleetMemberInfo(ulong fleetInsId, string gameUserId)
        {
        }
    }
}

