﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildFlagShipHangarListDataContainerClient : IGuildFlagShipHangarListDataContainer
    {
        bool AddGuildFlagShipHangarInfo(GuildFlagShipHangarDetailInfo info);
    }
}

