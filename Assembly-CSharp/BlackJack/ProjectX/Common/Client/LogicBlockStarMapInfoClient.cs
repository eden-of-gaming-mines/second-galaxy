﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockStarMapInfoClient
    {
        protected ILBPlayerContextClient m_playerCtx;
        protected LBGalaxyGuildOccupyClient m_occupyInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetVirtualBuffsFromSolarSystem;
        private static DelegateBridge __Hotfix_UpdateGalaxyGuildOccupyInfo;
        private static DelegateBridge __Hotfix_CalcLocationItemInfos;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForDailyExtraRewardQuest;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForQuestPoolCount;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForDelegateMissionPoolCount;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForSolarSystemDelegateMissionRewardMulti;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForSpaceSignal;

        [MethodImpl(0x8000)]
        public Dictionary<int, LocationItemInfo> CalcLocationItemInfos(Dictionary<int, LocationItemInfo> locationUIInfoDict, bool displayTradeShip, bool displayFlagShipHangar)
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForDailyExtraRewardQuest()
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForDelegateMissionPoolCount(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForQuestPoolCount(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public VirtualBuffDesc GetVirtualBuffDescForSolarSystemDelegateMissionRewardMulti(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForSpaceSignal(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> GetVirtualBuffsFromSolarSystem(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGalaxyGuildOccupyInfo(StarMapGuildOccupyStaticInfo info)
        {
        }

        public class LocationItemInfo
        {
            public int m_solarSystemId;
            public bool m_isMotherShipPos;
            public int m_playerCount;
            public bool m_isSelfShipHere;
            public bool m_rescueExistForSelf;
            public bool m_rescueExistForAlliance;
            public bool m_emegencySignalExist;
            public bool m_isGuildBase;
            public bool m_isTeamMember;
            public List<string> m_teamMemberGameUserId;
            public List<KeyValuePair<ulong, int>> m_tradeShipList;
            public ulong m_flagShipHangarInstId;
            public float m_uiLayerPositionX;
            public float m_uiLayerPositionY;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsExistShipOrMotherShipInfo;

            [MethodImpl(0x8000)]
            public bool IsExistShipOrMotherShipInfo()
            {
            }
        }
    }
}

