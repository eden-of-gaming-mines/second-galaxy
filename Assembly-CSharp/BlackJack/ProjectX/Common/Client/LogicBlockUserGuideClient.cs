﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ToolUtil;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockUserGuideClient : LogicBlockUserGuideBase
    {
        protected UserGuideManager m_userGuideManager;
        protected const int QuestionnaireSavePointStepId = 0x18;
        protected const int AppScoreFirstCheckPointStepId = 0x1a;
        protected const int AppScoreSecondCheckPointStepId = 0x1b;
        protected const int AppScoreCompleteStepId = 0x1c;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CheckStep;
        private static DelegateBridge __Hotfix_OnStepStart;
        private static DelegateBridge __Hotfix_OnStepEnd;
        private static DelegateBridge __Hotfix_ClearCurrGroup;
        private static DelegateBridge __Hotfix_SkipStep;
        private static DelegateBridge __Hotfix_SetupDebugState;
        private static DelegateBridge __Hotfix_SetCurrStepInfoForTest;
        private static DelegateBridge __Hotfix_GetStepInfo;
        private static DelegateBridge __Hotfix_IsStepGroupAlreadyCompleted;
        private static DelegateBridge __Hotfix_ResetGroup2NotComplete;
        private static DelegateBridge __Hotfix_IsQuestionnaireComplete;
        private static DelegateBridge __Hotfix_GetQuestionnaireSavePointStepId;
        private static DelegateBridge __Hotfix_IsAppScoreFirstCheckPointComplete;
        private static DelegateBridge __Hotfix_GetAppScoreFirstCheckPointStepId;
        private static DelegateBridge __Hotfix_IsAppScoreSecondCheckPointComplete;
        private static DelegateBridge __Hotfix_GetAppScoreSecondCheckPointStepId;
        private static DelegateBridge __Hotfix_IsAppScoreComplete;
        private static DelegateBridge __Hotfix_GetAppScoreCompleteStepId;

        [MethodImpl(0x8000)]
        public bool CheckStep(int stepId, out bool isGroupSaved, out int savedGroupId)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearCurrGroup()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAppScoreCompleteStepId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAppScoreFirstCheckPointStepId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAppScoreSecondCheckPointStepId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestionnaireSavePointStepId()
        {
        }

        [MethodImpl(0x8000)]
        public IUserGuideStepInfo GetStepInfo(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAppScoreComplete()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAppScoreFirstCheckPointComplete()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAppScoreSecondCheckPointComplete()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestionnaireComplete()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsStepGroupAlreadyCompleted(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStepEnd(out bool groupSaved)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnStepStart(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetGroup2NotComplete(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrStepInfoForTest(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetupDebugState(int stepId)
        {
        }

        [MethodImpl(0x8000)]
        public void SkipStep(int stepId)
        {
        }
    }
}

