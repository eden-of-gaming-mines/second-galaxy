﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    internal class ClientStoreItemEquipHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetEquipFunctionType_0;
        private static DelegateBridge __Hotfix_GetEquipFunctionType_1;
        private static DelegateBridge __Hotfix_GetAssembleTechRequirement_0;
        private static DelegateBridge __Hotfix_GetAssembleTechRequirement_1;
        private static DelegateBridge __Hotfix_GetAssembleCPUCost_0;
        private static DelegateBridge __Hotfix_GetAssembleCPUCost_1;
        private static DelegateBridge __Hotfix_GetAssemblePowerCost_0;
        private static DelegateBridge __Hotfix_GetAssemblePowerCost_1;
        private static DelegateBridge __Hotfix_GetSlotTypeStringKey_0;
        private static DelegateBridge __Hotfix_GetSlotTypeStringKey_1;
        private static DelegateBridge __Hotfix_GetMaximumFittingCount_0;
        private static DelegateBridge __Hotfix_GetEquipFunctionTypeStringKey;
        private static DelegateBridge __Hotfix_IsActiveEquip;
        private static DelegateBridge __Hotfix_GetMaximumFittingCount_1;
        private static DelegateBridge __Hotfix_IsShowEquipRange_0;
        private static DelegateBridge __Hotfix_IsShowEquipRange_1;
        private static DelegateBridge __Hotfix_GetEquipRangeTitleStr_0;
        private static DelegateBridge __Hotfix_GetEquipRangeTitleStr_1;
        private static DelegateBridge __Hotfix_GetLaunchEnergyCost_0;
        private static DelegateBridge __Hotfix_GetLaunchEnergyCost_1;

        [MethodImpl(0x8000)]
        public static uint GetAssembleCPUCost(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetAssembleCPUCost(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetAssemblePowerCost(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetAssemblePowerCost(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetAssembleTechRequirement(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetAssembleTechRequirement(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static EquipFunctionType GetEquipFunctionType(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static EquipFunctionType GetEquipFunctionType(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipFunctionTypeStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipRangeTitleStr(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipRangeTitleStr(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetLaunchEnergyCost(ILBStoreItemClient item, out bool isPercentage)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetLaunchEnergyCost(StoreItemType itemType, object confInfo, out bool isPercentage)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetMaximumFittingCount(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetMaximumFittingCount(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSlotTypeStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSlotTypeStringKey(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsActiveEquip(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsShowEquipRange(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsShowEquipRange(StoreItemType itemType, object confInfo)
        {
        }
    }
}

