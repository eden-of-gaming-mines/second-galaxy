﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompFakeWeaponAmmoSetup4PlayerShipClient : LogicBlockShipCompWeaponAmmoSetup4PlayerShipClient
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWeaponAmmoUpdating;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ReloadAmmo2SlotGroup;
        private static DelegateBridge __Hotfix_RemoveAmmoFromSlotGroup;
        private static DelegateBridge __Hotfix_add_EventOnWeaponAmmoUpdating;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponAmmoUpdating;

        public event Action EventOnWeaponAmmoUpdating
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool ReloadAmmo2SlotGroup(LBStaticWeaponEquipSlotGroup group, StoreItemType itemType, int itemConfigId, int itemCount)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveAmmoFromSlotGroup(LBStaticWeaponEquipSlotGroup group)
        {
        }
    }
}

