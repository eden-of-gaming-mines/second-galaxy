﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildFlagShipClient : IGuildFlagShipBase
    {
        void AddOrUpdateGuildFlagShipHangarInfo(ProGuildFlagShipHangarDetailInfo shipHangarDetailInfo, string hangarLocker);
        bool PackShipToItemStore(ulong shipHangarInsId, int hangarIndex, int resumeStoreItemIndex, ulong resumeItemInsId, List<StoreItemTransformInfo> resumedShipEquipInfoList, out LBStoreItem toItem);
        bool UnpackShipFromItemStore(ulong shipHangarInsId, ulong shipPackedItemInsId, int hangarIndex, ulong shipUnpackedItemInsId, int unpackedItemIndex, DateTime currTime);
        void UpdateGuildFlagShipOptLogInfoList(List<GuildFlagShipOptLogInfo> logInfoList);
    }
}

