﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompFlagShipClient : GuildCompFlagShipBase, IGuildCompFlagShipClient, IGuildCompFlagShipBase, IGuildFlagShipClient, IGuildFlagShipBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UnpackShipFromItemStore;
        private static DelegateBridge __Hotfix_PackShipToItemStore;
        private static DelegateBridge __Hotfix_AddOrUpdateGuildFlagShipHangarInfo;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipOptLogInfoList;
        private static DelegateBridge __Hotfix_ChangeFlagShipFuel;
        private static DelegateBridge __Hotfix_GetFlagShipDriverInfo_0;
        private static DelegateBridge __Hotfix_GetFlagShipDriverInfo_1;
        private static DelegateBridge __Hotfix_CreateLBStaticPlayerShip;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompFlagShipClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AddOrUpdateGuildFlagShipHangarInfo(ProGuildFlagShipHangarDetailInfo proShipHangarDetailInfo, string hangarLocker)
        {
        }

        [MethodImpl(0x8000)]
        public bool ChangeFlagShipFuel(ulong shipHangarInsId, int slotIndex, ulong fuelInstanceId, int fuelIndex, int fuelSupplementCount, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected override ILBStaticFlagShip CreateLBStaticPlayerShip(IStaticFlagShipDataContainer shipDC, int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, ulong shipInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool PackShipToItemStore(ulong shipHangarInsId, int hangarIndex, int resumeStoreItemIndex, ulong resumeItemInsId, List<StoreItemTransformInfo> resumedShipEquipInfoList, out LBStoreItem toItem)
        {
        }

        [MethodImpl(0x8000)]
        public bool UnpackShipFromItemStore(ulong shipHangarInsId, ulong shipPackedItemInsId, int hangarIndex, ulong shipUnpackedItemInsId, int unpackedItemIndex, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFlagShipOptLogInfoList(List<GuildFlagShipOptLogInfo> logInfoList)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <AddOrUpdateGuildFlagShipHangarInfo>c__AnonStorey0
        {
            internal GuildFlagShipHangarDetailInfo shipHangarDetailInfo;

            [MethodImpl(0x8000)]
            internal bool <>m__0(IGuildFlagShipHangarDataContainer elem)
            {
            }
        }
    }
}

