﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using System;

    public interface IGuildMemberListClient : IGuildMemberListBase
    {
        GuildMemberInfo GetMemberInfo(string gameUserId);
        void OnGuildLeaderTransferAbortAck();
        void OnGuildMemberJobUpdateAck(GuildJobType job, string playerGameUserId, bool isAppoint, DateTime serverTime);
        void OnGuildMemberListInfoAck(GuildMemberListInfoAck ack);
        void OnRemoveMember(string gameUserId);
        void OnSelfLeaveGuild(string gameUserId);
    }
}

