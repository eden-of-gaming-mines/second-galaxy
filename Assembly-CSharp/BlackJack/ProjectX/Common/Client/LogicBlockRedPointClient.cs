﻿namespace BlackJack.ProjectX.Common.Client
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockRedPointClient
    {
        protected ILBPlayerContextClient m_lbPlayerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsBranchStoryNeedRedPoint;
        private static DelegateBridge __Hotfix_SetBranchStoryProcessQuestId;
        private static DelegateBridge __Hotfix_GetBranchStoryTipRedPointCount;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForGuildJoinApply;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForGuildMessage;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForGuildShowDownTime;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForGuildBenefits;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForAllianceInvite;
        private static DelegateBridge __Hotfix_SetGuildMessageVersionData;
        private static DelegateBridge __Hotfix_set_RedPointGuildJoinApplyCount;
        private static DelegateBridge __Hotfix_set_RedPointGuildBenefits;
        private static DelegateBridge __Hotfix_set_RedPointGuildAllianceInviteCount;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForDailyLogin;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForCMDailySign;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForCommanderAuth;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForGuild;
        private static DelegateBridge __Hotfix_IsRedPointVisibleForRechargeMonthCard;

        [MethodImpl(0x8000)]
        public int GetBranchStoryTipRedPointCount(List<KeyValuePair<string, long>> keyValueList)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContextClient lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBranchStoryNeedRedPoint(string key, long currProcessQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForAllianceInvite()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForCMDailySign()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForCommanderAuth()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForDailyLogin()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForGuild()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForGuildBenefits()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForGuildJoinApply()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForGuildMessage()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForGuildShowDownTime()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRedPointVisibleForRechargeMonthCard()
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBranchStoryProcessQuestId(string key, long currProcessQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildMessageVersionData()
        {
        }

        public int RedPointGuildJoinApplyCount
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool RedPointGuildBenefits
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int RedPointGuildAllianceInviteCount
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

