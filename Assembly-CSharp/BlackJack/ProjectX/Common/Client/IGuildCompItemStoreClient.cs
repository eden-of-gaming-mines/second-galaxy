﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IGuildCompItemStoreClient : IGuildCompItemStoreBase, IGuildItemStoreClient, IGuildItemStoreBase, IGuildItemStoreItemOperationBase, IGuildItemStoreItemOperationClient
    {
    }
}

