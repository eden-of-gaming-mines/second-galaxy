﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupAttachBuf2SelfByDodgeClient : LBTacticalEquipGroupAttachBuf2SelfByDodgeBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<uint, LBTacticalEquipGroupBase> EventOnAttachBuf;
        protected LBBufBase m_currBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnBulletHitMiss;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnDetachBuf;
        private static DelegateBridge __Hotfix_add_EventOnAttachBuf;
        private static DelegateBridge __Hotfix_remove_EventOnAttachBuf;

        public event Action<uint, LBTacticalEquipGroupBase> EventOnAttachBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupAttachBuf2SelfByDodgeClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletHitMiss(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDetachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

