﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupAttachBufBySupperEnergyFullClient : LBTacticalEquipGroupAttachBufBySupperEnergyFullBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnEquipEffectStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEquipEffectEnd;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnBufDetach;
        private static DelegateBridge __Hotfix_add_EventOnEquipEffectStart;
        private static DelegateBridge __Hotfix_remove_EventOnEquipEffectStart;
        private static DelegateBridge __Hotfix_add_EventOnEquipEffectEnd;
        private static DelegateBridge __Hotfix_remove_EventOnEquipEffectEnd;

        public event Action EventOnEquipEffectEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEquipEffectStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupAttachBufBySupperEnergyFullClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBufDetach(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

