﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBEquipGroupBlinkClient : LBEquipGroupBlinkBase, IEquipFunctionCompBlinkOwnerClient, IEquipFunctionCompBlinkOwnerBase, ILBSpaceProcessEquipLaunchSource, IEquipFunctionCompOwner, ILBSpaceProcessSource
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBEquipGroupBase, StructProcessEquipLaunchBase> EventOnStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffect;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventOnBlinkComplete;
        private static DelegateBridge __Hotfix_CreateFuncComp;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessFire;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessCancel;
        private static DelegateBridge __Hotfix_OnProcessEffect;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_add_EventOnStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnProcessFire;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFire;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_add_EventOnProcessEffect;
        private static DelegateBridge __Hotfix_remove_EventOnProcessEffect;

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBEquipGroupBase, StructProcessEquipLaunchBase> EventOnStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBlinkClient(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected override EquipFunctionCompBlinkBase CreateFuncComp()
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessEquipBlinkLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventOnBlinkComplete(bool blinkSuccessful)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

