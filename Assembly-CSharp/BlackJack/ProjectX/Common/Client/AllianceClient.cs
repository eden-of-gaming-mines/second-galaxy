﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class AllianceClient : AllianceBase, IAllianceCompOwnerClient, IAllianceBasicClient, IAllianceChatClient, IAllianceInviteClient, IAllianceMailClient, IAllianceMembersClient, IAllianceCompOwnerBase, IAllianceBasicBase, IAllianceChatBase, IAllianceInviteBase, IAllianceMailBase, IAllianceMembersBase
    {
        private readonly AllianceDCBasicClient m_dcBasic;
        private readonly AllianceDCInviteClient m_dcInvite;
        private readonly AllianceDCMembersClient m_dcMember;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Refresh;
        private static DelegateBridge __Hotfix_RefreshMemberList;
        private static DelegateBridge __Hotfix_InitAllComps;
        private static DelegateBridge __Hotfix_get_CompBasicClient;
        private static DelegateBridge __Hotfix_AllianceNameSet;
        private static DelegateBridge __Hotfix_AllianceLogoSet;
        private static DelegateBridge __Hotfix_AllianceAnnouncementSet;
        private static DelegateBridge __Hotfix_AllianceRegionSet;
        private static DelegateBridge __Hotfix_get_CompChatClient;
        private static DelegateBridge __Hotfix_ChatInAlliance;
        private static DelegateBridge __Hotfix_SystemChatInAlliance;
        private static DelegateBridge __Hotfix_get_CompInviteClient;
        private static DelegateBridge __Hotfix_AllianceInvite;
        private static DelegateBridge __Hotfix_get_CompMailClient;
        private static DelegateBridge __Hotfix_get_CompMembersClient;
        private static DelegateBridge __Hotfix_AllianceMemberRemove;
        private static DelegateBridge __Hotfix_AllianceLeaderTransfer;

        [MethodImpl(0x8000)]
        public AllianceClient(AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceAnnouncementSet(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceInvite(AllianceInviteInfo invite)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceLeaderTransfer(uint srcGuildId, uint destGuildId)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceLogoSet(AllianceLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceMemberRemove(uint removedGuildId)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceNameSet(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceRegionSet(int languageType)
        {
        }

        [MethodImpl(0x8000)]
        public bool ChatInAlliance(PlayerSimplestInfo playerInfo, string chatContent, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool InitAllComps()
        {
        }

        [MethodImpl(0x8000)]
        public void Refresh(AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshMemberList(List<AllianceMemberInfo> memberList)
        {
        }

        [MethodImpl(0x8000)]
        public bool SystemChatInAlliance(int systemMessageId, List<FormatStringParamInfo> paramList, ChatExtraOutputLocationType chatExtraOutputLocationType = 0)
        {
        }

        protected AllianceCompBasicClient CompBasicClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected AllianceCompChatClient CompChatClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected AllianceCompInviteClient CompInviteClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected AllianceCompMailClient CompMailClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected AllianceCompMembersClient CompMembersClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

