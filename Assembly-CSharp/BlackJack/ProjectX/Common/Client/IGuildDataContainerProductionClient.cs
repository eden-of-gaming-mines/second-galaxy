﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;
    using System.Collections.Generic;

    public interface IGuildDataContainerProductionClient
    {
        void RefreshProductionLineList(List<GuildProductionLineInfo> lineList);
        void SetGuildProductionDataVersion(uint version);
    }
}

