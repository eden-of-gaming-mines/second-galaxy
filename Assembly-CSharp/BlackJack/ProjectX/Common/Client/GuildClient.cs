﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildClient : GuildBase, IGuildClientCompOwner, IGuildAllianceClient, IGuildAntiTeleportClient, IGuildBasicLogicClient, IGuildBattleClient, IGuildBenefitsClient, IGuildCompensationClient, IGuildCurrencyClient, IGuildCurrencyLogClient, IGuildDiplomacyClient, IGuildFlagShipClient, IGuildFleetClient, IGuildItemStoreClient, IGuildLostReportClient, IGuildMemberListClient, IGuildMiningClient, IGuildMomentsClient, IGuildProductionClient, IGuildPropertiesCalculaterClient, IGuildProtocolClient, IGuildPurchaseOrderClient, IGuildSolarSystemCtxClient, IGuildCompTeleportTunnelClient, IGuildTradeClient, IGuildCompOwnerBase, IGuildAllianceBase, IGuildAntiTeleportBase, IGuildBasicLogicBase, IGuildBattleBase, IGuildBenefitsBase, IGuildCompensationBase, IGuildCurrencyBase, IGuildCurrencyLogBase, IGuildDiplomacyBase, IGuildFlagShipBase, IGuildFleetBase, IGuildItemStoreBase, IGuildItemStoreItemOperationClient, IGuildItemStoreItemOperationBase, IGuildLostReportBase, IGuildMemberListBase, IGuildMiningBase, IGuildMomentsBase, IGuildProductionBase, IGuildPropertiesCalculaterBase, IGuildPurchaseOrderBase, IGuildSolarSystemCtxBase, IGuildCompTeleportTunnelBase, IGuildTeleportTunnelClient, IGuildTeleportTunnelBase, IGuildTradeBase
    {
        protected GuildCompProtocolClient m_compProtocol;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitAllComps;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_GetGuildDataContainer;
        private static DelegateBridge __Hotfix_GetCompProtocol;
        private static DelegateBridge __Hotfix_get_CompAllianceClient;
        private static DelegateBridge __Hotfix_OnAllianceInfoChange;
        private static DelegateBridge __Hotfix_RefreshAllianceInviteList;
        private static DelegateBridge __Hotfix_AllianceInviteRemove;
        private static DelegateBridge __Hotfix_UpdateGuildAntiTeleportInfoList;
        private static DelegateBridge __Hotfix_GetAntiTeleportInfoVersion;
        private static DelegateBridge __Hotfix_get_CompAntiTeleport;
        private static DelegateBridge __Hotfix_get_CompBasicClient;
        private static DelegateBridge __Hotfix_OnGuildChangeBasicInfoNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeManifestoNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeAnnouncementNtf;
        private static DelegateBridge __Hotfix_OnGuildChangeNameAndCodeNtf;
        private static DelegateBridge __Hotfix_OnGuildBaseSolarSystemSetAck;
        private static DelegateBridge __Hotfix_OnGuildLogoInfoNtf;
        private static DelegateBridge __Hotfix_OnSetDismissEndTime;
        private static DelegateBridge __Hotfix_GuildBattleReinforcementEndTimeSet;
        private static DelegateBridge __Hotfix_get_CompBattleClient;
        private static DelegateBridge __Hotfix_get_CompBenefitsClient;
        private static DelegateBridge __Hotfix_GetFinalBenefitsListVersion;
        private static DelegateBridge __Hotfix_RefreshBenefitsList;
        private static DelegateBridge __Hotfix_BenefitsReceive;
        private static DelegateBridge __Hotfix_GetBenefitsList;
        private static DelegateBridge __Hotfix_HasBenefitsAvailable;
        private static DelegateBridge __Hotfix_get_CompCompensationClient;
        private static DelegateBridge __Hotfix_RefreshCompensationList;
        private static DelegateBridge __Hotfix_get_CompCurrencyClient;
        private static DelegateBridge __Hotfix_UpdateGuildCurrencyInfo;
        private static DelegateBridge __Hotfix_OnGuildWalletAnnouncementSetAck;
        private static DelegateBridge __Hotfix_GuildTradeMoneyModify;
        private static DelegateBridge __Hotfix_get_CompCurrencyLogClient;
        private static DelegateBridge __Hotfix_GetGuildCurrencyLogList;
        private static DelegateBridge __Hotfix_RefreshGuildCurrencyLogList;
        private static DelegateBridge __Hotfix_get_CompDiplomacyClient;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyUpdateNtf;
        private static DelegateBridge __Hotfix_PackShipToItemStore;
        private static DelegateBridge __Hotfix_UnpackShipFromItemStore;
        private static DelegateBridge __Hotfix_AddOrUpdateGuildFlagShipHangarInfo;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipOptLogInfoList;
        private static DelegateBridge __Hotfix_ChangeFlagShipFuel;
        private static DelegateBridge __Hotfix_get_CompFlagShipClient;
        private static DelegateBridge __Hotfix_get_CompFleetClient;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberInfo;
        private static DelegateBridge __Hotfix_get_CompItemStoreClient;
        private static DelegateBridge __Hotfix_AddItemInMSStoreAutoOverlay;
        private static DelegateBridge __Hotfix_RemoveGuildStoreItemById;
        private static DelegateBridge __Hotfix_RemoveGuildStoreItemByIndex;
        private static DelegateBridge __Hotfix_ResumeItem;
        private static DelegateBridge __Hotfix_SuspendItem;
        private static DelegateBridge __Hotfix_UpdateGuildItemStoreInfo;
        private static DelegateBridge __Hotfix_GetSovereignLostReportList;
        private static DelegateBridge __Hotfix_RefleshSovereignLostReportList;
        private static DelegateBridge __Hotfix_get_GuildCompLostReportClient;
        private static DelegateBridge __Hotfix_get_CompMemberListlient;
        private static DelegateBridge __Hotfix_OnGuildMemberListInfoAck;
        private static DelegateBridge __Hotfix_OnSelfLeaveGuild;
        private static DelegateBridge __Hotfix_OnRemoveMember;
        private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateAck;
        private static DelegateBridge __Hotfix_GetMemberInfo;
        private static DelegateBridge __Hotfix_OnGuildLeaderTransferAbortAck;
        private static DelegateBridge __Hotfix_get_CompMiningClient;
        private static DelegateBridge __Hotfix_CheckMiningBalanceTimeRefreashCD;
        private static DelegateBridge __Hotfix_UpdateLastMiningBalanceTimeRefreashTime;
        private static DelegateBridge __Hotfix_GuildMiningBalanceTimeReset;
        private static DelegateBridge __Hotfix_get_CompMomentsClient;
        private static DelegateBridge __Hotfix_UpdateGuildMomentsInfoList;
        private static DelegateBridge __Hotfix_get_CompProductionlient;
        private static DelegateBridge __Hotfix_RefreshProductionLineList;
        private static DelegateBridge __Hotfix_SetGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_OnGuildMainPanelInfoAck;
        private static DelegateBridge __Hotfix_get_CompPurchaselient;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderListUpdate;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderCreate;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderModify;
        private static DelegateBridge __Hotfix_GuildPurchaseOrderCommitForSale;
        private static DelegateBridge __Hotfix_get_CompSolarSystemCtxlient;
        private static DelegateBridge __Hotfix_OnGuildOccupiedSolarSystemInfoAck;
        private static DelegateBridge __Hotfix_OnGuildBuildingDeployUpgradeAck;
        private static DelegateBridge __Hotfix_OnGuildBuildingRecycleAck;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemInfoAck;
        private static DelegateBridge __Hotfix_get_CompTeleportTunnel;
        private static DelegateBridge __Hotfix_get_CompTradeClient;
        private static DelegateBridge __Hotfix_OnGuildTradePurchaseOrderCreate;
        private static DelegateBridge __Hotfix_OnGuildTradePurchaseOrderRemove;
        private static DelegateBridge __Hotfix_OnGuildTradeTransportPlanCreate;
        private static DelegateBridge __Hotfix_OnGuildTradeTransportPlanComplete;
        private static DelegateBridge __Hotfix_GuildTradePortExtraInfoUpdate;
        private static DelegateBridge __Hotfix_GuildTradePortExtraInfoListUpdate;

        [MethodImpl(0x8000)]
        public GuildClient(IGuildDataContainerBase dc)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddItemInMSStoreAutoOverlay(int itemIndex, ulong instanceId, StoreItemType itemType, int itemConfigId, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void AddOrUpdateGuildFlagShipHangarInfo(ProGuildFlagShipHangarDetailInfo shipHangarDetailInfo, string hangarLocker)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceInviteRemove(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void BenefitsReceive(ulong benefitsInstanceId, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public bool ChangeFlagShipFuel(ulong shipHangarInsId, int slotIndex, ulong fuelInstanceId, int fuelIndex, int fuelSupplementCount, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckMiningBalanceTimeRefreashCD(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public int GetAntiTeleportInfoVersion()
        {
        }

        [MethodImpl(0x8000)]
        public void GetBenefitsList(out List<GuildCommonBenefitsInfo> commonBenefitsList, out GuildSovereigntyBenefitsInfo sovereigntyBenefits, out List<GuildInformationPointBenefitsInfo> informationPointBenefitsList, DateTime selfJoinTime, out int benefitsListVersion)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompProtocolClient GetCompProtocol()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFinalBenefitsListVersion(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildCurrencyLogInfo> GetGuildCurrencyLogList()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildDataContainerClient GetGuildDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo GetGuildFleetMemberInfo(ulong fleetInsId, string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberInfo GetMemberInfo(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public List<SovereignLostReport> GetSovereignLostReportList()
        {
        }

        [MethodImpl(0x8000)]
        public void GuildBattleReinforcementEndTimeSet(int endHour, int endMinuts)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildMiningBalanceTimeReset(int hour, int minute)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderCommitForSale(GuildPurchaseOrderInfo matchInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderCreate(GuildPurchaseOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderListUpdate(List<ProGuildPurchaseOrderListItemInfo> orderList)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildPurchaseOrderModify(bool isCancel, ulong instanceId, GuildPurchaseOrderInfo modifyOrder)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradeMoneyModify(long changeValue, GuildCurrencyLogType logType)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradePortExtraInfoListUpdate(List<GuildTradePortExtraInfo> guildTradePortExtraInfoList, ushort dataVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradePortExtraInfoUpdate(GuildTradePortExtraInfo guildTradePortExtraInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasBenefitsAvailable(DateTime currServerTime, DateTime selfJoinTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool InitAllComps()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllianceInfoChange(uint allianceId, string allianceName, AllianceLogoInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBaseSolarSystemSetAck(int solarSystemId, int stationId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBuildingDeployUpgradeAck(GuildBuildingDeployUpgradeAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildBuildingRecycleAck(GuildBuildingRecycleAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeAnnouncementNtf(string announcement, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeBasicInfoNtf(GuildAllianceLanguageType languageType, GuildJoinPolicy joinPolicy, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeManifestoNtf(string manifesto, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildChangeNameAndCodeNtf(string name, string code)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildDiplomacyUpdateNtf(GuildDiplomacyUpdateNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildLeaderTransferAbortAck()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildLogoInfoNtf(GuildLogoInfo logo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMainPanelInfoAck(GuildBasicInfo basicInfo, GuildSimpleRuntimeInfo simpleRuntimeInfo, GuildDynamicInfo dynamicInfo, List<GuildMomentsInfo> momentsBriefInfoList, uint momentsBriefInfoVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberJobUpdateAck(GuildJobType job, string playerGameUserId, bool isAppoint, DateTime serverTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberListInfoAck(GuildMemberListInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildOccupiedSolarSystemInfoAck(GuildOccupiedSolarSystemInfoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildSolarSystemInfoAck(GuildSolarSystemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradePurchaseOrderCreate(int solarSystemId, ulong purchaseInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradePurchaseOrderRemove(int solarSystemId, ulong purchaseInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradeTransportPlanComplete(int solarSystemId, ulong transportInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradeTransportPlanCreate(int solarSystemId, ulong transportInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildWalletAnnouncementSetAck(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRemoveMember(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSelfLeaveGuild(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSetDismissEndTime(long dismissEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool PackShipToItemStore(ulong shipHangarInsId, int hangarIndex, int resumeStoreItemIndex, ulong resumeItemInsId, List<StoreItemTransformInfo> resumedShipEquipInfoList, out LBStoreItem toItem)
        {
        }

        [MethodImpl(0x8000)]
        public void RefleshSovereignLostReportList(List<SovereignLostReport> sovereignLostReportList)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllianceInviteList(List<GuildAllianceInviteInfo> inviteList)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshBenefitsList(List<GuildBenefitsInfoBase> benefitsList, int listVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshCompensationList(List<LossCompensation> compensations, int version)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshGuildCurrencyLogList(List<GuildCurrencyLogInfo> list)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshProductionLineList(List<GuildProductionLineInfo> lineList)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveGuildStoreItemById(StoreItemType itemType, int itemId, int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveGuildStoreItemByIndex(int index, int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool ResumeItem(LBStoreItem from, int count, int resumedItemIndex, ulong instanceId, out LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildProductionDataVersion(uint version)
        {
        }

        [MethodImpl(0x8000)]
        public bool SuspendItem(LBStoreItem from, int count, ItemSuspendReason reason, int suspendedItemIndex, ulong instanceId, out LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        public bool UnpackShipFromItemStore(ulong shipHangarInsId, ulong shipPackedItemInsId, int hangarIndex, ulong shipUnpackedItemInsId, int unpackedItemIndex, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildAntiTeleportInfoList(List<GuildAntiTeleportEffectInfo> list, int dataVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildCurrencyInfo(GuildCurrencyInfo currency)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFlagShipOptLogInfoList(List<GuildFlagShipOptLogInfo> logInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildItemStoreInfo(List<GuildStoreItemListInfo> itemList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildMomentsInfoList(List<GuildMomentsInfo> momentsInfoList, bool isOlder)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLastMiningBalanceTimeRefreashTime(DateTime currTime)
        {
        }

        private IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompAllianceClient CompAllianceClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompAntiTeleportClient CompAntiTeleport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompBasicLogicClient CompBasicClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompBattleClient CompBattleClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompBenefitsClient CompBenefitsClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompCompensationClient CompCompensationClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompCurrencyClient CompCurrencyClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompCurrencyLogClient CompCurrencyLogClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompDiplomacyClient CompDiplomacyClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompFlagShipClient CompFlagShipClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompFleetClient CompFleetClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompItemStoreClient CompItemStoreClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected BlackJack.ProjectX.Common.Client.GuildCompLostReportClient GuildCompLostReportClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompMemberListClient CompMemberListlient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompMiningClient CompMiningClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompMomentsClient CompMomentsClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompProductionClient CompProductionlient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompPurchaseOrderClient CompPurchaselient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompSolarSystemCtxClient CompSolarSystemCtxlient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompTeleportTunnelClient CompTeleportTunnel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildCompTradeClient CompTradeClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

