﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildCurrencyLogClient : IGuildCurrencyLogBase
    {
        List<GuildCurrencyLogInfo> GetGuildCurrencyLogList();
        void RefreshGuildCurrencyLogList(List<GuildCurrencyLogInfo> list);
    }
}

