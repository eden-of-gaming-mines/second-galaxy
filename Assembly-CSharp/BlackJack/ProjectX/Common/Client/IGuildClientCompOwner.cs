﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IGuildClientCompOwner : IGuildCompOwnerBase
    {
        IGuildCompProtocolClient GetCompProtocol();
        IGuildDataContainerClient GetGuildDataContainer();
    }
}

