﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompProtocolClient : GuildCompBase, IGuildCompProtocolClient, IGuildProtocolClient
    {
        protected IGuildClientCompOwner m_owner;
        private IGuildCompBasicLogicClient m_compBasic;
        private IGuildCompSimpleRuntimeBase m_compSimpleRuntime;
        private IGuildCompMomentsClient m_compMoments;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_OnGuildMainPanelInfoAck;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompProtocolClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMainPanelInfoAck(GuildBasicInfo basicInfo, GuildSimpleRuntimeInfo simpleRuntimeInfo, GuildDynamicInfo dynamicInfo, List<GuildMomentsInfo> momentsBriefInfoList, uint momentsBriefInfoVersion)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitlialize()
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

