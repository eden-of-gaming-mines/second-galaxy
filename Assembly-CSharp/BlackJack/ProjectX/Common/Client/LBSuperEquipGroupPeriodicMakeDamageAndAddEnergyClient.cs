﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperEquipGroupPeriodicMakeDamageAndAddEnergyClient : LBSuperEquipGroupPeriodicMakeDamageAndAddEnergyBase, ILBSpaceProcessEquipPeriodicDamageSource, ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffect;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessLoop;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase> EventOnChannelEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnRecovergyEnergy;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessFire;
        private static DelegateBridge __Hotfix_OnProcessEffect;
        private static DelegateBridge __Hotfix_OnProcessPeriodicTrigger;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessCancel;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunch;
        private static DelegateBridge __Hotfix_add_EventOnStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnProcessFire;
        private static DelegateBridge __Hotfix_remove_EventOnProcessFire;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_add_EventOnProcessEffect;
        private static DelegateBridge __Hotfix_remove_EventOnProcessEffect;
        private static DelegateBridge __Hotfix_add_EventOnProcessLoop;
        private static DelegateBridge __Hotfix_remove_EventOnProcessLoop;
        private static DelegateBridge __Hotfix_add_EventOnChannelEnd;
        private static DelegateBridge __Hotfix_remove_EventOnChannelEnd;
        private static DelegateBridge __Hotfix_add_EventOnRecovergyEnergy;
        private static DelegateBridge __Hotfix_remove_EventOnRecovergyEnergy;

        public event Action<LBSuperEquipGroupBase> EventOnChannelEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnEquipLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnLaunchEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase, string> EventOnProcessEffect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnProcessLoop
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnRecovergyEnergy
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperEquipGroupBase, LBSpaceProcessEquipLaunchBase> EventOnStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupPeriodicMakeDamageAndAddEnergyClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessPeriodicTrigger(LBSpaceProcessEquipPeriodicDamageLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventEnd()
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessEquipPeriodicDamageLaunch process)
        {
        }
    }
}

