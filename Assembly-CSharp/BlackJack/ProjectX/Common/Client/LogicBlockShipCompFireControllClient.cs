﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompFireControllClient : LogicBlockShipCompFireControllBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLockedTargetLost;
        private Dictionary<uint, uint> m_hateTargetInitInfoDic;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnTargetEnterView;
        private static DelegateBridge __Hotfix_TryLockTarget;
        private static DelegateBridge __Hotfix_CheckTargetInView;
        private static DelegateBridge __Hotfix_OnSyncEventLockTarget;
        private static DelegateBridge __Hotfix_OnLockedTargetLost;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnSyncEventEnterFight;
        private static DelegateBridge __Hotfix_OnSyncEventLeaveFight;
        private static DelegateBridge __Hotfix_OnSyncEventKillOtherTarget;
        private static DelegateBridge __Hotfix_OnSyncEventWeaponLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventEquipLaunch;
        private static DelegateBridge __Hotfix_AddWeaponLaunchTargetsToHateList;
        private static DelegateBridge __Hotfix_DispatchWeaponLaunchSyncEventToLBWeaponEquip;
        private static DelegateBridge __Hotfix_AddEquipLaunchTargetsToHateList;
        private static DelegateBridge __Hotfix_DispatchEquipLaunchSyncEventToLBWeaponEquip;
        private static DelegateBridge __Hotfix_OnFlagShipTeleportChargingStart;
        private static DelegateBridge __Hotfix_OnFlagShipTeleportChargingFail;
        private static DelegateBridge __Hotfix_OnFlagShipTeleportProcessingFail;
        private static DelegateBridge __Hotfix_add_EventOnLockedTargetLost;
        private static DelegateBridge __Hotfix_remove_EventOnLockedTargetLost;

        public event Action EventOnLockedTargetLost
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AddEquipLaunchTargetsToHateList(LBSyncEvent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        private void AddWeaponLaunchTargetsToHateList(LBSyncEvent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool CheckTargetInView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        private void DispatchEquipLaunchSyncEventToLBWeaponEquip(LBSyncEvent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        private void DispatchWeaponLaunchSyncEventToLBWeaponEquip(LBSyncEvent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFlagShipTeleportChargingFail()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFlagShipTeleportChargingStart()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFlagShipTeleportProcessingFail()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLockedTargetLost()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventEquipLaunch(LBSyncEvent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventKillOtherTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventLeaveFight()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventLockTarget(LBSyncEventLockTarget syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventWeaponLaunch(LBSyncEvent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetEnterView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public override int TryLockTarget(ILBSpaceTarget target, bool forceNormalAttackRun = false, bool lockTargetByManualOpt = false)
        {
        }
    }
}

