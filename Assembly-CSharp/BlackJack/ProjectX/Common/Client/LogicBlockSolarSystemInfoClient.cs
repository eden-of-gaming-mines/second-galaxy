﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockSolarSystemInfoClient : LogicBlockSolarSystemInfoBase
    {
        private LinkedList<GlobalSceneInfo> m_globalSceneList;
        protected DateTime m_globalSceneLastUpdateTime;
        protected SolarSystemGlobalSceneSelector m_globalSceneSelector;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnGlobalSceneSync;
        private static DelegateBridge __Hotfix_OnGlobalSceneAdd;
        private static DelegateBridge __Hotfix_OnGlobalSceneRemove;
        private static DelegateBridge __Hotfix_GetGlobalSceneListLastUpdateTime;
        private static DelegateBridge __Hotfix_GetSelectedGlobalSceneList;
        private static DelegateBridge __Hotfix_GetGlobalSceneListInOrder;
        private static DelegateBridge __Hotfix_GetGlobalSceneByInstanceId;
        private static DelegateBridge __Hotfix_GetGlobalSceneByObjectId;
        private static DelegateBridge __Hotfix_GetInfectFinalBattleGlobalScene;
        private static DelegateBridge __Hotfix_OnSolarSystemChanged;
        private static DelegateBridge __Hotfix_ComparationForGlobalScenes;

        [MethodImpl(0x8000)]
        private int ComparationForGlobalScenes(GlobalSceneInfo scene1, GlobalSceneInfo scene2)
        {
        }

        [MethodImpl(0x8000)]
        public GlobalSceneInfo? GetGlobalSceneByInstanceId(uint insId)
        {
        }

        [MethodImpl(0x8000)]
        public GlobalSceneInfo? GetGlobalSceneByObjectId(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public LinkedList<GlobalSceneInfo> GetGlobalSceneListInOrder()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetGlobalSceneListLastUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        public GlobalSceneInfo? GetInfectFinalBattleGlobalScene()
        {
        }

        [MethodImpl(0x8000)]
        public LinkedList<GlobalSceneInfo> GetSelectedGlobalSceneList()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGlobalSceneAdd(GlobalSceneInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGlobalSceneRemove(uint sceneObjId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGlobalSceneSync(List<GlobalSceneInfo> infoList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemChanged(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }
    }
}

