﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBSuperWeaponGroupMissileClient : LBSuperWeaponGroupMissileBase, ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperWeaponGroupMissileClient, LBSpaceProcessSuperMissileLaunch> EventOnLaunchCancel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperWeaponGroupMissileClient, LBSpaceProcessSuperMissileLaunch, ushort> EventOnUnitStartCharge;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSuperWeaponGroupMissileClient, LBSpaceProcessSuperMissileLaunch, LBSpaceProcessBulletFly, ushort> EventOnUnitFire;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnProcessUnitStartCharge;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_OnProcessEnd;
        private static DelegateBridge __Hotfix_OnProcessCancel;
        private static DelegateBridge __Hotfix_CreateBulletFlyProcess;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCancel;
        private static DelegateBridge __Hotfix_add_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_remove_EventOnUnitStartCharge;
        private static DelegateBridge __Hotfix_add_EventOnUnitFire;
        private static DelegateBridge __Hotfix_remove_EventOnUnitFire;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunch;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunch;

        public event Action<LBSuperWeaponGroupMissileClient, LBSpaceProcessSuperMissileLaunch> EventOnLaunchCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperWeaponGroupMissileClient, LBSpaceProcessSuperMissileLaunch, LBSpaceProcessBulletFly, ushort> EventOnUnitFire
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSuperWeaponGroupMissileClient, LBSpaceProcessSuperMissileLaunch, ushort> EventOnUnitStartCharge
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, LBSpaceProcessWeaponLaunchBase> EventOnWeaponLaunch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBSuperWeaponGroupMissileClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBSpaceProcessBulletFly CreateBulletFlyProcess(LBSpaceProcessSuperMissileLaunch bulletGunLaunchProcess, LBProcessSingleMissileUnitLaunchInfo bullet)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort bulletIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnProcessUnitStartCharge(LBSpaceProcessWeaponLaunchBase process, ushort bulletIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessSuperMissileLaunch process)
        {
        }
    }
}

