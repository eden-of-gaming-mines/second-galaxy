﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildPurchaseOrderClient : IGuildPurchaseOrderBase
    {
        void GuildPurchaseOrderCommitForSale(GuildPurchaseOrderInfo matchInfo);
        void GuildPurchaseOrderCreate(GuildPurchaseOrderInfo order);
        void GuildPurchaseOrderListUpdate(List<ProGuildPurchaseOrderListItemInfo> orderList);
        void GuildPurchaseOrderModify(bool isCancel, ulong instanceId, GuildPurchaseOrderInfo modifyOrder);
    }
}

