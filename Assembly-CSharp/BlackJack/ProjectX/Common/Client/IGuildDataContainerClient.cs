﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildDataContainerClient : IGuildDataContainerBase, IGuildDataContainerProductionClient, IGuildLostReportDataContainerClient, IGuildBenefitsDataContainerClient, IGuildMomentsDataContainerClient, IGuildAllianceInviteDataContainerClient, IGuildTradeDataContainerClient, IGuildFlagShipHangarListDataContainerClient, IGuildAntiTeleportInfoDataContainerClient, IGuildAllianceTeleportTunnelInfoDataContainerClient, IGuildFlagShipOptLogDataContainerClient, IGuildBasicDataContainer, IGuildSimpleRuntimeInfoDataContainer, IGuildMemberListDataContainer, IDiplomacyDataContainer, IGuildItemStoreDataContainer, IGuildLostReportDataContainer, IGuildCurrencyDataContainer, IGuildProductionDataContainer, IGuildSolarSystemCtxDataContainer, IGuildPurchaseDataContainer, IGuildFleetDataContainer, IGuildCompensationContainer, IGuildMiningDataContainer, IGuildBenefitsDataContainer, IGuildMomentsDataContainer, IGuildAllianceInviteDataContainer, IGuildTradeDataContainerBase, IGuildFlagShipHangarListDataContainer, IGuildDynamicDataContainer, IGuildAntiTeleportInfoDataContainer, IGuildTeleportTunnelInfoDataContainer, IGuildFlagShipOptLogDataContainer
    {
        void ClearAllGuildDataOnLeaveGuild();
        uint GetGuildMessageVersion();
        void Init(GuildBasicInfo basicInfo, GuildMemberListInfo memberListInfo, GuildSimpleRuntimeInfo simpleRuntimeInfo, DiplomacyInfo diplomacyInfo, GuildCurrencyInfo currencyInfo);
        void RefreshCompensationList(List<LossCompensation> compensations, int version);
        void UpdateFleetVersionInfo(ulong fleetInsId, uint basicInfoVersion, uint fleetMemberListVersion);
        void UpdateGuildBasicInfo(GuildBasicInfo info);
        void UpdateGuildItemStoreInfo(List<GuildStoreItemListInfo> itemList);
        void UpdateGuildMessageVersion(uint version);
        void UpdateGuildSolarSystemVersionInfo(int solarSystemId, int version, int buildingVersion);
        void UpdateMemberBasicInfo(GuildMemberInfo readOnlyInfo, GuildMemberBasicInfo basicInfo);
        void UpdateMemberDynamicInfo(GuildMemberInfo readOnlyInfo, GuildMemberDynamicInfo dynamicInfo);
        void UpdateMemberListVersionInfo(uint basicInfoVersion, uint dynamicVersion, uint runtimeVersion);
    }
}

