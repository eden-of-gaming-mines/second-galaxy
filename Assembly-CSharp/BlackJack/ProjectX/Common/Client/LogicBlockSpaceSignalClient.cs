﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockSpaceSignalClient : LogicBlockSpaceSignalBase
    {
        private ILBPlayerContextClient m_playerCtxClient;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateSpaceSignalResultInfo;
        private static DelegateBridge __Hotfix_GetSolarSystemSpaceSignalInfo;
        private static DelegateBridge __Hotfix_get_PlayerCtxClient;

        [MethodImpl(0x8000)]
        public SolarSystemSpaceSignalInfo GetSolarSystemSpaceSignalInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpaceSignalResultInfo(int solarSystemId, SignalType signalType, DateTime expiredTime, List<SignalInfo> signalInfoList)
        {
        }

        protected ILBPlayerContextClient PlayerCtxClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

