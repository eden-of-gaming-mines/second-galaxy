﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompInSpaceWeaponEquipClient : LogicBlockShipCompInSpaceWeaponEquipBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSuperGroupEnergyAddToFull;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSuperGroupEnergyChangeToEmpty;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnResetWeaponEquipGroupCD;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<List<ItemInfo>> EventOnWeaponAmmoReloadByPercent;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBInSpaceWeaponEquipGroupBase> EventOnWeaponEquipEnterCD;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAllWeaponEquipCDComplete;
        protected uint m_blinkEndTime;
        protected uint m_weaponEquipCDStateMask;
        protected List<LBInSpaceWeaponEquipGroupBase> m_inSpaceWeaponEquipGroupList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CreateInSpaceGroup;
        private static DelegateBridge __Hotfix_CreateSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CreateSuperEquipGroup;
        private static DelegateBridge __Hotfix_CreateNormalAttackGroup;
        private static DelegateBridge __Hotfix_CreateTacticalEquipGroup;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_RemoveAmmoFromSlotGroup;
        private static DelegateBridge __Hotfix_OnSyncEventNormalAttackLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventRailgunLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperRailgunLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventPlasmaLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperPlasmaLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventCannonLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventMissileLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperMissileLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventLaserLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperLaserLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventDroneFighterLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventDroneDefenderLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventDroneSniperLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperDroneFighterLaunch;
        private static DelegateBridge __Hotfix_OnSyncHighSlotWeaponReadyForLaunchTime;
        private static DelegateBridge __Hotfix_OnSyncEventEquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipAntiJumpingForceSieldLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipTransformToTeleportTunnelLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipAddEnergyAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipClearCDAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipChannelLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipPeriodicDamageLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipPeriodicDamageEnd;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipAddShieldAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipAoeLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventEquipBlinkLaunch;
        private static DelegateBridge __Hotfix_OnSynEventEquipBlinkEnd;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEquipChannelEnd;
        private static DelegateBridge __Hotfix_OnSyncEventEquipInvisibleLaunch;
        private static DelegateBridge __Hotfix_OnSynEventEquipInvisibleEnd;
        private static DelegateBridge __Hotfix_OnSynEventSuperEquipDurationEnd;
        private static DelegateBridge __Hotfix_OnSynEventResetWeaponEquipGroupCD;
        private static DelegateBridge __Hotfix_OnSynEventResetTacticalEquipGroupCD;
        private static DelegateBridge __Hotfix_OnSyncEquipGroupReadyForLaunchTime;
        private static DelegateBridge __Hotfix_OnSyncEventTacticalEquipTimeInfo;
        private static DelegateBridge __Hotfix_OnSyncEventDroneDestoryedByDefender;
        private static DelegateBridge __Hotfix_OnSyncEventReloadAmmoStart;
        private static DelegateBridge __Hotfix_OnSyncEventReloadAmmoEnd;
        private static DelegateBridge __Hotfix_OnSyncEventAmmoReloadByPercent;
        private static DelegateBridge __Hotfix_OnSyncEventSuperGroupEnergyAddToFull;
        private static DelegateBridge __Hotfix_OnSyncEventSuperGroupEnergyChangeToEmpty;
        private static DelegateBridge __Hotfix_OnSyncEventSuperEnergyModify;
        private static DelegateBridge __Hotfix_add_EventOnSuperGroupEnergyAddToFull;
        private static DelegateBridge __Hotfix_remove_EventOnSuperGroupEnergyAddToFull;
        private static DelegateBridge __Hotfix_add_EventOnSuperGroupEnergyChangeToEmpty;
        private static DelegateBridge __Hotfix_remove_EventOnSuperGroupEnergyChangeToEmpty;
        private static DelegateBridge __Hotfix_add_EventOnResetWeaponEquipGroupCD;
        private static DelegateBridge __Hotfix_remove_EventOnResetWeaponEquipGroupCD;
        private static DelegateBridge __Hotfix_add_EventOnWeaponAmmoReloadByPercent;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponAmmoReloadByPercent;
        private static DelegateBridge __Hotfix_TickForWeaponEquipCDState;
        private static DelegateBridge __Hotfix_SetWeaponEquipCDStateMask;
        private static DelegateBridge __Hotfix_ClearWeaponEquipCDStateMask;
        private static DelegateBridge __Hotfix_IsWeaponEquipInCDState;
        private static DelegateBridge __Hotfix_add_EventOnWeaponEquipEnterCD;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponEquipEnterCD;
        private static DelegateBridge __Hotfix_add_EventOnAllWeaponEquipCDComplete;
        private static DelegateBridge __Hotfix_remove_EventOnAllWeaponEquipCDComplete;
        private static DelegateBridge __Hotfix_SetBlinkEndTime;
        private static DelegateBridge __Hotfix_GetBlinkEndTime;

        public event Action EventOnAllWeaponEquipCDComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnResetWeaponEquipGroupCD
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSuperGroupEnergyAddToFull
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSuperGroupEnergyChangeToEmpty
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<ItemInfo>> EventOnWeaponAmmoReloadByPercent
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase> EventOnWeaponEquipEnterCD
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void ClearWeaponEquipCDStateMask(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBInSpaceWeaponEquipGroupBase CreateInSpaceGroup(LBStaticWeaponEquipSlotGroup staticGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBInSpaceWeaponEquipGroupBase CreateNormalAttackGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBSuperEquipGroupBase CreateSuperEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBSuperWeaponGroupBase CreateSuperWeaponGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected override LBTacticalEquipGroupBase CreateTacticalEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetBlinkEndTime()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsWeaponEquipInCDState(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEquipGroupReadyForLaunchTime(List<uint> readyForLaunchTimeList)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventAmmoReloadByPercent(LBSyncEventWeaponAmmoReloadByPercent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventCannonLaunch(LBSyncEventCannonLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventDroneDefenderLaunch(LBSyncEventDroneDefenderLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventDroneDestoryedByDefender(LBSyncEventDroneDestoryedByDefender syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventDroneFighterLaunch(LBSyncEventDroneFighterLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventDroneSniperLaunch(LBSyncEventDroneSniperLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventEquipAttachBufLaunch(LBSynEventEquipAttachBufLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventEquipBlinkLaunch(LBSynEventEquipBlinkLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventEquipInvisibleLaunch(LBSynEventEquipInvisibleLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventLaserLaunch(LBSyncEventLaserLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventMissileLaunch(LBSyncEventMissileLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventNormalAttackLaunch(LBSyncEventNormalAttackLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventPlasmaLaunch(LBSyncEventPlasmaLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventRailgunLaunch(LBSyncEventRailgunLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventReloadAmmoEnd(LBSyncEventReloadAmmoEnd syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventReloadAmmoStart(LBSyncEventReloadAmmoStart syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperDroneFighterLaunch(LBSyncEventSuperDroneFighterLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventSuperEnergyModify(LBSynEventSuperEnergyModify syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipAddEnergyAndAttachBufLaunch(LBSynEventSuperEquipAddEnergyAndAttachBufLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipAddShieldAndAttachBufLaunch(LBSynEventSuperEquipAddShieldAndAttachBufLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipAntiJumpingForceSieldLaunch(LBSynEventSuperEquipAntiJumpingForceSieldLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipAoeLaunch(LBSyncEventSuperEquipAoeLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipAttachBufLaunch(LBSynEventSuperEquipAttachBufLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipChannelEnd(LBSynEventEquipChannelEnd syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipChannelLaunch(LBSynEventEquipChannelLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipClearCDAndAttachBuffLaunch(LBSynEventSuperEquipClearCDAndAttachBuffLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipPeriodicDamageEnd(LBSyncEventEquipPeriodicDamageEnd syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipPeriodicDamageLaunch(LBSyncEventEquipPeriodicDamageLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch(LBSynEventSuperEquipShipShield2ExtraShieldAndAttachBuffLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperEquipTransformToTeleportTunnelLaunch(LBSynEventSuperEquipTransformToTeleportTunnelLaunch syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperGroupEnergyAddToFull(LBSynEventSuperGroupEnergyFull syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperGroupEnergyChangeToEmpty()
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperLaserLaunch(LBSyncEventSuperLaserLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperMissileLaunch(LBSyncEventSuperMissileLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperPlasmaLaunch(LBSyncEventSuperPlasmaLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventSuperRailgunLaunch(LBSyncEventSuperRailgunLaunch launchEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSyncEventTacticalEquipTimeInfo(LBSyncEventTacticalEquipEffectTime timeInfoEvent)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncHighSlotWeaponReadyForLaunchTime(List<uint> readyForLaunchTimeList)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSynEventEquipBlinkEnd(LBSynEventEquipBlinkEnd syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSynEventEquipInvisibleEnd(LBSynEventEquipInvisibleEnd syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSynEventResetTacticalEquipGroupCD(uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSynEventResetWeaponEquipGroupCD(List<uint> highSlotGroupCDEndTime, List<uint> middleSlotGroupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        internal void OnSynEventSuperEquipDurationEnd(LBSynEventSuperEquipDurationEnd syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveAmmoFromSlotGroup(LBWeaponGroupBase group, AmmoStoreItemInfo ammoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBlinkEndTime(uint endTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetWeaponEquipCDStateMask(int index)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForWeaponEquipCDState(uint currTime)
        {
        }
    }
}

