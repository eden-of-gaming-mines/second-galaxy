﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class ClientStoreItemAmmoHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetTechRequirementForAmmo_0;
        private static DelegateBridge __Hotfix_GetTechRequirementForAmmo_1;
        private static DelegateBridge __Hotfix_GetWeaponTypeStringKeyForAmmo_0;
        private static DelegateBridge __Hotfix_GetWeaponTypeStringKeyForAmmo_1;
        private static DelegateBridge __Hotfix_GetAmmoPropertyList_0;
        private static DelegateBridge __Hotfix_GetAmmoPropertyList_1;
        private static DelegateBridge __Hotfix_GetAmmoNameStr;

        [MethodImpl(0x8000)]
        public static string GetAmmoNameStr(StoreItemType ammoType, int ammoConfId)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetAmmoPropertyList(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static List<CommonPropertyInfo> GetAmmoPropertyList(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetTechRequirementForAmmo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static uint GetTechRequirementForAmmo(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWeaponTypeStringKeyForAmmo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWeaponTypeStringKeyForAmmo(StoreItemType itemType, object confInfo)
        {
        }
    }
}

