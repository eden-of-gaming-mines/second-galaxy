﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IGuildCompMemberListClient : IGuildCompMemberListBase, IGuildMemberListClient, IGuildMemberListBase
    {
    }
}

