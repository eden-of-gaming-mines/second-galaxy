﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockSignalClient : LogicBlockSignalBase
    {
        private LBScanProbe m_delegateScanProbe;
        private LBScanProbe m_manualScanProbe;
        private LBScanProbe m_pvpScanProbe;
        private ILBPlayerContextClient m_playerCtxClient;
        [CompilerGenerated]
        private static Predicate<LBScanProbe> <>f__am$cache0;
        [CompilerGenerated]
        private static Predicate<LBScanProbe> <>f__am$cache1;
        [CompilerGenerated]
        private static Predicate<LBScanProbe> <>f__am$cache2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnDelegateSignalUpdate;
        private static DelegateBridge __Hotfix_OnManualSignalUpdate;
        private static DelegateBridge __Hotfix_OnEmergencySignalAdd;
        private static DelegateBridge __Hotfix_get_DelegateScanProbe;
        private static DelegateBridge __Hotfix_get_ManualScanProbe;
        private static DelegateBridge __Hotfix_get_PVPScanProbe;
        private static DelegateBridge __Hotfix_get_PlayerCtxClient;
        private static DelegateBridge __Hotfix_GetVirtualBuffsInfluenceToDelegateMission;
        private static DelegateBridge __Hotfix_GetDelegateMissionPoolCountInfoForSignal;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForDelegateMissionPoolCount;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForSolarSystemDelegateMissionRewardMulti;

        [MethodImpl(0x8000)]
        private ConfigDataSolarSystemDelegateMissionPoolCountInfo GetDelegateMissionPoolCountInfoForSignal(LBSignalDelegateBase signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public VirtualBuffDesc GetVirtualBuffDescForDelegateMissionPoolCount(SignalType signalType, ConfigDataSolarSystemDelegateMissionPoolCountInfo poolCountInfo)
        {
        }

        [MethodImpl(0x8000)]
        public VirtualBuffDesc GetVirtualBuffDescForSolarSystemDelegateMissionRewardMulti(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> GetVirtualBuffsInfluenceToDelegateMission(LBSignalDelegateBase signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDelegateSignalUpdate(List<SignalInfo> signalList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEmergencySignalAdd(SignalInfo signal)
        {
        }

        [MethodImpl(0x8000)]
        public void OnManualSignalUpdate(List<SignalInfo> manualSignalList)
        {
        }

        public LBScanProbe DelegateScanProbe
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public LBScanProbe ManualScanProbe
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public LBScanProbe PVPScanProbe
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected ILBPlayerContextClient PlayerCtxClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

