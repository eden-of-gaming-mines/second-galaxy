﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBWeaponGroupCannonClient : LBWeaponGroupRailgunClient
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CalcHitRateFinal;
        private static DelegateBridge __Hotfix_CalcCriticalRate;
        private static DelegateBridge __Hotfix_CalcDamageTotal;
        private static DelegateBridge __Hotfix_CalcCriticalDamageTotal;
        private static DelegateBridge __Hotfix_CalcDamageComposeHeat;
        private static DelegateBridge __Hotfix_CalcDamageComposeKinetic;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_CalcFireCtrlAccuracy;
        private static DelegateBridge __Hotfix_CalcWaveCD;
        private static DelegateBridge __Hotfix_CalcReloadAmmoCD;
        private static DelegateBridge __Hotfix_CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_CalcWaveGroupDamage;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4WeaponEquip;
        private static DelegateBridge __Hotfix_GetPropertyFromConfigData4Ammo;

        [MethodImpl(0x8000)]
        public LBWeaponGroupCannonClient(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcCriticalDamageTotal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcCriticalRate(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageComposeHeat()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageComposeKinetic()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageTotal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireCtrlAccuracy()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcHitRateFinal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcReloadAmmoCD()
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcWaveCD()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcWaveGroupDamage()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result)
        {
        }
    }
}

