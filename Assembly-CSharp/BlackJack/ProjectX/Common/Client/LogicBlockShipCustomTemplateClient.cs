﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCustomTemplateClient : LogicBlockShipCustomTemplateBase
    {
        protected const int ShipCustomTemplateInfoNameLengthMax = 20;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShipCustomTemplateAdd;
        private static DelegateBridge __Hotfix_ShipCustomTemplateRemove;
        private static DelegateBridge __Hotfix_ShipCustomTemplateUpdate;
        private static DelegateBridge __Hotfix_ShipCustomTemplateApply;
        private static DelegateBridge __Hotfix_GetCustomTemplateByShip;
        private static DelegateBridge __Hotfix_CheckShipCustomTemplateInfoNameValid;

        [MethodImpl(0x8000)]
        public bool CheckShipCustomTemplateInfoNameValid(string name, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public ShipCustomTemplateInfo GetCustomTemplateByShip(ILBStaticPlayerShip ship, Func<string, string, string> templateNameCreator)
        {
        }

        [MethodImpl(0x8000)]
        public void ShipCustomTemplateAdd(ShipCustomTemplateInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool ShipCustomTemplateApply(bool isPredefineTemplate, int templateIndex, ulong shipInstanceId, List<StoreItemTransformInfo> resumedItemInfoList, List<AmmoStoreItemInfo> resumedAmmoInfoList, List<StoreItemInfo> storeItemAddList, CurrencyUpdateInfo currencyUpdate, List<StoreItemTransformInfo> item2SlotGroupList)
        {
        }

        [MethodImpl(0x8000)]
        public void ShipCustomTemplateRemove(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void ShipCustomTemplateUpdate(int index, string name)
        {
        }
    }
}

