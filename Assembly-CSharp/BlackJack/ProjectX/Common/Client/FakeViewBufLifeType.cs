﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;

    public enum FakeViewBufLifeType
    {
        Forever,
        LeaveInvisible,
        Invalid
    }
}

