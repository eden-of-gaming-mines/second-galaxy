﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompItemStoreClient : GuildCompItemStoreBase, IGuildCompItemStoreClient, IGuildCompItemStoreBase, IGuildItemStoreClient, IGuildItemStoreBase, IGuildItemStoreItemOperationBase, IGuildItemStoreItemOperationClient
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddItemInMSStoreAutoOverlay;
        private static DelegateBridge __Hotfix_UpdateGuildItemStoreInfo;
        private static DelegateBridge __Hotfix_RemoveGuildStoreItemByIndex;
        private static DelegateBridge __Hotfix_RemoveGuildStoreItemById;
        private static DelegateBridge __Hotfix_SuspendItem;
        private static DelegateBridge __Hotfix_ResumeItem;
        private static DelegateBridge __Hotfix_ReInitStoreItem_1;
        private static DelegateBridge __Hotfix_ReInitStoreItem_0;
        private static DelegateBridge __Hotfix_CreateLBStoreItemByItemInfo;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompItemStoreClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddItemInMSStoreAutoOverlay(int itemIndex, ulong instanceId, StoreItemType itemType, int itemConfigId, int count)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStoreItem CreateLBStoreItemByItemInfo(GuildStoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem ReInitStoreItem(int index, ItemInfo itemInfo, bool isDelete, uint flag)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem ReInitStoreItem(int index, StoreItemType itemType, int itemConfigId, long count, ulong instanceId, uint flag)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveGuildStoreItemById(StoreItemType itemType, int itemId, int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveGuildStoreItemByIndex(int index, int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool ResumeItem(LBStoreItem from, int count, int resumedItemIndex, ulong instanceId, out LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        public bool SuspendItem(LBStoreItem from, int count, ItemSuspendReason reason, int suspendedItemIndex, ulong instanceId, out LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildItemStoreInfo(List<GuildStoreItemListInfo> itemList)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

