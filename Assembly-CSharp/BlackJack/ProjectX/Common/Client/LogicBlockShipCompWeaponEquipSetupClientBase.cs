﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class LogicBlockShipCompWeaponEquipSetupClientBase : LogicBlockShipCompWeaponEquipSetupBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetItem2SlotGroup;
        private static DelegateBridge __Hotfix_SetItem2SlotGroupImpl;
        private static DelegateBridge __Hotfix_RemoveItemFromSlotGroup;
        private static DelegateBridge __Hotfix_RemoveItemFromSlotGroupImpl;
        private static DelegateBridge __Hotfix_RemoveAllItemFromSlotGroup;
        private static DelegateBridge __Hotfix_TryToRemoveAllSlotGroups;

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompWeaponEquipSetupClientBase()
        {
        }

        protected abstract void AutoLoadAmmoForSlotGroupSet(SimpleItemInfo ammoRequire, LBStaticWeaponEquipSlotGroup group, AmmoStoreItemInfo removedAmmo);
        protected abstract List<LBStaticWeaponEquipSlotGroup> CollectAllRemoveWeaponEquipGroups();
        protected abstract void PostRemoveItemFromSlotGroup();
        protected abstract void PostSetItem2SlotGroup(LBStaticWeaponEquipSlotGroup group);
        protected abstract bool PreProcessingForUnsetWeaponGroup(LBStaticWeaponEquipSlotGroup group, AmmoStoreItemInfo ammoInfo);
        [MethodImpl(0x8000)]
        public bool RemoveAllItemFromSlotGroup(List<StoreItemTransformInfo> resumedItemInfoList, List<AmmoStoreItemInfo> ammoInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveItemFromSlotGroup(LBStaticWeaponEquipSlotGroup group, int destStoreItemIndex, ulong destStoreItemInstanceId, AmmoStoreItemInfo ammoInfo, out LBStoreItem destMSStoreItem)
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveItemFromSlotGroupImpl(LBStaticWeaponEquipSlotGroup group, int destStoreItemIndex, ulong destStoreItemInstanceId, AmmoStoreItemInfo ammoInfo, out LBStoreItem destMSStoreItem)
        {
        }

        protected abstract bool ResumeItemFromSlotGroup(LBStaticWeaponEquipSlotGroup group, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem to);
        [MethodImpl(0x8000)]
        public bool SetItem2SlotGroup(LBStaticWeaponEquipSlotGroup group, LBStoreItem srcMSStoreItem, SimpleItemInfo ammoRequire, AmmoStoreItemInfo removedAmmo, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem destStoreItem, out int operationResult)
        {
        }

        [MethodImpl(0x8000)]
        protected bool SetItem2SlotGroupImpl(LBStaticWeaponEquipSlotGroup group, LBStoreItem srcMSStoreItem, SimpleItemInfo ammoRequire, AmmoStoreItemInfo removedAmmo, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem destStoreItem, out int operationResult)
        {
        }

        protected abstract bool SuspendItemForSlotGroupSet(LBStoreItem srcItem, int destStoreItemIndex, ulong destStoreItemInstanceId, out LBStoreItem destStoreItem);
        [MethodImpl(0x8000)]
        protected bool TryToRemoveAllSlotGroups(List<LBStaticWeaponEquipSlotGroup> groups, List<StoreItemTransformInfo> resumedItemInfoList, List<AmmoStoreItemInfo> ammoInfoList)
        {
        }

        protected abstract void UpdateCacheInfoForSetSlotGroup(LBStaticWeaponEquipSlotGroup group, LBStoreItem destItem);
        protected abstract void UpdateCacheInfoForUnsetSlotGroup(LBStaticWeaponEquipSlotGroup group);
    }
}

