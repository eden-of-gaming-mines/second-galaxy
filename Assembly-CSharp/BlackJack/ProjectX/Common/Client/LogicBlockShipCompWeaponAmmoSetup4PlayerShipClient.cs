﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompWeaponAmmoSetup4PlayerShipClient : LogicBlockShipCompWeaponAmmoSetup4PlayerShip
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ReloadAmmoFromMSStore;
        private static DelegateBridge __Hotfix_RemoveAmmoFromSlotGroup;
        private static DelegateBridge __Hotfix_ReloadAmmoFromAmmoInfo;
        private static DelegateBridge __Hotfix_ResetWeaponGroupAmmoCount;

        [MethodImpl(0x8000)]
        public bool ReloadAmmoFromAmmoInfo(LBStaticWeaponEquipSlotGroup group, AmmoStoreItemInfo removedAmmo, AmmoInfo reloadAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool ReloadAmmoFromMSStore(LBStaticWeaponEquipSlotGroup group, LBStoreItem srcMSStoreItem, AmmoStoreItemInfo removedAmmo, int addAmmoCount, out int operationResult)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveAmmoFromSlotGroup(LBStaticWeaponEquipSlotGroup group, bool bCanUnloadToMS, AmmoStoreItemInfo ammoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool ResetWeaponGroupAmmoCount(LBStaticWeaponEquipSlotGroup group, int resetToCount, bool bCanOperateMSItemStore, AmmoStoreItemInfo removeAmmoInfo)
        {
        }
    }
}

