﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AllianceCompMembersClient : AllianceCompMembersBase, IAllianceCompMembersClient, IAllianceCompMembersBase, IAllianceMembersClient, IAllianceMembersBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_AllianceMemberRemove;
        private static DelegateBridge __Hotfix_AllianceLeaderTransfer;

        [MethodImpl(0x8000)]
        public AllianceCompMembersClient(IAllianceCompOwnerClient owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceLeaderTransfer(uint srcGuildId, uint destGuildId)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceMemberRemove(uint removedGuildId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitlialize()
        {
        }

        private IAllianceDCMembersClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

