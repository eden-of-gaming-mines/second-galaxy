﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupAttachShieldExBufByCostEnergyClient : LBTacticalEquipGroupAttachShieldExBufByCostEnergyBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<float, float, LBTacticalEquipGroupBase> EventOnEquipEffectStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnEquipEffectEnd;
        protected LBBufBase m_currBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnDetachBuf;
        private static DelegateBridge __Hotfix_GetShieldExBufId;
        private static DelegateBridge __Hotfix_add_EventOnEquipEffectStart;
        private static DelegateBridge __Hotfix_remove_EventOnEquipEffectStart;
        private static DelegateBridge __Hotfix_add_EventOnEquipEffectEnd;
        private static DelegateBridge __Hotfix_remove_EventOnEquipEffectEnd;

        public event Action EventOnEquipEffectEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float, float, LBTacticalEquipGroupBase> EventOnEquipEffectStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupAttachShieldExBufByCostEnergyClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public int GetShieldExBufId()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDetachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

