﻿namespace BlackJack.ProjectX.Common.Client
{
    using System;
    using System.Runtime.InteropServices;

    public interface IClientCustomDataContainer
    {
        void AddHaveReadBuildingLostReport(ulong reportId);
        void AddHaveReadDestoryedGuildFlagShip(ulong shipInstanceId);
        void AddHaveReadSovereignLostReport(ulong reportId);
        void AddOrUpdateCustomData(string dataStrKey, bool dataValue);
        void AddOrUpdateCustomData(string dataStrKey, long dataValue);
        void ClearHaveDestoryedGuildFlagShip();
        void ClearHaveReadLostReport();
        void Init();
        bool IsBuildingLostReportRead(ulong reportId);
        bool IsDestoryedGuildFlagShipRead(ulong shipInstanceId);
        bool IsSovereignLostReportRead(ulong reportId);
        void RemoveHaveReadDestoryedGuildFlagShip(ulong shipInstanceId);
        bool TryGetCustomData(string dataStrKey, out bool dataValue);
        bool TryGetCustomData(string dataStrKey, out long dataValue);
    }
}

