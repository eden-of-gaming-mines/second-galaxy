﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IAllianceBasicClient : IAllianceBasicBase
    {
        void AllianceAnnouncementSet(string announcement);
        void AllianceLogoSet(AllianceLogoInfo logo);
        void AllianceNameSet(string name);
        void AllianceRegionSet(int region);
    }
}

