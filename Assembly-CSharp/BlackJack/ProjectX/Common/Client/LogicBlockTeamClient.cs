﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockTeamClient : LogicBlockTeamBase
    {
        protected uint m_teamInstanceId;
        protected List<TeamMemberInfo> m_members;
        private ILBPlayerContext m_lbPlayerCtx;
        private Dictionary<string, DateTime> m_nextSendTeamInviteTimeDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnTeamInfoNtf;
        private static DelegateBridge __Hotfix_OnTeamInviteAck;
        private static DelegateBridge __Hotfix_OnTeamMemberAddNtf;
        private static DelegateBridge __Hotfix_OnTeamMemberLeaveNtf;
        private static DelegateBridge __Hotfix_OnTeamLeaveAck;
        private static DelegateBridge __Hotfix_OnTeamMamberSnapshotSyncNtf;
        private static DelegateBridge __Hotfix_GetTeamMemberList;
        private static DelegateBridge __Hotfix_GetTeamMemberByGameUserId;
        private static DelegateBridge __Hotfix_HasTeam;
        private static DelegateBridge __Hotfix_GetTeamInstanceId;
        private static DelegateBridge __Hotfix_CheckCondForSendTeamInvite;
        private static DelegateBridge __Hotfix_SetNextSendTeamInviteTime;

        [MethodImpl(0x8000)]
        public bool CheckCondForSendTeamInvite(string targetGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetTeamInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public TeamMemberInfo GetTeamMemberByGameUserId(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public List<TeamMemberInfo> GetTeamMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasTeam()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnTeamInfoNtf(uint teamInstanceId, List<TeamMemberInfo> members)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnTeamInviteAck(string inviteGameUserId, string inviteUserName)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamLeaveAck()
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamMamberSnapshotSyncNtf(TeamMemberSnapshotInfo snapShotInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamMemberAddNtf(TeamMemberInfo addMember)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamMemberLeaveNtf(string leaveMemberUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNextSendTeamInviteTime(string targetGameUserId, DateTime time)
        {
        }
    }
}

