﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildMomentsClient : IGuildMomentsBase
    {
        void UpdateGuildMomentsInfoList(List<GuildMomentsInfo> momentsInfoList, bool isOlder);
    }
}

