﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildCompBasicLogicClient : IGuildCompBasicLogicBase, IGuildBasicLogicClient, IGuildBasicLogicBase
    {
        void UpdateGuildBasicInfo(GuildBasicInfo info);
    }
}

