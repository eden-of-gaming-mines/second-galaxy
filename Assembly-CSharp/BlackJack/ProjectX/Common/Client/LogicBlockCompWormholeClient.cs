﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCompWormholeClient
    {
        protected Dictionary<int, Dictionary<int, ConfigDataWormholeStarGroupInfo>> m_starfieldId2WormholeGateGroupInfoDict;
        protected Dictionary<int, uint> m_starfieldWormholeGateDataVersionDict;
        protected Dictionary<int, Dictionary<int, ConfigDataWormholeStarGroupInfo>> m_starfieldId2RareWormholeGameGroupInfoDict;
        private ILBPlayerContext m_lbPlayerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_TryGetWormholeStargroupInfo;
        private static DelegateBridge __Hotfix_TryGetRareWormholeStargroupInfo;
        private static DelegateBridge __Hotfix_GetWormholeInfoDataVersion;
        private static DelegateBridge __Hotfix_UpdateStarfieldWormholeGateInfo;
        private static DelegateBridge __Hotfix_GetWormholeGateInfoList;
        private static DelegateBridge __Hotfix_GetRareWormholeGateInfoList;

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholeStarGroupInfo> GetRareWormholeGateInfoList(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, ConfigDataWormholeStarGroupInfo> GetWormholeGateInfoList(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetWormholeInfoDataVersion(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetRareWormholeStargroupInfo(int starfieldId, int entrySolarSystemId, out ConfigDataWormholeStarGroupInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetWormholeStargroupInfo(int starfieldId, int entrySolarSystemId, out ConfigDataWormholeStarGroupInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarfieldWormholeGateInfo(uint dataVersion, int starfieldId, List<IdLevelInfo> gateInfoList, List<IdLevelInfo> rareGateInfoList)
        {
        }
    }
}

