﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockCharacterChipClient : LogicBlockCharacterChipBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ChipSet;
        private static DelegateBridge __Hotfix_ChipUnSet;
        private static DelegateBridge __Hotfix_ChipSchemeUnlock;
        private static DelegateBridge __Hotfix_ChipSchemeSwitch;
        private static DelegateBridge __Hotfix_GetEquippedChipCountBySuitId;
        private static DelegateBridge __Hotfix_GetSchemeList;
        private static DelegateBridge __Hotfix_GetCurrSchemeIndex;
        private static DelegateBridge __Hotfix_GetChipSchemeByIndex;

        [MethodImpl(0x8000)]
        public void ChipSchemeSwitch(int switchIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void ChipSchemeUnlock(int needTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void ChipSet(int slotIndex, int itemStoreIndex, CharChipSlotInfo chipSlotInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ChipUnSet(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public CharChipSchemeInfo GetChipSchemeByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrSchemeIndex()
        {
        }

        [MethodImpl(0x8000)]
        public int GetEquippedChipCountBySuitId(int suitId)
        {
        }

        [MethodImpl(0x8000)]
        public List<CharChipSchemeInfo> GetSchemeList()
        {
        }
    }
}

