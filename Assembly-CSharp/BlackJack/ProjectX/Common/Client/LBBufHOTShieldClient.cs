﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBBufHOTShieldClient : LBBufHotBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsPercentHOT>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnPeriod;
        private static DelegateBridge __Hotfix_get_IsPercentHOT;
        private static DelegateBridge __Hotfix_set_IsPercentHOT;

        [MethodImpl(0x8000)]
        public LBBufHOTShieldClient(ConfigDataBufInfo confInfo, uint instanceId, bool isPercentHOT)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPeriod(uint tickTime)
        {
        }

        public bool IsPercentHOT
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

