﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompDroneFireControllClient : LogicBlockShipCompDroneFireControllBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBSpaceProcessDroneDefenderLaunch.AttackChance> EventOnDefenderAttackMissile;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBSpaceProcessDroneFighterLaunch> EventOnDefenderDestoryDroneFighter;
        protected List<LBSpaceProcessDroneDefenderLaunch.AttackChance> m_occupyedChanceList;
        protected List<LBSpaceProcessDroneDefenderLaunch.AttackChance> m_postChanceList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnSyncEventDroneDefenderPreCalcAttackMissile;
        private static DelegateBridge __Hotfix_OnSyncEventDroneDefenderDestoryEnergyDrone;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnDefenderAttackMissile;
        private static DelegateBridge __Hotfix_ComparationForOccupyedAttackChanceListSort;
        private static DelegateBridge __Hotfix_OnPostServerSyncEvent;
        private static DelegateBridge __Hotfix_add_EventOnDefenderAttackMissile;
        private static DelegateBridge __Hotfix_remove_EventOnDefenderAttackMissile;
        private static DelegateBridge __Hotfix_add_EventOnDefenderDestoryDroneFighter;
        private static DelegateBridge __Hotfix_remove_EventOnDefenderDestoryDroneFighter;

        public event Action<LBSpaceProcessDroneDefenderLaunch.AttackChance> EventOnDefenderAttackMissile
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBSpaceProcessDroneFighterLaunch> EventOnDefenderDestoryDroneFighter
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected int ComparationForOccupyedAttackChanceListSort(LBSpaceProcessDroneDefenderLaunch.AttackChance elem1, LBSpaceProcessDroneDefenderLaunch.AttackChance elem2)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDefenderAttackMissile(LBSpaceProcessDroneDefenderLaunch.AttackChance chance)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPostServerSyncEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventDroneDefenderDestoryEnergyDrone(LBSyncEventDroneDestoryedByDefender syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSyncEventDroneDefenderPreCalcAttackMissile(LBSyncEventDroneDefenderPreCalcAttackMissile syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

