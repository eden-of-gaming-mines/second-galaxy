﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AllianceMemberListDataSection
    {
        internal readonly List<AllianceMemberDataSection> m_memberList;
        internal readonly List<AllianceMemberDataSection> m_toBeRemovedMemberList;
        protected readonly List<AllianceMemberInfo> m_cachedMemberList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RemoveMember;
        private static DelegateBridge __Hotfix_GetMemberList;

        [MethodImpl(0x8000)]
        public List<AllianceMemberInfo> GetMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(List<AllianceMemberInfo> memberList)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveMember(uint guildId, GuildAllianceLeaveReason reason)
        {
        }
    }
}

