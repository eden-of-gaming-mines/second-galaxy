﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;

    public interface IEquipFunctionCompInOutRangeTriggerOwnerClient : IEquipFunctionCompInOutRangeTriggerOwnerBase, ILBSpaceProcessEquipLaunchSource, IEquipFunctionCompOwner, ILBSpaceProcessSource
    {
    }
}

