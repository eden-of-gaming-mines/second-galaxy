﻿namespace BlackJack.ProjectX.Common.Client
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SolarSystemGlobalSceneSelectorDefault : SolarSystemGlobalSceneSelector
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_TryToSelectGlobalScenes;
        private static DelegateBridge __Hotfix_IsPermissiveToSelectGlobalScenes;
        private static DelegateBridge __Hotfix_OnGlobalSceneRemove;

        [MethodImpl(0x8000)]
        public override bool IsPermissiveToSelectGlobalScenes()
        {
        }

        [MethodImpl(0x8000)]
        public override void OnGlobalSceneRemove(uint sceneObjId)
        {
        }

        [MethodImpl(0x8000)]
        public override void TryToSelectGlobalScenes(LinkedList<GlobalSceneInfo> srcGlobalSceneList)
        {
        }
    }
}

