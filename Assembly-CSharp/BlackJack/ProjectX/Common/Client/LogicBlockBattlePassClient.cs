﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockBattlePassClient : LogicBlockBattlePassBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnBattlePassRefresh;
        private static DelegateBridge __Hotfix_OnRmbUpgrade;
        private static DelegateBridge __Hotfix_OnLevelRewardBalance;
        private static DelegateBridge __Hotfix_OnAllLevelRewardBalance;
        private static DelegateBridge __Hotfix_OnLevelPurchase;
        private static DelegateBridge __Hotfix_OnChallangeQuestAccRewardBalance;
        private static DelegateBridge __Hotfix_HasAnyLevelReward2Balance;
        private static DelegateBridge __Hotfix_HasAnyChallangeQuestAccReward2Balance;
        private static DelegateBridge __Hotfix_GetCurrChallangeQuestAccRewardIndex;
        private static DelegateBridge __Hotfix_HasAnyBattlePassQuestReward2Balance;

        [MethodImpl(0x8000)]
        public int GetCurrChallangeQuestAccRewardIndex()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyBattlePassQuestReward2Balance()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyChallangeQuestAccReward2Balance()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyLevelReward2Balance()
        {
        }

        [MethodImpl(0x8000)]
        public void OnAllLevelRewardBalance()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBattlePassRefresh(int periodId, int challangeGroupIndex, DateTime periodStartTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnChallangeQuestAccRewardBalance(int accRewardIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLevelPurchase(int buyLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLevelRewardBalance(int level, bool isUpgrade)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRmbUpgrade(bool isPackage, int exp)
        {
        }
    }
}

