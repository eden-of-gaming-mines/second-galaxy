﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class EquipFunctionCompInvisibleClient : EquipFunctionCompInvisibleBase, ILBSpaceProcessEquipInvisibleLaunchSource, ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnSyncEventLaunch;
        private static DelegateBridge __Hotfix_OnSyncEventOnInvisibleEnd;
        private static DelegateBridge __Hotfix_GetOwner;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessEnd;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessSource.OnProcessCancel;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessFire;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipLaunchSource.OnProcessEffect;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBSpaceProcessEquipInvisibleLaunchSource.OnProcessLoop;

        [MethodImpl(0x8000)]
        public EquipFunctionCompInvisibleClient(IEquipFunctionCompInvisibleOwnerClient owner)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipInvisibleLaunchSource.OnProcessLoop(LBSpaceProcessEquipInvisibleLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessFire(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessEquipLaunchSource.OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        void ILBSpaceProcessSource.OnProcessEnd(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        protected IEquipFunctionCompInvisibleOwnerClient GetOwner()
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventLaunch(LBSpaceProcessEquipInvisibleLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnSyncEventOnInvisibleEnd()
        {
        }
    }
}

