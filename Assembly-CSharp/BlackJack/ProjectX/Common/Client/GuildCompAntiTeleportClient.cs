﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildCompAntiTeleportClient : GuildCompAntiTeleportBase, IGuildCompAntiTeleportClient, IGuildCompAntiTeleportBase, IGuildAntiTeleportClient, IGuildAntiTeleportBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckForActivateAntiTeleportEffect;
        private static DelegateBridge __Hotfix_UpdateGuildAntiTeleportInfoList;
        private static DelegateBridge __Hotfix_GetAntiTeleportInfoVersion;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompAntiTeleportClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CheckForActivateAntiTeleportEffect(string gameUserId, ulong buildingInsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int GetAntiTeleportInfoVersion()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildAntiTeleportInfoList(List<GuildAntiTeleportEffectInfo> list, int dataVersion)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

