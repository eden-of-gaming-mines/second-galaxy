﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompBattleClient : GuildCompBattleBase, IGuildCompBattleClient, IGuildCompBattleBase, IGuildBattleClient, IGuildBattleBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GuildBattleReinforcementEndTimeSet;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompBattleClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildBattleReinforcementEndTimeSet(int endHour, int endMinuts)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

