﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ClientStoreItemBaseHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetItemConfigData;
        private static DelegateBridge __Hotfix_CanSale_1;
        private static DelegateBridge __Hotfix_CanSale_0;
        private static DelegateBridge __Hotfix_CanDestory_1;
        private static DelegateBridge __Hotfix_CanDestory_0;
        private static DelegateBridge __Hotfix_GetStoreItemBindInfo;
        private static DelegateBridge __Hotfix_GetItemIconResFullPathWithConfigInfo;
        private static DelegateBridge __Hotfix_GetItemConfigId;
        private static DelegateBridge __Hotfix_GetItemIconResFullPath_1;
        private static DelegateBridge __Hotfix_GetBattleCurrencyIconResFullPath;
        private static DelegateBridge __Hotfix_GetItemIconResFullPath_0;
        private static DelegateBridge __Hotfix_GetItemLeftBottomIconResFullPath_0;
        private static DelegateBridge __Hotfix_GetItemLeftBottomIconResFullPath_1;
        private static DelegateBridge __Hotfix_GetSlotTypeIconResPathBySlotType;
        private static DelegateBridge __Hotfix_GetShipTypeIconByShipType;
        private static DelegateBridge __Hotfix_GetItemRankLevel_0;
        private static DelegateBridge __Hotfix_GetItemRankLevelStrKey_0;
        private static DelegateBridge __Hotfix_GetItemRankLevel_1;
        private static DelegateBridge __Hotfix_GetItemRankLevelStrKey_1;
        private static DelegateBridge __Hotfix_GetItemRankLevelStrFKey;
        private static DelegateBridge __Hotfix_GetItemRankLevelResFullPath;
        private static DelegateBridge __Hotfix_GetItemRankBigImageResFullPath;
        private static DelegateBridge __Hotfix_GetItemRankLevelResFullPathWithoutShadow;
        private static DelegateBridge __Hotfix_GetItemSubRankLevel_0;
        private static DelegateBridge __Hotfix_GetItemSubRankLevelStrKey_1;
        private static DelegateBridge __Hotfix_GetItemSubRankLevel_1;
        private static DelegateBridge __Hotfix_GetItemSubRankLevelStrKey_2;
        private static DelegateBridge __Hotfix_GetItemSubRankLevelStrKey_0;
        private static DelegateBridge __Hotfix_GetItemSubRankLevelResFullPath;
        private static DelegateBridge __Hotfix_GetItemSubRankLevelResFullPathWithOutShadow;
        private static DelegateBridge __Hotfix_GetItemSubRankLevelColorStr_1;
        private static DelegateBridge __Hotfix_GetItemSubRankLevelColorStr_0;
        private static DelegateBridge __Hotfix_GetItemNameStringKey_0;
        private static DelegateBridge __Hotfix_GetItemNameStringKey_1;
        private static DelegateBridge __Hotfix_GetItemNameStringKey_2;
        private static DelegateBridge __Hotfix_GetItemSizeType_0;
        private static DelegateBridge __Hotfix_GetItemSizeTypeResFullPath;
        private static DelegateBridge __Hotfix_GetItemSizeType_1;
        private static DelegateBridge __Hotfix_GetItemSizeTypeStringKey_0;
        private static DelegateBridge __Hotfix_GetItemSizeTypeStringKey_1;
        private static DelegateBridge __Hotfix_GetItemGeneralTypeDescStringKey_1;
        private static DelegateBridge __Hotfix_GetItemGeneralTypeDescStringKey_0;
        private static DelegateBridge __Hotfix_GetItemDetailTypeDescStringKey_0;
        private static DelegateBridge __Hotfix_GetItemDetailTypeDescStringKey_1;
        private static DelegateBridge __Hotfix_GetWeaponTypeStringKeyForAmmo;
        private static DelegateBridge __Hotfix_GetItemGrandFactionStringKey_0;
        private static DelegateBridge __Hotfix_GetItemGrandFactionStringKey_1;
        private static DelegateBridge __Hotfix_GetItemDescriptionStringkey_0;
        private static DelegateBridge __Hotfix_GetItemDescriptionStringkey_1;
        private static DelegateBridge __Hotfix_GetShipArtResPath;
        private static DelegateBridge __Hotfix_GetShipArtLOD0ResPath;
        private static DelegateBridge __Hotfix_GetPackedSize;
        private static DelegateBridge __Hotfix_GetItemSalePrice_0;
        private static DelegateBridge __Hotfix_GetItemSalePrice_1;
        private static DelegateBridge __Hotfix_IsItemHasDetailInfo_0;
        private static DelegateBridge __Hotfix_IsItemHasDetailInfo_1;
        private static DelegateBridge __Hotfix_GetNpcShopItemTypeFromItemTypeInfo;
        private static DelegateBridge __Hotfix_GetNpcShopItemTypeFromBlueprintId;
        private static DelegateBridge __Hotfix_GetBlueprintTypeFromItemTypeInfo;
        private static DelegateBridge __Hotfix_GetBlueprintCategoryFormItemTypeInfo;
        private static DelegateBridge __Hotfix_CanBuyAmmoUseBindMoney;
        private static DelegateBridge __Hotfix_GetItemObtainSourceTypeList;
        private static DelegateBridge __Hotfix_GetItemObtainSourceTypeInfoList;
        private static DelegateBridge __Hotfix_IsAutomaticSaleItem;
        private static DelegateBridge __Hotfix_IsStoreItemRecommend_0;
        private static DelegateBridge __Hotfix_IsStoreItemRecommend_1;
        private static DelegateBridge __Hotfix_GetBlueprintLeftBottomSlotResFullPath;
        private static DelegateBridge __Hotfix_GetBlueprintSizeType;
        private static DelegateBridge __Hotfix_GetShipTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetEquipTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetDroneTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetMissileTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetNormalAmmoTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetWeaponCategoryDescStringKey;
        private static DelegateBridge __Hotfix_GetNormalItemTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetWeaponTypeDescStringKey;
        private static DelegateBridge __Hotfix_GetGrandFactionNameStringKey;
        private static DelegateBridge __Hotfix_GetEquipCategoryDescStringKey;

        [MethodImpl(0x8000)]
        public static bool CanBuyAmmoUseBindMoney(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanDestory(StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanDestory(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanSale(StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanSale(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetBattleCurrencyIconResFullPath(CurrencyType currencyType)
        {
        }

        [MethodImpl(0x8000)]
        public static BlueprintCategory GetBlueprintCategoryFormItemTypeInfo(StoreItemType type, int equipConfigId = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetBlueprintLeftBottomSlotResFullPath(ConfigDataProduceBlueprintInfo blueprintConfigData)
        {
        }

        [MethodImpl(0x8000)]
        protected static ShipSizeType GetBlueprintSizeType(ConfigDataProduceBlueprintInfo blueprintConfigData)
        {
        }

        [MethodImpl(0x8000)]
        public static BlueprintType GetBlueprintTypeFromItemTypeInfo(StoreItemType type, int configId)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetDroneTypeDescStringKey(DroneType droneType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipCategoryDescStringKey(EquipCategory equipCategory)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetEquipTypeDescStringKey(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetGrandFactionNameStringKey(GrandFaction gfType)
        {
        }

        [MethodImpl(0x8000)]
        public static object GetItemConfigData(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetItemConfigId(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemDescriptionStringkey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemDescriptionStringkey(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemDetailTypeDescStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemDetailTypeDescStringKey(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemGeneralTypeDescStringKey(StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemGeneralTypeDescStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemGrandFactionStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemGrandFactionStringKey(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemIconResFullPath(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemIconResFullPath(StoreItemType itemType, int confId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemIconResFullPathWithConfigInfo(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemLeftBottomIconResFullPath(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemLeftBottomIconResFullPath(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemNameStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemNameStringKey(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemNameStringKey(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ConfigDataItemObtainSourceTypeInfo> GetItemObtainSourceTypeInfoList(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static List<ItemObtainSourceType> GetItemObtainSourceTypeList(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemRankBigImageResFullPath(RankType type)
        {
        }

        [MethodImpl(0x8000)]
        public static RankType GetItemRankLevel(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static RankType GetItemRankLevel(object configInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemRankLevelResFullPath(RankType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemRankLevelResFullPathWithoutShadow(RankType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemRankLevelStrFKey(RankType rankType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemRankLevelStrKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemRankLevelStrKey(object configInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetItemSalePrice(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetItemSalePrice(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipSizeType GetItemSizeType(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipSizeType GetItemSizeType(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSizeTypeResFullPath(ShipSizeType sizeType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSizeTypeStringKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSizeTypeStringKey(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static SubRankType GetItemSubRankLevel(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static SubRankType GetItemSubRankLevel(object configInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSubRankLevelColorStr(SubRankType subRank)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSubRankLevelColorStr(object configInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSubRankLevelResFullPath(SubRankType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSubRankLevelResFullPathWithOutShadow(SubRankType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSubRankLevelStrKey(SubRankType subRankType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSubRankLevelStrKey(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemSubRankLevelStrKey(object configInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetMissileTypeDescStringKey()
        {
        }

        [MethodImpl(0x8000)]
        private static string GetNormalAmmoTypeDescStringKey(WeaponCategory weaponCat)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetNormalItemTypeDescStringKey(NormalItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcShopItemType GetNpcShopItemTypeFromBlueprintId(int blueprintId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetNpcShopItemTypeFromItemTypeInfo(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetPackedSize(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipArtLOD0ResPath(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipArtResPath(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetShipTypeDescStringKey(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetShipTypeIconByShipType(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetSlotTypeIconResPathBySlotType(ShipEquipSlotType slotType)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetStoreItemBindInfo(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWeaponCategoryDescStringKey(WeaponCategory weaponCat)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetWeaponTypeDescStringKey(WeaponType weaponType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWeaponTypeStringKeyForAmmo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsAutomaticSaleItem(int shipItemID)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsItemHasDetailInfo(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsItemHasDetailInfo(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsStoreItemRecommend(ILBStoreItemClient srcItem, ILBStoreItemClient targetItem)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsStoreItemRecommend(StoreItemType srcItemType, int srcItemConfigId, StoreItemType destItemType, int destItemConfigId)
        {
        }
    }
}

