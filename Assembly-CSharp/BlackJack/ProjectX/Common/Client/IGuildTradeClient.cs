﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildTradeClient : IGuildTradeBase
    {
        void GuildTradePortExtraInfoListUpdate(List<GuildTradePortExtraInfo> guildTradePortExtraInfoList, ushort dataVersion);
        void GuildTradePortExtraInfoUpdate(GuildTradePortExtraInfo guildTradePortExtraInfo);
        void OnGuildTradePurchaseOrderCreate(int solarSystemId, ulong purchaseInstanceId);
        void OnGuildTradePurchaseOrderRemove(int solarSystemId, ulong purchaseInstanceId);
        void OnGuildTradeTransportPlanComplete(int solarSystemId, ulong transportInstanceId);
        void OnGuildTradeTransportPlanCreate(int solarSystemId, ulong transportInstanceId);
    }
}

