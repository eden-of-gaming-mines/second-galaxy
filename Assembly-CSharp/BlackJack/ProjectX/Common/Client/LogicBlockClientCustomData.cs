﻿namespace BlackJack.ProjectX.Common.Client
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockClientCustomData
    {
        private Dictionary<string, long> m_actionPlanDataDict;
        private Dictionary<string, long> m_guildTradeDict;
        private ILBClientCustomDataDCProvider m_playerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetBranchStoryData;
        private static DelegateBridge __Hotfix_SetBranchStroyData;
        private static DelegateBridge __Hotfix_IsBuildingLostReportRead;
        private static DelegateBridge __Hotfix_IsSovereignLostReportRead;
        private static DelegateBridge __Hotfix_AddHaveReadBuildingLostReport;
        private static DelegateBridge __Hotfix_AddHaveReadSovereignLostReport;
        private static DelegateBridge __Hotfix_IsDestoryedGuildFlagShipRead;
        private static DelegateBridge __Hotfix_AddHaveReadDestoryedGuildFlagShip;
        private static DelegateBridge __Hotfix_RemoveHaveReadDestoryedGuildFlagShip;
        private static DelegateBridge __Hotfix_ClearHaveReadLostReport;
        private static DelegateBridge __Hotfix_SetActionPlanData;
        private static DelegateBridge __Hotfix_TryGetActionPlanData;
        private static DelegateBridge __Hotfix_SetOrUpdateTradeData;
        private static DelegateBridge __Hotfix_ClearTradeData;
        private static DelegateBridge __Hotfix_TryGetTradeData;
        private static DelegateBridge __Hotfix_AddOrUpdateCustomData_1;
        private static DelegateBridge __Hotfix_AddOrUpdateCustomData_0;
        private static DelegateBridge __Hotfix_TryGetCustomData_1;
        private static DelegateBridge __Hotfix_TryGetCustomData_0;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public void AddHaveReadBuildingLostReport(ulong reportId)
        {
        }

        [MethodImpl(0x8000)]
        public void AddHaveReadDestoryedGuildFlagShip(ulong shipInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void AddHaveReadSovereignLostReport(ulong reportId)
        {
        }

        [MethodImpl(0x8000)]
        public void AddOrUpdateCustomData(string dataStrKey, bool dataValue)
        {
        }

        [MethodImpl(0x8000)]
        public void AddOrUpdateCustomData(string dataStrKey, long dataValue)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearHaveReadLostReport()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearTradeData(string key)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetBranchStoryData(string key)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBClientCustomDataDCProvider lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBuildingLostReportRead(ulong reportId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDestoryedGuildFlagShipRead(ulong shipInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSovereignLostReportRead(ulong reportId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveHaveReadDestoryedGuildFlagShip(ulong shipInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetActionPlanData(string key, long data)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBranchStroyData(string key, bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOrUpdateTradeData(string key, long data)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetActionPlanData(string key, out long data)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetCustomData(string dataStrKey, out bool dataValue)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetCustomData(string dataStrKey, out long dataValue)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetTradeData(string key, out long data)
        {
        }

        private IClientCustomDataContainer DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

