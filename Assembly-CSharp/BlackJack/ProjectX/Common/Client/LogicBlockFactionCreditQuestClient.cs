﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockFactionCreditQuestClient : LogicBlockFactionCreditQuestBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnGetFactionCreditQuestWeeklyReward;
        private static DelegateBridge __Hotfix_OnGetFactionCreditLevelReward;
        private static DelegateBridge __Hotfix_OnFactionInfoUpdate;

        [MethodImpl(0x8000)]
        public void OnFactionInfoUpdate(int factionCreditQuestCompleteCount, List<int> creditQuestWeeklyRewardGetList)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGetFactionCreditLevelReward(int id, GrandFaction grandFaction, int rewardId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGetFactionCreditQuestWeeklyReward(int id, GrandFaction grandFaction)
        {
        }
    }
}

