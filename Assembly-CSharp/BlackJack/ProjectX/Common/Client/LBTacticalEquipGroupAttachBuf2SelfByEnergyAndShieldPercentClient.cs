﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupAttachBuf2SelfByEnergyAndShieldPercentClient : LBTacticalEquipGroupAttachBuf2SelfByEnergyAndShieldPercentBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool> EventOnAttachBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnDetachBuf;
        private static DelegateBridge __Hotfix_add_EventOnAttachBuf;
        private static DelegateBridge __Hotfix_remove_EventOnAttachBuf;

        public event Action<bool, bool> EventOnAttachBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupAttachBuf2SelfByEnergyAndShieldPercentClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDetachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

