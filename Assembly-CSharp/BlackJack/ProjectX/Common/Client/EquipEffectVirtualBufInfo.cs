﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class EquipEffectVirtualBufInfo
    {
        protected bool m_isEasyChange;
        protected FakeViewBufLifeType m_lifeType;
        protected TacticalEquipFunctionType m_functionType;
        protected Func<ILBSpaceTarget, float> FactorGetFun;
        protected float m_factor;
        protected DateTime m_nextCalcTime;
        protected ILBSpaceContext m_spaceContext;
        protected ConfigDataEquipEffectVirtualBufInfo m_configInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetNameStrKey;
        private static DelegateBridge __Hotfix_GetDescStrKey;
        private static DelegateBridge __Hotfix_IsEnhance;
        private static DelegateBridge __Hotfix_GetDescParams;
        private static DelegateBridge __Hotfix_GetBufType;
        private static DelegateBridge __Hotfix_GetFactor;
        private static DelegateBridge __Hotfix_SetFactorFunc;
        private static DelegateBridge __Hotfix_SetSpaceCtx;
        private static DelegateBridge __Hotfix_get_IsEasyChange;
        private static DelegateBridge __Hotfix_get_LifeType;
        private static DelegateBridge __Hotfix_get_FunctionType;

        [MethodImpl(0x8000)]
        public EquipEffectVirtualBufInfo(ConfigDataEquipEffectVirtualBufInfo configInfo, FakeViewBufLifeType type, TacticalEquipFunctionType tacticalEquiptype, bool isEasyChange = false)
        {
        }

        [MethodImpl(0x8000)]
        public ShipFightBufType GetBufType()
        {
        }

        [MethodImpl(0x8000)]
        public List<ParamInfo> GetDescParams()
        {
        }

        [MethodImpl(0x8000)]
        public string GetDescStrKey()
        {
        }

        [MethodImpl(0x8000)]
        public float GetFactor(ILBSpaceTarget target = null, bool isNeedReCalc = false)
        {
        }

        [MethodImpl(0x8000)]
        public string GetNameStrKey()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEnhance()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFactorFunc(Func<ILBSpaceTarget, float> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceCtx(ILBSpaceContext spaceContext)
        {
        }

        public bool IsEasyChange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public FakeViewBufLifeType LifeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public TacticalEquipFunctionType FunctionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

