﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockCrackClient : LogicBlockCrackBase
    {
        private List<int> m_CrackedBoxStateList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CrackSlotExtend;
        private static DelegateBridge __Hotfix_CrackQueueAddBox;
        private static DelegateBridge __Hotfix_CrackQueueRemoveBox;
        private static DelegateBridge __Hotfix_CrackSpeedUpByRealMoney;
        private static DelegateBridge __Hotfix_CrackedBoxGetReward;
        private static DelegateBridge __Hotfix_CrackCompleteNtf;
        private static DelegateBridge __Hotfix_GetCrackedBoxStateList;
        private static DelegateBridge __Hotfix_CrackedAnimationPlay;

        [MethodImpl(0x8000)]
        public void CrackCompleteNtf(int boxItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void CrackedAnimationPlay(int boxItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void CrackedBoxGetReward(int boxItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool CrackQueueAddBox(int storeItemIndex, int suspendBoxItemIndex, ulong suspendBoxItemInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void CrackQueueRemoveBox(int boxItemIndex, int resumeBoxItemIndex, ulong resumeBoxItemInsId)
        {
        }

        [MethodImpl(0x8000)]
        public void CrackSlotExtend(int extendIndex, int realMoneyCost)
        {
        }

        [MethodImpl(0x8000)]
        public void CrackSpeedUpByRealMoney(int realMoneyCost)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetCrackedBoxStateList()
        {
        }
    }
}

