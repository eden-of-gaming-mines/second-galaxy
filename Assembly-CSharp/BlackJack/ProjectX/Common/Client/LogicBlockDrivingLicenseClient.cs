﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockDrivingLicenseClient : LogicBlockDrivingLicenseBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_DrivingLicenseUpgradeNtf;
        private static DelegateBridge __Hotfix_DrivingAssessmentStart;

        [MethodImpl(0x8000)]
        public void DrivingAssessmentStart(int skillId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public void DrivingLicenseUpgradeNtf(int skillId, int level)
        {
        }
    }
}

