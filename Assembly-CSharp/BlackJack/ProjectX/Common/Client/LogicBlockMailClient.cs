﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockMailClient : LogicBlockMailBase
    {
        public DateTime LastMailChangeTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddNewMail2Store;
        private static DelegateBridge __Hotfix_SetMailReaded;
        private static DelegateBridge __Hotfix_RemoveMail;
        private static DelegateBridge __Hotfix_RemoveAttachment;
        private static DelegateBridge __Hotfix_GetAllMailList;
        private static DelegateBridge __Hotfix_GetMailList;
        private static DelegateBridge __Hotfix_GetFilteredMailList;
        private static DelegateBridge __Hotfix_CreateLBStoredMailByMailInfo;

        [MethodImpl(0x8000)]
        public void AddNewMail2Store(MailInfo mailInfo, int storedIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override LBStoredMail CreateLBStoredMailByMailInfo(StoredMailInfo mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void GetAllMailList(List<int> storedIndexList)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoredMail> GetFilteredMailList(List<Func<LBStoredMail, bool>> filterlist)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoredMail> GetMailList()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAttachment(int storedIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveMail(int storedIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMailReaded(int storedIndex)
        {
        }
    }
}

