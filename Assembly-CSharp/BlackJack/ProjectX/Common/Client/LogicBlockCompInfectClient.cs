﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCompInfectClient
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsCurrSolarSystemInfected>k__BackingField;
        private Dictionary<int, DateTime> m_nextSelfHealingTimeDict;
        private Dictionary<int, Dictionary<int, SolarSystemInfectInfo>> m_solarSystemInfectProcessInfoDict;
        private Dictionary<int, uint> m_infectInfoDataVersionDict;
        private ILBPlayerContext m_lbPlayerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_TryGetInfectInfo;
        private static DelegateBridge __Hotfix_GetInfectInfoDataVersion;
        private static DelegateBridge __Hotfix_TryGetStarfieldSelfHealingTime;
        private static DelegateBridge __Hotfix_UpdateStarfieldInfectInfo;
        private static DelegateBridge __Hotfix_UpdateSolarSystemInfectInfo;
        private static DelegateBridge __Hotfix_GetInfectInfoDict;
        private static DelegateBridge __Hotfix_TryRemoveInfectInfo;
        private static DelegateBridge __Hotfix_get_IsCurrSolarSystemInfected;
        private static DelegateBridge __Hotfix_set_IsCurrSolarSystemInfected;

        [MethodImpl(0x8000)]
        public uint GetInfectInfoDataVersion(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, SolarSystemInfectInfo> GetInfectInfoDict(int starfieldId)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetInfectInfo(int starfieldId, int solarSystemId, out SolarSystemInfectInfo infectInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetStarfieldSelfHealingTime(int starfieldId, out DateTime time)
        {
        }

        [MethodImpl(0x8000)]
        private bool TryRemoveInfectInfo(int starfieldId, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemInfectInfo(int solarSystemId, float infectProgress, bool finalBattleOpen, long selfHealingTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarfieldInfectInfo(uint dataVersion, int starfieldId, long nextInfectRefreshTime, long nextSelfHealingTime, List<ProInfectInfo> infectInfoList)
        {
        }

        public bool IsCurrSolarSystemInfected
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public class SolarSystemInfectInfo
        {
            public int m_solarSystemId;
            public float m_infectProgress;
            public bool m_isFinalBattleOpen;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

