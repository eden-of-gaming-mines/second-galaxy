﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCompStaticBasicCtxClient : LogicBlockShipCompStaticBasicCtx
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsInMission;
        private static DelegateBridge __Hotfix_IsDestroyed;
        private static DelegateBridge __Hotfix_IsCPUEnough4TackOff;
        private static DelegateBridge __Hotfix_IsPowerEnough4TackOff;
        private static DelegateBridge __Hotfix_GetShipTypeDescStrKey;
        private static DelegateBridge __Hotfix_GetShipDescriptionStringkey;
        private static DelegateBridge __Hotfix_GetStringTableIdByShipType;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_GetShipTechLevel;
        private static DelegateBridge __Hotfix_GetShipTechLevelIconResoucePath;
        private static DelegateBridge __Hotfix_GetShipCPUMaxVaule;
        private static DelegateBridge __Hotfix_GetShipPowerMaxValue;
        private static DelegateBridge __Hotfix_GetShipEquimentValue;

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtxClient(ILBStaticShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipCPUMaxVaule()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipDescriptionStringkey()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipEquimentValue()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetShipName(out string shipName)
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipPowerMaxValue()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipTechLevel()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipTechLevelIconResoucePath()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipTypeDescStrKey()
        {
        }

        [MethodImpl(0x8000)]
        public StringTableId GetStringTableIdByShipType(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCPUEnough4TackOff()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDestroyed()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInMission()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPowerEnough4TackOff()
        {
        }
    }
}

