﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupReduceCDByKillingHitClient : LBTacticalEquipGroupReduceCDByKillingHitBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBTacticalEquipGroupBase> EventOnReduceCD;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnKillHitToTarget;
        private static DelegateBridge __Hotfix_add_EventOnReduceCD;
        private static DelegateBridge __Hotfix_remove_EventOnReduceCD;

        public event Action<LBTacticalEquipGroupBase> EventOnReduceCD
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupReduceCDByKillingHitClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnKillHitToTarget(ILBSpaceTarget target)
        {
        }
    }
}

