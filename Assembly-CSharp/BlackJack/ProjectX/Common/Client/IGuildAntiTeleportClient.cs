﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildAntiTeleportClient : IGuildAntiTeleportBase
    {
        int GetAntiTeleportInfoVersion();
        void UpdateGuildAntiTeleportInfoList(List<GuildAntiTeleportEffectInfo> list, int dataVersion);
    }
}

