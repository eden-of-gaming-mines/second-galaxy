﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockQuestClient : LogicBlockQuestBase
    {
        protected List<QuestWaitForAcceptInfo> m_waitingAutoAcceptQuestItemList;
        protected DateTime m_lastUnacceptQuestDataUpdateTime;
        private ILBPlayerContextClient m_playerCtxClient;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_QuestWaitForAcceptListAdd_0;
        private static DelegateBridge __Hotfix_QuestWaitForAcceptListAdd_1;
        private static DelegateBridge __Hotfix_QuestWaitForAcceptListRemove;
        private static DelegateBridge __Hotfix_GetLastUnAcceptQuestDataUpdateTime;
        private static DelegateBridge __Hotfix_QuestEnvOpen;
        private static DelegateBridge __Hotfix_QuestAccept;
        private static DelegateBridge __Hotfix_QuestCancel;
        private static DelegateBridge __Hotfix_OnQuestCompleteCondParamUpdate;
        private static DelegateBridge __Hotfix_OnQuestCompleteWait;
        private static DelegateBridge __Hotfix_OnQuestCompleteConfirm;
        private static DelegateBridge __Hotfix_QuestFail;
        private static DelegateBridge __Hotfix_QuestRetryOnFail;
        private static DelegateBridge __Hotfix_OnGMSetUnaccept2Complete;
        private static DelegateBridge __Hotfix_GetCompletedNPCSolarSystemIdByQuestId;
        private static DelegateBridge __Hotfix_GetCompletedNpcDNIdByQuestId;
        private static DelegateBridge __Hotfix_GetAccepteNpcDNIdByQuestId;
        private static DelegateBridge __Hotfix_HasAnyWaitingAutoAcceptQuest;
        private static DelegateBridge __Hotfix_GetFirstWaitingAutoAcceptQuest;
        private static DelegateBridge __Hotfix_GetQuestCompleteSolarSystemId;
        private static DelegateBridge __Hotfix_GetlastPuzzleGameQuestInstanceId;
        private static DelegateBridge __Hotfix_GetQuestInstanceIdWithParam;
        private static DelegateBridge __Hotfix_GetFristAcceptScienceExploreQuestInstanceId;
        private static DelegateBridge __Hotfix_GetFristAcceptPuzzleGameQuestInstanceId;
        private static DelegateBridge __Hotfix_GetFristFactionQuestInstanceId;
        private static DelegateBridge __Hotfix_GetFristAcceptFreeQuestInstanceIdWithType;
        private static DelegateBridge __Hotfix_GetFristUnaccptFreeQuestIdWithType;
        private static DelegateBridge __Hotfix_BalanceQuestReward;
        private static DelegateBridge __Hotfix_BalanceQuestRewardInternal;
        private static DelegateBridge __Hotfix_RefreshAutoAcceptDialogQuestList;
        private static DelegateBridge __Hotfix_get_PlayerCtxClient;
        private static DelegateBridge __Hotfix_GetVirtualBuffsInfluenceToUnacceptedQuest;
        private static DelegateBridge __Hotfix_GetVirtualBuffsInfluenceToAcceptedQuest;
        private static DelegateBridge __Hotfix_GetSolarSystemPoolCountReduceForUnaccptQuest;
        private static DelegateBridge __Hotfix_GetSolarSystemQuestPoolCountInfoForUnacceptedQuest;
        private static DelegateBridge __Hotfix_GetSolarSystemQuestPoolCountInfoForAcceptedQuest;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForDailyExtraRewardQuest;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForAcceptedSpecialQuest;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForUnacceptSpecialQuest;
        private static DelegateBridge __Hotfix_GetVirtualBuffDescForQuestPoolCount;

        [MethodImpl(0x8000)]
        protected void BalanceQuestReward(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        protected void BalanceQuestRewardInternal(QuestRewardInfo reward, LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId GetAccepteNpcDNIdByQuestId(int questId, ConfigDataQuestInfo questConf = null)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId GetCompletedNpcDNIdByQuestId(int questId, ConfigDataQuestInfo questConf = null)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCompletedNPCSolarSystemIdByQuestId(int questId, ConfigDataQuestInfo questConf = null)
        {
        }

        [MethodImpl(0x8000)]
        public QuestWaitForAcceptInfo GetFirstWaitingAutoAcceptQuest(Func<QuestWaitForAcceptInfo, bool> filter = null)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFristAcceptFreeQuestInstanceIdWithType(QuestType questType)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFristAcceptPuzzleGameQuestInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFristAcceptScienceExploreQuestInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFristFactionQuestInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFristUnaccptFreeQuestIdWithType(QuestType questType)
        {
        }

        [MethodImpl(0x8000)]
        public int GetlastPuzzleGameQuestInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetLastUnAcceptQuestDataUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestCompleteSolarSystemId(ConfigDataQuestInfo confQuest)
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestInstanceIdWithParam(int configId, int completeSolarSystemId, int questLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void GetSolarSystemPoolCountReduceForUnaccptQuest(ConfigDataQuestInfo quest, out float currencyReduce, out float expReduce)
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataSolarSystemQuestPoolCountInfo GetSolarSystemQuestPoolCountInfoForAcceptedQuest(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataSolarSystemQuestPoolCountInfo GetSolarSystemQuestPoolCountInfoForUnacceptedQuest(ConfigDataQuestInfo quest)
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForAcceptedSpecialQuest(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForDailyExtraRewardQuest(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForQuestPoolCount(ConfigDataSolarSystemQuestPoolCountInfo questPoolConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        private VirtualBuffDesc GetVirtualBuffDescForUnacceptSpecialQuest(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> GetVirtualBuffsInfluenceToAcceptedQuest(LBProcessingQuestBase questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> GetVirtualBuffsInfluenceToUnacceptedQuest(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAnyWaitingAutoAcceptQuest(Func<QuestWaitForAcceptInfo, bool> filter = null)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx, Func<int> instanceIdAllocator = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGMSetUnaccept2Complete(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestCompleteCondParamUpdate(int questInstanceId, int condIndex, int param)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestCompleteConfirm(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestCompleteWait(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void QuestAccept(QuestProcessingInfo questProcessingInfo, QuestEnvirmentInfo envInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool QuestCancel(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void QuestEnvOpen(QuestEnvirmentInfo envInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool QuestFail(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool QuestRetryOnFail(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void QuestWaitForAcceptListAdd(int questId, QuestEnvirmentInfo envInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override void QuestWaitForAcceptListAdd(int questId, int questFactionId = 0, bool fromQuestCancel = false, int questLevel = 0, int questSolarSystemId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public override void QuestWaitForAcceptListRemove(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected void RefreshAutoAcceptDialogQuestList()
        {
        }

        protected ILBPlayerContextClient PlayerCtxClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

