﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupKeepingInvisibleForWeaponLaunchClient : LBTacticalEquipGroupKeepingInvisibleForWeaponLaunchBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBTacticalEquipGroupBase> EventEnterInvisiable;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBTacticalEquipGroupBase> EventExitInvisiable;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnEnterInvisiable;
        private static DelegateBridge __Hotfix_OnExitInvisiable;
        private static DelegateBridge __Hotfix_add_EventEnterInvisiable;
        private static DelegateBridge __Hotfix_remove_EventEnterInvisiable;
        private static DelegateBridge __Hotfix_add_EventExitInvisiable;
        private static DelegateBridge __Hotfix_remove_EventExitInvisiable;

        public event Action<LBTacticalEquipGroupBase> EventEnterInvisiable
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBTacticalEquipGroupBase> EventExitInvisiable
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupKeepingInvisibleForWeaponLaunchClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnterInvisiable(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnExitInvisiable(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

