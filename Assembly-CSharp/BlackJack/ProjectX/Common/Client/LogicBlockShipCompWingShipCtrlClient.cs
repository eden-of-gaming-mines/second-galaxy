﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompWingShipCtrlClient : LogicBlockShipCompWingShipCtrlBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBInSpaceNpcShip> EventOnWingShipSet;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBInSpaceNpcShip> EventOnWingShipUnset;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBInSpaceNpcShip> EventOnWingShipEnterView;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBInSpaceNpcShip> EventOnWingShipLeaveView;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SceneWingShipSetting, bool> EventOnSceneWingShipSettingChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnWeaponLaunch2Target;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnEquipLaunch2Target;
        private ILogicBlockShipCompAOI m_lbAOI;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_SyncWingShipSet;
        private static DelegateBridge __Hotfix_SyncWingShipUnset;
        private static DelegateBridge __Hotfix_SyncSceneWingShipSettingChanged;
        private static DelegateBridge __Hotfix_WingShipEnable;
        private static DelegateBridge __Hotfix_WingShipDisable;
        private static DelegateBridge __Hotfix_OnTargetEnterView;
        private static DelegateBridge __Hotfix_OnTargetLeaveView;
        private static DelegateBridge __Hotfix_OnEnterFight;
        private static DelegateBridge __Hotfix_OnWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_OnEquipLaunch2Target;
        private static DelegateBridge __Hotfix_add_EventOnWingShipSet;
        private static DelegateBridge __Hotfix_remove_EventOnWingShipSet;
        private static DelegateBridge __Hotfix_add_EventOnWingShipUnset;
        private static DelegateBridge __Hotfix_remove_EventOnWingShipUnset;
        private static DelegateBridge __Hotfix_add_EventOnWingShipEnterView;
        private static DelegateBridge __Hotfix_remove_EventOnWingShipEnterView;
        private static DelegateBridge __Hotfix_add_EventOnWingShipLeaveView;
        private static DelegateBridge __Hotfix_remove_EventOnWingShipLeaveView;
        private static DelegateBridge __Hotfix_add_EventOnSceneWingShipSettingChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSceneWingShipSettingChanged;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunch2Target;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunch2Target;

        public event Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnEquipLaunch2Target
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SceneWingShipSetting, bool> EventOnSceneWingShipSettingChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnWeaponLaunch2Target
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceNpcShip> EventOnWingShipEnterView
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceNpcShip> EventOnWingShipLeaveView
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceNpcShip> EventOnWingShipSet
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceNpcShip> EventOnWingShipUnset
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEquipLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTargetEnterView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTargetLeaveView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeaponLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SyncSceneWingShipSettingChanged(SceneWingShipSetting setting)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncWingShipSet(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncWingShipUnset()
        {
        }

        [MethodImpl(0x8000)]
        public void WingShipDisable()
        {
        }

        [MethodImpl(0x8000)]
        public void WingShipEnable()
        {
        }
    }
}

