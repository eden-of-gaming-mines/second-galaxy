﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IGuildTradeDataContainerClient : IGuildTradeDataContainerBase
    {
        void GuildTradePortExtraInfoListUpdate(List<GuildTradePortExtraInfo> guildTradePortExtraInfoList, ushort dataVersion);
        void GuildTradePortExtraInfoUpdate(GuildTradePortExtraInfo guildTradePortExtraInfo);
    }
}

