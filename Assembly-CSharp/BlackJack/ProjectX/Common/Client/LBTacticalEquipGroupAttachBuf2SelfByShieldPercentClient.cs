﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LBTacticalEquipGroupAttachBuf2SelfByShieldPercentClient : LBTacticalEquipGroupAttachBuf2SelfByShieldPercentBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventAttachBuf;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventDetachBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnAttachBuf;
        private static DelegateBridge __Hotfix_OnDetachBuf;
        private static DelegateBridge __Hotfix_add_EventAttachBuf;
        private static DelegateBridge __Hotfix_remove_EventAttachBuf;
        private static DelegateBridge __Hotfix_add_EventDetachBuf;
        private static DelegateBridge __Hotfix_remove_EventDetachBuf;

        public event Action<bool> EventAttachBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventDetachBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupAttachBuf2SelfByShieldPercentClient(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDetachBuf(LBBufBase lbBuf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

