﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompTradeClient : GuildCompTradeBase, IGuildCompTradeClient, IGuildCompTradeBase, IGuildTradeClient, IGuildTradeBase
    {
        protected IGuildClientCompOwner m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnGuildTradePurchaseOrderCreate;
        private static DelegateBridge __Hotfix_OnGuildTradePurchaseOrderRemove;
        private static DelegateBridge __Hotfix_OnGuildTradeTransportPlanCreate;
        private static DelegateBridge __Hotfix_OnGuildTradeTransportPlanComplete;
        private static DelegateBridge __Hotfix_GuildTradePortExtraInfoUpdate;
        private static DelegateBridge __Hotfix_GuildTradePortExtraInfoListUpdate;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompTradeClient(IGuildClientCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradePortExtraInfoListUpdate(List<GuildTradePortExtraInfo> guildTradePortExtraInfoList, ushort dataVersion)
        {
        }

        [MethodImpl(0x8000)]
        public void GuildTradePortExtraInfoUpdate(GuildTradePortExtraInfo guildTradePortExtraInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradePurchaseOrderCreate(int solarSystemId, ulong purchaseInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradePurchaseOrderRemove(int solarSystemId, ulong purchaseInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradeTransportPlanComplete(int solarSystemId, ulong transportInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildTradeTransportPlanCreate(int solarSystemId, ulong transportInstanceId)
        {
        }

        protected IGuildDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

