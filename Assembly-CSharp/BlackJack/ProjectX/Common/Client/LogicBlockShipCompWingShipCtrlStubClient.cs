﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockShipCompWingShipCtrlStubClient : LogicBlockShipCompWingShipCtrlStubBase
    {
        private uint? m_masterObjId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_InitWithMaster;
        private static DelegateBridge __Hotfix_GetMasterShip;

        [MethodImpl(0x8000)]
        public override ILBInSpacePlayerShip GetMasterShip()
        {
        }

        [MethodImpl(0x8000)]
        public void InitWithMaster(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitialize()
        {
        }
    }
}

