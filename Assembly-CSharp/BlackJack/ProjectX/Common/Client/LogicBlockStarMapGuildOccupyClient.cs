﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockStarMapGuildOccupyClient
    {
        protected Dictionary<int, DateTime> m_starFieldLastCheckTimeDict;
        protected ILBPlayerContext m_playerCtx;
        protected IStarMapGuildOccupyDataContainer m_dc;
        protected StarMapGuildOccupyInfoManager m_starMapGuildOccupyInfoManager;
        private const int StarFieldInfoTimeOutCD = 60;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_IsStarMapGuildOccupyInfo4StarFieldTimeOut;
        private static DelegateBridge __Hotfix_UpdateStarMapGuildOccupyInfo4StarFieldTimeOut;
        private static DelegateBridge __Hotfix_StarMapGuildOccupyStaticInfoUpdate;
        private static DelegateBridge __Hotfix_StarMapGuildOccupyDynamicInfoUpdate;
        private static DelegateBridge __Hotfix_GetStarFieldGuildOccupyStaticInfoVersion;
        private static DelegateBridge __Hotfix_GetStarFieldGuildOccupyDynamicInfoVersion;
        private static DelegateBridge __Hotfix_GetStarMapGuildOccupyStaticInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildOccupyDynamicInfo;
        private static DelegateBridge __Hotfix_IsSolarSystemOccupyable;
        private static DelegateBridge __Hotfix_GetSolarSystemOccpuyGuild;
        private static DelegateBridge __Hotfix_GetSolarSystemDynamicInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildSimpleInfo;
        private static DelegateBridge __Hotfix_GetStarMapAllianceSimpleInfo;
        private static DelegateBridge __Hotfix_GetStarMapSolarSystemGuildOccupyDict;

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo GetSolarSystemDynamicInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetSolarSystemOccpuyGuild(int solarSystemId, out bool isNpcGuild)
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetStarFieldGuildOccupyDynamicInfoVersion(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetStarFieldGuildOccupyStaticInfoVersion(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapAllianceSimpleInfo GetStarMapAllianceSimpleInfo(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo GetStarMapGuildOccupyDynamicInfo(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyStaticInfo GetStarMapGuildOccupyStaticInfo(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo GetStarMapGuildSimpleInfo(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, uint> GetStarMapSolarSystemGuildOccupyDict()
        {
        }

        [MethodImpl(0x8000)]
        public void Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemOccupyable(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStarMapGuildOccupyInfo4StarFieldTimeOut(int starFieldId, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapGuildOccupyDynamicInfoUpdate(StarMapGuildOccupyDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapGuildOccupyStaticInfoUpdate(StarMapGuildOccupyStaticInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarMapGuildOccupyInfo4StarFieldTimeOut(int starFieldId, DateTime currTime)
        {
        }
    }
}

