﻿namespace BlackJack.ProjectX.Common.Client
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using System;

    public interface IGuildCurrencyClient : IGuildCurrencyBase
    {
        void GuildTradeMoneyModify(long changeValue, GuildCurrencyLogType logType);
        void OnGuildWalletAnnouncementSetAck(string announcement);
        void UpdateGuildCurrencyInfo(GuildCurrencyInfo currency);
    }
}

