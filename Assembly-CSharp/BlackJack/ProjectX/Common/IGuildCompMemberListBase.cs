﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildCompMemberListBase : IGuildMemberListBase
    {
        void AddMember(GuildMemberBase member);
        GuildMemberBase AddNewMemberFromDC(string gameUserId);
        GuildMemberBase GetMemberBySeq(ushort seq);
        void RemoveMember(string playerGameUserId);
    }
}

