﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class NpcDNId
    {
        public int SolarSystemId;
        public int SpaceStationId;
        public int NpcId;
        public bool IsGlobalLink;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Equals;
        private static DelegateBridge __Hotfix_EqualsForQuest;

        [MethodImpl(0x8000)]
        public NpcDNId(int npcDNIdNpcId)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId(int npcDNIdSolarSystemId, int npcDNIdSpaceStationId, int npcDNIdNpcId, bool isGlobalLink = false)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Equals(NpcDNId id1, NpcDNId id2)
        {
        }

        [MethodImpl(0x8000)]
        public static bool EqualsForQuest(NpcDNId id1, NpcDNId id2)
        {
        }
    }
}

