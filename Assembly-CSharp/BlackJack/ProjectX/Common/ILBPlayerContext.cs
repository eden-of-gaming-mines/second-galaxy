﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using System;
    using System.Collections.Generic;

    public interface ILBPlayerContext : ILBPlayerContextDCProvider, ILBPlayerContextLBProvider
    {
        List<ActivityInfo> GetActivityList();
        ILBBuffFactory GetBufFactory();
        DateTime GetCurrServerTime();
        GDBHelper GetGDBHelper();
        Func<ulong> GetInstanceIdAllocator();
        PlayerSimpleInfo GetPlayerSimpleInfo();
        Vector3D GetRamdomLocationForDynamicScene(GDBSolarSystemInfo solarSystemInfo, IConfigDataLoader configDataLoader, DynamicSceneLocationType locationType, Random rand, DynamicSceneNearCelestialType nearCelestialType);
        Random GetRandom();
        Vector3D GetRandomStandaloneLocation();
        string GetStringInDefaultStringTable(string key);
        string GetStringInGDBStringTable(string key);
    }
}

