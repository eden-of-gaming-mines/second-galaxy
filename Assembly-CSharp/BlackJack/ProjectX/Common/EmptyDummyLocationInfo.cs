﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="EmptyDummyLocationInfo")]
    public class EmptyDummyLocationInfo : IExtensible
    {
        private string _ScriptRefName;
        private PVector3D _Location;
        private PVector3D _Rotation;
        private uint _Radius;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ScriptRefName;
        private static DelegateBridge __Hotfix_set_ScriptRefName;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_get_Rotation;
        private static DelegateBridge __Hotfix_set_Rotation;
        private static DelegateBridge __Hotfix_get_Radius;
        private static DelegateBridge __Hotfix_set_Radius;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ScriptRefName", DataFormat=DataFormat.Default)]
        public string ScriptRefName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Location", DataFormat=DataFormat.Default)]
        public PVector3D Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Rotation", DataFormat=DataFormat.Default)]
        public PVector3D Rotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Radius", DataFormat=DataFormat.TwosComplement)]
        public uint Radius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

