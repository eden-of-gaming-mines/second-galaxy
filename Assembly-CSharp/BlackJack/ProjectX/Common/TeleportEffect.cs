﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum TeleportEffect
    {
        StarGateDefault,
        FadeInOut,
        JumpingTunnel,
        FlagShipTeleport,
        JumpingByTeleportTunnel
    }
}

