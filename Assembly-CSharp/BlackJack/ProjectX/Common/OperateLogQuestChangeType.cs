﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum OperateLogQuestChangeType
    {
        AddToAcceptList = 1,
        Accept = 2,
        Cancel = 3,
        Complete = 4,
        Fail = 5,
        Retry = 6
    }
}

