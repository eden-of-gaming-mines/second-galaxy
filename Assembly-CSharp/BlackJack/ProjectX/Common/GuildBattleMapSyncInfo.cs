﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildBattleMapSyncInfo
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public GuildBattleType m_type;
        public DateTime m_startTime;
        public DateTime m_endTime;
        public GuildSimplestInfo m_attackerGuildInfo;
        public GuildSimplestInfo m_defenderGuildInfo;
        public int m_npcGuildConfigId;
        public bool m_isAttackerWin;
        public GuildBattleStatus m_status;
        public DateTime m_currStatusStartTime;
        public DateTime m_currStatusEndTime;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildBattleMapSyncInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleMapSyncInfo(GuildBattleInfo battleInfo)
        {
        }
    }
}

