﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Flags]
    public enum ChatExtraOutputLocationType
    {
        None,
        TipsWindow,
        TipsWindowEx
    }
}

