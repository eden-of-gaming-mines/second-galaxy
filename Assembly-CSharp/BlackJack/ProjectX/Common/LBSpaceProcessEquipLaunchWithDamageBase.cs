﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class LBSpaceProcessEquipLaunchWithDamageBase : LBSpaceProcessEquipLaunchBase
    {
        protected LBBulletDamageInfo m_damageInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetDamage;
        private static DelegateBridge __Hotfix_GetDamageInfo;

        [MethodImpl(0x8000)]
        protected LBSpaceProcessEquipLaunchWithDamageBase(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public LBBulletDamageInfo GetDamageInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetDamage(float totalDamage, float totalCriticalDamage, float damageComposeHeat, float damageComposeKinetic, float realDamageHeat, float realDamageKinetic, float realDamageElectric, float percentDamage, float weaponFireCtrlAccuracy, WeaponCategory weaponType, float chargeAndChannelBreakFactor, float weaponTransverseVelocity, float targetTransverseVelocity, float allDamageMulti)
        {
        }
    }
}

