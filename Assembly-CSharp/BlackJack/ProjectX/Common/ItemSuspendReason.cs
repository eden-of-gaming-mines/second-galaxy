﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Flags]
    public enum ItemSuspendReason
    {
        Invalid = 0,
        EquipOnShip = 1,
        StoreOnShip = 2,
        UnpackedShip = 4,
        Cracking = 8,
        Freezing = 0x10
    }
}

