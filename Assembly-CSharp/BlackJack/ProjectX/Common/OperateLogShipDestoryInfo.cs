﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    public class OperateLogShipDestoryInfo
    {
        public int m_ownerShipId;
        public string m_killerGameUserId;
        public int m_currentSolarSystemId;
        public SceneType m_currentSceneType;
        public int m_relativeQuestId;
        public int m_currentSceneId;
        public int m_sceneLevel;
        public int m_killerNpcShipId;
        public OpetateDestoryType m_deadType;
        public List<SimpleItemInfoWithCount> m_lostItemList;
        public TeamStatus m_loserTeamStatus;
        public TeamStatus m_winnerTeamStatus;
        private static DelegateBridge _c__Hotfix_ctor;

        public enum OpetateDestoryType
        {
            OutSide = 1,
            QuestScene = 2,
            Wormhole = 3,
            beInvaded = 4,
            invading = 5
        }

        public enum TeamStatus
        {
            NotAvailable,
            InFleet,
            InTeam,
            InFleetAndTeam
        }
    }
}

