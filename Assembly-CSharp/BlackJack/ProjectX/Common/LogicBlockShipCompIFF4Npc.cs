﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompIFF4Npc : LogicBlockShipCompIFFBase
    {
        protected Func<ILBInSpacePlayerShip, IFFState> m_cbGetIFFState4Player;
        protected Func<ILBInSpaceNpcShip, IFFState> m_cbGetIFFState4HiredCaptain;
        protected Func<ILBInSpaceNpcShip, IFFState> m_cbGetIFFState4Npc;
        protected int m_factionId;
        protected int m_parentFactionId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitFactionCredit;
        private static DelegateBridge __Hotfix_ResetFactionId;
        private static DelegateBridge __Hotfix_GetFactionId;
        private static DelegateBridge __Hotfix_GetParentFactionId;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4Player;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4HiredCaptain;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4NpcShip;
        private static DelegateBridge __Hotfix_GetIFFStateBetweenSpecialFaction;
        private static DelegateBridge __Hotfix_RegCBGetIFFState4Player;
        private static DelegateBridge __Hotfix_RegCBGetIFFState4HiredCaptain;
        private static DelegateBridge __Hotfix_RegCBGetIFFState4Npc;

        [MethodImpl(0x8000)]
        public override int GetFactionId()
        {
        }

        [MethodImpl(0x8000)]
        protected IFFState GetIFFStateBetweenSpecialFaction(int selfFactionId, int targetFactionId)
        {
        }

        [MethodImpl(0x8000)]
        protected override IFFState GetIFFStateForTargetInternal4HiredCaptain(ILBInSpaceNpcShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public override IFFState GetIFFStateForTargetInternal4NpcShip(ILBInSpaceNpcShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override IFFState GetIFFStateForTargetInternal4Player(ILBInSpacePlayerShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetParentFactionId()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitFactionCredit()
        {
        }

        [MethodImpl(0x8000)]
        public void RegCBGetIFFState4HiredCaptain(Func<ILBInSpaceNpcShip, IFFState> func)
        {
        }

        [MethodImpl(0x8000)]
        public void RegCBGetIFFState4Npc(Func<ILBInSpaceNpcShip, IFFState> func)
        {
        }

        [MethodImpl(0x8000)]
        public void RegCBGetIFFState4Player(Func<ILBInSpacePlayerShip, IFFState> func)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ResetFactionId(int factionId)
        {
        }
    }
}

