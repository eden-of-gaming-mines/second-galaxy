﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SolarSystemShipInstanceInfoBase
    {
        public int m_shipConfId;
        public string m_shipName;
        public string m_ownerPlayerName;
        public ShipSlotGroupInfo[] m_highSlotList;
        public AmmoInfo[] m_highSlotAmmoList;
        public List<ShipSlotGroupInfo> m_middleSlotList;
        public List<ShipSlotGroupInfo> m_lowSlotList;
        public List<ShipStoreItemInfo> m_shipStoreItemList;
        public uint m_armorCurr;
        public uint m_shieldCurr;
        public uint m_energyCurr;
        public BasicPropertiesInfo m_captainBasicPropertiesInfo;
        public int m_captainDrivingLicenseId;
        public int m_captainDrivingLicenseLevel;
        public List<int> m_staticBufList;
        public List<object> m_dynamicBufList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

