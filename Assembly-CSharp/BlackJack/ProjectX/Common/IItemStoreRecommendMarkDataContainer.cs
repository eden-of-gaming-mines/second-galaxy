﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface IItemStoreRecommendMarkDataContainer
    {
        void AddStoreItemRecommendMarkInfo(StoreItemType itemType, int configId, bool isBind);
        StoreItemRecommendMarkInfo GetStoreItemRecommendMarkInfo(StoreItemType itemType, int configId, bool isBind);
        List<StoreItemRecommendMarkInfo> GetStoreItemRecommendMarkInfos();
        void RemoveStoreItemRecommendMarkInfo(StoreItemRecommendMarkInfo markInfo);
        void SetRecommendMarkInfoVisited(StoreItemRecommendMarkInfo markInfo);
    }
}

