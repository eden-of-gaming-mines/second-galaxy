﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildTeleportTunnelInfoDataContainer
    {
        void AddGuildTeleportTunnelEffect(GuildTeleportTunnelEffectInfo info);
        List<GuildTeleportTunnelEffectInfo> GetAllGuildTeleportTunnelEffectList();
        GuildTeleportTunnelEffectInfo GetGuildTeleportTunnelEffect(ulong instanceId);
        List<GuildTeleportTunnelEffectInfo> GetGuildTeleportTunnelEffectBySolarSystemId(int destSolarSystemId);
        void InitAllTeleportDistanceData(Dictionary<ulong, float> teleportDistanceDict);
        void RemoveGuildTeleportTunnelEffect(ulong instanceId);
        bool UpdateGuildTeleportTunnelTeleportDistance(ulong instanceId, float distance);
    }
}

