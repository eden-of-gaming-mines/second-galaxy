﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class CMDailySignInfo
    {
        public uint m_CMDailySignCount;
        public DateTime m_CMDailySignLastRecvTime;
        public DateTime m_dailyRandRewardMakeTime;
        public SimpleItemInfoWithCount m_dailyRandReward;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

