﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildPropertiesCalculater : BlackJack.PropertiesCalculater.PropertiesCalculater<ConfigDataGuildPropertiesInfo>
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CalcPropertiesByIdCustom;
        private static DelegateBridge __Hotfix_RemoveBuildingConfigProvider;
        private static DelegateBridge __Hotfix_UpdateBuildingConfigProvider;

        [MethodImpl(0x8000)]
        protected override float CalcPropertiesByIdCustom(int propertiesid, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveBuildingConfigProvider(GuildBuildingInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuildingConfigProvider(GuildBuildingInfo info)
        {
        }
    }
}

