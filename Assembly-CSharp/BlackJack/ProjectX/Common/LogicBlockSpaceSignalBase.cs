﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockSpaceSignalBase
    {
        protected Func<ulong> m_instanceIdAllocator;
        protected Random m_rand;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected ILBPlayerContext m_playerCtx;
        protected ISpaceSignalDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetSpaceSignalByInstanceId;
        private static DelegateBridge __Hotfix_GetSpaceSignalListBySolarSystemId;
        private static DelegateBridge __Hotfix_CheckSpaceSignalNotExpiredBySolarSystemId;
        private static DelegateBridge __Hotfix_RemoveSignalByInstanceId;
        private static DelegateBridge __Hotfix_MapEquipTypeToSignalType;
        private static DelegateBridge __Hotfix_GetValidSpaceSignalShipTypeList;
        private static DelegateBridge __Hotfix_CheckEquipIsAllowedToUseInCurrentSolarSystem;

        [MethodImpl(0x8000)]
        public bool CheckEquipIsAllowedToUseInCurrentSolarSystem(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSpaceSignalNotExpiredBySolarSystemId(int solarSystemId, SignalType signalType)
        {
        }

        [MethodImpl(0x8000)]
        public SignalInfo GetSpaceSignalByInstanceId(int solarSystemId, ulong insId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public List<SignalInfo> GetSpaceSignalListBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> GetValidSpaceSignalShipTypeList(SignalInfo spaceSignal)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx, Func<ulong> instanceIdAllocator)
        {
        }

        [MethodImpl(0x8000)]
        public SignalType MapEquipTypeToSignalType(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSignalByInstanceId(int solarSystemId, ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(DateTime currTime)
        {
        }
    }
}

