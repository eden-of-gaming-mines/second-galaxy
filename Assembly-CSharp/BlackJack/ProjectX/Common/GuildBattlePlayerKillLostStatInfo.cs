﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildBattlePlayerKillLostStatInfo : IComparable<GuildBattlePlayerKillLostStatInfo>
    {
        public GuildBattleGroupType m_groupType;
        public PlayerSimplestInfo m_playerInfo;
        public int m_killingCount;
        public double m_selfLostBindMoney;
        public double m_killingLostBindMoney;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CompareTo;
        private static DelegateBridge __Hotfix_Predicate4NonZeroKillCount;

        [MethodImpl(0x8000)]
        public int CompareTo(GuildBattlePlayerKillLostStatInfo other)
        {
        }

        [MethodImpl(0x8000)]
        public static bool Predicate4NonZeroKillCount(GuildBattlePlayerKillLostStatInfo info)
        {
        }
    }
}

