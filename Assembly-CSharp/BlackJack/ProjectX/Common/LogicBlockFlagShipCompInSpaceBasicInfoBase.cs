﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockFlagShipCompInSpaceBasicInfoBase
    {
        protected ILBInSpaceFlagShip m_ownerShip;
        protected IFlagShipDataContainer m_shipDC;
        private double m_currFuel;
        private double m_maxFuel;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetCurrFuel;
        private static DelegateBridge __Hotfix_GetMaxFuel;
        private static DelegateBridge __Hotfix_ChangeFuel;

        [MethodImpl(0x8000)]
        public void ChangeFuel(double changeValue)
        {
        }

        [MethodImpl(0x8000)]
        public double GetCurrFuel()
        {
        }

        [MethodImpl(0x8000)]
        public double GetMaxFuel()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceFlagShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

