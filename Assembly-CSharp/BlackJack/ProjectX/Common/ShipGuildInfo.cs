﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class ShipGuildInfo
    {
        public uint m_guildId;
        public string m_codeName;
        public uint m_allianceId;
        public string m_allianceName;
        public GuildFleetPersonalSetting m_guildFleetSetting;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

