﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBTacticalEquipGroupReduceCDByWeaponEquipLaunchBase : LBTacticalEquipGroupBase
    {
        protected uint m_reduceCDTimeOnce;
        protected float m_extraParam;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnWeaponEquipLaunchCycleStart;
        private static DelegateBridge __Hotfix_OnDestWeaponEquipLaunch;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupReduceCDByWeaponEquipLaunchBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnDestWeaponEquipLaunch()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeaponEquipLaunchCycleStart(LBInSpaceWeaponEquipGroupBase weaponEquipGroup)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

