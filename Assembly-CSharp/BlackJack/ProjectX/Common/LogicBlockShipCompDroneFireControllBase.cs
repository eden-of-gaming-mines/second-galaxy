﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompDroneFireControllBase
    {
        protected ILBInSpaceShip m_ownerShip;
        protected List<LBSpaceProcessDroneDefenderLaunch> m_defenderList;
        protected List<LBSpaceProcessDroneFighterLaunch> m_enemyFighterList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_RegisterDefender;
        private static DelegateBridge __Hotfix_UnregisterDefender;
        private static DelegateBridge __Hotfix_RegisterEnemyFighter;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickDefenderAgainstFighter;
        private static DelegateBridge __Hotfix_TickSingleDefenderWave;
        private static DelegateBridge __Hotfix_GetEnemyFighter4DefenderAgainst;
        private static DelegateBridge __Hotfix_OnProcessDroneDefenderLaunchCancel;
        private static DelegateBridge __Hotfix_OnEnemyProcessDroneFighterLaunchCancel;

        [MethodImpl(0x8000)]
        protected LBSpaceProcessDroneFighterLaunch GetEnemyFighter4DefenderAgainst()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyProcessDroneFighterLaunchCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProcessDroneDefenderLaunchCancel(LBSpaceProcess process)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void RegisterDefender(LBSpaceProcessDroneDefenderLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterEnemyFighter(LBSpaceProcessDroneFighterLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void TickDefenderAgainstFighter(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void TickSingleDefenderWave(LBSpaceProcessDroneDefenderLaunch defenderProcess, uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UnregisterDefender(LBSpaceProcessDroneDefenderLaunch process)
        {
        }
    }
}

