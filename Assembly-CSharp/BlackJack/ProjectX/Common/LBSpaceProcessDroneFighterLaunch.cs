﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessDroneFighterLaunch : LBSpaceProcessDroneLaunchBase
    {
        protected uint m_fightingStateEndAbsTime;
        protected List<uint> m_droneNextAttackAbsTimeList;
        protected bool m_isSuperWeaponLaunchProcess;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_ReturnDrones;
        private static DelegateBridge __Hotfix_IsAvaliable4Defender;
        private static DelegateBridge __Hotfix_IsAvaliable4DefenderInFruture;
        private static DelegateBridge __Hotfix_IsSuperWeaponLaunchProcess;
        private static DelegateBridge __Hotfix_OnHitByDefender;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnEnterFight;
        private static DelegateBridge __Hotfix_TickFighting;

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneFighterLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneFighterLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource, bool isSuperWeaponLaunchProcess)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource, bool isSuperWeaponLaunchProcess)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvaliable4Defender()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvaliable4DefenderInFruture()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSuperWeaponLaunchProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        public bool OnHitByDefender(LBSpaceProcessDroneDefenderLaunch defenderProcess, LBSpaceProcessDroneDefenderLaunch.AttackChance chance)
        {
        }

        [MethodImpl(0x8000)]
        public override void ReturnDrones(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickFighting(uint currTime)
        {
        }
    }
}

