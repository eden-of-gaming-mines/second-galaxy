﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class EquipFunctionCompInvisibleBase : EquipFunctionCompBase
    {
        protected PropertiesCalculaterBase m_propertiesCalc;
        protected List<LBSpaceProcess> m_processingList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_IsCharging;
        private static DelegateBridge __Hotfix_IsWorking;
        private static DelegateBridge __Hotfix_CalcLoopEnergyCost;

        [MethodImpl(0x8000)]
        protected EquipFunctionCompInvisibleBase(IEquipFunctionCompInvisibleOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcLoopEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsCharging()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsWorking()
        {
        }
    }
}

