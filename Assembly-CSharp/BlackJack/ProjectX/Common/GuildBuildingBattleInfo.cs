﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildBuildingBattleInfo
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public GuildBuildingType m_buildingType;
        public ulong m_battleInstanceId;
        public uint m_attackerGuildId;
        public int m_currSolarSystemFlourishLevel;
        public GuildBuildingBattleDynamicInfo m_dynamicInfo;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_2;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildBuildingBattleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingBattleInfo(GuildBuildingBattleInfo buildingBattleInfo)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingBattleInfo(GuildBuildingInfo buildingInfo, ulong battleInstanceId, uint attackerGuildId, int flourishLevel)
        {
        }
    }
}

