﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompStaticBasicCtx
    {
        protected ILBStaticShip m_ownerShip;
        protected int m_drivingLicenseId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetShipState;
        private static DelegateBridge __Hotfix_GetShipDestroyState;
        private static DelegateBridge __Hotfix_GetShipInstanceId;
        private static DelegateBridge __Hotfix_GetDrivingLicenseId;
        private static DelegateBridge __Hotfix_UpdateCurrEnergy;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_UpdateCurrShield;
        private static DelegateBridge __Hotfix_UpdateCurrArmor;
        private static DelegateBridge __Hotfix_UpdateShipState;
        private static DelegateBridge __Hotfix_UpdateShipDestroyState;
        private static DelegateBridge __Hotfix_GetShipDamageCapabilityValue;
        private static DelegateBridge __Hotfix_GetShipCollectionAbilityValue;
        private static DelegateBridge __Hotfix_GetShipTotalDPSValue;
        private static DelegateBridge __Hotfix_GetShipShieldHeatDamageResistValue;
        private static DelegateBridge __Hotfix_GetShipShieldElectricDamageResistValue;
        private static DelegateBridge __Hotfix_GetShipShieldKineticDamageResistValue;
        private static DelegateBridge __Hotfix_GetShipArmorKineticDamageResistValue;
        private static DelegateBridge __Hotfix_GetShipArmorElectricDamageResistValue;
        private static DelegateBridge __Hotfix_GetShipArmorHeatDamageResistValue;
        private static DelegateBridge __Hotfix_GetShipMaxAttackRangeValue;
        private static DelegateBridge __Hotfix_GetShipAverageAttackRangeValue;
        private static DelegateBridge __Hotfix_IsCollectionShip;
        private static DelegateBridge __Hotfix_GetShipJumpStableValue;
        private static DelegateBridge __Hotfix_GetShipSheildValue;
        private static DelegateBridge __Hotfix_GetShipArmorValue;
        private static DelegateBridge __Hotfix_GetShipVolumnRatioValue;
        private static DelegateBridge __Hotfix_GetShipItemStoreSizeValue;
        private static DelegateBridge __Hotfix_GetShipEnergyValue;
        private static DelegateBridge __Hotfix_GetShipSurvivalCapabilityValue;
        private static DelegateBridge __Hotfix_GetShipEquivalentHPMaxValue;
        private static DelegateBridge __Hotfix_GetShipMaxSpeed;
        private static DelegateBridge __Hotfix_CalcShipSensitivity;
        private static DelegateBridge __Hotfix_CalcShieldRepairSpeed;
        private static DelegateBridge __Hotfix_CalcEquivalentHPMaxValue;
        private static DelegateBridge __Hotfix_CalcEquivalentShieldMaxValue;
        private static DelegateBridge __Hotfix_CalcEquivalentShieldMulit;
        private static DelegateBridge __Hotfix_CalcEquivalentArmorMaxValue;
        private static DelegateBridge __Hotfix_CalcEquivalentArmorMulit;
        private static DelegateBridge __Hotfix_CalcRestoreHPFromRepairerEfficiency;
        private static DelegateBridge __Hotfix_CalcEnergyRelationMulit;
        private static DelegateBridge __Hotfix_CalcEnergyRealLastTime;
        private static DelegateBridge __Hotfix_GetConfigShipFightTime;
        private static DelegateBridge __Hotfix_GetInertiaParam;
        private static DelegateBridge __Hotfix_get_PlayerShipDataContainer;

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtx(ILBStaticShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEnergyRealLastTime()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEnergyRelationMulit()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEquivalentArmorMaxValue()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEquivalentArmorMulit()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEquivalentHPMaxValue()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEquivalentShieldMaxValue()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcEquivalentShieldMulit()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcRestoreHPFromRepairerEfficiency()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcShieldRepairSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcShipSensitivity()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetConfigShipFightTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetDrivingLicenseId()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetInertiaParam()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipArmorElectricDamageResistValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipArmorHeatDamageResistValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipArmorKineticDamageResistValue()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipArmorValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipAverageAttackRangeValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipCollectionAbilityValue()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipDamageCapabilityValue()
        {
        }

        [MethodImpl(0x8000)]
        public ShipDestroyState GetShipDestroyState()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipEnergyValue()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipEquivalentHPMaxValue()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetShipInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipItemStoreSizeValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipJumpStableValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipMaxAttackRangeValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipMaxSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipName()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipSheildValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipShieldElectricDamageResistValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipShieldHeatDamageResistValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipShieldKineticDamageResistValue()
        {
        }

        [MethodImpl(0x8000)]
        public ShipState GetShipState()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipSurvivalCapabilityValue()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipTotalDPSValue()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipVolumnRatioValue()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCollectionShip()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrArmor(float armor)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShield(float shield)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipDestroyState(ShipDestroyState state)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipState(ShipState state)
        {
        }

        protected IStaticPlayerShipDataContainer PlayerShipDataContainer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

