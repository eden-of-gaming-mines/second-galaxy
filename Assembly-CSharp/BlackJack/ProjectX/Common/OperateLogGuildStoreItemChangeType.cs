﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum OperateLogGuildStoreItemChangeType
    {
        None,
        GM,
        DestroyShip,
        SuspendItem,
        ResumeItem,
        Compensation,
        AutoDonate,
        Action,
        Mining,
        BlackMarketBuy,
        BuildingDeploy,
        BuildingRecycle,
        TradeTransportStart,
        TradeTransportCreateFail,
        TradeTransportSuccess,
        PurchaseOrder,
        ProducationBuildingStart,
        ProducationCancel,
        ProducationSuccess,
        FlagShipAddFuel,
        FlagShipRemoveFuel,
        FlagShipPackagedRemoveFuel,
        FlagShipDestroyed,
        UnpackedShip,
        PackShip,
        EquipOnShip,
        RemoveEquipFromShip,
        ProducationFlagShipStart
    }
}

