﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum VirtualBufType
    {
        SpecialQuest,
        DailyExtraRewardQuest,
        QuestPoolCount,
        DelegateMissionPoolCount,
        PlayerSovereignty,
        SpaceSignal,
        SpaceSignalTime
    }
}

