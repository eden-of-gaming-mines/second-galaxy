﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildPurchaseOrderInfo
    {
        public int m_auctionItemId;
        public long m_restItemCount;
        public ulong m_instanceId;
        public long m_addUpCount;
        public List<GuildPurchaseLogInfo> m_logList;
        public int m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

