﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class SceneGuildBuildingExtraDataInfo
    {
        public uint m_objId;
        public GuildBuildingStatus m_status;
        public GuildBattleStatus m_battleStatus;
        public DateTime m_currStatusStartTime;
        public DateTime m_currStatusEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

