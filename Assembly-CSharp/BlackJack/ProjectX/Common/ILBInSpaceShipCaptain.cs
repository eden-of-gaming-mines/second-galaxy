﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using System;

    public interface ILBInSpaceShipCaptain : ILBShipCaptain, IPropertiesProvider
    {
        int GetCaptainFirstNameId();
        int GetCaptainLastNameId();
        string GetCaptainName();
        ulong GetHiredCaptainInstanceId();
        string GetPlayerGameUserId();
        bool IsHiredCaptain();
        bool IsPlayerCaptain();
    }
}

