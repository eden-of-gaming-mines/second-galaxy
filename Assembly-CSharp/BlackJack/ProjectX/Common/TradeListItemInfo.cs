﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class TradeListItemInfo
    {
        public int m_tradeItemId;
        public NpcDNId m_tradeNpcDNId;
        public long m_currPrice;
        public long m_oldPrice;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

