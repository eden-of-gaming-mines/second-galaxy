﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Runtime.InteropServices;

    public interface ISpaceSignalDataContainer
    {
        void AddNewSolarSystemSpaceSignalInfo(int solarSystemId, SolarSystemSpaceSignalInfo ssssi);
        SolarSystemSpaceSignalInfo GetSolarSystemSpaceSignalInfo(int solarSystemId);
        void GetSolarSystemSpaceSignalInfo(int solarSystemId, SignalType signalType, out SolarSystemSpaceSignalInfo ssssi, out SpaceSignalResultInfo resultInfo);
        void RemoveSignalByInstanceId(int solarSystemId, ulong insId);
        void RemoveSolarSystemSpaceSignalResultInfo(SolarSystemSpaceSignalInfo ssssi, SignalType signalType);
        int TryClearExpiredSignalAndRecalculateTotalCount();
        void UpdateSignalPreciseness(int solarSystemId, SignalType signalType, ulong signalInstanceId, int preciseness);
    }
}

