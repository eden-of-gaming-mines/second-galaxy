﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IFormationMember
    {
        uint GetMemberObjId();
        void OnFormationDismiss();
        void OnFormationReset();
    }
}

