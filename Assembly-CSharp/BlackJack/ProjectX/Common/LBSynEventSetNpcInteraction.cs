﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventSetNpcInteraction : LBSyncEvent
    {
        public SceneInteractionType m_interactionType;
        public uint m_optTime;
        public uint m_range;
        public int m_interactionMessage;
        public SceneInteractionFlag m_falg;
        public int m_interactionTemplateId;
        public uint m_singlePlayerInteractionCountMax;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

