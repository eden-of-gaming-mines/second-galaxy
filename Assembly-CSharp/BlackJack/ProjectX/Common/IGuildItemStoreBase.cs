﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    public interface IGuildItemStoreBase : IGuildItemStoreItemOperationBase
    {
        bool IsGuildStoreItemMatched(int storeItemIndex, ItemInfo comparedItemInfo);
        void RemoveItemDirectly(LBStoreItem item, OperateLogGuildStoreItemChangeType operateLogCauseId = 0, string operateLogLocation = null, GuildMemberBase operateMember = null);
    }
}

