﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LogicBlockShipCompPlayerInfoBase : ILogicBlockShipCompPlayerInfo, IPlayerInfoProvider
    {
        protected ILBInSpacePlayerShip m_ownerShip;
        protected IPlayerInfoProvider m_playerInfoProvider;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetSessionId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerGameUserId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerName;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerLevel;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetTeamInstanceId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerAvatarId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetPlayerProfession;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetGrandFaction;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IPlayerInfoProvider.GetGenderType;
        private static DelegateBridge __Hotfix_GetCustomizedParameterList;
        private static DelegateBridge __Hotfix_GetCustomizedParameterValueAsInt;
        private static DelegateBridge __Hotfix_GetCustomizedParameterValueAsString;
        private static DelegateBridge __Hotfix_GetCustomizedParameterValueAsBool;

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompPlayerInfoBase()
        {
        }

        [MethodImpl(0x8000)]
        GenderType IPlayerInfoProvider.GetGenderType()
        {
        }

        [MethodImpl(0x8000)]
        GrandFaction IPlayerInfoProvider.GetGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        int IPlayerInfoProvider.GetPlayerAvatarId()
        {
        }

        [MethodImpl(0x8000)]
        string IPlayerInfoProvider.GetPlayerGameUserId()
        {
        }

        [MethodImpl(0x8000)]
        int IPlayerInfoProvider.GetPlayerLevel()
        {
        }

        [MethodImpl(0x8000)]
        string IPlayerInfoProvider.GetPlayerName()
        {
        }

        [MethodImpl(0x8000)]
        ProfessionType IPlayerInfoProvider.GetPlayerProfession()
        {
        }

        [MethodImpl(0x8000)]
        ulong IPlayerInfoProvider.GetSessionId()
        {
        }

        [MethodImpl(0x8000)]
        uint IPlayerInfoProvider.GetTeamInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public List<CustomizedParameterInfo> GetCustomizedParameterList()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetCustomizedParameterValueAsBool(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCustomizedParameterValueAsInt(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetCustomizedParameterValueAsString(CustomizedParameter parameterId)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpacePlayerShip owner, IPlayerInfoProvider playerInfoProvider)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }
    }
}

