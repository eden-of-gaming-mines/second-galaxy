﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct ShipPoseRequest
    {
        public ReqType m_type;
        public uint m_objId;
        public enum ReqType
        {
            None,
            ByStarGate,
            ByLeaveStation
        }
    }
}

