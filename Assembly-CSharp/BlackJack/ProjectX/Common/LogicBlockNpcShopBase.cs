﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockNpcShopBase
    {
        protected INpcShopDataContainer m_dc;
        protected ILBPlayerContext m_lbPlayerCtx;
        protected LogicBlockStationContextBase m_lbStationContext;
        protected LogicBlockCharacterFactionBase m_lbFaction;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected NpcDNId m_currNpcId;
        protected int m_npcShopFlag;
        protected List<ConfigDataNpcShopItemInfo> m_shopItemList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SetupNpcShopContext;
        private static DelegateBridge __Hotfix_CheckNpcShopBuyItem;
        private static DelegateBridge __Hotfix_GetNpcShopFlag;
        private static DelegateBridge __Hotfix_GetCurrNpcTalker;
        private static DelegateBridge __Hotfix_CheckNpcShipFlag;
        private static DelegateBridge __Hotfix_GetShopItemConfList;
        private static DelegateBridge __Hotfix_IsNpcShopItemFillToMSStore;
        private static DelegateBridge __Hotfix_UpdateSaleCountLimitRecord;

        [MethodImpl(0x8000)]
        public bool CheckNpcShipFlag(NpcShopFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckNpcShopBuyItem(List<KeyValuePair<int, long>> shopItemList, out Dictionary<CurrencyType, long> needCurrencyCountMap, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcTalkerBase GetCurrNpcTalker()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcShopFlag()
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataNpcShopItemInfo> GetShopItemConfList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNpcShopItemFillToMSStore()
        {
        }

        [MethodImpl(0x8000)]
        public void SetupNpcShopContext(NpcDNId npcId, List<int> shopItemList, int npcShopFlag)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSaleCountLimitRecord(int shopItemId, long addCount)
        {
        }
    }
}

