﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum SceneCameraAnimationType
    {
        SimpleProgramCamera,
        OutSceneClip,
        InSceneClip,
        Video,
        Timeline,
        Max
    }
}

