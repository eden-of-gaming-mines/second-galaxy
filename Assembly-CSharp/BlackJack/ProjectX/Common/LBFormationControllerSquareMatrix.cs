﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBFormationControllerSquareMatrix : LBFormationControllerBase
    {
        protected double m_distanceBetweenMember;
        protected MemberNode m_lastUseNode;
        protected Dictionary<uint, MemberNode> m_member2UsedNodeInfoDict;
        protected Vector3D[,] m_memberNode2PosArr;
        protected DiamondList[,] m_freeMemberNodeList;
        protected double m_ratioFromMemberSpaceToLeaderShipSize;
        protected double m_formationAngle;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByJoinMember;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByLeaveMember;
        private static DelegateBridge __Hotfix_CalcSymmetryPointSeqId;
        private static DelegateBridge __Hotfix_CalcMemberPos;
        private static DelegateBridge __Hotfix_CreateMemberNode;
        private static DelegateBridge __Hotfix_AddNode2LinkList;

        [MethodImpl(0x8000)]
        protected void AddNode2LinkList(LinkedList<MemberNode> memberList, MemberNode node)
        {
        }

        [MethodImpl(0x8000)]
        protected Vector3D CalcMemberPos(int xRatio, int yRatio, int zRatio)
        {
        }

        [MethodImpl(0x8000)]
        protected int CalcSymmetryPointSeqId(int diamondId, int originalSeqId)
        {
        }

        [MethodImpl(0x8000)]
        protected MemberNode CreateMemberNode(int panelId, int diamondId, int seqId, bool isVertex = false)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initialize(ILBInSpaceShip leader, FormationCreatingInfo formationInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByJoinMember(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByLeaveMember(IFormationMember member)
        {
        }

        protected class DiamondList
        {
            public LinkedList<LBFormationControllerSquareMatrix.MemberNode> m_vertexList;
            public LinkedList<LBFormationControllerSquareMatrix.MemberNode> m_randomList;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        protected class MemberNode
        {
            public int m_panelId;
            public int m_diamondId;
            public int m_seqId;
            public bool m_isVertex;
            private static DelegateBridge _c__Hotfix_ctor;

            public MemberNode()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

