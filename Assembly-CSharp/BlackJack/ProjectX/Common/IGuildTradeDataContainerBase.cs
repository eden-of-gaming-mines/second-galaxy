﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildTradeDataContainerBase
    {
        ushort GetGuildTradePortDataVersion();
        GuildTradePortExtraInfo GetGuildTradePortExtraInfoBySolarSystemId(int solarSystemId);
        List<GuildTradePortExtraInfo> GetGuildTradePortExtraInfoList();
        bool GuildTradePurchaseOrderAdd(int solarSystemId, ulong purchaseInstanceId);
        bool GuildTradePurchaseOrderRemove(int solarSystemId, ulong purchaseInstanceId);
        bool GuildTradeTransportPlanAdd(int solarSystemId, ulong transportInstanceId);
        bool GuildTradeTransportPlanRemove(int solarSystemId, ulong transportInstanceId);
    }
}

