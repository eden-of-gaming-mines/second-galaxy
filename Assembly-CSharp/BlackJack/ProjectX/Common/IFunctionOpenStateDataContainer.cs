﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IFunctionOpenStateDataContainer
    {
        bool IsFunctionOpen(SystemFuncType systemFuncType);
        bool IsFunctionOpenAnimationShow(SystemFuncType systemFuncType);
        void SetFunctionOpenAnimationEnd(SystemFuncType systemFuncType, bool isEndAnimation);
        void SetFunctionOpenState(SystemFuncType systemFuncType, bool isOpen);
    }
}

