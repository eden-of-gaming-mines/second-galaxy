﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class LBNpcTalkerBase
    {
        protected ConfigDataNpcTalkerInfo m_confInfo;
        protected ILBPlayerContext m_lbPlayerCtx;
        protected NpcDNId m_npcDNId;
        protected int m_currNpcFactionId;
        protected GrandFaction m_grandFaction;
        private SpaceStationType m_spaceStationType;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_IsStatic;
        private static DelegateBridge __Hotfix_GetNpcTalkerId;
        private static DelegateBridge __Hotfix_GetNpcDNId;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetCurrNpcFactionId;
        private static DelegateBridge __Hotfix_GetGrandFaction;
        private static DelegateBridge __Hotfix_GetSpaceStationType;
        private static DelegateBridge __Hotfix_HasQuest2Accept;
        private static DelegateBridge __Hotfix_HasQuest2Complete;
        private static DelegateBridge __Hotfix_GetNpcFunctionType;
        private static DelegateBridge __Hotfix_CalcCurrentNpcFactionId;
        private static DelegateBridge __Hotfix_CalcCurrentFactionId;
        private static DelegateBridge __Hotfix_CalcCurrentGrandFaction;
        private static DelegateBridge __Hotfix_GetCurrentSpaceStationType;

        [MethodImpl(0x8000)]
        public LBNpcTalkerBase(ConfigDataNpcTalkerInfo npcTaklerInfo, bool isGlobalNpc, int solarSystemId, int stationId, ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcCurrentFactionId()
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction CalcCurrentGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        public int CalcCurrentNpcFactionId()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcTalkerInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationType GetCurrentSpaceStationType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrNpcFactionId()
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction GetGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        public string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId GetNpcDNId()
        {
        }

        [MethodImpl(0x8000)]
        public NpcFunctionType GetNpcFunctionType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcTalkerId()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationType GetSpaceStationType()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasQuest2Accept()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasQuest2Complete()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStatic()
        {
        }

        public abstract bool IsVisible();
    }
}

