﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBScanProbe
    {
        protected ScanProbeInfo m_readOnlyInfo;
        protected ISignalDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetScanProbeType;
        private static DelegateBridge __Hotfix_IsFull;
        private static DelegateBridge __Hotfix_IsAvailable;
        private static DelegateBridge __Hotfix_GetScanProbeCountMax;
        private static DelegateBridge __Hotfix_GetCurrentCount;
        private static DelegateBridge __Hotfix_IncreaseScanProbeCount;
        private static DelegateBridge __Hotfix_IncreaseScanProbeCountDirectly;
        private static DelegateBridge __Hotfix_DecreaseScanProbe;
        private static DelegateBridge __Hotfix_GetScanProbeRechargeCD;
        private static DelegateBridge __Hotfix_GetRechargeCDStarTime;
        private static DelegateBridge __Hotfix_ResetRechargeCDStartTime;
        private static DelegateBridge __Hotfix_IsRechargeCDEnd;

        [MethodImpl(0x8000)]
        public LBScanProbe(ScanProbeInfo info, ISignalDataContainer dc)
        {
        }

        [MethodImpl(0x8000)]
        public void DecreaseScanProbe(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrentCount()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetRechargeCDStarTime()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetScanProbeCountMax()
        {
        }

        [MethodImpl(0x8000)]
        public int GetScanProbeRechargeCD()
        {
        }

        [MethodImpl(0x8000)]
        public ScanProbeType GetScanProbeType()
        {
        }

        [MethodImpl(0x8000)]
        public void IncreaseScanProbeCount(int count = 1, bool isByItem = false)
        {
        }

        [MethodImpl(0x8000)]
        public void IncreaseScanProbeCountDirectly(int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvailable()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFull()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRechargeCDEnd(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetRechargeCDStartTime(DateTime startTime)
        {
        }
    }
}

