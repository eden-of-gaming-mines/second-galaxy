﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessDroneLaunchSource : ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        void OnProcessDroneFighterFire(LBSpaceProcessDroneLaunchBase process, int droneIndex);
        bool OnProcessDroneHitByDefender(LBSpaceProcessDroneLaunchBase process, LBSpaceProcessDroneDefenderLaunch defenderProcess, LBSpaceProcessDroneDefenderLaunch.AttackChance chance);
        void OnProcessDroneReturn(LBSpaceProcessDroneLaunchBase process);
        void OnProcessDroneSelfExplode(LBSpaceProcessDroneLaunchBase process);
        void OnProcessDroneStateChange(LBSpaceProcessDroneLaunchBase process);
    }
}

