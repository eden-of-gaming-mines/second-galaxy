﻿namespace BlackJack.ProjectX.Common
{
    public interface IGuildCompOwnerBase
    {
        IGuildCompActionBase GetCompAction();
        IGuildCompAllianceBase GetCompAlliance();
        IGuildCompAntiTeleportBase GetCompAntiTeleport();
        IGuildCompBasicLogicBase GetCompBasic();
        IGuildCompBattleBase GetCompBattle();
        IGuildBenefitsBase GetCompBenefis();
        IGuildCompCompensationBase GetCompCompensation();
        IGuildCompCurrencyBase GetCompCurrency();
        IGuildCompCurrencyLogBase GetCompCurrencyLog();
        IGuildCompDiplomacyBase GetCompDiplomacy();
        IGuildCompDynamicBase GetCompDynamic();
        IGuildCompFlagShipBase GetCompFlagShip();
        IGuildCompFleetBase GetCompFleet();
        IGuildCompItemStoreBase GetCompItemStore();
        IGuildCompJoinApplyBase GetCompJoinApply();
        IGuildCompLostReportBase GetCompLostReport();
        IGuildCompMemberListBase GetCompMemberList();
        IGuildCompMessageBoardBase GetCompMessageBoard();
        IGuildCompMiningBase GetCompMining();
        IGuildCompMomentsBase GetCompMoments();
        IGuildCompProductionBase GetCompProduction();
        IGuildCompPropertiesCalculaterBase GetCompPropertiesCalculater();
        IGuildCompPurchaseOrderBase GetCompPurchaseOrder();
        IGuildCompSimpleRuntimeBase GetCompSimpleRuntime();
        IGuildCompSolarSystemCtxBase GetCompSolarSystemCtx();
        IGuildCompStaffingLogBase GetCompStaffingLog();
        IGuildCompTeleportTunnelBase GetCompTeleportTunnel();
        IGuildCompTradeBase GetCompTrade();
        IGuildDataContainerBase GetGuildDataContainer();
    }
}

