﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBSpaceProcessEquipChannelLaunch : LBSpaceProcessEquipLaunchWithDamageBase
    {
        protected uint? m_nextLoopTimeAbs;
        public uint m_loopCD;
        public uint m_loopCountLimit;
        public uint m_loopEnergyCost;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetLoopCostEnergy;
        private static DelegateBridge __Hotfix_SetLoopCD;
        private static DelegateBridge __Hotfix_SetLoopCount;
        private static DelegateBridge __Hotfix_IsLooping;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnChannelEnd;
        private static DelegateBridge __Hotfix_OnEquipFire;
        private static DelegateBridge __Hotfix_get_ProcessSource;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipChannelLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipChannelLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ShipEquipSlotType slotType, int groupIndex, EquipType equipType, ILBSpaceProcessEquipLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ShipEquipSlotType slotType, int groupIndex, EquipType equipType, ILBSpaceProcessEquipLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLooping()
        {
        }

        [MethodImpl(0x8000)]
        public void OnChannelEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEquipFire(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLoopCD(uint cd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLoopCostEnergy(ushort loopCostEnergy)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLoopCount(uint loopCount)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        protected ILBSpaceProcessEquipChannelLaunchSource ProcessSource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

