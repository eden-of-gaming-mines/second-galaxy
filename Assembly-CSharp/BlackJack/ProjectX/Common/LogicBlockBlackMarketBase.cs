﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockBlackMarketBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected LogicBlockStationContextBase m_lbStationContext;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected NpcDNId m_currNpcId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SetupBlackMarketContext;
        private static DelegateBridge __Hotfix_CheckBlackMarketBuyItem;
        private static DelegateBridge __Hotfix_RemapBlackMarketShopItemTypeToCurrencyType;
        private static DelegateBridge __Hotfix_RemapBlackMarketShopPayTypeToCurrencyType;

        [MethodImpl(0x8000)]
        public bool CheckBlackMarketBuyItem(List<KeyValuePair<int, int>> shopItemList, out Dictionary<CurrencyType, long> needCurrencyCountMap, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected CurrencyType RemapBlackMarketShopItemTypeToCurrencyType(BlackMarketShopItemType shopItemType)
        {
        }

        [MethodImpl(0x8000)]
        protected CurrencyType RemapBlackMarketShopPayTypeToCurrencyType(BlackMarketShopPayType ePayType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetupBlackMarketContext(NpcDNId npcId, List<int> itemList)
        {
        }
    }
}

