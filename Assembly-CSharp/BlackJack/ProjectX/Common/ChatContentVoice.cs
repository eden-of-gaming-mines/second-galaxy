﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    public class ChatContentVoice : ChatInfo
    {
        public ulong m_instanceId;
        public byte[] m_voice;
        public int m_voiceLenth;
        public int m_audioFrequency;
        public int m_sampleLength;
        public bool m_isOverdued;
        public bool m_showTranslate;
        public bool m_isAudio2String;
        public string m_audioMd5;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

