﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildMemberListInfo
    {
        public List<GuildMemberInfo> m_members;
        public ushort m_basicVersion;
        public ushort m_dynamicVersion;
        public ushort m_runtimeVersion;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

