﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBBufLimitDamage : LBTickableBuf
    {
        private float m_damageTakenPercent;
        private uint m_lastDamageTakenResetTime;
        private readonly int m_damageTakenResetPeriod;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddDamageTakenPercent;
        private static DelegateBridge __Hotfix_GetDamageLimitLeftPercent;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnPeriod;
        private static DelegateBridge __Hotfix_ResetDamageTaken;

        [MethodImpl(0x8000)]
        public LBBufLimitDamage(ConfigDataBufInfo confInfo, uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void AddDamageTakenPercent(float damagePercent)
        {
        }

        [MethodImpl(0x8000)]
        public float GetDamageLimitLeftPercent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPeriod(uint tickTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetDamageTaken(uint currTime)
        {
        }
    }
}

