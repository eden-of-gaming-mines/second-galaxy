﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperEquipGroupAntiJumplingForceShieldAndAttachBufBase : LBSuperEquipGroupBase, IEquipFunctionCompInOutRangeTriggerOwnerBase, IEquipFunctionCompOwner
    {
        protected EquipFunctionCompInOutRangeTriggerBase m_funcComp;
        protected bool m_isAntiJumpingForceShieldOpenState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsWorking;
        private static DelegateBridge __Hotfix_HasAvailableTarget;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompInOutRangeTriggerOwnerBase.GetDuration;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.AttachBufByEquip2Target;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcChargeTime;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_GetConfBufIdList;
        private static DelegateBridge __Hotfix_GetEquipType;
        private static DelegateBridge __Hotfix_GetOwnerShip;
        private static DelegateBridge __Hotfix_GetPropertiesCalc;
        private static DelegateBridge __Hotfix_GetSelfBufIdList;
        private static DelegateBridge __Hotfix_GetSlotGroupIndex;
        private static DelegateBridge __Hotfix_GetSlotType;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;

        [MethodImpl(0x8000)]
        protected LBSuperEquipGroupAntiJumplingForceShieldAndAttachBufBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        uint IEquipFunctionCompInOutRangeTriggerOwnerBase.GetDuration()
        {
        }

        [MethodImpl(0x8000)]
        ulong IEquipFunctionCompOwner.AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        public override bool CanMakeDamage()
        {
        }

        protected abstract EquipFunctionCompInOutRangeTriggerBase CreateFuncComp();
        [MethodImpl(0x8000)]
        public List<int> GetConfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        public EquipType GetEquipType()
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpaceShip GetOwnerShip()
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesCalculaterBase GetPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<int> GetSelfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSlotGroupIndex()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEquipSlotType GetSlotType()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasAvailableTarget(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsWorking()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

