﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBStaticShip : ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        LogicBlockShipCompStaticCaptain GetLBCaptain();
        LogicBlockCompPropertiesCalc GetLBPropertiesCalc();
        LogicBlockShipCompStaticBasicCtx GetLBStaticBasicCtx();
        LogicBlockShipCompStaticWeaponEquipBase GetLBWeaponEquip();
    }
}

