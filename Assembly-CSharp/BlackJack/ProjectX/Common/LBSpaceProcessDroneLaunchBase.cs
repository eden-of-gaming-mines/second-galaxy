﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBSpaceProcessDroneLaunchBase : LBSpaceProcessWeaponLaunchBase
    {
        protected ushort m_formupTime;
        protected ushort m_waitForLaunchTime;
        protected uint m_allUnitFireTime;
        protected ushort m_flyTime;
        protected ushort m_fightTime;
        protected ushort m_cd4DroneNextAttack;
        protected ushort m_cd4NextDroneAttack;
        protected float m_hitRateFinal;
        protected float m_criticalRateFinal;
        protected List<KeyValuePair<ushort, uint>> m_unitLaunchList;
        protected List<uint> m_unitChargeTimeList;
        protected ConfigDataDroneInfo m_confInfo;
        protected DroneFighterState m_state;
        protected uint m_stateStartTime;
        protected int m_droneCount;
        protected int m_beenAttackedDroneArmorLeft;
        protected int m_unitLaunchListLoopIndex;
        protected int m_unitChargeListLoopIndex;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetDroneConfigInfo;
        private static DelegateBridge __Hotfix_GetDroneConfigInfo;
        private static DelegateBridge __Hotfix_SetWaitForLaunchTime;
        private static DelegateBridge __Hotfix_SetFormupTime;
        private static DelegateBridge __Hotfix_GetFormupTime;
        private static DelegateBridge __Hotfix_SetAllUnitFireTime;
        private static DelegateBridge __Hotfix_GetAllUnitFireTime;
        private static DelegateBridge __Hotfix_SetFlyTime;
        private static DelegateBridge __Hotfix_GetFlyTime;
        private static DelegateBridge __Hotfix_SetFightTime;
        private static DelegateBridge __Hotfix_GetFightTime;
        private static DelegateBridge __Hotfix_GetHitRate;
        private static DelegateBridge __Hotfix_SetHitRate;
        private static DelegateBridge __Hotfix_GetCriticalRate;
        private static DelegateBridge __Hotfix_SetCriticalRate;
        private static DelegateBridge __Hotfix_SetAttackCD;
        private static DelegateBridge __Hotfix_GetCD4DroneNextAttack;
        private static DelegateBridge __Hotfix_GetCD4NextDroneAttack;
        private static DelegateBridge __Hotfix_AddUnitLaunch;
        private static DelegateBridge __Hotfix_AddUnitChargeInfo;
        private static DelegateBridge __Hotfix_SortUnitLaunchTime;
        private static DelegateBridge __Hotfix_ComparationForUnitLaunchInfo;
        private static DelegateBridge __Hotfix_GetUnitLaunchList;
        private static DelegateBridge __Hotfix_GetUnitChargeList;
        private static DelegateBridge __Hotfix_GetLaunchCount;
        private static DelegateBridge __Hotfix_GetState;
        private static DelegateBridge __Hotfix_SetState;
        private static DelegateBridge __Hotfix_GetDroneCount;
        private static DelegateBridge __Hotfix_DecDroneCount;
        private static DelegateBridge __Hotfix_ReturnDrones;
        private static DelegateBridge __Hotfix_GetBeenAttackedDroneArmorLeft;
        private static DelegateBridge __Hotfix_SetBeenAttackedDroneArmorLeft;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_get_ProcessSource;

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneLaunchBase(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void AddUnitChargeInfo(uint chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void AddUnitLaunch(ushort index, uint processTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        private int ComparationForUnitLaunchInfo(KeyValuePair<ushort, uint> info1, KeyValuePair<ushort, uint> info2)
        {
        }

        [MethodImpl(0x8000)]
        public void DecDroneCount()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAllUnitFireTime()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBeenAttackedDroneArmorLeft()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetCD4DroneNextAttack()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetCD4NextDroneAttack()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCriticalRate()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDroneInfo GetDroneConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetDroneCount()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetFightTime()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetFlyTime()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetFormupTime()
        {
        }

        [MethodImpl(0x8000)]
        public float GetHitRate()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetLaunchCount()
        {
        }

        [MethodImpl(0x8000)]
        public DroneFighterState GetState()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetUnitChargeList()
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<ushort, uint>> GetUnitLaunchList()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ReturnDrones(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllUnitFireTime(uint fireTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttackCD(ushort cd4DroneNextWave, ushort cd4NextDroneAttack)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBeenAttackedDroneArmorLeft(int beenAttackedDroneArmorLeft)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCriticalRate(float propertyCriticalRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDroneConfigInfo(ConfigDataDroneInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFightTime(ushort droneFightTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlyTime(ushort droneFlyTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFormupTime(ushort formupTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHitRate(float propertyHitRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetState(DroneFighterState state, uint startTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWaitForLaunchTime(ushort waitForLaunchTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SortUnitLaunchTime()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        protected ILBSpaceProcessDroneLaunchSource ProcessSource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

