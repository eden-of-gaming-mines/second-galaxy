﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;

    public class TeamMemberSnapshotInfo
    {
        public string m_playerGameUserId;
        public int m_charLevel;
        public int m_currSolarSystemId;
        public Vector3D m_location;
        public bool m_isInSpace;
        public int m_currShipConfigId;
        public int m_currSpaceStationId;
        public uint m_guildId;
        public string m_guildCodeName;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

