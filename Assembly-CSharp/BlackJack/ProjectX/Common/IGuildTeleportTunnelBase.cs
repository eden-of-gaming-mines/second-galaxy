﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildTeleportTunnelBase
    {
        List<GuildTeleportTunnelEffectInfo> GetAllGuildTeleportTunnelEffectList();
        GuildTeleportTunnelEffectInfo GetGuildTeleportTunnelBuildingEffectInfo(ulong buildingInsId);
        List<GuildTeleportTunnelEffectInfo> GetGuildTeleportTunnelEffectBySolarSystemId(int destSolarSystemId);
    }
}

