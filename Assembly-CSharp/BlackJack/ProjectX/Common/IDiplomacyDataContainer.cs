﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IDiplomacyDataContainer
    {
        void DiplomacyAddEnemyAllience(uint allienceId);
        void DiplomacyAddEnemyGuild(uint guildId);
        void DiplomacyAddEnemyPlayer(string playerGameUserId);
        void DiplomacyAddFriendlyAllience(uint allienceId);
        void DiplomacyAddFriendlyGuild(uint guildId);
        void DiplomacyAddFriendlyPlayer(string playerGameUserId);
        void DiplomacyRemoveEnemyAllience(uint allienceId);
        void DiplomacyRemoveEnemyGuild(uint guildId);
        void DiplomacyRemoveEnemyPlayer(string playerGameUserId);
        void DiplomacyRemoveFriendlyAllience(uint allienceId);
        void DiplomacyRemoveFriendlyGuild(uint guildId);
        void DiplomacyRemoveFriendlyPlayer(string playerGameUserId);
        DiplomacyInfo GetDiplomacyInfo();
        ushort GetVersion();
        void SetVersion(ushort version);
    }
}

