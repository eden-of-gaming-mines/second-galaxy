﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildFleetDataContainer
    {
        void AddGuildFleetInfo(GuildFleetInfo fleetInfo);
        void AddGuildFleetMemberInfo(ulong fleetInsId, GuildFleetMemberInfo memberInfo);
        void AddGuildFleetMemberPosition(ulong fleetInsId, string gameUserId, FleetPosition position);
        void ClearGuildFleetMemberPosition(ulong fleetInsId, string gameUserId);
        int GetCurrGuildFleetSeqId();
        GuildFleetInfo GetGuildFleetById(ulong fleetInsId);
        int GetGuildFleetCount();
        bool GetGuildFleetDataVersion(ulong fleetInsId, out uint basicInfoVersion, out uint memberListVersion);
        GuildFleetListInfo GetGuildFleetlist();
        GuildFleetMemberInfo GetGuildFleetMemberInfo(ulong fleetInsId, string gameUserId);
        void RemoveGuildFleetInfo(ulong fleetInsId);
        void RemoveGuildFleetMemberInfo(ulong fleetInsId, string gameUserId);
        void RemoveGuildFleetMemberPosition(ulong fleetInsId, string gameUserId, FleetPosition position);
        void SetFormationActive(ulong fleetInsId, bool isActive);
        void UpdateGuildFleetAllowToJoinFleetManual(ulong fleetInsId, bool allowToJoinFleetManual);
        void UpdateGuildFleetBasicInfo(ulong fleetInsId, string fleetName, int formationConfId, bool allowToJoinFleetManual);
        void UpdateGuildFleetFormation(ulong fleetInsId, int formationConfId);
        void UpdateGuildFleetMemberFireControllerSubstituteSeq(ulong fleetInsId, string gameUserId, int seq);
        void UpdateGuildFleetMemberInfo(ulong fleetInsId, GuildFleetMemberInfo memberInfo);
        void UpdateGuildFleetMemberListInfo(ulong fleetInsId, List<GuildFleetMemberInfo> memberList);
        void UpdateGuildFleetMemberNavigatorSubstituteSeq(ulong fleetInsId, string gameUserId, int seq);
        void UpdateGuildFleetName(ulong fleetInsId, string newName);
    }
}

