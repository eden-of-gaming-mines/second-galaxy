﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSyncEventRailgunLaunch : LBSyncEvent
    {
        public uint m_destObjectId;
        public LBSpaceProcessBulletGunLaunch m_process;
        public uint m_weaponGroupCDEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

