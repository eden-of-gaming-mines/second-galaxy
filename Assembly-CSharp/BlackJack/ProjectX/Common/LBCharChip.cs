﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBCharChip
    {
        protected ConfigDataCharChipInfo m_chipConf;
        protected ConfigDataCharChipSuitInfo m_chipSetConf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetChipConfInfo;
        private static DelegateBridge __Hotfix_GetChipId;
        private static DelegateBridge __Hotfix_GetChipSuitConfInfo;

        [MethodImpl(0x8000)]
        public LBCharChip(int confId)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipInfo GetChipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetChipId()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSuitInfo GetChipSuitConfInfo()
        {
        }
    }
}

