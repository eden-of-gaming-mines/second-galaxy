﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class NpcShipDataContainerBase : INpcShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        protected SolarSystemNpcShipInstanceInfo m_shipInfo;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_GetNpcShipConfigId;
        private static DelegateBridge __Hotfix_GetNpcMonsterConfigId;
        private static DelegateBridge __Hotfix_GetNPCMonsterLevel;
        private static DelegateBridge __Hotfix_GetNPCShipTemplateId;
        private static DelegateBridge __Hotfix_GetNpcMonsterLevelDropConfId;
        private static DelegateBridge __Hotfix_GetNpcMonsterLevelBufConfId;
        private static DelegateBridge __Hotfix_GetShipConfId;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_UpdateShipName;
        private static DelegateBridge __Hotfix_GetOwnerName;
        private static DelegateBridge __Hotfix_UpdateCurrEnergy;
        private static DelegateBridge __Hotfix_UpdateCurrShield;
        private static DelegateBridge __Hotfix_UpdateCurrArmor;
        private static DelegateBridge __Hotfix_GetCurrEnergy;
        private static DelegateBridge __Hotfix_GetCurrShield;
        private static DelegateBridge __Hotfix_GetCurrArmor;
        private static DelegateBridge __Hotfix_GetHighSoltGroupItemList;
        private static DelegateBridge __Hotfix_GetHighSoltAmmoList;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemList;
        private static DelegateBridge __Hotfix_GetLowSlotItemList;
        private static DelegateBridge __Hotfix_UpdateSlotGroupInfo;
        private static DelegateBridge __Hotfix_UpdateSlotGroupAmmoInfo;
        private static DelegateBridge __Hotfix_GetShipStoreItemList;
        private static DelegateBridge __Hotfix_UpdataShipStoreItem;
        private static DelegateBridge __Hotfix_AddShipStoreItem;
        private static DelegateBridge __Hotfix_RemoveShipStoreItem;
        private static DelegateBridge __Hotfix_GetStaticBufList;
        private static DelegateBridge __Hotfix_GetDynamicBufList;
        private static DelegateBridge __Hotfix_AddGuildSolarSystemBuf;
        private static DelegateBridge __Hotfix_CreateNpcSpaceShipInstanceInfoByNpcShipId;
        private static DelegateBridge __Hotfix_GetNpcShipTemplateIdByMonsterLevel;
        private static DelegateBridge __Hotfix_GetNpcMonsterLevelDropInfoByShipConfigAndLevel;
        private static DelegateBridge __Hotfix_GetNpcMonsterLevelBufInfoByShipConfigAndLevel;

        [MethodImpl(0x8000)]
        public NpcShipDataContainerBase()
        {
        }

        [MethodImpl(0x8000)]
        public NpcShipDataContainerBase(SolarSystemNpcShipInstanceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AddGuildSolarSystemBuf(List<int> bufList)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void AddShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static SolarSystemNpcShipInstanceInfo CreateNpcSpaceShipInstanceInfoByNpcShipId(int npcMonsterConfId, int monsterLevel, bool isCaptainShip, string customShipName = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetCurrArmor()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetCurrEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetCurrShield()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<object> GetDynamicBufList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual AmmoInfo[] GetHighSoltAmmoList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ShipSlotGroupInfo[] GetHighSoltGroupItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<ShipSlotGroupInfo> GetLowSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<ShipSlotGroupInfo> GetMiddleSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetNpcMonsterConfigId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetNPCMonsterLevel()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetNpcMonsterLevelBufConfId()
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataNpcMonsterLevelBufInfo GetNpcMonsterLevelBufInfoByShipConfigAndLevel(int npcMonsterLevelBufId, int monsterLevel)
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetNpcMonsterLevelDropConfId()
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataNpcMonsterLevelDropInfo GetNpcMonsterLevelDropInfoByShipConfigAndLevel(int npcMonsterLevelDropId, int monsterLevel)
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetNpcShipConfigId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetNPCShipTemplateId()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetNpcShipTemplateIdByMonsterLevel(ConfigDataNpcShipInfo npcShipConf, int monsterLevel)
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetOwnerName()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetShipConfId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetShipName()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<ShipStoreItemInfo> GetShipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<int> GetStaticBufList()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdataShipStoreItem(ShipStoreItemInfo shipItemInfo, long count, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateCurrArmor(float armor)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateCurrEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateCurrShield(float shield)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateShipName(string shipName)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateSlotGroupAmmoInfo(int groupIndex, StoreItemType ammoType, int ammoConfigId, int ammoCount)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateSlotGroupInfo(ShipEquipSlotType slotType, int groupIndex, int storeItemIndex)
        {
        }
    }
}

