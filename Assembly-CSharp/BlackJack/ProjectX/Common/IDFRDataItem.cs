﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IDFRDataItem : IDFRFilterResultItem
    {
        object GetData();
    }
}

