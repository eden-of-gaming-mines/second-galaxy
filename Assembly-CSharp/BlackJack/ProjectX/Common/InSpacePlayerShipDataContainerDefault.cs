﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class InSpacePlayerShipDataContainerDefault : IInSpacePlayerShipDataContainer, IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        protected SolarSystemPlayerShipInstanceInfo m_shipInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetShipConfId;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_UpdateShipName;
        private static DelegateBridge __Hotfix_GetOwnerName;
        private static DelegateBridge __Hotfix_UpdateCurrEnergy;
        private static DelegateBridge __Hotfix_UpdateCurrShield;
        private static DelegateBridge __Hotfix_UpdateCurrArmor;
        private static DelegateBridge __Hotfix_UpdateShipState;
        private static DelegateBridge __Hotfix_GetCurrEnergy;
        private static DelegateBridge __Hotfix_GetCurrShield;
        private static DelegateBridge __Hotfix_GetCurrArmor;
        private static DelegateBridge __Hotfix_GetGuildFleetId;
        private static DelegateBridge __Hotfix_GetHighSoltGroupItemList;
        private static DelegateBridge __Hotfix_GetHighSoltAmmoList;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemList;
        private static DelegateBridge __Hotfix_GetLowSlotItemList;
        private static DelegateBridge __Hotfix_UpdateSlotGroupInfo;
        private static DelegateBridge __Hotfix_UpdateSlotGroupAmmoInfo;
        private static DelegateBridge __Hotfix_GetShipStoreItemList;
        private static DelegateBridge __Hotfix_UpdataShipStoreItem;
        private static DelegateBridge __Hotfix_AddShipStoreItem;
        private static DelegateBridge __Hotfix_RemoveShipStoreItem;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetShipState;
        private static DelegateBridge __Hotfix_GetShipStoreItemIndex;
        private static DelegateBridge __Hotfix_GetHigSlotWeaponReadyForLaunchTime;
        private static DelegateBridge __Hotfix_GetEquipGroupReadyForLaunchTime;
        private static DelegateBridge __Hotfix_IsAutoFight;
        private static DelegateBridge __Hotfix_GetSuperWeaponEnergy;
        private static DelegateBridge __Hotfix_GetStaticBufList;
        private static DelegateBridge __Hotfix_GetDynamicBufList;
        private static DelegateBridge __Hotfix_AddGuildSolarSystemBuf;
        private static DelegateBridge __Hotfix_IsWingShipEnable;
        private static DelegateBridge __Hotfix_GetLastWingShipOpTime;
        private static DelegateBridge __Hotfix_GetCurrWingShipObjId;
        private static DelegateBridge __Hotfix_GetHateTagetInitInfoList;
        private static DelegateBridge __Hotfix_GetInSpaceShipStoreItemList;
        private static DelegateBridge __Hotfix_GetReadOnlyShipInfo;

        [MethodImpl(0x8000)]
        public void AddGuildSolarSystemBuf(List<int> bufList)
        {
        }

        [MethodImpl(0x8000)]
        public void AddShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrArmor()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrShield()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetCurrWingShipObjId()
        {
        }

        [MethodImpl(0x8000)]
        public List<object> GetDynamicBufList()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetEquipGroupReadyForLaunchTime()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetGuildFleetId()
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<KeyValuePair<uint, uint>> GetHateTagetInitInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual AmmoInfo[] GetHighSoltAmmoList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ShipSlotGroupInfo[] GetHighSoltGroupItemList()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetHigSlotWeaponReadyForLaunchTime()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipStoreItemInfo> GetInSpaceShipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetLastWingShipOpTime()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipSlotGroupInfo> GetLowSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<ShipSlotGroupInfo> GetMiddleSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public string GetOwnerName()
        {
        }

        [MethodImpl(0x8000)]
        public SolarSystemPlayerShipInstanceInfo GetReadOnlyShipInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipConfId()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipName()
        {
        }

        [MethodImpl(0x8000)]
        public ShipState GetShipState()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipStoreItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipStoreItemInfo> GetShipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetStaticBufList()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSuperWeaponEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(SolarSystemPlayerShipInstanceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAutoFight()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWingShipEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdataShipStoreItem(ShipStoreItemInfo shipItemInfo, long count, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrArmor(float armor)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShield(float shield)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipName(string shipName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipState(ShipState state)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateSlotGroupAmmoInfo(int groupIndex, StoreItemType ammoType, int ammoConfigId, int ammoCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSlotGroupInfo(ShipEquipSlotType slotType, int groupIndex, int storeItemIndex)
        {
        }
    }
}

