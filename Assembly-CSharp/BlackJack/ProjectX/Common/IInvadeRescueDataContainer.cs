﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IInvadeRescueDataContainer
    {
        void AddPVPInvadeRescueInfo(PVPInvadeRescueInfo info);
        List<PVPInvadeRescueInfo> GetPVPInvadeRescueList();
        void RemovePVPInvadeRescueInfo(ulong signalInsId, int manualQuestSolarsystemId = 0, uint manualQuestSceneInstanceId = 0);
    }
}

