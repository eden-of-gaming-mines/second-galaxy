﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessSuperRailgunBulletFly : LBSpaceProcess
    {
        protected int m_index;
        protected ushort m_bulletFlyTime;
        protected uint m_onHitTimeAbs;
        protected bool m_isCritical;
        protected LBBulletDamageInfo m_damageInfoForMainTarget;
        protected List<ILBSpaceTarget> m_allExtraTargetList;
        protected List<LBBulletDamageInfo> m_damageInfoListForExtraTarget;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetExtraBulletHitTarget;
        private static DelegateBridge __Hotfix_GetTargetCount;
        private static DelegateBridge __Hotfix_GetIndex;
        private static DelegateBridge __Hotfix_SetBulletDamageForMainTarget;
        private static DelegateBridge __Hotfix_SetBulletDamageForExtraTargets;
        private static DelegateBridge __Hotfix_SetBulletFlyTime;
        private static DelegateBridge __Hotfix_GetBulletFlyTime;
        private static DelegateBridge __Hotfix_SetCritical;
        private static DelegateBridge __Hotfix_GetCritical;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperRailgunBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperRailgunBulletFly(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int index, ILBSpaceProcessSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetBulletFlyTime()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetCritical()
        {
        }

        [MethodImpl(0x8000)]
        public int GetIndex()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTargetCount()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int index, ILBSpaceProcessSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletDamageForExtraTargets(List<LBBulletDamageInfo> damageInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletDamageForMainTarget(bool isCritical, LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletFlyTime(ushort bulletFlyTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCritical(bool isCritical)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExtraBulletHitTarget(List<ILBSpaceTarget> targetList)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

