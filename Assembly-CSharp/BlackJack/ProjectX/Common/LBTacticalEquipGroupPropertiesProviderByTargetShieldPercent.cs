﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderByTargetShieldPercent : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        protected float m_targetShieldCheckPercent;
        protected float m_propertyModifyFactor;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnTargetShieldPercent;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnTargetShieldPercent;
        private static DelegateBridge __Hotfix_get_TargetShieldCheckPercent;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderByTargetShieldPercent(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnTargetShieldPercent(ref float propertyValue, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnTargetShieldPercent(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float TargetShieldCheckPercent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

