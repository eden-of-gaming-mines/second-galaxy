﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessDroneSniperLaunch : LBSpaceProcessDroneLaunchBase
    {
        protected uint m_fightingStateEndAbsTime;
        protected List<uint> m_droneNextAttackAbsTimeList;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_ReturnDrones;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnEnterFight;
        private static DelegateBridge __Hotfix_TickFighting;

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneSniperLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneSniperLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        public override void ReturnDrones(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickFighting(uint currTime)
        {
        }
    }
}

