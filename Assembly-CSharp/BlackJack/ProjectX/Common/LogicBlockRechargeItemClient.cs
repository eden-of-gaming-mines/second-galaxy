﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockRechargeItemClient : LogicBlockRechargeItemBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnRechargeItemDeliver;
        private static DelegateBridge __Hotfix_OnRechargeItemBuy;
        private static DelegateBridge __Hotfix_OnRechargeItemBuyCancel;
        private static DelegateBridge __Hotfix_RefreshAllRechargeItems;
        private static DelegateBridge __Hotfix_GetRechargeItemList;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public List<LBRechargeItem> GetRechargeItemList(Func<int, bool> filter)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeItemBuy(RechargeOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeItemBuyCancel(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeItemDeliver(int itemId, bool isFirstTimeBuy)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllRechargeItems(List<RechargeItemInfo> itemList, int version)
        {
        }

        private IRechargeItemDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

