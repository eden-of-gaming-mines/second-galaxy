﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum SceneTextNoticeType
    {
        AutoOut_Normal,
        AutoOut_Urgent,
        KeepOnScreen_Normal,
        KeepOnScreen_Urgent,
        TipsWindowEx
    }
}

