﻿namespace BlackJack.ProjectX.Common
{
    using System.Collections.Generic;

    public interface IGuildMomentsBase
    {
        List<GuildMomentsInfo> GetGuildMomentsBriefInfoList();
        List<GuildMomentsInfo> GetGuildMomentsInfoList();
    }
}

