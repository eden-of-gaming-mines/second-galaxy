﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GuildStaffingLogType
    {
        Invalid,
        MemberJoinConfirm,
        MemberAutoAdd,
        JobAppoint,
        JobFire,
        MemberLeave,
        MemberKick,
        Max
    }
}

