﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBFormationControllerWildGoose : LBFormationControllerBase
    {
        protected double m_formationMemberDistance;
        protected Vector3D m_formationLeftDirVec;
        protected Vector3D m_formationRightDirVec;
        protected LinkedList<IFormationMember> m_formationLeftMemberList;
        protected LinkedList<IFormationMember> m_formationRightMemberList;
        protected double m_ratioFromMemberSpaceToLeaderShipSize;
        protected double m_formationAngle;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByJoinMember;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByLeaveMember;

        [MethodImpl(0x8000)]
        public override void Initialize(ILBInSpaceShip leader, FormationCreatingInfo formationInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByJoinMember(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByLeaveMember(IFormationMember member)
        {
        }
    }
}

