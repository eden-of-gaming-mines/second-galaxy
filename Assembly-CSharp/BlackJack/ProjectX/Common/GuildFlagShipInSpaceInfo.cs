﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildFlagShipInSpaceInfo
    {
        public ulong m_instanceId;
        public int m_shipConfId;
        public string m_shipName;
        public double m_currFuel;
        public List<int> m_moduleSlotList;
        public uint m_armorCurr;
        public uint m_shieldCurr;
        public ShipState m_shipState;
        public List<OffLineBufInfo> m_offLineBufList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

