﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupIncBufByNearbyDeadBase : LBTacticalEquipGroupBase
    {
        protected List<int> m_bufIdList;
        protected int m_bufGroupId;
        protected LBBufBase m_currBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsTargetValid;
        private static DelegateBridge __Hotfix_GetBufLevelMax;
        private static DelegateBridge __Hotfix_GetCurrBufLevel;
        private static DelegateBridge __Hotfix_GetBufIdByLevel;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupIncBufByNearbyDeadBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetBufIdByLevel(int currBufLevel)
        {
        }

        [MethodImpl(0x8000)]
        public int GetBufLevelMax()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrBufLevel()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }

        protected abstract void OnTargetDead(ILBSpaceTarget target, IFFState iffState);
        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

