﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockNavigationClient : LogicBlockNavigationBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNavigationStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNavigationStop;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNavigationDestChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnNavigationFinished;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnNavigationInfoNtf;
        private static DelegateBridge __Hotfix_OnNavigationStop;
        private static DelegateBridge __Hotfix_OnSolarSystemChanged;
        private static DelegateBridge __Hotfix_add_EventOnNavigationStart;
        private static DelegateBridge __Hotfix_remove_EventOnNavigationStart;
        private static DelegateBridge __Hotfix_add_EventOnNavigationStop;
        private static DelegateBridge __Hotfix_remove_EventOnNavigationStop;
        private static DelegateBridge __Hotfix_add_EventOnNavigationDestChanged;
        private static DelegateBridge __Hotfix_remove_EventOnNavigationDestChanged;
        private static DelegateBridge __Hotfix_add_EventOnNavigationFinished;
        private static DelegateBridge __Hotfix_remove_EventOnNavigationFinished;

        public event Action EventOnNavigationDestChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnNavigationFinished
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnNavigationStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnNavigationStop
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnNavigationInfoNtf(int startSolarSystemId, int endSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnNavigationStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSolarSystemChanged(int solarSystemId)
        {
        }
    }
}

