﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum QuestSrcType
    {
        None,
        GM,
        Common,
        ManualSignal,
        EmergencySignal_SignalDifficultGuide,
        FactionCredit,
        BranchStory,
        ItemUse,
        BattlePass
    }
}

