﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class DelegateMissionInvadeInfo
    {
        public PlayerSimpleInfo m_playerInfo;
        public DateTime m_startTime;
        public DateTime m_endTime;
        public List<ulong> m_destroyCaptainInstanceIdList;
        public List<DelegateMissionInvadeEventInfo> m_eventInfoList;
        public List<SimpleItemInfoWithCount> m_lostDelegateItemList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

