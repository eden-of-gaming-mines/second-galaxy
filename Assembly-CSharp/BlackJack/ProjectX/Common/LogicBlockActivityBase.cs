﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockActivityBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IActivityDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPlayerActivityList;
        private static DelegateBridge __Hotfix_GetActivityPoint;
        private static DelegateBridge __Hotfix_GetVitality;
        private static DelegateBridge __Hotfix_GetVitalityRewardList;
        private static DelegateBridge __Hotfix_CheckVitalityRewardGet;
        private static DelegateBridge __Hotfix_GetActivityPlayCountLimit;
        private static DelegateBridge __Hotfix_CheckVitalityGetReward;

        [MethodImpl(0x8000)]
        public bool CheckVitalityGetReward(int index, out ConfigDataVitalityRewardInfo rewardInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckVitalityRewardGet(int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetActivityPlayCountLimit(ConfigDataActivityInfo confInfo = null, int activityId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public int GetActivityPoint()
        {
        }

        [MethodImpl(0x8000)]
        public List<PlayerActivityInfo> GetPlayerActivityList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetVitality()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetVitalityRewardList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

