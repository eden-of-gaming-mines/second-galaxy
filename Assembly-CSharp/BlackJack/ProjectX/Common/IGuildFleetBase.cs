﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildFleetBase
    {
        bool CheckForFleetCreating(string gameUserId, string fleetName, out int errCode);
        bool CheckForFleetDetailInfoReq(string gameUserId, ulong fleetId, out int errCode);
        bool CheckForFleetDismiss(string gameUserId, ulong fleetId, out int errCode);
        bool CheckForFleetKickOut(string srcGameUserId, ulong fleetId, string destGameUserId, out int errCode);
        bool CheckForFleetListReq(string gameUserId, out int errCode);
        bool CheckForFleetMemberJoin(string gameUserId, ulong fleetId, out int errCode);
        bool CheckForFleetMemberLeave(string gameUserId, ulong fleetId, out int errCode);
        bool CheckForFleetRename(string gameUserId, ulong fleetId, string fleetName, out int errCode);
        bool CheckForFleetRename4DomesticPackage(string gameUserId, ulong fleetId, string fleetName, out int errCode);
        bool CheckForFleetSetAllowToJoinFleetManual(string gameUserId, ulong fleetId, bool allowToJoinFleetManual, out int errCode);
        bool CheckForFleetSetFormationActive(string gameUserId, ulong fleetId, bool isFormationActive, out int errCode);
        bool CheckForFleetSetFormationType(string gameUserId, ulong fleetId, int formationType, out int errCode);
        bool CheckForGuildFleetFireFocusTarget(string srcGameUserId, ulong fleetId, out int errCode);
        bool CheckForGuildFleetMemberRetire(string srcGameUserId, ulong fleetId, List<uint> positionList, string destGameUserId, out int errCode);
        bool CheckForGuildFleetMembersJumping(string srcGameUserId, ulong fleetId, out int errCode);
        bool CheckForGuildFleetMembersUseStargate(string srcGameUserId, ulong fleetId, out int errCode);
        bool CheckForGuildFleetProtectTarget(string srcGameUserId, ulong fleetId, out int errCode);
        bool CheckForGuildFleetSetCommanderReq(string srcGameUserId, ulong fleetId, string destGameUserId, out int errCode);
        bool CheckForGuildFleetSetInternalPositions(string gameUserId, ulong fleetId, GuildFleetMemberInfo member, bool hasPermission, out int errCode);
        bool CheckForGuildFleetTransferPosition(string srcGameUserId, ulong fleetId, string destGameUserId, FleetPosition position, out int errCode);
        GuildFleetInfo GetGuildFleetById(ulong fleetId);
        GuildFleetMemberInfo GetGuildFleetMemberByFleetPosition(ulong fleetId, FleetPosition position);
        ulong GetGuildPlayerFleetId(string gameUserId);
    }
}

