﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum MisbehaviorTargetType
    {
        Invalid,
        Chat,
        Mail,
        Comment,
        Max
    }
}

