﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessSuperRailgunLaunch : LBSpaceProcessSuperWeaponLaunchBase
    {
        protected List<ILBSpaceTarget> m_extraHitTargetList;
        protected List<LBBulletDamageInfo> m_damageInfoForExtraTargets;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetExtraHitTarget;
        private static DelegateBridge __Hotfix_GetExtraHitTarget;
        private static DelegateBridge __Hotfix_GetDamageInfoForExtraTargets;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperRailgunLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperRailgunLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBulletDamageInfo> GetDamageInfoForExtraTargets()
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> GetExtraHitTarget()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExtraHitTarget(List<ILBSpaceTarget> extraTargetList, List<LBBulletDamageInfo> damageInfoForExtraTargets)
        {
        }
    }
}

