﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildAntiTeleportEffectInfo
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public DateTime m_antiTeleportEffectStartTime;
        public DateTime m_antiTeleportEffectEndTime;
        public DateTime m_antiTeleportEffectCDEndTime;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo(GuildAntiTeleportEffectInfo info)
        {
        }
    }
}

