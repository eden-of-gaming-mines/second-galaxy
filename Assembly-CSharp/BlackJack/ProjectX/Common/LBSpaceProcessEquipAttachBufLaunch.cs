﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessEquipAttachBufLaunch : LBSpaceProcessEquipLaunchBase
    {
        protected List<int> m_bufList;
        protected List<uint> m_attachBufTargetList;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetBufListInfo_0;
        private static DelegateBridge __Hotfix_SetBufListInfo_1;
        private static DelegateBridge __Hotfix_GetBufList;
        private static DelegateBridge __Hotfix_SetAttachBufTargetList;
        private static DelegateBridge __Hotfix_GetAttachBufTarget;
        private static DelegateBridge __Hotfix_AddAttachBufTarget;
        private static DelegateBridge __Hotfix_OnEquipFire;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAttachBufLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAttachBufLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public void AddAttachBufTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetAttachBufTarget()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetBufList()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEquipFire(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttachBufTargetList(List<uint> targetList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBufListInfo(List<int> bufList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBufListInfo(params int[] bufList)
        {
        }
    }
}

