﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class TechUpgradeInfo
    {
        public int m_id;
        public int m_level;
        public DateTime m_startTime;
        public DateTime m_endTime;
        public DateTime m_originalEndTime;
        public ulong m_workingCaptain;
        public List<CostInfo> m_costList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetRestTime;
        private static DelegateBridge __Hotfix_GetTotalTime;

        [MethodImpl(0x8000)]
        public TimeSpan GetRestTime(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public TimeSpan GetTotalTime()
        {
        }
    }
}

