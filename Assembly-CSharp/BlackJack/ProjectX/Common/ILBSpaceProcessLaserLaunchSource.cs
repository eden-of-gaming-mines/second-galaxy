﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessLaserLaunchSource : ILBSpaceProcessWeaponLaunchSource, ILBSpaceProcessSource
    {
        void OnProcessUnitFireOnHit(LBSpaceProcessLaserSingleFire process, uint processTime);
    }
}

