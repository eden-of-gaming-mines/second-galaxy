﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildRedPointInfo
    {
        public int GuildJoinApplyCount;
        public int GuildMessageBoardVersion;
        public int AllianceInviteCount;
        public bool HasAnyBenefits2Balance;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

