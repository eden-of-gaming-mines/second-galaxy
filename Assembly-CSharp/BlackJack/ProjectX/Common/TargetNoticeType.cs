﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum TargetNoticeType
    {
        None,
        EnemyNormal,
        FriendlyNormal,
        Interaction,
        Dialog
    }
}

