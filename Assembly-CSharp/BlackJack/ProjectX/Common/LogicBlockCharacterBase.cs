﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCharacterBase
    {
        public Action<int> EventOnLevelUp;
        public Action<int, int> EventOnLevelUpBalance;
        public Action<int, bool> EventOnEvaluateScoreUpdate;
        public Action<uint> EventOnPrepareAddCharacterExp;
        protected ILBPlayerContext m_lbPlayerCtx;
        protected ICharacterDataContainer m_characterDC;
        protected GDBSolarSystemInfo m_currGDBSolarSystemInfo;
        protected LBCharacterStaticShipCaptain m_staticCaptain;
        private float m_questAfkFactor;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_InitCharacterEvaluateScore;
        private static DelegateBridge __Hotfix_CreateStaticCaptain;
        private static DelegateBridge __Hotfix_GetStaticCaptain;
        private static DelegateBridge __Hotfix_OnCharacterEvaluateScoreUpdate;
        private static DelegateBridge __Hotfix_CheckCostInfo;
        private static DelegateBridge __Hotfix_ApplyCostInfo;
        private static DelegateBridge __Hotfix_CalcRealMoneyByCostInfo;
        private static DelegateBridge __Hotfix_GetCurrTotalExp;
        private static DelegateBridge __Hotfix_GetCurrLevelExp;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetDailyExpAchieved;
        private static DelegateBridge __Hotfix_GetAFKExp;
        private static DelegateBridge __Hotfix_GetProfession;
        private static DelegateBridge __Hotfix_GetMotherLand;
        private static DelegateBridge __Hotfix_GetPlayerUserId;
        private static DelegateBridge __Hotfix_GetCreateTimeUtc;
        private static DelegateBridge __Hotfix_GetGender;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetAvatarId;
        private static DelegateBridge __Hotfix_OnAddExp;
        private static DelegateBridge __Hotfix_OnLevelUp;
        private static DelegateBridge __Hotfix_GetBindMoney;
        private static DelegateBridge __Hotfix_UpdateBindMoney;
        private static DelegateBridge __Hotfix_ModifyBindMoney;
        private static DelegateBridge __Hotfix_GetRealMoney;
        private static DelegateBridge __Hotfix_UpdateRealMoney;
        private static DelegateBridge __Hotfix_ModifyRealMoney;
        private static DelegateBridge __Hotfix_GetTradeMoney;
        private static DelegateBridge __Hotfix_UpdateTradeMoney;
        private static DelegateBridge __Hotfix_ModifyTradeMoney;
        private static DelegateBridge __Hotfix_GetPanGala;
        private static DelegateBridge __Hotfix_UpdatePanGala;
        private static DelegateBridge __Hotfix_ModifyPanGala;
        private static DelegateBridge __Hotfix_GetGuildGala;
        private static DelegateBridge __Hotfix_UpdateGuildGala;
        private static DelegateBridge __Hotfix_ModifyGuildGala;
        private static DelegateBridge __Hotfix_GetCreditCurrencyByCurrencyType;
        private static DelegateBridge __Hotfix_ModifyCreditCurrency;
        private static DelegateBridge __Hotfix_UpdateCreditCurrency;
        private static DelegateBridge __Hotfix_GetCharacterEvaluateScore;
        private static DelegateBridge __Hotfix_UpdateEvaluateScore;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_GetCurrSolarSystemSecurityLevel;
        private static DelegateBridge __Hotfix_UpdateSolarSystemId;
        private static DelegateBridge __Hotfix_GetCurrGDBSolarSystemInfo;
        private static DelegateBridge __Hotfix_SetGDBSolarSystemInfo;
        private static DelegateBridge __Hotfix_IsInSpace;
        private static DelegateBridge __Hotfix_UpdateIsInSpace;
        private static DelegateBridge __Hotfix_IsInDuplicate;
        private static DelegateBridge __Hotfix_UpdateIsInDuplicate;
        private static DelegateBridge __Hotfix_GetWormholeGateSolarsystemId;
        private static DelegateBridge __Hotfix_UpdateWormholeGateSolarsystemId;
        private static DelegateBridge __Hotfix_GetSpaceStationId;
        private static DelegateBridge __Hotfix_UpdateSpaceStationId;
        private static DelegateBridge __Hotfix_IsStayWithMotherShip;
        private static DelegateBridge __Hotfix_GetCurrShipInstanceId;
        private static DelegateBridge __Hotfix_UpdateCurrShipInstanceId;
        private static DelegateBridge __Hotfix_GetCurrShip;
        private static DelegateBridge __Hotfix_GetCurrShipLocation;
        private static DelegateBridge __Hotfix_GetCurrShipRotation;
        private static DelegateBridge __Hotfix_UpdateCurrShipPose;
        private static DelegateBridge __Hotfix_ModifyCurrency;
        private static DelegateBridge __Hotfix_BatchModifyCurrency;
        private static DelegateBridge __Hotfix_GetCurrencyByType;
        private static DelegateBridge __Hotfix_CheckEnoughCurrency;
        private static DelegateBridge __Hotfix_CheckDailyExpLimit;
        private static DelegateBridge __Hotfix_CalcAFKExp;
        private static DelegateBridge __Hotfix_CalcExpWithServerLevelFactor;
        private static DelegateBridge __Hotfix_GetExpServerLevelFactor;
        private static DelegateBridge __Hotfix_ModifyCurrencyInternal;
        private static DelegateBridge __Hotfix_CalcCharacterEvaluateScore;

        [MethodImpl(0x8000)]
        public bool ApplyCostInfo(List<CostInfo> costList, OperatieLogMoneyChangeType moneyChangeCauseId = 0, OperateLogItemChangeType itemChangeCauseId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public void BatchModifyCurrency(List<KeyValuePair<CurrencyType, long>> currencyChange, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public uint CalcAFKExp(long realAddExp)
        {
        }

        [MethodImpl(0x8000)]
        protected int CalcCharacterEvaluateScore()
        {
        }

        [MethodImpl(0x8000)]
        public uint CalcExpWithServerLevelFactor(long realAddExp)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcRealMoneyByCostInfo(List<CostInfo> costList)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCostInfo(List<CostInfo> costList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public uint CheckDailyExpLimit(long realAddExp)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckEnoughCurrency(List<KeyValuePair<CurrencyType, long>> currencyNeed)
        {
        }

        [MethodImpl(0x8000)]
        protected LBCharacterStaticShipCaptain CreateStaticCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public long GetAFKExp()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAvatarId()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetBindMoney()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCharacterEvaluateScore()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetCreateTimeUtc()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetCreditCurrencyByCurrencyType(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetCurrencyByType(CurrencyType currencyType)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemInfo GetCurrGDBSolarSystemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetCurrLevelExp()
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip GetCurrShip()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetCurrShipInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetCurrShipLocation()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetCurrShipRotation()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrSolarSystemSecurityLevel()
        {
        }

        [MethodImpl(0x8000)]
        public long GetCurrTotalExp()
        {
        }

        [MethodImpl(0x8000)]
        public long GetDailyExpAchieved()
        {
        }

        [MethodImpl(0x8000)]
        public float GetExpServerLevelFactor()
        {
        }

        [MethodImpl(0x8000)]
        public GenderType GetGender()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetGuildGala()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction GetMotherLand()
        {
        }

        [MethodImpl(0x8000)]
        public string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetPanGala()
        {
        }

        [MethodImpl(0x8000)]
        public string GetPlayerUserId()
        {
        }

        [MethodImpl(0x8000)]
        public ProfessionType GetProfession()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetRealMoney()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSpaceStationId()
        {
        }

        [MethodImpl(0x8000)]
        public ILBShipCaptain GetStaticCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetTradeMoney()
        {
        }

        [MethodImpl(0x8000)]
        public int GetWormholeGateSolarsystemId()
        {
        }

        [MethodImpl(0x8000)]
        public void InitCharacterEvaluateScore()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInDuplicate()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInSpace()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStayWithMotherShip()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ModifyBindMoney(long addValue, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ModifyCreditCurrency(CurrencyType type, long addValue, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public void ModifyCurrency(CurrencyType currencyType, long addValue, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        private static ulong ModifyCurrencyInternal(long addValue, ulong orgValue)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ModifyGuildGala(long addValue, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ModifyPanGala(long addValue, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ModifyRealMoney(long addValue, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ModifyTradeMoney(long addValue, OperatieLogMoneyChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual uint OnAddExp(uint addExp, PlayerExpAddSrcType srcType, bool ignoreDailyExpLimit = true)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCharacterEvaluateScoreUpdate(bool isExecuteForInit = false)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLevelUp()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetGDBSolarSystemInfo(GDBSolarSystemInfo gdbSolarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBindMoney(ulong currentyValue)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCreditCurrency(CurrencyType type, ulong updateInfoCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShipInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShipPose(Vector3D location, Vector3D roation)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEvaluateScore(int value)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildGala(ulong currentyValue)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateIsInDuplicate(bool inDuplicate)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateIsInSpace(bool inSpace)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePanGala(ulong currentyValue)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRealMoney(ulong currentyValue)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpaceStationId(int spaceStationId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTradeMoney(ulong currentyValue)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWormholeGateSolarsystemId(int solarSystemId)
        {
        }
    }
}

