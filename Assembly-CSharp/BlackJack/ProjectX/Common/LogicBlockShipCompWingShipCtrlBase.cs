﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompWingShipCtrlBase
    {
        protected ILBInSpaceShip m_ownerShip;
        protected ILBSpaceContext m_spaceCtx;
        protected LogicBlockShipCompSceneBase m_lbScene;
        protected bool m_isWingShipEnable;
        protected uint m_lastWingShipOpTime;
        protected SceneWingShipSetting m_currSceneWingShipSetting;
        protected ILBInSpaceNpcShip m_currWingShip;
        protected uint m_currWingShipObjId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CheckWingShipEnable;
        private static DelegateBridge __Hotfix_CheckWingShipDisable;
        private static DelegateBridge __Hotfix_IsWingShipEnable;
        private static DelegateBridge __Hotfix_GetSceneWingShipSetting;
        private static DelegateBridge __Hotfix_GetCurrWingShip;
        private static DelegateBridge __Hotfix_GetLastWingShipOptTime;
        private static DelegateBridge __Hotfix_GetCurrWingShipObjId;
        private static DelegateBridge __Hotfix_WingShipEnableImpl;
        private static DelegateBridge __Hotfix_WingShipDisableImpl;
        private static DelegateBridge __Hotfix_OnWingShipHitByTarget;
        private static DelegateBridge __Hotfix_OnEnterFight;

        [MethodImpl(0x8000)]
        public bool CheckWingShipDisable(bool needCheckCD, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckWingShipEnable(bool needCheckCD, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpaceNpcShip GetCurrWingShip()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetCurrWingShipObjId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetLastWingShipOptTime()
        {
        }

        [MethodImpl(0x8000)]
        public SceneWingShipSetting GetSceneWingShipSetting()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWingShipEnable()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnWingShipHitByTarget(ILBSpaceTarget target, bool isCritical, LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void WingShipDisableImpl()
        {
        }

        [MethodImpl(0x8000)]
        protected void WingShipEnableImpl()
        {
        }
    }
}

