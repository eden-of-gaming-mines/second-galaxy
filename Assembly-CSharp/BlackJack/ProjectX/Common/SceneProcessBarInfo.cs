﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct SceneProcessBarInfo
    {
        public SceneProcessBarType m_type;
        public int m_messageId;
        public uint m_npcObjId;
        public Vector3D m_location;
        public uint m_startTime;
        public uint m_endTime;
    }
}

