﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessBulletFly : LBSpaceProcess
    {
        protected int m_index;
        protected ushort m_bulletFlyTime;
        protected uint m_onHitTimeAbs;
        protected byte Flag;
        protected readonly LBBulletDamageInfo m_damageInfo;
        protected WeaponCategory m_sourceWeaponCategory;
        protected ShipEquipSlotType m_slotType;
        protected int m_slotIndex;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_GetIndex;
        private static DelegateBridge __Hotfix_SetBulletDamage;
        private static DelegateBridge __Hotfix_SetBulletFlyTime;
        private static DelegateBridge __Hotfix_GetBulletFlyTime;
        private static DelegateBridge __Hotfix_SetHit;
        private static DelegateBridge __Hotfix_GetHit;
        private static DelegateBridge __Hotfix_SetCritical;
        private static DelegateBridge __Hotfix_GetCritical;
        private static DelegateBridge __Hotfix_SetDestoryed;
        private static DelegateBridge __Hotfix_GetDestroyed;
        private static DelegateBridge __Hotfix_SetSourceWeaponCategory;
        private static DelegateBridge __Hotfix_GetSourceWeaponCategory;
        private static DelegateBridge __Hotfix_IsLaunchBySuperWeapon;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_ClearProcess;

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletFly(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int index, ILBSpaceProcessSource processSource, ShipEquipSlotType slotType, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetBulletFlyTime()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetCritical()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetDestroyed()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetHit()
        {
        }

        [MethodImpl(0x8000)]
        public int GetIndex()
        {
        }

        [MethodImpl(0x8000)]
        public WeaponCategory GetSourceWeaponCategory()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int index, ILBSpaceProcessSource processSource, ShipEquipSlotType slotType, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLaunchBySuperWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletDamage(bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, DamageInfoCalculation calculation = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletFlyTime(ushort bulletFlyTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCritical(bool isCritical)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDestoryed(bool isDestroyed)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHit(bool isHit)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSourceWeaponCategory(WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        public delegate void DamageInfoCalculation(LBBulletDamageInfo srcInfo, LBBulletDamageInfo destInfo);
    }
}

