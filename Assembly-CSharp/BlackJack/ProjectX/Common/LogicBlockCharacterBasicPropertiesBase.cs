﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCharacterBasicPropertiesBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnPropertiesGroupDirty;
        protected ICharacterPropertiesDataContainer m_dc;
        protected ILBPlayerContext m_lbPlayerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetFreeBasicProperties;
        private static DelegateBridge __Hotfix_GetBasicPropertiesByCategory;
        private static DelegateBridge __Hotfix_GetBasicPropertiesById;
        private static DelegateBridge __Hotfix_GetCurrMaxPropertiesPointCount;
        private static DelegateBridge __Hotfix_InitBasicPropertiesByResetProfession;
        private static DelegateBridge __Hotfix_AddBasicPropertiesById;
        private static DelegateBridge __Hotfix_CheckClearBasicProperties;
        private static DelegateBridge __Hotfix_ClearBasicProperties;
        private static DelegateBridge __Hotfix_GetReturnPropertyPointAfterClearProperties;
        private static DelegateBridge __Hotfix_CalcAllFreeBasicPropertiesAfterClearProperties;
        private static DelegateBridge __Hotfix_OnLevelUp;
        private static DelegateBridge __Hotfix_RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_add_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_remove_EventOnPropertiesGroupDirty;

        public event Action<int> EventOnPropertiesGroupDirty
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool AddBasicPropertiesById(List<int> inputPropertiesIdList, List<int> inputCountList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcAllFreeBasicPropertiesAfterClearProperties()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckClearBasicProperties(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool ClearBasicProperties(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public float GetBasicPropertiesByCategory(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetBasicPropertiesById(PropertiesId propertiesId, out float propertiesValue)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrMaxPropertiesPointCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFreeBasicProperties()
        {
        }

        [MethodImpl(0x8000)]
        public int GetReturnPropertyPointAfterClearProperties()
        {
        }

        [MethodImpl(0x8000)]
        public void InitBasicPropertiesByResetProfession(ProfessionType profession)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLevelUp(int currLevel)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }
    }
}

