﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBInSpaceWeaponEquipGroupBase : LBWeaponEquipGroupBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnLaunch2Target;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBInSpaceWeaponEquipGroupBase> EventOnLaunchCycleStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, bool> EventOnLaunchCycleEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<AmmoInfo, int> EventOnAmmoReduce;
        protected ILBInSpaceShip m_ownerShip;
        protected int m_equipedWeaponTurretCount;
        protected AmmoInfo m_ammoInfo;
        protected LBStaticWeaponEquipSlotGroup m_staticGroup;
        protected uint m_readyForLaunchTime;
        protected List<LBSpaceProcess> m_processingList;
        protected List<LBSpaceProcess> m_processingListToBeCancel;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetGlobalLogicBlockCompPropertiesCalc;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_IsTargetValid;
        private static DelegateBridge __Hotfix_IsTargetOnFireRange;
        private static DelegateBridge __Hotfix_GetStaticWeaponEquipGroup;
        private static DelegateBridge __Hotfix_GetReadyForLauchTime;
        private static DelegateBridge __Hotfix_CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_CalcTargetVolumnRatioFinal;
        private static DelegateBridge __Hotfix_CalcTargetTransverseVelocity;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickOnFrameEnd;
        private static DelegateBridge __Hotfix_SetReadyForLaunchTime;
        private static DelegateBridge __Hotfix_StartGroupCD;
        private static DelegateBridge __Hotfix_IsPlayerFlagShipWeaponEquipGroup;
        private static DelegateBridge __Hotfix_CancelProcess;
        private static DelegateBridge __Hotfix_CancelCurrLaunchProcess;
        private static DelegateBridge __Hotfix_CancelLaunchProcessByDestTarget;
        private static DelegateBridge __Hotfix_CancelAllProcessing;
        private static DelegateBridge __Hotfix_ReduceAmmoCount;
        private static DelegateBridge __Hotfix_FireEventOnLaynchCycleStart;
        private static DelegateBridge __Hotfix_FireEventOnLaunch2Target;
        private static DelegateBridge __Hotfix_FireEventOnLaunchCycleEnd;
        private static DelegateBridge __Hotfix_FireEventOnAmmoReduce;
        private static DelegateBridge __Hotfix_add_EventOnLaunch2Target;
        private static DelegateBridge __Hotfix_remove_EventOnLaunch2Target;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCycleStart;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCycleStart;
        private static DelegateBridge __Hotfix_add_EventOnLaunchCycleEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchCycleEnd;
        private static DelegateBridge __Hotfix_add_EventOnAmmoReduce;
        private static DelegateBridge __Hotfix_remove_EventOnAmmoReduce;

        public event Action<AmmoInfo, int> EventOnAmmoReduce
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnLaunch2Target
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, bool> EventOnLaunchCycleEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase> EventOnLaunchCycleStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LBInSpaceWeaponEquipGroupBase(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcTargetTransverseVelocity(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcTargetVolumnRatioFinal(PropertiesId fireCtrlAccuracyPropertiesId, float weaponTransverseVelocity, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CancelAllProcessing()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CancelCurrLaunchProcess()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CancelLaunchProcessByDestTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CancelProcess(LBSpaceProcess process)
        {
        }

        public abstract bool CanMakeDamage();
        [MethodImpl(0x8000)]
        protected void FireEventOnAmmoReduce(int changeCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnLaunchCycleEnd(LBInSpaceWeaponEquipGroupBase group, bool isCancel)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnLaynchCycleStart(LBInSpaceWeaponEquipGroupBase group)
        {
        }

        public abstract int GetAmmoClipSize();
        public abstract int GetAmmoConfId();
        public abstract int GetAmmoCount();
        public abstract StoreItemType GetAmmoType();
        [MethodImpl(0x8000)]
        protected override LogicBlockCompPropertiesCalc GetGlobalLogicBlockCompPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetReadyForLauchTime()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetStaticWeaponEquipGroup()
        {
        }

        public abstract bool IsHostile();
        public abstract bool IsNeedTargetOnLaunch();
        public abstract bool IsOrdinaryEquip();
        public abstract bool IsOrdinaryWeapon();
        [MethodImpl(0x8000)]
        protected bool IsPlayerFlagShipWeaponEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsReadyForLaunch(out int errCode)
        {
        }

        public abstract bool IsSuperEquip();
        public abstract bool IsSuperWeapon();
        [MethodImpl(0x8000)]
        public virtual bool IsTargetOnFireRange(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ReduceAmmoCount(int changeCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetReadyForLaunchTime(uint time)
        {
        }

        [MethodImpl(0x8000)]
        public void StartGroupCD(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void TickOnFrameEnd()
        {
        }
    }
}

