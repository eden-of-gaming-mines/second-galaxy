﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBPlayerContextDCProvider
    {
        IActivityDataContainer GetActivityDC();
        IAuctionDataContainer GetAuctionDC();
        IBattlePassDataContainer GetBattlePassDC();
        ICharacterChipDataContainer GetCharacterChipDC();
        ICharacterDataContainer GetCharacterDC();
        ICharacterFactionDataContainer GetCharacterFactionDC();
        ICharacterPropertiesDataContainer GetCharacterPropertiesDC();
        ICharacterPVPContainer GetCharacterPVPDC();
        ICharacterSkillDataContainer GetCharacterSkillDC();
        IChatDataContainer GetChatDC();
        ICrackDataContainer GetCrackDC();
        ICustomizedParameterDataContainer GetCustomizedParameterDC();
        IDelegateMissionListDataContainer GetDelegateMissionDC();
        IDevelopmentDataContainer GetDevelopmentDC();
        IDrivingLicenseDataContainer GetDrivingLicenseDC();
        IFleetListDataContainer GetFleetListDC();
        IFunctionOpenStateDataContainer GetFunctionOpenStateDC();
        IHiredCaptainManagementDataContainer GetHiredCaptainManagementDC();
        IInvadeRescueDataContainer GetInvadeRescueDC();
        IItemStoreDataContainer GetItemStoreDC();
        IKillRecordDataContainer GetKillRecordDC();
        ILoginActivityDataContainer GetLoginActivityDC();
        IMailDataContainer GetMailDC();
        IMotherShipBasicDataContainer GetMotherShipBasicDC();
        INpcShopDataContainer GetNpcShopDC();
        IDiplomacyDataContainer GetPlayerDiplomacyDC();
        IGuildDataContainerForPlayerCtx GetPlayerGuildDC();
        IProduceDataContainer GetProduceDC();
        IQuestDataContainer GetQuestDC();
        IRechargeGiftPackageDataContainer GetRechargeGiftPackageDC();
        IRechargeItemDataContainer GetRechargeItemDC();
        IRechargeMonthlyCardDataContainer GetRechargeMonthlyCardDC();
        IRechargeOrderDataContainer GetRechargeOrderDC();
        IShipCustomTemplateDataContainer GetShipCustomTemplateDC();
        IShipHangarListDataContainer GetShipHangarListDC();
        IShipSalvageDataContainer GetShipSalvageDC();
        ISignalDataContainer GetSignalDC();
        ISpaceSignalDataContainer GetSpaceSignalDC();
        IStarMapGuildOccupyDataContainer GetStartMapGuildOccupyDC();
        ISystemPushDataContainer GetSystemPushDC();
        ITechDataContainer GetTechDC();
        IUserGuideDataContainer GetUserGuideDC();
        IUserSettingDataContainer GetUserSettingDC();
    }
}

