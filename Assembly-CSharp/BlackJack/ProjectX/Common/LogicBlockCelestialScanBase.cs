﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCelestialScanBase
    {
        protected ILBPlayerContext m_playerContext;
        private const double DistanceThresholdMult = 0.2;
        private const double PlanetGravityThresholdMult = 0.2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CheckStarScanByQuest;
        private static DelegateBridge __Hotfix_CheckPlanetScanByQuest;
        private static DelegateBridge __Hotfix_CheckSolarSystemScanByQuest;
        private static DelegateBridge __Hotfix_IsStarScanQuest;
        private static DelegateBridge __Hotfix_IsPlanetScanQuest;
        private static DelegateBridge __Hotfix_IsSolarSystemScanQuest;
        private static DelegateBridge __Hotfix_GetQuestFirstCondType;
        private static DelegateBridge __Hotfix_CheckCelestialScaningDistanceValid;

        [MethodImpl(0x8000)]
        private bool CheckCelestialScaningDistanceValid(Vector3D scanLocation, Vector3D celestialLocation, uint celestialRadius)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckPlanetScanByQuest(int questInstanceId, Vector3D location, GDBPlanetInfo planetInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSolarSystemScanByQuest(int questInstanceId, GDBSolarSystemInfo solarSytemInfo, bool bIsInfected, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckStarScanByQuest(int questInstanceId, Vector3D location, GDBStarInfo starInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        private QuestCompleteCondType? GetQuestFirstCondType(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPlanetScanQuest(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemScanQuest(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStarScanQuest(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

