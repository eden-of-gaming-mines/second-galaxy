﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class SovereignBattleSimpleInfo
    {
        public DateTime m_startTime;
        public GuildSimplestInfo m_attackerGuildInfo;
        public GuildSimplestInfo m_defenderGuildInfo;
        public int m_npcGuildConfigId;
        public GuildBattleStatus m_status;
        public DateTime m_currStatusStartTime;
        public DateTime m_currStatusEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

