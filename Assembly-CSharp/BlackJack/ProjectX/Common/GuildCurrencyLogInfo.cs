﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCurrencyLogInfo : IDFRFilterResultItem
    {
        public GuildCurrencyLogType m_type;
        public ulong m_instanceId;
        public string m_operatorName;
        public long m_changeCount;
        public ulong m_currBalance;
        public int m_param;
        public DateTime m_time;
        public string m_gameUserId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetId;

        [MethodImpl(0x8000)]
        public ulong GetId()
        {
        }
    }
}

