﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LogicBlockShipCompFireControllBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEnterFight;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLeaveFight;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnLockTarget;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventOnLockedByOtherTarget;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnClearLockedByOtherTarget;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget, ILBInSpaceShip> EventOnTargetAdd2HateList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLaunchSuperWeaponEquip;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventOnKillOtherTarget;
        protected ILBInSpaceShip m_ownerShip;
        protected ILogicBlockShipCompSpaceObject m_lbSpaceObject;
        protected LogicBlockShipCompIFFBase m_lbIFF;
        protected LogicBlockShipCompTargetSelectBase m_lbTargetSelect;
        protected LogicBlockShipCompSceneBase m_lbScene;
        protected LogicBlockShipCompInSpaceBasicCtxBase m_lbBasicCtx;
        protected bool m_isSubSystemEnable;
        protected int m_isSubSystemEnableRef;
        protected uint m_lockedTargetObjId;
        protected bool m_isInFight;
        protected List<uint> m_hateTargetObjIdList;
        protected Dictionary<uint, uint> m_hateTargetTimeOutDict;
        protected uint m_hateTargetListLastUpdateTime;
        protected uint m_hateTargetListTickTime;
        protected int m_hateNpcTargetCount;
        protected uint m_lastAttackMeSrcTargetObjId;
        protected bool m_enableNormalAttackAutoRun;
        protected bool m_enableHighSlotAutoRun;
        protected bool m_enableSuperWeaponAutoRun;
        protected bool m_enableMiddleSlotAutoRun;
        protected bool m_neverFight2Player;
        protected bool m_isInfiniteAmmo;
        private const uint ClearAllTargetDelay = 0x1388;
        private const uint HostileBehaviorTickPeriod = 0x1388;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_EnableSubSystem;
        private static DelegateBridge __Hotfix_IsInFight;
        private static DelegateBridge __Hotfix_GetLockedTarget;
        private static DelegateBridge __Hotfix_GetFirstPriorityEnemyTarget;
        private static DelegateBridge __Hotfix_CheckLockTarget;
        private static DelegateBridge __Hotfix_TryLockTarget;
        private static DelegateBridge __Hotfix_TryLockTargetWithDelay;
        private static DelegateBridge __Hotfix_SetLockTarget;
        private static DelegateBridge __Hotfix_ClearLockTarget;
        private static DelegateBridge __Hotfix_OnLockedByOtherTarget;
        private static DelegateBridge __Hotfix_OnClearLockedByOtherTarget;
        private static DelegateBridge __Hotfix_ClearAllTarget;
        private static DelegateBridge __Hotfix_RemoveTarget;
        private static DelegateBridge __Hotfix_OnTargetLeaveView;
        private static DelegateBridge __Hotfix_OnLockedTargetLost;
        private static DelegateBridge __Hotfix_IsTargetAttackable;
        private static DelegateBridge __Hotfix_OnAttachDebufByTarget;
        private static DelegateBridge __Hotfix_OnHitByTarget;
        private static DelegateBridge __Hotfix_TryAddTargetToHateList;
        private static DelegateBridge __Hotfix_RemoveTargetFromHateList;
        private static DelegateBridge __Hotfix_ClearHateTargetList;
        private static DelegateBridge __Hotfix_UpdateHateTargetTimeOut_0;
        private static DelegateBridge __Hotfix_UpdateHateTargetTimeOut_1;
        private static DelegateBridge __Hotfix_GetHateTargetTimeOutByTarget;
        private static DelegateBridge __Hotfix_OnHostileBehavior;
        private static DelegateBridge __Hotfix_OnKillOtherTarget;
        private static DelegateBridge __Hotfix_TickHostileBehaviorTimeOut;
        private static DelegateBridge __Hotfix_OnLastLockedHateTargetHBTimeOut;
        private static DelegateBridge __Hotfix_CheckTargetInHateTargetList;
        private static DelegateBridge __Hotfix_GetHateTargetList;
        private static DelegateBridge __Hotfix_GetHateTargetListLastUpdateTime;
        private static DelegateBridge __Hotfix_GetHateTargetListCount;
        private static DelegateBridge __Hotfix_IsTargetFightingWithMe;
        private static DelegateBridge __Hotfix_GetConstHostileBehaviorCountDown;
        private static DelegateBridge __Hotfix_IsFightingWithPlayer;
        private static DelegateBridge __Hotfix_OnJumpingStart;
        private static DelegateBridge __Hotfix_OnJumpingEnd;
        private static DelegateBridge __Hotfix_OnInvsibleStart;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisible;
        private static DelegateBridge __Hotfix_IsCurrentScenePVPEnable;
        private static DelegateBridge __Hotfix_CheckPVPTargetValid;
        private static DelegateBridge __Hotfix_OnTargetFactionInfoChanged;
        private static DelegateBridge __Hotfix_OnSelfFactionInfoChanged;
        private static DelegateBridge __Hotfix_GetNpcCountInHateList;
        private static DelegateBridge __Hotfix_IsNormalAttackAutoRun;
        private static DelegateBridge __Hotfix_IsHighSlotAutoRun;
        private static DelegateBridge __Hotfix_IsSuperWeaponEquipAutoRun;
        private static DelegateBridge __Hotfix_IsMiddleSlotAutoRun;
        private static DelegateBridge __Hotfix_IsNeverFight2Player;
        private static DelegateBridge __Hotfix_EnableNormalAttackAutoRun;
        private static DelegateBridge __Hotfix_EnableHighSlotAutoRun;
        private static DelegateBridge __Hotfix_EnableSuperWeaponEquipAutoRun;
        private static DelegateBridge __Hotfix_EnableMiddleSlotAutoRun;
        private static DelegateBridge __Hotfix_SetNeverFight2Player;
        private static DelegateBridge __Hotfix_CheckForLaunchSuperWeaponEquipGroup;
        private static DelegateBridge __Hotfix_CheckTargetValidForSuperWeaponEquipLaunch;
        private static DelegateBridge __Hotfix_CheckTargetValidForSuperWeaponLaunch;
        private static DelegateBridge __Hotfix_CheckTargetValidForSuperEquipLaunch;
        private static DelegateBridge __Hotfix_GetNormalFireRangeMax;
        private static DelegateBridge __Hotfix_CheckForLaunchWeaponGroup;
        private static DelegateBridge __Hotfix_CheckForLaunchEquipGroup;
        private static DelegateBridge __Hotfix_CheckSelfShipStateForWeaponEquipLaunch;
        private static DelegateBridge __Hotfix_CheckWeaponEquipFireDisbale;
        private static DelegateBridge __Hotfix_IsInfiniteAmmo;
        private static DelegateBridge __Hotfix_EnableInfiniteAmmo;
        private static DelegateBridge __Hotfix_FireEventOnEnterFight;
        private static DelegateBridge __Hotfix_FireEventOnLeaveFight;
        private static DelegateBridge __Hotfix_FireEventOnLockTarget;
        private static DelegateBridge __Hotfix_FireEventOnLockedByOtherTarget;
        private static DelegateBridge __Hotfix_FireEventOnClearLockedByOtherTarget;
        private static DelegateBridge __Hotfix_FireEventOnLaunchSuperWeaponEquip;
        private static DelegateBridge __Hotfix_FireEventOnKillOtherTarget;
        private static DelegateBridge __Hotfix_add_EventOnEnterFight;
        private static DelegateBridge __Hotfix_remove_EventOnEnterFight;
        private static DelegateBridge __Hotfix_add_EventOnLeaveFight;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveFight;
        private static DelegateBridge __Hotfix_add_EventOnLockTarget;
        private static DelegateBridge __Hotfix_remove_EventOnLockTarget;
        private static DelegateBridge __Hotfix_add_EventOnLockedByOtherTarget;
        private static DelegateBridge __Hotfix_remove_EventOnLockedByOtherTarget;
        private static DelegateBridge __Hotfix_add_EventOnClearLockedByOtherTarget;
        private static DelegateBridge __Hotfix_remove_EventOnClearLockedByOtherTarget;
        private static DelegateBridge __Hotfix_add_EventOnTargetAdd2HateList;
        private static DelegateBridge __Hotfix_remove_EventOnTargetAdd2HateList;
        private static DelegateBridge __Hotfix_add_EventOnLaunchSuperWeaponEquip;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchSuperWeaponEquip;
        private static DelegateBridge __Hotfix_add_EventOnKillOtherTarget;
        private static DelegateBridge __Hotfix_remove_EventOnKillOtherTarget;

        public event Action<ILBSpaceTarget> EventOnClearLockedByOtherTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEnterFight
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnKillOtherTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLaunchSuperWeaponEquip
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLeaveFight
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnLockedByOtherTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnLockTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget, ILBInSpaceShip> EventOnTargetAdd2HateList
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompFireControllBase()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForLaunchEquipGroup(LBEquipGroupBase equipGroup, ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForLaunchSuperWeaponEquipGroup(uint targetObjId, out int errCode, bool needLog = true)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForLaunchWeaponGroup(LBWeaponGroupBase weaponGroup, ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckLockTarget(ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckPVPTargetValid(ILBSpaceTarget target, bool isHostile, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool CheckSelfShipStateForWeaponEquipLaunch(LBInSpaceWeaponEquipGroupBase weaponEquipGroup, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTargetInHateTargetList(ILBSpaceTarget target)
        {
        }

        protected abstract bool CheckTargetInView(ILBSpaceTarget target);
        [MethodImpl(0x8000)]
        protected bool CheckTargetValidForSuperEquipLaunch(uint targetObjId, out int errCode, bool needLog = true)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckTargetValidForSuperWeaponEquipLaunch(uint targetObjId, out int errCode, bool needLog = true)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckTargetValidForSuperWeaponLaunch(uint targetObjId, out int errCode, bool needLog = true)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckWeaponEquipFireDisbale()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearAllTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearHateTargetList()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearLockTarget()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void EnableHighSlotAutoRun(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableInfiniteAmmo(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableMiddleSlotAutoRun(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableNormalAttackAutoRun(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void EnableSubSystem(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableSuperWeaponEquipAutoRun(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnClearLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnKillOtherTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnLaunchSuperWeaponEquip()
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnLeaveFight()
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnLockTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetConstHostileBehaviorCountDown()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetFirstPriorityEnemyTarget()
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<ILBSpaceTarget> GetHateTargetList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetHateTargetListCount()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetHateTargetListLastUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetHateTargetTimeOutByTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetLockedTarget()
        {
        }

        [MethodImpl(0x8000)]
        public float GetNormalFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcCountInHateList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsCurrentScenePVPEnable()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFightingWithPlayer()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHighSlotAutoRun()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInFight()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInfiniteAmmo()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsMiddleSlotAutoRun()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeverFight2Player()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNormalAttackAutoRun()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSuperWeaponEquipAutoRun()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsTargetAttackable(ILBSpaceTarget target, bool checkView, out int errCode, bool onlyEnemy = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTargetFightingWithMe(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnAttachDebufByTarget(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void OnClearLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnHitByTarget(ILBSpaceTarget srcTarget, bool isCritical, LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnHostileBehavior(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnInvsibleStart(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnJumpingEnd(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnJumpingStart(ILBInSpaceShip ship, Vector3D dest)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnKillOtherTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLastLockedHateTargetHBTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void OnLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnLockedTargetLost()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOtherTargetInvisible(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfFactionInfoChanged(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTargetFactionInfoChanged(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTargetLeaveView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveTargetFromHateList(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetLockTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNeverFight2Player(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickHostileBehaviorTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void TryAddTargetToHateList(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual int TryLockTarget(ILBSpaceTarget target, bool forceNormalAttackRun = false, bool lockTargetByManualOpt = false)
        {
        }

        [MethodImpl(0x8000)]
        public virtual int TryLockTargetWithDelay(ILBSpaceTarget target, uint delayTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateHateTargetTimeOut(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateHateTargetTimeOut(ILBSpaceTarget target, uint currTime)
        {
        }
    }
}

