﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildPurchaseDataContainer
    {
        void AccomplishPartialGuildPurchaseOrder(ulong instanceId, long num, GuildPurchaseLogInfo log);
        void AddGuildPurchaseOrder(GuildPurchaseOrderInfo info);
        bool CancelGuildPurchaseOrder(ulong orderInstanceId);
        GuildPurchaseOrderInfo GetGuildPurchaseOrderByInstanceId(ulong orderInstanceId);
        List<GuildPurchaseOrderInfo> GetGuildPurchaseOrderListInfo();
        bool GetSingleOrderNums(ulong instanceId, ref long num);
        bool ModifyGuildPurchaseOrder(ulong instanceId, long itemCount, GuildPurchaseLogInfo log);
        bool UpdateGuildPurchaseOrder(GuildPurchaseOrderInfo info);
    }
}

