﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockFlagShipCompTeleportBase
    {
        protected ILBInSpaceFlagShip m_ownerShip;
        protected IFlagShipDataContainer m_shipDC;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CheckForFlagShipTeleportRandomly;
        private static DelegateBridge __Hotfix_CheckForFlagShipTeleportAccurately;
        private static DelegateBridge __Hotfix_CalcFlagShipTeleportDistanceMax;

        [MethodImpl(0x8000)]
        public float CalcFlagShipTeleportDistanceMax()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFlagShipTeleportAccurately(int destSolarSystemId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFlagShipTeleportRandomly(int destSolarSystemId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceFlagShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick()
        {
        }
    }
}

