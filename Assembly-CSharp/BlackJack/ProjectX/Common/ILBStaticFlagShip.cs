﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBStaticFlagShip : ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        IGuildItemStoreItemOperationBase GetGuildItemStore();
        int GetHangarIndex();
        ulong GetInstanceId();
        LogicBlockFlagShipCompStaticBasicInfo GetLBFlagShipBasicInfo();
        LogicBlockShipCompWeaponEquipSetupBase GetLBWeaponEquipSetup();
        LogicBlockShipCompWeaponEquipSetupEnv4FlagShip GetLBWeaponEquipSetupEnv();
    }
}

