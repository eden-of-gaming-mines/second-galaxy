﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCrackBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCrackComplete;
        protected ILBPlayerContext m_playerCtx;
        protected ICrackDataContainer m_dc;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected LogicBlockCharacterBase m_lbCharacter;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CheckCrackSlotExtend;
        private static DelegateBridge __Hotfix_CheckCrackQueueAddBox;
        private static DelegateBridge __Hotfix_CheckCrackQueueRemoveBox;
        private static DelegateBridge __Hotfix_CheckCrackSpeedUpByRealMoney;
        private static DelegateBridge __Hotfix_CheckCrackedBoxGetReward;
        private static DelegateBridge __Hotfix_GetCrackSlotInfo;
        private static DelegateBridge __Hotfix_IsCrackQueueFull;
        private static DelegateBridge __Hotfix_GetCrackedBoxList;
        private static DelegateBridge __Hotfix_ExtendCrackSlot;
        private static DelegateBridge __Hotfix_CrackQueueAddBoxImpl;
        private static DelegateBridge __Hotfix_CrackCompleteImpl;
        private static DelegateBridge __Hotfix_FireEventOnCrackComplete;
        private static DelegateBridge __Hotfix_CalcCrackSlotExtendRealmMoneyCost;
        private static DelegateBridge __Hotfix_CalcRealMoneyCostByCrackingTime;
        private static DelegateBridge __Hotfix_add_EventOnCrackComplete;
        private static DelegateBridge __Hotfix_remove_EventOnCrackComplete;

        public event Action<int> EventOnCrackComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int CalcCrackSlotExtendRealmMoneyCost(int extendIndex)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcRealMoneyCostByCrackingTime(double seconds)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCrackedBoxGetReward(int boxItemIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCrackQueueAddBox(int storeItemIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCrackQueueRemoveBox(int boxItemIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCrackSlotExtend(int extendIndex, out int realMoneyCost, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCrackSpeedUpByRealMoney(int boxItemIndex, out int realMoneyCost, out int slotIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void CrackCompleteImpl(int slotIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CrackQueueAddBoxImpl(LBStoreItem boxItem)
        {
        }

        [MethodImpl(0x8000)]
        protected void ExtendCrackSlot()
        {
        }

        [MethodImpl(0x8000)]
        public void FireEventOnCrackComplete(int boxItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetCrackedBoxList()
        {
        }

        [MethodImpl(0x8000)]
        public CrackSlotInfo GetCrackSlotInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsCrackQueueFull()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

