﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildSolarSystemInfo
    {
        public int m_solarSystemId;
        public DateTime m_nextSovereignBattleStartMinTime;
        public ushort m_version;
        public int m_flourishValue;
        public ushort m_buildingVersion;
        public List<GuildBuildingInfo> m_buildingList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildBuildingByType;
        private static DelegateBridge __Hotfix_GetGuildBuildingByInstanceId;

        [MethodImpl(0x8000)]
        public GuildBuildingInfo GetGuildBuildingByInstanceId(ulong instanceId, bool containsLost)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo GetGuildBuildingByType(GuildBuildingType type, bool containsLost)
        {
        }
    }
}

