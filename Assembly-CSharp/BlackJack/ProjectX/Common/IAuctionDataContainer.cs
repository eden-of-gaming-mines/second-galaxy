﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IAuctionDataContainer
    {
        void AddPlayerAuctionItem(AuctionItemDetailInfo info);
        void ClearPlayerAuctionItem();
        AuctionItemDetailInfo GetPlayerAuctionItemByInsId(ulong insId);
        List<AuctionItemDetailInfo> GetPlayerAuctionItemList();
        void RemovePlayerAuctionItem(ulong insId);
        void UpdatePlayerAuctionItem(ulong insId, AuctionItemState state, int currCount);
    }
}

