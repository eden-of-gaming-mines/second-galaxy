﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompTeleportTunnelBase : GuildCompBase, IGuildCompTeleportTunnelBase, IGuildTeleportTunnelBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildTeleportTunnelEffectBySolarSystemId;
        private static DelegateBridge __Hotfix_GetAllGuildTeleportTunnelEffectList;
        private static DelegateBridge __Hotfix_GetGuildTeleportTunnelBuildingEffectInfo;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompTeleportTunnelBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfo> GetAllGuildTeleportTunnelEffectList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildTeleportTunnelEffectInfo GetGuildTeleportTunnelBuildingEffectInfo(ulong buildingInsId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfo> GetGuildTeleportTunnelEffectBySolarSystemId(int destSolarSystemId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

