﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockMailBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IMailDataContainer m_mailDC;
        protected List<LBStoredMail> m_mailList;
        private const int MailSendTitleLengthMax = 30;
        private const int MailSendContentLengthMax = 360;
        private const int SendMailCDSeconds = 5;
        private DateTime m_lastSendMailDateTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_InitStoredMailList;
        private static DelegateBridge __Hotfix_GetStoredMailByInstanceId;
        private static DelegateBridge __Hotfix_GetStoredMailByIndex;
        private static DelegateBridge __Hotfix_CreateEmptyStoredMailItemForIndex;
        private static DelegateBridge __Hotfix_CreateLBStoredMailByMailInfo;
        private static DelegateBridge __Hotfix_AddMailDirectly;
        private static DelegateBridge __Hotfix_RemoveMailDirectly_1;
        private static DelegateBridge __Hotfix_RemoveMailDirectly_0;
        private static DelegateBridge __Hotfix_CheckRemoveMail;
        private static DelegateBridge __Hotfix_CheckGetAttachment;
        private static DelegateBridge __Hotfix_CheckMailSend;
        private static DelegateBridge __Hotfix_CheckMailSendForTitleContent;
        private static DelegateBridge __Hotfix_CheckStringValidation;

        [MethodImpl(0x8000)]
        protected void AddMailDirectly(int storedIndex, MailInfo mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckGetAttachment(int storedIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckMailSend(string title, string content, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckMailSendForTitleContent(string title, string content, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckRemoveMail(int storedIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckStringValidation(string stringToCheck)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoredMail CreateEmptyStoredMailItemForIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStoredMail CreateLBStoredMailByMailInfo(StoredMailInfo mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoredMail GetStoredMailByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoredMail GetStoredMailByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        private void InitStoredMailList()
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveMailDirectly(LBStoredMail storedMail)
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveMailDirectly(int storedIndex)
        {
        }
    }
}

