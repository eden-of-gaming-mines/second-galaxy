﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class LogicBlockGuildBase
    {
        protected readonly List<GuildSentryInterestScene> m_guildSentryInterestSceneList;
        protected ILBPlayerContext m_playerCtx;
        protected IGuildDataContainerForPlayerCtx m_guildPlayerDC;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CheckGuildCreate;
        private static DelegateBridge __Hotfix_CheckGuildChangeNameAndCode;
        private static DelegateBridge __Hotfix_CheckBaseSolarSystemSetReq;
        private static DelegateBridge __Hotfix_CheckGuildLogoSet;
        private static DelegateBridge __Hotfix_GetGuildId;
        private static DelegateBridge __Hotfix_GetAllianceId;
        private static DelegateBridge __Hotfix_GetAllianceName;
        private static DelegateBridge __Hotfix_GetGuildFleetId;
        private static DelegateBridge __Hotfix_GetSelfFlagShipInfo;
        private static DelegateBridge __Hotfix_GetGuildName;
        private static DelegateBridge __Hotfix_GetGuildCodeName;
        private static DelegateBridge __Hotfix_GetGuildLogo;
        private static DelegateBridge __Hotfix_GetDismissEndTime;
        private static DelegateBridge __Hotfix_GetLeaderTransferEndTime;
        private static DelegateBridge __Hotfix_GetLeaderTransferTargetUserId;
        private static DelegateBridge __Hotfix_GetBaseSolarSystem;
        private static DelegateBridge __Hotfix_GetBaseSpaceStation;
        private static DelegateBridge __Hotfix_CheckGuildPermission;
        private static DelegateBridge __Hotfix_CheckGuildSentryProbeAvailableTime;
        private static DelegateBridge __Hotfix_UpdateGuildSentryProbeAvailableTime;
        private static DelegateBridge __Hotfix_GetGuildSentryProbeAvailableTime;
        private static DelegateBridge __Hotfix_GetGuildSentryInterestScene;
        private static DelegateBridge __Hotfix_GetGuildSentryInterestSceneByInsId;
        private static DelegateBridge __Hotfix_GetGuildFleetSetting;
        private static DelegateBridge __Hotfix_UpdateGuildFleetSetting;
        private static DelegateBridge __Hotfix_RefreshGuildSentrySceneCache;

        [MethodImpl(0x8000)]
        protected LogicBlockGuildBase()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckBaseSolarSystemSetReq(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckGuildChangeNameAndCode()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildCreate(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildLogoSet(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildPermission(GuildPermission permission)
        {
        }

        [MethodImpl(0x8000)]
        public void CheckGuildSentryProbeAvailableTime(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAllianceId()
        {
        }

        [MethodImpl(0x8000)]
        public string GetAllianceName()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBaseSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBaseSpaceStation()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetDismissEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public string GetGuildCodeName()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetGuildFleetId()
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetPersonalSetting GetGuildFleetSetting()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildId()
        {
        }

        public abstract List<GuildJobType> GetGuildJob();
        [MethodImpl(0x8000)]
        public GuildLogoInfo GetGuildLogo()
        {
        }

        [MethodImpl(0x8000)]
        public string GetGuildName()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSentryInterestScene> GetGuildSentryInterestScene()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSentryInterestScene GetGuildSentryInterestSceneByInsId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetGuildSentryProbeAvailableTime()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetLeaderTransferEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public string GetLeaderTransferTargetUserId()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetSelfFlagShipInfo(out int hangarSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshGuildSentrySceneCache(List<ProGuildSentrySceneInfo> guildSentrySceneInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetSetting(GuildFleetPersonalSetting settingFlag)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildSentryProbeAvailableTime(DateTime availableTime)
        {
        }
    }
}

