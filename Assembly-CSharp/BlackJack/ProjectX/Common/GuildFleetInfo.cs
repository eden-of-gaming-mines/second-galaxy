﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildFleetInfo
    {
        public ulong m_fleetInstanceId;
        public int m_fleetSeqId;
        public string m_fleetCustomName;
        public List<GuildFleetMemberInfo> m_fleetMemberList;
        public int m_formationConfId;
        public bool m_allowToJoinFleetManual;
        public bool m_isFormationActive;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

