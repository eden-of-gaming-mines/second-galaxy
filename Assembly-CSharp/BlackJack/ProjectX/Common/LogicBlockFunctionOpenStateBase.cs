﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockFunctionOpenStateBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IFunctionOpenStateDataContainer m_dc;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<SystemFuncType> EventOnFuctionStateOpen;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsFunctionOpen;
        private static DelegateBridge __Hotfix_IsFunctionOpenAnimationShow;
        private static DelegateBridge __Hotfix_SetFunctionOpenState;
        private static DelegateBridge __Hotfix_SetFunctionOpenAnimationEnd;
        private static DelegateBridge __Hotfix_ProcessSystemFunctionOpenStateOnQuestComplete;
        private static DelegateBridge __Hotfix_OnQuestComplete;
        private static DelegateBridge __Hotfix_add_EventOnFuctionStateOpen;
        private static DelegateBridge __Hotfix_remove_EventOnFuctionStateOpen;

        public event Action<SystemFuncType> EventOnFuctionStateOpen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFunctionOpen(SystemFuncType systemFuncType)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFunctionOpenAnimationShow(SystemFuncType systemFuncType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestComplete(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void ProcessSystemFunctionOpenStateOnQuestComplete(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFunctionOpenAnimationEnd(SystemFuncType systemFuncType, bool isEndAnimation = true)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetFunctionOpenState(SystemFuncType systemFuncType, bool isOpen)
        {
        }
    }
}

