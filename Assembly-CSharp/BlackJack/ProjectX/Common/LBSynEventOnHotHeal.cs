﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class LBSynEventOnHotHeal : LBSyncEvent
    {
        public uint m_bufInstanceId;
        public float m_healValue;
        public BufType m_bufType;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

