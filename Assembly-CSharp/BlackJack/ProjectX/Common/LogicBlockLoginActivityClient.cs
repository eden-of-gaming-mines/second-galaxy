﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockLoginActivityClient : LogicBlockLoginActivityBase
    {
        private ICommanderAuthDataContainerClient m_commanderAuthDC;
        private ICMDailySignDataContainerClient m_dailySignDC;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_DailyLoginRewardTake;
        private static DelegateBridge __Hotfix_UpdateCommanderAuthInfo;
        private static DelegateBridge __Hotfix_UpdateDailySignInfo;
        private static DelegateBridge __Hotfix_IsCommanderAuthRecved;
        private static DelegateBridge __Hotfix_GetCommanderAuthRewardRecvedStatu;
        private static DelegateBridge __Hotfix_SetCommanderAuthRewardRecvedStatu;
        private static DelegateBridge __Hotfix_GetDailySignRewardRecvedStatu;
        private static DelegateBridge __Hotfix_GetDailySignCount;
        private static DelegateBridge __Hotfix_SetDailySignCount;
        private static DelegateBridge __Hotfix_GetDailyRandReward;
        private static DelegateBridge __Hotfix_SetDailyRandReward;
        private static DelegateBridge __Hotfix_IsCMDailySignRewardRecved;
        private static DelegateBridge __Hotfix_GetDailySignRewardRecvTime;

        [MethodImpl(0x8000)]
        public bool DailyLoginRewardTake(int dayIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetCommanderAuthRewardRecvedStatu()
        {
        }

        [MethodImpl(0x8000)]
        public SimpleItemInfoWithCount GetDailyRandReward()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetDailySignCount()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetDailySignRewardRecvedStatu()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetDailySignRewardRecvTime()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCMDailySignRewardRecved()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCommanderAuthRecved()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommanderAuthRewardRecvedStatu(bool statu)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDailyRandReward(SimpleItemInfoWithCount dailyRandReward)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDailySignCount(uint dailySignCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCommanderAuthInfo(bool statu)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDailySignInfo(uint dailySignCount, DateTime recvTime, SimpleItemInfoWithCount dailyRandReward)
        {
        }
    }
}

