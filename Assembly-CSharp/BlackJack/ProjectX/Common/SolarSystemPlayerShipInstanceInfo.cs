﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class SolarSystemPlayerShipInstanceInfo : SolarSystemShipInstanceInfoBase
    {
        public ulong m_instanceId;
        public ShipState m_state;
        public int m_storeItemIndex;
        public uint m_currWingShipObjId;
        public ulong m_currGuildFleetId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

