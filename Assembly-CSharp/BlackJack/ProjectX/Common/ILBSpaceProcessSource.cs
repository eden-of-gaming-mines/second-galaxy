﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessSource
    {
        void OnProcessCancel(LBSpaceProcess process);
        void OnProcessEnd(LBSpaceProcess process);
    }
}

