﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct GlobalSceneInfo
    {
        public uint m_objId;
        public Vector3D m_location;
        public int m_sceneId;
        public uint m_sceneInstanceId;
        public string m_sceneReplaceName;
        public WormholeGateInfo? m_wormholeGateInfo;
        public int m_wormholeTunnelDestSolarSystemId;
        public ulong m_guildBuildingInstanceId;
    }
}

