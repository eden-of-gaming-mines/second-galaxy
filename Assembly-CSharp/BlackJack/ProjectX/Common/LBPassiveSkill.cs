﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBPassiveSkill
    {
        protected int m_id;
        protected int m_level;
        protected ConfigDataPassiveSkillInfo m_skillConf;
        protected ConfigDataPassiveSkillLevelInfo m_levelConf;
        protected ConfigDataBufInfo m_bufConf;
        protected LBBufBase m_activeBuf;
        protected List<LBPassiveSkill> m_dependPassiveSkillList;
        protected ConfigDataDrivingLicenseInfo m_drivingLicenseConf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Reinitialize;
        private static DelegateBridge __Hotfix_GetConfId;
        private static DelegateBridge __Hotfix_GetConfPassiveSkill;
        private static DelegateBridge __Hotfix_GetConfPassiveSkillLevelConfInfo;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetAllNeedSkillPoint;
        private static DelegateBridge __Hotfix_IsOnlyInStation;
        private static DelegateBridge __Hotfix_IsOnlyInSpace;
        private static DelegateBridge __Hotfix_IsDrivingLicenseSkill;
        private static DelegateBridge __Hotfix_GetBufConfigInfo;
        private static DelegateBridge __Hotfix_GetDrivingLicenseInfo;
        private static DelegateBridge __Hotfix_SetActiveBuf;
        private static DelegateBridge __Hotfix_GetActiveBuf;
        private static DelegateBridge __Hotfix_GetIconArtResString;

        [MethodImpl(0x8000)]
        public LBPassiveSkill(int confId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetActiveBuf()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAllNeedSkillPoint()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBufInfo GetBufConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetConfId()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillInfo GetConfPassiveSkill()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPassiveSkillLevelInfo GetConfPassiveSkillLevelConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDrivingLicenseInfo GetDrivingLicenseInfo()
        {
        }

        [MethodImpl(0x8000)]
        public string GetIconArtResString()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDrivingLicenseSkill()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOnlyInSpace()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOnlyInStation()
        {
        }

        [MethodImpl(0x8000)]
        public void Reinitialize(int confId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetActiveBuf(LBBufBase activeBuf)
        {
        }
    }
}

