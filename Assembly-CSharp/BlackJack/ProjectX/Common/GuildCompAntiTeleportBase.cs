﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompAntiTeleportBase : GuildCompBase, IGuildCompAntiTeleportBase, IGuildAntiTeleportBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_CheckForActivateAntiTeleportEffect;
        private static DelegateBridge __Hotfix_CheckForAntiTeleportInfoReq;
        private static DelegateBridge __Hotfix_GetGuildAntiTeleportEffectBySolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildAntiTeleportEffectById;
        private static DelegateBridge __Hotfix_GetAllGuildAntiTeleportEffectList;
        private static DelegateBridge __Hotfix_IsAntiTeleportEffectActivate;

        [MethodImpl(0x8000)]
        public GuildCompAntiTeleportBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForActivateAntiTeleportEffect(string gameUserId, ulong buildingInsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForAntiTeleportInfoReq(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAntiTeleportEffectInfo> GetAllGuildAntiTeleportEffectList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectById(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectBySolarSystemId(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAntiTeleportEffectActivate(DateTime currTime, int destSolarSystemId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

