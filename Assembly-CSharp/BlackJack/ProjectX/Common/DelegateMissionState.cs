﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Serializable]
    public enum DelegateMissionState
    {
        Idle,
        Working,
        Invading,
        Complete,
        Cancel,
        CancelByPlayer
    }
}

