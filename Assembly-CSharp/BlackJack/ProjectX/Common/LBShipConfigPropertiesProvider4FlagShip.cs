﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBShipConfigPropertiesProvider4FlagShip : LBShipConfigPropertiesProvider
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitShipPropertiesCache;
        private static DelegateBridge __Hotfix_FindPropertiesBaseValueFromConfig;

        [MethodImpl(0x8000)]
        public LBShipConfigPropertiesProvider4FlagShip(ILBSpaceShipBasic ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool FindPropertiesBaseValueFromConfig(int propertiesId, out float baseValue)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitShipPropertiesCache()
        {
        }
    }
}

