﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IAllianceCompBasicBase : IAllianceBasicBase
    {
        void AllianceLeaderSet(uint guildId, string guildCode, string guildName);
    }
}

