﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IShipDataContainer : ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        float GetCurrArmor();
        float GetCurrEnergy();
        float GetCurrShield();
        string GetOwnerName();
        int GetShipConfId();
        string GetShipName();
        void UpdateCurrArmor(float armor);
        void UpdateCurrEnergy(float energy);
        void UpdateCurrShield(float shield);
        void UpdateShipName(string shipName);
    }
}

