﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBProcessSingleUnitLaunchInfo
    {
        public uint LaunchTime;
        public byte Flag;
        public ushort Index;
        public uint LaunchTimeAbs;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetIndex;
        private static DelegateBridge __Hotfix_SetLaunchTime;
        private static DelegateBridge __Hotfix_SetHit;
        private static DelegateBridge __Hotfix_GetHit;
        private static DelegateBridge __Hotfix_SetCritical;
        private static DelegateBridge __Hotfix_GetCritical;
        private static DelegateBridge __Hotfix_SetPreCalcDestroy;
        private static DelegateBridge __Hotfix_GetPreCalcDestroy;

        [MethodImpl(0x8000)]
        public bool GetCritical()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetHit()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPreCalcDestroy()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCritical(bool isCritical)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHit(bool isHit)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIndex(ushort index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLaunchTime(uint currTime, uint launchTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPreCalcDestroy()
        {
        }
    }
}

