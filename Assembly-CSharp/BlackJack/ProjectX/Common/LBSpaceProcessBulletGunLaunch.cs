﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessBulletGunLaunch : LBSpaceProcessWeaponLaunchBase
    {
        protected ushort m_bulletFlyTime;
        protected List<LBProcessSingleUnitLaunchInfo> m_unitLaunchList;
        protected List<uint> m_unitChargeTimeList;
        protected uint m_endTime;
        protected int m_bulletListLoopIndex;
        protected int m_unitChargeListLoopIndex;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetBulletFlyTime;
        private static DelegateBridge __Hotfix_GetBulletFlyTime;
        private static DelegateBridge __Hotfix_GetEndTime;
        private static DelegateBridge __Hotfix_AddUnitLaunchInfo;
        private static DelegateBridge __Hotfix_AddUnitChargeInfo;
        private static DelegateBridge __Hotfix_SortUnitLaunchInfoByLaunchTime;
        private static DelegateBridge __Hotfix_ComparationForUnitLaunchInfo;
        private static DelegateBridge __Hotfix_GetLaunchCount;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetUnitLaunchList;
        private static DelegateBridge __Hotfix_GetUnitChargeList;
        private static DelegateBridge __Hotfix_GetUnitLaunchInfoByIndex;
        private static DelegateBridge __Hotfix_get_ProcessSource;

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletGunLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessBulletGunLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void AddUnitChargeInfo(uint chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void AddUnitLaunchInfo(LBProcessSingleUnitLaunchInfo unitLaunchInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        private int ComparationForUnitLaunchInfo(LBProcessSingleUnitLaunchInfo info1, LBProcessSingleUnitLaunchInfo info2)
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetBulletFlyTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetLaunchCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetUnitChargeList()
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessSingleUnitLaunchInfo GetUnitLaunchInfoByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBProcessSingleUnitLaunchInfo> GetUnitLaunchList()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletFlyTime(ushort bulletFlyTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SortUnitLaunchInfoByLaunchTime()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        protected ILBSpaceProcessWeaponLaunchSource ProcessSource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

