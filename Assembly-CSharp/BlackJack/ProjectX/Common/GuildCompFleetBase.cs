﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompFleetBase : GuildCompBase, IGuildCompFleetBase, IGuildFleetBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckForFleetListReq;
        private static DelegateBridge __Hotfix_CheckForFleetCreating;
        private static DelegateBridge __Hotfix_CheckForFleetSetFormationType;
        private static DelegateBridge __Hotfix_CheckForFleetSetAllowToJoinFleetManual;
        private static DelegateBridge __Hotfix_CheckForFleetSetFormationActive;
        private static DelegateBridge __Hotfix_CheckForFleetMemberJoin;
        private static DelegateBridge __Hotfix_CheckForFleetMemberLeave;
        private static DelegateBridge __Hotfix_CheckForFleetKickOut;
        private static DelegateBridge __Hotfix_CheckForFleetDismiss;
        private static DelegateBridge __Hotfix_CheckForFleetRename;
        private static DelegateBridge __Hotfix_CheckForFleetRename4DomesticPackage;
        private static DelegateBridge __Hotfix_CheckForFleetDetailInfoReq;
        private static DelegateBridge __Hotfix_CheckForGuildFleetSetCommanderReq;
        private static DelegateBridge __Hotfix_CheckForGuildFleetSetInternalPositions;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMemberRetire;
        private static DelegateBridge __Hotfix_CheckForGuildFleetTransferPosition;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMembersJumping;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMembersUseStargate;
        private static DelegateBridge __Hotfix_CheckForGuildFleetFireFocusTarget;
        private static DelegateBridge __Hotfix_CheckForGuildFleetProtectTarget;
        private static DelegateBridge __Hotfix_GetGuildPlayerFleetId;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberByFleetPosition;
        private static DelegateBridge __Hotfix_GetGuildFleetlist;
        private static DelegateBridge __Hotfix_GetGuildFleetById;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompFleetBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetCreating(string gameUserId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetDetailInfoReq(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetDismiss(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetKickOut(string srcGameUserId, ulong fleetId, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetListReq(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetMemberJoin(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetMemberLeave(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetRename(string gameUserId, ulong fleetId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetRename4DomesticPackage(string gameUserId, ulong fleetId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetSetAllowToJoinFleetManual(string gameUserId, ulong fleetId, bool allowToJoinFleetManual, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetSetFormationActive(string gameUserId, ulong fleetId, bool isFormationActive, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForFleetSetFormationType(string gameUserId, ulong fleetId, int formationType, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetFireFocusTarget(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetMemberRetire(string srcGameUserId, ulong fleetId, List<uint> positionList, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetMembersJumping(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetMembersUseStargate(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetProtectTarget(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetSetCommanderReq(string srcGameUserId, ulong fleetId, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetSetInternalPositions(string gameUserId, ulong fleetId, GuildFleetMemberInfo member, bool hasPermission, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFleetTransferPosition(string srcGameUserId, ulong fleetId, string destGameUserId, FleetPosition position, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetInfo GetGuildFleetById(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetListInfo GetGuildFleetlist()
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo GetGuildFleetMemberByFleetPosition(ulong fleetId, FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        public virtual ulong GetGuildPlayerFleetId(string gameUserId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

