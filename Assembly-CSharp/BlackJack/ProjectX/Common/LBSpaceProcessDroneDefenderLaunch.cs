﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessDroneDefenderLaunch : LBSpaceProcessDroneLaunchBase
    {
        protected float m_attackRange;
        protected List<AttackChance[]> m_attackChanceList;
        protected List<AttackChance> m_idleAttackChanceList;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_InitAttackChanceList;
        private static DelegateBridge __Hotfix_SetAttackRange;
        private static DelegateBridge __Hotfix_GetAttackRange;
        private static DelegateBridge __Hotfix_HasIdleAttackChance;
        private static DelegateBridge __Hotfix_GetAttackChance;
        private static DelegateBridge __Hotfix_GetDefenderDamage;
        private static DelegateBridge __Hotfix_GetIdleAttackChanceList;
        private static DelegateBridge __Hotfix_RemoveIdleAttackChance;
        private static DelegateBridge __Hotfix_IsAvaliable4AgainstFighter;
        private static DelegateBridge __Hotfix_GetTimeOutIdleChance;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_ComparationForIdleAttackChanceListSort;

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneDefenderLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessDroneDefenderLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected int ComparationForIdleAttackChanceListSort(AttackChance elem1, AttackChance elem2)
        {
        }

        [MethodImpl(0x8000)]
        public AttackChance GetAttackChance(byte droneIndex, byte chanceIndex)
        {
        }

        [MethodImpl(0x8000)]
        public float GetAttackRange()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetDefenderDamage()
        {
        }

        [MethodImpl(0x8000)]
        public List<AttackChance> GetIdleAttackChanceList()
        {
        }

        [MethodImpl(0x8000)]
        public AttackChance GetTimeOutIdleChance(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasIdleAttackChance()
        {
        }

        [MethodImpl(0x8000)]
        public void InitAttackChanceList()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, int groupIndex, ILBSpaceProcessDroneLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvaliable4AgainstFighter()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveIdleAttackChance(AttackChance chance)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttackRange(float range)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        [Serializable]
        public class AttackChance
        {
            public LBSpaceProcessDroneDefenderLaunch SrcProcess;
            public byte DroneIndex;
            public byte ChanceIndex;
            public uint AbsTime;
            public ILBSpaceTarget SrcTarget;
            public LBSpaceProcessMissileLaunch AttackedMissileProcess;
            public uint ProcessMissileLaunchId;
            public LBSpaceProcessDroneFighterLaunch AttackedDroneFighterProcess;
            public byte TargetIndex;
            public bool IsDestroyTarget;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

