﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockKillRecordBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IKillRecordDataContainer m_killRecordDC;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetKillRecordList;
        private static DelegateBridge __Hotfix_GetKillRecordByInstanceId;
        private static DelegateBridge __Hotfix_AddKillRecordInfo;
        private static DelegateBridge __Hotfix_GetKillRecordByLostShipInstanceId;

        [MethodImpl(0x8000)]
        public virtual void AddKillRecordInfo(KillRecordInfo killRecordInfo)
        {
        }

        [MethodImpl(0x8000)]
        public KillRecordInfo GetKillRecordByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public KillRecordInfo GetKillRecordByLostShipInstanceId(ulong shipInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<KillRecordInfo> GetKillRecordList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }
    }
}

