﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IRechargeGiftPackageDataContainerClient : IRechargeGiftPackageDataContainer
    {
        void RefreshAllData(List<RechargeGiftPackageInfo> giftPackageList, int version);
    }
}

