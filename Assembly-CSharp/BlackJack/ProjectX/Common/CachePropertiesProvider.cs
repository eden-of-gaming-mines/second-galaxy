﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class CachePropertiesProvider : IPropertiesProvider
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnPropertiesGroupDirty;
        protected Dictionary<int, float> m_providerPropertiesCacheDict;
        protected uint m_providerPropertiesCacheMask;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddPropertiesCache;
        private static DelegateBridge __Hotfix_HasPropertiesGroup;
        private static DelegateBridge __Hotfix_HasProperty;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_add_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_remove_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_Clear;

        public event Action<int> EventOnPropertiesGroupDirty
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AddPropertiesCache(PropertiesId propertiesId, float value)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }
    }
}

