﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class AllianceCompBase
    {
        protected IAllianceCompOwnerBase m_owner;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initlialize;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        protected AllianceCompBase(IAllianceCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initlialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitlialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(DateTime currTime)
        {
        }
    }
}

