﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class SceneEventInfo
    {
        public SceneEvent m_eventId;
        public SceneMulticastType m_multicastType;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

