﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataSpaceStation3DInfo")]
    public class ConfigDataSpaceStation3DInfo : IExtensible
    {
        private int _ID;
        private PVector3D _LeaveStationDir;
        private PVector3D _leaveStationAreaCenterPoint;
        private float _leaveStationAreaRadius;
        private CollisionInfo _collision;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LeaveStationDir;
        private static DelegateBridge __Hotfix_set_LeaveStationDir;
        private static DelegateBridge __Hotfix_get_LeaveStationAreaCenterPoint;
        private static DelegateBridge __Hotfix_set_LeaveStationAreaCenterPoint;
        private static DelegateBridge __Hotfix_get_LeaveStationAreaRadius;
        private static DelegateBridge __Hotfix_set_LeaveStationAreaRadius;
        private static DelegateBridge __Hotfix_get_Collision;
        private static DelegateBridge __Hotfix_set_Collision;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="LeaveStationDir", DataFormat=DataFormat.Default)]
        public PVector3D LeaveStationDir
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="leaveStationAreaCenterPoint", DataFormat=DataFormat.Default)]
        public PVector3D LeaveStationAreaCenterPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="leaveStationAreaRadius", DataFormat=DataFormat.FixedSize)]
        public float LeaveStationAreaRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="collision", DataFormat=DataFormat.Default)]
        public CollisionInfo Collision
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

