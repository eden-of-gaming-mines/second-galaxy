﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class DrivingLicenseUpgradeInfo
    {
        public int m_id;
        public int m_level;
        public DateTime m_startTime;
        public DateTime m_endTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

