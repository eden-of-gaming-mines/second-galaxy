﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompLostReportBase : GuildCompBase, IGuildCompLostReportBase, IGuildLostReportBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddSovereignLostReport;
        private static DelegateBridge __Hotfix_AddBuildingLostReport;
        private static DelegateBridge __Hotfix_GetBuildingLostReport;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        protected GuildCompLostReportBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AddBuildingLostReport(BuildingLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSovereignLostReport(SovereignLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public BuildingLostReport GetBuildingLostReport(ulong reportId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

