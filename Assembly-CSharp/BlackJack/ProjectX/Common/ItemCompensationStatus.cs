﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum ItemCompensationStatus
    {
        NotAvailable,
        WaitForCompensation,
        CompensationDone
    }
}

