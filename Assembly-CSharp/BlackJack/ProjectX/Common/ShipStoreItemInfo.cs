﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ShipStoreItemInfo
    {
        public ItemInfo m_itemInfo;
        public bool m_isFreezing;
        public int m_storeIndex;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_IsContainSameItem;

        [MethodImpl(0x8000)]
        public ShipStoreItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ShipStoreItemInfo(ShipStoreItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsContainSameItem(ShipStoreItemInfo info)
        {
        }
    }
}

