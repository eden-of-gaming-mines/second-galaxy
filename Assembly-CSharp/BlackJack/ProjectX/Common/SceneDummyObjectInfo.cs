﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SceneDummyObjectInfo")]
    public class SceneDummyObjectInfo : IExtensible
    {
        private uint _ObjType;
        private string _ScriptRefName;
        private int _ObjConfId;
        private PVector3D _Location;
        private PVector3D _Rotation;
        private float _Scale;
        private bool _CreateOnSceneCreate;
        private float _Radius;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ObjType;
        private static DelegateBridge __Hotfix_set_ObjType;
        private static DelegateBridge __Hotfix_get_ScriptRefName;
        private static DelegateBridge __Hotfix_set_ScriptRefName;
        private static DelegateBridge __Hotfix_get_ObjConfId;
        private static DelegateBridge __Hotfix_set_ObjConfId;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_get_Rotation;
        private static DelegateBridge __Hotfix_set_Rotation;
        private static DelegateBridge __Hotfix_get_Scale;
        private static DelegateBridge __Hotfix_set_Scale;
        private static DelegateBridge __Hotfix_get_CreateOnSceneCreate;
        private static DelegateBridge __Hotfix_set_CreateOnSceneCreate;
        private static DelegateBridge __Hotfix_get_Radius;
        private static DelegateBridge __Hotfix_set_Radius;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ObjType", DataFormat=DataFormat.TwosComplement)]
        public uint ObjType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ScriptRefName", DataFormat=DataFormat.Default)]
        public string ScriptRefName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ObjConfId", DataFormat=DataFormat.TwosComplement)]
        public int ObjConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Location", DataFormat=DataFormat.Default)]
        public PVector3D Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Rotation", DataFormat=DataFormat.Default)]
        public PVector3D Rotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Scale", DataFormat=DataFormat.FixedSize)]
        public float Scale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="CreateOnSceneCreate", DataFormat=DataFormat.Default)]
        public bool CreateOnSceneCreate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="Radius", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float Radius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

