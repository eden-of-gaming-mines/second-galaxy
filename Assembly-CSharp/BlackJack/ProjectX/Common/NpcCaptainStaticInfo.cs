﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class NpcCaptainStaticInfo
    {
        public ulong m_instanceId;
        public int m_resId;
        public int m_lastNameId;
        public int m_firstNameId;
        public ProfessionType m_profession;
        public SubRankType m_subRank;
        public GrandFaction m_grandFaction;
        public GenderType m_gender;
        public int m_age;
        public DateTime m_birthDay;
        public ConstellationType m_constellation;
        public NpcPersonalityType m_personality;
        public int m_shipGrowthLineId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

