﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IShipItemStoreDataContainer
    {
        void AddShipStoreItem(ShipStoreItemInfo shipItemInfo);
        List<ShipStoreItemInfo> GetShipStoreItemList();
        void RemoveShipStoreItem(ShipStoreItemInfo shipItemInfo);
        void UpdataShipStoreItem(ShipStoreItemInfo shipItemInfo, long count, bool isFreezing = false);
    }
}

