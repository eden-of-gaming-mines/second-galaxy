﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SolarSystemPlayerGuildFleetInfo
    {
        public ulong m_fleetId;
        public List<FleetPosition> m_fleetPositionList;
        public bool m_isFormationActive;
        public FormationType m_formationType;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

