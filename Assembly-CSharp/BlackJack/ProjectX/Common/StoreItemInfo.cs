﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class StoreItemInfo
    {
        public ItemInfo m_itemInfo;
        public int m_index;
        public bool m_deleted;
        public uint m_version;
        public uint m_flag;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

