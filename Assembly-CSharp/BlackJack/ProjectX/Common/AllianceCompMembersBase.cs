﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class AllianceCompMembersBase : AllianceCompBase, IAllianceCompMembersBase, IAllianceMembersBase
    {
        protected IAllianceDCMembers m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetDC;
        private static DelegateBridge __Hotfix_GetAllianceMemberList;

        [MethodImpl(0x8000)]
        public AllianceCompMembersBase(IAllianceCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public List<AllianceMemberInfo> GetAllianceMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDC(IAllianceDCMembers dc)
        {
        }
    }
}

