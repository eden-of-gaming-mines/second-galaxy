﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessLaserSingleFire : LBSpaceProcess
    {
        protected uint m_t1EndTime;
        protected uint m_t2EndTime;
        protected uint m_t3EndTime;
        protected readonly LBBulletDamageInfo m_damageInfo;
        protected uint m_tickedOnHitCount;
        protected float m_criticalRate;
        protected ushort m_unitFireIndex;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetDamage;
        private static DelegateBridge __Hotfix_SetAttackTime;
        private static DelegateBridge __Hotfix_SetCriticalRate;
        private static DelegateBridge __Hotfix_GetDamageInfo;
        private static DelegateBridge __Hotfix_GetAttackTime;
        private static DelegateBridge __Hotfix_GetCriticalRate;
        private static DelegateBridge __Hotfix_GetUnitFireIndex;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public LBSpaceProcessLaserSingleFire()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessLaserSingleFire(ushort unitFireIndex, uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void GetAttackTime(out uint t1EndTime, out uint t2EndTime, out uint t3EndTime)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCriticalRate()
        {
        }

        [MethodImpl(0x8000)]
        public LBBulletDamageInfo GetDamageInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetUnitFireIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(ushort unitFireIndex, uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttackTime(uint t1EndTime, uint t2EndTime, uint t3EndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCriticalRate(float propertyCriticalRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDamage(LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

