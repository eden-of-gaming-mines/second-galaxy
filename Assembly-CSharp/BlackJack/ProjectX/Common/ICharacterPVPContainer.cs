﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ICharacterPVPContainer
    {
        PlayerPVPInfo GetPlayerPVPInfo();
        void UpdateCriminalHunter(bool isCriminalHunter, DateTime endTime);
        void UpdateCriminalLevel(float criminalLevel);
        void UpdateJustCrime(bool isJustCrime, DateTime endTime);
    }
}

