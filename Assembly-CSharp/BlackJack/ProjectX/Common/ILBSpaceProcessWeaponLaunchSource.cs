﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessWeaponLaunchSource : ILBSpaceProcessSource
    {
        void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex);
        void OnProcessUnitStartCharge(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex);
    }
}

