﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockAuctionClient : LogicBlockAuctionBase
    {
        private Dictionary<StoreItemType, Dictionary<int, int>> m_mapAuctionItemConf;
        private bool m_isFristGetPlayerAuctionItemList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetAuctionItemIdByStoreItemInfo;
        private static DelegateBridge __Hotfix_IsItemIsAuctionItem;
        private static DelegateBridge __Hotfix_AuctionItemOnShelve;
        private static DelegateBridge __Hotfix_AuctionItemCallBack;
        private static DelegateBridge __Hotfix_PlayerAuctionItemUpdateNtf;
        private static DelegateBridge __Hotfix_get_IsFristGetPlayerAuctionItemList;
        private static DelegateBridge __Hotfix_set_IsFristGetPlayerAuctionItemList;

        [MethodImpl(0x8000)]
        public void AuctionItemCallBack(ulong auctionItemInsId)
        {
        }

        [MethodImpl(0x8000)]
        public void AuctionItemOnShelve(ulong auctionItemInsId, int itemCount, long price, int period, AuctionItemDetailInfo newAuctionItem)
        {
        }

        [MethodImpl(0x8000)]
        public int GetAuctionItemIdByStoreItemInfo(StoreItemType type, int id)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsItemIsAuctionItem(StoreItemType type, int id)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayerAuctionItemUpdateNtf(ulong auctionItemInsId, AuctionItemState state, int currCount)
        {
        }

        public bool IsFristGetPlayerAuctionItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

