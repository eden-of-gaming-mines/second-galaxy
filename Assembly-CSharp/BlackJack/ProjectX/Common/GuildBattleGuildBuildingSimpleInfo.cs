﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildBattleGuildBuildingSimpleInfo
    {
        public GuildBuildingType m_buildingType;
        public float m_defenderBuildingShieldPercentCurr;
        public float m_defenderBuildingArmorPercentCurr;
        public float m_attackerBuildingShieldPercentCurr;
        public float m_attackerBuildingArmorPercentCurr;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

