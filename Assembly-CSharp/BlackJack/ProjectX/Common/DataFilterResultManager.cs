﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class DataFilterResultManager : DataFilterResultManagerBase
    {
        protected IDFRDataSource m_dataSource;
        protected int m_resultCheckCD;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetDataResultByFilter;
        private static DelegateBridge __Hotfix_IsResultCacheItemValid;

        [MethodImpl(0x8000)]
        public DataFilterResultManager(IDFRDataSource dataSource, int resultCheckCD)
        {
        }

        [MethodImpl(0x8000)]
        public override List<IDFRFilterResultItem> GetDataResultByFilter(IDFRDataFilter filter, IComparer<IDFRFilterResultItem> comparer = null, int startIndex = 0, int count = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsResultCacheItemValid(DataFilterResultManagerBase.FilterResultCacheItem item)
        {
        }
    }
}

