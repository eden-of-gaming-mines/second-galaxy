﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LBCharChipSlot
    {
        private CharChipSlotInfo m_readonlyCacheInfo;
        private LBCharChip m_chip;
        private List<LBBufBase> m_activeBufList;
        private ConfigDataCharChipSlotInfo m_slotConfInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetSlotIndex;
        private static DelegateBridge __Hotfix_GetChip;
        private static DelegateBridge __Hotfix_UpdateChip;
        private static DelegateBridge __Hotfix_GetChipConfInfo;
        private static DelegateBridge __Hotfix_GetChipSetConfInfo;
        private static DelegateBridge __Hotfix_GetChipSlotConfInfo;
        private static DelegateBridge __Hotfix_AddActiveBuf;
        private static DelegateBridge __Hotfix_RemoveAllActiveBuf;
        private static DelegateBridge __Hotfix_GetActiveBufList;
        private static DelegateBridge __Hotfix_GetChipSlotRandomBuf;

        [MethodImpl(0x8000)]
        public LBCharChipSlot(CharChipSlotInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AddActiveBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBufBase> GetActiveBufList()
        {
        }

        [MethodImpl(0x8000)]
        public LBCharChip GetChip()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipInfo GetChipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSuitInfo GetChipSetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataCharChipSlotInfo GetChipSlotConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetChipSlotRandomBuf()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSlotIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAllActiveBuf()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChip(LBCharChip chip)
        {
        }
    }
}

