﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBStarInfo")]
    public class GDBStarInfo : IExtensible
    {
        private int _Id;
        private uint _age;
        private uint _spectrum;
        private double _brightness;
        private uint _radius;
        private uint _temperature;
        private uint _artResourcesId;
        private double _Mass;
        private uint _templateId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Age;
        private static DelegateBridge __Hotfix_set_Age;
        private static DelegateBridge __Hotfix_get_Spectrum;
        private static DelegateBridge __Hotfix_set_Spectrum;
        private static DelegateBridge __Hotfix_get_Brightness;
        private static DelegateBridge __Hotfix_set_Brightness;
        private static DelegateBridge __Hotfix_get_Radius;
        private static DelegateBridge __Hotfix_set_Radius;
        private static DelegateBridge __Hotfix_get_Temperature;
        private static DelegateBridge __Hotfix_set_Temperature;
        private static DelegateBridge __Hotfix_get_ArtResourcesId;
        private static DelegateBridge __Hotfix_set_ArtResourcesId;
        private static DelegateBridge __Hotfix_get_Mass;
        private static DelegateBridge __Hotfix_set_Mass;
        private static DelegateBridge __Hotfix_get_TemplateId;
        private static DelegateBridge __Hotfix_set_TemplateId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="age", DataFormat=DataFormat.TwosComplement)]
        public uint Age
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="spectrum", DataFormat=DataFormat.TwosComplement)]
        public uint Spectrum
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="brightness", DataFormat=DataFormat.TwosComplement)]
        public double Brightness
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="radius", DataFormat=DataFormat.TwosComplement)]
        public uint Radius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="temperature", DataFormat=DataFormat.TwosComplement)]
        public uint Temperature
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="artResourcesId", DataFormat=DataFormat.TwosComplement)]
        public uint ArtResourcesId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Mass", DataFormat=DataFormat.TwosComplement)]
        public double Mass
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="templateId", DataFormat=DataFormat.TwosComplement)]
        public uint TemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

