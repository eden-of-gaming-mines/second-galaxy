﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockCharacterFactionBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, float> EventOnFactionCreditValueChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, float, FactionCreditLevel, FactionCreditLevel> EventOnFactionCreditLevelChanged;
        protected ICharacterFactionDataContainer m_dc;
        private int FactionCreditMax;
        private int FactionCreditMin;
        protected ILBPlayerContext m_lbPlayerCtx;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetFactionCreditMap;
        private static DelegateBridge __Hotfix_GetCreditByFactionId;
        private static DelegateBridge __Hotfix_InitFactionCredit;
        private static DelegateBridge __Hotfix_GetCreditLevelByFactionId;
        private static DelegateBridge __Hotfix_GetCreditLevelByFactionCreditValue;
        private static DelegateBridge __Hotfix_ModifyFactionCreditByRelativeInfo;
        private static DelegateBridge __Hotfix_ModifyFactionCredit;
        private static DelegateBridge __Hotfix_UpdateFactionCredit;
        private static DelegateBridge __Hotfix_CheckStationOpPermissionByFaction;
        private static DelegateBridge __Hotfix_GetGrandFactionByFactionId;
        private static DelegateBridge __Hotfix_GetFactionIdByGrandFaction;
        private static DelegateBridge __Hotfix_GetFactionCurrencyByGrandFaction;
        private static DelegateBridge __Hotfix_GetTopParentFactionId;
        private static DelegateBridge __Hotfix_IsFactionCreditCurrency;
        private static DelegateBridge __Hotfix_GetFactionCreditFactor;
        private static DelegateBridge __Hotfix_GetFactionCreditCurrencyFactor;
        private static DelegateBridge __Hotfix_OnFactionCreditValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnFactionCreditValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnFactionCreditValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnFactionCreditLevelChanged;
        private static DelegateBridge __Hotfix_remove_EventOnFactionCreditLevelChanged;

        public event Action<int, float, FactionCreditLevel, FactionCreditLevel> EventOnFactionCreditLevelChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, float> EventOnFactionCreditValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool CheckStationOpPermissionByFaction(int factionId, StationOpPermissionType permissionType)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCreditByFactionId(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditLevel GetCreditLevelByFactionCreditValue(int factionCreditValue)
        {
        }

        [MethodImpl(0x8000)]
        public FactionCreditLevel GetCreditLevelByFactionId(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public float GetFactionCreditCurrencyFactor(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public float GetFactionCreditFactor(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, float> GetFactionCreditMap()
        {
        }

        [MethodImpl(0x8000)]
        public CurrencyType GetFactionCurrencyByGrandFaction(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFactionIdByGrandFaction(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction GetGrandFactionByFactionId(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetTopParentFactionId(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitFactionCredit(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFactionCreditCurrency(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        public float ModifyFactionCredit(int factionId, float addValue)
        {
        }

        [MethodImpl(0x8000)]
        public void ModifyFactionCreditByRelativeInfo(float value, int creditRelativeModifyId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnFactionCreditValueChanged(int factionId, int finalValue)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateFactionCredit(int factionId, float factionCredit)
        {
        }
    }
}

