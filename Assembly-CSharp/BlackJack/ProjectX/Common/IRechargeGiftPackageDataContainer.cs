﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IRechargeGiftPackageDataContainer
    {
        RechargeGiftPackageInfo GetRechargeGiftPackage(int giftPackageId);
        List<RechargeGiftPackageInfo> GetRechargeGiftPackageList();
        int GetRechargeGiftPackageListVersion();
        void RechargeGiftPackageAdd(RechargeGiftPackageInfo giftPackage);
        void RechargeGiftPackageRemove(int giftPackageId);
        void SetGiftPackageCurrCycleBuyCount(int giftPackageId, int buyCount);
    }
}

