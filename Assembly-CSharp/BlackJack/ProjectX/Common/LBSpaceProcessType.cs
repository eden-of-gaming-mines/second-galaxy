﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum LBSpaceProcessType
    {
        BulletGunLaunch = 1,
        MissileLaunch = 2,
        LaserLaunch = 3,
        DroneFighterLaunch = 4,
        DroneDefenderLaunch = 5,
        DroneSniperLaunch = 6,
        SuperRailgunLaunch = 7,
        SuperPlasmaLaunch = 8,
        SuperMissileLaunch = 9,
        SuperLaserLaunch = 10,
        BulletFly = 11,
        SuperRailgunBulletFly = 12,
        LaserSingleFire = 13,
        SuperLaserSingleFire = 14,
        EquipAttachBufLaunch = 15,
        EquipInvisibleLaunch = 0x10,
        EquipBlinkLaunch = 0x11,
        EquipChannelLaunch = 0x12,
        EquipPeriodicDamage = 0x13,
        SuperEquipAoeLaunch = 20,
        AntiJumpingForceShieldAndAttachBufLaunch = 0x15,
        TransformToTeleportTunnel = 0x16,
        LBSpaceProcessType_Max = 0x17
    }
}

