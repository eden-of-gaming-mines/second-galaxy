﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBShipItemStoreContainer
    {
        LogicBlockShipCompItemStoreBase GetLBShipItemStore();
        IShipItemStoreDataContainer GetShipItemStoreDataContainer();
    }
}

