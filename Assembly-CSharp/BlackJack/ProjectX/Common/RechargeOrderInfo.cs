﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class RechargeOrderInfo
    {
        public ulong m_orderInstanceId;
        public RechargeGoodsType m_goodsType;
        public int m_goodsId;
        public DateTime m_orderCreateTime;
        public DateTime m_orderPaymentTime;
        public DateTime m_orderDeliverTime;
        public RechargeOrderInfoStatus m_status;
        public string m_customizedField1;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

