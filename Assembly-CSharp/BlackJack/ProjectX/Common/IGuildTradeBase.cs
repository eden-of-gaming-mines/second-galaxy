﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface IGuildTradeBase
    {
        ushort GetGuildTradePortDataVersion();
        GuildTradePortExtraInfo GetGuildTradePortExtraInfoBySolarSystemId(int solarSystemId);
        List<GuildTradePortExtraInfo> GetGuildTradePortExtraInfoList();
        ConfigDataSpaceShipInfo GetSpaceShipConfigInfoByTradePortSolarSystemId(int solarSystemId);
    }
}

