﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBFormationControllerDefault : LBFormationControllerBase
    {
        protected double m_formationSphereRadius;
        protected List<Vector3D> m_formationPosList;
        protected Dictionary<uint, int> m_usedFormationPosIndexDic;
        protected List<int> m_unusedFormationPosIndexList;
        protected double m_ratioFromSphereRadiusToLeaderShipSize;
        protected double m_formationPosSeparatingAngle;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByJoinMember;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByLeaveMember;
        private static DelegateBridge __Hotfix_InitialzeFormationPosList;
        private static DelegateBridge __Hotfix_CalcPosByAngleInfo;

        [MethodImpl(0x8000)]
        protected virtual Vector3D CalcPosByAngleInfo(double azimuth, double altitude)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initialize(ILBInSpaceShip leader, FormationCreatingInfo formationInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitialzeFormationPosList(bool isFormationFlipped)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByJoinMember(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByLeaveMember(IFormationMember member)
        {
        }
    }
}

