﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockRankingListBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        private DateTime[] m_lastRankingListQueryTime;
        public const int RankingListQueryPeriod = 0x1f40;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CheckRankingListInfoQuery;
        private static DelegateBridge __Hotfix_CheckRankingListAddPlayerLevel;
        private static DelegateBridge __Hotfix_UpdateLastRankingListQueryTime;

        [MethodImpl(0x8000)]
        public bool CheckRankingListAddPlayerLevel()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRankingListInfoQuery(RankingListType rankingListType, int queryTopNum, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLastRankingListQueryTime(RankingListType rankingListType, int queryTopNum, DateTime currTime)
        {
        }
    }
}

