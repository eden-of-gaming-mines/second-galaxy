﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupIncBuffBase : LBTacticalEquipGroupBase
    {
        protected List<int> m_bufIdList;
        protected int m_bufGroupId;
        protected LBBufBase m_currBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsTargetValid;
        private static DelegateBridge __Hotfix_GetIncBufEffectBaseValue;
        private static DelegateBridge __Hotfix_OnBufDetach;
        private static DelegateBridge __Hotfix_GetMaxBufLevel;
        private static DelegateBridge __Hotfix_GetCurrBufLevel;
        private static DelegateBridge __Hotfix_get_GroupId;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupIncBuffBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int GetCurrBufLevel()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetIncBufEffectBaseValue()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetMaxBufLevel()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBufDetach(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public int GroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

