﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using System;
    using System.Collections.Generic;

    public interface IEquipFunctionCompOwner
    {
        ulong AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId);
        ushort CalcChargeTime();
        ushort CalcLaunchEnergyCost();
        ushort CalcLaunchFuelCost();
        List<int> GetConfBufIdList();
        EquipType GetEquipType();
        ILBInSpaceShip GetOwnerShip();
        PropertiesCalculaterBase GetPropertiesCalc();
        IEnumerable<int> GetSelfBufIdList();
        int GetSlotGroupIndex();
        ShipEquipSlotType GetSlotType();
    }
}

