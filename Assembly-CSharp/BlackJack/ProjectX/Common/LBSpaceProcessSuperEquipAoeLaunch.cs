﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBSpaceProcessSuperEquipAoeLaunch : LBSpaceProcessEquipLaunchWithDamageBase
    {
        protected List<LBBulletDamageInfo> m_extraDamageInfoList;
        public List<int> m_bufIdList;
        public List<uint> m_targetList;
        public float m_targetEnergyCost;
        public bool m_isTargetEnergyCostByPercent;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_SetDamageInfoList;
        private static DelegateBridge __Hotfix_GetDamageInfoList;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetBufId;
        private static DelegateBridge __Hotfix_SetTargetCostEnergy;
        private static DelegateBridge __Hotfix_SetTargetList;
        private static DelegateBridge __Hotfix_AddTarget;
        private static DelegateBridge __Hotfix_OnEquipFire;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperEquipAoeLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperEquipAoeLaunch(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget = null, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public void AddTarget(uint target)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBulletDamageInfo> GetDamageInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget = null, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEquipFire(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBufId(List<int> bufIdList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDamageInfoList(List<LBBulletDamageInfo> damageInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetCostEnergy(float energy, bool isCostByPercent)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetList(List<uint> targetList)
        {
        }
    }
}

