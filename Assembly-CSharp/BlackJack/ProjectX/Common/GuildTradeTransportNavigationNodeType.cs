﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GuildTradeTransportNavigationNodeType
    {
        None,
        LeaveGuildTradePort,
        EnterStarGate,
        LeaveStarGate,
        EnterGuildTradePort,
        Max
    }
}

