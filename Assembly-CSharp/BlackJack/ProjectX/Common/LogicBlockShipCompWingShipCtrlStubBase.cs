﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LogicBlockShipCompWingShipCtrlStubBase
    {
        protected ILBInSpaceShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetWingShip;

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompWingShipCtrlStubBase()
        {
        }

        public abstract ILBInSpacePlayerShip GetMasterShip();
        [MethodImpl(0x8000)]
        public ILBInSpaceShip GetWingShip()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

