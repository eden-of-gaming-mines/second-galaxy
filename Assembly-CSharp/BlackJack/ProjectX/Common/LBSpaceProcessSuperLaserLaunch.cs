﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessSuperLaserLaunch : LBSpaceProcessSuperWeaponLaunchBase
    {
        protected uint m_t1EndTime;
        protected uint m_t2EndTime;
        protected uint m_t3EndTime;
        protected float m_propertyCriticalRateFinal;
        protected List<ILBSpaceTarget> m_extraHitTargetList;
        protected List<LBBulletDamageInfo> m_damageInfoForExtraTargets;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetCriticalRate;
        private static DelegateBridge __Hotfix_GetCriticalRateFinal;
        private static DelegateBridge __Hotfix_SetAttackTime;
        private static DelegateBridge __Hotfix_GetT1EndTime;
        private static DelegateBridge __Hotfix_GetT2EndTime;
        private static DelegateBridge __Hotfix_GetT3EndTime;
        private static DelegateBridge __Hotfix_SetExtraHitTarget;
        private static DelegateBridge __Hotfix_GetExtraBulletHitTarget;
        private static DelegateBridge __Hotfix_GetDamageInfoForExtraTargets;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperLaserLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperLaserLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCriticalRateFinal()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBulletDamageInfo> GetDamageInfoForExtraTargets()
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> GetExtraBulletHitTarget()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetT1EndTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetT2EndTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetT3EndTime()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttackTime(uint t1EndTime, uint t2EndTime, uint t3Endtime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCriticalRate(float propertyCriticalRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExtraHitTarget(List<ILBSpaceTarget> extraTargetList, List<LBBulletDamageInfo> damageInfoForExtraTargets)
        {
        }
    }
}

