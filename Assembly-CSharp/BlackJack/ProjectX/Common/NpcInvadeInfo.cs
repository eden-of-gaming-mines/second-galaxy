﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class NpcInvadeInfo
    {
        public DelegateMissionInfo m_sourceDelegateMission;
        public DateTime m_scanTime;
        public int m_currMonsterTeamId;
        public FakeNpcInfo m_fakeNpcInfo;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

