﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class UserCommonSetting
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <TroopInvitePromptState>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <MusicState>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <SoundEffectState>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IllegalRangeAttackPromptState>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IllegalAttackPromptState>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private LanguageType <LanguageCode>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetMusicState;
        private static DelegateBridge __Hotfix_SetSoundEffectState;
        private static DelegateBridge __Hotfix_SetLanguageCode;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_get_TroopInvitePromptState;
        private static DelegateBridge __Hotfix_set_TroopInvitePromptState;
        private static DelegateBridge __Hotfix_get_MusicState;
        private static DelegateBridge __Hotfix_set_MusicState;
        private static DelegateBridge __Hotfix_get_SoundEffectState;
        private static DelegateBridge __Hotfix_set_SoundEffectState;
        private static DelegateBridge __Hotfix_get_IllegalRangeAttackPromptState;
        private static DelegateBridge __Hotfix_set_IllegalRangeAttackPromptState;
        private static DelegateBridge __Hotfix_get_IllegalAttackPromptState;
        private static DelegateBridge __Hotfix_set_IllegalAttackPromptState;
        private static DelegateBridge __Hotfix_get_LanguageCode;
        private static DelegateBridge __Hotfix_set_LanguageCode;

        [MethodImpl(0x8000)]
        public void Reset()
        {
        }

        [MethodImpl(0x8000)]
        public void SetLanguageCode(LanguageType code)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMusicState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundEffectState(bool state)
        {
        }

        public bool TroopInvitePromptState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool MusicState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public bool SoundEffectState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public bool IllegalRangeAttackPromptState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IllegalAttackPromptState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public LanguageType LanguageCode
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

