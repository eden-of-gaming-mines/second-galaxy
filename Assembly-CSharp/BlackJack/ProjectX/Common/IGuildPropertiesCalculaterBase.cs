﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IGuildPropertiesCalculaterBase
    {
        float CalcGuildPropertiesById(int solarSystemId, GuildPropertiesId propertiesId);
        bool CheckGuildFlag(int solarSystemId, GuildFlag flag);
    }
}

