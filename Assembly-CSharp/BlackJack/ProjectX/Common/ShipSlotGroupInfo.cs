﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ShipSlotGroupInfo
    {
        public StoreItemType m_itemType;
        public int m_configId;
        public int m_storeIndex;
        public bool m_itemIsBind;
        public bool m_itemIsFreezing;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_2;

        [MethodImpl(0x8000)]
        public ShipSlotGroupInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ShipSlotGroupInfo(ShipSlotGroupInfo from)
        {
        }

        [MethodImpl(0x8000)]
        public ShipSlotGroupInfo(StoreItemInfo from)
        {
        }
    }
}

