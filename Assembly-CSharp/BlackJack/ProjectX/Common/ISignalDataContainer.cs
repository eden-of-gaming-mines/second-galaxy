﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface ISignalDataContainer
    {
        void AddDelegateSignal(SignalInfo signalInfo);
        void AddEmergencySignal(SignalInfo signalInfo);
        void AddManualSignal(SignalInfo signalInfo);
        void AddPVPSignal(PVPSignalInfo pvpSignal);
        EmergencySignalReqQueueItem DequeueEmergencySignalReq();
        void EnqueueEmergencySignalReq(EmergencySignalReqQueueItem item);
        SignalInfo GetDelegateSignalByInstanceId(ulong insId);
        List<SignalInfo> GetDelegateSignalInfoList();
        SignalInfo GetEmergencySignalByInstanceId(ulong insId);
        List<SignalInfo> GetEmergencySignalInfoList();
        SignalInfo GetManualSignalByInstanceId(ulong insId);
        List<SignalInfo> GetManualSignalInfoList();
        PVPSignalInfo GetPVPSignalByInstanceId(ulong insId);
        List<PVPSignalInfo> GetPVPSignalInfoList();
        List<ScanProbeInfo> GetScanProbeInfoList();
        void RemoveAllDelegateSignal();
        void RemoveAllManualSignal();
        void RemoveAllPVPSignal();
        void RemoveDelegateSignal(ulong insId);
        void RemoveEmergencySignal(ulong insId);
        void RemoveManualSignal(ulong insId);
        void RemovePVPSignal(ulong insId);
        void UpdateScanProbeInfo(ScanProbeType probeType, int currCount, DateTime cdStartTime);
    }
}

