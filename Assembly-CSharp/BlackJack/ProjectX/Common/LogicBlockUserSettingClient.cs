﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockUserSettingClient : LogicBlockUserSettingBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnMusicStateChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnSoundEffectStateChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnFrameRateLimitChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnShadowDisplayStateChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnShipDisplayStateChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnSpaceEffectStateChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnDamageTextDisplayStateChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGraphicQualityLevelChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGraphicEffectLevelChanged;
        protected string m_filePath;
        protected UserSettingInClient m_userSettingInClient;
        private const int LowFrameRate = 30;
        private const int HighFrameRate = 60;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_InitUserSettings;
        private static DelegateBridge __Hotfix_FlushUserSettings;
        private static DelegateBridge __Hotfix_GetUserSettingCacheFileName;
        private static DelegateBridge __Hotfix_add_EventOnMusicStateChanged;
        private static DelegateBridge __Hotfix_remove_EventOnMusicStateChanged;
        private static DelegateBridge __Hotfix_add_EventOnSoundEffectStateChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSoundEffectStateChanged;
        private static DelegateBridge __Hotfix_add_EventOnFrameRateLimitChanged;
        private static DelegateBridge __Hotfix_remove_EventOnFrameRateLimitChanged;
        private static DelegateBridge __Hotfix_add_EventOnShadowDisplayStateChanged;
        private static DelegateBridge __Hotfix_remove_EventOnShadowDisplayStateChanged;
        private static DelegateBridge __Hotfix_add_EventOnShipDisplayStateChanged;
        private static DelegateBridge __Hotfix_remove_EventOnShipDisplayStateChanged;
        private static DelegateBridge __Hotfix_add_EventOnSpaceEffectStateChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSpaceEffectStateChanged;
        private static DelegateBridge __Hotfix_add_EventOnDamageTextDisplayStateChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDamageTextDisplayStateChanged;
        private static DelegateBridge __Hotfix_add_EventOnGraphicQualityLevelChanged;
        private static DelegateBridge __Hotfix_remove_EventOnGraphicQualityLevelChanged;
        private static DelegateBridge __Hotfix_add_EventOnGraphicEffectLevelChanged;
        private static DelegateBridge __Hotfix_remove_EventOnGraphicEffectLevelChanged;
        private static DelegateBridge __Hotfix_SetTroopInvitePromptState;
        private static DelegateBridge __Hotfix_GetTroopInvitePromptState;
        private static DelegateBridge __Hotfix_SetMusicState;
        private static DelegateBridge __Hotfix_GetMusicState;
        private static DelegateBridge __Hotfix_SetSoundEffectState;
        private static DelegateBridge __Hotfix_GetSoundEffectState;
        private static DelegateBridge __Hotfix_SetIllegalRangeAttackPromptState;
        private static DelegateBridge __Hotfix_GetIllegalRangeAttackPromptState;
        private static DelegateBridge __Hotfix_SetIllegalAttackPromptState;
        private static DelegateBridge __Hotfix_GetIllegalAttackPromptState;
        private static DelegateBridge __Hotfix_SetLanguageCodeId;
        private static DelegateBridge __Hotfix_GetLanguageCodeId;
        private static DelegateBridge __Hotfix_SetFrameRateLimitState;
        private static DelegateBridge __Hotfix_GetFrameRateLimitState;
        private static DelegateBridge __Hotfix_SetShipDisplayState;
        private static DelegateBridge __Hotfix_GetShipDisplayState;
        private static DelegateBridge __Hotfix_SetSpecialEffectState;
        private static DelegateBridge __Hotfix_GetSpecialEffectState;
        private static DelegateBridge __Hotfix_SetShadowDisplayState;
        private static DelegateBridge __Hotfix_GetShadowDisplayState;
        private static DelegateBridge __Hotfix_SetDamageTextDisplayState;
        private static DelegateBridge __Hotfix_GetDamageTextDisplayState;
        private static DelegateBridge __Hotfix_SetGraphicQualityLevel;
        private static DelegateBridge __Hotfix_GetGraphicQualityLevel;
        private static DelegateBridge __Hotfix_SetGraphicEffectLevel;
        private static DelegateBridge __Hotfix_GetGraphicEffectLevel;
        private static DelegateBridge __Hotfix_GetFrameRateByLevel;

        public event Action<bool> EventOnDamageTextDisplayStateChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnFrameRateLimitChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGraphicEffectLevelChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGraphicQualityLevelChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnMusicStateChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnShadowDisplayStateChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnShipDisplayStateChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnSoundEffectStateChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnSpaceEffectStateChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void FlushUserSettings()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetDamageTextDisplayState()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFrameRateByLevel(FrameRateLevel level)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetFrameRateLimitState()
        {
        }

        [MethodImpl(0x8000)]
        public GraphicEffectLevel GetGraphicEffectLevel()
        {
        }

        [MethodImpl(0x8000)]
        public GraphicQualityLevel GetGraphicQualityLevel()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetIllegalAttackPromptState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetIllegalRangeAttackPromptState()
        {
        }

        [MethodImpl(0x8000)]
        public LanguageType GetLanguageCodeId()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetMusicState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetShadowDisplayState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetShipDisplayState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetSoundEffectState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetSpecialEffectState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetTroopInvitePromptState()
        {
        }

        [MethodImpl(0x8000)]
        private string GetUserSettingCacheFileName()
        {
        }

        [MethodImpl(0x8000)]
        public void Initialize(ILBPlayerContext lbPlayerCtx, string filePath)
        {
        }

        [MethodImpl(0x8000)]
        public void InitUserSettings()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDamageTextDisplayState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFrameRateLimitState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGraphicEffectLevel(GraphicEffectLevel level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGraphicQualityLevel(GraphicQualityLevel level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIllegalAttackPromptState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIllegalRangeAttackPromptState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLanguageCodeId(LanguageType languageType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMusicState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShadowDisplayState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipDisplayState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundEffectState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpecialEffectState(bool state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTroopInvitePromptState(bool state)
        {
        }
    }
}

