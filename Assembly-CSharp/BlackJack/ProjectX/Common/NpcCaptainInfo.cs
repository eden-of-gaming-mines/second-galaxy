﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class NpcCaptainInfo
    {
        public NpcCaptainStaticInfo m_staticInfo;
        public NpcCaptainDynamicInfo m_dynamicInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetGrandFaction;
        private static DelegateBridge __Hotfix_IsInDelegateMission;
        private static DelegateBridge __Hotfix_IsInStationWorking;
        private static DelegateBridge __Hotfix_IsInStationWorkingOnTech;
        private static DelegateBridge __Hotfix_IsInStationWorkingOnProduction;
        private static DelegateBridge __Hotfix_GetRandomLevelUpableFeats;
        private static DelegateBridge __Hotfix_HasFeatsIncludeAllLevel;
        private static DelegateBridge __Hotfix_GetInitFeats;
        private static DelegateBridge __Hotfix_AddInitFeats;
        private static DelegateBridge __Hotfix_RemoveInitFeats;
        private static DelegateBridge __Hotfix_GetAdditionFeats;
        private static DelegateBridge __Hotfix_AddAddtionFeats;
        private static DelegateBridge __Hotfix_RemoveAddtionFeats;

        [MethodImpl(0x8000)]
        public void AddAddtionFeats(IdLevelInfo fests)
        {
        }

        [MethodImpl(0x8000)]
        public void AddInitFeats(IdLevelInfo fests)
        {
        }

        [MethodImpl(0x8000)]
        public List<IdLevelInfo> GetAdditionFeats()
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction GetGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        public List<IdLevelInfo> GetInitFeats()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public IdLevelInfo GetRandomLevelUpableFeats(Func<IdLevelInfo, bool> testLevelUpableFeats, Random rand)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasFeatsIncludeAllLevel(int featsId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInDelegateMission()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInStationWorking()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInStationWorkingOnProduction()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInStationWorkingOnTech()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAddtionFeats(int featsId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveInitFeats(int featsId)
        {
        }
    }
}

