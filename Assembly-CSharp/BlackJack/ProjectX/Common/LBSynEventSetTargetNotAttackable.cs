﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventSetTargetNotAttackable : LBSyncEvent
    {
        public uint m_objectId;
        public bool m_isNotAttackable;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

