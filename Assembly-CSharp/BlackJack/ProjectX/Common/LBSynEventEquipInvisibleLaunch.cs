﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventEquipInvisibleLaunch : LBSyncEvent
    {
        public LBSpaceProcessEquipInvisibleLaunch m_process;
        public uint m_equipGroupCDEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

