﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class ItemInfo
    {
        public int m_configId;
        public StoreItemType m_type;
        public ulong m_instanceId;
        public bool m_isBind;
        public bool m_isFreezing;
        public long m_count;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_2;
        private static DelegateBridge __Hotfix_IsContainSameItem;

        [MethodImpl(0x8000)]
        public ItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo(ItemInfo from)
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo(SimpleItemInfoWithCount from)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsContainSameItem(ItemInfo info)
        {
        }
    }
}

