﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;

    public interface ILBInSpaceNpcShip : ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        LogicBlockShipCompWingShipCtrlStubBase GetLBWingShipCtrlStub();
        ConfigDataNpcMonsterInfo GetNpcMonsterConfInfo();
        ConfigDataNpcShipAIInfo GetNpcShipAIConfInfo();
        ConfigDataNpcShipInfo GetNpcShipConfInfo();
        ConfigDataNpcShipTemplateInfo GetNpcShipTemplateConfInfo();
        ConfigDataNpcMonsterLevelDropInfo GetNpcShipTypeLevelDropInfo();
    }
}

