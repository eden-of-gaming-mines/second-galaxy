﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class LBSignalDelegateBase : LBSignalBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetExp;
        private static DelegateBridge __Hotfix_GetValidShipTypeList_0;
        private static DelegateBridge __Hotfix_GetValidShipTypeList_1;

        [MethodImpl(0x8000)]
        protected LBSignalDelegateBase(SignalInfo signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public int GetExp()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> GetValidShipTypeList()
        {
        }

        [MethodImpl(0x8000)]
        public static List<ShipType> GetValidShipTypeList(int signalId, ConfigDataSignalInfo signalConf = null)
        {
        }
    }
}

