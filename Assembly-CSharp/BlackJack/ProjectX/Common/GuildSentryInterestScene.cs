﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildSentryInterestScene
    {
        public ulong instanceId;
        public uint sceneInstanceId;
        public int sceneConfigId;
        public int totalThreatValue;
        public int shipCount;
        public uint trackingDeviationDistance;
        public int questId;
        public int signalId;
        public GuildBuildingType buildingType;
        public Vector3D location;
        public List<GuildSentryInterestTargetInfo> m_targetList;
        public Dictionary<ShipType, int> m_shipTypeDict;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

