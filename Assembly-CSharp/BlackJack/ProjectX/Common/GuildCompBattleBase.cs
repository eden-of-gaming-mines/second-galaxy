﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompBattleBase : GuildCompBase, IGuildCompBattleBase, IGuildBattleBase
    {
        protected IGuildCompBasicLogicBase m_compBasic;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_GetReinforcementEndHour;
        private static DelegateBridge __Hotfix_GetReinforcementEndMinuts;
        private static DelegateBridge __Hotfix_HasReinforcementEndTimeSetted;

        [MethodImpl(0x8000)]
        public GuildCompBattleBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public int GetReinforcementEndHour()
        {
        }

        [MethodImpl(0x8000)]
        public int GetReinforcementEndMinuts()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasReinforcementEndTimeSetted()
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitlialize()
        {
        }
    }
}

