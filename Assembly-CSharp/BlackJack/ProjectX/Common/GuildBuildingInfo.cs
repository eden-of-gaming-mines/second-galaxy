﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildBuildingInfo
    {
        public GuildBuildingType m_buildingType;
        public int m_solarSystemId;
        public ulong m_instanceId;
        public int m_level;
        public GuildBuildingStatus m_status;
        public float m_shieldMax;
        public float m_armorMax;
        public DateTime m_currStatusStartTime;
        public DateTime m_currStatusEndTime;
        public Vector3D m_sceneLocation;
        public ulong m_battleInstanceId;
        public Dictionary<uint, DateTime> m_spyGuildDic;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildBuildingInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo(GuildBuildingInfo info)
        {
        }
    }
}

