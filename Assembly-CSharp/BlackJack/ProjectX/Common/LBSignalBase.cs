﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public abstract class LBSignalBase
    {
        protected SignalInfo m_readOnlySignalInfo;
        protected ConfigDataSignalInfo m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetConfId;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_IsValid;
        private static DelegateBridge __Hotfix_GetExpiryTime;
        private static DelegateBridge __Hotfix_GetSignalInfo;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_GetSignalType;
        private static DelegateBridge __Hotfix_GetLocation;
        private static DelegateBridge __Hotfix_GetParams;

        [MethodImpl(0x8000)]
        protected LBSignalBase(SignalInfo signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public int GetConfId()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSignalInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual DateTime GetExpiryTime()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual Vector3D GetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetParams()
        {
        }

        [MethodImpl(0x8000)]
        public SignalInfo GetSignalInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual SignalType GetSignalType()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsValid(DateTime now)
        {
        }
    }
}

