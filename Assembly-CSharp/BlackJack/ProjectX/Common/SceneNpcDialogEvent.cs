﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum SceneNpcDialogEvent
    {
        None,
        NpcTalkerConfirm,
        ChoiceA,
        ChoiceB,
        ChoiceC
    }
}

