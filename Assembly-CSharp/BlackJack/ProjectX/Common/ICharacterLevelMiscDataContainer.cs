﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface ICharacterLevelMiscDataContainer
    {
        long GetAFKExp();
        ulong GetBindMoney();
        ulong GetCreditCurrency(CurrencyType currencyType);
        List<KeyValuePair<CurrencyType, ulong>> GetCreditCurrencyList();
        uint GetCurrLevelExp();
        long GetCurrTotalExp();
        long GetDailyExpAchieved();
        int GetEvaluateScore();
        ulong GetGuildGala();
        int GetLevel();
        ulong GetPanGala();
        ulong GetRealMoney();
        ulong GetTradeMoney();
        void LevelUp();
        void UpdateAFKExp(long value);
        void UpdateBindMoney(ulong value);
        void UpdateCreditCurrency(CurrencyType type, ulong value);
        void UpdateCurrLevelExp(uint value);
        void UpdateDailyExpAchieved(long value);
        void UpdateEvaluateScore(int value);
        void UpdateGuildGala(ulong value);
        void UpdatePanGala(ulong value);
        void UpdateRealMoney(ulong value);
        void UpdateTotalExp(long value);
        void UpdateTradeMoney(ulong value);
    }
}

