﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum GraphicQualityLevel
    {
        None,
        Low,
        Medium,
        High
    }
}

