﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupKeepingInvisibleForWeaponLaunchBase : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_IsPropertyCalcDependOnInvisible;
        private static DelegateBridge __Hotfix_CalcPropertyDependOnInvisible;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupKeepingInvisibleForWeaponLaunchBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertyDependOnInvisible(ref float propertyValue)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalcDependOnInvisible(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

