﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompMiningBase : GuildCompBase, IGuildCompMiningBase, IGuildMiningBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildMiningBalanceTimeHour;
        private static DelegateBridge __Hotfix_GetGuildMiningBalanceTimeMinute;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompMiningBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public int GetGuildMiningBalanceTimeHour()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGuildMiningBalanceTimeMinute()
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

