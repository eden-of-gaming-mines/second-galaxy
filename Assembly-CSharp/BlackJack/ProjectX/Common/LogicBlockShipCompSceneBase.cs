﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompSceneBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnEnterScene;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLeaveScene;
        protected ILBInSpaceShip m_ownerShip;
        protected ILBSpaceContext m_spaceCtx;
        protected ILBScene m_currScene;
        private float m_spaceSecurityLevel;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnEnterScene;
        private static DelegateBridge __Hotfix_OnLeaveScene;
        private static DelegateBridge __Hotfix_GetCurrSceneWingShipSetting;
        private static DelegateBridge __Hotfix_GetCurrSceneShipTypeLimit;
        private static DelegateBridge __Hotfix_IsPVPEnable;
        private static DelegateBridge __Hotfix_GetSecurityLevel;
        private static DelegateBridge __Hotfix_GetCurrScene;
        private static DelegateBridge __Hotfix_add_EventOnEnterScene;
        private static DelegateBridge __Hotfix_remove_EventOnEnterScene;
        private static DelegateBridge __Hotfix_add_EventOnLeaveScene;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveScene;

        public event Action EventOnEnterScene
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLeaveScene
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public ILBScene GetCurrScene()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipType> GetCurrSceneShipTypeLimit()
        {
        }

        [MethodImpl(0x8000)]
        public SceneWingShipSetting GetCurrSceneWingShipSetting()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSecurityLevel()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPVPEnable(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnEnterScene(ILBScene scene)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnLeaveScene(ILBScene scene)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

