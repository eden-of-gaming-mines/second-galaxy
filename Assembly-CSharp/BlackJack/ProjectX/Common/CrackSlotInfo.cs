﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class CrackSlotInfo
    {
        public int m_availableSlotCount;
        public List<int> m_inQueueItemIndexList;
        public DateTime m_startTime;
        public DateTime m_endTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

