﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBGalaxyInfo")]
    public class GDBGalaxyInfo : IExtensible
    {
        private readonly List<GDBStarfieldsSimpleInfo> _starfields;
        private readonly List<GDBLinkInfo> _LinkList;
        private float _galaxyRadius;
        private int _colorBlockTemplateId;
        private float _colorBlockAlpha;
        private float _colorBlockScale;
        private float _colorBlockRotation;
        private float _colorBlockLocationX;
        private float _colorBlockLocationZ;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Starfields;
        private static DelegateBridge __Hotfix_get_LinkList;
        private static DelegateBridge __Hotfix_get_GalaxyRadius;
        private static DelegateBridge __Hotfix_set_GalaxyRadius;
        private static DelegateBridge __Hotfix_get_ColorBlockTemplateId;
        private static DelegateBridge __Hotfix_set_ColorBlockTemplateId;
        private static DelegateBridge __Hotfix_get_ColorBlockAlpha;
        private static DelegateBridge __Hotfix_set_ColorBlockAlpha;
        private static DelegateBridge __Hotfix_get_ColorBlockScale;
        private static DelegateBridge __Hotfix_set_ColorBlockScale;
        private static DelegateBridge __Hotfix_get_ColorBlockRotation;
        private static DelegateBridge __Hotfix_set_ColorBlockRotation;
        private static DelegateBridge __Hotfix_get_ColorBlockLocationX;
        private static DelegateBridge __Hotfix_set_ColorBlockLocationX;
        private static DelegateBridge __Hotfix_get_ColorBlockLocationZ;
        private static DelegateBridge __Hotfix_set_ColorBlockLocationZ;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="starfields", DataFormat=DataFormat.Default)]
        public List<GDBStarfieldsSimpleInfo> Starfields
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="LinkList", DataFormat=DataFormat.Default)]
        public List<GDBLinkInfo> LinkList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="galaxyRadius", DataFormat=DataFormat.FixedSize)]
        public float GalaxyRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="colorBlockTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int ColorBlockTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="colorBlockAlpha", DataFormat=DataFormat.FixedSize)]
        public float ColorBlockAlpha
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="colorBlockScale", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float ColorBlockScale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(7, IsRequired=false, Name="colorBlockRotation", DataFormat=DataFormat.FixedSize)]
        public float ColorBlockRotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(8, IsRequired=false, Name="colorBlockLocationX", DataFormat=DataFormat.FixedSize)]
        public float ColorBlockLocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(9, IsRequired=false, Name="colorBlockLocationZ", DataFormat=DataFormat.FixedSize)]
        public float ColorBlockLocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

