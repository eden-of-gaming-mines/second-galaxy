﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface ICrackDataContainer
    {
        void CrackedBoxAdd(int boxItemIndex);
        void CrackedBoxRemove(int boxItemIndex);
        void CrackSlotQueueAddBox(int boxItemIndex);
        void CrackSlotQueueRemoveBox(int boxItemIndex);
        void ExtendCrackSlot();
        List<int> GetCrackedBoxList();
        CrackSlotInfo GetCrackSlotInfo();
        void UpdateCrackSlotQueue(DateTime startTime, DateTime endTime);
    }
}

