﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBSpaceProcessWeaponLaunchBase : LBSpaceProcessWeaponEquipLaunchBase
    {
        protected LBBulletDamageInfo m_damageInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_GetLaunchCount;
        private static DelegateBridge __Hotfix_SetDamage;
        private static DelegateBridge __Hotfix_GetDamageInfo;

        [MethodImpl(0x8000)]
        public LBSpaceProcessWeaponLaunchBase(uint startTime, uint instanceId, LBSpaceProcessType type, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public LBBulletDamageInfo GetDamageInfo()
        {
        }

        [MethodImpl(0x8000)]
        public virtual int GetLaunchCount()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, LBSpaceProcessType type, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetDamage(float totalDamage, float totalCriticalDamage, float damageComposeHeat, float damageComposeKinetic, float realDamageHeat, float realDamageKinetic, float realDamageElectric, float percentDamage, float weaponFireCtrlAccuracy, WeaponCategory weaponType, float chargeAndChannelBreakFactor, float weaponTransverseVelocity, float targetTransverseVelocity, float allDamageMulti)
        {
        }
    }
}

