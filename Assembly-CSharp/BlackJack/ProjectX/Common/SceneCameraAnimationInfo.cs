﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SceneCameraAnimationInfo
    {
        public SceneCameraAnimationType m_type;
        public int m_confgId;
        public bool m_initRotationWithTarget;
        public bool m_isFollowTarget;
        public bool m_isLookAtTarget;
        public List<SceneCameraNameObjIdPair> m_objPairList;
        public List<SceneNpcTalkerChatInfo> m_npcChatList;
        public string m_targetDummyLocationName;
        public SceneMulticastType m_multicastType;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

