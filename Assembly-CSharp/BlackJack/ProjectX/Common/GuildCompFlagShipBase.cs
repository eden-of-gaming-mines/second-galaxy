﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompFlagShipBase : GuildCompBase, IGuildCompFlagShipBase, IGuildFlagShipBase
    {
        protected string m_guildFlagShipHangarLocker;
        protected Dictionary<ulong, List<ILBStaticFlagShip>> m_guildFlagShipHangarSlotListDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildFlagShipHangarById;
        private static DelegateBridge __Hotfix_GetGuildFlagShipHangarBySolarSystemId;
        private static DelegateBridge __Hotfix_GetAllFlagShipHangarList;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarScanning;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarInfoReq;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarLocking;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarUnlocking;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipUnpack;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipPack;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipAddModuleEquip;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRemoveModuleEquip;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipSupplementFuel;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipCaptainRegister;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipCaptainUnregister;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRename;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRename4DomesticPackage;
        private static DelegateBridge __Hotfix_IsHangarSlotUnlock;
        private static DelegateBridge __Hotfix_GetHangarUnlockSlotCount;
        private static DelegateBridge __Hotfix_GetHangarSlotRankType;
        private static DelegateBridge __Hotfix_GetStaticFlagShip;
        private static DelegateBridge __Hotfix_GetStaticFlagShipList;
        private static DelegateBridge __Hotfix_RegisterFlagShipDriver;
        private static DelegateBridge __Hotfix_UnregisterFlagShipDriver;
        private static DelegateBridge __Hotfix_UpdateShipHangarLockMaster;
        private static DelegateBridge __Hotfix_ClearShipHangarLockMaster;
        private static DelegateBridge __Hotfix_SetFlagShipCustomName;
        private static DelegateBridge __Hotfix_IsFlagShipUnsetFinished;
        private static DelegateBridge __Hotfix_GetShipHangarLockMaster;
        private static DelegateBridge __Hotfix_GetFlagShipDriverInfo_0;
        private static DelegateBridge __Hotfix_GetFlagShipDriverInfo_1;
        private static DelegateBridge __Hotfix_GetShipHangarFlagShipInstanceInfoList;
        private static DelegateBridge __Hotfix_GetFlagShipInfoByInsId;
        private static DelegateBridge __Hotfix_GetFlagShipInfoByDrivingInfo;
        private static DelegateBridge __Hotfix_GetStaticFlagShipByDriver;
        private static DelegateBridge __Hotfix_GetFlagShipEquipModuleConfIdList;
        private static DelegateBridge __Hotfix_GetGuildFlagShipOptLogInfoList;
        private static DelegateBridge __Hotfix_HasAvailableFlagShip_0;
        private static DelegateBridge __Hotfix_HasAvailableFlagShip_1;
        private static DelegateBridge __Hotfix_IsHangarSlotAvailableForUnpackFlagShip;
        private static DelegateBridge __Hotfix_DestoryShip;
        private static DelegateBridge __Hotfix_CreateLBStaticPlayerShip;
        private static DelegateBridge __Hotfix_GetEmptyFlagShipHangarShipList;
        private static DelegateBridge __Hotfix_CreateStaticFlagShipListByHangarDC;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompFlagShipBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipAddModuleEquip(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, int equipItemStoreIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipCaptainRegister(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, List<int> drivingLicenseIdList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipCaptainUnregister(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipHangarInfoReq(string gameUserId, int solarSystemId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipHangarLocking(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipHangarScanning(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipHangarUnlocking(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipPack(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipRemoveModuleEquip(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipRename(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipRename4DomesticPackage(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipSupplementFuel(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int supplementCount, int applySupplementCount, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool CheckForGuildFlagShipUnpack(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, ulong shipItemStoreInsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ClearShipHangarLockMaster()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ILBStaticFlagShip CreateLBStaticPlayerShip(IStaticFlagShipDataContainer shipDC, int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected List<ILBStaticFlagShip> CreateStaticFlagShipListByHangarDC(IGuildFlagShipHangarDataContainer hangarDC)
        {
        }

        [MethodImpl(0x8000)]
        protected void DestoryShip(ulong shipHangarInsId, int hangarIndex, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public List<IGuildFlagShipHangarDataContainer> GetAllFlagShipHangarList()
        {
        }

        [MethodImpl(0x8000)]
        protected List<ILBStaticFlagShip> GetEmptyFlagShipHangarShipList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public virtual PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, ulong shipInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetFlagShipEquipModuleConfIdList(IStaticFlagShipDataContainer ship)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer GetFlagShipInfoByDrivingInfo(int shipHangarSolarSystemId, string drivingGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer GetFlagShipInfoByInsId(int shipHangarSolarSystemId, ulong shipInsId)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarById(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFlagShipOptLogInfo> GetGuildFlagShipOptLogInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public RankType GetHangarSlotRankType(IGuildFlagShipHangarDataContainer shipHangarDC, int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHangarUnlockSlotCount(IGuildFlagShipHangarDataContainer shipHangarDC)
        {
        }

        [MethodImpl(0x8000)]
        public List<IStaticFlagShipDataContainer> GetShipHangarFlagShipInstanceInfoList(ulong shipHangarInsId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipHangarLockMaster()
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticFlagShip GetStaticFlagShip(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticFlagShip GetStaticFlagShipByDriver(string driverId)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticFlagShip> GetStaticFlagShipList(ulong shipHangarInsId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool HasAvailableFlagShip(IGuildFlagShipHangarDataContainer shipHangarDC, int index, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool HasAvailableFlagShip(IGuildFlagShipHangarDataContainer shipHangarDC, ulong shipInsId, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFlagShipUnsetFinished(DateTime currTime, ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsHangarSlotAvailableForUnpackFlagShip(IGuildFlagShipHangarDataContainer shipHangarDC, int index, ConfigDataSpaceShipInfo flagShipConfInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHangarSlotUnlock(IGuildFlagShipHangarDataContainer shipHangarDC, int index)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void RegisterFlagShipDriver(ulong shipHangarInsId, int slotIndex, PlayerSimplestInfo driverInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlagShipCustomName(ulong shipHangarInsId, int slotIndex, string name)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UnregisterFlagShipDriver(ulong shipHangarInsId, int slotIndex, string optGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateShipHangarLockMaster(string masterId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetFlagShipInfoByDrivingInfo>c__AnonStorey1
        {
            internal string drivingGameUserId;

            internal bool <>m__0(IStaticFlagShipDataContainer elem) => 
                ((elem.GetFlagShipCaptainInfo() != null) && (elem.GetFlagShipCaptainInfo().m_gameUserId == this.drivingGameUserId));
        }

        [CompilerGenerated]
        private sealed class <GetFlagShipInfoByInsId>c__AnonStorey0
        {
            internal ulong shipInsId;

            internal bool <>m__0(IStaticFlagShipDataContainer elem) => 
                (elem.GetInstanceId() == this.shipInsId);
        }

        [CompilerGenerated]
        private sealed class <HasAvailableFlagShip>c__AnonStorey2
        {
            internal ulong shipInsId;

            internal bool <>m__0(IStaticFlagShipDataContainer elem) => 
                (elem.GetInstanceId() == this.shipInsId);
        }
    }
}

