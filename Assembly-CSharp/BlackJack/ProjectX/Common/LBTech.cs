﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBTech : IPropertiesProvider
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnPropertiesGroupDirty;
        private ILBBuffFactory m_bufFactory;
        private int m_techId;
        private int m_level;
        private ConfigDataTechInfo m_conf;
        private ConfigDataTechLevelInfo m_levelConf;
        private LBBufBase m_inspaceBuf;
        protected HashSet<int> m_techPropertiesSet;
        protected uint m_techPropertiesMask;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Reinitialize;
        private static DelegateBridge __Hotfix_UpdateLevel;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetLevelConfInfo;
        private static DelegateBridge __Hotfix_GetInspaceBuf;
        private static DelegateBridge __Hotfix_GetTechId;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_IsInSpaceBufEffectToPlayer;
        private static DelegateBridge __Hotfix_IsInSpaceBufEffectToHiredCaptain;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_add_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_remove_EventOnPropertiesGroupDirty;

        public event Action<int> EventOnPropertiesGroupDirty
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBTech(int id, int level, ILBBuffFactory bufFactory)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetInspaceBuf()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechLevelInfo GetLevelConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTechId()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInSpaceBufEffectToHiredCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInSpaceBufEffectToPlayer()
        {
        }

        [MethodImpl(0x8000)]
        public void Reinitialize(int confId, int level, ILBBuffFactory bufFactory)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLevel(int level)
        {
        }
    }
}

