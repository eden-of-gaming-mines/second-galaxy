﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperEquipGroupAoeAndAttachBufBase : LBSuperEquipGroupBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_IsAvailableTarget;
        private static DelegateBridge __Hotfix_HasAvailableTarget;
        private static DelegateBridge __Hotfix_IsExistAttackEnemyOnFireRangeMax;
        private static DelegateBridge __Hotfix_IsTargetValid;

        [MethodImpl(0x8000)]
        protected LBSuperEquipGroupAoeAndAttachBufBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasAvailableTarget(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvailableTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsExistAttackEnemyOnFireRangeMax(bool isPVPEnable)
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }
    }
}

