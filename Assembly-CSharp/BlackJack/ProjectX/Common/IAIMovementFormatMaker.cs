﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;

    public interface IAIMovementFormatMaker
    {
        uint GetFormationMakerLeaderObjId();
        Vector3D GetFormationPosForMember(IFormationMember member);
        Vector3D JoinFormation(IFormationMember member);
        bool LeaveFormation(IFormationMember member);
    }
}

