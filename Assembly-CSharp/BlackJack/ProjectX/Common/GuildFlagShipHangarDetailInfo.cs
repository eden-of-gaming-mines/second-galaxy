﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildFlagShipHangarDetailInfo
    {
        public ulong m_instanceId;
        public GuildFlagShipHangarBasicInfo m_basicInfo;
        public List<GuildStaticFlagShipInstanceInfo> m_shipList;
        public int m_basicInfoVersion;
        public int m_shipListVersion;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

