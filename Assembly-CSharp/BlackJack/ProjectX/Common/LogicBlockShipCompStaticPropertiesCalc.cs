﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompStaticPropertiesCalc : LogicBlockCompPropertiesCalc
    {
        protected ILBStaticShip m_ownerShip;
        protected LBShipConfigPropertiesProvider m_configPropertiesProvider;
        private List<IPropertiesProvider> m_extraPropertiesProviders;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CollectProviderList;

        [MethodImpl(0x8000)]
        protected override void CollectProviderList()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBStaticShip ownerShip, List<IPropertiesProvider> extraPropertiesProviders)
        {
        }
    }
}

