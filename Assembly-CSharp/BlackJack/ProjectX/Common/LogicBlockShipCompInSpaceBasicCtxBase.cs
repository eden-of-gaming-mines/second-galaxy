﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ToolUtil;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompInSpaceBasicCtxBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget, bool, LBBulletDamageInfo> EventOnHitByTarget;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnBulletHitMiss;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLeaveStation;
        public DelegateEventEx<ILBSpaceTarget> EventOnDead;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<float> EventOnCostEnergy;
        protected ILBInSpaceShip m_ownerShip;
        protected IShipDataContainer m_shipDC;
        protected LogicBlockCompPropertiesCalc m_lbPropertiesCalc;
        protected bool m_inSpaceInitEnd;
        protected bool m_stateFreeze;
        protected uint m_stateFreezeTimeoutTime;
        protected bool m_isDead;
        protected float m_armorCurr;
        protected float m_shieldCurr;
        protected float m_energyCurr;
        protected uint? m_lastTickTime;
        protected uint m_lastShieldRecoveryTickTime;
        protected List<ShipEnergyRecoveryModify> m_sortedEnergyGrowModifyInfoList;
        [CompilerGenerated]
        private static Comparison<ShipEnergyRecoveryModify> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnInSpaceInitEnd;
        private static DelegateBridge __Hotfix_IsInSpaceInitEnd;
        private static DelegateBridge __Hotfix_RenewCtx;
        private static DelegateBridge __Hotfix_InitNpcArmorShieldEnergyByConfig;
        private static DelegateBridge __Hotfix_InitNpcArmorShieldEnergyByShipDC;
        private static DelegateBridge __Hotfix_OnLeaveStation;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_SetCurrEnergy;
        private static DelegateBridge __Hotfix_ChangeCurrEnergy;
        private static DelegateBridge __Hotfix_GetEnergyMax;
        private static DelegateBridge __Hotfix_GetCurrEnergy;
        private static DelegateBridge __Hotfix_GetEnergyPercent;
        private static DelegateBridge __Hotfix_SetCurrShield;
        private static DelegateBridge __Hotfix_GetShieldMax;
        private static DelegateBridge __Hotfix_GetCurrShield;
        private static DelegateBridge __Hotfix_GetShieldPercent;
        private static DelegateBridge __Hotfix_SetCurrArmor;
        private static DelegateBridge __Hotfix_GetArmorMax;
        private static DelegateBridge __Hotfix_GetCurrArmor;
        private static DelegateBridge __Hotfix_GetArmorPercent;
        private static DelegateBridge __Hotfix_GetArmorPercentInt;
        private static DelegateBridge __Hotfix_IsDead;
        private static DelegateBridge __Hotfix_GetShipType;
        private static DelegateBridge __Hotfix_GetShipMaxSpeed;
        private static DelegateBridge __Hotfix_IsStateFreeze;
        private static DelegateBridge __Hotfix_SetStateFreeze;
        private static DelegateBridge __Hotfix_GetShipJumpStableValue;
        private static DelegateBridge __Hotfix_TickForShieldRecovery;
        private static DelegateBridge __Hotfix_TickForArmorRecovery;
        private static DelegateBridge __Hotfix_TickForEnergyRecovery;
        private static DelegateBridge __Hotfix_GetEnergyRecoveryModifyInfo;
        private static DelegateBridge __Hotfix_AddSuperWeaponEnergyOnDamage;
        private static DelegateBridge __Hotfix_GetEnergyRecoveryOnHitByEnemyPropertiesIdForGrandFactionShip;
        private static DelegateBridge __Hotfix_AddSuperWeaponEquipOnEnergyCost;
        private static DelegateBridge __Hotfix_GetEnergyRecoveryOnEnergyUsePropertiesIdForGrandFactionShip;
        private static DelegateBridge __Hotfix_FireEventOnHitByTarget;
        private static DelegateBridge __Hotfix_FireEventOnBulletHitMiss;
        private static DelegateBridge __Hotfix_OnEventOnCostEnergy;
        private static DelegateBridge __Hotfix_FireEventOnDead;
        private static DelegateBridge __Hotfix_add_EventOnHitByTarget;
        private static DelegateBridge __Hotfix_remove_EventOnHitByTarget;
        private static DelegateBridge __Hotfix_add_EventOnBulletHitMiss;
        private static DelegateBridge __Hotfix_remove_EventOnBulletHitMiss;
        private static DelegateBridge __Hotfix_add_EventOnLeaveStation;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveStation;
        private static DelegateBridge __Hotfix_add_EventOnCostEnergy;
        private static DelegateBridge __Hotfix_remove_EventOnCostEnergy;
        private static DelegateBridge __Hotfix_get_ShieldHeatResist;
        private static DelegateBridge __Hotfix_get_ShieldKineticResist;
        private static DelegateBridge __Hotfix_get_ShieldElectricResist;
        private static DelegateBridge __Hotfix_get_ArmorHeatResist;
        private static DelegateBridge __Hotfix_get_ArmorKineticResist;
        private static DelegateBridge __Hotfix_get_ArmorElectricResist;

        public event Action<ILBSpaceTarget> EventOnBulletHitMiss
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float> EventOnCostEnergy
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget, bool, LBBulletDamageInfo> EventOnHitByTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLeaveStation
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void AddSuperWeaponEnergyOnDamage(float damageCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void AddSuperWeaponEquipOnEnergyCost(float costEnergyValue)
        {
        }

        [MethodImpl(0x8000)]
        public bool ChangeCurrEnergy(float energy, bool addSuperWeaponEquipEnergy = true)
        {
        }

        [MethodImpl(0x8000)]
        protected bool FireEventOnBulletHitMiss(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void FireEventOnDead(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected bool FireEventOnHitByTarget(ILBSpaceTarget target, bool isCritical, LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        public float GetArmorMax()
        {
        }

        [MethodImpl(0x8000)]
        public float GetArmorPercent()
        {
        }

        [MethodImpl(0x8000)]
        public int GetArmorPercentInt()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrArmor()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrShield()
        {
        }

        [MethodImpl(0x8000)]
        public float GetEnergyMax()
        {
        }

        [MethodImpl(0x8000)]
        public float GetEnergyPercent()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetEnergyRecoveryModifyInfo(out int currModifyIndex, out ShipEnergyRecoveryModify currModifyInfo)
        {
        }

        [MethodImpl(0x8000)]
        private PropertiesId GetEnergyRecoveryOnEnergyUsePropertiesIdForGrandFactionShip()
        {
        }

        [MethodImpl(0x8000)]
        private PropertiesId GetEnergyRecoveryOnHitByEnemyPropertiesIdForGrandFactionShip()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShieldMax()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShieldPercent()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipJumpStableValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipMaxSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public ShipType GetShipType()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void InitNpcArmorShieldEnergyByConfig()
        {
        }

        [MethodImpl(0x8000)]
        public void InitNpcArmorShieldEnergyByShipDC()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDead()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInSpaceInitEnd()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStateFreeze()
        {
        }

        [MethodImpl(0x8000)]
        public void OnEventOnCostEnergy(float costEnergy, bool addSuperWeaponEquipEnergy)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInSpaceInitEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnLeaveStation()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RenewCtx()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrArmor(float armor)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrShield(float shield)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStateFreeze(bool stateFreeze, uint timeoutTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForArmorRecovery(uint deltaTimeMs)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForEnergyRecovery(uint deltaTimeMs)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForShieldRecovery(uint deltaTimeMs)
        {
        }

        public float ShieldHeatResist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ShieldKineticResist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ShieldElectricResist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ArmorHeatResist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ArmorKineticResist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ArmorElectricResist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

