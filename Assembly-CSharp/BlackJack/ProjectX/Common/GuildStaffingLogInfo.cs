﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildStaffingLogInfo
    {
        public GuildStaffingLogType m_type;
        public int m_stringTemplateId;
        public List<FormatStringParamInfo> m_formatStrParamList;
        public DateTime m_time;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

