﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class KillRecordOpponentInfo
    {
        public string m_playerName;
        public int m_captainLastNameId;
        public int m_captainFirstNameId;
        public int m_playerAvatarId;
        public int m_shipId;
        public string m_guildCodeName;
        public uint m_guildId;
        public string m_allianceName;
        public string m_playerGameUserId;
        public int m_level;
        public ProfessionType m_profession;
        public uint m_teamId;
        public ulong m_fleetId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

