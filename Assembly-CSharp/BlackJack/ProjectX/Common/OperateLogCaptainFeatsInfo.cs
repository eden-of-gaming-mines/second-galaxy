﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    public class OperateLogCaptainFeatsInfo
    {
        public ulong m_captainInsId;
        public FeatsChangeType m_featsChangeType;
        public int m_oldFeatsId;
        public int m_newFeatsId;
        public FeatsSrc m_featsSrc;
        public int m_curFeatsLevel;
        private static DelegateBridge _c__Hotfix_ctor;

        public enum FeatsChangeType
        {
            Add = 1,
            Replase = 2,
            LevelUp = 3
        }

        public enum FeatsSrc
        {
            UseFeatsBooks = 1,
            LevelUp = 2
        }
    }
}

