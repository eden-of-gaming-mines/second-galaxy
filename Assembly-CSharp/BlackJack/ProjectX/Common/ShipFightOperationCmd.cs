﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class ShipFightOperationCmd
    {
        public CmdType m_type;
        public uint m_targetObjId;
        public ShipEquipSlotType m_slotType;
        public int m_slotIndex;
        private static DelegateBridge _c__Hotfix_ctor;

        public enum CmdType
        {
            None,
            DisableNormalAttackAutoRun,
            LockTarget,
            WeaponFire,
            EquipUse,
            SuperWeaponEquipUse,
            AutoFightStart,
            AutoFightStop,
            TypeMax
        }
    }
}

