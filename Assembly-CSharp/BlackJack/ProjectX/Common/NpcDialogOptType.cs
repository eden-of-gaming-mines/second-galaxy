﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum NpcDialogOptType
    {
        NextDialog = 1,
        Close = 2,
        Confirm2Server = 3,
        Quest2Accept = 4,
        QuestCondDialog = 5,
        QuestCondCommitItem = 6,
        OpenShop = 7,
        OpenAuction = 8,
        GuildBaseSolarSystemSet = 9,
        OpenBlackMarket = 10,
        OpenTradeLegalNpcShop = 11,
        OpenTradeIllegalNpcShop = 12,
        FactionCreditQuestToAccept = 13,
        Max = 14
    }
}

