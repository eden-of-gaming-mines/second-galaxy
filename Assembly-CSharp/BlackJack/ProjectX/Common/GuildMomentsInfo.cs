﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    public class GuildMomentsInfo
    {
        public uint m_seqId;
        public GuildMomentsType m_type;
        public int m_subTypeId;
        public List<FormatStringParamInfo> m_briefFormatStrParamList;
        public List<FormatStringParamInfo> m_contentFormatStrParamList;
        public DateTime m_createTime;
        public DateTime m_expireTime;
        public bool m_isShowInSummary;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

