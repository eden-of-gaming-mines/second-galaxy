﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class EquipFunctionCompBase
    {
        protected IEquipFunctionCompOwner m_owner;
        protected ILBInSpaceShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_Prepare4Remove;

        [MethodImpl(0x8000)]
        protected EquipFunctionCompBase(IEquipFunctionCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Prepare4Remove()
        {
        }
    }
}

