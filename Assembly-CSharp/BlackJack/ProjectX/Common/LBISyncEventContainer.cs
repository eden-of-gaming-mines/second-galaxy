﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBISyncEventContainer : ILBSyncEventContainer
    {
        protected SyncEventFlag m_eventFlag;
        protected List<LBSyncEventOnHitInfo> m_onHitList;
        protected List<LBSyncEvent> m_eventList;
        protected List<LBSyncEvent> m_holdOnList;
        private Dictionary<uint, LBSynEventAttachBuf> m_bufConfId2SyncEventAttachBufCache;
        private Dictionary<uint, LBSynEventAttachBufDuplicate> m_bufInstanceId2SyncEventAttachBufDuplicateCache;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RaiseSynEvent;
        private static DelegateBridge __Hotfix_RaiseSynEventNormalAttackLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventRailgunLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventPlasmaLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventCannonLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperCannonLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperPlasmaLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventMissileLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperMissileLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipAoeLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventLaserLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperLaserLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventDroneFighterLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperDroneFighterLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventDroneDefenderLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventDroneSniperLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventReloadAmmoStart;
        private static DelegateBridge __Hotfix_RaiseSynEventReloadAmmoEnd;
        private static DelegateBridge __Hotfix_RaiseSynEventOnHit;
        private static DelegateBridge __Hotfix_RaiseSynEventShieldBroken;
        private static DelegateBridge __Hotfix_RaiseSynEventDead;
        private static DelegateBridge __Hotfix_RaiseSynEventLockTarget;
        private static DelegateBridge __Hotfix_RaiseSynEventEnterFight;
        private static DelegateBridge __Hotfix_RaiseSynEventLeaveFight;
        private static DelegateBridge __Hotfix_RaiseSyncEventSuperGroupEnergyFull;
        private static DelegateBridge __Hotfix_RaiseSynEventDroneDestoryedByDefender;
        private static DelegateBridge __Hotfix_RaiseSynEventDroneDefenderPreCalcAttackMissile;
        private static DelegateBridge __Hotfix_RaiseSynEventSetInteraction;
        private static DelegateBridge __Hotfix_RaiseSynEventClearInteraction;
        private static DelegateBridge __Hotfix_RaiseSynEventStartInteraction;
        private static DelegateBridge __Hotfix_RaiseSynEventStopInteraction;
        private static DelegateBridge __Hotfix_RaiseSynEventSetTargetNotice;
        private static DelegateBridge __Hotfix_RaiseSynEventClearTargetNotice;
        private static DelegateBridge __Hotfix_RaiseSyncEventAttachBuf;
        private static DelegateBridge __Hotfix_RaiseSyncEventDetachBuf;
        private static DelegateBridge __Hotfix_RaiseSyncEventAttachBufDuplicate;
        private static DelegateBridge __Hotfix_RaiseSynEventOnDotDamage;
        private static DelegateBridge __Hotfix_RaiseSynEventOnHotHeal;
        private static DelegateBridge __Hotfix_RaiseSynEventEquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventEquipBlinkLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventEquipChannelLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventEquipChannelEnd;
        private static DelegateBridge __Hotfix_RaiseSyncEventEquipPeriodicDamageLaunch;
        private static DelegateBridge __Hotfix_RaiseSyncEventEquipPeriodicDamageEnd;
        private static DelegateBridge __Hotfix_RaiseSynEventEquipBlinkEnd;
        private static DelegateBridge __Hotfix_RaiseSynEventEquipInvisibleLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventTacticalEquipCDEndTime;
        private static DelegateBridge __Hotfix_RaiseSynEventEquipInvisibleEnd;
        private static DelegateBridge __Hotfix_RaiseSyncEventDropBoxPickStart;
        private static DelegateBridge __Hotfix_RaiseSyncEventDropBoxPickEnd;
        private static DelegateBridge __Hotfix_RaiseSyncEventAnimEffect;
        private static DelegateBridge __Hotfix_RaiseSyncEventSceneWingShipSettingChanged;
        private static DelegateBridge __Hotfix_RaiseSynEventWingShipSet;
        private static DelegateBridge __Hotfix_RaiseSynEventWingShipUnset;
        private static DelegateBridge __Hotfix_RaiseSyncEventResetJustCrime;
        private static DelegateBridge __Hotfix_RaiseSyncEventResetCriminalHunter;
        private static DelegateBridge __Hotfix_RaiseSyncEventJustCrimeTimeOut;
        private static DelegateBridge __Hotfix_RaiseSyncEventCriminalHunterTimeOut;
        private static DelegateBridge __Hotfix_RaiseSyncEventIncCriminalLevel;
        private static DelegateBridge __Hotfix_RaiseSyncEventOnNpcFactionInfoChanged;
        private static DelegateBridge __Hotfix_RaiseSyncEventReloadAllWeaponAmmo;
        private static DelegateBridge __Hotfix_RaiseSyncEventWeaponAmmoReloadByPercent;
        private static DelegateBridge __Hotfix_RaiseSyncEventShipItemStoreAddItem_0;
        private static DelegateBridge __Hotfix_RaiseSyncEventShipItemStoreRemoveItem_0;
        private static DelegateBridge __Hotfix_RaiseSyncEventShipItemStoreAddItem_1;
        private static DelegateBridge __Hotfix_RaiseSyncEventShipItemStoreRemoveItem_1;
        private static DelegateBridge __Hotfix_RaiseSyncEmptyEventShipItemStore;
        private static DelegateBridge __Hotfix_RaiseSyncEventSetTargetHideInTargetList;
        private static DelegateBridge __Hotfix_RaiseSyncEventSetTargetNotAttackable;
        private static DelegateBridge __Hotfix_RaiseSynEventResetWeaponEquipGroupCD;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipAntiJumpingForceShieldLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipDurationEnd;
        private static DelegateBridge __Hotfix_RaiseSynEventFlagShipTeleportTunnelLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipAddEnergyAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipClearCDAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipAddShieldAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEquipShipShield2ExtraShieldAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_RaiseSynEventSuperEnergyModify;
        private static DelegateBridge __Hotfix_RaiseSyncEventSetShipShield;
        private static DelegateBridge __Hotfix_RaiseSyncEventSetEnergy;
        private static DelegateBridge __Hotfix_RaiseSyncEventKillOtherTarget;
        private static DelegateBridge __Hotfix_RaiseSyncEventStartJumping;
        private static DelegateBridge __Hotfix_RaiseSyncEventTacticalEquipTimeInfo;
        private static DelegateBridge __Hotfix_RaiseSyncEventBlinkInterrupted;
        private static DelegateBridge __Hotfix_RaiseSyncEventFlagShipTeleportStartCharging;
        private static DelegateBridge __Hotfix_RaiseSyncEventFlagShipTeleportChargingFail;
        private static DelegateBridge __Hotfix_RaiseSyncEventFlagShipTeleportChargingFinish;
        private static DelegateBridge __Hotfix_RaiseSyncEventFlagShipTeleportProcessingFail;
        private static DelegateBridge __Hotfix_GetEventFlag;
        private static DelegateBridge __Hotfix_GetOnhitEventList;
        private static DelegateBridge __Hotfix_GetSynEventList;
        private static DelegateBridge __Hotfix_HasSyncEvent;
        private static DelegateBridge __Hotfix_AddSyncEvent;
        private static DelegateBridge __Hotfix_OptimizeSyncEvent4AttachBuf;
        private static DelegateBridge __Hotfix_Clear;

        [MethodImpl(0x8000)]
        protected void AddSyncEvent(LBSyncEvent syncEvent, bool isHoldOnEvent = false)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public SyncEventFlag GetEventFlag()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSyncEventOnHitInfo> GetOnhitEventList()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSyncEvent> GetSynEventList()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasSyncEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void OptimizeSyncEvent4AttachBuf()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEmptyEventShipItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventAnimEffect(NpcShipAnimEffectType effect, uint startTime, uint endTime, int openParam)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventAttachBuf(LBBufBase buf, ILBBufSource source)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventAttachBufDuplicate(int configId, uint bufInstanceId, int attachCount, uint lifeEndTime, ILBBufSource source, float bufInstanceParam)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventBlinkInterrupted(int reason)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventCriminalHunterTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventDetachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventDropBoxPickEnd(List<ShipStoreItemInfo> items)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventDropBoxPickStart(ILBSpaceDropBox dropBox, uint pickEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventEquipPeriodicDamageEnd(LBSpaceProcessEquipPeriodicDamageLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventEquipPeriodicDamageLaunch(LBSpaceProcessEquipPeriodicDamageLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventFlagShipTeleportChargingFail()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventFlagShipTeleportChargingFinish()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventFlagShipTeleportProcessingFail()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventFlagShipTeleportStartCharging()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventIncCriminalLevel(float incValue)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventJustCrimeTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventKillOtherTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventOnNpcFactionInfoChanged(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventReloadAllWeaponAmmo()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventResetCriminalHunter(DateTime endTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventResetJustCrime(DateTime endTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventSceneWingShipSettingChanged(SceneWingShipSetting setting)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventSetEnergy(float energyValue)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventSetShipShield(float shipShield)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventSetTargetHideInTargetList(ILBSpaceTarget target, bool isHide)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventSetTargetNotAttackable(ILBSpaceTarget target, bool isNotAttackable)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventShipItemStoreAddItem(ShipStoreItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventShipItemStoreAddItem(List<ShipStoreItemInfo> itemList)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventShipItemStoreRemoveItem(ShipStoreItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventShipItemStoreRemoveItem(List<ShipStoreItemInfo> itemList)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventStartJumping(bool isJumpingDisturbed)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventSuperGroupEnergyFull()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventTacticalEquipTimeInfo(uint effectStartTime, uint effectEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSyncEventWeaponAmmoReloadByPercent(List<ItemInfo> ammoCostList)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEvent(LBSyncEvent syncEvent)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventCannonLaunch(ILBSpaceTarget target, LBSpaceProcessBulletGunLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventClearInteraction()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventClearTargetNotice(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventDead()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventDroneDefenderLaunch(ILBSpaceTarget target, LBSpaceProcessDroneLaunchBase process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventDroneDefenderPreCalcAttackMissile(LBSpaceProcessDroneDefenderLaunch defenderProcess, LBSpaceProcessMissileLaunch missileProcess, List<LBSpaceProcessDroneDefenderLaunch.AttackChance> currOccupyedChanceList)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventDroneDestoryedByDefender(LBSpaceProcessDroneFighterLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventDroneFighterLaunch(ILBSpaceTarget target, LBSpaceProcessDroneLaunchBase process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventDroneSniperLaunch(ILBSpaceTarget target, LBSpaceProcessDroneLaunchBase process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEnterFight()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEquipAttachBufLaunch(ILBSpaceTarget target, LBSpaceProcessEquipAttachBufLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEquipBlinkEnd(ShipEquipSlotType equipSlotType, int equipSlotIndex, bool blinkSuccessful)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEquipBlinkLaunch(LBSpaceProcessEquipBlinkLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEquipChannelEnd(LBSpaceProcessEquipChannelLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEquipChannelLaunch(LBSpaceProcessEquipChannelLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEquipInvisibleEnd(ShipEquipSlotType equipSlotType, int equipSlotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventEquipInvisibleLaunch(LBSpaceProcessEquipInvisibleLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventFlagShipTeleportTunnelLaunch(LBSpaceProcessEquipTransformToTeleportTunnelLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventLaserLaunch(ILBSpaceTarget target, LBSpaceProcessLaserLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventLeaveFight()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventLockTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventMissileLaunch(ILBSpaceTarget target, LBSpaceProcessBulletGunLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventNormalAttackLaunch(ILBSpaceTarget target, LBSpaceProcessBulletGunLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventOnDotDamage(uint bufInstanceId, float damage, BufType bufType)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventOnHit(uint srcObjId, bool isHit, bool isCritical, float damage, bool onShield = false, bool onArmor = false, bool onShieldEx = false, bool onArmorEx = false)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventOnHotHeal(uint bufInstanceId, float healValue, BufType bufType)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventPlasmaLaunch(ILBSpaceTarget target, LBSpaceProcessBulletGunLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventRailgunLaunch(ILBSpaceTarget target, LBSpaceProcessBulletGunLaunch process, uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventReloadAmmoEnd(LBWeaponGroupBase group, ItemInfo reloadAmmoItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventReloadAmmoStart(LBWeaponGroupBase group, uint starTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventResetWeaponEquipGroupCD(List<uint> highSlotGroupCDEndTime, List<uint> middleSlotGroupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSetInteraction(SceneInteractionType type, uint optTime, uint range, int message, SceneInteractionFlag flag, int interactionTemplateId, uint singlePlayerInteractionCountMax)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSetTargetNotice(ILBSpaceTarget target, TargetNoticeType noticeType)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventShieldBroken()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventStartInteraction(uint objId, uint endTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventStopInteraction(uint objId, int stopReason, float? shipShield, float? shipArmor, float? shipEnergy)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperCannonLaunch(LBSpaceProcessSuperRailgunLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperDroneFighterLaunch(List<LBSpaceProcessDroneFighterLaunch> processList)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEnergyModify(float superEnergyModifyValue)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipAddEnergyAndAttachBufLaunch(ILBSpaceTarget target, LBSpaceProcessEquipAddEnergyAndAttachBufLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipAddShieldAndAttachBufLaunch(ILBSpaceTarget target, LBSpaceProcessEquipAddShieldAndAttachBufLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipAntiJumpingForceShieldLaunch(ILBSpaceTarget target, LBSpaceProcessEquipAntiJumpingForceShieldAndAttachBufLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipAoeLaunch(LBSpaceProcessSuperEquipAoeLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipAttachBufLaunch(ILBSpaceTarget target, LBSpaceProcessEquipAttachBufLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipClearCDAndAttachBuffLaunch(ILBSpaceTarget target, LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipDurationEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperEquipShipShield2ExtraShieldAndAttachBuffLaunch(ILBSpaceTarget target, LBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuffLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperLaserLaunch(LBSpaceProcessSuperLaserLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperMissileLaunch(LBSpaceProcessSuperMissileLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventSuperPlasmaLaunch(LBSpaceProcessSuperPlasmaLaunch process)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventTacticalEquipCDEndTime(uint groupCDEndTime)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventWingShipSet(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public void RaiseSynEventWingShipUnset()
        {
        }
    }
}

