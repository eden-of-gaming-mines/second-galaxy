﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildDiplomacyEnemyViewInfo
    {
        public List<PlayerSimplestInfo> m_enemyPlayer;
        public List<GuildSimplestInfo> m_enemyGuild;
        public List<AllianceBasicInfo> m_enemyAllience;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

