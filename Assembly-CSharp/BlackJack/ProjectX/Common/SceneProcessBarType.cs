﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum SceneProcessBarType
    {
        DistancePlayerToNpc,
        DistancePlayerToDummyLoc,
        DistanceNpcToDummyLoc,
        TimeOut,
        NpcHP,
        TimeOutPercentage,
        Custom,
        NpcShield,
        NpcArmor
    }
}

