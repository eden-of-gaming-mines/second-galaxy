﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildSimpleRuntimeInfoDataContainer
    {
        GuildSimpleRuntimeInfo GetGuildSimpleRuntimeInfo();
        void UpdateGuildSimpleRuntimeInfo(GuildSimpleRuntimeInfo info);
    }
}

