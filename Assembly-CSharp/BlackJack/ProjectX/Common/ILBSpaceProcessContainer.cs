﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessContainer
    {
        void CancelAllProcessByDestTarget(ILBSpaceTarget destTarget);
        void CancelProcess(LBSpaceProcess process);
        LBSpaceProcess GetProcessByInstanceId(uint instanceId);
        void RegistProcess(LBSpaceProcess process);
        void Tick(uint currTime);
    }
}

