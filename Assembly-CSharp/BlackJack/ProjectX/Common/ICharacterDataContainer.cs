﻿namespace BlackJack.ProjectX.Common
{
    public interface ICharacterDataContainer : ICharacterStaticDataContainer, ICharacterLevelMiscDataContainer, ICharacterSpaceDataContainer
    {
    }
}

