﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildStaticFlagShipInstanceInfo
    {
        public ulong m_instanceId;
        public int m_shipConfId;
        public string m_shipName;
        public bool m_isDestroyed;
        public double m_currFuel;
        public List<int> m_moduleSlotList;
        public uint m_armorCurr;
        public uint m_shieldCurr;
        public int m_shipHangarIndex;
        public bool m_isDeleted;
        public DateTime m_unpackTime;
        public int m_storeItemIndex;
        public PlayerSimplestInfo m_captainInfo;
        public int m_solarSystemId;
        public ShipState m_shipState;
        public DateTime m_shipDestroyedTime;
        public List<OffLineBufInfo> m_offLineBufList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

