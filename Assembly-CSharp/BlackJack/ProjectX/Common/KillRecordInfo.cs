﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class KillRecordInfo
    {
        public DateTime m_time;
        public KillRecordOpponentInfo m_winnerInfo;
        public KillRecordOpponentInfo m_losserInfo;
        public List<LostItemInfo> m_lostItemList;
        public int m_solarSystemId;
        public double m_lostBindMoney;
        public ulong m_lostShipInstanceId;
        public bool m_needSave2DB;
        public ulong m_instanceId;
        public CompensationStatus m_compensationStatus;
        public List<ItemCompensationStatus> m_itemCompensationStatus;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

