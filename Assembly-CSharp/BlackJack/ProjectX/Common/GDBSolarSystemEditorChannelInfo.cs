﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBSolarSystemEditorChannelInfo")]
    public class GDBSolarSystemEditorChannelInfo : IExtensible
    {
        private int _Culture;
        private int _factionId;
        private float _SignalDifficult;
        private float _SecurityLevel;
        private readonly List<int> _NpcMonsterCollectionIdList;
        private uint _PirateSignalWeight;
        private readonly List<int> _MineralIdList;
        private uint _MineralSignalWeight;
        private uint _DelegateTransportWeight;
        private bool _EnablePlayerSovereignty;
        private bool _CurrOccupyByNpc;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Culture;
        private static DelegateBridge __Hotfix_set_Culture;
        private static DelegateBridge __Hotfix_get_FactionId;
        private static DelegateBridge __Hotfix_set_FactionId;
        private static DelegateBridge __Hotfix_get_SignalDifficult;
        private static DelegateBridge __Hotfix_set_SignalDifficult;
        private static DelegateBridge __Hotfix_get_SecurityLevel;
        private static DelegateBridge __Hotfix_set_SecurityLevel;
        private static DelegateBridge __Hotfix_get_NpcMonsterCollectionIdList;
        private static DelegateBridge __Hotfix_get_PirateSignalWeight;
        private static DelegateBridge __Hotfix_set_PirateSignalWeight;
        private static DelegateBridge __Hotfix_get_MineralIdList;
        private static DelegateBridge __Hotfix_get_MineralSignalWeight;
        private static DelegateBridge __Hotfix_set_MineralSignalWeight;
        private static DelegateBridge __Hotfix_get_DelegateTransportWeight;
        private static DelegateBridge __Hotfix_set_DelegateTransportWeight;
        private static DelegateBridge __Hotfix_get_EnablePlayerSovereignty;
        private static DelegateBridge __Hotfix_set_EnablePlayerSovereignty;
        private static DelegateBridge __Hotfix_get_CurrOccupyByNpc;
        private static DelegateBridge __Hotfix_set_CurrOccupyByNpc;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Culture", DataFormat=DataFormat.TwosComplement)]
        public int Culture
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="factionId", DataFormat=DataFormat.TwosComplement)]
        public int FactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SignalDifficult", DataFormat=DataFormat.FixedSize)]
        public float SignalDifficult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SecurityLevel", DataFormat=DataFormat.FixedSize)]
        public float SecurityLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="NpcMonsterCollectionIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> NpcMonsterCollectionIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="PirateSignalWeight", DataFormat=DataFormat.TwosComplement)]
        public uint PirateSignalWeight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="MineralIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> MineralIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MineralSignalWeight", DataFormat=DataFormat.TwosComplement)]
        public uint MineralSignalWeight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="DelegateTransportWeight", DataFormat=DataFormat.TwosComplement)]
        public uint DelegateTransportWeight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="EnablePlayerSovereignty", DataFormat=DataFormat.Default)]
        public bool EnablePlayerSovereignty
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="CurrOccupyByNpc", DataFormat=DataFormat.Default)]
        public bool CurrOccupyByNpc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

