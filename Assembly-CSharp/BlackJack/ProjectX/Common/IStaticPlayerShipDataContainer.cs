﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IStaticPlayerShipDataContainer : IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        ulong GetInstanceId();
        ShipDestroyState GetShipDestroyState();
        int GetShipHangarIndex();
        ShipState GetShipState();
        int GetShipStoreItemIndex();
        DateTime GetUnpackTime();
        bool IsDeleted();
        void UpdateShipDestroyState(ShipDestroyState state);
        void UpdateShipState(ShipState state);
    }
}

