﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using System;

    public interface ILBShipCaptain : IPropertiesProvider
    {
        int? GetDrivingLicenseLevelById(int drivingLicenseId);
    }
}

