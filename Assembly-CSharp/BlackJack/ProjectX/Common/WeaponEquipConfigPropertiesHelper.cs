﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public static class WeaponEquipConfigPropertiesHelper
    {
        private static DelegateBridge __Hotfix_CalcPropertiesWithBaseValue;
        private static DelegateBridge __Hotfix_CalcPropertiesWithInternalValue;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4Equip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigColumn4Equip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromPropertiesList4Equip;
        private static DelegateBridge __Hotfix_GetDestBufPropertyFromConfigData4Equip;
        private static DelegateBridge __Hotfix_GetSelfBufPropertyFromConfigData4Equip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4TacticalEquip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigColumn4TacticalEquip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromPropertiesList4TacticalEquip;
        private static DelegateBridge __Hotfix_GetBufPropertyFromConfigData4TacticalEquip;
        private static DelegateBridge __Hotfix_GetLocalEquipCommonPropertyFromConfig;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4SuperEquip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigColumn4SuperEquip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromPropertiesList4SuperEquip;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4SuperRailgunWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4SuperPlasmaWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4SuperCannonWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4SuperMissileWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4SuperLaserWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4SuperDroneWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4NormalAttackWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4RailgunWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4PlasmaWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4CannonWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4LaserWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4MissileWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4DroneWeapon;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4NormalAttackAmmo;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4RailgunAmmo;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4PlasmaAmmo;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4CannonAmmo;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4LaserAmmo;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4MissileAmmo;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4DroneAmmo;

        [MethodImpl(0x8000)]
        public static float CalcPropertiesWithBaseValue(PropertiesId propertiesId, float currValue, float baseValue)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcPropertiesWithInternalValue(PropertiesId propertiesId, float? currValue, float internalValue)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetBufPropertyFromConfigData4TacticalEquip(ConfigDataShipTacticalEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetDestBufPropertyFromConfigData4Equip(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        private static bool GetLocalEquipCommonPropertyFromConfig(EquipType equipType, PropertiesId propertiesId, out float result, float launchCD, float fireRange, float launchEnergyCost = 0f, float chargeTime = 0f, float launchEnergyPercentCost = 0f, float cpuCost = 0f, float powerCost = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigColumn4Equip(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigColumn4SuperEquip(LBSuperEquipGroupBase superEquipGroup, ConfigDataShipSuperEquipInfo confInfo, PropertiesId propertiesId, ConfigDataShipSuperEquipRunningInfo runingInfo, out float columnResult)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigColumn4TacticalEquip(LBTacticalEquipGroupBase tacticalEquipGroup, ConfigDataShipTacticalEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4CannonAmmo(ConfigDataAmmoInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4CannonWeapon(ConfigDataWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4DroneAmmo(ConfigDataDroneInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4DroneWeapon(ConfigDataWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4Equip(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4LaserAmmo(ConfigDataAmmoInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4LaserWeapon(ConfigDataWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4MissileAmmo(ConfigDataMissileInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4MissileWeapon(ConfigDataWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4NormalAttackAmmo(ConfigDataAmmoInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4NormalAttackWeapon(ConfigDataWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4PlasmaAmmo(ConfigDataAmmoInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4PlasmaWeapon(ConfigDataWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4RailgunAmmo(ConfigDataAmmoInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4RailgunWeapon(ConfigDataWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4SuperCannonWeapon(ConfigDataSuperWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4SuperDroneWeapon(ConfigDataSuperWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4SuperEquip(LBSuperEquipGroupBase superEquipGroup, ConfigDataShipSuperEquipInfo confInfo, PropertiesId propertiesId, ConfigDataShipSuperEquipRunningInfo runingInfo, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4SuperLaserWeapon(ConfigDataSuperWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4SuperMissileWeapon(ConfigDataSuperWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4SuperPlasmaWeapon(ConfigDataSuperWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4SuperRailgunWeapon(ConfigDataSuperWeaponInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromConfigData4TacticalEquip(LBTacticalEquipGroupBase tacticalEquipGroup, ConfigDataShipTacticalEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromPropertiesList4Equip(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromPropertiesList4SuperEquip(ConfigDataShipSuperEquipInfo confInfo, PropertiesId propertiesId, out float listResult)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetLocalPropertyFromPropertiesList4TacticalEquip(ConfigDataShipTacticalEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetSelfBufPropertyFromConfigData4Equip(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId, out float result)
        {
        }
    }
}

