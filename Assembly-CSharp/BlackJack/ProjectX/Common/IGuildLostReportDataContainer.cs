﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildLostReportDataContainer
    {
        void AddBuildingLostReport(BuildingLostReport report);
        void AddSovereignLostReport(SovereignLostReport report);
        BuildingLostReport GetBuildingLostReport(ulong buildingInsId);
        SovereignLostReport GetSovereignLostReport(int solarSystemId);
        List<SovereignLostReport> GetSovereignLostReportList();
    }
}

