﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct FormationCreatingInfo
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static FormationCreatingInfo <Default>k__BackingField;
        public bool m_isFormationFlipped;
        public double m_ratioFromSphereRadiusToLeaderShipSize;
        public double m_formationPosSeparatingAngle;
        public double m_formationSphereScaleY;
        public double m_ratioFromMemberSpaceSizeToLeaderShipSize;
        public double m_formationAngle;
        public double m_distanceFixOffset;
        public int m_memberCountPerPanelSide;
        public int m_squareMatrixPanelCount;
        [MethodImpl(0x8000)]
        static FormationCreatingInfo()
        {
        }

        [MethodImpl(0x8000)]
        public static FormationCreatingInfo CreateFormationInfoDefaultType(double ratio, double angle, bool isFlipped = false, double fixOffset = 0.0)
        {
        }

        [MethodImpl(0x8000)]
        public static FormationCreatingInfo CreateFormationInfoEllipsoid(double ratio, double angle, double scale = 1.0, double fixOffset = 0.0)
        {
        }

        [MethodImpl(0x8000)]
        public static FormationCreatingInfo CreateFormationInfoWildGoose(double ratio, double angle, bool isFlipped = false, double fixOffset = 0.0)
        {
        }

        [MethodImpl(0x8000)]
        public static FormationCreatingInfo CreatFormationInfoSquareMatrix(double pannelCount, double memberCountPerSide, double ratio, double fixOffset = 0.0)
        {
        }

        public static FormationCreatingInfo Default
        {
            [CompilerGenerated]
            get => 
                <Default>k__BackingField;
            [CompilerGenerated]
            private set => 
                (<Default>k__BackingField = value);
        }
    }
}

