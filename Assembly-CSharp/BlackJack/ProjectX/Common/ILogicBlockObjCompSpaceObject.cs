﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;
    using System.Runtime.InteropServices;

    public interface ILogicBlockObjCompSpaceObject
    {
        Vector3D GetLocation();
        string GetName();
        uint GetObjectId();
        void GetPose(out Vector3D location, out Quaternion rotation, out Vector4D velocity);
        Vector3D GetRotationEuler();
        Quaternion GetRotationQuaternion();
        bool IsWaitForRemove();
        void SetLocation(Vector3D location);
        void SetPose(Vector3D location, Quaternion rotation, Vector4D velocity);
    }
}

