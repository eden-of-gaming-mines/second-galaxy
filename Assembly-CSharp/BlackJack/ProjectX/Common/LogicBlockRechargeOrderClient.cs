﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockRechargeOrderClient : LogicBlockRechargeOrderBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RefreshAllRechargeOrders;
        private static DelegateBridge __Hotfix_RechargeOrderAcknowledge;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public void RechargeOrderAcknowledge(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllRechargeOrders(List<RechargeOrderInfo> orderList, int version)
        {
        }

        private IRechargeOrderDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

