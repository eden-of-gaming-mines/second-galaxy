﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IBattlePassDataContainer
    {
        int GetChallangeQuestCompletedCount();
        int GetChallangeQuestGroupIndex();
        int GetExp();
        int GetPeriodId();
        DateTime GetPeriodStartTime();
        bool IsChallangeQuestAccRewardBalanced(int rewardIndex);
        bool IsChallangeQuestCompleted(int totalChallangeQuestIndex);
        bool IsRewardBalanced(int level, bool isUpgrade);
        bool IsUpgraded();
        void SetChallangeQuestAccRewardBalanced(int rewardIndex);
        void SetChallangeQuestCompleted(int totalChallangeQuestIndex);
        void SetChallangeQuestGroupIndex(int index);
        void SetExp(int exp);
        void SetPeriod(int id, DateTime periodStartTime);
        void SetRewardBalanced(int level, bool isUpgrade);
        void SetUpgraded();
    }
}

