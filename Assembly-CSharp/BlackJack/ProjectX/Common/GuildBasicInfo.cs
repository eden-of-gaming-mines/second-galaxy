﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildBasicInfo
    {
        public uint m_guildId;
        public string m_name;
        public string m_codeName;
        public GuildAllianceLanguageType m_languageType;
        public GuildJoinPolicy m_joinPllicy;
        public GuildLogoInfo m_logoInfo;
        public uint m_allianceId;
        public string m_allianceName;
        public AllianceLogoInfo m_allianceLogo;
        public string m_guildLeader;
        public int m_baseSolarSystem;
        public int m_baseStation;
        public string m_manifesto;
        public string m_announcement;
        public DateTime m_dismissEndTime;
        public DateTime m_leaderTransferEndTime;
        public string m_leaderTransferTargetUserId;
        public DateTime m_nextDailyRefreshTime;
        public DateTime m_nextWeeklyRefreshTime;
        public string m_walletAnnouncement;
        public int m_reinforcementEndHour;
        public int m_reinforcementEndMinuts;
        public bool m_reinforcementTimeHasSetted;
        public ushort m_version;
        public int m_memberCount;
        public bool m_haveOccupiedSolarSystem;
        public bool m_delete;
        public bool m_npcGuild;
        public bool m_isAutomaticCompensation;
        public DateTime m_createTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

