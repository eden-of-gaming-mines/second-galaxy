﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IGuildMemberListDataContainer
    {
        void AddMember(GuildMemberInfo info);
        GuildMemberInfo GetMemberInfo(string gameUserId);
        GuildMemberListInfo GetMemberListInfo();
        void RemoveMember(string gameUserId, DateTime reJoinCDEndTime);
        void UpdateMemberDynamicInfo(GuildMemberInfo readOnlyInfo, long informationPointCurrWeek);
        void UpdateMemberDynamicInfo(GuildMemberInfo readOnlyInfo, long contribute, long donateValue, long contirbuteCurrWeek);
        void UpdateMemberGuildFleetId(GuildMemberInfo readOnlyInfo, ulong fleetId);
        void UpdateMemberJobInfo(GuildMemberInfo readOnlyInfo, GuildJobType job, bool isAppoint);
        void UpdateMemberRuntimeInfo(GuildMemberInfo readOnlyInfo, GuildMemberRuntimeInfo runtimeInfo);
    }
}

