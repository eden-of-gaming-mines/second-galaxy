﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CapsuleCollisionInfo")]
    public class CapsuleCollisionInfo : IExtensible
    {
        private PVector3D _pointA;
        private PVector3D _pointB;
        private float _radius;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PointA;
        private static DelegateBridge __Hotfix_set_PointA;
        private static DelegateBridge __Hotfix_get_PointB;
        private static DelegateBridge __Hotfix_set_PointB;
        private static DelegateBridge __Hotfix_get_Radius;
        private static DelegateBridge __Hotfix_set_Radius;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="pointA", DataFormat=DataFormat.Default)]
        public PVector3D PointA
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="pointB", DataFormat=DataFormat.Default)]
        public PVector3D PointB
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="radius", DataFormat=DataFormat.FixedSize)]
        public float Radius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

