﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildDynamicBase
    {
        GuildDynamicInfo GetGuildDynamicInfo();
        void SetGuildDynamicInfo(GuildDynamicInfo info);
    }
}

