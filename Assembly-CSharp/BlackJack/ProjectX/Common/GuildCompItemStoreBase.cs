﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompItemStoreBase : GuildCompBase, IGuildCompItemStoreBase, IGuildItemStoreBase, IGuildItemStoreItemOperationBase
    {
        protected List<LBStoreItem> m_itemList;
        protected Func<ulong> m_instanceIdAllocator;
        [CompilerGenerated]
        private static Predicate<LBStoreItem> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_GetItemInfoByItemId;
        private static DelegateBridge __Hotfix_GetItemInfoByItemInsId;
        private static DelegateBridge __Hotfix_GetItemInfoByIndex;
        private static DelegateBridge __Hotfix_GetGuildStoreItemInfoList;
        private static DelegateBridge __Hotfix_RemoveItemDirectly;
        private static DelegateBridge __Hotfix_CheckAddItem;
        private static DelegateBridge __Hotfix_CheckRemoveGuildStoreItem;
        private static DelegateBridge __Hotfix_CheckSuspendItem;
        private static DelegateBridge __Hotfix_CheckResumeItem;
        private static DelegateBridge __Hotfix_IsGuildStoreItemMatched;
        private static DelegateBridge __Hotfix_AddNewItemDirectly;
        private static DelegateBridge __Hotfix_UpdateItemDirectly;
        private static DelegateBridge __Hotfix_FindFreeItemOrCreateEmpty;
        private static DelegateBridge __Hotfix_CreateEmptyItemForIndex;
        private static DelegateBridge __Hotfix_GenerateStoreItemSuspendFlag;
        private static DelegateBridge __Hotfix_CreateLBStoreItemByItemInfo;
        private static DelegateBridge __Hotfix_FireEventItemChange;
        private static DelegateBridge __Hotfix_InitStoreItemList;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompItemStoreBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStoreItem AddNewItemDirectly(StoreItemType itemType, int itemConfigId, long count, ItemSuspendReason suspendReason = 0, int index = -1, ulong instanceId = 0UL, OperateLogGuildStoreItemChangeType causeId = 0, string operateLogLocation = null, GuildMemberBase operateMember = null)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckAddItem(StoreItemType itemType, int itemConfigId, long count)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckRemoveGuildStoreItem(int index, long count)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckResumeItem(LBStoreItem from, long count, bool needCheckStoreSpace = true)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckSuspendItem(LBStoreItem from, long count, ItemSuspendReason reason, LBStoreItem to)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem CreateEmptyItemForIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStoreItem CreateLBStoreItemByItemInfo(GuildStoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStoreItem FindFreeItemOrCreateEmpty()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void FireEventItemChange(StoreItemType itemType, int itemConfId, long changeCount, OperateLogGuildStoreItemChangeType causeId = 0, string location = null, GuildMemberBase operateMember = null)
        {
        }

        [MethodImpl(0x8000)]
        protected uint GenerateStoreItemSuspendFlag(ItemSuspendReason reason)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> GetGuildStoreItemInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemInfoByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemInfoByItemId(StoreItemType type, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemInfoByItemInsId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildStoreItemMatched(int storeItemIndex, ItemInfo comparedItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitlialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void RemoveItemDirectly(LBStoreItem item, OperateLogGuildStoreItemChangeType causeId = 0, string operateLogLocation = null, GuildMemberBase operateMember = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateItemDirectly(LBStoreItem item, long count, long changeCount = 0L, OperateLogGuildStoreItemChangeType causeId = 0, string operateLogLocation = null, GuildMemberBase operateMember = null)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

