﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildProductionDataContainer
    {
        bool AddGuildProductionLine(GuildProductionLineInfo lineInfo);
        bool CheckGuildProductionDataVersion(uint version);
        void ClearGuildProductionLineData(ulong lineId);
        uint GetGuildProductionDataVersion();
        GuildProductionLineInfo GetGuildProductionLineById(ulong lineId);
        List<GuildProductionLineInfo> GetGuildProductionLineList();
        bool RemoveGuildProductionLine(ulong lineId);
        void UpdateGuildProductionLine(GuildProductionLineInfo info);
        void UpdateGuildProductionLineLevel(ulong lineId, int lineLevel);
        void UpdateGuildProductionLineLost(ulong lineId);
        void UpdateGuildProductionLineSpeedUpTime(ulong lineId, int reduceTime);
    }
}

