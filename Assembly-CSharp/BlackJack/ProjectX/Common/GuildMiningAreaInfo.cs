﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildMiningAreaInfo
    {
        public ulong m_buildingInstanceId;
        public int m_solarSystemId;
        public GuildMiningStatus m_miningStatus;
        public DateTime m_currMiningStatusStartTime;
        public DateTime m_currMiningStatusEndTime;
        public int m_miningBalanceRoundTotalCount;
        public int m_miningBalanceRoundCompletedCount;
        public List<GuildMiningMineralInfo> m_miningBalanceTotalRewardInfos;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildMiningAreaInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMiningAreaInfo(GuildMiningAreaInfo info)
        {
        }
    }
}

