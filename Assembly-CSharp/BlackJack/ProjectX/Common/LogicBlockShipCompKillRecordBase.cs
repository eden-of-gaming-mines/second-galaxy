﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompKillRecordBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo, ILBInSpaceShip, string, bool> EventOnKillRecordCreate4Winnner;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<KillRecordInfo, ILBInSpaceShip, string, bool> EventOnKillRecordCreate4Losser;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBInSpaceNpcShip> EventOnKillNpcShip;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBInSpaceNpcShip> EventOnKillHiredCaptainShip;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo> EventOnSendAnnouncementPlayerKilled;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo> EventOnOperateLogShipDestoryed;
        protected ILBInSpaceShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_add_EventOnKillRecordCreate4Winnner;
        private static DelegateBridge __Hotfix_remove_EventOnKillRecordCreate4Winnner;
        private static DelegateBridge __Hotfix_add_EventOnKillRecordCreate4Losser;
        private static DelegateBridge __Hotfix_remove_EventOnKillRecordCreate4Losser;
        private static DelegateBridge __Hotfix_add_EventOnKillNpcShip;
        private static DelegateBridge __Hotfix_remove_EventOnKillNpcShip;
        private static DelegateBridge __Hotfix_add_EventOnKillHiredCaptainShip;
        private static DelegateBridge __Hotfix_remove_EventOnKillHiredCaptainShip;
        private static DelegateBridge __Hotfix_add_EventOnSendAnnouncementPlayerKilled;
        private static DelegateBridge __Hotfix_remove_EventOnSendAnnouncementPlayerKilled;
        private static DelegateBridge __Hotfix_add_EventOnOperateLogShipDestoryed;
        private static DelegateBridge __Hotfix_remove_EventOnOperateLogShipDestoryed;
        private static DelegateBridge __Hotfix_FireEventOnKillRecordCreate4Winnner;
        private static DelegateBridge __Hotfix_FireEventOnKillRecordCreate4Losser;
        private static DelegateBridge __Hotfix_FireEventOnSendAnnouncementPlayerKilled;
        private static DelegateBridge __Hotfix_FireEventOnKillNpcShip;
        private static DelegateBridge __Hotfix_FireEventOnKillHiredCaptainShip;
        private static DelegateBridge __Hotfix_FireEventOnOperateLogShipDestoryed;

        public event Action<ILBInSpaceNpcShip> EventOnKillHiredCaptainShip
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBInSpaceNpcShip> EventOnKillNpcShip
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo, ILBInSpaceShip, string, bool> EventOnKillRecordCreate4Losser
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo, ILBInSpaceShip, string, bool> EventOnKillRecordCreate4Winnner
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnOperateLogShipDestoryed
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnSendAnnouncementPlayerKilled
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnKillHiredCaptainShip(ILBInSpaceNpcShip killedShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnKillNpcShip(ILBInSpaceNpcShip killedShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnKillRecordCreate4Losser(KillRecordInfo info, ILBInSpaceShip killedShip, string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnKillRecordCreate4Winnner(KillRecordInfo info, ILBInSpaceShip killedShip, string playerGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnOperateLogShipDestoryed(KillRecordInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnSendAnnouncementPlayerKilled(KillRecordInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

