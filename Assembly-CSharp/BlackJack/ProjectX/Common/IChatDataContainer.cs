﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IChatDataContainer
    {
        void Ban(DateTime bannedTime, string bannedReason);
        DateTime GetBannedTime();
        bool IsBanned(DateTime currentTime);
        bool IsSilentlyBanned(DateTime currentTime);
        void SilentBan(DateTime bannedTime);
        void SilentUnban();
        void Unban();
    }
}

