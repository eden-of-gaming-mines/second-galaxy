﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBBufShieldProtect : LBShipFightBuf
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnAttach;
        private static DelegateBridge __Hotfix_GetProtectPercent;

        [MethodImpl(0x8000)]
        public LBBufShieldProtect(ConfigDataBufInfo confInfo, uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public float GetProtectPercent()
        {
        }

        [MethodImpl(0x8000)]
        public override void OnAttach(ILBBufSource source, ILBBufOwner owner, uint currTime, uint lifeEndTime, LBBuffContainer container, object param = null, float bufInstanceParam1 = 0f, EquipType sourceEquipType = 0x30)
        {
        }
    }
}

