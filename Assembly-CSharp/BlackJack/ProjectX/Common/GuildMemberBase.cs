﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildMemberBase
    {
        protected IGuildDataContainerBase m_dc;
        protected GuildMemberInfo m_readOnlyInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsOnline;
        private static DelegateBridge __Hotfix_UpdateMemberJobs;
        private static DelegateBridge __Hotfix_UpdateMemberFleetId;
        private static DelegateBridge __Hotfix_OnMemberDonate;
        private static DelegateBridge __Hotfix_ClearDonateTradeMoneyToday;
        private static DelegateBridge __Hotfix_ClearWeeklyContribute;
        private static DelegateBridge __Hotfix_UpdateRuntimeInfo;
        private static DelegateBridge __Hotfix_GetGuildJob;
        private static DelegateBridge __Hotfix_GetShowGuildJobType;
        private static DelegateBridge __Hotfix_GetMemberInfo;
        private static DelegateBridge __Hotfix_GetMemberName;
        private static DelegateBridge __Hotfix_GetMemberSeq;
        private static DelegateBridge __Hotfix_GetJoinTime;
        private static DelegateBridge __Hotfix_GetLastOnlineTime;
        private static DelegateBridge __Hotfix_GetMemberGameUserId;
        private static DelegateBridge __Hotfix_GetDynamicInfo;
        private static DelegateBridge __Hotfix_HasPermission_0;
        private static DelegateBridge __Hotfix_HasPermission_1;
        private static DelegateBridge __Hotfix_get_GameUserId;
        private static DelegateBridge __Hotfix_get_Seq;

        [MethodImpl(0x8000)]
        public GuildMemberBase(IGuildDataContainerBase dc, GuildMemberInfo readOnlyInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearDonateTradeMoneyToday()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearWeeklyContribute()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberDynamicInfo GetDynamicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildJobType> GetGuildJob()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetJoinTime()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetLastOnlineTime()
        {
        }

        [MethodImpl(0x8000)]
        public string GetMemberGameUserId()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberInfo GetMemberInfo()
        {
        }

        [MethodImpl(0x8000)]
        public string GetMemberName()
        {
        }

        [MethodImpl(0x8000)]
        public int GetMemberSeq()
        {
        }

        [MethodImpl(0x8000)]
        public GuildJobType GetShowGuildJobType()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasPermission(GuildPermission permission)
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasPermission(List<GuildJobType> jobs, GuildPermission permission)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOnline()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemberDonate(long donateTradeMoney, long contributionAdd)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemberFleetId(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemberJobs(GuildJobType job, bool isAppoint)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRuntimeInfo(GuildMemberRuntimeInfo runtimeInfo)
        {
        }

        public string GameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ushort Seq
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

