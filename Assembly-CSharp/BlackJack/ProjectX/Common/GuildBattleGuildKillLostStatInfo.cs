﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildBattleGuildKillLostStatInfo
    {
        public GuildBattleGroupType m_groupType;
        public GuildSimplestInfo m_guildInfo;
        public double m_selfLostBindMoney;
        public double m_killingLostBindMoney;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

