﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CollisionInfo")]
    public class CollisionInfo : IExtensible
    {
        private float _RadiusMax;
        private readonly List<SphereCollisionInfo> _sphereCollisions;
        private readonly List<CapsuleCollisionInfo> _capsuleCollisions;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_RadiusMax;
        private static DelegateBridge __Hotfix_set_RadiusMax;
        private static DelegateBridge __Hotfix_get_SphereCollisions;
        private static DelegateBridge __Hotfix_get_CapsuleCollisions;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="RadiusMax", DataFormat=DataFormat.FixedSize)]
        public float RadiusMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="sphereCollisions", DataFormat=DataFormat.Default)]
        public List<SphereCollisionInfo> SphereCollisions
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="capsuleCollisions", DataFormat=DataFormat.Default)]
        public List<CapsuleCollisionInfo> CapsuleCollisions
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

