﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBSpaceProcessEquipPeriodicDamageLaunch : LBSpaceProcessEquipLaunchWithDamageBase
    {
        protected uint m_periodicCD;
        protected uint m_totalTime;
        protected uint m_nextPeriodicTimeAbs;
        protected uint m_endTime;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_EndPeriodicDamageProcess;
        private static DelegateBridge __Hotfix_SetPeriodicCD;
        private static DelegateBridge __Hotfix_GetTotalTime;
        private static DelegateBridge __Hotfix_GetPeriodicCD;
        private static DelegateBridge __Hotfix_SetTotalTime;
        private static DelegateBridge __Hotfix_OnEquipFire;
        private static DelegateBridge __Hotfix_IsInPeriodicing;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipPeriodicDamageLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipPeriodicDamageLaunch(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void EndPeriodicDamageProcess()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetPeriodicCD()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetTotalTime()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsInPeriodicing()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEquipFire(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPeriodicCD(uint cd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTotalTime(uint totalTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

