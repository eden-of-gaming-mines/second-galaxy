﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class AllianceInviteInfo
    {
        public ulong m_instanceId;
        public uint m_allianceId;
        public uint m_inviteeGuildId;
        public DateTime m_createTime;
        public DateTime m_expireTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

