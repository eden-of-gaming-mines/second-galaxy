﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessSuperPlasmaLaunch : LBSpaceProcessSuperWeaponLaunchBase
    {
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperPlasmaLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperPlasmaLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }
    }
}

