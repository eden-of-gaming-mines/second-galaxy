﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildBattleSimpleReportInfo
    {
        public ulong m_guildBattleInstanceId;
        public int m_solarSystemId;
        public GuildBattleType m_type;
        public GuildBuildingType m_buildingType;
        public GuildBattleGroupType m_groupType;
        public GuildSimplestInfo m_selfGuildInfo;
        public GuildSimplestInfo m_enemyGuildInfo;
        public int m_npcGuildConfigId;
        public GuildBattleStatus m_status;
        public DateTime m_currStatusStartTime;
        public DateTime m_currStatusEndTime;
        public bool m_isWin;
        public double m_killingLostBindMoney;
        public double m_selfLostBindMoney;
        public DateTime m_startTime;
        public DateTime m_endTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

