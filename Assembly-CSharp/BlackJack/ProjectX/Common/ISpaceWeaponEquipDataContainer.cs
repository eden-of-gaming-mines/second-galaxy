﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface ISpaceWeaponEquipDataContainer
    {
        AmmoInfo[] GetHighSoltAmmoList();
        ShipSlotGroupInfo[] GetHighSoltGroupItemList();
        List<ShipSlotGroupInfo> GetLowSlotItemList();
        List<ShipSlotGroupInfo> GetMiddleSlotItemList();
        void UpdateSlotGroupAmmoInfo(int groupIndex, StoreItemType ammoType, int ammoConfigId, int ammoCount);
        void UpdateSlotGroupInfo(ShipEquipSlotType slotType, int groupIndex, int storeItemIndex);
    }
}

