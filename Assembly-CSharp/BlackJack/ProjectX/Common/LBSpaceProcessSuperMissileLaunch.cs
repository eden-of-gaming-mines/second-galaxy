﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessSuperMissileLaunch : LBSpaceProcessSuperWeaponLaunchBase
    {
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_AllocTargetForEvergyUnitLaunch;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperMissileLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperMissileLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void AllocTargetForEvergyUnitLaunch(List<ILBSpaceTarget> targetList)
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceProcessWeaponLaunchSource processSource)
        {
        }
    }
}

