﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum PropertyGroup
    {
        PropertyGroup_Base,
        PropertyGroup_Static,
        PropertyGroup_InSpaceStatic,
        PropertyGroup_InSpaceDynamic,
        PropertyGroup_Final,
        PropertyGroup_FinalFrameCache,
        PropertyGroup_FinalNoCache
    }
}

