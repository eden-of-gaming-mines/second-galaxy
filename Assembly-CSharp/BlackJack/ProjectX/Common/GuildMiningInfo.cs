﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildMiningInfo
    {
        public DateTime m_nextBalanceTime;
        public DateTime m_nextNextBalanceTime;
        public int m_miningBalanceTimeHour;
        public int m_miningBalanceTimeMinute;
        public List<GuildMiningAreaInfo> m_miningAreaInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

