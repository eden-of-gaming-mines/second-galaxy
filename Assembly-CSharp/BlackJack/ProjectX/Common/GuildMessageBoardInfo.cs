﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildMessageBoardInfo
    {
        public List<GuildMessageInfo> m_messageList;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

