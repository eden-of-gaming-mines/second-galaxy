﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockRechargeMonthlyCardBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IRechargeMonthlyCardDataContainer m_dc;
        protected readonly List<LBRechargeMonthlyCard> m_lbRechargeMonthlyCardList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_InitLBRechargeMonthlyCardList;
        private static DelegateBridge __Hotfix_GetRechargeMonthlyCard;
        private static DelegateBridge __Hotfix_GetRechargeMonthlyCardList;
        private static DelegateBridge __Hotfix_GetRechargeMonthlyCardVersion;
        private static DelegateBridge __Hotfix_AddRechargeMonthlyCard;

        [MethodImpl(0x8000)]
        public void AddRechargeMonthlyCard(RechargeMonthlyCardInfo monthlyCard)
        {
        }

        [MethodImpl(0x8000)]
        public LBRechargeMonthlyCard GetRechargeMonthlyCard(int monthlyCardId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<LBRechargeMonthlyCard> GetRechargeMonthlyCardList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRechargeMonthlyCardVersion()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLBRechargeMonthlyCardList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

