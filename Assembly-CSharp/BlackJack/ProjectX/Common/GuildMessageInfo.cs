﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildMessageInfo
    {
        public ulong m_instanceId;
        public string m_playerUserId;
        public string m_playerName;
        public uint m_playerGuildId;
        public string m_content;
        public DateTime m_time;
        public bool m_delete;
        public string m_deleteAdminName;
        public string m_deleteAdminGameUserId;
        public PlayerSimplestInfo m_playerSimpleInfo;
        public GuildSimplestInfo m_playerGuildInfo;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

