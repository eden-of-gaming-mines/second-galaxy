﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBStaticPlayerShip : ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        int GetHangarIndex();
        ulong GetInstanceId();
        ILBPlayerContext GetLBPlayerContext();
        LogicBlockShipCompWeaponAmmoSetup4PlayerShip GetLBWeaponAmmoSetup();
        LogicBlockShipCompWeaponEquipSetupBase GetLBWeaponEquipSetup();
        LogicBlockShipCompWeaponEquipSetupEnv4PlayerShip GetLBWeaponEquipSetupEnv();
    }
}

