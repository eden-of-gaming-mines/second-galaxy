﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildCompPropertiesCalculaterBase : IGuildPropertiesCalculaterBase
    {
        void AddGuildBuildingConfigProvider(int solarSystemId, GuildBuildingInfo info);
        void AddSolarSystemPropertiesCalc(int solarSystemId);
        void RemoveGuildBuildingConfigProvider(int solarSystemId, GuildBuildingInfo info);
        void RemoveSolarSystemPropertiesCalc(int solarSystemId);
        void UpdateGuildBuildingConfigProvider(int solarSystemId, GuildBuildingInfo info);
    }
}

