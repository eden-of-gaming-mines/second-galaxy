﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockFactionCreditQuestBase
    {
        protected ICharacterFactionDataContainer m_dc;
        protected LogicBlockQuestBase m_lbQuest;
        protected LogicBlockCharacterFactionBase m_lbFaction;
        protected ILBPlayerContext m_lbPlayerCtx;
        protected int m_currentTalkingNpcId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnQuestComplete;
        private static DelegateBridge __Hotfix_OnQuestCancel;
        private static DelegateBridge __Hotfix_GetProcessingCreditQuest;
        private static DelegateBridge __Hotfix_GetFactionCreditQuestCompleteCount;
        private static DelegateBridge __Hotfix_HasFactionCreditQuestAcceptableTimes;
        private static DelegateBridge __Hotfix_FactionCreditQuestCancelable;
        private static DelegateBridge __Hotfix_CheckFactionCreditQuestAvailable;
        private static DelegateBridge __Hotfix_IncFactionCreditQuestCompleteCount;
        private static DelegateBridge __Hotfix_IsFactionCreditQuestWeeklyRewardGet;
        private static DelegateBridge __Hotfix_GetNpcQuestLevel;
        private static DelegateBridge __Hotfix_IsFactionCreditWeeklyRewardAvailable;
        private static DelegateBridge __Hotfix_SetCurrentTalkingNpcId;
        private static DelegateBridge __Hotfix_GetCurrentTalkingNpcId;
        private static DelegateBridge __Hotfix_GetFactionCreditNpcConfByNpcId;

        [MethodImpl(0x8000)]
        public bool CheckFactionCreditQuestAvailable(int npcId, int questId, int playerLevel, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool FactionCreditQuestCancelable()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrentTalkingNpcId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ConfigDataFactionCreditNpcInfo GetFactionCreditNpcConfByNpcId(int npcId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFactionCreditQuestCompleteCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcQuestLevel(int npcId, int playerLevel)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase GetProcessingCreditQuest()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasFactionCreditQuestAcceptableTimes()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void IncFactionCreditQuestCompleteCount(int nCount)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFactionCreditQuestWeeklyRewardGet(int id = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void IsFactionCreditWeeklyRewardAvailable(int id, out int errCode, out ConfigDataFactionCreditQuestWeeklyRewardInfo conf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCancel(LBProcessingQuestBase lbquest)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestComplete(LBProcessingQuestBase lbquest)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrentTalkingNpcId(int npcId)
        {
        }
    }
}

