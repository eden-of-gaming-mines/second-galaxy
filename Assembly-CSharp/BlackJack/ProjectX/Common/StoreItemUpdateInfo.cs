﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct StoreItemUpdateInfo
    {
        public StoreItemInfo m_item;
        public long m_changeValue;
        public StoreItemInfo m_freezingItem;
        public long m_freezingItemChangeValue;
        public StoreItemInfo m_bindItem;
        public long m_bindItemChangeValue;
        [MethodImpl(0x8000)]
        public StoreItemUpdateInfo(StoreItemInfo itemInfo, long changeValue)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEmptyInfo()
        {
        }
    }
}

