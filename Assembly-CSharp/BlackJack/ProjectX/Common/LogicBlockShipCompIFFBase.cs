﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ToolUtil;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LogicBlockShipCompIFFBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget, IFFState> EventOnTargetEnterIFF;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget, IFFState> EventOnTargetDead;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventOnNeutralTargetChangeToEnemy;
        public DelegateEventEx<ILBSpaceTarget> EventOnNpcFactionInfoChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, float> EventOnFactionCreditModify;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnTeamMemberAdd;
        protected ILBInSpaceShip m_ownerShip;
        protected ILogicBlockShipCompSpaceObject m_lbSpaceObject;
        protected Dictionary<int, float> m_factionCreditMap;
        protected HashSet<uint> m_friendlyObjIdList;
        protected uint m_friendlyListLastUpdateTime;
        protected HashSet<uint> m_neutralObjIdList;
        protected uint m_neutralListLastUpdateTime;
        protected HashSet<uint> m_enemyObjIdList;
        protected uint m_enemyListLastUpdateTime;
        protected uint m_lastEnemyListSortTickSeq;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventEnemyAdd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventEnemyRemove;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventEnemyClear;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetParentFactionId_1;
        private static DelegateBridge __Hotfix_GetIFFStateForTarget;
        private static DelegateBridge __Hotfix_SetTarget2Enemy;
        private static DelegateBridge __Hotfix_GetFriendList;
        private static DelegateBridge __Hotfix_GetNearestFriend;
        private static DelegateBridge __Hotfix_GetFriendListLastUpdateTime;
        private static DelegateBridge __Hotfix_GetNeutralList;
        private static DelegateBridge __Hotfix_GetNearestAttackableNeutral;
        private static DelegateBridge __Hotfix_GetNeutralListLastUpdateTime;
        private static DelegateBridge __Hotfix_GetEnemyList;
        private static DelegateBridge __Hotfix_GetEnemyListLastUpdateTime;
        private static DelegateBridge __Hotfix_HasEnemyInView;
        private static DelegateBridge __Hotfix_AddTarget2IFF;
        private static DelegateBridge __Hotfix_RemoveTarget4IFF;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal;
        private static DelegateBridge __Hotfix_OnTeamMemberAdd;
        private static DelegateBridge __Hotfix_OnTeamMemberLeave;
        private static DelegateBridge __Hotfix_OnHostileBehavior;
        private static DelegateBridge __Hotfix_OnSelfFactionInfoChanged;
        private static DelegateBridge __Hotfix_OnSelfShipGuildInfoUpdate;
        private static DelegateBridge __Hotfix_OnTargetEnterView;
        private static DelegateBridge __Hotfix_OnTargetLeaveView;
        private static DelegateBridge __Hotfix_OnTargetDead;
        private static DelegateBridge __Hotfix_OnTargetFactionInfoChanged;
        private static DelegateBridge __Hotfix_OnTargetShipGuildInfoUpdate;
        private static DelegateBridge __Hotfix_FireEventOnNpcFactionInfoChanged;
        private static DelegateBridge __Hotfix_FireEventOnNeutralTargetChangeToEnemy;
        private static DelegateBridge __Hotfix_FireEventOnFactionCreditModify;
        private static DelegateBridge __Hotfix_add_EventOnTargetEnterIFF;
        private static DelegateBridge __Hotfix_remove_EventOnTargetEnterIFF;
        private static DelegateBridge __Hotfix_add_EventOnTargetDead;
        private static DelegateBridge __Hotfix_remove_EventOnTargetDead;
        private static DelegateBridge __Hotfix_add_EventOnNeutralTargetChangeToEnemy;
        private static DelegateBridge __Hotfix_remove_EventOnNeutralTargetChangeToEnemy;
        private static DelegateBridge __Hotfix_add_EventOnFactionCreditModify;
        private static DelegateBridge __Hotfix_remove_EventOnFactionCreditModify;
        private static DelegateBridge __Hotfix_add_EventOnTeamMemberAdd;
        private static DelegateBridge __Hotfix_remove_EventOnTeamMemberAdd;
        private static DelegateBridge __Hotfix_GetFactionCreditLevel;
        private static DelegateBridge __Hotfix_RefreshAllTargetIFFState;
        private static DelegateBridge __Hotfix_PrepareRemoveFromSolarSystem;
        private static DelegateBridge __Hotfix_add_EventEnemyAdd;
        private static DelegateBridge __Hotfix_remove_EventEnemyAdd;
        private static DelegateBridge __Hotfix_add_EventEnemyRemove;
        private static DelegateBridge __Hotfix_remove_EventEnemyRemove;
        private static DelegateBridge __Hotfix_add_EventEnemyClear;
        private static DelegateBridge __Hotfix_remove_EventEnemyClear;

        public event Action<ILBSpaceTarget> EventEnemyAdd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventEnemyClear
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventEnemyRemove
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, float> EventOnFactionCreditModify
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnNeutralTargetChangeToEnemy
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget, IFFState> EventOnTargetDead
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget, IFFState> EventOnTargetEnterIFF
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnTeamMemberAdd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompIFFBase()
        {
        }

        [MethodImpl(0x8000)]
        protected void AddTarget2IFF(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnFactionCreditModify(int factionId, float addCreditValue)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnNeutralTargetChangeToEnemy(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnNpcFactionInfoChanged()
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<uint> GetEnemyList()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetEnemyListLastUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        protected FactionCreditLevel GetFactionCreditLevel(int factionId)
        {
        }

        public abstract int GetFactionId();
        [MethodImpl(0x8000)]
        public HashSet<uint> GetFriendList()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetFriendListLastUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        public virtual IFFState GetIFFStateForTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual IFFState GetIFFStateForTargetInternal(ILBSpaceTarget target)
        {
        }

        protected abstract IFFState GetIFFStateForTargetInternal4HiredCaptain(ILBInSpaceNpcShip ship);
        public abstract IFFState GetIFFStateForTargetInternal4NpcShip(ILBInSpaceNpcShip ship);
        protected abstract IFFState GetIFFStateForTargetInternal4Player(ILBInSpacePlayerShip ship);
        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetNearestAttackableNeutral(double inRadius = 1.7976931348623157E+308)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetNearestFriend(double inRadius = 1.7976931348623157E+308)
        {
        }

        [MethodImpl(0x8000)]
        public HashSet<uint> GetNeutralList()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetNeutralListLastUpdateTime()
        {
        }

        public abstract int GetParentFactionId();
        [MethodImpl(0x8000)]
        protected int GetParentFactionId(int npcFactionId)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasEnemyInView()
        {
        }

        protected abstract void InitFactionCredit();
        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip, Dictionary<int, float> factionCreditMap)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnHostileBehavior(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSelfFactionInfoChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfShipGuildInfoUpdate(ILBSpaceTarget target, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetDead(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTargetEnterView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetFactionInfoChanged(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTargetLeaveView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetShipGuildInfoUpdate(ILBSpaceTarget target, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnTeamMemberAdd(ILBSpaceTarget member)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnTeamMemberLeave(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void PrepareRemoveFromSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        protected void RefreshAllTargetIFFState()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveTarget4IFF(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetTarget2Enemy(ILBSpaceTarget target, IFFState oldState)
        {
        }
    }
}

