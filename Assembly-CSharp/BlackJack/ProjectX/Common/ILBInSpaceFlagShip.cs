﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBInSpaceFlagShip : ILBInSpacePlayerShip, ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        LogicBlockFlagShipCompInSpaceBasicInfoBase GetLBFlagShipBasicInfo();
        LogicBlockFlagShipCompTeleportBase GetLBFlagShipTeleport();
    }
}

