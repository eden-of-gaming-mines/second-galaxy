﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompFormationMaker : IAIMovementFormatMaker
    {
        protected ILBInSpaceShip m_ownerShip;
        protected FormationType m_formationType;
        protected LBFormationControllerBase m_formationController;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_SetFormationType;
        private static DelegateBridge __Hotfix_DismissFormation;
        private static DelegateBridge __Hotfix_GetFormationType;
        private static DelegateBridge __Hotfix_JoinFormation;
        private static DelegateBridge __Hotfix_LeaveFormation;
        private static DelegateBridge __Hotfix_GetFormationPosForMember;
        private static DelegateBridge __Hotfix_GetFormationMakerLeaderObjId;

        [MethodImpl(0x8000)]
        public void DismissFormation()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetFormationMakerLeaderObjId()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetFormationPosForMember(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        public FormationType GetFormationType()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D JoinFormation(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        public bool LeaveFormation(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFormationType(FormationType formationType, FormationCreatingInfo info)
        {
        }
    }
}

