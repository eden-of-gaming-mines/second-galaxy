﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderByOverloadSpeed : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        protected float m_shipOverSpeedMin;
        protected float m_shipOverSpeedMax;
        protected float m_factor4MinOverSpeed;
        protected float m_factor4MaxOverSpeed;
        protected float m_factor4LessMinOverSpeed;
        protected float m_factor4MoreMaxOverSpeed;
        protected bool m_isSpeedOverloadState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnOverSpeed;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnSelfVelocity;
        private static DelegateBridge __Hotfix_IsVeiocityOverload;
        private static DelegateBridge __Hotfix_GetDefaultMaxSpeed;
        private static DelegateBridge __Hotfix_get_ShipOverSpeedMin;
        private static DelegateBridge __Hotfix_get_ShipOverSpeedMax;
        private static DelegateBridge __Hotfix_get_Factor4MinOverSpeed;
        private static DelegateBridge __Hotfix_get_Factor4MaxOverSpeed;
        private static DelegateBridge __Hotfix_get_Factor4LessMinOverSpeed;
        private static DelegateBridge __Hotfix_get_Factor4MoreMaxOverSpeed;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderByOverloadSpeed(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnSelfVelocity(ref float propertyValue)
        {
        }

        [MethodImpl(0x8000)]
        protected float GetDefaultMaxSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnOverSpeed(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsVeiocityOverload()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        public float ShipOverSpeedMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ShipOverSpeedMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4MinOverSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4MaxOverSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4LessMinOverSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4MoreMaxOverSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

