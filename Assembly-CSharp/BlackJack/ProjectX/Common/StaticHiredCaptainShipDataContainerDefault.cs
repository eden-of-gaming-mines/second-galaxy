﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StaticHiredCaptainShipDataContainerDefault : NpcShipDataContainerBase, IStaticHiredCaptainShipDataContainer, INpcShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        private bool m_needRepair;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedRepire;
        private static DelegateBridge __Hotfix_CreateNpcSpaceShipInstanceInfoByNpcCaptainShipId;

        [MethodImpl(0x8000)]
        public StaticHiredCaptainShipDataContainerDefault(int npcCaptainShipId, bool needRepair)
        {
        }

        [MethodImpl(0x8000)]
        private SolarSystemNpcShipInstanceInfo CreateNpcSpaceShipInstanceInfoByNpcCaptainShipId(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedRepire()
        {
        }
    }
}

