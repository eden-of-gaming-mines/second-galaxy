﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class UserSettingInServer
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <AutoFightNotControlPlayerShipMoveState>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AutoFightNotControlPlayerShipMoveState;
        private static DelegateBridge __Hotfix_set_AutoFightNotControlPlayerShipMoveState;

        public bool AutoFightNotControlPlayerShipMoveState
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

