﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSpaceProcessEquipLaunchBase : LBSpaceProcessWeaponEquipLaunchBase
    {
        protected ShipEquipSlotType m_slotType;
        protected EquipType m_equipType;
        protected ushort m_chargeTime;
        protected bool m_chargeStarted;
        protected uint? m_chargeEndTimeAbs;
        protected bool m_isFired;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_GetEquipSlotType;
        private static DelegateBridge __Hotfix_GetEquipType;
        private static DelegateBridge __Hotfix_SetChargeTime;
        private static DelegateBridge __Hotfix_GetChargeTime;
        private static DelegateBridge __Hotfix_IsCharging;
        private static DelegateBridge __Hotfix_Tick;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipLaunchBase(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEquipSlotType GetEquipSlotType()
        {
        }

        [MethodImpl(0x8000)]
        public EquipType GetEquipType()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, LBSpaceProcessType processType, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCharging()
        {
        }

        protected abstract void OnEquipFire(uint currTime);
        [MethodImpl(0x8000)]
        public virtual void SetChargeTime(ushort chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

