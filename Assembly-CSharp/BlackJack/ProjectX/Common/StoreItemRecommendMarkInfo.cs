﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class StoreItemRecommendMarkInfo
    {
        public StoreItemType m_itemType;
        public int m_configId;
        public bool m_isBind;
        private DateTime m_getMarkTime;
        private DateTime m_visitMarkTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsRecommendMarkVisited;
        private static DelegateBridge __Hotfix_SetGetMark;
        private static DelegateBridge __Hotfix_SetMarkVisited;

        [MethodImpl(0x8000)]
        public bool IsRecommendMarkVisited()
        {
        }

        [MethodImpl(0x8000)]
        public void SetGetMark()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMarkVisited(bool isVisit = true)
        {
        }
    }
}

