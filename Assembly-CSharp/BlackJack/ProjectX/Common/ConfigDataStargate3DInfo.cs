﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataStargate3DInfo")]
    public class ConfigDataStargate3DInfo : IExtensible
    {
        private int _ID;
        private PVector3D _LeaveStargateDir;
        private PVector3D _leaveStargateAreaCenterPoint;
        private float _leaveStargateAreaRadius;
        private CollisionInfo _collision;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_LeaveStargateDir;
        private static DelegateBridge __Hotfix_set_LeaveStargateDir;
        private static DelegateBridge __Hotfix_get_LeaveStargateAreaCenterPoint;
        private static DelegateBridge __Hotfix_set_LeaveStargateAreaCenterPoint;
        private static DelegateBridge __Hotfix_get_LeaveStargateAreaRadius;
        private static DelegateBridge __Hotfix_set_LeaveStargateAreaRadius;
        private static DelegateBridge __Hotfix_get_Collision;
        private static DelegateBridge __Hotfix_set_Collision;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="LeaveStargateDir", DataFormat=DataFormat.Default)]
        public PVector3D LeaveStargateDir
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="leaveStargateAreaCenterPoint", DataFormat=DataFormat.Default)]
        public PVector3D LeaveStargateAreaCenterPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="leaveStargateAreaRadius", DataFormat=DataFormat.FixedSize)]
        public float LeaveStargateAreaRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="collision", DataFormat=DataFormat.Default)]
        public CollisionInfo Collision
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

