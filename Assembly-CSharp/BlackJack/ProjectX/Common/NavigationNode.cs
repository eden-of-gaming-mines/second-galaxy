﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class NavigationNode
    {
        public NavigationNodeType m_nodeType;
        public int m_solarSystemId;
        public int m_gdbConfId;
        public ulong m_pvpSignalInsId;
        public int m_questInsId;
        public string m_playerGameUserId;
        public uint m_sceneInstanceId;
        public ulong m_guildActionInsId;
        public string m_guildMemberGameUserId;
        public ulong m_spaceSignalInsId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

