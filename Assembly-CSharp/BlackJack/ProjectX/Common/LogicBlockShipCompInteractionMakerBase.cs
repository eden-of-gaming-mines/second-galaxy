﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompInteractionMakerBase
    {
        protected ILBInSpacePlayerShip m_ownerShip;
        protected uint m_interactionTargetId;
        protected ConfigDataNpcInteractionTemplate m_interactionTemplateConfInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_SetInteractionTargetId;
        private static DelegateBridge __Hotfix_GetInteractionTargetId;
        private static DelegateBridge __Hotfix_IsInteractionAvailable;
        private static DelegateBridge __Hotfix_CheckCondForInteractionStart;
        private static DelegateBridge __Hotfix_InitInteractionWithTemplate;
        private static DelegateBridge __Hotfix_ClearInteractionTemplate;
        private static DelegateBridge __Hotfix_CheckInteractionOnceEffect;
        private static DelegateBridge __Hotfix_ApplyInteractionOnceEffect;
        private static DelegateBridge __Hotfix_TickInteractionProcess;
        private static DelegateBridge __Hotfix_GetInteractionInfluenceInfo;

        [MethodImpl(0x8000)]
        private void ApplyInteractionOnceEffect(ConfigDataNpcInteractionTemplate template)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCondForInteractionStart(uint npcObjId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckInteractionOnceEffect(ConfigDataNpcInteractionTemplate template, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearInteractionTemplate()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetInteractionInfluenceInfo(out bool isShieldAffected, out bool isArmorAffected, out bool isEnergyAffected)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetInteractionTargetId()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitInteractionWithTemplate(ConfigDataNpcInteractionTemplate template)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsInteractionAvailable(ILBInSpaceShip target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetInteractionTargetId(uint targetId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool TickInteractionProcess(uint deltaTime, out int errCode)
        {
        }
    }
}

