﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessEquipTransformToTeleportTunnelLaunch : LBSpaceProcessEquipLaunchWithDurationBase
    {
        protected List<int> m_attach2SelfBufList;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetAttachBufList;
        private static DelegateBridge __Hotfix_GetAttach2SelfBufList;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipTransformToTeleportTunnelLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipTransformToTeleportTunnelLaunch(uint startTime, uint instanceId, uint duration, ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int groupIndex, EquipType equipType, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetAttach2SelfBufList()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttachBufList(List<int> bufList)
        {
        }
    }
}

