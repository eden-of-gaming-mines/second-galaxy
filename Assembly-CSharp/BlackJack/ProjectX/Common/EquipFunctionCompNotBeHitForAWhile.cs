﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class EquipFunctionCompNotBeHitForAWhile : EquipFunctionCompTrigerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNotBeHitForAWhile;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnNotBeHitStateBroken;
        private readonly uint m_timeLengthMs;
        private uint m_notBeHitStartTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetNotBeHitEffectTriggerTime;
        private static DelegateBridge __Hotfix_OnHitByTarget;
        private static DelegateBridge __Hotfix_OnBulletHitMiss;
        private static DelegateBridge __Hotfix_add_EventOnNotBeHitForAWhile;
        private static DelegateBridge __Hotfix_remove_EventOnNotBeHitForAWhile;
        private static DelegateBridge __Hotfix_add_EventOnNotBeHitStateBroken;
        private static DelegateBridge __Hotfix_remove_EventOnNotBeHitStateBroken;

        public event Action EventOnNotBeHitForAWhile
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnNotBeHitStateBroken
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public EquipFunctionCompNotBeHitForAWhile(IEquipFunctionCompOwner owner, float timeLengthSec)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetNotBeHitEffectTriggerTime()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBulletHitMiss(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHitByTarget(ILBSpaceTarget srcTarget, bool isCritical, LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }
    }
}

