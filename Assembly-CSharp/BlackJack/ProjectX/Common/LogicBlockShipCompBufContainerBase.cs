﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;

    [Serializable]
    public abstract class LogicBlockShipCompBufContainerBase : IPropertiesProvider
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBBufBase> EventOnAttachBuf;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnAttachDebufByTarget;
        protected ILBInSpaceShip m_ownerShip;
        protected IShipDataContainer m_shipDC;
        protected List<int> m_initStaticBufList;
        protected List<object> m_initDynamicBufList;
        protected LogicBlockShipCompFireControllBase m_lbFireControll;
        protected LogicBlockShipCompSceneBase m_lbScene;
        protected LBBuffContainer m_bufContainer;
        protected List<List<LBShipFightBuf>> m_shipFightBufList;
        protected List<LBBufBase> m_shipStaticBufList;
        public const int ShipFightBufTypeCount = 4;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetOwnerShip;
        private static DelegateBridge __Hotfix_GetFirstBufByBufType;
        private static DelegateBridge __Hotfix_GetFirstBufByBufConfId;
        private static DelegateBridge __Hotfix_CheckFightBufById;
        private static DelegateBridge __Hotfix_CheckBufById;
        private static DelegateBridge __Hotfix_GetFirstBufByConfIdCreateTimeLifeTime;
        private static DelegateBridge __Hotfix_AttachBufDirectly;
        private static DelegateBridge __Hotfix_DetachBufDirectly;
        private static DelegateBridge __Hotfix_GetFightBufList;
        private static DelegateBridge __Hotfix_GetIndexByFightBufType;
        private static DelegateBridge __Hotfix_GetAllFightBufList;
        private static DelegateBridge __Hotfix_GetFightBufCountBySourceEquipType;
        private static DelegateBridge __Hotfix_GetAllStaticBufList;
        private static DelegateBridge __Hotfix_GetAllNeedSyncBufList;
        private static DelegateBridge __Hotfix_GetFirstFightBufByType;
        private static DelegateBridge __Hotfix_GetFirstBuffByGamePlayFlag;
        private static DelegateBridge __Hotfix_GetBufByInstanceId;
        private static DelegateBridge __Hotfix_GetBufByGroupId;
        private static DelegateBridge __Hotfix_TestGamePlayFlag;
        private static DelegateBridge __Hotfix_GetShieldExValueByConfigId;
        private static DelegateBridge __Hotfix_GetAllShieldExValue;
        private static DelegateBridge __Hotfix_GetSheildExMaxValue;
        private static DelegateBridge __Hotfix_IsJumpingDisturbBuf;
        private static DelegateBridge __Hotfix_FireEventOnAttachDebufByTarget;
        private static DelegateBridge __Hotfix_FireEventOnAttachBuf;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_add_EventOnAttachBuf;
        private static DelegateBridge __Hotfix_remove_EventOnAttachBuf;
        private static DelegateBridge __Hotfix_add_EventOnAttachDebufByTarget;
        private static DelegateBridge __Hotfix_remove_EventOnAttachDebufByTarget;

        public event Action<LBBufBase> EventOnAttachBuf
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnAttachDebufByTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompBufContainerBase()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void AttachBufDirectly(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckBufById(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckFightBufById(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void DetachBufDirectly(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void FireEventOnAttachBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnAttachDebufByTarget(ILBSpaceTarget bufSrc)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable<LBShipFightBuf> GetAllFightBufList()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable<LBBufBase> GetAllNeedSyncBufList()
        {
        }

        [MethodImpl(0x8000)]
        public float GetAllShieldExValue()
        {
        }

        [MethodImpl(0x8000)]
        public IEnumerable<LBBufBase> GetAllStaticBufList()
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetBufByGroupId(int bufGroupId)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetBufByInstanceId(ulong instanceId)
        {
        }

        protected abstract ILBBufOwner GetBufOwner();
        [MethodImpl(0x8000)]
        public int GetFightBufCountBySourceEquipType(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBShipFightBuf> GetFightBufList(ShipFightBufType fightBufType, bool isEnhance)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetFirstBufByBufConfId(int confId)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetFirstBufByBufType(BufType bufType)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetFirstBufByConfIdCreateTimeLifeTime(int bufConfId, uint currTime, uint bufLifeTime)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetFirstBuffByGamePlayFlag(BufGamePlayFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipFightBuf GetFirstFightBufByType(ShipFightBufType fightBufType, bool isEnhance, BufType bufType)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetIndexByFightBufType(ShipFightBufType fightBufType, bool isEnhance)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpaceShip GetOwnerShip()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSheildExMaxValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShieldExValueByConfigId(int bufConfId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip, List<int> staticBufList = null, List<object> dynamicBufList = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsJumpingDisturbBuf(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public bool TestGamePlayFlag(BufGamePlayFlag flag)
        {
        }

        [CompilerGenerated]
        private sealed class <GetAllFightBufList>c__Iterator0 : IEnumerable, IEnumerable<LBShipFightBuf>, IEnumerator, IDisposable, IEnumerator<LBShipFightBuf>
        {
            internal List<LBBufBase>.Enumerator $locvar0;
            internal LBBufBase <it>__1;
            internal LogicBlockShipCompBufContainerBase $this;
            internal LBShipFightBuf $current;
            internal bool $disposing;
            internal int $PC;

            [MethodImpl(0x8000), DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [MethodImpl(0x8000), DebuggerHidden]
            IEnumerator<LBShipFightBuf> IEnumerable<LBShipFightBuf>.GetEnumerator()
            {
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<BlackJack.ProjectX.Common.LBShipFightBuf>.GetEnumerator();

            LBShipFightBuf IEnumerator<LBShipFightBuf>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <GetAllNeedSyncBufList>c__Iterator1 : IEnumerable, IEnumerable<LBBufBase>, IEnumerator, IDisposable, IEnumerator<LBBufBase>
        {
            internal List<LBBufBase>.Enumerator $locvar0;
            internal LBBufBase <it>__1;
            internal LogicBlockShipCompBufContainerBase $this;
            internal LBBufBase $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                uint num = (uint) this.$PC;
                this.$disposing = true;
                this.$PC = -1;
                switch (num)
                {
                    case 1:
                        try
                        {
                        }
                        finally
                        {
                            this.$locvar0.Dispose();
                        }
                        break;

                    default:
                        break;
                }
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                bool flag = false;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = this.$this.m_bufContainer.GetBufList().GetEnumerator();
                        num = 0xfffffffd;
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                try
                {
                    switch (num)
                    {
                        default:
                            while (true)
                            {
                                if (!this.$locvar0.MoveNext())
                                {
                                    break;
                                }
                                this.<it>__1 = this.$locvar0.Current;
                                if (this.<it>__1.IsNeedSyncInSpace() && !this.<it>__1.IsStatic())
                                {
                                    this.$current = this.<it>__1;
                                    if (!this.$disposing)
                                    {
                                        this.$PC = 1;
                                    }
                                    flag = true;
                                    return true;
                                }
                            }
                            break;
                    }
                }
                finally
                {
                    if (!flag)
                    {
                        this.$locvar0.Dispose();
                    }
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [DebuggerHidden]
            IEnumerator<LBBufBase> IEnumerable<LBBufBase>.GetEnumerator()
            {
                if (Interlocked.CompareExchange(ref this.$PC, 0, -2) == -2)
                {
                    return this;
                }
                return new LogicBlockShipCompBufContainerBase.<GetAllNeedSyncBufList>c__Iterator1 { $this = this.$this };
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<BlackJack.ProjectX.Common.LBBufBase>.GetEnumerator();

            LBBufBase IEnumerator<LBBufBase>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

