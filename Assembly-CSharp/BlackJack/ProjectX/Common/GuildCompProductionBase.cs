﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompProductionBase : GuildCompBase, IGuildCompProductionBase, IGuildProductionBase
    {
        private IGuildCompPropertiesCalculaterBase m_compPropertiesCalc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_CheckProductionLineStart;
        private static DelegateBridge __Hotfix_CheckProductionLineCancel;
        private static DelegateBridge __Hotfix_CheckProductionLineSpeedUpByTradeMoney;
        private static DelegateBridge __Hotfix_CheckProductionLineComplete;
        private static DelegateBridge __Hotfix_CheckProduceTempleteUnlockCondition;
        private static DelegateBridge __Hotfix_CheckProduceTempleteUnlockCondition_SovereignGalaxyCount;
        private static DelegateBridge __Hotfix_CalcGuildProductionTradeMoneyRealCost;
        private static DelegateBridge __Hotfix_CalcGuidProductionItemRealCost;
        private static DelegateBridge __Hotfix_CalcGuildProductionTimeRealCost;
        private static DelegateBridge __Hotfix_GetGuildProductionLineList;
        private static DelegateBridge __Hotfix_GetGuildProductionLineById;
        private static DelegateBridge __Hotfix_AddGuildProductionLine;
        private static DelegateBridge __Hotfix_RemoveGuildProductionLine;
        private static DelegateBridge __Hotfix_UpdateGuildProductionLine;
        private static DelegateBridge __Hotfix_ClearGuildProductionLineData;
        private static DelegateBridge __Hotfix_UpdateProductionLineSpeedUpTime;
        private static DelegateBridge __Hotfix_CheckGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_GetGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_CalcProductionCost;
        private static DelegateBridge __Hotfix_CalcGuildProductionCostModifyFactor;
        private static DelegateBridge __Hotfix_CalcGuildProductionSpeedModifyFactor;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompProductionBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddGuildProductionLine(GuildProductionLineInfo lineInfo)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> CalcGuidProductionItemRealCost(int solarSystemId, GuildProduceCategory category, List<CostInfo> configItemCostList, int count)
        {
        }

        [MethodImpl(0x8000)]
        private float CalcGuildProductionCostModifyFactor(int solarSystemId, GuildProduceCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private float CalcGuildProductionSpeedModifyFactor(int solarSystemId, GuildProduceCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcGuildProductionTimeRealCost(int solarSystemId, GuildProduceCategory category, int configTime, int count)
        {
        }

        [MethodImpl(0x8000)]
        public ulong CalcGuildProductionTradeMoneyRealCost(int solarSystemId, GuildProduceCategory category, ulong configMoneyCost, int count)
        {
        }

        [MethodImpl(0x8000)]
        private List<CostInfo> CalcProductionCost(int solarSystemId, GuildProduceCategory category, List<CostInfo> configCostList, int configTimeCost, int count, out ulong tradeMoneyCost, out int realTimeCost)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildProductionDataVersion(uint version)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProduceTempleteUnlockCondition(ConfigDataGuildProduceTempleteInfo confInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckProduceTempleteUnlockCondition_SovereignGalaxyCount(ConfigDataGuildProduceTempleteInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineCancel(ulong lineId, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineComplete(ulong lineId, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineSpeedUpByTradeMoney(ulong lineId, int speedUpLevel, ulong currTradeMoney, DateTime currTime, out int errCode, out int costTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineStart(ulong lineId, int templeteId, int produceCount, out int errCode, out ulong moneyCost, out int timeCost, out List<CostInfo> costList)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearGuildProductionLineData(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildProductionDataVersion()
        {
        }

        [MethodImpl(0x8000)]
        public GuildProductionLineInfo GetGuildProductionLineById(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildProductionLineInfo> GetGuildProductionLineList()
        {
        }

        [MethodImpl(0x8000)]
        public override bool PostInitlialize()
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveGuildProductionLine(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildProductionLine(GuildProductionLineInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProductionLineSpeedUpTime(ulong lineId, int reduceTime)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

