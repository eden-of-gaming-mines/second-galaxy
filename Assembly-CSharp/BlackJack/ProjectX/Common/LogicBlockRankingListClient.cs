﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockRankingListClient : LogicBlockRankingListBase
    {
        protected Dictionary<int, CachedRankingListInfo> m_cachedRankingListDict;
        protected Dictionary<int, CachedFirstPlayerRankingInfo> m_cachedRankingListFirstPlayerInfoDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddRankingListInfoToCache;
        private static DelegateBridge __Hotfix_AddFirstPlayerRankingInfoToCache;
        private static DelegateBridge __Hotfix_GetRankingListInfoByType;
        private static DelegateBridge __Hotfix_GetFirstPlayerRankingInfoByType;

        [MethodImpl(0x8000)]
        public void AddFirstPlayerRankingInfoToCache(RankingListType rankingType, RankingTargetInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void AddRankingListInfoToCache(RankingListType rankingType, int queryTopNum, RankingListInfo rankingListInfo)
        {
        }

        [MethodImpl(0x8000)]
        public RankingTargetInfo GetFirstPlayerRankingInfoByType(RankingListType rankingListType)
        {
        }

        [MethodImpl(0x8000)]
        public RankingListInfo GetRankingListInfoByType(RankingListType rankingType)
        {
        }

        public class CachedFirstPlayerRankingInfo
        {
            public RankingTargetInfo m_playerInfo;
            private static DelegateBridge _c__Hotfix_ctor;

            public CachedFirstPlayerRankingInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class CachedRankingListInfo
        {
            public RankingListInfo m_rankingListInfo;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

