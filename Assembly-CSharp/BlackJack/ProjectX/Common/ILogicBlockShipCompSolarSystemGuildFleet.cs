﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ProjectX.SimSolarSystemNs;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface ILogicBlockShipCompSolarSystemGuildFleet
    {
        bool CheckGuildFleetMemberSettingFlag(GuildFleetPersonalSetting flag);
        uint GetFireFocusTargetId();
        uint GetFormationVersion();
        ulong GetGuildFleetId();
        bool IsPlayerShipBelowToGuildFleet(string gameUserId);
        bool IsShipAIAutoLockTargetDisableByGuildFleet();
        void OnOtherPlayerEnterFleet(ILBSpaceTarget target);
        void OnOtherPlayerLeaveFleet(ILBSpaceTarget target);
        bool StartFleetMembersJumping(ulong fleetId, SimSpaceShip.MoveCmd moveCmd, out List<string> memberList, out int errCode);
        bool StartFleetMembersUseStargate(ulong fleetId, SimSpaceShip.MoveCmd moveCmd, out List<string> memberList, out int errCode);
        void UpdateSelfGuildFleet(ulong fleetId);
    }
}

