﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildAntiTeleportBase
    {
        bool CheckForActivateAntiTeleportEffect(string gameUserId, ulong buildingInsId, out int errCode);
        bool CheckForAntiTeleportInfoReq(string gameUserId, out int errCode);
        List<GuildAntiTeleportEffectInfo> GetAllGuildAntiTeleportEffectList();
        GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectById(ulong instanceId);
        GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectBySolarSystemId(int destSolarSystemId);
        bool IsAntiTeleportEffectActivate(DateTime currTime, int destSolarSystemId);
    }
}

