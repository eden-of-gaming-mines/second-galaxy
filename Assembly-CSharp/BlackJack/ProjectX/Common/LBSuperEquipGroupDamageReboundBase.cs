﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperEquipGroupDamageReboundBase : LBSuperEquipGroupBase
    {
        protected bool m_isReboundShieldOpen;
        protected uint m_reboundShieldOpenTime;
        protected float m_reboundDamage;
        protected float m_damageTransformPercent;
        protected float m_reboundDamageMax;
        protected ushort m_aoeChargeTime;
        protected ushort m_shieldDurationTime;
        private const string m_damageTransformPercentParamName = "DamageTransformPercent";
        private const string m_reboundDamageMaxParamName = "ReboundDamageMax";
        private const string m_aoeChargeTimeParamName = "AoeChargeTime";
        private const string m_shieldDurationTimeParamName = "ShieldDurationTime";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnHitByTarget;
        private static DelegateBridge __Hotfix_OnJumpingStart;
        private static DelegateBridge __Hotfix_InitConfigParams;
        private static DelegateBridge __Hotfix_GetReboundShieldBufId;
        private static DelegateBridge __Hotfix_GetReboundDamageMax;
        private static DelegateBridge __Hotfix_AttachBuf4ShieldOpen;
        private static DelegateBridge __Hotfix_ClearDamageReboundData;

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupDamageReboundBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void AttachBuf4ShieldOpen(LBSpaceProcessEquipAttachBufLaunch launchProcess)
        {
        }

        [MethodImpl(0x8000)]
        public override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearDamageReboundData()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetReboundDamageMax()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetReboundShieldBufId()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitConfigParams()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHitByTarget(ILBSpaceTarget target, bool isCritical, LBBulletDamageInfo damageInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnJumpingStart()
        {
        }

        protected abstract void OnReboundShieldTimeOut(uint currTime);
        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

