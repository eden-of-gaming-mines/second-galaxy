﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperWeaponEquipGroupBase : LBInSpaceWeaponEquipGroupBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnSuperWeaponEquipLaunchable;
        protected float m_currEnergy;
        protected TickExecutor m_tickExecutorForEnergyRecover;
        protected bool m_isEnergyAddToFull;
        protected bool m_launchableOnPrevTick;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsOrdinaryWeapon;
        private static DelegateBridge __Hotfix_IsOrdinaryEquip;
        private static DelegateBridge __Hotfix_OnJumpingEnd;
        private static DelegateBridge __Hotfix_SetSuperWeaponEquipSystemActive;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_PostInitialize4InitSuperEnergy;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_OnSuperWeaponEquipLaunchable;
        private static DelegateBridge __Hotfix_GetAllAttackableEnemyOrNeutral;
        private static DelegateBridge __Hotfix_AddEnergy;
        private static DelegateBridge __Hotfix_ClearEnergy;
        private static DelegateBridge __Hotfix_GetCurrentEnergy;
        private static DelegateBridge __Hotfix_SetEnergy;
        private static DelegateBridge __Hotfix_IsEnergyFull;
        private static DelegateBridge __Hotfix_IsWorking;
        private static DelegateBridge __Hotfix_GetSuperWeaponEquipLaunchEnergyCost;
        private static DelegateBridge __Hotfix_IsSuperWeaponEquipAttackable;
        private static DelegateBridge __Hotfix_OnEnergyAddToFull;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickForEnergyRecover;
        private static DelegateBridge __Hotfix_IsAllowToAutoRecoverEnergy;
        private static DelegateBridge __Hotfix_TickForLaunchable;
        private static DelegateBridge __Hotfix_CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_CalcSuperWeaponEquipEnergyRecovery;
        private static DelegateBridge __Hotfix_GetEnergyRecoveryModifyPropertiesIdForGrandFactionShip;
        private static DelegateBridge __Hotfix_add_EventOnSuperWeaponEquipLaunchable;
        private static DelegateBridge __Hotfix_remove_EventOnSuperWeaponEquipLaunchable;

        public event Action<bool> EventOnSuperWeaponEquipLaunchable
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LBSuperWeaponEquipGroupBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void AddEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcSuperWeaponEquipEnergyRecovery()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ClearEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> GetAllAttackableEnemyOrNeutral()
        {
        }

        protected abstract float GetConfMwEnergyGrow();
        protected abstract float GetConfMwEnergyNeed();
        [MethodImpl(0x8000)]
        public virtual float GetCurrentEnergy()
        {
        }

        [MethodImpl(0x8000)]
        private PropertiesId GetEnergyRecoveryModifyPropertiesIdForGrandFactionShip()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSuperWeaponEquipLaunchEnergyCost()
        {
        }

        protected abstract bool HasAvailableTarget(out int errCode);
        [MethodImpl(0x8000)]
        protected virtual bool IsAllowToAutoRecoverEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsEnergyFull()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsOrdinaryEquip()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsOrdinaryWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsSuperWeaponEquipAttackable()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnEnergyAddToFull()
        {
        }

        [MethodImpl(0x8000)]
        public void OnJumpingEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnSuperWeaponEquipLaunchable(bool isLaunchable)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PostInitialize4InitSuperEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuperWeaponEquipSystemActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForEnergyRecover()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForLaunchable()
        {
        }
    }
}

