﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildSimpleRuntimeInfo
    {
        public int m_memberCount;
        public int m_onlineMemberCount;
        public int m_occupySolorSystemCount;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

