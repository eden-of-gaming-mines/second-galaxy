﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderByTargetSpeed : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        protected float m_targetShipSpeedMin;
        protected float m_targetShipSpeedMax;
        protected float m_factor4SpeedMin;
        protected float m_factor4SpeedMax;
        protected float m_factor4LessSpeedMin;
        protected float m_factor4ThanSpeedMax;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnTargetSpeed;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnTargetVelocity;
        private static DelegateBridge __Hotfix_get_TargetShipSpeedMin;
        private static DelegateBridge __Hotfix_get_TargethipSpeedMax;
        private static DelegateBridge __Hotfix_get_Factor4SpeedMin;
        private static DelegateBridge __Hotfix_get_Factor4SpeedMax;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderByTargetSpeed(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnTargetVelocity(ref float propertyValue, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnTargetSpeed(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float TargetShipSpeedMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float TargethipSpeedMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4SpeedMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4SpeedMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

