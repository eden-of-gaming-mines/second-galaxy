﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildFleetMemberDynamicInfo
    {
        public string m_gameUserId;
        public int m_currShipConfigId;
        public int m_currShipLocation;
        public bool m_isInSpace;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

