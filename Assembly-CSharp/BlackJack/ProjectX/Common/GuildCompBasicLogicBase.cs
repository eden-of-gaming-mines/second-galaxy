﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompBasicLogicBase : GuildCompBase, IGuildCompBasicLogicBase, IGuildBasicLogicBase
    {
        protected const int BasicInfoChangeLockSeconds = 2;
        protected DateTime m_guildBasicInfoLastModifyTime;
        protected DateTime m_guildManifestoLastModifyTime;
        protected DateTime m_guildAnnouncementLastModifyTime;
        protected readonly GuildSimplestInfo m_simplestInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildId;
        private static DelegateBridge __Hotfix_GetGuildName;
        private static DelegateBridge __Hotfix_GetGuildCodeName;
        private static DelegateBridge __Hotfix_GetGuildLanguageType;
        private static DelegateBridge __Hotfix_GetBasicInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildSimpleInfo;
        private static DelegateBridge __Hotfix_GetStarMapAllianceSimpleInfo;
        private static DelegateBridge __Hotfix_ModifyGuildBasicInfo;
        private static DelegateBridge __Hotfix_ModifyName;
        private static DelegateBridge __Hotfix_ModifyCodeName;
        private static DelegateBridge __Hotfix_ModifyManifesto;
        private static DelegateBridge __Hotfix_ModifyAnnouncement;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_get_CodeName;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_get_Announcement;
        private static DelegateBridge __Hotfix_get_Manifesto;
        private static DelegateBridge __Hotfix_get_Logo;
        private static DelegateBridge __Hotfix_get_BasicInfo_ReadOnly;

        [MethodImpl(0x8000)]
        public GuildCompBasicLogicBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBasicInfo GetBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public string GetGuildCodeName()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildId()
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType GetGuildLanguageType()
        {
        }

        [MethodImpl(0x8000)]
        public string GetGuildName()
        {
        }

        [MethodImpl(0x8000)]
        public StarMapAllianceSimpleInfo GetStarMapAllianceSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo GetStarMapGuildSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void ModifyAnnouncement(string newAnnouncement)
        {
        }

        [MethodImpl(0x8000)]
        protected void ModifyCodeName(string newCodeName)
        {
        }

        [MethodImpl(0x8000)]
        protected void ModifyGuildBasicInfo(GuildAllianceLanguageType newLanguageType, GuildJoinPolicy joinPolicy)
        {
        }

        [MethodImpl(0x8000)]
        protected void ModifyManifesto(string newManifesto)
        {
        }

        [MethodImpl(0x8000)]
        protected void ModifyName(string newName)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string CodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GuildAllianceLanguageType LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Announcement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Manifesto
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GuildLogoInfo Logo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected GuildBasicInfo BasicInfo_ReadOnly
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

