﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch : LBSpaceProcessEquipAttachBufLaunch
    {
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }
    }
}

