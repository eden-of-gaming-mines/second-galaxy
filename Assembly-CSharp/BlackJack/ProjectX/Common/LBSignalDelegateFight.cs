﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LBSignalDelegateFight : LBSignalDelegateBase
    {
        protected List<int> m_monsterTeamList;
        protected List<int> m_npcList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetNpcMonsterList;
        private static DelegateBridge __Hotfix_GetMonsterTeamList;
        private static DelegateBridge __Hotfix_GetExpectBindMoneyReward;

        [MethodImpl(0x8000)]
        public LBSignalDelegateFight(SignalInfo signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public int GetExpectBindMoneyReward()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetMonsterTeamList()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetNpcMonsterList()
        {
        }
    }
}

