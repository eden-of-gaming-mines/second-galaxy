﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class PlayerSimpleInfo
    {
        public string m_playerName;
        public GrandFaction m_grandFaction;
        public int m_level;
        public int m_avatarId;
        public GenderType m_gender;
        public ProfessionType m_profession;
        public string m_guildCodeName;
        public uint m_guildId;
        public string m_playerGameUserId;
        public int m_currSolarSystemId;
        public int m_currShipId;
        public bool m_isInSpace;
        public uint m_allianceId;
        public string m_allianceName;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

