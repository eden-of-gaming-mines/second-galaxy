﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBInSpacePlayerShip : ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        LogicBlockShipCompGuildAllianceTeleportBase GetLBGuildAllianceTeleport();
        LogicBlockShipCompInteractionMakerBase GetLBInteractionMaker();
        LogicBlockShipCompPickDropBoxBase GetLBPickDropBox();
        ILogicBlockShipCompPlayerInfo GetLBPlayerInfo();
        LogicBlockShipCompWingShipCtrlBase GetLBWingShipCtrl();
    }
}

