﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct WormholeGateInfo
    {
        public int m_wormholeStarGroupId;
        public List<int> m_destSolarSystemIdList;
        public DateTime m_timeStamp;
        public bool m_isRareWormhole;
    }
}

