﻿namespace BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IAllianceDCInvite
    {
        void AddInvite(AllianceInviteInfo invite);
        AllianceInviteInfo GetInvite(ulong instanceId);
        List<AllianceInviteInfo> GetInviteList();
        void Init(List<AllianceInviteInfo> inviteList);
        void RemoveInvite(ulong instanceId);
    }
}

