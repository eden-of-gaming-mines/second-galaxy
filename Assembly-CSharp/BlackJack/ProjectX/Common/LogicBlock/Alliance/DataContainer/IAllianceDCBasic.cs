﻿namespace BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using System;

    public interface IAllianceDCBasic
    {
        AllianceBasicInfo GetAllianceBasicInfo();
        void Init(AllianceBasicInfo info);
        void SetAllianceAnnouncement(string announcement);
        void SetAllianceLanguageType(GuildAllianceLanguageType languageType);
        void SetAllianceLeader(uint leaderGuildId, string leaderGuildCode, string leaderGuildName);
        void SetAllianceLogo(AllianceLogoInfo logo);
        void SetAllianceName(string name);
    }
}

