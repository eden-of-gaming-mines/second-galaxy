﻿namespace BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer
{
    using BlackJack.ProjectX.Common;
    using System;
    using System.Collections.Generic;

    public interface IAllianceDCMembers
    {
        List<AllianceMemberInfo> GetMemberList();
        void Init(List<AllianceMemberInfo> memberList);
        void RemoveMember(uint guildId, GuildAllianceLeaveReason reason);
    }
}

