﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBNpcCaptainFeats
    {
        protected ConfigDataNpcCaptainFeatsInfo m_confInfo;
        protected int m_level;
        protected ConfigDataNpcCaptainFeatsLevelInfo m_levelConf;
        protected LBPassiveSkill m_passiveSkill;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitPassiveSkill;
        private static DelegateBridge __Hotfix_GetFeatsType;
        private static DelegateBridge __Hotfix_GetFeatsId;
        private static DelegateBridge __Hotfix_IsStatic;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetLevelConf;
        private static DelegateBridge __Hotfix_GetFeatsPassiveSkillId;
        private static DelegateBridge __Hotfix_GetFeatsPassiveSkillLevel;
        private static DelegateBridge __Hotfix_GetPassiveSkill;
        private static DelegateBridge __Hotfix_GetPassiveSkillBufConfigInfo;
        private static DelegateBridge __Hotfix_GetFeatsNextLevel;
        private static DelegateBridge __Hotfix_GetNpcCaptainShipId;
        private static DelegateBridge __Hotfix_GetShipDependIndexInGrowthLine;
        private static DelegateBridge __Hotfix_GetNpcCaptainFeatsShipId;

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats(ConfigDataNpcCaptainFeatsInfo info, int level)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFeatsId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFeatsNextLevel()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFeatsPassiveSkillId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFeatsPassiveSkillLevel()
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainFeatsType GetFeatsType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainFeatsLevelInfo GetLevelConf()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetNpcCaptainFeatsShipId(ConfigDataNpcCaptainFeatsInfo conf, int level)
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcCaptainShipId()
        {
        }

        [MethodImpl(0x8000)]
        public LBPassiveSkill GetPassiveSkill()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataBufInfo GetPassiveSkillBufConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipDependIndexInGrowthLine()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPassiveSkill()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStatic()
        {
        }
    }
}

