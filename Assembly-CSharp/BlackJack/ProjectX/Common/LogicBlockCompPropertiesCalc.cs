﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockCompPropertiesCalc
    {
        protected PropertiesCalculaterBase m_propertiesCalc;
        protected IPropertiesProvider m_propertiesCalcProvider;
        protected List<IPropertiesProvider> m_providerList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_UnInitialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_RegistEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_UnregistEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_GetPropertiesCalculater;
        private static DelegateBridge __Hotfix_SetDirtyByGroupMask;
        private static DelegateBridge __Hotfix_GetReplacementPropertyIdByNoCacheOrFrameCacheGroup;
        private static DelegateBridge __Hotfix_CollectProviderList;
        private static DelegateBridge __Hotfix_HasPropertiesGroupOnPropertiesCalc;
        private static DelegateBridge __Hotfix_get_PropertiesCalc;
        private static DelegateBridge __Hotfix_get_PropertiesCalcProvider;

        [MethodImpl(0x8000)]
        public float CalcPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void CollectProviderList()
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesCalculaterBase GetPropertiesCalculater()
        {
        }

        [MethodImpl(0x8000)]
        public PropertiesId GetReplacementPropertyIdByNoCacheOrFrameCacheGroup(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool HasPropertiesGroupOnPropertiesCalc(PropertyGroup group)
        {
        }

        [MethodImpl(0x8000)]
        protected bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RegistEventOnPropertiesGroupDirty(Action<int> onPropertiesGroupDirty)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDirtyByGroupMask(uint groupMask)
        {
        }

        [MethodImpl(0x8000)]
        public void UnInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregistEventOnPropertiesGroupDirty(Action<int> onPropertiesGroupDirty)
        {
        }

        protected virtual PropertiesCalculaterBase PropertiesCalc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected IPropertiesProvider PropertiesCalcProvider
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

