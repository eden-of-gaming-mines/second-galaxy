﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildAllianceInviteInfo
    {
        public ulong m_instanceId;
        public uint m_allianceId;
        public string m_allianceName;
        public GuildAllianceLanguageType m_allianceLanguageType;
        public int m_allianceMemberGuildCount;
        public AllianceLogoInfo m_allianceLogo;
        public string m_inviterGameUserId;
        public string m_inviterGameUserName;
        public uint m_inviterGuildId;
        public string m_inviterGuildCode;
        public string m_inviterGuildName;
        public uint m_inviteeGuildId;
        public DateTime m_createTime;
        public DateTime m_expireTime;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;

        [MethodImpl(0x8000)]
        public GuildAllianceInviteInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceInviteInfo(AllianceInviteInfo info)
        {
        }
    }
}

