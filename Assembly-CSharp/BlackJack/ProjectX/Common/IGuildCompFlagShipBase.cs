﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildCompFlagShipBase : IGuildFlagShipBase
    {
        ILBStaticFlagShip GetStaticFlagShipByDriver(string driverId);
    }
}

