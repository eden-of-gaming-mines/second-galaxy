﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PVector3D")]
    public class PVector3D : IExtensible
    {
        private float _X;
        private float _Y;
        private float _Z;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_X;
        private static DelegateBridge __Hotfix_set_X;
        private static DelegateBridge __Hotfix_get_Y;
        private static DelegateBridge __Hotfix_set_Y;
        private static DelegateBridge __Hotfix_get_Z;
        private static DelegateBridge __Hotfix_set_Z;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="X", DataFormat=DataFormat.FixedSize)]
        public float X
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Y", DataFormat=DataFormat.FixedSize)]
        public float Y
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Z", DataFormat=DataFormat.FixedSize)]
        public float Z
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

