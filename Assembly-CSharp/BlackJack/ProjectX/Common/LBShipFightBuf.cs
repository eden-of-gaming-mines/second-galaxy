﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBShipFightBuf : LBBufBase
    {
        protected EquipType m_sourceEquipType;
        protected uint m_srcTargetObjId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnAttach;
        private static DelegateBridge __Hotfix_GetProBufInfo;
        private static DelegateBridge __Hotfix_get_SrcEquipType;
        private static DelegateBridge __Hotfix_get_SrcTargetObjId;

        [MethodImpl(0x8000)]
        public LBShipFightBuf(ConfigDataBufInfo confInfo, uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public override ProBufInfo GetProBufInfo(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnAttach(ILBBufSource source, ILBBufOwner owner, uint currTime, uint lifeEndTime, LBBuffContainer container, object param = null, float bufInstanceParam1 = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        public EquipType SrcEquipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public uint SrcTargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

