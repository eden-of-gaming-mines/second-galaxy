﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildTradePurchaseInfo
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public int m_configId;
        public StoreItemType m_itemType;
        public int m_count;
        public long m_price;
        public StarMapGuildSimpleInfo m_guildInfo;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

