﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SceneTextNoticeInfo
    {
        public int m_dialogId;
        public SceneTextNoticeType m_noticeType;
        public SceneMulticastType m_multicastType;
        public List<string> m_extraParam;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

