﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Flags]
    public enum GuildFleetPosition
    {
        Normal = 0,
        Commander = 1,
        Navigator = 2,
        FireController = 4
    }
}

