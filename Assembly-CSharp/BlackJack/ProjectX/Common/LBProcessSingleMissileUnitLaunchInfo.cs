﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBProcessSingleMissileUnitLaunchInfo : LBProcessSingleUnitLaunchInfo
    {
        protected ushort m_missileFlyTime;
        protected ILBSpaceTarget m_destTarget;
        protected LBBulletDamageInfo m_damageInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetBulletFlyTime;
        private static DelegateBridge __Hotfix_SetDestTarget;
        private static DelegateBridge __Hotfix_GetBulletFlyTime;
        private static DelegateBridge __Hotfix_GetDestTarget;
        private static DelegateBridge __Hotfix_SetDamage;
        private static DelegateBridge __Hotfix_GetDamageInfo;

        [MethodImpl(0x8000)]
        public ushort GetBulletFlyTime()
        {
        }

        [MethodImpl(0x8000)]
        public LBBulletDamageInfo GetDamageInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetDestTarget()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletFlyTime(ushort flyTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetDamage(float totalDamage, float totalCriticalDamage, float damageComposeHeat, float damageComposeKinetic, float realDamageHeat, float realDamageKinetic, float realDamageElectric, float percentageDamage, float weaponFireCtrlAccuracy, WeaponCategory weaponType, float chargeAndChannelBreakFactor, float weaponTransverseVelocity, float targetTransverseVelocity, float allDamageMulti)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDestTarget(ILBSpaceTarget target)
        {
        }
    }
}

