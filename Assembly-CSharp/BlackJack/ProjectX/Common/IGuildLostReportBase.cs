﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildLostReportBase
    {
        void AddBuildingLostReport(BuildingLostReport report);
        void AddSovereignLostReport(SovereignLostReport report);
        BuildingLostReport GetBuildingLostReport(ulong reportId);
    }
}

