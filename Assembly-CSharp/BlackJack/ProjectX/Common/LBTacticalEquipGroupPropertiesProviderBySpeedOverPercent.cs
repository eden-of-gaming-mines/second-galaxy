﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderBySpeedOverPercent : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        protected float m_shipSpeedOverPercentMin;
        protected float m_shipSpeedOverPercentMax;
        protected float m_factor4SpeedOverPercentMin;
        protected float m_factor4SpeedOverPercentMax;
        protected float m_factor4LessSpeedOverPercentMin;
        protected float m_factor4ThanSpeedOverPercentMax;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnSelfSpeed;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnSelfVelocity;
        private static DelegateBridge __Hotfix_GetDefaultMaxSpeed;
        private static DelegateBridge __Hotfix_get_ShipSpeedOverPercentMin;
        private static DelegateBridge __Hotfix_get_ShipSpeedOverPercentMax;
        private static DelegateBridge __Hotfix_get_Factor4SpeedOverPercentMin;
        private static DelegateBridge __Hotfix_get_Factor4SpeedOverPercentMax;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderBySpeedOverPercent(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnSelfVelocity(ref float propertyValue)
        {
        }

        [MethodImpl(0x8000)]
        protected float GetDefaultMaxSpeed()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnSelfSpeed(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float ShipSpeedOverPercentMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ShipSpeedOverPercentMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4SpeedOverPercentMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4SpeedOverPercentMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

