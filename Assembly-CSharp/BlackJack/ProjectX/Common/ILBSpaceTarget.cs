﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using System;

    public interface ILBSpaceTarget : ILBBufSource, ILBSpaceObject
    {
        float CalcPropertiesById(PropertiesId propertiesId);
        uint GetAllienceId();
        uint GetGuildId();
        LogicBlockShipCompPVPBase GetLBPVP();
        int GetLockedMeNpcTargetCount();
        float GetMinAroundRadius();
        int GetNpcTargetCountInHateList();
        uint GetObjectId();
        LBSpaceProcess GetProcessByInstanceId(uint instanceId);
        Quaternion GetRotationQuaternion();
        string GetSpaceTargetName();
        object GetSpaceTargetOwner();
        float GetTargetArmor();
        float GetTargetEnergy();
        int GetTargetPriority();
        float GetTargetShield();
        Vector4D GetTargetVelocity();
        bool HasBuf(int bufId, ulong instanceid);
        bool IsAvailable4WeaponEquipTarget(bool isHostile);
        bool IsBufLifeEnd(ulong instanceid);
        bool IsDead();
        bool IsExistFightBuf(int bufId);
        bool IsHiredCaptainShip();
        bool IsInJumping();
        bool IsInJumpPrepare();
        bool IsInvisible();
        bool IsNpcPolice();
        bool IsNpcShip();
        bool IsPlayerShip();
        bool IsStateFreeze();
        bool IsWaitForRemove();
        bool IsWithNotAttackableFlag();
        bool IsWithShieldRepairerDisableFlag();
        ulong OnAttachBufByEquip(int bufId, uint bufLifeTime, float bufInstanceParam1, ILBSpaceTarget srcTarget, EquipType equipType);
        void OnClearInvisibleByTarget(ILBSpaceTarget target);
        void OnClearLockedByOtherTarget(ILBSpaceTarget srcTarget);
        void OnDetachBufByEquip(int bufId, ulong instanceid);
        void OnEnemyDroneFighterAttach(LBSpaceProcessDroneFighterLaunch process);
        void OnHitByBullet(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess bulletFlyProcess);
        void OnHitByDrone(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess, int droneIndex);
        void OnHitByEquip(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess);
        void OnHitByLaser(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isCritical, float damageMulti, LBBulletDamageInfo damageInfo, LBSpaceProcess singleLaserFireProcess);
        void OnKillOtherTarget(ILBSpaceTarget target);
        void OnLockedByOtherTarget(ILBSpaceTarget srcTarget);
        void OnMissileInComming(LBSpaceProcessMissileLaunch process);
        void OnOtherTargetBlink(ILBSpaceTarget target);
        void OnOtherTargetInvisibleEnd(ILBSpaceTarget target);
        void OnOtherTargetInvisibleStart(ILBSpaceTarget target);
        void OnOtherTargetJump(ILBSpaceTarget target);
    }
}

