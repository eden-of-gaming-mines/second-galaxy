﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IMotherShipBasicDataContainer
    {
        MSRedeployInfo GetRedeployInfo();
        int GetSolarSystemId();
        int GetSpaceStationId();
        void UpdateRedeployInfo(MSRedeployInfo info);
        void UpdateSolarSystemId(int solarSystemId);
        void UpdateSpaceStationId(int spaceStationId);
    }
}

