﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface IPlayerInfoProvider
    {
        List<CustomizedParameterInfo> GetCustomizedParameterList();
        bool GetCustomizedParameterValueAsBool(CustomizedParameter parameterId);
        int GetCustomizedParameterValueAsInt(CustomizedParameter parameterId);
        string GetCustomizedParameterValueAsString(CustomizedParameter parameterId);
        GenderType GetGenderType();
        GrandFaction GetGrandFaction();
        int GetPlayerAvatarId();
        string GetPlayerGameUserId();
        int GetPlayerLevel();
        string GetPlayerName();
        ProfessionType GetPlayerProfession();
        ulong GetSessionId();
        uint GetTeamInstanceId();
    }
}

