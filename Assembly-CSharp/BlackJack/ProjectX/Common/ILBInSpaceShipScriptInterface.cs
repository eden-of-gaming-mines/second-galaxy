﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using System;
    using System.Runtime.InteropServices;

    public interface ILBInSpaceShipScriptInterface : ILogicBlockShipCompAI
    {
        void AddTarget2HateList(ILBSpaceTarget target);
        void AddTarget2HateList(uint targetObjId);
        bool CheckDistanceToLocation(Vector3D location, float maxDistance);
        bool CheckDistanceToTarget(ILBSpaceTarget target, float maxDistance);
        bool CheckDistanceToTarget(uint targetObjId, float maxDistance);
        bool CheckFightBufById(int bufId);
        void ClearAllTarget();
        void ClearLockTarget();
        void ClearShieldProtectBuf();
        void EnableAccurateAOI(bool enable);
        void EnableHighSlotAutoRun(bool isEnable);
        void EnableNormalAttackAutoRun(bool isEnable);
        void EnableSuperWeaponEquipAutoRun(bool isEnable);
        int GetActivityCompleteCount(ActivityType type);
        uint GetAllianceId();
        float GetArmorPercent();
        uint GetGuildId();
        IFFState GetIFFStateForTarget(ILBSpaceTarget target);
        uint GetInteractionTime();
        SceneInteractionType GetInteractionType();
        float GetNormalFireRangeMax();
        string GetOwnerGameUserId();
        float GetShieldMax();
        float GetShieldPercent();
        bool IsHighSlotAutoRun();
        bool IsInFight();
        bool IsNormalAttackAutoRun();
        bool IsShipStoreHasSpace();
        void JumpToLocation(Vector3D location);
        void JumpToRandomPlanet();
        void ModifyBindMoney(int value, OperatieLogMoneyChangeType causeId);
        void PushCmdMoveToLocation(Vector3D location);
        void PushCmdMoveToTarget(ILBSpaceTarget target);
        void PushCmdMoveToTarget(uint targetObjId);
        void RaiseSyncEventAnimEffect(NpcShipAnimEffectType effect, uint startTime = 0, uint endTime = 0, int openParam = 1);
        void RegEventOnHitByTarget(Action<ILBSpaceTarget, bool, LBBulletDamageInfo> action);
        void RegEventOnJumpingEnd(Action<ILBInSpaceShip> action);
        void RegEventOnLaunchSuperWeapon(Action action);
        void RegEventOnPrepare4Jump(Action action);
        void RegEventOnTargetAdd2HateList(Action<ILBSpaceTarget, ILBInSpaceShip> action);
        void RegisterIneractionMakerTickEnd(Action<uint, int, ILBInSpacePlayerShip> action);
        void ReSetNpcFactionId(int factionid);
        void SetArmor2SpecifiedPercent(float percent);
        void SetEnergy2SpecifiedPercent(float percent);
        void SetNeverFight2Player(bool enable);
        void SetShield2SpecifiedPercent(float percent);
        void SetShieldArmorWhenCreated(float shield, float armor);
        void SetShieldProtectBuf(float percent);
        void SetStateFreeze(bool stateFreeze, uint timeoutTime);
        void StopShip();
        bool TryLaunchEquip(EquipType equipType, ILBSpaceTarget preferTarget = null);
        bool TryLaunchEquip(EquipType equipType, uint preferTargetObjId = 0);
        bool TryLaunchSuperWeaponEquip(uint preferTargetObjId = 0);
        bool TryLaunchWeapon2Target(ILBSpaceTarget target);
        int TryLockTarget(ILBSpaceTarget target, bool forceNormalAttackRun = false, bool lockTargetByManualOpt = true);
        int TryLockTarget(uint targetObjId, bool forceNormalAttackRun = false, bool lockTargetByManualOpt = true);
        void UnregEventOnHitByTarget(Action<ILBSpaceTarget, bool, LBBulletDamageInfo> action);
        void UnregEventOnJumpingEnd(Action<ILBInSpaceShip> action);
        void UnregEventOnLaunchSuperWeapon(Action action);
        void UnregEventOnPrepare4Jump(Action action);
        void UnRegisterIneractionMakerTickEnd(Action<uint, int, ILBInSpacePlayerShip> action);
        void UnretEventOnTargetAdd2HateList(Action<ILBSpaceTarget, ILBInSpaceShip> action);
    }
}

