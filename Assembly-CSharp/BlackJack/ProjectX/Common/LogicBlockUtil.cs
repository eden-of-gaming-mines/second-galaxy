﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class LogicBlockUtil
    {
        private static DelegateBridge __Hotfix_GetRandomIdInIdLevelWeightPool;
        private static DelegateBridge __Hotfix_CalcSpaceShipDistance2Target;
        private static DelegateBridge __Hotfix_IsJumpValidByDistanceToTarget;
        private static DelegateBridge __Hotfix_IsJumpValidForLocation;
        private static DelegateBridge __Hotfix_IdLevelInfoListContainsItem;
        private static DelegateBridge __Hotfix_CalcRandomLocationOnRadius;
        private static DelegateBridge __Hotfix_CheckIsInRadiusRange;

        [MethodImpl(0x8000)]
        public static Vector3D CalcRandomLocationOnRadius(Vector3D center, Random random, float radius)
        {
        }

        [MethodImpl(0x8000)]
        public static double CalcSpaceShipDistance2Target(ILBSpaceTarget ship, SpaceObjectType targetType, object targetObj)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckIsInRadiusRange(Vector3D center, float radius, Vector3D destLocation)
        {
        }

        [MethodImpl(0x8000)]
        public static IdLevelInfo GetRandomIdInIdLevelWeightPool(List<IdLevelWeightItem> weightList, Random random)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IdLevelInfoListContainsItem(List<IdLevelInfo> listInfo, IdLevelInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsJumpValidByDistanceToTarget(ILBSpaceTarget ship, SpaceObjectType targetType, object targetObj)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsJumpValidForLocation(ILBSpaceTarget ship, Vector3D location)
        {
        }
    }
}

