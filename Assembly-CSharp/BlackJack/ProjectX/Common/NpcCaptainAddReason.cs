﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Serializable]
    public enum NpcCaptainAddReason
    {
        QuestReward,
        GM,
        ItemUse,
        SceneReward,
        LoginActivity
    }
}

