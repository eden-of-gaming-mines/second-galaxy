﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupAttachBufBySupperEnergyFullBase : LBTacticalEquipGroupBase
    {
        protected LogicBlockShipCompInSpaceWeaponEquipBase m_lbWeaponEquip;
        protected LBBufBase m_currBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_GetBufId4Attach;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupAttachBufBySupperEnergyFullBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public int GetBufId4Attach()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

