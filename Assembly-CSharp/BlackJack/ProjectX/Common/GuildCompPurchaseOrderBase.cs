﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompPurchaseOrderBase : GuildCompBase, IGuildCompPurchaseOrderBase, IGuildPurchaseOrderBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildPurchaseOrderListInfo;
        private static DelegateBridge __Hotfix_AddGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_UpdateGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_CancelGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompPurchaseOrderBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AddGuildPurchaseOrder(GuildPurchaseOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool CancelGuildPurchaseOrder(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildPurchaseOrderInfo> GetGuildPurchaseOrderListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool UpdateGuildPurchaseOrder(GuildPurchaseOrderInfo info)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

