﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class RechargeMonthlyCardInfo
    {
        public int m_id;
        public DateTime m_expireTime;
        public bool m_isDailyRewardReceived;
        public bool m_isExpireEventFired;
        public bool m_isNearExpireEventFired;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

