﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class SolarSystemNpcShipInstanceInfo : SolarSystemShipInstanceInfoBase
    {
        public int m_npcShipTemplateId;
        public int m_npcShipConfId;
        public int m_npcMonsterConfId;
        public int m_monsterLevel;
        public int m_npcMonsterLevelDropConfId;
        public int m_npcMonsterLevelBufConfId;
        public uint m_masterShipObjId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

