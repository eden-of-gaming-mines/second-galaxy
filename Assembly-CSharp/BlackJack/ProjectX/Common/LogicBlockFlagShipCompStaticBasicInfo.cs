﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockFlagShipCompStaticBasicInfo
    {
        private double m_currFuel;
        private ILBStaticFlagShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetCurrFuel;
        private static DelegateBridge __Hotfix_ChangeFuel;

        [MethodImpl(0x8000)]
        public void ChangeFuel(double changeValue)
        {
        }

        [MethodImpl(0x8000)]
        public double GetCurrFuel()
        {
        }

        [MethodImpl(0x8000)]
        public void Initialize(ILBStaticFlagShip ship)
        {
        }

        [MethodImpl(0x8000)]
        public void PostInitialize()
        {
        }
    }
}

