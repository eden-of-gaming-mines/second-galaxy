﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBWeaponGroupLaserBase : LBWeaponGroupBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBWeaponGroupLaserBase, bool> EventOnAllLaserSingleFireEnd;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_OnProcessUnitFire;
        private static DelegateBridge __Hotfix_FireEventOnAllLaserSingleFireEnd;
        private static DelegateBridge __Hotfix_CreateProcess;
        private static DelegateBridge __Hotfix_CalcWaveCD;
        private static DelegateBridge __Hotfix_CalcHitRateFinal;
        private static DelegateBridge __Hotfix_CalcCriticalRate;
        private static DelegateBridge __Hotfix_CalcDamageTotal;
        private static DelegateBridge __Hotfix_CalcCriticalDamageTotal;
        private static DelegateBridge __Hotfix_CalcDamageComposeHeat;
        private static DelegateBridge __Hotfix_CalcDamageComposeKinetic;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_CalcFireCtrlAccuracy;
        private static DelegateBridge __Hotfix_CalcReloadAmmoCD;
        private static DelegateBridge __Hotfix_CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_CalcWaveGroupDamage;
        private static DelegateBridge __Hotfix_CalcLaserAttackTime;
        private static DelegateBridge __Hotfix_CalcDamageMultiByTime;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4WeaponEquip;
        private static DelegateBridge __Hotfix_GetPropertyFromConfigData4Ammo;
        private static DelegateBridge __Hotfix_add_EventOnAllLaserSingleFireEnd;
        private static DelegateBridge __Hotfix_remove_EventOnAllLaserSingleFireEnd;

        public event Action<LBWeaponGroupLaserBase, bool> EventOnAllLaserSingleFireEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupLaserBase(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcCriticalDamageTotal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcCriticalRate(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageComposeHeat()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageComposeKinetic()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcDamageMultiByTime(uint t1EndTime, uint t2EndTime, uint t3Endtime, uint processTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcDamageTotal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireCtrlAccuracy()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcHitRateFinal(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcLaserAttackTime(out uint t1EndTime, out uint t2EndTime, out uint t3Endtime)
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcReloadAmmoCD()
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcWaveCD()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcWaveGroupDamage()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBSpaceProcessLaserSingleFire CreateProcess(ushort unitIndex, LBSpaceProcessLaserLaunch laserLaunchProcess, uint unitLaunchTime)
        {
        }

        [MethodImpl(0x8000)]
        public void FireEventOnAllLaserSingleFireEnd(bool isCancel)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnProcessUnitFire(LBSpaceProcessWeaponLaunchBase process, ushort unitIndex, out LBSpaceProcessLaserSingleFire laserSingleFireProcess)
        {
        }
    }
}

