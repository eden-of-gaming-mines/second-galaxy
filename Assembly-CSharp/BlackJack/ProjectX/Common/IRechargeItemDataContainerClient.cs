﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IRechargeItemDataContainerClient : IRechargeItemDataContainer
    {
        void RefreshAllData(List<RechargeItemInfo> itemList, int version);
    }
}

