﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IFlagShipDataContainer : IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        ConfigDataFlagShipRelativeData GetConfRelativeData();
        double GetCurrShipFuel();
        int GetSolarSystemId();
        void SetSolarSystemId(int solarSystemId);
        void UpdateCurrShipFuel(double fuel);
    }
}

