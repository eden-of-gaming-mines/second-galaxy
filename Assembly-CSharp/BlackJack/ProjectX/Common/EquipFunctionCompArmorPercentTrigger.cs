﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class EquipFunctionCompArmorPercentTrigger : EquipFunctionCompDataPercentTriger
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetCurrPercent;
        private static DelegateBridge __Hotfix_ResetTriggerState;

        [MethodImpl(0x8000)]
        public EquipFunctionCompArmorPercentTrigger(IEquipFunctionCompOwner owner, float percent, bool lessEqual = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetCurrPercent()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetTriggerState()
        {
        }
    }
}

