﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    public class SolarSystemDefaultEvent
    {
        public const string EventIdCloseScene = "CloseScene";
        public const string EventIdOnGuildActionFail = "OnGuildActionFail";
        public const string EventIdOnGuildActionComplete = "OnGuildActionComplete";
        public const string EventIdOnGuildActionProcessUpdate = "OnGuildOnActionProcessUpdate";
        public const string EventIdOnGuildMemberInfectFinalBattleFinalStrike = "OnGuildMemberInfectFinalBattleFinalStrike";
        public const string EventIdOnGuildBuildingSpy = "OnGuildBuildingSpy";
        public const string EventIdOnGuildBuildingSceneCreate = "OnGuildBuildingSceneCreate";
        public const string EventIdOnGuildBuildingNpcObjCreate = "OnGuildBuildingNpcObjCreate";
        public const string EventIdOnGuildDefenderBuildingLowShield = "OnGuildDefenderBuildingLowShield";
        public const string EventIdOnGuildBuildingDynamicUpdate = "OnGuildBuildingDynamicUpdate";
        public const string EventIdOnGuildBuildingDestory = "OnGuildBuildingDestory";
        public const string EventIdOnGuildSovereignBattleDeclaration = "OnGuildSovereignBattleDeclaration";
        public const string EventIdSendGuildBuildingAttackerGuildDiplomacyReq = "SendGuildBuildingAttackerGuildDiplomacyReq";
        public const string EventIdOnGuildMiningBalanceSucceed = "OnGuildMiningBalanceSucceed";
        public const string EventIdOnGuildMiningBalanceFailed = "OnGuildMiningBalanceFailed";
        public const string EventIdOnGuildTradeTransportShipDestoryed = "OnGuildTradeTransportShipDestoryed";
        public const string EventIdOnGuildTradeTransportShipDynamicInfoUpdate = "OnGuildTradeTransportShipDynamicInfoUpdate";
        public const string EventIdSendSystemChatInGuildReq = "SendSystemChatInGuildReq";
        public const string EventIdSendSystemChatInLocalNtf = "SendSystemChatInLocalNtf";
        public const string EventIdTryToBroadingFlagShip = "BroadingFlagShip";
        public const string EventIdTryToLeaveFlagShip = "LeaveFlagShip";
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

