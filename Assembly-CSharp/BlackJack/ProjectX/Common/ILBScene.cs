﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface ILBScene
    {
        SceneType GetCurrSceneType();
        int GetEnterSceneProtectBuf();
        List<ShipType> GetShipTypeLimit();
        SceneWingShipSetting GetWingShipSetting();
        bool IsJumpEnable4Ship(ILBInSpacePlayerShip ship, Vector3D? jumpLocation);
        bool IsNeedAccurateAOI4Player();
        bool IsPVPEnable();
        bool IsSceneOwner(string playerGameUserId);
    }
}

