﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct ActivityInfo
    {
        public ActivityType m_type;
        public bool m_available;
        public DateTime m_startTime;
        public DateTime m_endTime;
    }
}

