﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class PVPInvadeRescueInfo
    {
        public SignalInfo m_sourceSignal;
        public PlayerSimpleInfo m_sourcePlayerInfo;
        public List<NpcCaptainStaticInfo> m_captainStaticList;
        public List<NpcCaptainSimpleDynamicInfo> m_captainDynamicList;
        public List<ulong> m_destroyCaptainInstanceIdList;
        public PlayerSimpleInfo m_invadePlayerInfo;
        public DateTime m_invadeStartTime;
        public uint m_manualQuestSceneInstanceId;
        public int m_manualQuestSolarsystemId;
        public int m_manualQuestId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

