﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompItemStoreBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEmptyShipItemStore;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected Func<ulong> m_instanceIdAllocator;
        protected List<LBShipStoreItemBase> m_shipStoreItemList;
        protected List<LBShipStoreItemBase> m_shipStoreContrabandList;
        protected ulong m_shipItemStoreCurrUsedSize;
        protected bool m_shipItemStoreCurrUsedSizeDirty;
        protected ILBSpaceShipBasic m_ownerShip;
        protected ILBStaticPlayerShip m_ownerShipStatic;
        protected ILBInSpaceShip m_ownerShipInSpace;
        protected IShipDataContainer m_shipDC;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetSpaceMax;
        private static DelegateBridge __Hotfix_GetSpaceUsed;
        private static DelegateBridge __Hotfix_GetSpaceFree;
        private static DelegateBridge __Hotfix_HasSpace_2;
        private static DelegateBridge __Hotfix_HasSpace_1;
        private static DelegateBridge __Hotfix_HasSpace_0;
        private static DelegateBridge __Hotfix_GetShipStoreItemByStoreIndex;
        private static DelegateBridge __Hotfix_GetShipStoreItemList;
        private static DelegateBridge __Hotfix_GetShipStoreItemByInsId;
        private static DelegateBridge __Hotfix_GetShipStoreItemByConfigId;
        private static DelegateBridge __Hotfix_GetShipStoreItemCountByConfigId;
        private static DelegateBridge __Hotfix_HasContraband;
        private static DelegateBridge __Hotfix_CheckTransformItemFromMSStore2Ship;
        private static DelegateBridge __Hotfix_CheckTransformItemListFromMSStore2Ship;
        private static DelegateBridge __Hotfix_CheckTransformItemFromShip2MSStore;
        private static DelegateBridge __Hotfix_CheckTransformItemListFromShip2MSStore;
        private static DelegateBridge __Hotfix_CheckTransformAllItemFromShip2MSStore;
        private static DelegateBridge __Hotfix_CheckAddItemAutoOverlay;
        private static DelegateBridge __Hotfix_CheckRemoveItem;
        private static DelegateBridge __Hotfix_CheckContraband;
        private static DelegateBridge __Hotfix_IsContraband;
        private static DelegateBridge __Hotfix_CalcShipItemStoreCurrUsedSize;
        private static DelegateBridge __Hotfix_CreateLBShipStoreItemByShipStoreItemInfo;
        private static DelegateBridge __Hotfix_RemoveItem;
        private static DelegateBridge __Hotfix_RemoveItemImpl;
        private static DelegateBridge __Hotfix_AddShipStoreItemImpl;
        private static DelegateBridge __Hotfix_OnEmptyShipItemStore;
        private static DelegateBridge __Hotfix_OutShipStoreItemChange;
        private static DelegateBridge __Hotfix_MergeFreezingItemInShipStore;
        private static DelegateBridge __Hotfix_DestoryAllShipStoreItems;
        private static DelegateBridge __Hotfix_add_EventOnEmptyShipItemStore;
        private static DelegateBridge __Hotfix_remove_EventOnEmptyShipItemStore;

        public event Action EventOnEmptyShipItemStore
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LBShipStoreItemBase AddShipStoreItemImpl(int storeItemIndex, StoreItemType itemType, int configId, bool isBind, bool isFreezing, long count, ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcShipItemStoreCurrUsedSize()
        {
        }

        [MethodImpl(0x8000)]
        public long CheckAddItemAutoOverlay(StoreItemType itemType, int configId, long count, bool fill2JustFull, bool skipSpaceCheck, out int erroCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckContraband(List<LBShipStoreItemBase> itemList)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckRemoveItem(LBShipStoreItemBase shipStoreItem, long count)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckTransformAllItemFromShip2MSStore()
        {
        }

        [MethodImpl(0x8000)]
        public int CheckTransformItemFromMSStore2Ship(LBStoreItem srcMSStoreItem, long count)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckTransformItemFromShip2MSStore(LBShipStoreItemBase srcShipStoreItem, long count)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckTransformItemListFromMSStore2Ship(List<int> storeItemIndexList, out List<LBStoreItem> srcMSStoreItemList)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckTransformItemListFromShip2MSStore(List<int> srcShipStoreItemIndexList, out List<LBShipStoreItemBase> srcShipStoreItemList)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBShipStoreItemBase CreateLBShipStoreItemByShipStoreItemInfo(ShipStoreItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void DestoryAllShipStoreItems(OperateLogItemChangeType operateLogCauseId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase GetShipStoreItemByConfigId(StoreItemType itemType, int itemId, bool isBind, bool isFreezing)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase GetShipStoreItemByInsId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public LBShipStoreItemBase GetShipStoreItemByStoreIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public long GetShipStoreItemCountByConfigId(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBShipStoreItemBase> GetShipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetSpaceFree()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetSpaceMax()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetSpaceUsed()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasContraband()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasSpace()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasSpace(float size)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasSpace(ulong size)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBSpaceShipBasic ownerShip, LogicBlockItemStoreBase lbItemStore = null, Func<ulong> InstanceIdAllocator = null)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsContraband(LBShipStoreItemBase lbItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void MergeFreezingItemInShipStore(StoreItemType storeItemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEmptyShipItemStore(int sceneId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OutShipStoreItemChange(ItemInfo shipStoreItem, long changeCount, bool isFreezing, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool RemoveItem(StoreItemType itemType, int configId, long count, out int errCode, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void RemoveItemImpl(LBShipStoreItemBase destItem, long count, OperateLogItemChangeType operateLogCauseId = 0, string operateLogLocation = null)
        {
        }
    }
}

