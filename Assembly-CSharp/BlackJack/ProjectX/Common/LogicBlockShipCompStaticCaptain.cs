﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompStaticCaptain : IPropertiesProvider
    {
        private ILBStaticShip m_ownerShip;
        private ILBShipCaptain m_captain;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetCaptain;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public ILBShipCaptain GetCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBStaticShip ownerShip, ILBShipCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }
    }
}

