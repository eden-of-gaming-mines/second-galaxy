﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IStarMapGuildOccupyDataContainer
    {
        StarMapGuildOccupyDynamicInfo GetStarMapGuildOccupyDynamicInfo(int starFieldId);
        StarMapGuildOccupyStaticInfo GetStarMapGuildOccupyStaticInfo(int starFieldId);
        void SaveStarMapGuildOccupyInfo2DB();
        void SetVersion(int starFieldId, ushort staticVersion, ushort dynamicVersion);
        void StarMapAllianceOccupyInfoAdd(int starFieldId, int solarSystemId, uint allianceId);
        bool StarMapAllianceOccupyInfoRemove(int starFieldId, int solarSystemId);
        StarMapAllianceSimpleInfo StarMapAllianceSimpleInfoAddOrUpdate(int starFieldId, StarMapAllianceSimpleInfo info);
        StarMapGuildOccupyDynamicInfo StarMapGuildOccupyDynamicInfoUpdate(StarMapGuildOccupyDynamicInfo info);
        void StarMapGuildOccupyInfoAdd(int starFieldId, int solarSystemId, uint guildId);
        bool StarMapGuildOccupyInfoRemove(int starFieldId, int solarSystemId);
        StarMapGuildOccupyStaticInfo StarMapGuildOccupyStaticInfoUpdate(StarMapGuildOccupyStaticInfo info);
        StarMapGuildSimpleInfo StarMapGuildSimpleInfoAddOrUpdate(int starFieldId, StarMapGuildSimpleInfo info);
        StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo StarMapSolarSystemDynamicInfoAddOrUpdate(int starFieldId, int solarSystemId, int? flourishValue, bool? isInGuildBattle);
    }
}

