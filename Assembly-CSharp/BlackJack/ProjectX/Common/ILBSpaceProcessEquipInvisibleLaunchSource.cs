﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessEquipInvisibleLaunchSource : ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        void OnProcessLoop(LBSpaceProcessEquipInvisibleLaunch process);
    }
}

