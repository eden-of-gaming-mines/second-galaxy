﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessEquipPeriodicDamageSource : ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        void OnProcessPeriodicTrigger(LBSpaceProcessEquipPeriodicDamageLaunch process);
    }
}

