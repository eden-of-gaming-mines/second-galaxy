﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildSentryInterestTargetInfo
    {
        public string m_playerUserId;
        public uint m_guildId;
        public int m_shipId;
        public ShipType m_shipType;
        public uint m_sceneInstanceId;
        public int m_sceneConfId;
        public int m_threatValue;
        public int m_questId;
        public int m_signalId;
        public GuildBuildingType m_buildingType;
        public DateTime m_stayingThreatValueRefreshTime;
        public DateTime? m_solarSystemLeaveTime;
        public uint m_objectId;
        public bool m_bIsInvisible;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

