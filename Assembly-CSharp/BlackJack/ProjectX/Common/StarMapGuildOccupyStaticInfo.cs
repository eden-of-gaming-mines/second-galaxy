﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StarMapGuildOccupyStaticInfo
    {
        public int m_starFieldId;
        public List<GuildOccupySolarSystemInfo> m_guildOccupyInfoList;
        public List<AllianceOccupySolarSystemInfo> m_allianceOccupyInfoList;
        public List<StarMapGuildSimpleInfo> m_guildSimpleInfoList;
        public List<StarMapAllianceSimpleInfo> m_allianceSimpleInfoList;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CopyForm;
        private static DelegateBridge __Hotfix_GetGuildOccupySolarSystemInfo;
        private static DelegateBridge __Hotfix_GetAllianceOccupySolarSystemInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildSimpleInfo;
        private static DelegateBridge __Hotfix_GetStarMapAllianceSimpleInfo;

        [MethodImpl(0x8000)]
        public void CopyForm(StarMapGuildOccupyStaticInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private AllianceOccupySolarSystemInfo GetAllianceOccupySolarSystemInfo(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        private GuildOccupySolarSystemInfo GetGuildOccupySolarSystemInfo(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        private StarMapAllianceSimpleInfo GetStarMapAllianceSimpleInfo(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        private StarMapGuildSimpleInfo GetStarMapGuildSimpleInfo(uint guildId)
        {
        }

        [Serializable]
        public class AllianceOccupySolarSystemInfo
        {
            public uint m_allianceId;
            public List<int> m_solarSystemIdList = new List<int>();
            private static DelegateBridge _c__Hotfix_ctor;

            public AllianceOccupySolarSystemInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        [Serializable]
        public class GuildOccupySolarSystemInfo
        {
            public uint m_guildId;
            public List<int> m_solarSystemIdList;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

