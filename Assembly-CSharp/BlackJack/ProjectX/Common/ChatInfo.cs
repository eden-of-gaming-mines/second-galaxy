﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    public class ChatInfo
    {
        public ChatSrcType m_srcType;
        public ChatContentType m_contentType;
        public PlayerSimplestInfo m_playerInfo;
        public LanguageType m_languageType;
        public bool m_isRead;
        public ChatLanguageChannel m_channelId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

