﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockRechargeGiftPackageClient : LogicBlockRechargeGiftPackageBase
    {
        private HashSet<int> m_recommendedIdSet;
        private Func<int, bool> m_rechargeGiftPackageFilter;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_RechargeGiftPackageAdd;
        private static DelegateBridge __Hotfix_GetWeeklyRechargeGiftPackageList;
        private static DelegateBridge __Hotfix_GetDailyRechargeGiftPackageList;
        private static DelegateBridge __Hotfix_GetSpecialRechargeGiftPackageList;
        private static DelegateBridge __Hotfix_GetRecommedSpecialRechargeGiftPackageList;
        private static DelegateBridge __Hotfix_MarkVaildSpecialRechargeGiftPackageAllRecommended;
        private static DelegateBridge __Hotfix_OnRechargeGiftPackageBuy;
        private static DelegateBridge __Hotfix_OnRechargeGiftPackageBuyCancel;
        private static DelegateBridge __Hotfix_OnRechargeGiftPackageDeliver;
        private static DelegateBridge __Hotfix_RefreshAllRechargeGiftPackages;
        private static DelegateBridge __Hotfix_AdjustTimeForConfig;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        private void AdjustTimeForConfig(LBRechargeGiftPackage lbRechargeGiftPackage)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeGiftPackage> GetDailyRechargeGiftPackageList(Func<LBRechargeGiftPackage, bool> filter = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeGiftPackage> GetRecommedSpecialRechargeGiftPackageList(Func<LBRechargeGiftPackage, bool> filter = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeGiftPackage> GetSpecialRechargeGiftPackageList(Func<LBRechargeGiftPackage, bool> filter = null)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeGiftPackage> GetWeeklyRechargeGiftPackageList(Func<LBRechargeGiftPackage, bool> filter = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx, Func<int, bool> rechargeGiftPackageFilter)
        {
        }

        [MethodImpl(0x8000)]
        public void MarkVaildSpecialRechargeGiftPackageAllRecommended()
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeGiftPackageBuy(RechargeOrderInfo order)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeGiftPackageBuyCancel(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRechargeGiftPackageDeliver(int giftPackageId)
        {
        }

        [MethodImpl(0x8000)]
        public override void RechargeGiftPackageAdd(RechargeGiftPackageInfo giftPackage)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshAllRechargeGiftPackages(List<RechargeGiftPackageInfo> giftPackageList, int version)
        {
        }

        private IRechargeGiftPackageDataContainerClient DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

