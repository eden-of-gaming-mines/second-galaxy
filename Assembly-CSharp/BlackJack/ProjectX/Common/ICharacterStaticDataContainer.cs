﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface ICharacterStaticDataContainer
    {
        int GetAvatarId();
        DateTime GetCreateTimeUtc();
        GenderType GetGender();
        GrandFaction GetMotherLand();
        string GetName();
        string GetPlayerUserId();
        ProfessionType GetProfession();
    }
}

