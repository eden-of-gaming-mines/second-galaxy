﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class StarMapGuildOccupyDynamicInfo
    {
        public int m_starFieldId;
        public List<SolarSystemDynamicInfo> m_solarSystemDynamicInfoList;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CopyForm;
        private static DelegateBridge __Hotfix_GetSolarSystemDynamicInfo;

        [MethodImpl(0x8000)]
        public void CopyForm(StarMapGuildOccupyDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private SolarSystemDynamicInfo GetSolarSystemDynamicInfo(int solarSystemId)
        {
        }

        [Serializable]
        public class SolarSystemDynamicInfo
        {
            public int m_solarSystemId;
            public int m_flourishValue;
            public bool m_isInGuildBattle;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

