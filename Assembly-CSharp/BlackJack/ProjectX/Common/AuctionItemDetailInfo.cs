﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class AuctionItemDetailInfo
    {
        public string m_gameUserId;
        public int m_auctionItemId;
        public ulong m_auctionItemInsId;
        public AuctionItemState m_auctionItemState;
        public int m_auctionItemCurrCount;
        public int m_auctionItemOrgCount;
        public long m_price;
        public DateTime m_expireTime;
        public float m_tradeTax;
        public long m_auctionFee;
        public long m_marketPrice;
        public string m_platformAuthId;
        public int m_playerLevel;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

