﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LogicBlockShipHangarsBase
    {
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected ILBPlayerContext m_playerCtx;
        protected IShipHangarListDataContainer m_hangarListDC;
        protected List<ILBStaticPlayerShip> m_shipHangarList;
        protected ulong m_shipHangarCurrUsedSlotCount;
        protected int m_hangerUnlockCount;
        protected List<int> m_hangerUnlockLevelList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnHangerPositionAdd;
        [CompilerGenerated]
        private static Comparison<int> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnPlayerLevelUp;
        private static DelegateBridge __Hotfix_CalcCurrentHangerPositionCount;
        private static DelegateBridge __Hotfix_GetHangerUnlockCount;
        private static DelegateBridge __Hotfix_GetSlotCountMax;
        private static DelegateBridge __Hotfix_GetSlotCountUsed;
        private static DelegateBridge __Hotfix_HasEmptySlot;
        private static DelegateBridge __Hotfix_GetShipHangarList;
        private static DelegateBridge __Hotfix_GetLBStaticPlayerShipByHangarIndex;
        private static DelegateBridge __Hotfix_GetILBStaticPlayerShipByInstanceId;
        private static DelegateBridge __Hotfix_GetFirstEmptyShipHangarIndex;
        private static DelegateBridge __Hotfix_GetAvailableShipInHangarCount;
        private static DelegateBridge __Hotfix_GetHangerListCount;
        private static DelegateBridge __Hotfix_GetPlayerShipDCByInstanceId;
        private static DelegateBridge __Hotfix_ShipChangeName;
        private static DelegateBridge __Hotfix_UpdateShipNameDirectly;
        private static DelegateBridge __Hotfix_CheckShipChangeName;
        private static DelegateBridge __Hotfix_CheckUnpackShipFromItemStore;
        private static DelegateBridge __Hotfix_CheckPackShipToItemStore;
        private static DelegateBridge __Hotfix_DestroyShip;
        private static DelegateBridge __Hotfix_DestoryShipImpl;
        private static DelegateBridge __Hotfix_add_EventOnHangerPositionAdd;
        private static DelegateBridge __Hotfix_remove_EventOnHangerPositionAdd;

        public event Action<int> EventOnHangerPositionAdd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LogicBlockShipHangarsBase()
        {
        }

        [MethodImpl(0x8000)]
        protected int CalcCurrentHangerPositionCount(int level)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckPackShipToItemStore(int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckShipChangeName(int hangarIndex, string shipName, bool isDomesticPackage = false)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckUnpackShipFromItemStore(LBStoreItem item, int hangarIndex)
        {
        }

        protected abstract ILBStaticPlayerShip CreateLBStaticPlayerShip(IStaticPlayerShipDataContainer shipDC, ILBPlayerContext playerCtx, int hangarIndex);
        public abstract void DestoryAllShipEquip(ILBStaticPlayerShip playerShip);
        [MethodImpl(0x8000)]
        public void DestoryShipImpl(int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool DestroyShip(int hangarIndex, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public int GetAvailableShipInHangarCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetFirstEmptyShipHangarIndex()
        {
        }

        [MethodImpl(0x8000)]
        public int GetHangerListCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetHangerUnlockCount()
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip GetILBStaticPlayerShipByInstanceId(ulong instanceId, bool includeDestroyShip = false)
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticPlayerShip GetLBStaticPlayerShipByHangarIndex(int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticPlayerShipDataContainer GetPlayerShipDCByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticPlayerShip> GetShipHangarList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSlotCountMax()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetSlotCountUsed()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasEmptySlot()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerLevelUp(int level)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public bool ShipChangeName(int hangarIndex, string shipName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipNameDirectly(int hangarIndex, string shipName)
        {
        }
    }
}

