﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildStoreItemInfo : StoreItemInfo
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_ClearForDelete;

        [MethodImpl(0x8000)]
        public void ClearForDelete()
        {
        }

        [MethodImpl(0x8000)]
        public long GetItemCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }
    }
}

