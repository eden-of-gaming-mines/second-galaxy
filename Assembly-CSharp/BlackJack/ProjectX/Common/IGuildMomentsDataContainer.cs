﻿namespace BlackJack.ProjectX.Common
{
    using System.Collections.Generic;

    public interface IGuildMomentsDataContainer
    {
        List<GuildMomentsInfo> GetGuildMomentsBriefInfoList();
        List<GuildMomentsInfo> GetGuildMomentsInfoList();
    }
}

