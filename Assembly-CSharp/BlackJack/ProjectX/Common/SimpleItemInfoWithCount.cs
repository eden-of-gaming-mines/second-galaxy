﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class SimpleItemInfoWithCount
    {
        public StoreItemType m_itemType;
        public int m_id;
        public long m_count;
        public bool m_isBind;
        public bool m_isFreezing;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public SimpleItemInfoWithCount()
        {
        }

        [MethodImpl(0x8000)]
        public SimpleItemInfoWithCount(int configId, StoreItemType type, long count, bool isBind = false, bool isFreezing = false)
        {
        }
    }
}

