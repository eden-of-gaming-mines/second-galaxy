﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupBase : LBInSpaceWeaponEquipGroupBase
    {
        protected ConfigDataShipTacticalEquipInfo m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetEquipFunctionType;
        private static DelegateBridge __Hotfix_GetTacticalEquipEnableState;
        private static DelegateBridge __Hotfix_EnableTacticalEquip;
        private static DelegateBridge __Hotfix_CalcEffectPreviewPropertiesById_1;
        private static DelegateBridge __Hotfix_CalcEffectPreviewPropertiesById_0;
        private static DelegateBridge __Hotfix_CalcEffectPreviewBufParamByIndex_1;
        private static DelegateBridge __Hotfix_CalcEffectPreviewBufParamByIndex_0;
        private static DelegateBridge __Hotfix_GetEquipPreviewPropertiesBaseByPropertiesFinal;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_IsOrdinaryWeapon;
        private static DelegateBridge __Hotfix_IsSuperWeapon;
        private static DelegateBridge __Hotfix_IsOrdinaryEquip;
        private static DelegateBridge __Hotfix_IsSuperEquip;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_GetAmmoConfId;
        private static DelegateBridge __Hotfix_GetAmmoType;
        private static DelegateBridge __Hotfix_GetAmmoCount;
        private static DelegateBridge __Hotfix_GetAmmoClipSize;
        private static DelegateBridge __Hotfix_CalcWaveCD;
        private static DelegateBridge __Hotfix_CalcGroupCD;
        private static DelegateBridge __Hotfix_CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_CalcChargeTime;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4WeaponEquip;
        private static DelegateBridge __Hotfix_GetPropertyFromConfigData4Ammo;
        private static DelegateBridge __Hotfix_CalcBufInstanceParam;
        private static DelegateBridge __Hotfix_AttachBufByEquip2Target_0;
        private static DelegateBridge __Hotfix_AttachBufByEquip2Target_1;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected ulong AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        protected ulong AttachBufByEquip2Target(ILBSpaceTarget srcTarget, ILBSpaceTarget target, EquipType equipType, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcBufInstanceParam(int bufId, out float bufInstanceParam, out PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcEffectPreviewBufParamByIndex(int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcEffectPreviewBufParamByIndex(ConfigDataShipTacticalEquipInfo confInfo, int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcEffectPreviewPropertiesById(PropertiesId propertiesId, object param)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcEffectPreviewPropertiesById(LBTacticalEquipGroupBase tacticalEquipGroup, ConfigDataShipTacticalEquipInfo confInfo, PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public override uint CalcGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcWaveCD()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void EnableTacticalEquip(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoClipSize()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoConfId()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetAmmoCount()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override StoreItemType GetAmmoType()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipTacticalEquipInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public TacticalEquipFunctionType GetEquipFunctionType()
        {
        }

        [MethodImpl(0x8000)]
        private static PropertiesId GetEquipPreviewPropertiesBaseByPropertiesFinal(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool GetTacticalEquipEnableState()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsOrdinaryEquip()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsOrdinaryWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsSuperEquip()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsSuperWeapon()
        {
        }
    }
}

