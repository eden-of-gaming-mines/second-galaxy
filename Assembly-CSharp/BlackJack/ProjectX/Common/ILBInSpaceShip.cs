﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBInSpaceShip : ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        ILogicBlockShipCompAOI GetLBAOI();
        LogicBlockShipCompInSpaceBasicCtxBase GetLBBasicCtx();
        LogicBlockShipCompBufContainerBase GetLBBufContainer();
        LogicBlockShipCompInSpaceCaptain GetLBCaptain();
        LogicBlockShipCompDelayExec GetLBDelayExec();
        LogicBlockShipCompDroneFireControllBase GetLBDroneFireControll();
        LogicBlockShipCompFireControllBase GetLBFireControll();
        LogicBlockShipCompGuildInfo GetLBGuildInfo();
        LogicBlockShipCompIFFBase GetLBIFF();
        LogicBlockShipCompInSpaceWeaponEquipBase GetLBInSpaceWeaponEquip();
        LogicBlockShipCompInteractionBase GetLBInteraction();
        LogicBlockShipCompKillRecordBase GetLBKillRecord();
        LogicBlockShipCompProcessContainer GetLBProcessContainer();
        LogicBlockCompPropertiesCalc GetLBPropertiesCalc();
        LogicBlockShipCompSceneBase GetLBScene();
        ILogicBlockShipCompSpaceObject GetLBSpaceObject();
        LogicBlockShipCompSpaceTargetBase GetLBSpaceTarget();
        LogicBlockShipCompSyncEventBase GetLBSyncEvent();
        LogicBlockShipCompTargetSelectBase GetLBTargetSelect();
        ILBSpaceContext GetSpaceCtx();
    }
}

