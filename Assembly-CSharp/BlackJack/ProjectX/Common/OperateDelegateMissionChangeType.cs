﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum OperateDelegateMissionChangeType
    {
        Occur = 1,
        Working = 2,
        Invading = 3,
        Complete = 4,
        Cancel = 5,
        CancelByPlayer = 6,
        Balance = 7
    }
}

