﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;

    public interface ICharacterSpaceDataContainer
    {
        bool GetIsInDuplicate();
        bool GetIsInSpace();
        Vector3D GetLocation();
        Vector3D GetRotation();
        ulong GetShipInstanceId();
        int GetSolarSystemId();
        int GetSpaceStationId();
        int GetWormholeGateSolarsystemId();
        void UpdateIsInDuplicate(bool inDuplicate);
        void UpdateIsInSpace(bool inSpace);
        void UpdatePose(Vector3D location, Vector3D roation);
        void UpdateShipInstanceId(ulong instanceId);
        void UpdateSolarSystemId(int solarSystemId);
        void UpdateSpaceStationId(int spaceStationId);
        void UpdateWormholeGateSolarsystemId(int solarSystemId);
    }
}

