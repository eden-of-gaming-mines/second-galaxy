﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct StructProcessEquipLaunchBase
    {
        private uint m_destTarget;
        private ushort m_chargeTime;
        [MethodImpl(0x8000)]
        public StructProcessEquipLaunchBase(ILBSpaceTarget target, ushort chargeTime)
        {
        }

        public uint GetDestTarget() => 
            this.m_destTarget;

        public ushort GetChargeTime() => 
            this.m_chargeTime;
    }
}

