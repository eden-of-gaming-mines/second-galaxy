﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum AuctionItemState
    {
        None,
        OnShelve,
        UnShelve,
        Expire,
        NeedDelete
    }
}

