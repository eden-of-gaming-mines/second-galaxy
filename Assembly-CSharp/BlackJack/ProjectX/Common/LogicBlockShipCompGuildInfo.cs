﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ToolUtil;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompGuildInfo
    {
        public DelegateEventEx<ILBSpaceTarget, uint, uint> EventOnShipGuildInfoUpdate;
        protected ILBInSpaceShip m_shipOwner;
        protected ShipGuildInfo m_shipGuildInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetGuildId;
        private static DelegateBridge __Hotfix_GetGuildCodeName;
        private static DelegateBridge __Hotfix_GetAllianceId;
        private static DelegateBridge __Hotfix_GetAllianceName;
        private static DelegateBridge __Hotfix_GetGuildFleetSetting;
        private static DelegateBridge __Hotfix_UpdateShipGuildInfo;
        private static DelegateBridge __Hotfix_UpdateGuildFleetPersonalSetting;

        [MethodImpl(0x8000)]
        public uint GetAllianceId()
        {
        }

        [MethodImpl(0x8000)]
        public string GetAllianceName()
        {
        }

        [MethodImpl(0x8000)]
        public string GetGuildCodeName()
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetPersonalSetting GetGuildFleetSetting()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildId()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip owner, ShipGuildInfo guildInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetPersonalSetting(GuildFleetPersonalSetting setting)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipGuildInfo(uint guildId, string codeName, uint allianceId, string allianceName)
        {
        }
    }
}

