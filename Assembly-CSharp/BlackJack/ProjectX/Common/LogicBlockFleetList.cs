﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockFleetList
    {
        protected IFleetListDataContainer m_fleetListDC;
        protected IShipHangarListDataContainer m_hangarDC;
        protected LogicBlockTechBase m_lbTech;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CreateFleet;
        private static DelegateBridge __Hotfix_DeleteFleet;
        private static DelegateBridge __Hotfix_ModifyFleet;
        private static DelegateBridge __Hotfix_CheckModifyFleet;
        private static DelegateBridge __Hotfix_CalcFleetShipCountMax;

        [MethodImpl(0x8000)]
        public int CalcFleetShipCountMax()
        {
        }

        [MethodImpl(0x8000)]
        protected int CheckModifyFleet(int fleetIndex, List<ulong> shipInstanceIdList)
        {
        }

        [MethodImpl(0x8000)]
        public bool CreateFleet(string name, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool DeleteFleet(int fleetIndex, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool ModifyFleet(int fleetIndex, List<ulong> shipInstanceIdList, out int errorCode)
        {
        }

        [CompilerGenerated]
        private sealed class <CreateFleet>c__AnonStorey0
        {
            internal string name;

            [MethodImpl(0x8000)]
            internal bool <>m__0(FleetInfo c)
            {
            }
        }
    }
}

