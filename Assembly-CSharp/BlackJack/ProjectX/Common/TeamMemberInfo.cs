﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;

    public class TeamMemberInfo
    {
        public string m_playerName;
        public int m_charLevel;
        public int m_charAvatarId;
        public string m_playerGameUserId;
        public ulong m_sessionId;
        public int m_currSolarSystemId;
        public Vector3D m_location;
        public bool m_isInSpace;
        public int m_currSpaceStationId;
        public int m_currShipConfigId;
        public string m_shipName;
        public ProfessionType m_profession;
        public uint m_guildId;
        public string m_guildCodeName;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

