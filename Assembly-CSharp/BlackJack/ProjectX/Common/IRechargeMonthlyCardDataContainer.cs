﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IRechargeMonthlyCardDataContainer
    {
        void AddRechargeMonthlyCard(RechargeMonthlyCardInfo monthlyCard);
        RechargeMonthlyCardInfo GetRechargeMonthlyCard(int cardId);
        List<RechargeMonthlyCardInfo> GetRechargeMonthlyCardList();
        int GetRechargeMonthlyCardListVersion();
        void SetMonthlyCardExpireTime(int cardId, DateTime expireTime);
        void SetMonthlyCardLastDailyRewardReceived(int cardId, bool isReceived = true);
    }
}

