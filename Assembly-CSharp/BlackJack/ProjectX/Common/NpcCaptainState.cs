﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Serializable, Flags]
    public enum NpcCaptainState : byte
    {
        Idle = 1,
        InDelegateMission = 2,
        InStationWorkingOnTech = 4,
        InStationWorkingOnProduction = 8,
        InStationWorking = 12
    }
}

