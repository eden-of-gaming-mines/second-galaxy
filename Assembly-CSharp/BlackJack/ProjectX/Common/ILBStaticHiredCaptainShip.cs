﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBStaticHiredCaptainShip : ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        ILBPlayerContext GetLBPlayerContext();
    }
}

