﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockQuestBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, int, int, int, bool, LBQuestEnv> EventOnQuestWaitForAcceptListAdd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnQuestWaitForAcceptListRemove;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBProcessingQuestBase> EventOnQuestAccept;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBProcessingQuestBase> EventOnQuestCompleteWait;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBProcessingQuestBase> EventOnQuestComplete;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBProcessingQuestBase, int, int> EventOnQuestCompleteCondParamUpdate;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBProcessingQuestBase> EventOnQuestCancel;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<QuestPostEventType, int, int> EventOnPostQuestEvent;
        protected Func<int> m_instanceIdAllocator;
        protected ILBPlayerContext m_lbPlayerCtx;
        protected LogicBlockStationContextBase m_lbStationCtx;
        protected IQuestDataContainer m_questDC;
        protected List<LBProcessingQuestBase> m_processingQuestList;
        protected List<LBQuestEnv> m_questEnvList;
        protected Random m_rand;
        protected const float QuestRewardFactionCreditFactor = 0.1f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CreateProcessingQuest;
        private static DelegateBridge __Hotfix_GetQuestEnvInstanceIdByQuestId;
        private static DelegateBridge __Hotfix_GetQuestEnvByQuestId;
        private static DelegateBridge __Hotfix_GetQuestEnv;
        private static DelegateBridge __Hotfix_QuestEnvOpen;
        private static DelegateBridge __Hotfix_QuestEnvClose;
        private static DelegateBridge __Hotfix_GetQuestWaitForAcceptList;
        private static DelegateBridge __Hotfix_IsQuestInWaitForAcceptList;
        private static DelegateBridge __Hotfix_QuestWaitForAcceptListAdd;
        private static DelegateBridge __Hotfix_QuestWaitForAcceptListRemove;
        private static DelegateBridge __Hotfix_GetProcessingQuestList;
        private static DelegateBridge __Hotfix_GetProcessingQuestByInstanceId;
        private static DelegateBridge __Hotfix_GetFirstProcessingQuestByConfigId;
        private static DelegateBridge __Hotfix_GetProcessingQuestListByConfigId;
        private static DelegateBridge __Hotfix_GetCurrChapterGoalQuest;
        private static DelegateBridge __Hotfix_GetCurrStoryQuest;
        private static DelegateBridge __Hotfix_IsQuestInProcessingByInstanceId;
        private static DelegateBridge __Hotfix_IsQuestInProcessingByConfigId;
        private static DelegateBridge __Hotfix_IsQuestWaitForComplete;
        private static DelegateBridge __Hotfix_IsQuestCompleted;
        private static DelegateBridge __Hotfix_IsQuestAutoComfirmComplete;
        private static DelegateBridge __Hotfix_HasQuest2CompleteWithNpc;
        private static DelegateBridge __Hotfix_PostQuestComplete;
        private static DelegateBridge __Hotfix_RemoveProcessingQuestDirectly;
        private static DelegateBridge __Hotfix_OnDailyRefresh;
        private static DelegateBridge __Hotfix_IsQuestCondCommitItemCountEnough;
        private static DelegateBridge __Hotfix_IsQuestVisiableForAccept;
        private static DelegateBridge __Hotfix_IsQuestVisiableForAcceptWithNpc;
        private static DelegateBridge __Hotfix_IsQuestAcceptable;
        private static DelegateBridge __Hotfix_IsQuestRetryableOnFail;
        private static DelegateBridge __Hotfix_IsQuestAcceptableWithNpc;
        private static DelegateBridge __Hotfix_CheckSingleAcceptCond;
        private static DelegateBridge __Hotfix_GetQuestConfigData;
        private static DelegateBridge __Hotfix_GetRealNpcDNIdWithEnv;
        private static DelegateBridge __Hotfix_GetRealScneSolarSystemIdWithEvn;
        private static DelegateBridge __Hotfix_CheckDailyFreeQuestBurstReward;
        private static DelegateBridge __Hotfix_GetDailyFreeQuestCompleteCount;
        private static DelegateBridge __Hotfix_GetDailyFreeQuestBurstRewardCompleteCount;
        private static DelegateBridge __Hotfix_GetDailyFreeQuestBurstRewardForExp;
        private static DelegateBridge __Hotfix_GetDailyFreeQuestBurstRewardForCurrency;
        private static DelegateBridge __Hotfix_GetDailyFreeQuestBurstRewardForItemCount;
        private static DelegateBridge __Hotfix_BalanceQuestRewardExp;
        private static DelegateBridge __Hotfix_BalanceQuestRewardBattlePassExp;
        private static DelegateBridge __Hotfix_GetRealAddExp_0;
        private static DelegateBridge __Hotfix_GetRealAddExp_1;
        private static DelegateBridge __Hotfix_GetExpReduceForSolarSystemQuestPoolCount;
        private static DelegateBridge __Hotfix_BalanceQuestRewardCurrency;
        private static DelegateBridge __Hotfix_GetRealAddCurrencyCount_0;
        private static DelegateBridge __Hotfix_GetRealAddCurrencyCount_1;
        private static DelegateBridge __Hotfix_GetCurrencyReduceForSolarSystemQuestPoolCount;
        private static DelegateBridge __Hotfix_GetFactionCreditCurrencyFactor;
        private static DelegateBridge __Hotfix_BalanceQuestRewardPlayerLevelExp;
        private static DelegateBridge __Hotfix_BalanceQuestRewardFactionCredit;
        private static DelegateBridge __Hotfix_GetRealAddFactionCredit;
        private static DelegateBridge __Hotfix_CreateQuestRewardItemInfo;
        private static DelegateBridge __Hotfix_GetRealRewardItemCount_0;
        private static DelegateBridge __Hotfix_GetRealRewardItemCount_1;
        private static DelegateBridge __Hotfix_BalanceQuestRewardScanProbe;
        private static DelegateBridge __Hotfix_GetRewardBonusMultiForUnacceptQuest;
        private static DelegateBridge __Hotfix_CreateQuestRewardGrandFactionItemInfo;
        private static DelegateBridge __Hotfix_FireEventOnQuestAccept;
        private static DelegateBridge __Hotfix_FireEventOnQuestComplete;
        private static DelegateBridge __Hotfix_FireEventOnQuestWaitForAcceptListAdd;
        private static DelegateBridge __Hotfix_FireEventOnQuestCompleteCondParamUpdate;
        private static DelegateBridge __Hotfix_FireEventOnQuestCompleteWait;
        private static DelegateBridge __Hotfix_FireEventOnQuestCancel;
        private static DelegateBridge __Hotfix_FireEventOnPostQuestEvent;
        private static DelegateBridge __Hotfix_add_EventOnQuestWaitForAcceptListAdd;
        private static DelegateBridge __Hotfix_remove_EventOnQuestWaitForAcceptListAdd;
        private static DelegateBridge __Hotfix_add_EventOnQuestWaitForAcceptListRemove;
        private static DelegateBridge __Hotfix_remove_EventOnQuestWaitForAcceptListRemove;
        private static DelegateBridge __Hotfix_add_EventOnQuestAccept;
        private static DelegateBridge __Hotfix_remove_EventOnQuestAccept;
        private static DelegateBridge __Hotfix_add_EventOnQuestCompleteWait;
        private static DelegateBridge __Hotfix_remove_EventOnQuestCompleteWait;
        private static DelegateBridge __Hotfix_add_EventOnQuestComplete;
        private static DelegateBridge __Hotfix_remove_EventOnQuestComplete;
        private static DelegateBridge __Hotfix_add_EventOnQuestCompleteCondParamUpdate;
        private static DelegateBridge __Hotfix_remove_EventOnQuestCompleteCondParamUpdate;
        private static DelegateBridge __Hotfix_add_EventOnQuestCancel;
        private static DelegateBridge __Hotfix_remove_EventOnQuestCancel;
        private static DelegateBridge __Hotfix_add_EventOnPostQuestEvent;
        private static DelegateBridge __Hotfix_remove_EventOnPostQuestEvent;

        public event Action<QuestPostEventType, int, int> EventOnPostQuestEvent
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProcessingQuestBase> EventOnQuestAccept
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProcessingQuestBase> EventOnQuestCancel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProcessingQuestBase> EventOnQuestComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProcessingQuestBase, int, int> EventOnQuestCompleteCondParamUpdate
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProcessingQuestBase> EventOnQuestCompleteWait
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, int, int, int, bool, LBQuestEnv> EventOnQuestWaitForAcceptListAdd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnQuestWaitForAcceptListRemove
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected virtual int BalanceQuestRewardBattlePassExp(LBProcessingQuestBase quest, int confAddExp)
        {
        }

        [MethodImpl(0x8000)]
        protected int BalanceQuestRewardCurrency(LBProcessingQuestBase quest, CurrencyType type, int confCount)
        {
        }

        [MethodImpl(0x8000)]
        protected uint BalanceQuestRewardExp(LBProcessingQuestBase quest, int confAddExp)
        {
        }

        [MethodImpl(0x8000)]
        protected int BalanceQuestRewardFactionCredit(LBProcessingQuestBase quest, float addValue)
        {
        }

        [MethodImpl(0x8000)]
        protected uint BalanceQuestRewardPlayerLevelExp(int coefficient)
        {
        }

        [MethodImpl(0x8000)]
        protected void BalanceQuestRewardScanProbe(int probeType, int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDailyFreeQuestBurstReward(ConfigDataQuestInfo questConf)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSingleAcceptCond(QuestAcceptCondInfo cond)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBProcessingQuestBase CreateProcessingQuest(QuestProcessingInfo questInfo, LBQuestEnv env)
        {
        }

        [MethodImpl(0x8000)]
        protected ItemInfo CreateQuestRewardGrandFactionItemInfo(LBProcessingQuestBase quest, int confId, bool isBind)
        {
        }

        [MethodImpl(0x8000)]
        protected ItemInfo CreateQuestRewardItemInfo(LBProcessingQuestBase quest, StoreItemType type, int configId, int confCount, bool isBind)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnPostQuestEvent(QuestPostEventType postEventType, int p1, int p2)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnQuestAccept(LBProcessingQuestBase acceptedQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnQuestCancel(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnQuestComplete(LBProcessingQuestBase acceptedQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnQuestCompleteCondParamUpdate(LBProcessingQuestBase quest, int condIndex, int param)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnQuestCompleteWait(LBProcessingQuestBase acceptedQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected void FireEventOnQuestWaitForAcceptListAdd(int questId, int questFactionId, int questLevel, int questSolarSystemId, bool fromQuestCancel, LBQuestEnv questEnv)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase GetCurrChapterGoalQuest()
        {
        }

        [MethodImpl(0x8000)]
        public static float GetCurrencyReduceForSolarSystemQuestPoolCount(int questPoolCount, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase GetCurrStoryQuest()
        {
        }

        [MethodImpl(0x8000)]
        public int GetDailyFreeQuestBurstRewardCompleteCount()
        {
        }

        [MethodImpl(0x8000)]
        public float GetDailyFreeQuestBurstRewardForCurrency()
        {
        }

        [MethodImpl(0x8000)]
        public float GetDailyFreeQuestBurstRewardForExp()
        {
        }

        [MethodImpl(0x8000)]
        public float GetDailyFreeQuestBurstRewardForItemCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetDailyFreeQuestCompleteCount()
        {
        }

        [MethodImpl(0x8000)]
        public static float GetExpReduceForSolarSystemQuestPoolCount(int questPoolCount, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetFactionCreditCurrencyFactor(ConfigDataQuestInfo questConf, CurrencyType type, ILBPlayerContext lbPlayerContext)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase GetFirstProcessingQuestByConfigId(int questConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase GetProcessingQuestByInstanceId(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBProcessingQuestBase> GetProcessingQuestList()
        {
        }

        [MethodImpl(0x8000)]
        public void GetProcessingQuestListByConfigId(int questConfigId, out List<LBProcessingQuestBase> questList)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestInfo GetQuestConfigData(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected LBQuestEnv GetQuestEnv(int envTypeId)
        {
        }

        [MethodImpl(0x8000)]
        public LBQuestEnv GetQuestEnvByQuestId(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestEnvInstanceIdByQuestId(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestWaitForAcceptInfo> GetQuestWaitForAcceptList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRealAddCurrencyCount(LBProcessingQuestBase quest, CurrencyType type, int confCount)
        {
        }

        [MethodImpl(0x8000)]
        public int GetRealAddCurrencyCount(ConfigDataQuestInfo questInfo, CurrencyType type, int confCount, float bonusMulti, float currencyReduce)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetRealAddExp(LBProcessingQuestBase quest, int confAddExp)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetRealAddExp(ConfigDataQuestInfo questInfo, int confAddExp, float bonusMulti, float expReduce)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetRealAddFactionCredit(float confCount, float bonusMulti)
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId GetRealNpcDNIdWithEnv(int srcNpcDNIdSolarSystemId, int srcNpcDNIdSpaceStationId, int srcNpcDNIdNpcId, int envTypeId)
        {
        }

        [MethodImpl(0x8000)]
        public long GetRealRewardItemCount(LBProcessingQuestBase quest, int confCount)
        {
        }

        [MethodImpl(0x8000)]
        public long GetRealRewardItemCount(ConfigDataQuestInfo quest, int confCount, float bonusMulti)
        {
        }

        [MethodImpl(0x8000)]
        public int GetRealScneSolarSystemIdWithEvn(int questSceneSolarSystemId, bool takeQuestSceneSolarSystemIdAsEnvIndex, int envTypeId)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetRewardBonusMultiForUnacceptQuest(ConfigDataQuestInfo questInfo, out float rewardBonusMulti)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasQuest2CompleteWithNpc(NpcDNId m_npcDNId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx, Func<int> instanceIdAllocator = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestAcceptable(int questId, out int errCode, bool checkQuestSrc = true, bool checkQuestAcceptCnd = true, bool checkRepeatQuestId = true, int replacedQuestLevel = 0, int replacedSolarSystemId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestAcceptableWithNpc(int questId, NpcDNId npcDNId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestAutoComfirmComplete(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestCompleted(int questId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsQuestCondCommitItemCountEnough(QuestCompleteCondInfo condInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestInProcessingByConfigId(int questConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestInProcessingByInstanceId(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestInWaitForAcceptList(int questId, int replacedQuestLevel = 0, int replacedSolarSystemId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestRetryableOnFail(int questConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestVisiableForAccept(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsQuestVisiableForAcceptWithNpc(int questId, NpcDNId npcDNId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestWaitForComplete(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDailyRefresh()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PostQuestComplete(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void QuestEnvClose(int envTypeId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool QuestEnvOpen(QuestEnvirmentInfo envInfo, out LBQuestEnv env, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void QuestWaitForAcceptListAdd(int questId, int questFactionId = 0, bool fromQuestCancel = false, int questLevel = 0, int questSolarSystemId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void QuestWaitForAcceptListRemove(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveProcessingQuestDirectly(LBProcessingQuestBase questInfo)
        {
        }
    }
}

