﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    public class RankingListInfo
    {
        public RankingListType m_type;
        public int m_score;
        public int m_currRank;
        public List<RankingTargetInfo> m_targetList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

