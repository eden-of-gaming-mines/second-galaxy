﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class LBSynEventResetWeaponEquipGroupCD : LBSyncEvent
    {
        public List<uint> m_highSlotGroupCDEndTime;
        public List<uint> m_middleSlotGroupCDEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

