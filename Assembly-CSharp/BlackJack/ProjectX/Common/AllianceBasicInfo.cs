﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class AllianceBasicInfo
    {
        public uint m_allianceId;
        public string m_name;
        public string m_announcement;
        public GuildAllianceLanguageType m_languageType;
        public AllianceLogoInfo m_logoInfo;
        public uint m_leaderGuildId;
        public string m_leaderGuildCode;
        public string m_leaderGuildName;
        public DateTime m_createTime;
        public uint m_createGuildId;
        public string m_createGuildName;
        public string m_createGuildCode;
        public string m_createGameUserId;
        public string m_createGameUserName;
        public int m_occupiedSolarSystemCount;
        public int m_totalGameUserCount;
        public int m_totalGuildCount;
        public int m_totalEvaluateScore;
        [NonSerialized]
        public DateTime m_cacheExpireTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

