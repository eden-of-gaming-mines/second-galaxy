﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildInfo
    {
        public GuildDynamicInfo m_dynamicInfo;
        public GuildBasicInfo m_basicInfo;
        public GuildSimpleRuntimeInfo m_simpleRuntimeInfo;
        public GuildMemberListInfo m_memeberListInfo;
        public GuildJoinApplyListInfo m_joinApplyListInfo;
        public GuildStaffingLogListInfo m_staffingLogInfo;
        public DiplomacyInfo m_diplomacyInfo;
        public GuildMessageBoardInfo m_messageBoardInfo;
        public List<GuildMomentsInfo> m_momentsInfoList;
        public List<GuildStoreItemListInfo> m_itemList;
        public List<GuildActionInfo> m_actionList;
        public List<GuildProductionLineInfo> m_productionLineList;
        public GuildFleetListInfo m_guildFleetList;
        public GuildCurrencyInfo m_currency;
        public List<GuildCurrencyLogInfo> m_currencyLogInfo;
        public List<GuildSolarSystemInfo> m_solarSystemInfoList;
        public List<GuildPurchaseOrderInfo> m_purchaseInfo;
        public GuildShipCustomTemplateInfo m_customTemplateInfo;
        public List<LossCompensation> m_lossCompensationList;
        public List<ulong> m_guildBattleInsIdList;
        public GuildMiningInfo m_miningInfo;
        public List<GuildTradePortExtraInfo> m_guildTradePortExtraInfoList;
        public List<GuildBenefitsInfoBase> m_guildBenefitsList;
        public List<GuildAllianceInviteInfo> m_guildAllianceInviteList;
        public List<BuildingLostReport> m_buildingLostReportList;
        public List<SovereignLostReport> m_sovereignLostReportList;
        public List<GuildFlagShipHangarDetailInfo> m_guildFlagShipHangarList;
        public List<GuildFlagShipOptLogInfo> m_guildFlagShipOptLogInfoList;
        public List<GuildAntiTeleportEffectInfo> m_guildAntiTeleportEffectInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

