﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Flags]
    public enum SyncEventFlag : uint
    {
        None = 0,
        ShieldBroken = 1,
        Dead = 2,
        EnterFight = 4,
        LeaveFight = 8
    }
}

