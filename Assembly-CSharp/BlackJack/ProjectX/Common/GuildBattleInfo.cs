﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildBattleInfo
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public GuildBattleType m_type;
        public DateTime m_startTime;
        public DateTime m_endTime;
        public GuildSimplestInfo m_attackerGuildInfo;
        public GuildSimplestInfo m_defenderGuildInfo;
        public int m_npcGuildConfigId;
        public bool m_isAttackerWin;
        public GuildBattleStatus m_status;
        public DateTime m_currStatusStartTime;
        public DateTime m_currStatusEndTime;
        public int m_reinforcementEndHour;
        public int m_reinforcementEndMinute;
        public List<GuildBuildingBattleInfo> m_relativeBuildingInfoList;
        public double m_totalKillingLostBindMoney;
        public int m_totalSelfLostShipCount;
        public double m_defenderKillingLostBindMoney;
        public double m_defenderSelfLostBindMoney;
        public int m_defenderSelfLostShipCount;
        public double m_attackerKillingLostBindMoney;
        public double m_attackerSelfLostBindMoney;
        public int m_attackerSelfLostShipCount;
        public List<GuildBattlePlayerKillLostStatInfo> m_playerKillLostStatisticsInfoList;
        public List<GuildBattleGuildKillLostStatInfo> m_guildKillLostStatisticsInfoList;
        public List<GuildBattleShipLostStatInfo> m_shipLostStatisticsInfoList;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildBattleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBattleInfo(ulong instanceId, int solarSystemId, GuildBattleType battleType, int npcGuildConfigId, DateTime startTime, GuildSimplestInfo attackerGuild, GuildSimplestInfo defenderGuild, int battlePrerareTime, int reinforcementEndHour, int reinforcementEndMinute)
        {
        }
    }
}

