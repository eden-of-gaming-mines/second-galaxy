﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompIFF4HiredCaptain : LogicBlockShipCompIFFBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitFactionCredit;
        private static DelegateBridge __Hotfix_GetFactionId;
        private static DelegateBridge __Hotfix_GetParentFactionId;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4Player;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4HiredCaptain;
        private static DelegateBridge __Hotfix_GetIFFStateForTargetInternal4NpcShip;

        [MethodImpl(0x8000)]
        public override int GetFactionId()
        {
        }

        [MethodImpl(0x8000)]
        protected override IFFState GetIFFStateForTargetInternal4HiredCaptain(ILBInSpaceNpcShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public override IFFState GetIFFStateForTargetInternal4NpcShip(ILBInSpaceNpcShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected override IFFState GetIFFStateForTargetInternal4Player(ILBInSpacePlayerShip targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetParentFactionId()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitFactionCredit()
        {
        }
    }
}

