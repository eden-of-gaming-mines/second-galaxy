﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface ICustomizedParameterDataContainer
    {
        void AddOrUpdateParameter(CustomizedParameter parameterId, string value);
        List<CustomizedParameterInfo> GetCustomizedParameterList();
        string GetParameterValue(CustomizedParameter parameterId);
        bool IsContainParameter(CustomizedParameter parameterId);
        void RemoveParameter(CustomizedParameter parameterId);
    }
}

