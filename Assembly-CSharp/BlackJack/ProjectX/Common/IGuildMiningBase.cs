﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildMiningBase
    {
        int GetGuildMiningBalanceTimeHour();
        int GetGuildMiningBalanceTimeMinute();
    }
}

