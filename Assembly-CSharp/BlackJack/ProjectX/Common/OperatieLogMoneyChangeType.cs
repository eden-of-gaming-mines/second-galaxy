﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum OperatieLogMoneyChangeType
    {
        None,
        GM,
        Produce,
        Tech,
        Crack,
        Quest,
        Mail,
        Activity,
        NpcShop,
        BindMoneyBuyAmmo,
        CharacterSkill,
        ShipCustomTemplateApply,
        UserGuide,
        UseItem,
        BaseRedeploy,
        WormHole,
        Auction,
        GuildChangeNameAndCode,
        GuildCreate,
        GuildLogoSet,
        CharChipUnlock,
        GuildDonate,
        BlackMarket,
        GuildPurchase,
        GuildPurchaseTradeTax,
        GuildBenefits,
        TradeNpcShop,
        FactionCreditWeeklyReward,
        FactionCreditLevelReward,
        Devlopment,
        HangarShipSalvage,
        BattlePass,
        Compensation,
        RechargeGiftPackage,
        RechargeMonthlyCard,
        RechargeItem,
        NpcShopTradeBuy,
        NpcShopPanGala,
        NpcShopFaction,
        NpcShopGuildGala,
        BattlePassRmbUpgrade,
        BattlePassLevelPurchase,
        AuctionFee
    }
}

