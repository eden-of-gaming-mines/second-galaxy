﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Remoting.Contexts;

    [Synchronization]
    public class StarMapGuildOccupyInfoManager : ContextBoundObject
    {
        private readonly IStarMapGuildOccupyDataContainer m_dc;
        private readonly GDBHelper m_gdbHelper;
        private readonly Dictionary<int, uint> m_solarSystemGuildOccupyDict;
        private readonly Dictionary<int, uint> m_solarSystemAllianceOccupyDict;
        private readonly Dictionary<int, StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo> m_solarSystemDynamicInfoDict;
        private readonly Dictionary<uint, StarMapGuildSimpleInfo> m_guildSimpleInfoDict;
        private readonly Dictionary<uint, StarMapAllianceSimpleInfo> m_allianceSimpleInfoDict;
        protected const int StarMapGuildOccupySaveDBTimeCD = 120;
        protected DateTime m_lastSaveDBTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_StarMapGuildOccupyInfoAdd;
        private static DelegateBridge __Hotfix_StarMapGuildOccupyInfoRemove;
        private static DelegateBridge __Hotfix_StarMapAllianceOccupyInfoAdd;
        private static DelegateBridge __Hotfix_StarMapAllianceOccupyInfoRemove;
        private static DelegateBridge __Hotfix_StarMapGuildSimpleInfoAddOrUpdate;
        private static DelegateBridge __Hotfix_StarMapAllianceSimpleInfoAddOrUpdate;
        private static DelegateBridge __Hotfix_StarMapSolarSystemDynamicInfoAddOrUpdate;
        private static DelegateBridge __Hotfix_StarMapGuildOccupyStaticInfoUpdate;
        private static DelegateBridge __Hotfix_StarMapGuildOccupyDynamicInfoUpdate;
        private static DelegateBridge __Hotfix_GetStarMapGuildOccupyStaticProInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildOccupyDynamicProInfo;
        private static DelegateBridge __Hotfix_GetSolarSystemOccpuyGuild;
        private static DelegateBridge __Hotfix_GetSolarSystemDynamicInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildSimpleInfo;
        private static DelegateBridge __Hotfix_GetStarMapAllianceSimpleInfo;
        private static DelegateBridge __Hotfix_GetStarMapSolarSystemGuildOccupyDict;
        private static DelegateBridge __Hotfix_GetStarMapGuildOccupyStaticInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildOccupyDynamicInfo;

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyInfoManager(IStarMapGuildOccupyDataContainer dc, GDBHelper gdbHelper)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo.SolarSystemDynamicInfo GetSolarSystemDynamicInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetSolarSystemOccpuyGuild(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapAllianceSimpleInfo GetStarMapAllianceSimpleInfo(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyDynamicInfo GetStarMapGuildOccupyDynamicInfo(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapGuildOccupyDynamicInfo GetStarMapGuildOccupyDynamicProInfo(int starFieldId, out int version)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyStaticInfo GetStarMapGuildOccupyStaticInfo(int starFieldId)
        {
        }

        [MethodImpl(0x8000)]
        public ProStarMapGuildOccupyStaticInfo GetStarMapGuildOccupyStaticProInfo(int starFieldId, out int version)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo GetStarMapGuildSimpleInfo(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, uint> GetStarMapSolarSystemGuildOccupyDict()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize()
        {
        }

        [MethodImpl(0x8000)]
        public void OnTick(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapAllianceOccupyInfoAdd(int starFieldId, int solarSystemId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapAllianceOccupyInfoRemove(int starFieldId, int solarSystemId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapAllianceSimpleInfoAddOrUpdate(int starFieldId, StarMapAllianceSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapGuildOccupyDynamicInfoUpdate(StarMapGuildOccupyDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapGuildOccupyInfoAdd(int starFieldId, int solarSystemId, uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapGuildOccupyInfoRemove(int starFieldId, int solarSystemId, uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapGuildOccupyStaticInfoUpdate(StarMapGuildOccupyStaticInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapGuildSimpleInfoAddOrUpdate(int starFieldId, StarMapGuildSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void StarMapSolarSystemDynamicInfoAddOrUpdate(int starFieldId, int solarSystemId, int? flourishValue, bool? isInGuildBattle)
        {
        }
    }
}

