﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;

    public interface IActivityDataContainer
    {
        bool CheckVitalityRewardGet(int index);
        void ClearPlayerActivityInfo();
        void ClearVitalityRewardList();
        int GetActivityPoint();
        List<PlayerActivityInfo> GetPlayerActivityList();
        int GetPlayerActivityPlayedCount(ActivityType type);
        int GetVitality();
        List<int> GetVitalityRewardList();
        void RemovePlayerActivityInfo(ActivityType type);
        void SetActivityPoint(int point);
        void SetVitality(int vitality);
        void UpdatePlayerActivityInfo(PlayerActivityInfo info);
        void VitalityGetReward(int index);
    }
}

