﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBSolarSystemInfo")]
    public class GDBSolarSystemInfo : IExtensible
    {
        private int _Id;
        private string _name;
        private float _gLocationX;
        private float _gLocationZ;
        private GDBStarInfo _star;
        private readonly List<GDBPlanetInfo> _planets;
        private readonly List<GDBPlanetLikeBuildingInfo> _planetLikeBuildings;
        private GDBAsteroidBeltInfo _asteroidBeltInfo;
        private readonly List<GDBSpaceStationInfo> _spaceStations;
        private readonly List<GDBStargateInfo> _starGates;
        private readonly List<GDBWormholeGateInfo> _wormholeGates;
        private readonly List<GDBSceneDummyInfo> _staticScenes;
        private int _templateId;
        private bool _isNeedLocalization;
        private string _localizationKey;
        private GDBSolarSystemEditorChannelInfo _EditorChannelInfo;
        private bool _isHide;
        private bool _isBackToMotherShip;
        private int _solarsystemType;
        private bool _IsNotInfectable;
        private readonly List<IdWeightInfo> _SpecialMineralList;
        private readonly List<GuildBuildingLocationInfo> _GuildBuildingLocationList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_GLocationX;
        private static DelegateBridge __Hotfix_set_GLocationX;
        private static DelegateBridge __Hotfix_get_GLocationZ;
        private static DelegateBridge __Hotfix_set_GLocationZ;
        private static DelegateBridge __Hotfix_get_Star;
        private static DelegateBridge __Hotfix_set_Star;
        private static DelegateBridge __Hotfix_get_Planets;
        private static DelegateBridge __Hotfix_get_PlanetLikeBuildings;
        private static DelegateBridge __Hotfix_get_AsteroidBeltInfo;
        private static DelegateBridge __Hotfix_set_AsteroidBeltInfo;
        private static DelegateBridge __Hotfix_get_SpaceStations;
        private static DelegateBridge __Hotfix_get_StarGates;
        private static DelegateBridge __Hotfix_get_WormholeGates;
        private static DelegateBridge __Hotfix_get_StaticScenes;
        private static DelegateBridge __Hotfix_get_TemplateId;
        private static DelegateBridge __Hotfix_set_TemplateId;
        private static DelegateBridge __Hotfix_get_IsNeedLocalization;
        private static DelegateBridge __Hotfix_set_IsNeedLocalization;
        private static DelegateBridge __Hotfix_get_LocalizationKey;
        private static DelegateBridge __Hotfix_set_LocalizationKey;
        private static DelegateBridge __Hotfix_get_EditorChannelInfo;
        private static DelegateBridge __Hotfix_set_EditorChannelInfo;
        private static DelegateBridge __Hotfix_get_IsHide;
        private static DelegateBridge __Hotfix_set_IsHide;
        private static DelegateBridge __Hotfix_get_IsBackToMotherShip;
        private static DelegateBridge __Hotfix_set_IsBackToMotherShip;
        private static DelegateBridge __Hotfix_get_SolarsystemType;
        private static DelegateBridge __Hotfix_set_SolarsystemType;
        private static DelegateBridge __Hotfix_get_IsNotInfectable;
        private static DelegateBridge __Hotfix_set_IsNotInfectable;
        private static DelegateBridge __Hotfix_get_SpecialMineralList;
        private static DelegateBridge __Hotfix_get_GuildBuildingLocationList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="gLocationX", DataFormat=DataFormat.FixedSize)]
        public float GLocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="gLocationZ", DataFormat=DataFormat.FixedSize)]
        public float GLocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="star", DataFormat=DataFormat.Default)]
        public GDBStarInfo Star
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="planets", DataFormat=DataFormat.Default)]
        public List<GDBPlanetInfo> Planets
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="planetLikeBuildings", DataFormat=DataFormat.Default)]
        public List<GDBPlanetLikeBuildingInfo> PlanetLikeBuildings
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="asteroidBeltInfo", DataFormat=DataFormat.Default)]
        public GDBAsteroidBeltInfo AsteroidBeltInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="spaceStations", DataFormat=DataFormat.Default)]
        public List<GDBSpaceStationInfo> SpaceStations
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="starGates", DataFormat=DataFormat.Default)]
        public List<GDBStargateInfo> StarGates
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x13, Name="wormholeGates", DataFormat=DataFormat.Default)]
        public List<GDBWormholeGateInfo> WormholeGates
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, Name="staticScenes", DataFormat=DataFormat.Default)]
        public List<GDBSceneDummyInfo> StaticScenes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="templateId", DataFormat=DataFormat.TwosComplement)]
        public int TemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="isNeedLocalization", DataFormat=DataFormat.Default)]
        public bool IsNeedLocalization
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(14, IsRequired=false, Name="localizationKey", DataFormat=DataFormat.Default)]
        public string LocalizationKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="EditorChannelInfo", DataFormat=DataFormat.Default)]
        public GDBSolarSystemEditorChannelInfo EditorChannelInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="isHide", DataFormat=DataFormat.Default)]
        public bool IsHide
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="isBackToMotherShip", DataFormat=DataFormat.Default)]
        public bool IsBackToMotherShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="solarsystemType", DataFormat=DataFormat.TwosComplement)]
        public int SolarsystemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=true, Name="IsNotInfectable", DataFormat=DataFormat.Default)]
        public bool IsNotInfectable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, Name="SpecialMineralList", DataFormat=DataFormat.Default)]
        public List<IdWeightInfo> SpecialMineralList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x16, Name="GuildBuildingLocationList", DataFormat=DataFormat.Default)]
        public List<GuildBuildingLocationInfo> GuildBuildingLocationList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

