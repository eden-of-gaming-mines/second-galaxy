﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct CurrencyUpdateInfo
    {
        public CurrencyType m_type;
        public ulong m_count;
        public long m_changeValue;
        public bool IsEmptyInfo() => 
            (this.m_type == ((CurrencyType) 0));
    }
}

