﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildFlagShipHangarBasicInfo
    {
        public int m_solarSystemId;
        public ulong m_buildingInstanceId;
        public int m_buildingLevel;
        public bool m_isDestroyed;
        public bool m_isRecycled;
        public DateTime m_deployedTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

