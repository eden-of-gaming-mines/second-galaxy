﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupAttachBufAndInvisibleByArmorPercentBase : LBTacticalEquipGroupBase, IEquipFunctionCompInvisibleOwnerBase, IEquipFunctionCompOwner
    {
        protected bool m_isAmrorBeHit;
        protected EquipFunctionCompArmorPercentTrigger m_compArmorTriger;
        protected EquipFunctionCompInvisibleBase m_compInvisible;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetOwnerShip;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetPropertiesCalc;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSlotType;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSlotGroupIndex;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetEquipType;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetConfBufIdList;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSelfBufIdList;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcChargeTime;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.AttachBufByEquip2Target;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompInvisibleOwnerBase.GetChargeBufId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompInvisibleOwnerBase.GetLoopBufId;
        private static DelegateBridge __Hotfix_GetSelfBufId;
        private static DelegateBridge __Hotfix_GetArmorPercentInConf;
        private static DelegateBridge __Hotfix_GetBufId4TrigerAttach;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupAttachBufAndInvisibleByArmorPercentBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompInvisibleOwnerBase.GetChargeBufId()
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompInvisibleOwnerBase.GetLoopBufId()
        {
        }

        [MethodImpl(0x8000)]
        ulong IEquipFunctionCompOwner.AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        List<int> IEquipFunctionCompOwner.GetConfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        EquipType IEquipFunctionCompOwner.GetEquipType()
        {
        }

        [MethodImpl(0x8000)]
        ILBInSpaceShip IEquipFunctionCompOwner.GetOwnerShip()
        {
        }

        [MethodImpl(0x8000)]
        PropertiesCalculaterBase IEquipFunctionCompOwner.GetPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        IEnumerable<int> IEquipFunctionCompOwner.GetSelfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompOwner.GetSlotGroupIndex()
        {
        }

        [MethodImpl(0x8000)]
        ShipEquipSlotType IEquipFunctionCompOwner.GetSlotType()
        {
        }

        protected abstract EquipFunctionCompInvisibleBase CreateCompInvisible();
        [MethodImpl(0x8000)]
        protected float GetArmorPercentInConf()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetBufId4TrigerAttach()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSelfBufId()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        protected abstract void OnEnterTrigerArmorPercent();
        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

