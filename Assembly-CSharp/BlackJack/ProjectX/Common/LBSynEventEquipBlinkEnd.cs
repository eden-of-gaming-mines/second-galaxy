﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class LBSynEventEquipBlinkEnd : LBSyncEvent
    {
        public ShipEquipSlotType m_equipSlotType;
        public int m_equipSlotIndex;
        public bool m_blinkSuccessful;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

