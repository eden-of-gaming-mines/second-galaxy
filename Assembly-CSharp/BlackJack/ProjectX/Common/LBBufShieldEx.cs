﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBBufShieldEx : LBShipFightBuf
    {
        protected float m_valueLeft;
        protected float m_valueTotal;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetValueLeft;
        private static DelegateBridge __Hotfix_GetValueTotal;
        private static DelegateBridge __Hotfix_SetValueLeft;
        private static DelegateBridge __Hotfix_OnAttach;
        private static DelegateBridge __Hotfix_OnAttachDuplicate;
        private static DelegateBridge __Hotfix_OnSyncAttachDuplicateInfo;

        [MethodImpl(0x8000)]
        public LBBufShieldEx(ConfigDataBufInfo confInfo, uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public float GetValueLeft()
        {
        }

        [MethodImpl(0x8000)]
        public float GetValueTotal()
        {
        }

        [MethodImpl(0x8000)]
        public override void OnAttach(ILBBufSource source, ILBBufOwner owner, uint currTime, uint lifeEndTime, LBBuffContainer container, object param = null, float bufInstanceParam1 = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnAttachDuplicate(ILBBufSource source, float additionalBufInstanceParam1 = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnSyncAttachDuplicateInfo(List<uint> sourceTargetList, ILBBufOwner owner, uint currTime, uint lifeEndTime, LBBuffContainer container, int attachCount, object param = null, float bufInstanceParam = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        [MethodImpl(0x8000)]
        public void SetValueLeft(float valueLeft)
        {
        }
    }
}

