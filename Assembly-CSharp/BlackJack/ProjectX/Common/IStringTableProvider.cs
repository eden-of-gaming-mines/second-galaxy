﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IStringTableProvider
    {
        string GetStringInDefaultStringTable(string key);
        string GetStringInGDBStringTable(string key);
        string GetStringInStoryStringTable(string key);
    }
}

