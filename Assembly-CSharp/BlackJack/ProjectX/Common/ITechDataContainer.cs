﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface ITechDataContainer
    {
        void AddTech(int techId, int level);
        List<IdLevelInfo> GetTechList();
        TechUpgradeInfo GetTechUpgradeInfo(int id);
        List<TechUpgradeInfo> GetTechUpgradeList();
        void RemoveTech(int id);
        void RemoveTechUpgradeInfo(int id);
        void UpdateTechUpgradeInfo(TechUpgradeInfo info);
    }
}

