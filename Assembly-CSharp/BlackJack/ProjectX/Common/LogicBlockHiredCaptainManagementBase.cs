﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockHiredCaptainManagementBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventAddCaptain;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected ILBPlayerContext m_playerContext;
        protected IHiredCaptainManagementDataContainer m_dc;
        protected List<LBStaticHiredCaptain> m_captainList;
        protected LBStaticHiredCaptain m_currActiveWingMan;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetCaptainList;
        private static DelegateBridge __Hotfix_GetCaptainCountBySubRankType;
        private static DelegateBridge __Hotfix_GetCaptainByInstanceId;
        private static DelegateBridge __Hotfix_AddCaptain;
        private static DelegateBridge __Hotfix_RemoveCaptain;
        private static DelegateBridge __Hotfix_IsCaptainWingMan;
        private static DelegateBridge __Hotfix_CheckAddExp2Captain;
        private static DelegateBridge __Hotfix_CheckLearnFeatsBook;
        private static DelegateBridge __Hotfix_CheckCaptainRetire;
        private static DelegateBridge __Hotfix_CalcRetireExp;
        private static DelegateBridge __Hotfix_CalcRetireFeatsReserveRate;
        private static DelegateBridge __Hotfix_CheckSetCurrShip;
        private static DelegateBridge __Hotfix_CheckUnlockShip;
        private static DelegateBridge __Hotfix_CheckUnlockShipCompList;
        private static DelegateBridge __Hotfix_CheckRepairShip;
        private static DelegateBridge __Hotfix_CheckRepairShipCompList;
        private static DelegateBridge __Hotfix_CheckCaptainWingManSet;
        private static DelegateBridge __Hotfix_CheckCaptainWingManSummon;
        private static DelegateBridge __Hotfix_GetWingManList;
        private static DelegateBridge __Hotfix_HasAvailableWingMan;
        private static DelegateBridge __Hotfix_GetAvailableWingMan;
        private static DelegateBridge __Hotfix_CheckWingShipEnable;
        private static DelegateBridge __Hotfix_InitCaptains;
        private static DelegateBridge __Hotfix_CreateCaptain;
        private static DelegateBridge __Hotfix_InitCaptainShips;
        private static DelegateBridge __Hotfix_TryCreateStaticShip4Feats;
        private static DelegateBridge __Hotfix_AddAddtionFeats2Captain;
        private static DelegateBridge __Hotfix_add_EventAddCaptain;
        private static DelegateBridge __Hotfix_remove_EventAddCaptain;

        public event Action<ulong> EventAddCaptain
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void AddAddtionFeats2Captain(LBStaticHiredCaptain captain, int festsId, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected bool AddCaptain(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        protected long CalcRetireExp(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcRetireFeatsReserveRate(LBStaticHiredCaptain captain, int selectedFeatsId, int useExtraItemCount)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckAddExp2Captain(LBStaticHiredCaptain captain, long addExp, out long wasteExp, out bool isTopLevel, out bool arriveTopLevel)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCaptainRetire(LBStaticHiredCaptain captain, int useExtraItemCount, int selectedFeatsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCaptainWingManSet(int srcIndex, int destIndex, ulong srcCaptainInstanceId, out ulong destCaptainInstanceId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCaptainWingManSummon(List<ShipType> sceneShipTypeLimit, out LBStaticHiredCaptain captain, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckLearnFeatsBook(LBStaticHiredCaptain captain, int featsBookItemIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRepairShip(ulong captainInstanceId, int npcCaptainShipId, out int errcode, out List<ShipCompListConfInfo> repairShipCompList, out float needBindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRepairShipCompList(LBStaticHiredCaptain.ShipInfo shipInfo, out int errcode, out List<ShipCompListConfInfo> repairShipCompList, out float needBindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSetCurrShip(ulong captainInstanceId, int npcCaptainShipId, out int errcode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckUnlockShip(ulong captainInstanceId, int npcCaptainShipId, out int errcode, out List<ShipCompListConfInfo> unlockShipCompList, out float needBindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckUnlockShipCompList(LBStaticHiredCaptain.ShipInfo shipInfo, out int errcode, out List<ShipCompListConfInfo> unlockShipCompList, out float needBindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckWingShipEnable()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticHiredCaptain CreateCaptain(NpcCaptainInfo captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetAvailableWingMan()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain GetCaptainByInstanceId(ulong captainInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCaptainCountBySubRankType(SubRankType rank)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticHiredCaptain> GetCaptainList()
        {
        }

        [MethodImpl(0x8000)]
        public ulong[] GetWingManList()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAvailableWingMan()
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitCaptains()
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitCaptainShips()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCaptainWingMan(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected bool RemoveCaptain(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBStaticHiredCaptainShipBase TryCreateStaticShip4Feats(int npcCaptainShipId, bool needRepire, LBStaticHiredCaptain captain)
        {
        }
    }
}

