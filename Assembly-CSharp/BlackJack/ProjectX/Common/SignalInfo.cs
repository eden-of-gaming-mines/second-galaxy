﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class SignalInfo
    {
        public SignalType m_type;
        public ulong m_instanceId;
        public int m_configId;
        public int m_solarSystemId;
        public Vector3D m_location;
        public DateTime m_expiryTime;
        public List<int> m_params;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

