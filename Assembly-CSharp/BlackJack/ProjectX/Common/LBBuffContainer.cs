﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBBuffContainer : IPropertiesProvider
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnPropertiesGroupDirty;
        protected ILBBufOwner m_owner;
        protected List<LBBufBase> m_bufList;
        protected List<LBBufBase> m_bufList2Remove;
        protected Dictionary<int, int> m_gamePlayFlagBufCountArray;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<uint> EventOnTick;
        protected uint? m_propertiesGroupMask;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AttachBuf;
        private static DelegateBridge __Hotfix_AttachBufDuplicate;
        private static DelegateBridge __Hotfix_SyncAttachBufDuplicate;
        private static DelegateBridge __Hotfix_DetachBuf;
        private static DelegateBridge __Hotfix_DetachBufInSpace_1;
        private static DelegateBridge __Hotfix_DetachBufInSpace_0;
        private static DelegateBridge __Hotfix_ModifyBufLifeTime;
        private static DelegateBridge __Hotfix_GetBufByInstanceId;
        private static DelegateBridge __Hotfix_GetBufByGroupId;
        private static DelegateBridge __Hotfix_GetFirstBufByGamePlayFlag;
        private static DelegateBridge __Hotfix_GetBufCountByGamePlayFlag;
        private static DelegateBridge __Hotfix_GetBufList;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_RegistBufTick;
        private static DelegateBridge __Hotfix_UnregistBufTick;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_HasPropertiesGroup;
        private static DelegateBridge __Hotfix_HasProperty;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_OnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_RecalculatePropertiesGroupMask;
        private static DelegateBridge __Hotfix_UpdateGamePlayFlagCacheOnBufAttach;
        private static DelegateBridge __Hotfix_UpdateGamePlayFlagCacheOnBufDetach;
        private static DelegateBridge __Hotfix_InitGamePlayFlagCache;
        private static DelegateBridge __Hotfix_add_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_remove_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_add_EventOnTick;
        private static DelegateBridge __Hotfix_remove_EventOnTick;

        public event Action<int> EventOnPropertiesGroupDirty
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        protected event Action<uint> EventOnTick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBBuffContainer(ILBBufOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AttachBuf(LBBufBase buf, ILBBufSource source, uint currTime, uint lifeEndTime = 0, object param = null, float bufInstanceParam1 = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        [MethodImpl(0x8000)]
        public void AttachBufDuplicate(LBBufBase buf, ILBBufSource source, float bufInstanceParam1)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear(bool neeSync = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool DetachBuf(LBBufBase buf, bool isLifeEnd, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool DetachBufInSpace(int bufId, bool isLifeEnd, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool DetachBufInSpace(ulong bufInstanceId, bool isLifeEnd, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetBufByGroupId(int bufGroupId)
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetBufByInstanceId(ulong bufInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetBufCountByGamePlayFlag(BufGamePlayFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBBufBase> GetBufList()
        {
        }

        [MethodImpl(0x8000)]
        public LBBufBase GetFirstBufByGamePlayFlag(BufGamePlayFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        private void InitGamePlayFlagCache()
        {
        }

        [MethodImpl(0x8000)]
        public bool ModifyBufLifeTime(uint bufInstanceId, uint lifeEndTime)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPropertiesGroupDirty(int group)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        private void RecalculatePropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        public void RegistBufTick(Action<uint> onTick)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncAttachBufDuplicate(LBBufBase buf, List<uint> sourceTargetList, uint currTime, uint lifeEndTime, LBBuffContainer container, int attachCount, object param = null, float additionalBufInstanceParam1 = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregistBufTick(Action<uint> onTick)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGamePlayFlagCacheOnBufAttach(LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGamePlayFlagCacheOnBufDetach(LBBufBase buf)
        {
        }
    }
}

