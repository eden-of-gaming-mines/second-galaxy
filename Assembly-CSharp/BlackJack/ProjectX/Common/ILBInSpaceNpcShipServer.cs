﻿namespace BlackJack.ProjectX.Common
{
    public interface ILBInSpaceNpcShipServer : ILBInSpaceNpcShip, ILBInSpaceShip, ILBSpaceShipBasic, ILBShipItemStoreContainer, ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        ILogicBlockShipCompAI GetLBAI();
        LogicBlockShipCompFormationMaker GetLBFormationMaker();
        ILBInSpaceShipScriptInterface GetLBScriptInterface();
    }
}

