﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBSceneDummyInfo")]
    public class GDBSceneDummyInfo : IExtensible
    {
        private int _Id;
        private int _confId;
        private string _name;
        private double _locationX;
        private double _locationY;
        private double _locationZ;
        private uint _rotationX;
        private uint _rotationY;
        private uint _rotationZ;
        private bool _isNeedLocalization;
        private string _localizationKey;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_ConfId;
        private static DelegateBridge __Hotfix_set_ConfId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_LocationX;
        private static DelegateBridge __Hotfix_set_LocationX;
        private static DelegateBridge __Hotfix_get_LocationY;
        private static DelegateBridge __Hotfix_set_LocationY;
        private static DelegateBridge __Hotfix_get_LocationZ;
        private static DelegateBridge __Hotfix_set_LocationZ;
        private static DelegateBridge __Hotfix_get_RotationX;
        private static DelegateBridge __Hotfix_set_RotationX;
        private static DelegateBridge __Hotfix_get_RotationY;
        private static DelegateBridge __Hotfix_set_RotationY;
        private static DelegateBridge __Hotfix_get_RotationZ;
        private static DelegateBridge __Hotfix_set_RotationZ;
        private static DelegateBridge __Hotfix_get_IsNeedLocalization;
        private static DelegateBridge __Hotfix_set_IsNeedLocalization;
        private static DelegateBridge __Hotfix_get_LocalizationKey;
        private static DelegateBridge __Hotfix_set_LocalizationKey;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="confId", DataFormat=DataFormat.TwosComplement)]
        public int ConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="locationX", DataFormat=DataFormat.TwosComplement)]
        public double LocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="locationY", DataFormat=DataFormat.TwosComplement)]
        public double LocationY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="locationZ", DataFormat=DataFormat.TwosComplement)]
        public double LocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="rotationX", DataFormat=DataFormat.TwosComplement)]
        public uint RotationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="rotationY", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint RotationY
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(9, IsRequired=false, Name="rotationZ", DataFormat=DataFormat.TwosComplement)]
        public uint RotationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="isNeedLocalization", DataFormat=DataFormat.Default)]
        public bool IsNeedLocalization
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(11, IsRequired=false, Name="localizationKey", DataFormat=DataFormat.Default)]
        public string LocalizationKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

