﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct SpaceGrideIndex
    {
        public int X;
        public int Y;
        public int Z;
        [MethodImpl(0x8000)]
        public static bool operator ==(SpaceGrideIndex lhs, SpaceGrideIndex rhs)
        {
        }

        public static bool operator !=(SpaceGrideIndex lhs, SpaceGrideIndex rhs) => 
            !(lhs == rhs);

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Equals(object other)
        {
        }
    }
}

