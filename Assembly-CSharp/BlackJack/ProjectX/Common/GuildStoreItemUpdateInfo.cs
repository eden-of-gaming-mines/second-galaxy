﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct GuildStoreItemUpdateInfo
    {
        public GuildStoreItemInfo m_item;
        public long m_changeValue;
        public GuildStoreItemUpdateInfo(GuildStoreItemInfo itemInfo, long changeValue)
        {
            this.m_item = itemInfo;
            this.m_changeValue = changeValue;
        }

        public bool IsEmptyInfo() => 
            ReferenceEquals(this.m_item, null);
    }
}

