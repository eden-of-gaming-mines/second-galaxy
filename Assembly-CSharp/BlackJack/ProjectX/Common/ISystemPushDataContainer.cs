﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface ISystemPushDataContainer
    {
        void UpdatePlayerSystemPushInfo(ClientOSType osType, string pushToken, LanguageType languageType);
    }
}

