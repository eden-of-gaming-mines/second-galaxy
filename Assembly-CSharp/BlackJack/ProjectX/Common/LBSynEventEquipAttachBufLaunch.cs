﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventEquipAttachBufLaunch : LBSyncEvent
    {
        public uint m_destObjectId;
        public LBSpaceProcessEquipAttachBufLaunch m_process;
        public uint m_equipGroupCDEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

