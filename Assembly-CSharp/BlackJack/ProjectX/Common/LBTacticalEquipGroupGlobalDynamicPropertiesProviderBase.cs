﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase : LBTacticalEquipGroupBase
    {
        protected Dictionary<int, float> m_globalPropertiesInfoDict;
        protected uint m_singleItemPropertiesMask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase> EventOnTacticalEquipGlobalDynamicPropertiesProviderActivate;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_InitPropertiesCache;
        private static DelegateBridge __Hotfix_HasPropertiesGroup_;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetModifyFinalFakeValue;
        private static DelegateBridge __Hotfix_add_EventOnTacticalEquipGlobalDynamicPropertiesProviderActivate;
        private static DelegateBridge __Hotfix_remove_EventOnTacticalEquipGlobalDynamicPropertiesProviderActivate;

        public event Action<LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase> EventOnTacticalEquipGlobalDynamicPropertiesProviderActivate
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase(ILBInSpaceShip ownerShip)
        {
        }

        public abstract float GetModifyFactor(ILBSpaceTarget target = null);
        [MethodImpl(0x8000)]
        public float GetModifyFinalFakeValue(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasPropertiesGroup_(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPropertiesCache()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

