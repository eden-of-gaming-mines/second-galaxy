﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public static class WebActivityDefine
    {
        public static class EventId
        {
            public const int Invalid = 0;
            public const int Login = 1;
            public const int Money_RealMoney = 2;
            public const int Money_TradeMoney = 3;
            public const int Money_BindMoney = 4;
            public const int Money_Item = 5;
            public const int Charge = 6;
            public const int Daily_ActivityPoint = 8;
            public const int Daily_ManualQuest = 9;
            public const int Daily_ChallengeQuest = 10;
            public const int Daily_ScienceExplore = 11;
            public const int Daily_Wormhole = 12;
            public const int Daily_FactionCreditQuest = 13;
            public const int Daily_Trade = 14;
            public const int Daily_Crack = 15;
            public const int Daily_DelegateMissionReward = 0x10;
            public const int Develop_DrivingLicense = 0x11;
            public const int Develop_Tech = 0x12;
            public const int Develop_Produce = 0x13;
            public const int Group_GuildDonate = 20;
            public const int Group_GuildProduce = 0x15;
            public const int Group_GuildOccupy = 0x16;
            public const int Ship_HangarUnpack = 0x17;
            public const int Ship_Destroy = 0x18;
            public const int Shop_NpcShop = 0x19;
            public const int BlackMarket = 0x1a;
            public const int Money_PanGala = 0x1b;
            public const int Money_GuildGala = 0x1c;
            public const int Money_CreditCurrency = 0x1d;
        }

        public static class EventType
        {
            public const string Login = "login";
            public const string Money = "money";
            public const string Charge = "charge";
            public const string Daily = "daily";
            public const string Develop = "develop";
            public const string Group = "group";
            public const string Ship = "ship";
            public const string Shop = "shop";
            public const string BlackMarket = "blackmarket";
        }
    }
}

