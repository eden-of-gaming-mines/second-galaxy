﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class ScanProbeInfo
    {
        public ScanProbeType m_type;
        public int m_currCount;
        public DateTime m_cdStartTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

