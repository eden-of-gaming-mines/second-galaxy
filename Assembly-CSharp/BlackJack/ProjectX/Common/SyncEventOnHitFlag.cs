﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Flags]
    public enum SyncEventOnHitFlag : byte
    {
        isHit = 1,
        isCritical = 2,
        isOnShield = 4,
        isOnArmor = 8,
        isOnShieldEx = 0x10,
        isOnArmorEx = 0x20
    }
}

