﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class LogicBlockShipCompWeaponEquipSetupEnv
    {
        protected ILBStaticShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetEquipCountByEquipLimitId;
        private static DelegateBridge __Hotfix_get_LbWeaponEquip;

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompWeaponEquipSetupEnv()
        {
        }

        public abstract bool CheckItemStoreCapacityForAllSlotItems();
        public abstract bool CheckItemStoreCapacityForSlotItem(LBStaticWeaponEquipSlotGroup group);
        [MethodImpl(0x8000)]
        public int GetEquipCountByEquipLimitId(int equipLimitId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBStaticShip ship)
        {
        }

        public abstract bool IsDependTechReadyForWeaponEquip(StoreItemType itemType, int configId);
        public abstract bool IsHighSlotGroupAllowedToSet();
        public abstract bool IsLowSlotGroupAllowedToSet();
        public abstract bool IsMiddleSlotGroupAllowedToSet();
        public abstract bool IsWeaponEquipSlotGroupOptAvaiable(LBStaticWeaponEquipSlotGroup group);
        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        protected LogicBlockShipCompStaticWeaponEquipBase LbWeaponEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

