﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ProjectX.Protocol;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildSolarSystemCtxBase
    {
        bool CheckBuildingSentryWorking(int solarSystemId);
        bool CheckGuildBuildingDeployUpgradeWithoutPermission(GuildBuildingDeployUpgradeReq req, out int errCode, out GuildBuildingInfo buildingInfo);
        bool CheckGuildBuildingRecycleWithoutPermission(GuildBuildingRecycleReq req, out int errCode, out GuildBuildingInfo buildingInfo);
        int GetFlourishLevel(int solarSystemId);
        GuildBuildingInfo GetGuildBuildingInfo(ulong instanceId, bool containsLost);
        List<GuildBuildingInfo> GetGuildBuildingInfoList(int solarSystemId);
        GuildBuildingSolarSystemEffectInfo GetGuildBuildingSolarSystemEffectInfo(int solarSystemId);
        GuildSolarSystemInfo GetGuildSolarSystemInfo(int solarSystemId);
        List<GuildSolarSystemInfo> GetOccupiedSolarSystemList();
        bool IsSolarSystemOccupied(int solarSystemId);
    }
}

