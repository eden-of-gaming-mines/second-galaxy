﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IQuestDataContainer
    {
        int GetDailyFreeQuestCompleteCount();
        List<QuestProcessingInfo> GetProcessingQuestList();
        IEnumerable<QuestEnvirmentInfo> GetQuestEnvList();
        List<QuestWaitForAcceptInfo> GetQuestWaitForAcceptList();
        bool IsQuestCompleted(int questId);
        void ProcessingQuestAdd(QuestProcessingInfo questProcessingInfo);
        void ProcessingQuestBindScene(int questInstanceId, uint sceneInstanceId);
        void ProcessingQuestCompleteCondParamSet(int questInstanceId, int condIndex, int value);
        void ProcessingQuestRemove(int questInstanceId);
        void ProcessingQuestSetCompleteWait(int questInstanceId);
        void QuestEnvAdd(QuestEnvirmentInfo env);
        void QuestEnvRemove(int envTypeId);
        void QuestWaitForAcceptListAdd(int questId, int questFactionId, bool fromQuestCancel, int questLevel, int questSolarSystemId);
        void QuestWaitForAcceptListRemove(int questId);
        void SetQuestCompleted(int questId);
        void SetQuestFail(int questInstanceId, bool isFail);
        void UpdateDailyFreeQuestCompleteCount(int count);
    }
}

