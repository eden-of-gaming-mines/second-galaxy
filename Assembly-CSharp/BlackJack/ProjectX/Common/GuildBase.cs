﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;

    public abstract class GuildBase : IGuildCompOwnerBase, IGuildActionBase, IGuildAllianceBase, IGuildAntiTeleportBase, IGuildBasicLogicBase, IGuildBattleBase, IGuildBenefitsBase, IGuildCompensationBase, IGuildCurrencyBase, IGuildCurrencyLogBase, IGuildDiplomacyBase, IGuildDynamicBase, IGuildFlagShipBase, IGuildFleetBase, IGuildItemStoreBase, IGuildJoinApplyBase, IGuildLostReportBase, IGuildMemberListBase, IGuildMessageBoardBase, IGuildMiningBase, IGuildMomentsBase, IGuildCompProductionBase, IGuildPropertiesCalculaterBase, IGuildPurchaseOrderBase, IGuildSimpleRuntimeBase, IGuildSolarSystemCtxBase, IGuildTeleportTunnelBase, IGuildTradeBase, IGuildItemStoreItemOperationBase, IGuildProductionBase
    {
        protected IGuildDataContainerBase m_dc;
        protected List<GuildCompBase> m_compList;
        protected GuildCompActionBase m_compAction;
        protected GuildCompAllianceBase m_compAlliance;
        protected GuildCompAntiTeleportBase m_compAntiTeleport;
        protected GuildCompBasicLogicBase m_compBasic;
        private static readonly Regex m_nameRegex;
        private static readonly Regex m_nameRegexWihtoutLength;
        private static readonly Regex m_codeNameRegex;
        private static readonly Regex m_codeNameRegexWihtoutLength;
        protected GuildCompBattleBase m_compBattle;
        protected GuildCompBenefitsBase m_compBenefits;
        protected GuildCompCompensationBase m_compCompensation;
        protected GuildCompCurrencyBase m_compCurrency;
        protected GuildCompCurrencyLogBase m_compCurrencyLog;
        protected GuildCompDiplomacyBase m_compDiplomacy;
        protected GuildCompDynamicBase m_compDynamic;
        protected GuildCompFlagShipBase m_compFlagShip;
        protected GuildCompFleetBase m_compFleet;
        protected GuildCompItemStoreBase m_compItemStore;
        protected GuildCompJoinApplyBase m_compJoinApply;
        protected GuildCompLostReportBase m_compLostReport;
        protected GuildCompMemberListBase m_compMemberList;
        protected GuildCompMessageBoardBase m_compMessageBoard;
        protected GuildCompMiningBase m_compMining;
        protected GuildCompMomentsBase m_compMoments;
        protected GuildCompProductionBase m_compProduction;
        protected GuildCompPropertiesCalculaterBase m_compPropertiesCalculater;
        protected GuildCompPurchaseOrderBase m_compPurchaseOrder;
        protected GuildCompSimpleRuntimeBase m_compSimpleRuntime;
        protected GuildCompSolarSystemCtxBase m_compSolarSystemCtx;
        protected GuildCompStaffingLogBase m_compStaffingLog;
        protected GuildCompTeleportTunnelBase m_compTeleportTunnel;
        protected GuildCompTradeBase m_compTrade;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initlize;
        private static DelegateBridge __Hotfix_PostInitlialize;
        private static DelegateBridge __Hotfix_GetGuildDataContainer;
        private static DelegateBridge __Hotfix_GetCompAction;
        private static DelegateBridge __Hotfix_GetCompDynamic;
        private static DelegateBridge __Hotfix_GetCompFlagShip;
        private static DelegateBridge __Hotfix_GetCompBasic;
        private static DelegateBridge __Hotfix_GetCompBattle;
        private static DelegateBridge __Hotfix_GetCompBenefis;
        private static DelegateBridge __Hotfix_GetCompCompensation;
        private static DelegateBridge __Hotfix_GetCompCurrency;
        private static DelegateBridge __Hotfix_GetCompCurrencyLog;
        private static DelegateBridge __Hotfix_GetCompDiplomacy;
        private static DelegateBridge __Hotfix_GetCompFleet;
        private static DelegateBridge __Hotfix_GetCompItemStore;
        private static DelegateBridge __Hotfix_GetCompJoinApply;
        private static DelegateBridge __Hotfix_GetCompMemberList;
        private static DelegateBridge __Hotfix_GetCompMessageBoard;
        private static DelegateBridge __Hotfix_GetCompMining;
        private static DelegateBridge __Hotfix_GetCompProduction;
        private static DelegateBridge __Hotfix_GetCompPropertiesCalculater;
        private static DelegateBridge __Hotfix_GetCompPurchaseOrder;
        private static DelegateBridge __Hotfix_GetCompSimpleRuntime;
        private static DelegateBridge __Hotfix_GetCompSolarSystemCtx;
        private static DelegateBridge __Hotfix_GetCompStaffingLog;
        private static DelegateBridge __Hotfix_GetCompAlliance;
        private static DelegateBridge __Hotfix_GetCompMoments;
        private static DelegateBridge __Hotfix_GetCompTrade;
        private static DelegateBridge __Hotfix_GetCompLostReport;
        private static DelegateBridge __Hotfix_GetCompAntiTeleport;
        private static DelegateBridge __Hotfix_GetCompTeleportTunnel;
        private static DelegateBridge __Hotfix_CalcGuildMinJumpDistance2TargetSolarSystem;
        private static DelegateBridge __Hotfix_CalcGuildActionInfomationPointCost;
        private static DelegateBridge __Hotfix_CalcInfoPointJumpMulti;
        private static DelegateBridge __Hotfix_CheckSecurityLevel;
        private static DelegateBridge __Hotfix_IsSolarSystemSovereignLevelFit;
        private static DelegateBridge __Hotfix_IsSovereignFit;
        private static DelegateBridge __Hotfix_IsEnoughInfoPoint;
        private static DelegateBridge __Hotfix_OnReceiveAllianceInvite;
        private static DelegateBridge __Hotfix_GetAllianceInviteList;
        private static DelegateBridge __Hotfix_CheckForActivateAntiTeleportEffect;
        private static DelegateBridge __Hotfix_CheckForAntiTeleportInfoReq;
        private static DelegateBridge __Hotfix_GetGuildAntiTeleportEffectBySolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildAntiTeleportEffectById;
        private static DelegateBridge __Hotfix_GetAllGuildAntiTeleportEffectList;
        private static DelegateBridge __Hotfix_IsAntiTeleportEffectActivate;
        private static DelegateBridge __Hotfix_GetGuildId;
        private static DelegateBridge __Hotfix_GetGuildName;
        private static DelegateBridge __Hotfix_GetGuildCodeName;
        private static DelegateBridge __Hotfix_GetGuildLanguageType;
        private static DelegateBridge __Hotfix_GetBasicInfo;
        private static DelegateBridge __Hotfix_GetStarMapGuildSimpleInfo;
        private static DelegateBridge __Hotfix_GetStarMapAllianceSimpleInfo;
        private static DelegateBridge __Hotfix_IsValidCodeName;
        private static DelegateBridge __Hotfix_CheckGuildNameValidation;
        private static DelegateBridge __Hotfix_CheckStringValidation;
        private static DelegateBridge __Hotfix_GetGuildLeaderJob;
        private static DelegateBridge __Hotfix_get_NameRegex;
        private static DelegateBridge __Hotfix_get_NameRegexWihtoutLength;
        private static DelegateBridge __Hotfix_get_CodeNameRegex;
        private static DelegateBridge __Hotfix_get_CodeNameRegexWihtoutLength;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_get_CodeName;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_get_Announcement;
        private static DelegateBridge __Hotfix_get_Manifesto;
        private static DelegateBridge __Hotfix_get_Logo;
        private static DelegateBridge __Hotfix_GetReinforcementEndHour;
        private static DelegateBridge __Hotfix_GetReinforcementEndMinuts;
        private static DelegateBridge __Hotfix_HasReinforcementEndTimeSetted;
        private static DelegateBridge __Hotfix_GetCompensationList;
        private static DelegateBridge __Hotfix_GetCompensationListVersion;
        private static DelegateBridge __Hotfix_CloseCompensation;
        private static DelegateBridge __Hotfix_UpdateCompensation;
        private static DelegateBridge __Hotfix_AddCompensation;
        private static DelegateBridge __Hotfix_SetAutoCompensation;
        private static DelegateBridge __Hotfix_GetCurrencyInfo;
        private static DelegateBridge __Hotfix_GetDiplomacyInfo;
        private static DelegateBridge __Hotfix_SetGuildDynamicInfo;
        private static DelegateBridge __Hotfix_GetGuildDynamicInfo;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipAddModuleEquip;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipCaptainRegister;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipCaptainUnregister;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarLocking;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarScanning;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipPack;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRemoveModuleEquip;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRename;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipRename4DomesticPackage;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipSupplementFuel;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipUnpack;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarInfoReq;
        private static DelegateBridge __Hotfix_CheckForGuildFlagShipHangarUnlocking;
        private static DelegateBridge __Hotfix_IsHangarSlotUnlock;
        private static DelegateBridge __Hotfix_GetHangarUnlockSlotCount;
        private static DelegateBridge __Hotfix_GetHangarSlotRankType;
        private static DelegateBridge __Hotfix_GetStaticFlagShip;
        private static DelegateBridge __Hotfix_GetStaticFlagShipList;
        private static DelegateBridge __Hotfix_UpdateShipHangarLockMaster;
        private static DelegateBridge __Hotfix_ClearShipHangarLockMaster;
        private static DelegateBridge __Hotfix_RegisterFlagShipDriver;
        private static DelegateBridge __Hotfix_UnregisterFlagShipDriver;
        private static DelegateBridge __Hotfix_SetFlagShipCustomName;
        private static DelegateBridge __Hotfix_IsFlagShipUnsetFinished;
        private static DelegateBridge __Hotfix_GetAllFlagShipHangarList;
        private static DelegateBridge __Hotfix_GetGuildFlagShipHangarById;
        private static DelegateBridge __Hotfix_GetShipHangarLockMaster;
        private static DelegateBridge __Hotfix_GetGuildFlagShipHangarBySolarSystemId;
        private static DelegateBridge __Hotfix_GetFlagShipDriverInfo_0;
        private static DelegateBridge __Hotfix_GetFlagShipDriverInfo_1;
        private static DelegateBridge __Hotfix_GetShipHangarFlagShipInstanceInfoList;
        private static DelegateBridge __Hotfix_GetFlagShipInfoByInsId;
        private static DelegateBridge __Hotfix_GetFlagShipInfoByDrivingInfo;
        private static DelegateBridge __Hotfix_GetFlagShipEquipModuleConfIdList;
        private static DelegateBridge __Hotfix_GetGuildFlagShipOptLogInfoList;
        private static DelegateBridge __Hotfix_CheckForFleetListReq;
        private static DelegateBridge __Hotfix_CheckForFleetCreating;
        private static DelegateBridge __Hotfix_CheckForFleetSetFormationType;
        private static DelegateBridge __Hotfix_CheckForFleetSetAllowToJoinFleetManual;
        private static DelegateBridge __Hotfix_CheckForFleetSetFormationActive;
        private static DelegateBridge __Hotfix_CheckForFleetMemberJoin;
        private static DelegateBridge __Hotfix_CheckForFleetMemberLeave;
        private static DelegateBridge __Hotfix_CheckForFleetKickOut;
        private static DelegateBridge __Hotfix_CheckForFleetDismiss;
        private static DelegateBridge __Hotfix_CheckForFleetRename;
        private static DelegateBridge __Hotfix_CheckForFleetRename4DomesticPackage;
        private static DelegateBridge __Hotfix_CheckForFleetDetailInfoReq;
        private static DelegateBridge __Hotfix_CheckForGuildFleetSetCommanderReq;
        private static DelegateBridge __Hotfix_CheckForGuildFleetSetInternalPositions;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMemberRetire;
        private static DelegateBridge __Hotfix_CheckForGuildFleetTransferPosition;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMembersJumping;
        private static DelegateBridge __Hotfix_CheckForGuildFleetMembersUseStargate;
        private static DelegateBridge __Hotfix_CheckForGuildFleetFireFocusTarget;
        private static DelegateBridge __Hotfix_CheckForGuildFleetProtectTarget;
        private static DelegateBridge __Hotfix_GetGuildPlayerFleetId;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberByFleetPosition;
        private static DelegateBridge __Hotfix_GetGuildFleetById;
        private static DelegateBridge __Hotfix_GetItemInfoByItemId;
        private static DelegateBridge __Hotfix_GetItemInfoByItemInsId;
        private static DelegateBridge __Hotfix_GetItemInfoByIndex;
        private static DelegateBridge __Hotfix_GetGuildStoreItemInfoList;
        private static DelegateBridge __Hotfix_IsGuildStoreItemMatched;
        private static DelegateBridge __Hotfix_RemoveItemDirectly;
        private static DelegateBridge __Hotfix_AddSovereignLostReport;
        private static DelegateBridge __Hotfix_AddBuildingLostReport;
        private static DelegateBridge __Hotfix_GetBuildingLostReport;
        private static DelegateBridge __Hotfix_GetMemberListInfo;
        private static DelegateBridge __Hotfix_GetMemberList;
        private static DelegateBridge __Hotfix_GetMember;
        private static DelegateBridge __Hotfix_GetMemberCount;
        private static DelegateBridge __Hotfix_GetGuildMiningBalanceTimeHour;
        private static DelegateBridge __Hotfix_GetGuildMiningBalanceTimeMinute;
        private static DelegateBridge __Hotfix_GetGuildMomentsInfoList;
        private static DelegateBridge __Hotfix_GetGuildMomentsBriefInfoList;
        private static DelegateBridge __Hotfix_CheckProductionLineStart;
        private static DelegateBridge __Hotfix_CheckProductionLineCancel;
        private static DelegateBridge __Hotfix_CheckProductionLineSpeedUpByTradeMoney;
        private static DelegateBridge __Hotfix_CheckProductionLineComplete;
        private static DelegateBridge __Hotfix_CheckProduceTempleteUnlockCondition;
        private static DelegateBridge __Hotfix_CheckProduceTempleteUnlockCondition_SovereignGalaxyCount;
        private static DelegateBridge __Hotfix_CalcGuildProductionTradeMoneyRealCost;
        private static DelegateBridge __Hotfix_CalcGuidProductionItemRealCost;
        private static DelegateBridge __Hotfix_CalcGuildProductionTimeRealCost;
        private static DelegateBridge __Hotfix_GetGuildProductionLineList;
        private static DelegateBridge __Hotfix_GetGuildProductionLineById;
        private static DelegateBridge __Hotfix_AddGuildProductionLine;
        private static DelegateBridge __Hotfix_RemoveGuildProductionLine;
        private static DelegateBridge __Hotfix_UpdateGuildProductionLine;
        private static DelegateBridge __Hotfix_ClearGuildProductionLineData;
        private static DelegateBridge __Hotfix_UpdateProductionLineSpeedUpTime;
        private static DelegateBridge __Hotfix_CheckGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_GetGuildProductionDataVersion;
        private static DelegateBridge __Hotfix_CalcGuildPropertiesById;
        private static DelegateBridge __Hotfix_CheckGuildFlag;
        private static DelegateBridge __Hotfix_GetGuildPurchaseOrderListInfo;
        private static DelegateBridge __Hotfix_AddGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_UpdateGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_CancelGuildPurchaseOrder;
        private static DelegateBridge __Hotfix_GetGuildSimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_GetSimplestInfo;
        private static DelegateBridge __Hotfix_UpdateSimplestInfo;
        private static DelegateBridge __Hotfix_GetOccupiedSolarSystemList;
        private static DelegateBridge __Hotfix_GetGuildBuildingInfoList;
        private static DelegateBridge __Hotfix_GetGuildSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetGuildBuildingInfo;
        private static DelegateBridge __Hotfix_IsSolarSystemOccupied;
        private static DelegateBridge __Hotfix_GetFlourishLevel;
        private static DelegateBridge __Hotfix_CheckBuildingSentryWorking;
        private static DelegateBridge __Hotfix_GetGuildBuildingSolarSystemEffectInfo;
        private static DelegateBridge __Hotfix_CheckGuildBuildingDeployUpgradeWithoutPermission;
        private static DelegateBridge __Hotfix_CheckGuildBuildingRecycleWithoutPermission;
        private static DelegateBridge __Hotfix_CalcFlourishValue;
        private static DelegateBridge __Hotfix_IsSovereignBuilding;
        private static DelegateBridge __Hotfix_GetGuildBuildingLevelDetailConf;
        private static DelegateBridge __Hotfix_IsGuildBuildingLevelMax;
        private static DelegateBridge __Hotfix_GetFlourishLevelByValue;
        private static DelegateBridge __Hotfix_GetGuildTeleportTunnelEffectBySolarSystemId;
        private static DelegateBridge __Hotfix_GetAllGuildTeleportTunnelEffectList;
        private static DelegateBridge __Hotfix_GetGuildTeleportTunnelBuildingEffectInfo;
        private static DelegateBridge __Hotfix_GetGuildTradePortExtraInfoBySolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildTradePortExtraInfoList;
        private static DelegateBridge __Hotfix_GetSpaceShipConfigInfoByTradePortSolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildTradePortDataVersion;

        [MethodImpl(0x8000)]
        protected GuildBase(IGuildDataContainerBase dc)
        {
        }

        [MethodImpl(0x8000)]
        public void AddBuildingLostReport(BuildingLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public void AddCompensation(LossCompensation compensation)
        {
        }

        [MethodImpl(0x8000)]
        public bool AddGuildProductionLine(GuildProductionLineInfo lineInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void AddGuildPurchaseOrder(GuildPurchaseOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSovereignLostReport(SovereignLostReport report)
        {
        }

        [MethodImpl(0x8000)]
        public static int CalcFlourishValue(int orgValue, int addValue, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<CostInfo> CalcGuidProductionItemRealCost(int solarSystemId, GuildProduceCategory category, List<CostInfo> configItemCostList, int count)
        {
        }

        [MethodImpl(0x8000)]
        public ulong CalcGuildActionInfomationPointCost(ConfigDataGuildActionInfo guildActionConfig, int solarSystemId, int minJumpDistance)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcGuildMinJumpDistance2TargetSolarSystem(GDBHelper gdbHelper, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcGuildProductionTimeRealCost(int solarSystemId, GuildProduceCategory category, int configTime, int count)
        {
        }

        [MethodImpl(0x8000)]
        public ulong CalcGuildProductionTradeMoneyRealCost(int solarSystemId, GuildProduceCategory category, ulong configMoneyCost, int count)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcGuildPropertiesById(int solarSystemId, GuildPropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcInfoPointJumpMulti(ConfigDataGuildActionInfo guildActionConfig, int minJumpDistance)
        {
        }

        [MethodImpl(0x8000)]
        public bool CancelGuildPurchaseOrder(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckBuildingSentryWorking(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForActivateAntiTeleportEffect(string gameUserId, ulong buildingInsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForAntiTeleportInfoReq(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetCreating(string gameUserId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetDetailInfoReq(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetDismiss(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetKickOut(string srcGameUserId, ulong fleetId, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetListReq(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetMemberJoin(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetMemberLeave(string gameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetRename(string gameUserId, ulong fleetId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetRename4DomesticPackage(string gameUserId, ulong fleetId, string fleetName, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetSetAllowToJoinFleetManual(string gameUserId, ulong fleetId, bool allowToJoinFleetManual, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetSetFormationActive(string gameUserId, ulong fleetId, bool isFormationActive, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForFleetSetFormationType(string gameUserId, ulong fleetId, int formationType, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipAddModuleEquip(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, int equipItemStoreIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipCaptainRegister(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, List<int> drivingLicenseIdList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipCaptainUnregister(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarInfoReq(string gameUserId, int solarSystemId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarLocking(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarScanning(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipHangarUnlocking(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipPack(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipRemoveModuleEquip(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int equipSlotIndex, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipRename(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipRename4DomesticPackage(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, string name, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipSupplementFuel(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, int supplementCount, int applySupplementCount, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFlagShipUnpack(string gameUserId, ulong shipHangarInsId, int hangarSlotIndex, ulong shipItemStoreInsId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetFireFocusTarget(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetMemberRetire(string srcGameUserId, ulong fleetId, List<uint> positionList, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetMembersJumping(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetMembersUseStargate(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetProtectTarget(string srcGameUserId, ulong fleetId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetSetCommanderReq(string srcGameUserId, ulong fleetId, string destGameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetSetInternalPositions(string gameUserId, ulong fleetId, GuildFleetMemberInfo member, bool hasPermission, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckForGuildFleetTransferPosition(string srcGameUserId, ulong fleetId, string destGameUserId, FleetPosition position, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildBuildingDeployUpgradeWithoutPermission(GuildBuildingDeployUpgradeReq req, out int errCode, out GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildBuildingRecycleWithoutPermission(GuildBuildingRecycleReq req, out int errCode, out GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildFlag(int solarSystemId, GuildFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public static int CheckGuildNameValidation(string guildName, bool checkLength, bool isDomesticPackage = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildProductionDataVersion(uint version)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProduceTempleteUnlockCondition(ConfigDataGuildProduceTempleteInfo confInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckProduceTempleteUnlockCondition_SovereignGalaxyCount(ConfigDataGuildProduceTempleteInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineCancel(ulong lineId, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineComplete(ulong lineId, DateTime currTime, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineSpeedUpByTradeMoney(ulong lineId, int speedUpLevel, ulong currTradeMoney, DateTime currTime, out int errCode, out int costTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckProductionLineStart(ulong lineId, int templeteId, int produceCount, out int errCode, out ulong moneyCost, out int timeCost, out List<CostInfo> costList)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSecurityLevel(int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckStringValidation(string stringToCheck, bool isDomesticPackage = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearGuildProductionLineData(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearShipHangarLockMaster()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseCompensation(ulong instanceId, CompensationCloseReason reason)
        {
        }

        [MethodImpl(0x8000)]
        public List<IGuildFlagShipHangarDataContainer> GetAllFlagShipHangarList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAntiTeleportEffectInfo> GetAllGuildAntiTeleportEffectList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfo> GetAllGuildTeleportTunnelEffectList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAllianceInviteInfo> GetAllianceInviteList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBasicInfo GetBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public BuildingLostReport GetBuildingLostReport(ulong reportId)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompActionBase GetCompAction()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompAllianceBase GetCompAlliance()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompAntiTeleportBase GetCompAntiTeleport()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompBasicLogicBase GetCompBasic()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompBattleBase GetCompBattle()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildBenefitsBase GetCompBenefis()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompCompensationBase GetCompCompensation()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompCurrencyBase GetCompCurrency()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompCurrencyLogBase GetCompCurrencyLog()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompDiplomacyBase GetCompDiplomacy()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompDynamicBase GetCompDynamic()
        {
        }

        [MethodImpl(0x8000)]
        public List<LossCompensation> GetCompensationList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCompensationListVersion()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompFlagShipBase GetCompFlagShip()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompFleetBase GetCompFleet()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompItemStoreBase GetCompItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompJoinApplyBase GetCompJoinApply()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompLostReportBase GetCompLostReport()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMemberListBase GetCompMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMessageBoardBase GetCompMessageBoard()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMiningBase GetCompMining()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompMomentsBase GetCompMoments()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompProductionBase GetCompProduction()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompPropertiesCalculaterBase GetCompPropertiesCalculater()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompPurchaseOrderBase GetCompPurchaseOrder()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompSimpleRuntimeBase GetCompSimpleRuntime()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompSolarSystemCtxBase GetCompSolarSystemCtx()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompStaffingLogBase GetCompStaffingLog()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompTeleportTunnelBase GetCompTeleportTunnel()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildCompTradeBase GetCompTrade()
        {
        }

        [MethodImpl(0x8000)]
        public GuildCurrencyInfo GetCurrencyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public DiplomacyInfo GetDiplomacyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo GetFlagShipDriverInfo(ulong shipHangarInsId, ulong shipInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetFlagShipEquipModuleConfIdList(IStaticFlagShipDataContainer ship)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer GetFlagShipInfoByDrivingInfo(int shipHangarSolarSystemId, string drivingGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public IStaticFlagShipDataContainer GetFlagShipInfoByInsId(int shipHangarSolarSystemId, ulong shipInsId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFlourishLevel(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetFlourishLevelByValue(int value, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectById(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectBySolarSystemId(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingInfo GetGuildBuildingInfo(ulong instanceId, bool containsLost)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildBuildingInfo> GetGuildBuildingInfoList(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataGuildBuildingLevelDetailInfo GetGuildBuildingLevelDetailConf(GuildBuildingType buildingType, int level)
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingSolarSystemEffectInfo GetGuildBuildingSolarSystemEffectInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetGuildCodeName()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildDataContainerBase GetGuildDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public GuildDynamicInfo GetGuildDynamicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarById(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public IGuildFlagShipHangarDataContainer GetGuildFlagShipHangarBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildFlagShipOptLogInfo> GetGuildFlagShipOptLogInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetInfo GetGuildFleetById(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildFleetMemberInfo GetGuildFleetMemberByFleetPosition(ulong fleetId, FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildId()
        {
        }

        [MethodImpl(0x8000)]
        public GuildAllianceLanguageType GetGuildLanguageType()
        {
        }

        [MethodImpl(0x8000)]
        public static GuildJobType GetGuildLeaderJob()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGuildMiningBalanceTimeHour()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGuildMiningBalanceTimeMinute()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMomentsInfo> GetGuildMomentsBriefInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMomentsInfo> GetGuildMomentsInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public string GetGuildName()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ulong GetGuildPlayerFleetId(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildProductionDataVersion()
        {
        }

        [MethodImpl(0x8000)]
        public GuildProductionLineInfo GetGuildProductionLineById(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildProductionLineInfo> GetGuildProductionLineList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildPurchaseOrderInfo> GetGuildPurchaseOrderListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimpleRuntimeInfo GetGuildSimpleRuntimeInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfo GetGuildSolarSystemInfo(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStoreItem> GetGuildStoreItemInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildTeleportTunnelEffectInfo GetGuildTeleportTunnelBuildingEffectInfo(ulong buildingInsId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTeleportTunnelEffectInfo> GetGuildTeleportTunnelEffectBySolarSystemId(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetGuildTradePortDataVersion()
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePortExtraInfo GetGuildTradePortExtraInfoBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradePortExtraInfo> GetGuildTradePortExtraInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public RankType GetHangarSlotRankType(IGuildFlagShipHangarDataContainer shipHangarDC, int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetHangarUnlockSlotCount(IGuildFlagShipHangarDataContainer shipHangarDC)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemInfoByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemInfoByItemId(StoreItemType type, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public LBStoreItem GetItemInfoByItemInsId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBase GetMember(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetMemberCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMemberBase> GetMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberListInfo GetMemberListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildSolarSystemInfo> GetOccupiedSolarSystemList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetReinforcementEndHour()
        {
        }

        [MethodImpl(0x8000)]
        public int GetReinforcementEndMinuts()
        {
        }

        [MethodImpl(0x8000)]
        public List<IStaticFlagShipDataContainer> GetShipHangarFlagShipInstanceInfoList(ulong shipHangarInsId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipHangarLockMaster()
        {
        }

        [MethodImpl(0x8000)]
        public GuildSimplestInfo GetSimplestInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetSpaceShipConfigInfoByTradePortSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public StarMapAllianceSimpleInfo GetStarMapAllianceSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public StarMapGuildSimpleInfo GetStarMapGuildSimpleInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ILBStaticFlagShip GetStaticFlagShip(ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBStaticFlagShip> GetStaticFlagShipList(ulong shipHangarInsId)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasReinforcementEndTimeSetted()
        {
        }

        protected abstract bool InitAllComps();
        [MethodImpl(0x8000)]
        public virtual void Initlize()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAntiTeleportEffectActivate(DateTime currTime, int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEnoughInfoPoint(int guildConfigId, int solarSysId, int minJumpDis, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFlagShipUnsetFinished(DateTime currTime, ulong shipHangarInsId, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsGuildBuildingLevelMax(GuildBuildingType buildingType, int level)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildStoreItemMatched(int storeItemIndex, ItemInfo comparedItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHangarSlotUnlock(IGuildFlagShipHangarDataContainer shipHangarDC, int index)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemOccupied(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemSovereignLevelFit(int guildConfigId, int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSovereignBuilding(GuildBuildingType type)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSovereignFit(int guildConfigId, int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsValidCodeName(string codeName, bool checkLength, bool isDomesticPackage = false)
        {
        }

        [MethodImpl(0x8000)]
        public void OnReceiveAllianceInvite(GuildAllianceInviteInfo invite)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PostInitlialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFlagShipDriver(ulong shipHangarInsId, int slotIndex, PlayerSimplestInfo driverInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveGuildProductionLine(ulong lineId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveItemDirectly(LBStoreItem item, OperateLogGuildStoreItemChangeType operateLogCauseId = 0, string operateLogLocation = null, GuildMemberBase operateMember = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAutoCompensation(bool isAuto)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlagShipCustomName(ulong shipHangarInsId, int slotIndex, string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildDynamicInfo(GuildDynamicInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterFlagShipDriver(ulong shipHangarInsId, int slotIndex, string optGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCompensation(LossCompensation compensation)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildProductionLine(GuildProductionLineInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public bool UpdateGuildPurchaseOrder(GuildPurchaseOrderInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProductionLineSpeedUpTime(ulong lineId, int reduceTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipHangarLockMaster(string masterId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSimplestInfo()
        {
        }

        public static Regex NameRegex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static Regex NameRegexWihtoutLength
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static Regex CodeNameRegex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static Regex CodeNameRegexWihtoutLength
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string CodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GuildAllianceLanguageType LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Announcement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string Manifesto
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GuildLogoInfo Logo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

