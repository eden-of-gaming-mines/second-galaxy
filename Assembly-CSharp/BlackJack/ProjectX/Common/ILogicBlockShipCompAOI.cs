﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface ILogicBlockShipCompAOI
    {
        event Action<ILBSpaceDropBox> EventOnDropBoxEnterView;

        event Action<ILBSpaceDropBox> EventOnDropBoxLeaveView;

        event Action<ILBSpaceTarget> EventOnOtherTargetBlink;

        event Action<ILBSpaceTarget> EventOnOtherTargetInvisibleEnd;

        event Action<ILBSpaceTarget> EventOnOtherTargetInvisibleStart;

        event Action<ILBSpaceTarget> EventOnOtherTargetJump;

        event Action<ILBSpaceTarget> EventOnTargetEnterView;

        event Action<ILBSpaceTarget> EventOnTargetLeaveView;

        void EnableAccurateAOI(bool enable);
        HashSet<ILBSpaceDropBox> GetCurrInterestDropBoxList();
        HashSet<ILBSpaceTarget> GetCurrInterestTargetList();
        ILBInSpacePlayerShip GetPlayerShipInView(string gameUserId);
        bool IsObjectInView(uint objId);
        void OnOtherTargetBlink(ILBSpaceTarget target);
        void OnOtherTargetInvisibleEnd(ILBSpaceTarget target);
        void OnOtherTargetInvisibleStart(ILBSpaceTarget target);
        void OnOtherTargetJump(ILBSpaceTarget target);
    }
}

