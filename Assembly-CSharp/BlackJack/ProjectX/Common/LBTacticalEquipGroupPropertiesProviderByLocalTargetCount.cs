﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderByLocalTargetCount : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        protected int m_localTargetCountCache;
        protected uint m_nextCalcTargetCountTime;
        protected float m_localTargetMin;
        protected float m_localTargetCountMax;
        protected float m_factor4LocalTargetCountMin;
        protected float m_factor4LocalTargetCountMax;
        protected float m_factor4LessMinLocalTargetCount;
        protected float m_factor4MoreMaxLocalTargetCount;
        protected TargetType m_paramTargetType;
        protected int m_paramDistance;
        protected uint CalcLocalTargetCountCDTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnLocalTargetCount;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnLocalTargetCount;
        private static DelegateBridge __Hotfix_GetLocalTargetCount;
        private static DelegateBridge __Hotfix_get_LocalTargetCountMin;
        private static DelegateBridge __Hotfix_get_LocalTargetCountMax;
        private static DelegateBridge __Hotfix_get_Factor4LocalTargetCountMin;
        private static DelegateBridge __Hotfix_get_Factor4LocalTargetCountMax;
        private static DelegateBridge __Hotfix_get_Factor4LessMinLocalTargetCount;
        private static DelegateBridge __Hotfix_get_Factor4MoreMaxLocalTargetCount;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderByLocalTargetCount(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnLocalTargetCount(ref float propertyValue)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetLocalTargetCount()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnLocalTargetCount(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float LocalTargetCountMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float LocalTargetCountMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4LocalTargetCountMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4LocalTargetCountMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4LessMinLocalTargetCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4MoreMaxLocalTargetCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum TargetType
        {
            Enemy,
            Friend
        }
    }
}

