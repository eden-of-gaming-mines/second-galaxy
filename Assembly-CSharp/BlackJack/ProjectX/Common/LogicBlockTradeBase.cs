﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockTradeBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected LogicBlockStationContextBase m_lbStationContext;
        protected NpcDNId m_currNpcId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_SetupTradeContext;
        private static DelegateBridge __Hotfix_get_CurrNpcDNId;
        private static DelegateBridge __Hotfix_CheckTradeItemBeforeSale;

        [MethodImpl(0x8000)]
        public bool CheckTradeItemBeforeSale(NpcDNId npcDNId, List<KeyValuePair<int, long>> saleItemInfos, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetupTradeContext(NpcDNId npcId)
        {
        }

        public NpcDNId CurrNpcDNId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

