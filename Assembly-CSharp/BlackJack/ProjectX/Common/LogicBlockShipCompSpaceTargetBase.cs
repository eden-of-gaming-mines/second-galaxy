﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LogicBlockShipCompSpaceTargetBase : ILBSpaceTarget, ILBBufSource, ILBSpaceObject
    {
        protected ILBInSpaceShip m_ownerShip;
        protected ConfigDataSpaceShip3DInfo m_conf3DInfo;
        protected TargetNoticeType m_targetNoticeFlag;
        protected bool? m_isHideInTargetList;
        protected bool? m_notAttackableFlag;
        protected bool m_isNpcPolice;
        protected int m_targetPriority;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_SetIsPolice;
        private static DelegateBridge __Hotfix_SetTargetPriority;
        private static DelegateBridge __Hotfix_GetObjectId;
        private static DelegateBridge __Hotfix_IsWaitForRemove;
        private static DelegateBridge __Hotfix_GetSpaceTargetName;
        private static DelegateBridge __Hotfix_GetMinAroundRadius;
        private static DelegateBridge __Hotfix_IsPlayerShip;
        private static DelegateBridge __Hotfix_IsHiredCaptainShip;
        private static DelegateBridge __Hotfix_IsNpcShip;
        private static DelegateBridge __Hotfix_IsNpcPolice;
        private static DelegateBridge __Hotfix_GetGuildId;
        private static DelegateBridge __Hotfix_GetAllienceId;
        private static DelegateBridge __Hotfix_IsExistFightBuf;
        private static DelegateBridge __Hotfix_GetSpaceTargetOwner;
        private static DelegateBridge __Hotfix_GetLBPVP;
        private static DelegateBridge __Hotfix_GetRotationQuaternion;
        private static DelegateBridge __Hotfix_GetTargetVelocity;
        private static DelegateBridge __Hotfix_GetTargetArmor;
        private static DelegateBridge __Hotfix_GetTargetShield;
        private static DelegateBridge __Hotfix_GetTargetEnergy;
        private static DelegateBridge __Hotfix_IsDead;
        private static DelegateBridge __Hotfix_IsStateFreeze;
        private static DelegateBridge __Hotfix_IsInJumping;
        private static DelegateBridge __Hotfix_IsInJumpPrepare;
        private static DelegateBridge __Hotfix_IsWithNotAttackableFlag;
        private static DelegateBridge __Hotfix_IsWithShieldRepairerDisableFlag;
        private static DelegateBridge __Hotfix_IsInvisible;
        private static DelegateBridge __Hotfix_IsAvailable4WeaponEquipTarget;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_GetProcessByInstanceId;
        private static DelegateBridge __Hotfix_HasBuf;
        private static DelegateBridge __Hotfix_IsBufLifeEnd;
        private static DelegateBridge __Hotfix_OnOtherTargetBlink;
        private static DelegateBridge __Hotfix_OnOtherTargetJump;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisibleStart;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisibleEnd;
        private static DelegateBridge __Hotfix_OnClearInvisibleByTarget;
        private static DelegateBridge __Hotfix_OnKillOtherTarget;
        private static DelegateBridge __Hotfix_OnLockedByOtherTarget;
        private static DelegateBridge __Hotfix_OnClearLockedByOtherTarget;
        private static DelegateBridge __Hotfix_GetTargetPriority;
        private static DelegateBridge __Hotfix_GetNpcTargetCountInHateList;
        private static DelegateBridge __Hotfix_GetLockedMeNpcTargetCount;
        private static DelegateBridge __Hotfix_GetSpaceObjectRadiusMax;
        private static DelegateBridge __Hotfix_CalcDistanceToTarget_1;
        private static DelegateBridge __Hotfix_CalcDistanceToTarget_0;
        private static DelegateBridge __Hotfix_CalcDistanceToLocation;
        private static DelegateBridge __Hotfix_GetLocation;
        private static DelegateBridge __Hotfix_GetRotation;
        private static DelegateBridge __Hotfix_get_Conf3DInfo;
        private static DelegateBridge __Hotfix_get_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_set_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_get_IsHideInTargetList;
        private static DelegateBridge __Hotfix_set_IsHideInTargetList;
        private static DelegateBridge __Hotfix_get_IsNpcNotAttackable;
        private static DelegateBridge __Hotfix_set_IsNpcNotAttackable;

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompSpaceTargetBase()
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToLocation(Vector3D location, bool considerSpaceObjectCollider = false)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToTarget(Vector3D position)
        {
        }

        [MethodImpl(0x8000)]
        public double CalcDistanceToTarget(ILBSpaceObject target, bool considerSpaceObjectCollider = false)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetAllienceId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetGuildId()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompPVPBase GetLBPVP()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLockedMeNpcTargetCount()
        {
        }

        [MethodImpl(0x8000)]
        public float GetMinAroundRadius()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNpcTargetCountInHateList()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetObjectId()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcess GetProcessByInstanceId(uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3D GetRotation()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetRotationQuaternion()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSpaceObjectRadiusMax()
        {
        }

        [MethodImpl(0x8000)]
        public string GetSpaceTargetName()
        {
        }

        [MethodImpl(0x8000)]
        public object GetSpaceTargetOwner()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetArmor()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTargetPriority()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetShield()
        {
        }

        [MethodImpl(0x8000)]
        public Vector4D GetTargetVelocity()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasBuf(int bufId, ulong instanceid)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAvailable4WeaponEquipTarget(bool isHostile)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBufLifeEnd(ulong instanceid)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDead()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsExistFightBuf(int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsHiredCaptainShip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInJumping()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInJumpPrepare()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInvisible()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNpcPolice()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNpcShip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPlayerShip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStateFreeze()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWaitForRemove()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWithNotAttackableFlag()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWithShieldRepairerDisableFlag()
        {
        }

        public abstract ulong OnAttachBufByEquip(int bufId, uint bufLifeTime, float bufInstanceParam1, ILBSpaceTarget srcTarget, EquipType equipType);
        [MethodImpl(0x8000)]
        public void OnClearInvisibleByTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnClearLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        public abstract void OnDetachBufByEquip(int bufId, ulong instanceid);
        public abstract void OnEnemyDroneFighterAttach(LBSpaceProcessDroneFighterLaunch process);
        public abstract void OnHitByBullet(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess bulletFlyProcess);
        public abstract void OnHitByDrone(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, bool isCritical, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess, int droneIndex);
        public abstract void OnHitByEquip(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isHit, LBBulletDamageInfo damageInfo, LBSpaceProcess launchProcess);
        public abstract void OnHitByLaser(ILBSpaceTarget srcTarget, ShipEquipSlotType slotType, int slotIndex, bool isCritical, float damageMulti, LBBulletDamageInfo damageInfo, LBSpaceProcess singleLaserFireProcess);
        [MethodImpl(0x8000)]
        public void OnKillOtherTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLockedByOtherTarget(ILBSpaceTarget srcTarget)
        {
        }

        public abstract void OnMissileInComming(LBSpaceProcessMissileLaunch process);
        [MethodImpl(0x8000)]
        public void OnOtherTargetBlink(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetInvisibleEnd(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetInvisibleStart(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetJump(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsPolice(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetPriority(int priority)
        {
        }

        public ConfigDataSpaceShip3DInfo Conf3DInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public virtual TargetNoticeType TargetNoticeFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public virtual bool? IsHideInTargetList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public virtual bool? IsNpcNotAttackable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

