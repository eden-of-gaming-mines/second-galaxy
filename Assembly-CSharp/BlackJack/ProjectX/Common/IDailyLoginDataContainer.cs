﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IDailyLoginDataContainer
    {
        bool AddNewReward(DailyLoginRewardInfo newRewardInfo);
        void ClearRewardList();
        int GetCurrDayIndex();
        List<DailyLoginRewardInfo> GetRewardList();
        void SetDayRewardState(int dayIndex, DailyLoginRewardState rewarState);
    }
}

