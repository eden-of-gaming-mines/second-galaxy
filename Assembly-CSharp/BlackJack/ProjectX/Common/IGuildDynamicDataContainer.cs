﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildDynamicDataContainer
    {
        GuildDynamicInfo GetGuildDynamicInfo();
        void SetGuildDynamicInfo(GuildDynamicInfo info);
    }
}

