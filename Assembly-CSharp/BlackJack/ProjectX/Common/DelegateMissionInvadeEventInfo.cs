﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class DelegateMissionInvadeEventInfo
    {
        public DelegateMissionInvadeEventType m_eventType;
        public DateTime m_time;
        public NpcCaptainSimpleInfo m_relativeCaptain;
        public PlayerSimpleInfo m_relativePlayer;
        public PlayerSimpleInfo m_killerPlayerInfo;
        public NpcCaptainSimpleInfo m_killerCaptainInfo;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

