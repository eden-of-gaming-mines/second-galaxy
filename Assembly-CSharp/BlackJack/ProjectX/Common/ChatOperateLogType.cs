﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum ChatOperateLogType
    {
        Wisper = 1,
        Local = 2,
        Team = 3,
        SolarSystem = 4,
        StarField = 5,
        Guild = 6,
        Alliance = 7,
        Sys = 8,
        PersonalMail = 9,
        GuildMail = 10,
        GuildMessage = 11,
        AllianceMail = 12
    }
}

