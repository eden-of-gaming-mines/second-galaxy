﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildDiplomacyFriendlyViewInfo
    {
        public List<PlayerSimplestInfo> m_friendlyPlayer;
        public List<GuildSimplestInfo> m_friendlyGuild;
        public List<AllianceBasicInfo> m_friendlyAllience;
        public ushort m_version;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

