﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildTradePortExtraInfo
    {
        public int m_solarSystemId;
        public ulong m_purchaseInstanceId;
        public ulong m_outGoingTransportInstanceId;
        public List<GuildTradePurchaseOrderTransportLogInfo> m_logList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

