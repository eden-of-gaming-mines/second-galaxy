﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    public class SpaceSignalResultInfo
    {
        public SignalType m_type;
        public DateTime m_expiryTime;
        public List<SignalInfo> m_infoList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

