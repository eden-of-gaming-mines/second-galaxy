﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class GuildCompTradeBase : GuildCompBase, IGuildCompTradeBase, IGuildTradeBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_GetGuildTradePortExtraInfoBySolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildTradePortExtraInfoList;
        private static DelegateBridge __Hotfix_GetSpaceShipConfigInfoByTradePortSolarSystemId;
        private static DelegateBridge __Hotfix_GetGuildTradePortDataVersion;
        private static DelegateBridge __Hotfix_CheckMemberGuildTradeManagerPermission;

        [MethodImpl(0x8000)]
        public GuildCompTradeBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckMemberGuildTradeManagerPermission(string gameUserId, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetGuildTradePortDataVersion()
        {
        }

        [MethodImpl(0x8000)]
        public GuildTradePortExtraInfo GetGuildTradePortExtraInfoBySolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildTradePortExtraInfo> GetGuildTradePortExtraInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetSpaceShipConfigInfoByTradePortSolarSystemId(int solarSystemId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

