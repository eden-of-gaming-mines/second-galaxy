﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ICommanderAuthDataContainerClient
    {
        bool GetCommanderAuthRewardRecvedStatu();
        void SetCommanderAuthRewardRecvedStatu(bool statu);
    }
}

