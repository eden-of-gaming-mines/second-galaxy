﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBStaticHiredCaptain : ILBShipCaptain, IPropertiesProvider
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnPropertiesGroupDirty;
        protected NpcCaptainInfo m_readOnlyInfo;
        protected IHiredCaptainManagementDataContainer m_dc;
        protected BasicPropertiesInfo m_basicProperties;
        protected List<LBNpcCaptainFeats> m_initFeats;
        protected List<LBNpcCaptainFeats> m_additionFeats;
        protected List<LBNpcCaptainFeats> m_shipFeats;
        protected List<ShipInfo> m_shipInfoList;
        protected List<LBPassiveSkill> m_passiveSkillList;
        protected ShipInfo m_currShip;
        protected LBBuffContainer m_bufContainer;
        private ILBBuffFactory m_bufFactory;
        protected ConfigDataNpcCaptainShipGrowthLineInfo m_captainShipGrowthLineConf;
        protected string m_name;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddExp;
        private static DelegateBridge __Hotfix_CheckAddExp;
        private static DelegateBridge __Hotfix_UpdateAllFeatsInfo;
        private static DelegateBridge __Hotfix_UpdateFeatsInfo;
        private static DelegateBridge __Hotfix_InitFeats;
        private static DelegateBridge __Hotfix_GetInitFeats;
        private static DelegateBridge __Hotfix_GetAdditionFeats;
        private static DelegateBridge __Hotfix_GetAdditionFeatsById;
        private static DelegateBridge __Hotfix_GetInitFeatsById;
        private static DelegateBridge __Hotfix_AddAdditionFeats;
        private static DelegateBridge __Hotfix_AddInitFeats;
        private static DelegateBridge __Hotfix_RemoveInitFeats;
        private static DelegateBridge __Hotfix_AddAdditionFeatsImpl;
        private static DelegateBridge __Hotfix_RemoveAddtionFeats;
        private static DelegateBridge __Hotfix_HasFeatsIncludeAllLevel;
        private static DelegateBridge __Hotfix_GetShipFeatsCount;
        private static DelegateBridge __Hotfix_GetShipFeats;
        private static DelegateBridge __Hotfix_GetFeatsById;
        private static DelegateBridge __Hotfix_GetRandomLevelUpableFeats;
        private static DelegateBridge __Hotfix_ActiveNpcCaptainShipDriveLisenceSkill;
        private static DelegateBridge __Hotfix_ActiveFeatsPassiveSkill;
        private static DelegateBridge __Hotfix_DeactiveFeatsPassiveSkill;
        private static DelegateBridge __Hotfix_InitShips;
        private static DelegateBridge __Hotfix_InitCurrShip;
        private static DelegateBridge __Hotfix_InitFeatsShipInfo;
        private static DelegateBridge __Hotfix_GetShipGrowthLineInfo;
        private static DelegateBridge __Hotfix_AttachShip;
        private static DelegateBridge __Hotfix_DetachShip;
        private static DelegateBridge __Hotfix_GetShipList;
        private static DelegateBridge __Hotfix_SetCurrShip;
        private static DelegateBridge __Hotfix_GetCurrShip;
        private static DelegateBridge __Hotfix_GetCurrShipConfigInfo;
        private static DelegateBridge __Hotfix_GetCurrShipType;
        private static DelegateBridge __Hotfix_SetShipAvialable;
        private static DelegateBridge __Hotfix_SetShipRepairState;
        private static DelegateBridge __Hotfix_SetCurrShipRepairState;
        private static DelegateBridge __Hotfix_GetShipInfoByNpcCaptainShipId;
        private static DelegateBridge __Hotfix_GetShipDamageCapabilityValueForCalc;
        private static DelegateBridge __Hotfix_GetShipDamageCapabilityValue;
        private static DelegateBridge __Hotfix_GetShipSurvivalCapabilityValue;
        private static DelegateBridge __Hotfix_GetShipCollectionAbilityValue;
        private static DelegateBridge __Hotfix_GetShipOneRoundMiningCount;
        private static DelegateBridge __Hotfix_GetShipItemStoreSpaceMax;
        private static DelegateBridge __Hotfix_GetDelegateSignalSpeedMulti;
        private static DelegateBridge __Hotfix_GetDelegateSignalRewardMulti;
        private static DelegateBridge __Hotfix_CalcPropertiesById;
        private static DelegateBridge __Hotfix_GetConfigDataSpaceShipInfoByNpcCaptainShipId;
        private static DelegateBridge __Hotfix_CalcShipItemStoreSpaceMaxByConf;
        private static DelegateBridge __Hotfix_CalcCaptainBasicPropertiesWhenLevelUp;
        private static DelegateBridge __Hotfix_CalcCaptainBasicProperties_0;
        private static DelegateBridge __Hotfix_CalcCaptainBasicProperties_1;
        private static DelegateBridge __Hotfix_GetLevelUpRandomPropertyGrowth;
        private static DelegateBridge __Hotfix_GetCaptainDrivingLicenseId;
        private static DelegateBridge __Hotfix_GetCaptainShipDrivinglicenseIdLevel;
        private static DelegateBridge __Hotfix_GetCaptainDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_GetCaptainNameByNameId;
        private static DelegateBridge __Hotfix_GetNpcCaptainInfo;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetResId;
        private static DelegateBridge __Hotfix_GetLastNameId;
        private static DelegateBridge __Hotfix_GetName;
        private static DelegateBridge __Hotfix_GetFirstNameId;
        private static DelegateBridge __Hotfix_GetProfession;
        private static DelegateBridge __Hotfix_GetSubRank;
        private static DelegateBridge __Hotfix_GetGrandFaction;
        private static DelegateBridge __Hotfix_GetGender;
        private static DelegateBridge __Hotfix_GetAge;
        private static DelegateBridge __Hotfix_GetBirthDay;
        private static DelegateBridge __Hotfix_GetConstellation;
        private static DelegateBridge __Hotfix_GetPersonalityType;
        private static DelegateBridge __Hotfix_GetCaptainShipGrowthLineId;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetExp;
        private static DelegateBridge __Hotfix_GetTotalExp;
        private static DelegateBridge __Hotfix_GetCurrShipId;
        private static DelegateBridge __Hotfix_GetCaptainState;
        private static DelegateBridge __Hotfix_SetInDelegateMission;
        private static DelegateBridge __Hotfix_SetInStationWorkingOnTech;
        private static DelegateBridge __Hotfix_SetInStationWorkingOnProduction;
        private static DelegateBridge __Hotfix_IsInDelegateMission;
        private static DelegateBridge __Hotfix_IsInStationWorking;
        private static DelegateBridge __Hotfix_IsInStationWorkingOnTech;
        private static DelegateBridge __Hotfix_IsInStationWorkingOnProduction;
        private static DelegateBridge __Hotfix_CalcHiredCaptainScore;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.ILBShipCaptain.GetDrivingLicenseLevelById;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_OnEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_add_EventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_remove_EventOnPropertiesGroupDirty;

        public event Action<int> EventOnPropertiesGroupDirty
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public LBStaticHiredCaptain(NpcCaptainInfo info, IHiredCaptainManagementDataContainer dc, ILBBuffFactory bufFactory)
        {
        }

        [MethodImpl(0x8000)]
        protected void ActiveFeatsPassiveSkill(LBNpcCaptainFeats feats)
        {
        }

        [MethodImpl(0x8000)]
        protected void ActiveNpcCaptainShipDriveLisenceSkill(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats AddAdditionFeats(int featsId, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected LBNpcCaptainFeats AddAdditionFeatsImpl(int featsId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public int AddExp(long addExp, int currPlayerLevel)
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats AddInitFeats(int featsId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public void AttachShip(int npcCaptainShipId, LBStaticHiredCaptainShipBase ship)
        {
        }

        [MethodImpl(0x8000)]
        int? ILBShipCaptain.GetDrivingLicenseLevelById(int drivingLicenseId)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public static BasicPropertiesInfo CalcCaptainBasicProperties(NpcCaptainInfo captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static BasicPropertiesInfo CalcCaptainBasicProperties(ProfessionType profession, SubRankType subRank, GrandFaction faction, int currLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static BasicPropertiesInfo CalcCaptainBasicPropertiesWhenLevelUp(NpcCaptainInfo captainInfo, BasicPropertiesInfo currentProperties, int levelUpCount)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcHiredCaptainScore()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcShipItemStoreSpaceMaxByConf(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public int CheckAddExp(long addExp, int currPlayerLevel, out long wasteExp, out bool isTopLevel, out bool arriveTopLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected void DeactiveFeatsPassiveSkill(LBNpcCaptainFeats feats)
        {
        }

        [MethodImpl(0x8000)]
        public void DetachShip()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBNpcCaptainFeats> GetAdditionFeats()
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats GetAdditionFeatsById(int featsId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetAge()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetBirthDay()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetCaptainDrivingLicenseId(NpcCaptainInfo captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetCaptainDrivingLicenseLevel(NpcCaptainInfo captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCaptainNameByNameId(int firstNameId, int lastNameId, GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static IdLevelInfo GetCaptainShipDrivinglicenseIdLevel(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCaptainShipGrowthLineId()
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainState GetCaptainState()
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataSpaceShipInfo GetConfigDataSpaceShipInfoByNpcCaptainShipId(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public ConstellationType GetConstellation()
        {
        }

        [MethodImpl(0x8000)]
        public ShipInfo GetCurrShip()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetCurrShipConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrShipId()
        {
        }

        [MethodImpl(0x8000)]
        public ShipType GetCurrShipType()
        {
        }

        [MethodImpl(0x8000)]
        public float GetDelegateSignalRewardMulti(SignalType signalType)
        {
        }

        [MethodImpl(0x8000)]
        public float GetDelegateSignalSpeedMulti(SignalType signalType)
        {
        }

        [MethodImpl(0x8000)]
        public long GetExp()
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats GetFeatsById(int featsId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetFirstNameId()
        {
        }

        [MethodImpl(0x8000)]
        public GenderType GetGender()
        {
        }

        [MethodImpl(0x8000)]
        public GrandFaction GetGrandFaction()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBNpcCaptainFeats> GetInitFeats()
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats GetInitFeatsById(int featsId)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLastNameId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        private static void GetLevelUpRandomPropertyGrowth(ConfigDataNpcCaptainGrowthInfo captainGrowthConf, out int randomAttack, out int randomDefence, out int randomElectron, out int randomDrive)
        {
        }

        [MethodImpl(0x8000)]
        public string GetName()
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainInfo GetNpcCaptainInfo()
        {
        }

        [MethodImpl(0x8000)]
        public NpcPersonalityType GetPersonalityType()
        {
        }

        [MethodImpl(0x8000)]
        public ProfessionType GetProfession()
        {
        }

        [MethodImpl(0x8000)]
        public LBNpcCaptainFeats GetRandomLevelUpableFeats(Random rand)
        {
        }

        [MethodImpl(0x8000)]
        public int GetResId()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipCollectionAbilityValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipDamageCapabilityValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipDamageCapabilityValueForCalc()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBNpcCaptainFeats> GetShipFeats()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipFeatsCount()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcCaptainShipGrowthLineInfo GetShipGrowthLineInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ShipInfo GetShipInfoByNpcCaptainShipId(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipItemStoreSpaceMax()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipInfo> GetShipList()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipOneRoundMiningCount(float miningMult)
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipSurvivalCapabilityValue()
        {
        }

        [MethodImpl(0x8000)]
        public SubRankType GetSubRank()
        {
        }

        [MethodImpl(0x8000)]
        public long GetTotalExp()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasFeatsIncludeAllLevel(int featsId)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitCurrShip()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFeats()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFeatsShipInfo(LBNpcCaptainFeats feats)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitShips()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInDelegateMission()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInStationWorking()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInStationWorkingOnProduction()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInStationWorkingOnTech()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEventOnPropertiesGroupDirty(int groupId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAddtionFeats(LBNpcCaptainFeats feats)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveInitFeats(LBNpcCaptainFeats feats)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrShip(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrShipRepairState(bool needRepair)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInDelegateMission(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInStationWorkingOnProduction(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInStationWorkingOnTech(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipAvialable(int npcCaptainShipId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipRepairState(int npcCaptainShipId, bool needRepair)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllFeatsInfo(List<IdLevelInfo> additionFeats, List<IdLevelInfo> InitFeats, out List<LBNpcCaptainFeats> newFeatsList, out List<LBNpcCaptainFeats> upgradeFeatsList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFeatsInfo(List<IdLevelInfo> additionFeats, out List<LBNpcCaptainFeats> newFeatsList, out List<LBNpcCaptainFeats> upgradeFeatsList)
        {
        }

        public class ShipInfo
        {
            public NpcCaptainShipInfo m_avilableShipInfo;
            public int m_npcCaptainShipId;
            public ConfigDataNpcCaptainShipInfo m_npcCaptainShipConf;
            public LBNpcCaptainFeats m_dependFeats;
            public LBStaticHiredCaptain.ShipInfo m_dependShipInfo;
            public LBStaticHiredCaptainShipBase m_ship;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_m_isAvailable;
            private static DelegateBridge __Hotfix_get_m_needRepair;

            public bool m_isAvailable
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }

            public bool m_needRepair
            {
                [MethodImpl(0x8000)]
                get
                {
                }
            }
        }
    }
}

