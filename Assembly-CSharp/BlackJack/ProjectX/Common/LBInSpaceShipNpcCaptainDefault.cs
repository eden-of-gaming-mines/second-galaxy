﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBInSpaceShipNpcCaptainDefault : LBInSpaceShipCaptainBase
    {
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_IsPlayerCaptain;
        private static DelegateBridge __Hotfix_IsHiredCaptain;
        private static DelegateBridge __Hotfix_GetPlayerGameUserId;
        private static DelegateBridge __Hotfix_GetHiredCaptainInstanceId;
        private static DelegateBridge __Hotfix_GetCaptainName;
        private static DelegateBridge __Hotfix_GetCaptainFirstNameId;
        private static DelegateBridge __Hotfix_GetCaptainLastNameId;

        [MethodImpl(0x8000)]
        public LBInSpaceShipNpcCaptainDefault()
        {
        }

        [MethodImpl(0x8000)]
        public LBInSpaceShipNpcCaptainDefault(BasicPropertiesInfo basicPropertiesInfo, int drivingLicenseId, int drivingLicenseLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetCaptainFirstNameId()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetCaptainLastNameId()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetCaptainName()
        {
        }

        [MethodImpl(0x8000)]
        public override ulong GetHiredCaptainInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetPlayerGameUserId()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHiredCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsPlayerCaptain()
        {
        }
    }
}

