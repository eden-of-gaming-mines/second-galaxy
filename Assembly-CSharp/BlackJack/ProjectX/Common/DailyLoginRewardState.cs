﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Serializable]
    public enum DailyLoginRewardState
    {
        None,
        CanTake,
        Token
    }
}

