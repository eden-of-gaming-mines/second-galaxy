﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public static class ShipViewPropertiesHelper
    {
        private static DelegateBridge __Hotfix_CalcStaticWeaponGroupDPS;
        private static DelegateBridge __Hotfix_CalcInSpaceWeaponGroupDPS;
        private static DelegateBridge __Hotfix_CalcWeaponGroupDPS;
        private static DelegateBridge __Hotfix_CalcRailgunPlasmaCannonDPS;
        private static DelegateBridge __Hotfix_CalcDroneDPS;
        private static DelegateBridge __Hotfix_CalcLaserDPS;
        private static DelegateBridge __Hotfix_CalcMissileDPS;
        private static DelegateBridge __Hotfix_GetWaveCountForOneShoot;
        private static DelegateBridge __Hotfix_GetTotalUnitCountInWeaponGroup;
        private static DelegateBridge __Hotfix_CalcDamageTotal;
        private static DelegateBridge __Hotfix_CalcRealDamageElectric;
        private static DelegateBridge __Hotfix_CalcRealDamageHeat;
        private static DelegateBridge __Hotfix_CalcRealDamageKinetic;
        private static DelegateBridge __Hotfix_CalcDroneFightTime;
        private static DelegateBridge __Hotfix_CalcCD4DroneNextAttack;
        private static DelegateBridge __Hotfix_CalcLaserDamageMultiSumDuringAttackTime;
        private static DelegateBridge __Hotfix_CalcWeaponHitRateFinal;
        private static DelegateBridge __Hotfix_CalcWeaponGroupCD;

        [MethodImpl(0x8000)]
        private static ushort CalcCD4DroneNextAttack(int ammoId)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcDamageTotal(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcDroneDPS(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category, int weaponConfId, int droneAmmoId)
        {
        }

        [MethodImpl(0x8000)]
        private static ushort CalcDroneFightTime(LBWeaponEquipGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcInSpaceWeaponGroupDPS(LBWeaponGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcLaserDamageMultiSumDuringAttackTime(int weaponConfId)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcLaserDPS(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category, int weaponConfId)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcMissileDPS(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category, int weaponConfId)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcRailgunPlasmaCannonDPS(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category, int weaponConfId)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcRealDamageElectric(LBWeaponEquipGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcRealDamageHeat(LBWeaponEquipGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcRealDamageKinetic(LBWeaponEquipGroupBase weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcStaticWeaponGroupDPS(LBStaticWeaponEquipSlotGroup weaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        private static uint CalcWeaponGroupCD(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcWeaponGroupDPS(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category, int weaponConfId, int ammoId)
        {
        }

        [MethodImpl(0x8000)]
        private static float CalcWeaponHitRateFinal(LBWeaponEquipGroupBase weaponGroup, WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private static int GetTotalUnitCountInWeaponGroup(int weaponConfId)
        {
        }

        [MethodImpl(0x8000)]
        private static int GetWaveCountForOneShoot(int weaponConfId)
        {
        }
    }
}

