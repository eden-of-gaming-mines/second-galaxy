﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class InSpaceNpcShipDataContainerDefault : NpcShipDataContainerBase, IInSpaceNpcShipDataContainer, INpcShipDataContainer, IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_UpdateShipName;
        private static DelegateBridge __Hotfix_GetMasterShipObjId;

        [MethodImpl(0x8000)]
        public InSpaceNpcShipDataContainerDefault(SolarSystemNpcShipInstanceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetMasterShipObjId()
        {
        }

        [MethodImpl(0x8000)]
        public override void UpdateShipName(string shipName)
        {
        }
    }
}

