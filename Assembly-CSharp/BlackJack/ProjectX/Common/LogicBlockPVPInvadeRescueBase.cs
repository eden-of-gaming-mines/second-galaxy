﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockPVPInvadeRescueBase
    {
        public Action<ulong> EventOnSelfMissionInvadeStart;
        public Action<ulong> EventOnSelfMissionInvadeEnd;
        public Action<ulong, int, uint> EventOnGuildMemberMissionInvadeStart;
        public Action<ulong, int, uint> EventOnGuildMemberMissionInvadeEnd;
        protected LogicBlockSignalBase m_lbSignal;
        protected LogicBlockDelegateMissionBase m_lbDelegateMission;
        protected ILBPlayerContext m_playerCtx;
        protected List<LBPVPInvadeRescue> m_invadeRescueList;
        private IInvadeRescueDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CreatePVPInvadeRescueInfo;
        private static DelegateBridge __Hotfix_OnSelfMissionInvadeStart;
        private static DelegateBridge __Hotfix_OnSelfMissionInvadeEnd;
        private static DelegateBridge __Hotfix_OnGuildMemberInvadeStart;
        private static DelegateBridge __Hotfix_OnGuildMemberInvadeEnd;
        private static DelegateBridge __Hotfix_GetInvadeRescueList;
        private static DelegateBridge __Hotfix_GetInvadeRescueByInstanceId;
        private static DelegateBridge __Hotfix_GetSelfInvadeRescueCount;
        private static DelegateBridge __Hotfix_AddPVPInvadeRescue;
        private static DelegateBridge __Hotfix_RemovePVPInvadeRescue;

        [MethodImpl(0x8000)]
        protected LBPVPInvadeRescue AddPVPInvadeRescue(PVPInvadeRescueInfo rescueInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static PVPInvadeRescueInfo CreatePVPInvadeRescueInfo(LBDelegateMission mission, PlayerSimpleInfo sourcePlayerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPInvadeRescue GetInvadeRescueByInstanceId(ulong instanceId, int manualQuestSolarSystemId = 0, uint manualQuestInstanceId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPVPInvadeRescue> GetInvadeRescueList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSelfInvadeRescueCount()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberInvadeEnd(ulong signalInstanceId, int manualQuestSolarsystemId, uint manualQuestSceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGuildMemberInvadeStart(PVPInvadeRescueInfo invadeInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSelfMissionInvadeEnd(LBDelegateMission mission)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSelfMissionInvadeStart(LBDelegateMission mission)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemovePVPInvadeRescue(ulong signalInstanceId, int manualQuestSolarsystemId = 0, uint manualQuestSceneInstanceId = 0)
        {
        }
    }
}

