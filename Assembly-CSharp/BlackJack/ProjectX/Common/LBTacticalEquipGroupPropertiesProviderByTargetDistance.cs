﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderByTargetDistance : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        protected ILogicBlockShipCompSpaceObject m_lbSpaceObj;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <DistanceFactorMax>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <DistanceMax>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <DistanceFactorMin>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <DistanceMin>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <FactorLessThanDistanceMin>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <FactorMoreThanDistanceMax>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnDistanceToTarget;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnDistanceToTarget;
        private static DelegateBridge __Hotfix_get_DistanceFactorMax;
        private static DelegateBridge __Hotfix_set_DistanceFactorMax;
        private static DelegateBridge __Hotfix_get_DistanceMax;
        private static DelegateBridge __Hotfix_set_DistanceMax;
        private static DelegateBridge __Hotfix_get_DistanceFactorMin;
        private static DelegateBridge __Hotfix_set_DistanceFactorMin;
        private static DelegateBridge __Hotfix_get_DistanceMin;
        private static DelegateBridge __Hotfix_set_DistanceMin;
        private static DelegateBridge __Hotfix_get_FactorLessThanDistanceMin;
        private static DelegateBridge __Hotfix_set_FactorLessThanDistanceMin;
        private static DelegateBridge __Hotfix_get_FactorMoreThanDistanceMax;
        private static DelegateBridge __Hotfix_set_FactorMoreThanDistanceMax;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderByTargetDistance(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnDistanceToTarget(ref float propertyValue, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnDistanceToTarget(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float DistanceFactorMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public float DistanceMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public float DistanceFactorMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public float DistanceMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public float FactorLessThanDistanceMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }

        public float FactorMoreThanDistanceMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            protected set
            {
            }
        }
    }
}

