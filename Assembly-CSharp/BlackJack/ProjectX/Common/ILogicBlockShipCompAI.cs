﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface ILogicBlockShipCompAI
    {
        float GetChaseDistance();
        float GetGuardRadius();
        Vector3D GetRequiredPosition();
        bool IsAutoFireEnabled();
        bool IsChaseTarget();
        void ResumeFightingAICtrl();
        void ReturnDefaultMovementState();
        void SetChaseTarget(bool isChase, float chaseDistance);
        void SetGuardRadius(float guardRadius);
        void ShutdownFightingAICtrl(bool stopMove);
        void StartMovementStateAroundLocation(Vector3D location, bool isDefault = false, float radius = 0f);
        void StartMovementStateAroundTarget(ILBSpaceTarget target, bool isDefault = false, float radius = 0f);
        void StartMovementStateAroundTarget(uint targetObjId, bool isDefault = false, float radius = 0f);
        void StartMovementStateFollowTarget(ILBSpaceTarget target, bool isDefault = false, float radius = 0f);
        void StartMovementStateFollowTarget(uint targetObjId, bool isDefault = false, float radius = 0f);
        void StartMovementStateJump2Location(Vector3D location);
        void StartMovementStateKeepFormation(ILBSpaceTarget target, IAIMovementFormatMaker formationMaker, bool isDefault = false);
        void StartMovementStateRanger(bool isDefault = false);
        void StartMovementStateStand(Vector3D location, bool isDefault = false);
        void StartMovementWayPoint(List<Vector3D> wayPointList, WayPointMode mode, bool isDefault = false);
        void StopFighting();
        bool TryUseBlink();
        bool TryUseBooster();
        bool TryUseEnemyDebufEquip(EquipType equipType = 0x30);
        bool TryUseSelfArmorRepairer();
        bool TryUseSelfEnergyHotEquip();
        bool TryUseSelfEnhanceEquip(EquipType equipType = 0x30);
        bool TryUseSelfShieldRepairer();
    }
}

