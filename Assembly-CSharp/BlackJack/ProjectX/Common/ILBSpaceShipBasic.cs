﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface ILBSpaceShipBasic
    {
        ILBBuffFactory GetBufFactory();
        float GetDefaultAroundRadius(ILBSpaceTarget target);
        ConfigDataSpaceShipInfo GetShipConfInfo();
        IShipDataContainer GetShipDataContainer();
    }
}

