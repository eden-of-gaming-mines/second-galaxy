﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IEquipFunctionCompInOutRangeTriggerOwnerBase : IEquipFunctionCompOwner
    {
        uint GetDuration();
    }
}

