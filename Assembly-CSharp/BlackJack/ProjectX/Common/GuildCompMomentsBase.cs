﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompMomentsBase : GuildCompBase, IGuildCompMomentsBase, IGuildMomentsBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetGuildMomentsInfoList;
        private static DelegateBridge __Hotfix_GetGuildMomentsBriefInfoList;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompMomentsBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMomentsInfo> GetGuildMomentsBriefInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMomentsInfo> GetGuildMomentsInfoList()
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

