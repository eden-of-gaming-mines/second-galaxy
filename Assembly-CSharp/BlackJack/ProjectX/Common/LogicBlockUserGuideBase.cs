﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class LogicBlockUserGuideBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IUserGuideDataContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;

        [MethodImpl(0x8000)]
        protected LogicBlockUserGuideBase()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        public abstract bool IsStepGroupAlreadyCompleted(int groupId);
    }
}

