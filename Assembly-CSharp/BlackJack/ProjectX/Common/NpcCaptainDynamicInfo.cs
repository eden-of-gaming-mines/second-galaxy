﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class NpcCaptainDynamicInfo
    {
        public long m_exp;
        public int m_level;
        public NpcCaptainState m_state;
        public List<IdLevelInfo> m_initFeats;
        public List<IdLevelInfo> m_additionFeats;
        public int m_currShipId;
        public List<NpcCaptainShipInfo> m_availableShipList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

