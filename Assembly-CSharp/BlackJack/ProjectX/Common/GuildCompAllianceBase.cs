﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompAllianceBase : GuildCompBase, IGuildCompAllianceBase, IGuildAllianceBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DC;
        private static DelegateBridge __Hotfix_OnReceiveAllianceInvite;
        private static DelegateBridge __Hotfix_GetAllianceInviteList;

        [MethodImpl(0x8000)]
        public GuildCompAllianceBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildAllianceInviteInfo> GetAllianceInviteList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnReceiveAllianceInvite(GuildAllianceInviteInfo invite)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

