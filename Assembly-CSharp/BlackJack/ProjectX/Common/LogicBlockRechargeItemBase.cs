﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LogicBlockRechargeItemBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IRechargeItemDataContainer m_dc;
        private readonly List<LBRechargeItem> m_lbRechargeItemList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_InitLBRechargeItemList;
        private static DelegateBridge __Hotfix_GetRechargeItem;
        private static DelegateBridge __Hotfix_GetRechargeItemList;
        private static DelegateBridge __Hotfix_AddRechargeItem;
        private static DelegateBridge __Hotfix_GetRechargeItemListVersion;

        [MethodImpl(0x8000)]
        public void AddRechargeItem(RechargeItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public LBRechargeItem GetRechargeItem(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBRechargeItem> GetRechargeItemList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRechargeItemListVersion()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLBRechargeItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }
    }
}

