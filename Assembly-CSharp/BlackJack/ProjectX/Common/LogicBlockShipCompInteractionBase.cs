﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompInteractionBase
    {
        protected ILBInSpaceShip m_ownerShip;
        protected SceneInteractionType m_interactionType;
        protected uint m_interactionTime;
        protected int m_interactionMsgId;
        protected uint m_interactionRange;
        protected SceneInteractionFlag m_interactionFlag;
        protected ConfigDataNpcInteractionTemplate m_interactionTemplateConfInfo;
        protected uint m_singlePlayerInteractionCountMax;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_SetInteraction;
        private static DelegateBridge __Hotfix_ClearInteraction;
        private static DelegateBridge __Hotfix_GetInteractionType;
        private static DelegateBridge __Hotfix_GetInteractionTime;
        private static DelegateBridge __Hotfix_GetInteractionRange;
        private static DelegateBridge __Hotfix_GetInteractionFlag;
        private static DelegateBridge __Hotfix_GetInteractionMsgId;
        private static DelegateBridge __Hotfix_GetInteractionTemplate;
        private static DelegateBridge __Hotfix_GetSinglePlayerInteractionCountMax;
        private static DelegateBridge __Hotfix_IsInteractionAvailable;

        [MethodImpl(0x8000)]
        public virtual void ClearInteraction()
        {
        }

        [MethodImpl(0x8000)]
        public SceneInteractionFlag GetInteractionFlag()
        {
        }

        [MethodImpl(0x8000)]
        public int GetInteractionMsgId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetInteractionRange()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataNpcInteractionTemplate GetInteractionTemplate()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetInteractionTime()
        {
        }

        [MethodImpl(0x8000)]
        public SceneInteractionType GetInteractionType()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetSinglePlayerInteractionCountMax()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsInteractionAvailable(ILBInSpacePlayerShip playerShip, out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetInteraction(SceneInteractionType type, uint optTime, uint range, int message, SceneInteractionFlag flag, int interactionTemplateId = 0, uint singlePlayerInteractionCountMax = 0)
        {
        }
    }
}

