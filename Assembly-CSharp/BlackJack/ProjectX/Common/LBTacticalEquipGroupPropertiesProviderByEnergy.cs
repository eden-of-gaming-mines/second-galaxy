﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderByEnergy : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <EnergyPercentMax>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <EnergyPercentMin>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <FinalModifyFactor4EnergyMax>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <FinalModifyFactor4EnergyMin>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <FinalModifyFactor4ThanEnergyMax>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <FinalModifyFactor4LessEnergyMin>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalcDependOnSelfShipEnergy;
        private static DelegateBridge __Hotfix_CalcPropertyDependOnSelfShipEnergy;
        private static DelegateBridge __Hotfix_get_EnergyPercentMax;
        private static DelegateBridge __Hotfix_set_EnergyPercentMax;
        private static DelegateBridge __Hotfix_get_EnergyPercentMin;
        private static DelegateBridge __Hotfix_set_EnergyPercentMin;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4EnergyMax;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4EnergyMax;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4EnergyMin;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4EnergyMin;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4ThanEnergyMax;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4ThanEnergyMax;
        private static DelegateBridge __Hotfix_get_FinalModifyFactor4LessEnergyMin;
        private static DelegateBridge __Hotfix_set_FinalModifyFactor4LessEnergyMin;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderByEnergy(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertyDependOnSelfShipEnergy(ref float propertyValue)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalcDependOnSelfShipEnergy(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float EnergyPercentMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float EnergyPercentMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4EnergyMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4EnergyMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4ThanEnergyMax
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public float FinalModifyFactor4LessEnergyMin
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

