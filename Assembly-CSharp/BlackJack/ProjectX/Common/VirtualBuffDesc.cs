﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    public class VirtualBuffDesc
    {
        public VirtualBufType m_bufType;
        public string m_bufIconPath;
        public string m_bufTitleText;
        public string m_bufDetailDesc;
        public List<string> m_bufEffectFormatStrList;
        public List<float> m_bufEffectValueList;
        public List<object> m_paramList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

