﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public abstract class LogicBlockStationContextBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected LogicBlockNpcShopBase m_lbNpcShop;
        protected LogicBlockFactionCreditQuestBase m_lbFactionCreditQuest;
        protected GDBSpaceStationInfo m_gdbSpaceStationInfo;
        protected ConfigDataSpaceStationResInfo m_spaceStationResInfo;
        protected ConfigDataSpaceStation3DInfo m_spaceStation3DInfo;
        protected List<LBNpcTalkerBase> m_npcTalkerList;
        protected NpcDialogInfo m_currNpcDialog;
        protected LogicBlockBlackMarketBase m_lbBlackMarket;
        protected LogicBlockTradeBase m_lbTrade;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsInSpaceStation;
        private static DelegateBridge __Hotfix_OnEnterStation;
        private static DelegateBridge __Hotfix_OnLeaveStation;
        private static DelegateBridge __Hotfix_InitNpcTalkerCtx;
        private static DelegateBridge __Hotfix_ClearNpcTalkerCtx;
        private static DelegateBridge __Hotfix_HasDynamicNpcTalker;
        private static DelegateBridge __Hotfix_GetNpcTalker;
        private static DelegateBridge __Hotfix_GetNpcTalkerInRoom;
        private static DelegateBridge __Hotfix_GetNpcTalkerList;
        private static DelegateBridge __Hotfix_SetupNpcDailogRelativeContext;
        private static DelegateBridge __Hotfix_SetupNpcShopContext;
        private static DelegateBridge __Hotfix_SetupFactionCreditShopContext;
        private static DelegateBridge __Hotfix_SetupBlackMarketContext;
        private static DelegateBridge __Hotfix_SetupTradeContext;
        private static DelegateBridge __Hotfix_IsQuestAcceptableByNpcDialog;
        private static DelegateBridge __Hotfix_Convert2HelloDialogIdByCompletedMainQuestInfoList;
        private static DelegateBridge __Hotfix_Convert2ConfigDataNpcTalkerInfo;

        [MethodImpl(0x8000)]
        protected LogicBlockStationContextBase()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ClearNpcTalkerCtx()
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataNpcTalkerInfo Convert2ConfigDataNpcTalkerInfo(GDBSpaceStationNpcTalkerInfo gdbInfo, GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static List<HelloDialogIdByCompletedMainQuestInfo> Convert2HelloDialogIdByCompletedMainQuestInfoList(List<HelloDialogIdToCompletedMainQuestInfo> info)
        {
        }

        protected abstract LBNpcTalkerBase CreateNpcTalker(ConfigDataNpcTalkerInfo npcTaklerInfo, bool isGlobalNpc);
        [MethodImpl(0x8000)]
        public LBNpcTalkerBase GetNpcTalker(int npcId)
        {
        }

        [MethodImpl(0x8000)]
        public void GetNpcTalkerInRoom(SpaceStationRoomType roomType, List<LBNpcTalkerBase> result)
        {
        }

        [MethodImpl(0x8000)]
        public void GetNpcTalkerList(List<LBNpcTalkerBase> result)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasDynamicNpcTalker()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitNpcTalkerCtx()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInSpaceStation()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestAcceptableByNpcDialog(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnEnterStation(int stationId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnLeaveStation(bool isToSpace)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetupBlackMarketContext(int npcId, NpcDialogOptInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetupFactionCreditShopContext(int npcId, NpcDialogOptInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetupNpcDailogRelativeContext(int npcId, NpcDialogInfo npcDialog)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetupNpcShopContext(int npcId, NpcDialogOptInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetupTradeContext(int npcId, NpcDialogOptInfo info)
        {
        }
    }
}

