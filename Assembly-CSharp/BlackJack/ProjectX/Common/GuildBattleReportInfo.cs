﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildBattleReportInfo
    {
        public ushort m_version;
        public ulong m_guildBattleInstanceId;
        public int m_solarSystemId;
        public GuildBattleType m_type;
        public GuildSimplestInfo m_attackerGuildInfo;
        public GuildSimplestInfo m_defenderGuildInfo;
        public int m_npcGuildConfigId;
        public DateTime m_startTime;
        public DateTime m_endTime;
        public bool m_isAttackerWin;
        public GuildBattleStatus m_status;
        public DateTime m_currStatusStartTime;
        public DateTime m_currStatusEndTime;
        public double m_totalKillingLostBindMoney;
        public int m_totalSelfLostShipCount;
        public double m_defenderKillingLostBindMoney;
        public int m_defenderSelfLostShipCount;
        public double m_attackerKillingLostBindMoney;
        public int m_attackerSelfLostShipCount;
        public List<GuildBattlePlayerKillLostStatInfo> m_playerKillLostStatInfoList;
        public List<GuildBattleGuildKillLostStatInfo> m_guildKillLostStatInfoList;
        public List<GuildBattleShipLostStatInfo> m_shipLostStatInfoList;
        public List<GuildBattleGuildBuildingSimpleInfo> m_guildBuildingSimpleInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

