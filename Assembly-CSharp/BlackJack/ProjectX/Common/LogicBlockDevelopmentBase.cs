﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockDevelopmentBase
    {
        protected ILBPlayerContext m_playerCtx;
        protected IDevelopmentDataContainer m_dc;
        protected LogicBlockItemStoreBase m_lbItemStore;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_HasEnoughItemByFilter1;
        private static DelegateBridge __Hotfix_CheckForDevelopmentProjectStart;
        private static DelegateBridge __Hotfix_GetItemByFilter;
        private static DelegateBridge __Hotfix_CheckItemByFilter;

        [MethodImpl(0x8000)]
        public bool CheckForDevelopmentProjectStart(int projectId, List<ItemInfo> costItems, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckItemByFilter(ItemInfo itemInfo, int filterId, RankType rank, SubRankType subRank, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public void GetItemByFilter(int filterId, RankType rank, SubRankType subRank, out List<ItemInfo> outItems, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool HasEnoughItemByFilter1(List<ItemInfo> items, int filterId, RankType rank, SubRankType subRank, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [CompilerGenerated]
        private sealed class <GetItemByFilter>c__AnonStorey0
        {
            internal int filterId;
            internal RankType rank;
            internal SubRankType subRank;
            internal List<ItemInfo> tempList;
            internal LogicBlockDevelopmentBase $this;

            [MethodImpl(0x8000)]
            internal bool <>m__0(LBStoreItem item)
            {
            }
        }
    }
}

