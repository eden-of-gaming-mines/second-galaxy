﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct StoreItemTransformInfo
    {
        public int m_fromIndex;
        public int m_toIndex;
        public ulong m_toInstanceId;
    }
}

