﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildCurrencyDataContainer
    {
        GuildCurrencyInfo GetCurrencyInfo();
        ulong GetInformationPoint();
        ulong GetTacticalPoint();
        ulong GetTradeMoney();
        void UpdateInformationPoint(ulong informationPoint);
        void UpdateTacticalPoint(ulong tacticalPoint);
        void UpdateTradeMoney(ulong tradeMoney);
    }
}

