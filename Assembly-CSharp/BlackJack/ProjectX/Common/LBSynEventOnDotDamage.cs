﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class LBSynEventOnDotDamage : LBSyncEvent
    {
        public uint m_bufInstanceId;
        public float m_damage;
        public BufType m_bufType;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

