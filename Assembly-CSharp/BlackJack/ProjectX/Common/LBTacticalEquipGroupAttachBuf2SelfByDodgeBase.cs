﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBTacticalEquipGroupAttachBuf2SelfByDodgeBase : LBTacticalEquipGroupBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_OnBulletHitMiss;
        private static DelegateBridge __Hotfix_GetAttachBuffList;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupAttachBuf2SelfByDodgeBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual List<int> GetAttachBuffList()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBulletHitMiss(ILBSpaceTarget srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

