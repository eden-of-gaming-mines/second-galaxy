﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildFlagShipSceneInfo
    {
        public int m_confId;
        public int m_shipHangarIndex;
        public string m_shipName;
        public int m_state;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

