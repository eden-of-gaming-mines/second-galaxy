﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum FormationType
    {
        None,
        Default,
        Ellipsoid,
        WildGoose,
        SquareMatrix
    }
}

