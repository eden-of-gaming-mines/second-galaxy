﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Serializable]
    public enum GuildAllianceLeaveReason
    {
        NotApplied,
        AllianceDismiss,
        RemovedByLeader,
        DecideToLeave
    }
}

