﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IDelegateMissionListDataContainer
    {
        void AddDelegateMission(DelegateMissionInfo mission);
        void AddHistoryDelegateMission(DelegateMissionInfo info);
        List<DelegateMissionInfo> GetDelegateMissionList();
        List<DelegateMissionInfo> GetHistoryDelegateMissionList();
        void RemoveDelegateMission(ulong signalInsId);
        void UpdateDelegateMission(ulong signalInsId, DelegateMissionState state);
        void UpdateDelegateMission4Complete(ulong signalInsId, DelegateMissionState state, DateTime completeTime, List<ItemInfo> completeReward, List<ItemInfo> processReward);
        void UpdateDelegateMission4InvadeEnd(ulong signalInsId, DelegateMissionInvadeInfo invadeInfo);
        void UpdateDelegateMission4InvadeStart(ulong signalInsId, PlayerSimpleInfo invadingPlayeInfo, DateTime invadingStartTime);
    }
}

