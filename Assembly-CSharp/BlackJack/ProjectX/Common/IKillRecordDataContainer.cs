﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IKillRecordDataContainer
    {
        void AddKillRecordInfo(KillRecordInfo killRecordInfo);
        List<KillRecordInfo> GetKillRecordInfoList();
        void UpdateCompensationStatus(ulong instanceId, CompensationStatus status, List<ItemCompensationStatus> itemStatus = null);
    }
}

