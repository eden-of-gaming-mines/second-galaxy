﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperEquipGroupBase : LBSuperWeaponEquipGroupBase
    {
        protected ConfigDataShipSuperEquipInfo m_confInfo;
        protected ConfigDataShipSuperEquipRunningInfo m_superEquipRunningInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetConfigRuningInfo;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_IsSuperWeapon;
        private static DelegateBridge __Hotfix_IsSuperEquip;
        private static DelegateBridge __Hotfix_GetGroupByEquipType;
        private static DelegateBridge __Hotfix_GetFunctionType;
        private static DelegateBridge __Hotfix_GetAmmoConfId;
        private static DelegateBridge __Hotfix_GetAmmoType;
        private static DelegateBridge __Hotfix_GetAmmoCount;
        private static DelegateBridge __Hotfix_GetAmmoClipSize;
        private static DelegateBridge __Hotfix_CalcWaveCD;
        private static DelegateBridge __Hotfix_CalcGroupCD;
        private static DelegateBridge __Hotfix_GetConfMwEnergyNeed;
        private static DelegateBridge __Hotfix_GetConfMwEnergyGrow;
        private static DelegateBridge __Hotfix_CalcChargeTime;
        private static DelegateBridge __Hotfix_GetChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_GetWeaponTransverseVelocity;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4WeaponEquip;
        private static DelegateBridge __Hotfix_GetPropertyFromConfigData4Ammo;
        private static DelegateBridge __Hotfix_CalcBufInstanceParam;
        private static DelegateBridge __Hotfix_AttachBufByEquip2Target;
        private static DelegateBridge __Hotfix_HasAvailableTarget;
        private static DelegateBridge __Hotfix_InitSuperEquipRunningConfInfo;
        private static DelegateBridge __Hotfix_GetBufListInfo;
        private static DelegateBridge __Hotfix_GetSelfBufListInfo;
        private static DelegateBridge __Hotfix_GetParamsInfo;

        [MethodImpl(0x8000)]
        protected LBSuperEquipGroupBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected ulong AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcBufInstanceParam(int bufId, out float bufInstanceParam, out PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override uint CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override float CalcFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override uint CalcGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override uint CalcWaveCD()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoClipSize()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoConfId()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoCount()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override StoreItemType GetAmmoType()
        {
        }

        [MethodImpl(0x8000)]
        protected List<int> GetBufListInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetChargeAndChannelBreakFactor()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipRunningInfo GetConfigRuningInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipSuperEquipInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override float GetConfMwEnergyGrow()
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override float GetConfMwEnergyNeed()
        {
        }

        [MethodImpl(0x8000)]
        public SuperEquipFunctionType GetFunctionType()
        {
        }

        [MethodImpl(0x8000)]
        public EquipType GetGroupByEquipType()
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected List<ParamInfo> GetParamsInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected sealed override bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected List<int> GetSelfBufListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public float GetWeaponTransverseVelocity()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasAvailableTarget(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitSuperEquipRunningConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsSuperEquip()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool IsSuperWeapon()
        {
        }
    }
}

