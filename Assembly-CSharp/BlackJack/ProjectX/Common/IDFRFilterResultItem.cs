﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IDFRFilterResultItem
    {
        ulong GetId();
    }
}

