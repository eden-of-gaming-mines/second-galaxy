﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;
    using System.Runtime.InteropServices;

    public interface ILogicBlockShipCompSpaceObject : ILogicBlockObjCompSpaceObject
    {
        bool CheckDistanceToLocation(Vector3D location, float maxDistance);
        bool CheckDistanceToTarget(ILBSpaceTarget target, float maxDistance);
        void DoArroundTargetInFightRange(ILBSpaceTarget target);
        float GetDistanceToTarget(ILBSpaceTarget target);
        bool GetKeepingInvisibleForWeaponEquipLaunch();
        uint GetMovementRelativeObjId();
        string GetOwnerName();
        string GetShipConfigName();
        string GetShipName();
        bool IsHoldMoveState();
        bool IsInBlinking();
        bool IsInJumping();
        bool IsInJumpPrepare();
        bool IsInvisible();
        bool IsJumpDisturbedBySpecialBuf();
        bool IsJumpDisturbTriggered();
        bool IsPreparingForEnterStation();
        bool IsPreparingForUsingStarGate();
        void JumpToLocation(Vector3D location);
        void JumpToRandomPlanet();
        void OnBlinkCancel();
        void OnBlinkEnd(float distance = 0f);
        void OnBlinkStart();
        void OnClearInvisibleByTarget(ILBSpaceTarget target);
        void OnInvisibleEnd();
        void OnInvisibleStart();
        void PushCmdMoveToLocation(Vector3D location);
        void PushCmdMoveToTarget(ILBSpaceTarget target);
        void RegEventOnBlinkEnd(Action<ILBInSpaceShip> action);
        void RegEventOnBlinkStart(Action action);
        void RegEventOnClearInvisibleByTarget(Action<ILBSpaceTarget> action);
        void RegEventOnEnterHighSpeedJumping(Action<ILBInSpaceShip> action);
        void RegEventOnEnterSpaceStation(Action action);
        void RegEventOnInvsibleEnd(Action<ILBInSpaceShip> action);
        void RegEventOnInvsibleStart(Action<ILBInSpaceShip> action);
        void RegEventOnJumpingEnd(Action<ILBInSpaceShip> action);
        void RegEventOnJumpingStart(Action<ILBInSpaceShip, Vector3D> action);
        void RegEventOnJumpPrepareEnd(Action<bool> action);
        void RegEventOnPrepare4Jump(Action OnJumpPrepireStart);
        void RegEventOnRemoveFromSolarSystem(Action<ILBInSpaceShip> action);
        void RegEventOnStartSolarSystemTeleport(Action<int, uint, TeleportEffect> action);
        void RegEventOnStartSolarSystemTeleport2Scene(Action<int, int, SceneType, TeleportEffect> action);
        void RegMovementRelativeTargetLost(Func<bool> func);
        void RemoveSelfFromSpace();
        void SetKeepingInvisibleForWeaponEquipLaunch(bool isKeeping);
        void StopShip();
        void UnregEventInvsibleEnd(Action<ILBInSpaceShip> action);
        void UnregEventInvsibleStart(Action<ILBInSpaceShip> action);
        void UnregEventOnBlinkEnd(Action<ILBInSpaceShip> action);
        void UnregEventOnBlinkStart(Action action);
        void UnregEventOnClearInvisibleByTarget(Action<ILBSpaceTarget> action);
        void UnregEventOnEnterHighSpeedJumping(Action<ILBInSpaceShip> action);
        void UnregEventOnEnterSpaceStation(Action action);
        void UnregEventOnJumpingEnd(Action<ILBInSpaceShip> action);
        void UnregEventOnJumpingStart(Action<ILBInSpaceShip, Vector3D> action);
        void UnregEventOnJumpPrepareEnd(Action<bool> action);
        void UnregEventOnPrepare4Jump(Action OnJumpPrepireStart);
        void UnregEventOnRemoveFromSolarSystem(Action<ILBInSpaceShip> action);
        void UnregEventOnStartSolarSystemTeleport(Action<int, uint, TeleportEffect> action);
        void UnregEventOnStartSolarSystemTeleport2Scene(Action<int, int, SceneType, TeleportEffect> action);
    }
}

