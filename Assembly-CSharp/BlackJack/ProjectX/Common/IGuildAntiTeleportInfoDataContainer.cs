﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildAntiTeleportInfoDataContainer
    {
        void AddGuildAntiTeleportEffect(GuildAntiTeleportEffectInfo info);
        List<GuildAntiTeleportEffectInfo> GetAllGuildAntiTeleportEffectList();
        int GetAntiTeleportEffectInfoVersion();
        GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectById(ulong instanceId);
        GuildAntiTeleportEffectInfo GetGuildAntiTeleportEffectBySolarSystemId(int destSolarSystemId);
        void RemoveGuildAntiTeleportEffect(ulong instanceId);
        bool UpdateGuildAntiTeleportTimeInfo(ulong instanceId, DateTime effectStartTime, DateTime effectEndTime, DateTime effectCDEndTime);
    }
}

