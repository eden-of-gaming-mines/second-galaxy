﻿namespace BlackJack.ProjectX.Common
{
    using System.Collections.Generic;

    public interface IAllianceMembersBase
    {
        List<AllianceMemberInfo> GetAllianceMemberList();
    }
}

