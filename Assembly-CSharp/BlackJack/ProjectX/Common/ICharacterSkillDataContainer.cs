﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface ICharacterSkillDataContainer
    {
        int GetFreeSkillPoint();
        List<IdLevelInfo> GetPassiveSkillList();
        void RemoveAllSkill();
        void RemovePassiveSkill(int skillId);
        void UpdateFreeSkillPoint(int value);
        void UpdatePassiveSkill(int skillId, int level);
    }
}

