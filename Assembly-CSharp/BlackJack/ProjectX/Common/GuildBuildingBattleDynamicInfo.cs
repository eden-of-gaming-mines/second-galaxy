﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildBuildingBattleDynamicInfo
    {
        public float m_shieldMax;
        public float m_armorMax;
        public float m_shieldCurr;
        public float m_armorCurr;
        public float m_attackerBuildingShieldMax;
        public float m_attackerBuildingArmorMax;
        public float m_attackerBuildingShieldCurr;
        public float m_attackerBuildingArmorCurr;
        public bool m_isEnterHalfReinforcement;
        public bool m_isOffline;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public GuildBuildingBattleDynamicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public GuildBuildingBattleDynamicInfo(GuildBuildingBattleDynamicInfo info)
        {
        }
    }
}

