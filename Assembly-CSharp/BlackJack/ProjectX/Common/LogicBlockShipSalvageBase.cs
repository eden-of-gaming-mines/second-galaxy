﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LogicBlockShipSalvageBase
    {
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected LogicBlockShipHangarsBase m_lbShipHangars;
        protected ILBPlayerContext m_playerCtx;
        protected IShipSalvageDataContainer m_dc;
        public int m_dailyShipSalvageCountLimit;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_CheckSalvageHangarShip;
        private static DelegateBridge __Hotfix_SalvageShipImpl;
        private static DelegateBridge __Hotfix_GetLeftDailyShipSalvageCount;

        [MethodImpl(0x8000)]
        protected LogicBlockShipSalvageBase()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSalvageHangarShip(int hangarIndex, bool isSalvageShip, out int errorCode, out bool useSalvageDailyCount, out int useSalvageItemId, out int useRealMoneyCount)
        {
        }

        [MethodImpl(0x8000)]
        public int GetLeftDailyShipSalvageCount()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void SalvageShipImpl(int hangarIndex, bool isSalvageShip)
        {
        }
    }
}

