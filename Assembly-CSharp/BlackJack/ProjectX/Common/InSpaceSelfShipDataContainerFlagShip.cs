﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class InSpaceSelfShipDataContainerFlagShip : InSpaceSelfShipDataContainerDefault, IInSpaceFlagShipDataContainer, IInSpacePlayerShipDataContainer, IFlagShipDataContainer, IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_UpdateCurrShipFuel;
        private static DelegateBridge __Hotfix_GetCurrShipFuel;
        private static DelegateBridge __Hotfix_GetConfRelativeData;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_SetSolarSystemId;
        private static DelegateBridge __Hotfix_GetHighSoltGroupItemList;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemList;
        private static DelegateBridge __Hotfix_GetHighSoltAmmoList;
        private static DelegateBridge __Hotfix_UpdateSlotGroupAmmoInfo;
        private static DelegateBridge __Hotfix_UpdateCurrEnergy;
        private static DelegateBridge __Hotfix_GetCurrEnergy;
        private static DelegateBridge __Hotfix_UpdataShipStoreItem;
        private static DelegateBridge __Hotfix_AddShipStoreItem;
        private static DelegateBridge __Hotfix_RemoveShipStoreItem;

        [MethodImpl(0x8000)]
        public override void AddShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFlagShipRelativeData GetConfRelativeData()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetCurrEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public double GetCurrShipFuel()
        {
        }

        [MethodImpl(0x8000)]
        public override AmmoInfo[] GetHighSoltAmmoList()
        {
        }

        [MethodImpl(0x8000)]
        public override ShipSlotGroupInfo[] GetHighSoltGroupItemList()
        {
        }

        [MethodImpl(0x8000)]
        public override List<ShipSlotGroupInfo> GetMiddleSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(IStaticFlagShipDataContainer shipDC, SpaceEnterNtf enterMsg)
        {
        }

        [MethodImpl(0x8000)]
        public override void RemoveShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public override void UpdataShipStoreItem(ShipStoreItemInfo shipItemInfo, long count, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        public override void UpdateCurrEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShipFuel(double fuel)
        {
        }

        [MethodImpl(0x8000)]
        public override void UpdateSlotGroupAmmoInfo(int groupIndex, StoreItemType ammoType, int ammoConfigId, int ammoCount)
        {
        }
    }
}

