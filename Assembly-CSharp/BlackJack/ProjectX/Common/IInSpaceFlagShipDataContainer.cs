﻿namespace BlackJack.ProjectX.Common
{
    public interface IInSpaceFlagShipDataContainer : IInSpacePlayerShipDataContainer, IFlagShipDataContainer, IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
    }
}

