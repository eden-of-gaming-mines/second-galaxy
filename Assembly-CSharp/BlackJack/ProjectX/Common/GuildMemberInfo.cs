﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildMemberInfo
    {
        public GuildMemberBasicInfo m_basicInfo;
        public GuildMemberDynamicInfo m_dynamicInfo;
        public GuildMemberRuntimeInfo m_runtimeInfo;
        public DateTime m_lastLeaveGuildTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

