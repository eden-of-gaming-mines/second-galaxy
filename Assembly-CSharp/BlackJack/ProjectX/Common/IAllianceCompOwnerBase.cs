﻿namespace BlackJack.ProjectX.Common
{
    public interface IAllianceCompOwnerBase
    {
        IAllianceCompBasicBase GetBasicComp();
        IAllianceCompChatBase GetChatComp();
        IAllianceCompInviteBase GetInviteComp();
        IAllianceCompMailBase GetMailComp();
        IAllianceCompMembersBase GetMembersComp();
    }
}

