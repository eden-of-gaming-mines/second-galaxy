﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildDataContainerForPlayerCtx
    {
        uint GetAllianceId();
        string GetAllianceName();
        GuildBasicInfo GetGuildBasicInfo();
        ulong GetGuildFleetId();
        GuildFleetPersonalSetting GetGuildFleetSetting();
        uint GetGuildId();
        DateTime GetGuildSentryProbeAvailableTime();
        bool GetSelfFlagShipInfo(out int hangarSolarSystemId);
        GuildMemberBasicInfo GetSelfMemberBasicInfo();
        GuildMemberDynamicInfo GetSelfMemberDynamicInfo();
        void UpdateCodeName(string codeName);
        void UpdateGuildBasicInfo(GuildBasicInfo info);
        void UpdateGuildBasicInfo(int baseSolarSystem, int baseStation);
        void UpdateGuildFleetSetting(GuildFleetPersonalSetting settingFlag);
        void UpdateGuildLogoInfo(GuildLogoInfo logo);
        void UpdateGuildSentryProbeAvailableTime(DateTime availableTime);
        void UpdateName(string name);
        void UpdateSelfFlagShipInfo(int hangarSolarSystemId);
        void UpdateSelfFleetId(ulong fleetId);
        void UpdateSelfMemberBasicInfo(GuildMemberBasicInfo info);
        void UpdateSelfMemberDynamicInfo(GuildMemberDynamicInfo info);
        void UpdateSelfMemberJob(List<GuildJobType> jobs);
    }
}

