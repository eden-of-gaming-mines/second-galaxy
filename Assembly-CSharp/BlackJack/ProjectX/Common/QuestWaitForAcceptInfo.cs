﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class QuestWaitForAcceptInfo
    {
        public int m_questId;
        public int m_factionId;
        public int m_level;
        public bool m_fromCancelQuest;
        public int m_sceneSolarSystemId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

