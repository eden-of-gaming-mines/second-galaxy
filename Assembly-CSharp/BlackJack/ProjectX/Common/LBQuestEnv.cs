﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBQuestEnv
    {
        protected ConfigDataQuestEnvirmentInstanceInfo m_confInfo;
        protected QuestEnvirmentInfo m_readOnlyEnvInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetEnvInstanceId;
        private static DelegateBridge __Hotfix_GetEnvirmentInfo;
        private static DelegateBridge __Hotfix_GetTypeId;
        private static DelegateBridge __Hotfix_GetSolarSystemByIndex;
        private static DelegateBridge __Hotfix_GetNpcByIndex;
        private static DelegateBridge __Hotfix_GetCustomStringByIndex;

        [MethodImpl(0x8000)]
        public LBQuestEnv(QuestEnvirmentInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public string GetCustomStringByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetEnvInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public QuestEnvirmentInfo GetEnvirmentInfo()
        {
        }

        [MethodImpl(0x8000)]
        public NpcDNId GetNpcByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public int GetTypeId()
        {
        }
    }
}

