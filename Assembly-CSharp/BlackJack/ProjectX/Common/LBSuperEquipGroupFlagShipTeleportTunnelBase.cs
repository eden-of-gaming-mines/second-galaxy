﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperEquipGroupFlagShipTeleportTunnelBase : LBSuperEquipGroupBase
    {
        protected bool m_isTeleportTunnelRunning;
        protected float m_teleportRange;
        private const string m_teleportRangeName = "TeleportRange";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsWorking;
        private static DelegateBridge __Hotfix_IsTargetValid;
        private static DelegateBridge __Hotfix_HasAvailableTarget;
        private static DelegateBridge __Hotfix_InitConfigParams;
        private static DelegateBridge __Hotfix_CalcTeleportRange;

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupFlagShipTeleportTunnelBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcTeleportRange()
        {
        }

        [MethodImpl(0x8000)]
        public override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasAvailableTarget(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitConfigParams()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsWorking()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

