﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IMailDataContainer
    {
        void ClearStoredMailAttachment(int index);
        StoredMailInfo CreateEmptyStoredMailInfo();
        StoredMailInfo GetStoredMailInfoByIndex(int index);
        List<StoredMailInfo> GetStoredMailInfoList();
        void UpdateStoredMailInfo(int index, bool isDeleted);
        void UpdateStoredMailInfo(int index, bool isDeleted, MailInfo mailInfo);
        void UpdateStoredMailInfoForReaded(int index, bool isReaded, DateTime currTime);
    }
}

