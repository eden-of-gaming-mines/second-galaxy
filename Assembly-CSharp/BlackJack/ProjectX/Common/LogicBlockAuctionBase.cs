﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockAuctionBase
    {
        protected ILBPlayerContext m_lbPlayerCtx;
        protected IAuctionDataContainer m_dc;
        protected LogicBlockItemStoreBase m_lbItemStore;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPlayerAuctionItemList;
        private static DelegateBridge __Hotfix_GetPlayerAuctionItemByInsId;
        private static DelegateBridge __Hotfix_AddPlayerAuctionItem;
        private static DelegateBridge __Hotfix_UpdatePlayerAuctionItem;
        private static DelegateBridge __Hotfix_RemovePlayerAuctionItem;
        private static DelegateBridge __Hotfix_ClearPlayerAuctionItem;
        private static DelegateBridge __Hotfix_CheckAuctionItemOnShelve;
        private static DelegateBridge __Hotfix_CheckAuctionItemCallBack;
        private static DelegateBridge __Hotfix_CalcAuctionFee;

        [MethodImpl(0x8000)]
        public void AddPlayerAuctionItem(AuctionItemDetailInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static long CalcAuctionFee(long price, long count, int period)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckAuctionItemCallBack(ulong auctionItemInsId, out int errCode, out AuctionItemDetailInfo auctionItemDetailInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckAuctionItemOnShelve(int auctionItemId, ulong auctionItemInsId, int itemCount, int period, long price, out int errCode, out int alreadyOnOrderItemCount, out long auctionFee)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearPlayerAuctionItem()
        {
        }

        [MethodImpl(0x8000)]
        public AuctionItemDetailInfo GetPlayerAuctionItemByInsId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public List<AuctionItemDetailInfo> GetPlayerAuctionItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RemovePlayerAuctionItem(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerAuctionItem(ulong insId, AuctionItemState state, int currCount)
        {
        }
    }
}

