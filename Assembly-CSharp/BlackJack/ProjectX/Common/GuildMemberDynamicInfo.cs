﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class GuildMemberDynamicInfo
    {
        public ushort m_seq;
        public long m_contribution;
        public long m_contributionCurrWeek;
        public long m_donateTradeMoneyToday;
        public int m_compensationDonateToday;
        public long m_informationPointCurrWeek;
        public int m_currDrivingFlagShipHangarSolarSystemId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

