﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompInSpaceCaptain : IPropertiesProvider
    {
        private ILBInSpaceShip m_ownerShip;
        private ILBInSpaceShipCaptain m_captain;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetCaptain;
        private static DelegateBridge __Hotfix_IsPlayerCaptain;
        private static DelegateBridge __Hotfix_GetPlayerGameUserId;
        private static DelegateBridge __Hotfix_HasNoOwnerPlayer;
        private static DelegateBridge __Hotfix_GetAllianceId;
        private static DelegateBridge __Hotfix_GetHiredCaptainInstanceId;
        private static DelegateBridge __Hotfix_GetBasicProperties;
        private static DelegateBridge __Hotfix_GetDrivingLicenseId;
        private static DelegateBridge __Hotfix_GetDrivingLicenseLevel;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_UnregEventOnPropertiesGroupDirty;

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetAllianceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetBasicProperties(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public ILBInSpaceShipCaptain GetCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public int GetDrivingLicenseId()
        {
        }

        [MethodImpl(0x8000)]
        public int? GetDrivingLicenseLevel()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetHiredCaptainInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public string GetPlayerGameUserId()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasNoOwnerPlayer()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip, ILBInSpaceShipCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPlayerCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }
    }
}

