﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockShipCustomTemplateBase
    {
        protected ILBPlayerContext m_playerCtx;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected LogicBlockShipHangarsBase m_lbHangars;
        protected IShipCustomTemplateDataContainer m_dc;
        protected static Dictionary<int, List<ShipCustomTemplateInfo>> s_preCustomTemplateList = new Dictionary<int, List<ShipCustomTemplateInfo>>();
        private const int ShipCustomTeamplateCountMax = 100;
        private const int ShipCustomTeamplateNameLenMax = 50;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_HasTemplate4Ship;
        private static DelegateBridge __Hotfix_GetTemplateList;
        private static DelegateBridge __Hotfix_GetPreCustomTemplateDic;
        private static DelegateBridge __Hotfix_GetTemplateByIndex;
        private static DelegateBridge __Hotfix_CheckShipCustomTemplateAdd;
        private static DelegateBridge __Hotfix_CheckShipCustomTemplateRemove;
        private static DelegateBridge __Hotfix_CheckShipCustomTemplateUpdate;
        private static DelegateBridge __Hotfix_CheckShipCustomTemplateInfo;
        private static DelegateBridge __Hotfix_CheckShipCustomTemplateApply;
        private static DelegateBridge __Hotfix_GetItemList4Template;
        private static DelegateBridge __Hotfix_IsShipCustomTemplateEquipSlotAvaiable;
        private static DelegateBridge __Hotfix_GetPreCustomTemplateInConf;
        private static DelegateBridge __Hotfix_InitPreCustomTemplateInfoDic;
        private static DelegateBridge __Hotfix_IsShipSameWithCustomTemplate;
        private static DelegateBridge __Hotfix_LogAllItemStoreItems;
        private static DelegateBridge __Hotfix_get_m_customTemplateList;

        [MethodImpl(0x8000)]
        public bool CheckShipCustomTemplateAdd(ShipCustomTemplateInfo info, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckShipCustomTemplateApply(bool isPredefineTemplate, int templateIndex, ulong shipInstanceId, out ShipCustomTemplateInfo template, out List<ItemInfo> lackBindMoneyBuyableItemList, out double needBindMoney, out List<ItemInfo> lackAuctionItemList, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckShipCustomTemplateInfo(ShipCustomTemplateInfo info, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckShipCustomTemplateRemove(int index, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckShipCustomTemplateUpdate(int index, string name, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetItemList4Template(ShipCustomTemplateInfo template)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<int, List<ShipCustomTemplateInfo>> GetPreCustomTemplateDic()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipCustomTemplateInfo> GetPreCustomTemplateInConf(int shipId)
        {
        }

        [MethodImpl(0x8000)]
        protected ShipCustomTemplateInfo GetTemplateByIndex(int templateIndex)
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipCustomTemplateInfo> GetTemplateList()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasTemplate4Ship(int shipId)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        private static void InitPreCustomTemplateInfoDic()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShipCustomTemplateEquipSlotAvaiable(int shipConfId, ShipEquipSlotType slotType, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShipSameWithCustomTemplate(ShipCustomTemplateInfo info, ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void LogAllItemStoreItems()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        protected List<ShipCustomTemplateInfo> m_customTemplateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

