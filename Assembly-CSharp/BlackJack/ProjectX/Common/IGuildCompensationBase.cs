﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildCompensationBase
    {
        void AddCompensation(LossCompensation compensation);
        void CloseCompensation(ulong instanceId, CompensationCloseReason reason);
        List<LossCompensation> GetCompensationList();
        int GetCompensationListVersion();
        void SetAutoCompensation(bool isAuto);
        void UpdateCompensation(LossCompensation compensation);
    }
}

