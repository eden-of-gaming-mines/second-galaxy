﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBFormationControllerEllipsoid : LBFormationControllerDefault
    {
        protected LinkedList<int> m_freeFormationPosIndexList;
        protected double m_formationSphereScaleY;
        protected double m_distanceFixOffset;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_InitialzeFormationPosList;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByJoinMember;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByLeaveMember;
        private static DelegateBridge __Hotfix_CalcPosByAngleInfo;
        private static DelegateBridge __Hotfix_AddUnuseIndex2List;

        [MethodImpl(0x8000)]
        protected void AddUnuseIndex2List(LinkedList<int> unusedIndexList, int freeIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override Vector3D CalcPosByAngleInfo(double azimuth, double altitude)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initialize(ILBInSpaceShip leader, FormationCreatingInfo formationInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitialzeFormationPosList(bool isFormationFlipped)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByJoinMember(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFormationPosChangedByLeaveMember(IFormationMember member)
        {
        }
    }
}

