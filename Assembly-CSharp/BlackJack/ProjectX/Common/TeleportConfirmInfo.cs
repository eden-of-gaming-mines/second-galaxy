﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct TeleportConfirmInfo
    {
        public TeleportConfirmInfoType m_type;
        public WormholeGateInfo? m_wormholeGateInfo;
    }
}

