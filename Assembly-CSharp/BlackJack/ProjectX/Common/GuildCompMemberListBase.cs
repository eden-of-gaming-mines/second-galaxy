﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompMemberListBase : GuildCompBase, IGuildCompMemberListBase, IGuildMemberListBase
    {
        protected List<GuildMemberBase> m_memberList;
        protected Dictionary<string, GuildMemberBase> m_memberDict;
        protected Dictionary<ushort, GuildMemberBase> m_memberSeqDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initlialize;
        private static DelegateBridge __Hotfix_GetMemberListInfo;
        private static DelegateBridge __Hotfix_GetMemberList;
        private static DelegateBridge __Hotfix_GetMember;
        private static DelegateBridge __Hotfix_GetMemberCount;
        private static DelegateBridge __Hotfix_AddMember;
        private static DelegateBridge __Hotfix_RemoveMember;
        private static DelegateBridge __Hotfix_AddNewMemberFromDC;
        private static DelegateBridge __Hotfix_GetMemberBySeq;
        private static DelegateBridge __Hotfix_CreateMemberCtx;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompMemberListBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AddMember(GuildMemberBase member)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBase AddNewMemberFromDC(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual GuildMemberBase CreateMemberCtx(GuildMemberInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBase GetMember(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberBase GetMemberBySeq(ushort seq)
        {
        }

        [MethodImpl(0x8000)]
        public int GetMemberCount()
        {
        }

        [MethodImpl(0x8000)]
        public List<GuildMemberBase> GetMemberList()
        {
        }

        [MethodImpl(0x8000)]
        public GuildMemberListInfo GetMemberListInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override bool Initlialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveMember(string playerGameUserId)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

