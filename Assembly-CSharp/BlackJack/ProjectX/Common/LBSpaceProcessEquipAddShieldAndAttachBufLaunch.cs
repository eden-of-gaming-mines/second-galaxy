﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBSpaceProcessEquipAddShieldAndAttachBufLaunch : LBSpaceProcessEquipAttachBufLaunch
    {
        protected float m_shieldRecoveryAmount;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_SetShieldRecoveryAmount;
        private static DelegateBridge __Hotfix_GetShieldRecoveryAmount;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAddShieldAndAttachBufLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipAddShieldAndAttachBufLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget = null, ILBSpaceTarget destTarget = null, ShipEquipSlotType slotType = 0, int groupIndex = 0, EquipType equipType = 0x30, ILBSpaceProcessEquipLaunchSource processSource = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShieldRecoveryAmount()
        {
        }

        [MethodImpl(0x8000)]
        public void SetShieldRecoveryAmount(float shieldRecoveryAmount)
        {
        }
    }
}

