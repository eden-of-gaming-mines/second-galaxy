﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class LogicBlockCharacterPVPBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float> EventOnCriminalLevelUpdate;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnContinousCriminalSet;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnContinousCriminalUnSet;
        private ILBPlayerContext m_lbPlayerCtx;
        private ICharacterPVPContainer m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetReadOnlyPlayerPVPInfo;
        private static DelegateBridge __Hotfix_GetCriminalLevel;
        private static DelegateBridge __Hotfix_DecCriminalLevel;
        private static DelegateBridge __Hotfix_UpdateCriminalLevel;
        private static DelegateBridge __Hotfix_IsJustCrime;
        private static DelegateBridge __Hotfix_GetJustCrimeTimeOut;
        private static DelegateBridge __Hotfix_IsCriminalHunter;
        private static DelegateBridge __Hotfix_GetCriminalHunterTimeOut;
        private static DelegateBridge __Hotfix_UpdateJustCrime;
        private static DelegateBridge __Hotfix_UpdateCriminalHunter;
        private static DelegateBridge __Hotfix_OnQuestComplete;
        private static DelegateBridge __Hotfix_add_EventOnCriminalLevelUpdate;
        private static DelegateBridge __Hotfix_remove_EventOnCriminalLevelUpdate;
        private static DelegateBridge __Hotfix_add_EventOnContinousCriminalSet;
        private static DelegateBridge __Hotfix_remove_EventOnContinousCriminalSet;
        private static DelegateBridge __Hotfix_add_EventOnContinousCriminalUnSet;
        private static DelegateBridge __Hotfix_remove_EventOnContinousCriminalUnSet;

        public event Action EventOnContinousCriminalSet
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnContinousCriminalUnSet
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float> EventOnCriminalLevelUpdate
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void DecCriminalLevel(float value)
        {
        }

        [MethodImpl(0x8000)]
        public DateTime? GetCriminalHunterTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCriminalLevel()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime? GetJustCrimeTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerPVPInfo GetReadOnlyPlayerPVPInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCriminalHunter()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsJustCrime()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestComplete(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCriminalHunter(bool isCriminalHunter, long endTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCriminalLevel(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateJustCrime(bool isJustCrime, long endTime)
        {
        }
    }
}

