﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public abstract class LBStaticShipBase : ILBStaticShip, ILBSpaceShipBasic, ILBShipItemStoreContainer
    {
        protected LogicBlockShipCompStaticWeaponEquipBase m_lbCompWeaponEquip;
        protected LogicBlockShipCompStaticPropertiesCalc m_lbCompPropertiesCalc;
        protected LogicBlockShipCompStaticBasicCtx m_lbCompBasicCtx;
        protected LogicBlockShipCompStaticCaptain m_lbCaptain;
        protected IShipDataContainer m_shipDC;
        protected ConfigDataSpaceShipInfo m_playerShipConf;
        protected int m_drivingLicenseId;
        protected int m_shipHangarIndex;
        private ILBShipCaptain m_caption;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetHangarIndex;
        private static DelegateBridge __Hotfix_GetShipType;
        private static DelegateBridge __Hotfix_CheckShipFlag;
        private static DelegateBridge __Hotfix_GetShipDataContainer;
        private static DelegateBridge __Hotfix_GetShipConfInfo;
        private static DelegateBridge __Hotfix_GetShipCaptiain;
        private static DelegateBridge __Hotfix_GetLBStaticBasicCtx;
        private static DelegateBridge __Hotfix_GetLBCaptain;
        private static DelegateBridge __Hotfix_GetLBWeaponEquip;
        private static DelegateBridge __Hotfix_GetLBPropertiesCalc;
        private static DelegateBridge __Hotfix_GetShipItemStoreDataContainer;
        private static DelegateBridge __Hotfix_GetLBShipItemStore;

        [MethodImpl(0x8000)]
        public LBStaticShipBase(IShipDataContainer shipDC, int hangarIndex, ILBShipCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckShipFlag(ShipFlag flag)
        {
        }

        public abstract ILBBuffFactory GetBufFactory();
        public abstract float GetDefaultAroundRadius(ILBSpaceTarget target);
        [MethodImpl(0x8000)]
        public int GetHangarIndex()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticCaptain GetLBCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockCompPropertiesCalc GetLBPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        public virtual LogicBlockShipCompItemStoreBase GetLBShipItemStore()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticBasicCtx GetLBStaticBasicCtx()
        {
        }

        [MethodImpl(0x8000)]
        public LogicBlockShipCompStaticWeaponEquipBase GetLBWeaponEquip()
        {
        }

        [MethodImpl(0x8000)]
        public ILBShipCaptain GetShipCaptiain()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSpaceShipInfo GetShipConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public IShipDataContainer GetShipDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public IShipItemStoreDataContainer GetShipItemStoreDataContainer()
        {
        }

        [MethodImpl(0x8000)]
        public ShipType GetShipType()
        {
        }

        public abstract DateTime GetUnpackTime();
    }
}

