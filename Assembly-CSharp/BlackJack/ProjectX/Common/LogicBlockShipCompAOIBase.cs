﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LogicBlockShipCompAOIBase : ILogicBlockShipCompAOI
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventOnTargetEnterView;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceTarget> EventOnTargetLeaveView;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnOtherTargetBlink;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnOtherTargetJump;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnOtherTargetInvisibleStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceTarget> EventOnOtherTargetInvisibleEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBSpaceDropBox> EventOnDropBoxEnterView;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBSpaceDropBox> EventOnDropBoxLeaveView;
        protected ILBInSpaceShip m_ownerShip;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnOtherTargetBlink;
        private static DelegateBridge __Hotfix_OnOtherTargetJump;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisibleStart;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisibleEnd;
        private static DelegateBridge __Hotfix_add_EventOnTargetEnterView;
        private static DelegateBridge __Hotfix_remove_EventOnTargetEnterView;
        private static DelegateBridge __Hotfix_add_EventOnTargetLeaveView;
        private static DelegateBridge __Hotfix_remove_EventOnTargetLeaveView;
        private static DelegateBridge __Hotfix_add_EventOnOtherTargetBlink;
        private static DelegateBridge __Hotfix_remove_EventOnOtherTargetBlink;
        private static DelegateBridge __Hotfix_add_EventOnOtherTargetJump;
        private static DelegateBridge __Hotfix_remove_EventOnOtherTargetJump;
        private static DelegateBridge __Hotfix_add_EventOnOtherTargetInvisibleStart;
        private static DelegateBridge __Hotfix_remove_EventOnOtherTargetInvisibleStart;
        private static DelegateBridge __Hotfix_add_EventOnOtherTargetInvisibleEnd;
        private static DelegateBridge __Hotfix_remove_EventOnOtherTargetInvisibleEnd;
        private static DelegateBridge __Hotfix_add_EventOnDropBoxEnterView;
        private static DelegateBridge __Hotfix_remove_EventOnDropBoxEnterView;
        private static DelegateBridge __Hotfix_add_EventOnDropBoxLeaveView;
        private static DelegateBridge __Hotfix_remove_EventOnDropBoxLeaveView;
        private static DelegateBridge __Hotfix_FireEventOnTargetEnterView;
        private static DelegateBridge __Hotfix_FireEventOnTargetLeaveView;
        private static DelegateBridge __Hotfix_FireEventOnDropBoxEnterView;
        private static DelegateBridge __Hotfix_FireEventOnDropBoxLeaveView;

        public event Action<ILBSpaceDropBox> EventOnDropBoxEnterView
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceDropBox> EventOnDropBoxLeaveView
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnOtherTargetBlink
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnOtherTargetInvisibleEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnOtherTargetInvisibleStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnOtherTargetJump
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnTargetEnterView
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBSpaceTarget> EventOnTargetLeaveView
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected LogicBlockShipCompAOIBase()
        {
        }

        public abstract void EnableAccurateAOI(bool enable);
        [MethodImpl(0x8000)]
        protected bool FireEventOnDropBoxEnterView(ILBSpaceDropBox dropBox)
        {
        }

        [MethodImpl(0x8000)]
        protected bool FireEventOnDropBoxLeaveView(ILBSpaceDropBox dropBox)
        {
        }

        [MethodImpl(0x8000)]
        protected bool FireEventOnTargetEnterView(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected bool FireEventOnTargetLeaveView(ILBSpaceTarget target)
        {
        }

        public abstract HashSet<ILBSpaceDropBox> GetCurrInterestDropBoxList();
        public abstract HashSet<ILBSpaceTarget> GetCurrInterestTargetList();
        public abstract ILBInSpacePlayerShip GetPlayerShipInView(string gameUserId);
        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        public abstract bool IsObjectInView(uint objId);
        [MethodImpl(0x8000)]
        public void OnOtherTargetBlink(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetInvisibleEnd(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetInvisibleStart(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnOtherTargetJump(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }
    }
}

