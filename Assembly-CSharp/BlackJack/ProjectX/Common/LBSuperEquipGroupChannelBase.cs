﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperEquipGroupChannelBase : LBSuperEquipGroupBase, IEquipFunctionCompChannelOwnerBase, IEquipFunctionCompOwner
    {
        protected EquipFunctionCompChannelBase m_funcComp;
        protected int m_chargeAttachBufId;
        protected int m_channelAttachBufId;
        protected int m_targetOnHitAttachBufId;
        protected List<int> m_changingBufIdListForChannel;
        protected float m_loopCD;
        protected float m_loopCountLimit;
        protected float m_damageFactor;
        protected float m_recoveryEnergyValue;
        protected float m_recoveryShieldValue;
        private const string RecoveryEnergyKey = "RecoveryEnergy";
        private const string RecoveryShieldKey = "RecoveryShield";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Prepare4Remove;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_IsTargetValid;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetOwnerShip;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetPropertiesCalc;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSlotType;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSlotGroupIndex;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetEquipType;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetConfBufIdList;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSelfBufIdList;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcChargeTime;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.AttachBufByEquip2Target;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.GetEquipTransverseVelocity;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.GetChargeBufId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.GetChannelingSelfBufId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.GetChannelingTargetBufId;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcLoopEnergyCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcLoopCD;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcLoopCount;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.IsChannelBreakable;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.GetBreakResistFactor;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcLoopDamageTotal;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcLoopDamageComposeHeat;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcLoopDamageComposeKinetic;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcLoopFireCtrlAccuracy;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompChannelOwnerBase.CalcFireRange;

        [MethodImpl(0x8000)]
        protected LBSuperEquipGroupChannelBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        float IEquipFunctionCompChannelOwnerBase.CalcFireRange()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompChannelOwnerBase.CalcLoopCD()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompChannelOwnerBase.CalcLoopCount()
        {
        }

        [MethodImpl(0x8000)]
        float IEquipFunctionCompChannelOwnerBase.CalcLoopDamageComposeHeat()
        {
        }

        [MethodImpl(0x8000)]
        float IEquipFunctionCompChannelOwnerBase.CalcLoopDamageComposeKinetic()
        {
        }

        [MethodImpl(0x8000)]
        float IEquipFunctionCompChannelOwnerBase.CalcLoopDamageTotal()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompChannelOwnerBase.CalcLoopEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        float IEquipFunctionCompChannelOwnerBase.CalcLoopFireCtrlAccuracy()
        {
        }

        [MethodImpl(0x8000)]
        float IEquipFunctionCompChannelOwnerBase.GetBreakResistFactor()
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompChannelOwnerBase.GetChannelingSelfBufId()
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompChannelOwnerBase.GetChannelingTargetBufId(int bufSeq)
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompChannelOwnerBase.GetChargeBufId()
        {
        }

        [MethodImpl(0x8000)]
        float IEquipFunctionCompChannelOwnerBase.GetEquipTransverseVelocity()
        {
        }

        [MethodImpl(0x8000)]
        bool IEquipFunctionCompChannelOwnerBase.IsChannelBreakable()
        {
        }

        [MethodImpl(0x8000)]
        ulong IEquipFunctionCompOwner.AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        List<int> IEquipFunctionCompOwner.GetConfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        EquipType IEquipFunctionCompOwner.GetEquipType()
        {
        }

        [MethodImpl(0x8000)]
        ILBInSpaceShip IEquipFunctionCompOwner.GetOwnerShip()
        {
        }

        [MethodImpl(0x8000)]
        PropertiesCalculaterBase IEquipFunctionCompOwner.GetPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        IEnumerable<int> IEquipFunctionCompOwner.GetSelfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompOwner.GetSlotGroupIndex()
        {
        }

        [MethodImpl(0x8000)]
        ShipEquipSlotType IEquipFunctionCompOwner.GetSlotType()
        {
        }

        [MethodImpl(0x8000)]
        public override bool CanMakeDamage()
        {
        }

        protected abstract EquipFunctionCompChannelBase CreateFuncComp();
        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public override void Prepare4Remove()
        {
        }
    }
}

