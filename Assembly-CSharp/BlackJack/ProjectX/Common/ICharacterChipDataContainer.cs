﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface ICharacterChipDataContainer
    {
        CharChipSchemeInfo GetChipSchemeByIndex(int index);
        List<CharChipSchemeInfo> GetChipSchemeList();
        int GetCurrSchemeIndex();
        void SchemeUnlock();
        void SwitchScheme(int index);
        void UpdateChipSlotInfo(CharChipSlotInfo info);
    }
}

