﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SphereCollisionInfo")]
    public class SphereCollisionInfo : IExtensible
    {
        private PVector3D _center;
        private float _radius;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Center;
        private static DelegateBridge __Hotfix_set_Center;
        private static DelegateBridge __Hotfix_get_Radius;
        private static DelegateBridge __Hotfix_set_Radius;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="center", DataFormat=DataFormat.Default)]
        public PVector3D Center
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="radius", DataFormat=DataFormat.FixedSize)]
        public float Radius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

