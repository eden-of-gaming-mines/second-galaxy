﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IInSpaceNpcShipDataContainer : INpcShipDataContainer, IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        uint GetMasterShipObjId();
    }
}

