﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBBufDotBase : LBTickableBuf
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetAbsoluteValue;
        private static DelegateBridge __Hotfix_GetPercentValue;

        [MethodImpl(0x8000)]
        public LBBufDotBase(ConfigDataBufInfo confInfo, uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public float GetAbsoluteValue()
        {
        }

        [MethodImpl(0x8000)]
        public float GetPercentValue()
        {
        }
    }
}

