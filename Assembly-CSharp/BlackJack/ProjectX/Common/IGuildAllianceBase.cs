﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildAllianceBase
    {
        List<GuildAllianceInviteInfo> GetAllianceInviteList();
        void OnReceiveAllianceInvite(GuildAllianceInviteInfo invite);
    }
}

