﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ICommonLog
    {
        void DebugLog(bool needInRelease, string format, params object[] args);
        void ErrorLog(bool needInRelease, string format, params object[] args);
        void ScriptDebugLog(string format, params object[] args);
        void WarnLog(bool needInRelease, string format, params object[] args);
    }
}

