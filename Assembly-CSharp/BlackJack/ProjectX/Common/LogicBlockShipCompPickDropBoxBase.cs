﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompPickDropBoxBase
    {
        protected ILBInSpacePlayerShip m_ownerShip;
        protected ILogicBlockShipCompAOI m_lbAOI;
        protected List<uint> m_dropBoxObjIdList;
        protected uint m_dropBoxListLastUpdateTime;
        protected uint m_currPickingDropBoxObjId;
        protected uint m_pickStartTime;
        protected uint m_pickEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetDropBoxList;
        private static DelegateBridge __Hotfix_GetCurrPickingDropBox;
        private static DelegateBridge __Hotfix_GetDropBoxListLastUpdateTime;
        private static DelegateBridge __Hotfix_OnDropBoxEnterView;
        private static DelegateBridge __Hotfix_OnDropBoxLeaveView;
        private static DelegateBridge __Hotfix_CalcPickTime;

        [MethodImpl(0x8000)]
        protected uint CalcPickTime()
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceDropBox GetCurrPickingDropBox()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetDropBoxList()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetDropBoxListLastUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpacePlayerShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDropBoxEnterView(ILBSpaceDropBox dropBox)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDropBoxLeaveView(ILBSpaceDropBox dropBox)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }
    }
}

