﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ProjectX.Common.LogicBlock.Alliance.DataContainer;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AllianceCompBasicBase : AllianceCompBase, IAllianceCompBasicBase, IAllianceBasicBase
    {
        protected IAllianceDCBasic m_dc;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetDC;
        private static DelegateBridge __Hotfix_GetAllianceBasicInfo;
        private static DelegateBridge __Hotfix_AllianceLeaderSet;

        [MethodImpl(0x8000)]
        public AllianceCompBasicBase(IAllianceCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AllianceLeaderSet(uint guildId, string guildCode, string guildName)
        {
        }

        [MethodImpl(0x8000)]
        public AllianceBasicInfo GetAllianceBasicInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDC(IAllianceDCBasic dc)
        {
        }
    }
}

