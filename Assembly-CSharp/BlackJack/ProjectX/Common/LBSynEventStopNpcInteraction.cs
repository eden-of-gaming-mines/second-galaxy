﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventStopNpcInteraction : LBSyncEvent
    {
        public uint m_objectId;
        public int m_reason;
        public float? m_shipShield;
        public float? m_shipArmor;
        public float? m_shipEnergy;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

