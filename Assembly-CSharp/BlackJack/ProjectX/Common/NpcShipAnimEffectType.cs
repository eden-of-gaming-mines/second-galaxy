﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum NpcShipAnimEffectType
    {
        None,
        FakeJumpIn,
        FakeJumpOut,
        FakeFadeIn4Invisible,
        Custom0,
        Custom1,
        Custom2,
        Custom3
    }
}

