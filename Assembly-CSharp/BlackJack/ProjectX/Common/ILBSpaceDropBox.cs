﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using System;
    using System.Collections.Generic;

    public interface ILBSpaceDropBox
    {
        List<ItemInfo> GetDropItemList4Player(string playerGameUserId);
        LogicBlockObjCompDropBoxBase GetLBDropBox();
        Vector3D GetLocation();
        uint GetObjId();
        ILBSpaceContext GetSpaceCtx();
        bool IsAvailable4Player(string playerGameUserId);
        void RemoveSelfOnTickEnd();
    }
}

