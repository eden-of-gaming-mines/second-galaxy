﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class AllianceMemberInfo
    {
        public uint m_allianceId;
        public uint m_guildId;
        public GuildSimplestInfo m_guildInfo;
        public int m_guildEvaluateScore;
        public DateTime m_joinTime;
        public DateTime m_lastLeaveAllianceTime;
        public GuildAllianceLeaveReason m_lastLeaveAllianceReason;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

