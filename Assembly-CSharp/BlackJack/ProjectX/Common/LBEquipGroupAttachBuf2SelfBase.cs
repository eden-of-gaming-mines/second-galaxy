﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBEquipGroupAttachBuf2SelfBase : LBEquipGroupAttachBufBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedTargetOnLaunch;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_IsTargetValid;

        [MethodImpl(0x8000)]
        public LBEquipGroupAttachBuf2SelfBase(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedTargetOnLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }
    }
}

