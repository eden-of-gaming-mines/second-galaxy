﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum TeleportConfirmInfoType
    {
        WormholeGate = 1,
        WormholeBackPoint = 2,
        WormholeDarkRoom = 3
    }
}

