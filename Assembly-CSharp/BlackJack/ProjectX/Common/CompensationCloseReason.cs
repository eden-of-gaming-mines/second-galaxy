﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum CompensationCloseReason
    {
        Finished,
        Cancelled,
        ClosedByAdmin,
        LeaveGuild,
        Timeout
    }
}

