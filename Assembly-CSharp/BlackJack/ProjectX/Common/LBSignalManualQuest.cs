﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class LBSignalManualQuest : LBSignalManualBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetQuestLevel;
        private static DelegateBridge __Hotfix_GetQuestRewardBonusMulti;
        private static DelegateBridge __Hotfix_GetQuestSrcType;
        private static DelegateBridge __Hotfix_GetQuestId;
        private static DelegateBridge __Hotfix_GetQuestInfo;
        private static DelegateBridge __Hotfix_GetAdditionReward;
        private static DelegateBridge __Hotfix_GetReward;
        private static DelegateBridge __Hotfix_GetRandomDropId;

        [MethodImpl(0x8000)]
        public LBSignalManualQuest(SignalInfo signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestRewardInfo> GetAdditionReward()
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestId()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataQuestInfo GetQuestInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetQuestLevel()
        {
        }

        [MethodImpl(0x8000)]
        public float GetQuestRewardBonusMulti()
        {
        }

        [MethodImpl(0x8000)]
        public QuestSrcType GetQuestSrcType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetRandomDropId()
        {
        }

        [MethodImpl(0x8000)]
        public List<QuestRewardInfo> GetReward()
        {
        }
    }
}

