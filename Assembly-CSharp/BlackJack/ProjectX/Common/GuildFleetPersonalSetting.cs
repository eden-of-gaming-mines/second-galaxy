﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Flags]
    public enum GuildFleetPersonalSetting
    {
        None = 0,
        RefuseJumping = 1,
        RefuseUseStargate = 2,
        RefuseFormation = 4
    }
}

