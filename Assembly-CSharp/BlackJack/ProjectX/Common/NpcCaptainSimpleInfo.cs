﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class NpcCaptainSimpleInfo
    {
        public ulong m_instanceId;
        public int m_lastNameId;
        public int m_firstNameId;
        public GrandFaction m_grandFaction;
        public int m_currShipId;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

