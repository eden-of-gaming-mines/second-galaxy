﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LostItem
    {
        public LostItemInfo m_item;
        public ItemCompensationStatus m_compensationStatus;
        public ItemCompensationDonatorType m_compensationDonatorType;
        public string m_compensationDonatorName;
        public DateTime m_compensationTime;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public LostItem()
        {
        }

        [MethodImpl(0x8000)]
        public LostItem(LostItemInfo item)
        {
        }
    }
}

