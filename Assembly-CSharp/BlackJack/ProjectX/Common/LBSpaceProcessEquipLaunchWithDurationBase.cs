﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessEquipLaunchWithDurationBase : LBSpaceProcessEquipLaunchBase
    {
        protected uint m_processDuration;
        protected uint m_processEndTime;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_GetDuration;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnEquipFire;

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipLaunchWithDurationBase()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessEquipLaunchWithDurationBase(uint startTime, uint instanceId, uint duration, LBSpaceProcessType processType, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ShipEquipSlotType slotType, int groupIndex, EquipType equipType, ILBSpaceProcessEquipLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetDuration()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, uint duration, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, ShipEquipSlotType slotType, int groupIndex, EquipType equipType, ILBSpaceProcessEquipLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnEquipFire(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }
    }
}

