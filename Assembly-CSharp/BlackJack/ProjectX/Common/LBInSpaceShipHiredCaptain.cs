﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBInSpaceShipHiredCaptain : LBInSpaceShipCaptainBase
    {
        private string m_playerGameUserId;
        private string m_playerName;
        private NpcCaptainInfo m_hiredCaptainInfo;
        private string m_captainName;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateLBInSpaceShipHiredCaptain;
        private static DelegateBridge __Hotfix_CreateNpcSpaceShipInstanceInfoByNpcCaptainInfo;
        private static DelegateBridge __Hotfix_CollectCaptainFeatsInSpaceBufList;
        private static DelegateBridge __Hotfix_IsPlayerCaptain;
        private static DelegateBridge __Hotfix_IsHiredCaptain;
        private static DelegateBridge __Hotfix_GetPlayerGameUserId;
        private static DelegateBridge __Hotfix_GetHiredCaptainInstanceId;
        private static DelegateBridge __Hotfix_GetCaptainName;
        private static DelegateBridge __Hotfix_GetCaptainFirstNameId;
        private static DelegateBridge __Hotfix_GetCaptainLastNameId;
        private static DelegateBridge __Hotfix_GetNpcCaptainInfo;
        private static DelegateBridge __Hotfix_GetOwnerPlayerName;

        [MethodImpl(0x8000)]
        public LBInSpaceShipHiredCaptain(BasicPropertiesInfo basicPropertiesInfo, int drivingLicenseId, int drivingLicenseLevel, string playerGameUserId, string playerName, NpcCaptainInfo hiredCaptainInfo, uint guildId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectCaptainFeatsInSpaceBufList(NpcCaptainInfo captainInfo, List<int> bufList)
        {
        }

        [MethodImpl(0x8000)]
        public static LBInSpaceShipHiredCaptain CreateLBInSpaceShipHiredCaptain(NpcCaptainInfo captainInfo, string ownerPlayerGameUserId, string ownerPlayerName, uint ownerPlayerGuildId)
        {
        }

        [MethodImpl(0x8000)]
        public static SolarSystemNpcShipInstanceInfo CreateNpcSpaceShipInstanceInfoByNpcCaptainInfo(NpcCaptainInfo captainInfo, List<int> captainStaticBufList = null, List<ShipStoreItemInfo> initShipStoreItemList = null)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetCaptainFirstNameId()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetCaptainLastNameId()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetCaptainName()
        {
        }

        [MethodImpl(0x8000)]
        public override ulong GetHiredCaptainInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public NpcCaptainInfo GetNpcCaptainInfo()
        {
        }

        [MethodImpl(0x8000)]
        public string GetOwnerPlayerName()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetPlayerGameUserId()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHiredCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsPlayerCaptain()
        {
        }
    }
}

