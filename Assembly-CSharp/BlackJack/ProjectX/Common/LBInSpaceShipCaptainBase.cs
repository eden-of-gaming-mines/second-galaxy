﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBInSpaceShipCaptainBase : ILBInSpaceShipCaptain, ILBShipCaptain, IPropertiesProvider
    {
        protected BasicPropertiesInfo m_basicPropertiesInfo;
        protected int m_drivingLicenseId;
        protected int m_drivingLicenseLevel;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_GetDrivingLicenseLevelById;
        private static DelegateBridge __Hotfix_GetDrivingLicenseId;
        private static DelegateBridge __Hotfix_HasPropertiesGroup;
        private static DelegateBridge __Hotfix_HasProperty;
        private static DelegateBridge __Hotfix_GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_UnregEventOnPropertiesGroupDirty;

        [MethodImpl(0x8000)]
        public LBInSpaceShipCaptainBase()
        {
        }

        [MethodImpl(0x8000)]
        public LBInSpaceShipCaptainBase(BasicPropertiesInfo basicPropertiesInfo, int drivingLicenseId, int drivingLicenseLevel)
        {
        }

        public abstract int GetCaptainFirstNameId();
        public abstract int GetCaptainLastNameId();
        public abstract string GetCaptainName();
        [MethodImpl(0x8000)]
        public int GetDrivingLicenseId()
        {
        }

        [MethodImpl(0x8000)]
        public int? GetDrivingLicenseLevelById(int drivingLicenseId)
        {
        }

        public abstract ulong GetHiredCaptainInstanceId();
        public abstract string GetPlayerGameUserId();
        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        public uint GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasProperty(int propertiesId)
        {
        }

        public abstract bool IsHiredCaptain();
        public abstract bool IsPlayerCaptain();
        [MethodImpl(0x8000)]
        public void RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }
    }
}

