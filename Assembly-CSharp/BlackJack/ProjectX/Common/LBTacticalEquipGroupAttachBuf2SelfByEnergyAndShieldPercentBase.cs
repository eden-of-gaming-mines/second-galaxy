﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBTacticalEquipGroupAttachBuf2SelfByEnergyAndShieldPercentBase : LBTacticalEquipGroupBase
    {
        protected float m_checkShieldPercent;
        protected float m_checkEnegyPercent;
        protected int m_shieldTriggerAttachBufId;
        protected int m_energyTriggerAttachBufId;
        protected LBBufBase m_currBufByShield;
        protected LBBufBase m_currBufByEnergy;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_get_ShieldTriggerAttachBufId;
        private static DelegateBridge __Hotfix_get_EnergyTriggerAttachBufId;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupAttachBuf2SelfByEnergyAndShieldPercentBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public int ShieldTriggerAttachBufId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int EnergyTriggerAttachBufId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

