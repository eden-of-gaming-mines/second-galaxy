﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildBenefitsDataContainer
    {
        void AddBenefits(GuildBenefitsInfoBase benefits);
        void AddUserToBenefitsReceivedList(string gameUserId, ulong benefitsInstanceId);
        List<GuildBenefitsInfoBase> GetAllBenefitsList();
        GuildBenefitsInfoBase GetBenefits(ulong instanceId);
        int GetBenefitsListVersion();
        void RemoveBenefits(ulong instanceId);
    }
}

