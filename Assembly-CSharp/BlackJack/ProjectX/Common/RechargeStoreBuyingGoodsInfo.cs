﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential)]
    public struct RechargeStoreBuyingGoodsInfo
    {
        public RechargeGoodsType m_type;
        public int m_goodsId;
        public DateTime m_expiredTime;
    }
}

