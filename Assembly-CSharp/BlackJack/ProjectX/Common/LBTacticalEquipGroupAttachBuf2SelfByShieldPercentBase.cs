﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBTacticalEquipGroupAttachBuf2SelfByShieldPercentBase : LBTacticalEquipGroupBase
    {
        protected float m_lessThanCheckShieldPercent;
        protected float m_moreThanCheckShieldPercent;
        protected int m_lessThanCheckAttachBufId;
        protected int m_moreThanCheckAttachBufId;
        protected LBBufBase m_lessThanlbBuf;
        protected LBBufBase m_moreThanlbBuf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_get_LessThanCheckAttachBufId;
        private static DelegateBridge __Hotfix_get_MoreThanCheckAttachBufId;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupAttachBuf2SelfByShieldPercentBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public int LessThanCheckAttachBufId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int MoreThanCheckAttachBufId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

