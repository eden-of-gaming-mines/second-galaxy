﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventDropBoxPickStart : LBSyncEvent
    {
        public uint m_destBoxObjectId;
        public uint m_pickEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

