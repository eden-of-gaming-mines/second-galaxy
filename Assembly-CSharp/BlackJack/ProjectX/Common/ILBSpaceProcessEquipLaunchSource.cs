﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessEquipLaunchSource : ILBSpaceProcessSource
    {
        void OnProcessEffect(LBSpaceProcessEquipLaunchBase process, string effect);
        void OnProcessFire(LBSpaceProcessEquipLaunchBase process);
        void OnProcessStartCharge(LBSpaceProcessEquipLaunchBase process);
    }
}

