﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LBSpaceProcessLaserLaunch : LBSpaceProcessWeaponLaunchBase
    {
        protected uint m_t1EndTime;
        protected uint m_t2EndTime;
        protected uint m_t3EndTime;
        protected float m_propertyCriticalRateFinal;
        protected List<KeyValuePair<ushort, uint>> m_unitLaunchList;
        protected List<uint> m_unitChargeTimeList;
        protected int m_unitLaunchListLoopIndex;
        protected int m_unitChargeListLoopIndex;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_GetT1EndTime;
        private static DelegateBridge __Hotfix_GetT2EndTime;
        private static DelegateBridge __Hotfix_GetT3EndTime;
        private static DelegateBridge __Hotfix_GetCriticalRateFinal;
        private static DelegateBridge __Hotfix_GetUnitLaunchList;
        private static DelegateBridge __Hotfix_GetUnitChargeList;
        private static DelegateBridge __Hotfix_GetLaunchCount;
        private static DelegateBridge __Hotfix_GetUnitLaunchTimeAbsByIndex;
        private static DelegateBridge __Hotfix_SetAttackTime;
        private static DelegateBridge __Hotfix_SetCriticalRate;
        private static DelegateBridge __Hotfix_AddUnitLaunch;
        private static DelegateBridge __Hotfix_AddUnitChargeInfo;
        private static DelegateBridge __Hotfix_SortUnitLaunchTime;
        private static DelegateBridge __Hotfix_ComparationForUnitLaunchInfo;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_get_ProcessSource;

        [MethodImpl(0x8000)]
        public LBSpaceProcessLaserLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public LBSpaceProcessLaserLaunch(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void AddUnitChargeInfo(uint chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void AddUnitLaunch(ushort index, uint processTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        private int ComparationForUnitLaunchInfo(KeyValuePair<ushort, uint> info1, KeyValuePair<ushort, uint> info2)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCriticalRateFinal()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetLaunchCount()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetT1EndTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetT2EndTime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetT3EndTime()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetUnitChargeList()
        {
        }

        [MethodImpl(0x8000)]
        public List<KeyValuePair<ushort, uint>> GetUnitLaunchList()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetUnitLaunchTimeAbsByIndex(ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessLaserLaunchSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttackTime(uint t1EndTime, uint t2EndTime, uint t3Endtime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCriticalRate(float propertyCriticalRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        public void SortUnitLaunchTime()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(uint currTime)
        {
        }

        protected ILBSpaceProcessLaserLaunchSource ProcessSource
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

