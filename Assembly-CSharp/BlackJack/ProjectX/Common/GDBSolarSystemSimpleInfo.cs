﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GDBSolarSystemSimpleInfo")]
    public class GDBSolarSystemSimpleInfo : IExtensible
    {
        private int _Id;
        private string _name;
        private float _gLocationX;
        private float _gLocationZ;
        private bool _isNeedLocalization;
        private string _localizationKey;
        private int _ownerStargroupId;
        private int _ownerStarfieldId;
        private GDBSolarSystemEditorChannelInfo _EditorChannelInfo;
        private int _solarsystemType;
        private bool _IsNotInfectable;
        private readonly List<IdWeightInfo> _SpecialMineralList;
        private bool _isHide;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_GLocationX;
        private static DelegateBridge __Hotfix_set_GLocationX;
        private static DelegateBridge __Hotfix_get_GLocationZ;
        private static DelegateBridge __Hotfix_set_GLocationZ;
        private static DelegateBridge __Hotfix_get_IsNeedLocalization;
        private static DelegateBridge __Hotfix_set_IsNeedLocalization;
        private static DelegateBridge __Hotfix_get_LocalizationKey;
        private static DelegateBridge __Hotfix_set_LocalizationKey;
        private static DelegateBridge __Hotfix_get_OwnerStargroupId;
        private static DelegateBridge __Hotfix_set_OwnerStargroupId;
        private static DelegateBridge __Hotfix_get_OwnerStarfieldId;
        private static DelegateBridge __Hotfix_set_OwnerStarfieldId;
        private static DelegateBridge __Hotfix_get_EditorChannelInfo;
        private static DelegateBridge __Hotfix_set_EditorChannelInfo;
        private static DelegateBridge __Hotfix_get_SolarsystemType;
        private static DelegateBridge __Hotfix_set_SolarsystemType;
        private static DelegateBridge __Hotfix_get_IsNotInfectable;
        private static DelegateBridge __Hotfix_set_IsNotInfectable;
        private static DelegateBridge __Hotfix_get_SpecialMineralList;
        private static DelegateBridge __Hotfix_get_IsHide;
        private static DelegateBridge __Hotfix_set_IsHide;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="gLocationX", DataFormat=DataFormat.FixedSize)]
        public float GLocationX
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="gLocationZ", DataFormat=DataFormat.FixedSize)]
        public float GLocationZ
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="isNeedLocalization", DataFormat=DataFormat.Default)]
        public bool IsNeedLocalization
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(6, IsRequired=false, Name="localizationKey", DataFormat=DataFormat.Default)]
        public string LocalizationKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="ownerStargroupId", DataFormat=DataFormat.TwosComplement)]
        public int OwnerStargroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="ownerStarfieldId", DataFormat=DataFormat.TwosComplement)]
        public int OwnerStarfieldId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="EditorChannelInfo", DataFormat=DataFormat.Default)]
        public GDBSolarSystemEditorChannelInfo EditorChannelInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="solarsystemType", DataFormat=DataFormat.TwosComplement)]
        public int SolarsystemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="IsNotInfectable", DataFormat=DataFormat.Default)]
        public bool IsNotInfectable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="SpecialMineralList", DataFormat=DataFormat.Default)]
        public List<IdWeightInfo> SpecialMineralList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="isHide", DataFormat=DataFormat.Default)]
        public bool IsHide
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

