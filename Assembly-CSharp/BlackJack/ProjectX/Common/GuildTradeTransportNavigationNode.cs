﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildTradeTransportNavigationNode
    {
        public GuildTradeTransportNavigationNodeType m_nodeType;
        public int m_solarSystemId;
        public int m_gdbConfId;
        public GuildBuildingType m_guildBuildingType;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

