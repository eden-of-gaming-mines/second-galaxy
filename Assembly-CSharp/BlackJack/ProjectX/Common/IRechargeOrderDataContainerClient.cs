﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IRechargeOrderDataContainerClient : IRechargeOrderDataContainer
    {
        void RefreshAllData(List<RechargeOrderInfo> orderList, int version);
    }
}

