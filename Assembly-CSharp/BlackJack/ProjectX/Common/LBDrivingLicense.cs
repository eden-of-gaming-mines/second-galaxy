﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBDrivingLicense
    {
        private int m_id;
        private LBPassiveSkill m_internalSkill;
        private ConfigDataDrivingLicenseInfo m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetPassiveSkill;
        private static DelegateBridge __Hotfix_GetId;
        private static DelegateBridge __Hotfix_GetDrivingLicenseConf;
        private static DelegateBridge __Hotfix_GetLevel;
        private static DelegateBridge __Hotfix_GetDrivingLicenseIdByShipConfig;

        [MethodImpl(0x8000)]
        public LBDrivingLicense(int id)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataDrivingLicenseInfo GetDrivingLicenseConf()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetDrivingLicenseIdByShipConfig(ConfigDataSpaceShipInfo shipConf)
        {
        }

        [MethodImpl(0x8000)]
        public int GetId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetLevel()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPassiveSkill(LBPassiveSkill skill)
        {
        }
    }
}

