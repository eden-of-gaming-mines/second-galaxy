﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    public class ChatContentText : ChatInfo
    {
        public string m_text;
        public int m_stringId;
        public List<FormatStringParamInfo> m_formatStrParamList;
        public bool m_showTranslate;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

