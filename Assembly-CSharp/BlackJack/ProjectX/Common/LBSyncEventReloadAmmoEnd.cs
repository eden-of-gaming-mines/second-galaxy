﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSyncEventReloadAmmoEnd : LBSyncEvent
    {
        public int m_weaponGroupIndex;
        public ItemInfo m_reloadAmmoItemInfo;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

