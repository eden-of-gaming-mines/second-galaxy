﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class DataFilterResultManagerBase
    {
        protected Dictionary<string, FilterResultCacheItem> m_filterResultDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetDataResultByFilter;
        private static DelegateBridge __Hotfix_ClearFilterResult;
        private static DelegateBridge __Hotfix_ClearAllResult;
        private static DelegateBridge __Hotfix_IsResultCacheItemValid;

        [MethodImpl(0x8000)]
        protected DataFilterResultManagerBase()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ClearAllResult()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ClearFilterResult(IDFRDataFilter filter)
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<IDFRFilterResultItem> GetDataResultByFilter(IDFRDataFilter filter, IComparer<IDFRFilterResultItem> comparer = null, int startIndex = 0, int count = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsResultCacheItemValid(FilterResultCacheItem item)
        {
        }

        protected class FilterResultCacheItem
        {
            public List<IDFRFilterResultItem> m_result;
            public int m_version;
            public IDFRDataFilter m_filter;
            public DateTime m_createTime;
            public DateTime m_nextCheckTime;
            public int m_totalCount;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

