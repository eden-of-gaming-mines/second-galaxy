﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildBuildingSolarSystemEffectInfo
    {
        public List<int> m_effectBufListForPlayerShip;
        public List<int> m_effectBufListForMiningShip;
        public List<int> m_effectBufListForFlagShip;
        public Dictionary<int, float> m_effectGuildPropertiesDic;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

