﻿namespace BlackJack.ProjectX.Common
{
    public interface IAllianceBase : IAllianceBasicBase, IAllianceChatBase, IAllianceMailBase, IAllianceInviteBase, IAllianceMembersBase
    {
    }
}

