﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IGuildMiningDataContainer
    {
        int GetMiningBalanceTimeHour();
        int GetMiningBalanceTimeMinute();
        void ModifyMiningBalanceSetting(int balanceHour, int balanceMinute);
    }
}

