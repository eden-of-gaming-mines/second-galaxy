﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBSuperWeaponGroupBase : LBSuperWeaponEquipGroupBase
    {
        protected ConfigDataSuperWeaponInfo m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_IsSuperWeapon;
        private static DelegateBridge __Hotfix_IsSuperEquip;
        private static DelegateBridge __Hotfix_GetWeaponCategory;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetEquipedItemCount;
        private static DelegateBridge __Hotfix_GetTotalUnitCount;
        private static DelegateBridge __Hotfix_GetTotalFireCount;
        private static DelegateBridge __Hotfix_GetAmmoConfId;
        private static DelegateBridge __Hotfix_GetAmmoCount;
        private static DelegateBridge __Hotfix_GetAmmoClipSize;
        private static DelegateBridge __Hotfix_GetCD4NextUnit;
        private static DelegateBridge __Hotfix_GetCD4NextTurret;
        private static DelegateBridge __Hotfix_GetWave;
        private static DelegateBridge __Hotfix_IsExistAttackEnemyOnFireRangeMax;
        private static DelegateBridge __Hotfix_GetConfMwEnergyNeed;
        private static DelegateBridge __Hotfix_GetConfMwEnergyGrow;
        private static DelegateBridge __Hotfix_HasAvailableTarget;
        private static DelegateBridge __Hotfix_CalcRealDamageElectric;
        private static DelegateBridge __Hotfix_CalcRealDamageHeat;
        private static DelegateBridge __Hotfix_CalcRealDamageKinetic;
        private static DelegateBridge __Hotfix_CalcIsHit;
        private static DelegateBridge __Hotfix_CalcIsCritical;
        private static DelegateBridge __Hotfix_CalcChargeTime;
        private static DelegateBridge __Hotfix_CalcPercentageDamge;
        private static DelegateBridge __Hotfix_GetChargeAndChannelBreakFactor;
        private static DelegateBridge __Hotfix_GetWeaponTransverseVelocity;
        private static DelegateBridge __Hotfix_CalcAllDamageMulti;
        private static DelegateBridge __Hotfix_GetDrivingLicenseDamageMulti;

        [MethodImpl(0x8000)]
        protected LBSuperWeaponGroupBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcAllDamageMulti()
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcChargeTime()
        {
        }

        protected abstract float CalcCriticalDamageTotal(ILBSpaceTarget target);
        protected abstract float CalcCriticalRate(ILBSpaceTarget target);
        protected abstract float CalcDamageComposeHeat();
        protected abstract float CalcDamageComposeKinetic();
        protected abstract float CalcDamageTotal(ILBSpaceTarget target);
        protected abstract float CalcFireCtrlAccuracy();
        protected abstract float CalcHitRateFinal(ILBSpaceTarget target);
        [MethodImpl(0x8000)]
        protected bool CalcIsCritical(float propertyCriticalRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CalcIsHit(float propertyHitRateFinal)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcPercentageDamge()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcRealDamageElectric(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcRealDamageHeat(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcRealDamageKinetic(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoClipSize()
        {
        }

        [MethodImpl(0x8000)]
        public override int GetAmmoConfId()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCD4NextTurret()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCD4NextUnit()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetChargeAndChannelBreakFactor()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataSuperWeaponInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetConfMwEnergyGrow()
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetConfMwEnergyNeed()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetDrivingLicenseDamageMulti()
        {
        }

        [MethodImpl(0x8000)]
        public int GetEquipedItemCount()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetTotalFireCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetTotalUnitCount()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetWave()
        {
        }

        [MethodImpl(0x8000)]
        public WeaponCategory GetWeaponCategory()
        {
        }

        [MethodImpl(0x8000)]
        public float GetWeaponTransverseVelocity()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool HasAvailableTarget(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsExistAttackEnemyOnFireRangeMax(bool isPVPEnable)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsSuperEquip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsSuperWeapon()
        {
        }
    }
}

