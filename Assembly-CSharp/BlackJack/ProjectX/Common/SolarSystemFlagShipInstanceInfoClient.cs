﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class SolarSystemFlagShipInstanceInfoClient : SolarSystemFlagShipInstanceInfo, IStaticFlagShipDataContainer, IStaticPlayerShipDataContainer, IFlagShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_AddShipStoreItem;
        private static DelegateBridge __Hotfix_GetConfRelativeData;
        private static DelegateBridge __Hotfix_GetCurrArmor;
        private static DelegateBridge __Hotfix_GetCurrEnergy;
        private static DelegateBridge __Hotfix_GetCurrShield;
        private static DelegateBridge __Hotfix_GetCurrShipFuel;
        private static DelegateBridge __Hotfix_GetHighSoltAmmoList;
        private static DelegateBridge __Hotfix_GetHighSoltGroupItemList;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetLowSlotItemList;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemList;
        private static DelegateBridge __Hotfix_GetOwnerName;
        private static DelegateBridge __Hotfix_GetShipConfId;
        private static DelegateBridge __Hotfix_GetShipDestroyState;
        private static DelegateBridge __Hotfix_GetShipHangarIndex;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_GetShipState;
        private static DelegateBridge __Hotfix_GetShipStoreItemIndex;
        private static DelegateBridge __Hotfix_GetShipStoreItemList;
        private static DelegateBridge __Hotfix_GetSolarSystemId;
        private static DelegateBridge __Hotfix_GetUnpackTime;
        private static DelegateBridge __Hotfix_IsDeleted;
        private static DelegateBridge __Hotfix_RemoveShipStoreItem;
        private static DelegateBridge __Hotfix_SetSolarSystemId;
        private static DelegateBridge __Hotfix_UpdataShipStoreItem;
        private static DelegateBridge __Hotfix_UpdateCurrArmor;
        private static DelegateBridge __Hotfix_UpdateCurrEnergy;
        private static DelegateBridge __Hotfix_UpdateCurrShield;
        private static DelegateBridge __Hotfix_UpdateCurrShipFuel;
        private static DelegateBridge __Hotfix_UpdateShipDestroyState;
        private static DelegateBridge __Hotfix_UpdateShipName;
        private static DelegateBridge __Hotfix_UpdateShipState;
        private static DelegateBridge __Hotfix_UpdateSlotGroupAmmoInfo;
        private static DelegateBridge __Hotfix_UpdateSlotGroupInfo;
        private static DelegateBridge __Hotfix_IsFlagShipDestroyed;
        private static DelegateBridge __Hotfix_UpdateFlagShipDestroyed;
        private static DelegateBridge __Hotfix_UpdateFlagShipDestroyedTime;
        private static DelegateBridge __Hotfix_RegisterFlagShipCaptain;
        private static DelegateBridge __Hotfix_UnregisterFlagShipCaptain;
        private static DelegateBridge __Hotfix_GetFlagShipCaptainInfo;
        private static DelegateBridge __Hotfix_GetReadOnlyShipInstanceInfo;
        private static DelegateBridge __Hotfix_UpdateOffLineBufList;
        private static DelegateBridge __Hotfix_GetOffLineBufList;

        [MethodImpl(0x8000)]
        public void AddShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static SolarSystemFlagShipInstanceInfoClient Create(SpaceEnterNtf ntf, ILBPlayerContext playerContext)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataFlagShipRelativeData GetConfRelativeData()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrArmor()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrShield()
        {
        }

        [MethodImpl(0x8000)]
        public double GetCurrShipFuel()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo GetFlagShipCaptainInfo()
        {
        }

        [MethodImpl(0x8000)]
        public AmmoInfo[] GetHighSoltAmmoList()
        {
        }

        [MethodImpl(0x8000)]
        public ShipSlotGroupInfo[] GetHighSoltGroupItemList()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipSlotGroupInfo> GetLowSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipSlotGroupInfo> GetMiddleSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public List<OffLineBufInfo> GetOffLineBufList()
        {
        }

        [MethodImpl(0x8000)]
        public string GetOwnerName()
        {
        }

        [MethodImpl(0x8000)]
        public GuildStaticFlagShipInstanceInfo GetReadOnlyShipInstanceInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipConfId()
        {
        }

        [MethodImpl(0x8000)]
        public ShipDestroyState GetShipDestroyState()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipHangarIndex()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipName()
        {
        }

        [MethodImpl(0x8000)]
        public ShipState GetShipState()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipStoreItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipStoreItemInfo> GetShipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetUnpackTime()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDeleted()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFlagShipDestroyed()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterFlagShipCaptain(PlayerSimplestInfo captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemId(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterFlagShipCaptain()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdataShipStoreItem(ShipStoreItemInfo shipItemInfo, long count, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrArmor(float armor)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShield(float shield)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShipFuel(double fuel)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFlagShipDestroyed(bool isDestroyed)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFlagShipDestroyedTime(DateTime time)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateOffLineBufList(List<OffLineBufInfo> offLineBufList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipDestroyState(ShipDestroyState state)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipName(string shipName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipState(ShipState state)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSlotGroupAmmoInfo(int groupIndex, StoreItemType ammoType, int ammoConfigId, int ammoCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSlotGroupInfo(ShipEquipSlotType slotType, int groupIndex, int storeItemIndex)
        {
        }
    }
}

