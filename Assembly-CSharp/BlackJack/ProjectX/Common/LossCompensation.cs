﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LossCompensation
    {
        public ulong m_InstanceId;
        public CompensationStatus m_compensationStatus;
        public DateTime m_compensationCreateTime;
        public DateTime m_compensationCloseTime;
        public CompensationCloseReason m_compensationCloseReason;
        public List<LostItem> m_lostItemList;
        public string m_userName;
        public string m_userId;
        public DateTime m_killTime;
        public KillRecordOpponentInfo m_winnerInfo;
        public KillRecordOpponentInfo m_loserInfo;
        public int m_killSolarSystemId;
        public int m_userMotherShipSolarSystemId;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public LossCompensation()
        {
        }

        [MethodImpl(0x8000)]
        public LossCompensation(List<LostItem> lostItemList)
        {
        }
    }
}

