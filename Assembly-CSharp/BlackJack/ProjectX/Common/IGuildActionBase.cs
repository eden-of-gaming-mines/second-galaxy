﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IGuildActionBase
    {
        ulong CalcGuildActionInfomationPointCost(ConfigDataGuildActionInfo guildActionConfig, int solarSystemId, int minJumpDistance);
        int CalcGuildMinJumpDistance2TargetSolarSystem(GDBHelper gdbHelper, int solarSystemId);
        float CalcInfoPointJumpMulti(ConfigDataGuildActionInfo guildActionConfig, int minJumpDistance);
        bool CheckSecurityLevel(int solarSystemId, ref int errCode);
        bool IsEnoughInfoPoint(int guildConfigId, int solarSysId, int minJumpDis, ref int errCode);
        bool IsSolarSystemSovereignLevelFit(int guildConfigId, int solarSystemId, ref int errCode);
        bool IsSovereignFit(int guildConfigId, int solarSystemId, ref int errCode);
    }
}

