﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildFlagShipHangarDataContainer
    {
        int GetBasicInfoVersion();
        List<IStaticFlagShipDataContainer> GetFlagShipDCList();
        GuildFlagShipHangarBasicInfo GetReadOnlyBasicInfo();
        IStaticFlagShipDataContainer GetShipDataContainer(int index, bool includeDeleted = false);
        IStaticFlagShipDataContainer GetShipDataContainer(ulong shipInstanceId, bool includeDeleted = false);
        int GetShipHangarSlotListVersion();
        IStaticFlagShipDataContainer SetShip(int storeItemIndex, ConfigDataSpaceShipInfo confInfo, int hangarIndex, DateTime currTime);
        void SetShipHangarDestroyed(bool isDestroyed);
        void SetShipHangarRecycled(bool isRecycled);
        void UnsetShip(int hangarIndex);
        void UpdateShipHangarBuildingLevel(int level);
        void UpdateShipHangarDeployedTime(DateTime time);
        void UpdateShipHangarInstanceId(ulong instanceId);
    }
}

