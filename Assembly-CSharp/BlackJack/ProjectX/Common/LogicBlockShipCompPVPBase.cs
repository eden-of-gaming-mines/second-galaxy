﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class LogicBlockShipCompPVPBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCleanupJustCrime;
        protected ILBInSpacePlayerShip m_ownerShip;
        protected ILBSpaceContext m_spaceCtx;
        public Action EventOnCleanupCriminalHunter;
        protected LogicBlockShipCompFireControllBase m_lbFireCtrl;
        protected LogicBlockShipCompTargetSelectBase m_lbTargetSelect;
        protected LogicBlockShipCompInSpaceWeaponEquipBase m_lbWeaponEquip;
        protected float m_spaceSecurityLevel;
        protected bool m_isJustCrime;
        protected DateTime? m_justCrimeTimeOut;
        protected float m_criminalLevel;
        protected bool m_isCriminalHunter;
        protected DateTime? m_criminalHunterTimeOut;
        protected HashSet<uint> m_legalAttackedTargetList;
        protected HashSet<uint> m_illegalAttackedTargetList;
        protected uint m_lastPVPFireTime;
        protected uint m_lastAidLeaveSolarSystemLimitTargetTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetSpaceSecurityLevel;
        private static DelegateBridge __Hotfix_GetPlayerPVPInfo;
        private static DelegateBridge __Hotfix_GetCriminalLevel;
        private static DelegateBridge __Hotfix_IsContinousCriminal;
        private static DelegateBridge __Hotfix_IsJustCriminal;
        private static DelegateBridge __Hotfix_IsCriminal;
        private static DelegateBridge __Hotfix_GetCriminalEndTime;
        private static DelegateBridge __Hotfix_IsCriminalHunter;
        private static DelegateBridge __Hotfix_GetCriminalHunterEndTime;
        private static DelegateBridge __Hotfix_IsInPVP;
        private static DelegateBridge __Hotfix_IsLimitLeaveSolarSystem;
        private static DelegateBridge __Hotfix_GetEndTimeForLimitLeaveSolarSystem;
        private static DelegateBridge __Hotfix_GetConstHostileBehaviorCountDownForLeaveSolarSystem;
        private static DelegateBridge __Hotfix_Islegal4AttackTarget;
        private static DelegateBridge __Hotfix_Islegal4AttackPlayer;
        private static DelegateBridge __Hotfix_Islegal4AttackHiredCaptain;
        private static DelegateBridge __Hotfix_Islegal4AttackNpc;
        private static DelegateBridge __Hotfix_Islegal4AttackHiredCaptainWithMaster;
        private static DelegateBridge __Hotfix_OnLockTarget;
        private static DelegateBridge __Hotfix_OnWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_OnWingShipWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_OnEquipLaunch2Target;
        private static DelegateBridge __Hotfix_OnWingShipEquipLaunch2Target;
        private static DelegateBridge __Hotfix_OnAidTarget;
        private static DelegateBridge __Hotfix_AddTarget2AttackedTargetList;
        private static DelegateBridge __Hotfix_OnIllegalAttack2Target;
        private static DelegateBridge __Hotfix_OnLeaveFight;
        private static DelegateBridge __Hotfix_ResetJustCrime_0;
        private static DelegateBridge __Hotfix_ResetJustCrime_1;
        private static DelegateBridge __Hotfix_ResetCriminalHunter_0;
        private static DelegateBridge __Hotfix_ResetCriminalHunter_1;
        private static DelegateBridge __Hotfix_CleanupJustCrime;
        private static DelegateBridge __Hotfix_CleanupCriminalHunter;
        private static DelegateBridge __Hotfix_ModifyCriminalLevel;
        private static DelegateBridge __Hotfix_GetTargetLBPVP;
        private static DelegateBridge __Hotfix_add_EventOnCleanupJustCrime;
        private static DelegateBridge __Hotfix_remove_EventOnCleanupJustCrime;

        public event Action EventOnCleanupJustCrime
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void AddTarget2AttackedTargetList(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void CleanupCriminalHunter()
        {
        }

        [MethodImpl(0x8000)]
        protected void CleanupJustCrime()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetConstHostileBehaviorCountDownForLeaveSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime? GetCriminalEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime? GetCriminalHunterEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCriminalLevel()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetEndTimeForLimitLeaveSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public PlayerPVPInfo GetPlayerPVPInfo()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSpaceSecurityLevel()
        {
        }

        [MethodImpl(0x8000)]
        private static LogicBlockShipCompPVPBase GetTargetLBPVP(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpacePlayerShip ownerShip, PlayerPVPInfo playerPVPInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsContinousCriminal()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCriminal()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCriminalHunter()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInPVP()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsJustCriminal()
        {
        }

        [MethodImpl(0x8000)]
        private bool Islegal4AttackHiredCaptain(ILBInSpaceNpcShip hiredCaptainShip)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool Islegal4AttackHiredCaptainWithMaster(ILBInSpaceNpcShip hiredCaptainShip)
        {
        }

        [MethodImpl(0x8000)]
        private bool Islegal4AttackNpc(ILBInSpaceNpcShip npcShip)
        {
        }

        [MethodImpl(0x8000)]
        private bool Islegal4AttackPlayer(ILBInSpacePlayerShip playerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool Islegal4AttackTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLimitLeaveSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public void ModifyCriminalLevel(float value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAidTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEquipLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnIllegalAttack2Target(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLeaveFight()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLockTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeaponLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWingShipEquipLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWingShipWeaponLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetCriminalHunter()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool ResetCriminalHunter(DateTime endTime)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetJustCrime()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ResetJustCrime(DateTime endTime)
        {
        }
    }
}

