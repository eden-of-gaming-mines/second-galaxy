﻿namespace BlackJack.ProjectX.Common
{
    using System;

    [Serializable]
    public enum ScanProbeType
    {
        DelegateScanProbe = 1,
        ManualScanProbe = 2,
        PVPScanProbe = 3
    }
}

