﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class PVPSignalInfo
    {
        public SignalType m_type;
        public ulong m_instanceId;
        public SignalInfo m_sourceSignal;
        public List<int> m_captainShipList;
        public uint m_dynamicSceneInstanceId;
        public int m_dynamicScenePlayerShipId;
        public int m_dynamicSceneRelativeQuestId;
        public int m_solarSystemId;
        public Vector3D m_location;
        public PlayerSimpleInfo m_srcPlayerInfo;
        public DateTime m_expiryTime;
        public List<ItemInfo> m_rewardList;
        public NpcInvadeInfo m_npcInvadeInfo;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

