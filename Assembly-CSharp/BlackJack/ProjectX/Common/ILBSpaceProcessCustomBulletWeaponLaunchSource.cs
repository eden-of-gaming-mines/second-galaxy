﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ILBSpaceProcessCustomBulletWeaponLaunchSource : ILBSpaceProcessEquipLaunchSource, ILBSpaceProcessSource
    {
        void OnBulletHitTarget();
    }
}

