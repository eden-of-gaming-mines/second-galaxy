﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class GuildFlagShipOptLogInfo
    {
        public uint m_seqId;
        public GuildFlagShipOptLogType m_type;
        public int m_solarSystemId;
        public List<FormatStringParamInfo> m_contentFormatStrParamList;
        public DateTime m_createTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

