﻿namespace BlackJack.ProjectX.Common
{
    using System.Collections.Generic;

    public interface IAllianceInviteBase
    {
        List<AllianceInviteInfo> GetAllianceInviteList();
    }
}

