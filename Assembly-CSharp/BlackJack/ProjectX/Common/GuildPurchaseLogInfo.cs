﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildPurchaseLogInfo
    {
        public GuildPurchaseLogType m_type;
        public GuildJobType m_operatorJob;
        public string m_operatorName;
        public long m_addUpCount;
        public long m_restCount;
        public DateTime m_time;
        public long m_completedCount;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

