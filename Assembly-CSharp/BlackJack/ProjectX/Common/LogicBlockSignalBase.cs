﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockSignalBase
    {
        protected Func<ulong> m_instanceIdAllocator;
        protected LogicBlockShipHangarsBase m_lbShipHangars;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected ILBPlayerContext m_playerCtx;
        protected ISignalDataContainer m_signalDC;
        protected List<LBSignalDelegateBase> m_delegateSignalList;
        protected List<LBSignalManualBase> m_manualSignalList;
        protected List<LBPVPSignal> m_pvpSignalList;
        protected List<LBSignalManualBase> m_emergencySignalList;
        protected Dictionary<ulong, LBSignalBase> m_allSignalDict;
        protected List<LBScanProbe> m_scanProbeList;
        protected Random m_rand;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnAddEmergencySignal;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRemoveEmergencySignal;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_CheckScanDelegateSignal;
        private static DelegateBridge __Hotfix_CheckScanManualSignal;
        private static DelegateBridge __Hotfix_CheckScanPVPSignal;
        private static DelegateBridge __Hotfix_OnScanProbeUsed;
        private static DelegateBridge __Hotfix_UpdatePVPSignalList;
        private static DelegateBridge __Hotfix_CheckAcceptQuest4ManualQuestSignal;
        private static DelegateBridge __Hotfix_CheckDelegateMissionStart;
        private static DelegateBridge __Hotfix_CalcDelegateSignalCountMax;
        private static DelegateBridge __Hotfix_CalcPVPSignalScanCount;
        private static DelegateBridge __Hotfix_CalcPVPSignalScanJumpStep;
        private static DelegateBridge __Hotfix_CalcDelegateMissionShipCountMax;
        private static DelegateBridge __Hotfix_GetSignalByInstanceId;
        private static DelegateBridge __Hotfix_GetSignalByConfigId;
        private static DelegateBridge __Hotfix_GetManualSignalByInstanceId;
        private static DelegateBridge __Hotfix_GetManualSignalByQuestId;
        private static DelegateBridge __Hotfix_GetEmergencySignalByQuestId;
        private static DelegateBridge __Hotfix_GetPVPSignalByInstanceId;
        private static DelegateBridge __Hotfix_GetEmergencySignalByInstanceId;
        private static DelegateBridge __Hotfix_GetDelegateSignalList;
        private static DelegateBridge __Hotfix_GetManualSignalList;
        private static DelegateBridge __Hotfix_GetPVPSignalList;
        private static DelegateBridge __Hotfix_GetEmergencySignalList;
        private static DelegateBridge __Hotfix_GetSignalDict;
        private static DelegateBridge __Hotfix_AddDelegateSignal;
        private static DelegateBridge __Hotfix_RemoveAllDelegateSignal;
        private static DelegateBridge __Hotfix_AddManualSignal;
        private static DelegateBridge __Hotfix_RemoveAllManualSignal;
        private static DelegateBridge __Hotfix_AddPVPSignal;
        private static DelegateBridge __Hotfix_RemoveAllPVPSignal;
        private static DelegateBridge __Hotfix_AddEmergencySignal;
        private static DelegateBridge __Hotfix_IsScanProbeAvailable;
        private static DelegateBridge __Hotfix_GetScanProbeByType;
        private static DelegateBridge __Hotfix_RemoveSignalByInstanceId;
        private static DelegateBridge __Hotfix_RemoveManualSignalByInstanceId;
        private static DelegateBridge __Hotfix_RemoveDelegateSignalByInstanceId;
        private static DelegateBridge __Hotfix_RemovePVPSignalByInstanceId;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickDelegateSignal;
        private static DelegateBridge __Hotfix_TickManualSignal;
        private static DelegateBridge __Hotfix_TickPVPSignal;
        private static DelegateBridge __Hotfix_TickEmergencySignal;
        private static DelegateBridge __Hotfix_TickScanProbe;
        private static DelegateBridge __Hotfix_InitSignals;
        private static DelegateBridge __Hotfix_InitScanProbe;
        private static DelegateBridge __Hotfix_add_EventOnAddEmergencySignal;
        private static DelegateBridge __Hotfix_remove_EventOnAddEmergencySignal;
        private static DelegateBridge __Hotfix_add_EventOnRemoveEmergencySignal;
        private static DelegateBridge __Hotfix_remove_EventOnRemoveEmergencySignal;

        public event Action<ulong> EventOnAddEmergencySignal
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRemoveEmergencySignal
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AddDelegateSignal(SignalInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void AddEmergencySignal(SignalInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AddManualSignal(SignalInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void AddPVPSignal(PVPSignalInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public int CalcDelegateMissionShipCountMax()
        {
        }

        [MethodImpl(0x8000)]
        protected int CalcDelegateSignalCountMax()
        {
        }

        [MethodImpl(0x8000)]
        public int CalcPVPSignalScanCount()
        {
        }

        [MethodImpl(0x8000)]
        public int CalcPVPSignalScanJumpStep()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckAcceptQuest4ManualQuestSignal(ulong signalIntstanceId, out LBSignalManualQuest manualQuest, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckDelegateMissionStart(ulong signalIntstanceId, List<ulong> captainInstanceId, out List<LBStaticHiredCaptain> captainList, out LBSignalBase signal, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckScanDelegateSignal(out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckScanManualSignal(out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckScanPVPSignal(out int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSignalDelegateBase> GetDelegateSignalList()
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualBase GetEmergencySignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualBase GetEmergencySignalByQuestId(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSignalManualBase> GetEmergencySignalList()
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualBase GetManualSignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalManualBase GetManualSignalByQuestId(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBSignalManualBase> GetManualSignalList()
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPSignal GetPVPSignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBPVPSignal> GetPVPSignalList()
        {
        }

        [MethodImpl(0x8000)]
        public LBScanProbe GetScanProbeByType(ScanProbeType probeType)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalBase GetSignalByConfigId(int id)
        {
        }

        [MethodImpl(0x8000)]
        public LBSignalBase GetSignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<ulong, LBSignalBase> GetSignalDict()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBPlayerContext lbPlayerCtx, Func<ulong> instanceIdAllocator = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitSignals()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsScanProbeAvailable(ScanProbeType probeType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnScanProbeUsed(ScanProbeType probeType, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAllDelegateSignal()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAllManualSignal()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveAllPVPSignal()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveDelegateSignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveManualSignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        protected void RemovePVPSignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSignalByInstanceId(ulong insId)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickDelegateSignal(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickEmergencySignal(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickManualSignal(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickPVPSignal(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickScanProbe(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePVPSignalList(List<PVPSignalInfo> pvpSignalList)
        {
        }
    }
}

