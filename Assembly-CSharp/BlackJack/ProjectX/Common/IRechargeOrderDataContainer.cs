﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IRechargeOrderDataContainer
    {
        void AddRechargeOrder(RechargeOrderInfo order);
        RechargeOrderInfo GetRechargeOrder(ulong orderInstanceId);
        List<RechargeOrderInfo> GetRechargeOrderList();
        int GetRechargeOrderListVersion();
        void RemoveRechargeOrder(ulong orderInstanceId);
    }
}

