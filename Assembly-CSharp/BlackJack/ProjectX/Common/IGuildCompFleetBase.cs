﻿namespace BlackJack.ProjectX.Common
{
    public interface IGuildCompFleetBase : IGuildFleetBase
    {
        GuildFleetListInfo GetGuildFleetlist();
    }
}

