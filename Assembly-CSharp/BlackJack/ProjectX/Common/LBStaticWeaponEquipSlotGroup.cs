﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBStaticWeaponEquipSlotGroup : IPropertiesProvider
    {
        protected LBWeaponEquipGroupBase m_inSpaceGroup;
        protected ShipEquipSlotType m_slotType;
        protected int m_groupIndex;
        protected StoreItemType m_equipedItemType;
        protected int m_equipedItemConfigId;
        protected bool m_equipedItemIsBind;
        protected bool m_equipedItemIsFreezing;
        protected int m_equipedWeaponTurretOrEquipCount;
        protected int m_equipedItemStoreIndex;
        protected int m_totalAmmoClipSize;
        protected AmmoInfo m_ammoInfo;
        protected object m_slotItemConfigData;
        protected float m_allItemStoreSize;
        protected bool m_allItemStoreSizeDirty;
        protected float m_allAmmoStoreSize;
        protected bool m_allAmmoStoreSizeDirty;
        protected Dictionary<int, float> m_singleItemPropertiesInfoDict;
        protected uint m_singleItemPropertiesMask;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_IsGroupEmpty;
        private static DelegateBridge __Hotfix_IsGroupLocked;
        private static DelegateBridge __Hotfix_IsWeapon;
        private static DelegateBridge __Hotfix_GetAmmoInfo;
        private static DelegateBridge __Hotfix_GetAmmoClipSize;
        private static DelegateBridge __Hotfix_IsWeaponAmmoFull;
        private static DelegateBridge __Hotfix_GetWeaponCategory;
        private static DelegateBridge __Hotfix_IsEquip;
        private static DelegateBridge __Hotfix_GetEquipCategory;
        private static DelegateBridge __Hotfix_GetEquipType;
        private static DelegateBridge __Hotfix_GetEquipFunctionType;
        private static DelegateBridge __Hotfix_IsLaunchableEquip;
        private static DelegateBridge __Hotfix_IsSameEquip;
        private static DelegateBridge __Hotfix_GetItemConfId;
        private static DelegateBridge __Hotfix_GetItemIsBind;
        private static DelegateBridge __Hotfix_GetItemIsFreezing;
        private static DelegateBridge __Hotfix_GetEquipedItemStoreIndex;
        private static DelegateBridge __Hotfix_GetEquipedWeaponTurrentOrEquipCount;
        private static DelegateBridge __Hotfix_GetEquipLimitId;
        private static DelegateBridge __Hotfix_GetAmmoConfId;
        private static DelegateBridge __Hotfix_GetAmmoType;
        private static DelegateBridge __Hotfix_GetAmmoCount;
        private static DelegateBridge __Hotfix_GetSlotType;
        private static DelegateBridge __Hotfix_GetItemType;
        private static DelegateBridge __Hotfix_GetGroupIndex;
        private static DelegateBridge __Hotfix_GetEmptyAmmoClipCount;
        private static DelegateBridge __Hotfix_SetItem;
        private static DelegateBridge __Hotfix_RemoveItem;
        private static DelegateBridge __Hotfix_ReloadAmmo;
        private static DelegateBridge __Hotfix_UpdateAmmo;
        private static DelegateBridge __Hotfix_UnloadAmmo;
        private static DelegateBridge __Hotfix_FreezingItemTimeout;
        private static DelegateBridge __Hotfix_GetAllItemStoreSize;
        private static DelegateBridge __Hotfix_GetAllAmmoStoreSize;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasPropertiesGroup;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.HasProperty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesGroupMask;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdAdd;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMin;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdMax;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneAddMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.GetPropertiesByIdOneSubMulti;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.RegEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_BlackJack.PropertiesCalculater.IPropertiesProvider.UnregEventOnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_InitSlotItemConfigData;
        private static DelegateBridge __Hotfix_CalcEquipedItemCount;
        private static DelegateBridge __Hotfix_InitPropertiesCache;
        private static DelegateBridge __Hotfix_CleanForEmptySlotGroup;
        private static DelegateBridge __Hotfix_CalcAllItemStoreSize;
        private static DelegateBridge __Hotfix_CalcAllAmmoStoreSize;
        private static DelegateBridge __Hotfix_GetDefaultAmmoInfo;
        private static DelegateBridge __Hotfix_GetAmmoBindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_get_InSpaceGroup;
        private static DelegateBridge __Hotfix_set_InSpaceGroup;
        private static DelegateBridge __Hotfix_GetPropertiesIdForCPUCostFinal;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForCPUCostFinal;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForCPUCostFinal;
        private static DelegateBridge __Hotfix_GetPropertiesIdForCPUCostBase;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForCPUCostBase;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForCPUCostBase;
        private static DelegateBridge __Hotfix_GetPropertiesIdForPowerCostFinal;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForPowerCostFinal;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForPowerCostFinal;
        private static DelegateBridge __Hotfix_GetPropertiesIdForPowerCostBase;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForPowerCostBase;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForPowerCostBase;
        private static DelegateBridge __Hotfix_GetPropertiesIdForPowerCostNS;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForPowerCostNS;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForPowerCostNS;
        private static DelegateBridge __Hotfix_GetPropertiesIdForGroupCD;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForGroupCD;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForGroupCD;
        private static DelegateBridge __Hotfix_GetPropertiesIdForEnergyCostFinal;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForEnergyCostFinal;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForEnergyCostFinal;
        private static DelegateBridge __Hotfix_GetPropertiesIdForEnergyPercentCostFinal;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForEnergyPercentCostFinal;
        private static DelegateBridge __Hotfix_GetPropertiesIdForFireRangeFinal;
        private static DelegateBridge __Hotfix_GetWeaponPropertiesIdForFireRangeFinal;
        private static DelegateBridge __Hotfix_GetEquipPropertiesIdForFireRangeFinal;

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup(ShipEquipSlotType slotType, int groupIndex, ShipSlotGroupInfo info, AmmoInfo ammoInfo)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdAdd(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMax(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdMin(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneAddMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.GetPropertiesByIdOneSubMulti(int propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        uint IPropertiesProvider.GetPropertiesGroupMask()
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasPropertiesGroup(int cacheGroup)
        {
        }

        [MethodImpl(0x8000)]
        bool IPropertiesProvider.HasProperty(int propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.RegEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        void IPropertiesProvider.UnregEventOnPropertiesGroupDirty(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAllAmmoStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAllItemStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        protected uint CalcEquipedItemCount()
        {
        }

        [MethodImpl(0x8000)]
        protected void CleanForEmptySlotGroup()
        {
        }

        [MethodImpl(0x8000)]
        public void FreezingItemTimeout()
        {
        }

        [MethodImpl(0x8000)]
        public float GetAllAmmoStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        public float GetAllItemStoreSize()
        {
        }

        [MethodImpl(0x8000)]
        public static float GetAmmoBindMoneyBuyPrice(AmmoInfo ammo)
        {
        }

        [MethodImpl(0x8000)]
        public int GetAmmoClipSize()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAmmoConfId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetAmmoCount()
        {
        }

        [MethodImpl(0x8000)]
        public AmmoInfo GetAmmoInfo()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemType GetAmmoType()
        {
        }

        [MethodImpl(0x8000)]
        public T GetConfInfo<T>() where T: class
        {
        }

        [MethodImpl(0x8000)]
        public static AmmoInfo GetDefaultAmmoInfo(ConfigDataWeaponInfo weaponConf)
        {
        }

        [MethodImpl(0x8000)]
        public int GetEmptyAmmoClipCount()
        {
        }

        [MethodImpl(0x8000)]
        public EquipCategory GetEquipCategory()
        {
        }

        [MethodImpl(0x8000)]
        public int GetEquipedItemStoreIndex()
        {
        }

        [MethodImpl(0x8000)]
        public int GetEquipedWeaponTurrentOrEquipCount()
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionType GetEquipFunctionType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetEquipLimitId()
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForCPUCostBase(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForCPUCostFinal(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForEnergyCostFinal(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForEnergyPercentCostFinal(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForFireRangeFinal(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForGroupCD(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForPowerCostBase(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForPowerCostFinal(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetEquipPropertiesIdForPowerCostNS(EquipType equipType)
        {
        }

        [MethodImpl(0x8000)]
        public EquipType GetEquipType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGroupIndex()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemConfId()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetItemIsBind()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetItemIsFreezing()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemType GetItemType()
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForCPUCostBase(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForCPUCostFinal(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForEnergyCostFinal(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForEnergyPercentCostFinal(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForFireRangeFinal(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForGroupCD(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForPowerCostBase(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForPowerCostFinal(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetPropertiesIdForPowerCostNS(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public ShipEquipSlotType GetSlotType()
        {
        }

        [MethodImpl(0x8000)]
        public WeaponCategory GetWeaponCategory()
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForCPUCostBase(WeaponCategory weaponCategory)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForCPUCostFinal(WeaponCategory weaponCategory)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForEnergyCostFinal(WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForFireRangeFinal(WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForGroupCD(WeaponCategory category, ShipSizeType sizeType)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForPowerCostBase(WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForPowerCostFinal(WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static PropertiesId GetWeaponPropertiesIdForPowerCostNS(WeaponCategory category)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPropertiesCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitSlotItemConfigData()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEquip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGroupEmpty()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGroupLocked()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLaunchableEquip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSameEquip(StoreItemType type, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWeaponAmmoFull()
        {
        }

        [MethodImpl(0x8000)]
        public bool ReloadAmmo(int itemConfigId, StoreItemType itemType, int count)
        {
        }

        [MethodImpl(0x8000)]
        public bool RemoveItem()
        {
        }

        [MethodImpl(0x8000)]
        public bool SetItem(LBStoreItem srcMSStoreItem, AmmoInfo defaultAmmoInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool UnloadAmmo()
        {
        }

        [MethodImpl(0x8000)]
        public bool UpdateAmmo(int itemConfigId, StoreItemType itemType, int count)
        {
        }

        public LBWeaponEquipGroupBase InSpaceGroup
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

