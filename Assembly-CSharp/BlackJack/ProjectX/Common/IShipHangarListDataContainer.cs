﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface IShipHangarListDataContainer
    {
        IStaticPlayerShipDataContainer GetShipDataContainer(int index);
        IStaticPlayerShipDataContainer GetShipDataContainer(ulong shipInstanceId);
        IStaticPlayerShipDataContainer SetShip(int storeItemIndex, ConfigDataSpaceShipInfo confInfo, int hangarIndex);
        void UnsetShip(int hangarIndex);
    }
}

