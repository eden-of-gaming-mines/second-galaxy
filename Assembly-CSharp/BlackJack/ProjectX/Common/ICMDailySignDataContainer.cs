﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface ICMDailySignDataContainer
    {
        SimpleItemInfoWithCount GetDailyRandReward();
        uint GetDailySignCount();
        DateTime GetDailySignRecvTime();
        void SetDailyRandReward(SimpleItemInfoWithCount dailyRandReward);
        void SetDailySignCount(uint signCount);
        void SetDailySignRecvTime(DateTime time);
    }
}

