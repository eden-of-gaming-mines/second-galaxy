﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSyncEventDroneDefenderLaunch : LBSyncEvent
    {
        public uint m_destObjectId;
        public LBSpaceProcessDroneDefenderLaunch m_process;
        public uint m_weaponGroupCDEndTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

