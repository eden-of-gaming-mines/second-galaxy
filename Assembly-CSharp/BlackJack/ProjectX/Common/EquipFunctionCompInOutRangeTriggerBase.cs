﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class EquipFunctionCompInOutRangeTriggerBase : EquipFunctionCompBase
    {
        protected HashSet<uint> m_touchedTargetList;
        protected List<LBSpaceProcess> m_processingList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_IsCharging;
        private static DelegateBridge __Hotfix_IsWorking;

        [MethodImpl(0x8000)]
        public EquipFunctionCompInOutRangeTriggerBase(IEquipFunctionCompOwner owner)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsCharging()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsWorking()
        {
        }
    }
}

