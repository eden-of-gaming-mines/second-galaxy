﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class GuildMemberRuntimeInfo
    {
        public ushort m_seq;
        public string m_name;
        public int m_level;
        public ProfessionType m_profession;
        public int m_avatarId;
        public int m_locateSolorSystem;
        public int m_evaluateScore;
        public DateTime m_lastOnlineTime;
        public bool m_isOnline;
        public int m_currSolarSystemId;
        public int m_currShipConfigId;
        public bool m_isInSpace;
        public DateTime m_lastReadGuildMomentsTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

