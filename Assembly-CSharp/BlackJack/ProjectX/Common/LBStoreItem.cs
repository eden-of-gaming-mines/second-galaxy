﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBStoreItem
    {
        protected StoreItemInfo m_readOnlyInfo;
        protected object m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Reinitlize;
        private static DelegateBridge __Hotfix_ClearForDelete;
        private static DelegateBridge __Hotfix_GetStoreItemIndex;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetItemType;
        private static DelegateBridge __Hotfix_GetConfId;
        private static DelegateBridge __Hotfix_GetConfigInfo;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_IsDeleted;
        private static DelegateBridge __Hotfix_IsBind;
        private static DelegateBridge __Hotfix_IsFreezing;
        private static DelegateBridge __Hotfix_GetFlag;
        private static DelegateBridge __Hotfix_IsSuspend;
        private static DelegateBridge __Hotfix_IsInBaseStore;
        private static DelegateBridge __Hotfix_IsSameItem;
        private static DelegateBridge __Hotfix_GetSuspendReason;
        private static DelegateBridge __Hotfix_GetInternalStoreItemInfo;
        private static DelegateBridge __Hotfix_GetSelfOccupySize;
        private static DelegateBridge __Hotfix_GetSelfSingleItemOccupySize;
        private static DelegateBridge __Hotfix_GetStoreItemConfigData;
        private static DelegateBridge __Hotfix_GetStoreItemOccupySize;
        private static DelegateBridge __Hotfix_GetOneSingleStoreItemOccupySize;
        private static DelegateBridge __Hotfix_CalcAmmoStoreSize;
        private static DelegateBridge __Hotfix_GetStoreItemRank;
        private static DelegateBridge __Hotfix_GetStoreItemSubRank;
        private static DelegateBridge __Hotfix_GetSaleItemPrice;
        private static DelegateBridge __Hotfix_CalcBindMoneyBuyPrice;
        private static DelegateBridge __Hotfix_IsBindOnPick;
        private static DelegateBridge __Hotfix_GetItemInfoCopy;
        private static DelegateBridge __Hotfix_GetBlueprintSubRankType;
        private static DelegateBridge __Hotfix_GetBlueprintRankType;

        [MethodImpl(0x8000)]
        public LBStoreItem(StoreItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static ulong CalcAmmoStoreSize(StoreItemType ammoType, int ammoConfigId, int ammoCount)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcBindMoneyBuyPrice(int confId, StoreItemType itemType, long count)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearForDelete()
        {
        }

        [MethodImpl(0x8000)]
        protected static RankType GetBlueprintRankType(ConfigDataProduceBlueprintInfo blueprintConfigData)
        {
        }

        [MethodImpl(0x8000)]
        protected static SubRankType GetBlueprintSubRankType(ConfigDataProduceBlueprintInfo blueprintConfigData)
        {
        }

        [MethodImpl(0x8000)]
        public int GetConfId()
        {
        }

        [MethodImpl(0x8000)]
        public T GetConfigInfo<T>() where T: class
        {
        }

        [MethodImpl(0x8000)]
        public uint GetFlag()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemInfo GetInternalStoreItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public long GetItemCount()
        {
        }

        [MethodImpl(0x8000)]
        public ItemInfo GetItemInfoCopy()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemType GetItemType()
        {
        }

        [MethodImpl(0x8000)]
        public static float GetOneSingleStoreItemOccupySize(object itemConfigData)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetSaleItemPrice(int confId, StoreItemType itemType, long count)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetSelfOccupySize()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetSelfSingleItemOccupySize(long count)
        {
        }

        [MethodImpl(0x8000)]
        public static object GetStoreItemConfigData(int configId, StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public int GetStoreItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public static ulong GetStoreItemOccupySize(object itemConfigData, long count)
        {
        }

        [MethodImpl(0x8000)]
        public static RankType GetStoreItemRank(object itemConfigData)
        {
        }

        [MethodImpl(0x8000)]
        public static SubRankType GetStoreItemSubRank(object itemConfigData)
        {
        }

        [MethodImpl(0x8000)]
        public ItemSuspendReason GetSuspendReason()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBind()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsBindOnPick(int confId, StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDeleted()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFreezing()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInBaseStore()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSameItem(StoreItemType type, int configId, bool isBind, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSuspend()
        {
        }

        [MethodImpl(0x8000)]
        public void Reinitlize(StoreItemInfo info)
        {
        }
    }
}

