﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class SolarSystemFlagShipInstanceInfo : SolarSystemPlayerShipInstanceInfo
    {
        public uint m_currFuel;
        public int m_solarSystemId;
        public bool m_isFlagShipBroading;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

