﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.PropertiesCalculater;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupReduceCDByKillingHitBase : LBTacticalEquipGroupBase, IEquipFunctionCompOwner
    {
        protected uint m_reduceCDTime;
        protected EquipFunctionCompKillingHitTriger m_killHitTrigger;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetOwnerShip;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetPropertiesCalc;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSlotType;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSlotGroupIndex;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetEquipType;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetConfBufIdList;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.GetSelfBufIdList;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.CalcChargeTime;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Common.IEquipFunctionCompOwner.AttachBufByEquip2Target;
        private static DelegateBridge __Hotfix_CreateKillHitTrigger;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupReduceCDByKillingHitBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        ulong IEquipFunctionCompOwner.AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        ushort IEquipFunctionCompOwner.CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        List<int> IEquipFunctionCompOwner.GetConfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        EquipType IEquipFunctionCompOwner.GetEquipType()
        {
        }

        [MethodImpl(0x8000)]
        ILBInSpaceShip IEquipFunctionCompOwner.GetOwnerShip()
        {
        }

        [MethodImpl(0x8000)]
        PropertiesCalculaterBase IEquipFunctionCompOwner.GetPropertiesCalc()
        {
        }

        [MethodImpl(0x8000)]
        IEnumerable<int> IEquipFunctionCompOwner.GetSelfBufIdList()
        {
        }

        [MethodImpl(0x8000)]
        int IEquipFunctionCompOwner.GetSlotGroupIndex()
        {
        }

        [MethodImpl(0x8000)]
        ShipEquipSlotType IEquipFunctionCompOwner.GetSlotType()
        {
        }

        [MethodImpl(0x8000)]
        protected EquipFunctionCompKillingHitTriger CreateKillHitTrigger()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        protected abstract void OnKillHitToTarget(ILBSpaceTarget target);
        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

