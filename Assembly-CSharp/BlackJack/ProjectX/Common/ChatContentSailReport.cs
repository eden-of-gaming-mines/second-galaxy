﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;

    public class ChatContentSailReport : ChatInfo
    {
        public float m_totalCauseDamage;
        public float m_totalSufferDamage;
        public double m_totalFlyDistance;
        public int m_totalJumpCount;
        public int m_destroyShipCount;
        public int m_totalEnterFightCount;
        public DateTime m_sailStartTime;
        public float m_sailDurationTime;
        public float m_sailGatherExp;
        public float m_sailGatherCredit;
        public float m_sailGatherMoney;
        public List<SimpleItemInfoWithCount> m_sailGatherItems;
        public List<byte[]> m_sailReportSolarSystemInfos;
        public string m_playerName;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

