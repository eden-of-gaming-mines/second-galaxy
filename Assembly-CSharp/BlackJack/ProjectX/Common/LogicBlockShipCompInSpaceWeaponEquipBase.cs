﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompInSpaceWeaponEquipBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase> EventOnWeaponLaunchCycleStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase> EventOnEquipLaunchCycleStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnWeaponLaunch2Target;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnEquipLaunch2Target;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LBInSpaceWeaponEquipGroupBase, bool> EventOnWeaponLaunchCycleEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBInSpaceWeaponEquipGroupBase, bool> EventOnEquipLaunchCycleEnd;
        protected ILBInSpaceShip m_ownerShip;
        protected IShipDataContainer m_shipDC;
        protected ILogicBlockShipCompSpaceObject m_lbSpaceObject;
        protected bool m_isSubSystemEnable;
        protected int m_isSubSystemEnableRef;
        protected LBStaticWeaponEquipSlotGroup m_staticNormalAttackGroup;
        protected LBStaticWeaponEquipSlotGroup m_staticDefaultBoosterGroup;
        protected LBStaticWeaponEquipSlotGroup m_staticDefaultRepairerGroup;
        protected List<LBStaticWeaponEquipSlotGroup> m_staticHighSlotGroupList;
        protected List<LBStaticWeaponEquipSlotGroup> m_staticMiddleSlotGroupList;
        protected List<LBStaticWeaponEquipSlotGroup> m_staticLowSlotGroupList;
        protected List<IPropertiesProvider> m_allStaticSlotGroupProviderList;
        protected LBWeaponGroupBase m_normalAttackGroup;
        protected List<LBWeaponGroupBase> m_highSlotWeaponGroupList;
        protected List<LBEquipGroupBase> m_equipGroupList;
        protected LBSuperWeaponEquipGroupBase m_superWeaponEquipGroup;
        protected LBEquipGroupBase m_defaultBoosterEquipGroup;
        protected LBEquipGroupBase m_defaultRepairerEquipGroup;
        protected LBTacticalEquipGroupBase m_defaultTacticalEquipGroup;
        protected float m_highSlotWeaponRangeMax;
        protected float m_highSlotWeaponRangeMin;
        protected bool?[] m_launchableEquipTypeCache;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PrepareRemoveFromSolarSystem;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_EnableSubSystem;
        private static DelegateBridge __Hotfix_EnableTacticalEquip;
        private static DelegateBridge __Hotfix_GetTacticalEquipEnableState;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickOnFrameEnd;
        private static DelegateBridge __Hotfix_CancelALlProcess;
        private static DelegateBridge __Hotfix_InitAllStaticGroup;
        private static DelegateBridge __Hotfix_InitNormalAttackWeaponStaticGroup;
        private static DelegateBridge __Hotfix_InitDefaultBoosterStaticGroup;
        private static DelegateBridge __Hotfix_InitDefaultRepairerStaticGroup;
        private static DelegateBridge __Hotfix_InitAllInSpaceGroup;
        private static DelegateBridge __Hotfix_PostInitAllInSpaceGroup;
        private static DelegateBridge __Hotfix_InitSingleInSpaceGroup;
        private static DelegateBridge __Hotfix_InitSuperWeaponEquip;
        private static DelegateBridge __Hotfix_InitNormalAttackInSpaceWeaponGroup;
        private static DelegateBridge __Hotfix_InitDefaultBoosterInSpaceWeaponGroup;
        private static DelegateBridge __Hotfix_InitDefaultRepairerInSpaceWeaponGroup;
        private static DelegateBridge __Hotfix_InitDefaultTacticalEquip;
        private static DelegateBridge __Hotfix_IsNecessaryToCreateTacticalEquipGroup;
        private static DelegateBridge __Hotfix_CreateInSpaceGroup;
        private static DelegateBridge __Hotfix_CreateSuperWeaponGroup;
        private static DelegateBridge __Hotfix_CreateSuperEquipGroup;
        private static DelegateBridge __Hotfix_CreateTacticalEquipGroup;
        private static DelegateBridge __Hotfix_CreateNormalAttackGroup;
        private static DelegateBridge __Hotfix_OnPropertiesGroupDirty;
        private static DelegateBridge __Hotfix_CalcAllWeaponRangeMaxMin;
        private static DelegateBridge __Hotfix_OnJumpingStart;
        private static DelegateBridge __Hotfix_OnJumpingEnd;
        private static DelegateBridge __Hotfix_OnBlinkEnd;
        private static DelegateBridge __Hotfix_OnInvsibleStart;
        private static DelegateBridge __Hotfix_OnOtherTargetBlink;
        private static DelegateBridge __Hotfix_OnOtherTargetJump;
        private static DelegateBridge __Hotfix_OnOtherTargetInvisible;
        private static DelegateBridge __Hotfix_CancelAllGroupLaunch;
        private static DelegateBridge __Hotfix_CancelAllGroupLaunchByDestTarget;
        private static DelegateBridge __Hotfix_OnWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_OnWeaponLaunchCycleStart;
        private static DelegateBridge __Hotfix_OnWeaponLaunchCycleEnd;
        private static DelegateBridge __Hotfix_OnWeaponOnAmmoReduce;
        private static DelegateBridge __Hotfix_OnEquipLaunch2Target;
        private static DelegateBridge __Hotfix_OnEquipLaunchCycleStart;
        private static DelegateBridge __Hotfix_OnEquipLaunchCycleEnd;
        private static DelegateBridge __Hotfix_OnReloadAllWeaponAmmo;
        private static DelegateBridge __Hotfix_GetStaticNormalAttackGroup;
        private static DelegateBridge __Hotfix_GetStaticHighSlotGroupList;
        private static DelegateBridge __Hotfix_GetStaticMiddleSlotGroupList;
        private static DelegateBridge __Hotfix_GetStaticLowSlotGroupList;
        private static DelegateBridge __Hotfix_GetStaticDefaultRepairerGroup;
        private static DelegateBridge __Hotfix_GetStaticDefaultBoosterGroup;
        private static DelegateBridge __Hotfix_GetAllStaticSlotGroupProviderList;
        private static DelegateBridge __Hotfix_GetNormalAttackWeaponGroup;
        private static DelegateBridge __Hotfix_GetHighSlotWeaponGroupList;
        private static DelegateBridge __Hotfix_GetHighSlotWeaponGroupByIndex;
        private static DelegateBridge __Hotfix_GetLaunchableEquipGroupList;
        private static DelegateBridge __Hotfix_GetEquipGroupByIndex;
        private static DelegateBridge __Hotfix_GetEquipGroupByType;
        private static DelegateBridge __Hotfix_HasLaunchableEquipByType;
        private static DelegateBridge __Hotfix_GetSuperWeaponGroup;
        private static DelegateBridge __Hotfix_GetSuperEquipGroup;
        private static DelegateBridge __Hotfix_GetTacticalEquipGroup;
        private static DelegateBridge __Hotfix_GetSuperWeaponEquipGroup;
        private static DelegateBridge __Hotfix_GetDefaultRepairerInSpaceEquipGroup;
        private static DelegateBridge __Hotfix_GetDefaultBoosterInSpaceEquipGroup;
        private static DelegateBridge __Hotfix_IsTargetInWeaponRangeMax;
        private static DelegateBridge __Hotfix_IsTargetInNormalAttachWeaponRange;
        private static DelegateBridge __Hotfix_GetWeaponRangeMin;
        private static DelegateBridge __Hotfix_GetWeaponRangeMax;
        private static DelegateBridge __Hotfix_ReloadAmmoImpl;
        private static DelegateBridge __Hotfix_ReloadAmmoImplForWeaponGroup;
        private static DelegateBridge __Hotfix_UnloadAmmoImpl;
        private static DelegateBridge __Hotfix_OutPutAmmoChangeOperationLog;
        private static DelegateBridge __Hotfix_CalcAllWeaponGroupDPS;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunchCycleStart;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunchCycleStart;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunchCycleStart;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunchCycleStart;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunch2Target;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunch2Target;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunch2Target;
        private static DelegateBridge __Hotfix_add_EventOnWeaponLaunchCycleEnd;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponLaunchCycleEnd;
        private static DelegateBridge __Hotfix_add_EventOnEquipLaunchCycleEnd;
        private static DelegateBridge __Hotfix_remove_EventOnEquipLaunchCycleEnd;

        public event Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnEquipLaunch2Target
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, bool> EventOnEquipLaunchCycleEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase> EventOnEquipLaunchCycleStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, ILBSpaceTarget> EventOnWeaponLaunch2Target
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase, bool> EventOnWeaponLaunchCycleEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBInSpaceWeaponEquipGroupBase> EventOnWeaponLaunchCycleStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public float CalcAllWeaponGroupDPS()
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcAllWeaponRangeMaxMin()
        {
        }

        [MethodImpl(0x8000)]
        protected void CancelAllGroupLaunch()
        {
        }

        [MethodImpl(0x8000)]
        protected void CancelAllGroupLaunchByDestTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CancelALlProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBInSpaceWeaponEquipGroupBase CreateInSpaceGroup(LBStaticWeaponEquipSlotGroup staticGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBInSpaceWeaponEquipGroupBase CreateNormalAttackGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBSuperEquipGroupBase CreateSuperEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBSuperWeaponGroupBase CreateSuperWeaponGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual LBTacticalEquipGroupBase CreateTacticalEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void EnableSubSystem(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void EnableTacticalEquip(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public List<IPropertiesProvider> GetAllStaticSlotGroupProviderList()
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase GetDefaultBoosterInSpaceEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase GetDefaultRepairerInSpaceEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase GetEquipGroupByIndex(ShipEquipSlotType slotType, int index)
        {
        }

        [MethodImpl(0x8000)]
        public LBEquipGroupBase GetEquipGroupByType(EquipType equipType, bool readyForLaunch = false)
        {
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupBase GetHighSlotWeaponGroupByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBWeaponGroupBase> GetHighSlotWeaponGroupList()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBEquipGroupBase> GetLaunchableEquipGroupList()
        {
        }

        [MethodImpl(0x8000)]
        public LBWeaponGroupBase GetNormalAttackWeaponGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetStaticDefaultBoosterGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetStaticDefaultRepairerGroup()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> GetStaticHighSlotGroupList()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> GetStaticLowSlotGroupList()
        {
        }

        [MethodImpl(0x8000)]
        public List<LBStaticWeaponEquipSlotGroup> GetStaticMiddleSlotGroupList()
        {
        }

        [MethodImpl(0x8000)]
        public LBStaticWeaponEquipSlotGroup GetStaticNormalAttackGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBSuperEquipGroupBase GetSuperEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBSuperWeaponEquipGroupBase GetSuperWeaponEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public LBSuperWeaponGroupBase GetSuperWeaponGroup()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool GetTacticalEquipEnableState()
        {
        }

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupBase GetTacticalEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public float GetWeaponRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public float GetWeaponRangeMin()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasLaunchableEquipByType(EquipType type)
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitAllInSpaceGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitAllStaticGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitDefaultBoosterInSpaceWeaponGroup(out LBInSpaceWeaponEquipGroupBase inSpaceGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitDefaultBoosterStaticGroup(ConfigDataSpaceShipInfo shipConfigData)
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitDefaultRepairerInSpaceWeaponGroup(out LBInSpaceWeaponEquipGroupBase inSpaceGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitDefaultRepairerStaticGroup(ConfigDataSpaceShipInfo shipConfigData)
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitDefaultTacticalEquip(out LBTacticalEquipGroupBase inSpaceGroup)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitNormalAttackInSpaceWeaponGroup(out LBInSpaceWeaponEquipGroupBase inSpaceGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitNormalAttackWeaponStaticGroup(ConfigDataSpaceShipInfo shipConfigData)
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitSingleInSpaceGroup(LBStaticWeaponEquipSlotGroup staticGroup, out LBInSpaceWeaponEquipGroupBase inSpaceGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool InitSuperWeaponEquip(out LBInSpaceWeaponEquipGroupBase inSpaceGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsNecessaryToCreateTacticalEquipGroup()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTargetInNormalAttachWeaponRange(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTargetInWeaponRangeMax(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBlinkEnd(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEquipLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEquipLaunchCycleEnd(LBInSpaceWeaponEquipGroupBase group, bool isCancel)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEquipLaunchCycleStart(LBInSpaceWeaponEquipGroupBase group)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnInvsibleStart(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnJumpingEnd(ILBInSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnJumpingStart(ILBInSpaceShip ship, Vector3D dest)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOtherTargetBlink(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOtherTargetInvisible(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOtherTargetJump(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPropertiesGroupDirty(int group)
        {
        }

        [MethodImpl(0x8000)]
        public void OnReloadAllWeaponAmmo(int sceneId = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeaponLaunch2Target(LBInSpaceWeaponEquipGroupBase group, ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeaponLaunchCycleEnd(LBInSpaceWeaponEquipGroupBase group, bool isCancel)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeaponLaunchCycleStart(LBInSpaceWeaponEquipGroupBase group)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnWeaponOnAmmoReduce(AmmoInfo ammoInfo, int changeCount)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OutPutAmmoChangeOperationLog(StoreItemType type, int configId, int changeValue, OperateLogItemChangeType causeId, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        protected bool PostInitAllInSpaceGroup()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PrepareRemoveFromSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        public bool ReloadAmmoImpl(LBWeaponGroupBase group, StoreItemType itemType, int itemConfigId, int count, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void ReloadAmmoImplForWeaponGroup(LBWeaponGroupBase group, StoreItemType itemType, int itemConfigId, int count, OperateLogItemChangeType causeId = 0, string location = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(uint currTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void TickOnFrameEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnloadAmmoImpl(LBWeaponGroupBase group, int unloadAmmoCount)
        {
        }
    }
}

