﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBTacticalEquipGroupAttachBuffByNotBeHitForAWhileBase : LBTacticalEquipGroupBase
    {
        protected float m_notBeHitTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_IsHostile;

        [MethodImpl(0x8000)]
        protected LBTacticalEquipGroupAttachBuffByNotBeHitForAWhileBase(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsHostile()
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }
    }
}

