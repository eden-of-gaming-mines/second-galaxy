﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface ICharacterFactionDataContainer
    {
        void ClearCreditQuestWeeklyRewardGet();
        void ClearFactionCreditNpcQuestInfo();
        void ClearFactionCreditNpcQuestInfo(int npcId);
        bool CreditQuestWeeklyRewardGet(int id);
        float? GetCreditByFactionId(int factionId);
        int GetCreditQuestCompleteTimes();
        Dictionary<int, FactionCreditLevelRewardInfo> GetFactionCreditLevelRewardInfo(int factionId);
        Dictionary<int, float> GetFactionCreditMap();
        FactionCreditNpcQuestInfo GetFactionCreditNpcQuestInfo(int npcId);
        DateTime GetNextCancelableTime();
        void SetCreditQuestWeeklyRewardGet(int id);
        void SetFactionCreditLevelRewardAchievedTime(int factionId, int rewardId, DateTime time);
        void SetFactionCreditLevelRewardGetIndex(int factionId, int rewardId);
        void SetFactionCreditNpcQuestInfo(int npcId, FactionCreditNpcQuestInfo questInfo);
        void UpdateCreditQuestCompleteTimes(int totalTimes);
        void UpdateFactionCredit(int factionId, float credit);
        void UpdateNextCancelableTime(DateTime cancelableTime);
    }
}

