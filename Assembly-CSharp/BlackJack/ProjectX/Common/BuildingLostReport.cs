﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class BuildingLostReport
    {
        public ulong m_instanceId;
        public int m_solarSystemId;
        public GuildBuildingType m_buildType;
        public List<CostInfo> m_lostItemList;
        public DateTime m_generateTime;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

