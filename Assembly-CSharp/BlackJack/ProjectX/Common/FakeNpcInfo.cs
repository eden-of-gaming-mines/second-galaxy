﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;

    [Serializable]
    public class FakeNpcInfo
    {
        public int m_firstNameId;
        public int m_lastNameId;
        public int m_guildNameId;
        public int m_level;
        public int m_avatarId;
        public ProfessionType m_profession;
        public GrandFaction m_grandFaction;
        public GenderType m_gender;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

