﻿namespace BlackJack.ProjectX.Common
{
    using System.Collections.Generic;

    public interface IGuildFlagShipOptLogDataContainer
    {
        List<GuildFlagShipOptLogInfo> GetFlagShipOptLogInfoList();
    }
}

