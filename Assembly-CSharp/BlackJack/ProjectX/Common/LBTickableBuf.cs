﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBTickableBuf : LBShipFightBuf
    {
        protected uint m_lastTickTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnAttach;
        private static DelegateBridge __Hotfix_OnDetach;
        private static DelegateBridge __Hotfix_OnTick;

        [MethodImpl(0x8000)]
        public LBTickableBuf(ConfigDataBufInfo confInfo, uint instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnAttach(ILBBufSource source, ILBBufOwner owner, uint currTime, uint lifeEndTime, LBBuffContainer container, object param = null, float bufInstanceParam1 = 0f, EquipType sourceEquipType = 0x30)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnDetach(bool isLifeEnd, object param = null)
        {
        }

        protected abstract void OnPeriod(uint tickTime);
        [MethodImpl(0x8000)]
        protected virtual void OnTick(uint currTime)
        {
        }
    }
}

