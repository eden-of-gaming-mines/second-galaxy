﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface ILBBufOwner
    {
        void OnAttachBuf(LBBufBase buf, ILBBufSource source, object param = null);
        void OnAttachDuplicate(LBBufBase buf, ILBBufSource source, float bufInstanceParam);
        void OnDetachBuf(LBBufBase buf, bool isLifeEnd, object param = null);
        void OnSyncAttachDuplicateInfo(LBBufBase buf, List<uint> sourceTargetList, int attachCount, float bufInstanceParam);
    }
}

