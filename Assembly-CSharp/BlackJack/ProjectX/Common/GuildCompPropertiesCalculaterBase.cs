﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompPropertiesCalculaterBase : GuildCompBase, IGuildCompPropertiesCalculaterBase, IGuildPropertiesCalculaterBase
    {
        protected Dictionary<int, GuildPropertiesCalculater> m_solarSystemPropertiesCalcDic;
        protected Dictionary<int, int> m_guildFlagDic;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CalcGuildPropertiesById;
        private static DelegateBridge __Hotfix_CheckGuildFlag;
        private static DelegateBridge __Hotfix_AddSolarSystemPropertiesCalc;
        private static DelegateBridge __Hotfix_RemoveSolarSystemPropertiesCalc;
        private static DelegateBridge __Hotfix_AddGuildBuildingConfigProvider;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingConfigProvider;
        private static DelegateBridge __Hotfix_RemoveGuildBuildingConfigProvider;

        [MethodImpl(0x8000)]
        public GuildCompPropertiesCalculaterBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AddGuildBuildingConfigProvider(int solarSystemId, GuildBuildingInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void AddSolarSystemPropertiesCalc(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcGuildPropertiesById(int solarSystemId, GuildPropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckGuildFlag(int solarSystemId, GuildFlag flag)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveGuildBuildingConfigProvider(int solarSystemId, GuildBuildingInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSolarSystemPropertiesCalc(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBuildingConfigProvider(int solarSystemId, GuildBuildingInfo info)
        {
        }
    }
}

