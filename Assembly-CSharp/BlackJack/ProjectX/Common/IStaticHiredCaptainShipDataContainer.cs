﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface IStaticHiredCaptainShipDataContainer : INpcShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        bool IsNeedRepire();
    }
}

