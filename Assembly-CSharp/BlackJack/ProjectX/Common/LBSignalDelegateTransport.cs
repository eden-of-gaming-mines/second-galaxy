﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LBSignalDelegateTransport : LBSignalDelegateBase
    {
        private readonly int m_signalDifficult;
        private readonly ConfigDataDelegateTransportSignalDifficultInfo m_signalDifficultConf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetSignalDifficult;
        private static DelegateBridge __Hotfix_GetGoodsCount;
        private static DelegateBridge __Hotfix_GetProcessDropInfoId;
        private static DelegateBridge __Hotfix_GetGoodsItemId;
        private static DelegateBridge __Hotfix_GetDestSolarSystemId;
        private static DelegateBridge __Hotfix_GetJumpDistance;

        [MethodImpl(0x8000)]
        public LBSignalDelegateTransport(SignalInfo signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public int GetDestSolarSystemId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGoodsCount()
        {
        }

        [MethodImpl(0x8000)]
        public int GetGoodsItemId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetJumpDistance()
        {
        }

        [MethodImpl(0x8000)]
        public int GetProcessDropInfoId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetSignalDifficult()
        {
        }
    }
}

