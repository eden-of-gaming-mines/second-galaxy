﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class GuildCompCompensationBase : GuildCompBase, IGuildCompCompensationBase, IGuildCompensationBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetCompensationList;
        private static DelegateBridge __Hotfix_GetCompensationListVersion;
        private static DelegateBridge __Hotfix_CloseCompensation;
        private static DelegateBridge __Hotfix_UpdateCompensation;
        private static DelegateBridge __Hotfix_AddCompensation;
        private static DelegateBridge __Hotfix_SetAutoCompensation;
        private static DelegateBridge __Hotfix_get_DC;

        [MethodImpl(0x8000)]
        public GuildCompCompensationBase(IGuildCompOwnerBase owner)
        {
        }

        [MethodImpl(0x8000)]
        public void AddCompensation(LossCompensation compensation)
        {
        }

        [MethodImpl(0x8000)]
        public void CloseCompensation(ulong instanceId, CompensationCloseReason reason)
        {
        }

        [MethodImpl(0x8000)]
        public List<LossCompensation> GetCompensationList()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCompensationListVersion()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAutoCompensation(bool isAuto)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCompensation(LossCompensation compensation)
        {
        }

        protected IGuildDataContainerBase DC
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

