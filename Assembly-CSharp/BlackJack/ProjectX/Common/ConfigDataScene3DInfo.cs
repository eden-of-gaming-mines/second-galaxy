﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataScene3DInfo")]
    public class ConfigDataScene3DInfo : IExtensible
    {
        private int _ID;
        private readonly List<SceneObject3DInfo> _SceneObjectList;
        private readonly List<SceneDummyObjectInfo> _DummyObjectList;
        private readonly List<EmptyDummyLocationInfo> _DummyLocationList;
        private readonly List<SceneWayPointListInfo> _WayPointList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ID;
        private static DelegateBridge __Hotfix_set_ID;
        private static DelegateBridge __Hotfix_get_SceneObjectList;
        private static DelegateBridge __Hotfix_get_DummyObjectList;
        private static DelegateBridge __Hotfix_get_DummyLocationList;
        private static DelegateBridge __Hotfix_get_WayPointList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ID", DataFormat=DataFormat.TwosComplement)]
        public int ID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="SceneObjectList", DataFormat=DataFormat.Default)]
        public List<SceneObject3DInfo> SceneObjectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="DummyObjectList", DataFormat=DataFormat.Default)]
        public List<SceneDummyObjectInfo> DummyObjectList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="DummyLocationList", DataFormat=DataFormat.Default)]
        public List<EmptyDummyLocationInfo> DummyLocationList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="WayPointList", DataFormat=DataFormat.Default)]
        public List<SceneWayPointListInfo> WayPointList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

