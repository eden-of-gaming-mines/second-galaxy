﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IStaticFlagShipDataContainer : IStaticPlayerShipDataContainer, IFlagShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        PlayerSimplestInfo GetFlagShipCaptainInfo();
        List<OffLineBufInfo> GetOffLineBufList();
        GuildStaticFlagShipInstanceInfo GetReadOnlyShipInstanceInfo();
        bool IsFlagShipDestroyed();
        void RegisterFlagShipCaptain(PlayerSimplestInfo captainInfo);
        void UnregisterFlagShipCaptain();
        void UpdateFlagShipDestroyed(bool isDestroyed);
        void UpdateFlagShipDestroyedTime(DateTime time);
        void UpdateOffLineBufList(List<OffLineBufInfo> offLineBufList);
    }
}

