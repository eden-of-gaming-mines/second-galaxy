﻿namespace BlackJack.ProjectX.Common
{
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBFormationControllerBase
    {
        protected LinkedList<IFormationMember> m_formationMemberList;
        protected Dictionary<uint, Vector3D> m_formationMemberId2LocalPosDict;
        protected ILBInSpaceShip m_formationLeader;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_OnMemberLeaveFormation;
        private static DelegateBridge __Hotfix_OnMemberJoinFormation;
        private static DelegateBridge __Hotfix_OnFormationDismiss;
        private static DelegateBridge __Hotfix_GetFormationPosForMember;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByJoinMember;
        private static DelegateBridge __Hotfix_OnFormationPosChangedByLeaveMember;

        [MethodImpl(0x8000)]
        public bool GetFormationPosForMember(uint memberObjId, out Vector3D pos)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Initialize(ILBInSpaceShip leader, FormationCreatingInfo formationInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFormationDismiss()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnFormationPosChangedByJoinMember(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnFormationPosChangedByLeaveMember(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnMemberJoinFormation(IFormationMember member)
        {
        }

        [MethodImpl(0x8000)]
        public bool OnMemberLeaveFormation(IFormationMember member)
        {
        }
    }
}

