﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public abstract class LBEquipGroupBase : LBInSpaceWeaponEquipGroupBase
    {
        protected ConfigDataShipEquipInfo m_confInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsOrdinaryWeapon;
        private static DelegateBridge __Hotfix_IsSuperWeapon;
        private static DelegateBridge __Hotfix_IsOrdinaryEquip;
        private static DelegateBridge __Hotfix_IsSuperEquip;
        private static DelegateBridge __Hotfix_GetEquipFunctionType;
        private static DelegateBridge __Hotfix_GetEquipSlotType;
        private static DelegateBridge __Hotfix_GetEquipType;
        private static DelegateBridge __Hotfix_GetEquipSlotIndex;
        private static DelegateBridge __Hotfix_GetAmmoConfId;
        private static DelegateBridge __Hotfix_GetAmmoType;
        private static DelegateBridge __Hotfix_GetAmmoCount;
        private static DelegateBridge __Hotfix_GetAmmoClipSize;
        private static DelegateBridge __Hotfix_CanMakeDamage;
        private static DelegateBridge __Hotfix_CalcGroupCD;
        private static DelegateBridge __Hotfix_CalcWaveCD;
        private static DelegateBridge __Hotfix_CalcLaunchEnergyCost;
        private static DelegateBridge __Hotfix_CalcChargeTime;
        private static DelegateBridge __Hotfix_CalcFireRangeMax;
        private static DelegateBridge __Hotfix_CalcLaunchFuelCost;
        private static DelegateBridge __Hotfix_CalcBufInstanceParam;
        private static DelegateBridge __Hotfix_AttachBufByEquip2Target;
        private static DelegateBridge __Hotfix_GetConfInfo;
        private static DelegateBridge __Hotfix_GetLocalPropertyFromConfigData4WeaponEquip;
        private static DelegateBridge __Hotfix_GetPropertyFromConfigData4Ammo;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewPropertiesById_1;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewPropertiesById_0;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewBufParamByIndex_1;
        private static DelegateBridge __Hotfix_CalcSelfEffectPreviewBufParamByIndex_0;
        private static DelegateBridge __Hotfix_CalcDestEffectPreviewPropertiesById_1;
        private static DelegateBridge __Hotfix_CalcDestEffectPreviewPropertiesById_0;
        private static DelegateBridge __Hotfix_CalcDestEffectPreviewBufParamByIndex_1;
        private static DelegateBridge __Hotfix_CalcDestEffectPreviewBufParamByIndex_0;
        private static DelegateBridge __Hotfix_GetEquipPreviewPropertiesBaseByPropertiesFinal;
        private static DelegateBridge __Hotfix_IsReadyForLaunch;
        private static DelegateBridge __Hotfix_IsTargetValid;
        private static DelegateBridge __Hotfix_GetAllAttackableEnemyOrNeutral;

        [MethodImpl(0x8000)]
        protected LBEquipGroupBase(ILBInSpaceShip ownerShip, LBStaticWeaponEquipSlotGroup staticSlotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected ulong AttachBufByEquip2Target(LBSpaceProcessEquipLaunchBase processEquipLaunch, ILBSpaceTarget target, int bufId)
        {
        }

        [MethodImpl(0x8000)]
        public void CalcBufInstanceParam(int bufId, out float bufInstanceParam, out PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcChargeTime()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcDestEffectPreviewBufParamByIndex(int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDestEffectPreviewBufParamByIndex(ConfigDataShipEquipInfo confInfo, int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcDestEffectPreviewPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcDestEffectPreviewPropertiesById(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcFireRangeMax()
        {
        }

        [MethodImpl(0x8000)]
        public override uint CalcGroupCD()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchEnergyCost()
        {
        }

        [MethodImpl(0x8000)]
        public override float CalcLaunchFuelCost()
        {
        }

        [MethodImpl(0x8000)]
        public float CalcSelfEffectPreviewBufParamByIndex(int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSelfEffectPreviewBufParamByIndex(ConfigDataShipEquipInfo confInfo, int index, out PropertiesId bufParamPropertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public float CalcSelfEffectPreviewPropertiesById(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public static float CalcSelfEffectPreviewPropertiesById(ConfigDataShipEquipInfo confInfo, PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        protected override uint CalcWaveCD()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override bool CanMakeDamage()
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> GetAllAttackableEnemyOrNeutral()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoClipSize()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoConfId()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override int GetAmmoCount()
        {
        }

        [MethodImpl(0x8000)]
        public sealed override StoreItemType GetAmmoType()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataShipEquipInfo GetConfInfo()
        {
        }

        [MethodImpl(0x8000)]
        public EquipFunctionType GetEquipFunctionType()
        {
        }

        [MethodImpl(0x8000)]
        private static PropertiesId GetEquipPreviewPropertiesBaseByPropertiesFinal(PropertiesId propertiesId)
        {
        }

        [MethodImpl(0x8000)]
        public int GetEquipSlotIndex()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEquipSlotType GetEquipSlotType()
        {
        }

        [MethodImpl(0x8000)]
        public EquipType GetEquipType()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetLocalPropertyFromConfigData4WeaponEquip(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertyFromConfigData4Ammo(PropertiesId propertiesId, out float result)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsOrdinaryEquip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsOrdinaryWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsReadyForLaunch(out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsSuperEquip()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsSuperWeapon()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsTargetValid(ILBSpaceTarget target, out int errCode)
        {
        }
    }
}

