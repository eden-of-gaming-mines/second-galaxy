﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LBTacticalEquipGroupPropertiesProviderBySpeed : LBTacticalEquipGroupGlobalDynamicPropertiesProviderBase
    {
        protected float m_shipSpeedMin;
        protected float m_shipSpeedMax;
        protected float m_factor4SpeedMin;
        protected float m_factor4SpeedMax;
        protected float m_factor4LessSpeedMin;
        protected float m_factor4ThanSpeedMax;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_GetPropertiesByIdAdd_;
        private static DelegateBridge __Hotfix_GetModifyFactor;
        private static DelegateBridge __Hotfix_IsPropertyCalculateDependOnSelfSpeed;
        private static DelegateBridge __Hotfix_CalcPropertiesDependOnSelfVelocity;
        private static DelegateBridge __Hotfix_get_ShipSpeedMin;
        private static DelegateBridge __Hotfix_get_ShipSpeedMax;
        private static DelegateBridge __Hotfix_get_Factor4SpeedMin;
        private static DelegateBridge __Hotfix_get_Factor4SpeedMax;

        [MethodImpl(0x8000)]
        public LBTacticalEquipGroupPropertiesProviderBySpeed(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CalcPropertiesDependOnSelfVelocity(ref float propertyValue)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetModifyFactor(ILBSpaceTarget target = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetPropertiesByIdAdd_(PropertiesId propertiesId, out float result, object ctx)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPropertyCalculateDependOnSelfSpeed(PropertiesId propertyId)
        {
        }

        [MethodImpl(0x8000)]
        public override void PostInitialize()
        {
        }

        public float ShipSpeedMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float ShipSpeedMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4SpeedMin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float Factor4SpeedMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

