﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;

    [Serializable]
    public class LBSynEventSuperEquipAttachBufLaunch : LBSyncEvent
    {
        public uint m_destObjectId;
        public LBSpaceProcessEquipAttachBufLaunch m_process;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

