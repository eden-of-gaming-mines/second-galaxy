﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IGuildPurchaseOrderBase
    {
        void AddGuildPurchaseOrder(GuildPurchaseOrderInfo info);
        bool CancelGuildPurchaseOrder(ulong orderInstanceId);
        List<GuildPurchaseOrderInfo> GetGuildPurchaseOrderListInfo();
        bool UpdateGuildPurchaseOrder(GuildPurchaseOrderInfo info);
    }
}

