﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public abstract class LBSpaceProcessWeaponEquipLaunchBase : LBSpaceProcess
    {
        protected byte m_weaponGroupId;
        protected ushort m_costEnergy;
        protected ushort m_costFuel;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_ClearProcess;
        private static DelegateBridge __Hotfix_GetWeaponGroupIndex;
        private static DelegateBridge __Hotfix_SetCostEnergy;
        private static DelegateBridge __Hotfix_GetCostEnergy;
        private static DelegateBridge __Hotfix_SetCostFuel;
        private static DelegateBridge __Hotfix_GetCostFuel;

        [MethodImpl(0x8000)]
        public LBSpaceProcessWeaponEquipLaunchBase(uint startTime, uint instanceId, LBSpaceProcessType type, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public override void ClearProcess()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetCostEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public ushort GetCostFuel()
        {
        }

        [MethodImpl(0x8000)]
        public virtual byte GetWeaponGroupIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Initilize(uint startTime, uint instanceId, LBSpaceProcessType type, ILBSpaceTarget srcTarget, ILBSpaceTarget destTarget, int groupIndex, ILBSpaceProcessSource processSource)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCostEnergy(ushort costEnergy)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCostFuel(ushort costFuel)
        {
        }
    }
}

