﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ToolUtil;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class LogicBlockFunctionOpenStateClient : LogicBlockFunctionOpenStateBase
    {
        protected BitArrayEx m_funcOpenAnimAddToQueueStateArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetFuncOpenAnimAddToQueue;
        private static DelegateBridge __Hotfix_SetFuncOpenAnimRemoveFromQueue;
        private static DelegateBridge __Hotfix_IsFuncOpenAnimAddToQueue;
        private static DelegateBridge __Hotfix_ClearFuncOpenAnimAddToQueue;

        [MethodImpl(0x8000)]
        public void ClearFuncOpenAnimAddToQueue()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFuncOpenAnimAddToQueue(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFuncOpenAnimAddToQueue(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFuncOpenAnimRemoveFromQueue(SystemFuncType funcType)
        {
        }
    }
}

