﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class UserSettingInClient
    {
        public UserCommonSetting m_commonSetting;
        public UserDisplaySetting m_displaySetting;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Reset;

        [MethodImpl(0x8000)]
        public void Reset()
        {
        }
    }
}

