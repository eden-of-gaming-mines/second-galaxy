﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;

    public interface IRechargeMonthlyCardDataContainerClient : IRechargeMonthlyCardDataContainer
    {
        void RefreshAllData(List<RechargeMonthlyCardInfo> montlyCardList, int version);
        void UpdateMonthlyCard(RechargeMonthlyCardInfo monthlyCard);
    }
}

