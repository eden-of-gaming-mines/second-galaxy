﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable]
    public class AllianceSimplestInfo
    {
        public uint m_allianceId;
        public string m_name;
        public AllianceLogoInfo m_logoInfo;
        public uint m_leaderGuildId;
        public string m_leaderGuildName;
        public string m_leaderGuildCode;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_1;

        [MethodImpl(0x8000)]
        public AllianceSimplestInfo()
        {
        }

        [MethodImpl(0x8000)]
        public AllianceSimplestInfo(AllianceBasicInfo basicInfo)
        {
        }
    }
}

