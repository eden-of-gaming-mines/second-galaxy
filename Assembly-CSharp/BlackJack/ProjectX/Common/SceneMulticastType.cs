﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public enum SceneMulticastType
    {
        None,
        Owner,
        OwnerTeamMember,
        GuildMember,
        All
    }
}

