﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class InSpaceSelfShipDataContainerDefault : IInSpacePlayerShipDataContainer, IInSpaceShipDataContainer, IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        protected IStaticPlayerShipDataContainer m_shipInfo;
        protected SpaceEnterNtf m_enterMsg;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetShipConfId;
        private static DelegateBridge __Hotfix_GetShipName;
        private static DelegateBridge __Hotfix_UpdateShipName;
        private static DelegateBridge __Hotfix_GetOwnerName;
        private static DelegateBridge __Hotfix_UpdateCurrEnergy;
        private static DelegateBridge __Hotfix_UpdateCurrShield;
        private static DelegateBridge __Hotfix_UpdateCurrArmor;
        private static DelegateBridge __Hotfix_UpdateShipState;
        private static DelegateBridge __Hotfix_GetCurrEnergy;
        private static DelegateBridge __Hotfix_GetCurrShield;
        private static DelegateBridge __Hotfix_GetCurrArmor;
        private static DelegateBridge __Hotfix_GetGuildFleetId;
        private static DelegateBridge __Hotfix_GetHighSoltGroupItemList;
        private static DelegateBridge __Hotfix_GetHighSoltAmmoList;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemList;
        private static DelegateBridge __Hotfix_GetLowSlotItemList;
        private static DelegateBridge __Hotfix_UpdateSlotGroupInfo;
        private static DelegateBridge __Hotfix_UpdateSlotGroupAmmoInfo;
        private static DelegateBridge __Hotfix_GetShipStoreItemList;
        private static DelegateBridge __Hotfix_UpdataShipStoreItem;
        private static DelegateBridge __Hotfix_AddShipStoreItem;
        private static DelegateBridge __Hotfix_RemoveShipStoreItem;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetShipState;
        private static DelegateBridge __Hotfix_GetShipStoreItemIndex;
        private static DelegateBridge __Hotfix_GetHigSlotWeaponReadyForLaunchTime;
        private static DelegateBridge __Hotfix_GetEquipGroupReadyForLaunchTime;
        private static DelegateBridge __Hotfix_IsAutoFight;
        private static DelegateBridge __Hotfix_GetSuperWeaponEnergy;
        private static DelegateBridge __Hotfix_GetStaticBufList;
        private static DelegateBridge __Hotfix_GetDynamicBufList;
        private static DelegateBridge __Hotfix_AddGuildSolarSystemBuf;
        private static DelegateBridge __Hotfix_IsWingShipEnable;
        private static DelegateBridge __Hotfix_GetLastWingShipOpTime;
        private static DelegateBridge __Hotfix_GetCurrWingShipObjId;
        private static DelegateBridge __Hotfix_GetHateTagetInitInfoList;
        private static DelegateBridge __Hotfix_GetInSpaceShipStoreItemList;

        [MethodImpl(0x8000)]
        public void AddGuildSolarSystemBuf(List<int> bufList)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void AddShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrArmor()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetCurrEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrShield()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetCurrWingShipObjId()
        {
        }

        [MethodImpl(0x8000)]
        public List<object> GetDynamicBufList()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetEquipGroupReadyForLaunchTime()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetGuildFleetId()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerable<KeyValuePair<uint, uint>> GetHateTagetInitInfoList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual AmmoInfo[] GetHighSoltAmmoList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ShipSlotGroupInfo[] GetHighSoltGroupItemList()
        {
        }

        [MethodImpl(0x8000)]
        public List<uint> GetHigSlotWeaponReadyForLaunchTime()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipStoreItemInfo> GetInSpaceShipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public uint GetLastWingShipOpTime()
        {
        }

        [MethodImpl(0x8000)]
        public List<ShipSlotGroupInfo> GetLowSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<ShipSlotGroupInfo> GetMiddleSlotItemList()
        {
        }

        [MethodImpl(0x8000)]
        public string GetOwnerName()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipConfId()
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipName()
        {
        }

        [MethodImpl(0x8000)]
        public ShipState GetShipState()
        {
        }

        [MethodImpl(0x8000)]
        public int GetShipStoreItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<ShipStoreItemInfo> GetShipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetStaticBufList()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSuperWeaponEnergy()
        {
        }

        [MethodImpl(0x8000)]
        public bool Initialize(IStaticPlayerShipDataContainer shipDC, SpaceEnterNtf enterMsg)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAutoFight()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWingShipEnable()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void RemoveShipStoreItem(ShipStoreItemInfo shipItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdataShipStoreItem(ShipStoreItemInfo shipItemInfo, long count, bool isFreezing = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrArmor(float armor)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateCurrEnergy(float energy)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrShield(float shield)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipName(string shipName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipState(ShipState state)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateSlotGroupAmmoInfo(int groupIndex, StoreItemType ammoType, int ammoConfigId, int ammoCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSlotGroupInfo(ShipEquipSlotType slotType, int groupIndex, int storeItemIndex)
        {
        }

        [CompilerGenerated]
        private sealed class <GetHateTagetInitInfoList>c__Iterator0 : IEnumerable, IEnumerable<KeyValuePair<uint, uint>>, IEnumerator, IDisposable, IEnumerator<KeyValuePair<uint, uint>>
        {
            internal List<ProHateTargetInfo>.Enumerator $locvar0;
            internal ProHateTargetInfo <it>__1;
            internal InSpaceSelfShipDataContainerDefault $this;
            internal KeyValuePair<uint, uint> $current;
            internal bool $disposing;
            internal int $PC;

            [MethodImpl(0x8000), DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [MethodImpl(0x8000), DebuggerHidden]
            IEnumerator<KeyValuePair<uint, uint>> IEnumerable<KeyValuePair<uint, uint>>.GetEnumerator()
            {
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<uint,uint>>.GetEnumerator();

            KeyValuePair<uint, uint> IEnumerator<KeyValuePair<uint, uint>>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

