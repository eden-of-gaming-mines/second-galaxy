﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LBStoredMail
    {
        protected StoredMailInfo m_readOnlyMailInfo;
        protected ConfigDataPredefineMailInfo m_preDefineMailConf;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitPreDefineMailConf;
        private static DelegateBridge __Hotfix_GetStoredIndex;
        private static DelegateBridge __Hotfix_IsDeleted;
        private static DelegateBridge __Hotfix_GetDeleteType;
        private static DelegateBridge __Hotfix_GetMailType;
        private static DelegateBridge __Hotfix_GetPredefineMailConf;
        private static DelegateBridge __Hotfix_ClearPredefineMailConf;
        private static DelegateBridge __Hotfix_GetMailInstanceId;
        private static DelegateBridge __Hotfix_GetReadonlyStoredMailInfo;
        private static DelegateBridge __Hotfix_GetDisplayTime;
        private static DelegateBridge __Hotfix_HasAttachement;
        private static DelegateBridge __Hotfix_HasAttachementStillInMail;
        private static DelegateBridge __Hotfix_GetAttachments;
        private static DelegateBridge __Hotfix_IsTimeOut;
        private static DelegateBridge __Hotfix_IsDisplay;
        private static DelegateBridge __Hotfix_CheckRemove;

        [MethodImpl(0x8000)]
        public LBStoredMail(StoredMailInfo stroedMailInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckRemove(out int errCode, DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearPredefineMailConf()
        {
        }

        [MethodImpl(0x8000)]
        public List<ItemInfo> GetAttachments()
        {
        }

        [MethodImpl(0x8000)]
        public MailDeleteType GetDeleteType()
        {
        }

        [MethodImpl(0x8000)]
        public DateTime GetDisplayTime()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetMailInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public MailType GetMailType()
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataPredefineMailInfo GetPredefineMailConf()
        {
        }

        [MethodImpl(0x8000)]
        public StoredMailInfo GetReadonlyStoredMailInfo()
        {
        }

        [MethodImpl(0x8000)]
        public int GetStoredIndex()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAttachement()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasAttachementStillInMail()
        {
        }

        [MethodImpl(0x8000)]
        public void InitPreDefineMailConf()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDeleted()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDisplay(DateTime currTime)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTimeOut(DateTime currTime)
        {
        }
    }
}

