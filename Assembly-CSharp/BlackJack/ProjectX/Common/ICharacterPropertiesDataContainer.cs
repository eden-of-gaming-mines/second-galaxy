﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;

    public interface ICharacterPropertiesDataContainer
    {
        float GetBasicPropertiesById(PropertiesId propertiesId);
        int GetFreeBasicProperties();
        void UpdateBasicPropertiesById(PropertiesId propertiesId, float value);
        void UpdateFreeBasicProperties(int value);
    }
}

