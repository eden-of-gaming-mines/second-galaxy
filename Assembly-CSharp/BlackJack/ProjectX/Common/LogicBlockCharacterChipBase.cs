﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockCharacterChipBase
    {
        protected ICharacterChipDataContainer m_dc;
        protected List<LBCharChipSlot> m_slotList;
        protected List<LBCharChipActiveSuit> m_activeSuitList;
        protected LBBuffContainer m_bufContainer;
        protected LogicBlockItemStoreBase m_lbItemStore;
        protected LogicBlockCharacterBase m_lbCharacter;
        protected ILBPlayerContext m_lbPlayerCtx;
        protected Random m_rand;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_InitCurrChipSchemeList;
        private static DelegateBridge __Hotfix_GetSlotList;
        private static DelegateBridge __Hotfix_GetSlotByIndex;
        private static DelegateBridge __Hotfix_CheckChipSet;
        private static DelegateBridge __Hotfix_CheckChipUnSet;
        private static DelegateBridge __Hotfix_CheckChipUnLock;
        private static DelegateBridge __Hotfix_CheckSchemeSwitch;
        private static DelegateBridge __Hotfix_ChipSchemeSwitchImpl;
        private static DelegateBridge __Hotfix_InitChipSuit;
        private static DelegateBridge __Hotfix_GetCharChipActiveSuit;
        private static DelegateBridge __Hotfix_UpdateChipSuit;
        private static DelegateBridge __Hotfix_ChipSetImpl;
        private static DelegateBridge __Hotfix_ChipUnSetImpl;
        private static DelegateBridge __Hotfix_GetBufContainer;
        private static DelegateBridge __Hotfix_GetCharacterEvaluateScore;

        [MethodImpl(0x8000)]
        public bool CheckChipSet(int slotIndex, int itemStoreIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChipUnLock(out int needTradeMoney, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckChipUnSet(int slotIndex, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckSchemeSwitch(int index, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public void ChipSchemeSwitchImpl(int switchIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected CharChipSlotInfo ChipSetImpl(int slotIndex, int chipId, int randomBuf, out int orgChipId)
        {
        }

        [MethodImpl(0x8000)]
        protected int ChipUnSetImpl(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public LBBuffContainer GetBufContainer()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCharacterEvaluateScore()
        {
        }

        [MethodImpl(0x8000)]
        public LBCharChipActiveSuit GetCharChipActiveSuit(int suitId)
        {
        }

        [MethodImpl(0x8000)]
        public LBCharChipSlot GetSlotByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public List<LBCharChipSlot> GetSlotList()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitChipSuit()
        {
        }

        [MethodImpl(0x8000)]
        private void InitCurrChipSchemeList()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateChipSuit()
        {
        }
    }
}

