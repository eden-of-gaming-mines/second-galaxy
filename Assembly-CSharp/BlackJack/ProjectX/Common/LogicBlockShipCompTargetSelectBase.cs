﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable]
    public class LogicBlockShipCompTargetSelectBase
    {
        protected ILBInSpaceShip m_ownerShip;
        protected ILogicBlockShipCompSpaceObject m_lbSpaceObject;
        protected uint m_lastEnemyListSortTickSeq;
        protected List<uint> m_sortedEnemyObjIdList;
        protected bool m_isPreferLessEnemyTarget;
        protected bool m_isRandPickLockTarget;
        protected bool m_isPreferPlayerTarget;
        protected bool m_isPreferNoPlayerTarget;
        protected bool m_isPreferPlayerAndHiredCaptainTarget;
        protected bool m_isOnlyAttackLockTarget;
        private const int SortedEnemyListCacheTimeout = 2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_OnEnemyAdd;
        private static DelegateBridge __Hotfix_OnEnemyRemove;
        private static DelegateBridge __Hotfix_OnEnemyClear;
        private static DelegateBridge __Hotfix_IsExistAttackableEnemy;
        private static DelegateBridge __Hotfix_GetAttackableEnemyByPriority;
        private static DelegateBridge __Hotfix_GetAllAttackableEnemyOrNeutral;
        private static DelegateBridge __Hotfix_SetIsPreferNoPlayerTarget;
        private static DelegateBridge __Hotfix_SetIsPreferPlayerTarget;
        private static DelegateBridge __Hotfix_SetIsPreferPlayerAndHiredCaptainTarget;
        private static DelegateBridge __Hotfix_SetIsRandPickLockTarget;
        private static DelegateBridge __Hotfix_SetIsPreferLessEnemyTarget;
        private static DelegateBridge __Hotfix_SetOnlyAttackLockTarget;
        private static DelegateBridge __Hotfix_IsPVPTarget;
        private static DelegateBridge __Hotfix_HasEnemyInView;
        private static DelegateBridge __Hotfix_SortEnemyList;
        private static DelegateBridge __Hotfix_ComparationForTargetListEnemyCountSort;
        private static DelegateBridge __Hotfix_ComparationForTargetListSort;
        private static DelegateBridge __Hotfix_get_IsPreferLessEnemyTarge;
        private static DelegateBridge __Hotfix_get_IsRandPickLockTarget;
        private static DelegateBridge __Hotfix_get_IsOnlyAttackLockTarget;

        [MethodImpl(0x8000)]
        public int ComparationForTargetListEnemyCountSort(ILBSpaceTarget t1, ILBSpaceTarget t2)
        {
        }

        [MethodImpl(0x8000)]
        public int ComparationForTargetListSort(uint t1Id, uint t2Id)
        {
        }

        [MethodImpl(0x8000)]
        public List<ILBSpaceTarget> GetAllAttackableEnemyOrNeutral(float radius)
        {
        }

        [MethodImpl(0x8000)]
        public ILBSpaceTarget GetAttackableEnemyByPriority(double inRadius = 1.7976931348623157E+308)
        {
        }

        [MethodImpl(0x8000)]
        public bool HasEnemyInView()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Initialize(ILBInSpaceShip ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsExistAttackableEnemy(float radius, bool isPVPEnable, bool isHostile)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsPVPTarget(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyAdd(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyClear()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyRemove(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsPreferLessEnemyTarget(bool isPreferLessEnemyPlayer)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsPreferNoPlayerTarget(bool isPreferNoPlayerTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsPreferPlayerAndHiredCaptainTarget(bool isPreferPlayerAndHiredCaptainTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsPreferPlayerTarget(bool isPreferPlayerTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsRandPickLockTarget(bool isRandPickLockTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOnlyAttackLockTarget(bool isOnlyAttackLockTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void SortEnemyList()
        {
        }

        public bool IsPreferLessEnemyTarge
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsRandPickLockTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsOnlyAttackLockTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

