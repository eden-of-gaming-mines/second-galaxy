﻿namespace BlackJack.ProjectX.Common
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogicBlockPlayerInfoCacheClient
    {
        private ILBPlayerContext m_playerCtx;
        private readonly Dictionary<string, PlayerSimplestInfo> m_playerInfoCacheDictionary;
        private const int PlayerSimpleInfoValidTime = 30;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initilize;
        private static DelegateBridge __Hotfix_PostInitialize;
        private static DelegateBridge __Hotfix_TryGetPlayerSimplestInfoList;
        private static DelegateBridge __Hotfix_GetPlayerSimplestInfoById;
        private static DelegateBridge __Hotfix_UpdatePlayerSimplestInfoCache;
        private static DelegateBridge __Hotfix_UpdatePlayerSimplestInfoValidTime;
        private static DelegateBridge __Hotfix_GetPlayerSimplestInfoByIdWithVersion;

        [MethodImpl(0x8000)]
        public PlayerSimplestInfo GetPlayerSimplestInfoById(string gameUserId)
        {
        }

        [MethodImpl(0x8000)]
        protected PlayerSimplestInfo GetPlayerSimplestInfoByIdWithVersion(string gameUserId, out int version)
        {
        }

        [MethodImpl(0x8000)]
        public bool Initilize(ILBPlayerContext lbPlayerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public bool PostInitialize()
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetPlayerSimplestInfoList(List<string> gameUserIdList, out List<PlayerSimplestInfo> playerSimpleInfoList, ref List<string> needReqIdList, ref List<int> reqIdVersionList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerSimplestInfoCache(List<PlayerSimplestInfo> newPlayerInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerSimplestInfoValidTime(List<PlayerSimplestInfo> newPlayerInfoList)
        {
        }
    }
}

