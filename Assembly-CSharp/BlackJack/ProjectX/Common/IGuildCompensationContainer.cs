﻿namespace BlackJack.ProjectX.Common
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildCompensationContainer
    {
        void AddCompensation(LossCompensation c);
        void CloseCompensation(ulong instanceId, CompensationCloseReason reason, DateTime? closeTime = new DateTime?());
        void DoItemCompensation(ulong compensationId, int itemIndex, ItemCompensationDonatorType donatorType, string donatorName, ItemCompensationStatus itemCompensationStatus = 2, DateTime? compensationTime = new DateTime?());
        List<LossCompensation> GetCompensationList();
        int GetCompensationListVersion();
        void SetGuildAutoCompensation(bool autoCompensation);
        void UpdateCompensation(LossCompensation compensation);
        void UpdateCompensationDonateToday(string playerUserId, int compensationDonateToday);
    }
}

