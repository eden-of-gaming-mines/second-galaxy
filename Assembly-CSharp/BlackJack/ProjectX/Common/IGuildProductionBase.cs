﻿namespace BlackJack.ProjectX.Common
{
    using BlackJack.ConfigData;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    public interface IGuildProductionBase
    {
        bool AddGuildProductionLine(GuildProductionLineInfo lineInfo);
        List<CostInfo> CalcGuidProductionItemRealCost(int solarSystemId, GuildProduceCategory category, List<CostInfo> configItemCostList, int count);
        int CalcGuildProductionTimeRealCost(int solarSystemId, GuildProduceCategory category, int configTime, int count);
        ulong CalcGuildProductionTradeMoneyRealCost(int solarSystemId, GuildProduceCategory category, ulong configMoneyCost, int count);
        bool CheckGuildProductionDataVersion(uint version);
        bool CheckProduceTempleteUnlockCondition(ConfigDataGuildProduceTempleteInfo confInfo, out int errCode);
        int CheckProduceTempleteUnlockCondition_SovereignGalaxyCount(ConfigDataGuildProduceTempleteInfo confInfo);
        bool CheckProductionLineCancel(ulong lineId, DateTime currTime, out int errCode);
        bool CheckProductionLineComplete(ulong lineId, DateTime currTime, out int errCode);
        bool CheckProductionLineSpeedUpByTradeMoney(ulong lineId, int speedUpLevel, ulong currTradeMoney, DateTime currTime, out int errCode, out int costTradeMoney);
        bool CheckProductionLineStart(ulong lineId, int templeteId, int produceCount, out int errCode, out ulong moneyCost, out int timeCost, out List<CostInfo> costList);
        void ClearGuildProductionLineData(ulong lineId);
        uint GetGuildProductionDataVersion();
        GuildProductionLineInfo GetGuildProductionLineById(ulong lineId);
        List<GuildProductionLineInfo> GetGuildProductionLineList();
        bool RemoveGuildProductionLine(ulong lineId);
        void UpdateGuildProductionLine(GuildProductionLineInfo info);
        void UpdateProductionLineSpeedUpTime(ulong lineId, int reduceTime);
    }
}

