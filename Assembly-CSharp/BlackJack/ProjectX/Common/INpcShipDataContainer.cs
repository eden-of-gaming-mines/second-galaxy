﻿namespace BlackJack.ProjectX.Common
{
    using System;

    public interface INpcShipDataContainer : IShipDataContainer, ISpaceWeaponEquipDataContainer, IShipItemStoreDataContainer
    {
        int GetNpcMonsterConfigId();
        int GetNPCMonsterLevel();
        int GetNpcMonsterLevelBufConfId();
        int GetNpcMonsterLevelDropConfId();
        int GetNpcShipConfigId();
        int GetNPCShipTemplateId();
    }
}

