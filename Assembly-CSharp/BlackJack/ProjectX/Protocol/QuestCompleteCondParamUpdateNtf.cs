﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="QuestCompleteCondParamUpdateNtf")]
    public class QuestCompleteCondParamUpdateNtf : IExtensible
    {
        private int _QuestInstanceId;
        private int _CondIndex;
        private int _CondParam;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_QuestInstanceId;
        private static DelegateBridge __Hotfix_set_QuestInstanceId;
        private static DelegateBridge __Hotfix_get_CondIndex;
        private static DelegateBridge __Hotfix_set_CondIndex;
        private static DelegateBridge __Hotfix_get_CondParam;
        private static DelegateBridge __Hotfix_set_CondParam;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="QuestInstanceId", DataFormat=DataFormat.TwosComplement)]
        public int QuestInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="CondIndex", DataFormat=DataFormat.TwosComplement)]
        public int CondIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="CondParam", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CondParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

