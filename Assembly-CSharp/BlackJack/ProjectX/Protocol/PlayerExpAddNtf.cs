﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerExpAddNtf")]
    public class PlayerExpAddNtf : IExtensible
    {
        private uint _ExpAdd;
        private bool _IgnoreDailyExpLimit;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ExpAdd;
        private static DelegateBridge __Hotfix_set_ExpAdd;
        private static DelegateBridge __Hotfix_get_IgnoreDailyExpLimit;
        private static DelegateBridge __Hotfix_set_IgnoreDailyExpLimit;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((long) 0L), ProtoMember(1, IsRequired=false, Name="ExpAdd", DataFormat=DataFormat.TwosComplement)]
        public uint ExpAdd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(2, IsRequired=false, Name="IgnoreDailyExpLimit", DataFormat=DataFormat.Default)]
        public bool IgnoreDailyExpLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

