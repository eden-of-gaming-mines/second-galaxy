﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBProcessSingleSuperMissileUnitLaunchInfo")]
    public class ProLBProcessSingleSuperMissileUnitLaunchInfo : IExtensible
    {
        private ProLBProcessSingleUnitLaunchInfo _baseInfo;
        private uint _BulletFlyTime;
        private uint _TargetObjId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BaseInfo;
        private static DelegateBridge __Hotfix_set_BaseInfo;
        private static DelegateBridge __Hotfix_get_BulletFlyTime;
        private static DelegateBridge __Hotfix_set_BulletFlyTime;
        private static DelegateBridge __Hotfix_get_TargetObjId;
        private static DelegateBridge __Hotfix_set_TargetObjId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="baseInfo", DataFormat=DataFormat.Default)]
        public ProLBProcessSingleUnitLaunchInfo BaseInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BulletFlyTime", DataFormat=DataFormat.TwosComplement)]
        public uint BulletFlyTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="TargetObjId", DataFormat=DataFormat.TwosComplement)]
        public uint TargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

