﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBenefitsSovereignty")]
    public class ProGuildBenefitsSovereignty : IExtensible
    {
        private bool _IsReceived;
        private ulong _InstanceId;
        private uint _GuildId;
        private long _CreateTime;
        private long _ExpireTime;
        private readonly List<ProSimpleItemInfoWithCount> _ItemList;
        private int _SolarSystemCount;
        private int _ConfigId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsReceived;
        private static DelegateBridge __Hotfix_set_IsReceived;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_get_ExpireTime;
        private static DelegateBridge __Hotfix_set_ExpireTime;
        private static DelegateBridge __Hotfix_get_ItemList;
        private static DelegateBridge __Hotfix_get_SolarSystemCount;
        private static DelegateBridge __Hotfix_set_SolarSystemCount;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsReceived", DataFormat=DataFormat.Default)]
        public bool IsReceived
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="CreateTime", DataFormat=DataFormat.TwosComplement)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ExpireTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long ExpireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="ItemList", DataFormat=DataFormat.Default)]
        public List<ProSimpleItemInfoWithCount> ItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="SolarSystemCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="ConfigId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

