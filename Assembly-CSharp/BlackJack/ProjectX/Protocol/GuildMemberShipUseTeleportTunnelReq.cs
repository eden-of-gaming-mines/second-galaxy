﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMemberShipUseTeleportTunnelReq")]
    public class GuildMemberShipUseTeleportTunnelReq : IExtensible
    {
        private ulong _SrcTeleportTunnelId;
        private ulong _DestTeleportTunnelId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SrcTeleportTunnelId;
        private static DelegateBridge __Hotfix_set_SrcTeleportTunnelId;
        private static DelegateBridge __Hotfix_get_DestTeleportTunnelId;
        private static DelegateBridge __Hotfix_set_DestTeleportTunnelId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SrcTeleportTunnelId", DataFormat=DataFormat.TwosComplement)]
        public ulong SrcTeleportTunnelId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DestTeleportTunnelId", DataFormat=DataFormat.TwosComplement)]
        public ulong DestTeleportTunnelId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

