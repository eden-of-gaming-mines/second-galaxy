﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SpaceStationLeaveForCelestialReq")]
    public class SpaceStationLeaveForCelestialReq : IExtensible
    {
        private ulong _ShipInstanceId;
        private int _DestType;
        private int _DestId;
        private int _DestSolarSystemId;
        private uint _DestSceneInstanceId;
        private ulong _GuildActionInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipInstanceId;
        private static DelegateBridge __Hotfix_set_ShipInstanceId;
        private static DelegateBridge __Hotfix_get_DestType;
        private static DelegateBridge __Hotfix_set_DestType;
        private static DelegateBridge __Hotfix_get_DestId;
        private static DelegateBridge __Hotfix_set_DestId;
        private static DelegateBridge __Hotfix_get_DestSolarSystemId;
        private static DelegateBridge __Hotfix_set_DestSolarSystemId;
        private static DelegateBridge __Hotfix_get_DestSceneInstanceId;
        private static DelegateBridge __Hotfix_set_DestSceneInstanceId;
        private static DelegateBridge __Hotfix_get_GuildActionInstanceId;
        private static DelegateBridge __Hotfix_set_GuildActionInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ShipInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="DestType", DataFormat=DataFormat.TwosComplement)]
        public int DestType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="DestId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="DestSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="DestSceneInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint DestSceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(6, IsRequired=false, Name="GuildActionInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong GuildActionInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

