﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProConfigFileMD5Info")]
    public class ProConfigFileMD5Info : IExtensible
    {
        private string _FileName;
        private string _MD5;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FileName;
        private static DelegateBridge __Hotfix_set_FileName;
        private static DelegateBridge __Hotfix_get_MD5;
        private static DelegateBridge __Hotfix_set_MD5;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="FileName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string FileName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="MD5", DataFormat=DataFormat.Default)]
        public string MD5
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

