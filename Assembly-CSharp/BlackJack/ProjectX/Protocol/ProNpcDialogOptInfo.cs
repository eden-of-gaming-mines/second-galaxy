﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProNpcDialogOptInfo")]
    public class ProNpcDialogOptInfo : IExtensible
    {
        private int _OptType;
        private readonly List<int> _ParamList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OptType;
        private static DelegateBridge __Hotfix_set_OptType;
        private static DelegateBridge __Hotfix_get_ParamList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="OptType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int OptType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="ParamList", DataFormat=DataFormat.TwosComplement)]
        public List<int> ParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

