﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMServerPlayerInfoQueryAck")]
    public class GMServerPlayerInfoQueryAck : IExtensible
    {
        private int _Result;
        private string _GameUserId;
        private int _QueryType;
        private readonly List<ProStoreItemInfo> _StoreItemList;
        private readonly List<ProStoredMailInfo> _MailInfoList;
        private readonly List<ProHiredCaptainInfo> _HiredCaptainList;
        private readonly List<ProHangerInfo> _HangerInfoList;
        private string _Name;
        private bool _IsPlayerOnline;
        private int _StartIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_GameUserId;
        private static DelegateBridge __Hotfix_set_GameUserId;
        private static DelegateBridge __Hotfix_get_QueryType;
        private static DelegateBridge __Hotfix_set_QueryType;
        private static DelegateBridge __Hotfix_get_StoreItemList;
        private static DelegateBridge __Hotfix_get_MailInfoList;
        private static DelegateBridge __Hotfix_get_HiredCaptainList;
        private static DelegateBridge __Hotfix_get_HangerInfoList;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_IsPlayerOnline;
        private static DelegateBridge __Hotfix_set_IsPlayerOnline;
        private static DelegateBridge __Hotfix_get_StartIndex;
        private static DelegateBridge __Hotfix_set_StartIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="GameUserId", DataFormat=DataFormat.Default)]
        public string GameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="QueryType", DataFormat=DataFormat.TwosComplement)]
        public int QueryType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="StoreItemList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemInfo> StoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="MailInfoList", DataFormat=DataFormat.Default)]
        public List<ProStoredMailInfo> MailInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="HiredCaptainList", DataFormat=DataFormat.Default)]
        public List<ProHiredCaptainInfo> HiredCaptainList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="HangerInfoList", DataFormat=DataFormat.Default)]
        public List<ProHangerInfo> HangerInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(""), ProtoMember(8, IsRequired=false, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(9, IsRequired=false, Name="IsPlayerOnline", DataFormat=DataFormat.Default)]
        public bool IsPlayerOnline
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="StartIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int StartIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

