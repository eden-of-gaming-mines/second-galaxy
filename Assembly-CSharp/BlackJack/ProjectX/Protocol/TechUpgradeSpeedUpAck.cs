﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TechUpgradeSpeedUpAck")]
    public class TechUpgradeSpeedUpAck : IExtensible
    {
        private int _Result;
        private ProTechUpgradeInfo _UpgradingTech;
        private int _UseItemId;
        private int _UseItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_UpgradingTech;
        private static DelegateBridge __Hotfix_set_UpgradingTech;
        private static DelegateBridge __Hotfix_get_UseItemId;
        private static DelegateBridge __Hotfix_set_UseItemId;
        private static DelegateBridge __Hotfix_get_UseItemCount;
        private static DelegateBridge __Hotfix_set_UseItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="UpgradingTech", DataFormat=DataFormat.Default)]
        public ProTechUpgradeInfo UpgradingTech
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="UseItemId", DataFormat=DataFormat.TwosComplement)]
        public int UseItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="UseItemCount", DataFormat=DataFormat.TwosComplement)]
        public int UseItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

