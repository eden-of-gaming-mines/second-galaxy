﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SolarSystemPlayerListAck")]
    public class SolarSystemPlayerListAck : IExtensible
    {
        private int _Result;
        private readonly List<ProPlayerSimplestInfo> _PlayerSimpleInfo;
        private int _SolarSystemPlayerCount;
        private int _StartIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_PlayerSimpleInfo;
        private static DelegateBridge __Hotfix_get_SolarSystemPlayerCount;
        private static DelegateBridge __Hotfix_set_SolarSystemPlayerCount;
        private static DelegateBridge __Hotfix_get_StartIndex;
        private static DelegateBridge __Hotfix_set_StartIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="PlayerSimpleInfo", DataFormat=DataFormat.Default)]
        public List<ProPlayerSimplestInfo> PlayerSimpleInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="SolarSystemPlayerCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemPlayerCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="StartIndex", DataFormat=DataFormat.TwosComplement)]
        public int StartIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

