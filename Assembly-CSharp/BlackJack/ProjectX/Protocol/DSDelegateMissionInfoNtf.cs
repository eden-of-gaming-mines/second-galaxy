﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSDelegateMissionInfoNtf")]
    public class DSDelegateMissionInfoNtf : IExtensible
    {
        private readonly List<ProDelegateMissionInfo> _MissionList;
        private readonly List<ProDelegateMissionInfo> _HistoryDelegateMissionList;
        private uint _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MissionList;
        private static DelegateBridge __Hotfix_get_HistoryDelegateMissionList;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="MissionList", DataFormat=DataFormat.Default)]
        public List<ProDelegateMissionInfo> MissionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="HistoryDelegateMissionList", DataFormat=DataFormat.Default)]
        public List<ProDelegateMissionInfo> HistoryDelegateMissionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

