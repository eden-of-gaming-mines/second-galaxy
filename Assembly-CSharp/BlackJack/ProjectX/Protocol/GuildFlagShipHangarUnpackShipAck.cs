﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarUnpackShipAck")]
    public class GuildFlagShipHangarUnpackShipAck : IExtensible
    {
        private int _Result;
        private ulong _InstanceId;
        private int _HangarShipIndex;
        private ulong _ShipSrcItemInsId;
        private ulong _ShipUnpackedItemInsId;
        private int _UnpackedItemIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_ShipSrcItemInsId;
        private static DelegateBridge __Hotfix_set_ShipSrcItemInsId;
        private static DelegateBridge __Hotfix_get_ShipUnpackedItemInsId;
        private static DelegateBridge __Hotfix_set_ShipUnpackedItemInsId;
        private static DelegateBridge __Hotfix_get_UnpackedItemIndex;
        private static DelegateBridge __Hotfix_set_UnpackedItemIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="ShipSrcItemInsId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong ShipSrcItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ShipUnpackedItemInsId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong ShipUnpackedItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="UnpackedItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int UnpackedItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

