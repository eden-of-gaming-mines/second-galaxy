﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFlagShipOptLogInfo")]
    public class ProGuildFlagShipOptLogInfo : IExtensible
    {
        private uint _SeqId;
        private int _LogType;
        private int _SolarSystemId;
        private readonly List<ProFormatStringParamInfo> _Content;
        private long _CreateTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SeqId;
        private static DelegateBridge __Hotfix_set_SeqId;
        private static DelegateBridge __Hotfix_get_LogType;
        private static DelegateBridge __Hotfix_set_LogType;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_Content;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SeqId", DataFormat=DataFormat.TwosComplement)]
        public uint SeqId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="LogType", DataFormat=DataFormat.TwosComplement)]
        public int LogType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="Content", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> Content
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="CreateTime", DataFormat=DataFormat.TwosComplement)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

