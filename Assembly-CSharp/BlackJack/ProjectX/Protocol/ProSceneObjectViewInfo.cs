﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSceneObjectViewInfo")]
    public class ProSceneObjectViewInfo : IExtensible
    {
        private int _SceneConfigId;
        private int _SceneObjectIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SceneConfigId;
        private static DelegateBridge __Hotfix_set_SceneConfigId;
        private static DelegateBridge __Hotfix_get_SceneObjectIndex;
        private static DelegateBridge __Hotfix_set_SceneObjectIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SceneConfigId", DataFormat=DataFormat.TwosComplement)]
        public int SceneConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SceneObjectIndex", DataFormat=DataFormat.TwosComplement)]
        public int SceneObjectIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

