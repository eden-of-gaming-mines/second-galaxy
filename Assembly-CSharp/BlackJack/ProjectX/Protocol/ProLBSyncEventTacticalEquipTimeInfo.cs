﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventTacticalEquipTimeInfo")]
    public class ProLBSyncEventTacticalEquipTimeInfo : IExtensible
    {
        private uint _EffectStartTime;
        private uint _EffectEndTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EffectStartTime;
        private static DelegateBridge __Hotfix_set_EffectStartTime;
        private static DelegateBridge __Hotfix_get_EffectEndTime;
        private static DelegateBridge __Hotfix_set_EffectEndTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="EffectStartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint EffectStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="EffectEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint EffectEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

