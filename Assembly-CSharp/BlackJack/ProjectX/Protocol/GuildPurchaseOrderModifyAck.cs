﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildPurchaseOrderModifyAck")]
    public class GuildPurchaseOrderModifyAck : IExtensible
    {
        private int _Result;
        private ProGuildPurchaseOrderInfo _ModifiedOrder;
        private bool _IsCancel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ModifiedOrder;
        private static DelegateBridge __Hotfix_set_ModifiedOrder;
        private static DelegateBridge __Hotfix_get_IsCancel;
        private static DelegateBridge __Hotfix_set_IsCancel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="ModifiedOrder", DataFormat=DataFormat.Default)]
        public ProGuildPurchaseOrderInfo ModifiedOrder
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="IsCancel", DataFormat=DataFormat.Default)]
        public bool IsCancel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

