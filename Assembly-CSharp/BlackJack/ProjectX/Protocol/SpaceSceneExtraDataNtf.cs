﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SpaceSceneExtraDataNtf")]
    public class SpaceSceneExtraDataNtf : IExtensible
    {
        private uint _SceneInstanceId;
        private readonly List<ProSceneGuildBuildingExtraDataInfo> _BuildingList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SceneInstanceId;
        private static DelegateBridge __Hotfix_set_SceneInstanceId;
        private static DelegateBridge __Hotfix_get_BuildingList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SceneInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint SceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="BuildingList", DataFormat=DataFormat.Default)]
        public List<ProSceneGuildBuildingExtraDataInfo> BuildingList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

