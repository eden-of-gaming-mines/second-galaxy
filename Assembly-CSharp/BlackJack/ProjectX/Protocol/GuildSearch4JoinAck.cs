﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildSearch4JoinAck")]
    public class GuildSearch4JoinAck : IExtensible
    {
        private int _Result;
        private readonly List<ProGuildSimplestInfo> _GuildList;
        private readonly List<uint> _SelfApplyList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_GuildList;
        private static DelegateBridge __Hotfix_get_SelfApplyList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="GuildList", DataFormat=DataFormat.Default)]
        public List<ProGuildSimplestInfo> GuildList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="SelfApplyList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> SelfApplyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

