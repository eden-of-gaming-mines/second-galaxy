﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CharChipSetReq")]
    public class CharChipSetReq : IExtensible
    {
        private int _SlotIndex;
        private int _ItemStoreIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SlotIndex;
        private static DelegateBridge __Hotfix_set_SlotIndex;
        private static DelegateBridge __Hotfix_get_ItemStoreIndex;
        private static DelegateBridge __Hotfix_set_ItemStoreIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SlotIndex", DataFormat=DataFormat.TwosComplement)]
        public int SlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ItemStoreIndex", DataFormat=DataFormat.TwosComplement)]
        public int ItemStoreIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

