﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipAddWeaponEquip2SlotAck")]
    public class HangarShipAddWeaponEquip2SlotAck : IExtensible
    {
        private int _Result;
        private int _HangarShipIndex;
        private int _EquipSlotType;
        private int _EquipSlotIndex;
        private int _SrcStoreItemIndex;
        private int _DestStoreItemIndex;
        private ulong _DestStoreItemInstanceId;
        private int _AddWeaponEquipCount;
        private int _RemoveWeaponEquipStoreItemIndex;
        private ulong _RemoveWeaponEquipStoreItemInstanceId;
        private int _RemoveWeaponEquipCount;
        private ProAmmoStoreItemInfo _RemoveAmmoInfo;
        private int _ErrorCode;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_EquipSlotType;
        private static DelegateBridge __Hotfix_set_EquipSlotType;
        private static DelegateBridge __Hotfix_get_EquipSlotIndex;
        private static DelegateBridge __Hotfix_set_EquipSlotIndex;
        private static DelegateBridge __Hotfix_get_SrcStoreItemIndex;
        private static DelegateBridge __Hotfix_set_SrcStoreItemIndex;
        private static DelegateBridge __Hotfix_get_DestStoreItemIndex;
        private static DelegateBridge __Hotfix_set_DestStoreItemIndex;
        private static DelegateBridge __Hotfix_get_DestStoreItemInstanceId;
        private static DelegateBridge __Hotfix_set_DestStoreItemInstanceId;
        private static DelegateBridge __Hotfix_get_AddWeaponEquipCount;
        private static DelegateBridge __Hotfix_set_AddWeaponEquipCount;
        private static DelegateBridge __Hotfix_get_RemoveWeaponEquipStoreItemIndex;
        private static DelegateBridge __Hotfix_set_RemoveWeaponEquipStoreItemIndex;
        private static DelegateBridge __Hotfix_get_RemoveWeaponEquipStoreItemInstanceId;
        private static DelegateBridge __Hotfix_set_RemoveWeaponEquipStoreItemInstanceId;
        private static DelegateBridge __Hotfix_get_RemoveWeaponEquipCount;
        private static DelegateBridge __Hotfix_set_RemoveWeaponEquipCount;
        private static DelegateBridge __Hotfix_get_RemoveAmmoInfo;
        private static DelegateBridge __Hotfix_set_RemoveAmmoInfo;
        private static DelegateBridge __Hotfix_get_ErrorCode;
        private static DelegateBridge __Hotfix_set_ErrorCode;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="EquipSlotType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int EquipSlotType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="EquipSlotIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int EquipSlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="SrcStoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int SrcStoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="DestStoreItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DestStoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(7, IsRequired=false, Name="DestStoreItemInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong DestStoreItemInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="AddWeaponEquipCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AddWeaponEquipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="RemoveWeaponEquipStoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int RemoveWeaponEquipStoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="RemoveWeaponEquipStoreItemInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong RemoveWeaponEquipStoreItemInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(11, IsRequired=false, Name="RemoveWeaponEquipCount", DataFormat=DataFormat.TwosComplement)]
        public int RemoveWeaponEquipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(12, IsRequired=false, Name="RemoveAmmoInfo", DataFormat=DataFormat.Default)]
        public ProAmmoStoreItemInfo RemoveAmmoInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(13, IsRequired=false, Name="ErrorCode", DataFormat=DataFormat.TwosComplement)]
        public int ErrorCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

