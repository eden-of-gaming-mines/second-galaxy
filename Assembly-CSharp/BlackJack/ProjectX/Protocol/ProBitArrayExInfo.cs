﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProBitArrayExInfo")]
    public class ProBitArrayExInfo : IExtensible
    {
        private int _IndexCapacity;
        private int _MaxIndex;
        private byte[] _Array;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IndexCapacity;
        private static DelegateBridge __Hotfix_set_IndexCapacity;
        private static DelegateBridge __Hotfix_get_MaxIndex;
        private static DelegateBridge __Hotfix_set_MaxIndex;
        private static DelegateBridge __Hotfix_get_Array;
        private static DelegateBridge __Hotfix_set_Array;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IndexCapacity", DataFormat=DataFormat.TwosComplement)]
        public int IndexCapacity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="MaxIndex", DataFormat=DataFormat.TwosComplement)]
        public int MaxIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Array", DataFormat=DataFormat.Default)]
        public byte[] Array
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

