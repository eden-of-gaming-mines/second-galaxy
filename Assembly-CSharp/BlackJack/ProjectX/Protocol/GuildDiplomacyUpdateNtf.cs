﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildDiplomacyUpdateNtf")]
    public class GuildDiplomacyUpdateNtf : IExtensible
    {
        private string _PlayerGameUserId;
        private uint _GuildId;
        private uint _AllienceId;
        private int _OptType;
        private ProPlayerSimplestInfo _PlayerInfo;
        private ProGuildSimplestInfo _GuildInfo;
        private ProAllianceBasicInfo _AllianceInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerGameUserId;
        private static DelegateBridge __Hotfix_set_PlayerGameUserId;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_AllienceId;
        private static DelegateBridge __Hotfix_set_AllienceId;
        private static DelegateBridge __Hotfix_get_OptType;
        private static DelegateBridge __Hotfix_set_OptType;
        private static DelegateBridge __Hotfix_get_PlayerInfo;
        private static DelegateBridge __Hotfix_set_PlayerInfo;
        private static DelegateBridge __Hotfix_get_GuildInfo;
        private static DelegateBridge __Hotfix_set_GuildInfo;
        private static DelegateBridge __Hotfix_get_AllianceInfo;
        private static DelegateBridge __Hotfix_set_AllianceInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(""), ProtoMember(1, IsRequired=false, Name="PlayerGameUserId", DataFormat=DataFormat.Default)]
        public string PlayerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="AllienceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllienceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="OptType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int OptType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="PlayerInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProPlayerSimplestInfo PlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(6, IsRequired=false, Name="GuildInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimplestInfo GuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="AllianceInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProAllianceBasicInfo AllianceInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

