﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBattleGuildBuildingSimpleInfo")]
    public class ProGuildBattleGuildBuildingSimpleInfo : IExtensible
    {
        private int _BuildingType;
        private float _DefenderBuildingShieldPercentCurr;
        private float _DefenderBuildingArmorPercentCurr;
        private float _AttackerBuildingShieldPercentCurr;
        private float _AttackerBuildingArmorPercentCurr;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BuildingType;
        private static DelegateBridge __Hotfix_set_BuildingType;
        private static DelegateBridge __Hotfix_get_DefenderBuildingShieldPercentCurr;
        private static DelegateBridge __Hotfix_set_DefenderBuildingShieldPercentCurr;
        private static DelegateBridge __Hotfix_get_DefenderBuildingArmorPercentCurr;
        private static DelegateBridge __Hotfix_set_DefenderBuildingArmorPercentCurr;
        private static DelegateBridge __Hotfix_get_AttackerBuildingShieldPercentCurr;
        private static DelegateBridge __Hotfix_set_AttackerBuildingShieldPercentCurr;
        private static DelegateBridge __Hotfix_get_AttackerBuildingArmorPercentCurr;
        private static DelegateBridge __Hotfix_set_AttackerBuildingArmorPercentCurr;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BuildingType", DataFormat=DataFormat.TwosComplement)]
        public int BuildingType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DefenderBuildingShieldPercentCurr", DataFormat=DataFormat.FixedSize)]
        public float DefenderBuildingShieldPercentCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DefenderBuildingArmorPercentCurr", DataFormat=DataFormat.FixedSize)]
        public float DefenderBuildingArmorPercentCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="AttackerBuildingShieldPercentCurr", DataFormat=DataFormat.FixedSize)]
        public float AttackerBuildingShieldPercentCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="AttackerBuildingArmorPercentCurr", DataFormat=DataFormat.FixedSize)]
        public float AttackerBuildingArmorPercentCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

