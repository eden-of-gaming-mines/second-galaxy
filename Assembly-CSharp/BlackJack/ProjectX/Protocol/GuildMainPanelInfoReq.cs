﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMainPanelInfoReq")]
    public class GuildMainPanelInfoReq : IExtensible
    {
        private uint _GuildId;
        private uint _BasicInfoVersion;
        private uint _SimpleRuntimeInfoVersion;
        private uint _DynamicInfoVersion;
        private uint _MomentsBriefInfoVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_BasicInfoVersion;
        private static DelegateBridge __Hotfix_set_BasicInfoVersion;
        private static DelegateBridge __Hotfix_get_SimpleRuntimeInfoVersion;
        private static DelegateBridge __Hotfix_set_SimpleRuntimeInfoVersion;
        private static DelegateBridge __Hotfix_get_DynamicInfoVersion;
        private static DelegateBridge __Hotfix_set_DynamicInfoVersion;
        private static DelegateBridge __Hotfix_get_MomentsBriefInfoVersion;
        private static DelegateBridge __Hotfix_set_MomentsBriefInfoVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BasicInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public uint BasicInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SimpleRuntimeInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public uint SimpleRuntimeInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="DynamicInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public uint DynamicInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MomentsBriefInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public uint MomentsBriefInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

