﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildSolarSystemInfo")]
    public class ProGuildSolarSystemInfo : IExtensible
    {
        private int _SolarSystemId;
        private int _FlourishValue;
        private readonly List<ProGuildBuildingInfo> _BuildingList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_FlourishValue;
        private static DelegateBridge __Hotfix_set_FlourishValue;
        private static DelegateBridge __Hotfix_get_BuildingList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="FlourishValue", DataFormat=DataFormat.TwosComplement)]
        public int FlourishValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="BuildingList", DataFormat=DataFormat.Default)]
        public List<ProGuildBuildingInfo> BuildingList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

