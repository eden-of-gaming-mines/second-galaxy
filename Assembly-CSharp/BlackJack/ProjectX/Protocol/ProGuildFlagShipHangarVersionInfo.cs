﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFlagShipHangarVersionInfo")]
    public class ProGuildFlagShipHangarVersionInfo : IExtensible
    {
        private int _SolarSystemId;
        private int _BasicInfoVersion;
        private int _ShipHangarSlotListVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_BasicInfoVersion;
        private static DelegateBridge __Hotfix_set_BasicInfoVersion;
        private static DelegateBridge __Hotfix_get_ShipHangarSlotListVersion;
        private static DelegateBridge __Hotfix_set_ShipHangarSlotListVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BasicInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public int BasicInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipHangarSlotListVersion", DataFormat=DataFormat.TwosComplement)]
        public int ShipHangarSlotListVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

