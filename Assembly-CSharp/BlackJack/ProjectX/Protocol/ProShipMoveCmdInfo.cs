﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipMoveCmdInfo")]
    public class ProShipMoveCmdInfo : IExtensible
    {
        private int _MoveCmdType;
        private ProVectorDouble _VectorParam;
        private uint _TargetObjId;
        private uint _RaduisParam;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MoveCmdType;
        private static DelegateBridge __Hotfix_set_MoveCmdType;
        private static DelegateBridge __Hotfix_get_VectorParam;
        private static DelegateBridge __Hotfix_set_VectorParam;
        private static DelegateBridge __Hotfix_get_TargetObjId;
        private static DelegateBridge __Hotfix_set_TargetObjId;
        private static DelegateBridge __Hotfix_get_RaduisParam;
        private static DelegateBridge __Hotfix_set_RaduisParam;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="MoveCmdType", DataFormat=DataFormat.TwosComplement)]
        public int MoveCmdType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="VectorParam", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProVectorDouble VectorParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="TargetObjId", DataFormat=DataFormat.TwosComplement)]
        public uint TargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="RaduisParam", DataFormat=DataFormat.TwosComplement)]
        public uint RaduisParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

