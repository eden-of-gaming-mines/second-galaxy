﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildSolarSystemBuildingInfoAck")]
    public class GuildSolarSystemBuildingInfoAck : IExtensible
    {
        private int _Result;
        private int _SolarSystemId;
        private int _BuildingDataVersion;
        private readonly List<ProGuildBuildingInfo> _BuildingList;
        private readonly List<ProGuildBuildingExInfo> _BuildingExInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_BuildingDataVersion;
        private static DelegateBridge __Hotfix_set_BuildingDataVersion;
        private static DelegateBridge __Hotfix_get_BuildingList;
        private static DelegateBridge __Hotfix_get_BuildingExInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="BuildingDataVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BuildingDataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="BuildingList", DataFormat=DataFormat.Default)]
        public List<ProGuildBuildingInfo> BuildingList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="BuildingExInfo", DataFormat=DataFormat.Default)]
        public List<ProGuildBuildingExInfo> BuildingExInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

