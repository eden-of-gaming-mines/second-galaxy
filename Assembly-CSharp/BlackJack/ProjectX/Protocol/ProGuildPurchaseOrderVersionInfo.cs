﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildPurchaseOrderVersionInfo")]
    public class ProGuildPurchaseOrderVersionInfo : IExtensible
    {
        private ulong _OrderInstanceID;
        private int _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OrderInstanceID;
        private static DelegateBridge __Hotfix_set_OrderInstanceID;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="OrderInstanceID", DataFormat=DataFormat.TwosComplement)]
        public ulong OrderInstanceID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public int Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

