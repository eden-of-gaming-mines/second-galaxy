﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GetGuildProductionInfoReq")]
    public class GetGuildProductionInfoReq : IExtensible
    {
        private uint _ProdcutionDataVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ProdcutionDataVersion;
        private static DelegateBridge __Hotfix_set_ProdcutionDataVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ProdcutionDataVersion", DataFormat=DataFormat.TwosComplement)]
        public uint ProdcutionDataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

