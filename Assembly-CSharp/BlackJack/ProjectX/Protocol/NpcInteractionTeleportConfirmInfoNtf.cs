﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="NpcInteractionTeleportConfirmInfoNtf")]
    public class NpcInteractionTeleportConfirmInfoNtf : IExtensible
    {
        private uint _NpcObjId;
        private int _TeleportConfirmInfoType;
        private int _WormholeStarGroupId;
        private bool _WormholeIsRare;
        private int _WormholePlayerCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NpcObjId;
        private static DelegateBridge __Hotfix_set_NpcObjId;
        private static DelegateBridge __Hotfix_get_TeleportConfirmInfoType;
        private static DelegateBridge __Hotfix_set_TeleportConfirmInfoType;
        private static DelegateBridge __Hotfix_get_WormholeStarGroupId;
        private static DelegateBridge __Hotfix_set_WormholeStarGroupId;
        private static DelegateBridge __Hotfix_get_WormholeIsRare;
        private static DelegateBridge __Hotfix_set_WormholeIsRare;
        private static DelegateBridge __Hotfix_get_WormholePlayerCount;
        private static DelegateBridge __Hotfix_set_WormholePlayerCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="NpcObjId", DataFormat=DataFormat.TwosComplement)]
        public uint NpcObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="TeleportConfirmInfoType", DataFormat=DataFormat.TwosComplement)]
        public int TeleportConfirmInfoType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="WormholeStarGroupId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int WormholeStarGroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="WormholeIsRare", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool WormholeIsRare
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(12, IsRequired=false, Name="WormholePlayerCount", DataFormat=DataFormat.TwosComplement)]
        public int WormholePlayerCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

