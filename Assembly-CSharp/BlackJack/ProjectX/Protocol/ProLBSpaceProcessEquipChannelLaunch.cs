﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessEquipChannelLaunch")]
    public class ProLBSpaceProcessEquipChannelLaunch : IExtensible
    {
        private uint _InstanceId;
        private uint _StartTime;
        private uint _ChargeTime;
        private uint _LaunchCostEnergy;
        private uint _LoopCostEnergy;
        private uint _LoopCD;
        private uint _LoopCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_LaunchCostEnergy;
        private static DelegateBridge __Hotfix_set_LaunchCostEnergy;
        private static DelegateBridge __Hotfix_get_LoopCostEnergy;
        private static DelegateBridge __Hotfix_set_LoopCostEnergy;
        private static DelegateBridge __Hotfix_get_LoopCD;
        private static DelegateBridge __Hotfix_set_LoopCD;
        private static DelegateBridge __Hotfix_get_LoopCount;
        private static DelegateBridge __Hotfix_set_LoopCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public uint StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ChargeTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="LaunchCostEnergy", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LaunchCostEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="LoopCostEnergy", DataFormat=DataFormat.TwosComplement)]
        public uint LoopCostEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="LoopCD", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LoopCD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="LoopCount", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LoopCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

