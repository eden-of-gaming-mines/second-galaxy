﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProQuestRewardInfo")]
    public class ProQuestRewardInfo : IExtensible
    {
        private int _RewardType;
        private readonly List<int> _RewardParamList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_RewardType;
        private static DelegateBridge __Hotfix_set_RewardType;
        private static DelegateBridge __Hotfix_get_RewardParamList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="RewardType", DataFormat=DataFormat.TwosComplement)]
        public int RewardType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="RewardParamList", DataFormat=DataFormat.TwosComplement)]
        public List<int> RewardParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

