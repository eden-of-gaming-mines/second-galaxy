﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProFightOperationCmdInfo")]
    public class ProFightOperationCmdInfo : IExtensible
    {
        private int _OperationCmdType;
        private uint _TargetObjId;
        private int _ShipEquipSlotType;
        private int _ShipEquipSlotIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OperationCmdType;
        private static DelegateBridge __Hotfix_set_OperationCmdType;
        private static DelegateBridge __Hotfix_get_TargetObjId;
        private static DelegateBridge __Hotfix_set_TargetObjId;
        private static DelegateBridge __Hotfix_get_ShipEquipSlotType;
        private static DelegateBridge __Hotfix_set_ShipEquipSlotType;
        private static DelegateBridge __Hotfix_get_ShipEquipSlotIndex;
        private static DelegateBridge __Hotfix_set_ShipEquipSlotIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="OperationCmdType", DataFormat=DataFormat.TwosComplement)]
        public int OperationCmdType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="TargetObjId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint TargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ShipEquipSlotType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ShipEquipSlotType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="ShipEquipSlotIndex", DataFormat=DataFormat.TwosComplement)]
        public int ShipEquipSlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

