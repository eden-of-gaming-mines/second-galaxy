﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildTradeTransportInfoListAck")]
    public class GuildTradeTransportInfoListAck : IExtensible
    {
        private int _Result;
        private readonly List<ProGuildTradeTransportInfo> _GuildTradeTransportInfos;
        private readonly List<ulong> _NeedRemoveTransportInstanceIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_GuildTradeTransportInfos;
        private static DelegateBridge __Hotfix_get_NeedRemoveTransportInstanceIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="GuildTradeTransportInfos", DataFormat=DataFormat.Default)]
        public List<ProGuildTradeTransportInfo> GuildTradeTransportInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="NeedRemoveTransportInstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> NeedRemoveTransportInstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

