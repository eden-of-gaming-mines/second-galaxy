﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StarFieldSolarSystemPoolCountInfoAck")]
    public class StarFieldSolarSystemPoolCountInfoAck : IExtensible
    {
        private int _SolarSystemId;
        private int _QuestPoolCount;
        private int _DelegateMissionPoolCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_QuestPoolCount;
        private static DelegateBridge __Hotfix_set_QuestPoolCount;
        private static DelegateBridge __Hotfix_get_DelegateMissionPoolCount;
        private static DelegateBridge __Hotfix_set_DelegateMissionPoolCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="QuestPoolCount", DataFormat=DataFormat.TwosComplement)]
        public int QuestPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DelegateMissionPoolCount", DataFormat=DataFormat.TwosComplement)]
        public int DelegateMissionPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

