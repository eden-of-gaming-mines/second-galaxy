﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="LostReportGetReq")]
    public class LostReportGetReq : IExtensible
    {
        private ulong _LostReportId;
        private int _LostReportType;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LostReportId;
        private static DelegateBridge __Hotfix_set_LostReportId;
        private static DelegateBridge __Hotfix_get_LostReportType;
        private static DelegateBridge __Hotfix_set_LostReportType;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LostReportId", DataFormat=DataFormat.TwosComplement)]
        public ulong LostReportId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="LostReportType", DataFormat=DataFormat.TwosComplement)]
        public int LostReportType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

