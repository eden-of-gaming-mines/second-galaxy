﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProPlayerBasicStaticInfo")]
    public class ProPlayerBasicStaticInfo : IExtensible
    {
        private string _CharactorPlayerName;
        private string _GameUserId;
        private int _Gender;
        private int _Age;
        private int _MotherLand;
        private int _AvatarId;
        private int _Profession;
        private long _NextDailyRefreshTime;
        private long _CreateTime;
        private string _PlatformAuthId;
        private int _DisconnectTimeoutTime;
        private long _NextWeeklyRefreshTime;
        private string _ServerDNName;
        private int _VIPLevel;
        private string _GuildName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CharactorPlayerName;
        private static DelegateBridge __Hotfix_set_CharactorPlayerName;
        private static DelegateBridge __Hotfix_get_GameUserId;
        private static DelegateBridge __Hotfix_set_GameUserId;
        private static DelegateBridge __Hotfix_get_Gender;
        private static DelegateBridge __Hotfix_set_Gender;
        private static DelegateBridge __Hotfix_get_Age;
        private static DelegateBridge __Hotfix_set_Age;
        private static DelegateBridge __Hotfix_get_MotherLand;
        private static DelegateBridge __Hotfix_set_MotherLand;
        private static DelegateBridge __Hotfix_get_AvatarId;
        private static DelegateBridge __Hotfix_set_AvatarId;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_NextDailyRefreshTime;
        private static DelegateBridge __Hotfix_set_NextDailyRefreshTime;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_get_PlatformAuthId;
        private static DelegateBridge __Hotfix_set_PlatformAuthId;
        private static DelegateBridge __Hotfix_get_DisconnectTimeoutTime;
        private static DelegateBridge __Hotfix_set_DisconnectTimeoutTime;
        private static DelegateBridge __Hotfix_get_NextWeeklyRefreshTime;
        private static DelegateBridge __Hotfix_set_NextWeeklyRefreshTime;
        private static DelegateBridge __Hotfix_get_ServerDNName;
        private static DelegateBridge __Hotfix_set_ServerDNName;
        private static DelegateBridge __Hotfix_get_VIPLevel;
        private static DelegateBridge __Hotfix_set_VIPLevel;
        private static DelegateBridge __Hotfix_get_GuildName;
        private static DelegateBridge __Hotfix_set_GuildName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CharactorPlayerName", DataFormat=DataFormat.Default)]
        public string CharactorPlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="GameUserId", DataFormat=DataFormat.Default)]
        public string GameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Gender", DataFormat=DataFormat.TwosComplement)]
        public int Gender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Age", DataFormat=DataFormat.TwosComplement)]
        public int Age
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MotherLand", DataFormat=DataFormat.TwosComplement)]
        public int MotherLand
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="AvatarId", DataFormat=DataFormat.TwosComplement)]
        public int AvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(8, IsRequired=false, Name="NextDailyRefreshTime", DataFormat=DataFormat.TwosComplement)]
        public long NextDailyRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="CreateTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="PlatformAuthId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlatformAuthId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(11, IsRequired=false, Name="DisconnectTimeoutTime", DataFormat=DataFormat.TwosComplement)]
        public int DisconnectTimeoutTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="NextWeeklyRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextWeeklyRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(13, IsRequired=false, Name="ServerDNName", DataFormat=DataFormat.Default)]
        public string ServerDNName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(14, IsRequired=false, Name="VIPLevel", DataFormat=DataFormat.TwosComplement)]
        public int VIPLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(15, IsRequired=false, Name="GuildName", DataFormat=DataFormat.Default)]
        public string GuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

