﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AuctionItemBuyReq")]
    public class AuctionItemBuyReq : IExtensible
    {
        private int _AuctionItemId;
        private int _BuyCount;
        private long _TradeMoneyForecast;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AuctionItemId;
        private static DelegateBridge __Hotfix_set_AuctionItemId;
        private static DelegateBridge __Hotfix_get_BuyCount;
        private static DelegateBridge __Hotfix_set_BuyCount;
        private static DelegateBridge __Hotfix_get_TradeMoneyForecast;
        private static DelegateBridge __Hotfix_set_TradeMoneyForecast;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="AuctionItemId", DataFormat=DataFormat.TwosComplement)]
        public int AuctionItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="BuyCount", DataFormat=DataFormat.TwosComplement)]
        public int BuyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="TradeMoneyForecast", DataFormat=DataFormat.TwosComplement)]
        public long TradeMoneyForecast
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

