﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerGuildInfoInitAck")]
    public class PlayerGuildInfoInitAck : IExtensible
    {
        private ProGuildDynamicInfo _GuildDynamicInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GuildDynamicInfo;
        private static DelegateBridge __Hotfix_set_GuildDynamicInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="GuildDynamicInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildDynamicInfo GuildDynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

