﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BattlePassRefreshNtf")]
    public class BattlePassRefreshNtf : IExtensible
    {
        private int _PeriodId;
        private int _ChallangeGroupIndex;
        private long _PeriodStartTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PeriodId;
        private static DelegateBridge __Hotfix_set_PeriodId;
        private static DelegateBridge __Hotfix_get_ChallangeGroupIndex;
        private static DelegateBridge __Hotfix_set_ChallangeGroupIndex;
        private static DelegateBridge __Hotfix_get_PeriodStartTime;
        private static DelegateBridge __Hotfix_set_PeriodStartTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="PeriodId", DataFormat=DataFormat.TwosComplement)]
        public int PeriodId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ChallangeGroupIndex", DataFormat=DataFormat.TwosComplement)]
        public int ChallangeGroupIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="PeriodStartTime", DataFormat=DataFormat.TwosComplement)]
        public long PeriodStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

