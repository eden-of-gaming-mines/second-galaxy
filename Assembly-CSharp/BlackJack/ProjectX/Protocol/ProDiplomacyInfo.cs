﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDiplomacyInfo")]
    public class ProDiplomacyInfo : IExtensible
    {
        private readonly List<string> _FriendlyPlayer;
        private readonly List<string> _EnemyPlayer;
        private readonly List<uint> _FriendlyGuild;
        private readonly List<uint> _EnemyGuild;
        private readonly List<uint> _FriendlyAllience;
        private readonly List<uint> _EnemyAllience;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FriendlyPlayer;
        private static DelegateBridge __Hotfix_get_EnemyPlayer;
        private static DelegateBridge __Hotfix_get_FriendlyGuild;
        private static DelegateBridge __Hotfix_get_EnemyGuild;
        private static DelegateBridge __Hotfix_get_FriendlyAllience;
        private static DelegateBridge __Hotfix_get_EnemyAllience;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="FriendlyPlayer", DataFormat=DataFormat.Default)]
        public List<string> FriendlyPlayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="EnemyPlayer", DataFormat=DataFormat.Default)]
        public List<string> EnemyPlayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="FriendlyGuild", DataFormat=DataFormat.TwosComplement)]
        public List<uint> FriendlyGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="EnemyGuild", DataFormat=DataFormat.TwosComplement)]
        public List<uint> EnemyGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="FriendlyAllience", DataFormat=DataFormat.TwosComplement)]
        public List<uint> FriendlyAllience
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="EnemyAllience", DataFormat=DataFormat.TwosComplement)]
        public List<uint> EnemyAllience
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

