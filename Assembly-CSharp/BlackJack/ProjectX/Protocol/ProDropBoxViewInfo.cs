﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDropBoxViewInfo")]
    public class ProDropBoxViewInfo : IExtensible
    {
        private uint _SrcObjId;
        private string _SrcTargetName;
        private int _SrcNPCShipConfId;
        private readonly List<ProItemInfo> _DropItemList;
        private int _SrcHiredCaptainFirstNameId;
        private int _SrcHiredCaptainLastNameId;
        private int _SrcHiredCaptainGrandFaction;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SrcObjId;
        private static DelegateBridge __Hotfix_set_SrcObjId;
        private static DelegateBridge __Hotfix_get_SrcTargetName;
        private static DelegateBridge __Hotfix_set_SrcTargetName;
        private static DelegateBridge __Hotfix_get_SrcNPCShipConfId;
        private static DelegateBridge __Hotfix_set_SrcNPCShipConfId;
        private static DelegateBridge __Hotfix_get_DropItemList;
        private static DelegateBridge __Hotfix_get_SrcHiredCaptainFirstNameId;
        private static DelegateBridge __Hotfix_set_SrcHiredCaptainFirstNameId;
        private static DelegateBridge __Hotfix_get_SrcHiredCaptainLastNameId;
        private static DelegateBridge __Hotfix_set_SrcHiredCaptainLastNameId;
        private static DelegateBridge __Hotfix_get_SrcHiredCaptainGrandFaction;
        private static DelegateBridge __Hotfix_set_SrcHiredCaptainGrandFaction;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SrcObjId", DataFormat=DataFormat.TwosComplement)]
        public uint SrcObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SrcTargetName", DataFormat=DataFormat.Default)]
        public string SrcTargetName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SrcNPCShipConfId", DataFormat=DataFormat.TwosComplement)]
        public int SrcNPCShipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="DropItemList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> DropItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SrcHiredCaptainFirstNameId", DataFormat=DataFormat.TwosComplement)]
        public int SrcHiredCaptainFirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SrcHiredCaptainLastNameId", DataFormat=DataFormat.TwosComplement)]
        public int SrcHiredCaptainLastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SrcHiredCaptainGrandFaction", DataFormat=DataFormat.TwosComplement)]
        public int SrcHiredCaptainGrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

