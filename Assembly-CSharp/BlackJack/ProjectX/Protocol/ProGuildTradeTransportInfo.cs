﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildTradeTransportInfo")]
    public class ProGuildTradeTransportInfo : IExtensible
    {
        private ulong _InstanceId;
        private int _SolarSystemId;
        private ProStarMapGuildSimpleInfo _GuildInfo;
        private int _TradePortLevel;
        private int _ConfigId;
        private int _ItemType;
        private int _Count;
        private long _Price;
        private ulong _PurchaseInstanceId;
        private int _PurchaseSolarSystemId;
        private ProStarMapGuildSimpleInfo _PurchaseGuildInfo;
        private int _CurrJumpDist;
        private int _TotalJumpDist;
        private int _CurrSolarSystem;
        private uint _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_GuildInfo;
        private static DelegateBridge __Hotfix_set_GuildInfo;
        private static DelegateBridge __Hotfix_get_TradePortLevel;
        private static DelegateBridge __Hotfix_set_TradePortLevel;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_Count;
        private static DelegateBridge __Hotfix_set_Count;
        private static DelegateBridge __Hotfix_get_Price;
        private static DelegateBridge __Hotfix_set_Price;
        private static DelegateBridge __Hotfix_get_PurchaseInstanceId;
        private static DelegateBridge __Hotfix_set_PurchaseInstanceId;
        private static DelegateBridge __Hotfix_get_PurchaseSolarSystemId;
        private static DelegateBridge __Hotfix_set_PurchaseSolarSystemId;
        private static DelegateBridge __Hotfix_get_PurchaseGuildInfo;
        private static DelegateBridge __Hotfix_set_PurchaseGuildInfo;
        private static DelegateBridge __Hotfix_get_CurrJumpDist;
        private static DelegateBridge __Hotfix_set_CurrJumpDist;
        private static DelegateBridge __Hotfix_get_TotalJumpDist;
        private static DelegateBridge __Hotfix_set_TotalJumpDist;
        private static DelegateBridge __Hotfix_get_CurrSolarSystem;
        private static DelegateBridge __Hotfix_set_CurrSolarSystem;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="GuildInfo", DataFormat=DataFormat.Default)]
        public ProStarMapGuildSimpleInfo GuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="TradePortLevel", DataFormat=DataFormat.TwosComplement)]
        public int TradePortLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ConfigId", DataFormat=DataFormat.TwosComplement)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ItemType", DataFormat=DataFormat.TwosComplement)]
        public int ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Count", DataFormat=DataFormat.TwosComplement)]
        public int Count
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Price", DataFormat=DataFormat.TwosComplement)]
        public long Price
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="PurchaseInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong PurchaseInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="PurchaseSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int PurchaseSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="PurchaseGuildInfo", DataFormat=DataFormat.Default)]
        public ProStarMapGuildSimpleInfo PurchaseGuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="CurrJumpDist", DataFormat=DataFormat.TwosComplement)]
        public int CurrJumpDist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="TotalJumpDist", DataFormat=DataFormat.TwosComplement)]
        public int TotalJumpDist
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="CurrSolarSystem", DataFormat=DataFormat.TwosComplement)]
        public int CurrSolarSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

