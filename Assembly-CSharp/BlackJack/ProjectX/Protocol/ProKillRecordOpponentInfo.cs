﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProKillRecordOpponentInfo")]
    public class ProKillRecordOpponentInfo : IExtensible
    {
        private string _PlayerName;
        private int _CaptainLastNameId;
        private int _CaptainFirstNameId;
        private int _PlayerAvatarId;
        private int _ShipId;
        private string _GuildCodeName;
        private string _AllianceName;
        private string _PlayerGameUserId;
        private int _Level;
        private int _Profession;
        private uint _GuildId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerName;
        private static DelegateBridge __Hotfix_set_PlayerName;
        private static DelegateBridge __Hotfix_get_CaptainLastNameId;
        private static DelegateBridge __Hotfix_set_CaptainLastNameId;
        private static DelegateBridge __Hotfix_get_CaptainFirstNameId;
        private static DelegateBridge __Hotfix_set_CaptainFirstNameId;
        private static DelegateBridge __Hotfix_get_PlayerAvatarId;
        private static DelegateBridge __Hotfix_set_PlayerAvatarId;
        private static DelegateBridge __Hotfix_get_ShipId;
        private static DelegateBridge __Hotfix_set_ShipId;
        private static DelegateBridge __Hotfix_get_GuildCodeName;
        private static DelegateBridge __Hotfix_set_GuildCodeName;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_set_AllianceName;
        private static DelegateBridge __Hotfix_get_PlayerGameUserId;
        private static DelegateBridge __Hotfix_set_PlayerGameUserId;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="PlayerName", DataFormat=DataFormat.Default)]
        public string PlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CaptainLastNameId", DataFormat=DataFormat.TwosComplement)]
        public int CaptainLastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CaptainFirstNameId", DataFormat=DataFormat.TwosComplement)]
        public int CaptainFirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="PlayerAvatarId", DataFormat=DataFormat.TwosComplement)]
        public int PlayerAvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ShipId", DataFormat=DataFormat.TwosComplement)]
        public int ShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="GuildCodeName", DataFormat=DataFormat.Default)]
        public string GuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="AllianceName", DataFormat=DataFormat.Default)]
        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="PlayerGameUserId", DataFormat=DataFormat.Default)]
        public string PlayerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(11, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

