﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBenefitsReceiveAck")]
    public class GuildBenefitsReceiveAck : IExtensible
    {
        private int _Result;
        private ulong _BenefitsInstanceId;
        private int _BenefitsType;
        private readonly List<ProStoreItemUpdateInfo> _StoreItemUpdateList;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdateList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_BenefitsInstanceId;
        private static DelegateBridge __Hotfix_set_BenefitsInstanceId;
        private static DelegateBridge __Hotfix_get_BenefitsType;
        private static DelegateBridge __Hotfix_set_BenefitsType;
        private static DelegateBridge __Hotfix_get_StoreItemUpdateList;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BenefitsInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong BenefitsInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="BenefitsType", DataFormat=DataFormat.TwosComplement)]
        public int BenefitsType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="StoreItemUpdateList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> StoreItemUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="CurrencyUpdateList", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

