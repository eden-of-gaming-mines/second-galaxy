﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DailyLoginRewardTakeAck")]
    public class DailyLoginRewardTakeAck : IExtensible
    {
        private int _Result;
        private int _DayIndex;
        private readonly List<ProStoreItemUpdateInfo> _MStoreItemList;
        private readonly List<ProHiredCaptainInfo> _CaptainAddList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_DayIndex;
        private static DelegateBridge __Hotfix_set_DayIndex;
        private static DelegateBridge __Hotfix_get_MStoreItemList;
        private static DelegateBridge __Hotfix_get_CaptainAddList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="DayIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DayIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="MStoreItemList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> MStoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="CaptainAddList", DataFormat=DataFormat.Default)]
        public List<ProHiredCaptainInfo> CaptainAddList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

