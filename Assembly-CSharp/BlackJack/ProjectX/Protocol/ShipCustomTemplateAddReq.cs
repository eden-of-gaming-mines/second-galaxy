﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipCustomTemplateAddReq")]
    public class ShipCustomTemplateAddReq : IExtensible
    {
        private ProShipCustomTemplateInfo _TemplateInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TemplateInfo;
        private static DelegateBridge __Hotfix_set_TemplateInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TemplateInfo", DataFormat=DataFormat.Default)]
        public ProShipCustomTemplateInfo TemplateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

