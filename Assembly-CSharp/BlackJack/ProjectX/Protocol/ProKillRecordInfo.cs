﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProKillRecordInfo")]
    public class ProKillRecordInfo : IExtensible
    {
        private long _Time;
        private ProKillRecordOpponentInfo _WinnerInfo;
        private ProKillRecordOpponentInfo _LosserInfo;
        private readonly List<ProLostItemInfo> _LostItemList;
        private int _SolarSystemId;
        private double _LostBindMoney;
        private ulong _InstanceId;
        private int _CompensationStatus;
        private readonly List<int> _ItemCompensationStatus;
        private ulong _LostShipInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Time;
        private static DelegateBridge __Hotfix_set_Time;
        private static DelegateBridge __Hotfix_get_WinnerInfo;
        private static DelegateBridge __Hotfix_set_WinnerInfo;
        private static DelegateBridge __Hotfix_get_LosserInfo;
        private static DelegateBridge __Hotfix_set_LosserInfo;
        private static DelegateBridge __Hotfix_get_LostItemList;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_LostBindMoney;
        private static DelegateBridge __Hotfix_set_LostBindMoney;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_CompensationStatus;
        private static DelegateBridge __Hotfix_set_CompensationStatus;
        private static DelegateBridge __Hotfix_get_ItemCompensationStatus;
        private static DelegateBridge __Hotfix_get_LostShipInstanceId;
        private static DelegateBridge __Hotfix_set_LostShipInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Time", DataFormat=DataFormat.TwosComplement)]
        public long Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="WinnerInfo", DataFormat=DataFormat.Default)]
        public ProKillRecordOpponentInfo WinnerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LosserInfo", DataFormat=DataFormat.Default)]
        public ProKillRecordOpponentInfo LosserInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="LostItemList", DataFormat=DataFormat.Default)]
        public List<ProLostItemInfo> LostItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="LostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double LostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(8, IsRequired=false, Name="CompensationStatus", DataFormat=DataFormat.TwosComplement)]
        public int CompensationStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="ItemCompensationStatus", DataFormat=DataFormat.TwosComplement)]
        public List<int> ItemCompensationStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(10, IsRequired=false, Name="LostShipInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong LostShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

