﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpacePlayerShipViewInfo")]
    public class ProSpacePlayerShipViewInfo : IExtensible
    {
        private int _ConfigId;
        private string _ShipName;
        private readonly List<ProBufInfo> _DynamicBufList;
        private readonly List<int> _StaticBufList;
        private int _Shield;
        private int _Armor;
        private int _Energy;
        private readonly List<ProShipSlotGroupInfo> _HighSlotList;
        private readonly List<ProShipSlotAmmoInfo> _HighSlotAmmoList;
        private readonly List<ProShipSlotGroupInfo> _MiddleSlotList;
        private readonly List<ProShipSlotGroupInfo> _LowSlotList;
        private ProCaptainInfo _PlayerCaptain;
        private ProPlayerPVPInfo _PlayerPVPInfo;
        private uint _WingShipObjId;
        private bool _IsInvisible;
        private ProSceneNpcInteractionInfo _InteractionInfo;
        private ulong _InstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_get_ShipName;
        private static DelegateBridge __Hotfix_set_ShipName;
        private static DelegateBridge __Hotfix_get_DynamicBufList;
        private static DelegateBridge __Hotfix_get_StaticBufList;
        private static DelegateBridge __Hotfix_get_Shield;
        private static DelegateBridge __Hotfix_set_Shield;
        private static DelegateBridge __Hotfix_get_Armor;
        private static DelegateBridge __Hotfix_set_Armor;
        private static DelegateBridge __Hotfix_get_Energy;
        private static DelegateBridge __Hotfix_set_Energy;
        private static DelegateBridge __Hotfix_get_HighSlotList;
        private static DelegateBridge __Hotfix_get_HighSlotAmmoList;
        private static DelegateBridge __Hotfix_get_MiddleSlotList;
        private static DelegateBridge __Hotfix_get_LowSlotList;
        private static DelegateBridge __Hotfix_get_PlayerCaptain;
        private static DelegateBridge __Hotfix_set_PlayerCaptain;
        private static DelegateBridge __Hotfix_get_PlayerPVPInfo;
        private static DelegateBridge __Hotfix_set_PlayerPVPInfo;
        private static DelegateBridge __Hotfix_get_WingShipObjId;
        private static DelegateBridge __Hotfix_set_WingShipObjId;
        private static DelegateBridge __Hotfix_get_IsInvisible;
        private static DelegateBridge __Hotfix_set_IsInvisible;
        private static DelegateBridge __Hotfix_get_InteractionInfo;
        private static DelegateBridge __Hotfix_set_InteractionInfo;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ConfigId", DataFormat=DataFormat.TwosComplement)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ShipName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string ShipName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="DynamicBufList", DataFormat=DataFormat.Default)]
        public List<ProBufInfo> DynamicBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="StaticBufList", DataFormat=DataFormat.TwosComplement)]
        public List<int> StaticBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Shield", DataFormat=DataFormat.TwosComplement)]
        public int Shield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Armor", DataFormat=DataFormat.TwosComplement)]
        public int Armor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Energy", DataFormat=DataFormat.TwosComplement)]
        public int Energy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="HighSlotList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotGroupInfo> HighSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="HighSlotAmmoList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotAmmoInfo> HighSlotAmmoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, Name="MiddleSlotList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotGroupInfo> MiddleSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, Name="LowSlotList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotGroupInfo> LowSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="PlayerCaptain", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProCaptainInfo PlayerCaptain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="PlayerPVPInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProPlayerPVPInfo PlayerPVPInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(15, IsRequired=false, Name="WingShipObjId", DataFormat=DataFormat.TwosComplement)]
        public uint WingShipObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(0x10, IsRequired=false, Name="IsInvisible", DataFormat=DataFormat.Default)]
        public bool IsInvisible
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=false, Name="InteractionInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSceneNpcInteractionInfo InteractionInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(0x12, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

