﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSFactionInfoNtf")]
    public class DSFactionInfoNtf : IExtensible
    {
        private uint _Version;
        private readonly List<ProIdFloatValueInfo> _FactionCreditList;
        private int _CreditQuestCompleteTimes;
        private readonly List<int> _CreditQuestWeeklyRewardGetList;
        private long _NextCancelableTime;
        private readonly List<ProFactionCreditLevelRewardInfo> _FactionCreditLevelRewardInfoList;
        private readonly List<ProFactionCreditNpcQuestInfo> _FactionCreditNpcQuestInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_FactionCreditList;
        private static DelegateBridge __Hotfix_get_CreditQuestCompleteTimes;
        private static DelegateBridge __Hotfix_set_CreditQuestCompleteTimes;
        private static DelegateBridge __Hotfix_get_CreditQuestWeeklyRewardGetList;
        private static DelegateBridge __Hotfix_get_NextCancelableTime;
        private static DelegateBridge __Hotfix_set_NextCancelableTime;
        private static DelegateBridge __Hotfix_get_FactionCreditLevelRewardInfoList;
        private static DelegateBridge __Hotfix_get_FactionCreditNpcQuestInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="FactionCreditList", DataFormat=DataFormat.Default)]
        public List<ProIdFloatValueInfo> FactionCreditList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CreditQuestCompleteTimes", DataFormat=DataFormat.TwosComplement)]
        public int CreditQuestCompleteTimes
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="CreditQuestWeeklyRewardGetList", DataFormat=DataFormat.TwosComplement)]
        public List<int> CreditQuestWeeklyRewardGetList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="NextCancelableTime", DataFormat=DataFormat.TwosComplement)]
        public long NextCancelableTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="FactionCreditLevelRewardInfoList", DataFormat=DataFormat.Default)]
        public List<ProFactionCreditLevelRewardInfo> FactionCreditLevelRewardInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="FactionCreditNpcQuestInfoList", DataFormat=DataFormat.Default)]
        public List<ProFactionCreditNpcQuestInfo> FactionCreditNpcQuestInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

