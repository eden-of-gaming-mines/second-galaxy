﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerPropertiesNtf")]
    public class DSPlayerPropertiesNtf : IExtensible
    {
        private uint _Version;
        private int _FreeBasicProperties;
        private int _PropertiesBasicAttack;
        private int _PropertiesBasicDefence;
        private int _PropertiesBasicElectron;
        private int _PropertiesBasicDrive;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_FreeBasicProperties;
        private static DelegateBridge __Hotfix_set_FreeBasicProperties;
        private static DelegateBridge __Hotfix_get_PropertiesBasicAttack;
        private static DelegateBridge __Hotfix_set_PropertiesBasicAttack;
        private static DelegateBridge __Hotfix_get_PropertiesBasicDefence;
        private static DelegateBridge __Hotfix_set_PropertiesBasicDefence;
        private static DelegateBridge __Hotfix_get_PropertiesBasicElectron;
        private static DelegateBridge __Hotfix_set_PropertiesBasicElectron;
        private static DelegateBridge __Hotfix_get_PropertiesBasicDrive;
        private static DelegateBridge __Hotfix_set_PropertiesBasicDrive;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="FreeBasicProperties", DataFormat=DataFormat.TwosComplement)]
        public int FreeBasicProperties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="PropertiesBasicAttack", DataFormat=DataFormat.TwosComplement)]
        public int PropertiesBasicAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="PropertiesBasicDefence", DataFormat=DataFormat.TwosComplement)]
        public int PropertiesBasicDefence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="PropertiesBasicElectron", DataFormat=DataFormat.TwosComplement)]
        public int PropertiesBasicElectron
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="PropertiesBasicDrive", DataFormat=DataFormat.TwosComplement)]
        public int PropertiesBasicDrive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

