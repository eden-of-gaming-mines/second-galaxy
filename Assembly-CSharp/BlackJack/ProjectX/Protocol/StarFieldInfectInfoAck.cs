﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StarFieldInfectInfoAck")]
    public class StarFieldInfectInfoAck : IExtensible
    {
        private int _StarFieldId;
        private readonly List<ProInfectInfo> _InfectInfoList;
        private long _NextRefreshTime;
        private long _SelfHealingTime;
        private uint _DataVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StarFieldId;
        private static DelegateBridge __Hotfix_set_StarFieldId;
        private static DelegateBridge __Hotfix_get_InfectInfoList;
        private static DelegateBridge __Hotfix_get_NextRefreshTime;
        private static DelegateBridge __Hotfix_set_NextRefreshTime;
        private static DelegateBridge __Hotfix_get_SelfHealingTime;
        private static DelegateBridge __Hotfix_set_SelfHealingTime;
        private static DelegateBridge __Hotfix_get_DataVersion;
        private static DelegateBridge __Hotfix_set_DataVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="StarFieldId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int StarFieldId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="InfectInfoList", DataFormat=DataFormat.Default)]
        public List<ProInfectInfo> InfectInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="NextRefreshTime", DataFormat=DataFormat.TwosComplement)]
        public long NextRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="SelfHealingTime", DataFormat=DataFormat.TwosComplement)]
        public long SelfHealingTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="DataVersion", DataFormat=DataFormat.TwosComplement)]
        public uint DataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

