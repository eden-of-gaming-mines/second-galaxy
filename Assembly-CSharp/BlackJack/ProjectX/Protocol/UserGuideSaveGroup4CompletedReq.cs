﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="UserGuideSaveGroup4CompletedReq")]
    public class UserGuideSaveGroup4CompletedReq : IExtensible
    {
        private int _StepId;
        private int _GroupId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StepId;
        private static DelegateBridge __Hotfix_set_StepId;
        private static DelegateBridge __Hotfix_get_GroupId;
        private static DelegateBridge __Hotfix_set_GroupId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="StepId", DataFormat=DataFormat.TwosComplement)]
        public int StepId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="GroupId", DataFormat=DataFormat.TwosComplement)]
        public int GroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

