﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TeamInviteReq")]
    public class TeamInviteReq : IExtensible
    {
        private string _DestGameUserId;
        private string _DestPlayerName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DestGameUserId;
        private static DelegateBridge __Hotfix_set_DestGameUserId;
        private static DelegateBridge __Hotfix_get_DestPlayerName;
        private static DelegateBridge __Hotfix_set_DestPlayerName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="DestGameUserId", DataFormat=DataFormat.Default)]
        public string DestGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DestPlayerName", DataFormat=DataFormat.Default)]
        public string DestPlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

