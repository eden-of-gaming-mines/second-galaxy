﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DelegateMissionBalanceAck")]
    public class DelegateMissionBalanceAck : IExtensible
    {
        private int _Result;
        private ulong _SignalInstanceId;
        private readonly List<ProStoreItemUpdateInfo> _ItemUpdateList;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdateList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SignalInstanceId;
        private static DelegateBridge __Hotfix_set_SignalInstanceId;
        private static DelegateBridge __Hotfix_get_ItemUpdateList;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="SignalInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong SignalInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ItemUpdateList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> ItemUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="CurrencyUpdateList", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

