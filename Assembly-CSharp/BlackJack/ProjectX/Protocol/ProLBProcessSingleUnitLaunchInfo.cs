﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBProcessSingleUnitLaunchInfo")]
    public class ProLBProcessSingleUnitLaunchInfo : IExtensible
    {
        private uint _LaunchTime;
        private int _UnitIndex;
        private int _Flag;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LaunchTime;
        private static DelegateBridge __Hotfix_set_LaunchTime;
        private static DelegateBridge __Hotfix_get_UnitIndex;
        private static DelegateBridge __Hotfix_set_UnitIndex;
        private static DelegateBridge __Hotfix_get_Flag;
        private static DelegateBridge __Hotfix_set_Flag;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LaunchTime", DataFormat=DataFormat.TwosComplement)]
        public uint LaunchTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="UnitIndex", DataFormat=DataFormat.TwosComplement)]
        public int UnitIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="Flag", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Flag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

