﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProTradeListItemInfo")]
    public class ProTradeListItemInfo : IExtensible
    {
        private int _TradeItemId;
        private ProNpcDNId _TradeNpcDNId;
        private long _CurrPrice;
        private long _OldPrice;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TradeItemId;
        private static DelegateBridge __Hotfix_set_TradeItemId;
        private static DelegateBridge __Hotfix_get_TradeNpcDNId;
        private static DelegateBridge __Hotfix_set_TradeNpcDNId;
        private static DelegateBridge __Hotfix_get_CurrPrice;
        private static DelegateBridge __Hotfix_set_CurrPrice;
        private static DelegateBridge __Hotfix_get_OldPrice;
        private static DelegateBridge __Hotfix_set_OldPrice;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TradeItemId", DataFormat=DataFormat.TwosComplement)]
        public int TradeItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="TradeNpcDNId", DataFormat=DataFormat.Default)]
        public ProNpcDNId TradeNpcDNId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CurrPrice", DataFormat=DataFormat.TwosComplement)]
        public long CurrPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="OldPrice", DataFormat=DataFormat.TwosComplement)]
        public long OldPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

