﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatInfo")]
    public class ProChatInfo : IExtensible
    {
        private int _ChatSrcType;
        private int _ChatContentType;
        private ProPlayerSimplestInfo _ProPlayerInfo;
        private int _LanguageType;
        private int _ChannelId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ChatSrcType;
        private static DelegateBridge __Hotfix_set_ChatSrcType;
        private static DelegateBridge __Hotfix_get_ChatContentType;
        private static DelegateBridge __Hotfix_set_ChatContentType;
        private static DelegateBridge __Hotfix_get_ProPlayerInfo;
        private static DelegateBridge __Hotfix_set_ProPlayerInfo;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_set_LanguageType;
        private static DelegateBridge __Hotfix_get_ChannelId;
        private static DelegateBridge __Hotfix_set_ChannelId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ChatSrcType", DataFormat=DataFormat.TwosComplement)]
        public int ChatSrcType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ChatContentType", DataFormat=DataFormat.TwosComplement)]
        public int ChatContentType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ProPlayerInfo", DataFormat=DataFormat.Default)]
        public ProPlayerSimplestInfo ProPlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="LanguageType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ChannelId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ChannelId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

