﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFleetBasicInfo")]
    public class ProGuildFleetBasicInfo : IExtensible
    {
        private string _FleetName;
        private int _FleetSeqId;
        private bool _AllowToJoinFleetManual;
        private int _FormationType;
        private bool _IsFormationActive;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FleetName;
        private static DelegateBridge __Hotfix_set_FleetName;
        private static DelegateBridge __Hotfix_get_FleetSeqId;
        private static DelegateBridge __Hotfix_set_FleetSeqId;
        private static DelegateBridge __Hotfix_get_AllowToJoinFleetManual;
        private static DelegateBridge __Hotfix_set_AllowToJoinFleetManual;
        private static DelegateBridge __Hotfix_get_FormationType;
        private static DelegateBridge __Hotfix_set_FormationType;
        private static DelegateBridge __Hotfix_get_IsFormationActive;
        private static DelegateBridge __Hotfix_set_IsFormationActive;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(""), ProtoMember(1, IsRequired=false, Name="FleetName", DataFormat=DataFormat.Default)]
        public string FleetName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="FleetSeqId", DataFormat=DataFormat.TwosComplement)]
        public int FleetSeqId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AllowToJoinFleetManual", DataFormat=DataFormat.Default)]
        public bool AllowToJoinFleetManual
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FormationType", DataFormat=DataFormat.TwosComplement)]
        public int FormationType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="IsFormationActive", DataFormat=DataFormat.Default)]
        public bool IsFormationActive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

