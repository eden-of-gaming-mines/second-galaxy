﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMessageGetAck")]
    public class GuildMessageGetAck : IExtensible
    {
        private int _Result;
        private uint _GuildId;
        private uint _Version;
        private long _CacheLastMessageTime;
        private long _EarliestGuildMessageTime;
        private readonly List<ProGuildMessageInfo> _MessageList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_CacheLastMessageTime;
        private static DelegateBridge __Hotfix_set_CacheLastMessageTime;
        private static DelegateBridge __Hotfix_get_EarliestGuildMessageTime;
        private static DelegateBridge __Hotfix_set_EarliestGuildMessageTime;
        private static DelegateBridge __Hotfix_get_MessageList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CacheLastMessageTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CacheLastMessageTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="EarliestGuildMessageTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long EarliestGuildMessageTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="MessageList", DataFormat=DataFormat.Default)]
        public List<ProGuildMessageInfo> MessageList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

