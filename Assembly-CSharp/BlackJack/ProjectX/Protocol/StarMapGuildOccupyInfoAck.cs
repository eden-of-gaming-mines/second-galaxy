﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StarMapGuildOccupyInfoAck")]
    public class StarMapGuildOccupyInfoAck : IExtensible
    {
        private int _Result;
        private int _StarFieldId;
        private int _CurrStaticVersion;
        private int _CurrDynamicVersion;
        private ProStarMapGuildOccupyStaticInfo _StaticInfo;
        private ProStarMapGuildOccupyDynamicInfo _DynamicInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_StarFieldId;
        private static DelegateBridge __Hotfix_set_StarFieldId;
        private static DelegateBridge __Hotfix_get_CurrStaticVersion;
        private static DelegateBridge __Hotfix_set_CurrStaticVersion;
        private static DelegateBridge __Hotfix_get_CurrDynamicVersion;
        private static DelegateBridge __Hotfix_set_CurrDynamicVersion;
        private static DelegateBridge __Hotfix_get_StaticInfo;
        private static DelegateBridge __Hotfix_set_StaticInfo;
        private static DelegateBridge __Hotfix_get_DynamicInfo;
        private static DelegateBridge __Hotfix_set_DynamicInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StarFieldId", DataFormat=DataFormat.TwosComplement)]
        public int StarFieldId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CurrStaticVersion", DataFormat=DataFormat.TwosComplement)]
        public int CurrStaticVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CurrDynamicVersion", DataFormat=DataFormat.TwosComplement)]
        public int CurrDynamicVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="StaticInfo", DataFormat=DataFormat.Default)]
        public ProStarMapGuildOccupyStaticInfo StaticInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(6, IsRequired=false, Name="DynamicInfo", DataFormat=DataFormat.Default)]
        public ProStarMapGuildOccupyDynamicInfo DynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

