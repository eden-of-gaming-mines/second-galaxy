﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFleetMemberListUpdateNtf")]
    public class GuildFleetMemberListUpdateNtf : IExtensible
    {
        private ulong _FleetId;
        private readonly List<ProGuildFleetMemberBasicInfo> _FleetMemberList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FleetId;
        private static DelegateBridge __Hotfix_set_FleetId;
        private static DelegateBridge __Hotfix_get_FleetMemberList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FleetId", DataFormat=DataFormat.TwosComplement)]
        public ulong FleetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="FleetMemberList", DataFormat=DataFormat.Default)]
        public List<ProGuildFleetMemberBasicInfo> FleetMemberList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

