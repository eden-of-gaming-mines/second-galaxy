﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildDynamicInfo")]
    public class ProGuildDynamicInfo : IExtensible
    {
        private long _InfomrationPointCurrWeek;
        private int _GuildEvaluateScore;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InfomrationPointCurrWeek;
        private static DelegateBridge __Hotfix_set_InfomrationPointCurrWeek;
        private static DelegateBridge __Hotfix_get_GuildEvaluateScore;
        private static DelegateBridge __Hotfix_set_GuildEvaluateScore;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="InfomrationPointCurrWeek", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long InfomrationPointCurrWeek
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="GuildEvaluateScore", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GuildEvaluateScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

