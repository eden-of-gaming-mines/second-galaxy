﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProStarMapGuildOccupyStaticInfo")]
    public class ProStarMapGuildOccupyStaticInfo : IExtensible
    {
        private int _StarFieldId;
        private readonly List<ProGuildOccupySolarSystemInfo> _GuildOccupyInfoList;
        private readonly List<ProAllianceOccupySolarSystemInfo> _AllianceOccupyInfoList;
        private readonly List<ProStarMapGuildSimpleInfo> _GuildInfoList;
        private readonly List<ProStarMapAllianceSimpleInfo> _AllianceInfoList;
        private int _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StarFieldId;
        private static DelegateBridge __Hotfix_set_StarFieldId;
        private static DelegateBridge __Hotfix_get_GuildOccupyInfoList;
        private static DelegateBridge __Hotfix_get_AllianceOccupyInfoList;
        private static DelegateBridge __Hotfix_get_GuildInfoList;
        private static DelegateBridge __Hotfix_get_AllianceInfoList;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="StarFieldId", DataFormat=DataFormat.TwosComplement)]
        public int StarFieldId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="GuildOccupyInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildOccupySolarSystemInfo> GuildOccupyInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="AllianceOccupyInfoList", DataFormat=DataFormat.Default)]
        public List<ProAllianceOccupySolarSystemInfo> AllianceOccupyInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="GuildInfoList", DataFormat=DataFormat.Default)]
        public List<ProStarMapGuildSimpleInfo> GuildInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="AllianceInfoList", DataFormat=DataFormat.Default)]
        public List<ProStarMapAllianceSimpleInfo> AllianceInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

