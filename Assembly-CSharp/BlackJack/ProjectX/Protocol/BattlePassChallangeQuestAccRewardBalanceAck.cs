﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BattlePassChallangeQuestAccRewardBalanceAck")]
    public class BattlePassChallangeQuestAccRewardBalanceAck : IExtensible
    {
        private int _Result;
        private int _PeriodId;
        private int _AccRewardIndex;
        private readonly List<ProStoreItemUpdateInfo> _MStoreItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_PeriodId;
        private static DelegateBridge __Hotfix_set_PeriodId;
        private static DelegateBridge __Hotfix_get_AccRewardIndex;
        private static DelegateBridge __Hotfix_set_AccRewardIndex;
        private static DelegateBridge __Hotfix_get_MStoreItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="PeriodId", DataFormat=DataFormat.TwosComplement)]
        public int PeriodId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AccRewardIndex", DataFormat=DataFormat.TwosComplement)]
        public int AccRewardIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="MStoreItemList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> MStoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

