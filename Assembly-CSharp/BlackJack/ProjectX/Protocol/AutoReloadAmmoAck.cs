﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AutoReloadAmmoAck")]
    public class AutoReloadAmmoAck : IExtensible
    {
        private int _Result;
        private int _HangarShipIndex;
        private bool _ChangeAmmoAllowable;
        private readonly List<int> _AmmoCountList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_ChangeAmmoAllowable;
        private static DelegateBridge __Hotfix_set_ChangeAmmoAllowable;
        private static DelegateBridge __Hotfix_get_AmmoCountList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="ChangeAmmoAllowable", DataFormat=DataFormat.Default)]
        public bool ChangeAmmoAllowable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="AmmoCountList", DataFormat=DataFormat.TwosComplement)]
        public List<int> AmmoCountList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

