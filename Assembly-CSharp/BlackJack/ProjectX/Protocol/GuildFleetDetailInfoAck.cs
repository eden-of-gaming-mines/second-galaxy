﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFleetDetailInfoAck")]
    public class GuildFleetDetailInfoAck : IExtensible
    {
        private int _Result;
        private uint _BasicInfoVersion;
        private uint _MemberListVersion;
        private ProGuildFleetDetailInfo _FleetDetailInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_BasicInfoVersion;
        private static DelegateBridge __Hotfix_set_BasicInfoVersion;
        private static DelegateBridge __Hotfix_get_MemberListVersion;
        private static DelegateBridge __Hotfix_set_MemberListVersion;
        private static DelegateBridge __Hotfix_get_FleetDetailInfo;
        private static DelegateBridge __Hotfix_set_FleetDetailInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="BasicInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public uint BasicInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="MemberListVersion", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint MemberListVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="FleetDetailInfo", DataFormat=DataFormat.Default)]
        public ProGuildFleetDetailInfo FleetDetailInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

