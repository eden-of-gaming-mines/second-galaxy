﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipStoreItemUpdateInfo")]
    public class ProShipStoreItemUpdateInfo : IExtensible
    {
        private ProShipStoreItemInfo _ShipItem;
        private long _ChangeValue;
        private ProShipStoreItemInfo _ShipBindItem;
        private long _BindItemChangeValue;
        private ProShipStoreItemInfo _ShipFreezingItem;
        private long _FreezingItemChangeValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipItem;
        private static DelegateBridge __Hotfix_set_ShipItem;
        private static DelegateBridge __Hotfix_get_ChangeValue;
        private static DelegateBridge __Hotfix_set_ChangeValue;
        private static DelegateBridge __Hotfix_get_ShipBindItem;
        private static DelegateBridge __Hotfix_set_ShipBindItem;
        private static DelegateBridge __Hotfix_get_BindItemChangeValue;
        private static DelegateBridge __Hotfix_set_BindItemChangeValue;
        private static DelegateBridge __Hotfix_get_ShipFreezingItem;
        private static DelegateBridge __Hotfix_set_ShipFreezingItem;
        private static DelegateBridge __Hotfix_get_FreezingItemChangeValue;
        private static DelegateBridge __Hotfix_set_FreezingItemChangeValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="ShipItem", DataFormat=DataFormat.Default)]
        public ProShipStoreItemInfo ShipItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ChangeValue", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long ChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="ShipBindItem", DataFormat=DataFormat.Default)]
        public ProShipStoreItemInfo ShipBindItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="BindItemChangeValue", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long BindItemChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ShipFreezingItem", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProShipStoreItemInfo ShipFreezingItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="FreezingItemChangeValue", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long FreezingItemChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

