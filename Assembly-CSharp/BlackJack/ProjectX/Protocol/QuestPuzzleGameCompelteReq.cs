﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="QuestPuzzleGameCompelteReq")]
    public class QuestPuzzleGameCompelteReq : IExtensible
    {
        private int _QuestInstanceId;
        private readonly List<int> _CurrencyCost;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_QuestInstanceId;
        private static DelegateBridge __Hotfix_set_QuestInstanceId;
        private static DelegateBridge __Hotfix_get_CurrencyCost;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="QuestInstanceId", DataFormat=DataFormat.TwosComplement)]
        public int QuestInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="CurrencyCost", DataFormat=DataFormat.TwosComplement)]
        public List<int> CurrencyCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

