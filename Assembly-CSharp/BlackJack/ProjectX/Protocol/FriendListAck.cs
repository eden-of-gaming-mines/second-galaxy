﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="FriendListAck")]
    public class FriendListAck : IExtensible
    {
        private readonly List<FriendListData> _friendData;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FriendData;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(3, Name="friendData", DataFormat=DataFormat.Default)]
        public List<FriendListData> FriendData
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [Serializable, ProtoContract(Name="FriendListData")]
        public class FriendListData : IExtensible
        {
            private string _GameuserId;
            private string _PlayerName;
            private bool _isOnline;
            private IExtension extensionObject;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_GameuserId;
            private static DelegateBridge __Hotfix_set_GameuserId;
            private static DelegateBridge __Hotfix_get_PlayerName;
            private static DelegateBridge __Hotfix_set_PlayerName;
            private static DelegateBridge __Hotfix_get_IsOnline;
            private static DelegateBridge __Hotfix_set_IsOnline;
            private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

            [MethodImpl(0x8000)]
            IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            {
            }

            [ProtoMember(1, IsRequired=true, Name="GameuserId", DataFormat=DataFormat.Default)]
            public string GameuserId
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(2, IsRequired=true, Name="PlayerName", DataFormat=DataFormat.Default)]
            public string PlayerName
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(3, IsRequired=true, Name="isOnline", DataFormat=DataFormat.Default)]
            public bool IsOnline
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }
        }
    }
}

