﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessDroneSniperLaunch")]
    public class ProLBSpaceProcessDroneSniperLaunch : IExtensible
    {
        private ProLBSpaceProcessDroneLaunchInfo _DroneSniperLaunch;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DroneSniperLaunch;
        private static DelegateBridge __Hotfix_set_DroneSniperLaunch;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="DroneSniperLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessDroneLaunchInfo DroneSniperLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

