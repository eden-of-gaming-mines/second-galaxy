﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDailyLoginRewardInfo")]
    public class ProDailyLoginRewardInfo : IExtensible
    {
        private int _DayIndex;
        private int _RewardState;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DayIndex;
        private static DelegateBridge __Hotfix_set_DayIndex;
        private static DelegateBridge __Hotfix_get_RewardState;
        private static DelegateBridge __Hotfix_set_RewardState;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="DayIndex", DataFormat=DataFormat.TwosComplement)]
        public int DayIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="RewardState", DataFormat=DataFormat.TwosComplement)]
        public int RewardState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

