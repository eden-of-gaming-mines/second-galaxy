﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerAuctionItemUpdateNtf")]
    public class PlayerAuctionItemUpdateNtf : IExtensible
    {
        private ulong _AuctionItemInsId;
        private int _AuctionItemState;
        private int _AuctionItemCurrCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AuctionItemInsId;
        private static DelegateBridge __Hotfix_set_AuctionItemInsId;
        private static DelegateBridge __Hotfix_get_AuctionItemState;
        private static DelegateBridge __Hotfix_set_AuctionItemState;
        private static DelegateBridge __Hotfix_get_AuctionItemCurrCount;
        private static DelegateBridge __Hotfix_set_AuctionItemCurrCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="AuctionItemInsId", DataFormat=DataFormat.TwosComplement)]
        public ulong AuctionItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="AuctionItemState", DataFormat=DataFormat.TwosComplement)]
        public int AuctionItemState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="AuctionItemCurrCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AuctionItemCurrCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

