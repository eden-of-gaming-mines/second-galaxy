﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBenefitsListAck")]
    public class GuildBenefitsListAck : IExtensible
    {
        private int _Result;
        private readonly List<ProGuildBenefitsInformationPoint> _InformationPointBenefitsList;
        private readonly List<ProGuildBenefitsCommon> _CommonBenefitsList;
        private ProGuildBenefitsSovereignty _BenefitsSovereignty;
        private int _BenefitsListVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_InformationPointBenefitsList;
        private static DelegateBridge __Hotfix_get_CommonBenefitsList;
        private static DelegateBridge __Hotfix_get_BenefitsSovereignty;
        private static DelegateBridge __Hotfix_set_BenefitsSovereignty;
        private static DelegateBridge __Hotfix_get_BenefitsListVersion;
        private static DelegateBridge __Hotfix_set_BenefitsListVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="InformationPointBenefitsList", DataFormat=DataFormat.Default)]
        public List<ProGuildBenefitsInformationPoint> InformationPointBenefitsList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="CommonBenefitsList", DataFormat=DataFormat.Default)]
        public List<ProGuildBenefitsCommon> CommonBenefitsList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="BenefitsSovereignty", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildBenefitsSovereignty BenefitsSovereignty
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="BenefitsListVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BenefitsListVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

