﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildStoreItemInfo")]
    public class ProGuildStoreItemInfo : IExtensible
    {
        private int _Version;
        private ProItemInfo _ItemInfo;
        private int _Index;
        private bool _deleted;
        private uint _Flag;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_ItemInfo;
        private static DelegateBridge __Hotfix_set_ItemInfo;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_Deleted;
        private static DelegateBridge __Hotfix_set_Deleted;
        private static DelegateBridge __Hotfix_get_Flag;
        private static DelegateBridge __Hotfix_set_Flag;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public int Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ItemInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProItemInfo ItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="Index", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="deleted", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool Deleted
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="Flag", DataFormat=DataFormat.TwosComplement)]
        public uint Flag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

