﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBasicInfo")]
    public class ProGuildBasicInfo : IExtensible
    {
        private uint _GuildId;
        private string _Name;
        private string _CodeName;
        private int _LanguageType;
        private int _JoinPllicy;
        private ProGuildLogoInfo _LogoInfo;
        private uint _AllianceId;
        private string _LeaderGameUserId;
        private int _BaseSolarSystem;
        private int _BaseStation;
        private string _Manifesto;
        private string _Announcement;
        private long _GuildDismissEndTime;
        private long _LeaderTransferEndTime;
        private string _LeaderTransferTargetUserId;
        private long _NextDailyRefreshTime;
        private long _NextWeeklyRefreshTime;
        private string _WalletAnnouncement;
        private int _ReinforcementEndHour;
        private uint _Version;
        private bool _AutoCompensation;
        private ulong _GuildFleetId;
        private string _AllianceName;
        private long _CreateTime;
        private ProAllianceLogoInfo _AllianceLogo;
        private int _ReinforcementEndMinuts;
        private bool _ReinforcementHasSetted;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_CodeName;
        private static DelegateBridge __Hotfix_set_CodeName;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_set_LanguageType;
        private static DelegateBridge __Hotfix_get_JoinPllicy;
        private static DelegateBridge __Hotfix_set_JoinPllicy;
        private static DelegateBridge __Hotfix_get_LogoInfo;
        private static DelegateBridge __Hotfix_set_LogoInfo;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_LeaderGameUserId;
        private static DelegateBridge __Hotfix_set_LeaderGameUserId;
        private static DelegateBridge __Hotfix_get_BaseSolarSystem;
        private static DelegateBridge __Hotfix_set_BaseSolarSystem;
        private static DelegateBridge __Hotfix_get_BaseStation;
        private static DelegateBridge __Hotfix_set_BaseStation;
        private static DelegateBridge __Hotfix_get_Manifesto;
        private static DelegateBridge __Hotfix_set_Manifesto;
        private static DelegateBridge __Hotfix_get_Announcement;
        private static DelegateBridge __Hotfix_set_Announcement;
        private static DelegateBridge __Hotfix_get_GuildDismissEndTime;
        private static DelegateBridge __Hotfix_set_GuildDismissEndTime;
        private static DelegateBridge __Hotfix_get_LeaderTransferEndTime;
        private static DelegateBridge __Hotfix_set_LeaderTransferEndTime;
        private static DelegateBridge __Hotfix_get_LeaderTransferTargetUserId;
        private static DelegateBridge __Hotfix_set_LeaderTransferTargetUserId;
        private static DelegateBridge __Hotfix_get_NextDailyRefreshTime;
        private static DelegateBridge __Hotfix_set_NextDailyRefreshTime;
        private static DelegateBridge __Hotfix_get_NextWeeklyRefreshTime;
        private static DelegateBridge __Hotfix_set_NextWeeklyRefreshTime;
        private static DelegateBridge __Hotfix_get_WalletAnnouncement;
        private static DelegateBridge __Hotfix_set_WalletAnnouncement;
        private static DelegateBridge __Hotfix_get_ReinforcementEndHour;
        private static DelegateBridge __Hotfix_set_ReinforcementEndHour;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_AutoCompensation;
        private static DelegateBridge __Hotfix_set_AutoCompensation;
        private static DelegateBridge __Hotfix_get_GuildFleetId;
        private static DelegateBridge __Hotfix_set_GuildFleetId;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_set_AllianceName;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_get_AllianceLogo;
        private static DelegateBridge __Hotfix_set_AllianceLogo;
        private static DelegateBridge __Hotfix_get_ReinforcementEndMinuts;
        private static DelegateBridge __Hotfix_set_ReinforcementEndMinuts;
        private static DelegateBridge __Hotfix_get_ReinforcementHasSetted;
        private static DelegateBridge __Hotfix_set_ReinforcementHasSetted;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CodeName", DataFormat=DataFormat.Default)]
        public string CodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="LanguageType", DataFormat=DataFormat.TwosComplement)]
        public int LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="JoinPllicy", DataFormat=DataFormat.TwosComplement)]
        public int JoinPllicy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="LogoInfo", DataFormat=DataFormat.Default)]
        public ProGuildLogoInfo LogoInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="AllianceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="LeaderGameUserId", DataFormat=DataFormat.Default)]
        public string LeaderGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BaseSolarSystem", DataFormat=DataFormat.TwosComplement)]
        public int BaseSolarSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="BaseStation", DataFormat=DataFormat.TwosComplement)]
        public int BaseStation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="Manifesto", DataFormat=DataFormat.Default)]
        public string Manifesto
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="Announcement", DataFormat=DataFormat.Default), DefaultValue("")]
        public string Announcement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(13, IsRequired=false, Name="GuildDismissEndTime", DataFormat=DataFormat.TwosComplement)]
        public long GuildDismissEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="LeaderTransferEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long LeaderTransferEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(15, IsRequired=false, Name="LeaderTransferTargetUserId", DataFormat=DataFormat.Default)]
        public string LeaderTransferTargetUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="NextDailyRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextDailyRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=false, Name="NextWeeklyRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextWeeklyRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(0x12, IsRequired=false, Name="WalletAnnouncement", DataFormat=DataFormat.Default)]
        public string WalletAnnouncement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x13, IsRequired=false, Name="ReinforcementEndHour", DataFormat=DataFormat.TwosComplement)]
        public int ReinforcementEndHour
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x17, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(0x18, IsRequired=false, Name="AutoCompensation", DataFormat=DataFormat.Default)]
        public bool AutoCompensation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=false, Name="GuildFleetId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong GuildFleetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(0x1a, IsRequired=false, Name="AllianceName", DataFormat=DataFormat.Default)]
        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x1b, IsRequired=false, Name="CreateTime", DataFormat=DataFormat.TwosComplement)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x1c, IsRequired=false, Name="AllianceLogo", DataFormat=DataFormat.Default)]
        public ProAllianceLogoInfo AllianceLogo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x1d, IsRequired=false, Name="ReinforcementEndMinuts", DataFormat=DataFormat.TwosComplement)]
        public int ReinforcementEndMinuts
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(30, IsRequired=false, Name="ReinforcementHasSetted", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool ReinforcementHasSetted
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

