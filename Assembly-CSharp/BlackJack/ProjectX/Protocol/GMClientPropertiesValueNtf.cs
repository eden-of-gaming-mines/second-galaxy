﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMClientPropertiesValueNtf")]
    public class GMClientPropertiesValueNtf : IExtensible
    {
        private int _Result;
        private int _PropertyId;
        private float _PropertyValueForPlayer;
        private float _PropertyValueForNormalAttack;
        private readonly List<float> _PropertyValueForHightSlot;
        private float _PropertyValueForSuperWeapon;
        private float _PropertyValueForTacticalEquip;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_PropertyId;
        private static DelegateBridge __Hotfix_set_PropertyId;
        private static DelegateBridge __Hotfix_get_PropertyValueForPlayer;
        private static DelegateBridge __Hotfix_set_PropertyValueForPlayer;
        private static DelegateBridge __Hotfix_get_PropertyValueForNormalAttack;
        private static DelegateBridge __Hotfix_set_PropertyValueForNormalAttack;
        private static DelegateBridge __Hotfix_get_PropertyValueForHightSlot;
        private static DelegateBridge __Hotfix_get_PropertyValueForSuperWeapon;
        private static DelegateBridge __Hotfix_set_PropertyValueForSuperWeapon;
        private static DelegateBridge __Hotfix_get_PropertyValueForTacticalEquip;
        private static DelegateBridge __Hotfix_set_PropertyValueForTacticalEquip;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="PropertyId", DataFormat=DataFormat.TwosComplement)]
        public int PropertyId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="PropertyValueForPlayer", DataFormat=DataFormat.FixedSize)]
        public float PropertyValueForPlayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="PropertyValueForNormalAttack", DataFormat=DataFormat.FixedSize)]
        public float PropertyValueForNormalAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="PropertyValueForHightSlot", DataFormat=DataFormat.FixedSize)]
        public List<float> PropertyValueForHightSlot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(6, IsRequired=false, Name="PropertyValueForSuperWeapon", DataFormat=DataFormat.FixedSize)]
        public float PropertyValueForSuperWeapon
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="PropertyValueForTacticalEquip", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float PropertyValueForTacticalEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

