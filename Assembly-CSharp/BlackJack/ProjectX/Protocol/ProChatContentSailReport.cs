﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatContentSailReport")]
    public class ProChatContentSailReport : IExtensible
    {
        private float _CauseDamage;
        private float _SufferDamage;
        private double _FlyDistance;
        private int _JumpCount;
        private int _DestroyShipCount;
        private int _EnterFightCount;
        private long _StartTime;
        private float _DurationTime;
        private float _GatherExp;
        private float _GatherCredit;
        private float _GatherMoney;
        private readonly List<ProSimpleItemInfoWithCount> _GatherItems;
        private readonly List<byte[]> _ReportSolarSystemInfos;
        private string _PlayerName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CauseDamage;
        private static DelegateBridge __Hotfix_set_CauseDamage;
        private static DelegateBridge __Hotfix_get_SufferDamage;
        private static DelegateBridge __Hotfix_set_SufferDamage;
        private static DelegateBridge __Hotfix_get_FlyDistance;
        private static DelegateBridge __Hotfix_set_FlyDistance;
        private static DelegateBridge __Hotfix_get_JumpCount;
        private static DelegateBridge __Hotfix_set_JumpCount;
        private static DelegateBridge __Hotfix_get_DestroyShipCount;
        private static DelegateBridge __Hotfix_set_DestroyShipCount;
        private static DelegateBridge __Hotfix_get_EnterFightCount;
        private static DelegateBridge __Hotfix_set_EnterFightCount;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_DurationTime;
        private static DelegateBridge __Hotfix_set_DurationTime;
        private static DelegateBridge __Hotfix_get_GatherExp;
        private static DelegateBridge __Hotfix_set_GatherExp;
        private static DelegateBridge __Hotfix_get_GatherCredit;
        private static DelegateBridge __Hotfix_set_GatherCredit;
        private static DelegateBridge __Hotfix_get_GatherMoney;
        private static DelegateBridge __Hotfix_set_GatherMoney;
        private static DelegateBridge __Hotfix_get_GatherItems;
        private static DelegateBridge __Hotfix_get_ReportSolarSystemInfos;
        private static DelegateBridge __Hotfix_get_PlayerName;
        private static DelegateBridge __Hotfix_set_PlayerName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="CauseDamage", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float CauseDamage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="SufferDamage", DataFormat=DataFormat.FixedSize)]
        public float SufferDamage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="FlyDistance", DataFormat=DataFormat.TwosComplement), DefaultValue((double) 0.0)]
        public double FlyDistance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="JumpCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int JumpCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="DestroyShipCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DestroyShipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="EnterFightCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int EnterFightCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="StartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="DurationTime", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float DurationTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="GatherExp", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float GatherExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="GatherCredit", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float GatherCredit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="GatherMoney", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float GatherMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="GatherItems", DataFormat=DataFormat.Default)]
        public List<ProSimpleItemInfoWithCount> GatherItems
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, Name="ReportSolarSystemInfos", DataFormat=DataFormat.Default)]
        public List<byte[]> ReportSolarSystemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(""), ProtoMember(14, IsRequired=false, Name="PlayerName", DataFormat=DataFormat.Default)]
        public string PlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

