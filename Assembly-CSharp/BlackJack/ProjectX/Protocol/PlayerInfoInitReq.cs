﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerInfoInitReq")]
    public class PlayerInfoInitReq : IExtensible
    {
        private int _DSLevelMiscVersion;
        private int _DSPropertiesBasicVersion;
        private int _DSSkillVersion;
        private int _DSSpaceBasicVersion;
        private int _DSChipSlotVersion;
        private int _DSMotherShipBasicVersion;
        private readonly List<int> _DSMotherShipHangarVersionList;
        private readonly List<int> _DSItemStoreVersionList;
        private readonly List<int> _DSHiredCaptainVersionList;
        private int _DSFleetVersion;
        private int _DSSignalVersion;
        private int _DSDelegateMissionVersion;
        private int _DSQuestVersion;
        private int _DSUserGuideVersion;
        private readonly List<int> _DSMailVersionList;
        private int _DSFactionVersion;
        private int _DSNpcShopVersion;
        private int _DSTechVersion;
        private int _DSProduceVersion;
        private int _DSShipCustomTemplateVersion;
        private readonly List<int> _DSKillRecordVersionList;
        private int _DSPVPVersion;
        private int _DSDrivingLicenseVersion;
        private int _DSCrackVersion;
        private int _DSActivityVersion;
        private int _DSFunctionOpenStateVersion;
        private int _DSDevelopmentVersion;
        private int _DSItemFreezingTimeVersion;
        private int _DSDiplomacyVersion;
        private int _DSChatVersion;
        private int _DSPVPInvadeRescueVersion;
        private int _DSSpaceSignalVersion;
        private int _DSBattlePassVersion;
        private int _DSRechargeOrderVersion;
        private int _DSRechargeGiftPackageVersion;
        private int _DSRechargeMonthlyCardVersion;
        private int _DSRechargeItemVersion;
        private int _DSShipSalvageVersion;
        private int _DSCustomizedParameterVersion;
        private int _DSLoginActivityVersion;
        private int _DSUserSettingVersion;
        private bool _OnlyCheckDSVerion;
        private string _SdkOperators;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DSLevelMiscVersion;
        private static DelegateBridge __Hotfix_set_DSLevelMiscVersion;
        private static DelegateBridge __Hotfix_get_DSPropertiesBasicVersion;
        private static DelegateBridge __Hotfix_set_DSPropertiesBasicVersion;
        private static DelegateBridge __Hotfix_get_DSSkillVersion;
        private static DelegateBridge __Hotfix_set_DSSkillVersion;
        private static DelegateBridge __Hotfix_get_DSSpaceBasicVersion;
        private static DelegateBridge __Hotfix_set_DSSpaceBasicVersion;
        private static DelegateBridge __Hotfix_get_DSChipSlotVersion;
        private static DelegateBridge __Hotfix_set_DSChipSlotVersion;
        private static DelegateBridge __Hotfix_get_DSMotherShipBasicVersion;
        private static DelegateBridge __Hotfix_set_DSMotherShipBasicVersion;
        private static DelegateBridge __Hotfix_get_DSMotherShipHangarVersionList;
        private static DelegateBridge __Hotfix_get_DSItemStoreVersionList;
        private static DelegateBridge __Hotfix_get_DSHiredCaptainVersionList;
        private static DelegateBridge __Hotfix_get_DSFleetVersion;
        private static DelegateBridge __Hotfix_set_DSFleetVersion;
        private static DelegateBridge __Hotfix_get_DSSignalVersion;
        private static DelegateBridge __Hotfix_set_DSSignalVersion;
        private static DelegateBridge __Hotfix_get_DSDelegateMissionVersion;
        private static DelegateBridge __Hotfix_set_DSDelegateMissionVersion;
        private static DelegateBridge __Hotfix_get_DSQuestVersion;
        private static DelegateBridge __Hotfix_set_DSQuestVersion;
        private static DelegateBridge __Hotfix_get_DSUserGuideVersion;
        private static DelegateBridge __Hotfix_set_DSUserGuideVersion;
        private static DelegateBridge __Hotfix_get_DSMailVersionList;
        private static DelegateBridge __Hotfix_get_DSFactionVersion;
        private static DelegateBridge __Hotfix_set_DSFactionVersion;
        private static DelegateBridge __Hotfix_get_DSNpcShopVersion;
        private static DelegateBridge __Hotfix_set_DSNpcShopVersion;
        private static DelegateBridge __Hotfix_get_DSTechVersion;
        private static DelegateBridge __Hotfix_set_DSTechVersion;
        private static DelegateBridge __Hotfix_get_DSProduceVersion;
        private static DelegateBridge __Hotfix_set_DSProduceVersion;
        private static DelegateBridge __Hotfix_get_DSShipCustomTemplateVersion;
        private static DelegateBridge __Hotfix_set_DSShipCustomTemplateVersion;
        private static DelegateBridge __Hotfix_get_DSKillRecordVersionList;
        private static DelegateBridge __Hotfix_get_DSPVPVersion;
        private static DelegateBridge __Hotfix_set_DSPVPVersion;
        private static DelegateBridge __Hotfix_get_DSDrivingLicenseVersion;
        private static DelegateBridge __Hotfix_set_DSDrivingLicenseVersion;
        private static DelegateBridge __Hotfix_get_DSCrackVersion;
        private static DelegateBridge __Hotfix_set_DSCrackVersion;
        private static DelegateBridge __Hotfix_get_DSActivityVersion;
        private static DelegateBridge __Hotfix_set_DSActivityVersion;
        private static DelegateBridge __Hotfix_get_DSFunctionOpenStateVersion;
        private static DelegateBridge __Hotfix_set_DSFunctionOpenStateVersion;
        private static DelegateBridge __Hotfix_get_DSDevelopmentVersion;
        private static DelegateBridge __Hotfix_set_DSDevelopmentVersion;
        private static DelegateBridge __Hotfix_get_DSItemFreezingTimeVersion;
        private static DelegateBridge __Hotfix_set_DSItemFreezingTimeVersion;
        private static DelegateBridge __Hotfix_get_DSDiplomacyVersion;
        private static DelegateBridge __Hotfix_set_DSDiplomacyVersion;
        private static DelegateBridge __Hotfix_get_DSChatVersion;
        private static DelegateBridge __Hotfix_set_DSChatVersion;
        private static DelegateBridge __Hotfix_get_DSPVPInvadeRescueVersion;
        private static DelegateBridge __Hotfix_set_DSPVPInvadeRescueVersion;
        private static DelegateBridge __Hotfix_get_DSSpaceSignalVersion;
        private static DelegateBridge __Hotfix_set_DSSpaceSignalVersion;
        private static DelegateBridge __Hotfix_get_DSBattlePassVersion;
        private static DelegateBridge __Hotfix_set_DSBattlePassVersion;
        private static DelegateBridge __Hotfix_get_DSRechargeOrderVersion;
        private static DelegateBridge __Hotfix_set_DSRechargeOrderVersion;
        private static DelegateBridge __Hotfix_get_DSRechargeGiftPackageVersion;
        private static DelegateBridge __Hotfix_set_DSRechargeGiftPackageVersion;
        private static DelegateBridge __Hotfix_get_DSRechargeMonthlyCardVersion;
        private static DelegateBridge __Hotfix_set_DSRechargeMonthlyCardVersion;
        private static DelegateBridge __Hotfix_get_DSRechargeItemVersion;
        private static DelegateBridge __Hotfix_set_DSRechargeItemVersion;
        private static DelegateBridge __Hotfix_get_DSShipSalvageVersion;
        private static DelegateBridge __Hotfix_set_DSShipSalvageVersion;
        private static DelegateBridge __Hotfix_get_DSCustomizedParameterVersion;
        private static DelegateBridge __Hotfix_set_DSCustomizedParameterVersion;
        private static DelegateBridge __Hotfix_get_DSLoginActivityVersion;
        private static DelegateBridge __Hotfix_set_DSLoginActivityVersion;
        private static DelegateBridge __Hotfix_get_DSUserSettingVersion;
        private static DelegateBridge __Hotfix_set_DSUserSettingVersion;
        private static DelegateBridge __Hotfix_get_OnlyCheckDSVerion;
        private static DelegateBridge __Hotfix_set_OnlyCheckDSVerion;
        private static DelegateBridge __Hotfix_get_SdkOperators;
        private static DelegateBridge __Hotfix_set_SdkOperators;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="DSLevelMiscVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSLevelMiscVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="DSPropertiesBasicVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSPropertiesBasicVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="DSSkillVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSSkillVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="DSSpaceBasicVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSSpaceBasicVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="DSChipSlotVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSChipSlotVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="DSMotherShipBasicVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSMotherShipBasicVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, Name="DSMotherShipHangarVersionList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DSMotherShipHangarVersionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, Name="DSItemStoreVersionList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DSItemStoreVersionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x11, Name="DSHiredCaptainVersionList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DSHiredCaptainVersionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x12, IsRequired=false, Name="DSFleetVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSFleetVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=false, Name="DSSignalVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSSignalVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(20, IsRequired=false, Name="DSDelegateMissionVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSDelegateMissionVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=false, Name="DSQuestVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSQuestVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=false, Name="DSUserGuideVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSUserGuideVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, Name="DSMailVersionList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DSMailVersionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x18, IsRequired=false, Name="DSFactionVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSFactionVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x19, IsRequired=false, Name="DSNpcShopVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSNpcShopVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x1a, IsRequired=false, Name="DSTechVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSTechVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1b, IsRequired=false, Name="DSProduceVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSProduceVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x1c, IsRequired=false, Name="DSShipCustomTemplateVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSShipCustomTemplateVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, Name="DSKillRecordVersionList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DSKillRecordVersionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(30, IsRequired=false, Name="DSPVPVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSPVPVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=false, Name="DSDrivingLicenseVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSDrivingLicenseVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x20, IsRequired=false, Name="DSCrackVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSCrackVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x21, IsRequired=false, Name="DSActivityVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSActivityVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x22, IsRequired=false, Name="DSFunctionOpenStateVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSFunctionOpenStateVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x23, IsRequired=false, Name="DSDevelopmentVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSDevelopmentVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x24, IsRequired=false, Name="DSItemFreezingTimeVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSItemFreezingTimeVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x25, IsRequired=false, Name="DSDiplomacyVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSDiplomacyVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x26, IsRequired=false, Name="DSChatVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSChatVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x27, IsRequired=false, Name="DSPVPInvadeRescueVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSPVPInvadeRescueVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(40, IsRequired=false, Name="DSSpaceSignalVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSSpaceSignalVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x29, IsRequired=false, Name="DSBattlePassVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSBattlePassVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2a, IsRequired=false, Name="DSRechargeOrderVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSRechargeOrderVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2b, IsRequired=false, Name="DSRechargeGiftPackageVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSRechargeGiftPackageVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2c, IsRequired=false, Name="DSRechargeMonthlyCardVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSRechargeMonthlyCardVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x2d, IsRequired=false, Name="DSRechargeItemVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSRechargeItemVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2e, IsRequired=false, Name="DSShipSalvageVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSShipSalvageVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2f, IsRequired=false, Name="DSCustomizedParameterVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSCustomizedParameterVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x30, IsRequired=false, Name="DSLoginActivityVersion", DataFormat=DataFormat.TwosComplement)]
        public int DSLoginActivityVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x31, IsRequired=false, Name="DSUserSettingVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DSUserSettingVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(80, IsRequired=false, Name="OnlyCheckDSVerion", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool OnlyCheckDSVerion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(0x51, IsRequired=false, Name="SdkOperators", DataFormat=DataFormat.Default)]
        public string SdkOperators
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

