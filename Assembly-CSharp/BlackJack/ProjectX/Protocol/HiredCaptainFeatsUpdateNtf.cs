﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptainFeatsUpdateNtf")]
    public class HiredCaptainFeatsUpdateNtf : IExtensible
    {
        private ulong _CaptainInstanceId;
        private readonly List<ProIdLevelInfo> _InBornFeats;
        private readonly List<ProIdLevelInfo> _AdditionFeats;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_InBornFeats;
        private static DelegateBridge __Hotfix_get_AdditionFeats;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="InBornFeats", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> InBornFeats
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="AdditionFeats", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> AdditionFeats
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

