﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ClientSystemPushInfoSyncNtf")]
    public class ClientSystemPushInfoSyncNtf : IExtensible
    {
        private int _ClientLanguageType;
        private string _ClientPushToken;
        private int _ClientOSType;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ClientLanguageType;
        private static DelegateBridge __Hotfix_set_ClientLanguageType;
        private static DelegateBridge __Hotfix_get_ClientPushToken;
        private static DelegateBridge __Hotfix_set_ClientPushToken;
        private static DelegateBridge __Hotfix_get_ClientOSType;
        private static DelegateBridge __Hotfix_set_ClientOSType;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="ClientLanguageType", DataFormat=DataFormat.TwosComplement)]
        public int ClientLanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="ClientPushToken", DataFormat=DataFormat.Default)]
        public string ClientPushToken
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ClientOSType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ClientOSType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

