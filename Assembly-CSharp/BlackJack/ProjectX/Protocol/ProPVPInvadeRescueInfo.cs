﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProPVPInvadeRescueInfo")]
    public class ProPVPInvadeRescueInfo : IExtensible
    {
        private ProSignalInfo _SourceSignalInfo;
        private ProPlayerSimpleInfo _SourcePlayerInfo;
        private readonly List<ProHiredCaptainStaticInfo> _StaticInfo;
        private readonly List<ProHiredCaptainSimpleDynamicInfo> _DynamicInfo;
        private readonly List<ulong> _DestroyCaptainInstanceIdList;
        private ProPlayerSimpleInfo _InvadePlayerInfo;
        private long _InvadeStartTime;
        private int _ManualQuestSolarSystemId;
        private uint _ManualQuestSceneInstanceId;
        private int _ManualQuestId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SourceSignalInfo;
        private static DelegateBridge __Hotfix_set_SourceSignalInfo;
        private static DelegateBridge __Hotfix_get_SourcePlayerInfo;
        private static DelegateBridge __Hotfix_set_SourcePlayerInfo;
        private static DelegateBridge __Hotfix_get_StaticInfo;
        private static DelegateBridge __Hotfix_get_DynamicInfo;
        private static DelegateBridge __Hotfix_get_DestroyCaptainInstanceIdList;
        private static DelegateBridge __Hotfix_get_InvadePlayerInfo;
        private static DelegateBridge __Hotfix_set_InvadePlayerInfo;
        private static DelegateBridge __Hotfix_get_InvadeStartTime;
        private static DelegateBridge __Hotfix_set_InvadeStartTime;
        private static DelegateBridge __Hotfix_get_ManualQuestSolarSystemId;
        private static DelegateBridge __Hotfix_set_ManualQuestSolarSystemId;
        private static DelegateBridge __Hotfix_get_ManualQuestSceneInstanceId;
        private static DelegateBridge __Hotfix_set_ManualQuestSceneInstanceId;
        private static DelegateBridge __Hotfix_get_ManualQuestId;
        private static DelegateBridge __Hotfix_set_ManualQuestId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="SourceSignalInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSignalInfo SourceSignalInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="SourcePlayerInfo", DataFormat=DataFormat.Default)]
        public ProPlayerSimpleInfo SourcePlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="StaticInfo", DataFormat=DataFormat.Default)]
        public List<ProHiredCaptainStaticInfo> StaticInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="DynamicInfo", DataFormat=DataFormat.Default)]
        public List<ProHiredCaptainSimpleDynamicInfo> DynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="DestroyCaptainInstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> DestroyCaptainInstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(6, IsRequired=false, Name="InvadePlayerInfo", DataFormat=DataFormat.Default)]
        public ProPlayerSimpleInfo InvadePlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="InvadeStartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long InvadeStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="ManualQuestSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ManualQuestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="ManualQuestSceneInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ManualQuestSceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="ManualQuestId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ManualQuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

