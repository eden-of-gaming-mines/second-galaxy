﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceNpcTalkerChatNtf")]
    public class InSpaceNpcTalkerChatNtf : IExtensible
    {
        private readonly List<int> _DailogIdList;
        private ProNpcDNId _ReplaceNpc;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DailogIdList;
        private static DelegateBridge __Hotfix_get_ReplaceNpc;
        private static DelegateBridge __Hotfix_set_ReplaceNpc;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="DailogIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> DailogIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ReplaceNpc", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProNpcDNId ReplaceNpc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

