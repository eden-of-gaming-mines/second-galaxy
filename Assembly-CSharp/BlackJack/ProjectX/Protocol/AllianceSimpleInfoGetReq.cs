﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AllianceSimpleInfoGetReq")]
    public class AllianceSimpleInfoGetReq : IExtensible
    {
        private readonly List<uint> _AllianceIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AllianceIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="AllianceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> AllianceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

