﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProRankingListInfo")]
    public class ProRankingListInfo : IExtensible
    {
        private int _Type;
        private int _Score;
        private int _CurrRank;
        private readonly List<ProRankingPlayerInfo> _PlayerList;
        private readonly List<ProRankingGuildInfo> _GuildList;
        private readonly List<ProRankingAllianceInfo> _AllianceList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_Score;
        private static DelegateBridge __Hotfix_set_Score;
        private static DelegateBridge __Hotfix_get_CurrRank;
        private static DelegateBridge __Hotfix_set_CurrRank;
        private static DelegateBridge __Hotfix_get_PlayerList;
        private static DelegateBridge __Hotfix_get_GuildList;
        private static DelegateBridge __Hotfix_get_AllianceList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Score", DataFormat=DataFormat.TwosComplement)]
        public int Score
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CurrRank", DataFormat=DataFormat.TwosComplement)]
        public int CurrRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="PlayerList", DataFormat=DataFormat.Default)]
        public List<ProRankingPlayerInfo> PlayerList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="GuildList", DataFormat=DataFormat.Default)]
        public List<ProRankingGuildInfo> GuildList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="AllianceList", DataFormat=DataFormat.Default)]
        public List<ProRankingAllianceInfo> AllianceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

