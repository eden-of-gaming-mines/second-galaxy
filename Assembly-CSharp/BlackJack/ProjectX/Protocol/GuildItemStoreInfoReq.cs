﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildItemStoreInfoReq")]
    public class GuildItemStoreInfoReq : IExtensible
    {
        private readonly List<uint> _ItemDSVersionList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ItemDSVersionList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ItemDSVersionList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> ItemDSVersionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

