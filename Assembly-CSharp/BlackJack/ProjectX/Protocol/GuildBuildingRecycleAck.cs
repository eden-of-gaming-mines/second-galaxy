﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBuildingRecycleAck")]
    public class GuildBuildingRecycleAck : IExtensible
    {
        private int _Result;
        private ProGuildBuildingInfo _Building;
        private readonly List<ProItemInfo> _ItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Building;
        private static DelegateBridge __Hotfix_set_Building;
        private static DelegateBridge __Hotfix_get_ItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="Building", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildBuildingInfo Building
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ItemList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> ItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

