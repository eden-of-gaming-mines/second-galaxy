﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildItemStoreInfoNtf")]
    public class GuildItemStoreInfoNtf : IExtensible
    {
        private readonly List<ProGuildStoreItemListInfo> _ItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ItemList", DataFormat=DataFormat.Default)]
        public List<ProGuildStoreItemListInfo> ItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

