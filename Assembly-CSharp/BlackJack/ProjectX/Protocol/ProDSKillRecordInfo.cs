﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDSKillRecordInfo")]
    public class ProDSKillRecordInfo : IExtensible
    {
        private int _StartIndex;
        private uint _Version;
        private readonly List<ProKillRecordInfo> _KillRecordList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StartIndex;
        private static DelegateBridge __Hotfix_set_StartIndex;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_KillRecordList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="StartIndex", DataFormat=DataFormat.TwosComplement)]
        public int StartIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="KillRecordList", DataFormat=DataFormat.Default)]
        public List<ProKillRecordInfo> KillRecordList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

