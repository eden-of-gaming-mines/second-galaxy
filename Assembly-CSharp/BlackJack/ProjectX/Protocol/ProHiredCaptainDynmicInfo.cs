﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProHiredCaptainDynmicInfo")]
    public class ProHiredCaptainDynmicInfo : IExtensible
    {
        private long _Exp;
        private int _Level;
        private int _State;
        private readonly List<ProIdLevelInfo> _InitFeats;
        private readonly List<ProIdLevelInfo> _AdditionFeats;
        private int _CurrShipId;
        private readonly List<ProHiredCaptainShipInfo> _AvailableShipList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Exp;
        private static DelegateBridge __Hotfix_set_Exp;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;
        private static DelegateBridge __Hotfix_get_InitFeats;
        private static DelegateBridge __Hotfix_get_AdditionFeats;
        private static DelegateBridge __Hotfix_get_CurrShipId;
        private static DelegateBridge __Hotfix_set_CurrShipId;
        private static DelegateBridge __Hotfix_get_AvailableShipList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Exp", DataFormat=DataFormat.TwosComplement)]
        public long Exp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="State", DataFormat=DataFormat.TwosComplement)]
        public int State
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="InitFeats", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> InitFeats
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="AdditionFeats", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> AdditionFeats
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="CurrShipId", DataFormat=DataFormat.TwosComplement)]
        public int CurrShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="AvailableShipList", DataFormat=DataFormat.Default)]
        public List<ProHiredCaptainShipInfo> AvailableShipList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

