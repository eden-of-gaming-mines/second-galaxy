﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildSearchReq")]
    public class GuildSearchReq : IExtensible
    {
        private string _Key;
        private bool _SearchGuildName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Key;
        private static DelegateBridge __Hotfix_set_Key;
        private static DelegateBridge __Hotfix_get_SearchGuildName;
        private static DelegateBridge __Hotfix_set_SearchGuildName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Key", DataFormat=DataFormat.Default)]
        public string Key
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="SearchGuildName", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool SearchGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

