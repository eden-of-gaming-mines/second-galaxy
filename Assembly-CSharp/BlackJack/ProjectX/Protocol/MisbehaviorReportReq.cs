﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="MisbehaviorReportReq")]
    public class MisbehaviorReportReq : IExtensible
    {
        private string _TargetUserId;
        private string _TargetUserName;
        private int _TargetUserLevel;
        private int _TargetType;
        private string _TargetMessage;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TargetUserId;
        private static DelegateBridge __Hotfix_set_TargetUserId;
        private static DelegateBridge __Hotfix_get_TargetUserName;
        private static DelegateBridge __Hotfix_set_TargetUserName;
        private static DelegateBridge __Hotfix_get_TargetUserLevel;
        private static DelegateBridge __Hotfix_set_TargetUserLevel;
        private static DelegateBridge __Hotfix_get_TargetType;
        private static DelegateBridge __Hotfix_set_TargetType;
        private static DelegateBridge __Hotfix_get_TargetMessage;
        private static DelegateBridge __Hotfix_set_TargetMessage;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TargetUserId", DataFormat=DataFormat.Default)]
        public string TargetUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="TargetUserName", DataFormat=DataFormat.Default)]
        public string TargetUserName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="TargetUserLevel", DataFormat=DataFormat.TwosComplement)]
        public int TargetUserLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="TargetType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int TargetType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="TargetMessage", DataFormat=DataFormat.Default), DefaultValue("")]
        public string TargetMessage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

