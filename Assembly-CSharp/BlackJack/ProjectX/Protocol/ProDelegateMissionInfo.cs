﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDelegateMissionInfo")]
    public class ProDelegateMissionInfo : IExtensible
    {
        private int _State;
        private ProSignalInfo _souceSignal;
        private readonly List<ProHiredCaptainInfo> _HiredCaptainList;
        private readonly List<ProDelegateMissionInvadeInfo> _InvadeInfo;
        private readonly List<ProItemInfo> _CompleteRewardList;
        private readonly List<ProItemInfo> _ProcessRewardList;
        private float _RewardMultiES;
        private long _StartTime;
        private long _CompleteTime;
        private readonly List<ulong> _DestroyCaptainInstanceIdList;
        private ProPlayerSimpleInfo _CurrInvadingPlayerInfo;
        private long _InvadeStartTime;
        private readonly List<int> _HiredCaptainStaticBufList;
        private int _CurrDelegateMissionPoolCount;
        private float _CurrGuildRewardMulti;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;
        private static DelegateBridge __Hotfix_get_SouceSignal;
        private static DelegateBridge __Hotfix_set_SouceSignal;
        private static DelegateBridge __Hotfix_get_HiredCaptainList;
        private static DelegateBridge __Hotfix_get_InvadeInfo;
        private static DelegateBridge __Hotfix_get_CompleteRewardList;
        private static DelegateBridge __Hotfix_get_ProcessRewardList;
        private static DelegateBridge __Hotfix_get_RewardMultiES;
        private static DelegateBridge __Hotfix_set_RewardMultiES;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_CompleteTime;
        private static DelegateBridge __Hotfix_set_CompleteTime;
        private static DelegateBridge __Hotfix_get_DestroyCaptainInstanceIdList;
        private static DelegateBridge __Hotfix_get_CurrInvadingPlayerInfo;
        private static DelegateBridge __Hotfix_set_CurrInvadingPlayerInfo;
        private static DelegateBridge __Hotfix_get_InvadeStartTime;
        private static DelegateBridge __Hotfix_set_InvadeStartTime;
        private static DelegateBridge __Hotfix_get_HiredCaptainStaticBufList;
        private static DelegateBridge __Hotfix_get_CurrDelegateMissionPoolCount;
        private static DelegateBridge __Hotfix_set_CurrDelegateMissionPoolCount;
        private static DelegateBridge __Hotfix_get_CurrGuildRewardMulti;
        private static DelegateBridge __Hotfix_set_CurrGuildRewardMulti;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="State", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int State
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="souceSignal", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSignalInfo SouceSignal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="HiredCaptainList", DataFormat=DataFormat.Default)]
        public List<ProHiredCaptainInfo> HiredCaptainList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="InvadeInfo", DataFormat=DataFormat.Default)]
        public List<ProDelegateMissionInvadeInfo> InvadeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="CompleteRewardList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> CompleteRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="ProcessRewardList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> ProcessRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(8, IsRequired=false, Name="RewardMultiES", DataFormat=DataFormat.FixedSize)]
        public float RewardMultiES
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="StartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(10, IsRequired=false, Name="CompleteTime", DataFormat=DataFormat.TwosComplement)]
        public long CompleteTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="DestroyCaptainInstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> DestroyCaptainInstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(12, IsRequired=false, Name="CurrInvadingPlayerInfo", DataFormat=DataFormat.Default)]
        public ProPlayerSimpleInfo CurrInvadingPlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(13, IsRequired=false, Name="InvadeStartTime", DataFormat=DataFormat.TwosComplement)]
        public long InvadeStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, Name="HiredCaptainStaticBufList", DataFormat=DataFormat.TwosComplement)]
        public List<int> HiredCaptainStaticBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(15, IsRequired=false, Name="CurrDelegateMissionPoolCount", DataFormat=DataFormat.TwosComplement)]
        public int CurrDelegateMissionPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(0x10, IsRequired=false, Name="CurrGuildRewardMulti", DataFormat=DataFormat.FixedSize)]
        public float CurrGuildRewardMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

