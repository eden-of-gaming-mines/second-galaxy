﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CustomizedParameterUpdateNtf")]
    public class CustomizedParameterUpdateNtf : IExtensible
    {
        private readonly List<ProCustomizedParameterInfo> _CustomizedParameterList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CustomizedParameterList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="CustomizedParameterList", DataFormat=DataFormat.Default)]
        public List<ProCustomizedParameterInfo> CustomizedParameterList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

