﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProHangerInfo")]
    public class ProHangerInfo : IExtensible
    {
        private BlackJack.ProjectX.Protocol.ProShipDetailInfo _proShipDetailInfo;
        private uint _ArmorCurr;
        private uint _ShieldCurr;
        private uint _EnergyCurr;
        private int _Index;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ProShipDetailInfo;
        private static DelegateBridge __Hotfix_set_ProShipDetailInfo;
        private static DelegateBridge __Hotfix_get_ArmorCurr;
        private static DelegateBridge __Hotfix_set_ArmorCurr;
        private static DelegateBridge __Hotfix_get_ShieldCurr;
        private static DelegateBridge __Hotfix_set_ShieldCurr;
        private static DelegateBridge __Hotfix_get_EnergyCurr;
        private static DelegateBridge __Hotfix_set_EnergyCurr;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="proShipDetailInfo", DataFormat=DataFormat.Default)]
        public BlackJack.ProjectX.Protocol.ProShipDetailInfo ProShipDetailInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ArmorCurr", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ArmorCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ShieldCurr", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ShieldCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="EnergyCurr", DataFormat=DataFormat.TwosComplement)]
        public uint EnergyCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="Index", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

