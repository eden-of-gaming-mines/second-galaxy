﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProtoTypeShipListAck")]
    public class ProtoTypeShipListAck : IExtensible
    {
        private readonly List<ProShipInstanceData> _ShipList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ShipList", DataFormat=DataFormat.Default)]
        public List<ProShipInstanceData> ShipList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [Serializable, ProtoContract(Name="ProShipInstanceData")]
        public class ProShipInstanceData : IExtensible
        {
            private int _Id;
            private ulong _InsId;
            private string _ShipName;
            private int _SolarSystemId;
            private int _StationId;
            private IExtension extensionObject;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_Id;
            private static DelegateBridge __Hotfix_set_Id;
            private static DelegateBridge __Hotfix_get_InsId;
            private static DelegateBridge __Hotfix_set_InsId;
            private static DelegateBridge __Hotfix_get_ShipName;
            private static DelegateBridge __Hotfix_set_ShipName;
            private static DelegateBridge __Hotfix_get_SolarSystemId;
            private static DelegateBridge __Hotfix_set_SolarSystemId;
            private static DelegateBridge __Hotfix_get_StationId;
            private static DelegateBridge __Hotfix_set_StationId;
            private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

            [MethodImpl(0x8000)]
            IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            {
            }

            [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
            public int Id
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(2, IsRequired=true, Name="InsId", DataFormat=DataFormat.TwosComplement)]
            public ulong InsId
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(3, IsRequired=true, Name="ShipName", DataFormat=DataFormat.Default)]
            public string ShipName
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(4, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
            public int SolarSystemId
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(5, IsRequired=true, Name="StationId", DataFormat=DataFormat.TwosComplement)]
            public int StationId
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }
        }
    }
}

