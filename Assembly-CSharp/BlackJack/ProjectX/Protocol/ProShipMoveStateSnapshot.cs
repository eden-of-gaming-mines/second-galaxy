﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipMoveStateSnapshot")]
    public class ProShipMoveStateSnapshot : IExtensible
    {
        private ProBaseMoveStateSnapshot _Basic;
        private ProShipMoveCmdInfo _MoveCmd;
        private ProJumpCtxSnapshot _jumpSnapshot;
        private ProArroundTargetCtxSnapshot _arroundSnapshot;
        private float _CurrArmor;
        private float _CurrShield;
        private float _CurrEnergy;
        private float _SuperWeaponEneryg;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Basic;
        private static DelegateBridge __Hotfix_set_Basic;
        private static DelegateBridge __Hotfix_get_MoveCmd;
        private static DelegateBridge __Hotfix_set_MoveCmd;
        private static DelegateBridge __Hotfix_get_JumpSnapshot;
        private static DelegateBridge __Hotfix_set_JumpSnapshot;
        private static DelegateBridge __Hotfix_get_ArroundSnapshot;
        private static DelegateBridge __Hotfix_set_ArroundSnapshot;
        private static DelegateBridge __Hotfix_get_CurrArmor;
        private static DelegateBridge __Hotfix_set_CurrArmor;
        private static DelegateBridge __Hotfix_get_CurrShield;
        private static DelegateBridge __Hotfix_set_CurrShield;
        private static DelegateBridge __Hotfix_get_CurrEnergy;
        private static DelegateBridge __Hotfix_set_CurrEnergy;
        private static DelegateBridge __Hotfix_get_SuperWeaponEneryg;
        private static DelegateBridge __Hotfix_set_SuperWeaponEneryg;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Basic", DataFormat=DataFormat.Default)]
        public ProBaseMoveStateSnapshot Basic
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="MoveCmd", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProShipMoveCmdInfo MoveCmd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="jumpSnapshot", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProJumpCtxSnapshot JumpSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="arroundSnapshot", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProArroundTargetCtxSnapshot ArroundSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(6, IsRequired=false, Name="CurrArmor", DataFormat=DataFormat.FixedSize)]
        public float CurrArmor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(7, IsRequired=false, Name="CurrShield", DataFormat=DataFormat.FixedSize)]
        public float CurrShield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(8, IsRequired=false, Name="CurrEnergy", DataFormat=DataFormat.FixedSize)]
        public float CurrEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="SuperWeaponEneryg", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float SuperWeaponEneryg
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

