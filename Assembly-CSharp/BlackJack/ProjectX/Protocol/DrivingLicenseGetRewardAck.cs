﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DrivingLicenseGetRewardAck")]
    public class DrivingLicenseGetRewardAck : IExtensible
    {
        private int _Result;
        private int _DrivingLicenseId;
        private readonly List<ProStoreItemUpdateInfo> _UpdateItemList;
        private ProDSShipHangarInfo _UpdateHangarInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_DrivingLicenseId;
        private static DelegateBridge __Hotfix_set_DrivingLicenseId;
        private static DelegateBridge __Hotfix_get_UpdateItemList;
        private static DelegateBridge __Hotfix_get_UpdateHangarInfo;
        private static DelegateBridge __Hotfix_set_UpdateHangarInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DrivingLicenseId", DataFormat=DataFormat.TwosComplement)]
        public int DrivingLicenseId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="UpdateItemList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> UpdateItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="UpdateHangarInfo", DataFormat=DataFormat.Default)]
        public ProDSShipHangarInfo UpdateHangarInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

