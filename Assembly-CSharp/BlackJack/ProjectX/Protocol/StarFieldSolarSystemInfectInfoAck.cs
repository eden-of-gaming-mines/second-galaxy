﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StarFieldSolarSystemInfectInfoAck")]
    public class StarFieldSolarSystemInfectInfoAck : IExtensible
    {
        private int _SolarSystemId;
        private float _Progress;
        private bool _FinalBattleOpen;
        private long _SelfHealingTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_Progress;
        private static DelegateBridge __Hotfix_set_Progress;
        private static DelegateBridge __Hotfix_get_FinalBattleOpen;
        private static DelegateBridge __Hotfix_set_FinalBattleOpen;
        private static DelegateBridge __Hotfix_get_SelfHealingTime;
        private static DelegateBridge __Hotfix_set_SelfHealingTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Progress", DataFormat=DataFormat.FixedSize)]
        public float Progress
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="FinalBattleOpen", DataFormat=DataFormat.Default)]
        public bool FinalBattleOpen
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="SelfHealingTime", DataFormat=DataFormat.TwosComplement)]
        public long SelfHealingTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

