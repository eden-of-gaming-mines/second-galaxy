﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildCurrencyInfo")]
    public class ProGuildCurrencyInfo : IExtensible
    {
        private ulong _TradeMoney;
        private ulong _InformationPoint;
        private ulong _TacticalPoint;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TradeMoney;
        private static DelegateBridge __Hotfix_set_TradeMoney;
        private static DelegateBridge __Hotfix_get_InformationPoint;
        private static DelegateBridge __Hotfix_set_InformationPoint;
        private static DelegateBridge __Hotfix_get_TacticalPoint;
        private static DelegateBridge __Hotfix_set_TacticalPoint;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="TradeMoney", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong TradeMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="InformationPoint", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong InformationPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(3, IsRequired=false, Name="TacticalPoint", DataFormat=DataFormat.TwosComplement)]
        public ulong TacticalPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

