﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CharactorObserverAck")]
    public class CharactorObserverAck : IExtensible
    {
        private int _Result;
        private string _CharacterName;
        private int _CharacterLevel;
        private ulong _CharacterExp;
        private readonly List<ProCharChipSlotInfo> _ChipSlotList;
        private int _FreePropertyPoint;
        private float _PropertiesCharactorAttack;
        private float _PropertiesCharactorDefence;
        private float _PropertiesCharactorElectron;
        private float _PropertiesCharactorDrive;
        private float _PropertiesTotalCharactorAttack;
        private float _PropertiesTotalCharactorDefence;
        private float _PropertiesTotalCharactorElectron;
        private float _PropertiesTotalCharactorDrive;
        private int _Profession;
        private string _DestGameUserId;
        private int _AvatarId;
        private float _CriminalLevel;
        private int _CharacterEvaluateScore;
        private uint _GuildId;
        private string _GuildName;
        private string _GuildCodeName;
        private ProGuildLogoInfo _GuildLogo;
        private uint _AllianceId;
        private string _AllianceName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_CharacterName;
        private static DelegateBridge __Hotfix_set_CharacterName;
        private static DelegateBridge __Hotfix_get_CharacterLevel;
        private static DelegateBridge __Hotfix_set_CharacterLevel;
        private static DelegateBridge __Hotfix_get_CharacterExp;
        private static DelegateBridge __Hotfix_set_CharacterExp;
        private static DelegateBridge __Hotfix_get_ChipSlotList;
        private static DelegateBridge __Hotfix_get_FreePropertyPoint;
        private static DelegateBridge __Hotfix_set_FreePropertyPoint;
        private static DelegateBridge __Hotfix_get_PropertiesCharactorAttack;
        private static DelegateBridge __Hotfix_set_PropertiesCharactorAttack;
        private static DelegateBridge __Hotfix_get_PropertiesCharactorDefence;
        private static DelegateBridge __Hotfix_set_PropertiesCharactorDefence;
        private static DelegateBridge __Hotfix_get_PropertiesCharactorElectron;
        private static DelegateBridge __Hotfix_set_PropertiesCharactorElectron;
        private static DelegateBridge __Hotfix_get_PropertiesCharactorDrive;
        private static DelegateBridge __Hotfix_set_PropertiesCharactorDrive;
        private static DelegateBridge __Hotfix_get_PropertiesTotalCharactorAttack;
        private static DelegateBridge __Hotfix_set_PropertiesTotalCharactorAttack;
        private static DelegateBridge __Hotfix_get_PropertiesTotalCharactorDefence;
        private static DelegateBridge __Hotfix_set_PropertiesTotalCharactorDefence;
        private static DelegateBridge __Hotfix_get_PropertiesTotalCharactorElectron;
        private static DelegateBridge __Hotfix_set_PropertiesTotalCharactorElectron;
        private static DelegateBridge __Hotfix_get_PropertiesTotalCharactorDrive;
        private static DelegateBridge __Hotfix_set_PropertiesTotalCharactorDrive;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_DestGameUserId;
        private static DelegateBridge __Hotfix_set_DestGameUserId;
        private static DelegateBridge __Hotfix_get_AvatarId;
        private static DelegateBridge __Hotfix_set_AvatarId;
        private static DelegateBridge __Hotfix_get_CriminalLevel;
        private static DelegateBridge __Hotfix_set_CriminalLevel;
        private static DelegateBridge __Hotfix_get_CharacterEvaluateScore;
        private static DelegateBridge __Hotfix_set_CharacterEvaluateScore;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_GuildName;
        private static DelegateBridge __Hotfix_set_GuildName;
        private static DelegateBridge __Hotfix_get_GuildCodeName;
        private static DelegateBridge __Hotfix_set_GuildCodeName;
        private static DelegateBridge __Hotfix_get_GuildLogo;
        private static DelegateBridge __Hotfix_set_GuildLogo;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_set_AllianceName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="CharacterName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string CharacterName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="CharacterLevel", DataFormat=DataFormat.TwosComplement)]
        public int CharacterLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CharacterExp", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong CharacterExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="ChipSlotList", DataFormat=DataFormat.Default)]
        public List<ProCharChipSlotInfo> ChipSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="FreePropertyPoint", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FreePropertyPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="PropertiesCharactorAttack", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float PropertiesCharactorAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="PropertiesCharactorDefence", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float PropertiesCharactorDefence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(9, IsRequired=false, Name="PropertiesCharactorElectron", DataFormat=DataFormat.FixedSize)]
        public float PropertiesCharactorElectron
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(10, IsRequired=false, Name="PropertiesCharactorDrive", DataFormat=DataFormat.FixedSize)]
        public float PropertiesCharactorDrive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(11, IsRequired=false, Name="PropertiesTotalCharactorAttack", DataFormat=DataFormat.FixedSize)]
        public float PropertiesTotalCharactorAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(12, IsRequired=false, Name="PropertiesTotalCharactorDefence", DataFormat=DataFormat.FixedSize)]
        public float PropertiesTotalCharactorDefence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="PropertiesTotalCharactorElectron", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float PropertiesTotalCharactorElectron
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="PropertiesTotalCharactorDrive", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float PropertiesTotalCharactorDrive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=false, Name="Profession", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="DestGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string DestGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=false, Name="AvatarId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(0x12, IsRequired=false, Name="CriminalLevel", DataFormat=DataFormat.FixedSize)]
        public float CriminalLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=false, Name="CharacterEvaluateScore", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CharacterEvaluateScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=false, Name="GuildName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string GuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=false, Name="GuildCodeName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string GuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x17, IsRequired=false, Name="GuildLogo", DataFormat=DataFormat.Default)]
        public ProGuildLogoInfo GuildLogo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x18, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(0x19, IsRequired=false, Name="AllianceName", DataFormat=DataFormat.Default)]
        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

