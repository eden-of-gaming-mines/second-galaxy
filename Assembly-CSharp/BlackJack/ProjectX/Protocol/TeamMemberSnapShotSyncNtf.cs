﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TeamMemberSnapShotSyncNtf")]
    public class TeamMemberSnapShotSyncNtf : IExtensible
    {
        private ProTeamMemberInfo _MemberSnapShot;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MemberSnapShot;
        private static DelegateBridge __Hotfix_set_MemberSnapShot;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="MemberSnapShot", DataFormat=DataFormat.Default)]
        public ProTeamMemberInfo MemberSnapShot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

