﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipTransformBatchItemToMSStoreReq")]
    public class HangarShipTransformBatchItemToMSStoreReq : IExtensible
    {
        private int _HangarShipIndex;
        private readonly List<int> _StoreItemIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_StoreItemIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="StoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public List<int> StoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

