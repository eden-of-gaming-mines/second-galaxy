﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProductionLineStartReq")]
    public class ProductionLineStartReq : IExtensible
    {
        private int _BlueprintIndexId;
        private int _BlueprintCount;
        private int _LineIndex;
        private ulong _CaptainInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BlueprintIndexId;
        private static DelegateBridge __Hotfix_set_BlueprintIndexId;
        private static DelegateBridge __Hotfix_get_BlueprintCount;
        private static DelegateBridge __Hotfix_set_BlueprintCount;
        private static DelegateBridge __Hotfix_get_LineIndex;
        private static DelegateBridge __Hotfix_set_LineIndex;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BlueprintIndexId", DataFormat=DataFormat.TwosComplement)]
        public int BlueprintIndexId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BlueprintCount", DataFormat=DataFormat.TwosComplement)]
        public int BlueprintCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LineIndex", DataFormat=DataFormat.TwosComplement)]
        public int LineIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

