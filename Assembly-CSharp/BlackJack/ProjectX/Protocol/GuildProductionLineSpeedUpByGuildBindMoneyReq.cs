﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildProductionLineSpeedUpByGuildBindMoneyReq")]
    public class GuildProductionLineSpeedUpByGuildBindMoneyReq : IExtensible
    {
        private ulong _LineId;
        private int _SpeedUpLevel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LineId;
        private static DelegateBridge __Hotfix_set_LineId;
        private static DelegateBridge __Hotfix_get_SpeedUpLevel;
        private static DelegateBridge __Hotfix_set_SpeedUpLevel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LineId", DataFormat=DataFormat.TwosComplement)]
        public ulong LineId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SpeedUpLevel", DataFormat=DataFormat.TwosComplement)]
        public int SpeedUpLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

