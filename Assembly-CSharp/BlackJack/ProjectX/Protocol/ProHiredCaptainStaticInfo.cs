﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProHiredCaptainStaticInfo")]
    public class ProHiredCaptainStaticInfo : IExtensible
    {
        private ulong _InstanceId;
        private int _ResId;
        private int _LastNameId;
        private int _FirstNameId;
        private int _Profession;
        private int _SubRank;
        private int _GrandFaction;
        private int _Gender;
        private int _Age;
        private long _BirthDay;
        private int _Constellation;
        private int _Personality;
        private int _ShipGrowthLineId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_ResId;
        private static DelegateBridge __Hotfix_set_ResId;
        private static DelegateBridge __Hotfix_get_LastNameId;
        private static DelegateBridge __Hotfix_set_LastNameId;
        private static DelegateBridge __Hotfix_get_FirstNameId;
        private static DelegateBridge __Hotfix_set_FirstNameId;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_SubRank;
        private static DelegateBridge __Hotfix_set_SubRank;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_Gender;
        private static DelegateBridge __Hotfix_set_Gender;
        private static DelegateBridge __Hotfix_get_Age;
        private static DelegateBridge __Hotfix_set_Age;
        private static DelegateBridge __Hotfix_get_BirthDay;
        private static DelegateBridge __Hotfix_set_BirthDay;
        private static DelegateBridge __Hotfix_get_Constellation;
        private static DelegateBridge __Hotfix_set_Constellation;
        private static DelegateBridge __Hotfix_get_Personality;
        private static DelegateBridge __Hotfix_set_Personality;
        private static DelegateBridge __Hotfix_get_ShipGrowthLineId;
        private static DelegateBridge __Hotfix_set_ShipGrowthLineId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ResId", DataFormat=DataFormat.TwosComplement)]
        public int ResId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LastNameId", DataFormat=DataFormat.TwosComplement)]
        public int LastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="FirstNameId", DataFormat=DataFormat.TwosComplement)]
        public int FirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SubRank", DataFormat=DataFormat.TwosComplement)]
        public int SubRank
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public int GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Gender", DataFormat=DataFormat.TwosComplement)]
        public int Gender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Age", DataFormat=DataFormat.TwosComplement)]
        public int Age
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="BirthDay", DataFormat=DataFormat.TwosComplement)]
        public long BirthDay
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="Constellation", DataFormat=DataFormat.TwosComplement)]
        public int Constellation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="Personality", DataFormat=DataFormat.TwosComplement)]
        public int Personality
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="ShipGrowthLineId", DataFormat=DataFormat.TwosComplement)]
        public int ShipGrowthLineId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

