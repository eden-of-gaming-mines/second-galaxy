﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMemberListInfoReq")]
    public class GuildMemberListInfoReq : IExtensible
    {
        private uint _BasicVersion;
        private uint _DynamicVersion;
        private uint _RuntimeVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BasicVersion;
        private static DelegateBridge __Hotfix_set_BasicVersion;
        private static DelegateBridge __Hotfix_get_DynamicVersion;
        private static DelegateBridge __Hotfix_set_DynamicVersion;
        private static DelegateBridge __Hotfix_get_RuntimeVersion;
        private static DelegateBridge __Hotfix_set_RuntimeVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BasicVersion", DataFormat=DataFormat.TwosComplement)]
        public uint BasicVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DynamicVersion", DataFormat=DataFormat.TwosComplement)]
        public uint DynamicVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="RuntimeVersion", DataFormat=DataFormat.TwosComplement)]
        public uint RuntimeVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

