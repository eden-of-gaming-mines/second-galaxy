﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GlobalSceneSyncNtf")]
    public class GlobalSceneSyncNtf : IExtensible
    {
        private readonly List<ProGlobalSceneInfo> _GlobalSceneList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GlobalSceneList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="GlobalSceneList", DataFormat=DataFormat.Default)]
        public List<ProGlobalSceneInfo> GlobalSceneList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

