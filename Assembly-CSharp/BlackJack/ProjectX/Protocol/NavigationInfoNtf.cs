﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="NavigationInfoNtf")]
    public class NavigationInfoNtf : IExtensible
    {
        private int _StartSolarSystemId;
        private int _EndSolarSystemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StartSolarSystemId;
        private static DelegateBridge __Hotfix_set_StartSolarSystemId;
        private static DelegateBridge __Hotfix_get_EndSolarSystemId;
        private static DelegateBridge __Hotfix_set_EndSolarSystemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="StartSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int StartSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="EndSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int EndSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

