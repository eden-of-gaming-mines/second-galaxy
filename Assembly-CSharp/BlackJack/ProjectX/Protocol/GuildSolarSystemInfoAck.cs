﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildSolarSystemInfoAck")]
    public class GuildSolarSystemInfoAck : IExtensible
    {
        private int _Result;
        private int _SolarSystemId;
        private int _CurrVersion;
        private int _FlourishValue;
        private uint _OccupiedGuildId;
        private string _OccupiedGuildName;
        private string _OccupiedGuildCodeName;
        private ProGuildLogoInfo _OccupiedGuildLogoInfo;
        private int _BuildingVersion;
        private readonly List<ProGuildBuildingInfo> _BuildingList;
        private readonly List<ProGuildBuildingExInfo> _BuildingExInfo;
        private ProGuildBattleClientSyncInfo _SovereignWarInfo;
        private string _OccupiedGuildManifesto;
        private long _NextSovereignBattleStartMinTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_CurrVersion;
        private static DelegateBridge __Hotfix_set_CurrVersion;
        private static DelegateBridge __Hotfix_get_FlourishValue;
        private static DelegateBridge __Hotfix_set_FlourishValue;
        private static DelegateBridge __Hotfix_get_OccupiedGuildId;
        private static DelegateBridge __Hotfix_set_OccupiedGuildId;
        private static DelegateBridge __Hotfix_get_OccupiedGuildName;
        private static DelegateBridge __Hotfix_set_OccupiedGuildName;
        private static DelegateBridge __Hotfix_get_OccupiedGuildCodeName;
        private static DelegateBridge __Hotfix_set_OccupiedGuildCodeName;
        private static DelegateBridge __Hotfix_get_OccupiedGuildLogoInfo;
        private static DelegateBridge __Hotfix_set_OccupiedGuildLogoInfo;
        private static DelegateBridge __Hotfix_get_BuildingVersion;
        private static DelegateBridge __Hotfix_set_BuildingVersion;
        private static DelegateBridge __Hotfix_get_BuildingList;
        private static DelegateBridge __Hotfix_get_BuildingExInfo;
        private static DelegateBridge __Hotfix_get_SovereignWarInfo;
        private static DelegateBridge __Hotfix_set_SovereignWarInfo;
        private static DelegateBridge __Hotfix_get_OccupiedGuildManifesto;
        private static DelegateBridge __Hotfix_set_OccupiedGuildManifesto;
        private static DelegateBridge __Hotfix_get_NextSovereignBattleStartMinTime;
        private static DelegateBridge __Hotfix_set_NextSovereignBattleStartMinTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="CurrVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="FlourishValue", DataFormat=DataFormat.TwosComplement)]
        public int FlourishValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="OccupiedGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint OccupiedGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="OccupiedGuildName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OccupiedGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(7, IsRequired=false, Name="OccupiedGuildCodeName", DataFormat=DataFormat.Default)]
        public string OccupiedGuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="OccupiedGuildLogoInfo", DataFormat=DataFormat.Default)]
        public ProGuildLogoInfo OccupiedGuildLogoInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="BuildingVersion", DataFormat=DataFormat.TwosComplement)]
        public int BuildingVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="BuildingList", DataFormat=DataFormat.Default)]
        public List<ProGuildBuildingInfo> BuildingList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, Name="BuildingExInfo", DataFormat=DataFormat.Default)]
        public List<ProGuildBuildingExInfo> BuildingExInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="SovereignWarInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildBattleClientSyncInfo SovereignWarInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="OccupiedGuildManifesto", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OccupiedGuildManifesto
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(14, IsRequired=false, Name="NextSovereignBattleStartMinTime", DataFormat=DataFormat.TwosComplement)]
        public long NextSovereignBattleStartMinTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

