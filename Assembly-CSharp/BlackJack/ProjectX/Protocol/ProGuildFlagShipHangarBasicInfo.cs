﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFlagShipHangarBasicInfo")]
    public class ProGuildFlagShipHangarBasicInfo : IExtensible
    {
        private bool _IsDestroyed;
        private bool _IsRecycled;
        private int _HangarBuildingLevel;
        private int _SolarSystemId;
        private long _DeployTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsDestroyed;
        private static DelegateBridge __Hotfix_set_IsDestroyed;
        private static DelegateBridge __Hotfix_get_IsRecycled;
        private static DelegateBridge __Hotfix_set_IsRecycled;
        private static DelegateBridge __Hotfix_get_HangarBuildingLevel;
        private static DelegateBridge __Hotfix_set_HangarBuildingLevel;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_DeployTime;
        private static DelegateBridge __Hotfix_set_DeployTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsDestroyed", DataFormat=DataFormat.Default)]
        public bool IsDestroyed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="IsRecycled", DataFormat=DataFormat.Default)]
        public bool IsRecycled
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="HangarBuildingLevel", DataFormat=DataFormat.TwosComplement)]
        public int HangarBuildingLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="DeployTime", DataFormat=DataFormat.TwosComplement)]
        public long DeployTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

