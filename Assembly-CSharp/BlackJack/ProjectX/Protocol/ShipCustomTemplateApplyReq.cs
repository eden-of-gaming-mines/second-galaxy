﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipCustomTemplateApplyReq")]
    public class ShipCustomTemplateApplyReq : IExtensible
    {
        private bool _IsPredefineTemplate;
        private int _TemplateIndex;
        private ulong _ShipInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsPredefineTemplate;
        private static DelegateBridge __Hotfix_set_IsPredefineTemplate;
        private static DelegateBridge __Hotfix_get_TemplateIndex;
        private static DelegateBridge __Hotfix_set_TemplateIndex;
        private static DelegateBridge __Hotfix_get_ShipInstanceId;
        private static DelegateBridge __Hotfix_set_ShipInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsPredefineTemplate", DataFormat=DataFormat.Default)]
        public bool IsPredefineTemplate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="TemplateIndex", DataFormat=DataFormat.TwosComplement)]
        public int TemplateIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

