﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AuctionItemBuyAck")]
    public class AuctionItemBuyAck : IExtensible
    {
        private int _Result;
        private AuctionItemBuyReq _req;
        private ProStoreItemUpdateInfo _UpdateItem;
        private ProCurrencyUpdateInfo _UpdateCurrency;
        private readonly List<ProAuctionOrderBriefInfo> _OrderListInfo;
        private long _MarketPrice;
        private float _TradeTax;
        private int _TotalItemCount;
        private int _ItemFreezingTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Req;
        private static DelegateBridge __Hotfix_set_Req;
        private static DelegateBridge __Hotfix_get_UpdateItem;
        private static DelegateBridge __Hotfix_set_UpdateItem;
        private static DelegateBridge __Hotfix_get_UpdateCurrency;
        private static DelegateBridge __Hotfix_set_UpdateCurrency;
        private static DelegateBridge __Hotfix_get_OrderListInfo;
        private static DelegateBridge __Hotfix_get_MarketPrice;
        private static DelegateBridge __Hotfix_set_MarketPrice;
        private static DelegateBridge __Hotfix_get_TradeTax;
        private static DelegateBridge __Hotfix_set_TradeTax;
        private static DelegateBridge __Hotfix_get_TotalItemCount;
        private static DelegateBridge __Hotfix_set_TotalItemCount;
        private static DelegateBridge __Hotfix_get_ItemFreezingTime;
        private static DelegateBridge __Hotfix_set_ItemFreezingTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="req", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public AuctionItemBuyReq Req
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="UpdateItem", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProStoreItemUpdateInfo UpdateItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="UpdateCurrency", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProCurrencyUpdateInfo UpdateCurrency
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="OrderListInfo", DataFormat=DataFormat.Default)]
        public List<ProAuctionOrderBriefInfo> OrderListInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="MarketPrice", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long MarketPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="TradeTax", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float TradeTax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="TotalItemCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int TotalItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="ItemFreezingTime", DataFormat=DataFormat.TwosComplement)]
        public int ItemFreezingTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

