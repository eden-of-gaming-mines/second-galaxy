﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMemberInvadedEndNtf")]
    public class GuildMemberInvadedEndNtf : IExtensible
    {
        private ulong _SignalInstanceId;
        private uint _ManualQuestSceneInstanceId;
        private int _ManualQuestSolarSystemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SignalInstanceId;
        private static DelegateBridge __Hotfix_set_SignalInstanceId;
        private static DelegateBridge __Hotfix_get_ManualQuestSceneInstanceId;
        private static DelegateBridge __Hotfix_set_ManualQuestSceneInstanceId;
        private static DelegateBridge __Hotfix_get_ManualQuestSolarSystemId;
        private static DelegateBridge __Hotfix_set_ManualQuestSolarSystemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="SignalInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong SignalInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="ManualQuestSceneInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint ManualQuestSceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ManualQuestSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ManualQuestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

