﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildDiplomacyDetailInfoAck")]
    public class GuildDiplomacyDetailInfoAck : IExtensible
    {
        private int _Result;
        private bool _FriendlyList;
        private bool _EnemyList;
        private uint _Version;
        private readonly List<ProPlayerSimplestInfo> _PlayerInfo;
        private readonly List<ProGuildSimplestInfo> _GuildInfo;
        private readonly List<ProAllianceBasicInfo> _AllianceInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_FriendlyList;
        private static DelegateBridge __Hotfix_set_FriendlyList;
        private static DelegateBridge __Hotfix_get_EnemyList;
        private static DelegateBridge __Hotfix_set_EnemyList;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_PlayerInfo;
        private static DelegateBridge __Hotfix_get_GuildInfo;
        private static DelegateBridge __Hotfix_get_AllianceInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(2, IsRequired=false, Name="FriendlyList", DataFormat=DataFormat.Default)]
        public bool FriendlyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="EnemyList", DataFormat=DataFormat.Default)]
        public bool EnemyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="PlayerInfo", DataFormat=DataFormat.Default)]
        public List<ProPlayerSimplestInfo> PlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="GuildInfo", DataFormat=DataFormat.Default)]
        public List<ProGuildSimplestInfo> GuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="AllianceInfo", DataFormat=DataFormat.Default)]
        public List<ProAllianceBasicInfo> AllianceInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

