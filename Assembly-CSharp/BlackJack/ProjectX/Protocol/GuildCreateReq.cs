﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildCreateReq")]
    public class GuildCreateReq : IExtensible
    {
        private string _Name;
        private string _CodeName;
        private int _LanguageType;
        private int _JoinPllicy;
        private ProGuildLogoInfo _LogoInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_CodeName;
        private static DelegateBridge __Hotfix_set_CodeName;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_set_LanguageType;
        private static DelegateBridge __Hotfix_get_JoinPllicy;
        private static DelegateBridge __Hotfix_set_JoinPllicy;
        private static DelegateBridge __Hotfix_get_LogoInfo;
        private static DelegateBridge __Hotfix_set_LogoInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CodeName", DataFormat=DataFormat.Default)]
        public string CodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LanguageType", DataFormat=DataFormat.TwosComplement)]
        public int LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="JoinPllicy", DataFormat=DataFormat.TwosComplement)]
        public int JoinPllicy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="LogoInfo", DataFormat=DataFormat.Default)]
        public ProGuildLogoInfo LogoInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

