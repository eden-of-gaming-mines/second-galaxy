﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventResetTacticalEquipGroupCD")]
    public class ProLBSyncEventResetTacticalEquipGroupCD : IExtensible
    {
        private uint _TacticalEquipGroupCDEndTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TacticalEquipGroupCDEndTime;
        private static DelegateBridge __Hotfix_set_TacticalEquipGroupCDEndTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((long) 0L), ProtoMember(1, IsRequired=false, Name="TacticalEquipGroupCDEndTime", DataFormat=DataFormat.TwosComplement)]
        public uint TacticalEquipGroupCDEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

