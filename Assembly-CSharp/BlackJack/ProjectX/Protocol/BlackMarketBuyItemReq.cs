﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BlackMarketBuyItemReq")]
    public class BlackMarketBuyItemReq : IExtensible
    {
        private int _ShopItemId;
        private int _ShopItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShopItemId;
        private static DelegateBridge __Hotfix_set_ShopItemId;
        private static DelegateBridge __Hotfix_get_ShopItemCount;
        private static DelegateBridge __Hotfix_set_ShopItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ShopItemId", DataFormat=DataFormat.TwosComplement)]
        public int ShopItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ShopItemCount", DataFormat=DataFormat.TwosComplement)]
        public int ShopItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

