﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProFactionInfo")]
    public class ProFactionInfo : IExtensible
    {
        private long _CreditQuestWeeklyResetTime;
        private int _FactionCreditQuestCompleteCountChange;
        private long _NextFactionCreditQuestAcceptableTime;
        private ProIdValueInfo _FactionCreditNpcTaskRefreshTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CreditQuestWeeklyResetTime;
        private static DelegateBridge __Hotfix_set_CreditQuestWeeklyResetTime;
        private static DelegateBridge __Hotfix_get_FactionCreditQuestCompleteCountChange;
        private static DelegateBridge __Hotfix_set_FactionCreditQuestCompleteCountChange;
        private static DelegateBridge __Hotfix_get_NextFactionCreditQuestAcceptableTime;
        private static DelegateBridge __Hotfix_set_NextFactionCreditQuestAcceptableTime;
        private static DelegateBridge __Hotfix_get_FactionCreditNpcTaskRefreshTime;
        private static DelegateBridge __Hotfix_set_FactionCreditNpcTaskRefreshTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((long) 0L), ProtoMember(1, IsRequired=false, Name="CreditQuestWeeklyResetTime", DataFormat=DataFormat.TwosComplement)]
        public long CreditQuestWeeklyResetTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="FactionCreditQuestCompleteCountChange", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FactionCreditQuestCompleteCountChange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="NextFactionCreditQuestAcceptableTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextFactionCreditQuestAcceptableTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="FactionCreditNpcTaskRefreshTime", DataFormat=DataFormat.Default)]
        public ProIdValueInfo FactionCreditNpcTaskRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

