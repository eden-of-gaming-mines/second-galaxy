﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarDetialInfoReq")]
    public class GuildFlagShipHangarDetialInfoReq : IExtensible
    {
        private ProGuildFlagShipHangarVersionInfo _HangarVersionInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_HangarVersionInfo;
        private static DelegateBridge __Hotfix_set_HangarVersionInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="HangarVersionInfo", DataFormat=DataFormat.Default)]
        public ProGuildFlagShipHangarVersionInfo HangarVersionInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

