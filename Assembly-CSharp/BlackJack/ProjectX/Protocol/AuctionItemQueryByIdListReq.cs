﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AuctionItemQueryByIdListReq")]
    public class AuctionItemQueryByIdListReq : IExtensible
    {
        private readonly List<int> _AuctionItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AuctionItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="AuctionItemList", DataFormat=DataFormat.TwosComplement)]
        public List<int> AuctionItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

