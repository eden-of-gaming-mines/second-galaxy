﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceHiredCaptainShipViewInfo")]
    public class ProSpaceHiredCaptainShipViewInfo : IExtensible
    {
        private int _ConfigId;
        private int _CaptainFirstNameId;
        private int _CaptainLastNameId;
        private string _OwnerPlayerName;
        private string _OwnerPlayerGameUserId;
        private int _NpcShipMonsterLevel;
        private int _Shield;
        private int _Armor;
        private int _Energy;
        private int _TargetNoticeFlag;
        private readonly List<ProBufInfo> _DynamicBufList;
        private ProHiredCaptainInfo _Captain;
        private int _CreateEffectType;
        private uint _MasterShipObjId;
        private readonly List<int> _StaticBufList;
        private uint _OwnerPlayerGuildId;
        private string _GuildCodeName;
        private uint _AllianceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_get_CaptainFirstNameId;
        private static DelegateBridge __Hotfix_set_CaptainFirstNameId;
        private static DelegateBridge __Hotfix_get_CaptainLastNameId;
        private static DelegateBridge __Hotfix_set_CaptainLastNameId;
        private static DelegateBridge __Hotfix_get_OwnerPlayerName;
        private static DelegateBridge __Hotfix_set_OwnerPlayerName;
        private static DelegateBridge __Hotfix_get_OwnerPlayerGameUserId;
        private static DelegateBridge __Hotfix_set_OwnerPlayerGameUserId;
        private static DelegateBridge __Hotfix_get_NpcShipMonsterLevel;
        private static DelegateBridge __Hotfix_set_NpcShipMonsterLevel;
        private static DelegateBridge __Hotfix_get_Shield;
        private static DelegateBridge __Hotfix_set_Shield;
        private static DelegateBridge __Hotfix_get_Armor;
        private static DelegateBridge __Hotfix_set_Armor;
        private static DelegateBridge __Hotfix_get_Energy;
        private static DelegateBridge __Hotfix_set_Energy;
        private static DelegateBridge __Hotfix_get_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_set_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_get_DynamicBufList;
        private static DelegateBridge __Hotfix_get_Captain;
        private static DelegateBridge __Hotfix_set_Captain;
        private static DelegateBridge __Hotfix_get_CreateEffectType;
        private static DelegateBridge __Hotfix_set_CreateEffectType;
        private static DelegateBridge __Hotfix_get_MasterShipObjId;
        private static DelegateBridge __Hotfix_set_MasterShipObjId;
        private static DelegateBridge __Hotfix_get_StaticBufList;
        private static DelegateBridge __Hotfix_get_OwnerPlayerGuildId;
        private static DelegateBridge __Hotfix_set_OwnerPlayerGuildId;
        private static DelegateBridge __Hotfix_get_GuildCodeName;
        private static DelegateBridge __Hotfix_set_GuildCodeName;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ConfigId", DataFormat=DataFormat.TwosComplement)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="CaptainFirstNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CaptainFirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="CaptainLastNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CaptainLastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="OwnerPlayerName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OwnerPlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(5, IsRequired=false, Name="OwnerPlayerGameUserId", DataFormat=DataFormat.Default)]
        public string OwnerPlayerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="NpcShipMonsterLevel", DataFormat=DataFormat.TwosComplement)]
        public int NpcShipMonsterLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="Shield", DataFormat=DataFormat.TwosComplement)]
        public int Shield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Armor", DataFormat=DataFormat.TwosComplement)]
        public int Armor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Energy", DataFormat=DataFormat.TwosComplement)]
        public int Energy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="TargetNoticeFlag", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int TargetNoticeFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="DynamicBufList", DataFormat=DataFormat.Default)]
        public List<ProBufInfo> DynamicBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(12, IsRequired=false, Name="Captain", DataFormat=DataFormat.Default)]
        public ProHiredCaptainInfo Captain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="CreateEffectType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CreateEffectType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="MasterShipObjId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint MasterShipObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, Name="StaticBufList", DataFormat=DataFormat.TwosComplement)]
        public List<int> StaticBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x10, IsRequired=false, Name="OwnerPlayerGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint OwnerPlayerGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(0x11, IsRequired=false, Name="GuildCodeName", DataFormat=DataFormat.Default)]
        public string GuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x12, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

