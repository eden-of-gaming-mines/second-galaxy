﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildOccupiedSolarSystemInfoReq")]
    public class GuildOccupiedSolarSystemInfoReq : IExtensible
    {
        private readonly List<GuildSolarSystemBasicInfoReq> _BasicInfoReqList;
        private readonly List<GuildSolarSystemBuildingInfoReq> _BuildingInfoReqList;
        private readonly List<GuildSolarSystemBattleInfoReq> _BattleInfoReqList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BasicInfoReqList;
        private static DelegateBridge __Hotfix_get_BuildingInfoReqList;
        private static DelegateBridge __Hotfix_get_BattleInfoReqList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="BasicInfoReqList", DataFormat=DataFormat.Default)]
        public List<GuildSolarSystemBasicInfoReq> BasicInfoReqList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="BuildingInfoReqList", DataFormat=DataFormat.Default)]
        public List<GuildSolarSystemBuildingInfoReq> BuildingInfoReqList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="BattleInfoReqList", DataFormat=DataFormat.Default)]
        public List<GuildSolarSystemBattleInfoReq> BattleInfoReqList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

