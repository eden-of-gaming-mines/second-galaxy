﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceNpcDialogNextAck")]
    public class InSpaceNpcDialogNextAck : IExtensible
    {
        private int _Result;
        private ProNpcDialogInfo _ReturnDailog;
        private int _NPCId;
        private uint _SceneInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ReturnDailog;
        private static DelegateBridge __Hotfix_set_ReturnDailog;
        private static DelegateBridge __Hotfix_get_NPCId;
        private static DelegateBridge __Hotfix_set_NPCId;
        private static DelegateBridge __Hotfix_get_SceneInstanceId;
        private static DelegateBridge __Hotfix_set_SceneInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="ReturnDailog", DataFormat=DataFormat.Default)]
        public ProNpcDialogInfo ReturnDailog
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="NPCId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int NPCId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="SceneInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint SceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

