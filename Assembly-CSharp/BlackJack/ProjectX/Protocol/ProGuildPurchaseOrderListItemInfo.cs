﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildPurchaseOrderListItemInfo")]
    public class ProGuildPurchaseOrderListItemInfo : IExtensible
    {
        private ulong _OrderInstanceId;
        private ProGuildPurchaseOrderInfo _OrderInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OrderInstanceId;
        private static DelegateBridge __Hotfix_set_OrderInstanceId;
        private static DelegateBridge __Hotfix_get_OrderInfo;
        private static DelegateBridge __Hotfix_set_OrderInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="OrderInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong OrderInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="OrderInfo", DataFormat=DataFormat.Default)]
        public ProGuildPurchaseOrderInfo OrderInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

