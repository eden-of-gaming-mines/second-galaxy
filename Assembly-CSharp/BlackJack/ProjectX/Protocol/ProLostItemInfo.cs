﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLostItemInfo")]
    public class ProLostItemInfo : IExtensible
    {
        private ProSimpleItemInfoWithCount _Item;
        private bool _IsShipStoreItem;
        private double _LostBindMoney;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Item;
        private static DelegateBridge __Hotfix_set_Item;
        private static DelegateBridge __Hotfix_get_IsShipStoreItem;
        private static DelegateBridge __Hotfix_set_IsShipStoreItem;
        private static DelegateBridge __Hotfix_get_LostBindMoney;
        private static DelegateBridge __Hotfix_set_LostBindMoney;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Item", DataFormat=DataFormat.Default)]
        public ProSimpleItemInfoWithCount Item
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="IsShipStoreItem", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsShipStoreItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="LostBindMoney", DataFormat=DataFormat.TwosComplement), DefaultValue((double) 0.0)]
        public double LostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

