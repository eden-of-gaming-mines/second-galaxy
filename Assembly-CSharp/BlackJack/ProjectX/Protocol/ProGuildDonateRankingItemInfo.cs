﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildDonateRankingItemInfo")]
    public class ProGuildDonateRankingItemInfo : IExtensible
    {
        private ProPlayerSimplestInfo _Player;
        private ulong _DonateCountCurrWeek;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Player;
        private static DelegateBridge __Hotfix_set_Player;
        private static DelegateBridge __Hotfix_get_DonateCountCurrWeek;
        private static DelegateBridge __Hotfix_set_DonateCountCurrWeek;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Player", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProPlayerSimplestInfo Player
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="DonateCountCurrWeek", DataFormat=DataFormat.TwosComplement)]
        public ulong DonateCountCurrWeek
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

