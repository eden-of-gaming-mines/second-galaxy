﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProBuildingLostReport")]
    public class ProBuildingLostReport : IExtensible
    {
        private ulong _InstanceId;
        private int _SolarSystemId;
        private int _BuildingType;
        private readonly List<ProCostInfo> _LostCostList;
        private long _LostTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_BuildingType;
        private static DelegateBridge __Hotfix_set_BuildingType;
        private static DelegateBridge __Hotfix_get_LostCostList;
        private static DelegateBridge __Hotfix_get_LostTime;
        private static DelegateBridge __Hotfix_set_LostTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="BuildingType", DataFormat=DataFormat.TwosComplement)]
        public int BuildingType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="LostCostList", DataFormat=DataFormat.Default)]
        public List<ProCostInfo> LostCostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="LostTime", DataFormat=DataFormat.TwosComplement)]
        public long LostTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

