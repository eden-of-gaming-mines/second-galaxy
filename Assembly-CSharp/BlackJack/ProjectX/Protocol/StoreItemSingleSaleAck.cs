﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StoreItemSingleSaleAck")]
    public class StoreItemSingleSaleAck : IExtensible
    {
        private int _Result;
        private int _StoreItemIndex;
        private long _StoreItemCount;
        private double _StoreItemSalePrice;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_StoreItemIndex;
        private static DelegateBridge __Hotfix_set_StoreItemIndex;
        private static DelegateBridge __Hotfix_get_StoreItemCount;
        private static DelegateBridge __Hotfix_set_StoreItemCount;
        private static DelegateBridge __Hotfix_get_StoreItemSalePrice;
        private static DelegateBridge __Hotfix_set_StoreItemSalePrice;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="StoreItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int StoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="StoreItemCount", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long StoreItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="StoreItemSalePrice", DataFormat=DataFormat.TwosComplement), DefaultValue((double) 0.0)]
        public double StoreItemSalePrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

