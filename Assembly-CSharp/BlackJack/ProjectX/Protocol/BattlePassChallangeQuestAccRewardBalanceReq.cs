﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BattlePassChallangeQuestAccRewardBalanceReq")]
    public class BattlePassChallangeQuestAccRewardBalanceReq : IExtensible
    {
        private int _AccRewardIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AccRewardIndex;
        private static DelegateBridge __Hotfix_set_AccRewardIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="AccRewardIndex", DataFormat=DataFormat.TwosComplement)]
        public int AccRewardIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

