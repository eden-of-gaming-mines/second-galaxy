﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProPVPSignalInfo")]
    public class ProPVPSignalInfo : IExtensible
    {
        private int _Type;
        private ulong _InstanceId;
        private ProSignalInfo _SourceSignal;
        private readonly List<int> _HiredCaptainShipConfigIdList;
        private int _DynamicScenePlayerShipId;
        private int _RelativeQuestId;
        private int _DestSolarSystemId;
        private ProVectorDouble _Location;
        private ProPlayerSimpleInfo _PlayerInfo;
        private long _ExpiryTime;
        private readonly List<ProItemInfo> _RewardList;
        private uint _DynamicSceneInstanceId;
        private ProFakeNpcInfo _FakeNpcInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_SourceSignal;
        private static DelegateBridge __Hotfix_set_SourceSignal;
        private static DelegateBridge __Hotfix_get_HiredCaptainShipConfigIdList;
        private static DelegateBridge __Hotfix_get_DynamicScenePlayerShipId;
        private static DelegateBridge __Hotfix_set_DynamicScenePlayerShipId;
        private static DelegateBridge __Hotfix_get_RelativeQuestId;
        private static DelegateBridge __Hotfix_set_RelativeQuestId;
        private static DelegateBridge __Hotfix_get_DestSolarSystemId;
        private static DelegateBridge __Hotfix_set_DestSolarSystemId;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_get_PlayerInfo;
        private static DelegateBridge __Hotfix_set_PlayerInfo;
        private static DelegateBridge __Hotfix_get_ExpiryTime;
        private static DelegateBridge __Hotfix_set_ExpiryTime;
        private static DelegateBridge __Hotfix_get_RewardList;
        private static DelegateBridge __Hotfix_get_DynamicSceneInstanceId;
        private static DelegateBridge __Hotfix_set_DynamicSceneInstanceId;
        private static DelegateBridge __Hotfix_get_FakeNpcInfo;
        private static DelegateBridge __Hotfix_set_FakeNpcInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="SourceSignal", DataFormat=DataFormat.Default)]
        public ProSignalInfo SourceSignal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="HiredCaptainShipConfigIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> HiredCaptainShipConfigIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="DynamicScenePlayerShipId", DataFormat=DataFormat.TwosComplement)]
        public int DynamicScenePlayerShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="RelativeQuestId", DataFormat=DataFormat.TwosComplement)]
        public int RelativeQuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="DestSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Location", DataFormat=DataFormat.Default)]
        public ProVectorDouble Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(9, IsRequired=false, Name="PlayerInfo", DataFormat=DataFormat.Default)]
        public ProPlayerSimpleInfo PlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="ExpiryTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long ExpiryTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="RewardList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> RewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="DynamicSceneInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint DynamicSceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="FakeNpcInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProFakeNpcInfo FakeNpcInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

