﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarShipAddModuleEquip2SlotAck")]
    public class GuildFlagShipHangarShipAddModuleEquip2SlotAck : IExtensible
    {
        private int _Result;
        private ulong _InstanceId;
        private int _HangarShipIndex;
        private int _EquipSlotIndex;
        private int _SrcStoreItemIndex;
        private int _DestStoreItemIndex;
        private ulong _DestStoreItemInstanceId;
        private int _AddEquipCount;
        private int _RemoveEquipStoreItemIndex;
        private ulong _RemoveEquipStoreItemInstanceId;
        private int _RemoveEquipCount;
        private int _ErrorCode;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_EquipSlotIndex;
        private static DelegateBridge __Hotfix_set_EquipSlotIndex;
        private static DelegateBridge __Hotfix_get_SrcStoreItemIndex;
        private static DelegateBridge __Hotfix_set_SrcStoreItemIndex;
        private static DelegateBridge __Hotfix_get_DestStoreItemIndex;
        private static DelegateBridge __Hotfix_set_DestStoreItemIndex;
        private static DelegateBridge __Hotfix_get_DestStoreItemInstanceId;
        private static DelegateBridge __Hotfix_set_DestStoreItemInstanceId;
        private static DelegateBridge __Hotfix_get_AddEquipCount;
        private static DelegateBridge __Hotfix_set_AddEquipCount;
        private static DelegateBridge __Hotfix_get_RemoveEquipStoreItemIndex;
        private static DelegateBridge __Hotfix_set_RemoveEquipStoreItemIndex;
        private static DelegateBridge __Hotfix_get_RemoveEquipStoreItemInstanceId;
        private static DelegateBridge __Hotfix_set_RemoveEquipStoreItemInstanceId;
        private static DelegateBridge __Hotfix_get_RemoveEquipCount;
        private static DelegateBridge __Hotfix_set_RemoveEquipCount;
        private static DelegateBridge __Hotfix_get_ErrorCode;
        private static DelegateBridge __Hotfix_set_ErrorCode;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="EquipSlotIndex", DataFormat=DataFormat.TwosComplement)]
        public int EquipSlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="SrcStoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int SrcStoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="DestStoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int DestStoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="DestStoreItemInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong DestStoreItemInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(8, IsRequired=false, Name="AddEquipCount", DataFormat=DataFormat.TwosComplement)]
        public int AddEquipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="RemoveEquipStoreItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int RemoveEquipStoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(10, IsRequired=false, Name="RemoveEquipStoreItemInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong RemoveEquipStoreItemInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="RemoveEquipCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int RemoveEquipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(12, IsRequired=false, Name="ErrorCode", DataFormat=DataFormat.TwosComplement)]
        public int ErrorCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

