﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarFleetModifyAck")]
    public class HangarFleetModifyAck : IExtensible
    {
        private int _Result;
        private int _FleetIndex;
        private readonly List<ulong> _ShipInstanceIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_FleetIndex;
        private static DelegateBridge __Hotfix_set_FleetIndex;
        private static DelegateBridge __Hotfix_get_ShipInstanceIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="FleetIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FleetIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ShipInstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> ShipInstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

