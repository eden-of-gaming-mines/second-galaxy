﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildPurchaseOrderModifyReq")]
    public class GuildPurchaseOrderModifyReq : IExtensible
    {
        private ulong _OrderInstanceId;
        private long _ItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OrderInstanceId;
        private static DelegateBridge __Hotfix_set_OrderInstanceId;
        private static DelegateBridge __Hotfix_get_ItemCount;
        private static DelegateBridge __Hotfix_set_ItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="OrderInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong OrderInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ItemCount", DataFormat=DataFormat.TwosComplement)]
        public long ItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

