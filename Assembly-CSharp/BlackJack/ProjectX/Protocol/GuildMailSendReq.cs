﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMailSendReq")]
    public class GuildMailSendReq : IExtensible
    {
        private string _Title;
        private string _Content;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Title;
        private static DelegateBridge __Hotfix_set_Title;
        private static DelegateBridge __Hotfix_get_Content;
        private static DelegateBridge __Hotfix_set_Content;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(""), ProtoMember(1, IsRequired=false, Name="Title", DataFormat=DataFormat.Default)]
        public string Title
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="Content", DataFormat=DataFormat.Default)]
        public string Content
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

