﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGlobalSceneInfo")]
    public class ProGlobalSceneInfo : IExtensible
    {
        private uint _ObjId;
        private ProVectorDouble _Location;
        private int _SceneId;
        private uint _SceneInstanceId;
        private string _SceneReplaceName;
        private int _WormholeStarGroupId;
        private readonly List<int> _WormholeDestSolarSystemIdList;
        private bool _WormholeIsRare;
        private int _WormholeTunnelDestSolarSystemId;
        private ulong _GuildBuildingInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ObjId;
        private static DelegateBridge __Hotfix_set_ObjId;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_get_SceneId;
        private static DelegateBridge __Hotfix_set_SceneId;
        private static DelegateBridge __Hotfix_get_SceneInstanceId;
        private static DelegateBridge __Hotfix_set_SceneInstanceId;
        private static DelegateBridge __Hotfix_get_SceneReplaceName;
        private static DelegateBridge __Hotfix_set_SceneReplaceName;
        private static DelegateBridge __Hotfix_get_WormholeStarGroupId;
        private static DelegateBridge __Hotfix_set_WormholeStarGroupId;
        private static DelegateBridge __Hotfix_get_WormholeDestSolarSystemIdList;
        private static DelegateBridge __Hotfix_get_WormholeIsRare;
        private static DelegateBridge __Hotfix_set_WormholeIsRare;
        private static DelegateBridge __Hotfix_get_WormholeTunnelDestSolarSystemId;
        private static DelegateBridge __Hotfix_set_WormholeTunnelDestSolarSystemId;
        private static DelegateBridge __Hotfix_get_GuildBuildingInstanceId;
        private static DelegateBridge __Hotfix_set_GuildBuildingInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ObjId", DataFormat=DataFormat.TwosComplement)]
        public uint ObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Location", DataFormat=DataFormat.Default)]
        public ProVectorDouble Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SceneId", DataFormat=DataFormat.TwosComplement)]
        public int SceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SceneInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint SceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="SceneReplaceName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string SceneReplaceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(10, IsRequired=false, Name="WormholeStarGroupId", DataFormat=DataFormat.TwosComplement)]
        public int WormholeStarGroupId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="WormholeDestSolarSystemIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> WormholeDestSolarSystemIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(false), ProtoMember(12, IsRequired=false, Name="WormholeIsRare", DataFormat=DataFormat.Default)]
        public bool WormholeIsRare
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="WormholeTunnelDestSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int WormholeTunnelDestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(14, IsRequired=false, Name="GuildBuildingInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong GuildBuildingInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

