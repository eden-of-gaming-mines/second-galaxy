﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProCMDailySignInfo")]
    public class ProCMDailySignInfo : IExtensible
    {
        private uint _CMDailySignCount;
        private long _RecvTime;
        private ProSimpleItemInfoWithCount _DailyRandReward;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CMDailySignCount;
        private static DelegateBridge __Hotfix_set_CMDailySignCount;
        private static DelegateBridge __Hotfix_get_RecvTime;
        private static DelegateBridge __Hotfix_set_RecvTime;
        private static DelegateBridge __Hotfix_get_DailyRandReward;
        private static DelegateBridge __Hotfix_set_DailyRandReward;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CMDailySignCount", DataFormat=DataFormat.TwosComplement)]
        public uint CMDailySignCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="RecvTime", DataFormat=DataFormat.TwosComplement)]
        public long RecvTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DailyRandReward", DataFormat=DataFormat.Default)]
        public ProSimpleItemInfoWithCount DailyRandReward
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

