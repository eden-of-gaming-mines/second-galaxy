﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBattleClientSyncInfo")]
    public class ProGuildBattleClientSyncInfo : IExtensible
    {
        private ulong _InstanceId;
        private int _BattleType;
        private ProGuildSimplestInfo _DefenderGuild;
        private ProGuildSimplestInfo _AttackerGuild;
        private int _NpcGuildConfigId;
        private long _StatTime;
        private int _Status;
        private long _CurrStatusStartTime;
        private long _CurrStatusEndTime;
        private readonly List<ProGuildBuildingBattleClientSyncInfo> _BuildingBattleInfos;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_BattleType;
        private static DelegateBridge __Hotfix_set_BattleType;
        private static DelegateBridge __Hotfix_get_DefenderGuild;
        private static DelegateBridge __Hotfix_set_DefenderGuild;
        private static DelegateBridge __Hotfix_get_AttackerGuild;
        private static DelegateBridge __Hotfix_set_AttackerGuild;
        private static DelegateBridge __Hotfix_get_NpcGuildConfigId;
        private static DelegateBridge __Hotfix_set_NpcGuildConfigId;
        private static DelegateBridge __Hotfix_get_StatTime;
        private static DelegateBridge __Hotfix_set_StatTime;
        private static DelegateBridge __Hotfix_get_Status;
        private static DelegateBridge __Hotfix_set_Status;
        private static DelegateBridge __Hotfix_get_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_set_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_get_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_set_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_get_BuildingBattleInfos;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BattleType", DataFormat=DataFormat.TwosComplement)]
        public int BattleType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="DefenderGuild", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildSimplestInfo DefenderGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="AttackerGuild", DataFormat=DataFormat.Default)]
        public ProGuildSimplestInfo AttackerGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="NpcGuildConfigId", DataFormat=DataFormat.TwosComplement)]
        public int NpcGuildConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="StatTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long StatTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(7, IsRequired=false, Name="Status", DataFormat=DataFormat.TwosComplement)]
        public int Status
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="CurrStatusStartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CurrStatusStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="CurrStatusEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CurrStatusEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="BuildingBattleInfos", DataFormat=DataFormat.Default)]
        public List<ProGuildBuildingBattleClientSyncInfo> BuildingBattleInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

