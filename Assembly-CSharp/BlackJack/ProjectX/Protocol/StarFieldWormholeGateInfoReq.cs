﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StarFieldWormholeGateInfoReq")]
    public class StarFieldWormholeGateInfoReq : IExtensible
    {
        private int _StarFieldId;
        private bool _WormholeInfo;
        private bool _RareWormholeInfo;
        private uint _DataVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StarFieldId;
        private static DelegateBridge __Hotfix_set_StarFieldId;
        private static DelegateBridge __Hotfix_get_WormholeInfo;
        private static DelegateBridge __Hotfix_set_WormholeInfo;
        private static DelegateBridge __Hotfix_get_RareWormholeInfo;
        private static DelegateBridge __Hotfix_set_RareWormholeInfo;
        private static DelegateBridge __Hotfix_get_DataVersion;
        private static DelegateBridge __Hotfix_set_DataVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="StarFieldId", DataFormat=DataFormat.TwosComplement)]
        public int StarFieldId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="WormholeInfo", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool WormholeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="RareWormholeInfo", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool RareWormholeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="DataVersion", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint DataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

