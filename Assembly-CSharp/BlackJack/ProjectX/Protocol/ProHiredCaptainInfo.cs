﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProHiredCaptainInfo")]
    public class ProHiredCaptainInfo : IExtensible
    {
        private ProHiredCaptainStaticInfo _StaticInfo;
        private ProHiredCaptainDynmicInfo _DynmicInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StaticInfo;
        private static DelegateBridge __Hotfix_set_StaticInfo;
        private static DelegateBridge __Hotfix_get_DynmicInfo;
        private static DelegateBridge __Hotfix_set_DynmicInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(15, IsRequired=true, Name="StaticInfo", DataFormat=DataFormat.Default)]
        public ProHiredCaptainStaticInfo StaticInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="DynmicInfo", DataFormat=DataFormat.Default)]
        public ProHiredCaptainDynmicInfo DynmicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

