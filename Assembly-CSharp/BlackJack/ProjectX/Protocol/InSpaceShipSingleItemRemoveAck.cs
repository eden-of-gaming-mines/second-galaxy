﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceShipSingleItemRemoveAck")]
    public class InSpaceShipSingleItemRemoveAck : IExtensible
    {
        private int _Result;
        private int _StoreItemType;
        private int _StoreItemId;
        private int _StoreItemCount;
        private bool _StoreItemIsBind;
        private bool _StoreItemIsFreezing;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_StoreItemType;
        private static DelegateBridge __Hotfix_set_StoreItemType;
        private static DelegateBridge __Hotfix_get_StoreItemId;
        private static DelegateBridge __Hotfix_set_StoreItemId;
        private static DelegateBridge __Hotfix_get_StoreItemCount;
        private static DelegateBridge __Hotfix_set_StoreItemCount;
        private static DelegateBridge __Hotfix_get_StoreItemIsBind;
        private static DelegateBridge __Hotfix_set_StoreItemIsBind;
        private static DelegateBridge __Hotfix_get_StoreItemIsFreezing;
        private static DelegateBridge __Hotfix_set_StoreItemIsFreezing;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StoreItemType", DataFormat=DataFormat.TwosComplement)]
        public int StoreItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StoreItemId", DataFormat=DataFormat.TwosComplement)]
        public int StoreItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="StoreItemCount", DataFormat=DataFormat.TwosComplement)]
        public int StoreItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="StoreItemIsBind", DataFormat=DataFormat.Default)]
        public bool StoreItemIsBind
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="StoreItemIsFreezing", DataFormat=DataFormat.Default)]
        public bool StoreItemIsFreezing
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

