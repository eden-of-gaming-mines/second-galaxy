﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProTechUpgradeInfo")]
    public class ProTechUpgradeInfo : IExtensible
    {
        private int _Id;
        private int _Level;
        private long _StartTime;
        private long _EndTime;
        private long _OriginalEndTime;
        private ulong _WorkingCaptain;
        private readonly List<ProCostInfo> _CostList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_EndTime;
        private static DelegateBridge __Hotfix_set_EndTime;
        private static DelegateBridge __Hotfix_get_OriginalEndTime;
        private static DelegateBridge __Hotfix_set_OriginalEndTime;
        private static DelegateBridge __Hotfix_get_WorkingCaptain;
        private static DelegateBridge __Hotfix_set_WorkingCaptain;
        private static DelegateBridge __Hotfix_get_CostList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="EndTime", DataFormat=DataFormat.TwosComplement)]
        public long EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="OriginalEndTime", DataFormat=DataFormat.TwosComplement)]
        public long OriginalEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="WorkingCaptain", DataFormat=DataFormat.TwosComplement)]
        public ulong WorkingCaptain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="CostList", DataFormat=DataFormat.Default)]
        public List<ProCostInfo> CostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

