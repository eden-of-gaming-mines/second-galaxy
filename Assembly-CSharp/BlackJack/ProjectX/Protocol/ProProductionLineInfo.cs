﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProProductionLineInfo")]
    public class ProProductionLineInfo : IExtensible
    {
        private int _Index;
        private int _BlueprintId;
        private int _BlueprintCount;
        private long _StartTime;
        private long _EndTime;
        private ulong _WorkingCaptain;
        private readonly List<ProCostInfo> _CostList;
        private bool _IsCompleteNotified;
        private bool _BlueprintIsBind;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_BlueprintId;
        private static DelegateBridge __Hotfix_set_BlueprintId;
        private static DelegateBridge __Hotfix_get_BlueprintCount;
        private static DelegateBridge __Hotfix_set_BlueprintCount;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_EndTime;
        private static DelegateBridge __Hotfix_set_EndTime;
        private static DelegateBridge __Hotfix_get_WorkingCaptain;
        private static DelegateBridge __Hotfix_set_WorkingCaptain;
        private static DelegateBridge __Hotfix_get_CostList;
        private static DelegateBridge __Hotfix_get_IsCompleteNotified;
        private static DelegateBridge __Hotfix_set_IsCompleteNotified;
        private static DelegateBridge __Hotfix_get_BlueprintIsBind;
        private static DelegateBridge __Hotfix_set_BlueprintIsBind;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Index", DataFormat=DataFormat.TwosComplement)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BlueprintId", DataFormat=DataFormat.TwosComplement)]
        public int BlueprintId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="BlueprintCount", DataFormat=DataFormat.TwosComplement)]
        public int BlueprintCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="EndTime", DataFormat=DataFormat.TwosComplement)]
        public long EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="WorkingCaptain", DataFormat=DataFormat.TwosComplement)]
        public ulong WorkingCaptain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="CostList", DataFormat=DataFormat.Default)]
        public List<ProCostInfo> CostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(false), ProtoMember(8, IsRequired=false, Name="IsCompleteNotified", DataFormat=DataFormat.Default)]
        public bool IsCompleteNotified
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="BlueprintIsBind", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool BlueprintIsBind
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

