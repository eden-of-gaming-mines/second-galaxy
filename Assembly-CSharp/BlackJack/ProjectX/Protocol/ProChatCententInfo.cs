﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatCententInfo")]
    public class ProChatCententInfo : IExtensible
    {
        private ProChatContentText _ChatContentText;
        private ProChatContentVoice _ChatContentVoice;
        private ProChatVoiceSimpleInfo _ChatVoiceSimpleInfo;
        private ProChatContentLocation _ChatContentLocation;
        private ProChatContentItem _ChatContentItem;
        private ProChatContentShip _ChatContentShip;
        private ProChatContentTeamInvite _ChatContentTeamInvite;
        private ProChatContentPlayer _ChatContentPlayer;
        private ProChatContentKillRecord _ChatContentKillRecord;
        private ProChatContentDelegateMission _ChatContentDelegateMission;
        private ProChatContentGuild _ChatContentGuild;
        private ProChatContentSailReport _ChatContentSailReport;
        private ProChatContentGuildBattleReport _ChatContentGuildBattleReport;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ChatContentText;
        private static DelegateBridge __Hotfix_set_ChatContentText;
        private static DelegateBridge __Hotfix_get_ChatContentVoice;
        private static DelegateBridge __Hotfix_set_ChatContentVoice;
        private static DelegateBridge __Hotfix_get_ChatVoiceSimpleInfo;
        private static DelegateBridge __Hotfix_set_ChatVoiceSimpleInfo;
        private static DelegateBridge __Hotfix_get_ChatContentLocation;
        private static DelegateBridge __Hotfix_set_ChatContentLocation;
        private static DelegateBridge __Hotfix_get_ChatContentItem;
        private static DelegateBridge __Hotfix_set_ChatContentItem;
        private static DelegateBridge __Hotfix_get_ChatContentShip;
        private static DelegateBridge __Hotfix_set_ChatContentShip;
        private static DelegateBridge __Hotfix_get_ChatContentTeamInvite;
        private static DelegateBridge __Hotfix_set_ChatContentTeamInvite;
        private static DelegateBridge __Hotfix_get_ChatContentPlayer;
        private static DelegateBridge __Hotfix_set_ChatContentPlayer;
        private static DelegateBridge __Hotfix_get_ChatContentKillRecord;
        private static DelegateBridge __Hotfix_set_ChatContentKillRecord;
        private static DelegateBridge __Hotfix_get_ChatContentDelegateMission;
        private static DelegateBridge __Hotfix_set_ChatContentDelegateMission;
        private static DelegateBridge __Hotfix_get_ChatContentGuild;
        private static DelegateBridge __Hotfix_set_ChatContentGuild;
        private static DelegateBridge __Hotfix_get_ChatContentSailReport;
        private static DelegateBridge __Hotfix_set_ChatContentSailReport;
        private static DelegateBridge __Hotfix_get_ChatContentGuildBattleReport;
        private static DelegateBridge __Hotfix_set_ChatContentGuildBattleReport;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="ChatContentText", DataFormat=DataFormat.Default)]
        public ProChatContentText ChatContentText
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="ChatContentVoice", DataFormat=DataFormat.Default)]
        public ProChatContentVoice ChatContentVoice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="ChatVoiceSimpleInfo", DataFormat=DataFormat.Default)]
        public ProChatVoiceSimpleInfo ChatVoiceSimpleInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="ChatContentLocation", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatContentLocation ChatContentLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ChatContentItem", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatContentItem ChatContentItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="ChatContentShip", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatContentShip ChatContentShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(7, IsRequired=false, Name="ChatContentTeamInvite", DataFormat=DataFormat.Default)]
        public ProChatContentTeamInvite ChatContentTeamInvite
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="ChatContentPlayer", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatContentPlayer ChatContentPlayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="ChatContentKillRecord", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatContentKillRecord ChatContentKillRecord
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="ChatContentDelegateMission", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatContentDelegateMission ChatContentDelegateMission
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="ChatContentGuild", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatContentGuild ChatContentGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(12, IsRequired=false, Name="ChatContentSailReport", DataFormat=DataFormat.Default)]
        public ProChatContentSailReport ChatContentSailReport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(13, IsRequired=false, Name="ChatContentGuildBattleReport", DataFormat=DataFormat.Default)]
        public ProChatContentGuildBattleReport ChatContentGuildBattleReport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

