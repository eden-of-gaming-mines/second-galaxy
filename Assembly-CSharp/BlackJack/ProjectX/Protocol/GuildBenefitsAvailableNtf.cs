﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBenefitsAvailableNtf")]
    public class GuildBenefitsAvailableNtf : IExtensible
    {
        private readonly List<ProGuildBenefitsInformationPoint> _InformationPointBenefitsList;
        private readonly List<ProGuildBenefitsCommon> _CommonBenefitsList;
        private ProGuildBenefitsSovereignty _BenefitsSovereignty;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InformationPointBenefitsList;
        private static DelegateBridge __Hotfix_get_CommonBenefitsList;
        private static DelegateBridge __Hotfix_get_BenefitsSovereignty;
        private static DelegateBridge __Hotfix_set_BenefitsSovereignty;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(2, Name="InformationPointBenefitsList", DataFormat=DataFormat.Default)]
        public List<ProGuildBenefitsInformationPoint> InformationPointBenefitsList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="CommonBenefitsList", DataFormat=DataFormat.Default)]
        public List<ProGuildBenefitsCommon> CommonBenefitsList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="BenefitsSovereignty", DataFormat=DataFormat.Default)]
        public ProGuildBenefitsSovereignty BenefitsSovereignty
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

