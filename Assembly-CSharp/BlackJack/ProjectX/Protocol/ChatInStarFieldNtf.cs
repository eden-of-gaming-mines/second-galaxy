﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ChatInStarFieldNtf")]
    public class ChatInStarFieldNtf : IExtensible
    {
        private ProChatInfo _ChatInfo;
        private ProChatCententInfo _ChatContent;
        private int _StoreIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ChatInfo;
        private static DelegateBridge __Hotfix_set_ChatInfo;
        private static DelegateBridge __Hotfix_get_ChatContent;
        private static DelegateBridge __Hotfix_set_ChatContent;
        private static DelegateBridge __Hotfix_get_StoreIndex;
        private static DelegateBridge __Hotfix_set_StoreIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ChatInfo", DataFormat=DataFormat.Default)]
        public ProChatInfo ChatInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ChatContent", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatCententInfo ChatContent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="StoreIndex", DataFormat=DataFormat.TwosComplement)]
        public int StoreIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

