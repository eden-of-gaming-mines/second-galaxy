﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSynEventOnDotDamage")]
    public class ProLBSynEventOnDotDamage : IExtensible
    {
        private uint _BufInstanceId;
        private float _Damage;
        private int _BufType;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BufInstanceId;
        private static DelegateBridge __Hotfix_set_BufInstanceId;
        private static DelegateBridge __Hotfix_get_Damage;
        private static DelegateBridge __Hotfix_set_Damage;
        private static DelegateBridge __Hotfix_get_BufType;
        private static DelegateBridge __Hotfix_set_BufType;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="BufInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint BufInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="Damage", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float Damage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="BufType", DataFormat=DataFormat.TwosComplement)]
        public int BufType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

