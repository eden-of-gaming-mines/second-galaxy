﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFleetMembersJumpingReq")]
    public class GuildFleetMembersJumpingReq : IExtensible
    {
        private ulong _FleetId;
        private ProShipMoveCmdInfo _MoveCmd;
        private uint _GlobalSceneId;
        private string _GuildMemberId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FleetId;
        private static DelegateBridge __Hotfix_set_FleetId;
        private static DelegateBridge __Hotfix_get_MoveCmd;
        private static DelegateBridge __Hotfix_set_MoveCmd;
        private static DelegateBridge __Hotfix_get_GlobalSceneId;
        private static DelegateBridge __Hotfix_set_GlobalSceneId;
        private static DelegateBridge __Hotfix_get_GuildMemberId;
        private static DelegateBridge __Hotfix_set_GuildMemberId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FleetId", DataFormat=DataFormat.TwosComplement)]
        public ulong FleetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="MoveCmd", DataFormat=DataFormat.Default)]
        public ProShipMoveCmdInfo MoveCmd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="GlobalSceneId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GlobalSceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(4, IsRequired=false, Name="GuildMemberId", DataFormat=DataFormat.Default)]
        public string GuildMemberId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

