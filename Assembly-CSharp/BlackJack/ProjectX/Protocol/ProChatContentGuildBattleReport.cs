﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatContentGuildBattleReport")]
    public class ProChatContentGuildBattleReport : IExtensible
    {
        private ulong _ReportId;
        private int _SolarSystemId;
        private int _BattleType;
        private int _RelativeBuildingType;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ReportId;
        private static DelegateBridge __Hotfix_set_ReportId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_BattleType;
        private static DelegateBridge __Hotfix_set_BattleType;
        private static DelegateBridge __Hotfix_get_RelativeBuildingType;
        private static DelegateBridge __Hotfix_set_RelativeBuildingType;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="ReportId", DataFormat=DataFormat.TwosComplement)]
        public ulong ReportId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="BattleType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BattleType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="RelativeBuildingType", DataFormat=DataFormat.TwosComplement)]
        public int RelativeBuildingType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

