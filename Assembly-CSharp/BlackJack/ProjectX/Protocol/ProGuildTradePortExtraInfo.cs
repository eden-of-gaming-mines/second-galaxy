﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildTradePortExtraInfo")]
    public class ProGuildTradePortExtraInfo : IExtensible
    {
        private int _SolarSystemId;
        private ulong _PurchaseInstanceId;
        private ulong _TransportInstanceId;
        private readonly List<ProGuildTradePurchaseOrderTransportLogInfo> _LogList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_PurchaseInstanceId;
        private static DelegateBridge __Hotfix_set_PurchaseInstanceId;
        private static DelegateBridge __Hotfix_get_TransportInstanceId;
        private static DelegateBridge __Hotfix_set_TransportInstanceId;
        private static DelegateBridge __Hotfix_get_LogList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="PurchaseInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong PurchaseInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="TransportInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong TransportInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="LogList", DataFormat=DataFormat.Default)]
        public List<ProGuildTradePurchaseOrderTransportLogInfo> LogList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

