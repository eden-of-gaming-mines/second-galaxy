﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ChatVoiceContentAck")]
    public class ChatVoiceContentAck : IExtensible
    {
        private readonly List<ProChatContentVoice> _ChatContentVoiceList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ChatContentVoiceList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ChatContentVoiceList", DataFormat=DataFormat.Default)]
        public List<ProChatContentVoice> ChatContentVoiceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

