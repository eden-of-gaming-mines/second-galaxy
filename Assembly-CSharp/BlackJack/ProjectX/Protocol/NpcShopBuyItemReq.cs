﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="NpcShopBuyItemReq")]
    public class NpcShopBuyItemReq : IExtensible
    {
        private readonly List<ProIdValueInfo> _ShopItemIdCountList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShopItemIdCountList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ShopItemIdCountList", DataFormat=DataFormat.Default)]
        public List<ProIdValueInfo> ShopItemIdCountList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

