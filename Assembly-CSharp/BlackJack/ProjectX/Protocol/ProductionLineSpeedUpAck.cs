﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProductionLineSpeedUpAck")]
    public class ProductionLineSpeedUpAck : IExtensible
    {
        private int _Result;
        private int _LineIndex;
        private long _NewEndTime;
        private int _ItemId;
        private int _ItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_LineIndex;
        private static DelegateBridge __Hotfix_set_LineIndex;
        private static DelegateBridge __Hotfix_get_NewEndTime;
        private static DelegateBridge __Hotfix_set_NewEndTime;
        private static DelegateBridge __Hotfix_get_ItemId;
        private static DelegateBridge __Hotfix_set_ItemId;
        private static DelegateBridge __Hotfix_get_ItemCount;
        private static DelegateBridge __Hotfix_set_ItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="LineIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int LineIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="NewEndTime", DataFormat=DataFormat.TwosComplement)]
        public long NewEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="ItemId", DataFormat=DataFormat.TwosComplement)]
        public int ItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="ItemCount", DataFormat=DataFormat.TwosComplement)]
        public int ItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

