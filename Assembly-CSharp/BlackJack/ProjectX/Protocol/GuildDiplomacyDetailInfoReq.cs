﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildDiplomacyDetailInfoReq")]
    public class GuildDiplomacyDetailInfoReq : IExtensible
    {
        private bool _FriendlyList;
        private bool _EnemyList;
        private uint _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FriendlyList;
        private static DelegateBridge __Hotfix_set_FriendlyList;
        private static DelegateBridge __Hotfix_get_EnemyList;
        private static DelegateBridge __Hotfix_set_EnemyList;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="FriendlyList", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool FriendlyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="EnemyList", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool EnemyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

