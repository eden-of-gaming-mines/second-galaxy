﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProIFFStatePairInfo")]
    public class ProIFFStatePairInfo : IExtensible
    {
        private int _OldIFFState;
        private uint _TargetObjId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OldIFFState;
        private static DelegateBridge __Hotfix_set_OldIFFState;
        private static DelegateBridge __Hotfix_get_TargetObjId;
        private static DelegateBridge __Hotfix_set_TargetObjId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="OldIFFState", DataFormat=DataFormat.TwosComplement)]
        public int OldIFFState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="TargetObjId", DataFormat=DataFormat.TwosComplement)]
        public uint TargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

