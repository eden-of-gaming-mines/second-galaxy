﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AuctionItemInfoAck")]
    public class AuctionItemInfoAck : IExtensible
    {
        private int _Result;
        private int _AuctionItemId;
        private readonly List<ProAuctionOrderBriefInfo> _OrderListInfo;
        private long _MarketPrice;
        private float _TradeTax;
        private int _TotalItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_AuctionItemId;
        private static DelegateBridge __Hotfix_set_AuctionItemId;
        private static DelegateBridge __Hotfix_get_OrderListInfo;
        private static DelegateBridge __Hotfix_get_MarketPrice;
        private static DelegateBridge __Hotfix_set_MarketPrice;
        private static DelegateBridge __Hotfix_get_TradeTax;
        private static DelegateBridge __Hotfix_set_TradeTax;
        private static DelegateBridge __Hotfix_get_TotalItemCount;
        private static DelegateBridge __Hotfix_set_TotalItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="AuctionItemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AuctionItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="OrderListInfo", DataFormat=DataFormat.Default)]
        public List<ProAuctionOrderBriefInfo> OrderListInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="MarketPrice", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long MarketPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(5, IsRequired=false, Name="TradeTax", DataFormat=DataFormat.FixedSize)]
        public float TradeTax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="TotalItemCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int TotalItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

