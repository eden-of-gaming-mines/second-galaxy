﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DevelopmentProjectStartAck")]
    public class DevelopmentProjectStartAck : IExtensible
    {
        private int _Result;
        private readonly List<ProStoreItemUpdateInfo> _DelUpdateItemInfos;
        private readonly List<ProStoreItemUpdateInfo> _AddUpdateItemInfos;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdateInfo;
        private int _ProjectID;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_DelUpdateItemInfos;
        private static DelegateBridge __Hotfix_get_AddUpdateItemInfos;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_get_ProjectID;
        private static DelegateBridge __Hotfix_set_ProjectID;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="DelUpdateItemInfos", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> DelUpdateItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="AddUpdateItemInfos", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> AddUpdateItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="CurrencyUpdateInfo", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ProjectID", DataFormat=DataFormat.TwosComplement)]
        public int ProjectID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

