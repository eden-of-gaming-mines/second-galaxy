﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch")]
    public class ProLBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch : IExtensible
    {
        private ProSpaceProcessEquipAttachBufLaunch _AttachBufInfo;
        private float _ShipShieldReducePercent;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AttachBufInfo;
        private static DelegateBridge __Hotfix_set_AttachBufInfo;
        private static DelegateBridge __Hotfix_get_ShipShieldReducePercent;
        private static DelegateBridge __Hotfix_set_ShipShieldReducePercent;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="AttachBufInfo", DataFormat=DataFormat.Default)]
        public ProSpaceProcessEquipAttachBufLaunch AttachBufInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ShipShieldReducePercent", DataFormat=DataFormat.FixedSize)]
        public float ShipShieldReducePercent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

