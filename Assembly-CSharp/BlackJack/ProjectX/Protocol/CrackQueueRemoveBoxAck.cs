﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CrackQueueRemoveBoxAck")]
    public class CrackQueueRemoveBoxAck : IExtensible
    {
        private int _Result;
        private int _BoxItemIndex;
        private int _ResumeBoxItemIndex;
        private ulong _ResumeBoxItemInsId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_BoxItemIndex;
        private static DelegateBridge __Hotfix_set_BoxItemIndex;
        private static DelegateBridge __Hotfix_get_ResumeBoxItemIndex;
        private static DelegateBridge __Hotfix_set_ResumeBoxItemIndex;
        private static DelegateBridge __Hotfix_get_ResumeBoxItemInsId;
        private static DelegateBridge __Hotfix_set_ResumeBoxItemInsId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="BoxItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BoxItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="ResumeBoxItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int ResumeBoxItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="ResumeBoxItemInsId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong ResumeBoxItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

