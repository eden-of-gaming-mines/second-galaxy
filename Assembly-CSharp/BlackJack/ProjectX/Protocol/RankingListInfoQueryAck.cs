﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="RankingListInfoQueryAck")]
    public class RankingListInfoQueryAck : IExtensible
    {
        private int _Result;
        private int _RankingListType;
        private ProRankingListInfo _ListInfo;
        private int _QueryTopNum;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_RankingListType;
        private static DelegateBridge __Hotfix_set_RankingListType;
        private static DelegateBridge __Hotfix_get_ListInfo;
        private static DelegateBridge __Hotfix_set_ListInfo;
        private static DelegateBridge __Hotfix_get_QueryTopNum;
        private static DelegateBridge __Hotfix_set_QueryTopNum;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="RankingListType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int RankingListType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ListInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProRankingListInfo ListInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="QueryTopNum", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int QueryTopNum
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

