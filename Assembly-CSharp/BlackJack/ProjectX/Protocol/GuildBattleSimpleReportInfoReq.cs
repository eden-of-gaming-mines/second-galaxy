﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBattleSimpleReportInfoReq")]
    public class GuildBattleSimpleReportInfoReq : IExtensible
    {
        private ulong _LastCompletedSimpleReportInstanceId;
        private uint _CompletedSimpleReportCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LastCompletedSimpleReportInstanceId;
        private static DelegateBridge __Hotfix_set_LastCompletedSimpleReportInstanceId;
        private static DelegateBridge __Hotfix_get_CompletedSimpleReportCount;
        private static DelegateBridge __Hotfix_set_CompletedSimpleReportCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LastCompletedSimpleReportInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong LastCompletedSimpleReportInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CompletedSimpleReportCount", DataFormat=DataFormat.TwosComplement)]
        public uint CompletedSimpleReportCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

