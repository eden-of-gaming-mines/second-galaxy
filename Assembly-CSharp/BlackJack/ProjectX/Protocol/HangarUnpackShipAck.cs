﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarUnpackShipAck")]
    public class HangarUnpackShipAck : IExtensible
    {
        private int _Result;
        private int _HangarIndex;
        private ulong _ShipPackedItemInsId;
        private ulong _ShipUnpackedItemInsId;
        private int _UnpackedItemIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_HangarIndex;
        private static DelegateBridge __Hotfix_set_HangarIndex;
        private static DelegateBridge __Hotfix_get_ShipPackedItemInsId;
        private static DelegateBridge __Hotfix_set_ShipPackedItemInsId;
        private static DelegateBridge __Hotfix_get_ShipUnpackedItemInsId;
        private static DelegateBridge __Hotfix_set_ShipUnpackedItemInsId;
        private static DelegateBridge __Hotfix_get_UnpackedItemIndex;
        private static DelegateBridge __Hotfix_set_UnpackedItemIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="HangarIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(3, IsRequired=false, Name="ShipPackedItemInsId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipPackedItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(4, IsRequired=false, Name="ShipUnpackedItemInsId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipUnpackedItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="UnpackedItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int UnpackedItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

