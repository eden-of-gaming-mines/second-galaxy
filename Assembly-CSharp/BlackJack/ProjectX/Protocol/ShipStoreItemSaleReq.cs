﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipStoreItemSaleReq")]
    public class ShipStoreItemSaleReq : IExtensible
    {
        private readonly List<ProItemInfo> _StoreItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StoreItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="StoreItemList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> StoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

