﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceProcessLaserLaunch")]
    public class ProSpaceProcessLaserLaunch : IExtensible
    {
        private uint _InstanceId;
        private uint _StartTime;
        private uint _CostEnergy;
        private uint _T1EndTime;
        private uint _T2EndTime;
        private uint _T3EndTime;
        private double _CriticalRateFinal;
        private readonly List<ProLBProcessSingleUnitLaunchInfo> _UnitLaunchList;
        private readonly List<uint> _ChargeList;
        private uint _CostFuel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_CostEnergy;
        private static DelegateBridge __Hotfix_set_CostEnergy;
        private static DelegateBridge __Hotfix_get_T1EndTime;
        private static DelegateBridge __Hotfix_set_T1EndTime;
        private static DelegateBridge __Hotfix_get_T2EndTime;
        private static DelegateBridge __Hotfix_set_T2EndTime;
        private static DelegateBridge __Hotfix_get_T3EndTime;
        private static DelegateBridge __Hotfix_set_T3EndTime;
        private static DelegateBridge __Hotfix_get_CriticalRateFinal;
        private static DelegateBridge __Hotfix_set_CriticalRateFinal;
        private static DelegateBridge __Hotfix_get_UnitLaunchList;
        private static DelegateBridge __Hotfix_get_ChargeList;
        private static DelegateBridge __Hotfix_get_CostFuel;
        private static DelegateBridge __Hotfix_set_CostFuel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public uint StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CostEnergy", DataFormat=DataFormat.TwosComplement)]
        public uint CostEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="T1EndTime", DataFormat=DataFormat.TwosComplement)]
        public uint T1EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="T2EndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint T2EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="T3EndTime", DataFormat=DataFormat.TwosComplement)]
        public uint T3EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="CriticalRateFinal", DataFormat=DataFormat.TwosComplement), DefaultValue((double) 0.0)]
        public double CriticalRateFinal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, Name="UnitLaunchList", DataFormat=DataFormat.Default)]
        public List<ProLBProcessSingleUnitLaunchInfo> UnitLaunchList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(9, Name="ChargeList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> ChargeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="CostFuel", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint CostFuel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

