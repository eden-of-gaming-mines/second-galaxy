﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildAllianceInviteInfo")]
    public class ProGuildAllianceInviteInfo : IExtensible
    {
        private ulong _InstanceId;
        private uint _InviteeGuildId;
        private uint _AllianceId;
        private string _AllianceName;
        private int _AllianceMemberGuildCount;
        private int _AllianceLanguageType;
        private ProAllianceLogoInfo _AllianceLogo;
        private string _InviterGameUserId;
        private string _InviterGameUserName;
        private uint _InviterGuildId;
        private string _InviterGuildCode;
        private string _InviterGuildName;
        private long _CreateTime;
        private long _ExpireTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_InviteeGuildId;
        private static DelegateBridge __Hotfix_set_InviteeGuildId;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_set_AllianceName;
        private static DelegateBridge __Hotfix_get_AllianceMemberGuildCount;
        private static DelegateBridge __Hotfix_set_AllianceMemberGuildCount;
        private static DelegateBridge __Hotfix_get_AllianceLanguageType;
        private static DelegateBridge __Hotfix_set_AllianceLanguageType;
        private static DelegateBridge __Hotfix_get_AllianceLogo;
        private static DelegateBridge __Hotfix_set_AllianceLogo;
        private static DelegateBridge __Hotfix_get_InviterGameUserId;
        private static DelegateBridge __Hotfix_set_InviterGameUserId;
        private static DelegateBridge __Hotfix_get_InviterGameUserName;
        private static DelegateBridge __Hotfix_set_InviterGameUserName;
        private static DelegateBridge __Hotfix_get_InviterGuildId;
        private static DelegateBridge __Hotfix_set_InviterGuildId;
        private static DelegateBridge __Hotfix_get_InviterGuildCode;
        private static DelegateBridge __Hotfix_set_InviterGuildCode;
        private static DelegateBridge __Hotfix_get_InviterGuildName;
        private static DelegateBridge __Hotfix_set_InviterGuildName;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_get_ExpireTime;
        private static DelegateBridge __Hotfix_set_ExpireTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="InviteeGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint InviteeGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="AllianceName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="AllianceMemberGuildCount", DataFormat=DataFormat.TwosComplement)]
        public int AllianceMemberGuildCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="AllianceLanguageType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AllianceLanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(7, IsRequired=false, Name="AllianceLogo", DataFormat=DataFormat.Default)]
        public ProAllianceLogoInfo AllianceLogo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="InviterGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string InviterGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(9, IsRequired=false, Name="InviterGameUserName", DataFormat=DataFormat.Default)]
        public string InviterGameUserName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="InviterGuildId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint InviterGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="InviterGuildCode", DataFormat=DataFormat.Default), DefaultValue("")]
        public string InviterGuildCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="InviterGuildName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string InviterGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="CreateTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(14, IsRequired=false, Name="ExpireTime", DataFormat=DataFormat.TwosComplement)]
        public long ExpireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

