﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProRechargeStoreBuyingGoodsInfo")]
    public class ProRechargeStoreBuyingGoodsInfo : IExtensible
    {
        private int _GoodsType;
        private int _GoodsId;
        private long _ExpiredTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GoodsType;
        private static DelegateBridge __Hotfix_set_GoodsType;
        private static DelegateBridge __Hotfix_get_GoodsId;
        private static DelegateBridge __Hotfix_set_GoodsId;
        private static DelegateBridge __Hotfix_get_ExpiredTime;
        private static DelegateBridge __Hotfix_set_ExpiredTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GoodsType", DataFormat=DataFormat.TwosComplement)]
        public int GoodsType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="GoodsId", DataFormat=DataFormat.TwosComplement)]
        public int GoodsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ExpiredTime", DataFormat=DataFormat.TwosComplement)]
        public long ExpiredTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

