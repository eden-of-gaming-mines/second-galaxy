﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProRechargeMonthlyCard")]
    public class ProRechargeMonthlyCard : IExtensible
    {
        private int _Id;
        private long _ExpireTime;
        private bool _IsDailyRewardReceived;
        private bool _IsExpireEventFired;
        private bool _IsNearExpireEventFired;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_ExpireTime;
        private static DelegateBridge __Hotfix_set_ExpireTime;
        private static DelegateBridge __Hotfix_get_IsDailyRewardReceived;
        private static DelegateBridge __Hotfix_set_IsDailyRewardReceived;
        private static DelegateBridge __Hotfix_get_IsExpireEventFired;
        private static DelegateBridge __Hotfix_set_IsExpireEventFired;
        private static DelegateBridge __Hotfix_get_IsNearExpireEventFired;
        private static DelegateBridge __Hotfix_set_IsNearExpireEventFired;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ExpireTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long ExpireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="IsDailyRewardReceived", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsDailyRewardReceived
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(4, IsRequired=false, Name="IsExpireEventFired", DataFormat=DataFormat.Default)]
        public bool IsExpireEventFired
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="IsNearExpireEventFired", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsNearExpireEventFired
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

