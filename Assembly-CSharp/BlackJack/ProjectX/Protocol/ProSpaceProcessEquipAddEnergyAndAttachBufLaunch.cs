﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceProcessEquipAddEnergyAndAttachBufLaunch")]
    public class ProSpaceProcessEquipAddEnergyAndAttachBufLaunch : IExtensible
    {
        private ProSpaceProcessEquipAttachBufLaunch _AttachBufInfo;
        private float _EnergyRecoveryAmount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AttachBufInfo;
        private static DelegateBridge __Hotfix_set_AttachBufInfo;
        private static DelegateBridge __Hotfix_get_EnergyRecoveryAmount;
        private static DelegateBridge __Hotfix_set_EnergyRecoveryAmount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="AttachBufInfo", DataFormat=DataFormat.Default)]
        public ProSpaceProcessEquipAttachBufLaunch AttachBufInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="EnergyRecoveryAmount", DataFormat=DataFormat.FixedSize)]
        public float EnergyRecoveryAmount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

