﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMemberJobUpdateReq")]
    public class GuildMemberJobUpdateReq : IExtensible
    {
        private string _PlayerGameUserId;
        private int _Jobs;
        private bool _IsAppoint;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerGameUserId;
        private static DelegateBridge __Hotfix_set_PlayerGameUserId;
        private static DelegateBridge __Hotfix_get_Jobs;
        private static DelegateBridge __Hotfix_set_Jobs;
        private static DelegateBridge __Hotfix_get_IsAppoint;
        private static DelegateBridge __Hotfix_set_IsAppoint;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="PlayerGameUserId", DataFormat=DataFormat.Default)]
        public string PlayerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Jobs", DataFormat=DataFormat.TwosComplement)]
        public int Jobs
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="IsAppoint", DataFormat=DataFormat.Default)]
        public bool IsAppoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

