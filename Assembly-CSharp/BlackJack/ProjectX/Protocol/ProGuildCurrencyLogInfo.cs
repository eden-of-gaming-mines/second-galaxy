﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildCurrencyLogInfo")]
    public class ProGuildCurrencyLogInfo : IExtensible
    {
        private int _Type;
        private ulong _InstanceId;
        private string _OperatorName;
        private long _ChangeCount;
        private ulong _CurrBalance;
        private int _Param;
        private long _Time;
        private string _GameUserID;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_OperatorName;
        private static DelegateBridge __Hotfix_set_OperatorName;
        private static DelegateBridge __Hotfix_get_ChangeCount;
        private static DelegateBridge __Hotfix_set_ChangeCount;
        private static DelegateBridge __Hotfix_get_CurrBalance;
        private static DelegateBridge __Hotfix_set_CurrBalance;
        private static DelegateBridge __Hotfix_get_Param;
        private static DelegateBridge __Hotfix_set_Param;
        private static DelegateBridge __Hotfix_get_Time;
        private static DelegateBridge __Hotfix_set_Time;
        private static DelegateBridge __Hotfix_get_GameUserID;
        private static DelegateBridge __Hotfix_set_GameUserID;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Type", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="OperatorName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OperatorName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="ChangeCount", DataFormat=DataFormat.TwosComplement)]
        public long ChangeCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(5, IsRequired=false, Name="CurrBalance", DataFormat=DataFormat.TwosComplement)]
        public ulong CurrBalance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="Param", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Param
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="Time", DataFormat=DataFormat.TwosComplement)]
        public long Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(8, IsRequired=false, Name="GameUserID", DataFormat=DataFormat.Default)]
        public string GameUserID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

