﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDevelopmentProjectInfo")]
    public class ProDevelopmentProjectInfo : IExtensible
    {
        private float _CurTotalResearch;
        private int _ProjectId;
        private long _StartTime;
        private readonly List<ProDevelopmentHistoryInfo> _DevelopmentHistoryInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CurTotalResearch;
        private static DelegateBridge __Hotfix_set_CurTotalResearch;
        private static DelegateBridge __Hotfix_get_ProjectId;
        private static DelegateBridge __Hotfix_set_ProjectId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_DevelopmentHistoryInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CurTotalResearch", DataFormat=DataFormat.FixedSize)]
        public float CurTotalResearch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ProjectId", DataFormat=DataFormat.TwosComplement)]
        public int ProjectId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="DevelopmentHistoryInfoList", DataFormat=DataFormat.Default)]
        public List<ProDevelopmentHistoryInfo> DevelopmentHistoryInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

