﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SceneCameraControlNtf")]
    public class SceneCameraControlNtf : IExtensible
    {
        private uint _LookAtTargetObjId;
        private bool _LookAtStar;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LookAtTargetObjId;
        private static DelegateBridge __Hotfix_set_LookAtTargetObjId;
        private static DelegateBridge __Hotfix_get_LookAtStar;
        private static DelegateBridge __Hotfix_set_LookAtStar;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="LookAtTargetObjId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LookAtTargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(2, IsRequired=false, Name="LookAtStar", DataFormat=DataFormat.Default)]
        public bool LookAtStar
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

