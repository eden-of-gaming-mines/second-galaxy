﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="FactionCreditLevelRewardAck")]
    public class FactionCreditLevelRewardAck : IExtensible
    {
        private int _Result;
        private int _Id;
        private int _GrandFaction;
        private readonly List<ProStoreItemUpdateInfo> _UpdateItem;
        private readonly List<ProCurrencyUpdateInfo> _UpdateCurrency;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_UpdateItem;
        private static DelegateBridge __Hotfix_get_UpdateCurrency;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public int GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="UpdateItem", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> UpdateItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="UpdateCurrency", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> UpdateCurrency
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

