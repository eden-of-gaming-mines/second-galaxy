﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBuildingExInfo")]
    public class ProGuildBuildingExInfo : IExtensible
    {
        private ulong _InstanceId;
        private bool _ShowBuildingDetailInfo;
        private ProGuildMineralBalanceInfo _MineralInfo;
        private ProGuildProduceInfo _ProduceInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_ShowBuildingDetailInfo;
        private static DelegateBridge __Hotfix_set_ShowBuildingDetailInfo;
        private static DelegateBridge __Hotfix_get_MineralInfo;
        private static DelegateBridge __Hotfix_set_MineralInfo;
        private static DelegateBridge __Hotfix_get_ProduceInfo;
        private static DelegateBridge __Hotfix_set_ProduceInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(2, IsRequired=false, Name="ShowBuildingDetailInfo", DataFormat=DataFormat.Default)]
        public bool ShowBuildingDetailInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="MineralInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildMineralBalanceInfo MineralInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="ProduceInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildProduceInfo ProduceInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

