﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TechUpgradeNtf")]
    public class TechUpgradeNtf : IExtensible
    {
        private int _TechId;
        private int _TechLevel;
        private bool _NotifyByTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TechId;
        private static DelegateBridge __Hotfix_set_TechId;
        private static DelegateBridge __Hotfix_get_TechLevel;
        private static DelegateBridge __Hotfix_set_TechLevel;
        private static DelegateBridge __Hotfix_get_NotifyByTime;
        private static DelegateBridge __Hotfix_set_NotifyByTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TechId", DataFormat=DataFormat.TwosComplement)]
        public int TechId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="TechLevel", DataFormat=DataFormat.TwosComplement)]
        public int TechLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="NotifyByTime", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool NotifyByTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

