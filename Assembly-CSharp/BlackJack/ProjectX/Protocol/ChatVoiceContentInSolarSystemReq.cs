﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ChatVoiceContentInSolarSystemReq")]
    public class ChatVoiceContentInSolarSystemReq : IExtensible
    {
        private readonly List<ulong> _InstanceIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="InstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> InstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

