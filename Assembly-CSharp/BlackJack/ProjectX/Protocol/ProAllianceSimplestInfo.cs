﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProAllianceSimplestInfo")]
    public class ProAllianceSimplestInfo : IExtensible
    {
        private uint _AllianceId;
        private string _Name;
        private ProAllianceLogoInfo _LogoInfo;
        private uint _LeaderGuildId;
        private string _LeaderGuildName;
        private string _LeaderGuildCode;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_LogoInfo;
        private static DelegateBridge __Hotfix_set_LogoInfo;
        private static DelegateBridge __Hotfix_get_LeaderGuildId;
        private static DelegateBridge __Hotfix_set_LeaderGuildId;
        private static DelegateBridge __Hotfix_get_LeaderGuildName;
        private static DelegateBridge __Hotfix_set_LeaderGuildName;
        private static DelegateBridge __Hotfix_get_LeaderGuildCode;
        private static DelegateBridge __Hotfix_set_LeaderGuildCode;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="AllianceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="LogoInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProAllianceLogoInfo LogoInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="LeaderGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint LeaderGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(5, IsRequired=false, Name="LeaderGuildName", DataFormat=DataFormat.Default)]
        public string LeaderGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="LeaderGuildCode", DataFormat=DataFormat.Default), DefaultValue("")]
        public string LeaderGuildCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

