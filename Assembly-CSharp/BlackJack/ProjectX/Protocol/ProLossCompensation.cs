﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLossCompensation")]
    public class ProLossCompensation : IExtensible
    {
        private ulong _InstanceId;
        private readonly List<ProLostItem> _LostItemList;
        private int _CompensationStatus;
        private long _CompensationCreateTime;
        private long _CompensationCloseTime;
        private int _CompensationCloseReason;
        private string _PlayerUserId;
        private string _PlayerUserName;
        private int _PlayerMotherShipSolarSystemId;
        private long _KillTime;
        private ProKillRecordOpponentInfo _WinnerInfo;
        private ProKillRecordOpponentInfo _LoserInfo;
        private int _KillSolarSystemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_LostItemList;
        private static DelegateBridge __Hotfix_get_CompensationStatus;
        private static DelegateBridge __Hotfix_set_CompensationStatus;
        private static DelegateBridge __Hotfix_get_CompensationCreateTime;
        private static DelegateBridge __Hotfix_set_CompensationCreateTime;
        private static DelegateBridge __Hotfix_get_CompensationCloseTime;
        private static DelegateBridge __Hotfix_set_CompensationCloseTime;
        private static DelegateBridge __Hotfix_get_CompensationCloseReason;
        private static DelegateBridge __Hotfix_set_CompensationCloseReason;
        private static DelegateBridge __Hotfix_get_PlayerUserId;
        private static DelegateBridge __Hotfix_set_PlayerUserId;
        private static DelegateBridge __Hotfix_get_PlayerUserName;
        private static DelegateBridge __Hotfix_set_PlayerUserName;
        private static DelegateBridge __Hotfix_get_PlayerMotherShipSolarSystemId;
        private static DelegateBridge __Hotfix_set_PlayerMotherShipSolarSystemId;
        private static DelegateBridge __Hotfix_get_KillTime;
        private static DelegateBridge __Hotfix_set_KillTime;
        private static DelegateBridge __Hotfix_get_WinnerInfo;
        private static DelegateBridge __Hotfix_set_WinnerInfo;
        private static DelegateBridge __Hotfix_get_LoserInfo;
        private static DelegateBridge __Hotfix_set_LoserInfo;
        private static DelegateBridge __Hotfix_get_KillSolarSystemId;
        private static DelegateBridge __Hotfix_set_KillSolarSystemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="LostItemList", DataFormat=DataFormat.Default)]
        public List<ProLostItem> LostItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="CompensationStatus", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CompensationStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="CompensationCreateTime", DataFormat=DataFormat.TwosComplement)]
        public long CompensationCreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="CompensationCloseTime", DataFormat=DataFormat.TwosComplement)]
        public long CompensationCloseTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="CompensationCloseReason", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CompensationCloseReason
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="PlayerUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="PlayerUserName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerUserName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="PlayerMotherShipSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int PlayerMotherShipSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(10, IsRequired=false, Name="KillTime", DataFormat=DataFormat.TwosComplement)]
        public long KillTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(11, IsRequired=false, Name="WinnerInfo", DataFormat=DataFormat.Default)]
        public ProKillRecordOpponentInfo WinnerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(12, IsRequired=false, Name="LoserInfo", DataFormat=DataFormat.Default)]
        public ProKillRecordOpponentInfo LoserInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(13, IsRequired=false, Name="KillSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int KillSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

