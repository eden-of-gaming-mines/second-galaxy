﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildMemberInfo")]
    public class ProGuildMemberInfo : IExtensible
    {
        private ProGuildMemberBasicInfo _BasicInfo;
        private ProGuildMemberDynamicInfo _DynamicInfo;
        private ProGuildMemberRuntimeInfo _RuntimeInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BasicInfo;
        private static DelegateBridge __Hotfix_set_BasicInfo;
        private static DelegateBridge __Hotfix_get_DynamicInfo;
        private static DelegateBridge __Hotfix_set_DynamicInfo;
        private static DelegateBridge __Hotfix_get_RuntimeInfo;
        private static DelegateBridge __Hotfix_set_RuntimeInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="BasicInfo", DataFormat=DataFormat.Default)]
        public ProGuildMemberBasicInfo BasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="DynamicInfo", DataFormat=DataFormat.Default)]
        public ProGuildMemberDynamicInfo DynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="RuntimeInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildMemberRuntimeInfo RuntimeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

