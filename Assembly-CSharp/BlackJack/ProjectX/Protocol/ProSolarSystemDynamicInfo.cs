﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSolarSystemDynamicInfo")]
    public class ProSolarSystemDynamicInfo : IExtensible
    {
        private int _SolarSystemID;
        private int _FlourishValue;
        private bool _IsInBattle;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemID;
        private static DelegateBridge __Hotfix_set_SolarSystemID;
        private static DelegateBridge __Hotfix_get_FlourishValue;
        private static DelegateBridge __Hotfix_set_FlourishValue;
        private static DelegateBridge __Hotfix_get_IsInBattle;
        private static DelegateBridge __Hotfix_set_IsInBattle;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemID", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="FlourishValue", DataFormat=DataFormat.TwosComplement)]
        public int FlourishValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="IsInBattle", DataFormat=DataFormat.Default)]
        public bool IsInBattle
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

