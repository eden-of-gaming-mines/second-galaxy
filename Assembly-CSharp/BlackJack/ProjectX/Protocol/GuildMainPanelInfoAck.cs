﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMainPanelInfoAck")]
    public class GuildMainPanelInfoAck : IExtensible
    {
        private int _Result;
        private ProGuildBasicInfo _BasicInfo;
        private ProGuildSimpleRuntimeInfo _SimpleRuntimeInfo;
        private ProGuildDynamicInfo _DynamicInfo;
        private uint _GuildMessageBoardInfoVersion;
        private readonly List<ProGuildMomentsInfo> _GuildMomentsBriefInfoList;
        private uint _MomentsBriefInfoVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_BasicInfo;
        private static DelegateBridge __Hotfix_set_BasicInfo;
        private static DelegateBridge __Hotfix_get_SimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_set_SimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_get_DynamicInfo;
        private static DelegateBridge __Hotfix_set_DynamicInfo;
        private static DelegateBridge __Hotfix_get_GuildMessageBoardInfoVersion;
        private static DelegateBridge __Hotfix_set_GuildMessageBoardInfoVersion;
        private static DelegateBridge __Hotfix_get_GuildMomentsBriefInfoList;
        private static DelegateBridge __Hotfix_get_MomentsBriefInfoVersion;
        private static DelegateBridge __Hotfix_set_MomentsBriefInfoVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="BasicInfo", DataFormat=DataFormat.Default)]
        public ProGuildBasicInfo BasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="SimpleRuntimeInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimpleRuntimeInfo SimpleRuntimeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="DynamicInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildDynamicInfo DynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="GuildMessageBoardInfoVersion", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GuildMessageBoardInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="GuildMomentsBriefInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildMomentsInfo> GuildMomentsBriefInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="MomentsBriefInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public uint MomentsBriefInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

