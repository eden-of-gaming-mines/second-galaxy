﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerChipNtf")]
    public class DSPlayerChipNtf : IExtensible
    {
        private uint _Version;
        private readonly List<ProCharChipSchemeInfo> _ChipSchemeList;
        private int _CurrSchemeIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_ChipSchemeList;
        private static DelegateBridge __Hotfix_get_CurrSchemeIndex;
        private static DelegateBridge __Hotfix_set_CurrSchemeIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="ChipSchemeList", DataFormat=DataFormat.Default)]
        public List<ProCharChipSchemeInfo> ChipSchemeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CurrSchemeIndex", DataFormat=DataFormat.TwosComplement)]
        public int CurrSchemeIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

