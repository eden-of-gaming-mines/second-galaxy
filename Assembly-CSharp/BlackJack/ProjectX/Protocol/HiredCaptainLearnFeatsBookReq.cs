﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptainLearnFeatsBookReq")]
    public class HiredCaptainLearnFeatsBookReq : IExtensible
    {
        private ulong _CaptainInstanceId;
        private int _FeatsBookItemIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_FeatsBookItemIndex;
        private static DelegateBridge __Hotfix_set_FeatsBookItemIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="FeatsBookItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int FeatsBookItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

