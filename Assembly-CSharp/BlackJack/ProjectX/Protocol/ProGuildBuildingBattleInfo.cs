﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBuildingBattleInfo")]
    public class ProGuildBuildingBattleInfo : IExtensible
    {
        private ulong _InstanceId;
        private int _SolarSystemId;
        private int _Type;
        private uint _AttackerGuildId;
        private ulong _BattleInstanceId;
        private int _FlourishLevel;
        private float _ShieldMax;
        private float _ArmorMax;
        private float _AttackerBuildingShieldMax;
        private float _AttackerBuildingArmorMax;
        private float _ShieldCurr;
        private float _ArmorCurr;
        private float _AttackerBuildingShieldCurr;
        private float _AttackerBuildingArmorCurr;
        private bool _IsEnterHalfReinforcement;
        private bool _IsOffline;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_AttackerGuildId;
        private static DelegateBridge __Hotfix_set_AttackerGuildId;
        private static DelegateBridge __Hotfix_get_BattleInstanceId;
        private static DelegateBridge __Hotfix_set_BattleInstanceId;
        private static DelegateBridge __Hotfix_get_FlourishLevel;
        private static DelegateBridge __Hotfix_set_FlourishLevel;
        private static DelegateBridge __Hotfix_get_ShieldMax;
        private static DelegateBridge __Hotfix_set_ShieldMax;
        private static DelegateBridge __Hotfix_get_ArmorMax;
        private static DelegateBridge __Hotfix_set_ArmorMax;
        private static DelegateBridge __Hotfix_get_AttackerBuildingShieldMax;
        private static DelegateBridge __Hotfix_set_AttackerBuildingShieldMax;
        private static DelegateBridge __Hotfix_get_AttackerBuildingArmorMax;
        private static DelegateBridge __Hotfix_set_AttackerBuildingArmorMax;
        private static DelegateBridge __Hotfix_get_ShieldCurr;
        private static DelegateBridge __Hotfix_set_ShieldCurr;
        private static DelegateBridge __Hotfix_get_ArmorCurr;
        private static DelegateBridge __Hotfix_set_ArmorCurr;
        private static DelegateBridge __Hotfix_get_AttackerBuildingShieldCurr;
        private static DelegateBridge __Hotfix_set_AttackerBuildingShieldCurr;
        private static DelegateBridge __Hotfix_get_AttackerBuildingArmorCurr;
        private static DelegateBridge __Hotfix_set_AttackerBuildingArmorCurr;
        private static DelegateBridge __Hotfix_get_IsEnterHalfReinforcement;
        private static DelegateBridge __Hotfix_set_IsEnterHalfReinforcement;
        private static DelegateBridge __Hotfix_get_IsOffline;
        private static DelegateBridge __Hotfix_set_IsOffline;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="Type", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="AttackerGuildId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint AttackerGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="BattleInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong BattleInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="FlourishLevel", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FlourishLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(7, IsRequired=false, Name="ShieldMax", DataFormat=DataFormat.FixedSize)]
        public float ShieldMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="ArmorMax", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float ArmorMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="AttackerBuildingShieldMax", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float AttackerBuildingShieldMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(10, IsRequired=false, Name="AttackerBuildingArmorMax", DataFormat=DataFormat.FixedSize)]
        public float AttackerBuildingArmorMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="ShieldCurr", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float ShieldCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="ArmorCurr", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float ArmorCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(13, IsRequired=false, Name="AttackerBuildingShieldCurr", DataFormat=DataFormat.FixedSize)]
        public float AttackerBuildingShieldCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="AttackerBuildingArmorCurr", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float AttackerBuildingArmorCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=false, Name="IsEnterHalfReinforcement", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsEnterHalfReinforcement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(0x10, IsRequired=false, Name="IsOffline", DataFormat=DataFormat.Default)]
        public bool IsOffline
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

