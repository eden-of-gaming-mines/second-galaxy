﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TradeListInfoAck")]
    public class TradeListInfoAck : IExtensible
    {
        private int _Result;
        private bool _IsLegal;
        private int _TradeItemId;
        private uint _DataVersion;
        private readonly List<ProTradeListItemInfo> _TradeListItemInfos;
        private long _NextRefreshTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_IsLegal;
        private static DelegateBridge __Hotfix_set_IsLegal;
        private static DelegateBridge __Hotfix_get_TradeItemId;
        private static DelegateBridge __Hotfix_set_TradeItemId;
        private static DelegateBridge __Hotfix_get_DataVersion;
        private static DelegateBridge __Hotfix_set_DataVersion;
        private static DelegateBridge __Hotfix_get_TradeListItemInfos;
        private static DelegateBridge __Hotfix_get_NextRefreshTime;
        private static DelegateBridge __Hotfix_set_NextRefreshTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="IsLegal", DataFormat=DataFormat.Default)]
        public bool IsLegal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="TradeItemId", DataFormat=DataFormat.TwosComplement)]
        public int TradeItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="DataVersion", DataFormat=DataFormat.TwosComplement)]
        public uint DataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="TradeListItemInfos", DataFormat=DataFormat.Default)]
        public List<ProTradeListItemInfo> TradeListItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="NextRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

