﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildTeleportTunnelInfo")]
    public class ProGuildTeleportTunnelInfo : IExtensible
    {
        private ulong _InstanceId;
        private int _SolarSystemId;
        private float _TeleportRange;
        private bool _IsFlagShipTeleportTunnel;
        private bool _IsLimitedByAnitTeleport;
        private int _FlagShipSceneConfId;
        private string _FlagShipGuildCodeName;
        private string _FlagShipDriverName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_TeleportRange;
        private static DelegateBridge __Hotfix_set_TeleportRange;
        private static DelegateBridge __Hotfix_get_IsFlagShipTeleportTunnel;
        private static DelegateBridge __Hotfix_set_IsFlagShipTeleportTunnel;
        private static DelegateBridge __Hotfix_get_IsLimitedByAnitTeleport;
        private static DelegateBridge __Hotfix_set_IsLimitedByAnitTeleport;
        private static DelegateBridge __Hotfix_get_FlagShipSceneConfId;
        private static DelegateBridge __Hotfix_set_FlagShipSceneConfId;
        private static DelegateBridge __Hotfix_get_FlagShipGuildCodeName;
        private static DelegateBridge __Hotfix_set_FlagShipGuildCodeName;
        private static DelegateBridge __Hotfix_get_FlagShipDriverName;
        private static DelegateBridge __Hotfix_set_FlagShipDriverName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="TeleportRange", DataFormat=DataFormat.FixedSize)]
        public float TeleportRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="IsFlagShipTeleportTunnel", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsFlagShipTeleportTunnel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(5, IsRequired=false, Name="IsLimitedByAnitTeleport", DataFormat=DataFormat.Default)]
        public bool IsLimitedByAnitTeleport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="FlagShipSceneConfId", DataFormat=DataFormat.TwosComplement)]
        public int FlagShipSceneConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(7, IsRequired=false, Name="FlagShipGuildCodeName", DataFormat=DataFormat.Default)]
        public string FlagShipGuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="FlagShipDriverName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string FlagShipDriverName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

