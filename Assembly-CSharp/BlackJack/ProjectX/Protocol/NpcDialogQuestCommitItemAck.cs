﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="NpcDialogQuestCommitItemAck")]
    public class NpcDialogQuestCommitItemAck : IExtensible
    {
        private int _Result;
        private ProShipStoreItemUpdateInfo _ShipStoreItemUpdateInfo;
        private ProStoreItemUpdateInfo _StoreItemUpdateInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ShipStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_set_ShipStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_get_StoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_set_StoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ShipStoreItemUpdateInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProShipStoreItemUpdateInfo ShipStoreItemUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="StoreItemUpdateInfo", DataFormat=DataFormat.Default)]
        public ProStoreItemUpdateInfo StoreItemUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

