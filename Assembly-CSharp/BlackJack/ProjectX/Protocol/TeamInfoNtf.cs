﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TeamInfoNtf")]
    public class TeamInfoNtf : IExtensible
    {
        private uint _TeamInstanceId;
        private readonly List<ProTeamMemberInfo> _MemberList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TeamInstanceId;
        private static DelegateBridge __Hotfix_set_TeamInstanceId;
        private static DelegateBridge __Hotfix_get_MemberList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TeamInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint TeamInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="MemberList", DataFormat=DataFormat.Default)]
        public List<ProTeamMemberInfo> MemberList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

