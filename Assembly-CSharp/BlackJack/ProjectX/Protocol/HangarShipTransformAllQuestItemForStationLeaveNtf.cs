﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipTransformAllQuestItemForStationLeaveNtf")]
    public class HangarShipTransformAllQuestItemForStationLeaveNtf : IExtensible
    {
        private ulong _ShipInstanceId;
        private readonly List<ProStoreItemTransformInfo> _ItemTransformInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipInstanceId;
        private static DelegateBridge __Hotfix_set_ShipInstanceId;
        private static DelegateBridge __Hotfix_get_ItemTransformInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="ShipInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong ShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="ItemTransformInfo", DataFormat=DataFormat.Default)]
        public List<ProStoreItemTransformInfo> ItemTransformInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

