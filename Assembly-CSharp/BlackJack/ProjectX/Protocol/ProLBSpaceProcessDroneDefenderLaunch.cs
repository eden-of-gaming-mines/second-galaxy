﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessDroneDefenderLaunch")]
    public class ProLBSpaceProcessDroneDefenderLaunch : IExtensible
    {
        private ProLBSpaceProcessDroneLaunchInfo _DroneDefenderLaunch;
        private double _AttackRange;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DroneDefenderLaunch;
        private static DelegateBridge __Hotfix_set_DroneDefenderLaunch;
        private static DelegateBridge __Hotfix_get_AttackRange;
        private static DelegateBridge __Hotfix_set_AttackRange;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="DroneDefenderLaunch", DataFormat=DataFormat.Default)]
        public ProLBSpaceProcessDroneLaunchInfo DroneDefenderLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="AttackRange", DataFormat=DataFormat.TwosComplement), DefaultValue((double) 0.0)]
        public double AttackRange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

