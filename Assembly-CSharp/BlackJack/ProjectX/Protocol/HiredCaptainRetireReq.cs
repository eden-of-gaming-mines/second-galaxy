﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptainRetireReq")]
    public class HiredCaptainRetireReq : IExtensible
    {
        private ulong _CaptainInstanceId;
        private int _SelectedFeatsId;
        private int _UseExtraItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_SelectedFeatsId;
        private static DelegateBridge __Hotfix_set_SelectedFeatsId;
        private static DelegateBridge __Hotfix_get_UseExtraItemCount;
        private static DelegateBridge __Hotfix_set_UseExtraItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="SelectedFeatsId", DataFormat=DataFormat.TwosComplement)]
        public int SelectedFeatsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="UseExtraItemCount", DataFormat=DataFormat.TwosComplement)]
        public int UseExtraItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

