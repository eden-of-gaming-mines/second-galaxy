﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildInfo")]
    public class ProGuildInfo : IExtensible
    {
        private ProGuildBasicInfo _BasicInfo;
        private ProGuildSimpleRuntimeInfo _RuntimeInfo;
        private ProGuildMemberListInfo _MemberInfo;
        private ProDiplomacyInfo _DiplomacyInfo;
        private readonly List<ProGuildSolarSystemInfo> _SolarSystemList;
        private ProGuildCurrencyInfo _CurrencyInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BasicInfo;
        private static DelegateBridge __Hotfix_set_BasicInfo;
        private static DelegateBridge __Hotfix_get_RuntimeInfo;
        private static DelegateBridge __Hotfix_set_RuntimeInfo;
        private static DelegateBridge __Hotfix_get_MemberInfo;
        private static DelegateBridge __Hotfix_set_MemberInfo;
        private static DelegateBridge __Hotfix_get_DiplomacyInfo;
        private static DelegateBridge __Hotfix_set_DiplomacyInfo;
        private static DelegateBridge __Hotfix_get_SolarSystemList;
        private static DelegateBridge __Hotfix_get_CurrencyInfo;
        private static DelegateBridge __Hotfix_set_CurrencyInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BasicInfo", DataFormat=DataFormat.Default)]
        public ProGuildBasicInfo BasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="RuntimeInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimpleRuntimeInfo RuntimeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="MemberInfo", DataFormat=DataFormat.Default)]
        public ProGuildMemberListInfo MemberInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="DiplomacyInfo", DataFormat=DataFormat.Default)]
        public ProDiplomacyInfo DiplomacyInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="SolarSystemList", DataFormat=DataFormat.Default)]
        public List<ProGuildSolarSystemInfo> SolarSystemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(6, IsRequired=false, Name="CurrencyInfo", DataFormat=DataFormat.Default)]
        public ProGuildCurrencyInfo CurrencyInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

