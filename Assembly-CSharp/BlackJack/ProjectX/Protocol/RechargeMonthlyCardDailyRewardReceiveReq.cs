﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="RechargeMonthlyCardDailyRewardReceiveReq")]
    public class RechargeMonthlyCardDailyRewardReceiveReq : IExtensible
    {
        private int _MonthlyCardId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MonthlyCardId;
        private static DelegateBridge __Hotfix_set_MonthlyCardId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="MonthlyCardId", DataFormat=DataFormat.TwosComplement)]
        public int MonthlyCardId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

