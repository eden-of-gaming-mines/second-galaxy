﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSceneGuildBuildingExtraDataInfo")]
    public class ProSceneGuildBuildingExtraDataInfo : IExtensible
    {
        private uint _ObjectId;
        private int _Status;
        private long _CurrStatusStartTime;
        private long _CurrStatusEndTime;
        private int _BattleStatus;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ObjectId;
        private static DelegateBridge __Hotfix_set_ObjectId;
        private static DelegateBridge __Hotfix_get_Status;
        private static DelegateBridge __Hotfix_set_Status;
        private static DelegateBridge __Hotfix_get_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_set_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_get_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_set_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_get_BattleStatus;
        private static DelegateBridge __Hotfix_set_BattleStatus;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ObjectId", DataFormat=DataFormat.TwosComplement)]
        public uint ObjectId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="Status", DataFormat=DataFormat.TwosComplement)]
        public int Status
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="CurrStatusStartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CurrStatusStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="CurrStatusEndTime", DataFormat=DataFormat.TwosComplement)]
        public long CurrStatusEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="BattleStatus", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BattleStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

