﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildTradePortTradeInfoAck")]
    public class GuildTradePortTradeInfoAck : IExtensible
    {
        private int _Result;
        private ProGuildTradePortExtraInfo _GuildTradePortExtraInfo;
        private ProGuildTradePurchaseInfo _GuildTradePurchaseInfo;
        private ProGuildTradeTransportInfo _GuildTradeTransportInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_GuildTradePortExtraInfo;
        private static DelegateBridge __Hotfix_set_GuildTradePortExtraInfo;
        private static DelegateBridge __Hotfix_get_GuildTradePurchaseInfo;
        private static DelegateBridge __Hotfix_set_GuildTradePurchaseInfo;
        private static DelegateBridge __Hotfix_get_GuildTradeTransportInfo;
        private static DelegateBridge __Hotfix_set_GuildTradeTransportInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="GuildTradePortExtraInfo", DataFormat=DataFormat.Default)]
        public ProGuildTradePortExtraInfo GuildTradePortExtraInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="GuildTradePurchaseInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildTradePurchaseInfo GuildTradePurchaseInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="GuildTradeTransportInfo", DataFormat=DataFormat.Default)]
        public ProGuildTradeTransportInfo GuildTradeTransportInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

