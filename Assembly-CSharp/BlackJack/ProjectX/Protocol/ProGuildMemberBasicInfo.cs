﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildMemberBasicInfo")]
    public class ProGuildMemberBasicInfo : IExtensible
    {
        private string _GameUserID;
        private int _Seq;
        private readonly List<int> _Jobs;
        private long _JoinTime;
        private ulong _FleetId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GameUserID;
        private static DelegateBridge __Hotfix_set_GameUserID;
        private static DelegateBridge __Hotfix_get_Seq;
        private static DelegateBridge __Hotfix_set_Seq;
        private static DelegateBridge __Hotfix_get_Jobs;
        private static DelegateBridge __Hotfix_get_JoinTime;
        private static DelegateBridge __Hotfix_set_JoinTime;
        private static DelegateBridge __Hotfix_get_FleetId;
        private static DelegateBridge __Hotfix_set_FleetId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GameUserID", DataFormat=DataFormat.Default)]
        public string GameUserID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Seq", DataFormat=DataFormat.TwosComplement)]
        public int Seq
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="Jobs", DataFormat=DataFormat.TwosComplement)]
        public List<int> Jobs
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="JoinTime", DataFormat=DataFormat.TwosComplement)]
        public long JoinTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="FleetId", DataFormat=DataFormat.TwosComplement)]
        public ulong FleetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

