﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMServerAccountInfoAck")]
    public class GMServerAccountInfoAck : IExtensible
    {
        private int _Result;
        private readonly List<AccountUserInfo> _Info;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Info;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="Info", DataFormat=DataFormat.Default)]
        public List<AccountUserInfo> Info
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [Serializable, ProtoContract(Name="AccountUserInfo")]
        public class AccountUserInfo : IExtensible
        {
            private string _UserId;
            private string _Name;
            private int _Level;
            private ulong _BindMoney;
            private string _AccountName;
            private ulong _TradeMoney;
            private ulong _RealMoney;
            private uint _ExpCurr;
            private long _ExpTotal;
            private IExtension extensionObject;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_UserId;
            private static DelegateBridge __Hotfix_set_UserId;
            private static DelegateBridge __Hotfix_get_Name;
            private static DelegateBridge __Hotfix_set_Name;
            private static DelegateBridge __Hotfix_get_Level;
            private static DelegateBridge __Hotfix_set_Level;
            private static DelegateBridge __Hotfix_get_BindMoney;
            private static DelegateBridge __Hotfix_set_BindMoney;
            private static DelegateBridge __Hotfix_get_AccountName;
            private static DelegateBridge __Hotfix_set_AccountName;
            private static DelegateBridge __Hotfix_get_TradeMoney;
            private static DelegateBridge __Hotfix_set_TradeMoney;
            private static DelegateBridge __Hotfix_get_RealMoney;
            private static DelegateBridge __Hotfix_set_RealMoney;
            private static DelegateBridge __Hotfix_get_ExpCurr;
            private static DelegateBridge __Hotfix_set_ExpCurr;
            private static DelegateBridge __Hotfix_get_ExpTotal;
            private static DelegateBridge __Hotfix_set_ExpTotal;
            private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

            [MethodImpl(0x8000)]
            IExtension IExtensible.GetExtensionObject(bool createIfMissing)
            {
            }

            [ProtoMember(1, IsRequired=true, Name="UserId", DataFormat=DataFormat.Default)]
            public string UserId
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(2, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
            public string Name
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(3, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
            public int Level
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(4, IsRequired=true, Name="BindMoney", DataFormat=DataFormat.TwosComplement)]
            public ulong BindMoney
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(5, IsRequired=false, Name="AccountName", DataFormat=DataFormat.Default), DefaultValue("")]
            public string AccountName
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(6, IsRequired=true, Name="TradeMoney", DataFormat=DataFormat.TwosComplement)]
            public ulong TradeMoney
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(7, IsRequired=true, Name="RealMoney", DataFormat=DataFormat.TwosComplement)]
            public ulong RealMoney
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(8, IsRequired=true, Name="ExpCurr", DataFormat=DataFormat.TwosComplement)]
            public uint ExpCurr
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }

            [ProtoMember(9, IsRequired=true, Name="ExpTotal", DataFormat=DataFormat.TwosComplement)]
            public long ExpTotal
            {
                [MethodImpl(0x8000)]
                get
                {
                }
                [MethodImpl(0x8000)]
                set
                {
                }
            }
        }
    }
}

