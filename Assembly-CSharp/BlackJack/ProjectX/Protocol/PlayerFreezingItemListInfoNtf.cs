﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerFreezingItemListInfoNtf")]
    public class PlayerFreezingItemListInfoNtf : IExtensible
    {
        private readonly List<ProFreezingItemInfo> _InfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="InfoList", DataFormat=DataFormat.Default)]
        public List<ProFreezingItemInfo> InfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

