﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="MailGetAllMailReq")]
    public class MailGetAllMailReq : IExtensible
    {
        private readonly List<int> _StoredIndexList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StoredIndexList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="StoredIndexList", DataFormat=DataFormat.TwosComplement)]
        public List<int> StoredIndexList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

