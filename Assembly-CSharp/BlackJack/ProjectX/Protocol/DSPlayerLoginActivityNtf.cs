﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerLoginActivityNtf")]
    public class DSPlayerLoginActivityNtf : IExtensible
    {
        private uint _Version;
        private readonly List<ProDailyLoginRewardInfo> _dailyLoginRewardList;
        private ProCommanderAuthInfo _CommanderAuthInfo;
        private ProCMDailySignInfo _CMDailySignInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_DailyLoginRewardList;
        private static DelegateBridge __Hotfix_get_CommanderAuthInfo;
        private static DelegateBridge __Hotfix_set_CommanderAuthInfo;
        private static DelegateBridge __Hotfix_get_CMDailySignInfo;
        private static DelegateBridge __Hotfix_set_CMDailySignInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="dailyLoginRewardList", DataFormat=DataFormat.Default)]
        public List<ProDailyLoginRewardInfo> DailyLoginRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CommanderAuthInfo", DataFormat=DataFormat.Default)]
        public ProCommanderAuthInfo CommanderAuthInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CMDailySignInfo", DataFormat=DataFormat.Default)]
        public ProCMDailySignInfo CMDailySignInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

