﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildSentryInterestSceneShipTypeInfo")]
    public class ProGuildSentryInterestSceneShipTypeInfo : IExtensible
    {
        private int _ShipType;
        private int _ShipCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipType;
        private static DelegateBridge __Hotfix_set_ShipType;
        private static DelegateBridge __Hotfix_get_ShipCount;
        private static DelegateBridge __Hotfix_set_ShipCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ShipType", DataFormat=DataFormat.TwosComplement)]
        public int ShipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ShipCount", DataFormat=DataFormat.TwosComplement)]
        public int ShipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

