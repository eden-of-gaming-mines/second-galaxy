﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProRechargeStoreGoodsInfo")]
    public class ProRechargeStoreGoodsInfo : IExtensible
    {
        private int _GoodsType;
        private int _GoodsId;
        private int _TotalBuyNums;
        private int _CurrCycleBuyNums;
        private long _NextRefreshTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GoodsType;
        private static DelegateBridge __Hotfix_set_GoodsType;
        private static DelegateBridge __Hotfix_get_GoodsId;
        private static DelegateBridge __Hotfix_set_GoodsId;
        private static DelegateBridge __Hotfix_get_TotalBuyNums;
        private static DelegateBridge __Hotfix_set_TotalBuyNums;
        private static DelegateBridge __Hotfix_get_CurrCycleBuyNums;
        private static DelegateBridge __Hotfix_set_CurrCycleBuyNums;
        private static DelegateBridge __Hotfix_get_NextRefreshTime;
        private static DelegateBridge __Hotfix_set_NextRefreshTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GoodsType", DataFormat=DataFormat.TwosComplement)]
        public int GoodsType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="GoodsId", DataFormat=DataFormat.TwosComplement)]
        public int GoodsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="TotalBuyNums", DataFormat=DataFormat.TwosComplement)]
        public int TotalBuyNums
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CurrCycleBuyNums", DataFormat=DataFormat.TwosComplement)]
        public int CurrCycleBuyNums
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="NextRefreshTime", DataFormat=DataFormat.TwosComplement)]
        public long NextRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

