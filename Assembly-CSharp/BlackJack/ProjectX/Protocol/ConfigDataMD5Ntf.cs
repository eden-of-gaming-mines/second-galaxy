﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ConfigDataMD5Ntf")]
    public class ConfigDataMD5Ntf : IExtensible
    {
        private readonly List<ProConfigFileMD5Info> _ConfigFileMD5InfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConfigFileMD5InfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ConfigFileMD5InfoList", DataFormat=DataFormat.Default)]
        public List<ProConfigFileMD5Info> ConfigFileMD5InfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

