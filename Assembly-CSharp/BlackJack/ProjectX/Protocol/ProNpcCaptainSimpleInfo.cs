﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProNpcCaptainSimpleInfo")]
    public class ProNpcCaptainSimpleInfo : IExtensible
    {
        private ulong _Instance;
        private int _LastNameId;
        private int _FirstNameId;
        private int _CurrShipId;
        private int _GrandFaction;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_set_Instance;
        private static DelegateBridge __Hotfix_get_LastNameId;
        private static DelegateBridge __Hotfix_set_LastNameId;
        private static DelegateBridge __Hotfix_get_FirstNameId;
        private static DelegateBridge __Hotfix_set_FirstNameId;
        private static DelegateBridge __Hotfix_get_CurrShipId;
        private static DelegateBridge __Hotfix_set_CurrShipId;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="Instance", DataFormat=DataFormat.TwosComplement)]
        public ulong Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="LastNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int LastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="FirstNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CurrShipId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="GrandFaction", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

