﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProBufInfo")]
    public class ProBufInfo : IExtensible
    {
        private int _ConfigId;
        private uint _InstanceId;
        private uint _LifeEndTime;
        private uint _SrcTargetId;
        private float _InstanceParam;
        private uint _LeftTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_LifeEndTime;
        private static DelegateBridge __Hotfix_set_LifeEndTime;
        private static DelegateBridge __Hotfix_get_SrcTargetId;
        private static DelegateBridge __Hotfix_set_SrcTargetId;
        private static DelegateBridge __Hotfix_get_InstanceParam;
        private static DelegateBridge __Hotfix_set_InstanceParam;
        private static DelegateBridge __Hotfix_get_LeftTime;
        private static DelegateBridge __Hotfix_set_LeftTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="ConfigId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="LifeEndTime", DataFormat=DataFormat.TwosComplement)]
        public uint LifeEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="SrcTargetId", DataFormat=DataFormat.TwosComplement)]
        public uint SrcTargetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="InstanceParam", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float InstanceParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="LeftTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LeftTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

