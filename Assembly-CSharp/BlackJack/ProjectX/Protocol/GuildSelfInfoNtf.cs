﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildSelfInfoNtf")]
    public class GuildSelfInfoNtf : IExtensible
    {
        private ProGuildBasicInfo _BasicInfo;
        private ProGuildMemberBasicInfo _MemberBasicInfo;
        private ProGuildMemberDynamicInfo _MemberDynamicInfo;
        private ProDiplomacyInfo _DiplomacyInfo;
        private ProGuildSimpleRuntimeInfo _SimpleRuntimeInfo;
        private ProGuildCurrencyInfo _CurrencyInfo;
        private ProGuildFleetDSInitInfo _FleetDSInitInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BasicInfo;
        private static DelegateBridge __Hotfix_set_BasicInfo;
        private static DelegateBridge __Hotfix_get_MemberBasicInfo;
        private static DelegateBridge __Hotfix_set_MemberBasicInfo;
        private static DelegateBridge __Hotfix_get_MemberDynamicInfo;
        private static DelegateBridge __Hotfix_set_MemberDynamicInfo;
        private static DelegateBridge __Hotfix_get_DiplomacyInfo;
        private static DelegateBridge __Hotfix_set_DiplomacyInfo;
        private static DelegateBridge __Hotfix_get_SimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_set_SimpleRuntimeInfo;
        private static DelegateBridge __Hotfix_get_CurrencyInfo;
        private static DelegateBridge __Hotfix_set_CurrencyInfo;
        private static DelegateBridge __Hotfix_get_FleetDSInitInfo;
        private static DelegateBridge __Hotfix_set_FleetDSInitInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="BasicInfo", DataFormat=DataFormat.Default)]
        public ProGuildBasicInfo BasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="MemberBasicInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildMemberBasicInfo MemberBasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="MemberDynamicInfo", DataFormat=DataFormat.Default)]
        public ProGuildMemberDynamicInfo MemberDynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="DiplomacyInfo", DataFormat=DataFormat.Default)]
        public ProDiplomacyInfo DiplomacyInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="SimpleRuntimeInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildSimpleRuntimeInfo SimpleRuntimeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(6, IsRequired=false, Name="CurrencyInfo", DataFormat=DataFormat.Default)]
        public ProGuildCurrencyInfo CurrencyInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="FleetDSInitInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildFleetDSInitInfo FleetDSInitInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

