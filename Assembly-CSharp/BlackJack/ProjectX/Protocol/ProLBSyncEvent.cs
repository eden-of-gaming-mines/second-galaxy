﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEvent")]
    public class ProLBSyncEvent : IExtensible
    {
        private int _Type;
        private uint _TargetObjId;
        private int _WeaponGroupIndex;
        private uint _ReloadAmmoStarTime;
        private readonly List<ProItemInfo> _ReloadAmmoItemList;
        private int _SuperGroupEnergyAdd;
        private uint _DroneProcessLaunchId;
        private uint _MissileProcessLaunchId;
        private readonly List<uint> _AttackChanceList;
        private uint _GroupCDEndTime;
        private int _NoticeType;
        private int _EquipSlotType;
        private uint _DropBoxPickEndTime;
        private readonly List<ProShipStoreItemInfo> _DropBoxPickItems;
        private int _SceneWingShipSetting;
        private long _PVPFlagEndTime;
        private float _IncCriminalLevel;
        private int _FactionId;
        private ProLBSyncEventInteractionEnd _InteractionEnd;
        private ProSpaceProcessBulletGunLaunch _BulletGunLaunchProcess;
        private ProSpaceProcessLaserLaunch _LaserLaunchProcess;
        private ProLBSpaceProcessDroneFighterLaunch _DroneFighterLaunchProcess;
        private ProLBSpaceProcessDroneDefenderLaunch _DroneDefenderLaunchProcess;
        private ProLBSpaceProcessDroneSniperLaunch _DroneSniperLaunchProcess;
        private ProLBSpaceProcessSuperRailgunLaunch _SuperRailgunLaunchProcess;
        private ProLBSpaceProcessSuperMissileLaunch _SuperMissileLaunchProcess;
        private ProLBSpaceProcessSuperPlasmaLaunch _SuperPlasmaLaunchProcess;
        private ProLBSpaceProcessSuperLaserLaunch _SuperLaserLaunchProcess;
        private ProLBSpaceProcessSuperDroneLaunch _SuperDroneLaunchProcess;
        private ProLBSynEventSetInteractionInfo _InteractionInfo;
        private ProLBSyncEventAttachBuf _AttachBuf;
        private ProLBSyncEventDetachBuf _DetachBuf;
        private ProLBSynEventOnDotDamage _DotDamage;
        private ProLBSynEventOnOnHotHeal _HotHeal;
        private ProSpaceProcessEquipAttachBufLaunch _EquipAttachBufLaunch;
        private ProSpaceProcessEquipBlinkLaunch _EquipBlinkLaunch;
        private ProLBSyncEventNpcShipAnimEffect _AnimEffect;
        private ProSpaceProcessEquipInvisibleLaunch _EquipInvisibleLaunch;
        private ProLBSyncEventResetWeaponEquipGroupCD _WeaponEquipGroupCD;
        private ProSpaceProcessEquipAddEnergyAndAttachBufLaunch _EquipAddEnergyAndAttachBufLaunch;
        private ProSpaceProcessEquipAddShieldAndAttachBufLaunch _EquipAddShieldAndAttachBufLaunch;
        private ProLBSpaceProcessSuperEquipAoeLaunch _SuperEquipAoeLaunchProcess;
        private ProLBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch _ShipShield2ExtraShieldAndAttachBuffLaunch;
        private ProLBSyncEventSuperEnergyModify _SuperEnergyModify;
        private ProLBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch _SuperEquipClearCDAndAttachBuffLaunch;
        private ProLBSpaceProcessEquipChannelLaunch _SuperEquipChannelLaunch;
        private ProLBSpaceProcessEquipChannelEnd _SuperEquipChannelEnd;
        private readonly List<ProShipStoreItemInfo> _AddOrRemoveShipStoreItem;
        private bool _IsHideInTargetList;
        private bool _IsNotAttackable;
        private uint _InteractionEndTime;
        private ProLBSyncEventSetEnergy _SetEnergyEvent;
        private ProLBSpaceProcessEquipPeriodicDamageLaunch _EquipPeriodicDamageLaunch;
        private ProLBSpaceProcessEquipPeriodicDamageEnd _EquipPeriodicDamageEnd;
        private ProLBSyncEventSetShipShield _ShipShieldSyncEvent;
        private ProLBSyncEventKillOtherTarget _KillOtherTargetEvent;
        private ProLBSyncEventStartJumping _StartJumpingEvent;
        private ProLBSyncEventTacticalEquipTimeInfo _TacticalEquipTimeInfo;
        private ProLBSyncEventOnBlinkEnd _BlinkEndInfo;
        private ProLBSyncEventOnBlinkInterrupted _BlinkInterrupted;
        private ProLBSyncEventResetTacticalEquipGroupCD _TaticalEquipGroupCD;
        private ProSpaceProcessEquipAntiJumpingForceShieldLaunch _AntiJumpingForceShieldLaunchEvent;
        private ProSpaceProcessSuperEquipTransformToTeleportTunnelLaunch _TransformToTeleportTunnelLaunchEvent;
        private ProLBSyncEventSuperEquipDurationEnd _SuperEquipDurationEnd;
        private ProLBSyncEventAttachBufDuplicate _AttachBufDuplicate;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_TargetObjId;
        private static DelegateBridge __Hotfix_set_TargetObjId;
        private static DelegateBridge __Hotfix_get_WeaponGroupIndex;
        private static DelegateBridge __Hotfix_set_WeaponGroupIndex;
        private static DelegateBridge __Hotfix_get_ReloadAmmoStarTime;
        private static DelegateBridge __Hotfix_set_ReloadAmmoStarTime;
        private static DelegateBridge __Hotfix_get_ReloadAmmoItemList;
        private static DelegateBridge __Hotfix_get_SuperGroupEnergyAdd;
        private static DelegateBridge __Hotfix_set_SuperGroupEnergyAdd;
        private static DelegateBridge __Hotfix_get_DroneProcessLaunchId;
        private static DelegateBridge __Hotfix_set_DroneProcessLaunchId;
        private static DelegateBridge __Hotfix_get_MissileProcessLaunchId;
        private static DelegateBridge __Hotfix_set_MissileProcessLaunchId;
        private static DelegateBridge __Hotfix_get_AttackChanceList;
        private static DelegateBridge __Hotfix_get_GroupCDEndTime;
        private static DelegateBridge __Hotfix_set_GroupCDEndTime;
        private static DelegateBridge __Hotfix_get_NoticeType;
        private static DelegateBridge __Hotfix_set_NoticeType;
        private static DelegateBridge __Hotfix_get_EquipSlotType;
        private static DelegateBridge __Hotfix_set_EquipSlotType;
        private static DelegateBridge __Hotfix_get_DropBoxPickEndTime;
        private static DelegateBridge __Hotfix_set_DropBoxPickEndTime;
        private static DelegateBridge __Hotfix_get_DropBoxPickItems;
        private static DelegateBridge __Hotfix_get_SceneWingShipSetting;
        private static DelegateBridge __Hotfix_set_SceneWingShipSetting;
        private static DelegateBridge __Hotfix_get_PVPFlagEndTime;
        private static DelegateBridge __Hotfix_set_PVPFlagEndTime;
        private static DelegateBridge __Hotfix_get_IncCriminalLevel;
        private static DelegateBridge __Hotfix_set_IncCriminalLevel;
        private static DelegateBridge __Hotfix_get_FactionId;
        private static DelegateBridge __Hotfix_set_FactionId;
        private static DelegateBridge __Hotfix_get_InteractionEnd;
        private static DelegateBridge __Hotfix_set_InteractionEnd;
        private static DelegateBridge __Hotfix_get_BulletGunLaunchProcess;
        private static DelegateBridge __Hotfix_set_BulletGunLaunchProcess;
        private static DelegateBridge __Hotfix_get_LaserLaunchProcess;
        private static DelegateBridge __Hotfix_set_LaserLaunchProcess;
        private static DelegateBridge __Hotfix_get_DroneFighterLaunchProcess;
        private static DelegateBridge __Hotfix_set_DroneFighterLaunchProcess;
        private static DelegateBridge __Hotfix_get_DroneDefenderLaunchProcess;
        private static DelegateBridge __Hotfix_set_DroneDefenderLaunchProcess;
        private static DelegateBridge __Hotfix_get_DroneSniperLaunchProcess;
        private static DelegateBridge __Hotfix_set_DroneSniperLaunchProcess;
        private static DelegateBridge __Hotfix_get_SuperRailgunLaunchProcess;
        private static DelegateBridge __Hotfix_set_SuperRailgunLaunchProcess;
        private static DelegateBridge __Hotfix_get_SuperMissileLaunchProcess;
        private static DelegateBridge __Hotfix_set_SuperMissileLaunchProcess;
        private static DelegateBridge __Hotfix_get_SuperPlasmaLaunchProcess;
        private static DelegateBridge __Hotfix_set_SuperPlasmaLaunchProcess;
        private static DelegateBridge __Hotfix_get_SuperLaserLaunchProcess;
        private static DelegateBridge __Hotfix_set_SuperLaserLaunchProcess;
        private static DelegateBridge __Hotfix_get_SuperDroneLaunchProcess;
        private static DelegateBridge __Hotfix_set_SuperDroneLaunchProcess;
        private static DelegateBridge __Hotfix_get_InteractionInfo;
        private static DelegateBridge __Hotfix_set_InteractionInfo;
        private static DelegateBridge __Hotfix_get_AttachBuf;
        private static DelegateBridge __Hotfix_set_AttachBuf;
        private static DelegateBridge __Hotfix_get_DetachBuf;
        private static DelegateBridge __Hotfix_set_DetachBuf;
        private static DelegateBridge __Hotfix_get_DotDamage;
        private static DelegateBridge __Hotfix_set_DotDamage;
        private static DelegateBridge __Hotfix_get_HotHeal;
        private static DelegateBridge __Hotfix_set_HotHeal;
        private static DelegateBridge __Hotfix_get_EquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_set_EquipAttachBufLaunch;
        private static DelegateBridge __Hotfix_get_EquipBlinkLaunch;
        private static DelegateBridge __Hotfix_set_EquipBlinkLaunch;
        private static DelegateBridge __Hotfix_get_AnimEffect;
        private static DelegateBridge __Hotfix_set_AnimEffect;
        private static DelegateBridge __Hotfix_get_EquipInvisibleLaunch;
        private static DelegateBridge __Hotfix_set_EquipInvisibleLaunch;
        private static DelegateBridge __Hotfix_get_WeaponEquipGroupCD;
        private static DelegateBridge __Hotfix_set_WeaponEquipGroupCD;
        private static DelegateBridge __Hotfix_get_EquipAddEnergyAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_set_EquipAddEnergyAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_get_EquipAddShieldAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_set_EquipAddShieldAndAttachBufLaunch;
        private static DelegateBridge __Hotfix_get_SuperEquipAoeLaunchProcess;
        private static DelegateBridge __Hotfix_set_SuperEquipAoeLaunchProcess;
        private static DelegateBridge __Hotfix_get_ShipShield2ExtraShieldAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_set_ShipShield2ExtraShieldAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_get_SuperEnergyModify;
        private static DelegateBridge __Hotfix_set_SuperEnergyModify;
        private static DelegateBridge __Hotfix_get_SuperEquipClearCDAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_set_SuperEquipClearCDAndAttachBuffLaunch;
        private static DelegateBridge __Hotfix_get_SuperEquipChannelLaunch;
        private static DelegateBridge __Hotfix_set_SuperEquipChannelLaunch;
        private static DelegateBridge __Hotfix_get_SuperEquipChannelEnd;
        private static DelegateBridge __Hotfix_set_SuperEquipChannelEnd;
        private static DelegateBridge __Hotfix_get_AddOrRemoveShipStoreItem;
        private static DelegateBridge __Hotfix_get_IsHideInTargetList;
        private static DelegateBridge __Hotfix_set_IsHideInTargetList;
        private static DelegateBridge __Hotfix_get_IsNotAttackable;
        private static DelegateBridge __Hotfix_set_IsNotAttackable;
        private static DelegateBridge __Hotfix_get_InteractionEndTime;
        private static DelegateBridge __Hotfix_set_InteractionEndTime;
        private static DelegateBridge __Hotfix_get_SetEnergyEvent;
        private static DelegateBridge __Hotfix_set_SetEnergyEvent;
        private static DelegateBridge __Hotfix_get_EquipPeriodicDamageLaunch;
        private static DelegateBridge __Hotfix_set_EquipPeriodicDamageLaunch;
        private static DelegateBridge __Hotfix_get_EquipPeriodicDamageEnd;
        private static DelegateBridge __Hotfix_set_EquipPeriodicDamageEnd;
        private static DelegateBridge __Hotfix_get_ShipShieldSyncEvent;
        private static DelegateBridge __Hotfix_set_ShipShieldSyncEvent;
        private static DelegateBridge __Hotfix_get_KillOtherTargetEvent;
        private static DelegateBridge __Hotfix_set_KillOtherTargetEvent;
        private static DelegateBridge __Hotfix_get_StartJumpingEvent;
        private static DelegateBridge __Hotfix_set_StartJumpingEvent;
        private static DelegateBridge __Hotfix_get_TacticalEquipTimeInfo;
        private static DelegateBridge __Hotfix_set_TacticalEquipTimeInfo;
        private static DelegateBridge __Hotfix_get_BlinkEndInfo;
        private static DelegateBridge __Hotfix_set_BlinkEndInfo;
        private static DelegateBridge __Hotfix_get_BlinkInterrupted;
        private static DelegateBridge __Hotfix_set_BlinkInterrupted;
        private static DelegateBridge __Hotfix_get_TaticalEquipGroupCD;
        private static DelegateBridge __Hotfix_set_TaticalEquipGroupCD;
        private static DelegateBridge __Hotfix_get_AntiJumpingForceShieldLaunchEvent;
        private static DelegateBridge __Hotfix_set_AntiJumpingForceShieldLaunchEvent;
        private static DelegateBridge __Hotfix_get_TransformToTeleportTunnelLaunchEvent;
        private static DelegateBridge __Hotfix_set_TransformToTeleportTunnelLaunchEvent;
        private static DelegateBridge __Hotfix_get_SuperEquipDurationEnd;
        private static DelegateBridge __Hotfix_set_SuperEquipDurationEnd;
        private static DelegateBridge __Hotfix_get_AttachBufDuplicate;
        private static DelegateBridge __Hotfix_set_AttachBufDuplicate;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="TargetObjId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint TargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="WeaponGroupIndex", DataFormat=DataFormat.TwosComplement)]
        public int WeaponGroupIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="ReloadAmmoStarTime", DataFormat=DataFormat.TwosComplement)]
        public uint ReloadAmmoStarTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="ReloadAmmoItemList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> ReloadAmmoItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(7, IsRequired=false, Name="SuperGroupEnergyAdd", DataFormat=DataFormat.TwosComplement)]
        public int SuperGroupEnergyAdd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(8, IsRequired=false, Name="DroneProcessLaunchId", DataFormat=DataFormat.TwosComplement)]
        public uint DroneProcessLaunchId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="MissileProcessLaunchId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint MissileProcessLaunchId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="AttackChanceList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> AttackChanceList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="GroupCDEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GroupCDEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(12, IsRequired=false, Name="NoticeType", DataFormat=DataFormat.TwosComplement)]
        public int NoticeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(13, IsRequired=false, Name="EquipSlotType", DataFormat=DataFormat.TwosComplement)]
        public int EquipSlotType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="DropBoxPickEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint DropBoxPickEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, Name="DropBoxPickItems", DataFormat=DataFormat.Default)]
        public List<ProShipStoreItemInfo> DropBoxPickItems
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="SceneWingShipSetting", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SceneWingShipSetting
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=false, Name="PVPFlagEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long PVPFlagEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(0x12, IsRequired=false, Name="IncCriminalLevel", DataFormat=DataFormat.FixedSize)]
        public float IncCriminalLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=false, Name="FactionId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=false, Name="InteractionEnd", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventInteractionEnd InteractionEnd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x15, IsRequired=false, Name="BulletGunLaunchProcess", DataFormat=DataFormat.Default)]
        public ProSpaceProcessBulletGunLaunch BulletGunLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x16, IsRequired=false, Name="LaserLaunchProcess", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceProcessLaserLaunch LaserLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x17, IsRequired=false, Name="DroneFighterLaunchProcess", DataFormat=DataFormat.Default)]
        public ProLBSpaceProcessDroneFighterLaunch DroneFighterLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x18, IsRequired=false, Name="DroneDefenderLaunchProcess", DataFormat=DataFormat.Default)]
        public ProLBSpaceProcessDroneDefenderLaunch DroneDefenderLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x19, IsRequired=false, Name="DroneSniperLaunchProcess", DataFormat=DataFormat.Default)]
        public ProLBSpaceProcessDroneSniperLaunch DroneSniperLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x1a, IsRequired=false, Name="SuperRailgunLaunchProcess", DataFormat=DataFormat.Default)]
        public ProLBSpaceProcessSuperRailgunLaunch SuperRailgunLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x1b, IsRequired=false, Name="SuperMissileLaunchProcess", DataFormat=DataFormat.Default)]
        public ProLBSpaceProcessSuperMissileLaunch SuperMissileLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1d, IsRequired=false, Name="SuperPlasmaLaunchProcess", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessSuperPlasmaLaunch SuperPlasmaLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(30, IsRequired=false, Name="SuperLaserLaunchProcess", DataFormat=DataFormat.Default)]
        public ProLBSpaceProcessSuperLaserLaunch SuperLaserLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1f, IsRequired=false, Name="SuperDroneLaunchProcess", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessSuperDroneLaunch SuperDroneLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x20, IsRequired=false, Name="InteractionInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSynEventSetInteractionInfo InteractionInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x21, IsRequired=false, Name="AttachBuf", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventAttachBuf AttachBuf
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x22, IsRequired=false, Name="DetachBuf", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventDetachBuf DetachBuf
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x23, IsRequired=false, Name="DotDamage", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSynEventOnDotDamage DotDamage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x24, IsRequired=false, Name="HotHeal", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSynEventOnOnHotHeal HotHeal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x25, IsRequired=false, Name="EquipAttachBufLaunch", DataFormat=DataFormat.Default)]
        public ProSpaceProcessEquipAttachBufLaunch EquipAttachBufLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x26, IsRequired=false, Name="EquipBlinkLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceProcessEquipBlinkLaunch EquipBlinkLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x27, IsRequired=false, Name="AnimEffect", DataFormat=DataFormat.Default)]
        public ProLBSyncEventNpcShipAnimEffect AnimEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(40, IsRequired=false, Name="EquipInvisibleLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceProcessEquipInvisibleLaunch EquipInvisibleLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x29, IsRequired=false, Name="WeaponEquipGroupCD", DataFormat=DataFormat.Default)]
        public ProLBSyncEventResetWeaponEquipGroupCD WeaponEquipGroupCD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2a, IsRequired=false, Name="EquipAddEnergyAndAttachBufLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceProcessEquipAddEnergyAndAttachBufLaunch EquipAddEnergyAndAttachBufLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2b, IsRequired=false, Name="EquipAddShieldAndAttachBufLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceProcessEquipAddShieldAndAttachBufLaunch EquipAddShieldAndAttachBufLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2c, IsRequired=false, Name="SuperEquipAoeLaunchProcess", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessSuperEquipAoeLaunch SuperEquipAoeLaunchProcess
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2d, IsRequired=false, Name="ShipShield2ExtraShieldAndAttachBuffLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessEquipShipShield2ExtraShieldAndAttachBuff2SelfLaunch ShipShield2ExtraShieldAndAttachBuffLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2e, IsRequired=false, Name="SuperEnergyModify", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventSuperEnergyModify SuperEnergyModify
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x2f, IsRequired=false, Name="SuperEquipClearCDAndAttachBuffLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch SuperEquipClearCDAndAttachBuffLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x30, IsRequired=false, Name="SuperEquipChannelLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessEquipChannelLaunch SuperEquipChannelLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x31, IsRequired=false, Name="SuperEquipChannelEnd", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessEquipChannelEnd SuperEquipChannelEnd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(50, Name="AddOrRemoveShipStoreItem", DataFormat=DataFormat.Default)]
        public List<ProShipStoreItemInfo> AddOrRemoveShipStoreItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(false), ProtoMember(0x33, IsRequired=false, Name="IsHideInTargetList", DataFormat=DataFormat.Default)]
        public bool IsHideInTargetList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x34, IsRequired=false, Name="IsNotAttackable", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsNotAttackable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x35, IsRequired=false, Name="InteractionEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint InteractionEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x36, IsRequired=false, Name="SetEnergyEvent", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventSetEnergy SetEnergyEvent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x37, IsRequired=false, Name="EquipPeriodicDamageLaunch", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessEquipPeriodicDamageLaunch EquipPeriodicDamageLaunch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x38, IsRequired=false, Name="EquipPeriodicDamageEnd", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSpaceProcessEquipPeriodicDamageEnd EquipPeriodicDamageEnd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x39, IsRequired=false, Name="ShipShieldSyncEvent", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventSetShipShield ShipShieldSyncEvent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x3a, IsRequired=false, Name="KillOtherTargetEvent", DataFormat=DataFormat.Default)]
        public ProLBSyncEventKillOtherTarget KillOtherTargetEvent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x3b, IsRequired=false, Name="StartJumpingEvent", DataFormat=DataFormat.Default)]
        public ProLBSyncEventStartJumping StartJumpingEvent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(60, IsRequired=false, Name="TacticalEquipTimeInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventTacticalEquipTimeInfo TacticalEquipTimeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x3d, IsRequired=false, Name="BlinkEndInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventOnBlinkEnd BlinkEndInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x3e, IsRequired=false, Name="BlinkInterrupted", DataFormat=DataFormat.Default)]
        public ProLBSyncEventOnBlinkInterrupted BlinkInterrupted
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x3f, IsRequired=false, Name="TaticalEquipGroupCD", DataFormat=DataFormat.Default)]
        public ProLBSyncEventResetTacticalEquipGroupCD TaticalEquipGroupCD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x40, IsRequired=false, Name="AntiJumpingForceShieldLaunchEvent", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceProcessEquipAntiJumpingForceShieldLaunch AntiJumpingForceShieldLaunchEvent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x41, IsRequired=false, Name="TransformToTeleportTunnelLaunchEvent", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceProcessSuperEquipTransformToTeleportTunnelLaunch TransformToTeleportTunnelLaunchEvent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x42, IsRequired=false, Name="SuperEquipDurationEnd", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProLBSyncEventSuperEquipDurationEnd SuperEquipDurationEnd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x43, IsRequired=false, Name="AttachBufDuplicate", DataFormat=DataFormat.Default)]
        public ProLBSyncEventAttachBufDuplicate AttachBufDuplicate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

