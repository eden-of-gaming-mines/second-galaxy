﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFleetMemberBasicInfo")]
    public class ProGuildFleetMemberBasicInfo : IExtensible
    {
        private string _GameUserId;
        private uint _FleetPositionFlag;
        private int _FireControllerSubstituteSeq;
        private int _NavigatorSubstituteSeq;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GameUserId;
        private static DelegateBridge __Hotfix_set_GameUserId;
        private static DelegateBridge __Hotfix_get_FleetPositionFlag;
        private static DelegateBridge __Hotfix_set_FleetPositionFlag;
        private static DelegateBridge __Hotfix_get_FireControllerSubstituteSeq;
        private static DelegateBridge __Hotfix_set_FireControllerSubstituteSeq;
        private static DelegateBridge __Hotfix_get_NavigatorSubstituteSeq;
        private static DelegateBridge __Hotfix_set_NavigatorSubstituteSeq;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GameUserId", DataFormat=DataFormat.Default)]
        public string GameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="FleetPositionFlag", DataFormat=DataFormat.TwosComplement)]
        public uint FleetPositionFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="FireControllerSubstituteSeq", DataFormat=DataFormat.TwosComplement)]
        public int FireControllerSubstituteSeq
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="NavigatorSubstituteSeq", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int NavigatorSubstituteSeq
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

