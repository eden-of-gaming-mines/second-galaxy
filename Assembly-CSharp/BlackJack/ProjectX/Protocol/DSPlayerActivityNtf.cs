﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerActivityNtf")]
    public class DSPlayerActivityNtf : IExtensible
    {
        private uint _Version;
        private readonly List<ProActivityInfo> _ActivityList;
        private readonly List<ProPlayerActivityInfo> _PlayerActivityList;
        private readonly List<int> _PlayerVitalityRewardList;
        private int _ActivityPoint;
        private int _Vitality;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_ActivityList;
        private static DelegateBridge __Hotfix_get_PlayerActivityList;
        private static DelegateBridge __Hotfix_get_PlayerVitalityRewardList;
        private static DelegateBridge __Hotfix_get_ActivityPoint;
        private static DelegateBridge __Hotfix_set_ActivityPoint;
        private static DelegateBridge __Hotfix_get_Vitality;
        private static DelegateBridge __Hotfix_set_Vitality;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="ActivityList", DataFormat=DataFormat.Default)]
        public List<ProActivityInfo> ActivityList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="PlayerActivityList", DataFormat=DataFormat.Default)]
        public List<ProPlayerActivityInfo> PlayerActivityList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="PlayerVitalityRewardList", DataFormat=DataFormat.TwosComplement)]
        public List<int> PlayerVitalityRewardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ActivityPoint", DataFormat=DataFormat.TwosComplement)]
        public int ActivityPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Vitality", DataFormat=DataFormat.TwosComplement)]
        public int Vitality
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

