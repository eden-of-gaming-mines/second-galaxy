﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StarFieldWormholeGateInfoAck")]
    public class StarFieldWormholeGateInfoAck : IExtensible
    {
        private int _StarFieldId;
        private readonly List<ProIdLevelInfo> _WormholeGateInfo;
        private long _WormholeNextRefreshTime;
        private readonly List<ProIdLevelInfo> _RareWormholeGateInfo;
        private long _RareWormholeNextRefreshTime;
        private uint _DataVersion;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StarFieldId;
        private static DelegateBridge __Hotfix_set_StarFieldId;
        private static DelegateBridge __Hotfix_get_WormholeGateInfo;
        private static DelegateBridge __Hotfix_get_WormholeNextRefreshTime;
        private static DelegateBridge __Hotfix_set_WormholeNextRefreshTime;
        private static DelegateBridge __Hotfix_get_RareWormholeGateInfo;
        private static DelegateBridge __Hotfix_get_RareWormholeNextRefreshTime;
        private static DelegateBridge __Hotfix_set_RareWormholeNextRefreshTime;
        private static DelegateBridge __Hotfix_get_DataVersion;
        private static DelegateBridge __Hotfix_set_DataVersion;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="StarFieldId", DataFormat=DataFormat.TwosComplement)]
        public int StarFieldId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="WormholeGateInfo", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> WormholeGateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="WormholeNextRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long WormholeNextRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="RareWormholeGateInfo", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> RareWormholeGateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="RareWormholeNextRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long RareWormholeNextRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="DataVersion", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint DataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

