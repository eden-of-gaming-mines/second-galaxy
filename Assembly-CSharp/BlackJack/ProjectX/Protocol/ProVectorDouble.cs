﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProVectorDouble")]
    public class ProVectorDouble : IExtensible
    {
        private double _X;
        private double _Y;
        private double _Z;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_X;
        private static DelegateBridge __Hotfix_set_X;
        private static DelegateBridge __Hotfix_get_Y;
        private static DelegateBridge __Hotfix_set_Y;
        private static DelegateBridge __Hotfix_get_Z;
        private static DelegateBridge __Hotfix_set_Z;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="X", DataFormat=DataFormat.TwosComplement)]
        public double X
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Y", DataFormat=DataFormat.TwosComplement)]
        public double Y
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Z", DataFormat=DataFormat.TwosComplement)]
        public double Z
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

