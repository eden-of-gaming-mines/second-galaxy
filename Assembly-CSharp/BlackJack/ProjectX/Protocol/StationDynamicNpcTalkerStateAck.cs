﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StationDynamicNpcTalkerStateAck")]
    public class StationDynamicNpcTalkerStateAck : IExtensible
    {
        private readonly List<ProStationDynamicNpcTalkerStateInfo> _NpcTalkerStateList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NpcTalkerStateList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="NpcTalkerStateList", DataFormat=DataFormat.Default)]
        public List<ProStationDynamicNpcTalkerStateInfo> NpcTalkerStateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

