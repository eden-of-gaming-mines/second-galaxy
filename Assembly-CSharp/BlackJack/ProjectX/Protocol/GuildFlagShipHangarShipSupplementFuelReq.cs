﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarShipSupplementFuelReq")]
    public class GuildFlagShipHangarShipSupplementFuelReq : IExtensible
    {
        private ulong _InstanceId;
        private readonly List<ProGuildFlagShipHangarShipSupplementFuelInfo> _HangarShipSupplementFuelInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_HangarShipSupplementFuelInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="HangarShipSupplementFuelInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildFlagShipHangarShipSupplementFuelInfo> HangarShipSupplementFuelInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

