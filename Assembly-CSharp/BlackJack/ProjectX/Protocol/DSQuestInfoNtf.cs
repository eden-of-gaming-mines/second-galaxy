﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSQuestInfoNtf")]
    public class DSQuestInfoNtf : IExtensible
    {
        private readonly List<ProQuestEnvirmentInfo> _EnvList;
        private readonly List<ProQuestProcessingInfo> _ProcessingList;
        private readonly List<ProQuestWaitForAcceptInfo> _WaitForAcceptList;
        private ProBitArrayExInfo _QuestCompletionSet;
        private int _DailyFreeQuestCompleteCount;
        private uint _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EnvList;
        private static DelegateBridge __Hotfix_get_ProcessingList;
        private static DelegateBridge __Hotfix_get_WaitForAcceptList;
        private static DelegateBridge __Hotfix_get_QuestCompletionSet;
        private static DelegateBridge __Hotfix_set_QuestCompletionSet;
        private static DelegateBridge __Hotfix_get_DailyFreeQuestCompleteCount;
        private static DelegateBridge __Hotfix_set_DailyFreeQuestCompleteCount;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="EnvList", DataFormat=DataFormat.Default)]
        public List<ProQuestEnvirmentInfo> EnvList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="ProcessingList", DataFormat=DataFormat.Default)]
        public List<ProQuestProcessingInfo> ProcessingList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="WaitForAcceptList", DataFormat=DataFormat.Default)]
        public List<ProQuestWaitForAcceptInfo> WaitForAcceptList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="QuestCompletionSet", DataFormat=DataFormat.Default)]
        public ProBitArrayExInfo QuestCompletionSet
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="DailyFreeQuestCompleteCount", DataFormat=DataFormat.TwosComplement)]
        public int DailyFreeQuestCompleteCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

