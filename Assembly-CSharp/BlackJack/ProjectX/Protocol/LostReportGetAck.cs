﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="LostReportGetAck")]
    public class LostReportGetAck : IExtensible
    {
        private int _Result;
        private ProBuildingLostReport _BuildingLostReport;
        private ProSovereignLostReport _SovereignLostReport;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_BuildingLostReport;
        private static DelegateBridge __Hotfix_set_BuildingLostReport;
        private static DelegateBridge __Hotfix_get_SovereignLostReport;
        private static DelegateBridge __Hotfix_set_SovereignLostReport;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="BuildingLostReport", DataFormat=DataFormat.Default)]
        public ProBuildingLostReport BuildingLostReport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="SovereignLostReport", DataFormat=DataFormat.Default)]
        public ProSovereignLostReport SovereignLostReport
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

