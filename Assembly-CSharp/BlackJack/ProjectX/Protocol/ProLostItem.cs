﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLostItem")]
    public class ProLostItem : IExtensible
    {
        private ProLostItemInfo _Item;
        private int _CompensationStatus;
        private int _CompensationDonatorType;
        private string _CompensationDonatorName;
        private long _CompensationTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Item;
        private static DelegateBridge __Hotfix_set_Item;
        private static DelegateBridge __Hotfix_get_CompensationStatus;
        private static DelegateBridge __Hotfix_set_CompensationStatus;
        private static DelegateBridge __Hotfix_get_CompensationDonatorType;
        private static DelegateBridge __Hotfix_set_CompensationDonatorType;
        private static DelegateBridge __Hotfix_get_CompensationDonatorName;
        private static DelegateBridge __Hotfix_set_CompensationDonatorName;
        private static DelegateBridge __Hotfix_get_CompensationTime;
        private static DelegateBridge __Hotfix_set_CompensationTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Item", DataFormat=DataFormat.Default)]
        public ProLostItemInfo Item
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="CompensationStatus", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CompensationStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="CompensationDonatorType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CompensationDonatorType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CompensationDonatorName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string CompensationDonatorName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="CompensationTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CompensationTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

