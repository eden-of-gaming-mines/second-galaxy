﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildProductionLineStartReq")]
    public class GuildProductionLineStartReq : IExtensible
    {
        private int _ProduceTempleteId;
        private int _ProduceCount;
        private ulong _LineId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ProduceTempleteId;
        private static DelegateBridge __Hotfix_set_ProduceTempleteId;
        private static DelegateBridge __Hotfix_get_ProduceCount;
        private static DelegateBridge __Hotfix_set_ProduceCount;
        private static DelegateBridge __Hotfix_get_LineId;
        private static DelegateBridge __Hotfix_set_LineId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ProduceTempleteId", DataFormat=DataFormat.TwosComplement)]
        public int ProduceTempleteId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ProduceCount", DataFormat=DataFormat.TwosComplement)]
        public int ProduceCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="LineId", DataFormat=DataFormat.TwosComplement)]
        public ulong LineId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

