﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDevelopmentHistoryInfo")]
    public class ProDevelopmentHistoryInfo : IExtensible
    {
        private int _DevelopState;
        private float _ReserchValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DevelopState;
        private static DelegateBridge __Hotfix_set_DevelopState;
        private static DelegateBridge __Hotfix_get_ReserchValue;
        private static DelegateBridge __Hotfix_set_ReserchValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="DevelopState", DataFormat=DataFormat.TwosComplement)]
        public int DevelopState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ReserchValue", DataFormat=DataFormat.FixedSize)]
        public float ReserchValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

