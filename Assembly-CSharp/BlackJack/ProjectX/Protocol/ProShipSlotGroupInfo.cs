﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipSlotGroupInfo")]
    public class ProShipSlotGroupInfo : IExtensible
    {
        private int _ConfigId;
        private int _ItemType;
        private bool _ItemIsBind;
        private int _ItemIndex;
        private bool _ItemIsFreezing;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_ItemIsBind;
        private static DelegateBridge __Hotfix_set_ItemIsBind;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;
        private static DelegateBridge __Hotfix_get_ItemIsFreezing;
        private static DelegateBridge __Hotfix_set_ItemIsFreezing;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="ConfigId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ItemType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ItemIsBind", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool ItemIsBind
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="ItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ItemIsFreezing", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool ItemIsFreezing
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

