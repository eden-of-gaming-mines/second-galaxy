﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipTransformSingleItemToMSStoreReq")]
    public class HangarShipTransformSingleItemToMSStoreReq : IExtensible
    {
        private int _HangarShipIndex;
        private int _StoreItemIndex;
        private long _StoreItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_StoreItemIndex;
        private static DelegateBridge __Hotfix_set_StoreItemIndex;
        private static DelegateBridge __Hotfix_get_StoreItemCount;
        private static DelegateBridge __Hotfix_set_StoreItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int StoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="StoreItemCount", DataFormat=DataFormat.TwosComplement)]
        public long StoreItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

