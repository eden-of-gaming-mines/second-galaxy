﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBenefitsReceiveReq")]
    public class GuildBenefitsReceiveReq : IExtensible
    {
        private ulong _BenefitsInstanceId;
        private readonly List<ProSimpleItemInfoWithCount> _ItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BenefitsInstanceId;
        private static DelegateBridge __Hotfix_set_BenefitsInstanceId;
        private static DelegateBridge __Hotfix_get_ItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BenefitsInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong BenefitsInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="ItemList", DataFormat=DataFormat.Default)]
        public List<ProSimpleItemInfoWithCount> ItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

