﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProQuestProcessingInfo")]
    public class ProQuestProcessingInfo : IExtensible
    {
        private int _QuestId;
        private int _InstanceId;
        private int _SceneSolarSystemId;
        private ProVectorDouble _SceneLocation;
        private long _AcceptTime;
        private bool _CompletedWaitConfirm;
        private readonly List<int> _CompleteCondParamList;
        private int _QuestFactionId;
        private bool _IsFail;
        private int _Level;
        private float _RewardBonusMulti;
        private int _QuestSrcType;
        private int _CurrQuestPoolCount;
        private int _SceneId;
        private float _GuildRewardMulti;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_QuestId;
        private static DelegateBridge __Hotfix_set_QuestId;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_SceneSolarSystemId;
        private static DelegateBridge __Hotfix_set_SceneSolarSystemId;
        private static DelegateBridge __Hotfix_get_SceneLocation;
        private static DelegateBridge __Hotfix_set_SceneLocation;
        private static DelegateBridge __Hotfix_get_AcceptTime;
        private static DelegateBridge __Hotfix_set_AcceptTime;
        private static DelegateBridge __Hotfix_get_CompletedWaitConfirm;
        private static DelegateBridge __Hotfix_set_CompletedWaitConfirm;
        private static DelegateBridge __Hotfix_get_CompleteCondParamList;
        private static DelegateBridge __Hotfix_get_QuestFactionId;
        private static DelegateBridge __Hotfix_set_QuestFactionId;
        private static DelegateBridge __Hotfix_get_IsFail;
        private static DelegateBridge __Hotfix_set_IsFail;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_RewardBonusMulti;
        private static DelegateBridge __Hotfix_set_RewardBonusMulti;
        private static DelegateBridge __Hotfix_get_QuestSrcType;
        private static DelegateBridge __Hotfix_set_QuestSrcType;
        private static DelegateBridge __Hotfix_get_CurrQuestPoolCount;
        private static DelegateBridge __Hotfix_set_CurrQuestPoolCount;
        private static DelegateBridge __Hotfix_get_SceneId;
        private static DelegateBridge __Hotfix_set_SceneId;
        private static DelegateBridge __Hotfix_get_GuildRewardMulti;
        private static DelegateBridge __Hotfix_set_GuildRewardMulti;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="QuestId", DataFormat=DataFormat.TwosComplement)]
        public int QuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public int InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="SceneSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SceneSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="SceneLocation", DataFormat=DataFormat.Default)]
        public ProVectorDouble SceneLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="AcceptTime", DataFormat=DataFormat.TwosComplement)]
        public long AcceptTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="CompletedWaitConfirm", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool CompletedWaitConfirm
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="CompleteCondParamList", DataFormat=DataFormat.TwosComplement)]
        public List<int> CompleteCondParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(8, IsRequired=false, Name="QuestFactionId", DataFormat=DataFormat.TwosComplement)]
        public int QuestFactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="IsFail", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsFail
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(10, IsRequired=false, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(11, IsRequired=false, Name="RewardBonusMulti", DataFormat=DataFormat.FixedSize)]
        public float RewardBonusMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="QuestSrcType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int QuestSrcType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="CurrQuestPoolCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrQuestPoolCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(14, IsRequired=false, Name="SceneId", DataFormat=DataFormat.TwosComplement)]
        public int SceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=false, Name="GuildRewardMulti", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float GuildRewardMulti
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

