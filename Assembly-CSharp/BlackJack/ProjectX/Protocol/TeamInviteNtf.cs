﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TeamInviteNtf")]
    public class TeamInviteNtf : IExtensible
    {
        private uint _TeamInstanceId;
        private string _InviteGameUserId;
        private string _InvitePlayerName;
        private int _InviteSolarSystemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TeamInstanceId;
        private static DelegateBridge __Hotfix_set_TeamInstanceId;
        private static DelegateBridge __Hotfix_get_InviteGameUserId;
        private static DelegateBridge __Hotfix_set_InviteGameUserId;
        private static DelegateBridge __Hotfix_get_InvitePlayerName;
        private static DelegateBridge __Hotfix_set_InvitePlayerName;
        private static DelegateBridge __Hotfix_get_InviteSolarSystemId;
        private static DelegateBridge __Hotfix_set_InviteSolarSystemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="TeamInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint TeamInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="InviteGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string InviteGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(3, IsRequired=false, Name="InvitePlayerName", DataFormat=DataFormat.Default)]
        public string InvitePlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="InviteSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int InviteSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

