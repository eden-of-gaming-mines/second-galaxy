﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildSentryPlayerInfo")]
    public class ProGuildSentryPlayerInfo : IExtensible
    {
        private string _GameUserid;
        private int _ThreatValue;
        private int _ShipId;
        private ProVectorDouble _Location;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GameUserid;
        private static DelegateBridge __Hotfix_set_GameUserid;
        private static DelegateBridge __Hotfix_get_ThreatValue;
        private static DelegateBridge __Hotfix_set_ThreatValue;
        private static DelegateBridge __Hotfix_get_ShipId;
        private static DelegateBridge __Hotfix_set_ShipId;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GameUserid", DataFormat=DataFormat.Default)]
        public string GameUserid
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ThreatValue", DataFormat=DataFormat.TwosComplement)]
        public int ThreatValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipId", DataFormat=DataFormat.TwosComplement)]
        public int ShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Location", DataFormat=DataFormat.Default)]
        public ProVectorDouble Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

