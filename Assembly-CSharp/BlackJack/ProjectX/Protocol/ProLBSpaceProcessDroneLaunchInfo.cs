﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessDroneLaunchInfo")]
    public class ProLBSpaceProcessDroneLaunchInfo : IExtensible
    {
        private uint _InstanceId;
        private uint _StartTime;
        private bool _IsBurst;
        private uint _CostEnergy;
        private uint _FormupTime;
        private uint _AllUnitFireTime;
        private uint _FlyTime;
        private uint _FightTime;
        private uint _CD4DroneNextAttack;
        private uint _CD4NextDroneAttack;
        private double _HitRateFinal;
        private double _CriticalRateFinal;
        private readonly List<ProLBProcessSingleUnitLaunchInfo> _UnitLaunchList;
        private readonly List<uint> _ChargeList;
        private uint _CostFuel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_IsBurst;
        private static DelegateBridge __Hotfix_set_IsBurst;
        private static DelegateBridge __Hotfix_get_CostEnergy;
        private static DelegateBridge __Hotfix_set_CostEnergy;
        private static DelegateBridge __Hotfix_get_FormupTime;
        private static DelegateBridge __Hotfix_set_FormupTime;
        private static DelegateBridge __Hotfix_get_AllUnitFireTime;
        private static DelegateBridge __Hotfix_set_AllUnitFireTime;
        private static DelegateBridge __Hotfix_get_FlyTime;
        private static DelegateBridge __Hotfix_set_FlyTime;
        private static DelegateBridge __Hotfix_get_FightTime;
        private static DelegateBridge __Hotfix_set_FightTime;
        private static DelegateBridge __Hotfix_get_CD4DroneNextAttack;
        private static DelegateBridge __Hotfix_set_CD4DroneNextAttack;
        private static DelegateBridge __Hotfix_get_CD4NextDroneAttack;
        private static DelegateBridge __Hotfix_set_CD4NextDroneAttack;
        private static DelegateBridge __Hotfix_get_HitRateFinal;
        private static DelegateBridge __Hotfix_set_HitRateFinal;
        private static DelegateBridge __Hotfix_get_CriticalRateFinal;
        private static DelegateBridge __Hotfix_set_CriticalRateFinal;
        private static DelegateBridge __Hotfix_get_UnitLaunchList;
        private static DelegateBridge __Hotfix_get_ChargeList;
        private static DelegateBridge __Hotfix_get_CostFuel;
        private static DelegateBridge __Hotfix_set_CostFuel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public uint StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="IsBurst", DataFormat=DataFormat.Default)]
        public bool IsBurst
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CostEnergy", DataFormat=DataFormat.TwosComplement)]
        public uint CostEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="FormupTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint FormupTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="AllUnitFireTime", DataFormat=DataFormat.TwosComplement)]
        public uint AllUnitFireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="FlyTime", DataFormat=DataFormat.TwosComplement)]
        public uint FlyTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="FightTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint FightTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(9, IsRequired=false, Name="CD4DroneNextAttack", DataFormat=DataFormat.TwosComplement)]
        public uint CD4DroneNextAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="CD4NextDroneAttack", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint CD4NextDroneAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((double) 0.0), ProtoMember(11, IsRequired=false, Name="HitRateFinal", DataFormat=DataFormat.TwosComplement)]
        public double HitRateFinal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((double) 0.0), ProtoMember(12, IsRequired=false, Name="CriticalRateFinal", DataFormat=DataFormat.TwosComplement)]
        public double CriticalRateFinal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, Name="UnitLaunchList", DataFormat=DataFormat.Default)]
        public List<ProLBProcessSingleUnitLaunchInfo> UnitLaunchList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, Name="ChargeList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> ChargeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(15, IsRequired=false, Name="CostFuel", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint CostFuel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

