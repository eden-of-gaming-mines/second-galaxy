﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventResetWeaponEquipGroupCD")]
    public class ProLBSyncEventResetWeaponEquipGroupCD : IExtensible
    {
        private readonly List<uint> _HighSlotGroupCDEndTime;
        private readonly List<uint> _MiddleSlotGroupCDEndTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_HighSlotGroupCDEndTime;
        private static DelegateBridge __Hotfix_get_MiddleSlotGroupCDEndTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="HighSlotGroupCDEndTime", DataFormat=DataFormat.TwosComplement)]
        public List<uint> HighSlotGroupCDEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="MiddleSlotGroupCDEndTime", DataFormat=DataFormat.TwosComplement)]
        public List<uint> MiddleSlotGroupCDEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

