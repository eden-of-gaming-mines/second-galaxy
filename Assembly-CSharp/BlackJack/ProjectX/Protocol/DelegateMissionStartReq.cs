﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DelegateMissionStartReq")]
    public class DelegateMissionStartReq : IExtensible
    {
        private ulong _SignalInstanceId;
        private readonly List<ulong> _HiredCaptainInstanceIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SignalInstanceId;
        private static DelegateBridge __Hotfix_set_SignalInstanceId;
        private static DelegateBridge __Hotfix_get_HiredCaptainInstanceIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="SignalInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong SignalInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="HiredCaptainInstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> HiredCaptainInstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

