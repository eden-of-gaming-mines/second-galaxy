﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ActivityInfoNtf")]
    public class ActivityInfoNtf : IExtensible
    {
        private readonly List<ProActivityInfo> _ActivityList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ActivityList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ActivityList", DataFormat=DataFormat.Default)]
        public List<ProActivityInfo> ActivityList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

