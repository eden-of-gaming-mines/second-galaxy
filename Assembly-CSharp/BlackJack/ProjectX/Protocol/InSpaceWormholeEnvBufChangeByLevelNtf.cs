﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceWormholeEnvBufChangeByLevelNtf")]
    public class InSpaceWormholeEnvBufChangeByLevelNtf : IExtensible
    {
        private int _OrgLevel;
        private int _CurrLevel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OrgLevel;
        private static DelegateBridge __Hotfix_set_OrgLevel;
        private static DelegateBridge __Hotfix_get_CurrLevel;
        private static DelegateBridge __Hotfix_set_CurrLevel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="OrgLevel", DataFormat=DataFormat.TwosComplement)]
        public int OrgLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CurrLevel", DataFormat=DataFormat.TwosComplement)]
        public int CurrLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

