﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerDrivingLicenseInfoNtf")]
    public class DSPlayerDrivingLicenseInfoNtf : IExtensible
    {
        private uint _Version;
        private ProDrivingLicenseUpgradeInfo _UpgradingInfo;
        private ProBitArrayExInfo _RewardState;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_UpgradingInfo;
        private static DelegateBridge __Hotfix_set_UpgradingInfo;
        private static DelegateBridge __Hotfix_get_RewardState;
        private static DelegateBridge __Hotfix_set_RewardState;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="UpgradingInfo", DataFormat=DataFormat.Default)]
        public ProDrivingLicenseUpgradeInfo UpgradingInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="RewardState", DataFormat=DataFormat.Default)]
        public ProBitArrayExInfo RewardState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

