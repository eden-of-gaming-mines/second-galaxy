﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMServerMailSendReq")]
    public class GMServerMailSendReq : IExtensible
    {
        private readonly List<string> _PlayerName;
        private string _Title;
        private string _Content;
        private int _ExpiryDay;
        private bool _DeleteAfterRead;
        private readonly List<ProItemInfo> _AttchmentList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerName;
        private static DelegateBridge __Hotfix_get_Title;
        private static DelegateBridge __Hotfix_set_Title;
        private static DelegateBridge __Hotfix_get_Content;
        private static DelegateBridge __Hotfix_set_Content;
        private static DelegateBridge __Hotfix_get_ExpiryDay;
        private static DelegateBridge __Hotfix_set_ExpiryDay;
        private static DelegateBridge __Hotfix_get_DeleteAfterRead;
        private static DelegateBridge __Hotfix_set_DeleteAfterRead;
        private static DelegateBridge __Hotfix_get_AttchmentList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="PlayerName", DataFormat=DataFormat.Default)]
        public List<string> PlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Title", DataFormat=DataFormat.Default)]
        public string Title
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Content", DataFormat=DataFormat.Default)]
        public string Content
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="ExpiryDay", DataFormat=DataFormat.TwosComplement)]
        public int ExpiryDay
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="DeleteAfterRead", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool DeleteAfterRead
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="AttchmentList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> AttchmentList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

