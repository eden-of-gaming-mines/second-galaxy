﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerTechNtf")]
    public class DSPlayerTechNtf : IExtensible
    {
        private uint _Version;
        private readonly List<ProTechUpgradeInfo> _UpgradingTechList;
        private readonly List<ProIdLevelInfo> _TechList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_UpgradingTechList;
        private static DelegateBridge __Hotfix_get_TechList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="UpgradingTechList", DataFormat=DataFormat.Default)]
        public List<ProTechUpgradeInfo> UpgradingTechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="TechList", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> TechList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

