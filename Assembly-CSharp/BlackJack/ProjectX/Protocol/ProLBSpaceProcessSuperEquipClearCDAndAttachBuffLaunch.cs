﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch")]
    public class ProLBSpaceProcessSuperEquipClearCDAndAttachBuffLaunch : IExtensible
    {
        private ProSpaceProcessEquipAttachBufLaunch _AttachBufInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AttachBufInfo;
        private static DelegateBridge __Hotfix_set_AttachBufInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="AttachBufInfo", DataFormat=DataFormat.Default)]
        public ProSpaceProcessEquipAttachBufLaunch AttachBufInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

