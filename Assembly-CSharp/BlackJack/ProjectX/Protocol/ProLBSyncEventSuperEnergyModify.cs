﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventSuperEnergyModify")]
    public class ProLBSyncEventSuperEnergyModify : IExtensible
    {
        private float _SuperEnergyModifyValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SuperEnergyModifyValue;
        private static DelegateBridge __Hotfix_set_SuperEnergyModifyValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SuperEnergyModifyValue", DataFormat=DataFormat.FixedSize)]
        public float SuperEnergyModifyValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

