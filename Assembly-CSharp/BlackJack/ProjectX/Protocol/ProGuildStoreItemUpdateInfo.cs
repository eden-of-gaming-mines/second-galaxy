﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildStoreItemUpdateInfo")]
    public class ProGuildStoreItemUpdateInfo : IExtensible
    {
        private ProGuildStoreItemInfo _Item;
        private long _ChangeValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Item;
        private static DelegateBridge __Hotfix_set_Item;
        private static DelegateBridge __Hotfix_get_ChangeValue;
        private static DelegateBridge __Hotfix_set_ChangeValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Item", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildStoreItemInfo Item
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="ChangeValue", DataFormat=DataFormat.TwosComplement)]
        public long ChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

