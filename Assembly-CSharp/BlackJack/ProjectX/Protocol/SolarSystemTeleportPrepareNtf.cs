﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SolarSystemTeleportPrepareNtf")]
    public class SolarSystemTeleportPrepareNtf : IExtensible
    {
        private uint _StarGateObjId;
        private int _TeleportEffect;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StarGateObjId;
        private static DelegateBridge __Hotfix_set_StarGateObjId;
        private static DelegateBridge __Hotfix_get_TeleportEffect;
        private static DelegateBridge __Hotfix_set_TeleportEffect;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="StarGateObjId", DataFormat=DataFormat.TwosComplement)]
        public uint StarGateObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="TeleportEffect", DataFormat=DataFormat.TwosComplement)]
        public int TeleportEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

