﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CharBasicPropertiesAddReq")]
    public class CharBasicPropertiesAddReq : IExtensible
    {
        private readonly List<int> _PropertiesIdList;
        private readonly List<int> _CountList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PropertiesIdList;
        private static DelegateBridge __Hotfix_get_CountList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="PropertiesIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> PropertiesIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="CountList", DataFormat=DataFormat.TwosComplement)]
        public List<int> CountList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

