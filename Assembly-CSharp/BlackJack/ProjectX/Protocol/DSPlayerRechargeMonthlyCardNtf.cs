﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerRechargeMonthlyCardNtf")]
    public class DSPlayerRechargeMonthlyCardNtf : IExtensible
    {
        private uint _Version;
        private readonly List<ProRechargeMonthlyCard> _RechargeMonthlyCardList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_RechargeMonthlyCardList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="RechargeMonthlyCardList", DataFormat=DataFormat.Default)]
        public List<ProRechargeMonthlyCard> RechargeMonthlyCardList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

