﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildBattleReinforcementEndTimeSetReq")]
    public class GuildBattleReinforcementEndTimeSetReq : IExtensible
    {
        private int _EndHour;
        private int _EndMinuts;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EndHour;
        private static DelegateBridge __Hotfix_set_EndHour;
        private static DelegateBridge __Hotfix_get_EndMinuts;
        private static DelegateBridge __Hotfix_set_EndMinuts;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="EndHour", DataFormat=DataFormat.TwosComplement)]
        public int EndHour
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="EndMinuts", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int EndMinuts
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

