﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildSimpleInfoGetReq")]
    public class GuildSimpleInfoGetReq : IExtensible
    {
        private readonly List<uint> _GuildIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GuildIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="GuildIdList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> GuildIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

