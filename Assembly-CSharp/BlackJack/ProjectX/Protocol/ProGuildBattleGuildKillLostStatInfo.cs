﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBattleGuildKillLostStatInfo")]
    public class ProGuildBattleGuildKillLostStatInfo : IExtensible
    {
        private int _Group;
        private ProGuildSimplestInfo _GuildInfo;
        private double _SelfLostBindMoney;
        private double _KillingLostBindMoney;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Group;
        private static DelegateBridge __Hotfix_set_Group;
        private static DelegateBridge __Hotfix_get_GuildInfo;
        private static DelegateBridge __Hotfix_set_GuildInfo;
        private static DelegateBridge __Hotfix_get_SelfLostBindMoney;
        private static DelegateBridge __Hotfix_set_SelfLostBindMoney;
        private static DelegateBridge __Hotfix_get_KillingLostBindMoney;
        private static DelegateBridge __Hotfix_set_KillingLostBindMoney;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Group", DataFormat=DataFormat.TwosComplement)]
        public int Group
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="GuildInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimplestInfo GuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SelfLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double SelfLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="KillingLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double KillingLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

