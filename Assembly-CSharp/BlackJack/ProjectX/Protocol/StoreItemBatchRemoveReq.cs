﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StoreItemBatchRemoveReq")]
    public class StoreItemBatchRemoveReq : IExtensible
    {
        private readonly List<int> _StoreItemIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StoreItemIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="StoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public List<int> StoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

