﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarShipAddModuleEquip2SlotReq")]
    public class GuildFlagShipHangarShipAddModuleEquip2SlotReq : IExtensible
    {
        private ulong _InstanceId;
        private int _HangarShipIndex;
        private int _HangarShipItemIndex;
        private ProItemInfo _HangarShipItemInfo;
        private int _EquipSlotIndex;
        private int _SrcEquipItemIndex;
        private ProItemInfo _SrcEquipItemInfo;
        private int _CurrEquipItemIndex;
        private ProItemInfo _CurrEquipItemInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_HangarShipItemIndex;
        private static DelegateBridge __Hotfix_set_HangarShipItemIndex;
        private static DelegateBridge __Hotfix_get_HangarShipItemInfo;
        private static DelegateBridge __Hotfix_set_HangarShipItemInfo;
        private static DelegateBridge __Hotfix_get_EquipSlotIndex;
        private static DelegateBridge __Hotfix_set_EquipSlotIndex;
        private static DelegateBridge __Hotfix_get_SrcEquipItemIndex;
        private static DelegateBridge __Hotfix_set_SrcEquipItemIndex;
        private static DelegateBridge __Hotfix_get_SrcEquipItemInfo;
        private static DelegateBridge __Hotfix_set_SrcEquipItemInfo;
        private static DelegateBridge __Hotfix_get_CurrEquipItemIndex;
        private static DelegateBridge __Hotfix_set_CurrEquipItemIndex;
        private static DelegateBridge __Hotfix_get_CurrEquipItemInfo;
        private static DelegateBridge __Hotfix_set_CurrEquipItemInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="HangarShipItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="HangarShipItemInfo", DataFormat=DataFormat.Default)]
        public ProItemInfo HangarShipItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="EquipSlotIndex", DataFormat=DataFormat.TwosComplement)]
        public int EquipSlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SrcEquipItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int SrcEquipItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="SrcEquipItemInfo", DataFormat=DataFormat.Default)]
        public ProItemInfo SrcEquipItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="CurrEquipItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrEquipItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="CurrEquipItemInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProItemInfo CurrEquipItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

