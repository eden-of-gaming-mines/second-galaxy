﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildOccupiedSolarSystemInfoAck")]
    public class GuildOccupiedSolarSystemInfoAck : IExtensible
    {
        private int _Result;
        private readonly List<GuildSolarSystemBasicInfoAck> _BasicInfoAckList;
        private readonly List<GuildSolarSystemBuildingInfoAck> _BuildingInfoAckList;
        private readonly List<GuildSolarSystemBattleInfoAck> _BattleInfoAckList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_BasicInfoAckList;
        private static DelegateBridge __Hotfix_get_BuildingInfoAckList;
        private static DelegateBridge __Hotfix_get_BattleInfoAckList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="BasicInfoAckList", DataFormat=DataFormat.Default)]
        public List<GuildSolarSystemBasicInfoAck> BasicInfoAckList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="BuildingInfoAckList", DataFormat=DataFormat.Default)]
        public List<GuildSolarSystemBuildingInfoAck> BuildingInfoAckList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="BattleInfoAckList", DataFormat=DataFormat.Default)]
        public List<GuildSolarSystemBattleInfoAck> BattleInfoAckList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

