﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessSuperDroneLaunch")]
    public class ProLBSpaceProcessSuperDroneLaunch : IExtensible
    {
        private readonly List<ProLBSpaceProcessDroneLaunchInfo> _DroneFighterLaunchList;
        private readonly List<uint> _TargetObjectIdList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DroneFighterLaunchList;
        private static DelegateBridge __Hotfix_get_TargetObjectIdList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="DroneFighterLaunchList", DataFormat=DataFormat.Default)]
        public List<ProLBSpaceProcessDroneLaunchInfo> DroneFighterLaunchList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="TargetObjectIdList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> TargetObjectIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

