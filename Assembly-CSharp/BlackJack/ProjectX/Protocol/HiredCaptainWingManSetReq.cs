﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptainWingManSetReq")]
    public class HiredCaptainWingManSetReq : IExtensible
    {
        private int _SrcIndex;
        private int _DestIndex;
        private ulong _CaptainInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SrcIndex;
        private static DelegateBridge __Hotfix_set_SrcIndex;
        private static DelegateBridge __Hotfix_get_DestIndex;
        private static DelegateBridge __Hotfix_set_DestIndex;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SrcIndex", DataFormat=DataFormat.TwosComplement)]
        public int SrcIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DestIndex", DataFormat=DataFormat.TwosComplement)]
        public int DestIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

