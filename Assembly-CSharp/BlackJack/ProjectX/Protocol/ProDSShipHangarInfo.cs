﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDSShipHangarInfo")]
    public class ProDSShipHangarInfo : IExtensible
    {
        private int _Index;
        private uint _Version;
        private bool _deleted;
        private int _StoreIndex;
        private ProShipDetailInfo _ShipInfo;
        private long _UnpackTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_Deleted;
        private static DelegateBridge __Hotfix_set_Deleted;
        private static DelegateBridge __Hotfix_get_StoreIndex;
        private static DelegateBridge __Hotfix_set_StoreIndex;
        private static DelegateBridge __Hotfix_get_ShipInfo;
        private static DelegateBridge __Hotfix_set_ShipInfo;
        private static DelegateBridge __Hotfix_get_UnpackTime;
        private static DelegateBridge __Hotfix_set_UnpackTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Index", DataFormat=DataFormat.TwosComplement)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="deleted", DataFormat=DataFormat.Default)]
        public bool Deleted
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="StoreIndex", DataFormat=DataFormat.TwosComplement)]
        public int StoreIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="ShipInfo", DataFormat=DataFormat.Default)]
        public ProShipDetailInfo ShipInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="UnpackTime", DataFormat=DataFormat.TwosComplement)]
        public long UnpackTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

