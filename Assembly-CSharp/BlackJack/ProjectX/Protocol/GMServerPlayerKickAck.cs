﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMServerPlayerKickAck")]
    public class GMServerPlayerKickAck : IExtensible
    {
        private readonly List<int> _Result;
        private readonly List<string> _PlayerNameList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_get_PlayerNameList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public List<int> Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="PlayerNameList", DataFormat=DataFormat.Default)]
        public List<string> PlayerNameList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

