﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarPackShipAck")]
    public class GuildFlagShipHangarPackShipAck : IExtensible
    {
        private int _Result;
        private ulong _InstanceId;
        private int _HangarShipIndex;
        private int _ShipItemIndex;
        private ulong _ShipItemInsId;
        private readonly List<ProStoreItemTransformInfo> _ShipEquips;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_ShipItemIndex;
        private static DelegateBridge __Hotfix_set_ShipItemIndex;
        private static DelegateBridge __Hotfix_get_ShipItemInsId;
        private static DelegateBridge __Hotfix_set_ShipItemInsId;
        private static DelegateBridge __Hotfix_get_ShipEquips;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="ShipItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ShipItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(5, IsRequired=false, Name="ShipItemInsId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="ShipEquips", DataFormat=DataFormat.Default)]
        public List<ProStoreItemTransformInfo> ShipEquips
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

