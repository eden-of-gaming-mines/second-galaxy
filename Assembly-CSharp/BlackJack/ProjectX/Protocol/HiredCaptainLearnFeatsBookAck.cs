﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptainLearnFeatsBookAck")]
    public class HiredCaptainLearnFeatsBookAck : IExtensible
    {
        private int _Result;
        private ulong _CaptainInstanceId;
        private int _FeatsBookItemIndex;
        private readonly List<ProIdLevelInfo> _AdditionFeats;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_FeatsBookItemIndex;
        private static DelegateBridge __Hotfix_set_FeatsBookItemIndex;
        private static DelegateBridge __Hotfix_get_AdditionFeats;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="FeatsBookItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int FeatsBookItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="AdditionFeats", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> AdditionFeats
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

