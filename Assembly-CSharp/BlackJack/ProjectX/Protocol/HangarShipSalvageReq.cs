﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipSalvageReq")]
    public class HangarShipSalvageReq : IExtensible
    {
        private int _HangarIndex;
        private bool _IsSalvageShip;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_HangarIndex;
        private static DelegateBridge __Hotfix_set_HangarIndex;
        private static DelegateBridge __Hotfix_get_IsSalvageShip;
        private static DelegateBridge __Hotfix_set_IsSalvageShip;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="HangarIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="IsSalvageShip", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsSalvageShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

