﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ScenePVPEnableNtf")]
    public class ScenePVPEnableNtf : IExtensible
    {
        private uint _SceneInstanceId;
        private bool _PVPEnable;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SceneInstanceId;
        private static DelegateBridge __Hotfix_set_SceneInstanceId;
        private static DelegateBridge __Hotfix_get_PVPEnable;
        private static DelegateBridge __Hotfix_set_PVPEnable;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((long) 0L), ProtoMember(1, IsRequired=false, Name="SceneInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint SceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="PVPEnable", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool PVPEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

