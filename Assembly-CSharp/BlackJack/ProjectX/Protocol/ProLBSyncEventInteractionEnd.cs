﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventInteractionEnd")]
    public class ProLBSyncEventInteractionEnd : IExtensible
    {
        private int _StopReason;
        private float _CurrShield;
        private float _CurrArmor;
        private float _CurrEnergy;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StopReason;
        private static DelegateBridge __Hotfix_set_StopReason;
        private static DelegateBridge __Hotfix_get_CurrShield;
        private static DelegateBridge __Hotfix_set_CurrShield;
        private static DelegateBridge __Hotfix_get_CurrArmor;
        private static DelegateBridge __Hotfix_set_CurrArmor;
        private static DelegateBridge __Hotfix_get_CurrEnergy;
        private static DelegateBridge __Hotfix_set_CurrEnergy;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="StopReason", DataFormat=DataFormat.TwosComplement)]
        public int StopReason
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="CurrShield", DataFormat=DataFormat.FixedSize)]
        public float CurrShield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(3, IsRequired=false, Name="CurrArmor", DataFormat=DataFormat.FixedSize)]
        public float CurrArmor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(4, IsRequired=false, Name="CurrEnergy", DataFormat=DataFormat.FixedSize)]
        public float CurrEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

