﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptinAddReq")]
    public class HiredCaptinAddReq : IExtensible
    {
        private int _PredefineNpcCaptainId;
        private bool _RandomAdd;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PredefineNpcCaptainId;
        private static DelegateBridge __Hotfix_set_PredefineNpcCaptainId;
        private static DelegateBridge __Hotfix_get_RandomAdd;
        private static DelegateBridge __Hotfix_set_RandomAdd;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="PredefineNpcCaptainId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int PredefineNpcCaptainId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(2, IsRequired=false, Name="RandomAdd", DataFormat=DataFormat.Default)]
        public bool RandomAdd
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

