﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipTransformSingleItemToMSStoreAck")]
    public class HangarShipTransformSingleItemToMSStoreAck : IExtensible
    {
        private int _Result;
        private int _HangarShipIndex;
        private int _StoreItemIndex;
        private long _StoreItemCount;
        private int _DestStoreItemIndex;
        private ulong _DestStoreItemInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_StoreItemIndex;
        private static DelegateBridge __Hotfix_set_StoreItemIndex;
        private static DelegateBridge __Hotfix_get_StoreItemCount;
        private static DelegateBridge __Hotfix_set_StoreItemCount;
        private static DelegateBridge __Hotfix_get_DestStoreItemIndex;
        private static DelegateBridge __Hotfix_set_DestStoreItemIndex;
        private static DelegateBridge __Hotfix_get_DestStoreItemInstanceId;
        private static DelegateBridge __Hotfix_set_DestStoreItemInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="StoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int StoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="StoreItemCount", DataFormat=DataFormat.TwosComplement)]
        public long StoreItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="DestStoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int DestStoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(6, IsRequired=false, Name="DestStoreItemInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong DestStoreItemInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

