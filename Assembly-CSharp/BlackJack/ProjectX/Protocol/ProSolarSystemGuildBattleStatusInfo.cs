﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSolarSystemGuildBattleStatusInfo")]
    public class ProSolarSystemGuildBattleStatusInfo : IExtensible
    {
        private int _SolarSystemId;
        private bool _InBattleStatus;
        private long _NextSovereignBattleStartMinTime;
        private bool _EnablePlayerSovereignty;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_InBattleStatus;
        private static DelegateBridge __Hotfix_set_InBattleStatus;
        private static DelegateBridge __Hotfix_get_NextSovereignBattleStartMinTime;
        private static DelegateBridge __Hotfix_set_NextSovereignBattleStartMinTime;
        private static DelegateBridge __Hotfix_get_EnablePlayerSovereignty;
        private static DelegateBridge __Hotfix_set_EnablePlayerSovereignty;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="InBattleStatus", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool InBattleStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="NextSovereignBattleStartMinTime", DataFormat=DataFormat.TwosComplement)]
        public long NextSovereignBattleStartMinTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(4, IsRequired=false, Name="EnablePlayerSovereignty", DataFormat=DataFormat.Default)]
        public bool EnablePlayerSovereignty
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

