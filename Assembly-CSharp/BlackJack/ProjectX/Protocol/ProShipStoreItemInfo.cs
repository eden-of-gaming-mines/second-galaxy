﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipStoreItemInfo")]
    public class ProShipStoreItemInfo : IExtensible
    {
        private ProItemInfo _ItemInfo;
        private int _StoreIndex;
        private bool _IsFreezing;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ItemInfo;
        private static DelegateBridge __Hotfix_set_ItemInfo;
        private static DelegateBridge __Hotfix_get_StoreIndex;
        private static DelegateBridge __Hotfix_set_StoreIndex;
        private static DelegateBridge __Hotfix_get_IsFreezing;
        private static DelegateBridge __Hotfix_set_IsFreezing;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="ItemInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProItemInfo ItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="StoreIndex", DataFormat=DataFormat.TwosComplement)]
        public int StoreIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="IsFreezing", DataFormat=DataFormat.Default)]
        public bool IsFreezing
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

