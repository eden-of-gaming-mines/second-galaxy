﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SceneFullScreenEffectNtf")]
    public class SceneFullScreenEffectNtf : IExtensible
    {
        private int _EffectType;
        private bool _IsCloseEffect;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EffectType;
        private static DelegateBridge __Hotfix_set_EffectType;
        private static DelegateBridge __Hotfix_get_IsCloseEffect;
        private static DelegateBridge __Hotfix_set_IsCloseEffect;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="EffectType", DataFormat=DataFormat.TwosComplement)]
        public int EffectType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(2, IsRequired=false, Name="IsCloseEffect", DataFormat=DataFormat.Default)]
        public bool IsCloseEffect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

