﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBattleReportInfo")]
    public class ProGuildBattleReportInfo : IExtensible
    {
        private uint _Version;
        private ulong _GuildBattleInstanceId;
        private int _SolarSystemId;
        private int _Type;
        private ProGuildSimplestInfo _AttackerGuildInfo;
        private ProGuildSimplestInfo _DefenderGuildInfo;
        private int _NpcGuildConfigId;
        private long _StartTime;
        private long _EndTime;
        private bool _IsAttackerWin;
        private int _Status;
        private long _CurrStatusStartTime;
        private long _CurrStatusEndTime;
        private double _TotalKillingLostBindMoney;
        private int _TotalSelfLostShipCount;
        private double _DefenderKillingLostBindMoney;
        private int _DefenderSelfLostShipCount;
        private double _AttackerKillingLostBindMoney;
        private int _AttackerSelfLostShipCount;
        private readonly List<ProGuildBattlePlayerKillLostStatInfo> _PlayerKillLostStatInfoList;
        private readonly List<ProGuildBattleGuildKillLostStatInfo> _GuildKillLostStatInfoList;
        private readonly List<ProGuildBattleShipLostStatInfo> _ShipLostStatInfoList;
        private readonly List<ProGuildBattleGuildBuildingSimpleInfo> _GuildBuildingSimpleInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_GuildBattleInstanceId;
        private static DelegateBridge __Hotfix_set_GuildBattleInstanceId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_AttackerGuildInfo;
        private static DelegateBridge __Hotfix_set_AttackerGuildInfo;
        private static DelegateBridge __Hotfix_get_DefenderGuildInfo;
        private static DelegateBridge __Hotfix_set_DefenderGuildInfo;
        private static DelegateBridge __Hotfix_get_NpcGuildConfigId;
        private static DelegateBridge __Hotfix_set_NpcGuildConfigId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_EndTime;
        private static DelegateBridge __Hotfix_set_EndTime;
        private static DelegateBridge __Hotfix_get_IsAttackerWin;
        private static DelegateBridge __Hotfix_set_IsAttackerWin;
        private static DelegateBridge __Hotfix_get_Status;
        private static DelegateBridge __Hotfix_set_Status;
        private static DelegateBridge __Hotfix_get_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_set_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_get_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_set_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_get_TotalKillingLostBindMoney;
        private static DelegateBridge __Hotfix_set_TotalKillingLostBindMoney;
        private static DelegateBridge __Hotfix_get_TotalSelfLostShipCount;
        private static DelegateBridge __Hotfix_set_TotalSelfLostShipCount;
        private static DelegateBridge __Hotfix_get_DefenderKillingLostBindMoney;
        private static DelegateBridge __Hotfix_set_DefenderKillingLostBindMoney;
        private static DelegateBridge __Hotfix_get_DefenderSelfLostShipCount;
        private static DelegateBridge __Hotfix_set_DefenderSelfLostShipCount;
        private static DelegateBridge __Hotfix_get_AttackerKillingLostBindMoney;
        private static DelegateBridge __Hotfix_set_AttackerKillingLostBindMoney;
        private static DelegateBridge __Hotfix_get_AttackerSelfLostShipCount;
        private static DelegateBridge __Hotfix_set_AttackerSelfLostShipCount;
        private static DelegateBridge __Hotfix_get_PlayerKillLostStatInfoList;
        private static DelegateBridge __Hotfix_get_GuildKillLostStatInfoList;
        private static DelegateBridge __Hotfix_get_ShipLostStatInfoList;
        private static DelegateBridge __Hotfix_get_GuildBuildingSimpleInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="GuildBattleInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong GuildBattleInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="AttackerGuildInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimplestInfo AttackerGuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="DefenderGuildInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimplestInfo DefenderGuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="NpcGuildConfigId", DataFormat=DataFormat.TwosComplement)]
        public int NpcGuildConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="EndTime", DataFormat=DataFormat.TwosComplement)]
        public long EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="IsAttackerWin", DataFormat=DataFormat.Default)]
        public bool IsAttackerWin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="Status", DataFormat=DataFormat.TwosComplement)]
        public int Status
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="CurrStatusStartTime", DataFormat=DataFormat.TwosComplement)]
        public long CurrStatusStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="CurrStatusEndTime", DataFormat=DataFormat.TwosComplement)]
        public long CurrStatusEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="TotalKillingLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double TotalKillingLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="TotalSelfLostShipCount", DataFormat=DataFormat.TwosComplement)]
        public int TotalSelfLostShipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="DefenderKillingLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double DefenderKillingLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=true, Name="DefenderSelfLostShipCount", DataFormat=DataFormat.TwosComplement)]
        public int DefenderSelfLostShipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=true, Name="AttackerKillingLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double AttackerKillingLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=true, Name="AttackerSelfLostShipCount", DataFormat=DataFormat.TwosComplement)]
        public int AttackerSelfLostShipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, Name="PlayerKillLostStatInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildBattlePlayerKillLostStatInfo> PlayerKillLostStatInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x15, Name="GuildKillLostStatInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildBattleGuildKillLostStatInfo> GuildKillLostStatInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x16, Name="ShipLostStatInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildBattleShipLostStatInfo> ShipLostStatInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x17, Name="GuildBuildingSimpleInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildBattleGuildBuildingSimpleInfo> GuildBuildingSimpleInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

