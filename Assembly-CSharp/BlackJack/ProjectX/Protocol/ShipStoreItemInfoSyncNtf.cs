﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipStoreItemInfoSyncNtf")]
    public class ShipStoreItemInfoSyncNtf : IExtensible
    {
        private readonly List<ProShipStoreItemUpdateInfo> _ShipItemUpdateList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipItemUpdateList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="ShipItemUpdateList", DataFormat=DataFormat.Default)]
        public List<ProShipStoreItemUpdateInfo> ShipItemUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

