﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSolarSystemSpaceSignalInfo")]
    public class ProSolarSystemSpaceSignalInfo : IExtensible
    {
        private int _SolarSystemId;
        private readonly List<ProSpaceSignalResultInfo> _SpaceSignalResultInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_SpaceSignalResultInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="SpaceSignalResultInfoList", DataFormat=DataFormat.Default)]
        public List<ProSpaceSignalResultInfo> SpaceSignalResultInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

