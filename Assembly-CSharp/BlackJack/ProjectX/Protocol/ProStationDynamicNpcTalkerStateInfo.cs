﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProStationDynamicNpcTalkerStateInfo")]
    public class ProStationDynamicNpcTalkerStateInfo : IExtensible
    {
        private int _NPCId;
        private bool _IsVisiable;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NPCId;
        private static DelegateBridge __Hotfix_set_NPCId;
        private static DelegateBridge __Hotfix_get_IsVisiable;
        private static DelegateBridge __Hotfix_set_IsVisiable;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="NPCId", DataFormat=DataFormat.TwosComplement)]
        public int NPCId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="IsVisiable", DataFormat=DataFormat.Default)]
        public bool IsVisiable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

