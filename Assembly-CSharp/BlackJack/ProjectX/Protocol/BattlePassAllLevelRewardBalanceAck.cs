﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BattlePassAllLevelRewardBalanceAck")]
    public class BattlePassAllLevelRewardBalanceAck : IExtensible
    {
        private int _Result;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdate;
        private readonly List<ProStoreItemUpdateInfo> _MStoreItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_CurrencyUpdate;
        private static DelegateBridge __Hotfix_get_MStoreItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="CurrencyUpdate", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="MStoreItemList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> MStoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

