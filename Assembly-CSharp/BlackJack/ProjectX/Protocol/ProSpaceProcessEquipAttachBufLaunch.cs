﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceProcessEquipAttachBufLaunch")]
    public class ProSpaceProcessEquipAttachBufLaunch : IExtensible
    {
        private uint _InstanceId;
        private uint _StartTime;
        private uint _CostEnergy;
        private uint _ChargeTime;
        private readonly List<int> _BufList;
        private readonly List<uint> _AttachBufTargetObjIdList;
        private uint _CostFuel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_CostEnergy;
        private static DelegateBridge __Hotfix_set_CostEnergy;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_BufList;
        private static DelegateBridge __Hotfix_get_AttachBufTargetObjIdList;
        private static DelegateBridge __Hotfix_get_CostFuel;
        private static DelegateBridge __Hotfix_set_CostFuel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public uint StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CostEnergy", DataFormat=DataFormat.TwosComplement)]
        public uint CostEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="ChargeTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="BufList", DataFormat=DataFormat.TwosComplement)]
        public List<int> BufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="AttachBufTargetObjIdList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> AttachBufTargetObjIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="CostFuel", DataFormat=DataFormat.TwosComplement)]
        public uint CostFuel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

