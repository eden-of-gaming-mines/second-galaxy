﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerInfoInitAck")]
    public class PlayerInfoInitAck : IExtensible
    {
        private int _Result;
        private long _ServerCurrTime;
        private bool _NeedCreateCharactor;
        private ProPlayerBasicStaticInfo _PlayerBasicStaticInfo;
        private DSVersionNtf _DSVersion;
        private bool _OnlyCheckDSVerion;
        private int _ServerRealmId;
        private int _ServerSiteId;
        private bool _IsBlockRechargeUI;
        private int _ShipHangarsDSCount;
        private int _ItemStoreDSCount;
        private int _HiredCaptainDSCount;
        private int _MailDSCount;
        private int _KillRecordDSCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ServerCurrTime;
        private static DelegateBridge __Hotfix_set_ServerCurrTime;
        private static DelegateBridge __Hotfix_get_NeedCreateCharactor;
        private static DelegateBridge __Hotfix_set_NeedCreateCharactor;
        private static DelegateBridge __Hotfix_get_PlayerBasicStaticInfo;
        private static DelegateBridge __Hotfix_set_PlayerBasicStaticInfo;
        private static DelegateBridge __Hotfix_get_DSVersion;
        private static DelegateBridge __Hotfix_set_DSVersion;
        private static DelegateBridge __Hotfix_get_OnlyCheckDSVerion;
        private static DelegateBridge __Hotfix_set_OnlyCheckDSVerion;
        private static DelegateBridge __Hotfix_get_ServerRealmId;
        private static DelegateBridge __Hotfix_set_ServerRealmId;
        private static DelegateBridge __Hotfix_get_ServerSiteId;
        private static DelegateBridge __Hotfix_set_ServerSiteId;
        private static DelegateBridge __Hotfix_get_IsBlockRechargeUI;
        private static DelegateBridge __Hotfix_set_IsBlockRechargeUI;
        private static DelegateBridge __Hotfix_get_ShipHangarsDSCount;
        private static DelegateBridge __Hotfix_set_ShipHangarsDSCount;
        private static DelegateBridge __Hotfix_get_ItemStoreDSCount;
        private static DelegateBridge __Hotfix_set_ItemStoreDSCount;
        private static DelegateBridge __Hotfix_get_HiredCaptainDSCount;
        private static DelegateBridge __Hotfix_set_HiredCaptainDSCount;
        private static DelegateBridge __Hotfix_get_MailDSCount;
        private static DelegateBridge __Hotfix_set_MailDSCount;
        private static DelegateBridge __Hotfix_get_KillRecordDSCount;
        private static DelegateBridge __Hotfix_set_KillRecordDSCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ServerCurrTime", DataFormat=DataFormat.TwosComplement)]
        public long ServerCurrTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="NeedCreateCharactor", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool NeedCreateCharactor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="PlayerBasicStaticInfo", DataFormat=DataFormat.Default)]
        public ProPlayerBasicStaticInfo PlayerBasicStaticInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="DSVersion", DataFormat=DataFormat.Default)]
        public DSVersionNtf DSVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="OnlyCheckDSVerion", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool OnlyCheckDSVerion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="ServerRealmId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ServerRealmId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(8, IsRequired=false, Name="ServerSiteId", DataFormat=DataFormat.TwosComplement)]
        public int ServerSiteId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="IsBlockRechargeUI", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsBlockRechargeUI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x11, IsRequired=false, Name="ShipHangarsDSCount", DataFormat=DataFormat.TwosComplement)]
        public int ShipHangarsDSCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x12, IsRequired=false, Name="ItemStoreDSCount", DataFormat=DataFormat.TwosComplement)]
        public int ItemStoreDSCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x13, IsRequired=false, Name="HiredCaptainDSCount", DataFormat=DataFormat.TwosComplement)]
        public int HiredCaptainDSCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=false, Name="MailDSCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int MailDSCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=false, Name="KillRecordDSCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int KillRecordDSCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

