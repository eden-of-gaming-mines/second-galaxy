﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProHiredCaptainShipInfo")]
    public class ProHiredCaptainShipInfo : IExtensible
    {
        private int _CaptainShipId;
        private bool _NeedRepair;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CaptainShipId;
        private static DelegateBridge __Hotfix_set_CaptainShipId;
        private static DelegateBridge __Hotfix_get_NeedRepair;
        private static DelegateBridge __Hotfix_set_NeedRepair;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CaptainShipId", DataFormat=DataFormat.TwosComplement)]
        public int CaptainShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="NeedRepair", DataFormat=DataFormat.Default)]
        public bool NeedRepair
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

