﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipDetailInfo")]
    public class ProShipDetailInfo : IExtensible
    {
        private int _ShipConfigId;
        private string _ShipName;
        private ulong _InstanceId;
        private int _State;
        private readonly List<int> _HighSlotList;
        private readonly List<ProAmmoStoreItemInfo> _HighSlotAmmoList;
        private readonly List<int> _MiddleSlotList;
        private readonly List<int> _LowSlotList;
        private readonly List<int> _StoreItemList;
        private int _DestroyState;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipConfigId;
        private static DelegateBridge __Hotfix_set_ShipConfigId;
        private static DelegateBridge __Hotfix_get_ShipName;
        private static DelegateBridge __Hotfix_set_ShipName;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;
        private static DelegateBridge __Hotfix_get_HighSlotList;
        private static DelegateBridge __Hotfix_get_HighSlotAmmoList;
        private static DelegateBridge __Hotfix_get_MiddleSlotList;
        private static DelegateBridge __Hotfix_get_LowSlotList;
        private static DelegateBridge __Hotfix_get_StoreItemList;
        private static DelegateBridge __Hotfix_get_DestroyState;
        private static DelegateBridge __Hotfix_set_DestroyState;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ShipConfigId", DataFormat=DataFormat.TwosComplement)]
        public int ShipConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ShipName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string ShipName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="State", DataFormat=DataFormat.TwosComplement)]
        public int State
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="HighSlotList", DataFormat=DataFormat.TwosComplement)]
        public List<int> HighSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="HighSlotAmmoList", DataFormat=DataFormat.Default)]
        public List<ProAmmoStoreItemInfo> HighSlotAmmoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="MiddleSlotList", DataFormat=DataFormat.TwosComplement)]
        public List<int> MiddleSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="LowSlotList", DataFormat=DataFormat.TwosComplement)]
        public List<int> LowSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, Name="StoreItemList", DataFormat=DataFormat.TwosComplement)]
        public List<int> StoreItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="DestroyState", DataFormat=DataFormat.TwosComplement)]
        public int DestroyState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

