﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CrackSpeedUpByRealMoneyReq")]
    public class CrackSpeedUpByRealMoneyReq : IExtensible
    {
        private int _BoxItemIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BoxItemIndex;
        private static DelegateBridge __Hotfix_set_BoxItemIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="BoxItemIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BoxItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

