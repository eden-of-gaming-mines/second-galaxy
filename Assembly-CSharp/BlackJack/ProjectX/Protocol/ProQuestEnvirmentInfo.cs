﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProQuestEnvirmentInfo")]
    public class ProQuestEnvirmentInfo : IExtensible
    {
        private int _EnvInstanceId;
        private int _EnvTypeId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EnvInstanceId;
        private static DelegateBridge __Hotfix_set_EnvInstanceId;
        private static DelegateBridge __Hotfix_get_EnvTypeId;
        private static DelegateBridge __Hotfix_set_EnvTypeId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="EnvInstanceId", DataFormat=DataFormat.TwosComplement)]
        public int EnvInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="EnvTypeId", DataFormat=DataFormat.TwosComplement)]
        public int EnvTypeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

