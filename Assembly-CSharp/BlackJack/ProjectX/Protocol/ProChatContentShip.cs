﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatContentShip")]
    public class ProChatContentShip : IExtensible
    {
        private int _ShipConfId;
        private string _ShipName;
        private readonly List<ProShipSlotGroupInfo> _HighSlotList;
        private readonly List<ProAmmoStoreItemInfo> _AmmoStoreItemInfoList;
        private readonly List<ProShipSlotGroupInfo> _MiddleSlotList;
        private readonly List<ProShipSlotGroupInfo> _LowSlotList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipConfId;
        private static DelegateBridge __Hotfix_set_ShipConfId;
        private static DelegateBridge __Hotfix_get_ShipName;
        private static DelegateBridge __Hotfix_set_ShipName;
        private static DelegateBridge __Hotfix_get_HighSlotList;
        private static DelegateBridge __Hotfix_get_AmmoStoreItemInfoList;
        private static DelegateBridge __Hotfix_get_MiddleSlotList;
        private static DelegateBridge __Hotfix_get_LowSlotList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="ShipConfId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ShipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ShipName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string ShipName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="HighSlotList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotGroupInfo> HighSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="AmmoStoreItemInfoList", DataFormat=DataFormat.Default)]
        public List<ProAmmoStoreItemInfo> AmmoStoreItemInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="MiddleSlotList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotGroupInfo> MiddleSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="LowSlotList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotGroupInfo> LowSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

