﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerCurrencyInfoNtf")]
    public class PlayerCurrencyInfoNtf : IExtensible
    {
        private ProCurrencyUpdateInfo _UpdateInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_UpdateInfo;
        private static DelegateBridge __Hotfix_set_UpdateInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="UpdateInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProCurrencyUpdateInfo UpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

