﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerDiplomacyUpdateAck")]
    public class PlayerDiplomacyUpdateAck : IExtensible
    {
        private int _Result;
        private string _PlayerGameUserId;
        private uint _GuildId;
        private uint _AllienceId;
        private int _OptType;
        private int _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_PlayerGameUserId;
        private static DelegateBridge __Hotfix_set_PlayerGameUserId;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_AllienceId;
        private static DelegateBridge __Hotfix_set_AllienceId;
        private static DelegateBridge __Hotfix_get_OptType;
        private static DelegateBridge __Hotfix_set_OptType;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="PlayerGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="AllienceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint AllienceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="OptType", DataFormat=DataFormat.TwosComplement)]
        public int OptType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public int Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

