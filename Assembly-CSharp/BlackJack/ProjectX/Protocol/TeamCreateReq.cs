﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TeamCreateReq")]
    public class TeamCreateReq : IExtensible
    {
        private bool _NeedNoticeNearBy;
        private bool _NeedNoticeLegion;
        private bool _NeedNoticeStation;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NeedNoticeNearBy;
        private static DelegateBridge __Hotfix_set_NeedNoticeNearBy;
        private static DelegateBridge __Hotfix_get_NeedNoticeLegion;
        private static DelegateBridge __Hotfix_set_NeedNoticeLegion;
        private static DelegateBridge __Hotfix_get_NeedNoticeStation;
        private static DelegateBridge __Hotfix_set_NeedNoticeStation;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(false), ProtoMember(1, IsRequired=false, Name="NeedNoticeNearBy", DataFormat=DataFormat.Default)]
        public bool NeedNoticeNearBy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="NeedNoticeLegion", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool NeedNoticeLegion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="NeedNoticeStation", DataFormat=DataFormat.Default)]
        public bool NeedNoticeStation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

