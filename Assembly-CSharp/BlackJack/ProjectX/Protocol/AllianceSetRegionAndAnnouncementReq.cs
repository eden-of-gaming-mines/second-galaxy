﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AllianceSetRegionAndAnnouncementReq")]
    public class AllianceSetRegionAndAnnouncementReq : IExtensible
    {
        private int _Region;
        private string _Announcement;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Region;
        private static DelegateBridge __Hotfix_set_Region;
        private static DelegateBridge __Hotfix_get_Announcement;
        private static DelegateBridge __Hotfix_set_Announcement;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="Region", DataFormat=DataFormat.TwosComplement)]
        public int Region
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="Announcement", DataFormat=DataFormat.Default)]
        public string Announcement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

