﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TechUpgradeSpeedUpReq")]
    public class TechUpgradeSpeedUpReq : IExtensible
    {
        private int _TechId;
        private int _UseItemId;
        private int _UseItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TechId;
        private static DelegateBridge __Hotfix_set_TechId;
        private static DelegateBridge __Hotfix_get_UseItemId;
        private static DelegateBridge __Hotfix_set_UseItemId;
        private static DelegateBridge __Hotfix_get_UseItemCount;
        private static DelegateBridge __Hotfix_set_UseItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TechId", DataFormat=DataFormat.TwosComplement)]
        public int TechId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="UseItemId", DataFormat=DataFormat.TwosComplement)]
        public int UseItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="UseItemCount", DataFormat=DataFormat.TwosComplement)]
        public int UseItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

