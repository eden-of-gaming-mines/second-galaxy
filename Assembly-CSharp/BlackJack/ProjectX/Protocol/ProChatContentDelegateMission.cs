﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatContentDelegateMission")]
    public class ProChatContentDelegateMission : IExtensible
    {
        private ProDelegateMissionInfo _DelegateMissionInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DelegateMissionInfo;
        private static DelegateBridge __Hotfix_set_DelegateMissionInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="DelegateMissionInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProDelegateMissionInfo DelegateMissionInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

