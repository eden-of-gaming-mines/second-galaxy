﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProAuctionItemDetailInfo")]
    public class ProAuctionItemDetailInfo : IExtensible
    {
        private int _AuctionItemId;
        private ulong _AuctionItemInsId;
        private int _AuctionItemState;
        private int _AuctionItemCurrCount;
        private int _AuctionItemOrgCount;
        private long _Price;
        private long _ExpireTime;
        private float _TradeTax;
        private long _AuctionFee;
        private long _MarketPrice;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AuctionItemId;
        private static DelegateBridge __Hotfix_set_AuctionItemId;
        private static DelegateBridge __Hotfix_get_AuctionItemInsId;
        private static DelegateBridge __Hotfix_set_AuctionItemInsId;
        private static DelegateBridge __Hotfix_get_AuctionItemState;
        private static DelegateBridge __Hotfix_set_AuctionItemState;
        private static DelegateBridge __Hotfix_get_AuctionItemCurrCount;
        private static DelegateBridge __Hotfix_set_AuctionItemCurrCount;
        private static DelegateBridge __Hotfix_get_AuctionItemOrgCount;
        private static DelegateBridge __Hotfix_set_AuctionItemOrgCount;
        private static DelegateBridge __Hotfix_get_Price;
        private static DelegateBridge __Hotfix_set_Price;
        private static DelegateBridge __Hotfix_get_ExpireTime;
        private static DelegateBridge __Hotfix_set_ExpireTime;
        private static DelegateBridge __Hotfix_get_TradeTax;
        private static DelegateBridge __Hotfix_set_TradeTax;
        private static DelegateBridge __Hotfix_get_AuctionFee;
        private static DelegateBridge __Hotfix_set_AuctionFee;
        private static DelegateBridge __Hotfix_get_MarketPrice;
        private static DelegateBridge __Hotfix_set_MarketPrice;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="AuctionItemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AuctionItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="AuctionItemInsId", DataFormat=DataFormat.TwosComplement)]
        public ulong AuctionItemInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="AuctionItemState", DataFormat=DataFormat.TwosComplement)]
        public int AuctionItemState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="AuctionItemCurrCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AuctionItemCurrCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="AuctionItemOrgCount", DataFormat=DataFormat.TwosComplement)]
        public int AuctionItemOrgCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="Price", DataFormat=DataFormat.TwosComplement)]
        public long Price
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="ExpireTime", DataFormat=DataFormat.TwosComplement)]
        public long ExpireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(8, IsRequired=false, Name="TradeTax", DataFormat=DataFormat.FixedSize)]
        public float TradeTax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(9, IsRequired=false, Name="AuctionFee", DataFormat=DataFormat.TwosComplement)]
        public long AuctionFee
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="MarketPrice", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long MarketPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

