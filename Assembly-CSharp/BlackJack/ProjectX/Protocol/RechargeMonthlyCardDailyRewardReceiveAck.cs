﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="RechargeMonthlyCardDailyRewardReceiveAck")]
    public class RechargeMonthlyCardDailyRewardReceiveAck : IExtensible
    {
        private int _Result;
        private int _MonthlyCardId;
        private readonly List<ProStoreItemUpdateInfo> _ItemUpdateList;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdateList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_MonthlyCardId;
        private static DelegateBridge __Hotfix_set_MonthlyCardId;
        private static DelegateBridge __Hotfix_get_ItemUpdateList;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="MonthlyCardId", DataFormat=DataFormat.TwosComplement)]
        public int MonthlyCardId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ItemUpdateList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> ItemUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="CurrencyUpdateList", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

