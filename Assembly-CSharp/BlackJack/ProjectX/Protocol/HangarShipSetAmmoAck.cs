﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipSetAmmoAck")]
    public class HangarShipSetAmmoAck : IExtensible
    {
        private int _Result;
        private int _HangarShipIndex;
        private readonly List<ShipSetAmmoInfo> _SetAmmoReqInfoList;
        private readonly List<ProAmmoStoreItemInfo> _RemoveAmmoInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_SetAmmoReqInfoList;
        private static DelegateBridge __Hotfix_get_RemoveAmmoInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="SetAmmoReqInfoList", DataFormat=DataFormat.Default)]
        public List<ShipSetAmmoInfo> SetAmmoReqInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="RemoveAmmoInfoList", DataFormat=DataFormat.Default)]
        public List<ProAmmoStoreItemInfo> RemoveAmmoInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

