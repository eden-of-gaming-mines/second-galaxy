﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarSimpleInfoAck")]
    public class GuildFlagShipHangarSimpleInfoAck : IExtensible
    {
        private int _FlagShipCount;
        private int _HangarSlotCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FlagShipCount;
        private static DelegateBridge __Hotfix_set_FlagShipCount;
        private static DelegateBridge __Hotfix_get_HangarSlotCount;
        private static DelegateBridge __Hotfix_set_HangarSlotCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FlagShipCount", DataFormat=DataFormat.TwosComplement)]
        public int FlagShipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="HangarSlotCount", DataFormat=DataFormat.TwosComplement)]
        public int HangarSlotCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

