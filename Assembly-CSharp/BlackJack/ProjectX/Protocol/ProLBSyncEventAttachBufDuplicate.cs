﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventAttachBufDuplicate")]
    public class ProLBSyncEventAttachBufDuplicate : IExtensible
    {
        private uint _InstanceId;
        private int _AttachCount;
        private float _BufInstanceParam;
        private readonly List<uint> _SrcTargetIdList;
        private int _BufConfigId;
        private uint _LifeEndTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_AttachCount;
        private static DelegateBridge __Hotfix_set_AttachCount;
        private static DelegateBridge __Hotfix_get_BufInstanceParam;
        private static DelegateBridge __Hotfix_set_BufInstanceParam;
        private static DelegateBridge __Hotfix_get_SrcTargetIdList;
        private static DelegateBridge __Hotfix_get_BufConfigId;
        private static DelegateBridge __Hotfix_set_BufConfigId;
        private static DelegateBridge __Hotfix_get_LifeEndTime;
        private static DelegateBridge __Hotfix_set_LifeEndTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="AttachCount", DataFormat=DataFormat.TwosComplement)]
        public int AttachCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="BufInstanceParam", DataFormat=DataFormat.FixedSize)]
        public float BufInstanceParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="SrcTargetIdList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> SrcTargetIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="BufConfigId", DataFormat=DataFormat.TwosComplement)]
        public int BufConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="LifeEndTime", DataFormat=DataFormat.TwosComplement)]
        public uint LifeEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

