﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBenefitsCommon")]
    public class ProGuildBenefitsCommon : IExtensible
    {
        private bool _IsReceived;
        private ulong _InstanceId;
        private uint _GuildId;
        private int _SubType;
        private long _CreateTime;
        private long _ExpireTime;
        private readonly List<ProSimpleItemInfoWithCount> _ItemList;
        private int _ContentTemplateId;
        private readonly List<ProFormatStringParamInfo> _ContentFormatStrParamList;
        private int _ConfigId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsReceived;
        private static DelegateBridge __Hotfix_set_IsReceived;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_SubType;
        private static DelegateBridge __Hotfix_set_SubType;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_get_ExpireTime;
        private static DelegateBridge __Hotfix_set_ExpireTime;
        private static DelegateBridge __Hotfix_get_ItemList;
        private static DelegateBridge __Hotfix_get_ContentTemplateId;
        private static DelegateBridge __Hotfix_set_ContentTemplateId;
        private static DelegateBridge __Hotfix_get_ContentFormatStrParamList;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsReceived", DataFormat=DataFormat.Default)]
        public bool IsReceived
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SubType", DataFormat=DataFormat.TwosComplement)]
        public int SubType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="CreateTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="ExpireTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long ExpireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="ItemList", DataFormat=DataFormat.Default)]
        public List<ProSimpleItemInfoWithCount> ItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="ContentTemplateId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ContentTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="ContentFormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> ContentFormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(0), ProtoMember(10, IsRequired=false, Name="ConfigId", DataFormat=DataFormat.TwosComplement)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

