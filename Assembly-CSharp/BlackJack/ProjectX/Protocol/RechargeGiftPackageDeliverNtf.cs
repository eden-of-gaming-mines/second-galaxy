﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="RechargeGiftPackageDeliverNtf")]
    public class RechargeGiftPackageDeliverNtf : IExtensible
    {
        private ProRechargeOrder _Order;
        private int _giftPackageId;
        private readonly List<ProStoreItemUpdateInfo> _ItemUpdateList;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdateList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Order;
        private static DelegateBridge __Hotfix_set_Order;
        private static DelegateBridge __Hotfix_get_GiftPackageId;
        private static DelegateBridge __Hotfix_set_GiftPackageId;
        private static DelegateBridge __Hotfix_get_ItemUpdateList;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Order", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProRechargeOrder Order
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="giftPackageId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GiftPackageId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ItemUpdateList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> ItemUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="CurrencyUpdateList", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

