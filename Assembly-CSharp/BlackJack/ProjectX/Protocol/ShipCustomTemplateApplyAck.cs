﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipCustomTemplateApplyAck")]
    public class ShipCustomTemplateApplyAck : IExtensible
    {
        private int _Result;
        private bool _IsPredefineTemplate;
        private int _TemplateIndex;
        private ulong _ShipInstanceId;
        private readonly List<ProStoreItemTransformInfo> _RemovedShipEquips;
        private readonly List<ProAmmoStoreItemInfo> _RemovedAmmoInfoList;
        private readonly List<ProStoreItemInfo> _StoreItemAddList;
        private ProCurrencyUpdateInfo _CurrencyUpdateInfo;
        private readonly List<ProStoreItemTransformInfo> _Item2SlotGroupList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_IsPredefineTemplate;
        private static DelegateBridge __Hotfix_set_IsPredefineTemplate;
        private static DelegateBridge __Hotfix_get_TemplateIndex;
        private static DelegateBridge __Hotfix_set_TemplateIndex;
        private static DelegateBridge __Hotfix_get_ShipInstanceId;
        private static DelegateBridge __Hotfix_set_ShipInstanceId;
        private static DelegateBridge __Hotfix_get_RemovedShipEquips;
        private static DelegateBridge __Hotfix_get_RemovedAmmoInfoList;
        private static DelegateBridge __Hotfix_get_StoreItemAddList;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_set_CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_get_Item2SlotGroupList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="IsPredefineTemplate", DataFormat=DataFormat.Default)]
        public bool IsPredefineTemplate
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="TemplateIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int TemplateIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="ShipInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong ShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="RemovedShipEquips", DataFormat=DataFormat.Default)]
        public List<ProStoreItemTransformInfo> RemovedShipEquips
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="RemovedAmmoInfoList", DataFormat=DataFormat.Default)]
        public List<ProAmmoStoreItemInfo> RemovedAmmoInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="StoreItemAddList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemInfo> StoreItemAddList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(8, IsRequired=false, Name="CurrencyUpdateInfo", DataFormat=DataFormat.Default)]
        public ProCurrencyUpdateInfo CurrencyUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="Item2SlotGroupList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemTransformInfo> Item2SlotGroupList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

