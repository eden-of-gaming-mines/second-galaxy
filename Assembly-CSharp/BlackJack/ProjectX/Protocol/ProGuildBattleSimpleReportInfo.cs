﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBattleSimpleReportInfo")]
    public class ProGuildBattleSimpleReportInfo : IExtensible
    {
        private ulong _GuildBattleInstanceId;
        private int _SolarSystemId;
        private int _Type;
        private int _BuildingType;
        private int _GroupType;
        private ProGuildSimplestInfo _SelfGuildInfo;
        private ProGuildSimplestInfo _EnemyGuildInfo;
        private int _NpcGuildConfigId;
        private int _Status;
        private long _CurrStatusStartTime;
        private long _CurrStatusEndTime;
        private bool _IsWin;
        private double _KillingLostBindMoney;
        private double _SelfLostBindMoney;
        private long _StartTime;
        private long _EndTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GuildBattleInstanceId;
        private static DelegateBridge __Hotfix_set_GuildBattleInstanceId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_BuildingType;
        private static DelegateBridge __Hotfix_set_BuildingType;
        private static DelegateBridge __Hotfix_get_GroupType;
        private static DelegateBridge __Hotfix_set_GroupType;
        private static DelegateBridge __Hotfix_get_SelfGuildInfo;
        private static DelegateBridge __Hotfix_set_SelfGuildInfo;
        private static DelegateBridge __Hotfix_get_EnemyGuildInfo;
        private static DelegateBridge __Hotfix_set_EnemyGuildInfo;
        private static DelegateBridge __Hotfix_get_NpcGuildConfigId;
        private static DelegateBridge __Hotfix_set_NpcGuildConfigId;
        private static DelegateBridge __Hotfix_get_Status;
        private static DelegateBridge __Hotfix_set_Status;
        private static DelegateBridge __Hotfix_get_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_set_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_get_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_set_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_get_IsWin;
        private static DelegateBridge __Hotfix_set_IsWin;
        private static DelegateBridge __Hotfix_get_KillingLostBindMoney;
        private static DelegateBridge __Hotfix_set_KillingLostBindMoney;
        private static DelegateBridge __Hotfix_get_SelfLostBindMoney;
        private static DelegateBridge __Hotfix_set_SelfLostBindMoney;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_EndTime;
        private static DelegateBridge __Hotfix_set_EndTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="GuildBattleInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong GuildBattleInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="BuildingType", DataFormat=DataFormat.TwosComplement)]
        public int BuildingType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="GroupType", DataFormat=DataFormat.TwosComplement)]
        public int GroupType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="SelfGuildInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimplestInfo SelfGuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="EnemyGuildInfo", DataFormat=DataFormat.Default)]
        public ProGuildSimplestInfo EnemyGuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="NpcGuildConfigId", DataFormat=DataFormat.TwosComplement)]
        public int NpcGuildConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Status", DataFormat=DataFormat.TwosComplement)]
        public int Status
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="CurrStatusStartTime", DataFormat=DataFormat.TwosComplement)]
        public long CurrStatusStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="CurrStatusEndTime", DataFormat=DataFormat.TwosComplement)]
        public long CurrStatusEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="IsWin", DataFormat=DataFormat.Default)]
        public bool IsWin
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="KillingLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double KillingLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="SelfLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double SelfLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=true, Name="EndTime", DataFormat=DataFormat.TwosComplement)]
        public long EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

