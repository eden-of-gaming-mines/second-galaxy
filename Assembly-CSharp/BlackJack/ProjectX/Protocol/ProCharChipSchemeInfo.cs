﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProCharChipSchemeInfo")]
    public class ProCharChipSchemeInfo : IExtensible
    {
        private int _Index;
        private bool _UnLock;
        private readonly List<ProCharChipSlotInfo> _ChipSlotList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_UnLock;
        private static DelegateBridge __Hotfix_set_UnLock;
        private static DelegateBridge __Hotfix_get_ChipSlotList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Index", DataFormat=DataFormat.TwosComplement)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="UnLock", DataFormat=DataFormat.Default)]
        public bool UnLock
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ChipSlotList", DataFormat=DataFormat.Default)]
        public List<ProCharChipSlotInfo> ChipSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

