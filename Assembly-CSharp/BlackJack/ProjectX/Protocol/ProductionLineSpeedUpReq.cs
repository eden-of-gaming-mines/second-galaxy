﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProductionLineSpeedUpReq")]
    public class ProductionLineSpeedUpReq : IExtensible
    {
        private int _LineIndex;
        private int _ItemId;
        private int _ItemCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LineIndex;
        private static DelegateBridge __Hotfix_set_LineIndex;
        private static DelegateBridge __Hotfix_get_ItemId;
        private static DelegateBridge __Hotfix_set_ItemId;
        private static DelegateBridge __Hotfix_get_ItemCount;
        private static DelegateBridge __Hotfix_set_ItemCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LineIndex", DataFormat=DataFormat.TwosComplement)]
        public int LineIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ItemId", DataFormat=DataFormat.TwosComplement)]
        public int ItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ItemCount", DataFormat=DataFormat.TwosComplement)]
        public int ItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

