﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProMailInfo")]
    public class ProMailInfo : IExtensible
    {
        private ulong _InstanceId;
        private int _MailType;
        private bool _HasAttachment;
        private bool _IsReaded;
        private int _PredefineMailId;
        private int _DeleteType;
        private string _SenderGameUserId;
        private string _SenderPlayerName;
        private int _SenderAvatarId;
        private string _SendGuildCodeName;
        private string _ReciverGameUserId;
        private string _Title;
        private string _Content;
        private byte[] _BinContent;
        private int _BinContentType;
        private readonly List<ProItemInfo> _AttchmentList;
        private long _SendTime;
        private long _TimeoutTime;
        private int _Profession;
        private readonly List<ProFormatStringParamInfo> _TitleFormatStrParamList;
        private readonly List<ProFormatStringParamInfo> _ContentFormatStrParamList;
        private long _DisplayTime;
        private int _ReadedExpiredTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_MailType;
        private static DelegateBridge __Hotfix_set_MailType;
        private static DelegateBridge __Hotfix_get_HasAttachment;
        private static DelegateBridge __Hotfix_set_HasAttachment;
        private static DelegateBridge __Hotfix_get_IsReaded;
        private static DelegateBridge __Hotfix_set_IsReaded;
        private static DelegateBridge __Hotfix_get_PredefineMailId;
        private static DelegateBridge __Hotfix_set_PredefineMailId;
        private static DelegateBridge __Hotfix_get_DeleteType;
        private static DelegateBridge __Hotfix_set_DeleteType;
        private static DelegateBridge __Hotfix_get_SenderGameUserId;
        private static DelegateBridge __Hotfix_set_SenderGameUserId;
        private static DelegateBridge __Hotfix_get_SenderPlayerName;
        private static DelegateBridge __Hotfix_set_SenderPlayerName;
        private static DelegateBridge __Hotfix_get_SenderAvatarId;
        private static DelegateBridge __Hotfix_set_SenderAvatarId;
        private static DelegateBridge __Hotfix_get_SendGuildCodeName;
        private static DelegateBridge __Hotfix_set_SendGuildCodeName;
        private static DelegateBridge __Hotfix_get_ReciverGameUserId;
        private static DelegateBridge __Hotfix_set_ReciverGameUserId;
        private static DelegateBridge __Hotfix_get_Title;
        private static DelegateBridge __Hotfix_set_Title;
        private static DelegateBridge __Hotfix_get_Content;
        private static DelegateBridge __Hotfix_set_Content;
        private static DelegateBridge __Hotfix_get_BinContent;
        private static DelegateBridge __Hotfix_set_BinContent;
        private static DelegateBridge __Hotfix_get_BinContentType;
        private static DelegateBridge __Hotfix_set_BinContentType;
        private static DelegateBridge __Hotfix_get_AttchmentList;
        private static DelegateBridge __Hotfix_get_SendTime;
        private static DelegateBridge __Hotfix_set_SendTime;
        private static DelegateBridge __Hotfix_get_TimeoutTime;
        private static DelegateBridge __Hotfix_set_TimeoutTime;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_TitleFormatStrParamList;
        private static DelegateBridge __Hotfix_get_ContentFormatStrParamList;
        private static DelegateBridge __Hotfix_get_DisplayTime;
        private static DelegateBridge __Hotfix_set_DisplayTime;
        private static DelegateBridge __Hotfix_get_ReadedExpiredTime;
        private static DelegateBridge __Hotfix_set_ReadedExpiredTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="MailType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int MailType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="HasAttachment", DataFormat=DataFormat.Default)]
        public bool HasAttachment
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="IsReaded", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsReaded
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="PredefineMailId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int PredefineMailId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="DeleteType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DeleteType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="SenderGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string SenderGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="SenderPlayerName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string SenderPlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="SenderAvatarId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SenderAvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(10, IsRequired=false, Name="SendGuildCodeName", DataFormat=DataFormat.Default)]
        public string SendGuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(11, IsRequired=false, Name="ReciverGameUserId", DataFormat=DataFormat.Default)]
        public string ReciverGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="Title", DataFormat=DataFormat.Default), DefaultValue("")]
        public string Title
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(13, IsRequired=false, Name="Content", DataFormat=DataFormat.Default)]
        public string Content
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(14, IsRequired=false, Name="BinContent", DataFormat=DataFormat.Default)]
        public byte[] BinContent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(15, IsRequired=false, Name="BinContentType", DataFormat=DataFormat.TwosComplement)]
        public int BinContentType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, Name="AttchmentList", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> AttchmentList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x11, IsRequired=false, Name="SendTime", DataFormat=DataFormat.TwosComplement)]
        public long SendTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=false, Name="TimeoutTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long TimeoutTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x13, IsRequired=false, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, Name="TitleFormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> TitleFormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x15, Name="ContentFormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> ContentFormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x16, IsRequired=false, Name="DisplayTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long DisplayTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(0x17, IsRequired=false, Name="ReadedExpiredTime", DataFormat=DataFormat.TwosComplement)]
        public int ReadedExpiredTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

