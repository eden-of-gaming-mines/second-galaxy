﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarListAck")]
    public class GuildFlagShipHangarListAck : IExtensible
    {
        private int _Result;
        private readonly List<ProGuildFlagShipHangarDetailInfo> _HangarDetailInfoList;
        private string _HangarLockMasterUserId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_HangarDetailInfoList;
        private static DelegateBridge __Hotfix_get_HangarLockMasterUserId;
        private static DelegateBridge __Hotfix_set_HangarLockMasterUserId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="HangarDetailInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildFlagShipHangarDetailInfo> HangarDetailInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(""), ProtoMember(3, IsRequired=false, Name="HangarLockMasterUserId", DataFormat=DataFormat.Default)]
        public string HangarLockMasterUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

