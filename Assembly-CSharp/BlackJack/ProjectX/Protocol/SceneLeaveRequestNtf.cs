﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SceneLeaveRequestNtf")]
    public class SceneLeaveRequestNtf : IExtensible
    {
        private uint _LeaveTime;
        private int _LeaveReason;
        private bool _IsForceLeave;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LeaveTime;
        private static DelegateBridge __Hotfix_set_LeaveTime;
        private static DelegateBridge __Hotfix_get_LeaveReason;
        private static DelegateBridge __Hotfix_set_LeaveReason;
        private static DelegateBridge __Hotfix_get_IsForceLeave;
        private static DelegateBridge __Hotfix_set_IsForceLeave;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LeaveTime", DataFormat=DataFormat.TwosComplement)]
        public uint LeaveTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="LeaveReason", DataFormat=DataFormat.TwosComplement)]
        public int LeaveReason
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="IsForceLeave", DataFormat=DataFormat.Default)]
        public bool IsForceLeave
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

