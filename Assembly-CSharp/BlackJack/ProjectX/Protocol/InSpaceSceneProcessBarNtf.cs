﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceSceneProcessBarNtf")]
    public class InSpaceSceneProcessBarNtf : IExtensible
    {
        private bool _IsClear;
        private int _Type;
        private int _MessageId;
        private uint _NpcObjId;
        private ProVectorDouble _Location;
        private uint _StartTime;
        private uint _EndTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsClear;
        private static DelegateBridge __Hotfix_set_IsClear;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_MessageId;
        private static DelegateBridge __Hotfix_set_MessageId;
        private static DelegateBridge __Hotfix_get_NpcObjId;
        private static DelegateBridge __Hotfix_set_NpcObjId;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_EndTime;
        private static DelegateBridge __Hotfix_set_EndTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="IsClear", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsClear
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="MessageId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int MessageId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="NpcObjId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint NpcObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="Location", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProVectorDouble Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public uint StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="EndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

