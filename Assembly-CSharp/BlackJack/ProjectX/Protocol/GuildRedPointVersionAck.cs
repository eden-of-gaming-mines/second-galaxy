﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildRedPointVersionAck")]
    public class GuildRedPointVersionAck : IExtensible
    {
        private int _GuildJoinApplyCount;
        private int _GuildMessageBoardVersion;
        private int _AllianceInviteCount;
        private bool _HasAnyBenefits2Balance;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GuildJoinApplyCount;
        private static DelegateBridge __Hotfix_set_GuildJoinApplyCount;
        private static DelegateBridge __Hotfix_get_GuildMessageBoardVersion;
        private static DelegateBridge __Hotfix_set_GuildMessageBoardVersion;
        private static DelegateBridge __Hotfix_get_AllianceInviteCount;
        private static DelegateBridge __Hotfix_set_AllianceInviteCount;
        private static DelegateBridge __Hotfix_get_HasAnyBenefits2Balance;
        private static DelegateBridge __Hotfix_set_HasAnyBenefits2Balance;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="GuildJoinApplyCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GuildJoinApplyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="GuildMessageBoardVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GuildMessageBoardVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="AllianceInviteCount", DataFormat=DataFormat.TwosComplement)]
        public int AllianceInviteCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="HasAnyBenefits2Balance", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool HasAnyBenefits2Balance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

