﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildStaffingLogInfo")]
    public class ProGuildStaffingLogInfo : IExtensible
    {
        private int _Type;
        private int _StringTemplateId;
        private readonly List<ProFormatStringParamInfo> _FormatStrParamList;
        private long _Time;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_StringTemplateId;
        private static DelegateBridge __Hotfix_set_StringTemplateId;
        private static DelegateBridge __Hotfix_get_FormatStrParamList;
        private static DelegateBridge __Hotfix_get_Time;
        private static DelegateBridge __Hotfix_set_Time;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StringTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int StringTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="FormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> FormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Time", DataFormat=DataFormat.TwosComplement)]
        public long Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

