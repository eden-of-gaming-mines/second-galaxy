﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipNavigationCmdReq")]
    public class ShipNavigationCmdReq : IExtensible
    {
        private int _DestType;
        private int _DestSolarSystemId;
        private int _DestId;
        private ulong _SignalInstanceId;
        private int _QuestInstanceId;
        private string _TeamMemberGameUserId;
        private uint _GlobalSceneInstanceId;
        private ulong _GuildActionInstanceId;
        private string _GuildMemberGameUserId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DestType;
        private static DelegateBridge __Hotfix_set_DestType;
        private static DelegateBridge __Hotfix_get_DestSolarSystemId;
        private static DelegateBridge __Hotfix_set_DestSolarSystemId;
        private static DelegateBridge __Hotfix_get_DestId;
        private static DelegateBridge __Hotfix_set_DestId;
        private static DelegateBridge __Hotfix_get_SignalInstanceId;
        private static DelegateBridge __Hotfix_set_SignalInstanceId;
        private static DelegateBridge __Hotfix_get_QuestInstanceId;
        private static DelegateBridge __Hotfix_set_QuestInstanceId;
        private static DelegateBridge __Hotfix_get_TeamMemberGameUserId;
        private static DelegateBridge __Hotfix_set_TeamMemberGameUserId;
        private static DelegateBridge __Hotfix_get_GlobalSceneInstanceId;
        private static DelegateBridge __Hotfix_set_GlobalSceneInstanceId;
        private static DelegateBridge __Hotfix_get_GuildActionInstanceId;
        private static DelegateBridge __Hotfix_set_GuildActionInstanceId;
        private static DelegateBridge __Hotfix_get_GuildMemberGameUserId;
        private static DelegateBridge __Hotfix_set_GuildMemberGameUserId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="DestType", DataFormat=DataFormat.TwosComplement)]
        public int DestType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="DestSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="DestId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="SignalInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong SignalInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="QuestInstanceId", DataFormat=DataFormat.TwosComplement)]
        public int QuestInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(6, IsRequired=false, Name="TeamMemberGameUserId", DataFormat=DataFormat.Default)]
        public string TeamMemberGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="GlobalSceneInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GlobalSceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="GuildActionInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong GuildActionInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(9, IsRequired=false, Name="GuildMemberGameUserId", DataFormat=DataFormat.Default)]
        public string GuildMemberGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

