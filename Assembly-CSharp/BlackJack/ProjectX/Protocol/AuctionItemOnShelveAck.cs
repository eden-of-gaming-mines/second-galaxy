﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AuctionItemOnShelveAck")]
    public class AuctionItemOnShelveAck : IExtensible
    {
        private int _Result;
        private AuctionItemOnShelveReq _Req;
        private ProAuctionItemDetailInfo _DetailInfo;
        private ProStoreItemUpdateInfo _UpdateItem;
        private ProShipStoreItemUpdateInfo _ShipUpdateItem;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Req;
        private static DelegateBridge __Hotfix_set_Req;
        private static DelegateBridge __Hotfix_get_DetailInfo;
        private static DelegateBridge __Hotfix_set_DetailInfo;
        private static DelegateBridge __Hotfix_get_UpdateItem;
        private static DelegateBridge __Hotfix_set_UpdateItem;
        private static DelegateBridge __Hotfix_get_ShipUpdateItem;
        private static DelegateBridge __Hotfix_set_ShipUpdateItem;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="Req", DataFormat=DataFormat.Default)]
        public AuctionItemOnShelveReq Req
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="DetailInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProAuctionItemDetailInfo DetailInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="UpdateItem", DataFormat=DataFormat.Default)]
        public ProStoreItemUpdateInfo UpdateItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ShipUpdateItem", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProShipStoreItemUpdateInfo ShipUpdateItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

