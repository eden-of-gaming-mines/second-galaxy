﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildChangeBasicInfoReq")]
    public class GuildChangeBasicInfoReq : IExtensible
    {
        private int _LanguageType;
        private int _JoinPllicy;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_set_LanguageType;
        private static DelegateBridge __Hotfix_get_JoinPllicy;
        private static DelegateBridge __Hotfix_set_JoinPllicy;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="LanguageType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="JoinPllicy", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int JoinPllicy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

