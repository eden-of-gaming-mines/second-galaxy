﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSItemStoreNtf")]
    public class DSItemStoreNtf : IExtensible
    {
        private ProDSItemStoreInfo _DSItemStoreInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DSItemStoreInfo;
        private static DelegateBridge __Hotfix_set_DSItemStoreInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="DSItemStoreInfo", DataFormat=DataFormat.Default)]
        public ProDSItemStoreInfo DSItemStoreInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

