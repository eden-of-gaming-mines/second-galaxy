﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptainUnlockShipReq")]
    public class HiredCaptainUnlockShipReq : IExtensible
    {
        private ulong _CaptainInstanceId;
        private int _CaptainShipId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_CaptainShipId;
        private static DelegateBridge __Hotfix_set_CaptainShipId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CaptainShipId", DataFormat=DataFormat.TwosComplement)]
        public int CaptainShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

