﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CelestialAnimEffectNtf")]
    public class CelestialAnimEffectNtf : IExtensible
    {
        private uint _CelestialGDBId;
        private int _CelestialEffectType;
        private uint _ShipObjId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CelestialGDBId;
        private static DelegateBridge __Hotfix_set_CelestialGDBId;
        private static DelegateBridge __Hotfix_get_CelestialEffectType;
        private static DelegateBridge __Hotfix_set_CelestialEffectType;
        private static DelegateBridge __Hotfix_get_ShipObjId;
        private static DelegateBridge __Hotfix_set_ShipObjId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="CelestialGDBId", DataFormat=DataFormat.TwosComplement)]
        public uint CelestialGDBId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CelestialEffectType", DataFormat=DataFormat.TwosComplement)]
        public int CelestialEffectType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipObjId", DataFormat=DataFormat.TwosComplement)]
        public uint ShipObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

