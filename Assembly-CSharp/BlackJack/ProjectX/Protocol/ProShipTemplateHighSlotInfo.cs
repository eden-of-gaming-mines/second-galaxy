﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipTemplateHighSlotInfo")]
    public class ProShipTemplateHighSlotInfo : IExtensible
    {
        private int _ItemType;
        private int _ItemId;
        private int _AmmoItemType;
        private int _AmmoItemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_ItemId;
        private static DelegateBridge __Hotfix_set_ItemId;
        private static DelegateBridge __Hotfix_get_AmmoItemType;
        private static DelegateBridge __Hotfix_set_AmmoItemType;
        private static DelegateBridge __Hotfix_get_AmmoItemId;
        private static DelegateBridge __Hotfix_set_AmmoItemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ItemType", DataFormat=DataFormat.TwosComplement)]
        public int ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ItemId", DataFormat=DataFormat.TwosComplement)]
        public int ItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AmmoItemType", DataFormat=DataFormat.TwosComplement)]
        public int AmmoItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="AmmoItemId", DataFormat=DataFormat.TwosComplement)]
        public int AmmoItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

