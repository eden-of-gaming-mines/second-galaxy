﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDelegateMissionInvadeEventInfo")]
    public class ProDelegateMissionInvadeEventInfo : IExtensible
    {
        private int _EventType;
        private long _Time;
        private ProNpcCaptainSimpleInfo _RelativeCaptain;
        private ProPlayerSimpleInfo _RelativePlayer;
        private ProNpcCaptainSimpleInfo _KillerCaptainInfo;
        private ProPlayerSimpleInfo _KillerPlayerInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EventType;
        private static DelegateBridge __Hotfix_set_EventType;
        private static DelegateBridge __Hotfix_get_Time;
        private static DelegateBridge __Hotfix_set_Time;
        private static DelegateBridge __Hotfix_get_RelativeCaptain;
        private static DelegateBridge __Hotfix_set_RelativeCaptain;
        private static DelegateBridge __Hotfix_get_RelativePlayer;
        private static DelegateBridge __Hotfix_set_RelativePlayer;
        private static DelegateBridge __Hotfix_get_KillerCaptainInfo;
        private static DelegateBridge __Hotfix_set_KillerCaptainInfo;
        private static DelegateBridge __Hotfix_get_KillerPlayerInfo;
        private static DelegateBridge __Hotfix_set_KillerPlayerInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="EventType", DataFormat=DataFormat.TwosComplement)]
        public int EventType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="Time", DataFormat=DataFormat.TwosComplement)]
        public long Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="RelativeCaptain", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProNpcCaptainSimpleInfo RelativeCaptain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="RelativePlayer", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProPlayerSimpleInfo RelativePlayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="KillerCaptainInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProNpcCaptainSimpleInfo KillerCaptainInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="KillerPlayerInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProPlayerSimpleInfo KillerPlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

