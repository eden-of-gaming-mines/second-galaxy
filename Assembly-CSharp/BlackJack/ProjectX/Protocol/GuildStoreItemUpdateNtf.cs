﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildStoreItemUpdateNtf")]
    public class GuildStoreItemUpdateNtf : IExtensible
    {
        private readonly List<ProGuildStoreItemUpdateInfo> _UpdateList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_UpdateList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="UpdateList", DataFormat=DataFormat.Default)]
        public List<ProGuildStoreItemUpdateInfo> UpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

