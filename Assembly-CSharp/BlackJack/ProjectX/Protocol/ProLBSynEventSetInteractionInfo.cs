﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSynEventSetInteractionInfo")]
    public class ProLBSynEventSetInteractionInfo : IExtensible
    {
        private int _InteractionType;
        private uint _OptTime;
        private uint _Range;
        private int _Flag;
        private int _InteractionMessage;
        private int _InteractionTemplateId;
        private uint _SinglePlayerIteractionCountMax;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InteractionType;
        private static DelegateBridge __Hotfix_set_InteractionType;
        private static DelegateBridge __Hotfix_get_OptTime;
        private static DelegateBridge __Hotfix_set_OptTime;
        private static DelegateBridge __Hotfix_get_Range;
        private static DelegateBridge __Hotfix_set_Range;
        private static DelegateBridge __Hotfix_get_Flag;
        private static DelegateBridge __Hotfix_set_Flag;
        private static DelegateBridge __Hotfix_get_InteractionMessage;
        private static DelegateBridge __Hotfix_set_InteractionMessage;
        private static DelegateBridge __Hotfix_get_InteractionTemplateId;
        private static DelegateBridge __Hotfix_set_InteractionTemplateId;
        private static DelegateBridge __Hotfix_get_SinglePlayerIteractionCountMax;
        private static DelegateBridge __Hotfix_set_SinglePlayerIteractionCountMax;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="InteractionType", DataFormat=DataFormat.TwosComplement)]
        public int InteractionType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="OptTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint OptTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="Range", DataFormat=DataFormat.TwosComplement)]
        public uint Range
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="Flag", DataFormat=DataFormat.TwosComplement)]
        public int Flag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="InteractionMessage", DataFormat=DataFormat.TwosComplement)]
        public int InteractionMessage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="InteractionTemplateId", DataFormat=DataFormat.TwosComplement)]
        public int InteractionTemplateId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="SinglePlayerIteractionCountMax", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint SinglePlayerIteractionCountMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

