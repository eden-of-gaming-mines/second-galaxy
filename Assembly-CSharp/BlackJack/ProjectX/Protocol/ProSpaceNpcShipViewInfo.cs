﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceNpcShipViewInfo")]
    public class ProSpaceNpcShipViewInfo : IExtensible
    {
        private int _NpcConfigId;
        private int _Shield;
        private int _Armor;
        private int _Energy;
        private int _NpcShipMonsterLevel;
        private int _CreateEffectType;
        private int _FactionId;
        private int _TargetNoticeFlag;
        private readonly List<ProBufInfo> _DynamicBufList;
        private bool _IsNpcPollice;
        private ProSceneNpcInteractionInfo _InteractionInfo;
        private bool _IsHideInTargetListRestByServer;
        private bool _IsHideInTargetList;
        private bool _IsNotAttackableRestByServer;
        private bool _IsNotAttackable;
        private bool _IsInvisible;
        private uint _GuildId;
        private string _GuildCodeName;
        private uint _AllianceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NpcConfigId;
        private static DelegateBridge __Hotfix_set_NpcConfigId;
        private static DelegateBridge __Hotfix_get_Shield;
        private static DelegateBridge __Hotfix_set_Shield;
        private static DelegateBridge __Hotfix_get_Armor;
        private static DelegateBridge __Hotfix_set_Armor;
        private static DelegateBridge __Hotfix_get_Energy;
        private static DelegateBridge __Hotfix_set_Energy;
        private static DelegateBridge __Hotfix_get_NpcShipMonsterLevel;
        private static DelegateBridge __Hotfix_set_NpcShipMonsterLevel;
        private static DelegateBridge __Hotfix_get_CreateEffectType;
        private static DelegateBridge __Hotfix_set_CreateEffectType;
        private static DelegateBridge __Hotfix_get_FactionId;
        private static DelegateBridge __Hotfix_set_FactionId;
        private static DelegateBridge __Hotfix_get_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_set_TargetNoticeFlag;
        private static DelegateBridge __Hotfix_get_DynamicBufList;
        private static DelegateBridge __Hotfix_get_IsNpcPollice;
        private static DelegateBridge __Hotfix_set_IsNpcPollice;
        private static DelegateBridge __Hotfix_get_InteractionInfo;
        private static DelegateBridge __Hotfix_set_InteractionInfo;
        private static DelegateBridge __Hotfix_get_IsHideInTargetListRestByServer;
        private static DelegateBridge __Hotfix_set_IsHideInTargetListRestByServer;
        private static DelegateBridge __Hotfix_get_IsHideInTargetList;
        private static DelegateBridge __Hotfix_set_IsHideInTargetList;
        private static DelegateBridge __Hotfix_get_IsNotAttackableRestByServer;
        private static DelegateBridge __Hotfix_set_IsNotAttackableRestByServer;
        private static DelegateBridge __Hotfix_get_IsNotAttackable;
        private static DelegateBridge __Hotfix_set_IsNotAttackable;
        private static DelegateBridge __Hotfix_get_IsInvisible;
        private static DelegateBridge __Hotfix_set_IsInvisible;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_GuildCodeName;
        private static DelegateBridge __Hotfix_set_GuildCodeName;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="NpcConfigId", DataFormat=DataFormat.TwosComplement)]
        public int NpcConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Shield", DataFormat=DataFormat.TwosComplement)]
        public int Shield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Armor", DataFormat=DataFormat.TwosComplement)]
        public int Armor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Energy", DataFormat=DataFormat.TwosComplement)]
        public int Energy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="NpcShipMonsterLevel", DataFormat=DataFormat.TwosComplement)]
        public int NpcShipMonsterLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="CreateEffectType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CreateEffectType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="FactionId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="TargetNoticeFlag", DataFormat=DataFormat.TwosComplement)]
        public int TargetNoticeFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="DynamicBufList", DataFormat=DataFormat.Default)]
        public List<ProBufInfo> DynamicBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(false), ProtoMember(11, IsRequired=false, Name="IsNpcPollice", DataFormat=DataFormat.Default)]
        public bool IsNpcPollice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(12, IsRequired=false, Name="InteractionInfo", DataFormat=DataFormat.Default)]
        public ProSceneNpcInteractionInfo InteractionInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(13, IsRequired=false, Name="IsHideInTargetListRestByServer", DataFormat=DataFormat.Default)]
        public bool IsHideInTargetListRestByServer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(14, IsRequired=false, Name="IsHideInTargetList", DataFormat=DataFormat.Default)]
        public bool IsHideInTargetList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(15, IsRequired=false, Name="IsNotAttackableRestByServer", DataFormat=DataFormat.Default)]
        public bool IsNotAttackableRestByServer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="IsNotAttackable", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsNotAttackable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=false, Name="IsInvisible", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsInvisible
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x12, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, IsRequired=false, Name="GuildCodeName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string GuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(20, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

