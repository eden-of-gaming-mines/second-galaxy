﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSHiredCaptainManagmentNtf")]
    public class DSHiredCaptainManagmentNtf : IExtensible
    {
        private readonly List<ProDSHiredCaptainInfo> _CaptainList;
        private readonly List<ulong> _WingManList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CaptainList;
        private static DelegateBridge __Hotfix_get_WingManList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="CaptainList", DataFormat=DataFormat.Default)]
        public List<ProDSHiredCaptainInfo> CaptainList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="WingManList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> WingManList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

