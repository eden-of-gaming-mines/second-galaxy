﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TradeNpcShopSaleItemsReq")]
    public class TradeNpcShopSaleItemsReq : IExtensible
    {
        private ProNpcDNId _TradeNpcDNId;
        private uint _DataVersion;
        private readonly List<ProIdValueInfo> _ShipStoreTradeItemInfos;
        private readonly List<ProIdValueInfo> _StoreTradeItemInfos;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TradeNpcDNId;
        private static DelegateBridge __Hotfix_set_TradeNpcDNId;
        private static DelegateBridge __Hotfix_get_DataVersion;
        private static DelegateBridge __Hotfix_set_DataVersion;
        private static DelegateBridge __Hotfix_get_ShipStoreTradeItemInfos;
        private static DelegateBridge __Hotfix_get_StoreTradeItemInfos;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TradeNpcDNId", DataFormat=DataFormat.Default)]
        public ProNpcDNId TradeNpcDNId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="DataVersion", DataFormat=DataFormat.TwosComplement)]
        public uint DataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ShipStoreTradeItemInfos", DataFormat=DataFormat.Default)]
        public List<ProIdValueInfo> ShipStoreTradeItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="StoreTradeItemInfos", DataFormat=DataFormat.Default)]
        public List<ProIdValueInfo> StoreTradeItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

