﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildChangeNameAndCodeReq")]
    public class GuildChangeNameAndCodeReq : IExtensible
    {
        private string _NewGuildName;
        private string _newGuildCode;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NewGuildName;
        private static DelegateBridge __Hotfix_set_NewGuildName;
        private static DelegateBridge __Hotfix_get_NewGuildCode;
        private static DelegateBridge __Hotfix_set_NewGuildCode;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="NewGuildName", DataFormat=DataFormat.Default)]
        public string NewGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="newGuildCode", DataFormat=DataFormat.Default)]
        public string NewGuildCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

