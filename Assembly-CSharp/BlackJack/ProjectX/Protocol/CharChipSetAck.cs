﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CharChipSetAck")]
    public class CharChipSetAck : IExtensible
    {
        private int _Result;
        private int _SlotIndex;
        private int _ItemStoreIndex;
        private int _ChipId;
        private int _ChipSlotRandomBuf;
        private ProStoreItemUpdateInfo _UpdateItem;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SlotIndex;
        private static DelegateBridge __Hotfix_set_SlotIndex;
        private static DelegateBridge __Hotfix_get_ItemStoreIndex;
        private static DelegateBridge __Hotfix_set_ItemStoreIndex;
        private static DelegateBridge __Hotfix_get_ChipId;
        private static DelegateBridge __Hotfix_set_ChipId;
        private static DelegateBridge __Hotfix_get_ChipSlotRandomBuf;
        private static DelegateBridge __Hotfix_set_ChipSlotRandomBuf;
        private static DelegateBridge __Hotfix_get_UpdateItem;
        private static DelegateBridge __Hotfix_set_UpdateItem;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="SlotIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ItemStoreIndex", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ItemStoreIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="ChipId", DataFormat=DataFormat.TwosComplement)]
        public int ChipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ChipSlotRandomBuf", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ChipSlotRandomBuf
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(6, IsRequired=false, Name="UpdateItem", DataFormat=DataFormat.Default)]
        public ProStoreItemUpdateInfo UpdateItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

