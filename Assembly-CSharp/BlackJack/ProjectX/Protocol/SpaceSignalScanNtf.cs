﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SpaceSignalScanNtf")]
    public class SpaceSignalScanNtf : IExtensible
    {
        private int _Result;
        private int _SolarSystemId;
        private int _SignalType;
        private long _ExpiryTime;
        private readonly List<ProSignalInfo> _ProSignalInfoList;
        private bool _IsGenNewSignal;
        private bool _IsSignalCalculatePriceses;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_SignalType;
        private static DelegateBridge __Hotfix_set_SignalType;
        private static DelegateBridge __Hotfix_get_ExpiryTime;
        private static DelegateBridge __Hotfix_set_ExpiryTime;
        private static DelegateBridge __Hotfix_get_ProSignalInfoList;
        private static DelegateBridge __Hotfix_get_IsGenNewSignal;
        private static DelegateBridge __Hotfix_set_IsGenNewSignal;
        private static DelegateBridge __Hotfix_get_IsSignalCalculatePriceses;
        private static DelegateBridge __Hotfix_set_IsSignalCalculatePriceses;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="SignalType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SignalType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="ExpiryTime", DataFormat=DataFormat.TwosComplement)]
        public long ExpiryTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="ProSignalInfoList", DataFormat=DataFormat.Default)]
        public List<ProSignalInfo> ProSignalInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(false), ProtoMember(6, IsRequired=false, Name="IsGenNewSignal", DataFormat=DataFormat.Default)]
        public bool IsGenNewSignal
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="IsSignalCalculatePriceses", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsSignalCalculatePriceses
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

