﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="IFFSetTarget2EnemyReq")]
    public class IFFSetTarget2EnemyReq : IExtensible
    {
        private readonly List<ProIFFStatePairInfo> _PairList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PairList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="PairList", DataFormat=DataFormat.Default)]
        public List<ProIFFStatePairInfo> PairList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

