﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HiredCaptainRetireAck")]
    public class HiredCaptainRetireAck : IExtensible
    {
        private int _Result;
        private ulong _CaptainInstanceId;
        private int _SelectedFeatsId;
        private int _UseExtraItemCount;
        private readonly List<ProStoreItemUpdateInfo> _UpdateItemList;
        private long _RetireExp;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_CaptainInstanceId;
        private static DelegateBridge __Hotfix_set_CaptainInstanceId;
        private static DelegateBridge __Hotfix_get_SelectedFeatsId;
        private static DelegateBridge __Hotfix_set_SelectedFeatsId;
        private static DelegateBridge __Hotfix_get_UseExtraItemCount;
        private static DelegateBridge __Hotfix_set_UseExtraItemCount;
        private static DelegateBridge __Hotfix_get_UpdateItemList;
        private static DelegateBridge __Hotfix_get_RetireExp;
        private static DelegateBridge __Hotfix_set_RetireExp;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CaptainInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong CaptainInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="SelectedFeatsId", DataFormat=DataFormat.TwosComplement)]
        public int SelectedFeatsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="UseExtraItemCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int UseExtraItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="UpdateItemList", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> UpdateItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="RetireExp", DataFormat=DataFormat.TwosComplement)]
        public long RetireExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

