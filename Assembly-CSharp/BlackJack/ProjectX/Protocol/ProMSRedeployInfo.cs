﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProMSRedeployInfo")]
    public class ProMSRedeployInfo : IExtensible
    {
        private long _RedeployStartTime;
        private long _RedeployChargingEndTime;
        private long _RedeployChargingOriginalEndTime;
        private int _RedeployDestSolarsystemId;
        private int _RedeployDestStationId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_RedeployStartTime;
        private static DelegateBridge __Hotfix_set_RedeployStartTime;
        private static DelegateBridge __Hotfix_get_RedeployChargingEndTime;
        private static DelegateBridge __Hotfix_set_RedeployChargingEndTime;
        private static DelegateBridge __Hotfix_get_RedeployChargingOriginalEndTime;
        private static DelegateBridge __Hotfix_set_RedeployChargingOriginalEndTime;
        private static DelegateBridge __Hotfix_get_RedeployDestSolarsystemId;
        private static DelegateBridge __Hotfix_set_RedeployDestSolarsystemId;
        private static DelegateBridge __Hotfix_get_RedeployDestStationId;
        private static DelegateBridge __Hotfix_set_RedeployDestStationId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="RedeployStartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long RedeployStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="RedeployChargingEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long RedeployChargingEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="RedeployChargingOriginalEndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long RedeployChargingOriginalEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="RedeployDestSolarsystemId", DataFormat=DataFormat.TwosComplement)]
        public int RedeployDestSolarsystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="RedeployDestStationId", DataFormat=DataFormat.TwosComplement)]
        public int RedeployDestStationId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

