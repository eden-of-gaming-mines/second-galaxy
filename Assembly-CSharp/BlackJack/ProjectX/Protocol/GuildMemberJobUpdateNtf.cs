﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildMemberJobUpdateNtf")]
    public class GuildMemberJobUpdateNtf : IExtensible
    {
        private readonly List<int> _Jobs;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Jobs;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="Jobs", DataFormat=DataFormat.TwosComplement)]
        public List<int> Jobs
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

