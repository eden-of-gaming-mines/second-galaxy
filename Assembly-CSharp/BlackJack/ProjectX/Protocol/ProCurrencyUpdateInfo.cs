﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProCurrencyUpdateInfo")]
    public class ProCurrencyUpdateInfo : IExtensible
    {
        private int _CurrencyType;
        private ulong _CurrencyNum;
        private long _ChangeValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_CurrencyType;
        private static DelegateBridge __Hotfix_set_CurrencyType;
        private static DelegateBridge __Hotfix_get_CurrencyNum;
        private static DelegateBridge __Hotfix_set_CurrencyNum;
        private static DelegateBridge __Hotfix_get_ChangeValue;
        private static DelegateBridge __Hotfix_set_ChangeValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="CurrencyType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrencyType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="CurrencyNum", DataFormat=DataFormat.TwosComplement)]
        public ulong CurrencyNum
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="ChangeValue", DataFormat=DataFormat.TwosComplement)]
        public long ChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

