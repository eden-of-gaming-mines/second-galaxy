﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AuctionItemTypeListAck")]
    public class AuctionItemTypeListAck : IExtensible
    {
        private int _Result;
        private int _AuctionItemType;
        private readonly List<ProAuctionItemBriefInfo> _ItemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_AuctionItemType;
        private static DelegateBridge __Hotfix_set_AuctionItemType;
        private static DelegateBridge __Hotfix_get_ItemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="AuctionItemType", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AuctionItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ItemList", DataFormat=DataFormat.Default)]
        public List<ProAuctionItemBriefInfo> ItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

