﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DrivingAssessmentStartAck")]
    public class DrivingAssessmentStartAck : IExtensible
    {
        private int _Result;
        private int _SkillId;
        private int _Level;
        private ProQuestProcessingInfo _QuestInfo;
        private ProQuestEnvirmentInfo _QuestEnvInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SkillId;
        private static DelegateBridge __Hotfix_set_SkillId;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_QuestInfo;
        private static DelegateBridge __Hotfix_set_QuestInfo;
        private static DelegateBridge __Hotfix_get_QuestEnvInfo;
        private static DelegateBridge __Hotfix_set_QuestEnvInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="SkillId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SkillId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="Level", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="QuestInfo", DataFormat=DataFormat.Default)]
        public ProQuestProcessingInfo QuestInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="QuestEnvInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProQuestEnvirmentInfo QuestEnvInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

