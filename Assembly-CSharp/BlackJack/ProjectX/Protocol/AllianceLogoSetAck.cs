﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AllianceLogoSetAck")]
    public class AllianceLogoSetAck : IExtensible
    {
        private int _Result;
        private ProAllianceLogoInfo _Logo;
        private int _GuildTradeMoneyCost;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Logo;
        private static DelegateBridge __Hotfix_set_Logo;
        private static DelegateBridge __Hotfix_get_GuildTradeMoneyCost;
        private static DelegateBridge __Hotfix_set_GuildTradeMoneyCost;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="Logo", DataFormat=DataFormat.Default)]
        public ProAllianceLogoInfo Logo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="GuildTradeMoneyCost", DataFormat=DataFormat.TwosComplement)]
        public int GuildTradeMoneyCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

