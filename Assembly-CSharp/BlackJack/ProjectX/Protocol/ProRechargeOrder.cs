﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProRechargeOrder")]
    public class ProRechargeOrder : IExtensible
    {
        private ulong _OrderInstanceId;
        private int _GoodsType;
        private int _GoodsId;
        private long _OrderCreateTime;
        private long _OrderPaymentTime;
        private long _OrderDeliverTime;
        private int _OrderStatus;
        private string _CustomizedField;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_OrderInstanceId;
        private static DelegateBridge __Hotfix_set_OrderInstanceId;
        private static DelegateBridge __Hotfix_get_GoodsType;
        private static DelegateBridge __Hotfix_set_GoodsType;
        private static DelegateBridge __Hotfix_get_GoodsId;
        private static DelegateBridge __Hotfix_set_GoodsId;
        private static DelegateBridge __Hotfix_get_OrderCreateTime;
        private static DelegateBridge __Hotfix_set_OrderCreateTime;
        private static DelegateBridge __Hotfix_get_OrderPaymentTime;
        private static DelegateBridge __Hotfix_set_OrderPaymentTime;
        private static DelegateBridge __Hotfix_get_OrderDeliverTime;
        private static DelegateBridge __Hotfix_set_OrderDeliverTime;
        private static DelegateBridge __Hotfix_get_OrderStatus;
        private static DelegateBridge __Hotfix_set_OrderStatus;
        private static DelegateBridge __Hotfix_get_CustomizedField;
        private static DelegateBridge __Hotfix_set_CustomizedField;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="OrderInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong OrderInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="GoodsType", DataFormat=DataFormat.TwosComplement)]
        public int GoodsType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="GoodsId", DataFormat=DataFormat.TwosComplement)]
        public int GoodsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="OrderCreateTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long OrderCreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="OrderPaymentTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long OrderPaymentTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="OrderDeliverTime", DataFormat=DataFormat.TwosComplement)]
        public long OrderDeliverTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="OrderStatus", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int OrderStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="CustomizedField", DataFormat=DataFormat.Default), DefaultValue("")]
        public string CustomizedField
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

