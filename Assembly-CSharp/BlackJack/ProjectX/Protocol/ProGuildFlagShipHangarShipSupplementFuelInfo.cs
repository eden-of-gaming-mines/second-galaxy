﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFlagShipHangarShipSupplementFuelInfo")]
    public class ProGuildFlagShipHangarShipSupplementFuelInfo : IExtensible
    {
        private int _HangarShipIndex;
        private int _SupplementCount;
        private int _FuelIndex;
        private ulong _FuelInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_SupplementCount;
        private static DelegateBridge __Hotfix_set_SupplementCount;
        private static DelegateBridge __Hotfix_get_FuelIndex;
        private static DelegateBridge __Hotfix_set_FuelIndex;
        private static DelegateBridge __Hotfix_get_FuelInstanceId;
        private static DelegateBridge __Hotfix_set_FuelInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SupplementCount", DataFormat=DataFormat.TwosComplement)]
        public int SupplementCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="FuelIndex", DataFormat=DataFormat.TwosComplement)]
        public int FuelIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(4, IsRequired=false, Name="FuelInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong FuelInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

