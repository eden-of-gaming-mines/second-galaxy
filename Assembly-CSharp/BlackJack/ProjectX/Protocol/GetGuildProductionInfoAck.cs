﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GetGuildProductionInfoAck")]
    public class GetGuildProductionInfoAck : IExtensible
    {
        private int _Result;
        private uint _ProdcutionDataVersion;
        private readonly List<ProGuildProductionLineInfo> _ProductionLineList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ProdcutionDataVersion;
        private static DelegateBridge __Hotfix_set_ProdcutionDataVersion;
        private static DelegateBridge __Hotfix_get_ProductionLineList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ProdcutionDataVersion", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ProdcutionDataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="ProductionLineList", DataFormat=DataFormat.Default)]
        public List<ProGuildProductionLineInfo> ProductionLineList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

