﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSSignalInfoNtf")]
    public class DSSignalInfoNtf : IExtensible
    {
        private readonly List<ProSignalInfo> _DelegateSignalList;
        private readonly List<ProSignalInfo> _ManualSignalList;
        private readonly List<ProPVPSignalInfo> _PVPSignalList;
        private readonly List<ProScanProbeInfo> _ScanProbeList;
        private uint _Version;
        private readonly List<ProSignalInfo> _EmergencySignalList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DelegateSignalList;
        private static DelegateBridge __Hotfix_get_ManualSignalList;
        private static DelegateBridge __Hotfix_get_PVPSignalList;
        private static DelegateBridge __Hotfix_get_ScanProbeList;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_EmergencySignalList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="DelegateSignalList", DataFormat=DataFormat.Default)]
        public List<ProSignalInfo> DelegateSignalList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="ManualSignalList", DataFormat=DataFormat.Default)]
        public List<ProSignalInfo> ManualSignalList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="PVPSignalList", DataFormat=DataFormat.Default)]
        public List<ProPVPSignalInfo> PVPSignalList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="ScanProbeList", DataFormat=DataFormat.Default)]
        public List<ProScanProbeInfo> ScanProbeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, Name="EmergencySignalList", DataFormat=DataFormat.Default)]
        public List<ProSignalInfo> EmergencySignalList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

