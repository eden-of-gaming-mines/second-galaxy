﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceSceneTextNoticeNtf")]
    public class InSpaceSceneTextNoticeNtf : IExtensible
    {
        private bool _IsClear;
        private int _DailogId;
        private int _NoticeType;
        private readonly List<string> _ExtraParam;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsClear;
        private static DelegateBridge __Hotfix_set_IsClear;
        private static DelegateBridge __Hotfix_get_DailogId;
        private static DelegateBridge __Hotfix_set_DailogId;
        private static DelegateBridge __Hotfix_get_NoticeType;
        private static DelegateBridge __Hotfix_set_NoticeType;
        private static DelegateBridge __Hotfix_get_ExtraParam;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(false), ProtoMember(1, IsRequired=false, Name="IsClear", DataFormat=DataFormat.Default)]
        public bool IsClear
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="DailogId", DataFormat=DataFormat.TwosComplement)]
        public int DailogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="NoticeType", DataFormat=DataFormat.TwosComplement)]
        public int NoticeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="ExtraParam", DataFormat=DataFormat.Default)]
        public List<string> ExtraParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

