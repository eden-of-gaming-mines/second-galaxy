﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFlagShipHangarDetailInfo")]
    public class ProGuildFlagShipHangarDetailInfo : IExtensible
    {
        private ulong _InstanceId;
        private int _BasicInfoVersion;
        private int _ShipHangarSlotListVersion;
        private ProGuildFlagShipHangarBasicInfo _ShipHangarBasicInfo;
        private readonly List<ProGuildFlagShipDetailInfo> _ShipHangarDetailInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_BasicInfoVersion;
        private static DelegateBridge __Hotfix_set_BasicInfoVersion;
        private static DelegateBridge __Hotfix_get_ShipHangarSlotListVersion;
        private static DelegateBridge __Hotfix_set_ShipHangarSlotListVersion;
        private static DelegateBridge __Hotfix_get_ShipHangarBasicInfo;
        private static DelegateBridge __Hotfix_set_ShipHangarBasicInfo;
        private static DelegateBridge __Hotfix_get_ShipHangarDetailInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="BasicInfoVersion", DataFormat=DataFormat.TwosComplement)]
        public int BasicInfoVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipHangarSlotListVersion", DataFormat=DataFormat.TwosComplement)]
        public int ShipHangarSlotListVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="ShipHangarBasicInfo", DataFormat=DataFormat.Default)]
        public ProGuildFlagShipHangarBasicInfo ShipHangarBasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="ShipHangarDetailInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildFlagShipDetailInfo> ShipHangarDetailInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

