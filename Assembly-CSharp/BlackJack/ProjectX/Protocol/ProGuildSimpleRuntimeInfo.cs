﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildSimpleRuntimeInfo")]
    public class ProGuildSimpleRuntimeInfo : IExtensible
    {
        private int _MemberCount;
        private int _OnlineMemberCount;
        private int _OccupySolorSystemCount;
        private uint _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_MemberCount;
        private static DelegateBridge __Hotfix_set_MemberCount;
        private static DelegateBridge __Hotfix_get_OnlineMemberCount;
        private static DelegateBridge __Hotfix_set_OnlineMemberCount;
        private static DelegateBridge __Hotfix_get_OccupySolorSystemCount;
        private static DelegateBridge __Hotfix_set_OccupySolorSystemCount;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="MemberCount", DataFormat=DataFormat.TwosComplement)]
        public int MemberCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="OnlineMemberCount", DataFormat=DataFormat.TwosComplement)]
        public int OnlineMemberCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="OccupySolorSystemCount", DataFormat=DataFormat.TwosComplement)]
        public int OccupySolorSystemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

