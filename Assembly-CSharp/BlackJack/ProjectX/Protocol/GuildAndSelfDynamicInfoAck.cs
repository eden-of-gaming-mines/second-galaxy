﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildAndSelfDynamicInfoAck")]
    public class GuildAndSelfDynamicInfoAck : IExtensible
    {
        private int _Result;
        private ProGuildDynamicInfo _GuildDynamicInfo;
        private ProGuildMemberDynamicInfo _SelfDynamicInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_GuildDynamicInfo;
        private static DelegateBridge __Hotfix_set_GuildDynamicInfo;
        private static DelegateBridge __Hotfix_get_SelfDynamicInfo;
        private static DelegateBridge __Hotfix_set_SelfDynamicInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="GuildDynamicInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildDynamicInfo GuildDynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(3, IsRequired=false, Name="SelfDynamicInfo", DataFormat=DataFormat.Default)]
        public ProGuildMemberDynamicInfo SelfDynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

