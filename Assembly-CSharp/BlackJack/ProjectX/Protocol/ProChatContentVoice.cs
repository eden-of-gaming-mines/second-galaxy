﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatContentVoice")]
    public class ProChatContentVoice : IExtensible
    {
        private ulong _InstanceId;
        private byte[] _Voice;
        private int _VoiceLenth;
        private int _AudioFrequency;
        private int _AudioSampleLength;
        private bool _IsOverdued;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_Voice;
        private static DelegateBridge __Hotfix_set_Voice;
        private static DelegateBridge __Hotfix_get_VoiceLenth;
        private static DelegateBridge __Hotfix_set_VoiceLenth;
        private static DelegateBridge __Hotfix_get_AudioFrequency;
        private static DelegateBridge __Hotfix_set_AudioFrequency;
        private static DelegateBridge __Hotfix_get_AudioSampleLength;
        private static DelegateBridge __Hotfix_set_AudioSampleLength;
        private static DelegateBridge __Hotfix_get_IsOverdued;
        private static DelegateBridge __Hotfix_set_IsOverdued;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="Voice", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public byte[] Voice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="VoiceLenth", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int VoiceLenth
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="AudioFrequency", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AudioFrequency
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="AudioSampleLength", DataFormat=DataFormat.TwosComplement)]
        public int AudioSampleLength
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="IsOverdued", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsOverdued
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

