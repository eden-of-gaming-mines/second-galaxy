﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceSimpleNpcDailogEndReq")]
    public class InSpaceSimpleNpcDailogEndReq : IExtensible
    {
        private int _StartDailogId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StartDailogId;
        private static DelegateBridge __Hotfix_set_StartDailogId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="StartDailogId", DataFormat=DataFormat.TwosComplement)]
        public int StartDailogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

