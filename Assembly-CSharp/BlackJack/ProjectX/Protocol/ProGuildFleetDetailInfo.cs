﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFleetDetailInfo")]
    public class ProGuildFleetDetailInfo : IExtensible
    {
        private ulong _FleetId;
        private ProGuildFleetBasicInfo _FleetBasicInfo;
        private readonly List<ProGuildFleetMemberBasicInfo> _FleetMemberList;
        private readonly List<ProGuildFleetMemberDynamicInfo> _FleetMemberDynamicInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FleetId;
        private static DelegateBridge __Hotfix_set_FleetId;
        private static DelegateBridge __Hotfix_get_FleetBasicInfo;
        private static DelegateBridge __Hotfix_set_FleetBasicInfo;
        private static DelegateBridge __Hotfix_get_FleetMemberList;
        private static DelegateBridge __Hotfix_get_FleetMemberDynamicInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FleetId", DataFormat=DataFormat.TwosComplement)]
        public ulong FleetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="FleetBasicInfo", DataFormat=DataFormat.Default)]
        public ProGuildFleetBasicInfo FleetBasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="FleetMemberList", DataFormat=DataFormat.Default)]
        public List<ProGuildFleetMemberBasicInfo> FleetMemberList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="FleetMemberDynamicInfo", DataFormat=DataFormat.Default)]
        public List<ProGuildFleetMemberDynamicInfo> FleetMemberDynamicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

