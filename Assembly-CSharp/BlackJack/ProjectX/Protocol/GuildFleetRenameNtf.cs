﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFleetRenameNtf")]
    public class GuildFleetRenameNtf : IExtensible
    {
        private ulong _FleetId;
        private string _FleetName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FleetId;
        private static DelegateBridge __Hotfix_set_FleetId;
        private static DelegateBridge __Hotfix_get_FleetName;
        private static DelegateBridge __Hotfix_set_FleetName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FleetId", DataFormat=DataFormat.TwosComplement)]
        public ulong FleetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="FleetName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string FleetName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

