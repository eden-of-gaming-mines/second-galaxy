﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProQuestWaitForAcceptInfo")]
    public class ProQuestWaitForAcceptInfo : IExtensible
    {
        private int _QuestId;
        private int _QuestFactionId;
        private bool _FromQuestCancel;
        private int _QuestLevel;
        private int _QuestSolarSystemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_QuestId;
        private static DelegateBridge __Hotfix_set_QuestId;
        private static DelegateBridge __Hotfix_get_QuestFactionId;
        private static DelegateBridge __Hotfix_set_QuestFactionId;
        private static DelegateBridge __Hotfix_get_FromQuestCancel;
        private static DelegateBridge __Hotfix_set_FromQuestCancel;
        private static DelegateBridge __Hotfix_get_QuestLevel;
        private static DelegateBridge __Hotfix_set_QuestLevel;
        private static DelegateBridge __Hotfix_get_QuestSolarSystemId;
        private static DelegateBridge __Hotfix_set_QuestSolarSystemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="QuestId", DataFormat=DataFormat.TwosComplement)]
        public int QuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="QuestFactionId", DataFormat=DataFormat.TwosComplement)]
        public int QuestFactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="FromQuestCancel", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool FromQuestCancel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="QuestLevel", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int QuestLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="QuestSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int QuestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

