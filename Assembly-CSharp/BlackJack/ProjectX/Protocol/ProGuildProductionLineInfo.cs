﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildProductionLineInfo")]
    public class ProGuildProductionLineInfo : IExtensible
    {
        private ulong _LineId;
        private int _ProduceTempleteId;
        private int _ProduceCount;
        private int _SolarSystemId;
        private ulong _FactoryId;
        private int _LineLevel;
        private long _StartTime;
        private long _EndTime;
        private int _SpeedUpReduceTime;
        private readonly List<ProCostInfo> _CostList;
        private ulong _TradeMoneyCost;
        private ulong _LostReporId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LineId;
        private static DelegateBridge __Hotfix_set_LineId;
        private static DelegateBridge __Hotfix_get_ProduceTempleteId;
        private static DelegateBridge __Hotfix_set_ProduceTempleteId;
        private static DelegateBridge __Hotfix_get_ProduceCount;
        private static DelegateBridge __Hotfix_set_ProduceCount;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_FactoryId;
        private static DelegateBridge __Hotfix_set_FactoryId;
        private static DelegateBridge __Hotfix_get_LineLevel;
        private static DelegateBridge __Hotfix_set_LineLevel;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_EndTime;
        private static DelegateBridge __Hotfix_set_EndTime;
        private static DelegateBridge __Hotfix_get_SpeedUpReduceTime;
        private static DelegateBridge __Hotfix_set_SpeedUpReduceTime;
        private static DelegateBridge __Hotfix_get_CostList;
        private static DelegateBridge __Hotfix_get_TradeMoneyCost;
        private static DelegateBridge __Hotfix_set_TradeMoneyCost;
        private static DelegateBridge __Hotfix_get_LostReporId;
        private static DelegateBridge __Hotfix_set_LostReporId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LineId", DataFormat=DataFormat.TwosComplement)]
        public ulong LineId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ProduceTempleteId", DataFormat=DataFormat.TwosComplement)]
        public int ProduceTempleteId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ProduceCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ProduceCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="FactoryId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong FactoryId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="LineLevel", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int LineLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="EndTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="SpeedUpReduceTime", DataFormat=DataFormat.TwosComplement)]
        public int SpeedUpReduceTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, Name="CostList", DataFormat=DataFormat.Default)]
        public List<ProCostInfo> CostList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="TradeMoneyCost", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong TradeMoneyCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(12, IsRequired=false, Name="LostReporId", DataFormat=DataFormat.TwosComplement)]
        public ulong LostReporId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

