﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProFakeNpcInfo")]
    public class ProFakeNpcInfo : IExtensible
    {
        private int _GrandFaction;
        private int _CharAvatarId;
        private int _LastNameId;
        private int _FirstNameId;
        private int _CharLevel;
        private int _CharGender;
        private int _CharProfession;
        private int _GuildNameId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_CharAvatarId;
        private static DelegateBridge __Hotfix_set_CharAvatarId;
        private static DelegateBridge __Hotfix_get_LastNameId;
        private static DelegateBridge __Hotfix_set_LastNameId;
        private static DelegateBridge __Hotfix_get_FirstNameId;
        private static DelegateBridge __Hotfix_set_FirstNameId;
        private static DelegateBridge __Hotfix_get_CharLevel;
        private static DelegateBridge __Hotfix_set_CharLevel;
        private static DelegateBridge __Hotfix_get_CharGender;
        private static DelegateBridge __Hotfix_set_CharGender;
        private static DelegateBridge __Hotfix_get_CharProfession;
        private static DelegateBridge __Hotfix_set_CharProfession;
        private static DelegateBridge __Hotfix_get_GuildNameId;
        private static DelegateBridge __Hotfix_set_GuildNameId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="GrandFaction", DataFormat=DataFormat.TwosComplement)]
        public int GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="CharAvatarId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CharAvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="LastNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int LastNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="FirstNameId", DataFormat=DataFormat.TwosComplement)]
        public int FirstNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="CharLevel", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CharLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="CharGender", DataFormat=DataFormat.TwosComplement)]
        public int CharGender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(7, IsRequired=false, Name="CharProfession", DataFormat=DataFormat.TwosComplement)]
        public int CharProfession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="GuildNameId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GuildNameId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

