﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ChatBannedNtf")]
    public class ChatBannedNtf : IExtensible
    {
        private long _BannedTime;
        private string _BanReason;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BannedTime;
        private static DelegateBridge __Hotfix_set_BannedTime;
        private static DelegateBridge __Hotfix_get_BanReason;
        private static DelegateBridge __Hotfix_set_BanReason;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BannedTime", DataFormat=DataFormat.TwosComplement)]
        public long BannedTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="BanReason", DataFormat=DataFormat.Default), DefaultValue("")]
        public string BanReason
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

