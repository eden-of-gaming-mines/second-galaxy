﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceSignalResultInfo")]
    public class ProSpaceSignalResultInfo : IExtensible
    {
        private int _SignalType;
        private long _ExpiryTime;
        private readonly List<ProSignalInfo> _SignalInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SignalType;
        private static DelegateBridge __Hotfix_set_SignalType;
        private static DelegateBridge __Hotfix_get_ExpiryTime;
        private static DelegateBridge __Hotfix_set_ExpiryTime;
        private static DelegateBridge __Hotfix_get_SignalInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SignalType", DataFormat=DataFormat.TwosComplement)]
        public int SignalType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="ExpiryTime", DataFormat=DataFormat.TwosComplement)]
        public long ExpiryTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="SignalInfoList", DataFormat=DataFormat.Default)]
        public List<ProSignalInfo> SignalInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

