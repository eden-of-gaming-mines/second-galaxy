﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildSolarSystemBasicInfoAck")]
    public class GuildSolarSystemBasicInfoAck : IExtensible
    {
        private int _Result;
        private int _SolarSystemId;
        private bool _HasStaticData;
        private uint _OccupiedGuildId;
        private string _OccupiedGuildName;
        private string _OccupiedGuildCodeName;
        private ProGuildLogoInfo _OccupiedGuildLogoInfo;
        private string _OccupiedGuildManifesto;
        private long _NextSovereignBattleStartMinTime;
        private int _CurrDynamicDataVersion;
        private int _FlourishValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_HasStaticData;
        private static DelegateBridge __Hotfix_set_HasStaticData;
        private static DelegateBridge __Hotfix_get_OccupiedGuildId;
        private static DelegateBridge __Hotfix_set_OccupiedGuildId;
        private static DelegateBridge __Hotfix_get_OccupiedGuildName;
        private static DelegateBridge __Hotfix_set_OccupiedGuildName;
        private static DelegateBridge __Hotfix_get_OccupiedGuildCodeName;
        private static DelegateBridge __Hotfix_set_OccupiedGuildCodeName;
        private static DelegateBridge __Hotfix_get_OccupiedGuildLogoInfo;
        private static DelegateBridge __Hotfix_set_OccupiedGuildLogoInfo;
        private static DelegateBridge __Hotfix_get_OccupiedGuildManifesto;
        private static DelegateBridge __Hotfix_set_OccupiedGuildManifesto;
        private static DelegateBridge __Hotfix_get_NextSovereignBattleStartMinTime;
        private static DelegateBridge __Hotfix_set_NextSovereignBattleStartMinTime;
        private static DelegateBridge __Hotfix_get_CurrDynamicDataVersion;
        private static DelegateBridge __Hotfix_set_CurrDynamicDataVersion;
        private static DelegateBridge __Hotfix_get_FlourishValue;
        private static DelegateBridge __Hotfix_set_FlourishValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(3, IsRequired=false, Name="HasStaticData", DataFormat=DataFormat.Default)]
        public bool HasStaticData
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="OccupiedGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint OccupiedGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="OccupiedGuildName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OccupiedGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="OccupiedGuildCodeName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OccupiedGuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="OccupiedGuildLogoInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildLogoInfo OccupiedGuildLogoInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="OccupiedGuildManifesto", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OccupiedGuildManifesto
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="NextSovereignBattleStartMinTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextSovereignBattleStartMinTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="CurrDynamicDataVersion", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrDynamicDataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(11, IsRequired=false, Name="FlourishValue", DataFormat=DataFormat.TwosComplement)]
        public int FlourishValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

