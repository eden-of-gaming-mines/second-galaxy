﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerLevelMiscNtf")]
    public class DSPlayerLevelMiscNtf : IExtensible
    {
        private uint _Version;
        private int _Level;
        private uint _ExpCurr;
        private ulong _BindMoney;
        private ulong _TradeMoney;
        private ulong _RealMoney;
        private long _DailyExpAchieved;
        private long _AFKExp;
        private readonly List<ProCreditCurrencyInfo> _CreditCurrencyList;
        private ulong _PanGala;
        private ulong _GuildGala;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_ExpCurr;
        private static DelegateBridge __Hotfix_set_ExpCurr;
        private static DelegateBridge __Hotfix_get_BindMoney;
        private static DelegateBridge __Hotfix_set_BindMoney;
        private static DelegateBridge __Hotfix_get_TradeMoney;
        private static DelegateBridge __Hotfix_set_TradeMoney;
        private static DelegateBridge __Hotfix_get_RealMoney;
        private static DelegateBridge __Hotfix_set_RealMoney;
        private static DelegateBridge __Hotfix_get_DailyExpAchieved;
        private static DelegateBridge __Hotfix_set_DailyExpAchieved;
        private static DelegateBridge __Hotfix_get_AFKExp;
        private static DelegateBridge __Hotfix_set_AFKExp;
        private static DelegateBridge __Hotfix_get_CreditCurrencyList;
        private static DelegateBridge __Hotfix_get_PanGala;
        private static DelegateBridge __Hotfix_set_PanGala;
        private static DelegateBridge __Hotfix_get_GuildGala;
        private static DelegateBridge __Hotfix_set_GuildGala;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ExpCurr", DataFormat=DataFormat.TwosComplement)]
        public uint ExpCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="BindMoney", DataFormat=DataFormat.TwosComplement)]
        public ulong BindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="TradeMoney", DataFormat=DataFormat.TwosComplement)]
        public ulong TradeMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="RealMoney", DataFormat=DataFormat.TwosComplement)]
        public ulong RealMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="DailyExpAchieved", DataFormat=DataFormat.TwosComplement)]
        public long DailyExpAchieved
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="AFKExp", DataFormat=DataFormat.TwosComplement)]
        public long AFKExp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="CreditCurrencyList", DataFormat=DataFormat.Default)]
        public List<ProCreditCurrencyInfo> CreditCurrencyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="PanGala", DataFormat=DataFormat.TwosComplement)]
        public ulong PanGala
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="GuildGala", DataFormat=DataFormat.TwosComplement)]
        public ulong GuildGala
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

