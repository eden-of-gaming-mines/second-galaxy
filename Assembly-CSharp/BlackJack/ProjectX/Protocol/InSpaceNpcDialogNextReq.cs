﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceNpcDialogNextReq")]
    public class InSpaceNpcDialogNextReq : IExtensible
    {
        private uint _SceneInstanceId;
        private int _NextDialogId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SceneInstanceId;
        private static DelegateBridge __Hotfix_set_SceneInstanceId;
        private static DelegateBridge __Hotfix_get_NextDialogId;
        private static DelegateBridge __Hotfix_set_NextDialogId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="SceneInstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint SceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="NextDialogId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int NextDialogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

