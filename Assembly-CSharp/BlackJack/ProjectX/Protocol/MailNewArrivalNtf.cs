﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="MailNewArrivalNtf")]
    public class MailNewArrivalNtf : IExtensible
    {
        private readonly List<ProStoredMailInfo> _NewMailInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_NewMailInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="NewMailInfo", DataFormat=DataFormat.Default)]
        public List<ProStoredMailInfo> NewMailInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

