﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DevelopmentProjectStartReq")]
    public class DevelopmentProjectStartReq : IExtensible
    {
        private int _ProjectID;
        private readonly List<ProItemInfo> _itemInfos;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ProjectID;
        private static DelegateBridge __Hotfix_set_ProjectID;
        private static DelegateBridge __Hotfix_get_ItemInfos;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ProjectID", DataFormat=DataFormat.TwosComplement)]
        public int ProjectID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="itemInfos", DataFormat=DataFormat.Default)]
        public List<ProItemInfo> ItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

