﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProStarMapGuildOccupyDynamicInfo")]
    public class ProStarMapGuildOccupyDynamicInfo : IExtensible
    {
        private int _StarFieldId;
        private readonly List<ProSolarSystemDynamicInfo> _SolarSystemInfo;
        private int _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StarFieldId;
        private static DelegateBridge __Hotfix_set_StarFieldId;
        private static DelegateBridge __Hotfix_get_SolarSystemInfo;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="StarFieldId", DataFormat=DataFormat.TwosComplement)]
        public int StarFieldId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="SolarSystemInfo", DataFormat=DataFormat.Default)]
        public List<ProSolarSystemDynamicInfo> SolarSystemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

