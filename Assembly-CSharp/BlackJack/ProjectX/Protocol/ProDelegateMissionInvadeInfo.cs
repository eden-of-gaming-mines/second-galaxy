﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProDelegateMissionInvadeInfo")]
    public class ProDelegateMissionInvadeInfo : IExtensible
    {
        private ProPlayerSimpleInfo _PlayerInfo;
        private long _StartTime;
        private long _EndTime;
        private readonly List<ulong> _DestroyCaptainInstanceIdList;
        private readonly List<ProSimpleItemInfoWithCount> _SimpleItemInfoWithCountList;
        private readonly List<ProDelegateMissionInvadeEventInfo> _EventInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerInfo;
        private static DelegateBridge __Hotfix_set_PlayerInfo;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_EndTime;
        private static DelegateBridge __Hotfix_set_EndTime;
        private static DelegateBridge __Hotfix_get_DestroyCaptainInstanceIdList;
        private static DelegateBridge __Hotfix_get_SimpleItemInfoWithCountList;
        private static DelegateBridge __Hotfix_get_EventInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="PlayerInfo", DataFormat=DataFormat.Default)]
        public ProPlayerSimpleInfo PlayerInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public long StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="EndTime", DataFormat=DataFormat.TwosComplement)]
        public long EndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="DestroyCaptainInstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> DestroyCaptainInstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="SimpleItemInfoWithCountList", DataFormat=DataFormat.Default)]
        public List<ProSimpleItemInfoWithCount> SimpleItemInfoWithCountList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="EventInfoList", DataFormat=DataFormat.Default)]
        public List<ProDelegateMissionInvadeEventInfo> EventInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

