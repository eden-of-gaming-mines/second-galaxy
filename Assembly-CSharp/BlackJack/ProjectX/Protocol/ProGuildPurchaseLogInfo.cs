﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildPurchaseLogInfo")]
    public class ProGuildPurchaseLogInfo : IExtensible
    {
        private int _Type;
        private int _OperatorJob;
        private string _OperatorName;
        private long _RestCount;
        private long _AddUpCount;
        private long _Time;
        private long _CompletedCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_OperatorJob;
        private static DelegateBridge __Hotfix_set_OperatorJob;
        private static DelegateBridge __Hotfix_get_OperatorName;
        private static DelegateBridge __Hotfix_set_OperatorName;
        private static DelegateBridge __Hotfix_get_RestCount;
        private static DelegateBridge __Hotfix_set_RestCount;
        private static DelegateBridge __Hotfix_get_AddUpCount;
        private static DelegateBridge __Hotfix_set_AddUpCount;
        private static DelegateBridge __Hotfix_get_Time;
        private static DelegateBridge __Hotfix_set_Time;
        private static DelegateBridge __Hotfix_get_CompletedCount;
        private static DelegateBridge __Hotfix_set_CompletedCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Type", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="OperatorJob", DataFormat=DataFormat.TwosComplement)]
        public int OperatorJob
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="OperatorName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string OperatorName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="RestCount", DataFormat=DataFormat.TwosComplement)]
        public long RestCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="AddUpCount", DataFormat=DataFormat.TwosComplement)]
        public long AddUpCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="Time", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="CompletedCount", DataFormat=DataFormat.TwosComplement)]
        public long CompletedCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

