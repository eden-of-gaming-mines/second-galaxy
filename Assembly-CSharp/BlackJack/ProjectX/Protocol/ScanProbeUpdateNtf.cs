﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ScanProbeUpdateNtf")]
    public class ScanProbeUpdateNtf : IExtensible
    {
        private int _ProbeType;
        private int _AddCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ProbeType;
        private static DelegateBridge __Hotfix_set_ProbeType;
        private static DelegateBridge __Hotfix_get_AddCount;
        private static DelegateBridge __Hotfix_set_AddCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="ProbeType", DataFormat=DataFormat.TwosComplement)]
        public int ProbeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="AddCount", DataFormat=DataFormat.TwosComplement)]
        public int AddCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

