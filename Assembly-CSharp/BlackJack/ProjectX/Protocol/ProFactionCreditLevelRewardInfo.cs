﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProFactionCreditLevelRewardInfo")]
    public class ProFactionCreditLevelRewardInfo : IExtensible
    {
        private int _FactionId;
        private readonly List<ProFactionCreditLevelRewardInfoSub> _RewardInfoList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FactionId;
        private static DelegateBridge __Hotfix_set_FactionId;
        private static DelegateBridge __Hotfix_get_RewardInfoList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FactionId", DataFormat=DataFormat.TwosComplement)]
        public int FactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="RewardInfoList", DataFormat=DataFormat.Default)]
        public List<ProFactionCreditLevelRewardInfoSub> RewardInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

