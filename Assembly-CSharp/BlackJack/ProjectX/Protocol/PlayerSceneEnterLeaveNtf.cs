﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerSceneEnterLeaveNtf")]
    public class PlayerSceneEnterLeaveNtf : IExtensible
    {
        private bool _EnterScene;
        private int _SceneId;
        private uint _SceneInstanceId;
        private int _RelativeQuestInstanceId;
        private int _RelativeQuestId;
        private int _SolarSystemId;
        private int _RelativeSignalId;
        private bool _PVPEnable;
        private readonly List<int> _SceneRelativeContextParam;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EnterScene;
        private static DelegateBridge __Hotfix_set_EnterScene;
        private static DelegateBridge __Hotfix_get_SceneId;
        private static DelegateBridge __Hotfix_set_SceneId;
        private static DelegateBridge __Hotfix_get_SceneInstanceId;
        private static DelegateBridge __Hotfix_set_SceneInstanceId;
        private static DelegateBridge __Hotfix_get_RelativeQuestInstanceId;
        private static DelegateBridge __Hotfix_set_RelativeQuestInstanceId;
        private static DelegateBridge __Hotfix_get_RelativeQuestId;
        private static DelegateBridge __Hotfix_set_RelativeQuestId;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_RelativeSignalId;
        private static DelegateBridge __Hotfix_set_RelativeSignalId;
        private static DelegateBridge __Hotfix_get_PVPEnable;
        private static DelegateBridge __Hotfix_set_PVPEnable;
        private static DelegateBridge __Hotfix_get_SceneRelativeContextParam;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="EnterScene", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool EnterScene
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="SceneId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SceneId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="SceneInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint SceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="RelativeQuestInstanceId", DataFormat=DataFormat.TwosComplement)]
        public int RelativeQuestInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="RelativeQuestId", DataFormat=DataFormat.TwosComplement)]
        public int RelativeQuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(7, IsRequired=false, Name="RelativeSignalId", DataFormat=DataFormat.TwosComplement)]
        public int RelativeSignalId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="PVPEnable", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool PVPEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, Name="SceneRelativeContextParam", DataFormat=DataFormat.TwosComplement)]
        public List<int> SceneRelativeContextParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

