﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="PlayerSimpleInfoGetReq")]
    public class PlayerSimpleInfoGetReq : IExtensible
    {
        private readonly List<string> _GameUserIdList;
        private readonly List<int> _VersionList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GameUserIdList;
        private static DelegateBridge __Hotfix_get_VersionList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="GameUserIdList", DataFormat=DataFormat.Default)]
        public List<string> GameUserIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, Name="VersionList", DataFormat=DataFormat.TwosComplement)]
        public List<int> VersionList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

