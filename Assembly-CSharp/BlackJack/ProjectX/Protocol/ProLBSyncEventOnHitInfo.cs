﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventOnHitInfo")]
    public class ProLBSyncEventOnHitInfo : IExtensible
    {
        private float _Damage;
        private uint _Flag;
        private uint _SrcObjId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Damage;
        private static DelegateBridge __Hotfix_set_Damage;
        private static DelegateBridge __Hotfix_get_Flag;
        private static DelegateBridge __Hotfix_set_Flag;
        private static DelegateBridge __Hotfix_get_SrcObjId;
        private static DelegateBridge __Hotfix_set_SrcObjId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="Damage", DataFormat=DataFormat.FixedSize)]
        public float Damage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="Flag", DataFormat=DataFormat.TwosComplement)]
        public uint Flag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="SrcObjId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint SrcObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

