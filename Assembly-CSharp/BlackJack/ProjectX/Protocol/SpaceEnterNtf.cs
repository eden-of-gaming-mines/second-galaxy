﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SpaceEnterNtf")]
    public class SpaceEnterNtf : IExtensible
    {
        private uint _SolarSystemTickTime;
        private int _SolarySystemId;
        private ulong _ShipInstanceId;
        private uint _ObjId;
        private bool _IsAutoFight;
        private int _Shield;
        private int _Armor;
        private int _Energy;
        private float _SuperWeaponEnergy;
        private readonly List<int> _StaticBufList;
        private readonly List<ProBufInfo> _DynamicBufList;
        private readonly List<uint> _HighSlotWeaponReadyForLaunchTime;
        private readonly List<uint> _EquipGroupReadyForLaunchTime;
        private bool _IsWingShipEnable;
        private uint _LastWingShipOpTime;
        private uint _CurrWingShipObjId;
        private readonly List<ProHateTargetInfo> _HateTargetList;
        private ProShipMoveStateSnapshot _SelfShipMoveStateSnapshot;
        private bool _IsInfect;
        private bool _IsInvisible;
        private readonly List<ProShipStoreItemInfo> _ShipStoreItem;
        private bool _IsFlagShip;
        private ProFlagShipInitExtraInfo _FlagShipInfo;
        private ProPlayerPVPInfo _PlayerPVPInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_set_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_get_SolarySystemId;
        private static DelegateBridge __Hotfix_set_SolarySystemId;
        private static DelegateBridge __Hotfix_get_ShipInstanceId;
        private static DelegateBridge __Hotfix_set_ShipInstanceId;
        private static DelegateBridge __Hotfix_get_ObjId;
        private static DelegateBridge __Hotfix_set_ObjId;
        private static DelegateBridge __Hotfix_get_IsAutoFight;
        private static DelegateBridge __Hotfix_set_IsAutoFight;
        private static DelegateBridge __Hotfix_get_Shield;
        private static DelegateBridge __Hotfix_set_Shield;
        private static DelegateBridge __Hotfix_get_Armor;
        private static DelegateBridge __Hotfix_set_Armor;
        private static DelegateBridge __Hotfix_get_Energy;
        private static DelegateBridge __Hotfix_set_Energy;
        private static DelegateBridge __Hotfix_get_SuperWeaponEnergy;
        private static DelegateBridge __Hotfix_set_SuperWeaponEnergy;
        private static DelegateBridge __Hotfix_get_StaticBufList;
        private static DelegateBridge __Hotfix_get_DynamicBufList;
        private static DelegateBridge __Hotfix_get_HighSlotWeaponReadyForLaunchTime;
        private static DelegateBridge __Hotfix_get_EquipGroupReadyForLaunchTime;
        private static DelegateBridge __Hotfix_get_IsWingShipEnable;
        private static DelegateBridge __Hotfix_set_IsWingShipEnable;
        private static DelegateBridge __Hotfix_get_LastWingShipOpTime;
        private static DelegateBridge __Hotfix_set_LastWingShipOpTime;
        private static DelegateBridge __Hotfix_get_CurrWingShipObjId;
        private static DelegateBridge __Hotfix_set_CurrWingShipObjId;
        private static DelegateBridge __Hotfix_get_HateTargetList;
        private static DelegateBridge __Hotfix_get_SelfShipMoveStateSnapshot;
        private static DelegateBridge __Hotfix_set_SelfShipMoveStateSnapshot;
        private static DelegateBridge __Hotfix_get_IsInfect;
        private static DelegateBridge __Hotfix_set_IsInfect;
        private static DelegateBridge __Hotfix_get_IsInvisible;
        private static DelegateBridge __Hotfix_set_IsInvisible;
        private static DelegateBridge __Hotfix_get_ShipStoreItem;
        private static DelegateBridge __Hotfix_get_IsFlagShip;
        private static DelegateBridge __Hotfix_set_IsFlagShip;
        private static DelegateBridge __Hotfix_get_FlagShipInfo;
        private static DelegateBridge __Hotfix_set_FlagShipInfo;
        private static DelegateBridge __Hotfix_get_PlayerPVPInfo;
        private static DelegateBridge __Hotfix_set_PlayerPVPInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemTickTime", DataFormat=DataFormat.TwosComplement)]
        public uint SolarSystemTickTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarySystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarySystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ObjId", DataFormat=DataFormat.TwosComplement)]
        public uint ObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(7, IsRequired=false, Name="IsAutoFight", DataFormat=DataFormat.Default)]
        public bool IsAutoFight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="Shield", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Shield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(9, IsRequired=false, Name="Armor", DataFormat=DataFormat.TwosComplement)]
        public int Armor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(10, IsRequired=false, Name="Energy", DataFormat=DataFormat.TwosComplement)]
        public int Energy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(11, IsRequired=false, Name="SuperWeaponEnergy", DataFormat=DataFormat.FixedSize)]
        public float SuperWeaponEnergy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, Name="StaticBufList", DataFormat=DataFormat.TwosComplement)]
        public List<int> StaticBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(13, Name="DynamicBufList", DataFormat=DataFormat.Default)]
        public List<ProBufInfo> DynamicBufList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(14, Name="HighSlotWeaponReadyForLaunchTime", DataFormat=DataFormat.TwosComplement)]
        public List<uint> HighSlotWeaponReadyForLaunchTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(15, Name="EquipGroupReadyForLaunchTime", DataFormat=DataFormat.TwosComplement)]
        public List<uint> EquipGroupReadyForLaunchTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="IsWingShipEnable", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsWingShipEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(0x11, IsRequired=false, Name="LastWingShipOpTime", DataFormat=DataFormat.TwosComplement)]
        public uint LastWingShipOpTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x12, IsRequired=false, Name="CurrWingShipObjId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint CurrWingShipObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x13, Name="HateTargetList", DataFormat=DataFormat.Default)]
        public List<ProHateTargetInfo> HateTargetList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(20, IsRequired=false, Name="SelfShipMoveStateSnapshot", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProShipMoveStateSnapshot SelfShipMoveStateSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=false, Name="IsInfect", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsInfect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(0x16, IsRequired=false, Name="IsInvisible", DataFormat=DataFormat.Default)]
        public bool IsInvisible
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x17, Name="ShipStoreItem", DataFormat=DataFormat.Default)]
        public List<ProShipStoreItemInfo> ShipStoreItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(0x18, IsRequired=true, Name="IsFlagShip", DataFormat=DataFormat.Default)]
        public bool IsFlagShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x19, IsRequired=false, Name="FlagShipInfo", DataFormat=DataFormat.Default)]
        public ProFlagShipInitExtraInfo FlagShipInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(0x1a, IsRequired=false, Name="PlayerPVPInfo", DataFormat=DataFormat.Default)]
        public ProPlayerPVPInfo PlayerPVPInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

