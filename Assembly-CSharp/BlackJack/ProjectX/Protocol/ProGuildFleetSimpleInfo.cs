﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFleetSimpleInfo")]
    public class ProGuildFleetSimpleInfo : IExtensible
    {
        private ulong _FleetId;
        private string _FleetName;
        private int _FleetMemberCount;
        private string _CommanderGameUserId;
        private string _CommanderName;
        private int _AvatarId;
        private string _FireControllerGameUserId;
        private string _NavigatorGameUserId;
        private int _FleetSeqId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FleetId;
        private static DelegateBridge __Hotfix_set_FleetId;
        private static DelegateBridge __Hotfix_get_FleetName;
        private static DelegateBridge __Hotfix_set_FleetName;
        private static DelegateBridge __Hotfix_get_FleetMemberCount;
        private static DelegateBridge __Hotfix_set_FleetMemberCount;
        private static DelegateBridge __Hotfix_get_CommanderGameUserId;
        private static DelegateBridge __Hotfix_set_CommanderGameUserId;
        private static DelegateBridge __Hotfix_get_CommanderName;
        private static DelegateBridge __Hotfix_set_CommanderName;
        private static DelegateBridge __Hotfix_get_AvatarId;
        private static DelegateBridge __Hotfix_set_AvatarId;
        private static DelegateBridge __Hotfix_get_FireControllerGameUserId;
        private static DelegateBridge __Hotfix_set_FireControllerGameUserId;
        private static DelegateBridge __Hotfix_get_NavigatorGameUserId;
        private static DelegateBridge __Hotfix_set_NavigatorGameUserId;
        private static DelegateBridge __Hotfix_get_FleetSeqId;
        private static DelegateBridge __Hotfix_set_FleetSeqId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FleetId", DataFormat=DataFormat.TwosComplement)]
        public ulong FleetId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="FleetName", DataFormat=DataFormat.Default)]
        public string FleetName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="FleetMemberCount", DataFormat=DataFormat.TwosComplement)]
        public int FleetMemberCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(4, IsRequired=false, Name="CommanderGameUserId", DataFormat=DataFormat.Default)]
        public string CommanderGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(5, IsRequired=false, Name="CommanderName", DataFormat=DataFormat.Default)]
        public string CommanderName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="AvatarId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="FireControllerGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string FireControllerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(8, IsRequired=false, Name="NavigatorGameUserId", DataFormat=DataFormat.Default)]
        public string NavigatorGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="FleetSeqId", DataFormat=DataFormat.TwosComplement)]
        public int FleetSeqId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

