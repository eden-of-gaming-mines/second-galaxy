﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CharSkillLearnReq")]
    public class CharSkillLearnReq : IExtensible
    {
        private int _SkillId;
        private int _SkillLevel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SkillId;
        private static DelegateBridge __Hotfix_set_SkillId;
        private static DelegateBridge __Hotfix_get_SkillLevel;
        private static DelegateBridge __Hotfix_set_SkillLevel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SkillId", DataFormat=DataFormat.TwosComplement)]
        public int SkillId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SkillLevel", DataFormat=DataFormat.TwosComplement)]
        public int SkillLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

