﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ChatInWisperReq")]
    public class ChatInWisperReq : IExtensible
    {
        private ProChatInfo _ChatInfo;
        private ProChatCententInfo _ChatContent;
        private string _DestGameUserId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ChatInfo;
        private static DelegateBridge __Hotfix_set_ChatInfo;
        private static DelegateBridge __Hotfix_get_ChatContent;
        private static DelegateBridge __Hotfix_set_ChatContent;
        private static DelegateBridge __Hotfix_get_DestGameUserId;
        private static DelegateBridge __Hotfix_set_DestGameUserId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ChatInfo", DataFormat=DataFormat.Default)]
        public ProChatInfo ChatInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ChatContent", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProChatCententInfo ChatContent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="DestGameUserId", DataFormat=DataFormat.Default)]
        public string DestGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

