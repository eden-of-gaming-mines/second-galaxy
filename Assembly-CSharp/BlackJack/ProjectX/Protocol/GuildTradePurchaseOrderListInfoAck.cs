﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildTradePurchaseOrderListInfoAck")]
    public class GuildTradePurchaseOrderListInfoAck : IExtensible
    {
        private int _Result;
        private int _ConfigId;
        private int _ItemType;
        private readonly List<ProGuildTradePurchaseInfo> _GuildTradePurchaseInfoList;
        private readonly List<ulong> _RemoveList;
        private bool _IsRefreshList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_set_ConfigId;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_GuildTradePurchaseInfoList;
        private static DelegateBridge __Hotfix_get_RemoveList;
        private static DelegateBridge __Hotfix_get_IsRefreshList;
        private static DelegateBridge __Hotfix_set_IsRefreshList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ConfigId", DataFormat=DataFormat.TwosComplement)]
        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ItemType", DataFormat=DataFormat.TwosComplement)]
        public int ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="GuildTradePurchaseInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildTradePurchaseInfo> GuildTradePurchaseInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="RemoveList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> RemoveList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="IsRefreshList", DataFormat=DataFormat.Default)]
        public bool IsRefreshList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

