﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildSentrySceneInfo")]
    public class ProGuildSentrySceneInfo : IExtensible
    {
        private ulong _InstanceId;
        private uint _SceneInstanceId;
        private int _SceneConfigId;
        private int _TotalThreatValue;
        private int _ShipCount;
        private uint _TrackingDeviationDistance;
        private int _QuestId;
        private int _SignalId;
        private int _BuildingType;
        private ProVectorDouble _Location;
        private readonly List<ProGuildSentryPlayerInfo> _GuildSentryPlayerInfoList;
        private readonly List<ProGuildSentryInterestSceneShipTypeInfo> _ShipTypeList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_SceneInstanceId;
        private static DelegateBridge __Hotfix_set_SceneInstanceId;
        private static DelegateBridge __Hotfix_get_SceneConfigId;
        private static DelegateBridge __Hotfix_set_SceneConfigId;
        private static DelegateBridge __Hotfix_get_TotalThreatValue;
        private static DelegateBridge __Hotfix_set_TotalThreatValue;
        private static DelegateBridge __Hotfix_get_ShipCount;
        private static DelegateBridge __Hotfix_set_ShipCount;
        private static DelegateBridge __Hotfix_get_TrackingDeviationDistance;
        private static DelegateBridge __Hotfix_set_TrackingDeviationDistance;
        private static DelegateBridge __Hotfix_get_QuestId;
        private static DelegateBridge __Hotfix_set_QuestId;
        private static DelegateBridge __Hotfix_get_SignalId;
        private static DelegateBridge __Hotfix_set_SignalId;
        private static DelegateBridge __Hotfix_get_BuildingType;
        private static DelegateBridge __Hotfix_set_BuildingType;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_get_GuildSentryPlayerInfoList;
        private static DelegateBridge __Hotfix_get_ShipTypeList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SceneInstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint SceneInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SceneConfigId", DataFormat=DataFormat.TwosComplement)]
        public int SceneConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="TotalThreatValue", DataFormat=DataFormat.TwosComplement)]
        public int TotalThreatValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="ShipCount", DataFormat=DataFormat.TwosComplement)]
        public int ShipCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="TrackingDeviationDistance", DataFormat=DataFormat.TwosComplement)]
        public uint TrackingDeviationDistance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="QuestId", DataFormat=DataFormat.TwosComplement)]
        public int QuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="SignalId", DataFormat=DataFormat.TwosComplement)]
        public int SignalId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="BuildingType", DataFormat=DataFormat.TwosComplement)]
        public int BuildingType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="Location", DataFormat=DataFormat.Default)]
        public ProVectorDouble Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="GuildSentryPlayerInfoList", DataFormat=DataFormat.Default)]
        public List<ProGuildSentryPlayerInfo> GuildSentryPlayerInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, Name="ShipTypeList", DataFormat=DataFormat.Default)]
        public List<ProGuildSentryInterestSceneShipTypeInfo> ShipTypeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

