﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProFlagShipInitExtraInfo")]
    public class ProFlagShipInitExtraInfo : IExtensible
    {
        private int _FlagShipConfId;
        private string _FlagShipName;
        private int _FlagShipState;
        private readonly List<ProShipSlotGroupInfo> _FlagShipLowSlotList;
        private uint _FuelCurr;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FlagShipConfId;
        private static DelegateBridge __Hotfix_set_FlagShipConfId;
        private static DelegateBridge __Hotfix_get_FlagShipName;
        private static DelegateBridge __Hotfix_set_FlagShipName;
        private static DelegateBridge __Hotfix_get_FlagShipState;
        private static DelegateBridge __Hotfix_set_FlagShipState;
        private static DelegateBridge __Hotfix_get_FlagShipLowSlotList;
        private static DelegateBridge __Hotfix_get_FuelCurr;
        private static DelegateBridge __Hotfix_set_FuelCurr;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FlagShipConfId", DataFormat=DataFormat.TwosComplement)]
        public int FlagShipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="FlagShipName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string FlagShipName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="FlagShipState", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int FlagShipState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="FlagShipLowSlotList", DataFormat=DataFormat.Default)]
        public List<ProShipSlotGroupInfo> FlagShipLowSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="FuelCurr", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint FuelCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

