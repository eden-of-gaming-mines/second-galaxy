﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMServerAllServerAnnouncementSendReq")]
    public class GMServerAllServerAnnouncementSendReq : IExtensible
    {
        private string _SendTime;
        private int _SendCount;
        private string _AnnouncementStr;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SendTime;
        private static DelegateBridge __Hotfix_set_SendTime;
        private static DelegateBridge __Hotfix_get_SendCount;
        private static DelegateBridge __Hotfix_set_SendCount;
        private static DelegateBridge __Hotfix_get_AnnouncementStr;
        private static DelegateBridge __Hotfix_set_AnnouncementStr;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SendTime", DataFormat=DataFormat.Default)]
        public string SendTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SendCount", DataFormat=DataFormat.TwosComplement)]
        public int SendCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="AnnouncementStr", DataFormat=DataFormat.Default)]
        public string AnnouncementStr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

