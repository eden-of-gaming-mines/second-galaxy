﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSPlayerSkillNtf")]
    public class DSPlayerSkillNtf : IExtensible
    {
        private uint _Version;
        private int _FreeSkillPoint;
        private readonly List<ProIdLevelInfo> _PassiveSkillList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_FreeSkillPoint;
        private static DelegateBridge __Hotfix_set_FreeSkillPoint;
        private static DelegateBridge __Hotfix_get_PassiveSkillList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="FreeSkillPoint", DataFormat=DataFormat.TwosComplement)]
        public int FreeSkillPoint
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="PassiveSkillList", DataFormat=DataFormat.Default)]
        public List<ProIdLevelInfo> PassiveSkillList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

