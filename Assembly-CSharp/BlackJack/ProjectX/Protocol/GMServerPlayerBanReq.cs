﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMServerPlayerBanReq")]
    public class GMServerPlayerBanReq : IExtensible
    {
        private readonly List<string> _PlayerNameList;
        private int _BanDay;
        private bool _IsBan;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerNameList;
        private static DelegateBridge __Hotfix_get_BanDay;
        private static DelegateBridge __Hotfix_set_BanDay;
        private static DelegateBridge __Hotfix_get_IsBan;
        private static DelegateBridge __Hotfix_set_IsBan;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="PlayerNameList", DataFormat=DataFormat.Default)]
        public List<string> PlayerNameList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="BanDay", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int BanDay
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="IsBan", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsBan
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

