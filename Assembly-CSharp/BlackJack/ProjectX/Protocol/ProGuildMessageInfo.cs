﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildMessageInfo")]
    public class ProGuildMessageInfo : IExtensible
    {
        private ulong _InstanceId;
        private string _PlayerUserId;
        private string _PlayerName;
        private uint _PlayerGuildId;
        private string _Content;
        private long _Time;
        private bool _Delete;
        private string _DeleteAdminName;
        private string _DeleteAdminGameUserId;
        private ProGuildSimplestInfo _PlayerGuildInfo;
        private ProPlayerSimplestInfo _PlayerSimpleInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_PlayerUserId;
        private static DelegateBridge __Hotfix_set_PlayerUserId;
        private static DelegateBridge __Hotfix_get_PlayerName;
        private static DelegateBridge __Hotfix_set_PlayerName;
        private static DelegateBridge __Hotfix_get_PlayerGuildId;
        private static DelegateBridge __Hotfix_set_PlayerGuildId;
        private static DelegateBridge __Hotfix_get_Content;
        private static DelegateBridge __Hotfix_set_Content;
        private static DelegateBridge __Hotfix_get_Time;
        private static DelegateBridge __Hotfix_set_Time;
        private static DelegateBridge __Hotfix_get_Delete;
        private static DelegateBridge __Hotfix_set_Delete;
        private static DelegateBridge __Hotfix_get_DeleteAdminName;
        private static DelegateBridge __Hotfix_set_DeleteAdminName;
        private static DelegateBridge __Hotfix_get_DeleteAdminGameUserId;
        private static DelegateBridge __Hotfix_set_DeleteAdminGameUserId;
        private static DelegateBridge __Hotfix_get_PlayerGuildInfo;
        private static DelegateBridge __Hotfix_set_PlayerGuildInfo;
        private static DelegateBridge __Hotfix_get_PlayerSimpleInfo;
        private static DelegateBridge __Hotfix_set_PlayerSimpleInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="PlayerUserId", DataFormat=DataFormat.Default)]
        public string PlayerUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="PlayerName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="PlayerGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint PlayerGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(5, IsRequired=false, Name="Content", DataFormat=DataFormat.Default)]
        public string Content
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="Time", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long Time
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="Delete", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool Delete
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="DeleteAdminName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string DeleteAdminName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(9, IsRequired=false, Name="DeleteAdminGameUserId", DataFormat=DataFormat.Default)]
        public string DeleteAdminGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="PlayerGuildInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildSimplestInfo PlayerGuildInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="PlayerSimpleInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProPlayerSimplestInfo PlayerSimpleInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

