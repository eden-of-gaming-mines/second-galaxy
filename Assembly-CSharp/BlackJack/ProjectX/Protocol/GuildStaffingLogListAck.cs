﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildStaffingLogListAck")]
    public class GuildStaffingLogListAck : IExtensible
    {
        private int _Result;
        private uint _Version;
        private long _EarliestLogTime;
        private long _CacheLastLogTime;
        private readonly List<ProGuildStaffingLogInfo> _LogList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_EarliestLogTime;
        private static DelegateBridge __Hotfix_set_EarliestLogTime;
        private static DelegateBridge __Hotfix_get_CacheLastLogTime;
        private static DelegateBridge __Hotfix_set_CacheLastLogTime;
        private static DelegateBridge __Hotfix_get_LogList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="EarliestLogTime", DataFormat=DataFormat.TwosComplement)]
        public long EarliestLogTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CacheLastLogTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CacheLastLogTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="LogList", DataFormat=DataFormat.Default)]
        public List<ProGuildStaffingLogInfo> LogList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

