﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SpaceObjectSyncNtf")]
    public class SpaceObjectSyncNtf : IExtensible
    {
        private uint _SolarSystemTickTime;
        private ProShipMoveStateSnapshot _SelfShipMoveStateSnapshot;
        private ProSynEventList4ObjInfo _SelfSynEventList4Obj;
        private readonly List<ProSpaceObjectEnterInfo> _EnterList;
        private readonly List<uint> _LeaveList;
        private readonly List<ProSpaceObjectMoveStateSnapshot> _MoveStateUpdateList;
        private readonly List<ProSynEventList4ObjInfo> _SynEventList4ObjList;
        private bool _IsSubPackage;
        private bool _IsEndPackage;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_set_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_get_SelfShipMoveStateSnapshot;
        private static DelegateBridge __Hotfix_set_SelfShipMoveStateSnapshot;
        private static DelegateBridge __Hotfix_get_SelfSynEventList4Obj;
        private static DelegateBridge __Hotfix_set_SelfSynEventList4Obj;
        private static DelegateBridge __Hotfix_get_EnterList;
        private static DelegateBridge __Hotfix_get_LeaveList;
        private static DelegateBridge __Hotfix_get_MoveStateUpdateList;
        private static DelegateBridge __Hotfix_get_SynEventList4ObjList;
        private static DelegateBridge __Hotfix_get_IsSubPackage;
        private static DelegateBridge __Hotfix_set_IsSubPackage;
        private static DelegateBridge __Hotfix_get_IsEndPackage;
        private static DelegateBridge __Hotfix_set_IsEndPackage;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemTickTime", DataFormat=DataFormat.TwosComplement)]
        public uint SolarSystemTickTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="SelfShipMoveStateSnapshot", DataFormat=DataFormat.Default)]
        public ProShipMoveStateSnapshot SelfShipMoveStateSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="SelfSynEventList4Obj", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSynEventList4ObjInfo SelfSynEventList4Obj
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="EnterList", DataFormat=DataFormat.Default)]
        public List<ProSpaceObjectEnterInfo> EnterList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="LeaveList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> LeaveList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="MoveStateUpdateList", DataFormat=DataFormat.Default)]
        public List<ProSpaceObjectMoveStateSnapshot> MoveStateUpdateList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="SynEventList4ObjList", DataFormat=DataFormat.Default)]
        public List<ProSynEventList4ObjInfo> SynEventList4ObjList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue(false), ProtoMember(20, IsRequired=false, Name="IsSubPackage", DataFormat=DataFormat.Default)]
        public bool IsSubPackage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x15, IsRequired=false, Name="IsEndPackage", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsEndPackage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

