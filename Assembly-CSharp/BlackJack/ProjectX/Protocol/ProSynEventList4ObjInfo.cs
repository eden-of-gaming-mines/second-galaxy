﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSynEventList4ObjInfo")]
    public class ProSynEventList4ObjInfo : IExtensible
    {
        private uint _ObjectId;
        private uint _EventFlag;
        private readonly List<ProLBSyncEvent> _EventList;
        private readonly List<ProLBSyncEventOnHitInfo> _OnHitList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ObjectId;
        private static DelegateBridge __Hotfix_set_ObjectId;
        private static DelegateBridge __Hotfix_get_EventFlag;
        private static DelegateBridge __Hotfix_set_EventFlag;
        private static DelegateBridge __Hotfix_get_EventList;
        private static DelegateBridge __Hotfix_get_OnHitList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="ObjectId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ObjectId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="EventFlag", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint EventFlag
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="EventList", DataFormat=DataFormat.Default)]
        public List<ProLBSyncEvent> EventList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="OnHitList", DataFormat=DataFormat.Default)]
        public List<ProLBSyncEventOnHitInfo> OnHitList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

