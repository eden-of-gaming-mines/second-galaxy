﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CharBasicPropertiesAddAck")]
    public class CharBasicPropertiesAddAck : IExtensible
    {
        private int _Result;
        private readonly List<int> _PropertiesIdList;
        private readonly List<int> _CountList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_PropertiesIdList;
        private static DelegateBridge __Hotfix_get_CountList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="PropertiesIdList", DataFormat=DataFormat.TwosComplement)]
        public List<int> PropertiesIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(3, Name="CountList", DataFormat=DataFormat.TwosComplement)]
        public List<int> CountList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

