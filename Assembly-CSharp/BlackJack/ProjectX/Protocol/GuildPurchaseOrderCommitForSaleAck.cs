﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildPurchaseOrderCommitForSaleAck")]
    public class GuildPurchaseOrderCommitForSaleAck : IExtensible
    {
        private int _Result;
        private ulong _OrderInstanceId;
        private long _OrderRestCount;
        private ProGuildPurchaseOrderInfo _MatcherOrder;
        private ProCurrencyUpdateInfo _CurrencyUpdateInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_OrderInstanceId;
        private static DelegateBridge __Hotfix_set_OrderInstanceId;
        private static DelegateBridge __Hotfix_get_OrderRestCount;
        private static DelegateBridge __Hotfix_set_OrderRestCount;
        private static DelegateBridge __Hotfix_get_MatcherOrder;
        private static DelegateBridge __Hotfix_set_MatcherOrder;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_set_CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="OrderInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong OrderInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="OrderRestCount", DataFormat=DataFormat.TwosComplement)]
        public long OrderRestCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="MatcherOrder", DataFormat=DataFormat.Default)]
        public ProGuildPurchaseOrderInfo MatcherOrder
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="CurrencyUpdateInfo", DataFormat=DataFormat.Default)]
        public ProCurrencyUpdateInfo CurrencyUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

