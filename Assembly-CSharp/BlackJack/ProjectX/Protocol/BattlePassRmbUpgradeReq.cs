﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BattlePassRmbUpgradeReq")]
    public class BattlePassRmbUpgradeReq : IExtensible
    {
        private bool _IsPackage;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsPackage;
        private static DelegateBridge __Hotfix_set_IsPackage;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsPackage", DataFormat=DataFormat.Default)]
        public bool IsPackage
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

