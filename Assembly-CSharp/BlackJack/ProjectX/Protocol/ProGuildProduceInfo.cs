﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildProduceInfo")]
    public class ProGuildProduceInfo : IExtensible
    {
        private ulong _BuildingInsId;
        private long _ProduceStartTime;
        private long _ProduceEndTime;
        private int _ProduceSpeedUpTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BuildingInsId;
        private static DelegateBridge __Hotfix_set_BuildingInsId;
        private static DelegateBridge __Hotfix_get_ProduceStartTime;
        private static DelegateBridge __Hotfix_set_ProduceStartTime;
        private static DelegateBridge __Hotfix_get_ProduceEndTime;
        private static DelegateBridge __Hotfix_set_ProduceEndTime;
        private static DelegateBridge __Hotfix_get_ProduceSpeedUpTime;
        private static DelegateBridge __Hotfix_set_ProduceSpeedUpTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((float) 0f), ProtoMember(1, IsRequired=false, Name="BuildingInsId", DataFormat=DataFormat.TwosComplement)]
        public ulong BuildingInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ProduceStartTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long ProduceStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="ProduceEndTime", DataFormat=DataFormat.TwosComplement)]
        public long ProduceEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="ProduceSpeedUpTime", DataFormat=DataFormat.TwosComplement)]
        public int ProduceSpeedUpTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

