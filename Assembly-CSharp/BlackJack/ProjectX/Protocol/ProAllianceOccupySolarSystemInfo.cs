﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProAllianceOccupySolarSystemInfo")]
    public class ProAllianceOccupySolarSystemInfo : IExtensible
    {
        private uint _AllianceId;
        private readonly List<int> _OccupySolarSystemList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_OccupySolarSystemList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, Name="OccupySolarSystemList", DataFormat=DataFormat.TwosComplement)]
        public List<int> OccupySolarSystemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

