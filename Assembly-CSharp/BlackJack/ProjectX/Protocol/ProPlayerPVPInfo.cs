﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProPlayerPVPInfo")]
    public class ProPlayerPVPInfo : IExtensible
    {
        private bool _IsJustCrime;
        private long _JustCrimeTimeOut;
        private bool _IsCriminalHunter;
        private long _CriminalHunterTimeOut;
        private float _CriminalLevel;
        private uint _LastAidLeaveSolarSystemLimitTargetTime;
        private uint _LastPVPFireTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsJustCrime;
        private static DelegateBridge __Hotfix_set_IsJustCrime;
        private static DelegateBridge __Hotfix_get_JustCrimeTimeOut;
        private static DelegateBridge __Hotfix_set_JustCrimeTimeOut;
        private static DelegateBridge __Hotfix_get_IsCriminalHunter;
        private static DelegateBridge __Hotfix_set_IsCriminalHunter;
        private static DelegateBridge __Hotfix_get_CriminalHunterTimeOut;
        private static DelegateBridge __Hotfix_set_CriminalHunterTimeOut;
        private static DelegateBridge __Hotfix_get_CriminalLevel;
        private static DelegateBridge __Hotfix_set_CriminalLevel;
        private static DelegateBridge __Hotfix_get_LastAidLeaveSolarSystemLimitTargetTime;
        private static DelegateBridge __Hotfix_set_LastAidLeaveSolarSystemLimitTargetTime;
        private static DelegateBridge __Hotfix_get_LastPVPFireTime;
        private static DelegateBridge __Hotfix_set_LastPVPFireTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsJustCrime", DataFormat=DataFormat.Default)]
        public bool IsJustCrime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="JustCrimeTimeOut", DataFormat=DataFormat.TwosComplement)]
        public long JustCrimeTimeOut
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="IsCriminalHunter", DataFormat=DataFormat.Default)]
        public bool IsCriminalHunter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CriminalHunterTimeOut", DataFormat=DataFormat.TwosComplement)]
        public long CriminalHunterTimeOut
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="CriminalLevel", DataFormat=DataFormat.FixedSize)]
        public float CriminalLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="LastAidLeaveSolarSystemLimitTargetTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LastAidLeaveSolarSystemLimitTargetTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="LastPVPFireTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LastPVPFireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

