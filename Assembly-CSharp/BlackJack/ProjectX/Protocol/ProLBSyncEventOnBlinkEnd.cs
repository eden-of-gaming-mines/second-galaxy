﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSyncEventOnBlinkEnd")]
    public class ProLBSyncEventOnBlinkEnd : IExtensible
    {
        private bool _BlinkSuccessful;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BlinkSuccessful;
        private static DelegateBridge __Hotfix_set_BlinkSuccessful;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BlinkSuccessful", DataFormat=DataFormat.Default)]
        public bool BlinkSuccessful
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

