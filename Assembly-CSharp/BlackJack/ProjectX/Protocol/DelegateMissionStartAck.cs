﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DelegateMissionStartAck")]
    public class DelegateMissionStartAck : IExtensible
    {
        private int _Result;
        private ulong _SignalInstanceId;
        private readonly List<ulong> _HiredCaptainInstanceIdList;
        private ProDelegateMissionInfo _Mission;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_SignalInstanceId;
        private static DelegateBridge __Hotfix_set_SignalInstanceId;
        private static DelegateBridge __Hotfix_get_HiredCaptainInstanceIdList;
        private static DelegateBridge __Hotfix_get_Mission;
        private static DelegateBridge __Hotfix_set_Mission;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="SignalInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong SignalInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="HiredCaptainInstanceIdList", DataFormat=DataFormat.TwosComplement)]
        public List<ulong> HiredCaptainInstanceIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="Mission", DataFormat=DataFormat.Default)]
        public ProDelegateMissionInfo Mission
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

