﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildWalletInfoAck")]
    public class GuildWalletInfoAck : IExtensible
    {
        private int _Result;
        private ProGuildCurrencyInfo _CurrencyInfo;
        private readonly List<ProGuildDonateRankingItemInfo> _DonateRankingList;
        private string _WalletAnnouncement;
        private readonly List<ProGuildCurrencyLogInfo> _CurrencyLogInfo;
        private int _SolarSystemCount;
        private ulong _CountInformationPoints;
        private long _DonateTradeMoneyToday;
        private long _DonateTradeMoneyWeek;
        private long _TotalDonateTradeMoney;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_CurrencyInfo;
        private static DelegateBridge __Hotfix_set_CurrencyInfo;
        private static DelegateBridge __Hotfix_get_DonateRankingList;
        private static DelegateBridge __Hotfix_get_WalletAnnouncement;
        private static DelegateBridge __Hotfix_set_WalletAnnouncement;
        private static DelegateBridge __Hotfix_get_CurrencyLogInfo;
        private static DelegateBridge __Hotfix_get_SolarSystemCount;
        private static DelegateBridge __Hotfix_set_SolarSystemCount;
        private static DelegateBridge __Hotfix_get_CountInformationPoints;
        private static DelegateBridge __Hotfix_set_CountInformationPoints;
        private static DelegateBridge __Hotfix_get_DonateTradeMoneyToday;
        private static DelegateBridge __Hotfix_set_DonateTradeMoneyToday;
        private static DelegateBridge __Hotfix_get_DonateTradeMoneyWeek;
        private static DelegateBridge __Hotfix_set_DonateTradeMoneyWeek;
        private static DelegateBridge __Hotfix_get_TotalDonateTradeMoney;
        private static DelegateBridge __Hotfix_set_TotalDonateTradeMoney;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="CurrencyInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildCurrencyInfo CurrencyInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="DonateRankingList", DataFormat=DataFormat.Default)]
        public List<ProGuildDonateRankingItemInfo> DonateRankingList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="WalletAnnouncement", DataFormat=DataFormat.Default), DefaultValue("")]
        public string WalletAnnouncement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="CurrencyLogInfo", DataFormat=DataFormat.Default)]
        public List<ProGuildCurrencyLogInfo> CurrencyLogInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="SolarSystemCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int SolarSystemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(7, IsRequired=false, Name="CountInformationPoints", DataFormat=DataFormat.TwosComplement)]
        public ulong CountInformationPoints
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="DonateTradeMoneyToday", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long DonateTradeMoneyToday
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(9, IsRequired=false, Name="DonateTradeMoneyWeek", DataFormat=DataFormat.TwosComplement)]
        public long DonateTradeMoneyWeek
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(10, IsRequired=false, Name="TotalDonateTradeMoney", DataFormat=DataFormat.TwosComplement)]
        public long TotalDonateTradeMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

