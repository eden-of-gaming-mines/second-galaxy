﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DelegateMissionStateNtf")]
    public class DelegateMissionStateNtf : IExtensible
    {
        private ProDelegateMissionInfo _updateMission;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_UpdateMission;
        private static DelegateBridge __Hotfix_set_UpdateMission;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="updateMission", DataFormat=DataFormat.Default)]
        public ProDelegateMissionInfo UpdateMission
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

