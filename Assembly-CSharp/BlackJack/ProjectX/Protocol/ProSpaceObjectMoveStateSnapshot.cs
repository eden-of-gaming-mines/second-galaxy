﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceObjectMoveStateSnapshot")]
    public class ProSpaceObjectMoveStateSnapshot : IExtensible
    {
        private uint _ObjectId;
        private ProBaseMoveStateSnapshot _BasicSnapshot;
        private ProShipMoveStateSnapshot _ShipSnapshot;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ObjectId;
        private static DelegateBridge __Hotfix_set_ObjectId;
        private static DelegateBridge __Hotfix_get_BasicSnapshot;
        private static DelegateBridge __Hotfix_set_BasicSnapshot;
        private static DelegateBridge __Hotfix_get_ShipSnapshot;
        private static DelegateBridge __Hotfix_set_ShipSnapshot;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ObjectId", DataFormat=DataFormat.TwosComplement)]
        public uint ObjectId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(2, IsRequired=false, Name="BasicSnapshot", DataFormat=DataFormat.Default)]
        public ProBaseMoveStateSnapshot BasicSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ShipSnapshot", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProShipMoveStateSnapshot ShipSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

