﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TradeNpcShopSaleItemsAck")]
    public class TradeNpcShopSaleItemsAck : IExtensible
    {
        private int _Result;
        private uint _DataVersion;
        private readonly List<ProTradeNpcShopItemInfo> _TradeNpcShopItemInfos;
        private long _NextRefreshTime;
        private readonly List<ProShipStoreItemUpdateInfo> _ShipStoreItemUpdateInfos;
        private readonly List<ProStoreItemUpdateInfo> _StoreItemUpdateInfos;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdateInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_DataVersion;
        private static DelegateBridge __Hotfix_set_DataVersion;
        private static DelegateBridge __Hotfix_get_TradeNpcShopItemInfos;
        private static DelegateBridge __Hotfix_get_NextRefreshTime;
        private static DelegateBridge __Hotfix_set_NextRefreshTime;
        private static DelegateBridge __Hotfix_get_ShipStoreItemUpdateInfos;
        private static DelegateBridge __Hotfix_get_StoreItemUpdateInfos;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="DataVersion", DataFormat=DataFormat.TwosComplement)]
        public uint DataVersion
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="TradeNpcShopItemInfos", DataFormat=DataFormat.Default)]
        public List<ProTradeNpcShopItemInfo> TradeNpcShopItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="NextRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="ShipStoreItemUpdateInfos", DataFormat=DataFormat.Default)]
        public List<ProShipStoreItemUpdateInfo> ShipStoreItemUpdateInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="StoreItemUpdateInfos", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> StoreItemUpdateInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="CurrencyUpdateInfo", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

