﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildMomentsInfo")]
    public class ProGuildMomentsInfo : IExtensible
    {
        private uint _SeqId;
        private int _TypeId;
        private int _SubTypeId;
        private long _CreateTime;
        private long _ExpireTime;
        private bool _IsShowInSummary;
        private readonly List<ProFormatStringParamInfo> _BriefFormatStrParamList;
        private readonly List<ProFormatStringParamInfo> _ContentFormatStrParamList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SeqId;
        private static DelegateBridge __Hotfix_set_SeqId;
        private static DelegateBridge __Hotfix_get_TypeId;
        private static DelegateBridge __Hotfix_set_TypeId;
        private static DelegateBridge __Hotfix_get_SubTypeId;
        private static DelegateBridge __Hotfix_set_SubTypeId;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_get_ExpireTime;
        private static DelegateBridge __Hotfix_set_ExpireTime;
        private static DelegateBridge __Hotfix_get_IsShowInSummary;
        private static DelegateBridge __Hotfix_set_IsShowInSummary;
        private static DelegateBridge __Hotfix_get_BriefFormatStrParamList;
        private static DelegateBridge __Hotfix_get_ContentFormatStrParamList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SeqId", DataFormat=DataFormat.TwosComplement)]
        public uint SeqId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="TypeId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int TypeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="SubTypeId", DataFormat=DataFormat.TwosComplement)]
        public int SubTypeId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CreateTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="ExpireTime", DataFormat=DataFormat.TwosComplement)]
        public long ExpireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(6, IsRequired=false, Name="IsShowInSummary", DataFormat=DataFormat.Default)]
        public bool IsShowInSummary
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="BriefFormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> BriefFormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, Name="ContentFormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> ContentFormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

