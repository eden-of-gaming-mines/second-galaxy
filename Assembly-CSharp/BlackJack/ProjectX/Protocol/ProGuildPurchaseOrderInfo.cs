﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildPurchaseOrderInfo")]
    public class ProGuildPurchaseOrderInfo : IExtensible
    {
        private int _AuctionItemId;
        private long _RestItemCount;
        private ulong _InstanceId;
        private long _AddUpCount;
        private readonly List<ProGuildPurchaseLogInfo> _LogList;
        private long _MarketPrice;
        private int _Version;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AuctionItemId;
        private static DelegateBridge __Hotfix_set_AuctionItemId;
        private static DelegateBridge __Hotfix_get_RestItemCount;
        private static DelegateBridge __Hotfix_set_RestItemCount;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_AddUpCount;
        private static DelegateBridge __Hotfix_set_AddUpCount;
        private static DelegateBridge __Hotfix_get_LogList;
        private static DelegateBridge __Hotfix_get_MarketPrice;
        private static DelegateBridge __Hotfix_set_MarketPrice;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="AuctionItemId", DataFormat=DataFormat.TwosComplement)]
        public int AuctionItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(2, IsRequired=false, Name="RestItemCount", DataFormat=DataFormat.TwosComplement)]
        public long RestItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(3, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="AddUpCount", DataFormat=DataFormat.TwosComplement)]
        public long AddUpCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="LogList", DataFormat=DataFormat.Default)]
        public List<ProGuildPurchaseLogInfo> LogList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="MarketPrice", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long MarketPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(7, IsRequired=false, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public int Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

