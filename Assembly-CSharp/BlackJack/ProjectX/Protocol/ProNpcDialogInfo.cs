﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProNpcDialogInfo")]
    public class ProNpcDialogInfo : IExtensible
    {
        private int _DialogId;
        private ProNpcDNId _ReplaceNpc;
        private readonly List<ProNpcDialogOptInfo> _OptList;
        private readonly List<ProFormatStringParamInfo> _FormatStrParamList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DialogId;
        private static DelegateBridge __Hotfix_set_DialogId;
        private static DelegateBridge __Hotfix_get_ReplaceNpc;
        private static DelegateBridge __Hotfix_set_ReplaceNpc;
        private static DelegateBridge __Hotfix_get_OptList;
        private static DelegateBridge __Hotfix_get_FormatStrParamList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="DialogId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DialogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ReplaceNpc", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProNpcDNId ReplaceNpc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, Name="OptList", DataFormat=DataFormat.Default)]
        public List<ProNpcDialogOptInfo> OptList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(4, Name="FormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> FormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

