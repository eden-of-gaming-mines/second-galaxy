﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildMemberRuntimeInfo")]
    public class ProGuildMemberRuntimeInfo : IExtensible
    {
        private int _Seq;
        private string _Name;
        private int _Level;
        private int _AvatarId;
        private int _LocateSolorSystem;
        private int _EvaluateScore;
        private long _LastOnlineTime;
        private bool _IsOnline;
        private int _Profession;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Seq;
        private static DelegateBridge __Hotfix_set_Seq;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_AvatarId;
        private static DelegateBridge __Hotfix_set_AvatarId;
        private static DelegateBridge __Hotfix_get_LocateSolorSystem;
        private static DelegateBridge __Hotfix_set_LocateSolorSystem;
        private static DelegateBridge __Hotfix_get_EvaluateScore;
        private static DelegateBridge __Hotfix_set_EvaluateScore;
        private static DelegateBridge __Hotfix_get_LastOnlineTime;
        private static DelegateBridge __Hotfix_set_LastOnlineTime;
        private static DelegateBridge __Hotfix_get_IsOnline;
        private static DelegateBridge __Hotfix_set_IsOnline;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Seq", DataFormat=DataFormat.TwosComplement)]
        public int Seq
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="AvatarId", DataFormat=DataFormat.TwosComplement)]
        public int AvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="LocateSolorSystem", DataFormat=DataFormat.TwosComplement)]
        public int LocateSolorSystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="EvaluateScore", DataFormat=DataFormat.TwosComplement)]
        public int EvaluateScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="LastOnlineTime", DataFormat=DataFormat.TwosComplement)]
        public long LastOnlineTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="IsOnline", DataFormat=DataFormat.Default)]
        public bool IsOnline
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

