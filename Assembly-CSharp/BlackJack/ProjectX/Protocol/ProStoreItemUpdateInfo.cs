﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProStoreItemUpdateInfo")]
    public class ProStoreItemUpdateInfo : IExtensible
    {
        private ProStoreItemInfo _Item;
        private long _ChangeValue;
        private ProStoreItemInfo _BindItem;
        private long _BindItemChangeValue;
        private ProStoreItemInfo _FreezingItem;
        private long _FreezingItemChangeValue;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Item;
        private static DelegateBridge __Hotfix_set_Item;
        private static DelegateBridge __Hotfix_get_ChangeValue;
        private static DelegateBridge __Hotfix_set_ChangeValue;
        private static DelegateBridge __Hotfix_get_BindItem;
        private static DelegateBridge __Hotfix_set_BindItem;
        private static DelegateBridge __Hotfix_get_BindItemChangeValue;
        private static DelegateBridge __Hotfix_set_BindItemChangeValue;
        private static DelegateBridge __Hotfix_get_FreezingItem;
        private static DelegateBridge __Hotfix_set_FreezingItem;
        private static DelegateBridge __Hotfix_get_FreezingItemChangeValue;
        private static DelegateBridge __Hotfix_set_FreezingItemChangeValue;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Item", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProStoreItemInfo Item
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ChangeValue", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long ChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="BindItem", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProStoreItemInfo BindItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="BindItemChangeValue", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long BindItemChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="FreezingItem", DataFormat=DataFormat.Default)]
        public ProStoreItemInfo FreezingItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="FreezingItemChangeValue", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long FreezingItemChangeValue
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

