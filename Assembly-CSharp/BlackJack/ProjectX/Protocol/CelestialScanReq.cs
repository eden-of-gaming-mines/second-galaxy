﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="CelestialScanReq")]
    public class CelestialScanReq : IExtensible
    {
        private bool _IsSolarSystemScan;
        private ProVectorDouble _PlayerShipLocation;
        private bool _IsInfected;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_IsSolarSystemScan;
        private static DelegateBridge __Hotfix_set_IsSolarSystemScan;
        private static DelegateBridge __Hotfix_get_PlayerShipLocation;
        private static DelegateBridge __Hotfix_set_PlayerShipLocation;
        private static DelegateBridge __Hotfix_get_IsInfected;
        private static DelegateBridge __Hotfix_set_IsInfected;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="IsSolarSystemScan", DataFormat=DataFormat.Default)]
        public bool IsSolarSystemScan
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="PlayerShipLocation", DataFormat=DataFormat.Default)]
        public ProVectorDouble PlayerShipLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="IsInfected", DataFormat=DataFormat.Default)]
        public bool IsInfected
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

