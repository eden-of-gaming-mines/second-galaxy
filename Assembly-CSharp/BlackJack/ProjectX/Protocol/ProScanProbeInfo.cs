﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProScanProbeInfo")]
    public class ProScanProbeInfo : IExtensible
    {
        private int _ScanProbeType;
        private int _CurrCount;
        private long _CDStartTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ScanProbeType;
        private static DelegateBridge __Hotfix_set_ScanProbeType;
        private static DelegateBridge __Hotfix_get_CurrCount;
        private static DelegateBridge __Hotfix_set_CurrCount;
        private static DelegateBridge __Hotfix_get_CDStartTime;
        private static DelegateBridge __Hotfix_set_CDStartTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ScanProbeType", DataFormat=DataFormat.TwosComplement)]
        public int ScanProbeType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="CurrCount", DataFormat=DataFormat.TwosComplement)]
        public int CurrCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="CDStartTime", DataFormat=DataFormat.TwosComplement)]
        public long CDStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

