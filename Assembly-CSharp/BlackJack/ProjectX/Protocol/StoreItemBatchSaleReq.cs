﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="StoreItemBatchSaleReq")]
    public class StoreItemBatchSaleReq : IExtensible
    {
        private readonly List<ProIdValueInfo> _StoreItemIndexCountList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StoreItemIndexCountList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, Name="StoreItemIndexCountList", DataFormat=DataFormat.Default)]
        public List<ProIdValueInfo> StoreItemIndexCountList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

