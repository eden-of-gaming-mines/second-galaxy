﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildFlagShipHangarShipRemoveModuleEquip2SlotReq")]
    public class GuildFlagShipHangarShipRemoveModuleEquip2SlotReq : IExtensible
    {
        private ulong _InstanceId;
        private int _HangarShipIndex;
        private int _HangarShipItemIndex;
        private ProItemInfo _HangarShipItemInfo;
        private int _EquipSlotIndex;
        private int _EquipItemStoreIndex;
        private ProItemInfo _EquipItemInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_HangarShipIndex;
        private static DelegateBridge __Hotfix_set_HangarShipIndex;
        private static DelegateBridge __Hotfix_get_HangarShipItemIndex;
        private static DelegateBridge __Hotfix_set_HangarShipItemIndex;
        private static DelegateBridge __Hotfix_get_HangarShipItemInfo;
        private static DelegateBridge __Hotfix_set_HangarShipItemInfo;
        private static DelegateBridge __Hotfix_get_EquipSlotIndex;
        private static DelegateBridge __Hotfix_set_EquipSlotIndex;
        private static DelegateBridge __Hotfix_get_EquipItemStoreIndex;
        private static DelegateBridge __Hotfix_set_EquipItemStoreIndex;
        private static DelegateBridge __Hotfix_get_EquipItemInfo;
        private static DelegateBridge __Hotfix_set_EquipItemInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="HangarShipIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="HangarShipItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarShipItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="HangarShipItemInfo", DataFormat=DataFormat.Default)]
        public ProItemInfo HangarShipItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="EquipSlotIndex", DataFormat=DataFormat.TwosComplement)]
        public int EquipSlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="EquipItemStoreIndex", DataFormat=DataFormat.TwosComplement)]
        public int EquipItemStoreIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="EquipItemInfo", DataFormat=DataFormat.Default)]
        public ProItemInfo EquipItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

