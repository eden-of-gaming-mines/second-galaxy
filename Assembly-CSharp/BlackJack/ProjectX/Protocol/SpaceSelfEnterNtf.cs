﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SpaceSelfEnterNtf")]
    public class SpaceSelfEnterNtf : IExtensible
    {
        private uint _SolarSystemTickTime;
        private int _SolarySystemId;
        private ulong _ShipInstanceId;
        private uint _ObjId;
        private ProVectorDouble _Location;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_set_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_get_SolarySystemId;
        private static DelegateBridge __Hotfix_set_SolarySystemId;
        private static DelegateBridge __Hotfix_get_ShipInstanceId;
        private static DelegateBridge __Hotfix_set_ShipInstanceId;
        private static DelegateBridge __Hotfix_get_ObjId;
        private static DelegateBridge __Hotfix_set_ObjId;
        private static DelegateBridge __Hotfix_get_Location;
        private static DelegateBridge __Hotfix_set_Location;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemTickTime", DataFormat=DataFormat.TwosComplement)]
        public uint SolarSystemTickTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarySystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarySystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ShipInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ObjId", DataFormat=DataFormat.TwosComplement)]
        public uint ObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Location", DataFormat=DataFormat.Default)]
        public ProVectorDouble Location
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

