﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SpaceStationEnterNtf")]
    public class SpaceStationEnterNtf : IExtensible
    {
        private uint _SolarSystemTickTime;
        private int _SolarySystemId;
        private int _SpaceStationId;
        private ulong _ShipInstanceId;
        private bool _IsInfect;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_set_SolarSystemTickTime;
        private static DelegateBridge __Hotfix_get_SolarySystemId;
        private static DelegateBridge __Hotfix_set_SolarySystemId;
        private static DelegateBridge __Hotfix_get_SpaceStationId;
        private static DelegateBridge __Hotfix_set_SpaceStationId;
        private static DelegateBridge __Hotfix_get_ShipInstanceId;
        private static DelegateBridge __Hotfix_set_ShipInstanceId;
        private static DelegateBridge __Hotfix_get_IsInfect;
        private static DelegateBridge __Hotfix_set_IsInfect;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="SolarSystemTickTime", DataFormat=DataFormat.TwosComplement)]
        public uint SolarSystemTickTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SolarySystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarySystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SpaceStationId", DataFormat=DataFormat.TwosComplement)]
        public int SpaceStationId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ShipInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong ShipInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="IsInfect", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsInfect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

