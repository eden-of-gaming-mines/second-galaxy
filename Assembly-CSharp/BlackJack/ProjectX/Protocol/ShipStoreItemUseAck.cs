﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ShipStoreItemUseAck")]
    public class ShipStoreItemUseAck : IExtensible
    {
        private int _Result;
        private ProItemInfo _StoreItem;
        private readonly List<ProShipStoreItemUpdateInfo> _UpdateItemList;
        private readonly List<ProCurrencyUpdateInfo> _UpdateCurrencyList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_StoreItem;
        private static DelegateBridge __Hotfix_set_StoreItem;
        private static DelegateBridge __Hotfix_get_UpdateItemList;
        private static DelegateBridge __Hotfix_get_UpdateCurrencyList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="StoreItem", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProItemInfo StoreItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="UpdateItemList", DataFormat=DataFormat.Default)]
        public List<ProShipStoreItemUpdateInfo> UpdateItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="UpdateCurrencyList", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> UpdateCurrencyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

