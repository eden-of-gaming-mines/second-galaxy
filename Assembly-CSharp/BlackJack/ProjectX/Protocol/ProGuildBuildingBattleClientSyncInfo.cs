﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBuildingBattleClientSyncInfo")]
    public class ProGuildBuildingBattleClientSyncInfo : IExtensible
    {
        private ulong _InstanceId;
        private float _ShieldMax;
        private float _ArmorMax;
        private float _ShieldCurr;
        private float _ArmorCurr;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_ShieldMax;
        private static DelegateBridge __Hotfix_set_ShieldMax;
        private static DelegateBridge __Hotfix_get_ArmorMax;
        private static DelegateBridge __Hotfix_set_ArmorMax;
        private static DelegateBridge __Hotfix_get_ShieldCurr;
        private static DelegateBridge __Hotfix_set_ShieldCurr;
        private static DelegateBridge __Hotfix_get_ArmorCurr;
        private static DelegateBridge __Hotfix_set_ArmorCurr;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="InstanceId", DataFormat=DataFormat.TwosComplement), DefaultValue((float) 0f)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="ShieldMax", DataFormat=DataFormat.FixedSize)]
        public float ShieldMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ArmorMax", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float ArmorMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(4, IsRequired=false, Name="ShieldCurr", DataFormat=DataFormat.FixedSize)]
        public float ShieldCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="ArmorCurr", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float ArmorCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

