﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="TechUpgradeByRealMoneyReq")]
    public class TechUpgradeByRealMoneyReq : IExtensible
    {
        private int _TechId;
        private int _TechLevel;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_TechId;
        private static DelegateBridge __Hotfix_set_TechId;
        private static DelegateBridge __Hotfix_get_TechLevel;
        private static DelegateBridge __Hotfix_set_TechLevel;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="TechId", DataFormat=DataFormat.TwosComplement)]
        public int TechId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="TechLevel", DataFormat=DataFormat.TwosComplement)]
        public int TechLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

