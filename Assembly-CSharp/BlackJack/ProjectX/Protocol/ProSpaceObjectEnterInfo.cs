﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProSpaceObjectEnterInfo")]
    public class ProSpaceObjectEnterInfo : IExtensible
    {
        private uint _ObjectId;
        private int _SpaceObjectType;
        private int _GDBSolarSystemObjectId;
        private ProSpacePlayerShipViewInfo _PlayerShipViewInfo;
        private ProSpaceNpcShipViewInfo _NpcShipViewInfo;
        private ProSimpleObjectViewInfo _SimpleObjViewInfo;
        private ProSceneObjectViewInfo _SceneObjectViewInfo;
        private ProDropBoxViewInfo _DropBoxViewInfo;
        private ProSpaceHiredCaptainShipViewInfo _HiredCaptainShipViewInfo;
        private ProBaseMoveStateSnapshot _BasicSnapshot;
        private ProShipMoveStateSnapshot _ShipSnapshot;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ObjectId;
        private static DelegateBridge __Hotfix_set_ObjectId;
        private static DelegateBridge __Hotfix_get_SpaceObjectType;
        private static DelegateBridge __Hotfix_set_SpaceObjectType;
        private static DelegateBridge __Hotfix_get_GDBSolarSystemObjectId;
        private static DelegateBridge __Hotfix_set_GDBSolarSystemObjectId;
        private static DelegateBridge __Hotfix_get_PlayerShipViewInfo;
        private static DelegateBridge __Hotfix_set_PlayerShipViewInfo;
        private static DelegateBridge __Hotfix_get_NpcShipViewInfo;
        private static DelegateBridge __Hotfix_set_NpcShipViewInfo;
        private static DelegateBridge __Hotfix_get_SimpleObjViewInfo;
        private static DelegateBridge __Hotfix_set_SimpleObjViewInfo;
        private static DelegateBridge __Hotfix_get_SceneObjectViewInfo;
        private static DelegateBridge __Hotfix_set_SceneObjectViewInfo;
        private static DelegateBridge __Hotfix_get_DropBoxViewInfo;
        private static DelegateBridge __Hotfix_set_DropBoxViewInfo;
        private static DelegateBridge __Hotfix_get_HiredCaptainShipViewInfo;
        private static DelegateBridge __Hotfix_set_HiredCaptainShipViewInfo;
        private static DelegateBridge __Hotfix_get_BasicSnapshot;
        private static DelegateBridge __Hotfix_set_BasicSnapshot;
        private static DelegateBridge __Hotfix_get_ShipSnapshot;
        private static DelegateBridge __Hotfix_set_ShipSnapshot;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ObjectId", DataFormat=DataFormat.TwosComplement)]
        public uint ObjectId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="SpaceObjectType", DataFormat=DataFormat.TwosComplement)]
        public int SpaceObjectType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="GDBSolarSystemObjectId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GDBSolarSystemObjectId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="PlayerShipViewInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpacePlayerShipViewInfo PlayerShipViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="NpcShipViewInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSpaceNpcShipViewInfo NpcShipViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="SimpleObjViewInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSimpleObjectViewInfo SimpleObjViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="SceneObjectViewInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProSceneObjectViewInfo SceneObjectViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="DropBoxViewInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProDropBoxViewInfo DropBoxViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(9, IsRequired=false, Name="HiredCaptainShipViewInfo", DataFormat=DataFormat.Default)]
        public ProSpaceHiredCaptainShipViewInfo HiredCaptainShipViewInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x19, IsRequired=false, Name="BasicSnapshot", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProBaseMoveStateSnapshot BasicSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x1a, IsRequired=false, Name="ShipSnapshot", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProShipMoveStateSnapshot ShipSnapshot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

