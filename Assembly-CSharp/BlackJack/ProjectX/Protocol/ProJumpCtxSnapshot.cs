﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProJumpCtxSnapshot")]
    public class ProJumpCtxSnapshot : IExtensible
    {
        private ProVectorDouble _JumpStartLocation;
        private uint _JumpTimespan;
        private bool _IsEnterView;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_JumpStartLocation;
        private static DelegateBridge __Hotfix_set_JumpStartLocation;
        private static DelegateBridge __Hotfix_get_JumpTimespan;
        private static DelegateBridge __Hotfix_set_JumpTimespan;
        private static DelegateBridge __Hotfix_get_IsEnterView;
        private static DelegateBridge __Hotfix_set_IsEnterView;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue((string) null), ProtoMember(1, IsRequired=false, Name="JumpStartLocation", DataFormat=DataFormat.Default)]
        public ProVectorDouble JumpStartLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="JumpTimespan", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint JumpTimespan
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="IsEnterView", DataFormat=DataFormat.Default)]
        public bool IsEnterView
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

