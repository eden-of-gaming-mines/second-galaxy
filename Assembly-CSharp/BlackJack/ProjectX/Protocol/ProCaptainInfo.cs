﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProCaptainInfo")]
    public class ProCaptainInfo : IExtensible
    {
        private int _PropertyBasicAttack;
        private int _PropertyBasicDefence;
        private int _PropertyBasicElectron;
        private int _PropertyBasicDrive;
        private int _DrivingLicenseId;
        private int _DrivingLicenseLevel;
        private int _Level;
        private string _Name;
        private string _GameUserId;
        private int _AvatarId;
        private int _Profession;
        private uint _GuildId;
        private string _GuildCodeName;
        private uint _AllianceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PropertyBasicAttack;
        private static DelegateBridge __Hotfix_set_PropertyBasicAttack;
        private static DelegateBridge __Hotfix_get_PropertyBasicDefence;
        private static DelegateBridge __Hotfix_set_PropertyBasicDefence;
        private static DelegateBridge __Hotfix_get_PropertyBasicElectron;
        private static DelegateBridge __Hotfix_set_PropertyBasicElectron;
        private static DelegateBridge __Hotfix_get_PropertyBasicDrive;
        private static DelegateBridge __Hotfix_set_PropertyBasicDrive;
        private static DelegateBridge __Hotfix_get_DrivingLicenseId;
        private static DelegateBridge __Hotfix_set_DrivingLicenseId;
        private static DelegateBridge __Hotfix_get_DrivingLicenseLevel;
        private static DelegateBridge __Hotfix_set_DrivingLicenseLevel;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_GameUserId;
        private static DelegateBridge __Hotfix_set_GameUserId;
        private static DelegateBridge __Hotfix_get_AvatarId;
        private static DelegateBridge __Hotfix_set_AvatarId;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_GuildCodeName;
        private static DelegateBridge __Hotfix_set_GuildCodeName;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [DefaultValue(0), ProtoMember(1, IsRequired=false, Name="PropertyBasicAttack", DataFormat=DataFormat.TwosComplement)]
        public int PropertyBasicAttack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="PropertyBasicDefence", DataFormat=DataFormat.TwosComplement)]
        public int PropertyBasicDefence
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="PropertyBasicElectron", DataFormat=DataFormat.TwosComplement)]
        public int PropertyBasicElectron
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="PropertyBasicDrive", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int PropertyBasicDrive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="DrivingLicenseId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DrivingLicenseId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="DrivingLicenseLevel", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DrivingLicenseLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(7, IsRequired=false, Name="Level", DataFormat=DataFormat.TwosComplement)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="Name", DataFormat=DataFormat.Default), DefaultValue("")]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(9, IsRequired=false, Name="GameUserId", DataFormat=DataFormat.Default)]
        public string GameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="AvatarId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(11, IsRequired=false, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(12, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(13, IsRequired=false, Name="GuildCodeName", DataFormat=DataFormat.Default)]
        public string GuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

