﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BlackMarketBuyItemAck")]
    public class BlackMarketBuyItemAck : IExtensible
    {
        private int _Result;
        private readonly List<ProShipStoreItemUpdateInfo> _ShipStoreItemUpdateInfo;
        private readonly List<ProStoreItemUpdateInfo> _StoreItemUpdateInfo;
        private readonly List<ProCurrencyUpdateInfo> _CurrencyUpdateInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ShipStoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_get_StoreItemUpdateInfo;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="ShipStoreItemUpdateInfo", DataFormat=DataFormat.Default)]
        public List<ProShipStoreItemUpdateInfo> ShipStoreItemUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="StoreItemUpdateInfo", DataFormat=DataFormat.Default)]
        public List<ProStoreItemUpdateInfo> StoreItemUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="CurrencyUpdateInfo", DataFormat=DataFormat.Default)]
        public List<ProCurrencyUpdateInfo> CurrencyUpdateInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

