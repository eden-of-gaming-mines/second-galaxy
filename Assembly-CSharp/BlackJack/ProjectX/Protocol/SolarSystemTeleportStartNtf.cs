﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SolarSystemTeleportStartNtf")]
    public class SolarSystemTeleportStartNtf : IExtensible
    {
        private int _DestSolarSystemId;
        private bool _NeedReturnMotherShip;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DestSolarSystemId;
        private static DelegateBridge __Hotfix_set_DestSolarSystemId;
        private static DelegateBridge __Hotfix_get_NeedReturnMotherShip;
        private static DelegateBridge __Hotfix_set_NeedReturnMotherShip;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="DestSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int DestSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="NeedReturnMotherShip", DataFormat=DataFormat.Default)]
        public bool NeedReturnMotherShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

