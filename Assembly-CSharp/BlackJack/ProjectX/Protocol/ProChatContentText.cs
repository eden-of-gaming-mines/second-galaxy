﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProChatContentText")]
    public class ProChatContentText : IExtensible
    {
        private string _Text;
        private int _StringId;
        private readonly List<ProFormatStringParamInfo> _FormatStrParamList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Text;
        private static DelegateBridge __Hotfix_set_Text;
        private static DelegateBridge __Hotfix_get_StringId;
        private static DelegateBridge __Hotfix_set_StringId;
        private static DelegateBridge __Hotfix_get_FormatStrParamList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Text", DataFormat=DataFormat.Default), DefaultValue("")]
        public string Text
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="StringId", DataFormat=DataFormat.TwosComplement)]
        public int StringId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="FormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> FormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

