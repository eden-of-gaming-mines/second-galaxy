﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProStoreItemTransformInfo")]
    public class ProStoreItemTransformInfo : IExtensible
    {
        private int _FromIndex;
        private int _ToIndex;
        private ulong _ToInstanceId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_FromIndex;
        private static DelegateBridge __Hotfix_set_FromIndex;
        private static DelegateBridge __Hotfix_get_ToIndex;
        private static DelegateBridge __Hotfix_set_ToIndex;
        private static DelegateBridge __Hotfix_get_ToInstanceId;
        private static DelegateBridge __Hotfix_set_ToInstanceId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="FromIndex", DataFormat=DataFormat.TwosComplement)]
        public int FromIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ToIndex", DataFormat=DataFormat.TwosComplement)]
        public int ToIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ToInstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong ToInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

