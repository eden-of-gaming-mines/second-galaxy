﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProAllianceBasicInfo")]
    public class ProAllianceBasicInfo : IExtensible
    {
        private uint _AllianceId;
        private string _Name;
        private string _Announcement;
        private int _LanguageType;
        private ProAllianceLogoInfo _logo;
        private uint _LeaderGuildId;
        private long _CreateTime;
        private uint _CreateGuildId;
        private string _CreateGuildName;
        private string _CreateGuildCode;
        private string _CreateGameUserId;
        private string _CreateGameUserName;
        private int _TotalGameUserCount;
        private int _OccupiedSolarSystemCount;
        private string _LeaderGuildCode;
        private string _LeaderGuildName;
        private int _TotalGuildCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_Announcement;
        private static DelegateBridge __Hotfix_set_Announcement;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_set_LanguageType;
        private static DelegateBridge __Hotfix_get_Logo;
        private static DelegateBridge __Hotfix_set_Logo;
        private static DelegateBridge __Hotfix_get_LeaderGuildId;
        private static DelegateBridge __Hotfix_set_LeaderGuildId;
        private static DelegateBridge __Hotfix_get_CreateTime;
        private static DelegateBridge __Hotfix_set_CreateTime;
        private static DelegateBridge __Hotfix_get_CreateGuildId;
        private static DelegateBridge __Hotfix_set_CreateGuildId;
        private static DelegateBridge __Hotfix_get_CreateGuildName;
        private static DelegateBridge __Hotfix_set_CreateGuildName;
        private static DelegateBridge __Hotfix_get_CreateGuildCode;
        private static DelegateBridge __Hotfix_set_CreateGuildCode;
        private static DelegateBridge __Hotfix_get_CreateGameUserId;
        private static DelegateBridge __Hotfix_set_CreateGameUserId;
        private static DelegateBridge __Hotfix_get_CreateGameUserName;
        private static DelegateBridge __Hotfix_set_CreateGameUserName;
        private static DelegateBridge __Hotfix_get_TotalGameUserCount;
        private static DelegateBridge __Hotfix_set_TotalGameUserCount;
        private static DelegateBridge __Hotfix_get_OccupiedSolarSystemCount;
        private static DelegateBridge __Hotfix_set_OccupiedSolarSystemCount;
        private static DelegateBridge __Hotfix_get_LeaderGuildCode;
        private static DelegateBridge __Hotfix_set_LeaderGuildCode;
        private static DelegateBridge __Hotfix_get_LeaderGuildName;
        private static DelegateBridge __Hotfix_set_LeaderGuildName;
        private static DelegateBridge __Hotfix_get_TotalGuildCount;
        private static DelegateBridge __Hotfix_set_TotalGuildCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="AllianceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="Announcement", DataFormat=DataFormat.Default), DefaultValue("")]
        public string Announcement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="LanguageType", DataFormat=DataFormat.TwosComplement)]
        public int LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(5, IsRequired=false, Name="logo", DataFormat=DataFormat.Default)]
        public ProAllianceLogoInfo Logo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="LeaderGuildId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint LeaderGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="CreateTime", DataFormat=DataFormat.TwosComplement)]
        public long CreateTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(8, IsRequired=false, Name="CreateGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint CreateGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(9, IsRequired=false, Name="CreateGuildName", DataFormat=DataFormat.Default)]
        public string CreateGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(10, IsRequired=false, Name="CreateGuildCode", DataFormat=DataFormat.Default)]
        public string CreateGuildCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="CreateGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string CreateGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="CreateGameUserName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string CreateGameUserName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(13, IsRequired=false, Name="TotalGameUserCount", DataFormat=DataFormat.TwosComplement)]
        public int TotalGameUserCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=false, Name="OccupiedSolarSystemCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int OccupiedSolarSystemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(15, IsRequired=false, Name="LeaderGuildCode", DataFormat=DataFormat.Default)]
        public string LeaderGuildCode
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="LeaderGuildName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string LeaderGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, IsRequired=false, Name="TotalGuildCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int TotalGuildCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

