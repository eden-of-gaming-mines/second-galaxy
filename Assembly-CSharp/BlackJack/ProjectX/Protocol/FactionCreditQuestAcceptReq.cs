﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="FactionCreditQuestAcceptReq")]
    public class FactionCreditQuestAcceptReq : IExtensible
    {
        private int _QuestId;
        private int _QuestFactionId;
        private int _QuestLevel;
        private int _SceneSolarSysytemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_QuestId;
        private static DelegateBridge __Hotfix_set_QuestId;
        private static DelegateBridge __Hotfix_get_QuestFactionId;
        private static DelegateBridge __Hotfix_set_QuestFactionId;
        private static DelegateBridge __Hotfix_get_QuestLevel;
        private static DelegateBridge __Hotfix_set_QuestLevel;
        private static DelegateBridge __Hotfix_get_SceneSolarSysytemId;
        private static DelegateBridge __Hotfix_set_SceneSolarSysytemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="QuestId", DataFormat=DataFormat.TwosComplement)]
        public int QuestId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="QuestFactionId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int QuestFactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="QuestLevel", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int QuestLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(4, IsRequired=false, Name="SceneSolarSysytemId", DataFormat=DataFormat.TwosComplement)]
        public int SceneSolarSysytemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

