﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProShipCustomTemplateInfo")]
    public class ProShipCustomTemplateInfo : IExtensible
    {
        private int _Index;
        private int _ShipConfId;
        private string _Name;
        private bool _CanBeDelete;
        private readonly List<ProShipTemplateHighSlotInfo> _HighSlot;
        private readonly List<int> _MiddleSlot;
        private readonly List<int> _LowSlot;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_ShipConfId;
        private static DelegateBridge __Hotfix_set_ShipConfId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_CanBeDelete;
        private static DelegateBridge __Hotfix_set_CanBeDelete;
        private static DelegateBridge __Hotfix_get_HighSlot;
        private static DelegateBridge __Hotfix_get_MiddleSlot;
        private static DelegateBridge __Hotfix_get_LowSlot;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Index", DataFormat=DataFormat.TwosComplement)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ShipConfId", DataFormat=DataFormat.TwosComplement)]
        public int ShipConfId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="CanBeDelete", DataFormat=DataFormat.Default)]
        public bool CanBeDelete
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, Name="HighSlot", DataFormat=DataFormat.Default)]
        public List<ProShipTemplateHighSlotInfo> HighSlot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="MiddleSlot", DataFormat=DataFormat.TwosComplement)]
        public List<int> MiddleSlot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(7, Name="LowSlot", DataFormat=DataFormat.TwosComplement)]
        public List<int> LowSlot
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

