﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="DSBattlePassNtf")]
    public class DSBattlePassNtf : IExtensible
    {
        private uint _Version;
        private int _PeriodId;
        private int _ChallangeQuestGroupIndex;
        private bool _Upgraded;
        private int _Exp;
        private int _ChallangeQuestCompletedCount;
        private ProBitArrayExInfo _RewardBalanced;
        private ProBitArrayExInfo _UpgradeRewardBalanced;
        private int _ChallangeQuestAccRewardBalanced;
        private ProBitArrayExInfo _CompleteQuest;
        private long _PeriodStartTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Version;
        private static DelegateBridge __Hotfix_set_Version;
        private static DelegateBridge __Hotfix_get_PeriodId;
        private static DelegateBridge __Hotfix_set_PeriodId;
        private static DelegateBridge __Hotfix_get_ChallangeQuestGroupIndex;
        private static DelegateBridge __Hotfix_set_ChallangeQuestGroupIndex;
        private static DelegateBridge __Hotfix_get_Upgraded;
        private static DelegateBridge __Hotfix_set_Upgraded;
        private static DelegateBridge __Hotfix_get_Exp;
        private static DelegateBridge __Hotfix_set_Exp;
        private static DelegateBridge __Hotfix_get_ChallangeQuestCompletedCount;
        private static DelegateBridge __Hotfix_set_ChallangeQuestCompletedCount;
        private static DelegateBridge __Hotfix_get_RewardBalanced;
        private static DelegateBridge __Hotfix_set_RewardBalanced;
        private static DelegateBridge __Hotfix_get_UpgradeRewardBalanced;
        private static DelegateBridge __Hotfix_set_UpgradeRewardBalanced;
        private static DelegateBridge __Hotfix_get_ChallangeQuestAccRewardBalanced;
        private static DelegateBridge __Hotfix_set_ChallangeQuestAccRewardBalanced;
        private static DelegateBridge __Hotfix_get_CompleteQuest;
        private static DelegateBridge __Hotfix_set_CompleteQuest;
        private static DelegateBridge __Hotfix_get_PeriodStartTime;
        private static DelegateBridge __Hotfix_set_PeriodStartTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Version", DataFormat=DataFormat.TwosComplement)]
        public uint Version
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="PeriodId", DataFormat=DataFormat.TwosComplement)]
        public int PeriodId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="ChallangeQuestGroupIndex", DataFormat=DataFormat.TwosComplement)]
        public int ChallangeQuestGroupIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="Upgraded", DataFormat=DataFormat.Default)]
        public bool Upgraded
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="Exp", DataFormat=DataFormat.TwosComplement)]
        public int Exp
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="ChallangeQuestCompletedCount", DataFormat=DataFormat.TwosComplement)]
        public int ChallangeQuestCompletedCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="RewardBalanced", DataFormat=DataFormat.Default)]
        public ProBitArrayExInfo RewardBalanced
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="UpgradeRewardBalanced", DataFormat=DataFormat.Default)]
        public ProBitArrayExInfo UpgradeRewardBalanced
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="ChallangeQuestAccRewardBalanced", DataFormat=DataFormat.TwosComplement)]
        public int ChallangeQuestAccRewardBalanced
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="CompleteQuest", DataFormat=DataFormat.Default)]
        public ProBitArrayExInfo CompleteQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(11, IsRequired=false, Name="PeriodStartTime", DataFormat=DataFormat.TwosComplement)]
        public long PeriodStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

