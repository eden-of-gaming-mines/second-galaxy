﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProPlayerSimplestInfo")]
    public class ProPlayerSimplestInfo : IExtensible
    {
        private string _PlayerGameUserId;
        private string _Name;
        private int _Level;
        private int _AvatarId;
        private int _EvaluateScore;
        private int _Profession;
        private uint _GuildId;
        private string _GuildName;
        private string _GuildCodeName;
        private uint _AllianceId;
        private string _AllianceName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerGameUserId;
        private static DelegateBridge __Hotfix_set_PlayerGameUserId;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_set_Level;
        private static DelegateBridge __Hotfix_get_AvatarId;
        private static DelegateBridge __Hotfix_set_AvatarId;
        private static DelegateBridge __Hotfix_get_EvaluateScore;
        private static DelegateBridge __Hotfix_set_EvaluateScore;
        private static DelegateBridge __Hotfix_get_Profession;
        private static DelegateBridge __Hotfix_set_Profession;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_GuildName;
        private static DelegateBridge __Hotfix_set_GuildName;
        private static DelegateBridge __Hotfix_get_GuildCodeName;
        private static DelegateBridge __Hotfix_set_GuildCodeName;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_set_AllianceName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="PlayerGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(2, IsRequired=false, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="Level", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="AvatarId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="EvaluateScore", DataFormat=DataFormat.TwosComplement)]
        public int EvaluateScore
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="Profession", DataFormat=DataFormat.TwosComplement)]
        public int Profession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(8, IsRequired=false, Name="GuildName", DataFormat=DataFormat.Default)]
        public string GuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(9, IsRequired=false, Name="GuildCodeName", DataFormat=DataFormat.Default)]
        public string GuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=false, Name="AllianceName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

