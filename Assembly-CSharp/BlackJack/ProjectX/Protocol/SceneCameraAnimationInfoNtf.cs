﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SceneCameraAnimationInfoNtf")]
    public class SceneCameraAnimationInfoNtf : IExtensible
    {
        private int _Type;
        private int _ConfgId;
        private bool _InitRotationWithTarget;
        private bool _IsFollowTarget;
        private bool _IsLookAtTarget;
        private uint _TargetObjId;
        private ProVectorDouble _LookAtLocation;
        private ProVectorDouble _CameraLocation;
        private ProVectorDouble _CameraRotation;
        private readonly List<ProSceneCameraNameObjIdPair> _ObjPairList;
        private readonly List<InSpaceNpcTalkerChatNtf> _NpcChatList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_ConfgId;
        private static DelegateBridge __Hotfix_set_ConfgId;
        private static DelegateBridge __Hotfix_get_InitRotationWithTarget;
        private static DelegateBridge __Hotfix_set_InitRotationWithTarget;
        private static DelegateBridge __Hotfix_get_IsFollowTarget;
        private static DelegateBridge __Hotfix_set_IsFollowTarget;
        private static DelegateBridge __Hotfix_get_IsLookAtTarget;
        private static DelegateBridge __Hotfix_set_IsLookAtTarget;
        private static DelegateBridge __Hotfix_get_TargetObjId;
        private static DelegateBridge __Hotfix_set_TargetObjId;
        private static DelegateBridge __Hotfix_get_LookAtLocation;
        private static DelegateBridge __Hotfix_set_LookAtLocation;
        private static DelegateBridge __Hotfix_get_CameraLocation;
        private static DelegateBridge __Hotfix_set_CameraLocation;
        private static DelegateBridge __Hotfix_get_CameraRotation;
        private static DelegateBridge __Hotfix_set_CameraRotation;
        private static DelegateBridge __Hotfix_get_ObjPairList;
        private static DelegateBridge __Hotfix_get_NpcChatList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="Type", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ConfgId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int ConfgId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="InitRotationWithTarget", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool InitRotationWithTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(5, IsRequired=false, Name="IsFollowTarget", DataFormat=DataFormat.Default)]
        public bool IsFollowTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="IsLookAtTarget", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsLookAtTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="TargetObjId", DataFormat=DataFormat.TwosComplement)]
        public uint TargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(8, IsRequired=false, Name="LookAtLocation", DataFormat=DataFormat.Default)]
        public ProVectorDouble LookAtLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="CameraLocation", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProVectorDouble CameraLocation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="CameraRotation", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProVectorDouble CameraRotation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, Name="ObjPairList", DataFormat=DataFormat.Default)]
        public List<ProSceneCameraNameObjIdPair> ObjPairList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(12, Name="NpcChatList", DataFormat=DataFormat.Default)]
        public List<InSpaceNpcTalkerChatNtf> NpcChatList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

