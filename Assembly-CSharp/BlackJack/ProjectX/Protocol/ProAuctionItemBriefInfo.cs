﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProAuctionItemBriefInfo")]
    public class ProAuctionItemBriefInfo : IExtensible
    {
        private int _AuctionItemId;
        private long _LowestPrice;
        private int _TotalCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_AuctionItemId;
        private static DelegateBridge __Hotfix_set_AuctionItemId;
        private static DelegateBridge __Hotfix_get_LowestPrice;
        private static DelegateBridge __Hotfix_set_LowestPrice;
        private static DelegateBridge __Hotfix_get_TotalCount;
        private static DelegateBridge __Hotfix_set_TotalCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="AuctionItemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int AuctionItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="LowestPrice", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long LowestPrice
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="TotalCount", DataFormat=DataFormat.TwosComplement)]
        public int TotalCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

