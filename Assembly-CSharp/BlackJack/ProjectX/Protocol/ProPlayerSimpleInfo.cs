﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProPlayerSimpleInfo")]
    public class ProPlayerSimpleInfo : IExtensible
    {
        private string _PlayerName;
        private int _GrandFaction;
        private int _CharLevel;
        private int _CharAvatarId;
        private string _PlayerGameUserId;
        private int _CurrSolarSystemId;
        private int _CurrShipId;
        private bool _IsInSpace;
        private int _CharGender;
        private int _CharProfession;
        private uint _GuildId;
        private string _GuildCodeName;
        private uint _AllianceId;
        private string _AllianceName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerName;
        private static DelegateBridge __Hotfix_set_PlayerName;
        private static DelegateBridge __Hotfix_get_GrandFaction;
        private static DelegateBridge __Hotfix_set_GrandFaction;
        private static DelegateBridge __Hotfix_get_CharLevel;
        private static DelegateBridge __Hotfix_set_CharLevel;
        private static DelegateBridge __Hotfix_get_CharAvatarId;
        private static DelegateBridge __Hotfix_set_CharAvatarId;
        private static DelegateBridge __Hotfix_get_PlayerGameUserId;
        private static DelegateBridge __Hotfix_set_PlayerGameUserId;
        private static DelegateBridge __Hotfix_get_CurrSolarSystemId;
        private static DelegateBridge __Hotfix_set_CurrSolarSystemId;
        private static DelegateBridge __Hotfix_get_CurrShipId;
        private static DelegateBridge __Hotfix_set_CurrShipId;
        private static DelegateBridge __Hotfix_get_IsInSpace;
        private static DelegateBridge __Hotfix_set_IsInSpace;
        private static DelegateBridge __Hotfix_get_CharGender;
        private static DelegateBridge __Hotfix_set_CharGender;
        private static DelegateBridge __Hotfix_get_CharProfession;
        private static DelegateBridge __Hotfix_set_CharProfession;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_GuildCodeName;
        private static DelegateBridge __Hotfix_set_GuildCodeName;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_set_AllianceName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="PlayerName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="GrandFaction", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int GrandFaction
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="CharLevel", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CharLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CharAvatarId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CharAvatarId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="PlayerGameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="CurrSolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int CurrSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(7, IsRequired=false, Name="CurrShipId", DataFormat=DataFormat.TwosComplement)]
        public int CurrShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=false, Name="IsInSpace", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsInSpace
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=false, Name="CharGender", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CharGender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=false, Name="CharProfession", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CharProfession
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(11, IsRequired=false, Name="GuildId", DataFormat=DataFormat.TwosComplement)]
        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=false, Name="GuildCodeName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string GuildCodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=false, Name="AllianceId", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(14, IsRequired=false, Name="AllianceName", DataFormat=DataFormat.Default)]
        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

