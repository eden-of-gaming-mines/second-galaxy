﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildBattleShipLostStatInfo")]
    public class ProGuildBattleShipLostStatInfo : IExtensible
    {
        private int _Group;
        private int _ShipId;
        private int _SelfLostCount;
        private double _SelfLostBindMoney;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Group;
        private static DelegateBridge __Hotfix_set_Group;
        private static DelegateBridge __Hotfix_get_ShipId;
        private static DelegateBridge __Hotfix_set_ShipId;
        private static DelegateBridge __Hotfix_get_SelfLostCount;
        private static DelegateBridge __Hotfix_set_SelfLostCount;
        private static DelegateBridge __Hotfix_get_SelfLostBindMoney;
        private static DelegateBridge __Hotfix_set_SelfLostBindMoney;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Group", DataFormat=DataFormat.TwosComplement)]
        public int Group
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="ShipId", DataFormat=DataFormat.TwosComplement)]
        public int ShipId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="SelfLostCount", DataFormat=DataFormat.TwosComplement)]
        public int SelfLostCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="SelfLostBindMoney", DataFormat=DataFormat.TwosComplement)]
        public double SelfLostBindMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

