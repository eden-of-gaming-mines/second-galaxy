﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="AllianceCreateReq")]
    public class AllianceCreateReq : IExtensible
    {
        private string _Name;
        private int _Region;
        private string _Announcement;
        private ProAllianceLogoInfo _Logo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_Region;
        private static DelegateBridge __Hotfix_set_Region;
        private static DelegateBridge __Hotfix_get_Announcement;
        private static DelegateBridge __Hotfix_set_Announcement;
        private static DelegateBridge __Hotfix_get_Logo;
        private static DelegateBridge __Hotfix_set_Logo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="Region", DataFormat=DataFormat.TwosComplement)]
        public int Region
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(3, IsRequired=false, Name="Announcement", DataFormat=DataFormat.Default)]
        public string Announcement
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="Logo", DataFormat=DataFormat.Default)]
        public ProAllianceLogoInfo Logo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

