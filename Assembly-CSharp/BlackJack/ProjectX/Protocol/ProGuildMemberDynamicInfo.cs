﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildMemberDynamicInfo")]
    public class ProGuildMemberDynamicInfo : IExtensible
    {
        private int _Seq;
        private long _Contribution;
        private long _DonateTradeMoneyToday;
        private long _ContributionCurrWeek;
        private int _CompensationDonateToday;
        private long _InformationPointCurrWeek;
        private int _DrivingFlagShipHangarSolarSystemId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Seq;
        private static DelegateBridge __Hotfix_set_Seq;
        private static DelegateBridge __Hotfix_get_Contribution;
        private static DelegateBridge __Hotfix_set_Contribution;
        private static DelegateBridge __Hotfix_get_DonateTradeMoneyToday;
        private static DelegateBridge __Hotfix_set_DonateTradeMoneyToday;
        private static DelegateBridge __Hotfix_get_ContributionCurrWeek;
        private static DelegateBridge __Hotfix_set_ContributionCurrWeek;
        private static DelegateBridge __Hotfix_get_CompensationDonateToday;
        private static DelegateBridge __Hotfix_set_CompensationDonateToday;
        private static DelegateBridge __Hotfix_get_InformationPointCurrWeek;
        private static DelegateBridge __Hotfix_set_InformationPointCurrWeek;
        private static DelegateBridge __Hotfix_get_DrivingFlagShipHangarSolarSystemId;
        private static DelegateBridge __Hotfix_set_DrivingFlagShipHangarSolarSystemId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Seq", DataFormat=DataFormat.TwosComplement)]
        public int Seq
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Contribution", DataFormat=DataFormat.TwosComplement)]
        public long Contribution
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="DonateTradeMoneyToday", DataFormat=DataFormat.TwosComplement)]
        public long DonateTradeMoneyToday
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="ContributionCurrWeek", DataFormat=DataFormat.TwosComplement)]
        public long ContributionCurrWeek
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="CompensationDonateToday", DataFormat=DataFormat.TwosComplement)]
        public int CompensationDonateToday
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(6, IsRequired=false, Name="InformationPointCurrWeek", DataFormat=DataFormat.TwosComplement)]
        public long InformationPointCurrWeek
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=false, Name="DrivingFlagShipHangarSolarSystemId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int DrivingFlagShipHangarSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

