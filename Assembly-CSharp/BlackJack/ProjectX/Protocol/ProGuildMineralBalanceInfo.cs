﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildMineralBalanceInfo")]
    public class ProGuildMineralBalanceInfo : IExtensible
    {
        private ulong _BuildingInsId;
        private int _MiningStatus;
        private long _CurrStatusStartTime;
        private long _CurrStatusEndTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BuildingInsId;
        private static DelegateBridge __Hotfix_set_BuildingInsId;
        private static DelegateBridge __Hotfix_get_MiningStatus;
        private static DelegateBridge __Hotfix_set_MiningStatus;
        private static DelegateBridge __Hotfix_get_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_set_CurrStatusStartTime;
        private static DelegateBridge __Hotfix_get_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_set_CurrStatusEndTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BuildingInsId", DataFormat=DataFormat.TwosComplement)]
        public ulong BuildingInsId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="MiningStatus", DataFormat=DataFormat.TwosComplement)]
        public int MiningStatus
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="CurrStatusStartTime", DataFormat=DataFormat.TwosComplement)]
        public long CurrStatusStartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="CurrStatusEndTime", DataFormat=DataFormat.TwosComplement)]
        public long CurrStatusEndTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

