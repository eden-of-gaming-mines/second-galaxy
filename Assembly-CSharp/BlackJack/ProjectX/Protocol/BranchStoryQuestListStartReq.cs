﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="BranchStoryQuestListStartReq")]
    public class BranchStoryQuestListStartReq : IExtensible
    {
        private int _BranchStoryQuestListId;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BranchStoryQuestListId;
        private static DelegateBridge __Hotfix_set_BranchStoryQuestListId;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BranchStoryQuestListId", DataFormat=DataFormat.TwosComplement)]
        public int BranchStoryQuestListId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

