﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GuildDonateTradeMoneyReq")]
    public class GuildDonateTradeMoneyReq : IExtensible
    {
        private long _DonateTradeMoney;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DonateTradeMoney;
        private static DelegateBridge __Hotfix_set_DonateTradeMoney;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="DonateTradeMoney", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long DonateTradeMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

