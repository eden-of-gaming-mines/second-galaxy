﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildFlagShipDetailInfo")]
    public class ProGuildFlagShipDetailInfo : IExtensible
    {
        private int _ShipConfigId;
        private string _ShipName;
        private ulong _InstanceId;
        private int _State;
        private int _SolarSystemId;
        private ProPlayerSimplestInfo _PlayerCaptain;
        private readonly List<int> _FlagShipModuleEquip;
        private int _Index;
        private long _UnpackTime;
        private bool _IsDestroyed;
        private double _CurrFuel;
        private bool _IsDeleted;
        private int _StoreItemIndex;
        private uint _CurrArmor;
        private uint _CurrShield;
        private long _DestroyedTime;
        private readonly List<ProOffLineBufInfo> _OffLineBufInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_ShipConfigId;
        private static DelegateBridge __Hotfix_set_ShipConfigId;
        private static DelegateBridge __Hotfix_get_ShipName;
        private static DelegateBridge __Hotfix_set_ShipName;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_State;
        private static DelegateBridge __Hotfix_set_State;
        private static DelegateBridge __Hotfix_get_SolarSystemId;
        private static DelegateBridge __Hotfix_set_SolarSystemId;
        private static DelegateBridge __Hotfix_get_PlayerCaptain;
        private static DelegateBridge __Hotfix_set_PlayerCaptain;
        private static DelegateBridge __Hotfix_get_FlagShipModuleEquip;
        private static DelegateBridge __Hotfix_get_Index;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_UnpackTime;
        private static DelegateBridge __Hotfix_set_UnpackTime;
        private static DelegateBridge __Hotfix_get_IsDestroyed;
        private static DelegateBridge __Hotfix_set_IsDestroyed;
        private static DelegateBridge __Hotfix_get_CurrFuel;
        private static DelegateBridge __Hotfix_set_CurrFuel;
        private static DelegateBridge __Hotfix_get_IsDeleted;
        private static DelegateBridge __Hotfix_set_IsDeleted;
        private static DelegateBridge __Hotfix_get_StoreItemIndex;
        private static DelegateBridge __Hotfix_set_StoreItemIndex;
        private static DelegateBridge __Hotfix_get_CurrArmor;
        private static DelegateBridge __Hotfix_set_CurrArmor;
        private static DelegateBridge __Hotfix_get_CurrShield;
        private static DelegateBridge __Hotfix_set_CurrShield;
        private static DelegateBridge __Hotfix_get_DestroyedTime;
        private static DelegateBridge __Hotfix_set_DestroyedTime;
        private static DelegateBridge __Hotfix_get_OffLineBufInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="ShipConfigId", DataFormat=DataFormat.TwosComplement)]
        public int ShipConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="ShipName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string ShipName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public ulong InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="State", DataFormat=DataFormat.TwosComplement)]
        public int State
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="SolarSystemId", DataFormat=DataFormat.TwosComplement)]
        public int SolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(6, IsRequired=false, Name="PlayerCaptain", DataFormat=DataFormat.Default)]
        public ProPlayerSimplestInfo PlayerCaptain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, Name="FlagShipModuleEquip", DataFormat=DataFormat.TwosComplement)]
        public List<int> FlagShipModuleEquip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="Index", DataFormat=DataFormat.TwosComplement)]
        public int Index
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(9, IsRequired=false, Name="UnpackTime", DataFormat=DataFormat.TwosComplement)]
        public long UnpackTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="IsDestroyed", DataFormat=DataFormat.Default)]
        public bool IsDestroyed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="CurrFuel", DataFormat=DataFormat.TwosComplement)]
        public double CurrFuel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(12, IsRequired=true, Name="IsDeleted", DataFormat=DataFormat.Default)]
        public bool IsDeleted
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(13, IsRequired=true, Name="StoreItemIndex", DataFormat=DataFormat.TwosComplement)]
        public int StoreItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(14, IsRequired=true, Name="CurrArmor", DataFormat=DataFormat.TwosComplement)]
        public uint CurrArmor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(15, IsRequired=true, Name="CurrShield", DataFormat=DataFormat.TwosComplement)]
        public uint CurrShield
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x10, IsRequired=false, Name="DestroyedTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long DestroyedTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(0x11, Name="OffLineBufInfo", DataFormat=DataFormat.Default)]
        public List<ProOffLineBufInfo> OffLineBufInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

