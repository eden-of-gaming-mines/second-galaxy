﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="GMServerPlayerInfoQueryReq")]
    public class GMServerPlayerInfoQueryReq : IExtensible
    {
        private string _GameUserId;
        private string _PlayerName;
        private int _QueryType;
        private bool _IsPlayerOnline;
        private int _StartIndex;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GameUserId;
        private static DelegateBridge __Hotfix_set_GameUserId;
        private static DelegateBridge __Hotfix_get_PlayerName;
        private static DelegateBridge __Hotfix_set_PlayerName;
        private static DelegateBridge __Hotfix_get_QueryType;
        private static DelegateBridge __Hotfix_set_QueryType;
        private static DelegateBridge __Hotfix_get_IsPlayerOnline;
        private static DelegateBridge __Hotfix_set_IsPlayerOnline;
        private static DelegateBridge __Hotfix_get_StartIndex;
        private static DelegateBridge __Hotfix_set_StartIndex;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="GameUserId", DataFormat=DataFormat.Default), DefaultValue("")]
        public string GameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="PlayerName", DataFormat=DataFormat.Default), DefaultValue("")]
        public string PlayerName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="QueryType", DataFormat=DataFormat.TwosComplement)]
        public int QueryType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(4, IsRequired=false, Name="IsPlayerOnline", DataFormat=DataFormat.Default)]
        public bool IsPlayerOnline
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="StartIndex", DataFormat=DataFormat.TwosComplement)]
        public int StartIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

