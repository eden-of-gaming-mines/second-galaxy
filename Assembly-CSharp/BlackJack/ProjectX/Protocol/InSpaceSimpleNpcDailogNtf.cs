﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="InSpaceSimpleNpcDailogNtf")]
    public class InSpaceSimpleNpcDailogNtf : IExtensible
    {
        private int _StartDailogId;
        private int _EndDailogId;
        private uint _WatchTargetObjId;
        private ProNpcDNId _ReplaceNpc;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_StartDailogId;
        private static DelegateBridge __Hotfix_set_StartDailogId;
        private static DelegateBridge __Hotfix_get_EndDailogId;
        private static DelegateBridge __Hotfix_set_EndDailogId;
        private static DelegateBridge __Hotfix_get_WatchTargetObjId;
        private static DelegateBridge __Hotfix_set_WatchTargetObjId;
        private static DelegateBridge __Hotfix_get_ReplaceNpc;
        private static DelegateBridge __Hotfix_set_ReplaceNpc;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="StartDailogId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int StartDailogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="EndDailogId", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int EndDailogId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="WatchTargetObjId", DataFormat=DataFormat.TwosComplement)]
        public uint WatchTargetObjId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((string) null), ProtoMember(4, IsRequired=false, Name="ReplaceNpc", DataFormat=DataFormat.Default)]
        public ProNpcDNId ReplaceNpc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

