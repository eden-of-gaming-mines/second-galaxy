﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessEquipPeriodicDamageLaunch")]
    public class ProLBSpaceProcessEquipPeriodicDamageLaunch : IExtensible
    {
        private uint _InstanceId;
        private uint _StartTime;
        private uint _ChargeTime;
        private uint _PeriodicCD;
        private uint _TotalTime;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_ChargeTime;
        private static DelegateBridge __Hotfix_set_ChargeTime;
        private static DelegateBridge __Hotfix_get_PeriodicCD;
        private static DelegateBridge __Hotfix_set_PeriodicCD;
        private static DelegateBridge __Hotfix_get_TotalTime;
        private static DelegateBridge __Hotfix_set_TotalTime;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public uint StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="ChargeTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint ChargeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="PeriodicCD", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public uint PeriodicCD
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(5, IsRequired=false, Name="TotalTime", DataFormat=DataFormat.TwosComplement)]
        public uint TotalTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

