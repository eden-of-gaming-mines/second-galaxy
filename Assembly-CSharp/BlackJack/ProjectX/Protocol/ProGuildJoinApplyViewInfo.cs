﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildJoinApplyViewInfo")]
    public class ProGuildJoinApplyViewInfo : IExtensible
    {
        private ProGuildJoinApplyBasicInfo _BasicInfo;
        private ProPlayerSimplestInfo _PlayerSimplestInfo;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_BasicInfo;
        private static DelegateBridge __Hotfix_set_BasicInfo;
        private static DelegateBridge __Hotfix_get_PlayerSimplestInfo;
        private static DelegateBridge __Hotfix_set_PlayerSimplestInfo;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="BasicInfo", DataFormat=DataFormat.Default)]
        public ProGuildJoinApplyBasicInfo BasicInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="PlayerSimplestInfo", DataFormat=DataFormat.Default)]
        public ProPlayerSimplestInfo PlayerSimplestInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

