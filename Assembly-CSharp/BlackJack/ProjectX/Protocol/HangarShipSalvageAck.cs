﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="HangarShipSalvageAck")]
    public class HangarShipSalvageAck : IExtensible
    {
        private int _Result;
        private int _HangarIndex;
        private bool _IsSalvageShip;
        private bool _UseSalvageDailyCount;
        private int _UseSalvageItemId;
        private int _UseRealMoneyCount;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_HangarIndex;
        private static DelegateBridge __Hotfix_set_HangarIndex;
        private static DelegateBridge __Hotfix_get_IsSalvageShip;
        private static DelegateBridge __Hotfix_set_IsSalvageShip;
        private static DelegateBridge __Hotfix_get_UseSalvageDailyCount;
        private static DelegateBridge __Hotfix_set_UseSalvageDailyCount;
        private static DelegateBridge __Hotfix_get_UseSalvageItemId;
        private static DelegateBridge __Hotfix_set_UseSalvageItemId;
        private static DelegateBridge __Hotfix_get_UseRealMoneyCount;
        private static DelegateBridge __Hotfix_set_UseRealMoneyCount;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Result", DataFormat=DataFormat.TwosComplement)]
        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="HangarIndex", DataFormat=DataFormat.TwosComplement)]
        public int HangarIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="IsSalvageShip", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool IsSalvageShip
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="UseSalvageDailyCount", DataFormat=DataFormat.Default), DefaultValue(false)]
        public bool UseSalvageDailyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(5, IsRequired=false, Name="UseSalvageItemId", DataFormat=DataFormat.TwosComplement)]
        public int UseSalvageItemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(6, IsRequired=false, Name="UseRealMoneyCount", DataFormat=DataFormat.TwosComplement)]
        public int UseRealMoneyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

