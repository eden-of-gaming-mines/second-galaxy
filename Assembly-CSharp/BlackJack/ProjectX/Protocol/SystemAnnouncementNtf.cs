﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="SystemAnnouncementNtf")]
    public class SystemAnnouncementNtf : IExtensible
    {
        private int _Type;
        private int _Id;
        private string _Content;
        private readonly List<string> _ExtraParam;
        private readonly List<ProFormatStringParamInfo> _FormatStrParamList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Type;
        private static DelegateBridge __Hotfix_set_Type;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Content;
        private static DelegateBridge __Hotfix_set_Content;
        private static DelegateBridge __Hotfix_get_ExtraParam;
        private static DelegateBridge __Hotfix_get_FormatStrParamList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Type", DataFormat=DataFormat.TwosComplement)]
        public int Type
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=false, Name="Id", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(3, IsRequired=false, Name="Content", DataFormat=DataFormat.Default)]
        public string Content
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="ExtraParam", DataFormat=DataFormat.Default)]
        public List<string> ExtraParam
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="FormatStrParamList", DataFormat=DataFormat.Default)]
        public List<ProFormatStringParamInfo> FormatStrParamList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

