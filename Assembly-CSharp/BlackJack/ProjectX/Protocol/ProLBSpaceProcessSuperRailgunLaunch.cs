﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProLBSpaceProcessSuperRailgunLaunch")]
    public class ProLBSpaceProcessSuperRailgunLaunch : IExtensible
    {
        private uint _InstanceId;
        private uint _StartTime;
        private uint _BulletFlyTime;
        private readonly List<ProLBProcessSingleUnitLaunchInfo> _BulletList;
        private readonly List<uint> _ExtraTargetObjIdList;
        private readonly List<uint> _ChargeList;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_InstanceId;
        private static DelegateBridge __Hotfix_set_InstanceId;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_get_BulletFlyTime;
        private static DelegateBridge __Hotfix_set_BulletFlyTime;
        private static DelegateBridge __Hotfix_get_BulletList;
        private static DelegateBridge __Hotfix_get_ExtraTargetObjIdList;
        private static DelegateBridge __Hotfix_get_ChargeList;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="InstanceId", DataFormat=DataFormat.TwosComplement)]
        public uint InstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="StartTime", DataFormat=DataFormat.TwosComplement)]
        public uint StartTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(3, IsRequired=false, Name="BulletFlyTime", DataFormat=DataFormat.TwosComplement)]
        public uint BulletFlyTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, Name="BulletList", DataFormat=DataFormat.Default)]
        public List<ProLBProcessSingleUnitLaunchInfo> BulletList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(5, Name="ExtraTargetObjIdList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> ExtraTargetObjIdList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [ProtoMember(6, Name="ChargeList", DataFormat=DataFormat.TwosComplement)]
        public List<uint> ChargeList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

