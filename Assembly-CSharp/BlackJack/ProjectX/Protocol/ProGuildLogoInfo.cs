﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildLogoInfo")]
    public class ProGuildLogoInfo : IExtensible
    {
        private int _LayerId;
        private int _LayerColor;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_LayerId;
        private static DelegateBridge __Hotfix_set_LayerId;
        private static DelegateBridge __Hotfix_get_LayerColor;
        private static DelegateBridge __Hotfix_set_LayerColor;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="LayerId", DataFormat=DataFormat.TwosComplement)]
        public int LayerId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="LayerColor", DataFormat=DataFormat.TwosComplement)]
        public int LayerColor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

