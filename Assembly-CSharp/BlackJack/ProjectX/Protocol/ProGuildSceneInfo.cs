﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildSceneInfo")]
    public class ProGuildSceneInfo : IExtensible
    {
        private ProGuildBuildingInfo _DefenderBuildingInfo;
        private float _AttackerShieldCurr;
        private float _AttackerArmorCurr;
        private uint _AttackerGuildId;
        private float _AttackerShieldMax;
        private float _AttackerArmorMax;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_DefenderBuildingInfo;
        private static DelegateBridge __Hotfix_set_DefenderBuildingInfo;
        private static DelegateBridge __Hotfix_get_AttackerShieldCurr;
        private static DelegateBridge __Hotfix_set_AttackerShieldCurr;
        private static DelegateBridge __Hotfix_get_AttackerArmorCurr;
        private static DelegateBridge __Hotfix_set_AttackerArmorCurr;
        private static DelegateBridge __Hotfix_get_AttackerGuildId;
        private static DelegateBridge __Hotfix_set_AttackerGuildId;
        private static DelegateBridge __Hotfix_get_AttackerShieldMax;
        private static DelegateBridge __Hotfix_set_AttackerShieldMax;
        private static DelegateBridge __Hotfix_get_AttackerArmorMax;
        private static DelegateBridge __Hotfix_set_AttackerArmorMax;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=false, Name="DefenderBuildingInfo", DataFormat=DataFormat.Default), DefaultValue((string) null)]
        public ProGuildBuildingInfo DefenderBuildingInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(2, IsRequired=false, Name="AttackerShieldCurr", DataFormat=DataFormat.FixedSize)]
        public float AttackerShieldCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=false, Name="AttackerArmorCurr", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float AttackerArmorCurr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(4, IsRequired=false, Name="AttackerGuildId", DataFormat=DataFormat.TwosComplement)]
        public uint AttackerGuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="AttackerShieldMax", DataFormat=DataFormat.FixedSize), DefaultValue((float) 0f)]
        public float AttackerShieldMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((float) 0f), ProtoMember(6, IsRequired=false, Name="AttackerArmorMax", DataFormat=DataFormat.FixedSize)]
        public float AttackerArmorMax
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

