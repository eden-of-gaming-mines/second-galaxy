﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProRechargeGiftPackage")]
    public class ProRechargeGiftPackage : IExtensible
    {
        private int _Id;
        private int _Category;
        private int _BuyCountLimitType;
        private int _CurrCycleBuyCountLimit;
        private int _CurrCycleBuyCount;
        private long _nextRefreshTime;
        private long _ExpireTime;
        private bool _IsTriggered;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Id;
        private static DelegateBridge __Hotfix_set_Id;
        private static DelegateBridge __Hotfix_get_Category;
        private static DelegateBridge __Hotfix_set_Category;
        private static DelegateBridge __Hotfix_get_BuyCountLimitType;
        private static DelegateBridge __Hotfix_set_BuyCountLimitType;
        private static DelegateBridge __Hotfix_get_CurrCycleBuyCountLimit;
        private static DelegateBridge __Hotfix_set_CurrCycleBuyCountLimit;
        private static DelegateBridge __Hotfix_get_CurrCycleBuyCount;
        private static DelegateBridge __Hotfix_set_CurrCycleBuyCount;
        private static DelegateBridge __Hotfix_get_NextRefreshTime;
        private static DelegateBridge __Hotfix_set_NextRefreshTime;
        private static DelegateBridge __Hotfix_get_ExpireTime;
        private static DelegateBridge __Hotfix_set_ExpireTime;
        private static DelegateBridge __Hotfix_get_IsTriggered;
        private static DelegateBridge __Hotfix_set_IsTriggered;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Id", DataFormat=DataFormat.TwosComplement)]
        public int Id
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(2, IsRequired=false, Name="Category", DataFormat=DataFormat.TwosComplement)]
        public int Category
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(0), ProtoMember(3, IsRequired=false, Name="BuyCountLimitType", DataFormat=DataFormat.TwosComplement)]
        public int BuyCountLimitType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=false, Name="CurrCycleBuyCountLimit", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrCycleBuyCountLimit
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=false, Name="CurrCycleBuyCount", DataFormat=DataFormat.TwosComplement), DefaultValue(0)]
        public int CurrCycleBuyCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=false, Name="nextRefreshTime", DataFormat=DataFormat.TwosComplement), DefaultValue((long) 0L)]
        public long NextRefreshTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue((long) 0L), ProtoMember(7, IsRequired=false, Name="ExpireTime", DataFormat=DataFormat.TwosComplement)]
        public long ExpireTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(false), ProtoMember(8, IsRequired=false, Name="IsTriggered", DataFormat=DataFormat.Default)]
        public bool IsTriggered
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

