﻿namespace BlackJack.ProjectX.Protocol
{
    using IL;
    using ProtoBuf;
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    [Serializable, ProtoContract(Name="ProGuildSimplestInfo")]
    public class ProGuildSimplestInfo : IExtensible
    {
        private uint _Guid;
        private string _Name;
        private string _CodeName;
        private int _LanguageType;
        private int _MemberCount;
        private int _BaseSolaySystem;
        private int _BaseStation;
        private int _GuildJoinPolicy;
        private ProGuildLogoInfo _LogoInfo;
        private uint _AllianceId;
        private string _AllianceName;
        private string _LeaderGameUserId;
        private string _LeaderGameUserName;
        private IExtension extensionObject;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Guid;
        private static DelegateBridge __Hotfix_set_Guid;
        private static DelegateBridge __Hotfix_get_Name;
        private static DelegateBridge __Hotfix_set_Name;
        private static DelegateBridge __Hotfix_get_CodeName;
        private static DelegateBridge __Hotfix_set_CodeName;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_set_LanguageType;
        private static DelegateBridge __Hotfix_get_MemberCount;
        private static DelegateBridge __Hotfix_set_MemberCount;
        private static DelegateBridge __Hotfix_get_BaseSolaySystem;
        private static DelegateBridge __Hotfix_set_BaseSolaySystem;
        private static DelegateBridge __Hotfix_get_BaseStation;
        private static DelegateBridge __Hotfix_set_BaseStation;
        private static DelegateBridge __Hotfix_get_GuildJoinPolicy;
        private static DelegateBridge __Hotfix_set_GuildJoinPolicy;
        private static DelegateBridge __Hotfix_get_LogoInfo;
        private static DelegateBridge __Hotfix_set_LogoInfo;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_set_AllianceName;
        private static DelegateBridge __Hotfix_get_LeaderGameUserId;
        private static DelegateBridge __Hotfix_set_LeaderGameUserId;
        private static DelegateBridge __Hotfix_get_LeaderGameUserName;
        private static DelegateBridge __Hotfix_set_LeaderGameUserName;
        private static DelegateBridge __Hotfix_ProtoBuf.IExtensible.GetExtensionObject;

        [MethodImpl(0x8000)]
        IExtension IExtensible.GetExtensionObject(bool createIfMissing)
        {
        }

        [ProtoMember(1, IsRequired=true, Name="Guid", DataFormat=DataFormat.TwosComplement)]
        public uint Guid
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(2, IsRequired=true, Name="Name", DataFormat=DataFormat.Default)]
        public string Name
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(3, IsRequired=true, Name="CodeName", DataFormat=DataFormat.Default)]
        public string CodeName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(4, IsRequired=true, Name="LanguageType", DataFormat=DataFormat.TwosComplement)]
        public int LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(5, IsRequired=true, Name="MemberCount", DataFormat=DataFormat.TwosComplement)]
        public int MemberCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(6, IsRequired=true, Name="BaseSolaySystem", DataFormat=DataFormat.TwosComplement)]
        public int BaseSolaySystem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(7, IsRequired=true, Name="BaseStation", DataFormat=DataFormat.TwosComplement)]
        public int BaseStation
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(8, IsRequired=true, Name="GuildJoinPolicy", DataFormat=DataFormat.TwosComplement)]
        public int GuildJoinPolicy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(9, IsRequired=true, Name="LogoInfo", DataFormat=DataFormat.Default)]
        public ProGuildLogoInfo LogoInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(10, IsRequired=true, Name="AllianceId", DataFormat=DataFormat.TwosComplement)]
        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [ProtoMember(11, IsRequired=true, Name="AllianceName", DataFormat=DataFormat.Default)]
        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(12, IsRequired=false, Name="LeaderGameUserId", DataFormat=DataFormat.Default)]
        public string LeaderGameUserId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [DefaultValue(""), ProtoMember(13, IsRequired=false, Name="LeaderGameUserName", DataFormat=DataFormat.Default)]
        public string LeaderGameUserName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

