﻿namespace BlackJack.ProjectX.SceneEditor
{
    using BlackJack.ProjectX;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode]
    public class SpaceEmptyObjectDummy : EditorDummyObjectBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Action<SpaceSimpleObjectDummy> <ReloadDataHandler>k__BackingField;
        public uint Radius;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_ReloadData;
        private static DelegateBridge __Hotfix_GetConfigIdByConfInfo;
        private static DelegateBridge __Hotfix_get_ReloadDataHandler;
        private static DelegateBridge __Hotfix_set_ReloadDataHandler;

        [MethodImpl(0x8000)]
        protected override int GetConfigIdByConfInfo(object m_loadedConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ReloadData()
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        public static Action<SpaceSimpleObjectDummy> ReloadDataHandler
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

