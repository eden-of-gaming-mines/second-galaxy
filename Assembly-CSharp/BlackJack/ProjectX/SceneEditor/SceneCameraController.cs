﻿namespace BlackJack.ProjectX.SceneEditor
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneCameraController : MonoBehaviour
    {
        [Header("俯仰角(上:0-90;下：0- -90)")]
        public float xDegree;
        [Header("水平旋绕角(目标Z轴正方向，顺时针(0-360))")]
        public float yDegree;
        [Header("摄像机偏移(以飞船外包围球半径为单位1)")]
        public float zOffset;
        [Header("摄像机观察的船(手动设置)")]
        public GameObject watchShip;
        [Header("")]
        public Transform yRotationRoot;
        public Transform xRotationRoot;
        public Transform Camera;
        private static DelegateBridge __Hotfix_OnApplySetting;
        private static DelegateBridge __Hotfix_GetShipRadius;

        [MethodImpl(0x8000)]
        protected float GetShipRadius(PrefabShipDesc shipDesc)
        {
        }

        [MethodImpl(0x8000)]
        public void OnApplySetting()
        {
        }
    }
}

