﻿namespace BlackJack.ProjectX.SceneEditor
{
    using BlackJack.ProjectX;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode]
    public class WayPointListDummy : EditorDummyObjectBase
    {
        public bool showLine;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnDrawGizmos;

        [MethodImpl(0x8000)]
        private void OnDrawGizmos()
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }
    }
}

