﻿namespace BlackJack.ProjectX.SceneEditor
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [ExecuteInEditMode]
    public class NpcSpaceShipDummy : EditorDummyObjectBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private static Action<NpcSpaceShipDummy> <ReloadDataHandler>k__BackingField;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_ReloadData;
        private static DelegateBridge __Hotfix_GetConfigIdByConfInfo;
        private static DelegateBridge __Hotfix_get_SpaceShipConfigInfo;
        private static DelegateBridge __Hotfix_set_SpaceShipConfigInfo;
        private static DelegateBridge __Hotfix_get_ReloadDataHandler;
        private static DelegateBridge __Hotfix_set_ReloadDataHandler;

        [MethodImpl(0x8000)]
        protected override int GetConfigIdByConfInfo(object m_loadedConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ReloadData()
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        public ConfigDataSpaceShipInfo SpaceShipConfigInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public static Action<NpcSpaceShipDummy> ReloadDataHandler
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

