﻿namespace BlackJack.ProjectX.SceneEditor
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceSceneRootDesc : MonoBehaviour
    {
        public List<GameObject> SceneObjectList;
        public string SceneObjectPrefabExportPath;
        [SerializeField]
        private int m_dummyObjectCount;
        private static DelegateBridge __Hotfix_get_DummyObjectCount;
        private static DelegateBridge __Hotfix_set_DummyObjectCount;

        public int DummyObjectCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

