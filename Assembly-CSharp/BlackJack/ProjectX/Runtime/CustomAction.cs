﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CustomAction
    {
        private readonly List<Action> m_actions;
        private readonly List<Action> m_loopActions;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddAction;
        private static DelegateBridge __Hotfix_RemoveAction;
        private static DelegateBridge __Hotfix_Invoke;
        private static DelegateBridge __Hotfix_Clear;

        [MethodImpl(0x8000)]
        public void AddAction(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void Invoke()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAction(Action action)
        {
        }
    }
}

