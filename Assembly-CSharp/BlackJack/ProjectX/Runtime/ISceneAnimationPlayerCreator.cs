﻿namespace BlackJack.ProjectX.Runtime
{
    using UnityEngine;

    public interface ISceneAnimationPlayerCreator
    {
        ISceneAnimationPlayerController CreateSceneAnimationPlayerCtrlFromGameObj(GameObject go, SceneAnimationPlayerType playerType);
    }
}

