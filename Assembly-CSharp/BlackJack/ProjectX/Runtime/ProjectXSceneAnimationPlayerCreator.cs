﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ProjectXSceneAnimationPlayerCreator : ISceneAnimationPlayerCreator
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CreateSceneAnimationPlayerCtrlFromGameObj;

        [MethodImpl(0x8000)]
        public ProjectXSceneAnimationPlayerCreator()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public ISceneAnimationPlayerController CreateSceneAnimationPlayerCtrlFromGameObj(GameObject go, SceneAnimationPlayerType playerType)
        {
        }
    }
}

