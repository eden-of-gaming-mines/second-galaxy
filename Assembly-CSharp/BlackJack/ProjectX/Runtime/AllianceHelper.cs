﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AllianceHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_HasAllianceCreationPermission;
        private static DelegateBridge __Hotfix_NoGuildOrIsInAnyAlliance;
        private static DelegateBridge __Hotfix_NotInAnyAlliance;
        private static DelegateBridge __Hotfix_CanOperateLeaderTransfer;
        private static DelegateBridge __Hotfix_CanOperateMemberRemove;
        private static DelegateBridge __Hotfix_CanOperateMemberInvite;
        private static DelegateBridge __Hotfix_CanOperateDismiss;
        private static DelegateBridge __Hotfix_CanOperateMailSend;
        private static DelegateBridge __Hotfix_CanOperateInfoEdit;
        private static DelegateBridge __Hotfix_CanOperateJoinLeaveAlliance;
        private static DelegateBridge __Hotfix_GetPlayerContext;

        [MethodImpl(0x8000)]
        public static bool CanOperateDismiss(AllianceInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanOperateInfoEdit(AllianceInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanOperateJoinLeaveAlliance()
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanOperateLeaderTransfer(AllianceInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanOperateMailSend(AllianceInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanOperateMemberInvite(AllianceInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static bool CanOperateMemberRemove(AllianceInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static ProjectXPlayerContext GetPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasAllianceCreationPermission()
        {
        }

        [MethodImpl(0x8000)]
        public static bool NoGuildOrIsInAnyAlliance()
        {
        }

        [MethodImpl(0x8000)]
        public static bool NotInAnyAlliance()
        {
        }
    }
}

