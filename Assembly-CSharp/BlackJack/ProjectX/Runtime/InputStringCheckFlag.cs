﻿namespace BlackJack.ProjectX.Runtime
{
    using System;

    [Flags]
    public enum InputStringCheckFlag
    {
        OnlyCheck = 1,
        CheckLength = 2,
        CheckSensitiveWord = 4,
        CheckIllegal = 8,
        CheckRange = 0x10,
        CheckAllButSensitiveWord = 0x1b,
        CheckAllAndGetValidStr = 30,
        CheckAll = 0x1f
    }
}

