﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.BJFramework.Runtime;
    using System;

    public class ProjectXGameClientSetting : GameClientSetting
    {
        public bool DisableUserGuide;
        public bool EnableUserGuideLog;
        public string TranslateServerUrl = "http://192.168.1.10:8096/Translate";
    }
}

