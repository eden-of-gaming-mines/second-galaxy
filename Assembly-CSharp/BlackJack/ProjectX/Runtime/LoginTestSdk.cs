﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class LoginTestSdk : MonoBehaviour
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnInitEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnLoginEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnLogoutEnd;
        private static LoginTestSdk m_instance;
        public List<string> m_loginResultList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsInit>k__BackingField;
        [Header("SDK userId")]
        public string m_loginUserId;
        [Header("SDK初始化结果")]
        public bool m_sdkInitFailed;
        [Header("SDK登录结果")]
        public bool m_sdkLoginFailed;
        [Header("SDK登录返回结果信息错误")]
        public bool m_sdkLoginResultError;
        [Header("SDK登出结果")]
        public bool m_sdkLogoutFailed;
        [Header("SDK是否重新登录")]
        public bool m_sdkNeedRelogin;
        [Header("SDK LoginAgent www下载结果")]
        public bool m_loginAgentLoginWWWResultError;
        [Header("SDK LoginAgent www 错误字符串")]
        public string m_loginAgentLoginWWWErrorStr;
        [Header("SDK LoginAgent 结果解析")]
        public bool m_loginAgentLoginResultParseError;
        [Header("SDK LoginAgent 结果错误信息")]
        public string m_loginAgentLoginResultErrStr;
        [Header("SDK LoginAgent 结果")]
        public bool m_loginAgentLoginResultStateError;
        [Header("Bundle版本号检查失败")]
        public bool m_bundleVersionCheckFailed;
        [Header("服务器不可用")]
        public bool m_serverDisactive;
        [Header("发送LoginAuthToken 错误")]
        public bool m_sendLoginAuthTokenFailed;
        [Header("LoginAuthToken 登录错误")]
        public bool m_loginAuthTokenFailed;
        [Header("LoginAuthToken 登录错误码")]
        public int m_loginAuthTokenErrorCode;
        [Header("发送LoginSessionToken 错误")]
        public bool m_sendLoginSessionTokenFailed;
        [Header("LoginSessionToken 登录错误")]
        public bool m_loginSessionTokenFailed;
        [Header("LoginSessionToken 登录错误码")]
        public int m_loginSessionTokenErrorCode;
        [Header("EnterWorld错误")]
        public bool m_enterWorldFailed;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Login;
        private static DelegateBridge __Hotfix_LoginImp;
        private static DelegateBridge __Hotfix_Logout;
        private static DelegateBridge __Hotfix_DelayExecute;
        private static DelegateBridge __Hotfix_add_EventOnInitEnd;
        private static DelegateBridge __Hotfix_remove_EventOnInitEnd;
        private static DelegateBridge __Hotfix_add_EventOnLoginEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLoginEnd;
        private static DelegateBridge __Hotfix_add_EventOnLogoutEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLogoutEnd;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_get_IsInit;
        private static DelegateBridge __Hotfix_set_IsInit;

        public event Action<bool> EventOnInitEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnLoginEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnLogoutEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator DelayExecute(Action action)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void Login()
        {
        }

        [MethodImpl(0x8000)]
        private void LoginImp()
        {
        }

        [MethodImpl(0x8000)]
        public void Logout()
        {
        }

        public static LoginTestSdk Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsInit
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DelayExecute>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action action;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

