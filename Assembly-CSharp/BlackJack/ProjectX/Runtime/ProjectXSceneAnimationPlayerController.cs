﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.BJFramework.Runtime.Scene;
    using IL;
    using Slate;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ProjectXSceneAnimationPlayerController : ISceneAnimationPlayerController
    {
        protected Action<string, object> m_onCutsceneMessage;
        protected GameObject m_cutSceneGo;
        protected Cutscene m_cutScene;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_GetRenderCamera;
        private static DelegateBridge __Hotfix_EnableRenderCamera;
        private static DelegateBridge __Hotfix_SetLocation;
        private static DelegateBridge __Hotfix_SetRotation;
        private static DelegateBridge __Hotfix_SetLensFlareCamera;
        private static DelegateBridge __Hotfix_GetLayerRenderSettingDesc;
        private static DelegateBridge __Hotfix_GetDirectorCameraLocation;
        private static DelegateBridge __Hotfix_GetDirectorCameraRotate;
        private static DelegateBridge __Hotfix_GetDirectorCameraFov;
        private static DelegateBridge __Hotfix_RegisterSceneAnimationMessageHandler;
        private static DelegateBridge __Hotfix_RegisterSceneAnimationShipActionHandler;
        private static DelegateBridge __Hotfix_GetTimelineBindInfoRequest;
        private static DelegateBridge __Hotfix_SetTimelineBindInfos;
        private static DelegateBridge __Hotfix_OnCutsceneMessage;

        [MethodImpl(0x8000)]
        public ProjectXSceneAnimationPlayerController(GameObject cutSceneGo)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableRenderCamera(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public float GetDirectorCameraFov()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetDirectorCameraLocation()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetDirectorCameraRotate()
        {
        }

        [MethodImpl(0x8000)]
        public LayerRenderSettingDesc GetLayerRenderSettingDesc()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetRenderCamera()
        {
        }

        [MethodImpl(0x8000)]
        public List<TimelineBindInfo> GetTimelineBindInfoRequest()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCutsceneMessage(string msg, object obj)
        {
        }

        [MethodImpl(0x8000)]
        public void Play()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterSceneAnimationMessageHandler(Action<string, object> onMessage)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterSceneAnimationShipActionHandler(Action<TimelineShipActionInfo> actionHandler)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetLensFlareCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocation(Vector3 location)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRotation(Quaternion rotate)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTimelineBindInfos(List<TimelineBindInfo> bindInfos)
        {
        }

        [MethodImpl(0x8000)]
        public void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }
    }
}

