﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class IosPromotingPurchaseTask : RechargeGoodsPurchaseTaskBase
    {
        private readonly string m_goodsResiterId;
        private RechargeGoodsType m_goodsType;
        private int m_goodsConfigId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckCanDoPurchase;
        private static DelegateBridge __Hotfix_IsIosPromoting;
        private static DelegateBridge __Hotfix_SendPurchaseReq;
        private static DelegateBridge __Hotfix_GetGoodsFilter;
        private static DelegateBridge __Hotfix_SendPayCancelOrFailedReq;
        private static DelegateBridge __Hotfix_CheckRechargeGoodsState;
        private static DelegateBridge __Hotfix_CheckIosPromotingAppleSubscribe;
        private static DelegateBridge __Hotfix_SendIosPromotingAppleSubscribeReq;

        [MethodImpl(0x8000)]
        public IosPromotingPurchaseTask(string registerId, int count)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool CheckCanDoPurchase()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckIosPromotingAppleSubscribe(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected override IEnumerator CheckRechargeGoodsState(RechargeGoodsInfo goodsInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool GetGoodsFilter(RechargeGoodsInfo goods)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsIosPromoting()
        {
        }

        [MethodImpl(0x8000)]
        private void SendIosPromotingAppleSubscribeReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SendPayCancelOrFailedReq(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SendPurchaseReq(Action<bool> onEnd)
        {
        }

        [CompilerGenerated]
        private sealed class <CheckIosPromotingAppleSubscribe>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0()
            {
                PDSDK.Instance.m_eventOnAllowBuySubscription = null;
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CheckRechargeGoodsState>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal RechargeGoodsInfo goodsInfo;
            internal Action<bool> onEnd;
            internal IosPromotingPurchaseTask $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

