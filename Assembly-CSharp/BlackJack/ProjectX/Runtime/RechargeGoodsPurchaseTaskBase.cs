﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public abstract class RechargeGoodsPurchaseTaskBase : Task
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<PurchaseRechargeGoodsResult> EventOnPurchaseEnd;
        protected TinyCorutineHelper m_corutineHelper;
        protected RechargeGoodsInfo m_goodsInfo;
        protected int m_count;
        protected bool m_isPromoting;
        protected ulong m_orderInstanceId;
        protected DateTime m_timeoutCheckTime;
        private const float RECHARGETIMEOUT = 5f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_PurchaseRechargeGood;
        private static DelegateBridge __Hotfix_CheckPaymentState;
        private static DelegateBridge __Hotfix_CheckRechargeGoodsState;
        private static DelegateBridge __Hotfix_RegisterPaymentCallback;
        private static DelegateBridge __Hotfix_UnregisterPaymentCallback;
        private static DelegateBridge __Hotfix_GetRechargeGoodsInfo;
        private static DelegateBridge __Hotfix_OnPayCallback;
        private static DelegateBridge __Hotfix_OnPayCancel;
        private static DelegateBridge __Hotfix_OnTimeout;
        private static DelegateBridge __Hotfix_OnPurchaseEnd;
        private static DelegateBridge __Hotfix_add_EventOnPurchaseEnd;
        private static DelegateBridge __Hotfix_remove_EventOnPurchaseEnd;
        private static DelegateBridge __Hotfix_get_SdkInterface;

        public event Action<PurchaseRechargeGoodsResult> EventOnPurchaseEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public RechargeGoodsPurchaseTaskBase(int count = 1)
        {
        }

        protected abstract bool CheckCanDoPurchase();
        [MethodImpl(0x8000)]
        private bool CheckPaymentState()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected virtual IEnumerator CheckRechargeGoodsState(RechargeGoodsInfo goodsInfo, Action<bool> onEnd)
        {
        }

        protected abstract bool GetGoodsFilter(RechargeGoodsInfo goods);
        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator GetRechargeGoodsInfo(Action<bool> onEnd)
        {
        }

        protected abstract bool IsIosPromoting();
        [MethodImpl(0x8000)]
        private void OnPayCallback(bool isSuccess)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPayCancel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPurchaseEnd(PurchaseRechargeGoodsResult result)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(object param, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTimeout()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator PurchaseRechargeGood()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPaymentCallback()
        {
        }

        protected abstract void SendPayCancelOrFailedReq(Action onEnd);
        protected abstract void SendPurchaseReq(Action<bool> onEnd);
        [MethodImpl(0x8000)]
        private void UnregisterPaymentCallback()
        {
        }

        protected LogicBlockClientSdkInterface SdkInterface
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckRechargeGoodsState>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal RechargeGoodsInfo goodsInfo;
            internal Action<bool> onEnd;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.goodsInfo == null)
                        {
                            if (this.onEnd != null)
                            {
                                this.onEnd(false);
                            }
                            break;
                        }
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <GetRechargeGoodsInfo>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal Action<bool> onEnd;
            internal RechargeGoodsPurchaseTaskBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <GetRechargeGoodsInfo>c__AnonStorey4 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$locvar0 = new <GetRechargeGoodsInfo>c__AnonStorey4();
                        this.$locvar0.<>f__ref$2 = this;
                        this.$locvar0.lret = null;
                        this.$this.SdkInterface.RequestSdkRechargeGoodsInfoList(new Action<bool>(this.$locvar0.<>m__0));
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.$locvar0.lret == null)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                if (!this.$locvar0.lret.Value)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    this.$this.m_goodsInfo = this.$this.SdkInterface.GetRechargeGoodsInfo(new Predicate<RechargeGoodsInfo>(this.$this.GetGoodsFilter));
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                    this.$PC = -1;
                }
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <GetRechargeGoodsInfo>c__AnonStorey4
            {
                internal bool? lret;
                internal RechargeGoodsPurchaseTaskBase.<GetRechargeGoodsInfo>c__Iterator2 <>f__ref$2;

                internal void <>m__0(bool ret)
                {
                    this.lret = new bool?(ret);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PurchaseRechargeGood>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal RechargeGoodsPurchaseTaskBase $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;
            private <PurchaseRechargeGood>c__AnonStorey3 $locvar0;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;

            private sealed class <PurchaseRechargeGood>c__AnonStorey3
            {
                internal bool? lret;
                internal RechargeGoodsPurchaseTaskBase.<PurchaseRechargeGood>c__Iterator0 <>f__ref$0;

                internal void <>m__0(bool ret)
                {
                    this.lret = new bool?(ret);
                }

                internal void <>m__1(bool ret)
                {
                    this.lret = new bool?(ret);
                }

                internal void <>m__2(bool ret)
                {
                    this.lret = new bool?(ret);
                }
            }
        }
    }
}

