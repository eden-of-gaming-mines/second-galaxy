﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ByteArrayFactory
    {
        private readonly Stack<byte[]> m_pool;
        private const int BytesForPoint = 3;
        private readonly int m_length;
        private const int PoolDepth = 0x10;
        private static ByteArrayFactory m_instance;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_Request;
        private static DelegateBridge __Hotfix_Recycle;
        private static DelegateBridge __Hotfix_DestroyPool;

        [MethodImpl(0x8000)]
        public void DestroyPool()
        {
        }

        [MethodImpl(0x8000)]
        public void Recycle(byte[] bytes)
        {
        }

        [MethodImpl(0x8000)]
        public byte[] Request()
        {
        }

        public static ByteArrayFactory Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

