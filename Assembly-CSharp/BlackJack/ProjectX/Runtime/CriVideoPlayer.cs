﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CriVideoPlayer : IVideoPlayer
    {
        protected bool m_isPlaying;
        protected Action m_endAction;
        protected Text m_subTitleText;
        protected CriManaMovieControllerForUI m_videoPlayer;
        protected const int SubtitleBufferSize = 0x100;
        protected byte[] m_lastSubtitleBuffer;
        protected byte[] m_currSubtitleBuffer;
        protected byte[] m_subtitleBuffer1;
        protected byte[] m_subtitleBuffer2;
        private const int ChineseSubtitleChannel = 0;
        private const int EnglishSubtitleChannel = 1;
        private const int TraditionalChSubtitleChannel = 2;
        private const int ChineseAudioTrack = 0;
        private const int EnglishAndioTrack = 1;
        private const int TraditionalChAndioTrack = 1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_PlayCG;
        private static DelegateBridge __Hotfix_RegisterEndAction;
        private static DelegateBridge __Hotfix_StopCG;
        private static DelegateBridge __Hotfix_CGSettingBeforePlay;
        private static DelegateBridge __Hotfix_IsSubtitileBuffTheSame;
        private static DelegateBridge __Hotfix_OnCgEnd;
        private static DelegateBridge __Hotfix_OnSoundMute;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public CriVideoPlayer(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        protected void CGSettingBeforePlay(string cgResPath)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSubtitileBuffTheSame(byte[] srcBuff, byte[] destBuff, int size)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCgEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundMute(bool isMute)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCG(string cgResPath)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterEndAction(Action endAction)
        {
        }

        [MethodImpl(0x8000)]
        public void StopCG()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        protected ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

