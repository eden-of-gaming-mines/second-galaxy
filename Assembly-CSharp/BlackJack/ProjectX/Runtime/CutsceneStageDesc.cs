﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ProjectX.Runtime.Timeline;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CutsceneStageDesc : TimelineCustomEventHandler
    {
        [Header("是否创建fake演员")]
        public bool IsCreateFakeActor;
        [Header("是否使用演员位置重置动画位子")]
        public bool IsRelocationByActor;
        [Header("用于重置动画位子的演员")]
        public CutsceneActorDesc ActorToRelocation;
        [Header("飞船事件处理器")]
        public TimelineShipActionEventHandler m_shipActionEventHandler;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string> EventOnCutSceneMessage;
        private static DelegateBridge __Hotfix_OnCustomEvent;
        private static DelegateBridge __Hotfix_add_EventOnCutSceneMessage;
        private static DelegateBridge __Hotfix_remove_EventOnCutSceneMessage;

        public event Action<string> EventOnCutSceneMessage
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void OnCustomEvent(string msg)
        {
        }
    }
}

