﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ConfigData;
    using Boo.Lang;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;

    public class InputStringValidCheckHelper
    {
        private static Dictionary<char, float> m_char2LengthDict = new Dictionary<char, float>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckInputStringValid;
        private static DelegateBridge __Hotfix_PostProcessInputText;
        private static DelegateBridge __Hotfix_CheckInputStringLength;
        private static DelegateBridge __Hotfix_CheckInputStringSensitiveWord;
        private static DelegateBridge __Hotfix_CheckInputStringIllegal;
        private static DelegateBridge __Hotfix_CheckInputStringCharRange;
        private static DelegateBridge __Hotfix_GetSingleCharCheckLength;

        [MethodImpl(0x8000)]
        public static InputStringCheckResult CheckInputStringCharRange(string inputString, System.Collections.Generic.List<int> validRangeIdList)
        {
        }

        [MethodImpl(0x8000)]
        public static InputStringCheckResult CheckInputStringIllegal(string inputString)
        {
        }

        [MethodImpl(0x8000)]
        public static InputStringCheckResult CheckInputStringLength(string inputString, int lengthLimit, bool isOnlyCheck, out string validString)
        {
        }

        [MethodImpl(0x8000)]
        public static InputStringCheckResult CheckInputStringSensitiveWord(string inputString)
        {
        }

        [MethodImpl(0x8000)]
        public static InputStringCheckResult CheckInputStringValid(string inputString, int lengthLimit, InputStringCheckFlag checkFlag, out string validString, InputType inputType = 8, Regex customRegex = null, Function<string, bool> customRule = null)
        {
        }

        [MethodImpl(0x8000)]
        private static float GetSingleCharCheckLength(char singleChar)
        {
        }

        [MethodImpl(0x8000)]
        public static string PostProcessInputText(string rawText, int lengthLimit, bool replaceSensitive)
        {
        }
    }
}

