﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimationPlayerHelper
    {
        protected static ISceneAnimationPlayerCreator m_sceneAnimationCreator;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegistSceneAnimationCreator;
        private static DelegateBridge __Hotfix_CreateSceneAnimationCtrlFromGameObj;

        [MethodImpl(0x8000)]
        public static ISceneAnimationPlayerController CreateSceneAnimationCtrlFromGameObj(GameObject go, SceneAnimationPlayerType playerType)
        {
        }

        [MethodImpl(0x8000)]
        public static void RegistSceneAnimationCreator(ISceneAnimationPlayerCreator creator)
        {
            DelegateBridge bridge = __Hotfix_RegistSceneAnimationCreator;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(creator);
            }
            else
            {
                m_sceneAnimationCreator = creator;
            }
        }
    }
}

