﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.BJFramework.Runtime.Scene;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public interface ISceneAnimationPlayerController
    {
        void EnableRenderCamera(bool isEnable);
        float GetDirectorCameraFov();
        Vector3 GetDirectorCameraLocation();
        Quaternion GetDirectorCameraRotate();
        LayerRenderSettingDesc GetLayerRenderSettingDesc();
        Camera GetRenderCamera();
        List<TimelineBindInfo> GetTimelineBindInfoRequest();
        void Play();
        void RegisterSceneAnimationMessageHandler(Action<string, object> onMessage);
        void RegisterSceneAnimationShipActionHandler(Action<TimelineShipActionInfo> actionHandler);
        void Release();
        void SetLensFlareCamera(Camera camera);
        void SetLocation(Vector3 location);
        void SetRotation(Quaternion rotate);
        void SetTimelineBindInfos(List<TimelineBindInfo> bindInfos);
        void Stop();
        void Tick();
    }
}

