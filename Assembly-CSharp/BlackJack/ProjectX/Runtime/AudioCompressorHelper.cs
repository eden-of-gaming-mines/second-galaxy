﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AudioCompressorHelper
    {
        protected static IAudioCompressor m_audioCompressor;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Compress;
        private static DelegateBridge __Hotfix_Decompress;
        private static DelegateBridge __Hotfix_get_AudioCompressor;
        private static DelegateBridge __Hotfix_set_AudioCompressor;

        [MethodImpl(0x8000)]
        public static byte[] Compress(float[] audioData)
        {
        }

        [MethodImpl(0x8000)]
        public static float[] Decompress(byte[] compressData, int audioDatalength)
        {
        }

        public static IAudioCompressor AudioCompressor
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
                DelegateBridge bridge = __Hotfix_set_AudioCompressor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(value);
                }
                else
                {
                    m_audioCompressor = value;
                }
            }
        }
    }
}

