﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class GraphicSettingHelper
    {
        private const int QualityLevelPcFantastic = 0;
        private const int QualityLevelIosLow = 0;
        private const int QualityLevelIosMedium = 1;
        private const int QualityLevelIosHigh = 2;
        private const int QualityLevelAndroidLow = 0;
        private const int QualityLevelAndroidMedium = 1;
        private const int QualityLevelAndroidHigh = 2;
        private static ConfigDataDeviceSetting m_deviceSetting;
        private static bool m_needUpdateDeviceSetting = true;
        private const float MarginFixX = 88f;
        private const float MarginFixY = 40f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ChangeQualitySettingLevel;
        private static DelegateBridge __Hotfix_SetShadowDistance;
        private static DelegateBridge __Hotfix_SetGraphicSettingLevel;
        private static DelegateBridge __Hotfix_GetQualityLevelByGraphicQualityLevel4Android;
        private static DelegateBridge __Hotfix_GetQualityLevelByQualityLevel4Ios;
        private static DelegateBridge __Hotfix_GetConfigDataDeviceSetting;
        private static DelegateBridge __Hotfix_MarginAdjustHorizontal;
        private static DelegateBridge __Hotfix_MarginAdjustVertical;

        [MethodImpl(0x8000)]
        public static void ChangeQualitySettingLevel(bool isIncrease)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataDeviceSetting GetConfigDataDeviceSetting()
        {
        }

        [MethodImpl(0x8000)]
        private static int GetQualityLevelByGraphicQualityLevel4Android(GraphicQualityLevel graphicQualityLevel)
        {
        }

        [MethodImpl(0x8000)]
        private static int GetQualityLevelByQualityLevel4Ios(GraphicQualityLevel graphicQualityLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static bool MarginAdjustHorizontal(RectTransform rt)
        {
        }

        [MethodImpl(0x8000)]
        public static bool MarginAdjustVertical(RectTransform rt)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetGraphicSettingLevel(GraphicQualityLevel graphicQualityLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetShadowDistance(float distance)
        {
        }
    }
}

