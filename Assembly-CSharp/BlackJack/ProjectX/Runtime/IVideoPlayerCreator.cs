﻿namespace BlackJack.ProjectX.Runtime
{
    using UnityEngine;

    public interface IVideoPlayerCreator
    {
        IVideoPlayer CreateVideoPlayer(GameObject go);
    }
}

