﻿namespace BlackJack.ProjectX.Runtime
{
    using System;
    using UnityEngine;

    public class CutsceneActorDesc : MonoBehaviour
    {
        [Header("演员名")]
        public string ActorName;
        [Header("演员挂载点")]
        public Transform ActorDummy;
        [Range(0f, 1f), Header("锁定的LODLevel")]
        public int FixLODLevel;
    }
}

