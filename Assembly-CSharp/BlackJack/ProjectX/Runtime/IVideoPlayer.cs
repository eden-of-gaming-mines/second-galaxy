﻿namespace BlackJack.ProjectX.Runtime
{
    using System;

    public interface IVideoPlayer
    {
        void PlayCG(string cgResPath);
        void RegisterEndAction(Action endAction);
        void StopCG();
        void Tick();
    }
}

