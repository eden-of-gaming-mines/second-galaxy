﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using BlackJack.ProjectX.Runtime.Timeline;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CutsceneShipActionHandler
    {
        protected Camera m_camera;
        protected Transform m_bulletRoot;
        protected List<BulletObjectController> m_bulletCtrlList;
        protected static TypeDNName m_bulletRailgunTypeDNName;
        protected static TypeDNName m_bulletCannonTypeDNName;
        protected static TypeDNName m_bulletPlasmaTypDNName;
        protected static TypeDNName m_bulletSuperMissileTypeDNName;
        protected static TypeDNName m_bulletMissileTypeDNName;
        protected static TypeDNName m_bulletLaserTypeDNName;
        protected static TypeDNName m_bulletDroneTypeName;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnShipAction;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_SetCamera;
        private static DelegateBridge __Hotfix_SetBulletRoot;
        private static DelegateBridge __Hotfix_OnShipDeath;
        private static DelegateBridge __Hotfix_OnShipAttackTarget;
        private static DelegateBridge __Hotfix_OnShipCharge;
        private static DelegateBridge __Hotfix_OnRailgunWeaponGroupUintFire;
        private static DelegateBridge __Hotfix_OnPlasmWeaponGroupUintFire;
        private static DelegateBridge __Hotfix_OnCannonWeaponGroupUnitFire;
        private static DelegateBridge __Hotfix_OnMissileWeaponGroupUnitFire;
        private static DelegateBridge __Hotfix_OnLaserWeaponGroupUnitFire;
        private static DelegateBridge __Hotfix_SetTraceTargetToWeaponCtrl;
        private static DelegateBridge __Hotfix_CalcBulletFlyTime;
        private static DelegateBridge __Hotfix_CreateBullet;
        private static DelegateBridge __Hotfix_StartWeaponCharge;
        private static DelegateBridge __Hotfix_AddBullet;
        private static DelegateBridge __Hotfix_TickBullet;
        private static DelegateBridge __Hotfix_DestroyBullet;
        private static DelegateBridge __Hotfix_OnShipSuperAttackTarget;
        private static DelegateBridge __Hotfix_OnShipSuperCharge;
        private static DelegateBridge __Hotfix_OnSuperRailgunWeaponGroupUnitFire;
        private static DelegateBridge __Hotfix_OnSuperPlasmaWeaponGroupUnitFire;
        private static DelegateBridge __Hotfix_OnSuperMissileWeaponGroupUnitFire;
        private static DelegateBridge __Hotfix_OnSuperLaserWeaponGroupUnitFire;
        private static DelegateBridge __Hotfix_CreateSuperRailgunBullet;
        private static DelegateBridge __Hotfix_StartSuperWeaponCharge;
        private static DelegateBridge __Hotfix_SetTraceTargetToSuperWeaponCtrl;

        [MethodImpl(0x8000)]
        public void AddBullet(BulletObjectController bulletCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcBulletFlyTime(SpaceObjectControllerBase srcTarget, SpaceObjectControllerBase destTarget, float bulletSpeed)
        {
        }

        [MethodImpl(0x8000)]
        protected BulletObjectController CreateBullet(BulletLaunchInfo bulletLaunchInfo, WeaponCategory weaponCategory, BulletTargetProviderWarpper bulletTarget, float bulletFlyTime, bool isHit, bool isSuper = false)
        {
        }

        [MethodImpl(0x8000)]
        protected BulletRailgunController CreateSuperRailgunBullet(BulletLaunchInfo bulletLaunchInfo, WeaponCategory weaponCategory, BulletTargetProviderWarpper bulletTarget, float bulletFlyTime, bool isHit, bool isBulletRebound = false)
        {
        }

        [MethodImpl(0x8000)]
        public void DestroyBullet()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCannonWeaponGroupUnitFire(SpaceShipController srcShip, SpaceShipController targetShip, LBWeaponGroupCannonClient weaponGroup, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLaserWeaponGroupUnitFire(SpaceShipController srcShip, SpaceShipController targetShip, LBWeaponGroupLaserClient weaponGroup, ushort unitIndex, float lifeTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMissileWeaponGroupUnitFire(SpaceShipController srcShip, SpaceShipController targetShip, LBWeaponGroupMissileClient weaponGroup, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlasmWeaponGroupUintFire(SpaceShipController srcShip, SpaceShipController targetShip, LBWeaponGroupPlasmaClient weaponGroup, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRailgunWeaponGroupUintFire(SpaceShipController srcShip, SpaceShipController targetShip, LBWeaponGroupRailgunClient weaponGroup, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipAction(SpaceShipController srcShip, List<SpaceShipController> targetShips, TimelineShipActionInfo actionInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipAttackTarget(SpaceShipController srcShip, SpaceShipController targetShip, TimelineShipActionInfo actionInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipCharge(SpaceShipController srcShip, SpaceShipController targetShip, TimelineShipActionInfo actionInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipDeath(SpaceShipController ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipSuperAttackTarget(SpaceShipController srcShip, List<SpaceShipController> targetShips, TimelineShipActionInfo actionInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipSuperCharge(SpaceShipController srcShip, SpaceShipController targetShip, TimelineShipActionInfo actionInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSuperLaserWeaponGroupUnitFire(SpaceShipController srcShip, List<SpaceShipController> targetShips, LBSuperWeaponGroupLaserClient weaponGroup, ushort unitIndex, float lifeTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSuperMissileWeaponGroupUnitFire(SpaceShipController srcShip, SpaceShipController targetShip, LBSuperWeaponGroupMissileClient weaponGroup, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSuperPlasmaWeaponGroupUnitFire(SpaceShipController srcShip, SpaceShipController targetShip, LBSuperWeaponGroupPlasmaClient weaponGroup, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSuperRailgunWeaponGroupUnitFire(SpaceShipController srcShip, SpaceShipController targetShip, LBSuperWeaponGroupRailgunClient weaponGroup, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletRoot(Transform root)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetTraceTargetToSuperWeaponCtrl(IShipSuperWeaponController weaponCtrl, IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetTraceTargetToWeaponCtrl(IShipWeaponController weaponCtrl, int groupIndex, IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartSuperWeaponCharge(SpaceShipController srcShip, SpaceShipController targetShip, LBSuperWeaponGroupBase weaponGroup, ushort unitIndex, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartWeaponCharge(SpaceShipController srcShip, SpaceShipController targetShip, LBWeaponGroupBase weaponGroup, ushort unitIndex, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        public void TickBullet()
        {
        }
    }
}

