﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public static class UnityUtil
    {
        private static DelegateBridge __Hotfix_SetGameObjectActive;
        private static DelegateBridge __Hotfix_DestroyChildren;
        private static DelegateBridge __Hotfix_SupportH264;
        private static DelegateBridge __Hotfix_ResizeTexture;
        private static DelegateBridge __Hotfix_SaveShareTextureToFile;

        [MethodImpl(0x8000)]
        public static void DestroyChildren(GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public static Texture2D ResizeTexture(Texture2D pSource, ImageFilterMode pFilterMode, float pScale)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerator SaveShareTextureToFile(Texture2D shareTexture, string imageFilePath, string thumbImageFilePath, float thumbImageWidth, int imageQuality = 100)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetGameObjectActive(GameObject obj, bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public static bool SupportH264()
        {
        }

        [CompilerGenerated]
        private sealed class <SaveShareTextureToFile>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal string imageFilePath;
            internal Texture2D shareTexture;
            internal int imageQuality;
            internal string thumbImageFilePath;
            internal float thumbImageWidth;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (string.IsNullOrEmpty(this.imageFilePath))
                        {
                            Debug.LogError("CaptureScreen: imageFilePath can't null!");
                        }
                        else if (this.shareTexture != null)
                        {
                            try
                            {
                                File.WriteAllBytes(this.imageFilePath, this.shareTexture.EncodeToJPG(this.imageQuality));
                                if (!string.IsNullOrEmpty(this.thumbImageFilePath))
                                {
                                    Texture2D tex = UnityUtil.ResizeTexture(this.shareTexture, UnityUtil.ImageFilterMode.Average, this.thumbImageWidth / ((float) this.shareTexture.width));
                                    File.WriteAllBytes(this.thumbImageFilePath, tex.EncodeToJPG(this.imageQuality));
                                    UnityEngine.Object.Destroy(tex);
                                }
                            }
                            catch (Exception exception)
                            {
                                Debug.LogError($"{"UnityUtil.SaveShareTextureToFile"} : {exception}");
                            }
                            this.$current = new WaitForSeconds(0.5f);
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                            return true;
                        }
                        break;

                    case 1:
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public enum ImageFilterMode
        {
            Nearest,
            Biliner,
            Average
        }
    }
}

