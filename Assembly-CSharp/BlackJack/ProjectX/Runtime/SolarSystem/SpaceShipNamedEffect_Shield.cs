﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipNamedEffect_Shield : SpaceShipNamedEffect
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_StartShieldRecoverEffect;
        private static DelegateBridge __Hotfix_StartShieldRecoverEffectImp;
        private static DelegateBridge __Hotfix_StopShieldRecoverEffect;
        private static DelegateBridge __Hotfix_StopShieldRecoverEffectImp;
        private static DelegateBridge __Hotfix_StartShieldResistUpEffect;
        private static DelegateBridge __Hotfix_StartShieldResistDownEffect;
        private static DelegateBridge __Hotfix_OnPostShipLODLevelChanged;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_Shield(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPostShipLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDic, ISpaceTargetProvider targett)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShieldRecoverEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShieldRecoverEffectImp()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShieldResistDownEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShieldResistUpEffect()
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void StopShieldRecoverEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopShieldRecoverEffectImp()
        {
        }
    }
}

