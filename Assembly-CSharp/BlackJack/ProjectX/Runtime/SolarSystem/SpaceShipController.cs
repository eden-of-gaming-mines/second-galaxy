﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceShipController : SpaceLODObjectController, ISpaceShipController, IBulletTargetProvider, ISpaceTargetProvider
    {
        protected CustomAction<object> m_eventOnShipDestory;
        private CRIAudioSourceHelperImpl m_audioPlayer;
        public GameObject m_shipRoot;
        [AutoBind("./ShipRoot", AutoBindAttribute.InitState.NotInit, true)]
        public PrefabSuperWeaponEffectDesc m_superWeaponEffectDesc;
        [AutoBind("./ShipRoot", AutoBindAttribute.InitState.NotInit, true)]
        public PrefabWeaponEffectDesc m_normalWeaponEffectDesc;
        [AutoBind("./ShipRoot", AutoBindAttribute.InitState.NotInit, true)]
        public PrefabShipDroneInfoDesc m_prefabShipDroneInfoDesc;
        protected ShipEffectController m_shipEffectCtrl;
        protected ShipBuffEffectController m_shipBuffEffectCtrl;
        protected ShipSuperEquipController m_shipSuperEquipEffectCtrl;
        protected ShipTacticalEquipController m_shipTacticalEquipEffectCtrl;
        protected ShipLocalMovementController m_shipLocalMovementCtrl;
        protected ShipSuperLaserEffectController m_shipSuperLaserEffectCtrl;
        protected SpaceShipGuildBuildingController m_spaceShipGuildBuildingCtrl;
        protected List<BulletDroneController> m_launchDroneCtrlList;
        protected List<BulletLaserController> m_launchLaserCtrlList;
        protected float m_shipRadius;
        protected bool m_isShipPartVisible;
        protected float m_forceActiveLeftTime;
        protected CachedShipAnimatorStateInfo m_cachedShipAnimatorStateInfo;
        protected const float ForceActiveDelayTime = 5f;
        protected int m_nextLaunchDroneRallyPointIndex;
        protected int m_droneRallyPointCount;
        public static string ShipAnimatorParam_Rest;
        public static string ShipAnimatorParam_Jump;
        public static string ShipAnimatorParam_SuperWeaponLaunch;
        public static string ShipAnimatorParam_IsDead;
        public static string ShipAnimatorParam_FakeJumpIn;
        public static string ShipAnimatorParam_FakeJumpOut;
        public static string ShipAnimatorParam_Custom0;
        public static string ShipAnimatorParam_Custom1;
        public static string ShipAnimatorParam_Custom2;
        public static string ShipAnimatorParam_Custom3;
        public static string ShipAnimatorParam_Custom4;
        public static string ShipAnimatorParam_BlinkState;
        public static string ShipAnimatorParam_InvisibleState;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_SetActive;
        private static DelegateBridge __Hotfix_SetShipPartVisible;
        private static DelegateBridge __Hotfix_SetEffectVisible;
        private static DelegateBridge __Hotfix_CreateLODDispatch;
        private static DelegateBridge __Hotfix_InitSpaceObjectPrefab;
        private static DelegateBridge __Hotfix_OnCreateInternalComplete;
        private static DelegateBridge __Hotfix_CreateShipEffectCtrl;
        private static DelegateBridge __Hotfix_CreateShipBuffEffectCtrl;
        private static DelegateBridge __Hotfix_CreateShipSuperEquipEffectCtrl;
        private static DelegateBridge __Hotfix_CreateShipTacticalEquipEffectCtrl;
        private static DelegateBridge __Hotfix_CreateShipLocalMovementController;
        private static DelegateBridge __Hotfix_CreateShipSuperLaserEffectController;
        private static DelegateBridge __Hotfix_GetShipBuffEffectCtrl;
        private static DelegateBridge __Hotfix_GetShipSuperEquipCtrl;
        private static DelegateBridge __Hotfix_GetShipTacticalEquipCtrl;
        private static DelegateBridge __Hotfix_GetShipEffectCtrl;
        private static DelegateBridge __Hotfix_GetShipLocalMovementCtrl;
        private static DelegateBridge __Hotfix_GetShipSuperLaserEffectCtrl;
        private static DelegateBridge __Hotfix_GetShipEffectLODCtrl;
        private static DelegateBridge __Hotfix_GetShipGuildBuildingController;
        private static DelegateBridge __Hotfix_EnterLowSpeedJump;
        private static DelegateBridge __Hotfix_OnDead;
        private static DelegateBridge __Hotfix_AddDroneCtrlToShip;
        private static DelegateBridge __Hotfix_RemoveDroneCtrlFromShip;
        private static DelegateBridge __Hotfix_ReleaseAllDroneCtrl;
        private static DelegateBridge __Hotfix_AddLaserCtrlToShip;
        private static DelegateBridge __Hotfix_RemoveLaserCtrlFromShip;
        private static DelegateBridge __Hotfix_ReleaseAllLaserCtrl;
        private static DelegateBridge __Hotfix_IsNeedFrozenCamera;
        private static DelegateBridge __Hotfix_IsNeedStartCameraAnimator;
        private static DelegateBridge __Hotfix_SetShipInInfectScene;
        private static DelegateBridge __Hotfix_GetPrefabShipCelestialWeaponDesc;
        private static DelegateBridge __Hotfix_GetPrefabShipDroneInfoDesc;
        private static DelegateBridge __Hotfix_GetNextDroneRallyPointIndex;
        private static DelegateBridge __Hotfix_OnBeforeLODLevelChanged;
        private static DelegateBridge __Hotfix_OnPostLODLevelChanged;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_UpdateShipMoveState;
        private static DelegateBridge __Hotfix_StartShipExplosion;
        private static DelegateBridge __Hotfix_StartPlayShipAnimation;
        private static DelegateBridge __Hotfix_PlaySound;
        private static DelegateBridge __Hotfix_SetSoundParameter;
        private static DelegateBridge __Hotfix_StopSound;
        private static DelegateBridge __Hotfix_GetSpaceShipLODController;
        private static DelegateBridge __Hotfix_GetShipWeaponController;
        private static DelegateBridge __Hotfix_GetShipSuperWeaponController;
        private static DelegateBridge __Hotfix_GetShipEngineEffectController;
        private static DelegateBridge __Hotfix_GetShipShieldController;
        private static DelegateBridge __Hotfix_GetShipRadius;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.RegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.UnregisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetTransform;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetSize;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalHitPoint;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.OnBulletHit;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetTargetScale;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.IsTargetHasShield;
        private static DelegateBridge __Hotfix_RegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_UnRegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_GetTargetTransform;
        private static DelegateBridge __Hotfix_GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_OnBulletHit;
        private static DelegateBridge __Hotfix_GetTargetScale;
        private static DelegateBridge __Hotfix_IsTargetHasShield;
        private static DelegateBridge __Hotfix_get_DisplayNameForUI;
        private static DelegateBridge __Hotfix_get_AudioPlayer;
        private static DelegateBridge __Hotfix_get_LODDispatch;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;

        [MethodImpl(0x8000)]
        public void AddDroneCtrlToShip(BulletDroneController droneCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void AddLaserCtrlToShip(BulletLaserController laserCtrl)
        {
        }

        [MethodImpl(0x8000)]
        void IBulletTargetProvider.GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        bool IBulletTargetProvider.GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        Vector3 IBulletTargetProvider.GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        float IBulletTargetProvider.GetTargetScale()
        {
        }

        [MethodImpl(0x8000)]
        bool IBulletTargetProvider.IsTargetHasShield()
        {
        }

        [MethodImpl(0x8000)]
        void IBulletTargetProvider.OnBulletHit(Vector3 worldHitPoint, bool m_isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        float ISpaceTargetProvider.GetTargetSize()
        {
        }

        [MethodImpl(0x8000)]
        Transform ISpaceTargetProvider.GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.RegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.UnregisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override ControllerLODDispatchBase CreateLODDispatch()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateShipBuffEffectCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateShipEffectCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateShipLocalMovementController()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateShipSuperEquipEffectCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateShipSuperLaserEffectController()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateShipTacticalEquipEffectCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public void EnterLowSpeedJump()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        protected bool GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual Vector3 GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        public int GetNextDroneRallyPointIndex()
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipCelestialWeaponDesc GetPrefabShipCelestialWeaponDesc()
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipDroneInfoDesc GetPrefabShipDroneInfoDesc()
        {
        }

        [MethodImpl(0x8000)]
        public ShipBuffEffectController GetShipBuffEffectCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEffectController GetShipEffectCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEffectLODController GetShipEffectLODCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEngineEffectController GetShipEngineEffectController()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipGuildBuildingController GetShipGuildBuildingController()
        {
        }

        [MethodImpl(0x8000)]
        public ShipLocalMovementController GetShipLocalMovementCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipRadius()
        {
        }

        [MethodImpl(0x8000)]
        public ShipShieldController GetShipShieldController()
        {
        }

        [MethodImpl(0x8000)]
        public ShipSuperEquipController GetShipSuperEquipCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public ShipSuperLaserEffectController GetShipSuperLaserEffectCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public IShipSuperWeaponController GetShipSuperWeaponController()
        {
        }

        [MethodImpl(0x8000)]
        public ShipTacticalEquipController GetShipTacticalEquipCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public IShipWeaponController GetShipWeaponController()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipLODController GetSpaceShipLODController()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetTargetScale()
        {
        }

        [MethodImpl(0x8000)]
        protected Transform GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSpaceObjectPrefab()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedFrozenCamera()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedStartCameraAnimator(out string paramName, out bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsTargetHasShield()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBeforeLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBulletHit(Vector3 worldHitPoint, bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnCreateInternalComplete()
        {
        }

        [MethodImpl(0x8000)]
        public void OnDead()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPostLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySound(string audioName, PlaySoundOption playOption, bool isLoop)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        public override void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected void ReleaseAllDroneCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected void ReleaseAllLaserCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveDroneCtrlFromShip(object bulletCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveLaserCtrlFromShip(object bulletCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SetActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEffectVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipInInfectScene(bool isInfect)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipPartVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundParameter(string paramName, float value)
        {
        }

        [MethodImpl(0x8000)]
        public void StartPlayShipAnimation(string animaionParamName, object animaionParamValue, bool isTrigger = false)
        {
        }

        [MethodImpl(0x8000)]
        public void StartShipExplosion()
        {
        }

        [MethodImpl(0x8000)]
        public void StopSound(string audioName)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateShipMoveState(Vector3 location, Vector3 rotation, double currSpeed, double currMaxSpeed, double defaultMaxSpeed, double jumpMaxSpeed, bool isJumping, bool isBooster, bool isGridChanged)
        {
        }

        public override string DisplayNameForUI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CRIAudioSourceHelperImpl AudioPlayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SpaceShipControllerLODDispatch LODDispatch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientSpaceShip ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected class CachedShipAnimatorStateInfo
        {
            public string AnimatorParamName;
            public object AnimatorParamValue;
            public bool IsTrigger;
            protected bool[] m_LODAnimatorPlayedStates;
            protected DateTime AnimatorOutTime;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_IsNeedSetAnimatorState;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_Rest;
            private static DelegateBridge __Hotfix_OnStart;
            private static DelegateBridge __Hotfix_SetAnimatorParam;
            private static DelegateBridge __Hotfix_SetLODAnimationPlayed;
            private static DelegateBridge __Hotfix_IsLoopAnimation;

            [MethodImpl(0x8000)]
            protected bool IsLoopAnimation()
            {
            }

            [MethodImpl(0x8000)]
            public bool IsNeedSetAnimatorState(int LODLevel)
            {
            }

            [MethodImpl(0x8000)]
            public void OnStart()
            {
            }

            [MethodImpl(0x8000)]
            public void Rest()
            {
            }

            [MethodImpl(0x8000)]
            public void SetAnimatorParam(string paramName, object paramValue, bool isTrigger)
            {
            }

            [MethodImpl(0x8000)]
            public void SetLODAnimationPlayed(int LODLevel)
            {
            }

            [MethodImpl(0x8000)]
            public void Tick()
            {
            }
        }
    }
}

