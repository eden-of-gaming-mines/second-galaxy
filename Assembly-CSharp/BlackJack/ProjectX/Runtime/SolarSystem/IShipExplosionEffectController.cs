﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public interface IShipExplosionEffectController
    {
        void StartShipExplosionEffect();
    }
}

