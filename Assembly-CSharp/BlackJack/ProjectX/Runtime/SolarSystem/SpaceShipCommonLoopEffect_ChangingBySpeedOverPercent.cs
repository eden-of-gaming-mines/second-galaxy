﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect_ChangingBySpeedOverPercent : SpaceShipCommonLoopEffect_ChangingByLerpParam01
    {
        private float m_speedOverPercentMax;
        private float m_speedOverPercentMin;
        private bool m_isParamIncreaseBySpeedOverPercent;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitEffectChangingData;
        private static DelegateBridge __Hotfix_GetLerpParam01;
        private static DelegateBridge __Hotfix_GetDefaultMaxSpeed;
        private static DelegateBridge __Hotfix_get_LerpParamVarySpeed;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect_ChangingBySpeedOverPercent(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected float GetDefaultMaxSpeed()
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetLerpParam01()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEffectChangingData(float speedOverPercentMin, float speedOverPercentMax, bool isParamIncreaseBySpeedOverload)
        {
        }

        protected override float LerpParamVarySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

