﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopChannelEffect : SpaceShipEffectBase
    {
        protected List<ShipChannelEffectObjectController> m_effectCtrlList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StartEffectCtrl;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_ReleaseEffectCtrl;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopChannelEffect(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ReleaseEffectCtrl(ShipLoopEffectObjectController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(Dictionary<string, GameObject> effectAssets, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartEffectCtrl(ShipLoopEffectObjectController ctrl)
        {
        }
    }
}

