﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BulletDroneController : BulletObjectController, IBulletDroneInfoProvider, IBulletTargetProvider, ISpaceTargetProvider
    {
        protected BlackJack.ConfigData.DroneType m_droneType;
        protected uint m_bulletProcessId;
        private IFFState m_droneOwnerIFFType;
        protected bool m_isDroneOwnerIFFTypeChanged;
        protected BulletDroneFormationController m_droneFormationCtrl;
        protected GameObject m_targetDroneAroundTrajectGo;
        protected List<BulletDroneAroundTrajectoryController> m_targetDroneAroundTrajectList;
        protected PrefabDroneDesc m_droneDesc;
        protected PrefabWeaponEffectDesc m_droneWeaponEffectDesc;
        protected List<BulletDroneFlyController> m_droneFlyCtrlList;
        protected Transform m_droneMeshRoot;
        protected Transform m_droneFlareRoot;
        protected int m_droneCount;
        protected int m_finishLaunchDroneCount;
        protected bool m_isDroneCountChange;
        protected IBulletTargetProvider m_srcTarget;
        protected Vector3 m_worldSelfHoldPosition;
        protected DroneLaunchSide m_launchSide;
        protected int m_currLODLevel;
        protected DroneState m_droneState;
        protected float m_flyToTargetLeftTime;
        protected int MaxLaunchDroneCountPerGroup;
        protected float m_droneLaunchSpeed;
        protected float m_droneFlySpeed;
        protected float m_aroundTargetRadius;
        protected Transform m_rallyPointRoot;
        protected int m_rallyPointIndex;
        protected List<Vector3> m_tempDroneFlyWorldLocationList;
        protected List<Quaternion> m_tempDroneFlyWorldRotationList;
        protected static TypeDNName m_droneFormationTypeDNName;
        protected static TypeDNName m_droneFlyCtrlTypeDNName;
        protected static TypeDNName m_droneAroundTrajectoryTypeDNName;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_SetDroneState;
        private static DelegateBridge __Hotfix_StartBulletFly;
        private static DelegateBridge __Hotfix_SetBulletObjectScale;
        private static DelegateBridge __Hotfix_CalcBulletLifeTime;
        private static DelegateBridge __Hotfix_ShowBulletEffectObject;
        private static DelegateBridge __Hotfix_UpdateBulletFly;
        private static DelegateBridge __Hotfix_TickDroneObjectLocation;
        private static DelegateBridge __Hotfix_OnBulletDestroy;
        private static DelegateBridge __Hotfix_LaunchDroneByIndex;
        private static DelegateBridge __Hotfix_TestLaunchDrone;
        private static DelegateBridge __Hotfix_LaunchDroneByIndexImp;
        private static DelegateBridge __Hotfix_GetDroneBulletLaunchInfoByIndex;
        private static DelegateBridge __Hotfix_DestroyOneDrone;
        private static DelegateBridge __Hotfix_DestroyAllDrone;
        private static DelegateBridge __Hotfix_SetDroneEnterFlyBackState;
        private static DelegateBridge __Hotfix_SetDroneLaunchSide;
        private static DelegateBridge __Hotfix_GetSrcTargetLocation;
        private static DelegateBridge __Hotfix_GetLaunchSide;
        private static DelegateBridge __Hotfix_GetSrcTargetScale;
        private static DelegateBridge __Hotfix_GetLaunchLocation;
        private static DelegateBridge __Hotfix_GetWorldRallyLocationByIndex;
        private static DelegateBridge __Hotfix_GetWorldAroundLocationByIndex;
        private static DelegateBridge __Hotfix_GetWorldDestTargetLocation;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.RegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.UnregisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetTransform;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetSize;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalHitPoint;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.OnBulletHit;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetTargetScale;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.IsTargetHasShield;
        private static DelegateBridge __Hotfix_GetTargetTransform;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_SetBulletAsset;
        private static DelegateBridge __Hotfix_SetDroneCount;
        private static DelegateBridge __Hotfix_SetSrcTarget;
        private static DelegateBridge __Hotfix_SetDroneLaunchSpeed;
        private static DelegateBridge __Hotfix_SetDroneRallyPointRoot;
        private static DelegateBridge __Hotfix_SetDroneAroundTargetRadius;
        private static DelegateBridge __Hotfix_OnBulletEffectLODLevelChanged;
        private static DelegateBridge __Hotfix_GetDroneControllBoneByIndex;
        private static DelegateBridge __Hotfix_CreateDroneFormationController;
        private static DelegateBridge __Hotfix_CreateDroneFlyCtrlList;
        private static DelegateBridge __Hotfix_CreateDroneObject;
        private static DelegateBridge __Hotfix_CreateDroneFlareObject;
        private static DelegateBridge __Hotfix_CreateDroneAroundTrajectory;
        private static DelegateBridge __Hotfix_UpdateAnimator;
        private static DelegateBridge __Hotfix_GetDroneFlyCtrlByIndex;
        private static DelegateBridge __Hotfix_CreateDroneExplosionEffect;
        private static DelegateBridge __Hotfix_DestoryDroneExplosionEffect;
        private static DelegateBridge __Hotfix_DestroyBulletLater;
        private static DelegateBridge __Hotfix_GetRallyPointWorldLocation;
        private static DelegateBridge __Hotfix_CalcDroneMeshObjectWorldLocation;
        private static DelegateBridge __Hotfix_OnSingleDroneTraceTargetEnd;
        private static DelegateBridge __Hotfix_OnDroneFormationTraceTargetEnd;
        private static DelegateBridge __Hotfix_get_DroneType;
        private static DelegateBridge __Hotfix_set_DroneType;
        private static DelegateBridge __Hotfix_get_BulletProcessID;
        private static DelegateBridge __Hotfix_set_BulletProcessID;
        private static DelegateBridge __Hotfix_get_DroneOwnerIFFType;
        private static DelegateBridge __Hotfix_set_DroneOwnerIFFType;

        [MethodImpl(0x8000)]
        void IBulletTargetProvider.GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        bool IBulletTargetProvider.GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        Vector3 IBulletTargetProvider.GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        float IBulletTargetProvider.GetTargetScale()
        {
        }

        [MethodImpl(0x8000)]
        bool IBulletTargetProvider.IsTargetHasShield()
        {
        }

        [MethodImpl(0x8000)]
        void IBulletTargetProvider.OnBulletHit(Vector3 worldHitPoint, bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        float ISpaceTargetProvider.GetTargetSize()
        {
        }

        [MethodImpl(0x8000)]
        Transform ISpaceTargetProvider.GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.RegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.UnregisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcBulletLifeTime()
        {
        }

        [MethodImpl(0x8000)]
        protected Vector3 CalcDroneMeshObjectWorldLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateDroneAroundTrajectory()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateDroneExplosionEffect(Vector3 worldLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateDroneFlareObject()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateDroneFlyCtrlList()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateDroneFormationController()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateDroneObject()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DestoryDroneExplosionEffect(GameObject explosionEffect)
        {
        }

        [MethodImpl(0x8000)]
        public void DestroyAllDrone()
        {
        }

        [MethodImpl(0x8000)]
        protected override IEnumerator DestroyBulletLater(float time)
        {
        }

        [MethodImpl(0x8000)]
        public void DestroyOneDrone()
        {
        }

        [MethodImpl(0x8000)]
        public BulletLaunchInfo GetDroneBulletLaunchInfoByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected Transform GetDroneControllBoneByIndex(int index, bool isLocalBone)
        {
        }

        [MethodImpl(0x8000)]
        protected BulletDroneFlyController GetDroneFlyCtrlByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetLaunchLocation()
        {
        }

        [MethodImpl(0x8000)]
        public DroneLaunchSide GetLaunchSide()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        protected Vector3 GetRallyPointWorldLocation()
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetSrcTargetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSrcTargetScale()
        {
        }

        [MethodImpl(0x8000)]
        protected Transform GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetWorldAroundLocationByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetWorldDestTargetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetWorldRallyLocationByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void LaunchDroneByIndex(int index)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator LaunchDroneByIndexImp(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletDestroy()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletEffectLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDroneFormationTraceTargetEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSingleDroneTraceTargetEnd()
        {
        }

        [MethodImpl(0x8000)]
        public override void Reset()
        {
        }

        [MethodImpl(0x8000)]
        public override void SetBulletAsset(GameObject bulletAsset, Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SetBulletObjectScale(GameObject bulletObject)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDroneAroundTargetRadius(float radius)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDroneCount(int droneCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDroneEnterFlyBackState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDroneLaunchSide(DroneLaunchSide side)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDroneLaunchSpeed(float speed)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDroneRallyPointRoot(Transform rallyPointRoot, int rallyPointIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetDroneState(DroneState droneState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSrcTarget(IBulletTargetProvider srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowBulletEffectObject(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartBulletFly()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public IEnumerator TestLaunchDrone()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickDroneObjectLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateAnimator()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletFly()
        {
        }

        public BlackJack.ConfigData.DroneType DroneType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public uint BulletProcessID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public IFFState DroneOwnerIFFType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DestoryDroneExplosionEffect>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal GameObject explosionEffect;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(1.5f);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        UnityEngine.Object.Destroy(this.explosionEffect);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <LaunchDroneByIndexImp>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal SkinnedMeshRenderer <realDroneSkinRender>__0;
            internal SkinnedMeshRenderer <realFlareRender>__0;
            internal int index;
            internal BulletDroneFlyController <droneFlyCtrl>__0;
            internal BulletDroneController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.<realDroneSkinRender>__0 = this.$this.m_droneDesc.m_droneRender;
                        if (this.<realDroneSkinRender>__0 != null)
                        {
                            this.<realDroneSkinRender>__0.enabled = true;
                        }
                        this.<realFlareRender>__0 = this.$this.m_droneDesc.m_droneFlareRender;
                        if (this.<realFlareRender>__0 != null)
                        {
                            this.<realFlareRender>__0.enabled = true;
                        }
                        this.<droneFlyCtrl>__0 = this.$this.GetDroneFlyCtrlByIndex(this.index);
                        if (this.<droneFlyCtrl>__0 != null)
                        {
                            this.<droneFlyCtrl>__0.SetActive(true);
                            this.<droneFlyCtrl>__0.transform.position = this.$this.GetLaunchLocation().position;
                            this.<droneFlyCtrl>__0.StartLaunch(1.5f, this.$this.m_droneLaunchSpeed, this.$this.m_droneDesc.m_launchVelocityCurve, this.$this.m_droneFormationCtrl.GetSingleDroneFormationTransformByIndex(this.<droneFlyCtrl>__0.DroneIndex));
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <TestLaunchDrone>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal int <i>__1;
            internal BulletDroneController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

