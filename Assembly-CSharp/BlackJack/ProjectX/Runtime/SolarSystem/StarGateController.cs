﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StarGateController : SpaceLODObjectController
    {
        protected PrefabStarGateDesc m_prefabStarGateDesc;
        private const string EffectAnimName = "Activate";
        [AutoBind("./StarGateBody", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starGateBody;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_PlayTeleportEffect;
        private static DelegateBridge __Hotfix_GetTeleportStartTransform;
        private static DelegateBridge __Hotfix_GetTeleportEndTransform;
        private static DelegateBridge __Hotfix_GetTeleportCameraLookAtStarGateTime;
        private static DelegateBridge __Hotfix_GetTeleportCameraAccMoveTime;
        private static DelegateBridge __Hotfix_CreateLODDispatch;
        private static DelegateBridge __Hotfix_GetPrefabStarGateDesc;
        private static DelegateBridge __Hotfix_InitSpaceObjectPrefab;
        private static DelegateBridge __Hotfix_get_DisplayNameForUI;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;
        private static DelegateBridge __Hotfix_get_LODDispatch;

        [MethodImpl(0x8000)]
        public override bool Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override ControllerLODDispatchBase CreateLODDispatch()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual PrefabStarGateDesc GetPrefabStarGateDesc()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTeleportCameraAccMoveTime()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTeleportCameraLookAtStarGateTime()
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetTeleportEndTransform()
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetTeleportStartTransform()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSpaceObjectPrefab()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTeleportEffect()
        {
        }

        public override string DisplayNameForUI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientStarGate ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected SpaceStationControllerLODDispatch LODDispatch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

