﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceSimpleObjectController : SpaceLODObjectController
    {
        [AutoBind("./SimpleObjectBodyRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_simpleObjectBodyRoot;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_CreateLODDispatch;
        private static DelegateBridge __Hotfix_InitSpaceObjectPrefab;
        private static DelegateBridge __Hotfix_get_DisplayNameForUI;
        private static DelegateBridge __Hotfix_get_LODDispatch;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;

        [MethodImpl(0x8000)]
        public override bool Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override ControllerLODDispatchBase CreateLODDispatch()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSpaceObjectPrefab()
        {
        }

        public override string DisplayNameForUI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected SpaceSimpleObjectControllerLODDispatch LODDispatch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientSpaceSimpleObject ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

