﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class SpaceShipNamedEffect_FlagShipBlink : SpaceShipNamedEffect_Blink
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectAllDynamicResPath;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StarFlagShipBlinkStart;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_FlagShipBlink(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllDynamicResPath(ShipNamedEffectType effectName, List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        protected void StarFlagShipBlinkStart(Dictionary<string, GameObject> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }
    }
}

