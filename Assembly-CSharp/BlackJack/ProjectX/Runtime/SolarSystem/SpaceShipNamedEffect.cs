﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class SpaceShipNamedEffect : SpaceShipEffectBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsEffectActive>k__BackingField;
        protected int m_referenceCount;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_CollectAllDynamicResPath;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_PlaySound;
        private static DelegateBridge __Hotfix_StopSound;
        private static DelegateBridge __Hotfix_get_IsEffectActive;
        private static DelegateBridge __Hotfix_set_IsEffectActive;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CollectAllDynamicResPath(ShipNamedEffectType effectName, List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void PlaySound(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StopEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void StopSound(ShipNamedEffectType effectName)
        {
        }

        public virtual bool IsEffectActive
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

