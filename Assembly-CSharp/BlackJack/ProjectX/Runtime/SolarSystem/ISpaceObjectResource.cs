﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface ISpaceObjectResource
    {
        bool Create(ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null);
        bool IsCreateCompleted();
        void OnResourcesReady();
        void Release();
    }
}

