﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ShipSuperEquipController : PrefabControllerBase
    {
        protected SpaceShipController m_ownerShip;
        private SpaceShipEffectBase m_superEquipChargeLoopEffect;
        private SpaceShipEffectBase m_superEquipExtraLoopEffect1;
        private SpaceShipEffectBase m_superEquipExtraLoopEffect2;
        private SpaceShipEffectBase m_superEquipExtraLoopEffect3;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_CollectAllSuperEquipDynamicResPath_1;
        private static DelegateBridge __Hotfix_CollectAllSuperEquipDynamicResPath_0;
        private static DelegateBridge __Hotfix_PlaySuperEquipChargeEffect_1;
        private static DelegateBridge __Hotfix_PlaySuperEquipChargeEffect_0;
        private static DelegateBridge __Hotfix_PlaySuperEquipFireEffect_1;
        private static DelegateBridge __Hotfix_PlaySuperEquipFireEffect_0;
        private static DelegateBridge __Hotfix_PlaySuperEquipExtraEffect1_1;
        private static DelegateBridge __Hotfix_PlaySuperEquipExtraEffect2_1;
        private static DelegateBridge __Hotfix_PlaySuperEquipExtraEffect3_1;
        private static DelegateBridge __Hotfix_PlaySuperEquipExtraEffect1_0;
        private static DelegateBridge __Hotfix_PlaySuperEquipExtraEffect2_0;
        private static DelegateBridge __Hotfix_PlaySuperEquipExtraEffect3_0;
        private static DelegateBridge __Hotfix_StopSuperEquipChargeEffect_1;
        private static DelegateBridge __Hotfix_StopSuperEquipChargeEffect_0;
        private static DelegateBridge __Hotfix_StopSuperEquipExtraEffect1;
        private static DelegateBridge __Hotfix_StopSuperEquipExtraEffect2;
        private static DelegateBridge __Hotfix_StopSuperEquipExtraEffect3;
        private static DelegateBridge __Hotfix_get_ShipEffectCtrl;

        [MethodImpl(0x8000)]
        public void CollectAllSuperEquipDynamicResPath(ConfigDataShipSuperEquipInfo confInfo, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void CollectAllSuperEquipDynamicResPath(LBSuperEquipGroupBase equip, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipChargeEffect(ConfigDataShipSuperEquipInfo config, SpaceShipController targetShipCtrl, uint chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipChargeEffect(LBSuperEquipGroupBase equip, SpaceShipController targetShipCtrl, uint chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipExtraEffect1(ConfigDataShipSuperEquipInfo config, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipExtraEffect1(LBSuperEquipGroupBase equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipExtraEffect2(ConfigDataShipSuperEquipInfo config, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipExtraEffect2(LBSuperEquipGroupBase equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipExtraEffect3(ConfigDataShipSuperEquipInfo config, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipExtraEffect3(LBSuperEquipGroupBase equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipFireEffect(ConfigDataShipSuperEquipInfo config, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySuperEquipFireEffect(LBSuperEquipGroupBase equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void StopSuperEquipChargeEffect(ConfigDataShipSuperEquipInfo config)
        {
        }

        [MethodImpl(0x8000)]
        public void StopSuperEquipChargeEffect(LBSuperEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        public void StopSuperEquipExtraEffect1()
        {
        }

        [MethodImpl(0x8000)]
        public void StopSuperEquipExtraEffect2()
        {
        }

        [MethodImpl(0x8000)]
        public void StopSuperEquipExtraEffect3()
        {
        }

        protected ShipEffectController ShipEffectCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

