﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityStandardAssets.ImageEffects;

    public class SolarSystemCelestialLayerController : PrefabControllerBase
    {
        [AutoBind("./SpaceObjectList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_spaceObjectList;
        [AutoBind("./CameraRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraSyncController m_celestialCameraSyncCtrl;
        [AutoBind("./CameraRoot/LayerCamera", AutoBindAttribute.InitState.NotInit, false)]
        public ColorCorrectionCurves m_infectImageEffect;
        protected Dictionary<uint, SpaceObjectControllerBase> m_spaceObjectCtrlDict;
        protected ProFlare m_lensFlare;
        protected GameObject m_lensFlareObj;
        public float StarScreenSizeLowValue;
        public float StarScreenSizeHigValue;
        public float StarScreenSizeThreshhold;
        private WeakReference m_star;
        private static DelegateBridge __Hotfix_GetSpaceObjectById;
        private static DelegateBridge __Hotfix_RemoveSpaceObject;
        private static DelegateBridge __Hotfix_ForeachSpaceObject;
        private static DelegateBridge __Hotfix_CreateLensFlareAndLight;
        private static DelegateBridge __Hotfix_UpdateLensFlareAndLight;
        private static DelegateBridge __Hotfix_EnableLensFlare;
        private static DelegateBridge __Hotfix_DisableSceneLight;
        private static DelegateBridge __Hotfix_UpdateStar;
        private static DelegateBridge __Hotfix_CreateStar;
        private static DelegateBridge __Hotfix_CreatePlanet;
        private static DelegateBridge __Hotfix_CreatePlanetLikeBuilding;
        private static DelegateBridge __Hotfix_CreateMoon;
        private static DelegateBridge __Hotfix_SetSkyBoxMaterial;
        private static DelegateBridge __Hotfix_GetSpaceObjectScreenPosition_0;
        private static DelegateBridge __Hotfix_GetSpaceObjectScreenPosition_1;
        private static DelegateBridge __Hotfix_EnableCelestialLayerCamera;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_SetCelestialVisible;
        private static DelegateBridge __Hotfix_SetPlanetLikeBuildingVisible;
        private static DelegateBridge __Hotfix_GetCelestialLayerCamera;
        private static DelegateBridge __Hotfix_EnableInfectImageEffect;
        private static DelegateBridge __Hotfix_GetStarScreenPortion;

        [MethodImpl(0x8000)]
        public void CreateLensFlareAndLight(string resPath, IDynamicAssetProvider dynamicAssetProvider, Camera shipLayerCamera)
        {
        }

        [MethodImpl(0x8000)]
        public MoonController CreateMoon(ClientMoon clientObj, Vector3 location, Vector3 scale, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public PlanetController CreatePlanet(ClientPlanet clientObj, Vector3 location, Vector3 scale, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public PlanetLikeBuildingController CreatePlanetLikeBuilding(ClientPlanetLikeBuilding clientObj, Vector3 location, Vector3 scale, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public BlackJack.ProjectX.Runtime.SolarSystem.StarController CreateStar(ClientStar clientObj, Vector3 location, Vector3 scale, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public void DisableSceneLight(bool disableLight)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableCelestialLayerCamera(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableInfectImageEffect(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableLensFlare(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachSpaceObject(Action<SpaceObjectControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetCelestialLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceObjectControllerBase GetSpaceObjectById(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSpaceObjectScreenPosition(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSpaceObjectScreenPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        protected float GetStarScreenPortion()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSpaceObject(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCelestialVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlanetLikeBuildingVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkyBoxMaterial(string resPath, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLensFlareAndLight(Vector3 starLocation, Vector3 shipLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateStar(float starScreenSize)
        {
        }
    }
}

