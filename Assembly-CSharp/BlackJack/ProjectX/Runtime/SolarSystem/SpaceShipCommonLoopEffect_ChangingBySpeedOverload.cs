﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect_ChangingBySpeedOverload : SpaceShipCommonLoopEffect_ChangingByLerpParam01
    {
        private float m_speedOverMax;
        private float m_speedOverMin;
        private bool m_isParamIncreaseBySpeedOverload;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitEffectChangingData;
        private static DelegateBridge __Hotfix_GetLerpParam01;
        private static DelegateBridge __Hotfix_GetDefaultMaxSpeed;
        private static DelegateBridge __Hotfix_get_LerpParamVarySpeed;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect_ChangingBySpeedOverload(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected float GetDefaultMaxSpeed()
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetLerpParam01()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEffectChangingData(float speedOverMin, float speedOverMax, bool isParamIncreaseBySpeedOverload)
        {
        }

        protected override float LerpParamVarySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

