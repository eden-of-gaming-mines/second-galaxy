﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public enum QueueSyncEventType
    {
        None,
        NPCShipAnimEffect,
        ShipBuffAttached,
        ShipDuplicateBuffAttached,
        GuildBuildingStatus,
        ShipDead,
        ShipBuffDetached,
        PickDropBoxStart,
        PickDropBoxEnd,
        InteractionStart,
        InteractionEnd,
        EquipCharge,
        EquipFire,
        EquipLaunchCancel,
        EquipLaunchEnd,
        EquipEffect,
        SuperEquipCharge,
        SuperEquipFire,
        SuperEquipLaunchCancel,
        SuperEquipLaunchEnd,
        SuperEquipEffect,
        AoeSuperEquipProcessFire,
        SuperEquipRecoveryEnergy,
        TacticalEquipIncByNearByDead_TargetDead,
        SuperEnergyModify,
        TacticalEquipIncBufRecoveryShieldOnDetach_RecoveryShield,
        TacticalEquipGroupReduceCDByWeaponEquipLaunch_ReduceCD,
        TacticalEquipCharge,
        TacticalEquipFire,
        TacticalEquipCancel,
        TacticalEquipLaunchEnd,
        TacticalEquipEffect,
        TacticalEquipIncBuf_LastIncBufDetach,
        TacticalEquipIncBufRecoveryEnergyyByMakeDamage_RecoveryEnergy,
        TacticalEquipGroupReduceCDByKillingHit_ReduceCD,
        TacticalEquipAttachBufByKillingHit,
        TacticalEquipPropertyProviderByTargetShieldPercent_Active,
        RecoveryShipEnergy,
        Max
    }
}

