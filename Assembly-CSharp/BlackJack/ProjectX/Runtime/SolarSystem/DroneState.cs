﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public enum DroneState
    {
        None,
        AroundSelf,
        MoveToTarget,
        AroundTarget,
        BackToSelf
    }
}

