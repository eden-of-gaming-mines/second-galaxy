﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipNamedEffect_FakeJumpInOut : SpaceShipNamedEffect
    {
        private DelayExecHelper m_delayExecHelper;
        protected ShipNamedEffectType m_currEffetName;
        protected bool m_isRegistLODChangEvent;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_StartShipFakeJumpIn;
        private static DelegateBridge __Hotfix_StartShipFakeJumpOut;
        private static DelegateBridge __Hotfix_StartShipFakeJumpInLocalMovement;
        private static DelegateBridge __Hotfix_StartShipFakeJumpOutLocalMovement;
        private static DelegateBridge __Hotfix_OnShipLocalMovementEnd;
        private static DelegateBridge __Hotfix_PostOnLODLevelChanged;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_FakeJumpInOut(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipLocalMovementEnd(ShipLocalMovementController.MovementType moveType)
        {
        }

        [MethodImpl(0x8000)]
        protected void PostOnLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipFakeJumpIn()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipFakeJumpInLocalMovement()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipFakeJumpOut()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipFakeJumpOutLocalMovement()
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float delayTime, uint solarSystemTime)
        {
        }
    }
}

