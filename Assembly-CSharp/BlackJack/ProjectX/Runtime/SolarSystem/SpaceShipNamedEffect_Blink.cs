﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceShipNamedEffect_Blink : SpaceShipNamedEffect
    {
        protected string m_currAssetPath;
        protected BlinkState m_blinkState;
        protected float m_blinkLeftLifeTime;
        protected const float BlinkLifeTime = 10f;
        protected ShipLoopEffectObjectController m_blinkEffectCtrl;
        protected bool m_isFrozenCamera;
        protected const int BlinkAnimatorParamValue_Charge = 0;
        protected const int BlinkAnimatorParamValue_Start = 1;
        protected const int BlinkAnimatorParamValue_End = 2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_UpdateBlinkEndLifeTime;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_CollectAllDynamicResPath;
        private static DelegateBridge __Hotfix_StartEffect_0;
        private static DelegateBridge __Hotfix_StartEffect_1;
        private static DelegateBridge __Hotfix_StartBlinkCharge;
        private static DelegateBridge __Hotfix_StartBlinkStart;
        private static DelegateBridge __Hotfix_StartBlinkEnd;
        private static DelegateBridge __Hotfix_StopEffect_1;
        private static DelegateBridge __Hotfix_StopEffect_0;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_IsNeedFrozenCamera;
        private static DelegateBridge __Hotfix_GetAssetByName;
        private static DelegateBridge __Hotfix_GetBlinkEffectAssetName;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_Blink(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllDynamicResPath(ShipNamedEffectType effectName, List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetAssetByName(string assetName, Dictionary<string, GameObject> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetBlinkEffectAssetName(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedFrozenCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected override void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartBlinkCharge(Dictionary<string, GameObject> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartBlinkEnd(Dictionary<string, GameObject> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartBlinkStart(Dictionary<string, GameObject> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect()
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float delayTime, uint solarSystemTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateBlinkEndLifeTime(float delayTime)
        {
        }

        protected enum BlinkState
        {
            BlinkNone,
            BlinkCharge,
            BlinkStart,
            BlinkEnd
        }
    }
}

