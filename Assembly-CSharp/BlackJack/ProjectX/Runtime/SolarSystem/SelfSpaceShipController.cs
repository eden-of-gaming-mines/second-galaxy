﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SelfSpaceShipController : SpaceShipController
    {
        [AutoBind("./JumpingTunnelObj", AutoBindAttribute.InitState.NotInit, false)]
        protected JumpingTunnelController m_jumpingTunnelCtrl;
        [AutoBind("./SpaceDustObj", AutoBindAttribute.InitState.NotInit, false)]
        protected SpaceDustController m_spaceDustCtrl;
        private bool m_isInJumpState;
        private double m_currSpeed;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnEnterWarpTunnelState;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLeaveWarpTunnelState;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_CreateLODDispatch;
        private static DelegateBridge __Hotfix_InitSpaceObjectPrefab;
        private static DelegateBridge __Hotfix_CreateShipAudioComponent;
        private static DelegateBridge __Hotfix_CreateJumpingTunnelObject;
        private static DelegateBridge __Hotfix_CreateSpaceDustObject;
        private static DelegateBridge __Hotfix_CreateReflectionProbe;
        private static DelegateBridge __Hotfix_OnSelfShipEnterJumpState;
        private static DelegateBridge __Hotfix_OnSelfShipLeaveJumpState;
        private static DelegateBridge __Hotfix_UpdateShipMoveState;
        private static DelegateBridge __Hotfix_GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_get_LODDispatch;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;
        private static DelegateBridge __Hotfix_add_EventOnEnterWarpTunnelState;
        private static DelegateBridge __Hotfix_remove_EventOnEnterWarpTunnelState;
        private static DelegateBridge __Hotfix_add_EventOnLeaveWarpTunnelState;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveWarpTunnelState;

        public event Action EventOnEnterWarpTunnelState
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLeaveWarpTunnelState
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override bool Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateJumpingTunnelObject()
        {
        }

        [MethodImpl(0x8000)]
        protected override ControllerLODDispatchBase CreateLODDispatch()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateReflectionProbe(string resPath, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateShipAudioComponent()
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateSpaceDustObject()
        {
        }

        [MethodImpl(0x8000)]
        protected override void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        protected override Vector3 GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSpaceObjectPrefab()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSelfShipEnterJumpState()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSelfShipLeaveJumpState()
        {
        }

        [MethodImpl(0x8000)]
        public override void UpdateShipMoveState(Vector3 location, Vector3 rotation, double currSpeed, double currMaxSpeed, double defaultMaxSpeed, double jumpMaxSpeed, bool isJumping, bool isBooster, bool isGridChanged)
        {
        }

        protected SelfSpaceShipControllerLODDispatch LODDispatch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientSelfPlayerSpaceShip ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

