﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect : SpaceShipEffectBase
    {
        protected Action<int> m_onPostShipLODChangedAction;
        protected List<ShipLoopEffectObjectController>[] m_effectCtrlList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_PauseEffect;
        private static DelegateBridge __Hotfix_ResumeEffect;
        private static DelegateBridge __Hotfix_StartCurrLODLevelEffect;
        private static DelegateBridge __Hotfix_StartEffectCtrl;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_ReleaseEffectCtrl;
        private static DelegateBridge __Hotfix_OnPostShipLODChanged;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPostShipLODChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void PauseEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected override void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ReleaseEffectCtrl(ShipLoopEffectObjectController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void ResumeEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartCurrLODLevelEffect(Dictionary<string, GameObject> effectAssets, ISpaceTargetProvider target, int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(Dictionary<string, GameObject> effectAssets, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartEffectCtrl(ShipLoopEffectObjectController ctrl)
        {
        }
    }
}

