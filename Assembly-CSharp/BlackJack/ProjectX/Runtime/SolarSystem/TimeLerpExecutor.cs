﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class TimeLerpExecutor
    {
        protected bool m_isStoped;
        protected float m_lastTime;
        protected float m_curLeftTime;
        protected float m_startValue;
        protected float m_endValue;
        protected float m_curValue;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_IsStop;
        private static DelegateBridge __Hotfix_GetCurValue;

        [MethodImpl(0x8000)]
        public float GetCurValue()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStop()
        {
        }

        [MethodImpl(0x8000)]
        public void Start(float time, float startVal, float endVal)
        {
        }

        [MethodImpl(0x8000)]
        public void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(float delayTime)
        {
        }
    }
}

