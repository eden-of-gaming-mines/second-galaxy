﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Runtime.SolarSystem.Weapon;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceShipLODController : SpaceObjectLODController, ISpaceShipController, IWeaponOwnerProvider
    {
        protected Material m_shipBodyMaterial;
        protected Material m_shipInvisbileBodyMaterial;
        protected AnimatorControllerParameter[] m_animatorCtrlParams;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipBodyRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, true)]
        public Animator m_shipAnimator;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, true)]
        public ShipWeaponController m_shipWeaponCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, true)]
        public ShipSuperWeaponController m_shipSuperWeaponCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, true)]
        public ShipExplosionEffectController m_shipExplosionCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, true)]
        protected ShipEngineEffectController m_engineEffectCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, true)]
        protected ShipEffectLODController m_shipEffectLODCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, true)]
        protected ShipShieldController m_shipShieldCtrl;
        protected PrefabShipDesc m_prefabShipDesc;
        protected PrefabSuperWeaponDesc m_prefabSuperWeaponDesc;
        protected float m_shipRadius;
        protected SkinnedMeshRenderer m_shipSkinnedMeshRenderer;
        protected ShipSignalLightController m_signalLightCtrl;
        protected string m_currAnimatorParamName;
        protected object m_currAnimatorParamObj;
        private BuildingNodeDisplaySwitcher m_switcher;
        public static string WeaponLODAssetName = "WeaponLOD{0}Asset";
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_UpdateShipMoveState;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_StartShipExplosion;
        private static DelegateBridge __Hotfix_StartPlayShipAnimation;
        private static DelegateBridge __Hotfix_HasAnimatorParam;
        private static DelegateBridge __Hotfix_SetShipReceiveAndCastShadow;
        private static DelegateBridge __Hotfix_PlayCommonEffect;
        private static DelegateBridge __Hotfix_GetShipWeaponController;
        private static DelegateBridge __Hotfix_GetShipSuperWeaponController;
        private static DelegateBridge __Hotfix_GetShipBurningController;
        private static DelegateBridge __Hotfix_GetShipEngineEffectController;
        private static DelegateBridge __Hotfix_GetShipEffectLODController;
        private static DelegateBridge __Hotfix_GetShipShieldController;
        private static DelegateBridge __Hotfix_GetPrefabShipCelestialWeaponDesc;
        private static DelegateBridge __Hotfix_SetArmorRecoverValue;
        private static DelegateBridge __Hotfix_SetArmorResistUpValue;
        private static DelegateBridge __Hotfix_SetArmorResistDownValue;
        private static DelegateBridge __Hotfix_SetShipOnHitEffectStrength;
        private static DelegateBridge __Hotfix_SetShipBlinkEffectStrength;
        private static DelegateBridge __Hotfix_GetWeaponOwnerRoot;
        private static DelegateBridge __Hotfix_GetOwnerSkinMeshRender;
        private static DelegateBridge __Hotfix_IsLogicWeaponSlotWithIndexExist;
        private static DelegateBridge __Hotfix_GetRealSlotsByLogicWeaponSlotIndex;
        private static DelegateBridge __Hotfix_GetRealSlotsBySuperWeaponSlotIndex;
        private static DelegateBridge __Hotfix_GetRealSlotsByNormalWeaponSlotIndex;
        private static DelegateBridge __Hotfix_GetShipWeaponScale;
        private static DelegateBridge __Hotfix_GetShipWeaponEffectScale;
        private static DelegateBridge __Hotfix_GetSuperWeaponScale;
        private static DelegateBridge __Hotfix_GetPrefabSuperWeaponDesc;
        private static DelegateBridge __Hotfix_GetPrefabNormalWeaponDesc;
        private static DelegateBridge __Hotfix_SetShipPartVisible;
        private static DelegateBridge __Hotfix_SetEffectVisible;
        private static DelegateBridge __Hotfix_SetShipRadius;
        private static DelegateBridge __Hotfix_GetShipRadius;
        private static DelegateBridge __Hotfix_SetShipInInfectScene;
        private static DelegateBridge __Hotfix_InitShipWeaponController;
        private static DelegateBridge __Hotfix_InitShipSuperWeaponController;
        private static DelegateBridge __Hotfix_InitShipEngineEffectController;
        private static DelegateBridge __Hotfix_InitShipInvisibleBody;
        private static DelegateBridge __Hotfix_GetPrefabShipDesc;
        private static DelegateBridge __Hotfix_GetSwitcher;
        private static DelegateBridge __Hotfix_GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_GetTargetScale;
        private static DelegateBridge __Hotfix_GetWeaponGroupConstructInfoListFromClientShipObject;
        private static DelegateBridge __Hotfix_GetWeaponEquipArtResource;
        private static DelegateBridge __Hotfix_GetWeaponEquipItemCount;
        private static DelegateBridge __Hotfix_GetWeaponItemCount;
        private static DelegateBridge __Hotfix_GetWeaponEquipUnitCount;
        private static DelegateBridge __Hotfix_GetWeaponEquipWaveCount;
        private static DelegateBridge __Hotfix_GetSuperWeaponGroupConstructInfoFromClientShipObject;
        private static DelegateBridge __Hotfix_GetNormalWeaponGroupConstructInfoFromClientShipObject;
        private static DelegateBridge __Hotfix_GetSuperWeaponEquipUnitCount;
        private static DelegateBridge __Hotfix_GetSuperWeaponEquipWaveCount;
        private static DelegateBridge __Hotfix_GetShipBodyMaterial;
        private static DelegateBridge __Hotfix_GetShipInvisibleBodyMaterial;
        private static DelegateBridge __Hotfix_GetSkyboxCubmapResPath;
        private static DelegateBridge __Hotfix_IsSelfPlayerShip;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;

        [MethodImpl(0x8000)]
        public override void Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObject, IDynamicAssetProvider dynamicAssetProvider, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration)
        {
        }

        [MethodImpl(0x8000)]
        public virtual Vector3 GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        protected WeaponSlotGroupConstructInfo GetNormalWeaponGroupConstructInfoFromClientShipObject()
        {
        }

        [MethodImpl(0x8000)]
        public virtual SkinnedMeshRenderer GetOwnerSkinMeshRender()
        {
        }

        [MethodImpl(0x8000)]
        public virtual PrefabNormalWeaponDesc GetPrefabNormalWeaponDesc()
        {
        }

        [MethodImpl(0x8000)]
        public PrefabShipCelestialWeaponDesc GetPrefabShipCelestialWeaponDesc()
        {
        }

        [MethodImpl(0x8000)]
        public virtual PrefabShipDesc GetPrefabShipDesc()
        {
        }

        [MethodImpl(0x8000)]
        public virtual PrefabSuperWeaponDesc GetPrefabSuperWeaponDesc()
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<GameObject> GetRealSlotsByLogicWeaponSlotIndex(int groupIndex, int slotIndex, out bool isCreateVisibleObject)
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<GameObject> GetRealSlotsByNormalWeaponSlotIndex(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public virtual List<GameObject> GetRealSlotsBySuperWeaponSlotIndex(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public Material GetShipBodyMaterial()
        {
        }

        [MethodImpl(0x8000)]
        public IShipBurningController GetShipBurningController()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEffectLODController GetShipEffectLODController()
        {
        }

        [MethodImpl(0x8000)]
        public ShipEngineEffectController GetShipEngineEffectController()
        {
        }

        [MethodImpl(0x8000)]
        protected Material GetShipInvisibleBodyMaterial()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipRadius()
        {
        }

        [MethodImpl(0x8000)]
        public ShipShieldController GetShipShieldController()
        {
        }

        [MethodImpl(0x8000)]
        public IShipSuperWeaponController GetShipSuperWeaponController()
        {
        }

        [MethodImpl(0x8000)]
        public IShipWeaponController GetShipWeaponController()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetShipWeaponEffectScale()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetShipWeaponScale()
        {
        }

        [MethodImpl(0x8000)]
        private string GetSkyboxCubmapResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuperWeaponEquipUnitCount(LBSuperWeaponGroupBase lbSuperGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuperWeaponEquipWaveCount(LBSuperWeaponGroupBase lbSuperGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected WeaponSlotGroupConstructInfo GetSuperWeaponGroupConstructInfoFromClientShipObject(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetSuperWeaponScale()
        {
        }

        [MethodImpl(0x8000)]
        public BuildingNodeDisplaySwitcher GetSwitcher()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetTargetScale()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetWeaponEquipArtResource(LBStaticWeaponEquipSlotGroup lbStaticGroup, GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetWeaponEquipItemCount(LBStaticWeaponEquipSlotGroup lbStaticGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetWeaponEquipUnitCount(LBStaticWeaponEquipSlotGroup lbStaticGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetWeaponEquipWaveCount(LBStaticWeaponEquipSlotGroup lbStaticGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected List<WeaponSlotGroupConstructInfo> GetWeaponGroupConstructInfoListFromClientShipObject(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetWeaponItemCount(LBWeaponGroupBase lbWeaponGroup)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetWeaponOwnerRoot()
        {
        }

        [MethodImpl(0x8000)]
        protected bool HasAnimatorParam(string paramName)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitShipEngineEffectController()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitShipInvisibleBody()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitShipSuperWeaponController()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitShipWeaponController()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsLogicWeaponSlotWithIndexExist(int groupIndex, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSelfPlayerShip()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PlayCommonEffect(GameObject effectAsset, float lastTime, SpaceShipController target = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorRecoverValue(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorResistDownValue(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorResistUpValue(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEffectVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipBlinkEffectStrength(float strength)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipInInfectScene(bool isInfect)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipOnHitEffectStrength(float hitStrength)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipPartVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipRadius(float raduis)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipReceiveAndCastShadow(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void StartPlayShipAnimation(string animaionParamName, object animaionParamValue, bool isTrigger = false)
        {
        }

        [MethodImpl(0x8000)]
        public void StartShipExplosion()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateShipMoveState(Vector3 location, Vector3 rotation, double currSpeed, double currMaxSpeed, double defaultMaxSpeed, double jumpMaxSpeed, bool isJumping, bool isBooster, bool isGridChanged)
        {
        }

        public ClientSpaceShip ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

