﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimation_Timeline : SceneAnimation_CutSceneBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_IsRelocationCamera;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_GetCameraFov;
        private static DelegateBridge __Hotfix_GetPlayerType;
        private static DelegateBridge __Hotfix_GainTimelineBindInfos;

        [MethodImpl(0x8000)]
        public SceneAnimation_Timeline(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        protected void GainTimelineBindInfos(List<TimelineBindInfo> timelineBindInfos)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetCameraFov()
        {
        }

        [MethodImpl(0x8000)]
        public override Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected override SceneAnimationPlayerType GetPlayerType()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsRelocationCamera()
        {
        }

        [MethodImpl(0x8000)]
        public override void Play(Transform target, float targetSize)
        {
        }
    }
}

