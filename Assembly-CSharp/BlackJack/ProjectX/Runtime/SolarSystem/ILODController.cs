﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface ILODController
    {
        void Create(ClientSpaceObject clientObject, IDynamicAssetProvider dynamicAssetProvider, Camera camera = null);
        void EnterLODLevel(object param);
        object LeaveLODLevel();
        void Release();
    }
}

