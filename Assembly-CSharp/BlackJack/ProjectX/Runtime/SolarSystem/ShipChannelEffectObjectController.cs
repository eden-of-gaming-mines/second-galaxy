﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ShipChannelEffectObjectController : ShipLoopEffectObjectController
    {
        protected Action<object> m_onTargetDestoryAction;
        protected List<LineRenderer> m_lineRenderList;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_RestEffect;
        private static DelegateBridge __Hotfix_SetTarget;
        private static DelegateBridge __Hotfix_OnTargetDestory;
        private static DelegateBridge __Hotfix_GetLineRenderList;
        private static DelegateBridge __Hotfix_UpdateLineRenderPosition;

        [MethodImpl(0x8000)]
        protected List<LineRenderer> GetLineRenderList()
        {
        }

        [MethodImpl(0x8000)]
        protected override void Init(string effectAssetPath, string effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetDestory(object obj)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void RestEffect()
        {
        }

        [MethodImpl(0x8000)]
        public override void SetTarget(ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateLineRenderPosition()
        {
        }
    }
}

