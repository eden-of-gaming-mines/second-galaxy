﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using TouchScript.Gestures;

    public class SolarSystemCameraInputController : PrefabControllerBase
    {
        private float m_cameraManualOperationCountDownLeftTime;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public ScreenTransformGesture m_screenTransformGesture;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TapGesture m_tapGesture;
        [AutoBind("CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraOperationController m_cameraOperationformCtrl;
        [AutoBind("CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraTransformController m_cameraTransformCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <LockScale>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <LockRotation>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector3> eventOnShipLayerDoubleClick;
        private const float CameraOffsetSensitivityForFingerGesture = 40f;
        private const float CameraManualOperationCountDownTime = 2f;
        private static DelegateBridge __Hotfix_IsCameraManualOperationCountDown;
        private static DelegateBridge __Hotfix_ClearCameraManualOperationCD;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_manipulationTransformedHandler;
        private static DelegateBridge __Hotfix_manipulationTappedHandler;
        private static DelegateBridge __Hotfix_EnableGestureInput;
        private static DelegateBridge __Hotfix_get_LockScale;
        private static DelegateBridge __Hotfix_set_LockScale;
        private static DelegateBridge __Hotfix_get_LockRotation;
        private static DelegateBridge __Hotfix_set_LockRotation;
        private static DelegateBridge __Hotfix_add_eventOnShipLayerDoubleClick;
        private static DelegateBridge __Hotfix_remove_eventOnShipLayerDoubleClick;

        public event Action<Vector3> eventOnShipLayerDoubleClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearCameraManualOperationCD()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableGestureInput(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCameraManualOperationCountDown()
        {
        }

        [MethodImpl(0x8000)]
        private void manipulationTappedHandler(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void manipulationTransformedHandler(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        public bool LockScale
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool LockRotation
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

