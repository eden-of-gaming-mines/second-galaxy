﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class SpaceShipNamedEffect_Booster : SpaceShipNamedEffect
    {
        protected bool m_isNeedStartCameraAnimator;
        protected string m_cameraAnimatorParamName;
        protected bool m_isEnableCameraAnimator;
        protected bool m_isNeedStartCameraShake;
        protected bool m_isEnableCameraShake;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_IsNeedStartCameraAnimator;
        private static DelegateBridge __Hotfix_IsNeedStartCameraShake;
        private static DelegateBridge __Hotfix_EnableBoosterCamera;
        private static DelegateBridge __Hotfix_EnableBoosterCameraShake;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_Booster(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableBoosterCamera(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableBoosterCameraShake(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedStartCameraAnimator(out string paramName, out bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedStartCameraShake(out string shakeType, out bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect()
        {
        }
    }
}

