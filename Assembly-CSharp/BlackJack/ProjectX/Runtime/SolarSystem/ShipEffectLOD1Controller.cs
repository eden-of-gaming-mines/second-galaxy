﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public class ShipEffectLOD1Controller : ShipEffectLODController
    {
        public ShipEffectLOD1Controller()
        {
            base.m_LODLevel = 1;
        }
    }
}

