﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletSuperMissileFlyController : MonoBehaviour
    {
        protected PrefabSuperMissileDesc m_superMissileDesc;
        protected Vector3 m_launchInitLocalPosition;
        protected Vector3 m_launchInitWorldDir;
        protected float m_launchFlyDistance;
        protected GameObject m_missileGo;
        protected GameObject m_missileControllGo;
        protected bool m_isStartLocalOffset;
        protected float m_localOffsetLastTime;
        protected Vector3 m_missileGoLastPos;
        protected SuperMissileFlyState m_missileState;
        protected Vector3 m_worldSelfShipLocation;
        protected Vector3 m_worldTargetLocation;
        public Bounds m_hitPointBounds;
        public const float DetonationDistance = 0.5f;
        public const float PrepareDetonationDistance = 10f;
        protected float m_velocity;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Transform <LaunchLocation>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <SrcShipScale>k__BackingField;
        protected float m_realVelocity;
        protected float m_unGuideDecelerationRate;
        protected float m_guideAccelerationRate;
        protected float m_alignSpeedAccelerationRate;
        protected float m_flyUpAlignSpeed;
        protected float m_flyTurnAlignSpeed;
        protected float m_flyDownAlignSpeed;
        protected float m_realAlignSpeed;
        protected Transform m_cahedTransform;
        protected Vector3 m_step;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnHit;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnStartGuideFly;
        private DateTime m_currStateOutTime;
        protected float m_missileLaunchTime;
        protected float m_missileFlyUpTime;
        protected float m_missileFlyTurnTime;
        protected float m_missileFlyDownTime;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetFlyState;
        private static DelegateBridge __Hotfix_OnHit;
        private static DelegateBridge __Hotfix_UpdateMissileFly;
        private static DelegateBridge __Hotfix_LocalOffsetMissileGo;
        private static DelegateBridge __Hotfix_UpdateLaunch;
        private static DelegateBridge __Hotfix_UpdateFlyUp;
        private static DelegateBridge __Hotfix_UpdateFlyTurn;
        private static DelegateBridge __Hotfix_UpdateFlyDown;
        private static DelegateBridge __Hotfix_CheckIsCrossTargetPoint;
        private static DelegateBridge __Hotfix_get_Velocity;
        private static DelegateBridge __Hotfix_set_Velocity;
        private static DelegateBridge __Hotfix_get_LaunchLocation;
        private static DelegateBridge __Hotfix_set_LaunchLocation;
        private static DelegateBridge __Hotfix_get_SrcShipScale;
        private static DelegateBridge __Hotfix_set_SrcShipScale;
        private static DelegateBridge __Hotfix_add_EventOnHit;
        private static DelegateBridge __Hotfix_remove_EventOnHit;
        private static DelegateBridge __Hotfix_add_EventOnStartGuideFly;
        private static DelegateBridge __Hotfix_remove_EventOnStartGuideFly;

        public event Action EventOnHit
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStartGuideFly
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsCrossTargetPoint(Vector3 startPos, Vector3 endPos, Vector3 targetPos, float checkDistance)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(Vector3 selfLocation, float flyTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void LocalOffsetMissileGo()
        {
        }

        [MethodImpl(0x8000)]
        public void OnHit()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlyState(SuperMissileFlyState state)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateFlyDown(Vector3 targetPos)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateFlyTurn(Vector3 targetPos)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateFlyUp(Vector3 targetPos)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateLaunch()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMissileFly(Vector3 selfShipLocation, Vector3 targetLocation, Vector3 upPoint, Vector3 downPoint1, Vector3 downPoint2)
        {
        }

        public float Velocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public Transform LaunchLocation
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public float SrcShipScale
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public enum SuperMissileFlyState
        {
            Launch,
            FlyUp,
            FlyTurn,
            FlyDown
        }
    }
}

