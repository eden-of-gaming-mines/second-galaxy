﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class JumpingTunnelController : PrefabControllerBase
    {
        public Material m_material;
        public float FadeInLowSpeedValue;
        public float FadeInHighSpeedValue;
        public float DistortionLowSpeedValue;
        public float DistortionHighSpeedValue;
        public float HighLightLowSpeedValue;
        public float HighLightHighSpeedValue;
        protected Material m_redShiftMaterial;
        protected Color m_oriRedShiftColor;
        protected Material m_blueShiftMaterial;
        protected Color m_oriBlueShiftColor;
        public float ShiftBrightnessHighValue;
        [AutoBind("./Mesh_FX_Redshift", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject RedShift;
        [AutoBind("./Mesh_FX_Blueshift", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject BlueShift;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetBrightnessValue;
        private static DelegateBridge __Hotfix_UpdateJumpingTunnelWithSpeedValue;
        private static DelegateBridge __Hotfix_UpdateTunnelWarpSpeed;
        private static DelegateBridge __Hotfix_UpdateTunnelDistortion;
        private static DelegateBridge __Hotfix_UpdateTunnelHighLight;
        private static DelegateBridge __Hotfix_UpdateTunnelFadeValue;
        private static DelegateBridge __Hotfix_UpdateShiftBrightness;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private float GetBrightnessValue(Color col)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateJumpingTunnelWithSpeedValue(double currSpeed, double maxSpeed)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShiftBrightness(float brightness)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTunnelDistortion(float distortion)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTunnelFadeValue(float effectFadeIn)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTunnelHighLight(float hlValue)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTunnelWarpSpeed(float warpSpeed)
        {
        }
    }
}

