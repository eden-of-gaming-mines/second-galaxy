﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ShipTacticalEquipController : PrefabControllerBase
    {
        protected SpaceShipController m_ownerShip;
        protected SpaceShipEffectBase m_dynamicPropertiesProviderTacticalEquipEffect;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_CollectAllTacticalEquipDynamicResPath;
        private static DelegateBridge __Hotfix_PlayTacticalEquipChargeEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipFireEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipExtraEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipCancelEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipEndEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipProcessEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipGlobalDynamicPropertiesProviderByTargetDistanceEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipGlobalDynamicPropertiesProviderBySpeedOverPercent;
        private static DelegateBridge __Hotfix_PlayTacticalEquipClobalDynamicPropertiesProviderBySpeedOverloadEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipClobalDynamicPropertiesProviderBySelfSpeedEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipGlobalDynamicPropertiesProviderBySelfEnergyEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipGlobalDynamicPropertiesProviderBySelfShieldEffect;
        private static DelegateBridge __Hotfix_PlayTacticalEquipGlobalDynamicPropertiesProviderByTargetShieldPercent;
        private static DelegateBridge __Hotfix_PlayTacticalEquipGlobalDynamicPropertiesProviderByLocalTargetCountEffect;
        private static DelegateBridge __Hotfix_StopTacticalEquipGroupGlobalDynamicPropertiesProviderEffect;
        private static DelegateBridge __Hotfix_IsDynamicPropertiesProviderTacticalEquipEffectPlaying;
        private static DelegateBridge __Hotfix_get_ShipEffectCtrl;

        [MethodImpl(0x8000)]
        public void CollectAllTacticalEquipDynamicResPath(LBTacticalEquipGroupBase equip, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDynamicPropertiesProviderTacticalEquipEffectPlaying()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipCancelEffect(ClientSpaceShip ship, LBTacticalEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipChargeEffect(LBTacticalEquipGroupBase equip, SpaceShipController targetShipCtrl, uint chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipClobalDynamicPropertiesProviderBySelfSpeedEffect(LBTacticalEquipGroupPropertiesProviderBySpeed equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipClobalDynamicPropertiesProviderBySpeedOverloadEffect(LBTacticalEquipGroupPropertiesProviderByOverloadSpeed equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipEndEffect(ClientSpaceShip ship, LBTacticalEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipExtraEffect(LBTacticalEquipGroupBase equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipFireEffect(LBTacticalEquipGroupBase equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipGlobalDynamicPropertiesProviderByLocalTargetCountEffect(LBTacticalEquipGroupPropertiesProviderByLocalTargetCount equip, SpaceShipController targetShipCtrl, Func<SpaceShipController, SpaceShipController> targetGetter)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipGlobalDynamicPropertiesProviderBySelfEnergyEffect(LBTacticalEquipGroupPropertiesProviderByEnergy equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipGlobalDynamicPropertiesProviderBySelfShieldEffect(LBTacticalEquipGroupPropertiesProviderBySelfShield equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipGlobalDynamicPropertiesProviderBySpeedOverPercent(LBTacticalEquipGroupPropertiesProviderBySpeedOverPercent equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipGlobalDynamicPropertiesProviderByTargetDistanceEffect(LBTacticalEquipGroupPropertiesProviderByTargetDistance equip, SpaceShipController targetShipCtrl, Func<SpaceShipController, SpaceShipController> targetGetter)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipGlobalDynamicPropertiesProviderByTargetShieldPercent(LBTacticalEquipGroupPropertiesProviderByTargetShieldPercent equip, SpaceShipController targetShipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTacticalEquipProcessEffect(ClientSpaceShip ship, LBTacticalEquipGroupBase equip)
        {
        }

        [MethodImpl(0x8000)]
        public void StopTacticalEquipGroupGlobalDynamicPropertiesProviderEffect()
        {
        }

        protected ShipEffectController ShipEffectCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

