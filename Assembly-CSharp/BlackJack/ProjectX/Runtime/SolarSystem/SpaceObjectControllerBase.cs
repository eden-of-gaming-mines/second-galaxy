﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceObjectControllerBase : PrefabControllerBase, ISpaceObjectResource
    {
        protected string m_displayNameForUI;
        protected Transform m_transform;
        protected BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject m_clientSpaceObject;
        protected IDynamicAssetProvider m_dynamicAssetProvider;
        protected Camera m_camera;
        protected bool m_isLogicActive;
        protected float m_displayPriority;
        protected double m_distanceToSelf;
        protected bool m_isDisplayActive;
        protected List<string> m_refrencedAssetList;
        protected bool m_isCreateCompleted;
        protected bool m_isReleased;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_IsAllResourceLoadCompleted;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_OnResourcesReady;
        private static DelegateBridge __Hotfix_IsCreateCompleted;
        private static DelegateBridge __Hotfix_CreateInternal;
        private static DelegateBridge __Hotfix_OnCreateInternalComplete;
        private static DelegateBridge __Hotfix_InitSpaceObjectPrefab;
        private static DelegateBridge __Hotfix_CheckAsset;
        private static DelegateBridge __Hotfix_AllocAsset;
        private static DelegateBridge __Hotfix_SetLocation;
        private static DelegateBridge __Hotfix_SetScale;
        private static DelegateBridge __Hotfix_SetRotation;
        private static DelegateBridge __Hotfix_SetLogicActive;
        private static DelegateBridge __Hotfix_SetDisplayActive;
        private static DelegateBridge __Hotfix_SetActive;
        private static DelegateBridge __Hotfix_AddDisplayPriority;
        private static DelegateBridge __Hotfix_ClearDisplayPriority;
        private static DelegateBridge __Hotfix_CalcDistanceToCamera;
        private static DelegateBridge __Hotfix_IsObjectVisibleByCamera;
        private static DelegateBridge __Hotfix_get_DisplayNameForUI;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;
        private static DelegateBridge __Hotfix_get_DisplayPriority;
        private static DelegateBridge __Hotfix_set_DisplayPriority;
        private static DelegateBridge __Hotfix_get_DisplayActive;
        private static DelegateBridge __Hotfix_set_DisplayActive;
        private static DelegateBridge __Hotfix_get_DistanceToSelf;
        private static DelegateBridge __Hotfix_set_DistanceToSelf;
        private static DelegateBridge __Hotfix_get_IsReleased;

        [MethodImpl(0x8000)]
        public void AddDisplayPriority(float priority)
        {
        }

        [MethodImpl(0x8000)]
        protected T AllocAsset<T>(string name) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000)]
        public void CalcDistanceToCamera(Vector3 cameraLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckAsset(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearDisplayPriority()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void CreateInternal()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitSpaceObjectPrefab()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual bool IsAllResourceLoadCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCreateCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsObjectVisibleByCamera(Camera cam = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnCreateInternalComplete()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnResourcesReady()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDisplayActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetLocation(Vector3 location)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLogicActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetRotation(Vector3 rotation)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetScale(Vector3 scale)
        {
        }

        public virtual string DisplayNameForUI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float DisplayPriority
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public bool DisplayActive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public double DistanceToSelf
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public bool IsReleased
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

