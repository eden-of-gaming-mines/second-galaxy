﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using TouchScript.Layers;
    using UnityEngine;

    public class SolarSystemShipLayerController : PrefabControllerBase
    {
        [AutoBind("./SelfShipRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfShipRoot;
        [AutoBind("./SpaceObjectList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_spaceObjectList;
        [AutoBind("./BulletObjectList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_bulletObjectList;
        [AutoBind("./GlobalEffectList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_globalEffectList;
        [AutoBind("./CacheBulletObjectList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_cacheBulletObjectList;
        [AutoBind("./CacheSlotEffectList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_cacheSlotEffectList;
        [AutoBind("./CacheShipEffectList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_cacheShipEffectList;
        [AutoBind("./CacheSpaceGlobalEffectList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_cacheSpaceGlobalEffectList;
        [AutoBind("./SceneAnimationRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sceneAnimationRoot;
        [AutoBind("./CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraTransformController m_shipCameraTransformCtrl;
        [AutoBind("./CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraOperationController m_shipCameraOperationCtrl;
        [AutoBind("./CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraInputController m_cameraInputCtrl;
        [AutoBind("./CameraPlatformRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public FullscreenLayer m_fullScreenLayer;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public AudioSource m_jumpingLoopAudioSource;
        protected Dictionary<uint, SpaceObjectControllerBase> m_spaceObjectCtrlDict;
        protected TacticalViewLineController m_middleDistanceTacticalViewLineCtrl;
        protected TacticalViewLineController m_longDistanceTacticalViewLineCtrl;
        protected List<SpaceObjectControllerBase> m_sortSpaceObjectList;
        private const string SelfShipName = "SelfShip";
        private const string OtherShipName = "PlayerShip";
        private const string NpcShipName = "NpcShip";
        private const string StarGateName = "Stargate";
        private const string SpaceStationName = "SpaceStation";
        private const string SimpleObjectName = "SimpleObject";
        private const string SceneObjectName = "SceneObject";
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetSpaceObjectById;
        private static DelegateBridge __Hotfix_RemoveSpaceObject;
        private static DelegateBridge __Hotfix_ForeachSpaceObject;
        private static DelegateBridge __Hotfix_GetSpaceObjectScreenPosition_1;
        private static DelegateBridge __Hotfix_GetSpaceObjectScreenPosition_0;
        private static DelegateBridge __Hotfix_OnSolarSystemInfectNtf;
        private static DelegateBridge __Hotfix_CreateSelfPlayerShipImmediately;
        private static DelegateBridge __Hotfix_CreateOtherPlayerSpaceShip;
        private static DelegateBridge __Hotfix_CreateNpcSpaceShip;
        private static DelegateBridge __Hotfix_CreateHiredCaptainShip;
        private static DelegateBridge __Hotfix_CreateMoon;
        private static DelegateBridge __Hotfix_CreateStarGate;
        private static DelegateBridge __Hotfix_CreateSpaceStation;
        private static DelegateBridge __Hotfix_CreateSpaceSimpleObject;
        private static DelegateBridge __Hotfix_CreateSpaceSceneAnimation;
        private static DelegateBridge __Hotfix_CreateSceneObject;
        private static DelegateBridge __Hotfix_CreateSpaceDropBox;
        private static DelegateBridge __Hotfix_EnableShipLayerCamera;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_SetAllShipDisplayActive;
        private static DelegateBridge __Hotfix_SetSelfShipVisible;
        private static DelegateBridge __Hotfix_SetOtherShipVisible;
        private static DelegateBridge __Hotfix_SetNpcShipVisible;
        private static DelegateBridge __Hotfix_SetSpaceStationVisible;
        private static DelegateBridge __Hotfix_SetStarGateVisible;
        private static DelegateBridge __Hotfix_SetSpaceSimpleObjectVisible;
        private static DelegateBridge __Hotfix_SetSceneObjectVisible;
        private static DelegateBridge __Hotfix_SetBulletObjectVisible;
        private static DelegateBridge __Hotfix_SetRefNameObjectVisible;
        private static DelegateBridge __Hotfix_CreateMiddleDistanceTacticalViewLine;
        private static DelegateBridge __Hotfix_ShowMiddleDistanceTacticalViewLine;
        private static DelegateBridge __Hotfix_CreateLongDistanceTacticalViewLine;
        private static DelegateBridge __Hotfix_ShowLongDistanceTacticalViewLine;
        private static DelegateBridge __Hotfix_UpdateTacticalViewLocation;
        private static DelegateBridge __Hotfix_ResetSolarSystemTacticalViewCamera;
        private static DelegateBridge __Hotfix_PlayJumpingLoopAudio;
        private static DelegateBridge __Hotfix_GetSpaceObjectsByFilter;
        private static DelegateBridge __Hotfix_GetSelfShip;

        [MethodImpl(0x8000)]
        public SpaceShipController CreateHiredCaptainShip(ClientHiredCaptainSpaceShip clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider, bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void CreateLongDistanceTacticalViewLine(Vector3 centerPos, int circleCount, int baseSegmentCount, float baseRadius, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public void CreateMiddleDistanceTacticalViewLine(Vector3 centerPos, int circleCount, int baseSegmentCount, float baseRadius, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public MoonController CreateMoon(ClientMoon clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController CreateNpcSpaceShip(ClientNpcSpaceShip clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider, bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController CreateOtherPlayerSpaceShip(ClientOtherPlayerSpaceShip clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSceneObjectController CreateSceneObject(ClientSpaceSceneObject clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipController CreateSelfPlayerShipImmediately(ClientSelfPlayerSpaceShip clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceDropBoxController CreateSpaceDropBox(ClientSpaceDropBox clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSceneAnimationController CreateSpaceSceneAnimation(IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceSimpleObjectController CreateSpaceSimpleObject(ClientSpaceSimpleObject clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationController CreateSpaceStation(ClientSpaceStation clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public StarGateController CreateStarGate(ClientStarGate clientObj, Vector3 location, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableShipLayerCamera(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void ForeachSpaceObject(Action<SpaceObjectControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        protected SpaceShipController GetSelfShip()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceObjectControllerBase GetSpaceObjectById(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public List<SpaceObjectControllerBase> GetSpaceObjectsByFilter(Func<SpaceObjectControllerBase, bool> objectFilter)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSpaceObjectScreenPosition(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSpaceObjectScreenPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSolarSystemInfectNtf(bool isInfect)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayJumpingLoopAudio(bool isPlay)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveSpaceObject(uint objId)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetSolarSystemTacticalViewCamera()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllShipDisplayActive(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletObjectVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNpcShipVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOtherShipVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRefNameObjectVisible(List<string> refNameList, bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSceneObjectVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelfShipVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceSimpleObjectVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceStationVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarGateVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLongDistanceTacticalViewLine(bool isshow, float delayTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMiddleDistanceTacticalViewLine(bool isshow, float delayTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTacticalViewLocation(Vector3 location)
        {
        }
    }
}

