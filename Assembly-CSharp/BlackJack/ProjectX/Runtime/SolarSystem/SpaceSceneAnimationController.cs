﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceSceneAnimationController : PrefabControllerBase, ISceneAnimationCameraCtrlProvider
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <SceneAnimationCameraConfigId>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private BlackJack.ProjectX.Common.SceneCameraAnimationType <SceneCameraAnimationType>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <ClipPrefabName>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsInitRotationWithTarget>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsFollowTarget>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsLookAtTarget>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private uint <TargetObjId>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Vector3 <LookAtLocation>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Vector3 <CameraLocation>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Quaternion <CameraRotate>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private List<InSpaceNpcTalkerChatNtf> <NPCChatList>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsSceneAnimationEnd>k__BackingField;
        protected Dictionary<string, SpaceObjectControllerBase> m_realActorDict;
        protected SceneAnimationBase m_currSceneAnimation;
        protected Camera m_lensFlareCamera;
        protected Camera m_mainCamera;
        protected Transform m_bulletRoot;
        protected ISpaceTargetProvider m_target;
        protected bool m_isCameraStayOnLocation;
        protected GameObject m_sceneAniamtionGo;
        protected IDynamicAssetProvider m_dynamicAssetProvider;
        protected List<string> m_refrencedAssetList;
        protected int m_currNPCChatIndex;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSceneAnimationEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<List<SceneCameraAnimationClipFilterType>, List<string>, bool> EventOnSceneAnimationFilterSpaceObject;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnSceneAnimationDisableSceneLight;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnSceneAniamtionDisableSceneCameras;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_OnResourcesReady;
        private static DelegateBridge __Hotfix_AddRealActor;
        private static DelegateBridge __Hotfix_DelayFadeOut;
        private static DelegateBridge __Hotfix_DelayExecuteAfterPlay;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_OnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_OnSceneAnimationFilterSpaceObject;
        private static DelegateBridge __Hotfix_OnSceneAnimationDisableSceneLight;
        private static DelegateBridge __Hotfix_OnSceneAnimationDisableSceneCameras;
        private static DelegateBridge __Hotfix_AllocAsset;
        private static DelegateBridge __Hotfix_IsRelocationCamera;
        private static DelegateBridge __Hotfix_IsEnableCinemachineBrain;
        private static DelegateBridge __Hotfix_GetCameraZOffset;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_GetCameraFov;
        private static DelegateBridge __Hotfix_SetTarget;
        private static DelegateBridge __Hotfix_SetLensFlareCamera;
        private static DelegateBridge __Hotfix_SetMainCamera;
        private static DelegateBridge __Hotfix_SetBulletRoot;
        private static DelegateBridge __Hotfix_CreateFakeActors;
        private static DelegateBridge __Hotfix_InitSceneAnimationLocation;
        private static DelegateBridge __Hotfix_OnTargetDestory;
        private static DelegateBridge __Hotfix_GetRealAcotrByName;
        private static DelegateBridge __Hotfix_OnNpcChatShow;
        private static DelegateBridge __Hotfix_get_SceneAnimationCameraConfigId;
        private static DelegateBridge __Hotfix_set_SceneAnimationCameraConfigId;
        private static DelegateBridge __Hotfix_get_SceneCameraAnimationType;
        private static DelegateBridge __Hotfix_set_SceneCameraAnimationType;
        private static DelegateBridge __Hotfix_get_ClipPrefabName;
        private static DelegateBridge __Hotfix_set_ClipPrefabName;
        private static DelegateBridge __Hotfix_get_IsInitRotationWithTarget;
        private static DelegateBridge __Hotfix_set_IsInitRotationWithTarget;
        private static DelegateBridge __Hotfix_get_IsFollowTarget;
        private static DelegateBridge __Hotfix_set_IsFollowTarget;
        private static DelegateBridge __Hotfix_get_IsLookAtTarget;
        private static DelegateBridge __Hotfix_set_IsLookAtTarget;
        private static DelegateBridge __Hotfix_get_TargetObjId;
        private static DelegateBridge __Hotfix_set_TargetObjId;
        private static DelegateBridge __Hotfix_get_LookAtLocation;
        private static DelegateBridge __Hotfix_set_LookAtLocation;
        private static DelegateBridge __Hotfix_get_CameraLocation;
        private static DelegateBridge __Hotfix_set_CameraLocation;
        private static DelegateBridge __Hotfix_get_CameraRotate;
        private static DelegateBridge __Hotfix_set_CameraRotate;
        private static DelegateBridge __Hotfix_get_NPCChatList;
        private static DelegateBridge __Hotfix_set_NPCChatList;
        private static DelegateBridge __Hotfix_get_IsSceneAnimationEnd;
        private static DelegateBridge __Hotfix_set_IsSceneAnimationEnd;
        private static DelegateBridge __Hotfix_add_EventOnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_add_EventOnSceneAnimationFilterSpaceObject;
        private static DelegateBridge __Hotfix_remove_EventOnSceneAnimationFilterSpaceObject;
        private static DelegateBridge __Hotfix_add_EventOnSceneAnimationDisableSceneLight;
        private static DelegateBridge __Hotfix_remove_EventOnSceneAnimationDisableSceneLight;
        private static DelegateBridge __Hotfix_add_EventOnSceneAniamtionDisableSceneCameras;
        private static DelegateBridge __Hotfix_remove_EventOnSceneAniamtionDisableSceneCameras;

        public event Action<bool> EventOnSceneAniamtionDisableSceneCameras
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnSceneAnimationDisableSceneLight
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSceneAnimationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<SceneCameraAnimationClipFilterType>, List<string>, bool> EventOnSceneAnimationFilterSpaceObject
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AddRealActor(string actorName, SpaceObjectControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        protected T AllocAsset<T>(string name) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000)]
        public bool Create(IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateFakeActors()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DelayExecuteAfterPlay()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DelayFadeOut()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraFov()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraZOffset()
        {
        }

        [MethodImpl(0x8000)]
        protected SpaceObjectControllerBase GetRealAcotrByName(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitSceneAnimationLocation()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsEnableCinemachineBrain()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRelocationCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNpcChatShow()
        {
        }

        [MethodImpl(0x8000)]
        public void OnResourcesReady()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSceneAnimationDisableSceneCameras(bool disableCameras)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSceneAnimationDisableSceneLight(bool disableLight)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSceneAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSceneAnimationFilterSpaceObject(List<SceneCameraAnimationClipFilterType> filterTypeList, List<string> filterStrList, bool visible)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTargetDestory(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletRoot(Transform bulletRoot)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLensFlareCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMainCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTarget(ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        public int SceneAnimationCameraConfigId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public BlackJack.ProjectX.Common.SceneCameraAnimationType SceneCameraAnimationType
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public string ClipPrefabName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsInitRotationWithTarget
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsFollowTarget
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsLookAtTarget
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public uint TargetObjId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public Vector3 LookAtLocation
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public Vector3 CameraLocation
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public Quaternion CameraRotate
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public List<InSpaceNpcTalkerChatNtf> NPCChatList
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsSceneAnimationEnd
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DelayExecuteAfterPlay>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal SpaceSceneAnimationController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(0.3f);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        if (this.$this.m_currSceneAnimation != null)
                        {
                            this.$this.m_currSceneAnimation.DelayExecuteAfterPlay();
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DelayFadeOut>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

