﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletRailgunController : BulletObjectController
    {
        protected GameObject m_onHitEffectAssetAfterPenetration;
        protected GameObject m_onHitEffectObjAfterPenetration;
        protected float m_onHitEffectAfterPenetrationLastTime;
        protected Vector3 m_onHitEffectObjAfterPenetrationInitDir;
        protected bool m_isOnHitEffectAfterPenetrationLODObjectHasCreated;
        protected Vector3 m_localTargetOnHitPositionAfterPenetration;
        protected Vector3 m_localTargetOnHitNormalAfterPenetration;
        protected bool m_isNeedPenetrationHitEffect;
        protected GameObject[] m_onHitEffectAfterPenetrationLODObjectArray;
        protected float m_bulletPenetrationSpendTime;
        private static DelegateBridge __Hotfix_UpdateBulletFly;
        private static DelegateBridge __Hotfix_UpdateBulletOnHit;
        private static DelegateBridge __Hotfix_SetBulletObjectScale;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_SetOnHitAssetAfterPenetration;
        private static DelegateBridge __Hotfix_OnHitEffectAfterPenetrationLODLevelChanged;
        private static DelegateBridge __Hotfix_UpdateTrailScale;
        private static DelegateBridge __Hotfix_ShowOnHitEffectObjectAfterPenetration;
        private static DelegateBridge __Hotfix_SetOnHitEffectAfterPenetrationFollowTarget;
        private static DelegateBridge __Hotfix_GetWorldTargetOnHitAfterPenetrationPosition;
        private static DelegateBridge __Hotfix_DestroyBullet;
        private static DelegateBridge __Hotfix_SetBulletPenetrationActive;
        private static DelegateBridge __Hotfix_SetBulletTarget;

        [MethodImpl(0x8000)]
        public override void DestroyBullet()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual Vector3 GetWorldTargetOnHitAfterPenetrationPosition()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHitEffectAfterPenetrationLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void Reset()
        {
        }

        [MethodImpl(0x8000)]
        protected override void SetBulletObjectScale(GameObject bulletObject)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletPenetrationActive(bool needPenetration)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletTarget(BulletTargetProviderWarpper target, bool needPenetration)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOnHitAssetAfterPenetration(GameObject onHitAsset, Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetOnHitEffectAfterPenetrationFollowTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowOnHitEffectObjectAfterPenetration(bool isShow, Vector3 worldOnHitLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletOnHit()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateTrailScale()
        {
        }
    }
}

