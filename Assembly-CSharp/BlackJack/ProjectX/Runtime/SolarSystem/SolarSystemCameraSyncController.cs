﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SolarSystemCameraSyncController : PrefabControllerBase
    {
        [AutoBind("CameraRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraRoot;
        [AutoBind("./LayerCamera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        [AutoBind("./LayerCamera", AutoBindAttribute.InitState.NotInit, false)]
        public Animator m_cameraAnimator;
        private static DelegateBridge __Hotfix_SyncCameraRootPosition;
        private static DelegateBridge __Hotfix_SyncCameraOffset;
        private static DelegateBridge __Hotfix_SyncCameraRootRotation;
        private static DelegateBridge __Hotfix_SyncCameraLocalRotation;
        private static DelegateBridge __Hotfix_SyncCameraFov;
        private static DelegateBridge __Hotfix_GetCamera;

        [MethodImpl(0x8000)]
        public Camera GetCamera()
        {
        }

        [MethodImpl(0x8000)]
        public void SyncCameraFov(float fov)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncCameraLocalRotation(Quaternion rotation)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncCameraOffset(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncCameraRootPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void SyncCameraRootRotation(Quaternion rotation)
        {
        }
    }
}

