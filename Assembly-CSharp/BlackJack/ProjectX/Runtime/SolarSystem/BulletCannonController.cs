﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletCannonController : BulletObjectController
    {
        private static DelegateBridge __Hotfix_UpdateBulletFly;
        private static DelegateBridge __Hotfix_UpdateBulletOnHit;
        private static DelegateBridge __Hotfix_SetBulletObjectScale;
        private static DelegateBridge __Hotfix_ResetEffect;
        private static DelegateBridge __Hotfix_UpdateTrailScale;

        [MethodImpl(0x8000)]
        protected override void ResetEffect(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SetBulletObjectScale(GameObject bulletObject)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletOnHit()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateTrailScale()
        {
        }
    }
}

