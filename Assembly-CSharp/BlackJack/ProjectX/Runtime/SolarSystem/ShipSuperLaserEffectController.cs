﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ShipSuperLaserEffectController : PrefabControllerBase
    {
        protected SpaceShipController m_ownerShip;
        protected List<ShipSuperLaserEffect> m_activeEffectList;
        protected List<ShipSuperLaserEffect> m_removeEffectList;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CreateShipSuperLaserEffect;

        [MethodImpl(0x8000)]
        public ShipSuperLaserEffect CreateShipSuperLaserEffect(ShipSuperLaserEffect.BulletCreator bulletCreator)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }
    }
}

