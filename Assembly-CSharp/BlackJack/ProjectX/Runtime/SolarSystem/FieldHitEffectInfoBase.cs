﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal abstract class FieldHitEffectInfoBase
    {
        protected Vector3 m_hitPosition;
        protected float m_hitEffectRadius;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <HitEffectCurrTime>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <HitEffectCurrRadius>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private float <HitEffectCurrWaveTimeParam>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <HitEffectCurrWaveFadeParam>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_HitPosition;
        private static DelegateBridge __Hotfix_get_HitEffectRadius;
        private static DelegateBridge __Hotfix_get_HitEffectCurrTime;
        private static DelegateBridge __Hotfix_set_HitEffectCurrTime;
        private static DelegateBridge __Hotfix_get_HitEffectCurrRadius;
        private static DelegateBridge __Hotfix_set_HitEffectCurrRadius;
        private static DelegateBridge __Hotfix_get_HitEffectCurrWaveTimeParam;
        private static DelegateBridge __Hotfix_set_HitEffectCurrWaveTimeParam;
        private static DelegateBridge __Hotfix_get_HitEffectCurrWaveFadeParam;
        private static DelegateBridge __Hotfix_set_HitEffectCurrWaveFadeParam;

        [MethodImpl(0x8000)]
        public FieldHitEffectInfoBase(Vector3 position, float radius)
        {
        }

        public abstract bool IsFieldHitEffectFinished();
        public abstract void ResetData();
        public abstract void Update(float deltaTime);

        public Vector3 HitPosition
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float HitEffectRadius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public float HitEffectCurrTime
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public float HitEffectCurrRadius
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public float HitEffectCurrWaveTimeParam
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public float HitEffectCurrWaveFadeParam
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

