﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimation_Video : SceneAnimationBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <AnimationClipConfigId>k__BackingField;
        protected ConfigDataSceneCameraAnimationClipInfo m_animationClipConfInfo;
        protected CGPlayerUITask m_cgPlayerUITask;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_IsRelocationCamera;
        private static DelegateBridge __Hotfix_GetCameraZOffset;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_GetCameraFov;
        private static DelegateBridge __Hotfix_CreateClipAssetGo;
        private static DelegateBridge __Hotfix_OnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_get_AnimationClipConfigId;
        private static DelegateBridge __Hotfix_set_AnimationClipConfigId;

        [MethodImpl(0x8000)]
        public SceneAnimation_Video(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public override void CreateClipAssetGo()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetCameraFov()
        {
        }

        [MethodImpl(0x8000)]
        public override Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetCameraZOffset()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsRelocationCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSceneAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        public override void Play(Transform target, float targetSize)
        {
        }

        [MethodImpl(0x8000)]
        public override void Release()
        {
        }

        [MethodImpl(0x8000)]
        public override void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float delayTime, Vector3 targetLocation)
        {
        }

        public int AnimationClipConfigId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

