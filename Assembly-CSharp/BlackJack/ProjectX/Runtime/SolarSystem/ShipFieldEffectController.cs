﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipFieldEffectController : MonoBehaviour
    {
        private bool m_isFieldFadeInAnimationPlaying;
        private bool m_isFieldActive;
        private Collider m_fieldCollider;
        private Material m_fieldMaterial;
        private FieldHitEffectInfoBase[] m_fieldHitEffects;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_IsFieldActivate;
        private static DelegateBridge __Hotfix_ActivateField;
        private static DelegateBridge __Hotfix_InActivateField;
        private static DelegateBridge __Hotfix_OnFieldFadeInAnimationStart;
        private static DelegateBridge __Hotfix_OnFieldFadeInAnimationEnd;
        private static DelegateBridge __Hotfix_OnFieldFadeOutAnimationEnd;
        private static DelegateBridge __Hotfix_GetGlobalHitPointByRay;
        private static DelegateBridge __Hotfix_OnHitFieldOnce;
        private static DelegateBridge __Hotfix_OnContinuousHitStart;
        private static DelegateBridge __Hotfix_OnContinuousHitUpdate;
        private static DelegateBridge __Hotfix_OnContinuousHitFinished;
        private static DelegateBridge __Hotfix_Update;

        [MethodImpl(0x8000)]
        public void ActivateField()
        {
        }

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public void GetGlobalHitPointByRay(Ray ray, float testDistance, out Vector3 hitPoint, out Vector3 hitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        public void InActivateField()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFieldActivate()
        {
        }

        [MethodImpl(0x8000)]
        public void OnContinuousHitFinished(int hitDataId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnContinuousHitStart(int hitDataId, Vector3 position, float radius, float effectPeriodTime)
        {
        }

        [MethodImpl(0x8000)]
        public void OnContinuousHitUpdate(int hitDataId, ShipContinuousHitFieldUpdateDataInfo updateData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFieldFadeInAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnFieldFadeInAnimationStart()
        {
        }

        [MethodImpl(0x8000)]
        public void OnFieldFadeOutAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitFieldOnce(Vector3 position, float radius, float effectPeriodTime)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

