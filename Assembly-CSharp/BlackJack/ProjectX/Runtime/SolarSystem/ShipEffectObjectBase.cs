﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipEffectObjectBase : PrefabControllerBase
    {
        protected ISpaceTargetProvider m_target;
        protected Action<bool, string, PlaySoundOption, bool> m_soundPlayerAction;
        private Action<string, float> m_setSoundParamAction;
        protected string m_effectAssetPath;
        protected string m_effectName;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsFollowTarget>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private PrefabShipEffectDesc.ShipEffectScaleType <ScaleType>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private string <EffectSoundName>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private PlaySoundOption <EffectPlaySoundOption>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsSoundLoop>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <EffectSoundParamName>k__BackingField;
        protected Vector3 m_defaultLocalScale;
        protected DateTime m_effectLifeDateTime;
        protected Animator m_effectAnimator;
        private static DelegateBridge __Hotfix_CreateShipEffectObject;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_SetLifeTime;
        private static DelegateBridge __Hotfix_SetParent;
        private static DelegateBridge __Hotfix_SetTarget;
        private static DelegateBridge __Hotfix_GetTarget;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_OnEffectStart;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_Destroy;
        private static DelegateBridge __Hotfix_PlayAnimation_1;
        private static DelegateBridge __Hotfix_PlayAnimation_0;
        private static DelegateBridge __Hotfix_PlayAnimation_2;
        private static DelegateBridge __Hotfix_SetSoundParam;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_RestEffect;
        private static DelegateBridge __Hotfix_get_EffectAnimator;
        private static DelegateBridge __Hotfix_set_EffectAnimator;
        private static DelegateBridge __Hotfix_get_IsFollowTarget;
        private static DelegateBridge __Hotfix_set_IsFollowTarget;
        private static DelegateBridge __Hotfix_get_ScaleType;
        private static DelegateBridge __Hotfix_set_ScaleType;
        private static DelegateBridge __Hotfix_get_EffectSoundName;
        private static DelegateBridge __Hotfix_set_EffectSoundName;
        private static DelegateBridge __Hotfix_get_EffectPlaySoundOption;
        private static DelegateBridge __Hotfix_set_EffectPlaySoundOption;
        private static DelegateBridge __Hotfix_get_IsSoundLoop;
        private static DelegateBridge __Hotfix_set_IsSoundLoop;
        private static DelegateBridge __Hotfix_get_EffectSoundParamName;
        private static DelegateBridge __Hotfix_set_EffectSoundParamName;

        [MethodImpl(0x8000)]
        public static T CreateShipEffectObject<T>(string assetPath, GameObject asset, PrefabShipEffectDesc effectDesc, float lifeTime = 0f) where T: ShipEffectObjectBase
        {
        }

        [MethodImpl(0x8000)]
        public void Destroy()
        {
        }

        [MethodImpl(0x8000)]
        public ISpaceTargetProvider GetTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void Init(string effectAssetPath, string effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnEffectStart()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public bool PlayAnimation(string paramName, bool value)
        {
        }

        [MethodImpl(0x8000)]
        public bool PlayAnimation(string paramName, int value)
        {
        }

        [MethodImpl(0x8000)]
        public bool PlayAnimation(string paramName, float value)
        {
        }

        [MethodImpl(0x8000)]
        protected void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void RestEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void SetLifeTime(float lifeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParent(Transform parent, float scaleMulit = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSoundParam(float value)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetTarget(ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public void StartEffect(Action<bool, string, PlaySoundOption, bool> soundPlayerAction, Action<string, float> setSoundParamAction, bool isNeedPlayerSound = true)
        {
        }

        [MethodImpl(0x8000)]
        public void StopEffect(float delayTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        protected Animator EffectAnimator
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public bool IsFollowTarget
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public PrefabShipEffectDesc.ShipEffectScaleType ScaleType
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public string EffectSoundName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public PlaySoundOption EffectPlaySoundOption
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsSoundLoop
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public string EffectSoundParamName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

