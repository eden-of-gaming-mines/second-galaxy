﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface IWeaponOwnerProvider
    {
        SkinnedMeshRenderer GetOwnerSkinMeshRender();
        PrefabNormalWeaponDesc GetPrefabNormalWeaponDesc();
        PrefabSuperWeaponDesc GetPrefabSuperWeaponDesc();
        List<GameObject> GetRealSlotsByLogicWeaponSlotIndex(int groupIndex, int slotIndex, out bool isCreateVisibleObject);
        List<GameObject> GetRealSlotsByNormalWeaponSlotIndex(int slotIndex);
        List<GameObject> GetRealSlotsBySuperWeaponSlotIndex(int slotIndex);
        float GetShipWeaponEffectScale();
        float GetShipWeaponScale();
        float GetSuperWeaponScale();
        Transform GetWeaponOwnerRoot();
        bool IsLogicWeaponSlotWithIndexExist(int groupIndex, int slotIndex);
    }
}

