﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipWeaponLOD0Controller : ShipWeaponController
    {
        private GameObject m_weaponGo;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_CreateShipWeaponGroupControl;
        private static DelegateBridge __Hotfix_StartFireImp;
        private static DelegateBridge __Hotfix_StartCharge;
        private static DelegateBridge __Hotfix_OnShipDeath;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public override void CreateShipWeaponGroupControl(List<WeaponSlotGroupConstructInfo> creatorInfos, IWeaponOwnerProvider ownerProvider)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnShipDeath()
        {
        }

        [MethodImpl(0x8000)]
        public override Transform StartCharge(int groupIndex, int unitIndex, BulletTargetProviderWarpper bulletTarget, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartFireImp(int groupIndex, int unitIndex, BulletTargetProviderWarpper bulletTarget, float scale, float lastTime = -1f, Transform defaultLaunchPoint = null)
        {
        }
    }
}

