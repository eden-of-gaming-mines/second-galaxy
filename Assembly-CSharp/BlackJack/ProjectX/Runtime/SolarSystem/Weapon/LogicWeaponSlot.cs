﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class LogicWeaponSlot
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsAbleControllRealSlot>k__BackingField;
        protected int m_logicWeaponSlotIndex;
        protected List<RealWeaponSlot> m_realWeaponSlotList;
        protected List<SlotEffectObject> m_fireEffectObjectList;
        protected List<SlotEffectObject> m_chargeEffectObjectList;
        protected List<SlotEffectObject> m_removeEffectList;
        protected RealWeaponSlot m_currTraceRealWeaponSlot;
        protected const string EffectParamName = "BulletState";
        protected const int EffectParam_Init = 0;
        protected const int EffectParam_Start = 1;
        protected const int EffectParam_FadeOut = 3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_TraceTarget;
        private static DelegateBridge __Hotfix_WeaponTraceTargetPosition;
        private static DelegateBridge __Hotfix_GetLaunchLocationGameObject;
        private static DelegateBridge __Hotfix_GetFirePointFromOneSide;
        private static DelegateBridge __Hotfix_GetSuitableWeaponSlotToTraceTarget;
        private static DelegateBridge __Hotfix_get_IsAbleControllRealSlot;
        private static DelegateBridge __Hotfix_set_IsAbleControllRealSlot;
        private static DelegateBridge __Hotfix_get_LogicWeaponSlotIndex;
        private static DelegateBridge __Hotfix_set_LogicWeaponSlotIndex;
        private static DelegateBridge __Hotfix_get_RealWeaponSlotList;
        private static DelegateBridge __Hotfix_set_RealWeaponSlotList;

        [MethodImpl(0x8000)]
        public Transform GetFirePointFromOneSide(Transform shipRoot, DroneLaunchSide side, int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetLaunchLocationGameObject(int unitIndex, Vector3 worldTargetLocation)
        {
        }

        [MethodImpl(0x8000)]
        private RealWeaponSlot GetSuitableWeaponSlotToTraceTarget(Vector3 worldTargetLocation)
        {
        }

        [MethodImpl(0x8000)]
        public void TraceTarget(Vector3 targetPos)
        {
        }

        [MethodImpl(0x8000)]
        protected void WeaponTraceTargetPosition(Vector3 targetPosition)
        {
        }

        public bool IsAbleControllRealSlot
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public int LogicWeaponSlotIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public List<RealWeaponSlot> RealWeaponSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

