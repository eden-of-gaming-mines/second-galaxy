﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipSuperWeaponController : PrefabControllerBase, IShipSuperWeaponController
    {
        protected int m_LODLevel;
        protected float m_superWeaponScale;
        protected WeaponCategory m_weaponCategory;
        protected PrefabSuperWeaponEffectDesc m_superWeaponEffectDesc;
        protected Dictionary<KeyValuePair<uint, ushort>, Transform> m_cachChargePointList;
        protected ShipSuperWeaponGroup m_weaponGroup;
        protected List<SlotEffectObject> m_fireEffectObjectList;
        protected List<SlotEffectObject> m_chargeEffectObjectList;
        protected List<SlotEffectObject> m_removeEffectList;
        public static string FireLODAssetName;
        public static string ChargingLODAssetName;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_SetWeaponTraceTarget;
        private static DelegateBridge __Hotfix_GetWeaponTraceTarget;
        private static DelegateBridge __Hotfix_CreateShipSuperWeaponGroupControl;
        private static DelegateBridge __Hotfix_CreateSuperWeaponGroup;
        private static DelegateBridge __Hotfix_SetSuperWeaponEffectDesc;
        private static DelegateBridge __Hotfix_StartSuperWeaponLaunchAnimation;
        private static DelegateBridge __Hotfix_StopSuperWeaponLaunchAnimation;
        private static DelegateBridge __Hotfix_StartCharge;
        private static DelegateBridge __Hotfix_StartPlasmSuperWeaponCharge;
        private static DelegateBridge __Hotfix_StartCommonSuperWeaponCharge;
        private static DelegateBridge __Hotfix_StartFire;
        private static DelegateBridge __Hotfix_StartCommonSuperWeaponFire;
        private static DelegateBridge __Hotfix_StartPlasmSuperWeaponFire;
        private static DelegateBridge __Hotfix_StopAllFire;
        private static DelegateBridge __Hotfix_StopAllCharge;
        private static DelegateBridge __Hotfix_LoseTraceTarget;
        private static DelegateBridge __Hotfix_GetBulletLaunchInfo;
        private static DelegateBridge __Hotfix_AddChargePoint;
        private static DelegateBridge __Hotfix_GetAndRemoveChargePoint;
        private static DelegateBridge __Hotfix_UpdateSlotEffectList;
        private static DelegateBridge __Hotfix_StopSlotEffect;

        [MethodImpl(0x8000)]
        public void AddChargePoint(uint processId, ushort unitIndex, Transform chargePoint)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CreateShipSuperWeaponGroupControl(WeaponSlotGroupConstructInfo creatorInfo, IWeaponOwnerProvider ownerProvider)
        {
        }

        [MethodImpl(0x8000)]
        protected ShipSuperWeaponGroup CreateSuperWeaponGroup(WeaponSlotGroupConstructInfo creatorInfo)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetAndRemoveChargePoint(uint processId, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public BulletLaunchInfo GetBulletLaunchInfo(int unitIndex, bool isCalcLaunchPoint = true)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper GetWeaponTraceTarget()
        {
        }

        [MethodImpl(0x8000)]
        public void LoseTraceTarget(IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuperWeaponEffectDesc(PrefabSuperWeaponEffectDesc weaponEffectDesc)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWeaponTraceTarget(BulletTargetProviderWarpper traceTarget)
        {
        }

        [MethodImpl(0x8000)]
        public virtual Transform StartCharge(int unitIndex, BulletTargetProviderWarpper bulletTarget, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartCommonSuperWeaponCharge(Transform launchLocation, BulletTargetProviderWarpper bulletTarget, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartCommonSuperWeaponFire(Transform launchLocation, BulletTargetProviderWarpper bulletTarget, float lastTime)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartFire(int unitIndex, BulletTargetProviderWarpper bulletTarget, float lastTime = -1f, Transform defaultLaunchPoint = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartPlasmSuperWeaponCharge(int unitIndex, Transform launchLocation, BulletTargetProviderWarpper bulletTarget, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartPlasmSuperWeaponFire(int unitIndex, Transform launchLocation, BulletTargetProviderWarpper bulletTarget, float lastTime)
        {
        }

        [MethodImpl(0x8000)]
        public void StartSuperWeaponLaunchAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void StopAllCharge()
        {
        }

        [MethodImpl(0x8000)]
        public void StopAllFire()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopSlotEffect(List<SlotEffectObject> effectList)
        {
        }

        [MethodImpl(0x8000)]
        public void StopSuperWeaponLaunchAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateSlotEffectList(List<SlotEffectObject> effectList)
        {
        }
    }
}

