﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SlotEffectObject
    {
        public GameObject m_effectAsset;
        public GameObject m_effectObject;
        public CRIAudioSourceHelperImpl m_audioHelper;
        public BulletTargetProviderWarpper m_target;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <WeaponGroupIndex>k__BackingField;
        protected GameObject m_effectLOD0Object;
        protected GameObject m_effectLOD1Object;
        protected GameObject m_effectLOD2Object;
        protected float m_scale;
        protected DateTime m_effectStopTime;
        protected DateTime m_lifeTime;
        protected Animator m_animator;
        protected int m_LODLevel;
        protected static string FireLODAssetName;
        protected static string ChargingLODAssetName;
        protected ParticleSystem[] m_particleSystems;
        protected const string EffectParamName = "BulletState";
        protected const int EffectParam_Init = 0;
        protected const int EffectParam_Start = 1;
        protected const int EffectParam_FadeOut = 3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PlayEffect_0;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_Destory;
        private static DelegateBridge __Hotfix_SetLODLevel;
        private static DelegateBridge __Hotfix_GetLODAssetName;
        private static DelegateBridge __Hotfix_SetScale;
        private static DelegateBridge __Hotfix_PlayAudio;
        private static DelegateBridge __Hotfix_StopAudio;
        private static DelegateBridge __Hotfix_PlayParticle;
        private static DelegateBridge __Hotfix_IsStop;
        private static DelegateBridge __Hotfix_PlayAnimation;
        private static DelegateBridge __Hotfix_SetEffectLifeTime;
        private static DelegateBridge __Hotfix_GetCurrLODObject;
        private static DelegateBridge __Hotfix_SetCurrLODObject;
        private static DelegateBridge __Hotfix_SetCurrLODObjectVisible;
        private static DelegateBridge __Hotfix_PlayEffect_1;
        private static DelegateBridge __Hotfix_ResetEffect;
        private static DelegateBridge __Hotfix_get_WeaponGroupIndex;
        private static DelegateBridge __Hotfix_set_WeaponGroupIndex;
        private static DelegateBridge __Hotfix_get_LifeTime;
        private static DelegateBridge __Hotfix_set_LifeTime;

        [MethodImpl(0x8000)]
        public void Destory()
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetCurrLODObject(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetLODAssetName()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStop()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayAnimation(string paramName, int paramValue)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayAudio()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void PlayEffect(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayParticle()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Reset()
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetEffect(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrLODObject(int LODLevel, GameObject LODObject)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCurrLODObjectVisible(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEffectLifeTime(float time)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetScale(float scale)
        {
        }

        [MethodImpl(0x8000)]
        protected void StopAudio()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        public int WeaponGroupIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public DateTime LifeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

