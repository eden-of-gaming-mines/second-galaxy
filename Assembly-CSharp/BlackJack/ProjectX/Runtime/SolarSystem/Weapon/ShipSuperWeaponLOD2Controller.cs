﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipSuperWeaponLOD2Controller : ShipSuperWeaponController
    {
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_StartFire;
        private static DelegateBridge __Hotfix_StartCharge;

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        public override Transform StartCharge(int unitIndex, BulletTargetProviderWarpper bulletTarget, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartFire(int unitIndex, BulletTargetProviderWarpper bulletTarget, float lastTime = -1f, Transform defaultLaunchPoint = null)
        {
        }
    }
}

