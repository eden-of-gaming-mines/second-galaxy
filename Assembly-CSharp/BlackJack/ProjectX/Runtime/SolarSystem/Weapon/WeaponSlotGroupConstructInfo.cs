﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class WeaponSlotGroupConstructInfo
    {
        private int m_groupIndex;
        private StoreItemType m_itemType;
        private WeaponCategory m_weaponType;
        private EquipCategory m_equipType;
        private int m_startLogicWeaponIndex;
        private int m_totalLogicWeaponSlotCount;
        private int m_usedLogicWeaponSlotCount;
        private int m_unitCount;
        private int m_waveCount;
        private GameObject m_weaponLODPrefab;
        private float m_weaponRotateSpeed;
        private float m_weaponPitchSpeed;
        protected GameObject m_chargeEffectAsset;
        protected GameObject m_fireEffectAsset;
        protected GameObject m_bulletEffectAsset;
        protected GameObject m_onHitEffectAsset;
        protected float m_weaponEffectScale;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GroupIndex;
        private static DelegateBridge __Hotfix_set_GroupIndex;
        private static DelegateBridge __Hotfix_get_ItemType;
        private static DelegateBridge __Hotfix_set_ItemType;
        private static DelegateBridge __Hotfix_get_WeaponType;
        private static DelegateBridge __Hotfix_set_WeaponType;
        private static DelegateBridge __Hotfix_get_EquipType;
        private static DelegateBridge __Hotfix_set_EquipType;
        private static DelegateBridge __Hotfix_get_StartLogicWeaponIndex;
        private static DelegateBridge __Hotfix_set_StartLogicWeaponIndex;
        private static DelegateBridge __Hotfix_get_TotalLogicWeaponSlotCount;
        private static DelegateBridge __Hotfix_set_TotalLogicWeaponSlotCount;
        private static DelegateBridge __Hotfix_get_UsedLogicWeaponSlotCount;
        private static DelegateBridge __Hotfix_set_UsedLogicWeaponSlotCount;
        private static DelegateBridge __Hotfix_get_UnitCount;
        private static DelegateBridge __Hotfix_set_UnitCount;
        private static DelegateBridge __Hotfix_get_WaveCount;
        private static DelegateBridge __Hotfix_set_WaveCount;
        private static DelegateBridge __Hotfix_get_WeaponLODPrefab;
        private static DelegateBridge __Hotfix_set_WeaponLODPrefab;
        private static DelegateBridge __Hotfix_get_WeaponRotateSpeed;
        private static DelegateBridge __Hotfix_set_WeaponRotateSpeed;
        private static DelegateBridge __Hotfix_get_WeaponPitchSpeed;
        private static DelegateBridge __Hotfix_set_WeaponPitchSpeed;
        private static DelegateBridge __Hotfix_get_ChargingEffectAsset;
        private static DelegateBridge __Hotfix_set_ChargingEffectAsset;
        private static DelegateBridge __Hotfix_get_FireEffectAsset;
        private static DelegateBridge __Hotfix_set_FireEffectAsset;
        private static DelegateBridge __Hotfix_get_BulletEffectAsset;
        private static DelegateBridge __Hotfix_set_BulletEffectAsset;
        private static DelegateBridge __Hotfix_get_OnHitEffectAsset;
        private static DelegateBridge __Hotfix_set_OnHitEffectAsset;
        private static DelegateBridge __Hotfix_get_WeaponEffectScale;
        private static DelegateBridge __Hotfix_set_WeaponEffectScale;

        public int GroupIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public StoreItemType ItemType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public WeaponCategory WeaponType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public EquipCategory EquipType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int StartLogicWeaponIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int TotalLogicWeaponSlotCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int UsedLogicWeaponSlotCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int UnitCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int WaveCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject WeaponLODPrefab
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float WeaponRotateSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float WeaponPitchSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject ChargingEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject FireEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject BulletEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject OnHitEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float WeaponEffectScale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

