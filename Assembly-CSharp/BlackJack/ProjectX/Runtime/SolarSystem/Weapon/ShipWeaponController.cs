﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipWeaponController : PrefabControllerBase, IShipWeaponController
    {
        protected int m_LODLevel;
        protected float m_shipWeaponEffectScale;
        protected float m_shipWeaponScale;
        protected NormalWeaponSlotLODGroup m_normalWeaponGroup;
        protected WeaponSlotLODGroup[] m_weaponGroups;
        protected Dictionary<KeyValuePair<uint, ushort>, Transform> m_cacheChargePointList;
        protected DateTime m_nextTraceTargetTime;
        protected float m_traceTargeTimeDelay;
        protected Vector3 m_targetLocalHitPos;
        protected List<SlotEffectObject> m_fireEffectObjectList;
        protected List<SlotEffectObject> m_chargeEffectObjectList;
        protected List<SlotEffectObject> m_removeEffectList;
        protected DroneLaunchSide m_currSniperDroneLaunchSide;
        protected DroneLaunchSide m_currFightDroneLaunchSide;
        protected const int MaxWeaponGroupCount = 3;
        protected const int NormalWeaponGroupIndex = -1;
        public static string FireLODAssetName;
        public static string ChargingLODAssetName;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CreateShipWeaponGroupControl;
        private static DelegateBridge __Hotfix_CreateShipNormalWeaponGroupControl;
        private static DelegateBridge __Hotfix_SetNormalWeaponEffectDesc;
        private static DelegateBridge __Hotfix_SetWeaponTraceTarget;
        private static DelegateBridge __Hotfix_GetWeaponTraceTarget;
        private static DelegateBridge __Hotfix_SetTraceTargetLocalOnHitPos;
        private static DelegateBridge __Hotfix_StartFire;
        private static DelegateBridge __Hotfix_StartFireImp;
        private static DelegateBridge __Hotfix_StartNormalWeaponFire;
        private static DelegateBridge __Hotfix_StopFireAtTarget;
        private static DelegateBridge __Hotfix_StopChargeAtTarget;
        private static DelegateBridge __Hotfix_StopAllFire;
        private static DelegateBridge __Hotfix_StopAllCharge;
        private static DelegateBridge __Hotfix_StartCharge;
        private static DelegateBridge __Hotfix_LoseTraceTarget;
        private static DelegateBridge __Hotfix_StartNormalWeaponCharge;
        private static DelegateBridge __Hotfix_GetBulletLaunchInfo;
        private static DelegateBridge __Hotfix_GetBulletLaunchInfoForNormalWeaponGroup;
        private static DelegateBridge __Hotfix_AddChargePoint;
        private static DelegateBridge __Hotfix_GetAndRemoveChargePoint;
        private static DelegateBridge __Hotfix_GetCurrSniperDroneLaunchSide;
        private static DelegateBridge __Hotfix_GetCurrFightDroneLaunchSide;
        private static DelegateBridge __Hotfix_GetFirePointFromOneSide;
        private static DelegateBridge __Hotfix_OnShipDeath;
        private static DelegateBridge __Hotfix_GetWeaponGroupByIndex;
        private static DelegateBridge __Hotfix_CreateWeaponSlotLODGroup;
        private static DelegateBridge __Hotfix_CombineShipWeapon;
        private static DelegateBridge __Hotfix_GetNextLaunchSide;
        private static DelegateBridge __Hotfix_UpdateSlotEffectList;
        private static DelegateBridge __Hotfix_StopSlotEffect;

        [MethodImpl(0x8000)]
        public void AddChargePoint(uint processId, ushort unitIndex, Transform chargePoint)
        {
        }

        [MethodImpl(0x8000)]
        protected void CombineShipWeapon(SkinnedMeshRenderer baseRender, List<SkinnedMeshRenderer> combineRenderList)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CreateShipNormalWeaponGroupControl(WeaponSlotGroupConstructInfo creatorInfo, IWeaponOwnerProvider ownerProvider)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CreateShipWeaponGroupControl(List<WeaponSlotGroupConstructInfo> creatorInfos, IWeaponOwnerProvider ownerProvider)
        {
        }

        [MethodImpl(0x8000)]
        protected WeaponSlotLODGroup CreateWeaponSlotLODGroup(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetAndRemoveChargePoint(uint processId, ushort unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public BulletLaunchInfo GetBulletLaunchInfo(int groupIndex, int unitIndex, bool isCalcLaunchPoint = true)
        {
        }

        [MethodImpl(0x8000)]
        public BulletLaunchInfo GetBulletLaunchInfoForNormalWeaponGroup(int unitIndex, bool isCalcLaunchPoint = true)
        {
        }

        [MethodImpl(0x8000)]
        public DroneLaunchSide GetCurrFightDroneLaunchSide()
        {
        }

        [MethodImpl(0x8000)]
        public DroneLaunchSide GetCurrSniperDroneLaunchSide()
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetFirePointFromOneSide(DroneLaunchSide side, int groupIndex, int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected DroneLaunchSide GetNextLaunchSide(DroneLaunchSide currSide)
        {
        }

        [MethodImpl(0x8000)]
        protected WeaponSlotLODGroup GetWeaponGroupByIndex(int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper GetWeaponTraceTarget(int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void LoseTraceTarget(IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnShipDeath()
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetNormalWeaponEffectDesc(PrefabWeaponEffectDesc weaponEffectDesc)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTraceTargetLocalOnHitPos(Vector3 localOnHit)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWeaponTraceTarget(int groupIndex, BulletTargetProviderWarpper traceTarget)
        {
        }

        [MethodImpl(0x8000)]
        public virtual Transform StartCharge(int groupIndex, int unitIndex, BulletTargetProviderWarpper bulletTarget, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void StartFire(int groupIndex, int unitIndex, BulletTargetProviderWarpper bulletTarget, float lastTime = -1f, Transform defaultLaunchPoint = null)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartFireImp(int groupIndex, int unitIndex, BulletTargetProviderWarpper bulletTarget, float scale, float lastTime = -1f, Transform defaultLaunchPoint = null)
        {
        }

        [MethodImpl(0x8000)]
        public Transform StartNormalWeaponCharge(int unitIndex, BulletTargetProviderWarpper bulletTarget, float chargeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void StartNormalWeaponFire(int unitIndex, BulletTargetProviderWarpper bulletTarget, float lastTime = -1f, Transform defaultLaunchPoint = null)
        {
        }

        [MethodImpl(0x8000)]
        public void StopAllCharge()
        {
        }

        [MethodImpl(0x8000)]
        public void StopAllFire()
        {
        }

        [MethodImpl(0x8000)]
        public void StopChargeAtTarget(IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public void StopFireAtTarget(IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void StopSlotEffect(int weaponGroupIndex, List<SlotEffectObject> effectList)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateSlotEffectList(List<SlotEffectObject> effectList)
        {
        }
    }
}

