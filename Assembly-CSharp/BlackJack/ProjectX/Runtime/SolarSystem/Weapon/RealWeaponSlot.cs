﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class RealWeaponSlot
    {
        protected GameObject m_root;
        protected List<GameObject> m_shootPointList;
        protected GameObject m_rotationRoot;
        protected GameObject m_pitchRoot;
        protected float m_pitchMaxDegree;
        protected float m_pitchMinDegree;
        protected float m_rotationMaxDegree;
        protected float m_rotationMinDegree;
        public float m_rotationSpeed;
        public float m_pitchSpeed;
        public Vector3 m_targetPosition;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initial;
        private static DelegateBridge __Hotfix_InitialSuperWeaponSlot;
        private static DelegateBridge __Hotfix_InitialNormalWeaponSlot;
        private static DelegateBridge __Hotfix_TraceTarget;
        private static DelegateBridge __Hotfix_ConvertDegree;
        private static DelegateBridge __Hotfix_CreateFakeShootPointList;
        private static DelegateBridge __Hotfix_get_Root;
        private static DelegateBridge __Hotfix_get_ShootPointList;
        private static DelegateBridge __Hotfix_set_ShootPointList;

        [MethodImpl(0x8000)]
        private float ConvertDegree(float degree)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateFakeShootPointList(GameObject shootPoint, int unitCount)
        {
        }

        [MethodImpl(0x8000)]
        public void Initial(GameObject root, int unitCount, PrefabWeaponDesc weaponDesc = null)
        {
        }

        [MethodImpl(0x8000)]
        public void InitialNormalWeaponSlot(GameObject root, int unitCount, PrefabNormalWeaponDesc normalWeaponDesc)
        {
        }

        [MethodImpl(0x8000)]
        public void InitialSuperWeaponSlot(GameObject root, int unitCount, PrefabSuperWeaponDesc superWeaponDesc)
        {
        }

        [MethodImpl(0x8000)]
        public void TraceTarget(float delayTime)
        {
        }

        public GameObject Root
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<GameObject> ShootPointList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

