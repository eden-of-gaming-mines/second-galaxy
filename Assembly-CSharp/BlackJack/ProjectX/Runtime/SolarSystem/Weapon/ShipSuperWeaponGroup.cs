﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipSuperWeaponGroup : WeaponSlotLODGroup
    {
        protected GameObject m_secondChargeEffectAsset;
        protected GameObject m_secondFireEffectAsset;
        protected GameObject m_secondBulletEffectAsset;
        protected GameObject m_secondOnHitEffectAsset;
        protected GameObject m_onHitEffectAssetAfterPenetration;
        protected float m_secondWeaponEffectScale;
        protected float m_secondFireEffectDurationTime;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initial;
        private static DelegateBridge __Hotfix_GetLaunchLocationGameObject;
        private static DelegateBridge __Hotfix_CalcSecondFireEffectDurationTime;
        private static DelegateBridge __Hotfix_get_SecondChargingEffectAsset;
        private static DelegateBridge __Hotfix_set_SecondChargingEffectAsset;
        private static DelegateBridge __Hotfix_get_SecondFireEffectAsset;
        private static DelegateBridge __Hotfix_set_SecondFireEffectAsset;
        private static DelegateBridge __Hotfix_get_SecondBulletEffectAsset;
        private static DelegateBridge __Hotfix_set_SecondBulletEffectAsset;
        private static DelegateBridge __Hotfix_get_SecondOnHitEffectAsset;
        private static DelegateBridge __Hotfix_set_SecondOnHitEffectAsset;
        private static DelegateBridge __Hotfix_get_OnHitEffectAssetAfterPenetration;
        private static DelegateBridge __Hotfix_set_OnHitEffectAssetAfterPenetration;
        private static DelegateBridge __Hotfix_get_SecondWeaponEffectScale;
        private static DelegateBridge __Hotfix_set_SecondWeaponEffectScale;

        [MethodImpl(0x8000)]
        protected void CalcSecondFireEffectDurationTime()
        {
        }

        [MethodImpl(0x8000)]
        public override Transform GetLaunchLocationGameObject(int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initial(WeaponSlotGroupConstructInfo creatorInfo, IWeaponOwnerProvider ownerProvider, List<GameObject> weaponGoList = null)
        {
        }

        public GameObject SecondChargingEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject SecondFireEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject SecondBulletEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject SecondOnHitEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject OnHitEffectAssetAfterPenetration
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float SecondWeaponEffectScale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

