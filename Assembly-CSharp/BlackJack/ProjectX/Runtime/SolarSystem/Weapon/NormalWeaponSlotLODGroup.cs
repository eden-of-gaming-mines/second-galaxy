﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class NormalWeaponSlotLODGroup : WeaponSlotLOD1Group
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initial;
        private static DelegateBridge __Hotfix_GetLaunchLocationGameObject;

        [MethodImpl(0x8000)]
        public override Transform GetLaunchLocationGameObject(int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initial(WeaponSlotGroupConstructInfo creatorInfo, IWeaponOwnerProvider ownerProvider, List<GameObject> weaponGoList = null)
        {
        }
    }
}

