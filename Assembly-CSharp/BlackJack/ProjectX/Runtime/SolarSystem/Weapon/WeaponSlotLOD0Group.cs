﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class WeaponSlotLOD0Group : WeaponSlotLODGroup
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initial;
        private static DelegateBridge __Hotfix_TraceTarget;
        private static DelegateBridge __Hotfix_GetLaunchLocationGameObject;
        private static DelegateBridge __Hotfix_GetFirePointFromOneSide;

        [MethodImpl(0x8000)]
        public override Transform GetFirePointFromOneSide(Transform shipRoot, DroneLaunchSide side, int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override Transform GetLaunchLocationGameObject(int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override void Initial(WeaponSlotGroupConstructInfo creatorInfo, IWeaponOwnerProvider ownerProvider, List<GameObject> weaponGoList = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void TraceTarget()
        {
        }
    }
}

