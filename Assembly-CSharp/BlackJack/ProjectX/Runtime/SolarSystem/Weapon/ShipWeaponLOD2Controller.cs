﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ShipWeaponLOD2Controller : ShipWeaponController
    {
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_CreateShipWeaponGroupControl;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public override void CreateShipWeaponGroupControl(List<WeaponSlotGroupConstructInfo> creatorInfos, IWeaponOwnerProvider ownerProvider)
        {
        }
    }
}

