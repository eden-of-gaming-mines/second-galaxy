﻿namespace BlackJack.ProjectX.Runtime.SolarSystem.Weapon
{
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class WeaponSlotLODGroup
    {
        protected BulletTargetProviderWarpper m_traceTarget;
        protected List<LogicWeaponSlot> m_logicWeaponSlotList;
        protected GameObject m_chargeEffectAsset;
        protected GameObject m_fireEffectAsset;
        protected GameObject m_bulletEffectAsset;
        protected GameObject m_onHitEffectAsset;
        protected int m_waveCount;
        protected float m_weaponEffectScale;
        protected Transform m_weaponOwnerRoot;
        protected int m_usedSlotCount;
        protected int m_unitCount;
        protected float m_fireEffectDurationTime;
        protected const float MinFireChargeEffectDurationTime = 1.5f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Initial;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_SetTraceTarget;
        private static DelegateBridge __Hotfix_GetTraceTarget;
        private static DelegateBridge __Hotfix_IsTheSameTraceTarget;
        private static DelegateBridge __Hotfix_LoseTraceTarget;
        private static DelegateBridge __Hotfix_TraceTarget;
        private static DelegateBridge __Hotfix_GetLaunchLocationGameObject;
        private static DelegateBridge __Hotfix_GetFirePointFromOneSide;
        private static DelegateBridge __Hotfix_GetCurrLODLevel;
        private static DelegateBridge __Hotfix_CalcFireEffectDurationTime;
        private static DelegateBridge __Hotfix_GetDurationTimeFromEffectObject;
        private static DelegateBridge __Hotfix_SetTransformParent;
        private static DelegateBridge __Hotfix_CalcSlotIndexInfoByUnitIndex;
        private static DelegateBridge __Hotfix_CreateLogicSlot;
        private static DelegateBridge __Hotfix_get_LogicWeaponSlotList;
        private static DelegateBridge __Hotfix_get_ChargingEffectAsset;
        private static DelegateBridge __Hotfix_set_ChargingEffectAsset;
        private static DelegateBridge __Hotfix_get_FireEffectAsset;
        private static DelegateBridge __Hotfix_set_FireEffectAsset;
        private static DelegateBridge __Hotfix_get_BulletEffectAsset;
        private static DelegateBridge __Hotfix_set_BulletEffectAsset;
        private static DelegateBridge __Hotfix_get_OnHitEffectAsset;
        private static DelegateBridge __Hotfix_set_OnHitEffectAsset;
        private static DelegateBridge __Hotfix_get_WaveCount;
        private static DelegateBridge __Hotfix_set_WaveCount;
        private static DelegateBridge __Hotfix_get_WeaponEffectScale;
        private static DelegateBridge __Hotfix_set_WeaponEffectScale;
        private static DelegateBridge __Hotfix_get_FireEffectDurationTime;
        private static DelegateBridge __Hotfix_set_FireEffectDurationTime;

        [MethodImpl(0x8000)]
        protected void CalcFireEffectDurationTime()
        {
        }

        [MethodImpl(0x8000)]
        public void CalcSlotIndexInfoByUnitIndex(int uintIndex, out int waveIndex, out int logicSlotIndex, out int firePointIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateLogicSlot(WeaponSlotGroupConstructInfo creatorInfo, IWeaponOwnerProvider ownerProvider, List<GameObject> weaponGoList = null)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int GetCurrLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetDurationTimeFromEffectObject(GameObject effectGo)
        {
        }

        [MethodImpl(0x8000)]
        public virtual Transform GetFirePointFromOneSide(Transform shipRoot, DroneLaunchSide side, int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public virtual Transform GetLaunchLocationGameObject(int unitIndex)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper GetTraceTarget()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Initial(WeaponSlotGroupConstructInfo creatorInfo, IWeaponOwnerProvider ownerProvider, List<GameObject> weaponGoList = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTheSameTraceTarget(IBulletTargetProvider newTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void LoseTraceTarget(IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTraceTarget(BulletTargetProviderWarpper traceTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetTransformParent(GameObject parent, GameObject child, float scale)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void TraceTarget()
        {
        }

        public List<LogicWeaponSlot> LogicWeaponSlotList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public GameObject ChargingEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject FireEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject BulletEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public GameObject OnHitEffectAsset
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int WaveCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float WeaponEffectScale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float FireEffectDurationTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

