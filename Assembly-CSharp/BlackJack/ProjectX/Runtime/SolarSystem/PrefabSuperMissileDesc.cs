﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public class PrefabSuperMissileDesc : PrefabMissileDesc
    {
        [Header("中间点距离目标和开火连线最小距离(Unity单位)")]
        public float MiddlePointMinDistance;
        [Header("中间点距离目标和开火连线距离修正")]
        public float MiddlePointDistanceMulit;
        [Header("中间点切分目标与开火点连线的比例(0-1)"), Range(0f, 1f)]
        public float MiddlePointSeparateToTarget;
        [Header("命中点到目标的距离")]
        public float HitPosDistanceToTarget;
        [Header("预命中点到目标距离")]
        public float PreHitPosDistanceToTarget;
        [Header("飞往中间点的角速度")]
        public float FlyToMiddlePosAlignSpeed;
        [Header("飞往预命中点的角速度")]
        public float FlyToPreHitPosAlignSpeed;
        [Header("飞往命中点的角速度")]
        public float FlyToHitPosAlignSpeed;
        [Header("飞行节点碰撞球半径")]
        public float ColliderSphereRadius;
    }
}

