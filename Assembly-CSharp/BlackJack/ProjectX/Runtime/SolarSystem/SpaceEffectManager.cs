﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime.SolarSystem.Weapon;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceEffectManager
    {
        protected static SpaceEffectManager m_instance;
        protected Transform m_cacheBulletRoot;
        protected Transform m_cacheSlotEffectRoot;
        protected Transform m_cacheShipEffectRoot;
        protected Transform m_cacheSpaceGlobalEffectRoot;
        protected Dictionary<string, CacheBulletObject> m_cacheBulletDict;
        protected List<BulletObjectController> m_bulletObjectList;
        protected List<string> m_bulletRemoveList;
        protected Dictionary<GameObject, CacheSlotEffectObject> m_cacheSlotEffectDict;
        protected List<GameObject> m_cacheSlotEffectRemoveList;
        protected Queue<BulletTargetProviderWarpper> m_cacheBulletTargets;
        protected Dictionary<string, CacheShipEffectObject> m_cacheShipEffectDict;
        protected List<string> m_cacheShipEffectRemoveList;
        protected Dictionary<string, CacheSpaceGlobalEffectObject> m_cacheSpaceGlobalEffectDict;
        protected List<string> m_cacheSpaceGlobalEffectRemoveList;
        private Dictionary<string, ConfigDataImportantEffectInfo> m_importantEffectInfoDict;
        private Dictionary<int, ImportantEffectGroupInfo> m_importantEffectGroupCountDict;
        private int m_activeBulletCount;
        private int m_activeLaserBulletCount;
        private int m_activeNormalBulletCount;
        private int m_activeSlotEffectCount;
        private int m_activeShipEffectCount;
        private int m_activeGlobalEffectCount;
        public bool m_isInDisplayTest;
        public int m_displayTestImportantEffectCountLimit;
        protected const float EffectCacheLifeTime = 30000f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_SetCacheBulletRoot;
        private static DelegateBridge __Hotfix_SetCacheSlotEffectRoot;
        private static DelegateBridge __Hotfix_SetCacheShipEffectRoot;
        private static DelegateBridge __Hotfix_SetCacheSpaceGlobalEffectRoot;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_AddBullet;
        private static DelegateBridge __Hotfix_AllocBullet;
        private static DelegateBridge __Hotfix_ClearAllCacheBullet;
        private static DelegateBridge __Hotfix_DestoryBullet;
        private static DelegateBridge __Hotfix_FreeBullet;
        private static DelegateBridge __Hotfix_TickBullet;
        private static DelegateBridge __Hotfix_AllocSlotEffectObject;
        private static DelegateBridge __Hotfix_CreateSlotEffect;
        private static DelegateBridge __Hotfix_FreeSlotEffectObject;
        private static DelegateBridge __Hotfix_ClearSlotEffectObject;
        private static DelegateBridge __Hotfix_TickSlotEffect;
        private static DelegateBridge __Hotfix_AllocBulletTarget_0;
        private static DelegateBridge __Hotfix_AllocBulletTarget_1;
        private static DelegateBridge __Hotfix_FreeBulletTarget;
        private static DelegateBridge __Hotfix_ClearBulletTarget;
        private static DelegateBridge __Hotfix_AllocShipEffectObject;
        private static DelegateBridge __Hotfix_FreeShipEffectObject;
        private static DelegateBridge __Hotfix_ClearShipEffectObject;
        private static DelegateBridge __Hotfix_TickShipEffect;
        private static DelegateBridge __Hotfix_AllocSpaceGlobalEffect;
        private static DelegateBridge __Hotfix_FreeSpaceGlobalEffectObject;
        private static DelegateBridge __Hotfix_ClearSpaceGlobalEffectObject;
        private static DelegateBridge __Hotfix_TickSpaceGlobalEffect;
        private static DelegateBridge __Hotfix_AddActiveCount;
        private static DelegateBridge __Hotfix_ReduceActiveCount;
        private static DelegateBridge __Hotfix_RemoveImportantEffect;
        private static DelegateBridge __Hotfix_CheckImportantEffectCanCreate;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_get_ActiveBulletCount;
        private static DelegateBridge __Hotfix_set_ActiveBulletCount;
        private static DelegateBridge __Hotfix_get_ActiveLaserBulletCount;
        private static DelegateBridge __Hotfix_set_ActiveLaserBulletCount;
        private static DelegateBridge __Hotfix_get_ActiveNormalBulletCount;
        private static DelegateBridge __Hotfix_set_ActiveNormalBulletCount;
        private static DelegateBridge __Hotfix_get_ActiveSlotEffectCount;
        private static DelegateBridge __Hotfix_set_ActiveSlotEffectCount;
        private static DelegateBridge __Hotfix_get_ActiveShipEffectCount;
        private static DelegateBridge __Hotfix_set_ActiveShipEffectCount;
        private static DelegateBridge __Hotfix_get_ActiveGlobalEffectCount;
        private static DelegateBridge __Hotfix_set_ActiveGlobalEffectCount;
        private static DelegateBridge __Hotfix_get_ActiveSpaceEffectCount;

        [MethodImpl(0x8000)]
        private void AddActiveCount(ref int activeCount)
        {
        }

        [MethodImpl(0x8000)]
        public void AddBullet(BulletObjectController bulletCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public BulletObjectController AllocBullet(GameObject bulletAsset, GameObject onHitAsset, WeaponCategory weaponCategory)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper AllocBulletTarget(IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper AllocBulletTarget(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public ShipEffectObjectBase AllocShipEffectObject(string effectName)
        {
        }

        [MethodImpl(0x8000)]
        public SlotEffectObject AllocSlotEffectObject(GameObject effectAsset)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceGlobalEffectController AllocSpaceGlobalEffect(string effectName)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckImportantEffectCanCreate(string effectName)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearAllCacheBullet()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearBulletTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearShipEffectObject()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearSlotEffectObject()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearSpaceGlobalEffectObject()
        {
        }

        [MethodImpl(0x8000)]
        public T CreateSlotEffect<T>(BulletTargetProviderWarpper target, GameObject effectAsset, int currLODLevel, float effectDurationTime, float effectScale, Transform parentTransform, bool needRotateToTarget = false) where T: SlotEffectObject, new()
        {
        }

        [MethodImpl(0x8000)]
        public void DestoryBullet(BulletObjectController bulletCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void FreeBullet(BulletObjectController bulletCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void FreeBulletTarget(BulletTargetProviderWarpper targetWarpper)
        {
        }

        [MethodImpl(0x8000)]
        public void FreeShipEffectObject(string effectName, string effectAssetPath, ShipEffectObjectBase effectCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void FreeSlotEffectObject(SlotEffectObject slotEffect)
        {
        }

        [MethodImpl(0x8000)]
        public void FreeSpaceGlobalEffectObject(string effectName, string effectAssetPath, SpaceGlobalEffectController effectCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void ReduceActiveCount(ref int activeCount)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveImportantEffect(string effectAssetName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCacheBulletRoot(Transform root)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCacheShipEffectRoot(Transform root)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCacheSlotEffectRoot(Transform root)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCacheSpaceGlobalEffectRoot(Transform root)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickBullet()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickShipEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickSlotEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickSpaceGlobalEffect()
        {
        }

        public static SpaceEffectManager Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int ActiveBulletCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public int ActiveLaserBulletCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public int ActiveNormalBulletCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public int ActiveSlotEffectCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public int ActiveShipEffectCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public int ActiveGlobalEffectCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public int ActiveSpaceEffectCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public class CacheBulletObject
        {
            public DateTime m_modifyTime;
            public Queue<BulletObjectController> m_cachedBulletQueue = new Queue<BulletObjectController>();
            private static DelegateBridge _c__Hotfix_ctor;

            public CacheBulletObject()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class CacheShipEffectObject
        {
            public DateTime m_modifyTime;
            public Queue<ShipEffectObjectBase> m_cacheShipEffectObjectQueue = new Queue<ShipEffectObjectBase>();
            private static DelegateBridge _c__Hotfix_ctor;

            public CacheShipEffectObject()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class CacheSlotEffectObject
        {
            public DateTime m_modifyTime;
            public Queue<SlotEffectObject> m_cachedSlotEffectQueue = new Queue<SlotEffectObject>();
            private static DelegateBridge _c__Hotfix_ctor;

            public CacheSlotEffectObject()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class CacheSpaceGlobalEffectObject
        {
            public DateTime m_modifyTime;
            public Queue<SpaceGlobalEffectController> m_cacheSpaceGlobalEffectQueue = new Queue<SpaceGlobalEffectController>();
            private static DelegateBridge _c__Hotfix_ctor;

            public CacheSpaceGlobalEffectObject()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class ImportantEffectGroupInfo
        {
            public int m_currEffectCount;
            public ConfigDataDisplayObjectGroupInfo m_config;
            public DateTime m_coolDownTime;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_GetCountLimit;

            [MethodImpl(0x8000)]
            public int GetCountLimit(int level)
            {
            }
        }
    }
}

