﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShipEffectLODController : PrefabControllerBase
    {
        protected int m_LODLevel;
        protected PrefabShipDesc m_prefabShipDesc;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipBodyRoot;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_GetPrefabShipDesc;
        private static DelegateBridge __Hotfix_get_LODLevel;
        private static DelegateBridge __Hotfix_set_LODLevel;

        [MethodImpl(0x8000)]
        protected PrefabShipDesc GetPrefabShipDesc()
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isVisible)
        {
        }

        public int LODLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

