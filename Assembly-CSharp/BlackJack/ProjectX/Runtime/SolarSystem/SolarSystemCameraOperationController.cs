﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Threading;
    using UnityEngine;

    public class SolarSystemCameraOperationController : PrefabControllerBase
    {
        [AutoBind("./CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        [AutoBind("CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraTransformController m_cameraTransformCtrl;
        [AutoBind("CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public SolarSystemCameraInputController m_cameraInputCtrl;
        protected SolarSystemCameraState m_currCameraState;
        protected CameraStateController m_currCameraStateCtrl;
        protected Dictionary<SolarSystemCameraState, CameraStateController> m_cameraStateCtrlDict;
        public float m_jumpingShakeAmbitude;
        private static string m_strShieldBroken;
        private static string m_strBooster;
        protected CameraLoopShakeBase m_jumpCameraLoopShake;
        protected CameraLoopShakeBase m_boosterCameraLoopShake;
        private float m_ang;
        private float m_tarAng;
        private float m_angSpeed;
        private float m_jumpAmbitude;
        public float m_nearerCamera;
        public float m_fartherCamera;
        private static bool m_isIn;
        private static readonly int m_sampleCount;
        private static readonly int m_intervalCount;
        public static readonly float[] m_multiplyPoints;
        public static readonly float[] m_enterCameraMoveRate;
        public static readonly float[] m_exitCameraMoveRate;
        private static bool m_clockDir;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraFollowTargetForward;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnChangeToState;
        private static DelegateBridge __Hotfix_SetCameraLayerMaskStateByGraphicQualityLevel;
        private static DelegateBridge __Hotfix_EnableCameraAutoRotate;
        private static DelegateBridge __Hotfix_SetCameraWatchSelf;
        private static DelegateBridge __Hotfix_SetCameraWatchSelfImp;
        private static DelegateBridge __Hotfix_SetCameraWatchTarget;
        private static DelegateBridge __Hotfix_SetCameraTraceDirAndMove;
        private static DelegateBridge __Hotfix_SetTwoPointAccMove;
        private static DelegateBridge __Hotfix_SetCameraLowerLeftWatchToTarget;
        private static DelegateBridge __Hotfix_SetCameraWatchTargetImp;
        private static DelegateBridge __Hotfix_SetTraceDirAndMoveImp;
        private static DelegateBridge __Hotfix_SetTwoPointAccMoveImp;
        private static DelegateBridge __Hotfix_SetLowerLeftWatchToTargetImpl;
        private static DelegateBridge __Hotfix_IsCameraFollowTarget;
        private static DelegateBridge __Hotfix_SetCameraMoveImp;
        private static DelegateBridge __Hotfix_SetCameraTrackObj;
        private static DelegateBridge __Hotfix_SetCameraTracePosition;
        private static DelegateBridge __Hotfix_ClearCameraTrackObj;
        private static DelegateBridge __Hotfix_IsTheTraceObject;
        private static DelegateBridge __Hotfix_SetCameraToJumpView;
        private static DelegateBridge __Hotfix_SetCameraToJumpViewImp;
        private static DelegateBridge __Hotfix_SetCameraToTacticalView;
        private static DelegateBridge __Hotfix_SetCameraWatchPosition;
        private static DelegateBridge __Hotfix_SetCameraContrlBySceneAnimation;
        private static DelegateBridge __Hotfix_UpdateCameraRotate;
        private static DelegateBridge __Hotfix_GetCameraStateCtrlByState;
        private static DelegateBridge __Hotfix_SetCurrCameraState;
        private static DelegateBridge __Hotfix_GetCameraDistanceWithScreenSize;
        private static DelegateBridge __Hotfix_GetPixelsPerMeter;
        private static DelegateBridge __Hotfix_GetShipDefaultCameraOffsetMulti;
        private static DelegateBridge __Hotfix_get_CurrCameraState;
        private static DelegateBridge __Hotfix_set_CurrCameraState;
        private static DelegateBridge __Hotfix_get_CurrCameraStateCtrl;
        private static DelegateBridge __Hotfix_set_CurrCameraStateCtrl;
        private static DelegateBridge __Hotfix_StartOnHitCameraShake;
        private static DelegateBridge __Hotfix_StartOnExploreCameraShake;
        private static DelegateBridge __Hotfix_StartShieldBreakCameraShake;
        private static DelegateBridge __Hotfix_OnShipJumpCameraShake;
        private static DelegateBridge __Hotfix_StartShipBoosterCameraShake;
        private static DelegateBridge __Hotfix_StopShipBoosterCameraShake;
        private static DelegateBridge __Hotfix_StartSuperGearCameraShake;
        private static DelegateBridge __Hotfix_FloatLerp;
        private static DelegateBridge __Hotfix_AddDisturb;
        private static DelegateBridge __Hotfix_AddJumpOffset;
        private static DelegateBridge __Hotfix_JumpOffset;
        private static DelegateBridge __Hotfix_OnJumpEnterEnd;
        private static DelegateBridge __Hotfix_OnJumpExitEnd;
        private static DelegateBridge __Hotfix_DirectionOnLocalCoodinateSystem;
        private static DelegateBridge __Hotfix_RanAng;
        private static DelegateBridge __Hotfix_GetAbsOffsetByPoint;
        private static DelegateBridge __Hotfix_GeneratePoints;

        [MethodImpl(0x8000)]
        public void AddDisturb(CameraShakeSourceType disturbType, float disturbCent = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void AddJumpOffset(float duration, bool isIn)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearCameraTrackObj()
        {
        }

        [MethodImpl(0x8000)]
        private Vector3 DirectionOnLocalCoodinateSystem(Vector3 direction, float dummyY, float dummyX)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableCameraAutoRotate(bool enable, float cycleTime)
        {
        }

        [MethodImpl(0x8000)]
        private static float FloatLerp(float leftValue, float rightValue, float leftScale, float rightScale, float scale)
        {
        }

        [MethodImpl(0x8000)]
        private static void GeneratePoints()
        {
        }

        [MethodImpl(0x8000)]
        private static float GetAbsOffsetByPoint(float pointValue)
        {
        }

        [MethodImpl(0x8000)]
        private float GetCameraDistanceWithScreenSize(float size)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCameraFollowTargetForward()
        {
        }

        [MethodImpl(0x8000)]
        protected CameraStateController GetCameraStateCtrlByState(SolarSystemCameraState state)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        private float GetPixelsPerMeter()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetShipDefaultCameraOffsetMulti(ClientSpaceShip ship)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCameraFollowTarget(SpaceShipController targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTheTraceObject(ClientSpaceObject target)
        {
        }

        [MethodImpl(0x8000)]
        private Vector3 JumpOffset(Vector3 direction, float scaledTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChangeToState(SolarSystemCameraState nextState)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpEnterEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpExitEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipJumpCameraShake(bool isIn, float duaration = 6f)
        {
        }

        [MethodImpl(0x8000)]
        private void RanAng()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraContrlBySceneAnimation(ISceneAnimationCameraCtrlProvider animationProvider)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraLayerMaskStateByGraphicQualityLevel(GraphicQualityLevel level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraLowerLeftWatchToTarget(SpaceShipController targetShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraMoveImp(SpaceShipController destTarget, Action onEnd = null, float? customOffsetZ = new float?())
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraToJumpView(SpaceShipController srcObj, float timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraToJumpViewImp(SpaceShipController srcObj, float timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraToTacticalView(float viewSize, float timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraTraceDirAndMove(Vector3 destPosition, Vector3 lookDir, float lengthTime, bool isImmediate, bool useDefaultRotation, bool keepZOffset, Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraTracePosition(Vector3D position)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraTrackObj(ClientSpaceObject target, bool forceTrace)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraWatchPosition(SolarSystemCameraState lastState, CameraStateController lastStateCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraWatchSelf(SpaceShipController selfShip, bool isImmediate, bool useDefaultRotation, EnterSolarSystemType enterType, bool keepZOffset = false, Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraWatchSelfImp(SpaceShipController selfShip, bool useDefaultRotation, EnterSolarSystemType enterType, bool keepZOffset)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraWatchTarget(SpaceShipController targetShip, bool isImmediate, bool useDefaultRotation, bool keepZOffset = false, Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraWatchTargetImp(SpaceShipController targetShip, bool useDefaultRotation, bool keepZOffset)
        {
        }

        [MethodImpl(0x8000)]
        protected CameraStateController SetCurrCameraState(SolarSystemCameraState state)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetLowerLeftWatchToTargetImpl(SpaceShipController targetShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetTraceDirAndMoveImp(Vector3 destPosition, Vector3 lookDir, float lengthTime, bool isImmediate, bool useDefaultRotation, bool keepZOffset, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTwoPointAccMove(Vector3 beginPosition, Vector3 destPosition, float timeLength, Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetTwoPointAccMoveImp(Vector3 beginPosition, Vector3 endPosition, float timeLength, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StartOnExploreCameraShake()
        {
        }

        [MethodImpl(0x8000)]
        public void StartOnHitCameraShake(float damagePercent)
        {
        }

        [MethodImpl(0x8000)]
        public void StartShieldBreakCameraShake()
        {
        }

        [MethodImpl(0x8000)]
        public void StartShipBoosterCameraShake()
        {
        }

        [MethodImpl(0x8000)]
        public void StartSuperGearCameraShake(float duaration, float ambitude)
        {
        }

        [MethodImpl(0x8000)]
        public void StopShipBoosterCameraShake()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCameraRotate(float xDegree, float yDegree, float time)
        {
        }

        public SolarSystemCameraState CurrCameraState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public CameraStateController CurrCameraStateCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetCameraToJumpView>c__AnonStorey2
        {
            internal SpaceShipController srcObj;
            internal float timeSpan;
            internal SolarSystemCameraOperationController $this;

            internal void <>m__0()
            {
                this.$this.SetCameraToJumpViewImp(this.srcObj, this.timeSpan);
            }
        }

        [CompilerGenerated]
        private sealed class <SetCameraWatchSelf>c__AnonStorey0
        {
            internal SpaceShipController selfShip;
            internal bool useDefaultRotation;
            internal EnterSolarSystemType enterType;
            internal bool keepZOffset;
            internal Action onEnd;
            internal SolarSystemCameraOperationController $this;

            internal void <>m__0()
            {
                this.$this.SetCameraWatchSelfImp(this.selfShip, this.useDefaultRotation, this.enterType, this.keepZOffset);
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SetCameraWatchTarget>c__AnonStorey1
        {
            internal SpaceShipController targetShip;
            internal bool keepZOffset;
            internal Action onEnd;
            internal SolarSystemCameraOperationController $this;

            internal void <>m__0()
            {
                this.$this.SetCameraWatchTargetImp(this.targetShip, false, this.keepZOffset);
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        public class CameraControllBySceneAniamtion : SolarSystemCameraOperationController.CameraStateController
        {
            private float m_cameraBaseFov;
            protected ISceneAnimationCameraCtrlProvider m_cameraCtrlProvider;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_EnterState;
            private static DelegateBridge __Hotfix_LeaveState;
            private static DelegateBridge __Hotfix_GetCameraWatchLocation;
            private static DelegateBridge __Hotfix_SetAnimationProvider;

            public CameraControllBySceneAniamtion()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.EnterState();
                    if (base.m_cameraTransformCtrl == null)
                    {
                        Debug.LogError("CameraControllBySceneAniamtion.EnterState error: m_cameraTransformCtrl == null");
                    }
                    if (this.m_cameraCtrlProvider == null)
                    {
                        Debug.LogError("CameraControllBySceneAniamtion.EnterState error: m_cameraCtrlProvider == null");
                    }
                    base.m_cameraTransformCtrl.ResetAllCameraAniamtion();
                    base.m_cameraTransformCtrl.EnableCameraAnimator(false);
                    base.m_cameraTransformCtrl.SetCameraShakeLocked(true);
                    base.m_cameraTransformCtrl.SetCameraMinMaxYOffset(0f, 0f, 0f);
                    float cameraZOffset = this.m_cameraCtrlProvider.GetCameraZOffset();
                    base.m_cameraTransformCtrl.SetCameraMinMaxZOffset(cameraZOffset, cameraZOffset);
                    base.m_cameraTransformCtrl.SetCameraOffset(cameraZOffset, 0f);
                    base.m_cameraTransformCtrl.SetCameraPlatformPosition(this.GetCameraWatchLocation());
                    base.m_cameraTransformCtrl.UpdateCameraDockData(Vector3.zero);
                    this.m_cameraBaseFov = base.m_cameraTransformCtrl.GetCameraFov();
                    if (this.m_cameraCtrlProvider.IsEnableCinemachineBrain())
                    {
                        base.m_cameraTransformCtrl.EnableCinemachineBrain(true);
                    }
                }
            }

            public override Vector3 GetCameraWatchLocation()
            {
                DelegateBridge bridge = __Hotfix_GetCameraWatchLocation;
                return ((bridge == null) ? this.m_cameraCtrlProvider.GetCameraWatchLocation() : bridge.__Gen_Delegate_Imp3701(this));
            }

            public override void LeaveState()
            {
                DelegateBridge bridge = __Hotfix_LeaveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.m_cameraTransformCtrl.SetCameraShakeLocked(false);
                    base.m_cameraTransformCtrl.SetCameraFov(this.m_cameraBaseFov);
                    base.m_cameraTransformCtrl.EnableCameraAnimator(true);
                    if (this.m_cameraCtrlProvider.IsEnableCinemachineBrain())
                    {
                        base.m_cameraTransformCtrl.EnableCinemachineBrain(false);
                    }
                    base.LeaveState();
                }
            }

            public void SetAnimationProvider(ISceneAnimationCameraCtrlProvider provider)
            {
                DelegateBridge bridge = __Hotfix_SetAnimationProvider;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, provider);
                }
                else
                {
                    this.m_cameraCtrlProvider = provider;
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    if (this.m_cameraCtrlProvider.IsRelocationCamera())
                    {
                        base.m_cameraTransformCtrl.SetCameraPlatformPosition(this.m_cameraCtrlProvider.GetCameraWatchLocation());
                        base.m_cameraTransformCtrl.SetCameraRotation(this.m_cameraCtrlProvider.GetCameraRotation());
                        base.m_cameraTransformCtrl.SetCameraFov(this.m_cameraCtrlProvider.GetCameraFov());
                    }
                }
            }
        }

        public class CameraFollowObject : SolarSystemCameraOperationController.CameraStateController, SolarSystemCameraOperationController.IFollowObjectCamera
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private SpaceShipController <FollowSpaceTarget>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <UseDefaultRotation>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private bool <KeepZOffset>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private float <DefaultOffsetMulti>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private float? <CustomMaxZForYOffset>k__BackingField;
            protected bool m_enableAutoRotate;
            protected float m_autoRotateSpeed;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private EnterSolarSystemType <EnterType>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_EnterState;
            private static DelegateBridge __Hotfix_LeaveState;
            private static DelegateBridge __Hotfix_GetCameraWatchLocation;
            private static DelegateBridge __Hotfix_EnableCameraAutoRotate;
            private static DelegateBridge __Hotfix_GetCameraFollowTargetForward;
            private static DelegateBridge __Hotfix_SetFollowObject;
            private static DelegateBridge __Hotfix_GetFollowObject;
            private static DelegateBridge __Hotfix_get_FollowSpaceTarget;
            private static DelegateBridge __Hotfix_set_FollowSpaceTarget;
            private static DelegateBridge __Hotfix_get_UseDefaultRotation;
            private static DelegateBridge __Hotfix_set_UseDefaultRotation;
            private static DelegateBridge __Hotfix_get_KeepZOffset;
            private static DelegateBridge __Hotfix_set_KeepZOffset;
            private static DelegateBridge __Hotfix_get_DefaultOffsetMulti;
            private static DelegateBridge __Hotfix_set_DefaultOffsetMulti;
            private static DelegateBridge __Hotfix_get_CustomMaxZForYOffset;
            private static DelegateBridge __Hotfix_set_CustomMaxZForYOffset;
            private static DelegateBridge __Hotfix_get_EnterType;
            private static DelegateBridge __Hotfix_set_EnterType;

            public CameraFollowObject()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void EnableCameraAutoRotate(bool enable, float cycleTime)
            {
                DelegateBridge bridge = __Hotfix_EnableCameraAutoRotate;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3750(this, enable, cycleTime);
                }
                else
                {
                    this.m_enableAutoRotate = enable;
                    if (this.m_enableAutoRotate && (cycleTime > 0f))
                    {
                        this.m_autoRotateSpeed = 360f / cycleTime;
                    }
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    float local1;
                    base.EnterState();
                    float shipRadius = this.FollowSpaceTarget.GetShipRadius();
                    if (this.UseDefaultRotation)
                    {
                        float rotateAngleForAxisX = 0f;
                        float num3 = 0f;
                        EnterSolarSystemType enterType = this.EnterType;
                        if ((enterType == EnterSolarSystemType.FromLogin) || (enterType == EnterSolarSystemType.FromSpace))
                        {
                            rotateAngleForAxisX = 5f;
                            num3 = 0f;
                        }
                        else if (enterType == EnterSolarSystemType.FromStation)
                        {
                            rotateAngleForAxisX = -15f;
                            num3 = 172f;
                        }
                        base.m_cameraTransformCtrl.SetCameraRotation(rotateAngleForAxisX, this.FollowSpaceTarget.transform.rotation.eulerAngles.y + num3);
                    }
                    float minZOffset = (shipRadius * 1f) + 0.3f;
                    base.m_cameraTransformCtrl.SetCameraMinMaxZOffset(minZOffset, shipRadius * 35f);
                    float minYOffset = shipRadius * 0f;
                    float maxYOffset = shipRadius * 1.5f;
                    if (this.CustomMaxZForYOffset == null)
                    {
                        local1 = shipRadius * 10f;
                    }
                    else
                    {
                        local1 = this.CustomMaxZForYOffset.Value;
                    }
                    base.m_cameraTransformCtrl.SetCameraMinMaxYOffset(minYOffset, maxYOffset, local1);
                    if (!this.KeepZOffset)
                    {
                        base.m_cameraTransformCtrl.SetCameraOffset((float) (shipRadius * this.DefaultOffsetMulti), 0f);
                    }
                    base.m_cameraTransformCtrl.SetCameraPlatformPosition(this.GetCameraWatchLocation());
                    base.m_cameraTransformCtrl.UpdateCameraDockData(this.GetCameraFollowTargetForward());
                }
            }

            public override Vector3 GetCameraFollowTargetForward()
            {
                DelegateBridge bridge = __Hotfix_GetCameraFollowTargetForward;
                return ((bridge == null) ? this.FollowSpaceTarget.transform.forward : bridge.__Gen_Delegate_Imp3701(this));
            }

            public override Vector3 GetCameraWatchLocation()
            {
                DelegateBridge bridge = __Hotfix_GetCameraWatchLocation;
                return ((bridge == null) ? this.FollowSpaceTarget.transform.position : bridge.__Gen_Delegate_Imp3701(this));
            }

            public SpaceShipController GetFollowObject()
            {
                DelegateBridge bridge = __Hotfix_GetFollowObject;
                return ((bridge == null) ? this.FollowSpaceTarget : bridge.__Gen_Delegate_Imp3765(this));
            }

            public override void LeaveState()
            {
                DelegateBridge bridge = __Hotfix_LeaveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.LeaveState();
                    this.FollowSpaceTarget = null;
                    this.UseDefaultRotation = false;
                    this.DefaultOffsetMulti = 1f;
                }
            }

            public void SetFollowObject(SpaceShipController followObject)
            {
                DelegateBridge bridge = __Hotfix_SetFollowObject;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, followObject);
                }
                else
                {
                    this.FollowSpaceTarget = followObject;
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    base.m_cameraTransformCtrl.SetCameraPlatformPosition(this.GetCameraWatchLocation());
                    if (this.m_enableAutoRotate)
                    {
                        base.m_cameraTransformCtrl.UpdateCameraRotation(0f, Time.deltaTime * this.m_autoRotateSpeed, 0f);
                    }
                }
            }

            protected SpaceShipController FollowSpaceTarget
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_FollowSpaceTarget;
                    return ((bridge == null) ? this.<FollowSpaceTarget>k__BackingField : bridge.__Gen_Delegate_Imp3765(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_FollowSpaceTarget;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<FollowSpaceTarget>k__BackingField = value;
                    }
                }
            }

            public bool UseDefaultRotation
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_UseDefaultRotation;
                    return ((bridge == null) ? this.<UseDefaultRotation>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_UseDefaultRotation;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<UseDefaultRotation>k__BackingField = value;
                    }
                }
            }

            public bool KeepZOffset
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_KeepZOffset;
                    return ((bridge == null) ? this.<KeepZOffset>k__BackingField : bridge.__Gen_Delegate_Imp9(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_KeepZOffset;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp18(this, value);
                    }
                    else
                    {
                        this.<KeepZOffset>k__BackingField = value;
                    }
                }
            }

            public float DefaultOffsetMulti
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_DefaultOffsetMulti;
                    return ((bridge == null) ? this.<DefaultOffsetMulti>k__BackingField : bridge.__Gen_Delegate_Imp3(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_DefaultOffsetMulti;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp29(this, value);
                    }
                    else
                    {
                        this.<DefaultOffsetMulti>k__BackingField = value;
                    }
                }
            }

            public float? CustomMaxZForYOffset
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_CustomMaxZForYOffset;
                    return ((bridge == null) ? this.<CustomMaxZForYOffset>k__BackingField : bridge.__Gen_Delegate_Imp3766(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_CustomMaxZForYOffset;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp3767(this, value);
                    }
                    else
                    {
                        this.<CustomMaxZForYOffset>k__BackingField = value;
                    }
                }
            }

            public EnterSolarSystemType EnterType
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_EnterType;
                    return ((bridge == null) ? this.<EnterType>k__BackingField : bridge.__Gen_Delegate_Imp3768(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_EnterType;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp3769(this, value);
                    }
                    else
                    {
                        this.<EnterType>k__BackingField = value;
                    }
                }
            }
        }

        public class CameraFollowPosition : SolarSystemCameraOperationController.CameraStateController
        {
            protected Vector3 m_followPosition;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_GetCameraWatchLocation;

            public CameraFollowPosition()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override Vector3 GetCameraWatchLocation()
            {
                DelegateBridge bridge = __Hotfix_GetCameraWatchLocation;
                return ((bridge == null) ? this.m_followPosition : bridge.__Gen_Delegate_Imp3701(this));
            }
        }

        public class CameraFollowPositionNoTrace : SolarSystemCameraOperationController.CameraFollowPosition
        {
            private static DelegateBridge _c__Hotfix_ctor;

            public CameraFollowPositionNoTrace()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class CameraFollowTargetAndTraceTarget : SolarSystemCameraOperationController.CameraFollowObject, SolarSystemCameraOperationController.ITraceObjectCamera
        {
            protected bool m_isTrace;
            protected ClientSpaceObject m_traceObject;
            protected Vector3D m_tracePosition;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_LeaveState;
            private static DelegateBridge __Hotfix_SetTraceObject;
            private static DelegateBridge __Hotfix_SetTracePosition;
            private static DelegateBridge __Hotfix_ClearCameraTrackObj;
            private static DelegateBridge __Hotfix_IsTheTraceObject;

            public CameraFollowTargetAndTraceTarget()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void ClearCameraTrackObj()
            {
                DelegateBridge bridge = __Hotfix_ClearCameraTrackObj;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_isTrace = false;
                    this.m_traceObject = null;
                    this.m_tracePosition = Vector3D.zero;
                    base.m_cameraTransformCtrl.StopCameraRotationSlerp();
                }
            }

            public bool IsTheTraceObject(ClientSpaceObject obj)
            {
                DelegateBridge bridge = __Hotfix_IsTheTraceObject;
                return ((bridge == null) ? (this.m_isTrace ? ReferenceEquals(obj, this.m_traceObject) : false) : bridge.__Gen_Delegate_Imp16(this, obj));
            }

            public override void LeaveState()
            {
                DelegateBridge bridge = __Hotfix_LeaveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.LeaveState();
                    this.ClearCameraTrackObj();
                }
            }

            public void SetTraceObject(ClientSpaceObject traceTarget)
            {
                DelegateBridge bridge = __Hotfix_SetTraceObject;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, traceTarget);
                }
                else
                {
                    this.m_isTrace = true;
                    this.m_traceObject = traceTarget;
                    base.m_cameraTransformCtrl.StartCameraRotationSlerp(1.5f);
                }
            }

            public void SetTracePosition(Vector3D tracePosition)
            {
                DelegateBridge bridge = __Hotfix_SetTracePosition;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp350(this, tracePosition);
                }
                else
                {
                    this.m_isTrace = true;
                    this.m_traceObject = null;
                    this.m_tracePosition = tracePosition;
                    base.m_cameraTransformCtrl.StartCameraRotationSlerp(1.5f);
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    if (this.m_isTrace)
                    {
                        Vector3 zero = Vector3.zero;
                        if (this.m_traceObject != null)
                        {
                            Vector3D vectord = this.m_traceObject.SmoothLocation - (base.FollowSpaceTarget.ClientSpaceObject.SmoothLocation + new Vector3D(0.0, (double) (base.m_cameraTransformCtrl.GetCameraCurrYOffset() * 100f), 0.0));
                            zero = new Vector3((float) vectord.x, (float) vectord.y, (float) vectord.z);
                        }
                        else
                        {
                            Vector3D vectord2 = this.m_tracePosition - (base.FollowSpaceTarget.ClientSpaceObject.SmoothLocation + new Vector3D(0.0, (double) (base.m_cameraTransformCtrl.GetCameraCurrYOffset() * 100f), 0.0));
                            zero = new Vector3((float) vectord2.x, (float) vectord2.y, (float) vectord2.z);
                        }
                        base.m_cameraTransformCtrl.SetCameraLookAtDirection(zero, 0f);
                    }
                }
            }
        }

        public class CameraFollowTargetNoTrace : SolarSystemCameraOperationController.CameraFollowObject
        {
            private static DelegateBridge _c__Hotfix_ctor;

            public CameraFollowTargetNoTrace()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class CameraStateController
        {
            protected SolarSystemCameraOperationController m_cameraOperationController;
            protected SolarSystemCameraTransformController m_cameraTransformCtrl;
            protected SolarSystemCameraInputController m_cameraInputCtrl;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private Action<SolarSystemCameraOperationController.SolarSystemCameraState> EventOnChangeToState;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_SetCameraTransformCtrl;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_EnterState;
            private static DelegateBridge __Hotfix_LeaveState;
            private static DelegateBridge __Hotfix_GetCameraWatchLocation;
            private static DelegateBridge __Hotfix_GetCameraFollowTargetForward;
            private static DelegateBridge __Hotfix_ChangeToState;
            private static DelegateBridge __Hotfix_add_EventOnChangeToState;
            private static DelegateBridge __Hotfix_remove_EventOnChangeToState;

            public event Action<SolarSystemCameraOperationController.SolarSystemCameraState> EventOnChangeToState
            {
                add
                {
                    DelegateBridge bridge = __Hotfix_add_EventOnChangeToState;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        Action<SolarSystemCameraOperationController.SolarSystemCameraState> eventOnChangeToState = this.EventOnChangeToState;
                        while (true)
                        {
                            Action<SolarSystemCameraOperationController.SolarSystemCameraState> a = eventOnChangeToState;
                            eventOnChangeToState = Interlocked.CompareExchange<Action<SolarSystemCameraOperationController.SolarSystemCameraState>>(ref this.EventOnChangeToState, (Action<SolarSystemCameraOperationController.SolarSystemCameraState>) System.Delegate.Combine(a, value), eventOnChangeToState);
                            if (ReferenceEquals(eventOnChangeToState, a))
                            {
                                return;
                            }
                        }
                    }
                }
                remove
                {
                    DelegateBridge bridge = __Hotfix_remove_EventOnChangeToState;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        Action<SolarSystemCameraOperationController.SolarSystemCameraState> eventOnChangeToState = this.EventOnChangeToState;
                        while (true)
                        {
                            Action<SolarSystemCameraOperationController.SolarSystemCameraState> source = eventOnChangeToState;
                            eventOnChangeToState = Interlocked.CompareExchange<Action<SolarSystemCameraOperationController.SolarSystemCameraState>>(ref this.EventOnChangeToState, (Action<SolarSystemCameraOperationController.SolarSystemCameraState>) System.Delegate.Remove(source, value), eventOnChangeToState);
                            if (ReferenceEquals(eventOnChangeToState, source))
                            {
                                return;
                            }
                        }
                    }
                }
            }

            public CameraStateController()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected void ChangeToState(SolarSystemCameraOperationController.SolarSystemCameraState state)
            {
                DelegateBridge bridge = __Hotfix_ChangeToState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3749(this, state);
                }
                else if (this.EventOnChangeToState != null)
                {
                    this.EventOnChangeToState(state);
                }
            }

            public virtual void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public virtual Vector3 GetCameraFollowTargetForward()
            {
                DelegateBridge bridge = __Hotfix_GetCameraFollowTargetForward;
                return ((bridge == null) ? Vector3.zero : bridge.__Gen_Delegate_Imp3701(this));
            }

            public virtual Vector3 GetCameraWatchLocation()
            {
                DelegateBridge bridge = __Hotfix_GetCameraWatchLocation;
                return ((bridge == null) ? Vector3.zero : bridge.__Gen_Delegate_Imp3701(this));
            }

            public virtual void LeaveState()
            {
                DelegateBridge bridge = __Hotfix_LeaveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void SetCameraTransformCtrl(SolarSystemCameraOperationController operationCtrl, SolarSystemCameraTransformController ctrl, SolarSystemCameraInputController inputCtrl)
            {
                DelegateBridge bridge = __Hotfix_SetCameraTransformCtrl;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp37(this, operationCtrl, ctrl, inputCtrl);
                }
                else
                {
                    this.m_cameraOperationController = operationCtrl;
                    this.m_cameraTransformCtrl = ctrl;
                    this.m_cameraInputCtrl = inputCtrl;
                }
            }

            public virtual void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public class CameraStateCtrl_Jumping : SolarSystemCameraOperationController.CameraFollowTargetAndTraceTarget
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private float <JumpingAdjustTimeSpan>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_EnterState;
            private static DelegateBridge __Hotfix_get_JumpingAdjustTimeSpan;
            private static DelegateBridge __Hotfix_set_JumpingAdjustTimeSpan;

            public CameraStateCtrl_Jumping()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    float maxZOffset = base.FollowSpaceTarget.GetShipRadius() * 15f;
                    if ((base.m_cameraTransformCtrl.GetCameraLocalOffsetZ() > maxZOffset) || base.m_cameraInputCtrl.IsCameraManualOperationCountDown())
                    {
                        float deltaOffset = base.FollowSpaceTarget.GetShipRadius() * 5f;
                        base.m_cameraTransformCtrl.SetCameraMinMaxZOffset(base.m_cameraTransformCtrl.GetCameraMinZOffset(), maxZOffset);
                        base.m_cameraTransformCtrl.SetCameraOffset(deltaOffset, this.JumpingAdjustTimeSpan);
                        Vector3 forward = base.FollowSpaceTarget.transform.forward;
                        base.m_cameraTransformCtrl.SetCameraLookAtDirection(forward, this.JumpingAdjustTimeSpan);
                    }
                }
            }

            public float JumpingAdjustTimeSpan
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_JumpingAdjustTimeSpan;
                    return ((bridge == null) ? this.<JumpingAdjustTimeSpan>k__BackingField : bridge.__Gen_Delegate_Imp3(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_JumpingAdjustTimeSpan;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp29(this, value);
                    }
                    else
                    {
                        this.<JumpingAdjustTimeSpan>k__BackingField = value;
                    }
                }
            }
        }

        public class CameraStateCtrl_LowerLeftWatchToTarget : SolarSystemCameraOperationController.CameraStateController
        {
            protected Vector3 m_srcCameraPos;
            protected Vector3 m_currCameraPlatformPos;
            private float m_lengthTime = 0.0001f;
            protected SpaceShipController m_targetShipCtrl;
            protected float m_targetShipRadius;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_EnterState;
            private static DelegateBridge __Hotfix_LeaveState;
            private static DelegateBridge __Hotfix_GetCameraWatchLocation;
            private static DelegateBridge __Hotfix_SetTargetShipCtrl;
            private static DelegateBridge __Hotfix_SetMoveStartPost;

            public CameraStateCtrl_LowerLeftWatchToTarget()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.EnterState();
                    base.m_cameraTransformCtrl.StoreCameraDummyRot();
                    this.m_currCameraPlatformPos = this.m_srcCameraPos;
                    Vector3D vectord = (Vector3D) -((Dest.Math.Quaternion.Euler(this.m_targetShipCtrl.ClientSpaceObject.Rotation) * Dest.Math.Quaternion.Euler(-15.0, 45.0, 0.0)) * Vector3D.forward);
                    base.m_cameraTransformCtrl.SetCameraLookAtDirection(new Vector3((float) vectord.x, (float) vectord.y, (float) vectord.z), 0f);
                    this.m_targetShipRadius = this.m_targetShipCtrl.GetShipRadius();
                    Vector3 deltaOffset = new Vector3(-this.m_targetShipRadius * 0.5f, -this.m_targetShipRadius * 0.5f, -this.m_targetShipRadius * 4f);
                    base.m_cameraTransformCtrl.SetCameraMinMaxYOffset(0f, 0f, base.m_cameraTransformCtrl.GetCameraMaxZForYOffset());
                    base.m_cameraTransformCtrl.SetCameraOffset((float) 0f, 0f);
                    base.m_cameraTransformCtrl.SetCameraOffset(deltaOffset, this.m_lengthTime);
                }
            }

            public override Vector3 GetCameraWatchLocation()
            {
                DelegateBridge bridge = __Hotfix_GetCameraWatchLocation;
                return ((bridge == null) ? this.m_currCameraPlatformPos : bridge.__Gen_Delegate_Imp3701(this));
            }

            public override void LeaveState()
            {
                DelegateBridge bridge = __Hotfix_LeaveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.LeaveState();
                    base.m_cameraTransformCtrl.RestoreCameraDummyRot();
                    base.m_cameraTransformCtrl.ResetCameraDummyLocalOffset();
                    base.m_cameraTransformCtrl.StopCameraOffsetSlerp();
                }
            }

            public void SetMoveStartPost(Vector3 pos)
            {
                DelegateBridge bridge = __Hotfix_SetMoveStartPost;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3699(this, pos);
                }
                else
                {
                    this.m_srcCameraPos = pos;
                }
            }

            public void SetTargetShipCtrl(SpaceShipController ctrl)
            {
                DelegateBridge bridge = __Hotfix_SetTargetShipCtrl;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, ctrl);
                }
                else
                {
                    this.m_targetShipCtrl = ctrl;
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    this.m_currCameraPlatformPos = this.m_targetShipCtrl.transform.position;
                    base.m_cameraTransformCtrl.SetCameraPlatformPosition(this.GetCameraWatchLocation());
                }
            }
        }

        public class CameraStateCtrl_NormalMoving : SolarSystemCameraOperationController.CameraStateController
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private float <DestTargetDefaultOffsetMulti>k__BackingField;
            protected float m_srcCameraOffsetZ;
            protected float m_srcCameraMinOffsetY;
            protected float m_srcCameraMaxOffsetY;
            protected float m_srcCameraMaxZForYOffset;
            protected float m_destTargetBaseDistance;
            protected float? m_customTargetOffsetY;
            protected Vector3 m_currCameraPos;
            protected Vector3 m_srcPos;
            protected SpaceShipController m_srcTarget;
            protected SpaceShipController m_destTarget;
            protected Action m_onEnd;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_EnterState;
            private static DelegateBridge __Hotfix_LeaveState;
            private static DelegateBridge __Hotfix_GetCameraWatchLocation;
            private static DelegateBridge __Hotfix_SetMoveStartPost;
            private static DelegateBridge __Hotfix_SetMoveDestTarget;
            private static DelegateBridge __Hotfix_SetCustomTargetOffsetY;
            private static DelegateBridge __Hotfix_Clear;
            private static DelegateBridge __Hotfix_get_DestTargetDefaultOffsetMulti;
            private static DelegateBridge __Hotfix_set_DestTargetDefaultOffsetMulti;

            public CameraStateCtrl_NormalMoving()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_srcPos = Vector3.zero;
                    this.m_destTarget = null;
                    this.m_onEnd = null;
                    base.m_cameraTransformCtrl.StopCameraMoveAnimation();
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.EnterState();
                    this.m_currCameraPos = this.m_srcPos;
                    this.m_srcCameraOffsetZ = base.m_cameraTransformCtrl.GetCameraLocalOffsetZ();
                    this.m_srcCameraMinOffsetY = base.m_cameraTransformCtrl.GetCameraMinOffsetY();
                    this.m_srcCameraMaxOffsetY = base.m_cameraTransformCtrl.GetCameraMaxOffsetY();
                    this.m_srcCameraMaxZForYOffset = base.m_cameraTransformCtrl.GetCameraMaxZForYOffset();
                    base.m_cameraTransformCtrl.StartCameraMoveAnimation();
                    this.m_destTargetBaseDistance = this.m_destTarget.GetShipRadius();
                }
            }

            public override Vector3 GetCameraWatchLocation()
            {
                DelegateBridge bridge = __Hotfix_GetCameraWatchLocation;
                return ((bridge == null) ? this.m_currCameraPos : bridge.__Gen_Delegate_Imp3701(this));
            }

            public override void LeaveState()
            {
                DelegateBridge bridge = __Hotfix_LeaveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.LeaveState();
                    this.Clear();
                }
            }

            public void SetCustomTargetOffsetY(float? offsetY)
            {
                DelegateBridge bridge = __Hotfix_SetCustomTargetOffsetY;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3767(this, offsetY);
                }
                else
                {
                    this.m_customTargetOffsetY = offsetY;
                }
            }

            public void SetMoveDestTarget(SpaceShipController destTarget, Action onEnd)
            {
                DelegateBridge bridge = __Hotfix_SetMoveDestTarget;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp26(this, destTarget, onEnd);
                }
                else
                {
                    this.m_destTarget = destTarget;
                    this.m_onEnd = onEnd;
                }
            }

            public void SetMoveStartPost(Vector3 pos)
            {
                DelegateBridge bridge = __Hotfix_SetMoveStartPost;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3699(this, pos);
                }
                else
                {
                    this.m_srcPos = pos;
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    if (base.m_cameraTransformCtrl.IsCameraMoveAnimationFinished())
                    {
                        if (this.m_onEnd != null)
                        {
                            this.m_onEnd();
                        }
                        this.Clear();
                    }
                    else
                    {
                        float t = Mathf.Clamp01(base.m_cameraTransformCtrl.GetCameraMoveAnimationFactor());
                        float b = (this.m_customTargetOffsetY != null) ? this.m_customTargetOffsetY.Value : (this.m_destTargetBaseDistance * 0f);
                        float num3 = (this.m_customTargetOffsetY != null) ? this.m_customTargetOffsetY.Value : (this.m_destTargetBaseDistance * 1.5f);
                        float minYOffset = Mathf.Lerp(this.m_srcCameraMinOffsetY, b, t);
                        base.m_cameraTransformCtrl.SetCameraMinMaxYOffset(minYOffset, Mathf.Lerp(this.m_srcCameraMaxOffsetY, num3, t), Mathf.Lerp(this.m_srcCameraMaxZForYOffset, this.m_destTargetBaseDistance * 10f, t));
                        float deltaOffset = Mathf.Lerp(this.m_srcCameraOffsetZ, this.m_destTargetBaseDistance * this.DestTargetDefaultOffsetMulti, t);
                        base.m_cameraTransformCtrl.SetCameraMinMaxZOffset((this.m_destTargetBaseDistance * 1f) + 0.3f, this.m_destTargetBaseDistance * 35f);
                        base.m_cameraTransformCtrl.SetCameraOffset(deltaOffset, 0f);
                        Vector3 position = this.m_destTarget.transform.position;
                        this.m_currCameraPos = Vector3.Lerp(this.m_srcPos, position, t);
                        base.m_cameraTransformCtrl.SetCameraPlatformPosition(this.GetCameraWatchLocation());
                    }
                }
            }

            public float DestTargetDefaultOffsetMulti
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_DestTargetDefaultOffsetMulti;
                    return ((bridge == null) ? this.<DestTargetDefaultOffsetMulti>k__BackingField : bridge.__Gen_Delegate_Imp3(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_DestTargetDefaultOffsetMulti;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp29(this, value);
                    }
                    else
                    {
                        this.<DestTargetDefaultOffsetMulti>k__BackingField = value;
                    }
                }
            }
        }

        public class CameraStateCtrl_NormalWatchAndTrace : SolarSystemCameraOperationController.CameraStateController
        {
            private Vector3 m_traceLookDir = Vector3.zero;
            private UnityEngine.Quaternion m_startRotation = UnityEngine.Quaternion.identity;
            private Vector3 m_followPos = Vector3.zero;
            private Vector3 m_startPos = Vector3.zero;
            private bool m_isImmediate;
            private float m_tickTime;
            private float m_lengthTime = 1f;
            protected Action m_onEnd;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_IsFinished;
            private static DelegateBridge __Hotfix_GetCameraWatchLocation;
            private static DelegateBridge __Hotfix_SetFollowPosition;
            private static DelegateBridge __Hotfix_SetImmediate;
            private static DelegateBridge __Hotfix_Clear;
            private static DelegateBridge __Hotfix_SetTraceDirAndMove;
            private static DelegateBridge __Hotfix_IsTheTraceObject;
            private static DelegateBridge __Hotfix_EnterState;

            public CameraStateCtrl_NormalWatchAndTrace()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_onEnd = null;
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.EnterState();
                    this.m_tickTime = 0f;
                }
            }

            public override Vector3 GetCameraWatchLocation()
            {
                DelegateBridge bridge = __Hotfix_GetCameraWatchLocation;
                return ((bridge == null) ? this.m_traceLookDir : bridge.__Gen_Delegate_Imp3701(this));
            }

            protected bool IsFinished()
            {
                DelegateBridge bridge = __Hotfix_IsFinished;
                return ((bridge == null) ? (this.m_isImmediate || (this.m_tickTime >= this.m_lengthTime)) : bridge.__Gen_Delegate_Imp9(this));
            }

            public bool IsTheTraceObject(ClientSpaceObject obj)
            {
                DelegateBridge bridge = __Hotfix_IsTheTraceObject;
                return ((bridge != null) && bridge.__Gen_Delegate_Imp16(this, obj));
            }

            public void SetFollowPosition(Vector3 pos)
            {
                DelegateBridge bridge = __Hotfix_SetFollowPosition;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3699(this, pos);
                }
                else
                {
                    this.m_followPos = pos;
                }
            }

            public void SetImmediate(bool isImmediate)
            {
                DelegateBridge bridge = __Hotfix_SetImmediate;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp18(this, isImmediate);
                }
                else
                {
                    this.m_isImmediate = isImmediate;
                }
            }

            public void SetTraceDirAndMove(Vector3 followPosition, Vector3 lookDir, float lengthTime, Action onEnd)
            {
                DelegateBridge bridge = __Hotfix_SetTraceDirAndMove;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3755(this, followPosition, lookDir, lengthTime, onEnd);
                }
                else
                {
                    this.SetFollowPosition(followPosition);
                    this.m_traceLookDir = lookDir;
                    this.m_startPos = base.m_cameraTransformCtrl.GetCamera().transform.position;
                    this.m_startRotation = base.m_cameraTransformCtrl.GetCamera().transform.rotation;
                    this.m_lengthTime = lengthTime;
                    this.m_onEnd = onEnd;
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    Vector3 followPos = this.m_followPos;
                    float t = 1f;
                    if (!this.m_isImmediate)
                    {
                        this.m_tickTime += Time.deltaTime;
                        t = Mathf.Clamp01(this.m_tickTime / this.m_lengthTime);
                    }
                    Vector3 newPos = Vector3.Lerp(this.m_startPos, followPos, t);
                    UnityEngine.Quaternion b = new UnityEngine.Quaternion();
                    b.SetLookRotation(this.m_traceLookDir);
                    UnityEngine.Quaternion rotation = UnityEngine.Quaternion.Lerp(this.m_startRotation, b, t);
                    base.m_cameraTransformCtrl.SetCameraPlatformPosition(newPos);
                    base.m_cameraTransformCtrl.SetCameraRotation(rotation);
                    if (this.IsFinished())
                    {
                        if (this.m_onEnd != null)
                        {
                            this.m_onEnd();
                        }
                        this.Clear();
                    }
                }
            }
        }

        public class CameraStateCtrl_NormalWatchOther : SolarSystemCameraOperationController.CameraFollowTargetNoTrace
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;

            public CameraStateCtrl_NormalWatchOther()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    if (base.FollowSpaceTarget.IsNeedFrozenCamera())
                    {
                        base.ChangeToState(SolarSystemCameraOperationController.SolarSystemCameraState.NormalWatchPosition);
                    }
                    string paramName = string.Empty;
                    bool isEnable = false;
                    if (base.FollowSpaceTarget.IsNeedStartCameraAnimator(out paramName, out isEnable))
                    {
                        base.m_cameraTransformCtrl.PlayCameraAnimator(paramName, isEnable);
                    }
                }
            }
        }

        public class CameraStateCtrl_NormalWatchPosition : SolarSystemCameraOperationController.CameraFollowPositionNoTrace
        {
            public SolarSystemCameraOperationController.SolarSystemCameraState LastSolarSystemCameraState;
            public SpaceShipController LastFollowTarget;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_EnterState;

            public CameraStateCtrl_NormalWatchPosition()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.EnterState();
                    base.m_followPosition = this.LastFollowTarget.transform.position;
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    base.m_cameraTransformCtrl.SetCameraPlatformPosition(this.GetCameraWatchLocation());
                    if ((this.LastFollowTarget != null) && !this.LastFollowTarget.IsNeedFrozenCamera())
                    {
                        base.ChangeToState(this.LastSolarSystemCameraState);
                    }
                }
            }
        }

        public class CameraStateCtrl_NormalWatchSelf : SolarSystemCameraOperationController.CameraFollowTargetAndTraceTarget, SolarSystemCameraOperationController.IRotationCamrea
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_UpdateCameraRotate;

            public CameraStateCtrl_NormalWatchSelf()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    if (base.FollowSpaceTarget.IsNeedFrozenCamera())
                    {
                        base.ChangeToState(SolarSystemCameraOperationController.SolarSystemCameraState.NormalWatchPosition);
                    }
                    string paramName = string.Empty;
                    bool isEnable = false;
                    if (base.FollowSpaceTarget.IsNeedStartCameraAnimator(out paramName, out isEnable))
                    {
                        base.m_cameraTransformCtrl.PlayCameraAnimator(paramName, isEnable);
                    }
                    string shakeType = string.Empty;
                    bool flag2 = false;
                    if (base.FollowSpaceTarget.GetShipEffectCtrl().IsNeedStartCameraShake(out shakeType, out flag2) && (shakeType == "BoosterShake"))
                    {
                        if (flag2)
                        {
                            base.m_cameraOperationController.StartShipBoosterCameraShake();
                        }
                        else
                        {
                            base.m_cameraOperationController.StopShipBoosterCameraShake();
                        }
                    }
                }
            }

            public void UpdateCameraRotate(float xDegree, float yDegree, float time)
            {
                DelegateBridge bridge = __Hotfix_UpdateCameraRotate;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3758(this, xDegree, yDegree, time);
                }
                else
                {
                    base.m_cameraTransformCtrl.UpdateCameraRotation(xDegree, yDegree, time);
                }
            }
        }

        public class CameraStateCtrl_TacticalView : SolarSystemCameraOperationController.CameraFollowTargetNoTrace
        {
            protected float m_viewSize;
            protected float m_timeSpan;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_EnterState;
            private static DelegateBridge __Hotfix_LeaveState;
            private static DelegateBridge __Hotfix_SetCameraViewSize;
            private static DelegateBridge __Hotfix_GetCameraDistanceWithScreenSize;
            private static DelegateBridge __Hotfix_GetPixelsPerMeter;

            public CameraStateCtrl_TacticalView()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    float cameraDistanceWithScreenSize = this.GetCameraDistanceWithScreenSize(this.m_viewSize);
                    base.m_cameraTransformCtrl.SetCameraMinMaxZOffset(base.m_cameraTransformCtrl.GetCameraMinZOffset(), cameraDistanceWithScreenSize);
                    base.m_cameraTransformCtrl.SetCameraOffset(cameraDistanceWithScreenSize, this.m_timeSpan);
                    Vector3 cameraFoward = base.m_cameraTransformCtrl.GetCameraFoward();
                    Vector3 vector2 = new Vector3(cameraFoward.x, 0f, cameraFoward.z);
                    Vector3 dir = (Vector3) (UnityEngine.Quaternion.AngleAxis(-45f, Vector3.Cross(cameraFoward, Vector3.up)) * vector2);
                    base.m_cameraTransformCtrl.SetCameraLookAtDirection(dir, this.m_timeSpan);
                }
            }

            protected float GetCameraDistanceWithScreenSize(float size)
            {
                DelegateBridge bridge = __Hotfix_GetCameraDistanceWithScreenSize;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp21(this, size);
                }
                float pixelsPerMeter = this.GetPixelsPerMeter();
                return ((size * pixelsPerMeter) / (Screen.height * 1f));
            }

            protected float GetPixelsPerMeter()
            {
                DelegateBridge bridge = __Hotfix_GetPixelsPerMeter;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp3(this);
                }
                return (1f / (Vector3.Distance(base.m_cameraTransformCtrl.GetCamera().ScreenToWorldPoint(new Vector3((Screen.width - 100f) / 2f, (Screen.height - 100f) / 2f, 1f)), base.m_cameraTransformCtrl.GetCamera().ScreenToWorldPoint(new Vector3((Screen.width + 100f) / 2f, (Screen.height + 100f) / 2f, 1f))) / 141.4f));
            }

            public override void LeaveState()
            {
                DelegateBridge bridge = __Hotfix_LeaveState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.LeaveState();
                    this.m_viewSize = 0f;
                    this.m_timeSpan = 0f;
                }
            }

            public void SetCameraViewSize(float viewSize, float timeSpan)
            {
                DelegateBridge bridge = __Hotfix_SetCameraViewSize;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1495(this, viewSize, timeSpan);
                }
                else
                {
                    this.m_viewSize = viewSize;
                    this.m_timeSpan = timeSpan;
                }
            }
        }

        public class CameraStateCtrl_TwoPointAccMove : SolarSystemCameraOperationController.CameraStateController
        {
            private Vector3 m_beginPoint = Vector3.zero;
            private Vector3 m_endPoint = Vector3.zero;
            private float m_tickTime;
            private float m_lengthTime;
            private float m_distance;
            private float m_acc;
            private Vector3 m_dir;
            protected Action m_onEnd;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Tick;
            private static DelegateBridge __Hotfix_IsFinished;
            private static DelegateBridge __Hotfix_Clear;
            private static DelegateBridge __Hotfix_SetPointsAndTime;
            private static DelegateBridge __Hotfix_EnterState;

            public CameraStateCtrl_TwoPointAccMove()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            protected void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_onEnd = null;
                }
            }

            public override void EnterState()
            {
                DelegateBridge bridge = __Hotfix_EnterState;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.EnterState();
                    this.m_tickTime = 0f;
                }
            }

            protected bool IsFinished()
            {
                DelegateBridge bridge = __Hotfix_IsFinished;
                return ((bridge == null) ? (this.m_tickTime >= this.m_lengthTime) : bridge.__Gen_Delegate_Imp9(this));
            }

            public void SetPointsAndTime(Vector3 beginPoint, Vector3 endPoint, float timeLength, Action onEnd)
            {
                DelegateBridge bridge = __Hotfix_SetPointsAndTime;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp3755(this, beginPoint, endPoint, timeLength, onEnd);
                }
                else
                {
                    this.m_lengthTime = timeLength;
                    this.m_beginPoint = beginPoint;
                    this.m_endPoint = endPoint;
                    this.m_onEnd = onEnd;
                    this.m_distance = Vector3.Distance(beginPoint, endPoint);
                    this.m_acc = (this.m_distance / (this.m_lengthTime * this.m_lengthTime)) * 2f;
                    this.m_dir = this.m_endPoint - this.m_beginPoint;
                    this.m_dir.Normalize();
                }
            }

            public override void Tick()
            {
                DelegateBridge bridge = __Hotfix_Tick;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Tick();
                    this.m_tickTime += Time.deltaTime;
                    Vector3 newPos = this.m_beginPoint + ((((this.m_acc * this.m_tickTime) * this.m_tickTime) * this.m_dir) * 0.5f);
                    base.m_cameraTransformCtrl.SetCameraPlatformPosition(newPos);
                    if (this.IsFinished())
                    {
                        if (this.m_onEnd != null)
                        {
                            this.m_onEnd();
                        }
                        this.Clear();
                    }
                }
            }
        }

        public interface IFollowObjectCamera
        {
            SpaceShipController GetFollowObject();
            void SetFollowObject(SpaceShipController followObject);
        }

        public interface IRotationCamrea
        {
            void UpdateCameraRotate(float xDegree, float yDegree, float time);
        }

        public interface ITraceObjectCamera
        {
            void ClearCameraTrackObj();
            bool IsTheTraceObject(ClientSpaceObject obj);
            void SetTraceObject(ClientSpaceObject traceTarget);
            void SetTracePosition(Vector3D tracePosition);
        }

        public enum SolarSystemCameraState
        {
            Invalid,
            NormalWatchSelf,
            NormalWatchOther,
            NormalWatchPosition,
            NormalMoving,
            NormalWatchAndTrace,
            TwoPointAccMove,
            Jumping,
            SceneAnimation,
            TacticalView,
            LowerLeftWatchToTarget
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct SolarSystemCameraStateComparer : IEqualityComparer<SolarSystemCameraOperationController.SolarSystemCameraState>
        {
            public bool Equals(SolarSystemCameraOperationController.SolarSystemCameraState x, SolarSystemCameraOperationController.SolarSystemCameraState y) => 
                (x == y);

            public int GetHashCode(SolarSystemCameraOperationController.SolarSystemCameraState obj) => 
                ((int) obj);
        }
    }
}

