﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SelfSpaceShipLOD0Controller : SelfSpaceShipLODController
    {
        protected Mesh m_toughGridMesh;
        protected Vector3[] m_toughGridVectors;
        protected Vector3[] m_toughGridNormals;
        private static DelegateBridge __Hotfix_GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_GetPrefabShipDesc;

        [MethodImpl(0x8000)]
        public override void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        public override bool GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration)
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        protected override PrefabShipDesc GetPrefabShipDesc()
        {
        }
    }
}

