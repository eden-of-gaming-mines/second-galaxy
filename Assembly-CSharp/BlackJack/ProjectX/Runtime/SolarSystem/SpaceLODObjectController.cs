﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceLODObjectController : SpaceObjectControllerBase
    {
        protected ControllerLODDispatchBase m_LODDispatch;
        protected int m_fixedMinLODLevel;
        protected int m_fixedMaxLODLevel;
        private static DelegateBridge __Hotfix_CreateInternal;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_SetLODLevel;
        private static DelegateBridge __Hotfix_ComputeLODLevel;
        private static DelegateBridge __Hotfix_SetFixedMinMaxLODLevel;
        private static DelegateBridge __Hotfix_GetFixedMinLODLevel;
        private static DelegateBridge __Hotfix_GetFixedMaxLODLevel;
        private static DelegateBridge __Hotfix_CreateLODDispatch;
        private static DelegateBridge __Hotfix_get_LODDispatch;

        [MethodImpl(0x8000)]
        public void ComputeLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void CreateInternal()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual ControllerLODDispatchBase CreateLODDispatch()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetFixedMaxLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetFixedMinLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        public override void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFixedMinMaxLODLevel(int minLODLevel, int maxLODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLODLevel(int LODLevel)
        {
        }

        protected ControllerLODDispatchBase LODDispatch
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

