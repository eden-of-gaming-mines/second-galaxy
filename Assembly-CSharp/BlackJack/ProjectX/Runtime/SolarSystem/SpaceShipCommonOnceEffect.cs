﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceShipCommonOnceEffect : SpaceShipEffectBase
    {
        protected const float CommonOnceEffectLifeTime = 5f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_PlayCommonOnceEffect_0;
        private static DelegateBridge __Hotfix_PlayCommonOnceEffect_1;

        [MethodImpl(0x8000)]
        public SpaceShipCommonOnceEffect(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCommonOnceEffect(Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target, float lifeTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCommonOnceEffect(GameObject asset, string assetPath, ISpaceTargetProvider target, float lifeTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(Dictionary<string, GameObject> effectAssets, ISpaceTargetProvider target)
        {
        }
    }
}

