﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SelfSpaceShipLODController : PrefabControllerBase
    {
        protected PrefabShipDesc m_prefabShipDesc;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipBodyRoot;
        protected float m_shipRadius;
        private static DelegateBridge __Hotfix_GetPrefabShipDesc;
        private static DelegateBridge __Hotfix_GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_SetShipRadius;
        private static DelegateBridge __Hotfix_GetShipRadius;

        [MethodImpl(0x8000)]
        public virtual void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration)
        {
        }

        [MethodImpl(0x8000)]
        public virtual Vector3 GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual PrefabShipDesc GetPrefabShipDesc()
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipRadius()
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipRadius(float shipRadius)
        {
        }
    }
}

