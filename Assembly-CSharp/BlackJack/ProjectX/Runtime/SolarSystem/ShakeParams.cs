﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct ShakeParams
    {
        public float m_disturb;
        public float m_duration;
        public bool m_includeZrotate;
        [MethodImpl(0x8000)]
        public ShakeParams(float disturb, float duration, bool includeZrotate)
        {
        }
    }
}

