﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SyncEventCacheItem
    {
        private QueueSyncEventType m_eventType;
        public bool m_isImportantEffect;
        public uint m_spaceObjId1;
        public uint m_spaceObjId2;
        public ulong m_ulongParam1;
        public ulong m_ulongParam2;
        public ulong m_ulongParam3;
        public long m_longParam1;
        public object m_objParam1;
        public object m_objParam2;
        public float m_floatParam1;
        public DateTime m_timeoutTime;
        private bool m_isFree;
        public static Stack<SyncEventCacheItem> m_freeItemList = new Stack<SyncEventCacheItem>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ClearPool;
        private static DelegateBridge __Hotfix_GetItem;
        private static DelegateBridge __Hotfix_FreeItem;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_get_EventType;
        private static DelegateBridge __Hotfix_set_EventType;

        [MethodImpl(0x8000)]
        private SyncEventCacheItem()
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearPool()
        {
        }

        [MethodImpl(0x8000)]
        public static void FreeItem(SyncEventCacheItem item)
        {
        }

        [MethodImpl(0x8000)]
        public static SyncEventCacheItem GetItem()
        {
        }

        public QueueSyncEventType EventType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

