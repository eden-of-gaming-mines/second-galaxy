﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public interface IShipEngineEffectController
    {
        void CloseAllEngineEffect();
        void UpdateEngineEffect(double currSpeed, double currMaxSpeed, double defaultMaxSpeed, double jumpMaxSpeed, bool isJumping, bool isOnFight);
        void UpdateShipTrail(double currSpeed, bool isJumping);
    }
}

