﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceObjectLODController : PrefabControllerBase, ILODController
    {
        protected BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject m_clientSpaceObject;
        protected IDynamicAssetProvider m_dynamicAssetProvider;
        protected List<string> m_refrencedAssetList;
        protected Camera m_camera;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_AllocAsset;
        private static DelegateBridge __Hotfix_LeaveLODLevel;
        private static DelegateBridge __Hotfix_EnterLODLevel;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;

        [MethodImpl(0x8000)]
        protected T AllocAsset<T>(string name) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObject, IDynamicAssetProvider dynamicAssetProvider, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        public void EnterLODLevel(object param)
        {
        }

        [MethodImpl(0x8000)]
        public object LeaveLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Release()
        {
        }

        public BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

