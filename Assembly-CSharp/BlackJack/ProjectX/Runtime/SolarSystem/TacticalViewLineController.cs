﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using Vectrosity;

    public class TacticalViewLineController : PrefabControllerBase
    {
        private Camera m_camera;
        private int m_circleCount;
        private int m_baseSegmentCount;
        private float m_baseRadius;
        private VectorLine m_circleVectorLine;
        private VectorLine m_axisXVectorLine;
        private VectorLine m_axisZVectorLine;
        private GameObject m_numberAsset;
        private GameObject m_numberGoRoot;
        private Color32 LineColor;
        protected IDynamicAssetProvider m_dynamicAssetProvider;
        protected List<string> m_refrencedAssetList;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_SetLocation;
        private static DelegateBridge __Hotfix_ResetTracticalViewCamera;
        private static DelegateBridge __Hotfix_OnResourcesReady;
        private static DelegateBridge __Hotfix_ShowLine;
        private static DelegateBridge __Hotfix_WaitToShowLine;
        private static DelegateBridge __Hotfix_ShowLineImp;
        private static DelegateBridge __Hotfix_CreateTacticalViewCircleVectorLine;
        private static DelegateBridge __Hotfix_CreateTacticalViewAxisVectorLine;
        private static DelegateBridge __Hotfix_CreateRulerTitle;
        private static DelegateBridge __Hotfix_AllocAsset;

        [MethodImpl(0x8000)]
        protected T AllocAsset<T>(string name) where T: UnityEngine.Object
        {
        }

        [MethodImpl(0x8000)]
        public bool Create(int circleCount, int baseSegmentCount, float baseRadius, Camera camera, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateRulerTitle(Vector3 location, int distance)
        {
        }

        [MethodImpl(0x8000)]
        protected VectorLine CreateTacticalViewAxisVectorLine(Vector3 axis, int circleCount, int segmentCount, float baseRadius, IDynamicAssetProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        protected VectorLine CreateTacticalViewCircleVectorLine(Vector3 centerPos, int circleCount, int baseSegmentCount, float baseRadius, IDynamicAssetProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public void OnResourcesReady()
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetTracticalViewCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocation(Vector3 locaiton)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLine(bool isshow, float delayTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowLineImp(bool isshow)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator WaitToShowLine(bool isshow, float delayTime)
        {
        }

        [CompilerGenerated]
        private sealed class <WaitToShowLine>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal float delayTime;
            internal bool isshow;
            internal TacticalViewLineController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

