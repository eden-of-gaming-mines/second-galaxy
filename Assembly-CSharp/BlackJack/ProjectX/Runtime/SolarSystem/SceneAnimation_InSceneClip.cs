﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimation_InSceneClip : SceneAnimation_CutSceneBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_GetCameraFov;

        [MethodImpl(0x8000)]
        public SceneAnimation_InSceneClip(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetCameraFov()
        {
        }

        [MethodImpl(0x8000)]
        public override Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        public override void Play(Transform target, float targetSize)
        {
        }
    }
}

