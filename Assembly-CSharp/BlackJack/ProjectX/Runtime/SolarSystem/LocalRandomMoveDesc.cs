﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public class LocalRandomMoveDesc : MonoBehaviour
    {
        public float RandomMoveSpeed;
        public float RandomRange;
        public float RandomMoveRadius;
        public RandomMoveCoordinates RandomMoveCoordinate;
        public bool IsDeviation;
    }
}

