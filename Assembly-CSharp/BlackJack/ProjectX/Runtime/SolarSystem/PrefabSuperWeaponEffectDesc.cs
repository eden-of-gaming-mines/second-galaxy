﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using System;
    using UnityEngine;

    [AddComponentMenu("SolarSystem/PrefabSuperWeaponEffectDesc")]
    public class PrefabSuperWeaponEffectDesc : PrefabResourceContainerBase
    {
        [Header("开火特效")]
        public GameObject FireEffectAsset;
        [Header("子弹特效")]
        public GameObject BulletEffectAsset;
        [Header("命中特效")]
        public GameObject OnHitEffectAsset;
        [Header("充能特效")]
        public GameObject ChargingEffectAsset;
        [Header("开火特效2")]
        public GameObject FireEffect2Asset;
        [Header("子弹特效2")]
        public GameObject BulletEffect2Asset;
        [Header("命中特效2")]
        public GameObject OnHitEffec2tAsset;
        [Header("充能特效2")]
        public GameObject ChargingEffect2Asset;
        [Header("子弹穿透后的命中特效")]
        public GameObject OnHitEffectAssetAfterPenetration;
        public const string FireEffectAssetName = "FireEffectAsset";
        public const string BulletEffectAssetName = "BulletEffectAsset";
        public const string OnHitEffectAssetName = "OnHitEffectAsset";
        public const string ChargingEffectAssetName = "ChargingEffectAsset";
        public const string FireEffect2AssetName = "FireEffect2Asset";
        public const string BulletEffect2AssetName = "BulletEffect2Asset";
        public const string OnHitEffec2tAssetName = "OnHitEffec2tAsset";
        public const string ChargingEffect2AssetName = "ChargingEffect2Asset";
        public const string OnHitEffectAssetAfterPenetrationName = "OnHitEffectAssetAfterPenetration";
    }
}

