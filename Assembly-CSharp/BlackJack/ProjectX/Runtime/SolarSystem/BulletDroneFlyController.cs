﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletDroneFlyController : SpaceObjectTraceMoveController
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLaunchEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <DroneIndex>k__BackingField;
        protected bool m_isAcitve;
        protected TrailRenderer m_trailRender;
        protected float m_launchLeftTime;
        protected float m_velocityChangeRate;
        protected float m_predictVelocity;
        protected float m_velocityChangeLeftTime;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_UpdateVelocity;
        private static DelegateBridge __Hotfix_SetActive;
        private static DelegateBridge __Hotfix_StartLaunch;
        private static DelegateBridge __Hotfix_StartTraceTarget;
        private static DelegateBridge __Hotfix_add_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLaunchEnd;
        private static DelegateBridge __Hotfix_get_IsActive;
        private static DelegateBridge __Hotfix_get_DroneIndex;
        private static DelegateBridge __Hotfix_set_DroneIndex;

        public event Action EventOnLaunchEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void SetActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void StartLaunch(float launchTime, float velocity, AnimationCurve curve, Transform target)
        {
        }

        [MethodImpl(0x8000)]
        public void StartTraceTarget(Transform target, float velocity, float maxAlignSpeed)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateVelocity(float distance)
        {
        }

        public bool IsActive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int DroneIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

