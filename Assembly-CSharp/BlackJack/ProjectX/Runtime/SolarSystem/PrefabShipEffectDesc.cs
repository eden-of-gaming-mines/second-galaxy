﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using System;
    using UnityEngine;

    [AddComponentMenu("SolarSystem/PrefabShipEffectDesc")]
    public class PrefabShipEffectDesc : PrefabResourceContainerBase
    {
        [Header("特效是否追踪目标")]
        public bool IsFollowTarget;
        [Header("特效缩放类型")]
        public ShipEffectScaleType ScaleType;
        [Header("LOD0特效")]
        public GameObject LOD0EffectAsset;
        [Header("LOD1特效")]
        public GameObject LOD1EffectAsset;
        [Header("LOD2特效")]
        public GameObject LOD2EffectAsset;
        public const string LOD0EffectAssetName = "LOD0EffectAsset";
        public const string LOD1EffectAssetName = "LOD1EffectAsset";
        public const string LOD2EffectAssetName = "LOD2EffectAsset";

        public enum ShipEffectScaleType
        {
            NoScale,
            ScaleByEffectRoot,
            ScaleByShipScale,
            ScaleByCustomUniformScale
        }
    }
}

