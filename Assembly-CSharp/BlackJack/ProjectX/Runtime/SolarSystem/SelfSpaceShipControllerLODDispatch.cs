﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SelfSpaceShipControllerLODDispatch : SpaceShipControllerLODDispatch
    {
        protected SelfSpaceShipLODController m_currSelfSpaceShipLODCtrl;
        protected SelfSpaceShipLODController[] m_selfShipLODCtrl;
        protected ControllerDesc[] m_selfShipLODCtrlsDesc;
        private static DelegateBridge __Hotfix_CreateLODController;
        private static DelegateBridge __Hotfix_PostLODLevelChanged;
        private static DelegateBridge __Hotfix_CreateSelfShipExtraCtrl;
        private static DelegateBridge __Hotfix_get_CurrSelfSpaceShipLODCtrl;
        private static DelegateBridge __Hotfix_set_CurrSelfSpaceShipLODCtrl;

        [MethodImpl(0x8000)]
        public override ILODController CreateLODController(GameObject LODGameObject, int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected SelfSpaceShipLODController CreateSelfShipExtraCtrl(GameObject LODGameObject, int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostLODLevelChanged(int LODLevel)
        {
        }

        public SelfSpaceShipLODController CurrSelfSpaceShipLODCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

