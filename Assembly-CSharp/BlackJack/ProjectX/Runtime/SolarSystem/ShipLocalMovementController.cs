﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShipLocalMovementController : PrefabControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<MovementType> EventOnLocalMovementEnd;
        protected MovementType m_moveType;
        protected Vector3 m_worldLerpSrcLocation;
        protected Vector3 m_worldLerpTargetLocation;
        protected float m_moveLeftTime;
        protected float m_moveTime;
        [AutoBind("./ShipRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipRoot;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickLerpToZero;
        private static DelegateBridge __Hotfix_TickLerpToTarget;
        private static DelegateBridge __Hotfix_SetSrcLocalLocation;
        private static DelegateBridge __Hotfix_SetTargetLocalLocation;
        private static DelegateBridge __Hotfix_StartLocalMovement;
        private static DelegateBridge __Hotfix_OnMovementEnd;
        private static DelegateBridge __Hotfix_add_EventOnLocalMovementEnd;
        private static DelegateBridge __Hotfix_remove_EventOnLocalMovementEnd;

        public event Action<MovementType> EventOnLocalMovementEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void OnMovementEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSrcLocalLocation(Vector3 location)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetLocalLocation(Vector3 location)
        {
        }

        [MethodImpl(0x8000)]
        public void StartLocalMovement(MovementType moveType, float time)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickLerpToTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickLerpToZero()
        {
        }

        public enum MovementType
        {
            None,
            LerpToZero,
            LerpToTarget
        }
    }
}

