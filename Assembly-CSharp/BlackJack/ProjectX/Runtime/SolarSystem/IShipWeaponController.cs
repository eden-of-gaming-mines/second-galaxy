﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface IShipWeaponController
    {
        void AddChargePoint(uint processId, ushort unitIndex, Transform chargePoint);
        Transform GetAndRemoveChargePoint(uint processId, ushort unitIndex);
        BulletLaunchInfo GetBulletLaunchInfo(int groupIndex, int uintIndex, bool isCalcLaunchPoint = true);
        BulletLaunchInfo GetBulletLaunchInfoForNormalWeaponGroup(int unitIndex, bool isCalcLaunchPoint = true);
        DroneLaunchSide GetCurrFightDroneLaunchSide();
        DroneLaunchSide GetCurrSniperDroneLaunchSide();
        Transform GetFirePointFromOneSide(DroneLaunchSide side, int groupIndex, int unitIndex);
        BulletTargetProviderWarpper GetWeaponTraceTarget(int groupIndex);
        void LoseTraceTarget(IBulletTargetProvider target);
        void OnShipDeath();
        void Release();
        void SetNormalWeaponEffectDesc(PrefabWeaponEffectDesc weaponEffectDesc);
        void SetWeaponTraceTarget(int groupIndex, BulletTargetProviderWarpper traceTarget);
        Transform StartCharge(int groupIndex, int unitIndex, BulletTargetProviderWarpper bulletTarget, float chargeTime);
        void StartFire(int groupIndex, int unitIndex, BulletTargetProviderWarpper bulletTarget, float lastTime = -1f, Transform defaultLaunchPoint = null);
        Transform StartNormalWeaponCharge(int unitIndex, BulletTargetProviderWarpper bulletTarget, float chargeTime);
        void StartNormalWeaponFire(int unitIndex, BulletTargetProviderWarpper bulletTarget, float lastTime = -1f, Transform defaultLaunchPoint = null);
        void StopAllCharge();
        void StopAllFire();
    }
}

