﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [AddComponentMenu("SolarSystem/PrefabSpaceGlobalEffectDesc")]
    public class PrefabSpaceGlobalEffectDesc : PrefabResourceContainerBase
    {
        [Header("LOD0特效")]
        public GameObject LOD0EffectAsset;
        [Header("LOD1特效")]
        public GameObject LOD1EffectAsset;
        [Header("LOD2特效")]
        public GameObject LOD2EffectAsset;
        public const string LOD0EffectAssetName = "LOD0EffectAsset";
        public const string LOD1EffectAssetName = "LOD1EffectAsset";
        public const string LOD2EffectAssetName = "LOD2EffectAsset";
        private static DelegateBridge __Hotfix_GetEffectLODAsset;

        [MethodImpl(0x8000)]
        public GameObject GetEffectLODAsset(int LODLevel)
        {
        }
    }
}

