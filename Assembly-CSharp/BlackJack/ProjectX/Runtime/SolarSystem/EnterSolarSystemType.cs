﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public enum EnterSolarSystemType
    {
        FromLogin,
        FromSpace,
        FromStation
    }
}

