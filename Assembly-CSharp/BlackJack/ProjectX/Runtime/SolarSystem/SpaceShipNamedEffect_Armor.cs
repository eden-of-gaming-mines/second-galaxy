﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipNamedEffect_Armor : SpaceShipNamedEffect
    {
        protected const float OnHitCountAttenRate = 8f;
        protected const float MaxOnHitEffectStrength = 5f;
        protected const float IncreaseStrengthPerOnHit = 0.5f;
        protected float m_currOnHitStrength;
        protected TimeLerpExecutor m_armorRecoverLerpExecutor;
        protected TimeLerpExecutor m_armorResistUpLerpExecutor;
        protected TimeLerpExecutor m_armorResistDownLerpExecutor;
        protected const float LerpTime = 1f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_StartArmorOnHitEffect;
        private static DelegateBridge __Hotfix_StartArmorRecoverEffect;
        private static DelegateBridge __Hotfix_StopArmorRecoverEffect;
        private static DelegateBridge __Hotfix_StartArmorResistUpEffect;
        private static DelegateBridge __Hotfix_StartArmorResistDownEffect;
        private static DelegateBridge __Hotfix_UpdateOnHitEffect;
        private static DelegateBridge __Hotfix_get_IsEffectActive;
        private static DelegateBridge __Hotfix_set_IsEffectActive;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_Armor(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartArmorOnHitEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartArmorRecoverEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartArmorResistDownEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartArmorResistUpEffect()
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void StopArmorRecoverEffect()
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float delayTime, uint solarSystemTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateOnHitEffect()
        {
        }

        public override bool IsEffectActive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

