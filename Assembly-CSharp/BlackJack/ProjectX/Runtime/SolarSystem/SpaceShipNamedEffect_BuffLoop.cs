﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipNamedEffect_BuffLoop : SpaceShipNamedEffect
    {
        protected List<ShipLoopEffectObjectController>[] m_effectCtrlList;
        protected ShipNamedEffectType m_currEffectType;
        protected Action<int> m_onPostShipLODChangedAction;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_PauseEffect;
        private static DelegateBridge __Hotfix_ResumeEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_CollectAllDynamicResPath;
        private static DelegateBridge __Hotfix_OnPostLODLevelChanged;
        private static DelegateBridge __Hotfix_StartCurrLODLevelEffect;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_BuffLoop(SpaceShipController ownerShip, ShipNamedEffectType namedEffectType)
        {
        }

        [MethodImpl(0x8000)]
        public override void CollectAllDynamicResPath(ShipNamedEffectType effectName, List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnPostLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void PauseEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected override void Release()
        {
        }

        [MethodImpl(0x8000)]
        public override void ResumeEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void StartCurrLODLevelEffect(Dictionary<string, GameObject> effectAssets, ISpaceTargetProvider target, int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect()
        {
        }
    }
}

