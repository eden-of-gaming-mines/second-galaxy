﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface IBulletTargetProvider : ISpaceTargetProvider
    {
        void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out bool isHitOnShield);
        bool GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration, out bool isHitOnShield);
        Vector3 GetLocalTargetAimPointForHit();
        float GetTargetScale();
        bool IsTargetHasShield();
        void OnBulletHit(Vector3 worldHitPoint, bool isHitOnShield);
    }
}

