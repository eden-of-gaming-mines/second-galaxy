﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShipExplosionEffectController : PrefabControllerBase, IShipExplosionEffectController
    {
        protected int m_currLeftDelayPlayCount;
        protected PrefabShipExplosionEffectDesc m_prefabShipExplosionDesc;
        protected PrefabShipDesc m_prefabShipDesc;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipBodyRoot;
        private static DelegateBridge __Hotfix_StartShipExplosionEffect;
        private static DelegateBridge __Hotfix_GetPrefabShipExplosionEffectDesc;
        private static DelegateBridge __Hotfix_GetPrefabShipDesc;
        private static DelegateBridge __Hotfix_DelayPlayEffectObject;
        private static DelegateBridge __Hotfix_DelayPlayEffectAsset;
        private static DelegateBridge __Hotfix_HideShip;
        private static DelegateBridge __Hotfix_GetRandomWorldVectorLocation;

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DelayPlayEffectAsset()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DelayPlayEffectObject(GameObject effectObject, float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual PrefabShipDesc GetPrefabShipDesc()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual PrefabShipExplosionEffectDesc GetPrefabShipExplosionEffectDesc()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual Vector3 GetRandomWorldVectorLocation()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator HideShip()
        {
        }

        [MethodImpl(0x8000)]
        public void StartShipExplosionEffect()
        {
        }

        [CompilerGenerated]
        private sealed class <DelayPlayEffectAsset>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal GameObject <effectAsset>__1;
            internal GameObject <particleGo>__1;
            internal ShipExplosionEffectController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (((this.$this.m_prefabShipExplosionDesc == null) || (this.$this.m_prefabShipExplosionDesc.DelayPlayEffectAssetList.Count <= 0)) || (this.$this.m_currLeftDelayPlayCount <= 0))
                        {
                            this.$current = null;
                            if (!this.$disposing)
                            {
                                this.$PC = 1;
                            }
                        }
                        else
                        {
                            this.$this.m_currLeftDelayPlayCount--;
                            this.<effectAsset>__1 = this.$this.m_prefabShipExplosionDesc.DelayPlayEffectAssetList[UnityEngine.Random.Range(0, this.$this.m_prefabShipExplosionDesc.DelayPlayEffectAssetList.Count)];
                            this.<particleGo>__1 = UnityEngine.Object.Instantiate<GameObject>(this.<effectAsset>__1);
                            this.<particleGo>__1.transform.SetParent(this.$this.m_prefabShipDesc.m_deathEffect.transform, false);
                            this.<particleGo>__1.transform.position = this.$this.GetRandomWorldVectorLocation();
                            this.<particleGo>__1.SetActive(true);
                            UnityEngine.Object.Destroy(this.<particleGo>__1, 2f);
                            this.$current = new WaitForSeconds(UnityEngine.Random.Range(this.$this.m_prefabShipExplosionDesc.MinDelayTime, this.$this.m_prefabShipExplosionDesc.MaxDelayTime));
                            if (!this.$disposing)
                            {
                                this.$PC = 2;
                            }
                        }
                        return true;

                    case 1:
                        break;

                    case 2:
                        this.$this.StartCoroutine(this.$this.DelayPlayEffectAsset());
                        break;

                    default:
                        goto TR_0000;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DelayPlayEffectObject>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal float delayTime;
            internal GameObject effectObject;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <HideShip>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal PrefabShipDesc <shipDesc>__0;
            internal ShipExplosionEffectController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(this.$this.m_prefabShipExplosionDesc.HideShipModelTime);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.<shipDesc>__0 = this.$this.GetPrefabShipDesc();
                        if ((this.<shipDesc>__0 != null) && this.<shipDesc>__0.m_shipBody)
                        {
                            this.<shipDesc>__0.m_shipBody.SetActive(false);
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

