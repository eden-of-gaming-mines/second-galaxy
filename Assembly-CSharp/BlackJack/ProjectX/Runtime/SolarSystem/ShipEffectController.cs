﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ShipEffectController : PrefabControllerBase
    {
        protected Dictionary<ShipNamedEffectType, SpaceShipNamedEffect> m_shipNamedEffectDict;
        protected HashSet<SpaceShipNamedEffect> m_activeShipNamedEffectList;
        protected List<SpaceShipEffectBase> m_shipLoopEffectList;
        protected List<CachedNamedEffectInfo> m_cachedNamedEffectList;
        protected readonly List<string> m_resPathList;
        protected SpaceShipController m_ownerShip;
        protected IDynamicAssetProvider m_dynamicAssetProvider;
        protected TinyCorutineHelper m_corutineHelper;
        protected SpaceShipNamedEffect_Booster m_namedEffectBooster;
        protected SpaceShipNamedEffect_Blink m_namedEffectBlink;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickCacheNamedEffectList;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_CollectAllDynamicResPathByEffectName;
        private static DelegateBridge __Hotfix_CollectAllEffectConfigName;
        private static DelegateBridge __Hotfix_AllocAssets;
        private static DelegateBridge __Hotfix_ReleaseAssets;
        private static DelegateBridge __Hotfix_InitShipNamedEffectDict;
        private static DelegateBridge __Hotfix_TryGetShipNameEffectType;
        private static DelegateBridge __Hotfix_IsInvisible;
        private static DelegateBridge __Hotfix_PlayCommonOnceEffect;
        private static DelegateBridge __Hotfix_PlayCommonLoopEffectChangingByLerp01;
        private static DelegateBridge __Hotfix_PlayCommonLoopEffect;
        private static DelegateBridge __Hotfix_ReleaseCommonLoopEffect;
        private static DelegateBridge __Hotfix_PlayCommonLoopChannelEffect;
        private static DelegateBridge __Hotfix_StopCommonLoopEffect;
        private static DelegateBridge __Hotfix_PlayNamedEffect;
        private static DelegateBridge __Hotfix_StopNamedEffect;
        private static DelegateBridge __Hotfix_IsNamedEffectCanStart;
        private static DelegateBridge __Hotfix_IsNeedFrozenCamera;
        private static DelegateBridge __Hotfix_IsNeedStartCameraAnimator;
        private static DelegateBridge __Hotfix_IsNeedStartCameraShake;
        private static DelegateBridge __Hotfix_GetShipNamedEffect;
        private static DelegateBridge __Hotfix_AddShipNameEffectToActiveList;
        private static DelegateBridge __Hotfix_RemoveShipNamedEffectFromActiveList;
        private static DelegateBridge __Hotfix_IsSelfShip;
        private static DelegateBridge __Hotfix_get_NamedEffectBooster;
        private static DelegateBridge __Hotfix_get_NamedEffectBlink;

        [MethodImpl(0x8000)]
        protected void AddShipNameEffectToActiveList(SpaceShipNamedEffect namedEffect)
        {
        }

        [MethodImpl(0x8000)]
        public Dictionary<string, GameObject> AllocAssets(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void CollectAllDynamicResPathByEffectName(string effectName, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void CollectAllEffectConfigName(string effectName, List<string> resConfigNameList)
        {
        }

        [MethodImpl(0x8000)]
        protected SpaceShipNamedEffect GetShipNamedEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(SpaceShipController ownerShip, IDynamicAssetProvider dynamicAssetProvider)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitShipNamedEffectDict()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInvisible()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsNamedEffectCanStart(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedFrozenCamera()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedStartCameraAnimator(out string paramName, out bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedStartCameraShake(out string shakeType, out bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSelfShip()
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipEffectBase PlayCommonLoopChannelEffect(string effectName, ISpaceTargetProvider target, float loopTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipEffectBase PlayCommonLoopEffect(string effectName, ISpaceTargetProvider target, float loopTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceShipEffectBase PlayCommonLoopEffectChangingByLerp01(string effectName, ISpaceTargetProvider target, float loopTime, LoopEffectChangingByLerpParam01Type effectType, params object[] extraParams)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCommonOnceEffect(string effectName, ISpaceTargetProvider targetShip)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayNamedEffect(ShipNamedEffectType effectName, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public void ReleaseAssets(IEnumerable<string> resList)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator ReleaseCommonLoopEffect(SpaceShipEffectBase effect, float loopTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void RemoveShipNamedEffectFromActiveList(SpaceShipNamedEffect namedEffect)
        {
        }

        [MethodImpl(0x8000)]
        public void StopCommonLoopEffect(SpaceShipEffectBase shipEffect)
        {
        }

        [MethodImpl(0x8000)]
        public void StopNamedEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickCacheNamedEffectList()
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetShipNameEffectType(string effectName, out ShipNamedEffectType effectType)
        {
        }

        protected SpaceShipNamedEffect_Booster NamedEffectBooster
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected SpaceShipNamedEffect_Blink NamedEffectBlink
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ReleaseCommonLoopEffect>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal float loopTime;
            internal DateTime <endTime>__0;
            internal SpaceShipEffectBase effect;
            internal ShipEffectController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<endTime>__0 = Timer.m_currTime.AddSeconds((double) this.loopTime);
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<endTime>__0 > Timer.m_currTime)
                {
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$this.StopCommonLoopEffect(this.effect);
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public class CachedNamedEffectInfo
        {
            public ShipNamedEffectType m_effectName;
            public ISpaceTargetProvider m_target;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

