﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceObjectTraceMoveController : PrefabControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEndTraceTarget;
        protected float m_baseVelocity;
        public float m_currVelocity;
        protected float m_initAlign;
        protected float m_maxAlign;
        protected float m_alignAccelerateValue;
        public float m_currAlign;
        protected float m_reduceSpeedRadius;
        protected AnimationCurve m_velocityMulitClip;
        public Transform m_target;
        protected Vector3 m_lastWorldPosition;
        protected Quaternion m_lastWorldRotation;
        protected bool m_isUseLocalRandomMove;
        public float m_randomMoveSpeed;
        public float m_randomRange;
        public float m_randomMoveRadius;
        private Vector3 m_smootRandomPos;
        private Vector3 m_oldSmootRandomPos;
        private float m_deltaSpeed;
        private float m_startTime;
        private float m_randomSpeed;
        private float m_randomRadiusX;
        private float m_randomRadiusY;
        private int m_randomDirection1;
        private int m_randomDirection2;
        private int m_randomDirection3;
        public RandomMoveCoordinates m_randomMoveCoordinate;
        public bool m_isDeviation;
        private Transform m_child;
        private static DelegateBridge __Hotfix_SetVeloctiy;
        private static DelegateBridge __Hotfix_SetAlignSpeed;
        private static DelegateBridge __Hotfix_SetReduceSpeedRadius;
        private static DelegateBridge __Hotfix_SetTarget;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_InitLocalRandom;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_UpdateVelocity;
        private static DelegateBridge __Hotfix_UpdateAlign;
        private static DelegateBridge __Hotfix_UpdateSmootRandomhPos;
        private static DelegateBridge __Hotfix_OnTraceEnd;
        private static DelegateBridge __Hotfix_add_EventOnEndTraceTarget;
        private static DelegateBridge __Hotfix_remove_EventOnEndTraceTarget;

        public event Action EventOnEndTraceTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init(bool useLocalRandomMove)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLocalRandom()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTraceEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAlignSpeed(float initAlign, float maxAlign, float predictTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetReduceSpeedRadius(float radius)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTarget(Transform target)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVeloctiy(float baseVelocity, AnimationCurve clip)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateAlign()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSmootRandomhPos(float distance)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateVelocity(float distance)
        {
        }
    }
}

