﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipShieldController : PrefabControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipBodyRoot;
        protected PrefabShipDesc m_prefabShipDesc;
        protected Animator m_shipShieldAnimator;
        protected Material m_shipShieldMaterial;
        protected MeshFilter m_shipShieldMeshFilter;
        protected Vector3[] m_shipShieldMeshVertices;
        protected Vector3[] m_shipShieldMeshNormals;
        protected float m_shipRadius;
        protected int m_currOnHitNumber;
        protected const int OnHitCount = 5;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_OnBulletHit;
        private static DelegateBridge __Hotfix_StartShieldRecoverEffect;
        private static DelegateBridge __Hotfix_StopShieldRecoverEffect;
        private static DelegateBridge __Hotfix_StartShieldResistUpEffect;
        private static DelegateBridge __Hotfix_StartShieldResistDownEffect;
        private static DelegateBridge __Hotfix_SetShipRadius;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_GetPrefabShipDesc;
        private static DelegateBridge __Hotfix_GetShipShieldAnimator;
        private static DelegateBridge __Hotfix_GetShipShieldMaterial;
        private static DelegateBridge __Hotfix_GetShipShieldMeshFilter;
        private static DelegateBridge __Hotfix_GetCurrOnHitNumber;
        private static DelegateBridge __Hotfix_GetOnHitNumberParamNameForMaterial;
        private static DelegateBridge __Hotfix_GetOnHitNumberParamNameForAnimator;

        [MethodImpl(0x8000)]
        protected int GetCurrOnHitNumber()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetOnHitNumberParamNameForAnimator(int onHitNumber)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetOnHitNumberParamNameForMaterial(int onHitNumber)
        {
        }

        [MethodImpl(0x8000)]
        protected PrefabShipDesc GetPrefabShipDesc()
        {
        }

        [MethodImpl(0x8000)]
        protected Animator GetShipShieldAnimator()
        {
        }

        [MethodImpl(0x8000)]
        protected Material GetShipShieldMaterial()
        {
        }

        [MethodImpl(0x8000)]
        protected MeshFilter GetShipShieldMeshFilter()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void OnBulletHit(Vector3 worldHitPoint)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipRadius(float shipRadius)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartShieldRecoverEffect()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartShieldResistDownEffect()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartShieldResistUpEffect()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StopShieldRecoverEffect()
        {
        }
    }
}

