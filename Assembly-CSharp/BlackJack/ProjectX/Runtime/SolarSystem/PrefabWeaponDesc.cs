﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class PrefabWeaponDesc : MonoBehaviour
    {
        [Header("武器根骨骼")]
        public GameObject WeaponRootBone;
        [Header("旋转控制")]
        public GameObject RotationRoot;
        [Header("俯仰控制")]
        public GameObject PitchRoot;
        [Header("炮台旋转最大值")]
        public float RotationMaxDegree;
        [Header("炮台旋转最小值")]
        public float RotationMinDegree;
        [Header("炮台仰角最大值")]
        public float PitchMaxDegree;
        [Header("炮台仰角最小值")]
        public float PitchMinDegree;
        [Header("武器发射点列表")]
        public List<GameObject> LaunchPointList;
    }
}

