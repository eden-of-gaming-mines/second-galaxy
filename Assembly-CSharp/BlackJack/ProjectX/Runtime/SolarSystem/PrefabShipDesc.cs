﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class PrefabShipDesc : MonoBehaviour
    {
        [Header("飞船模型")]
        public GameObject m_shipBody;
        [Header("飞船隐身模型")]
        public GameObject m_shipInvisibleBody;
        [Header("飞船RoughGrid")]
        public GameObject m_shipRoughGrid;
        [Header("飞船护盾对象")]
        public GameObject m_shipShield;
        [Header("飞船DeathEffect")]
        public GameObject m_deathEffect;
        [Header("飞船缩放特效根节点")]
        public GameObject m_scaleEffectRoot;
        [Header("飞船无缩放特效根节点")]
        public GameObject m_noScaleEffectRoot;
        [Header("飞船自定义位置和缩放特效根节点")]
        public GameObject m_customUniformScaleEffectRoot;
        [Header("常规武器逻辑炮位组的定义")]
        public List<LogicWeaponGroupDesc> LogicWeaponGroupList;
        [Header("超级武器逻辑炮位组定义")]
        public List<LogicWeaponSlotDesc> SuperWeaponSlotList;
        [Header("普通武器逻辑炮位组定义")]
        public List<LogicWeaponSlotDesc> NormalWeaponSlotList;
        [Header("飞船缩放比")]
        public float m_shipScale;
        [Header("飞船武器缩放比")]
        public float m_shipWeaponScale;
        [Header("飞船武器效果缩放比")]
        public float m_shipWeaponEffectScale;
        [Header("飞船超级武器缩放比")]
        public float m_shipSuperWeaponScale;

        [Serializable]
        public class LogicWeaponGroupDesc
        {
            public List<PrefabShipDesc.LogicWeaponSlotDesc> LogicWeaponSlots;
            private static DelegateBridge _c__Hotfix_ctor;

            public LogicWeaponGroupDesc()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        [Serializable]
        public class LogicWeaponSlotDesc
        {
            public List<GameObject> RealSlots;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

