﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class PlanetController : SpaceObjectControllerBase
    {
        protected Animator m_planetEffectAnimator;
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_InitSpaceObjectPrefab;
        private static DelegateBridge __Hotfix_PlayPlanetEffect;
        private static DelegateBridge __Hotfix_GetPrefabCelestialOnHitEffectDesc;
        private static DelegateBridge __Hotfix_get_DisplayNameForUI;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;
        private static DelegateBridge __Hotfix_get_PlanetEffectAnimator;
        private static DelegateBridge __Hotfix_set_PlanetEffectAnimator;

        [MethodImpl(0x8000)]
        public override bool Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        public PrefabCelestialOnHitEffectDesc GetPrefabCelestialOnHitEffectDesc()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSpaceObjectPrefab()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayPlanetEffect(CelestialEffectType effectType)
        {
        }

        public override string DisplayNameForUI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientPlanet ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected Animator PlanetEffectAnimator
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

