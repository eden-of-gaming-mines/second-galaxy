﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShipSignalLightController : MonoBehaviour
    {
        public List<LightsAndParams> LightAndParamsList;
        [Header("单个闪烁的持续时长")]
        public float PulseDuration;
        public bool LightEnabled;
        [Header("灯光Prefab")]
        public GameObject LightPrefab;
        [Header("合并后的Mesh所在的对象")]
        public GameObject CombinedMeshObject;
        private bool m_hasInitialized;
        private static DelegateBridge __Hotfix_EnableLight_1;
        private static DelegateBridge __Hotfix_EnableLight_0;
        private static DelegateBridge __Hotfix_SetVisible;
        private static DelegateBridge __Hotfix_LateUpdate;
        private static DelegateBridge __Hotfix_CombineSignalLights;

        [MethodImpl(0x8000)]
        protected void CombineSignalLights(SkinnedMeshRenderer baseRender, List<SkinnedMeshRenderer> combineRenderList)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableLight(bool enableLight)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableLight(bool enableImportent, bool enableNotImportent)
        {
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(bool isVisible)
        {
        }

        [Serializable]
        public class LightsAndParams
        {
            public List<GameObject> LightList;
            public float MaxScale;
            public float MinScale;
            public float LoopTime;
            public List<float> PulseStartTime;
            [HideInInspector]
            public float AnimationTimer;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

