﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public class ShipEffectLOD2Controller : ShipEffectLODController
    {
        public ShipEffectLOD2Controller()
        {
            base.m_LODLevel = 2;
        }
    }
}

