﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimation_OutSceneClip : SceneAnimation_CutSceneBase
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_DelayExecuteAfterPlay;
        private static DelegateBridge __Hotfix_IsRelocationCamera;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_GetLayerMaskForFakeActor;

        [MethodImpl(0x8000)]
        public SceneAnimation_OutSceneClip(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public override void DelayExecuteAfterPlay()
        {
        }

        [MethodImpl(0x8000)]
        public override Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected override int GetLayerMaskForFakeActor()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsRelocationCamera()
        {
        }

        [MethodImpl(0x8000)]
        public override void Play(Transform target, float targetSize)
        {
        }
    }
}

