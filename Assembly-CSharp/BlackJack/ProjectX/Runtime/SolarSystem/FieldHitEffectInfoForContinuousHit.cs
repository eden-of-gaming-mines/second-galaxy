﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class FieldHitEffectInfoForContinuousHit : FieldHitEffectInfoBase
    {
        public int m_hitDataId;
        private float m_hitEffectPeriodTime;
        private float m_continuousHitFinishedTime;
        private int m_currFieldHitState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnContinuousHitStart;
        private static DelegateBridge __Hotfix_OnContinuousHitFnished;
        private static DelegateBridge __Hotfix_IsFieldHitEffectFinished;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_ResetData;
        private static DelegateBridge __Hotfix_UpdateEffectHitPosition;

        [MethodImpl(0x8000)]
        public FieldHitEffectInfoForContinuousHit(Vector3 position, float radius, float effectPeriodTime, int hitDataId)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsFieldHitEffectFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void OnContinuousHitFnished()
        {
        }

        [MethodImpl(0x8000)]
        public void OnContinuousHitStart()
        {
        }

        [MethodImpl(0x8000)]
        public override void ResetData()
        {
        }

        [MethodImpl(0x8000)]
        public override void Update(float deltaTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEffectHitPosition(Vector3 hitPosition)
        {
        }
    }
}

