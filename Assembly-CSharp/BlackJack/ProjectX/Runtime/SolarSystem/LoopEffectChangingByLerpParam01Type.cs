﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public enum LoopEffectChangingByLerpParam01Type
    {
        ChangingByDistanceToTarget,
        ChangingBySpeedOverload,
        ChangingBySelfSpeed,
        ChangingBySelfEnergyPercent,
        ChangingBySelfShieldPercent,
        ChangingBySpeedOverPercent,
        ChangingByLocalTargetCount
    }
}

