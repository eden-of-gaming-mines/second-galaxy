﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimation_SimpleProgram : SceneAnimationBase
    {
        protected float m_leftLifeTime;
        protected int m_simpleProgramCameraConfigId;
        protected ConfigDataSimpleProgramCamera m_simpleProgramCameraConfigInfo;
        protected GameObject m_cameraPlatform;
        protected GameObject m_yRotationRootForCamera;
        protected GameObject m_xRotationRootForCamera;
        protected GameObject m_cameraDummy;
        protected bool m_isAutoRotation;
        protected float m_autoRotationSpeed;
        private const float CAMERA_EXTRA_ROTATION_ANGLE_AXIS_X = 6f;
        protected const float NearCameraDistanceMulit = 5f;
        protected const float MiddleCameraDistanceMulit = 10f;
        protected const float FarCameraDistanceMulit = 25f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CreateClipAssetGo;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_OnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_SetSimpleProgramCameraConfigId;
        private static DelegateBridge __Hotfix_GetCameraZOffset;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_CreateCameraDummyObject;
        private static DelegateBridge __Hotfix_GetSimpleProgramConfigInfo;
        private static DelegateBridge __Hotfix_InitCameraLocation;
        private static DelegateBridge __Hotfix_InitCameraLocationInCenter;
        private static DelegateBridge __Hotfix_InitCameraLocationAlongTargetDirection;
        private static DelegateBridge __Hotfix_InitCameraLocationSyncToCamera;
        private static DelegateBridge __Hotfix_UpdateCameraDummyLocation;
        private static DelegateBridge __Hotfix_SetCameraRotation;
        private static DelegateBridge __Hotfix_SetCameraDummyLocalOffset;
        private static DelegateBridge __Hotfix_SetCameraLookAtDirection;

        [MethodImpl(0x8000)]
        public SceneAnimation_SimpleProgram(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        protected void CreateCameraDummyObject()
        {
        }

        [MethodImpl(0x8000)]
        public override void CreateClipAssetGo()
        {
        }

        [MethodImpl(0x8000)]
        public override Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetCameraZOffset()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetSimpleProgramConfigInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitCameraLocation(Transform target, float targetSize)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitCameraLocationAlongTargetDirection(Transform target, float targetSize, float distanceMulit)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitCameraLocationInCenter(Transform target, float targetSize)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitCameraLocationSyncToCamera(Transform target, float targetSize)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSceneAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        public override void Play(Transform target, float targetSize)
        {
        }

        [MethodImpl(0x8000)]
        public override void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraDummyLocalOffset(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetCameraLookAtDirection(Vector3 dir)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraRotation(float rotateAngleForAxisX, float rotateAngleForAxisY)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSimpleProgramCameraConfigId(int configId)
        {
        }

        [MethodImpl(0x8000)]
        public override void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float delayTime, Vector3 targetLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCameraDummyLocation(float delayTime, Vector3 targetLocation)
        {
        }
    }
}

