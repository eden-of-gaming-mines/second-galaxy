﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public class PrefabDroneAroundTrajectoryDesc : MonoBehaviour
    {
        [Header("环绕速度(度/s)")]
        public float m_aroundDegreeSpeed;
        [Header("俯仰速度(度/s)")]
        public float m_upDownDregreeSpeed;
    }
}

