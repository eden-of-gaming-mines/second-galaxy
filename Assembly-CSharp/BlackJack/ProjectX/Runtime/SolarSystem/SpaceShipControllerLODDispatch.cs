﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceShipControllerLODDispatch : ControllerLODDispatchBase
    {
        protected PrefabSuperWeaponEffectDesc m_superWeaponEffectDesc;
        protected PrefabWeaponEffectDesc m_normalWeaponEffectDesc;
        private GameObject m_lod0Asset;
        protected float m_shipRadius;
        private ControllerDesc[] m_LOD0CtrlsDesc;
        private ControllerDesc[] m_LOD1CtrlsDesc;
        private ControllerDesc[] m_LOD2CtrlsDesc;
        private static DelegateBridge __Hotfix_InitLODController;
        private static DelegateBridge __Hotfix_GetLODObjectPrefabNameByLODLevel;
        private static DelegateBridge __Hotfix_SetShipLOD0Asset;
        private static DelegateBridge __Hotfix_GetLODPrefab;
        private static DelegateBridge __Hotfix_GetLODGameObjectName;
        private static DelegateBridge __Hotfix_InitAllLODRoot;
        private static DelegateBridge __Hotfix_SetShipNormalWeaponEffectDesc;
        private static DelegateBridge __Hotfix_GetShipRadius;
        private static DelegateBridge __Hotfix_SetShipSuperWeaponEffectDesc;
        private static DelegateBridge __Hotfix_get_CurrLODController;
        private static DelegateBridge __Hotfix_get_LOD0CtrlsDesc;
        private static DelegateBridge __Hotfix_get_LOD1CtrlsDesc;
        private static DelegateBridge __Hotfix_get_LOD2CtrlsDesc;

        [MethodImpl(0x8000)]
        protected override string GetLODGameObjectName()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetLODObjectPrefabNameByLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override GameObject GetLODPrefab(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public float GetShipRadius()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllLODRoot()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLODController(ILODController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipLOD0Asset(GameObject asset)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipNormalWeaponEffectDesc(PrefabWeaponEffectDesc effectDesc)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipSuperWeaponEffectDesc(PrefabSuperWeaponEffectDesc effectDesc)
        {
        }

        public SpaceShipLODController CurrLODController
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override ControllerDesc[] LOD0CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override ControllerDesc[] LOD1CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override ControllerDesc[] LOD2CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

