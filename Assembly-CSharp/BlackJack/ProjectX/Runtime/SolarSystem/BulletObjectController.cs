﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Art;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletObjectController : PrefabControllerBase
    {
        public CustomAction<object> m_eventOnBulletDestory;
        protected Action<object> m_onBulletTargetDestroy;
        protected BulletControllerState m_bulletCtrlState;
        protected bool m_cachedWhenDestroy;
        protected WeaponCategory m_bulletType;
        protected float m_bulletFlyTime;
        protected float m_bulletLifeTime;
        protected float m_bulletScale;
        protected bool m_isBulletRebound;
        protected bool m_isAutoDestroy;
        protected DateTime m_currLifeTime;
        protected DateTime m_currFlyOutTime;
        protected bool m_isHit;
        protected bool m_isOnHit;
        protected BulletTargetProviderWarpper m_bulletTarget;
        protected Transform m_bulletTargetTransform;
        protected Transform m_bulletLaunchTransform;
        protected Vector3 m_worldBulletLaunchInitPosition;
        protected Quaternion m_worldBulletLaunchInitRotate;
        protected GameObject m_bulletEffectAsset;
        protected GameObject m_bulletEffectObject;
        protected GameObject[] m_bulletEffectLODObjectArray;
        protected int m_currBulletLODLevel;
        protected Vector3 m_bulletEffectObjectInitDir;
        protected Vector3 m_onHitEffectOjbectInitDir;
        protected GameObject m_onHitEffectAsset;
        protected GameObject m_onHitEffectObject;
        protected GameObject[] m_onHitEffectLODObjectArray;
        protected float m_onHitEffectLastTime;
        protected bool m_isHitOnShield;
        protected bool m_isOnHitEffectLODObjectHasCreated;
        protected ParticleSystem[] m_particleSystems;
        protected FxParticleScaler m_fxParticleScaler;
        protected static TypeDNName m_lodSwitcherDescTypeDNName;
        protected static TypeDNName m_lodSwitcherTypeDNName;
        protected const float BulletOnHitEffectFadeOutMaxLifeTime = 5f;
        protected const int MAXLODLEVEL = 3;
        protected const string BulletAniStateName = "BulletState";
        protected const int BulletAniStateParam_Fly = 1;
        protected const int BulletAniStateParam_BurstFly = 2;
        protected const int BulletAniStateParam_FadeOut = 3;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_StartBulletFly;
        private static DelegateBridge __Hotfix_StopBulletFly;
        private static DelegateBridge __Hotfix_CalcBulletLifeTime;
        private static DelegateBridge __Hotfix_UpdateBulletFly;
        private static DelegateBridge __Hotfix_UpdateBulletOnHit;
        private static DelegateBridge __Hotfix_SetBulletAsset;
        private static DelegateBridge __Hotfix_GetBulletEffectAsset;
        private static DelegateBridge __Hotfix_SetOnHitAsset;
        private static DelegateBridge __Hotfix_GetOnHitEffectAsset;
        private static DelegateBridge __Hotfix_CalcOnHitEffectLastTime;
        private static DelegateBridge __Hotfix_OnBulletEffectLODLevelChanged;
        private static DelegateBridge __Hotfix_SetBulletObjectScale;
        private static DelegateBridge __Hotfix_OnHitEffectLODLevelChanged;
        private static DelegateBridge __Hotfix_SetIsHit;
        private static DelegateBridge __Hotfix_SetOnHitObjectScale;
        private static DelegateBridge __Hotfix_SetBulletTarget;
        private static DelegateBridge __Hotfix_SetBulletLaunchTransform;
        private static DelegateBridge __Hotfix_OnBulletTargetDestory;
        private static DelegateBridge __Hotfix_OnBulletDestroy;
        private static DelegateBridge __Hotfix_IsBulletHit;
        private static DelegateBridge __Hotfix_OnBulletHit;
        private static DelegateBridge __Hotfix_GetLocalTargetOnHitPosition;
        private static DelegateBridge __Hotfix_DestroyBulletLater;
        private static DelegateBridge __Hotfix_DestroyBullet;
        private static DelegateBridge __Hotfix_DesotryBulletImp;
        private static DelegateBridge __Hotfix_ShowBulletEffectObject;
        private static DelegateBridge __Hotfix_SetBulletEffectObjectState;
        private static DelegateBridge __Hotfix_SetBulletEffectObjectStateImp;
        private static DelegateBridge __Hotfix_ShowOnHitEffectObject;
        private static DelegateBridge __Hotfix_SetOnHitEffectFollowTarget;
        private static DelegateBridge __Hotfix_PlayEffect;
        private static DelegateBridge __Hotfix_ResetEffect;
        private static DelegateBridge __Hotfix_GetOnHitLODEffectNameByLODLevel;
        private static DelegateBridge __Hotfix_GetBulletLODEffectNameByLODLevel;
        private static DelegateBridge __Hotfix_get_BulletCtrlState;
        private static DelegateBridge __Hotfix_set_BulletCtrlState;
        private static DelegateBridge __Hotfix_get_CachedWhenDestroy;
        private static DelegateBridge __Hotfix_set_CachedWhenDestroy;
        private static DelegateBridge __Hotfix_get_BulletType;
        private static DelegateBridge __Hotfix_set_BulletType;
        private static DelegateBridge __Hotfix_get_BulletFlyTime;
        private static DelegateBridge __Hotfix_set_BulletFlyTime;
        private static DelegateBridge __Hotfix_get_BulletLifeTime;
        private static DelegateBridge __Hotfix_set_BulletLifeTime;
        private static DelegateBridge __Hotfix_get_BulletScale;
        private static DelegateBridge __Hotfix_set_BulletScale;
        private static DelegateBridge __Hotfix_get_IsBulletRebound;
        private static DelegateBridge __Hotfix_set_IsBulletRebound;

        [MethodImpl(0x8000)]
        protected void Awake()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual float CalcBulletLifeTime()
        {
        }

        [MethodImpl(0x8000)]
        protected float CalcOnHitEffectLastTime(GameObject onHitEffectObject)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DesotryBulletImp()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void DestroyBullet()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected virtual IEnumerator DestroyBulletLater(float time)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetBulletEffectAsset()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetBulletLODEffectNameByLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLocalTargetOnHitPosition()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetOnHitEffectAsset()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetOnHitLODEffectNameByLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsBulletHit()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBulletDestroy()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBulletEffectLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBulletHit(Vector3 worldOnHitLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnBulletTargetDestory(object obj)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnHitEffectLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PlayEffect(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Reset()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ResetEffect(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetBulletAsset(GameObject bulletAsset, Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetBulletEffectObjectState(BulletControllerState bulletState)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetBulletEffectObjectStateImp(BulletControllerState bulletState, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletLaunchTransform(Transform launchTransform)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetBulletObjectScale(GameObject bulletObject)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetBulletTarget(BulletTargetProviderWarpper target)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsHit(bool isHit)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetOnHitAsset(GameObject onHitAsset, Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetOnHitEffectFollowTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SetOnHitObjectScale(GameObject onHitObject)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowBulletEffectObject(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void ShowOnHitEffectObject(bool isShow, Vector3 worldOnHitLocation)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StopBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void UpdateBulletOnHit()
        {
        }

        protected BulletControllerState BulletCtrlState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool CachedWhenDestroy
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public WeaponCategory BulletType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float BulletFlyTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float BulletLifeTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        public float BulletScale
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsBulletRebound
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <DesotryBulletImp>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal BulletObjectController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(1f);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        UnityEngine.Object.Destroy(this.$this.gameObject);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <DestroyBulletLater>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal float time;
            internal BulletObjectController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = new WaitForSeconds(this.time);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.$this.DestroyBullet();
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public enum BulletControllerState
        {
            None,
            Flying,
            BurstFly,
            OnHit,
            Destroy
        }

        protected enum BulletTargetType
        {
            Transform,
            Position
        }
    }
}

