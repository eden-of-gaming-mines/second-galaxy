﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ShipSuperLaserEffect
    {
        public BulletCreator BulletCreatorFunc;
        protected BulletLaunchInfo m_baseBulletLaunchInfo;
        protected List<BulletTargetProviderWarpper> m_targetList;
        protected List<BulletObjectController> m_laserCtrlList;
        protected SpaceShipController m_ownerShip;
        protected float m_leftLifeTime;
        protected float m_lifeTime;
        protected BulletTargetProviderWarpper m_lastTarget;
        protected BulletLaunchInfo m_lastBulletLaunchInfo;
        protected BulletObjectController m_lastBulletCtrl;
        protected int m_nextTargetIndex;
        protected float m_nextTargetDelayTime;
        protected const float NextTargetDelayTime = 0.3f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_IsLife;
        private static DelegateBridge __Hotfix_SetSuperLaserBulletLaunchInfo;
        private static DelegateBridge __Hotfix_SetSuperLaserTargetList;
        private static DelegateBridge __Hotfix_StarSuperLaserFire;
        private static DelegateBridge __Hotfix_StartSuperLaserNextFire;
        private static DelegateBridge __Hotfix_StopSuperLaser;

        [MethodImpl(0x8000)]
        public void Init(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLife()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuperLaserBulletLaunchInfo(BulletLaunchInfo bulletLaunchInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuperLaserTargetList(List<BulletTargetProviderWarpper> targetList)
        {
        }

        [MethodImpl(0x8000)]
        public void StarSuperLaserFire(float lifeTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartSuperLaserNextFire()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopSuperLaser()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        public delegate BulletObjectController BulletCreator(BulletLaunchInfo bulletLaunchInfo, WeaponCategory weaponCategory, BulletTargetProviderWarpper bulletTarget, float bulletFlyTime, bool isHit, bool isSuper = false);
    }
}

