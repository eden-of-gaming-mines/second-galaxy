﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceObjectUtil
    {
        public const int MAXLODLEVEL = 3;
        public const float MinBulletHitMissCircleRadiusMulti = 0.8f;
        public const float MaxBulletHitMissCircleRadiusMulti = 1f;
        public const float MinBulletHitMissFlyDistanceMulti = 10f;
        public const float MaxBulletHitMissFlyDistanceMulti = 20f;
        public const float MinBulletHitFlyDistanceMulti = -1f;
        public const float MaxBulletHitFlyDistanceMulti = -0.5f;
        public const string ShipLoopEffectParam_IsActive = "isActive";
        public const string ShipChangingLoopEffectParam_PassiveState = "PassiveState";
        public const float ShipLoopEffectEndLifeTime = 2f;
        public const float ShipChangingLoopEffectEndLifeTime = 5f;
        public const float SpaceGlobalEffectDefaultLiftTime = 10f;
        public static string ShipDefaultErrorAssetPath;
        public static string ShipLODDefaultErrorAssetPath;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetLocalHitPointWhenBulletHit;
        private static DelegateBridge __Hotfix_GetLocalHitPointWhenBulletHitMiss;
        private static DelegateBridge __Hotfix_GetRandomPointFromVerticalCircleOnSphere;
        private static DelegateBridge __Hotfix_SetRendererSortingOrderByMaterial_1;
        private static DelegateBridge __Hotfix_SetRendererSortingOrderByMaterial_0;
        private static DelegateBridge __Hotfix_GetNameMatchedChildsFromParent;
        private static DelegateBridge __Hotfix_FillLineRender;
        private static DelegateBridge __Hotfix_CollectShipDefaultErrorAssetPath;
        private static DelegateBridge __Hotfix_GetShipDefaultErrorPrefab;
        private static DelegateBridge __Hotfix_GetShipLODDefaultErrorPrefab;
        private static DelegateBridge __Hotfix_PlaySoundOnShip_0;
        private static DelegateBridge __Hotfix_PlaySoundOnShip_1;
        private static DelegateBridge __Hotfix_StopSoundOnShip_1;
        private static DelegateBridge __Hotfix_StopSoundOnShip_0;
        private static DelegateBridge __Hotfix_SetGameObjectLayer;

        [MethodImpl(0x8000)]
        public static void CollectShipDefaultErrorAssetPath(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void FillLineRender(LineRenderer line, Vector3 startPos, Vector3 endPos, int segmentCount, float startWidth, float endWidth)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3 GetLocalHitPointWhenBulletHit(Vector3 worldBulletLocation, Transform shipTransform, float shipRadius)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3 GetLocalHitPointWhenBulletHitMiss(Vector3 worldBulletLocation, Transform shipTransform, float shipRadius)
        {
        }

        [MethodImpl(0x8000)]
        public static List<Transform> GetNameMatchedChildsFromParent(Transform parent, string compareStr)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector3 GetRandomPointFromVerticalCircleOnSphere(Vector3 normal, Vector3 point, float radius)
        {
        }

        [MethodImpl(0x8000)]
        public static GameObject GetShipDefaultErrorPrefab(IDynamicAssetProvider assetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public static GameObject GetShipLODDefaultErrorPrefab(IDynamicAssetProvider assetProvider)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlaySoundOnShip(GameObject soundHolder, SpaceShipController shipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlaySoundOnShip(string soundName, PlaySoundOption playOption, bool isLoop, SpaceShipController shipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetGameObjectLayer(GameObject go, int layer)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetRendererSortingOrderByMaterial(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetRendererSortingOrderByMaterial(Renderer r)
        {
        }

        [MethodImpl(0x8000)]
        public static void StopSoundOnShip(string soundName, SpaceShipController shipCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public static void StopSoundOnShip(GameObject soundHolder, SpaceShipController shipCtrl)
        {
        }
    }
}

