﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class PrefabShipEngineEffectDesc : MonoBehaviour
    {
        public List<MeshRenderer> FlameList;
        public List<Renderer> FlareList;
        public int NotImportentFlareStartIndex;
        public List<TrailRenderer> TrailList;
        [Header("推进器加力特效")]
        public GameObject EngineBoosterGo;
        public int NotImportentTrailStartIndex;
        [Header("飞船尾焰的速度参数-最小")]
        public float m_flameSpeedMin;
        [Header("飞船尾焰的速度参数-最大")]
        public float m_flameSpeedMax;
        [Header("飞船尾焰的强度参数-最小")]
        public float m_flameIntensityMin;
        [Header("飞船尾焰的强度参数-最大")]
        public float m_flameIntensityMax;
        [Header("飞船尾部光效强度-最小")]
        public float m_flareIntensityMin;
        [Header("飞船尾部光效强度-最大")]
        public float m_flareIntensityMax;
        [Header("跃迁达到最大亮度的速度百分比"), Range(0f, 100f)]
        public double SpeedRatioForJumpingMaxFlameBrightness;
        [Range(0f, 100f), Header("跃迁最大亮度")]
        public double MaxFlameBrightnessOnJumping;
        [Header("巡航最大亮度"), Range(0f, 100f)]
        public double MaxFlameBrightnessOnNormalCruise;
        [Header("推进器额外增加的亮度"), Range(0f, 100f)]
        public double MaxFlameBrightnessForEquipExtra;
        [Header("飞船条带消失的速度线")]
        public double TrailDisappearShipSpeed;
        [Header("飞船条带最亮时的速度线")]
        public double TrailMaxBrightnessShipSpeed;
        [Header("飞船条带的最小生命期")]
        public double TrailLifeTimeMin;
        [Header("飞船条带的最大生命期")]
        public double TrailLifeTimeMax;
    }
}

