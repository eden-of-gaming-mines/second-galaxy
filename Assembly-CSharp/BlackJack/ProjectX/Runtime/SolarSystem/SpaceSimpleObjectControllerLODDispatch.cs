﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceSimpleObjectControllerLODDispatch : ControllerLODDispatchBase
    {
        private ControllerDesc[] m_LOD0CtrlsDesc;
        private ControllerDesc[] m_LOD1CtrlsDesc;
        private ControllerDesc[] m_LOD2CtrlsDesc;
        private static DelegateBridge __Hotfix_InitLODController;
        private static DelegateBridge __Hotfix_GetLODObjectPrefabNameByLODLevel;
        private static DelegateBridge __Hotfix_GetLODGameObjectName;
        private static DelegateBridge __Hotfix_InitAllLODRoot;
        private static DelegateBridge __Hotfix_get_CurrLODController;
        private static DelegateBridge __Hotfix_get_LOD0CtrlsDesc;
        private static DelegateBridge __Hotfix_get_LOD1CtrlsDesc;
        private static DelegateBridge __Hotfix_get_LOD2CtrlsDesc;

        [MethodImpl(0x8000)]
        protected override string GetLODGameObjectName()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetLODObjectPrefabNameByLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllLODRoot()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLODController(ILODController ctrl)
        {
        }

        public SpaceSimpleObjectControllerLODDispatch CurrLODController
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override ControllerDesc[] LOD0CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override ControllerDesc[] LOD1CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override ControllerDesc[] LOD2CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

