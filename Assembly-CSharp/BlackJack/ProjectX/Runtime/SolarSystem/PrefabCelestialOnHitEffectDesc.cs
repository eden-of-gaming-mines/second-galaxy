﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public class PrefabCelestialOnHitEffectDesc : MonoBehaviour
    {
        [Header("受击特效的宽度(米)")]
        public float m_onHitEffectWidth;
        [Header("武器特效的起点")]
        public Transform m_onHitEffctStartPoint;
        [Header("特效本体")]
        public LineRenderer m_onHitEffectLine;
        [Header("line的分段数")]
        public int m_lineSegmentCount;
    }
}

