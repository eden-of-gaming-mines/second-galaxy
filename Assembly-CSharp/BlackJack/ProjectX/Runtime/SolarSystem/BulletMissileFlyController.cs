﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletMissileFlyController : MonoBehaviour
    {
        protected const float m_minEndGuidedTime = 2f;
        protected PrefabMissileDesc m_missileDesc;
        protected GameObject m_missileGo;
        protected GameObject m_missileControllGo;
        protected bool m_isStartLocalOffset;
        protected float m_localOffsetLastTime;
        protected Vector3 m_missileGoLastPos;
        protected MissileFlyState m_missileState;
        protected Vector3 m_worldSelfShipLocation;
        protected Vector3 m_worldTargetLocation;
        protected Vector3 m_launchInitWorldDir;
        protected float m_launchFlyDistance;
        protected Vector3 m_launchInitLocalPosition;
        public Bounds m_hitPointBounds;
        public const float DetonationDistance = 0.5f;
        public const float PrepareDetonationDistance = 20f;
        protected float m_velocity;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Transform <LaunchLocation>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private float <SrcShipScale>k__BackingField;
        protected float m_realVelocity;
        protected float m_unGuideDecelerationRate;
        protected float m_guideAccelerationRate;
        protected float m_alignSpeedAccelerationRate;
        protected float m_alignSpeed;
        protected float m_realAlignSpeed;
        protected Transform m_cahedTransform;
        protected Vector3 m_step;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnHit;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStartGuideFly;
        private DateTime m_currStateOutTime;
        protected float m_launchUnguideTime;
        protected float m_launchGuideTime;
        protected float m_endGuideTime;
        private float m_endGuideLeftTime;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetFlyState;
        private static DelegateBridge __Hotfix_OnHit;
        private static DelegateBridge __Hotfix_UpdateMissileFly;
        private static DelegateBridge __Hotfix_LocalOffsetMissileGo;
        private static DelegateBridge __Hotfix_UpdateUnguided;
        private static DelegateBridge __Hotfix_UpdateGuided;
        private static DelegateBridge __Hotfix_UpdateEndGuided;
        private static DelegateBridge __Hotfix_CalcMinFlyTime;
        private static DelegateBridge __Hotfix_CheckIsCrossTargetPoint;
        private static DelegateBridge __Hotfix_get_Velocity;
        private static DelegateBridge __Hotfix_set_Velocity;
        private static DelegateBridge __Hotfix_get_LaunchLocation;
        private static DelegateBridge __Hotfix_set_LaunchLocation;
        private static DelegateBridge __Hotfix_get_SrcShipScale;
        private static DelegateBridge __Hotfix_set_SrcShipScale;
        private static DelegateBridge __Hotfix_add_EventOnHit;
        private static DelegateBridge __Hotfix_remove_EventOnHit;
        private static DelegateBridge __Hotfix_add_EventOnStartGuideFly;
        private static DelegateBridge __Hotfix_remove_EventOnStartGuideFly;
        private static DelegateBridge __Hotfix_get_LaunchUnguideTime;
        private static DelegateBridge __Hotfix_set_LaunchUnguideTime;
        private static DelegateBridge __Hotfix_get_LaunchGuideTime;
        private static DelegateBridge __Hotfix_set_LaunchGuideTime;
        private static DelegateBridge __Hotfix_get_EndGuideTime;
        private static DelegateBridge __Hotfix_set_EndGuideTime;

        public event Action EventOnHit
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStartGuideFly
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public static float CalcMinFlyTime()
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsCrossTargetPoint(Vector3 startPos, Vector3 endPos)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(Vector3 selfShipLocation, float flyTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void LocalOffsetMissileGo()
        {
        }

        [MethodImpl(0x8000)]
        public void OnHit()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlyState(MissileFlyState state)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateEndGuided()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateGuided()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMissileFly(Vector3 selfLocation, Vector3 targetLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateUnguided()
        {
        }

        public float Velocity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public Transform LaunchLocation
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public float SrcShipScale
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public float LaunchUnguideTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float LaunchGuideTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float EndGuideTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public enum MissileFlyState
        {
            Unguided,
            Guided,
            EndGuided
        }
    }
}

