﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using System;
    using UnityEngine;

    [AddComponentMenu("SolarSystem/PrefabDroneDesc")]
    public class PrefabDroneDesc : PrefabResourceContainerBase
    {
        [Header("无人机资源")]
        public GameObject m_droneAsset;
        [Header("无人机Render")]
        public SkinnedMeshRenderer m_droneRender;
        [Header("无人机骨骼根节点")]
        public Transform m_droneBoneRoot;
        [Header("无人机爆炸效果资源")]
        public GameObject m_droneExplosionAsset;
        [Header("自己无人机EngineFlare资源()")]
        public GameObject m_selfDroneEngineFlareAsset;
        [Header("友方无人机EngineFlare资源")]
        public GameObject m_frienDroneEngineFlareAsset;
        [Header("敌方无人机EngineFlare资源")]
        public GameObject m_enemyDroneEngineFlareAsset;
        [Header("无人机EngineFlare Render")]
        public SkinnedMeshRenderer m_droneFlareRender;
        [Header("环绕动画资源")]
        public GameObject m_aroundAnimatorAsset;
        [Header("集合动画资源")]
        public GameObject m_rallyAnimatorAsset;
        [Header("无人机队形资源")]
        public GameObject m_droneFormationAsset;
        [Header("发射期飞行速度曲线")]
        public AnimationCurve m_launchVelocityCurve;
        [Header("飞行速度(unity单位/s)")]
        public float m_flySpeed;
        public const string DroneAssetName = "DroneAsset";
        public const string DroneExplosionAssetName = "DroneExplosionAsset";
        public const string SelfDroneEngineFlareAssetName = "SelfDroneEngineFlareAsset";
        public const string FrienDroneEngineFlareAssetName = "FrienDroneEngineFlareAsset";
        public const string EnemyDroneEngineFlareAssetName = "EnemyDroneEngineFlareAsset";
        public const string AroundAnimatorAssetName = "AroundAnimatorAsset";
        public const string RallyAnimatorAssetName = "RallyAnimatorAsset";
        public const string DroneFormationAssetName = "DroneFormationAsset";
    }
}

