﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BulletMissileController : BulletObjectController, IBulletTargetProvider, ISpaceTargetProvider
    {
        protected BulletTargetProviderWarpper m_srcTarget;
        protected BulletMissileFlyController m_missileFlyCtrl;
        protected float m_missileFlySpeed;
        protected uint m_bulletProcessId;
        protected int m_bulletIndex;
        protected bool m_isForceExplosion;
        protected static TypeDNName m_missileFlyTypeDNName;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_StartBulletFly;
        private static DelegateBridge __Hotfix_SetSrcTarget;
        private static DelegateBridge __Hotfix_OnBulletDestroy;
        private static DelegateBridge __Hotfix_CalcBulletLifeTime;
        private static DelegateBridge __Hotfix_UpdateBulletFly;
        private static DelegateBridge __Hotfix_OnBulletHit;
        private static DelegateBridge __Hotfix_ForceBulletOnHit;
        private static DelegateBridge __Hotfix_ShowBulletEffectObject;
        private static DelegateBridge __Hotfix_OnHitTarget;
        private static DelegateBridge __Hotfix_OnMissileStartGuideFly;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.RegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.UnregisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetTransform;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetSize;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalHitPoint;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.OnBulletHit;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.IsTargetHasShield;
        private static DelegateBridge __Hotfix_RegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_UnregisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_GetTargetTransform;
        private static DelegateBridge __Hotfix_GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.IBulletTargetProvider.GetTargetScale;
        private static DelegateBridge __Hotfix_get_MissileFlySpeed;
        private static DelegateBridge __Hotfix_set_MissileFlySpeed;
        private static DelegateBridge __Hotfix_get_BulletProcessID;
        private static DelegateBridge __Hotfix_set_BulletProcessID;
        private static DelegateBridge __Hotfix_get_BulletIndex;
        private static DelegateBridge __Hotfix_set_BulletIndex;

        [MethodImpl(0x8000)]
        void IBulletTargetProvider.GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        bool IBulletTargetProvider.GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        Vector3 IBulletTargetProvider.GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        float IBulletTargetProvider.GetTargetScale()
        {
        }

        [MethodImpl(0x8000)]
        bool IBulletTargetProvider.IsTargetHasShield()
        {
        }

        [MethodImpl(0x8000)]
        void IBulletTargetProvider.OnBulletHit(Vector3 worldHitPoint, bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        float ISpaceTargetProvider.GetTargetSize()
        {
        }

        [MethodImpl(0x8000)]
        Transform ISpaceTargetProvider.GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.RegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.UnregisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        protected override float CalcBulletLifeTime()
        {
        }

        [MethodImpl(0x8000)]
        protected void ForceBulletOnHit()
        {
        }

        [MethodImpl(0x8000)]
        protected void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        protected Vector3 GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        protected Transform GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletDestroy()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletHit(Vector3 worldOnHitLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHitTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMissileStartGuideFly()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        public override void Reset()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSrcTarget(BulletTargetProviderWarpper srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowBulletEffectObject(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletFly()
        {
        }

        public float MissileFlySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public uint BulletProcessID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int BulletIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

