﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using BlackJack.Utils;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipNamedEffect_Invisible : SpaceShipNamedEffect
    {
        protected ShipNamedEffectType m_currEffectName;
        private DelayExecHelper m_delayExecHelper;
        protected bool m_isRegistLODLevelChangeEvent;
        protected const int InvisibleState_Charging = 1;
        protected const int InvisibleState_Start = 2;
        protected const int InvisibleState_End = 0;
        protected const int InvisibleState_ChagingInterrupt = 0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_StartShipInvisibleStart;
        private static DelegateBridge __Hotfix_StartShipInvisibleEnd;
        private static DelegateBridge __Hotfix_StartShipInvisibleCharging;
        private static DelegateBridge __Hotfix_StartShipInvisibleChargingInterrupt;
        private static DelegateBridge __Hotfix_PostOnLODLevelChanged;
        private static DelegateBridge __Hotfix_IsSelfShip;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_Invisible(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSelfShip()
        {
        }

        [MethodImpl(0x8000)]
        protected void PostOnLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipInvisibleCharging()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipInvisibleChargingInterrupt()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipInvisibleEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipInvisibleStart()
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect(ShipNamedEffectType effectName)
        {
        }
    }
}

