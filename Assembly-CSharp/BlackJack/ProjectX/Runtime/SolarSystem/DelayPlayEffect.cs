﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class DelayPlayEffect
    {
        public float delayTime;
        public GameObject effectObject;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

