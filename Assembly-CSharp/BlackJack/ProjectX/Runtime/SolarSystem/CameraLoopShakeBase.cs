﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CameraLoopShakeBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Func<float, float> <CalcLoopShakeFunc>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_get_CalcLoopShakeFunc;
        private static DelegateBridge __Hotfix_set_CalcLoopShakeFunc;
        private static DelegateBridge __Hotfix_GetAmbitude;
        private static DelegateBridge __Hotfix_GetConfigValue;

        [MethodImpl(0x8000)]
        public virtual float GetAmbitude()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetConfigValue(ConfigableConstId id)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Init()
        {
        }

        public Func<float, float> CalcLoopShakeFunc
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

