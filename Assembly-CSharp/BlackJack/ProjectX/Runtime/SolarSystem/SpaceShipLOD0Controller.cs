﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceShipLOD0Controller : SpaceShipLODController
    {
        protected Mesh m_toughGridMesh;
        protected Vector3[] m_toughGridVectors;
        protected Vector3[] m_toughGridNormals;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_GetOwnerSkinMeshRender;
        private static DelegateBridge __Hotfix_GetRealSlotsByLogicWeaponSlotIndex;
        private static DelegateBridge __Hotfix_GetRealSlotsBySuperWeaponSlotIndex;
        private static DelegateBridge __Hotfix_GetRealSlotsByNormalWeaponSlotIndex;
        private static DelegateBridge __Hotfix_GetShipWeaponScale;
        private static DelegateBridge __Hotfix_GetShipWeaponEffectScale;
        private static DelegateBridge __Hotfix_GetSuperWeaponScale;
        private static DelegateBridge __Hotfix_GetPrefabSuperWeaponDesc;
        private static DelegateBridge __Hotfix_GetPrefabNormalWeaponDesc;
        private static DelegateBridge __Hotfix_InitShipWeaponController;
        private static DelegateBridge __Hotfix_InitShipSuperWeaponController;
        private static DelegateBridge __Hotfix_InitShipEngineEffectController;
        private static DelegateBridge __Hotfix_GetPrefabShipDesc;

        [MethodImpl(0x8000)]
        public override void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        public override bool GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration)
        {
        }

        [MethodImpl(0x8000)]
        public override SkinnedMeshRenderer GetOwnerSkinMeshRender()
        {
        }

        [MethodImpl(0x8000)]
        public override PrefabNormalWeaponDesc GetPrefabNormalWeaponDesc()
        {
        }

        [MethodImpl(0x8000)]
        public override PrefabShipDesc GetPrefabShipDesc()
        {
        }

        [MethodImpl(0x8000)]
        public override PrefabSuperWeaponDesc GetPrefabSuperWeaponDesc()
        {
        }

        [MethodImpl(0x8000)]
        public override List<GameObject> GetRealSlotsByLogicWeaponSlotIndex(int groupIndex, int slotIndex, out bool isCreateVisibleObject)
        {
        }

        [MethodImpl(0x8000)]
        public override List<GameObject> GetRealSlotsByNormalWeaponSlotIndex(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override List<GameObject> GetRealSlotsBySuperWeaponSlotIndex(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public override float GetShipWeaponEffectScale()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetShipWeaponScale()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetSuperWeaponScale()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitShipEngineEffectController()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitShipSuperWeaponController()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitShipWeaponController()
        {
        }
    }
}

