﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletDroneFormationController : SpaceObjectTraceMoveController
    {
        protected float m_traceEndDistance;
        protected float m_traceLeftTime;
        protected List<SpaceObjectVelocityController> m_droneSlotVelocityCtrlList;
        protected PrefabDroneFormationDesc m_droneFormationDesc;
        protected static TypeDNName m_velociTypeDNName;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_StartTraceTarget;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_UpdateVelocity;
        private static DelegateBridge __Hotfix_GetSingleDroneFormationTransformByIndex;

        [MethodImpl(0x8000)]
        public Transform GetSingleDroneFormationTransformByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void StartTraceTarget(float traceTime, float baseVelocity, Transform target, float traceEndDistance)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateVelocity(float distance)
        {
        }
    }
}

