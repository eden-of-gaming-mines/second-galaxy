﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    [StructLayout(LayoutKind.Sequential)]
    public struct BulletLaunchInfo
    {
        public GameObject m_bulletEffectAsset;
        public GameObject m_onHitEffectAsset;
        public GameObject m_onHitEffectAssetAfterPenetration;
        public Transform m_launchLocation;
        public float m_bulletScale;
    }
}

