﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public interface ISceneAnimationCameraCtrlProvider
    {
        float GetCameraFov();
        Quaternion GetCameraRotation();
        Vector3 GetCameraWatchLocation();
        float GetCameraZOffset();
        bool IsEnableCinemachineBrain();
        bool IsRelocationCamera();
    }
}

