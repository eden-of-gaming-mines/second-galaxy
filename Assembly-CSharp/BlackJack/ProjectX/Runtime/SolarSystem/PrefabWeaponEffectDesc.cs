﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using System;
    using UnityEngine;

    [AddComponentMenu("SolarSystem/PrefabWeaponEffectDesc11")]
    public class PrefabWeaponEffectDesc : PrefabResourceContainerBase
    {
        [Header("开火特效")]
        public GameObject FireEffectAsset;
        [Header("子弹特效")]
        public GameObject BulletEffectAsset;
        [Header("命中特效")]
        public GameObject OnHitEffectAsset;
        [Header("充能特效")]
        public GameObject ChargingEffectAsset;
        [Header("Burst开火特效")]
        public GameObject BurstFireEffectAsset;
        [Header("Burst子弹特效")]
        public GameObject BurstBulletEffectAsset;
        [Header("Burst命中特效")]
        public GameObject BurstOnHitEffectAsset;
        [Header("Burst充能特效")]
        public GameObject BurstChargingEffectAsset;
        [Header("武器效果缩放")]
        public float WeaponEffectScale;
        public const string FireEffectAssetName = "FireEffectAsset";
        public const string OnHitEffectAssetName = "OnHitEffectAsset";
        public const string BulletEffectAssetName = "BulletEffectAsset";
        public const string ChargingEffectAssetName = "ChargingEffectAsset";
    }
}

