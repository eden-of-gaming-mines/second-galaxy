﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [AddComponentMenu("SolarSystem/PrefabShipExplosionEffectDesc")]
    public class PrefabShipExplosionEffectDesc : PrefabResourceContainerBase
    {
        [Header("延迟播放效果对象列表")]
        public List<DelayPlayEffect> DelayPlayEffectObjectList;
        [Header("延迟随机播放资源列表")]
        public List<GameObject> DelayPlayEffectAssetList;
        [Header("延迟效果的次数")]
        public int DelayPlayCount;
        [Header("延迟最小时间")]
        public float MinDelayTime;
        [Header("延迟最大时间")]
        public float MaxDelayTime;
        [Header("隐藏飞船模型时间")]
        public float HideShipModelTime;
        [Header("爆炸动画")]
        public Animator ExplosionAnimator;
    }
}

