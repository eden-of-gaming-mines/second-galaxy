﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Runtime;
    using BlackJack.ProjectX.Runtime.Timeline;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimation_CutSceneBase : SceneAnimationBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnNpcChatShow;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <AnimationClipConfigId>k__BackingField;
        protected ISceneAnimationPlayerController m_animationPlayer;
        protected ConfigDataSceneCameraAnimationClipInfo m_animationClipConfInfo;
        protected CutsceneShipActionHandler m_shipActionHandler;
        protected CutsceneStageDesc m_cutsceneStage;
        protected List<CutsceneActorDesc> m_cutsceneActorList;
        protected List<string> m_playingSoundNames;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_IsRelocationByActor;
        private static DelegateBridge __Hotfix_GetActorNameForReloaction;
        private static DelegateBridge __Hotfix_GetRelocationInfoByActor;
        private static DelegateBridge __Hotfix_IsCreateFakeActors;
        private static DelegateBridge __Hotfix_InitFakeActorsState;
        private static DelegateBridge __Hotfix_GetCameraZOffset;
        private static DelegateBridge __Hotfix_CreateClipAssetGo;
        private static DelegateBridge __Hotfix_GetPlayerType;
        private static DelegateBridge __Hotfix_OnSceneAnimationMessage;
        private static DelegateBridge __Hotfix_OnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_CreateFakeSpaceShip;
        private static DelegateBridge __Hotfix_GetCutsceneActorDescByName;
        private static DelegateBridge __Hotfix_GetLayerMaskForFakeActor;
        private static DelegateBridge __Hotfix_GetTargetShipCtrlsByActionActorNames;
        private static DelegateBridge __Hotfix_OnAnimationEvent_NPCChat;
        private static DelegateBridge __Hotfix_OnAnimationEvent_PlayAudio;
        private static DelegateBridge __Hotfix_OnTimelineShipAction;
        private static DelegateBridge __Hotfix_add_EventOnNpcChatShow;
        private static DelegateBridge __Hotfix_remove_EventOnNpcChatShow;
        private static DelegateBridge __Hotfix_get_AnimationClipConfigId;
        private static DelegateBridge __Hotfix_set_AnimationClipConfigId;

        public event Action EventOnNpcChatShow
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SceneAnimation_CutSceneBase(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public override void CreateClipAssetGo()
        {
        }

        [MethodImpl(0x8000)]
        protected override SpaceObjectControllerBase CreateFakeSpaceShip(string name, ClientSpaceShip clientShip, IDynamicAssetProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public override string GetActorNameForReloaction()
        {
        }

        [MethodImpl(0x8000)]
        public override float GetCameraZOffset()
        {
        }

        [MethodImpl(0x8000)]
        protected CutsceneActorDesc GetCutsceneActorDescByName(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual int GetLayerMaskForFakeActor()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual SceneAnimationPlayerType GetPlayerType()
        {
        }

        [MethodImpl(0x8000)]
        public override void GetRelocationInfoByActor(Transform root, Transform realActor, ref Vector3 pos, ref Quaternion rotate, bool IsInitRotationWithTarget)
        {
        }

        [MethodImpl(0x8000)]
        protected List<SpaceShipController> GetTargetShipCtrlsByActionActorNames(string actorName)
        {
        }

        [MethodImpl(0x8000)]
        public override void Init()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitFakeActorsState(IEnumerable<string> realActorNames)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsCreateFakeActors()
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsRelocationByActor()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAnimationEvent_NPCChat()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAnimationEvent_PlayAudio(object obj)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSceneAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSceneAnimationMessage(string msg, object obj)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTimelineShipAction(TimelineShipActionInfo actionInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override void Play(Transform target, float targetSize)
        {
        }

        [MethodImpl(0x8000)]
        public override void Release()
        {
        }

        [MethodImpl(0x8000)]
        public override void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float delayTime, Vector3 targetLocation)
        {
        }

        public int AnimationClipConfigId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

