﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ShipBuffEffect
    {
        protected SpaceShipController m_ownerShip;
        protected SpaceShipController m_sourceShip;
        protected string m_continueNamedEffectName;
        protected SpaceShipEffectBase m_continueEffect;
        protected SpaceShipEffectBase m_channelEffect;
        protected string m_detachEffectName;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_PauseLoopEffect;
        private static DelegateBridge __Hotfix_ResumeLoopEffect;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_StartAttachEffect;
        private static DelegateBridge __Hotfix_StartContinueEffect;
        private static DelegateBridge __Hotfix_StartChannelEffect;
        private static DelegateBridge __Hotfix_StartDetachEffect;
        private static DelegateBridge __Hotfix_get_ShipEffectCtrl;

        [MethodImpl(0x8000)]
        public ShipBuffEffect(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void PauseLoopEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void ResumeLoopEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void Start(LBBufBase buf, SpaceShipController buffSourceShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartAttachEffect(string attachEffectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartChannelEffect(string channelEffectName, LBBufBase buf)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartContinueEffect(string continueEffectName)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartDetachEffect(string detachEffectName)
        {
        }

        [MethodImpl(0x8000)]
        public void Stop()
        {
        }

        protected ShipEffectController ShipEffectCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

