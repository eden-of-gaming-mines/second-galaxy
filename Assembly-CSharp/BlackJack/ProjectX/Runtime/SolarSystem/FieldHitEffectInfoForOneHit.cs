﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    internal class FieldHitEffectInfoForOneHit : FieldHitEffectInfoBase
    {
        private float m_hitEffectPeriodTime;
        private int m_currFieldHitState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnHitOnce;
        private static DelegateBridge __Hotfix_IsFieldHitEffectFinished;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_ResetData;
        private static DelegateBridge __Hotfix_get_HitEffectPeriodTime;

        [MethodImpl(0x8000)]
        public FieldHitEffectInfoForOneHit(Vector3 position, float radius, float effectPeriodTime)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsFieldHitEffectFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void OnHitOnce()
        {
        }

        [MethodImpl(0x8000)]
        public override void ResetData()
        {
        }

        [MethodImpl(0x8000)]
        public override void Update(float deltaTime)
        {
        }

        public float HitEffectPeriodTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

