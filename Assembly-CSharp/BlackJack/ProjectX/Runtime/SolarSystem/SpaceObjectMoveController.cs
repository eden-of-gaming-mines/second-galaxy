﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceObjectMoveController : PrefabControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SpaceObjectMoveController> EventOnEndMoveFoward;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<SpaceObjectMoveController> EventOnEndTraceTarget;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SpaceObjectMoveController> EventOnEndFollowTarget;
        protected Transform m_target;
        protected MoveState m_moveState;
        protected float m_velocity;
        public float m_realVelocity;
        protected float m_velocityChangeRate;
        protected float m_alignSpeed;
        public float m_realAlignSpeed;
        protected float m_alignChangeRate;
        protected float m_moveForwardTime;
        protected float m_moveForwardLeftTime;
        protected float m_followTargetTime;
        protected float m_followTargetLeftTime;
        protected Vector3 m_lastPosition;
        protected const float m_longDistanceToTarget = 10f;
        protected const float m_nearDistanceToTarget = 1f;
        private static DelegateBridge __Hotfix_SetState;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateMoveFoward;
        private static DelegateBridge __Hotfix_UpdateTarceTarget;
        private static DelegateBridge __Hotfix_UpdateFollowTarget;
        private static DelegateBridge __Hotfix_MoveForward;
        private static DelegateBridge __Hotfix_RotateToFaceTarget;
        private static DelegateBridge __Hotfix_UpdateVolecity;
        private static DelegateBridge __Hotfix_UpdateAlignSpeed;
        private static DelegateBridge __Hotfix_SetVeloctiy;
        private static DelegateBridge __Hotfix_SetAlign;
        private static DelegateBridge __Hotfix_SetMoveForward;
        private static DelegateBridge __Hotfix_SetTraceTarget;
        private static DelegateBridge __Hotfix_SetFollowTarget;
        private static DelegateBridge __Hotfix_add_EventOnEndMoveFoward;
        private static DelegateBridge __Hotfix_remove_EventOnEndMoveFoward;
        private static DelegateBridge __Hotfix_add_EventOnEndTraceTarget;
        private static DelegateBridge __Hotfix_remove_EventOnEndTraceTarget;
        private static DelegateBridge __Hotfix_add_EventOnEndFollowTarget;
        private static DelegateBridge __Hotfix_remove_EventOnEndFollowTarget;

        public event Action<SpaceObjectMoveController> EventOnEndFollowTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SpaceObjectMoveController> EventOnEndMoveFoward
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SpaceObjectMoveController> EventOnEndTraceTarget
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void MoveForward()
        {
        }

        [MethodImpl(0x8000)]
        protected void RotateToFaceTarget()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetAlign(float align, float time = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFollowTarget(Transform target, float time = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMoveForward(Vector3 dir, float velocity, float time, float velocityChangeTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetState(MoveState moveState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTraceTarget(Transform target, float velocity, float alignSpeed, float velocityChangeTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetVeloctiy(float velocity, float time = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateAlignSpeed(float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateFollowTarget(float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateMoveFoward(float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateTarceTarget(float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateVolecity(float delayTime)
        {
        }

        public enum MoveState
        {
            Foward,
            TraceTarget,
            FollowTarget
        }
    }
}

