﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    [Serializable]
    public class BuildingNodeDisplaySwitcher : MonoBehaviour
    {
        [SerializeField]
        public NodeSwitchInfo[] m_switchList;
        private static DelegateBridge __Hotfix_SetProcess;

        [MethodImpl(0x8000)]
        public void SetProcess(float process)
        {
        }

        [Serializable]
        public class NodeSwitchInfo
        {
            public float m_leastStep;
            public float m_mostStep;
            public GameObject m_node;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

