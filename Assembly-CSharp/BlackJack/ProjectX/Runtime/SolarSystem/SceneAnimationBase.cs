﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SceneAnimationBase : ISceneAnimationCameraCtrlProvider
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSceneAnimationEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<List<SceneCameraAnimationClipFilterType>, List<string>, bool> EventOnSceneAnimationFilterSpaceObject;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnSceneAnimationDisableSceneLight;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool> EventOnDisableSceneCameras;
        protected Camera m_lensFlareCamera;
        protected Camera m_mainCamera;
        protected Transform m_bulletRoot;
        protected GameObject m_clipAsset;
        protected GameObject m_clipGo;
        protected Transform m_parent;
        protected bool m_isLookAtTarget;
        protected bool m_isFollowTarget;
        protected bool m_isStayOnLocation;
        protected bool m_disableSceneCameras;
        protected Dictionary<string, SpaceObjectControllerBase> m_fakeActorDict;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CreateClipAssetGo;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_DelayExecuteAfterPlay;
        private static DelegateBridge __Hotfix_SetCameraLookAtDirection;
        private static DelegateBridge __Hotfix_Stop;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_IsRelocationByActor;
        private static DelegateBridge __Hotfix_GetActorNameForReloaction;
        private static DelegateBridge __Hotfix_GetRelocationInfoByActor;
        private static DelegateBridge __Hotfix_SetClipAseet;
        private static DelegateBridge __Hotfix_SetLensFlareCamera;
        private static DelegateBridge __Hotfix_SetMainCamera;
        private static DelegateBridge __Hotfix_SetBulletRoot;
        private static DelegateBridge __Hotfix_SetLookAtTarget;
        private static DelegateBridge __Hotfix_SetFollowTarget;
        private static DelegateBridge __Hotfix_SetStayOnLocation;
        private static DelegateBridge __Hotfix_IsCreateFakeActors;
        private static DelegateBridge __Hotfix_CreateFakeActor;
        private static DelegateBridge __Hotfix_InitFakeActorsState;
        private static DelegateBridge __Hotfix_IsRelocationCamera;
        private static DelegateBridge __Hotfix_IsEnableCinemachineBrain;
        private static DelegateBridge __Hotfix_GetCameraZOffset;
        private static DelegateBridge __Hotfix_GetCameraWatchLocation;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_GetCameraFov;
        private static DelegateBridge __Hotfix_OnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_OnSceneAnimationFilterSpaceObject;
        private static DelegateBridge __Hotfix_DisableSceneLight;
        private static DelegateBridge __Hotfix_DisableSceneCameras;
        private static DelegateBridge __Hotfix_InitLocalizedString;
        private static DelegateBridge __Hotfix_CreateFakeSpaceShip;
        private static DelegateBridge __Hotfix_GetFakeActorByName;
        private static DelegateBridge __Hotfix_add_EventOnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_add_EventOnSceneAnimationFilterSpaceObject;
        private static DelegateBridge __Hotfix_remove_EventOnSceneAnimationFilterSpaceObject;
        private static DelegateBridge __Hotfix_add_EventOnSceneAnimationDisableSceneLight;
        private static DelegateBridge __Hotfix_remove_EventOnSceneAnimationDisableSceneLight;
        private static DelegateBridge __Hotfix_add_EventOnDisableSceneCameras;
        private static DelegateBridge __Hotfix_remove_EventOnDisableSceneCameras;

        public event Action<bool> EventOnDisableSceneCameras
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool> EventOnSceneAnimationDisableSceneLight
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSceneAnimationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<List<SceneCameraAnimationClipFilterType>, List<string>, bool> EventOnSceneAnimationFilterSpaceObject
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SceneAnimationBase(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CreateClipAssetGo()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateFakeActor(string actorName, ClientSpaceObject clientSpaceObject, IDynamicAssetProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual SpaceObjectControllerBase CreateFakeSpaceShip(string name, ClientSpaceShip clientShip, IDynamicAssetProvider provider)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void DelayExecuteAfterPlay()
        {
        }

        [MethodImpl(0x8000)]
        protected void DisableSceneCameras(bool disableSceneCameras)
        {
        }

        [MethodImpl(0x8000)]
        protected void DisableSceneLight(bool disalbeSceneLight)
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetActorNameForReloaction()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetCameraFov()
        {
        }

        [MethodImpl(0x8000)]
        public virtual Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public virtual Vector3 GetCameraWatchLocation()
        {
        }

        [MethodImpl(0x8000)]
        public virtual float GetCameraZOffset()
        {
        }

        [MethodImpl(0x8000)]
        protected SpaceObjectControllerBase GetFakeActorByName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void GetRelocationInfoByActor(Transform root, Transform realActor, ref Vector3 pos, ref Quaternion rotate, bool IsInitRotationWithTarget)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Init()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void InitFakeActorsState(IEnumerable<string> realActorNames)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLocalizedString(GameObject goRoot)
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsCreateFakeActors()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsEnableCinemachineBrain()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsRelocationByActor()
        {
        }

        [MethodImpl(0x8000)]
        public virtual bool IsRelocationCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnSceneAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnSceneAnimationFilterSpaceObject(List<SceneCameraAnimationClipFilterType> filterTypeList, List<string> filterStrList, bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Play(Transform target, float targetSize)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletRoot(Transform bulletRoot)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetCameraLookAtDirection(Vector3 dir)
        {
        }

        [MethodImpl(0x8000)]
        public void SetClipAseet(GameObject asset)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFollowTarget(bool isFollowTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLensFlareCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLookAtTarget(bool isLookAtTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMainCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStayOnLocation(bool isStayOnLocation)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Stop()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(float delayTime, Vector3 targetLocation)
        {
        }
    }
}

