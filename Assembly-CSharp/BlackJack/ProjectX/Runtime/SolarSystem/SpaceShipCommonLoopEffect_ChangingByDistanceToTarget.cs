﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect_ChangingByDistanceToTarget : SpaceShipCommonLoopEffect_ChangingByLerpParam01
    {
        private float m_distanceMax;
        private float m_distanceMin;
        private bool m_isParamIncreaseByDistance;
        private Func<SpaceShipController, SpaceShipController> m_targetGettingFunc;
        private SpaceShipController m_currTarget;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitEffectChaningData;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickForEffectTarget;
        private static DelegateBridge __Hotfix_StartEffectCtrl;
        private static DelegateBridge __Hotfix_GetLerpParam01;
        private static DelegateBridge __Hotfix_get_LerpParamVarySpeed;
        private static DelegateBridge __Hotfix_get_CurrTarget;
        private static DelegateBridge __Hotfix_set_CurrTarget;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect_ChangingByDistanceToTarget(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetLerpParam01()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEffectChaningData(float distanceMin, float distanceMax, bool isParamIncreaseByDistance, Func<SpaceShipController, SpaceShipController> targetGetter)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartEffectCtrl(ShipLoopEffectObjectController effect)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float deltaTime, uint solarSystemTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForEffectTarget()
        {
        }

        protected override float LerpParamVarySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private SpaceShipController CurrTarget
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

