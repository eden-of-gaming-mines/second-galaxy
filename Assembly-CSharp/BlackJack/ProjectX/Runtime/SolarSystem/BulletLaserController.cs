﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletLaserController : BulletObjectController
    {
        protected BulletTargetProviderWarpper m_bulletSrcTarget;
        protected Vector3 m_localBulletLaunchPosition;
        protected LineRenderer[] m_laserLineRenders;
        protected bool m_isFixedBulletEffectLODLevel;
        protected int m_fixedBulletEffectLODLevel;
        protected DateTime m_preOnHitOutTime;
        protected const float m_preOnHitDelayTime = 0.3f;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_StartBulletFly;
        private static DelegateBridge __Hotfix_StopBulletFly;
        private static DelegateBridge __Hotfix_SetBulletObjectScale;
        private static DelegateBridge __Hotfix_CalcBulletLifeTime;
        private static DelegateBridge __Hotfix_ShowBulletEffectObject;
        private static DelegateBridge __Hotfix_UpdateBulletFly;
        private static DelegateBridge __Hotfix_UpdateBulletOnHit;
        private static DelegateBridge __Hotfix_UpdateLaserEffectLocation;
        private static DelegateBridge __Hotfix_UpdateLaserOnHitEffectLocation;
        private static DelegateBridge __Hotfix_OnBulletEffectLODLevelChanged;
        private static DelegateBridge __Hotfix_GetLineRenderScaleWidth;
        private static DelegateBridge __Hotfix_SetFixedBulletEffectLODLevel;
        private static DelegateBridge __Hotfix_SetLocalBulletLaunchLocation;
        private static DelegateBridge __Hotfix_OnBulletTargetDestory;
        private static DelegateBridge __Hotfix_OnBulletDestroy;
        private static DelegateBridge __Hotfix_SetBulletSrcTarget;

        [MethodImpl(0x8000)]
        protected override float CalcBulletLifeTime()
        {
        }

        [MethodImpl(0x8000)]
        private float GetLineRenderScaleWidth(LineRenderer lineRender)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletDestroy()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletEffectLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBulletTargetDestory(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public override void Reset()
        {
        }

        [MethodImpl(0x8000)]
        protected override void SetBulletObjectScale(GameObject bulletObject)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBulletSrcTarget(BulletTargetProviderWarpper srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFixedBulletEffectLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocalBulletLaunchLocation(Vector3 localLaunchLocation)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowBulletEffectObject(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        public override void StopBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletFly()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateBulletOnHit()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateLaserEffectLocation()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateLaserOnHitEffectLocation()
        {
        }
    }
}

