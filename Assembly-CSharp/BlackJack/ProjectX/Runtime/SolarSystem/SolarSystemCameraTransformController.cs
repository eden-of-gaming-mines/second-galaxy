﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using JetBrains.Annotations;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemCameraTransformController : PrefabControllerBase
    {
        [AutoBind("./CameraPlatform", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraPlatform;
        [AutoBind("./CameraPlatform/YRotationRootForCamera", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_yRotationRootForCamera;
        [AutoBind("./CameraPlatform/YRotationRootForCamera/XRotationRootForCamera", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_xRotationRootForCamera;
        [AutoBind("./CameraPlatform/YRotationRootForCamera/XRotationRootForCamera/CameraDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraDummy;
        [AutoBind("./CameraRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraRoot;
        [AutoBind("./CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public TweenFactor m_cameraMovementTween;
        [AutoBind("./CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Animator m_cameraAnimator;
        private float m_maxZOffset;
        private float m_minZOffset;
        private float m_maxYOffset;
        private float m_minYOffset;
        private float m_maxZForYOffset;
        private float m_storedRotX;
        private float m_storedRotY;
        private bool m_isAutoUpdateOffset;
        private int m_autoUpdateOffsetMode;
        private bool m_isAutoUpdateRotate;
        private float m_cameraSlerpStartTime;
        private float m_cameraSlerpTimespan;
        private Quaternion m_cameraSlerpStartRotation;
        private float m_cameraOffsetSlerpStartTime;
        private float m_cameraOffsetSlerpTimespan;
        private float m_cameraOffsetStartValueDefault;
        private Vector3 m_cameraOffsetStartValueMode1;
        private float m_currCameraPlatformYOffset;
        private bool m_isCameraShakeLocked;
        private const float CameraExtraRotationAngleAxisX = 6f;
        private const string CameraAnimatorParamSuperBooster = "isSuperBoost";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCameraShakeAnimationUpdate;
        private Vector3 m_cameraOffset;
        private Quaternion m_cameraDiflection;
        private const float ReferenceDistance = 5f;
        private Vector3 m_offset;
        private Vector3 m_rotVelocity;
        private Vector3 m_rotAccelaration;
        public float m_returnCoefficient;
        public float m_maxVeloAbs;
        public float m_attenByDistance;
        private float m_extent;
        private float m_duration;
        public bool m_includeZrotate;
        private float m_disturb;
        private bool m_disturbIncludeZ;
        private Vector3 m_keepingOffset;
        public ShakeParams m_debugParam;
        private readonly List<ContinuouslyShakeParams> m_continuousShakeParamList;
        private ShakeParams[] m_paramsOfStatus;
        private readonly List<ShakeCoolDown> m_coolDown;
        private readonly List<OffsetMakerRemain> m_offsetMakerList;
        private static readonly float m_defaultCameraDistance = 3f;
        [CompilerGenerated]
        private static Func<float, float> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<KeyValuePair<int, ConfigDataCameraShakeLevelConfig>> <>f__am$cache1;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_GetCamera;
        private static DelegateBridge __Hotfix_UpdateCameraDockData;
        private static DelegateBridge __Hotfix_UpdateCameraOffsetDefaultMode;
        private static DelegateBridge __Hotfix_UpdateCameraOffsetMode1;
        private static DelegateBridge __Hotfix_UpdateCameraRotation;
        private static DelegateBridge __Hotfix_SetCameraRotation_1;
        private static DelegateBridge __Hotfix_SetCameraRotation_0;
        private static DelegateBridge __Hotfix_GetCameraRotation;
        private static DelegateBridge __Hotfix_GetCameraFoward;
        private static DelegateBridge __Hotfix_GetCameraLocalRotation;
        private static DelegateBridge __Hotfix_SetCameraLookAtDirection;
        private static DelegateBridge __Hotfix_SetCameraPlatformPosition;
        private static DelegateBridge __Hotfix_PlayCameraAnimator;
        private static DelegateBridge __Hotfix_SetCameraMinMaxZOffset;
        private static DelegateBridge __Hotfix_GetCameraMinZOffset;
        private static DelegateBridge __Hotfix_GetCameraMaxZOffset;
        private static DelegateBridge __Hotfix_SetCameraMinMaxYOffset;
        private static DelegateBridge __Hotfix_UpdateCameraOffset_0;
        private static DelegateBridge __Hotfix_UpdateCameraOffset_1;
        private static DelegateBridge __Hotfix_SetCameraOffset_0;
        private static DelegateBridge __Hotfix_SetCameraOffset_1;
        private static DelegateBridge __Hotfix_SetCameraOffset_2;
        private static DelegateBridge __Hotfix_SetCameraDummyLocalOffsetZ;
        private static DelegateBridge __Hotfix_ResetCameraDummyLocalOffset;
        private static DelegateBridge __Hotfix_SetCameraDummyLocalOffsetVec;
        private static DelegateBridge __Hotfix_GetCameraDummyLocalOffsetZ;
        private static DelegateBridge __Hotfix_GetCameraDummyLocalOffsetVec;
        private static DelegateBridge __Hotfix_SetCameraLocalOffsetZ;
        private static DelegateBridge __Hotfix_SetCameraLocalOffsetVec;
        private static DelegateBridge __Hotfix_GetCameraLocalOffsetZ;
        private static DelegateBridge __Hotfix_GetCameraLocalOffsetVec;
        private static DelegateBridge __Hotfix_GetCameraCurrYOffset;
        private static DelegateBridge __Hotfix_GetCameraMinOffsetY;
        private static DelegateBridge __Hotfix_GetCameraMaxOffsetY;
        private static DelegateBridge __Hotfix_GetCameraMaxZForYOffset;
        private static DelegateBridge __Hotfix_GetCameraFov;
        private static DelegateBridge __Hotfix_SetCameraFov;
        private static DelegateBridge __Hotfix_ResetAllCameraAniamtion;
        private static DelegateBridge __Hotfix_EnableCameraAnimator;
        private static DelegateBridge __Hotfix_EnableCinemachineBrain;
        private static DelegateBridge __Hotfix_UpdateCameraYOffset;
        private static DelegateBridge __Hotfix_StartCameraRotationSlerp;
        private static DelegateBridge __Hotfix_StopCameraRotationSlerp;
        private static DelegateBridge __Hotfix_StartCameraOffsetSlerpDefaultMode;
        private static DelegateBridge __Hotfix_StartCameraOffsetSlerpMode1;
        private static DelegateBridge __Hotfix_StopCameraOffsetSlerp;
        private static DelegateBridge __Hotfix_StoreCameraDummyRot;
        private static DelegateBridge __Hotfix_RestoreCameraDummyRot;
        private static DelegateBridge __Hotfix_StartCameraMoveAnimation;
        private static DelegateBridge __Hotfix_StopCameraMoveAnimation;
        private static DelegateBridge __Hotfix_IsCameraMoveAnimationFinished;
        private static DelegateBridge __Hotfix_GetCameraMoveAnimationFactor;
        private static DelegateBridge __Hotfix_add_EventOnCameraShakeAnimationUpdate;
        private static DelegateBridge __Hotfix_remove_EventOnCameraShakeAnimationUpdate;
        private static DelegateBridge __Hotfix_UpdateShakeContext;
        private static DelegateBridge __Hotfix_SetDiflection;
        private static DelegateBridge __Hotfix_SetOffset;
        private static DelegateBridge __Hotfix_CalcAccelaration;
        private static DelegateBridge __Hotfix_SetDisturb;
        private static DelegateBridge __Hotfix_StartIdle;
        private static DelegateBridge __Hotfix_AddContinuousDisturb;
        private static DelegateBridge __Hotfix_GetLock;
        private static DelegateBridge __Hotfix_GetContinuouslyShakeParamsFormMarkId;
        private static DelegateBridge __Hotfix_SetKeepingOffset;
        private static DelegateBridge __Hotfix_AddOffsetMethods;
        private static DelegateBridge __Hotfix_SetAttenuatShakeLevel;
        private static DelegateBridge __Hotfix_SetAttenuatShake;
        private static DelegateBridge __Hotfix_StartLoopShake_1;
        private static DelegateBridge __Hotfix_StartLoopShake_0;
        private static DelegateBridge __Hotfix_StopLoopShake;
        private static DelegateBridge __Hotfix_GetCoolDownLock;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SetCameraShakeLocked;

        [UsedImplicitly]
        public event Action EventOnCameraShakeAnimationUpdate
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AddContinuousDisturb(float amplify, float duration, Func<float, float> timeValueFunction = null, bool stopManually = false, object objectMark = null, bool includeZ = true)
        {
        }

        [MethodImpl(0x8000)]
        public void AddOffsetMethods(OffsetMaker method, float duration, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void Awake()
        {
        }

        [MethodImpl(0x8000)]
        private void CalcAccelaration()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableCameraAnimator(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableCinemachineBrain(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetCamera()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraCurrYOffset()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCameraDummyLocalOffsetVec()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraDummyLocalOffsetZ()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraFov()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCameraFoward()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCameraLocalOffsetVec()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraLocalOffsetZ()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetCameraLocalRotation()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraMaxOffsetY()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraMaxZForYOffset()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraMaxZOffset()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraMinOffsetY()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraMinZOffset()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraMoveAnimationFactor()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        private ContinuouslyShakeParams GetContinuouslyShakeParamsFormMarkId(object mark)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetCoolDownLock(object lockId, float coolDownTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private ShakeCoolDown GetLock(object keyId)
        {
        }

        [MethodImpl(0x8000)]
        private void Initialize()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCameraMoveAnimationFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCameraAnimator(string paramName, bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetAllCameraAniamtion()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetCameraDummyLocalOffset()
        {
        }

        [MethodImpl(0x8000)]
        public void RestoreCameraDummyRot()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttenuatShake(float disturb, float duration, bool includeZ = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAttenuatShakeLevel(int level = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraDummyLocalOffsetVec(Vector3 vec)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraDummyLocalOffsetZ(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraFov(float fov)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraLocalOffsetVec(Vector3 vec)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraLocalOffsetZ(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraLookAtDirection(Vector3 dir, float timeSpan = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraMinMaxYOffset(float minYOffset, float maxYOffset, float maxZForY)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraMinMaxZOffset(float minZOffset, float maxZOffset)
        {
        }

        [MethodImpl(0x8000)]
        public float SetCameraOffset(float deltaOffset, float timeSpan = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 SetCameraOffset(Vector3 deltaOffset, float timeSpan = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public float SetCameraOffset(float deltaOffset, float minZOffset, float maxZOffset, float minYOffset, float maxYOffset, float maxZForY, float timeSpan = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraPlatformPosition(Vector3 newPos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraRotation(Quaternion rotation)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraRotation(float rotateAngleForAxisX, float rotateAngleForAxisY)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraShakeLocked(bool isLocked)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDiflection()
        {
        }

        [MethodImpl(0x8000)]
        private void SetDisturb(ShakeParams param)
        {
        }

        [MethodImpl(0x8000)]
        public void SetKeepingOffset(Vector3 offset)
        {
        }

        [MethodImpl(0x8000)]
        private void SetOffset(Vector3 direction)
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraMoveAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraOffsetSlerpDefaultMode(float timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraOffsetSlerpMode1(float timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraRotationSlerp(float timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        private void StartIdle()
        {
        }

        [MethodImpl(0x8000)]
        public void StartLoopShake(Func<float, float> calcLoopShakeDisturbFunc, object mark, float amplitude = 1f, bool includeZ = false)
        {
        }

        [MethodImpl(0x8000)]
        public void StartLoopShake(Func<float, float> calcLoopShakeDisturbFunc, float duaration, float amplitude = 1f, bool includeZ = false)
        {
        }

        [MethodImpl(0x8000)]
        public void StopCameraMoveAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void StopCameraOffsetSlerp()
        {
        }

        [MethodImpl(0x8000)]
        public void StopCameraRotationSlerp()
        {
        }

        [MethodImpl(0x8000)]
        public void StopLoopShake(object mark, float duration = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void StoreCameraDummyRot()
        {
        }

        [MethodImpl(0x8000)]
        public void Tick(Vector3 targetForward)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCameraDockData(Vector3 targetForward)
        {
        }

        [MethodImpl(0x8000)]
        public float UpdateCameraOffset(float deltaOffset, float timeSpan = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public float UpdateCameraOffset(float deltaOffset, float minZOffset, float maxZOffset, float minYOffset, float maxYOffset, float maxZForY, float timeSpan = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCameraOffsetDefaultMode()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCameraOffsetMode1()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCameraRotation(float rotateAngleForAxisX, float rotateAngleForAxisY, float time = 0f)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCameraYOffset(float zOffset)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShakeContext(Vector3 direction)
        {
        }
    }
}

