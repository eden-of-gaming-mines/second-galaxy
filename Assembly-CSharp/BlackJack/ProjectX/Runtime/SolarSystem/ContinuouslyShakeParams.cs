﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;

    public class ContinuouslyShakeParams
    {
        public Func<float, float> m_disturbOfTime;
        public float m_amplify;
        public bool m_stopManually;
        public float m_duration;
        public float m_existedTime;
        public bool m_includeZrotate;
        public object m_manuallyStopMark;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

