﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect_ChangingBySelfShield : SpaceShipCommonLoopEffect_ChangingByLerpParam01
    {
        private float m_shieldPercentMin;
        private float m_shieldPercentMax;
        private bool m_isParamIncreaseByShield;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitEffectChangingData;
        private static DelegateBridge __Hotfix_GetLerpParam01;
        private static DelegateBridge __Hotfix_get_LerpParamVarySpeed;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect_ChangingBySelfShield(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetLerpParam01()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEffectChangingData(float shieldPercentMin, float shieldPercentMax, bool isParamIncreaseByShield)
        {
        }

        protected override float LerpParamVarySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

