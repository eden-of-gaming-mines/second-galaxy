﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public class PrefabShipDroneInfoDesc : MonoBehaviour
    {
        [Header("无人机环绕半径")]
        public float DroneAroundRadius;
        [Header("无人机环绕缩放修正")]
        protected Vector3 DroneAroundScaleMulit;
        [Header("无人机集结点根节点")]
        public Transform DroneRallyPointRoot;
        [Header("自己发射的无人机的飞行速度")]
        public float DroneLaunchSpeed;
    }
}

