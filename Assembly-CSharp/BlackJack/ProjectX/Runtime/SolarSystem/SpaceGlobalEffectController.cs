﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using SimpleLOD;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceGlobalEffectController : PrefabControllerBase
    {
        protected string m_effectName;
        protected string m_effectAssetName;
        protected DateTime m_effectLifeDateTime;
        protected LODSwitcher m_LODSwitcher;
        protected GameObject[] m_effectLODGameObjList;
        protected uint m_fixedMaxLODLevel;
        protected uint m_fixedMinLODLevel;
        protected bool m_isEffectActive;
        protected GameObject m_effectGameObj;
        private static DelegateBridge __Hotfix_CreateSpaceGlobalEffectObject;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Destroy;
        private static DelegateBridge __Hotfix_SetLifeTime;
        private static DelegateBridge __Hotfix_SetParent;
        private static DelegateBridge __Hotfix_SetEffectPos;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_RestEffect;
        private static DelegateBridge __Hotfix_OnEffectLODChanged;

        [MethodImpl(0x8000)]
        public static SpaceGlobalEffectController CreateSpaceGlobalEffectObject(GameObject asset, string assetPath, Camera camera, float lifeTime = 0f, uint fixedMaxLODLevel = 2, uint fixedMinLODLevel = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void Destroy()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void Init(GameObject asset, string assetPath, Camera LODCamera)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEffectLODChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected void Release()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void RestEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEffectPos(Vector3 worldPos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLifeTime(float lifeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetParent(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        public void StopEffect(float delayTime = 0f)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

