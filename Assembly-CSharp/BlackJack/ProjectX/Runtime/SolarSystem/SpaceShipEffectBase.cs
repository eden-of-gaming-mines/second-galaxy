﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceShipEffectBase
    {
        protected ISpaceTargetProvider m_target;
        protected Dictionary<string, GameObject> m_effectAssetDict;
        protected SpaceShipController m_ownerShip;
        protected Action<bool, string, PlaySoundOption, bool> m_playEffectSoundAction;
        protected Action<string, float> m_setSoundParamAction;
        protected bool m_isPauseState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_CollectAllDynamicResPath;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_PauseEffect;
        private static DelegateBridge __Hotfix_ResumeEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_CacheEffectAssets;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_CreateShipEffectObjectCtrls;
        private static DelegateBridge __Hotfix_CreateShipEffectObjectCtrl;
        private static DelegateBridge __Hotfix_GetEffectLODAsset;
        private static DelegateBridge __Hotfix_GetEffectRoot;
        private static DelegateBridge __Hotfix_GetEffectScaleMulit;
        private static DelegateBridge __Hotfix_PlayEffectSound;
        private static DelegateBridge __Hotfix_SetSoundParameter;

        [MethodImpl(0x8000)]
        public SpaceShipEffectBase(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected void CacheEffectAssets(Dictionary<string, GameObject> effectAssets)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void CollectAllDynamicResPath(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        protected T CreateShipEffectObjectCtrl<T>(GameObject asset, string assetPath, ISpaceTargetProvider target, float lifeTime = 0f, bool isNeedPlaySound = true) where T: ShipEffectObjectBase
        {
        }

        [MethodImpl(0x8000)]
        protected List<T> CreateShipEffectObjectCtrls<T>(Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target, bool isNeedPlaySound = true) where T: ShipEffectObjectBase
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetEffectLODAsset(PrefabShipEffectDesc effectDesc)
        {
        }

        [MethodImpl(0x8000)]
        protected Transform GetEffectRoot(PrefabShipEffectDesc effectDesc)
        {
        }

        [MethodImpl(0x8000)]
        protected float GetEffectScaleMulit(PrefabShipEffectDesc effectDesc)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void PauseEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void PlayEffectSound(bool enable, string soundName, PlaySoundOption playoOption, bool isLoop)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void Release()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void ResumeEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetSoundParameter(string paramName, float value)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StartEffect(Dictionary<string, GameObject> effectAssets, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void StopEffect()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Tick(float deltaTime, uint solarSystemTime)
        {
        }
    }
}

