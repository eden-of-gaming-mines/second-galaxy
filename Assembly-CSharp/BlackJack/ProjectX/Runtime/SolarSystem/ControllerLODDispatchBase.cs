﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using SimpleLOD;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ControllerLODDispatchBase : PrefabControllerBase
    {
        protected LODSwitcher m_LODSwitcher;
        protected ClientSpaceObject m_clientSpaceObject;
        protected Camera m_camera;
        protected IDynamicAssetProvider m_dynamicAssetProvider;
        protected int m_currLODLevel;
        protected ILODController m_currLODController;
        protected ILODController[] m_LODCtrls;
        protected List<ControllerDesc> m_LODCtrlDescs;
        protected List<PrefabControllerBase> m_cacheCtrls;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnBeforeLODLevelChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnPostLODLevelChanged;
        protected int m_fixedMaxLODLevel;
        protected int m_fixedMinLODLevel;
        public GameObject m_LOD0Root;
        public GameObject m_LOD1Root;
        public GameObject m_LOD2Root;
        public Transform m_unusedLODRoot;
        private const string UnusedNode = "UnusedNode";
        private static DelegateBridge __Hotfix_InitLODDispatch;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_ComputeLODLevel;
        private static DelegateBridge __Hotfix_InitAllLODRoot;
        private static DelegateBridge __Hotfix_SetLODLevel;
        private static DelegateBridge __Hotfix_GetCurrLODLevel;
        private static DelegateBridge __Hotfix_GetSuitableLODLevel;
        private static DelegateBridge __Hotfix_GetLODController;
        private static DelegateBridge __Hotfix_CreateLODObject;
        private static DelegateBridge __Hotfix_GetLODPrefab;
        private static DelegateBridge __Hotfix_GetLODObjectPrefabNameByLODLevel;
        private static DelegateBridge __Hotfix_GetLODRootByLODLevel;
        private static DelegateBridge __Hotfix_GetLODGameObjectName;
        private static DelegateBridge __Hotfix_CreateLODController;
        private static DelegateBridge __Hotfix_GetLODCtrlsDescByLODLevel;
        private static DelegateBridge __Hotfix_InitLODController;
        private static DelegateBridge __Hotfix_OnLODLevelChanged;
        private static DelegateBridge __Hotfix_BeforeLODLevelChanged;
        private static DelegateBridge __Hotfix_PostLODLevelChanged;
        private static DelegateBridge __Hotfix_GetMaxLODLevel;
        private static DelegateBridge __Hotfix_GetMinLODLevel;
        private static DelegateBridge __Hotfix_GetUnusedLODRoot;
        private static DelegateBridge __Hotfix_get_CurrLODController;
        private static DelegateBridge __Hotfix_add_EventOnBeforeLODLevelChanged;
        private static DelegateBridge __Hotfix_remove_EventOnBeforeLODLevelChanged;
        private static DelegateBridge __Hotfix_add_EventOnPostLODLevelChanged;
        private static DelegateBridge __Hotfix_remove_EventOnPostLODLevelChanged;
        private static DelegateBridge __Hotfix_get_LOD0CtrlsDesc;
        private static DelegateBridge __Hotfix_get_LOD1CtrlsDesc;
        private static DelegateBridge __Hotfix_get_LOD2CtrlsDesc;

        public event Action<int> EventOnBeforeLODLevelChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnPostLODLevelChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected virtual void BeforeLODLevelChanged(int oldLODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void ComputeLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        public virtual ILODController CreateLODController(GameObject LODGameObject, int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public ILODController CreateLODObject(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        public ILODController GetLODController(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual List<ControllerDesc> GetLODCtrlsDescByLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetLODGameObjectName()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetLODObjectPrefabNameByLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual GameObject GetLODPrefab(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetLODRootByLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public int GetMaxLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        public int GetMinLODLevel()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuitableLODLevel(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected Transform GetUnusedLODRoot()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitAllLODRoot()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitLODController(ILODController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void InitLODDispatch(ClientSpaceObject clientObject, IDynamicAssetProvider dynamicAssetProvider, Camera camera, int fixedMaxLODLevel = 2, int fixedMinLODLevel = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void PostLODLevelChanged(int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetLODLevel(int LODLevel)
        {
        }

        public virtual ILODController CurrLODController
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected virtual ControllerDesc[] LOD0CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected virtual ControllerDesc[] LOD1CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected virtual ControllerDesc[] LOD2CtrlsDesc
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

