﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipFieldController : MonoBehaviour
    {
        private Dictionary<int, ShipFieldEffectController> m_fieldObjDict;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_AddNewFieldEffect;
        private static DelegateBridge __Hotfix_IsAnyFieldActivate;
        private static DelegateBridge __Hotfix_ActivateField;
        private static DelegateBridge __Hotfix_InactivateField;
        private static DelegateBridge __Hotfix_GetGlobalHitPointByRay;
        private static DelegateBridge __Hotfix_DoFieldOnHitOnce;
        private static DelegateBridge __Hotfix_DoFieldOnContinuousHitStart;
        private static DelegateBridge __Hotfix_DoFieldOnContinuousHitUpdate;
        private static DelegateBridge __Hotfix_DoFieldOnContinuousHitFinished;
        private static DelegateBridge __Hotfix_GetCurrActiveFieldCtrl;

        [MethodImpl(0x8000)]
        public void ActivateField(FieldType fieldType)
        {
        }

        [MethodImpl(0x8000)]
        public void AddNewFieldEffect(FieldType fieldType, ShipFieldEffectController effectCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public void DoFieldOnContinuousHitFinished(int dataId)
        {
        }

        [MethodImpl(0x8000)]
        public void DoFieldOnContinuousHitStart(int dataId, Vector3 position, float radius, float effectTimePeriod)
        {
        }

        [MethodImpl(0x8000)]
        public void DoFieldOnContinuousHitUpdate(int dataId, ShipContinuousHitFieldUpdateDataInfo updateData)
        {
        }

        [MethodImpl(0x8000)]
        public void DoFieldOnHitOnce(Vector3 position, float radius, float effectTimePeriod)
        {
        }

        [MethodImpl(0x8000)]
        protected ShipFieldEffectController GetCurrActiveFieldCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public void GetGlobalHitPointByRay(Ray ray, float testDistance, out Vector3 hitPoint, out Vector3 hitPointNormal)
        {
        }

        [MethodImpl(0x8000)]
        public void InactivateField(FieldType fieldType)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnyFieldActivate()
        {
        }
    }
}

