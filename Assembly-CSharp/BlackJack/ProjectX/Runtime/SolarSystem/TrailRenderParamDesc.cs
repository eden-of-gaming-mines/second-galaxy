﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class TrailRenderParamDesc : MonoBehaviour
    {
        [Header("TrailRender宽度")]
        public float m_trailRenderWidth;
        private TrailRenderer m_trailRender;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_GetGlobalScale;
        private static DelegateBridge __Hotfix_get_TrailRender;
        private static DelegateBridge __Hotfix_set_TrailRender;

        [MethodImpl(0x8000)]
        protected float GetGlobalScale(Transform tf)
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        protected TrailRenderer TrailRender
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

