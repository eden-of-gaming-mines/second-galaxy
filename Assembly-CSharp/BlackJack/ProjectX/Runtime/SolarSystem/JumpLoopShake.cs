﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class JumpLoopShake : CameraLoopShakeBase
    {
        private static readonly int m_sampleCount;
        private static readonly int m_intervalCount;
        public static readonly float[] m_eigenPoints;
        private static JumpLoopShake m_instance;
        [CompilerGenerated]
        private static Func<float, float> <>f__mg$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetInstance;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_GetDisturbByPoint;
        private static DelegateBridge __Hotfix_Init;

        [MethodImpl(0x8000)]
        private static float GetDisturbByPoint(float pointValue)
        {
        }

        [MethodImpl(0x8000)]
        public static JumpLoopShake GetInstance()
        {
        }

        [MethodImpl(0x8000)]
        public override void Init()
        {
        }

        [MethodImpl(0x8000)]
        private static void Initialize()
        {
        }
    }
}

