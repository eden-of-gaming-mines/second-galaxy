﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect_ChangingBySelfEnergy : SpaceShipCommonLoopEffect_ChangingByLerpParam01
    {
        private float m_energyPercentMin;
        private float m_energyPercentMax;
        private bool m_isParamIncreaseByEnergy;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitEffectChangingData;
        private static DelegateBridge __Hotfix_GetLerpParam01;
        private static DelegateBridge __Hotfix_get_LerpParamVarySpeed;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect_ChangingBySelfEnergy(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetLerpParam01()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEffectChangingData(float energyPercentMin, float energyPercentMax, bool isParamIncreaseByEnergy)
        {
        }

        protected override float LerpParamVarySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

