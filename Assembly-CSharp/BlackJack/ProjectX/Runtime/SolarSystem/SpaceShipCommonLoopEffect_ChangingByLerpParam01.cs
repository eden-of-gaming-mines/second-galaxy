﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect_ChangingByLerpParam01 : SpaceShipCommonLoopEffect
    {
        protected float m_lastTickParamValue;
        private float m_currParamTargetValue;
        private float m_currParamValue;
        private bool m_isNeedReleaseEffect;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_TickForEffectChanging;
        private static DelegateBridge __Hotfix_ReleaseEffectCtrl;
        private static DelegateBridge __Hotfix_StartCurrLODLevelEffect;
        private static DelegateBridge __Hotfix_StartEffectCtrl;
        private static DelegateBridge __Hotfix_GetLerpParam01;
        private static DelegateBridge __Hotfix_get_LerpParamVarySpeed;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect_ChangingByLerpParam01(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual float GetLerpParam01()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ReleaseEffectCtrl(ShipLoopEffectObjectController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartCurrLODLevelEffect(Dictionary<string, GameObject> effectAssets, ISpaceTargetProvider target, int LODLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartEffectCtrl(ShipLoopEffectObjectController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float deltaTime, uint solarSystemTime)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForEffectChanging(float deltaTime)
        {
        }

        protected virtual float LerpParamVarySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

