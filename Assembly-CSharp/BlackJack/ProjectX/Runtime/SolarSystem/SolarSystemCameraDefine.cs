﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;

    public static class SolarSystemCameraDefine
    {
        public const float CameraNearPlaneDistance = 0.3f;
        public const float CameraZOffsetMinMulti = 1f;
        public const float CameraZOffsetMaxMulti = 35f;
        public const float PlatformMaxZOffsetForJumpMulti = 15f;
        public const float PlatformDefaultZOffsetForJumpMulti = 5f;
        public const float PlatformYOffsetMinMulti = 0f;
        public const float PlatformYOffsetMaxMulti = 1.5f;
        public const float PlatformZForYOffsetMaxMulti = 10f;
        public const float DefaultInitCameraXDegree = 5f;
        public const float DefaultInitCameraYDegree = 0f;
        public const float LeaveStationInitCameraXDegree = -15f;
        public const float LeaveStationInitCameraYDegree = 172f;
        public const float CameraTraceTargetDelayTime = 1.5f;
    }
}

