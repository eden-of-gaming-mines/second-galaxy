﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceDustController : MonoBehaviour
    {
        public ParticleSystemRenderer m_particleRender;
        public ParticleSystem m_particleSystem;
        public double m_dustSpeedScaleMin;
        public double m_dustSpeedScaleMax;
        public double m_dustVelocityMin;
        public double m_dustVelocityMax;
        [Range(0f, 100f), Header("速度线达到最大强度的速度百分比")]
        public double SpeedRatioForJumpingMaxDustIntensity;
        [Header("跃迁最大速度线强度"), Range(0f, 100f)]
        public double MaxDustIntensityOnJumping;
        [Header("巡航最大速度线强度"), Range(0f, 100f)]
        public double MaxDustIntensityOnNormalCruise;
        [Range(0f, 100f), Header("推进器额外增加的速度线强度")]
        public double MaxDustIntensityForEquipExtra;
        private float INIT_OFFSET_Z;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_UpdateSpaceDust;
        private static DelegateBridge __Hotfix_UpdateParticle;
        private static DelegateBridge __Hotfix_UpdateVelocity;
        private static DelegateBridge __Hotfix_CalcDustEffectParamsOnNormalCruise;
        private static DelegateBridge __Hotfix_CalcDustEffectParamsOnJumping;

        [MethodImpl(0x8000)]
        public void Awake()
        {
        }

        [MethodImpl(0x8000)]
        protected double CalcDustEffectParamsOnJumping(double currSpeed, double defaultMaxSpeed)
        {
        }

        [MethodImpl(0x8000)]
        protected double CalcDustEffectParamsOnNormalCruise(double currSpeed, double defaultMaxSpeed)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateParticle(float speedScale)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpaceDust(double currSpeed, double currMaxSpeed, double defaultMaxSpeed, double jumpMaxSpeed, bool isJumping, bool isBooster)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateVelocity(float value)
        {
        }
    }
}

