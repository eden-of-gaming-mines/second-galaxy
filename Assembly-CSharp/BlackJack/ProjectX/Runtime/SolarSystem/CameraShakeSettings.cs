﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    [Serializable]
    public class CameraShakeSettings : ScriptableObject
    {
        [SerializeField]
        public ShakeParams[] m_shakeParamsArray;
        public static string m_defaultClientPrefPath = "Assets/GameProject/Resources/ShakeParamsConfig";
    }
}

