﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface ISpaceShipController
    {
        ShipEngineEffectController GetShipEngineEffectController();
        IShipSuperWeaponController GetShipSuperWeaponController();
        IShipWeaponController GetShipWeaponController();
        void StartPlayShipAnimation(string animaionParamName, object animaionParamValue, bool isTrigger = false);
        void StartShipExplosion();
        void UpdateShipMoveState(Vector3 location, Vector3 rotation, double currSpeed, double currMaxSpeed, double defaultMaxSpeed, double jumpMaxSpeed, bool isJumping, bool isBooster, bool isGridChanged);
    }
}

