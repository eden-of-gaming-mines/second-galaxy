﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ShipBuffEffectController : PrefabControllerBase
    {
        protected Dictionary<uint, List<ShipBuffEffect>> m_buffEffctDict;
        protected Dictionary<int, List<KeyValuePair<uint, ShipBuffEffect>>> m_groupId2BuffEffectDict;
        protected Dictionary<uint, int> m_bufInsId2GroupIdDict;
        protected SpaceShipController m_ownerShip;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_CollectAllDynamicResPathByBuffInfo;
        private static DelegateBridge __Hotfix_AttachShipBuff;
        private static DelegateBridge __Hotfix_AttachShipBuffDuplicate;
        private static DelegateBridge __Hotfix_DetachShipBuff;
        private static DelegateBridge __Hotfix_ClearShipBuff;
        private static DelegateBridge __Hotfix_TryStopLastSameGroupBufLoopEffect;
        private static DelegateBridge __Hotfix_TryStartLastSameGroupBufLoopEffect;

        [MethodImpl(0x8000)]
        public void AttachShipBuff(LBBufBase buf, SpaceShipController buffSourceShip)
        {
        }

        [MethodImpl(0x8000)]
        public void AttachShipBuffDuplicate(LBBufBase buf, List<SpaceShipController> sourceShipList)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearShipBuff()
        {
        }

        [MethodImpl(0x8000)]
        public void CollectAllDynamicResPathByBuffInfo(LBBufBase buf, List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void DetachShipBuff(uint bufInsId)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected bool TryStartLastSameGroupBufLoopEffect(uint bufInsId)
        {
        }

        [MethodImpl(0x8000)]
        protected bool TryStopLastSameGroupBufLoopEffect(LBBufBase buf, out int bufEffectGroupId)
        {
        }
    }
}

