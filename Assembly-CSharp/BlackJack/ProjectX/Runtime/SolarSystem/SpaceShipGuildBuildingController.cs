﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceShipGuildBuildingController : PrefabControllerBase
    {
        private SpaceShipController m_ownerShip;
        private GameObject m_shipObject;
        private Animator m_shipAnimator;
        private Material m_shipMaterial;
        private string m_currAnimatorParamName;
        private GuildBuildingStatus m_buildingStatus;
        private GuildBattleStatus m_buildingBattleStatus;
        private DateTime m_currStateStartTime;
        private DateTime m_currStateEndTime;
        private float m_currStateLifeTime;
        private const string AnimatorStateUnableAttack = "Custom1";
        private const string AnimatorStateReinforcement = "Custom2";
        private const string AnimatorStateOffLine = "Custom3";
        private const string AnimatorStateDeclaration = "Custom4";
        public BuildingNodeDisplaySwitcher m_switcher;
        private static DelegateBridge __Hotfix_Init_0;
        private static DelegateBridge __Hotfix_Init_1;
        private static DelegateBridge __Hotfix_SetGuildBuildingInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetBuildingAnimatorInfoByBuildingInfo;
        private static DelegateBridge __Hotfix_PlayShipAnimator;
        private static DelegateBridge __Hotfix_SetGuildBuildingDeployProcess;

        [MethodImpl(0x8000)]
        private void GetBuildingAnimatorInfoByBuildingInfo(GuildBuildingStatus buildingState, GuildBattleStatus buildingBattleState, out string buildingAnimatorName, out int buildingAnimatorParam)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(GameObject shipObject)
        {
        }

        [MethodImpl(0x8000)]
        private void PlayShipAnimator(string animatorParamName, int animatorParamValue)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildBuildingDeployProcess(float process)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildBuildingInfo(GuildBuildingStatus buildingStatus, GuildBattleStatus buildingBattleStatus, DateTime currStateStartTime, DateTime currStateEndTime)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

