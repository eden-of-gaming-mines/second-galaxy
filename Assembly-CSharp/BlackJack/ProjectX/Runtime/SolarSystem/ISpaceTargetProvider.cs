﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public interface ISpaceTargetProvider
    {
        float GetTargetSize();
        Transform GetTargetTransform();
        void RegisterTargetDestroyEvent(Action<object> onTargetDestroy);
        void UnregisterTargetDestroyEvent(Action<object> onTargetDestroy);
    }
}

