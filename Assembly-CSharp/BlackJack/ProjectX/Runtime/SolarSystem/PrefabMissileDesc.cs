﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public class PrefabMissileDesc : MonoBehaviour
    {
        [Header("导弹局部sin运动的周期")]
        public float m_cycleTime;
        [Header("导弹局部sin运动的振幅")]
        public float m_amplitude;
        [Header("发射速度参数A(speed = A*scale +B)")]
        public float m_launchSpeedParamA;
        [Header("发射速度参数B")]
        public float m_launchSpeedParamB;
        [Header("发射时间")]
        public float m_launchTime;
    }
}

