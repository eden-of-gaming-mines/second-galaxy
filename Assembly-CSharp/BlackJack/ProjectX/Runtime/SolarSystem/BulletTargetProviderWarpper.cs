﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BulletTargetProviderWarpper : IBulletTargetProvider, ISpaceTargetProvider
    {
        protected Action<object> m_onTargetDestroyAction;
        protected BulletTargetType m_bulletTargetType;
        protected Vector3 m_localTargetOnHitPosition;
        protected Vector3 m_worldTargetFinalPosition;
        protected bool m_isHit;
        protected IBulletTargetProvider m_srcTarget;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge _c__Hotfix_ctor_2;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge __Hotfix_SetTarget_1;
        private static DelegateBridge __Hotfix_SetTarget_0;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_IsTheSameWithSrcTarget;
        private static DelegateBridge __Hotfix_AllocOnHitPos;
        private static DelegateBridge __Hotfix_GetWorldTargetOnHitPosition;
        private static DelegateBridge __Hotfix_GetWorldTargetLocation;
        private static DelegateBridge __Hotfix_GetLocalTargetOnHitPosition;
        private static DelegateBridge __Hotfix_IsTargetDestoryed;
        private static DelegateBridge __Hotfix_RegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_UnregisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_GetTargetTransform;
        private static DelegateBridge __Hotfix_GetTargetSize;
        private static DelegateBridge __Hotfix_GetLocalTargetAimPointForHit;
        private static DelegateBridge __Hotfix_GetLocalHitPoint;
        private static DelegateBridge __Hotfix_GetLocalHitPointAfterPenetration;
        private static DelegateBridge __Hotfix_OnBulletHit;
        private static DelegateBridge __Hotfix_GetTargetScale;
        private static DelegateBridge __Hotfix_IsTargetHasShield;
        private static DelegateBridge __Hotfix_OnBulletTargetDestory;

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper()
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper(IBulletTargetProvider srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        public BulletTargetProviderWarpper(Vector3 worldPos)
        {
        }

        [MethodImpl(0x8000)]
        public void AllocOnHitPos(Vector3 bulletWorldLocation, bool isHit)
        {
        }

        [MethodImpl(0x8000)]
        public void GetLocalHitPoint(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetLocalHitPointAfterPenetration(Vector3 worldBulletLocation, Vector3 localAimPoint, bool isHit, out Vector3 localHitPoint, out Vector3 localHitPointNormal, out Vector3 localHitPointAfterPenetration, out Vector3 localHitPointNormalAfterPenetration, out bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLocalTargetAimPointForHit()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLocalTargetOnHitPosition()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetScale()
        {
        }

        [MethodImpl(0x8000)]
        public float GetTargetSize()
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetWorldTargetLocation()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetWorldTargetOnHitPosition()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTargetDestoryed()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTargetHasShield()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTheSameWithSrcTarget(IBulletTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBulletHit(Vector3 worldHitPoint, bool isHitOnShield)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBulletTargetDestory(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        public void Release()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTarget(IBulletTargetProvider srcTarget)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTarget(Vector3 worldPos)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        protected enum BulletTargetType
        {
            Transform,
            Position
        }
    }
}

