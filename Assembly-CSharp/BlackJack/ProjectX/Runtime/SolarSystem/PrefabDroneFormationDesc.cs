﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System.Collections.Generic;
    using UnityEngine;

    public class PrefabDroneFormationDesc : MonoBehaviour
    {
        [Header("无人机位列表")]
        public List<GameObject> m_droneSlotList;
        [Header("队形移动时的速度曲线")]
        public AnimationCurve m_velocityCurve;
    }
}

