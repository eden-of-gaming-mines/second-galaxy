﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipEngineEffectController : PrefabControllerBase
    {
        protected PrefabShipEngineEffectDesc m_engineEffectDesc;
        private static MaterialPropertyBlock s_materialPropertyBlock;
        private static int s_ColorID;
        private static int s_FlameSpeedID;
        private static int s_FlameIntensityID;
        private static int s_FlareIntensityID;
        protected bool m_isTrailEnable;
        protected bool m_isEngineBoosterOpen;
        protected bool m_isDuringEngineBoosterFade;
        protected float m_engineBoosterFadeLeftTime;
        protected float m_currEngineBoosterFlameIntensity;
        protected float m_currEngineBoosterFlameUVTiling;
        protected float m_currEngineBoosterFlareIntensity;
        protected const float EngineBoosterLifeTime = 8f;
        protected const float EngineBoosterFadeOutTime = 1.25f;
        protected const float EngineBoosterFlameIntensityFadeSpeed = 0.8f;
        protected const float EngineBoosterFlameUVTilingFadeSpeed = 0.4f;
        protected const float EngineBoosterFlameIntensityMax = 1f;
        protected const float EngineBoosterFlareIntensityFadeSpeed = 2f;
        protected const float EngineBoosterFlareIntensityMax = 2.5f;
        protected const float EngineBoosterFlameUVTilingMin = 0.5f;
        protected const float EngineBoosterFlameUVTilingMax = 1f;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetCamera;
        private static DelegateBridge __Hotfix_ClearAllTrail;
        private static DelegateBridge __Hotfix_UpdateEngineEffect;
        private static DelegateBridge __Hotfix_SetActiveTrail;
        private static DelegateBridge __Hotfix_UpdateShipTrail;
        private static DelegateBridge __Hotfix_SetFlareActive;
        private static DelegateBridge __Hotfix_PlayEngineBoosterEffect;
        private static DelegateBridge __Hotfix_StartEngineBooster;
        private static DelegateBridge __Hotfix_StopEngineBooster;
        private static DelegateBridge __Hotfix_StartEngineBoosterFade;
        private static DelegateBridge __Hotfix_TickEngineBoosterFadeState;
        private static DelegateBridge __Hotfix_GetExtraFlameIntensityParamFromEngineBooster;
        private static DelegateBridge __Hotfix_GetExtraFlareIntensityParamFromEngineBooster;
        private static DelegateBridge __Hotfix_SetAllEngineEffectVisible;
        private static DelegateBridge __Hotfix_CalcEngineEffectParamsOnNormalCruise;
        private static DelegateBridge __Hotfix_CalcEngineEffectParamsOnJumping;

        [MethodImpl(0x8000)]
        protected double CalcEngineEffectParamsOnJumping(double currSpeed, double defaultMaxSpeed)
        {
        }

        [MethodImpl(0x8000)]
        protected double CalcEngineEffectParamsOnNormalCruise(double currSpeed, double defaultMaxSpeed)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearAllTrail()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetExtraFlameIntensityParamFromEngineBooster()
        {
        }

        [MethodImpl(0x8000)]
        protected float GetExtraFlareIntensityParamFromEngineBooster()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayEngineBoosterEffect(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void SetActiveTrail(bool active)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllEngineEffectVisible(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFlareActive(bool active)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartEngineBooster()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartEngineBoosterFade()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopEngineBooster()
        {
        }

        [MethodImpl(0x8000)]
        protected void TickEngineBoosterFadeState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEngineEffect(double currSpeed, double currMaxSpeed, double defaultMaxSpeed, double jumpMaxSpeed, bool isJumping)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipTrail(double currSpeed, bool isJumping, bool isGridChanged, bool isSelfShip = false)
        {
        }
    }
}

