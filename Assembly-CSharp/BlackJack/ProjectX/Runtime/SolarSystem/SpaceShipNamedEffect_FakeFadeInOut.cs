﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class SpaceShipNamedEffect_FakeFadeInOut : SpaceShipNamedEffect
    {
        protected TimeLerpExecutor m_blinkFadeLerp;
        protected const float BlinkFadeLifeTime = 2f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_IsNeedEffectAsset;
        private static DelegateBridge __Hotfix_StartEffect;
        private static DelegateBridge __Hotfix_StopEffect;
        private static DelegateBridge __Hotfix_StartShipFakeFadeIn;
        private static DelegateBridge __Hotfix_StartShipFakeFadeOut;
        private static DelegateBridge __Hotfix_get_IsEffectActive;
        private static DelegateBridge __Hotfix_set_IsEffectActive;

        [MethodImpl(0x8000)]
        public SpaceShipNamedEffect_FakeFadeInOut(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        public override bool IsNeedEffectAsset(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public override void StartEffect(ShipNamedEffectType effectName, Dictionary<string, GameObject> assetDict, ISpaceTargetProvider target)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipFakeFadeIn()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartShipFakeFadeOut()
        {
        }

        [MethodImpl(0x8000)]
        public override void StopEffect(ShipNamedEffectType effectName)
        {
        }

        [MethodImpl(0x8000)]
        public override void Tick(float delayTime, uint solarSystemTime)
        {
        }

        public override bool IsEffectActive
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

