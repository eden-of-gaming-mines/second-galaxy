﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class PrefabNormalWeaponDesc : MonoBehaviour
    {
        [Header("飞船普通武器显示炮位")]
        public List<RealSlotPoint> RealSlotList;

        [Serializable]
        public class RealSlotPoint
        {
            [Header("武器根骨骼")]
            public GameObject WeaponRootBone;
            [Header("旋转控制")]
            public GameObject RotationRoot;
            [Header("俯仰控制")]
            public GameObject PitchRoot;
            [Header("炮台旋转最大值")]
            public float RotationMaxDegree;
            [Header("炮台旋转最小值")]
            public float RotationMinDegree;
            [Header("炮台仰角最大值")]
            public float PitchMaxDegree;
            [Header("炮台仰角最小值")]
            public float PitchMinDegree;
            [Header("炮位上的开火点")]
            public List<GameObject> LaunchPointList;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

