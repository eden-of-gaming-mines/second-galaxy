﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceShipCommonLoopEffect_ChangingBySelfSpeed : SpaceShipCommonLoopEffect_ChangingByLerpParam01
    {
        private float m_speedMin;
        private float m_speedMax;
        private bool m_isParamIncreaseBySpeed;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitEffectChangingData;
        private static DelegateBridge __Hotfix_GetLerpParam01;
        private static DelegateBridge __Hotfix_get_LerpParamVarySpeed;

        [MethodImpl(0x8000)]
        public SpaceShipCommonLoopEffect_ChangingBySelfSpeed(SpaceShipController ownerShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetLerpParam01()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEffectChangingData(float speedMin, float speedMax, bool isParamIncreaseBySpeed)
        {
        }

        protected override float LerpParamVarySpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

