﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public interface IBulletDroneInfoProvider
    {
        Transform GetLaunchLocation();
        DroneLaunchSide GetLaunchSide();
        Transform GetSrcTargetLocation();
        float GetSrcTargetScale();
        Vector3 GetWorldAroundLocationByIndex(int index);
        Vector3 GetWorldDestTargetLocation();
        Vector3 GetWorldRallyLocationByIndex(int index);
    }
}

