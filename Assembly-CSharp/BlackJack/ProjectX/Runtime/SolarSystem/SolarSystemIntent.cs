﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class SolarSystemIntent
    {
        private readonly Dictionary<string, object> m_params;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetParam;
        private static DelegateBridge __Hotfix_TryGetParam;
        private static DelegateBridge __Hotfix_GetClassParam;
        private static DelegateBridge __Hotfix_GetStructParam;

        [MethodImpl(0x8000)]
        public T GetClassParam<T>(string key) where T: class
        {
        }

        [MethodImpl(0x8000)]
        public T GetStructParam<T>(string key) where T: struct
        {
        }

        [MethodImpl(0x8000)]
        public void SetParam(string key, object value)
        {
        }

        [MethodImpl(0x8000)]
        public bool TryGetParam(string key, out object value)
        {
        }
    }
}

