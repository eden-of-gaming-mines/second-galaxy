﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public interface IShipSuperWeaponController
    {
        void AddChargePoint(uint processId, ushort unitIndex, Transform chargePoint);
        Transform GetAndRemoveChargePoint(uint processId, ushort unitIndex);
        BulletLaunchInfo GetBulletLaunchInfo(int uintIndex, bool isCalcLaunchPoint = true);
        BulletTargetProviderWarpper GetWeaponTraceTarget();
        void LoseTraceTarget(IBulletTargetProvider target);
        void Release();
        void SetSuperWeaponEffectDesc(PrefabSuperWeaponEffectDesc weaponEffectDesc);
        void SetWeaponTraceTarget(BulletTargetProviderWarpper traceTarget);
        Transform StartCharge(int unitIndex, BulletTargetProviderWarpper target, float chargeTime);
        void StartFire(int uintIndex, BulletTargetProviderWarpper target, float lastTime = -1f, Transform defaultLaunchPoint = null);
        void StopAllCharge();
        void StopAllFire();
    }
}

