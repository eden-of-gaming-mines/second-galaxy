﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class PrefabSuperWeaponDesc : MonoBehaviour
    {
        [Header("飞船是否具有武器发射动画")]
        public bool HasSuperWeaponLaunchAnimator;
        [Header("飞船超级武器显示炮位")]
        public List<RealSlotPoint> RealSlotList;

        [Serializable]
        public class RealSlotPoint
        {
            [Header("显示炮位根节点")]
            public GameObject realSlotPoint;
            [Header("炮位上的开火点")]
            public List<GameObject> LaunchPointList;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

