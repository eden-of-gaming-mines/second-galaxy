﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceDropBoxController : SpaceObjectControllerBase, ISpaceTargetProvider
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<object> EventOnDropBoxDestory;
        protected const string SubRank1Color = "#0x7E7E7EFF";
        protected const string SubRank2Color = "#0x548f60FF";
        protected const string SubRank3Color = "#0x5c87D5FF";
        protected const string SubRank4Color = "#0x8F50BFFF";
        protected const string SubRank5Color = "#0xDF9146FF";
        private static DelegateBridge __Hotfix_Create;
        private static DelegateBridge __Hotfix_Release;
        private static DelegateBridge __Hotfix_InitSpaceObjectPrefab;
        private static DelegateBridge __Hotfix_GetColorStrBySubRank;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.RegisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.UnregisterTargetDestroyEvent;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetTransform;
        private static DelegateBridge __Hotfix_BlackJack.ProjectX.Runtime.SolarSystem.ISpaceTargetProvider.GetTargetSize;
        private static DelegateBridge __Hotfix_get_DisplayNameForUI;
        private static DelegateBridge __Hotfix_get_ClientSpaceObject;
        private static DelegateBridge __Hotfix_add_EventOnDropBoxDestory;
        private static DelegateBridge __Hotfix_remove_EventOnDropBoxDestory;

        protected event Action<object> EventOnDropBoxDestory
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        float ISpaceTargetProvider.GetTargetSize()
        {
        }

        [MethodImpl(0x8000)]
        Transform ISpaceTargetProvider.GetTargetTransform()
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.RegisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        void ISpaceTargetProvider.UnregisterTargetDestroyEvent(Action<object> onTargetDestroy)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Create(BlackJack.ProjectX.ClientSolarSystemNs.ClientSpaceObject clientObj, IDynamicAssetProvider dynamicAssetProvider, bool initActive = true, Camera camera = null)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetColorStrBySubRank(SubRankType subRank)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSpaceObjectPrefab()
        {
        }

        [MethodImpl(0x8000)]
        public override void Release()
        {
        }

        public override string DisplayNameForUI
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ClientSpaceDropBox ClientSpaceObject
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

