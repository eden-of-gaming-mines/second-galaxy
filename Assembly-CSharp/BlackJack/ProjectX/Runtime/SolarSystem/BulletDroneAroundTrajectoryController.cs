﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class BulletDroneAroundTrajectoryController : PrefabControllerBase
    {
        protected float m_moveDir;
        protected float m_aroundSpeed;
        protected float m_upDownSpeed;
        protected float m_radius;
        protected float m_aroundScaleX;
        protected Transform m_rotate;
        protected Transform m_dummy;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_GetDroneDummyTransform;
        private static DelegateBridge __Hotfix_GetAroundTrajectorySpeed;
        private static DelegateBridge __Hotfix_get_AroundSpeed;
        private static DelegateBridge __Hotfix_set_AroundSpeed;
        private static DelegateBridge __Hotfix_get_UpDownSpeed;
        private static DelegateBridge __Hotfix_set_UpDownSpeed;
        private static DelegateBridge __Hotfix_get_Radius;
        private static DelegateBridge __Hotfix_set_Radius;

        [MethodImpl(0x8000)]
        public float GetAroundTrajectorySpeed()
        {
        }

        [MethodImpl(0x8000)]
        public Transform GetDroneDummyTransform()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        public float AroundSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float UpDownSpeed
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public float Radius
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

