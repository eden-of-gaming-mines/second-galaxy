﻿namespace BlackJack.ProjectX.Runtime.SolarSystem
{
    using System;
    using UnityEngine;

    public class PrefabTeleportTunnelDesc : MonoBehaviour
    {
        [Header("跳跃通道跳跃摄像机移动开始点")]
        public GameObject m_teleportCameraFadeInPosStart;
        [Header("跳跃通道跳跃摄像机移动结束点")]
        public GameObject m_teleportCameraFadeInPosEnd;
        [Header("跳跃通道跳跃摄像机从盯住舰船到盯住星门的动画时间")]
        public float m_teleportCameraLookAtStarGateTime;
        [Header("跳跃通道跳跃摄像机向星门加速移动动画时间")]
        public float m_teleportCameraAccMoveTime;
    }
}

