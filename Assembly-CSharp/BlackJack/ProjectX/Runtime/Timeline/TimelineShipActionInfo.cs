﻿namespace BlackJack.ProjectX.Runtime.Timeline
{
    using IL;
    using System;

    [Serializable]
    public class TimelineShipActionInfo
    {
        public string m_srcActorName;
        public string m_targetActorName;
        public string m_actionType;
        public string m_actionParam;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

