﻿namespace BlackJack.ProjectX.Runtime.Timeline
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;

    public class ShipActionMixerBehaviour : PlayableBehaviour
    {
        private TimelineShipActionEventHandler m_eventHandler;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ProcessFrame;
        private static DelegateBridge __Hotfix_SendMessage;

        [MethodImpl(0x8000)]
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendMessage(List<TimelineShipActionInfo> actionInfos)
        {
        }
    }
}

