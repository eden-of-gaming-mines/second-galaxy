﻿namespace BlackJack.ProjectX.Runtime.Timeline
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [Serializable]
    public class ShipActionClip : PlayableAsset, ITimelineClipAsset
    {
        [HideInInspector]
        public ShipActionBehaviour m_template;
        public List<TimelineShipActionInfo> m_shipActionInfos;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private double <StartTime>k__BackingField;
        private static DelegateBridge __Hotfix_get_clipCaps;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_CreatePlayable;

        [MethodImpl(0x8000)]
        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
        }

        public ClipCaps clipCaps
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public double StartTime
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

