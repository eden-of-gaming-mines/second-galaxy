﻿namespace BlackJack.ProjectX.Runtime.Timeline
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.Playables;
    using UnityEngine.Timeline;

    [TrackClipType(typeof(ShipActionClip)), TrackColor(0.5808823f, 0.5808823f, 0.5808823f), TrackBindingType(typeof(TimelineShipActionEventHandler))]
    public class ShipActionTrack : TrackAsset
    {
        private static DelegateBridge __Hotfix_CreateTrackMixer;

        [MethodImpl(0x8000)]
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
        }
    }
}

