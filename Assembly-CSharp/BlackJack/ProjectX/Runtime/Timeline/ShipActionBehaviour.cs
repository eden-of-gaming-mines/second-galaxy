﻿namespace BlackJack.ProjectX.Runtime.Timeline
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.Playables;

    [Serializable]
    public class ShipActionBehaviour : PlayableBehaviour
    {
        public List<TimelineShipActionInfo> m_shipActionInfos;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <EventTriggered>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private double <StartTime>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_EventTriggered;
        private static DelegateBridge __Hotfix_set_EventTriggered;
        private static DelegateBridge __Hotfix_get_StartTime;
        private static DelegateBridge __Hotfix_set_StartTime;
        private static DelegateBridge __Hotfix_OnGraphStart;

        [MethodImpl(0x8000)]
        public override void OnGraphStart(Playable playable)
        {
        }

        public bool EventTriggered
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public double StartTime
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

