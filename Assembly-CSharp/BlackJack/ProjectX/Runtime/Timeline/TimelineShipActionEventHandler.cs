﻿namespace BlackJack.ProjectX.Runtime.Timeline
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class TimelineShipActionEventHandler : MonoBehaviour
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<TimelineShipActionInfo> EventOnTimelineShipActionInfo;
        private static DelegateBridge __Hotfix_OnShipAction;
        private static DelegateBridge __Hotfix_add_EventOnTimelineShipActionInfo;
        private static DelegateBridge __Hotfix_remove_EventOnTimelineShipActionInfo;

        public event Action<TimelineShipActionInfo> EventOnTimelineShipActionInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnShipAction(TimelineShipActionInfo actionInfo)
        {
        }
    }
}

