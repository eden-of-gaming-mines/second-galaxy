﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class DeviceInfoRatingHelper
    {
        private const float ProcessorFrequencyLowAndroid = 1800f;
        private const float ProcessorFrequencyMediumAndroid = 2000f;
        private const float ProcessorCountLowAndroid = 4f;
        private const float ProcessorCountMediumAndroid = 6f;
        private const float SystemMemorySizeLowAndroid = 3000f;
        private const float SystemMemorySizeMediumAndroid = 5000f;
        private const float SystemMemorySizeLowIOS = 1200f;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetCurrSystemInfoRatingLevel;
        private static DelegateBridge __Hotfix_IsLowMemory;
        private static DelegateBridge __Hotfix_GetCurrSystemInfoRatingLevel4Android;
        private static DelegateBridge __Hotfix_GetLevelByMemory;
        private static DelegateBridge __Hotfix_GetLevelProcessorCount;
        private static DelegateBridge __Hotfix_GetLevelProcessorFrequency;
        private static DelegateBridge __Hotfix_GetCurrSystemInfoRatingLevel4Ios;
        private static DelegateBridge __Hotfix_CalcSystemInfoRatingLevel;

        [MethodImpl(0x8000)]
        private static DeviceRatingLevel CalcSystemInfoRatingLevel()
        {
        }

        [MethodImpl(0x8000)]
        public static DeviceRatingLevel GetCurrSystemInfoRatingLevel()
        {
        }

        [MethodImpl(0x8000)]
        private static DeviceRatingLevel GetCurrSystemInfoRatingLevel4Android()
        {
        }

        [MethodImpl(0x8000)]
        private static DeviceRatingLevel GetCurrSystemInfoRatingLevel4Ios()
        {
        }

        [MethodImpl(0x8000)]
        private static DeviceRatingLevel GetLevelByMemory(int memorySize)
        {
        }

        [MethodImpl(0x8000)]
        private static DeviceRatingLevel GetLevelProcessorCount(int processorCount)
        {
        }

        [MethodImpl(0x8000)]
        private static DeviceRatingLevel GetLevelProcessorFrequency(int processorFrequency)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsLowMemory()
        {
        }
    }
}

