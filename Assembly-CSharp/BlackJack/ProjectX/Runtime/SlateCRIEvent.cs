﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SlateCRIEvent : MonoBehaviour
    {
        private static DelegateBridge __Hotfix_OnDestroy;
        private static DelegateBridge __Hotfix_EventOnPlayCRIMusic;

        [MethodImpl(0x8000)]
        public void EventOnPlayCRIMusic(object value)
        {
        }

        [MethodImpl(0x8000)]
        public void OnDestroy()
        {
        }
    }
}

