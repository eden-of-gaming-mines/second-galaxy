﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using NSpeex;
    using System;
    using System.Runtime.CompilerServices;

    public static class VoiceChatUtils
    {
        private static SpeexEncoder _speexEnc;
        private static SpeexDecoder _speexDec;
        private static DelegateBridge __Hotfix_FloatToShortArray;
        private static DelegateBridge __Hotfix_ShortToFloatArray;
        private static DelegateBridge __Hotfix_SpeexDecompress;
        private static DelegateBridge __Hotfix_SpeexCompress;
        private static DelegateBridge __Hotfix_NormalizeVolume;

        [MethodImpl(0x8000)]
        public static void FloatToShortArray(float[] input, short[] output)
        {
        }

        [MethodImpl(0x8000)]
        public static void NormalizeVolume(float[] voiceData)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShortToFloatArray(short[] input, float[] output, int length)
        {
        }

        [MethodImpl(0x8000)]
        public static byte[] SpeexCompress(float[] input)
        {
        }

        [MethodImpl(0x8000)]
        public static float[] SpeexDecompress(byte[] data, int dataLength, int samples)
        {
        }

        [CompilerGenerated]
        private sealed class <NormalizeVolume>c__AnonStorey0
        {
            internal float factor;

            internal float <>m__0(float i) => 
                (i * this.factor);
        }
    }
}

