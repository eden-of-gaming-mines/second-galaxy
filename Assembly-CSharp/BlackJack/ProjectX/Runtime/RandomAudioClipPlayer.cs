﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    [Serializable, RequireComponent(typeof(AudioSource))]
    public class RandomAudioClipPlayer : MonoBehaviour
    {
        private AudioSource m_audioSource;
        [Header("播放延时")]
        public float m_delayTime;
        [Header("播放速度范围最小值")]
        public float m_minPitch;
        [Header("播放速度范围最大值")]
        public float m_maxPitch;
        [Header("待播放的AudioClip列表")]
        public List<AudioClip> m_audioClipList;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_Play;
        private static DelegateBridge __Hotfix_PlayWithDelay;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        public void Play(float volume = -1f)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator PlayWithDelay()
        {
        }

        [CompilerGenerated]
        private sealed class <PlayWithDelay>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal AudioClip <clip>__0;
            internal float <pitch>__0;
            internal RandomAudioClipPlayer $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

