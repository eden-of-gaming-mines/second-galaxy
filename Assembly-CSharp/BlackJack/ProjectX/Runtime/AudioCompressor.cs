﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AudioCompressor : IAudioCompressor
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Compress;
        private static DelegateBridge __Hotfix_Decompress;

        [MethodImpl(0x8000)]
        public AudioCompressor()
        {
            DelegateBridge bridge = _c__Hotfix_ctor;
            if (bridge != null)
            {
                bridge.__Gen_Delegate_Imp1(this);
            }
        }

        [MethodImpl(0x8000)]
        public byte[] Compress(float[] audioData)
        {
        }

        [MethodImpl(0x8000)]
        public float[] Decompress(byte[] compressData, int audioDataLength)
        {
        }
    }
}

