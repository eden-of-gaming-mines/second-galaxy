﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class AllianceInfoReqNetworkTask : NetWorkTransactionTask
    {
        private int m_result;
        private readonly uint m_allianceId;
        private AllianceInfo m_info;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAllianceInfoAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public AllianceInfoReqNetworkTask(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceInfoAck(int result, AllianceInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(uint allianceId, Action<AllianceInfo> callBack = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartTask>c__AnonStorey0
        {
            internal Action<AllianceInfo> callBack;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }
    }
}

