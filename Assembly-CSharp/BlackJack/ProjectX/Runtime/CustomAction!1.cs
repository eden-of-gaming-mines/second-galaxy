﻿namespace BlackJack.ProjectX.Runtime
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CustomAction<T>
    {
        private readonly List<Action<T>> m_actions;
        private readonly List<Action<T>> m_loopActions;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_AddAction;
        private static DelegateBridge __Hotfix_RemoveAction;
        private static DelegateBridge __Hotfix_Invoke;
        private static DelegateBridge __Hotfix_Clear;

        [MethodImpl(0x8000)]
        public void AddAction(Action<T> action)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void Invoke(T t)
        {
        }

        [MethodImpl(0x8000)]
        public void RemoveAction(Action<T> action)
        {
        }
    }
}

