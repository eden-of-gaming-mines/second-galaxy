﻿namespace BlackJack.ProjectX.Runtime
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SystemDescriptionController : MonoBehaviour
    {
        [Header("UI模块的id")]
        public SystemFuncDescType m_uiModuledType;
        private List<string> m_dynamicCopyPathList;
        private static DelegateBridge __Hotfix_Awake;
        private static DelegateBridge __Hotfix_OnDescriptionButtonClick;
        private static DelegateBridge __Hotfix_SetCurrSystemFuncDescType;
        private static DelegateBridge __Hotfix_SetDynamicCopyPathList;
        private static DelegateBridge __Hotfix_ShowOrHideButton;

        [MethodImpl(0x8000)]
        private void Awake()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDescriptionButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrSystemFuncDescType(SystemFuncDescType funcType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDynamicCopyPathList(List<string> pathList)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideButton(bool isShow)
        {
        }
    }
}

