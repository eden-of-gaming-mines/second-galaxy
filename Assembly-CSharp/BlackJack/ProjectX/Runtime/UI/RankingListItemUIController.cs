﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class RankingListItemUIController : UIControllerBase
    {
        private RankingTargetInfo m_itemRankingPlayerInfo;
        private RankingListType m_type;
        private CommonCaptainIconUIController m_captainIconUICtrl;
        private GuildLogoController m_logoController;
        public ScrollItemBaseUIController m_scrollItemCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<RankingListType, RankingTargetInfo> EventOnItemDetailButtonClick;
        [AutoBind("./NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./DetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DetailButton;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonCaptainUIInfoDummy;
        [AutoBind("./LogoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LogoDummy;
        [AutoBind("./RankingText04", AutoBindAttribute.InitState.NotInit, false)]
        public Text RankingText;
        [AutoBind("./BgImageRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemBGStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateRankingListItemInfo;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemDetailButtonClick;

        public event Action<RankingListType, RankingTargetInfo> EventOnItemDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRankingListItemInfo(RankingListType type, RankingTargetInfo rankingTargetInfo, int ranking, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

