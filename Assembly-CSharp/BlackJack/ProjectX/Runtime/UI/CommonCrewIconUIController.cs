﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonCrewIconUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IconUIState;
        [AutoBind("./CrewIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CrewIconImage;
        [AutoBind("./CornerNoticImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RareImage;
        [AutoBind("./CornerNoticText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrewIconCornerNoticText;
        [AutoBind("./LVandProfessionInfo/ProfessionIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CrewProfessionIconImage;
        [AutoBind("./LVandProfessionInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrewLevelText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetItemSprite;
        private static DelegateBridge __Hotfix_SetLevelText;
        private static DelegateBridge __Hotfix_SetRareColor;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        internal void SetItemSprite(Sprite crewIcon)
        {
        }

        [MethodImpl(0x8000)]
        internal void SetLevelText(string level)
        {
        }

        [MethodImpl(0x8000)]
        internal void SetRareColor(Color rareColor)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }
    }
}

