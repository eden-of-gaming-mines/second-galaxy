﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StoryUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnStoryGoImmediateButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool, bool> EventOnRequestStoryUIClose;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ActionPlanTabType, bool, int, object> EventOnRequestSwitchTab;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GotoType, bool, object> EventOnGoToTask;
        private bool m_isTaskInit;
        private LBProcessingQuestBase m_currChapterGoalQuest;
        private bool m_finishAllQuest;
        private List<int> m_inProcessSubQuestList;
        private List<int> m_subQuestList;
        private List<int> m_waitForCompleteSubQuestList;
        private List<int> m_branchQuestList;
        public List<int> m_mainStoryList;
        public int m_waitOrAlreadyCompelteCount;
        private int m_currStoryQuest;
        private LogicBlockQuestClient m_lbStory;
        private IUIBackgroundManager m_backgroundManager;
        private bool m_isPlayPanelOpenAnim;
        private StoryUIController m_mainCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string IsPlayPanelOpenAaim_ParamKey = "IsPlayPanelOpenAaim";
        public const string TaskName = "StoryUITask";
        [CompilerGenerated]
        private static Action<UIIntent> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartStoryUITask;
        private static DelegateBridge __Hotfix_ShowStoryPanel;
        private static DelegateBridge __Hotfix_HideStoryPanel;
        private static DelegateBridge __Hotfix_SetPanelPostion;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_SetBackGroundManager;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewStory;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnStoryQuestButtonClick;
        private static DelegateBridge __Hotfix_OnChapterGoalQuestButtonClick;
        private static DelegateBridge __Hotfix_OnChapterSubItemButtonClick;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_OnQuestAcceptAck;
        private static DelegateBridge __Hotfix_OnQuestWaitForCompleteNtf;
        private static DelegateBridge __Hotfix_OnQuestCompleteAck;
        private static DelegateBridge __Hotfix_OnQuestCompCondParamUpdate;
        private static DelegateBridge __Hotfix_FinishChapterGoalQuest;
        private static DelegateBridge __Hotfix_ShowMsgBoxForEnterStation;
        private static DelegateBridge __Hotfix_GotoTask;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_ShowSystemFunctionNotOpenTipWindow;
        private static DelegateBridge __Hotfix_CloseStoryPanel;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_RequestSwitchTab;
        private static DelegateBridge __Hotfix_LeaveCurrBackground;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_IsGoButtonShow;
        private static DelegateBridge __Hotfix_RepeatGuideStep;
        private static DelegateBridge __Hotfix_add_EventOnStoryGoImmediateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStoryGoImmediateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRequestStoryUIClose;
        private static DelegateBridge __Hotfix_remove_EventOnRequestStoryUIClose;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTab;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTab;
        private static DelegateBridge __Hotfix_add_EventOnGoToTask;
        private static DelegateBridge __Hotfix_remove_EventOnGoToTask;
        private static DelegateBridge __Hotfix_get_CurrChapterGoalQuest;
        private static DelegateBridge __Hotfix_get_WaitForCompleteSubQuestList;
        private static DelegateBridge __Hotfix_get_CurrStoryQuest;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<GotoType, bool, object> EventOnGoToTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool, bool> EventOnRequestStoryUIClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ActionPlanTabType, bool, int, object> EventOnRequestSwitchTab
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnStoryGoImmediateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public StoryUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseStoryPanel(Action evntAfterPause, bool isIgnoreAnim = false, bool isReapeatableUserGuide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void FinishChapterGoalQuest()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void GotoTask(GotoType type, object param = null)
        {
        }

        [MethodImpl(0x8000)]
        public void HideStoryPanel(bool isImmediate, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGoButtonShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveCurrBackground()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChapterGoalQuestButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChapterSubItemButtonClick(int questId, bool isBranchQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestAcceptAck(int result, int questInstanceID)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestCompCondParamUpdate(LBProcessingQuestBase quest, int condIndex, int param)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestCompleteAck(int result, int questId, List<QuestRewardInfo> rewardList, bool diableReturnStationNotice)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestWaitForCompleteNtf(int questInstanceID)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(CommonItemIconUIController commonItemIconCtrl, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStoryQuestButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void RepeatGuideStep()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTab(ActionPlanTabType type, int param, bool reapetableUserGuide = true, object parm2 = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackGroundManager(IUIBackgroundManager backgroundManager)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPostion(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMsgBoxForEnterStation(bool enterBase)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowStoryPanel(bool isImmediate = false, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected bool ShowSystemFunctionNotOpenTipWindow(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        public static StoryUITask StartStoryUITask(bool isPlayOpenAnim = true, Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewStory()
        {
        }

        public LBProcessingQuestBase CurrChapterGoalQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<int> WaitForCompleteSubQuestList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int CurrStoryQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideStoryPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal StoryUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess process, bool result)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey1
        {
            internal FakeLBStoreItem item;
            internal Vector3 pos;
            internal string mode;
            internal ItemSimpleInfoUITask.PositionType type;
            internal StoryUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.item, returnIntent, true, this.pos, this.mode, this.type, false, null, backgroundManager, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }
    }
}

