﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class PlayLogicalSoundUtil
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PlaySoundIndependently;
        private static DelegateBridge __Hotfix_PlaySoundOnlyOnce;
        private static DelegateBridge __Hotfix_PlaySoundReplacement;
        private static DelegateBridge __Hotfix_StopSound;
        private static DelegateBridge __Hotfix_IsSoundPlaying;
        private static DelegateBridge __Hotfix_PlaySpeech;
        private static DelegateBridge __Hotfix_PlayBackGroundMusic;

        [MethodImpl(0x8000)]
        public static void IsSoundPlaying(LogicalSoundResTableID id)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlayBackGroundMusic(LogicalSoundResTableID id, bool isInterruptSame = true)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlaySoundIndependently(LogicalSoundResTableID id, AudioClip audioClip = null, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlaySoundOnlyOnce(LogicalSoundResTableID id, AudioClip audioClip = null, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlaySoundReplacement(LogicalSoundResTableID id, AudioClip audioClip = null, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlaySpeech(LogicalSoundResTableID id, float volume = 1f)
        {
        }

        [MethodImpl(0x8000)]
        public static void StopSound(LogicalSoundResTableID id)
        {
        }
    }
}

