﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterSkillDetailInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTipsButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPreSkillTipGoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPrePropPointTipGoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDetailButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLearnButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnEffectDetailPanelBgClick;
        private List<LevelPropEffectDetailInfoItemCtrl> m_levelPropInfoCtrlList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./DetailInfo/LearnButtonButton/MoneyTextGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyNumberText;
        [AutoBind("./DetailInfo/LearnButtonButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_upgradeButtonCtrl;
        [AutoBind("./TipPanelNew", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipPanelNewStateCtrl;
        [AutoBind("./TipPanelNew/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgButtonEx;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/SkillGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_skillGroupObject;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/SkillGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tipPanelSkillNameText;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/SkillGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tipPanelSkillValueText;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/SkillGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipPanelSkillValueTextStateCtrl;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/SkillGroup/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipPanelSkillSkipButton;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/SkillGroup/SkillItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_tipPanelSkillIconImage;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/CaptainGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_captainGroupObject;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/CaptainGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tipPanelCaptainNameText;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/CaptainGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tipPanelCaptainValueText;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/CaptainGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipPanelCaptainValueTextStateCtrl;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/CaptainGroup/LevelItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_tipPanelCaptainIconImage;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_propertyGroupObject;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/PropertyGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tipPanelPropertyValueText;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/PropertyGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipPanelPropertyValueTextStateCtrl;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/PropertyGroup/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipPanelPropertySkipButton;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/PropertyGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_tipPanelPropertyIconImage;
        [AutoBind("./TipPanelNew/FrameImage/BGImage/DetailInfo/PropertyGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tipPanelPropertyNameText;
        [AutoBind("./TipPanel/Tips/PreSkillCondTip/GoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_preSkillGoButton;
        [AutoBind("./TipPanel/Tips/PreSkillCondTip/ValueGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_preSkillTipNameText;
        [AutoBind("./TipPanel/Tips/PreSkillCondTip/ValueGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_preSkillTipValueText;
        [AutoBind("./TipPanel/Tips/PropertyCondTip/GoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_prePropPointGoButton;
        [AutoBind("./TipPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipPanelShowCtrl;
        [AutoBind("./TipPanel/Tips", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipCondStateCtrl;
        [AutoBind("./DetailInfo/CondItem/SkillItem", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_preSkillButton;
        [AutoBind("./DetailInfo/CondItem/LevelItem", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_charLevelButton;
        [AutoBind("./DetailInfo/CondItem/PropertyItem", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_propertyButton;
        [AutoBind("./DetailInfo/CondItem/SkillItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_preSkillIcon;
        [AutoBind("./DetailInfo/CondItem/SkillItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_preSkillLevelCondText;
        [AutoBind("./DetailInfo/CondItem/SkillItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_preSkillCondStateCtrl;
        [AutoBind("./DetailInfo/CondItem/LevelItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_characterIcon;
        [AutoBind("./DetailInfo/CondItem/LevelItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterLevelCondText;
        [AutoBind("./DetailInfo/CondItem/LevelItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_characterCondStateCtrl;
        [AutoBind("./DetailInfo/CondItem/PropertyItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_propertyIcon;
        [AutoBind("./DetailInfo/CondItem/PropertyItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyCondText;
        [AutoBind("./DetailInfo/CondItem/PropertyItem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_propertyCondStateCtrl;
        [AutoBind("./DetailInfo/EffectItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_effectDesc;
        [AutoBind("./DetailInfo/TechItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_skillIcon;
        [AutoBind("./DetailInfo/TechItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_skillNameText;
        [AutoBind("./DetailInfo/TechItem/LevelChange/OldLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currLevelText;
        [AutoBind("./DetailInfo/TechItem/LevelChange/NewLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nextLevelText;
        [AutoBind("./DetailInfo/TechItem/FullLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_fullLevelText;
        [AutoBind("./DetailInfo/TechItem/IconBg", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_skillBgState;
        [AutoBind("./DetailInfo/EffectItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_skillEffectDesc;
        [AutoBind("./DetailInfo/EffectItem/FullLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_fullLevelEffectText;
        [AutoBind("./DetailInfo/EffectItem/LevelChange/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currLevelEffectText;
        [AutoBind("./DetailInfo/EffectItem/LevelChange/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nextLevelEffectText;
        [AutoBind("./DetailInfo/LearnButtonButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_learnButton;
        [AutoBind("./DetailInfo/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./DetailInfo/TechItem/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailButton;
        [AutoBind("./UpgradeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailPanelBgButton;
        [AutoBind("./UpgradeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_detailPanel;
        [AutoBind("./DetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelController;
        [AutoBind("./UpgradeInfo/Scroll View/Viewport/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_propEffectItemListRoot;
        [AutoBind("./UpgradeInfo/Scroll View/Viewport/ItemGroup/Item1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_propEffectItem;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closePanelButton;
        [AutoBind("./DetailInfo/TechItem/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_skillUpgradeEffectCtrl;
        [AutoBind("./MoneyGroup/CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BindMoneyValueText;
        [AutoBind("./MoneyGroup/CMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BindMoneyAddButton;
        private const string Satisfy = "Satisfy";
        private const string Dissatisfy = "Dissatisfy";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPanelMode;
        private static DelegateBridge __Hotfix_PlaySkillUpgradeEffect;
        private static DelegateBridge __Hotfix_SetSkillName;
        private static DelegateBridge __Hotfix_SetSkillDesc;
        private static DelegateBridge __Hotfix_SetSkillIcon;
        private static DelegateBridge __Hotfix_SetSkillBgState;
        private static DelegateBridge __Hotfix_SetSkillEffectForNotFull;
        private static DelegateBridge __Hotfix_SetSkillLevelForNotFull;
        private static DelegateBridge __Hotfix_SetSkillEffectForFull;
        private static DelegateBridge __Hotfix_SetSkillLevelForFull;
        private static DelegateBridge __Hotfix_SetPreSkillCondition;
        private static DelegateBridge __Hotfix_SetCharacterCondition;
        private static DelegateBridge __Hotfix_SetPropertyCondition;
        private static DelegateBridge __Hotfix_SetcaptainGroupActive;
        private static DelegateBridge __Hotfix_SetpropertyGroupActive;
        private static DelegateBridge __Hotfix_UpdatePreSkillCondTipWindowInfo;
        private static DelegateBridge __Hotfix_UpdateCharacterLevelCondTipWindowInfo;
        private static DelegateBridge __Hotfix_UpdatePropertyCondTipWindowInfo;
        private static DelegateBridge __Hotfix_UpdateTipsWindow;
        private static DelegateBridge __Hotfix_SetCostBindCurrencyForNotFull;
        private static DelegateBridge __Hotfix_SetUpgradeButtonUIState;
        private static DelegateBridge __Hotfix_ShowSkillEveryLevelEffectInfoPanel;
        private static DelegateBridge __Hotfix_UpdateMoneyInfo;
        private static DelegateBridge __Hotfix_UpdateCharacterSkillDetailLevelPropInfo;
        private static DelegateBridge __Hotfix_GetPropValueStr;
        private static DelegateBridge __Hotfix_GetSkillBuffDescStr;
        private static DelegateBridge __Hotfix_OnPreSkillConditionTipsItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnLearnButtonClick;
        private static DelegateBridge __Hotfix_OnPreSkillTipGoButtonClick;
        private static DelegateBridge __Hotfix_OnPrePropPointTipGoButtonClick;
        private static DelegateBridge __Hotfix_OnEffectDetailPanelBgClick;
        private static DelegateBridge __Hotfix_add_EventOnTipsButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTipsButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPreSkillTipGoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPreSkillTipGoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPrePropPointTipGoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPrePropPointTipGoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLearnButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLearnButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnEffectDetailPanelBgClick;
        private static DelegateBridge __Hotfix_remove_EventOnEffectDetailPanelBgClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEffectDetailPanelBgClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLearnButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPrePropPointTipGoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPreSkillTipGoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTipsButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        private string GetPropValueStr(ConfigDataPassiveSkillLevelInfo skillLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetSkillBuffDescStr(ConfigDataPassiveSkillLevelInfo skillLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEffectDetailPanelBgClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLearnButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrePropPointTipGoButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreSkillConditionTipsItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreSkillTipGoButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySkillUpgradeEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void SetcaptainGroupActive(bool active)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharacterCondition(Sprite sprite, int currCharLevel, int requireCharLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCostBindCurrencyForNotFull(int money)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelMode(bool isFullLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPreSkillCondition(Sprite sprite, int requirelevel, bool isSatisfy)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyCondition(Sprite sprite, int currCount, int requireCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetpropertyGroupActive(bool active)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillBgState(int grade)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillDesc(string desc)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillEffectForFull(string currEffectStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillEffectForNotFull(string currEffectStr, string nextEffectStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillIcon(Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillLevelForFull(int currLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillLevelForNotFull(int currLevel, int nextLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUpgradeButtonUIState(bool isActive, bool isHaveEnoughBindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSkillEveryLevelEffectInfoPanel(bool isShow, ConfigDataPassiveSkillInfo skillInfo = null, int level = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCharacterLevelCondTipWindowInfo(int currChatLevel, int requireLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCharacterSkillDetailLevelPropInfo(ConfigDataPassiveSkillInfo skillInfo, int level)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMoneyInfo(ulong money)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePreSkillCondTipWindowInfo(string name, int currSkillLevel, int requireSkillLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePropertyCondTipWindowInfo(PropertyCategory needPropertyCategory, int propertyCount, int requireCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTipsWindow(bool isActive)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public class LevelPropEffectDetailInfoItemCtrl : UIControllerBase
        {
            public Text m_nameText;
            public Text m_valueText;
            public ToggleEx m_toggle;
        }
    }
}

