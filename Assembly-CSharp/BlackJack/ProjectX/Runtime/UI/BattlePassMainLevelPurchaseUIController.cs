﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BattlePassMainLevelPurchaseUIController : UIControllerBase
    {
        private const int POOL_SIZE = 20;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<FakeLBStoreItem, CommonItemIconUIController> EventOnDropItemClick;
        private List<QuestRewardInfo> m_QuestRewardInfo;
        private Dictionary<string, UnityEngine.Object> m_resData;
        protected const string CommonItemName = "CommonItemUIPrefab";
        private GameObject m_CommonItemTemplateGo;
        [AutoBind("./TemplateRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TemplateRoot;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./ConfirmButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ConfimrGroupUIState;
        [AutoBind("./ConfirmButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./ConfirmButtonGroup/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RemoveButton;
        [AutoBind("./ConfirmButtonGroup/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RemoveButtonUIState;
        [AutoBind("./ConfirmButtonGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./ConfirmButtonGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AddButtonUIState;
        [AutoBind("./BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGImageButton;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect LoopScrollView;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool SelfEasyObjectPool;
        [AutoBind("./BuyBgImage/NumberNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelNumberText;
        [AutoBind("./BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./ConfirmButtonGroup/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CostText;
        [AutoBind("./MedalGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberText;
        [AutoBind("./MedalGroup/MedalIconGroupTrump", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MedalGroupUIState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTemplateItem;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolCommonItemCreated;
        private static DelegateBridge __Hotfix_OnCommonItemClick;
        private static DelegateBridge __Hotfix_OnCommonItemFill;
        private static DelegateBridge __Hotfix_GetFakeLBStoreItem;
        private static DelegateBridge __Hotfix_SetBuyInfoSpecia;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_add_EventOnDropItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDropItemClick;

        public event Action<FakeLBStoreItem, CommonItemIconUIController> EventOnDropItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CreateTemplateItem()
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem GetFakeLBStoreItem(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolCommonItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuyInfoSpecia(int totalBuyLv, int LvMax)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(List<QuestRewardInfo> questRewardList, int totalBuyLv, Dictionary<string, UnityEngine.Object> resData)
        {
        }
    }
}

