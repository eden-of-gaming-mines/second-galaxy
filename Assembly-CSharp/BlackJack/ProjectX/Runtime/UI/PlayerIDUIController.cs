﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class PlayerIDUIController : UIControllerBase
    {
        [AutoBind("./TextGroup/IDText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_playerIdText;
        [AutoBind("./Buttons/IDButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_copyButton;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_determineButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPlayerID;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlayerID(string str)
        {
        }
    }
}

