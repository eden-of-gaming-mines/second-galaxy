﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipStrikeForQuestNetTask : NetWorkTransactionTask
    {
        public ulong m_shipInstanceId;
        public int m_questInsId;
        public int m_strikeForQuestAckResult;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnSpaceStationLeaveStationForQuestAck;
        private static DelegateBridge __Hotfix_OnSpaceEnterNtf;

        [MethodImpl(0x8000)]
        public HangarShipStrikeForQuestNetTask(ulong shipInstanceId, int questInsId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceEnterNtf(bool isLeaveStation)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationLeaveStationForQuestAck(int result, int questInsId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

