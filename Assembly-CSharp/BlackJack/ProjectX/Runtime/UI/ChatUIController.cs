﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ChatUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSendButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSoundButtonMoveUp;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnMoveOutButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSoundButtonDown;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSoundButtonUp;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnAutoPlayToggleChange;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBGButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnNewMsgTipPanelClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string> EventOnInputEndEdit;
        public EventTriggerListener SoundTrigger;
        private const string Show = "Show";
        private const string Close = "Close";
        private const string Speak = "Speak";
        private const string Cancel = "Cancel";
        private const string CommonSystemTimePrefabName = "CommonSystemTimePrefab";
        private CommonSystemTimeUIController m_timeUICtrl;
        private EventTriggerListener CancelAreaTrigger;
        [AutoBind("./ChatPart/NewMassagePanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NewMsgTipPanel;
        [AutoBind("./ChatPart/InPutPanel/SoundButton/CancelArea", AutoBindAttribute.InitState.NotInit, false)]
        public Image CancelArea;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController showUIStateCtrl;
        [AutoBind("./ChatPart/SendSpeekMessageState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController recordStateCtrl;
        [AutoBind("./ChatPart/SendSpeekMessageState/StateGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController recordCancelUIStateCtrl;
        [AutoBind("./ChatPart/ButtonImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./ChatPart/InPutPanel/SendButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SendButton;
        [AutoBind("./ChatPart/InPutPanel/SoundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SoundButton;
        [AutoBind("./ChatPart/InPutPanel/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField InputFld;
        [AutoBind("./ChatPart/InPutPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject InputFldRoot;
        [AutoBind("./ChatPart/AutoPlay/ChoosenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AutoPlay;
        [AutoBind("./ChatPart/AutoPlay/ChoosenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx AutoPlayToggle;
        [AutoBind("./ChatPart/AutoPlay/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text AutoPlayText;
        [AutoBind("./ChatPart/InPutPanel/InputField/Placeholder", AutoBindAttribute.InitState.NotInit, false)]
        public Text InputTipText;
        [AutoBind("./ChatPart/ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemsimpleInfoDummy;
        [AutoBind("./TimeRootDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_timeRootTf;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_EnableAutoPlayToggleAndText;
        private static DelegateBridge __Hotfix_SetAutoPlayToggleState;
        private static DelegateBridge __Hotfix_GetAutoPlayToggleState;
        private static DelegateBridge __Hotfix_EnableChatInput;
        private static DelegateBridge __Hotfix_ShowOrHideChatInput;
        private static DelegateBridge __Hotfix_GetTextString;
        private static DelegateBridge __Hotfix_ClearTextString;
        private static DelegateBridge __Hotfix_SetInputFiledText;
        private static DelegateBridge __Hotfix_SetInputTipText;
        private static DelegateBridge __Hotfix_SetCancelAreaState;
        private static DelegateBridge __Hotfix_PlaySoundRecordAnimation;
        private static DelegateBridge __Hotfix_StopSoundRecordAnimation;
        private static DelegateBridge __Hotfix_ShowCancelSoundRecordTip;
        private static DelegateBridge __Hotfix_SetNewMsgTip;
        private static DelegateBridge __Hotfix_GetNewMsgTipActiveState;
        private static DelegateBridge __Hotfix_GetInputFieldSelectState;
        private static DelegateBridge __Hotfix_OnNewMsgTipPanelClick;
        private static DelegateBridge __Hotfix_OnMoveOutButtonClick;
        private static DelegateBridge __Hotfix_OnAutoPlayToggleChange;
        private static DelegateBridge __Hotfix_OnSoundButtonDown;
        private static DelegateBridge __Hotfix_OnSoundButtonUp;
        private static DelegateBridge __Hotfix_OnSoundButtonMoveUp;
        private static DelegateBridge __Hotfix_OnSendButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroupButtonClick;
        private static DelegateBridge __Hotfix_OnInputEndEdit;
        private static DelegateBridge __Hotfix_add_EventOnSendButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSoundButtonMoveUp;
        private static DelegateBridge __Hotfix_remove_EventOnSoundButtonMoveUp;
        private static DelegateBridge __Hotfix_add_EventOnMoveOutButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMoveOutButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSoundButtonDown;
        private static DelegateBridge __Hotfix_remove_EventOnSoundButtonDown;
        private static DelegateBridge __Hotfix_add_EventOnSoundButtonUp;
        private static DelegateBridge __Hotfix_remove_EventOnSoundButtonUp;
        private static DelegateBridge __Hotfix_add_EventOnAutoPlayToggleChange;
        private static DelegateBridge __Hotfix_remove_EventOnAutoPlayToggleChange;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnNewMsgTipPanelClick;
        private static DelegateBridge __Hotfix_remove_EventOnNewMsgTipPanelClick;
        private static DelegateBridge __Hotfix_add_EventOnInputEndEdit;
        private static DelegateBridge __Hotfix_remove_EventOnInputEndEdit;

        public event Action<bool> EventOnAutoPlayToggleChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<string> EventOnInputEndEdit
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnMoveOutButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnNewMsgTipPanelClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSendButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSoundButtonDown
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSoundButtonMoveUp
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSoundButtonUp
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearTextString()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableAutoPlayToggleAndText(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableChatInput(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetAutoPlayToggleState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetInputFieldSelectState()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetMainUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetNewMsgTipActiveState()
        {
        }

        [MethodImpl(0x8000)]
        public string GetTextString()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAutoPlayToggleChange(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroupButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnInputEndEdit(string input)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMoveOutButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewMsgTipPanelClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonDown(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonMoveUp(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonUp(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySoundRecordAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAutoPlayToggleState(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCancelAreaState(bool active)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInputFiledText(string inputContent)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInputTipText(string tipStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNewMsgTip(bool active)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCancelSoundRecordTip()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideChatInput(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void StopSoundRecordAnimation()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

