﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class BranchStoryUnOpenDetailUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStoryUITask.BranchStoryItemInfo> EventOnOpenButtonClick;
        private BranchStoryUITask.BranchStoryItemInfo m_itemInfo;
        [AutoBind("./TextGroup/ConditionGroup01/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ConditionText;
        [AutoBind("./TextGroup/ConditionGroup01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ConditionStateCtrl;
        [AutoBind("./OpenButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonStateCtrl;
        [AutoBind("./OpenButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OpenButton;
        [AutoBind("./ConditionImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BgImage;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateUnOpenPanel;
        private static DelegateBridge __Hotfix_OnOpenButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOpenButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOpenButtonClick;

        public event Action<BranchStoryUITask.BranchStoryItemInfo> EventOnOpenButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateUnOpenPanel(BranchStoryUITask.BranchStoryItemInfo itemInfo, Dictionary<string, Object> resDict)
        {
        }
    }
}

