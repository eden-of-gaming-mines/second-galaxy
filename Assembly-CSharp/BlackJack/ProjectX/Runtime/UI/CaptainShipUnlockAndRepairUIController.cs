﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainShipUnlockAndRepairUIController : UIControllerBase
    {
        private CommonItemIconUIController m_shipInfoCtrl;
        private List<CommonItemIconUIController> m_moduleItemCtrlList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnModuleItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnShipItemClick;
        public const string UIState_Unlock = "Unlock";
        public const string UIState_Repair = "Repair";
        private string m_currentUIState;
        public const string ModulePanelState_Show = "Show";
        public const string ModulePanelState_Close = "Close";
        private const string PrefabAssetName_CommonItem = "CommonItem";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./UnlockAndRepair", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_uiModeStateCtrl;
        [AutoBind("./UnlockAndRepair/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./UnlockAndRepair/UnlockButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnlockButton;
        [AutoBind("./UnlockAndRepair/RepairButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RepairButton;
        [AutoBind("./UnlockAndRepair/ShipInfo/ShipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipInfoDummyObject;
        [AutoBind("./UnlockAndRepair/ShipInfo/ShipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipNameText;
        [AutoBind("./UnlockAndRepair/ShipInfo/ShipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipTypeText;
        [AutoBind("./UnlockAndRepair/ShipInfo/TechImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipTechImage;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item1/ItemDummy01", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ModuleItemDummy01;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item1/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ModuleItem1Name;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item1/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ModuleItem1Count;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item2/ItemDummy02", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ModuleItemDummy02;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item2/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ModuleItem2Name;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item2/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ModuleItem2Count;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item3/ItemDummy03", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ModuleItemDummy03;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item3/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ModuleItem3Name;
        [AutoBind("./UnlockAndRepair/ModuleGroup/ItemGroup/Item3/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ModuleItem3Count;
        [AutoBind("./ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_ShowPanel;
        private static DelegateBridge __Hotfix_SetUnlockShipInfo;
        private static DelegateBridge __Hotfix_SetRepairShipInfo;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelDummyPos;
        private static DelegateBridge __Hotfix_SetShipInfo;
        private static DelegateBridge __Hotfix_SetModuleItemInfo;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_OnModuleItemClick;
        private static DelegateBridge __Hotfix_OnShipItemClick;
        private static DelegateBridge __Hotfix_add_EventOnModuleItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnModuleItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShipItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipItemClick;

        public event Action<int> EventOnModuleItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnShipItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnModuleItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetModuleItemInfo(CommonItemIconUIController itemCtrl, ShipCompListConfInfo compInfo, int index, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRepairShipInfo(LBStaticHiredCaptain.ShipInfo shipInfo, List<ShipCompListConfInfo> compList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipInfo(LBStaticHiredCaptain.ShipInfo shipInfo, List<ShipCompListConfInfo> compList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUnlockShipInfo(LBStaticHiredCaptain captain, LBStaticHiredCaptain.ShipInfo shipInfo, List<ShipCompListConfInfo> compList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowPanel(bool isShow)
        {
        }
    }
}

