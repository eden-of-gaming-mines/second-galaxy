﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildManagerUITask : GuildUITaskBase
    {
        private uint m_guildId;
        private bool m_showGuildInfoToOtherState;
        private GuildManagerUIController m_mainCtrl;
        private GuildInfoUITask m_guildInfoUITask;
        private GuildActionUITask m_actionUITask;
        private GuildFleetManagementUITask m_fleetManagementUITask;
        private GuildWarUITask m_guildWarUITask;
        private GuildBenefitsUITask m_guildBenefitsUITask;
        private bool m_isGuildInfoUITaskResLoadComplete;
        private bool m_isGuildBenefitsUITaskResLoadComplete;
        private bool m_isGuildFleetManagementUITaskResLoadComplete;
        private bool m_isGuildWarUITaskResLoadComplete;
        private bool m_isGuildActionUITaskResLoadComplete;
        private bool? m_prepareDataForRedPoint;
        private bool? m_prepareDataForGuildInfo;
        private bool? m_prepareDataForGuildAction;
        private bool? m_prepareDataForGuildWar;
        private bool? m_prepareDataForFleet;
        protected GuildManagerTabType m_currTabType;
        private IUIBackgroundManager m_backgroundManager;
        public const string ModeInfo = "ModeInfo";
        public const string ModeBenefits = "ModeBenefits";
        public const string ModeAction = "ModeAction";
        public const string ModeWar = "ModeWar";
        public const string ModeFleet = "ModeFleet";
        public const string ParamKeyGuildId = "GuildId";
        public const string ParamKeyShowGuildInfoToOtherState = "ShowGuildInfoToOtherState";
        public const string ParamKeyResumeStationBgWhenResume = "ResumeStationBgWhenResume";
        public const string ParamKeyBackgroundManager = "BackgroundManager";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected bool m_resumeStationBgWhenResume;
        public const string TaskName = "GuildManagerUITask";
        private const string ParamaKeyGuildWarStartIndex = "GuildWarStartIndex";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildManagerUITaskWithPrepare;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnPrepareDataEnd;
        private static DelegateBridge __Hotfix_IsOpenedSelfGuildManager;
        private static DelegateBridge __Hotfix_IsPrepareDataSucess;
        private static DelegateBridge __Hotfix_GetPrepareDataResult;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnGuildInfoUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_OnGuildBenefitsUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_OnGuildFleetManagementUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_OnGuildWarUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_OnGuildActionUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewForRedPoints;
        private static DelegateBridge __Hotfix_IsSelfGuildInvalid;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnGuildManagerTabChanged;
        private static DelegateBridge __Hotfix_OnGuildManagerTabChangedImp;
        private static DelegateBridge __Hotfix_OnPauseGuildManagerUITask;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_RequestBackgroundManager;
        private static DelegateBridge __Hotfix_OnBenefitsChange;
        private static DelegateBridge __Hotfix_OnGuildReportItemClick;
        private static DelegateBridge __Hotfix_OnGuildWarRecordListStartIndex;
        private static DelegateBridge __Hotfix_OnGuildFleetDetailButtonClick;
        private static DelegateBridge __Hotfix_PrepareDataForGuildInfo;
        private static DelegateBridge __Hotfix_PrepareDataForGuildAction;
        private static DelegateBridge __Hotfix_PrepareDataForGuildBenefits;
        private static DelegateBridge __Hotfix_PrepareDataForGuildFleetManagement;
        private static DelegateBridge __Hotfix_PrepareDataForGuildWar;
        private static DelegateBridge __Hotfix_ClearPrePareFlags;
        private static DelegateBridge __Hotfix_IsMyGuild;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_GetHideAllUIProcess;
        private static DelegateBridge __Hotfix_GetHideAllSubTaskImmediateUIProcess;
        private static DelegateBridge __Hotfix_GetGuildInfoPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetGuildBenefitsPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetGuildActionPanelProcess;
        private static DelegateBridge __Hotfix_GetGuildFleetManagementPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetGuildWarPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_CloseGuildManagerUIPanel;
        private static DelegateBridge __Hotfix_ReturnToPreTask;
        private static DelegateBridge __Hotfix_SetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LBGuild;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearPrePareFlags()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseGuildManagerUIPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetGuildActionPanelProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetGuildBenefitsPanelShowOrHideProcess(bool isShown, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetGuildFleetManagementPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetGuildInfoPanelShowOrHideProcess(bool isShow, bool isImmediate, bool showGuildInfoToOtherState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetGuildWarPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetHideAllSubTaskImmediateUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetHideAllUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        private bool GetPrepareDataResult(string mode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsMyGuild()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsOpenedSelfGuildManager()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPrepareDataSucess(string mode)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsSelfGuildInvalid()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBenefitsChange()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildActionUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildBenefitsUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildFleetDetailButtonClick(GuildFleetSimpleInfo guildSimpleInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildFleetManagementUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildInfoUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildManagerTabChanged(GuildManagerTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildManagerTabChangedImp(GuildManagerTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildReportItemClick(GuildBattleSimpleReportInfo reportInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildWarRecordListStartIndex(int startIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildWarUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPauseGuildManagerUITask(Action onPauseEnd, bool ignoreCloseAnimation = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrepareDataEnd(string mode, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareDataForGuildAction(Action<GuildManagerTabType, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareDataForGuildBenefits(Action<GuildManagerTabType, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareDataForGuildFleetManagement(Action<GuildManagerTabType, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareDataForGuildInfo(Action<GuildManagerTabType, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareDataForGuildWar(Action<GuildManagerTabType, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RequestBackgroundManager(Action<IUIBackgroundManager> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequstEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnToPreTask()
        {
        }

        [MethodImpl(0x8000)]
        private void SetSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildManagerUITaskWithPrepare(uint guildId, UIIntent returnToIntent, string mode, Action<bool> onPrepareEnd, Action<bool> onPipelineEnd = null, bool showGuildInfoToOtherState = true, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewForRedPoints()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockGuildClient LBGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildActionPanelProcess>c__AnonStorey9
        {
            internal bool isImmediate;
            internal GuildManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                this.$this.m_actionUITask.StartGuidActionUITask("ModeInGuildManager", playerContext.CurrSolarSystemId, this.$this.CurrentIntent, onEnd, null);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                if (this.$this.m_actionUITask != null)
                {
                    this.$this.m_actionUITask.CloseActionPanel(this.isImmediate, onEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildBenefitsPanelShowOrHideProcess>c__AnonStorey7
        {
            internal bool isImmediate;
            internal GuildManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                <GetGuildBenefitsPanelShowOrHideProcess>c__AnonStorey8 storey = new <GetGuildBenefitsPanelShowOrHideProcess>c__AnonStorey8 {
                    <>f__ref$7 = this,
                    onEnd = onEnd
                };
                this.$this.m_guildBenefitsUITask.ShowGuildBenefitsPanel(this.isImmediate, new Action<bool>(storey.<>m__0), storey.onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_guildBenefitsUITask.HideGuildBenefitsPanel(this.isImmediate, onEnd);
            }

            private sealed class <GetGuildBenefitsPanelShowOrHideProcess>c__AnonStorey8
            {
                internal Action<bool> onEnd;
                internal GuildManagerUITask.<GetGuildBenefitsPanelShowOrHideProcess>c__AnonStorey7 <>f__ref$7;

                internal void <>m__0(bool res)
                {
                    if (!res)
                    {
                        this.<>f__ref$7.$this.Stop();
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildFleetManagementPanelShowOrHideProcess>c__AnonStoreyA
        {
            internal bool isImmediate;
            internal GuildManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_fleetManagementUITask.StartGuildFleetManagementUITask(true, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_fleetManagementUITask.HideGuildFleetManagementUITask(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildInfoPanelShowOrHideProcess>c__AnonStorey6
        {
            internal bool isImmediate;
            internal bool showGuildInfoToOtherState;
            internal GuildManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_guildInfoUITask.ShowGuildInfoPanel(this.$this.m_guildId, this.isImmediate, this.showGuildInfoToOtherState, onEnd, this.$this.m_backgroundManager);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_guildInfoUITask.HideGuildInfoPanel(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetGuildWarPanelShowOrHideProcess>c__AnonStoreyB
        {
            internal bool isImmediate;
            internal GuildManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_guildWarUITask.StartGuideWarUITask(true, (this.$this.m_currIntent as UIIntentCustom).GetStructParam<int>("GuildWarStartIndex"), onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_guildWarUITask.HideGuildWarPanel(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <OnPauseGuildManagerUITask>c__AnonStorey1
        {
            internal Action onPauseEnd;
            internal GuildManagerUITask $this;

            internal void <>m__0(UIProcess process, bool b)
            {
                this.$this.Pause();
                if (this.onPauseEnd != null)
                {
                    this.onPauseEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareDataForGuildAction>c__AnonStorey3
        {
            internal Action<GuildManagerTabType, bool> onEnd;

            internal void <>m__0(bool ret)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(GuildManagerTabType.GuildAction, ret);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareDataForGuildFleetManagement>c__AnonStorey4
        {
            internal Action<GuildManagerTabType, bool> onEnd;
            internal GuildManagerUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetListSimpleInfoReqNetTask task2 = task as GuildFleetListSimpleInfoReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(GuildManagerTabType.GuildFleetManagement, false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(GuildManagerTabType.GuildFleetManagement, true);
                    }
                }
                else
                {
                    if (task2.Result == -1800)
                    {
                        this.$this.CloseGuildManagerUIPanel();
                    }
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    if (this.onEnd != null)
                    {
                        this.onEnd(GuildManagerTabType.GuildFleetManagement, false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareDataForGuildInfo>c__AnonStorey2
        {
            internal Action<GuildManagerTabType, bool> onEnd;

            internal void <>m__0(bool ret)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(GuildManagerTabType.GuildInfo, ret);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareDataForGuildWar>c__AnonStorey5
        {
            internal Action<GuildManagerTabType, bool> onEnd;
            internal GuildManagerUITask $this;

            internal void <>m__0(Task task)
            {
                GuildBattleSimpleReportInfoReqNetTask task2 = task as GuildBattleSimpleReportInfoReqNetTask;
                if (task2 == null)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(GuildManagerTabType.GuildWar, false);
                    }
                }
                else if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(GuildManagerTabType.GuildWar, false);
                    }
                }
                else
                {
                    Debug.Log($"<color=red>为军团战争准备数据 返回值 = {task2.Result}</color>");
                    if (task2.Result == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(GuildManagerTabType.GuildWar, true);
                        }
                    }
                    else
                    {
                        if (task2.Result == -1800)
                        {
                            this.$this.CloseGuildManagerUIPanel();
                        }
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(GuildManagerTabType.GuildWar, false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal UIIntent intent;
            internal Action<bool> onPrepareEnd;
            internal GuildManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_prepareDataForRedPoint = new bool?(res);
                this.$this.OnPrepareDataEnd(this.intent.TargetMode, this.onPrepareEnd);
            }

            internal void <>m__1(GuildManagerTabType prepareTabType, bool ret)
            {
                this.$this.m_prepareDataForGuildWar = new bool?(ret);
                this.$this.OnPrepareDataEnd(this.intent.TargetMode, this.onPrepareEnd);
            }

            internal void <>m__2(GuildManagerTabType prepareTabType, bool ret)
            {
                this.$this.m_prepareDataForGuildAction = new bool?(ret);
                this.$this.OnPrepareDataEnd(this.intent.TargetMode, this.onPrepareEnd);
            }

            internal void <>m__3(GuildManagerTabType prepareTabType, bool ret)
            {
                this.$this.m_prepareDataForGuildInfo = new bool?(ret);
                this.$this.OnPrepareDataEnd(this.intent.TargetMode, this.onPrepareEnd);
            }

            internal void <>m__4(GuildManagerTabType prepareTabType, bool ret)
            {
                this.$this.m_prepareDataForFleet = new bool?(ret);
                this.$this.OnPrepareDataEnd(this.intent.TargetMode, this.onPrepareEnd);
            }

            internal void <>m__5(GuildManagerTabType prepareType, bool ret)
            {
                if (this.onPrepareEnd != null)
                {
                    this.onPrepareEnd(ret);
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            TabTypeChanged
        }

        public class SendGuildRedPointVersionReqNetWorkTask : NetWorkTransactionTask
        {
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnGuildRedPointVersionAck;

            public SendGuildRedPointVersionReqNetWorkTask() : base(10f, true, false, false)
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            private void OnGuildRedPointVersionAck()
            {
                DelegateBridge bridge = __Hotfix_OnGuildRedPointVersionAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    (GameManager.Instance.PlayerContext as ProjectXPlayerContext).EventOnGuildRedPointVersionAck += new Action(this.OnGuildRedPointVersionAck);
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                return (GameManager.Instance.PlayerContext as ProjectXPlayerContext).SendGuildRedPointVersionReq();
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    (GameManager.Instance.PlayerContext as ProjectXPlayerContext).EventOnGuildRedPointVersionAck -= new Action(this.OnGuildRedPointVersionAck);
                }
            }
        }
    }
}

