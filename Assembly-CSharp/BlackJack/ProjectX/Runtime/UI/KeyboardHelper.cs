﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class KeyboardHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_IsDisableKeyboard;
        private static DelegateBridge __Hotfix_IsInputing;
        private static DelegateBridge __Hotfix_IsInUserGuide;

        [MethodImpl(0x8000)]
        public static bool IsDisableKeyboard()
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsInputing()
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsInUserGuide()
        {
        }
    }
}

