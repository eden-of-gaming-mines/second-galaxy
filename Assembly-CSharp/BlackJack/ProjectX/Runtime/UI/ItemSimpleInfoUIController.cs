﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ItemSimpleInfoUIController : UIControllerBase
    {
        protected ProjectXPlayerContext m_playerCtx;
        public CommonItemIconUIController CommonItemCtrl;
        public int CurrentSelectedItemCount;
        public ILBStoreItemClient CurrentSelectItem;
        public List<ItemSimpleInfoBlueprintMaterialUICtrl> m_blueprintMaterialList;
        public List<CommonDependenceAndRelativeTechItemUICtrl> m_blueprintTechList;
        private GameObject m_techItem;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGotoTechButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemCountInputSubmit;
        protected const string BtnState_MoveToStoreAndDetail = "MoveToStoreAndDetail";
        protected const string BtnState_MoveToShipAndDetail = "MoveToShipAndDetail";
        protected const string BtnState_DestroyAndDetail = "DestroyAndDetail";
        protected const string BtnState_OnlyMoveToStore = "OnlyMoveToStore";
        protected const string BtnState_OnlyMoveToShip = "OnlyMoveToShip";
        protected const string BtnState_OnlyDestroy = "OnlyDestroy";
        protected const string BtnState_OnlyDetail = "OnlyDetail";
        protected const string BtnState_NoButton = "NoButton";
        protected const string BtnState_MoveToHangarAndDetail = "MoveToHangarAndDetail";
        protected const string BtnState_PreviewAndProduce = "PreviewAndProduce";
        protected const string BtnState_PreviewAndTech = "PreviewAndTech";
        protected const string BtnState_PreviewAndChoose = "PreviewAndChoose";
        protected const string BtnState_AddToCrackList = "AddToCrackList";
        protected const string BtnState_GoToCrackList = "GoToCrack";
        protected const string BtnState_Use = "Use";
        protected const string BtnState_DetailAndChange = "Ammunition";
        protected const string BtnNormalState = "Normal";
        protected const string BtnGrayState = "Gray";
        protected const string BlueprintPanelNormalState = "Normal";
        protected const string BlueprintPanelTechlState = "Tech";
        [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController btnUIStateCtrl;
        protected const string BtnPos_Up = "Up";
        protected const string BtnPos_Down = "Down";
        [AutoBind("./ButtonGroup/ButtonPosition", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController btnPosUIStateCtrl;
        protected const string Panel_Ship = "Ship";
        protected const string Panel_FlagShip = "MessageShipGroup";
        protected const string Panel_Weapon = "Weapon";
        protected const string Panel_Ammo = "Ammo";
        protected const string Panel_Mineral = "Mineral";
        protected const string Panel_Chip = "Chip";
        protected const string Panel_Equip = "Equip";
        protected const string Panel_Component = "Component";
        protected const string Panel_Blueprint = "Blueprint";
        protected const string Panel_NormalItem = "NormalItem";
        protected const string Panel_BlueprintTech = "BlueprintTech";
        protected const string Panel_CrackBox = "CrackBox";
        protected const string Panel_BlueprintLock = "GuidBlueprintGroup";
        [AutoBind("./DetailInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemInfoPanelUIStateCtrl;
        [AutoBind("./DetailInfoGroup/BlueprintGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BlueprintPanelUIStateCtrl;
        protected const string Panel_Open = "Show";
        protected const string Panel_Close = "Close";
        protected const string Blueprint_TechOk = "Green";
        protected const string Blueprint_TechFail = "Red";
        protected const string BlueprintUnLock_ConditionOk = "Normal";
        protected const string BlueprintUnLock_ConditionFail = "Red";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        protected ItemInfoUIMode m_currMode;
        protected const string DynamicPropertyPoolName = "PropertyItem";
        [AutoBind("./ButtonGroup/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DetailButton;
        [AutoBind("./ButtonGroup/PutInShipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PutInShipButton;
        [AutoBind("./ButtonGroup/PutInStoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PutInStoreButton;
        [AutoBind("./ButtonGroup/DiscardButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button DiscardButton;
        [AutoBind("./ButtonGroup/PutInButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PutInButton;
        [AutoBind("./ButtonGroup/ChoosenButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ChooseButton;
        [AutoBind("./ButtonGroup/ChoosenButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ChooseButtonStateCtrl;
        [AutoBind("./ButtonGroup/ItemPreviewButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemPreviewButton;
        [AutoBind("./ButtonGroup/GoProduceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ProduceButton;
        [AutoBind("./ButtonGroup/GoProduceButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GoProduceButtonStateCtrl;
        [AutoBind("./ButtonGroup/GoTechButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GoTechButton;
        [AutoBind("./ButtonGroup/AddToCrackListButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddToCrackListButton;
        [AutoBind("./ButtonGroup/GoCrackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GoCrackButton;
        [AutoBind("./ButtonGroup/UseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UseButton;
        [AutoBind("./ButtonGroup/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ChangeButton;
        [AutoBind("./ButtonGroup/AddAndRemovePanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AddAndRemovePanelGo;
        [AutoBind("./ButtonGroup/AddAndRemovePanel/InputField/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text PutinOutNumber;
        [AutoBind("./ButtonGroup/AddAndRemovePanel/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./ButtonGroup/AddAndRemovePanel/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RemoveButton;
        [AutoBind("./ButtonGroup/AddAndRemovePanel/CountSlider", AutoBindAttribute.InitState.NotInit, false)]
        public Slider CountSlider;
        [AutoBind("./ButtonGroup/AddAndRemovePanel/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField ItemCountInputField;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public LongPressDesc m_longPressDescList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonItemDummy;
        [AutoBind("./ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ShareButton;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./Tech/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RankImage;
        [AutoBind("./Quality/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SubRankImage;
        [AutoBind("./Quality/SubRankBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SubRankCtrl;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BGImageUIStateCtrl;
        [AutoBind("./DetailInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool DynamicPropertyPool;
        [AutoBind("./DetailInfoGroup/ShipGroup/ShipUseType/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipUseType4Ship;
        [AutoBind("./DetailInfoGroup/ShipGroup/ShipRadarChart", AutoBindAttribute.InitState.NotInit, false)]
        public CommonShipRadarChartUIController m_shiRadarChartCtrl;
        [AutoBind("./DetailInfoGroup/WeaponGroup/WeaponDamage/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponDamageText;
        [AutoBind("./DetailInfoGroup/WeaponGroup/Range/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponRangeText;
        [AutoBind("./DetailInfoGroup/WeaponGroup/EnergyCost/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponEnergyCostText;
        [AutoBind("./DetailInfoGroup/WeaponGroup/CD/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponLaunchCDText;
        [AutoBind("./DetailInfoGroup/WeaponGroup/CPU", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WeaponCPUGameObject;
        [AutoBind("./DetailInfoGroup/WeaponGroup/CPU/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponCPUText;
        [AutoBind("./DetailInfoGroup/WeaponGroup/POW", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WeaponPOWGameObject;
        [AutoBind("./DetailInfoGroup/WeaponGroup/POW/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponPOWText;
        [AutoBind("./DetailInfoGroup/WeaponGroup/AssembleTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WeaponCPUAndPOWTitle;
        [AutoBind("./DetailInfoGroup/AmmoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AmmoGroupStateCtrl;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageCompose/LightingDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElectricalDamageResistNumber;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageCompose/LightingDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ElectricalDamageResistImage;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageCompose/EnergyDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KineticDamageResistNumber;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageCompose/EnergyDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image KineticDamageResistImage;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageCompose/FireDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HeatDamageResistNumber;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageCompose/FireDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image HeatDamageResistImage;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageModify/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoDamageModifyText;
        [AutoBind("./DetailInfoGroup/AmmoGroup/DamageBasic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoDamageBaseText;
        [AutoBind("./DetailInfoGroup/AmmoGroup/AttackCD/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoAttackCDText;
        [AutoBind("./DetailInfoGroup/AmmoGroup/RangeModify/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoRangeModifyText;
        [AutoBind("./DetailInfoGroup/AmmoGroup/RangeMax/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoRangeMaxText;
        [AutoBind("./DetailInfoGroup/AmmoGroup/FlySpeed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoFlySpeedText;
        [AutoBind("./DetailInfoGroup/AmmoGroup/FightTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoFightTimeText;
        [AutoBind("./DetailInfoGroup/MineralItemGroup/Desc/Title/Scroll View/Viewport/Content/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text Desc4MineralAndNormal;
        [AutoBind("./DetailInfoGroup/ChipGroup/BasePropertyTitle/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ChipBasePropertyItemContentRoot;
        [AutoBind("./DetailInfoGroup/ChipGroup/ExtraPropertyTitle/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ChipExtraPropertyContentItemRoot;
        [AutoBind("./DetailInfoGroup/ChipGroup/ExtraPropertyTitle", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ChipExtraPropertyRoot;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect EquipScrollRect;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/ActiveEffectDesc/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text EquipActiveEffectDesc;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/Range", AutoBindAttribute.InitState.NotInit, false)]
        public Transform EquipRangeRoot;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/Range/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EquipRangeTitleText;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/Range/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EquipRangeValueText;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/EnergyCost/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EquipEnergyCostValueText;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/CD/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EquipCDValueText;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/CPU", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EquipCPUGameObject;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/CPU/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EquipCPUText;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/POW", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EquipPOWGameObject;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/POW/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EquipPOWText;
        [AutoBind("./DetailInfoGroup/EquipGroup/Scroll View/Viewport/Content/AssembleTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EquipCPUAndPOWTitle;
        [AutoBind("./DetailInfoGroup/ComponentGroup/PropertyList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ComponentGroupRoot;
        [AutoBind("./DetailInfoGroup/ComponentGroup/NegativePropertyList", AutoBindAttribute.InitState.NotInit, false)]
        public Transform NegativePropGroupRoot;
        [AutoBind("./DetailInfoGroup/ComponentGroup/NegativeTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NegativePropGroupTitle;
        [AutoBind("./DetailInfoGroup/ComponentGroup/CPU", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ComponentCPUGameObject;
        [AutoBind("./DetailInfoGroup/ComponentGroup/CPU/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ComponentCPUText;
        [AutoBind("./DetailInfoGroup/ComponentGroup/POW", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ComponentPOWGameObject;
        [AutoBind("./DetailInfoGroup/ComponentGroup/POW/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ComponentPOWText;
        [AutoBind("./DetailInfoGroup/ComponentGroup/AssembleTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ComponentCPUAndPOWTitle;
        [AutoBind("./DetailInfoGroup/NormalItem/Desc/Scroll View/Viewport/Content/InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text Desc4NormalItem;
        [AutoBind("./DetailInfoGroup/BlueprintGroup/Time/DetailGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeCostText;
        [AutoBind("./DetailInfoGroup/BlueprintGroup/Time/Number/DetailGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProduceCountText;
        [AutoBind("./DetailInfoGroup/BlueprintGroup/MaterialListGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MaterialItemRoot;
        [AutoBind("./DetailInfoGroup/BlueprintGroup/MaterialListGroup/MaterialInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MaterialItem;
        [AutoBind("./DetailInfoGroup/BlueprintGroup/DependeceTechList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DependeceTechItemRoot;
        [AutoBind("./DetailInfoGroup/GuidBlueprintGroup/Time/DetailGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildBlueprintTimeCostText;
        [AutoBind("./DetailInfoGroup/GuidBlueprintGroup/Time/Number/DetailGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildBlueprintProduceCountText;
        [AutoBind("./DetailInfoGroup/GuidBlueprintGroup/UnlockConditionGroup/SovereignGalaxyCountCondition", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SovereignGalaxyCountConditionStateCtrl;
        [AutoBind("./DetailInfoGroup/GuidBlueprintGroup/UnlockConditionGroup/SovereignGalaxyCountCondition/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SovereignGalaxyCountText;
        [AutoBind("./DetailInfoGroup/GuidBlueprintGroup/UnlockConditionGroup/SovereignGalaxyCountCondition/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SovereignGalaxyTitleText;
        [AutoBind("./DetailInfoGroup/CrackInfo/CrackTime/Time/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackTimeText;
        [AutoBind("./DetailInfoGroup/CrackInfo/Reward/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RewardGetCount;
        [AutoBind("./DetailInfoGroup/CrackInfo/Scroll View/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackBocDesc;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/Speed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SpeedText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/Store/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StoreText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/TeleportRadius/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TeleportRadiusText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/Shield/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldValueText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/Amor/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmorValueText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/DamageCompose/LightingDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldElectricValueText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/DamageCompose/EnergyDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldKineticValueText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/DamageCompose/FireDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldHeatValueText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/VolumnRatio/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text VolumnValueText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/JumpStable/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text JumpSteadyValueText;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/DamageCompose/LightingDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldElectricProgBar;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/DamageCompose/EnergyDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldKineticProgBar;
        [AutoBind("./DetailInfoGroup/MessageShipGroup/DamageCompose/FireDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldHeatProgBar;
        private bool m_isInInputEventTrigger;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_SetItemInfoUIMode;
        private static DelegateBridge __Hotfix_SetBtnMode4UIMode;
        private static DelegateBridge __Hotfix_SetBtnPos;
        private static DelegateBridge __Hotfix_ShowBlackBackGround;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_GetLongPressValueChangSpeed;
        private static DelegateBridge __Hotfix_UpateShipInfo;
        private static DelegateBridge __Hotfix_SetCommonItemInfo;
        private static DelegateBridge __Hotfix_SetButtonState;
        private static DelegateBridge __Hotfix_CreatePropertyItemPool;
        private static DelegateBridge __Hotfix_AllocDynamicPropretyItem;
        private static DelegateBridge __Hotfix_FreeDynamicPropertyItem;
        private static DelegateBridge __Hotfix_OnNewItemCreateInPool;
        private static DelegateBridge __Hotfix_PreparePropertyItemListForUpdate;
        private static DelegateBridge __Hotfix_ShowShipItemInfo;
        private static DelegateBridge __Hotfix_ShowWeaponItemInfo;
        private static DelegateBridge __Hotfix_SetCpuAndPowerInfo;
        private static DelegateBridge __Hotfix_ShowAmmoItemInfo;
        private static DelegateBridge __Hotfix_ShowMissileItemInfo;
        private static DelegateBridge __Hotfix_ShowDroneItemInfo;
        private static DelegateBridge __Hotfix_UpdateDamageComposeUI;
        private static DelegateBridge __Hotfix_ShowNormalItemInfo;
        private static DelegateBridge __Hotfix_ShowMineralAndOtherItemInfo;
        private static DelegateBridge __Hotfix_ShowChipItemInfo;
        private static DelegateBridge __Hotfix_ShowEquipItemInfo;
        private static DelegateBridge __Hotfix_ShowComponetItemInfo;
        private static DelegateBridge __Hotfix_ShowProduceBlueprintItemInfo;
        private static DelegateBridge __Hotfix_ShowGuildProduceTempleteInfo;
        private static DelegateBridge __Hotfix_ShowCrackItemInfo;
        private static DelegateBridge __Hotfix_UpdateMaterialListInfo;
        private static DelegateBridge __Hotfix_UpdateTechDepenceInfo;
        private static DelegateBridge __Hotfix_UpdateGuildDependeceInfo;
        private static DelegateBridge __Hotfix_ShowOnlyMoveToShipUI;
        private static DelegateBridge __Hotfix_ShowMoveToShipAndDetailUI;
        private static DelegateBridge __Hotfix_ShowOnlyMoveToStoreUI;
        private static DelegateBridge __Hotfix_ShowMoveToStoreAndDetailUI;
        private static DelegateBridge __Hotfix_ShowOnlyDestroyUI;
        private static DelegateBridge __Hotfix_ShowOnlyDetailUI;
        private static DelegateBridge __Hotfix_ShowDestroyAndDetailUI;
        private static DelegateBridge __Hotfix_ShowNoButtonUI;
        private static DelegateBridge __Hotfix_ShowDetailAndMoveToHangarUI;
        private static DelegateBridge __Hotfix_ShowItemPreviewAndProduceUI;
        private static DelegateBridge __Hotfix_ShowItemPreviewAndTechUI;
        private static DelegateBridge __Hotfix_ShowItemPreviewAndChooseUI;
        private static DelegateBridge __Hotfix_ShowAddToCrackListUI;
        private static DelegateBridge __Hotfix_ShowGoToCrackUI;
        private static DelegateBridge __Hotfix_ShowUseMode;
        private static DelegateBridge __Hotfix_ShowItemDetailAndChangeUI;
        private static DelegateBridge __Hotfix_InitCountSliderMinMaxValue;
        private static DelegateBridge __Hotfix_SetCurrentSelectedCount;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedCount;
        private static DelegateBridge __Hotfix_OnItemCountInputFieldChange;
        private static DelegateBridge __Hotfix_OnItemInfoSliderValueChanged;
        private static DelegateBridge __Hotfix_OnItemCountInputFieldSubmit;
        private static DelegateBridge __Hotfix_SetItemCountInputFieldText;
        private static DelegateBridge __Hotfix_CreateSimpleInfoWindowProcess;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemCountInputSubmit;
        private static DelegateBridge __Hotfix_remove_EventOnItemCountInputSubmit;

        public event Action<int> EventOnGotoTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemCountInputSubmit
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private GameObject AllocDynamicPropretyItem()
        {
        }

        [MethodImpl(0x8000)]
        private void CreatePropertyItemPool(GameObject templateItem)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateSimpleInfoWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        private void FreeDynamicPropertyItem(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public int GetLongPressValueChangSpeed(float longPressTime)
        {
        }

        [MethodImpl(0x8000)]
        public void InitCountSliderMinMaxValue(long maxValue, long minValue = 1L, long initValue = 1L)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCountInputFieldChange(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCountInputFieldSubmit(string text)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemInfoSliderValueChanged(float value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewItemCreateInPool(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void PreparePropertyItemListForUpdate(Transform root, int propertyNumber)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBtnMode4UIMode(ItemInfoUIMode mode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBtnPos(ItemInfoButtonPosition posType)
        {
        }

        [MethodImpl(0x8000)]
        private void SetButtonState(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCommonItemInfo(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCpuAndPowerInfo(object config, StoreItemType type, bool isComponet = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrentSelectedCount(long count)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetItemCountInputFieldText(long itemCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemInfoUIMode(ItemInfoUIMode mode, ItemInfoButtonPosition pos = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 worldPos)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowAddToCrackListUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowAmmoItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBlackBackGround(bool isshow)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowChipItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowComponetItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowCrackItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDestroyAndDetailUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDetailAndMoveToHangarUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDroneItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowEquipItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowGoToCrackUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowGuildProduceTempleteInfo(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemDetailAndChangeUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemPreviewAndChooseUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemPreviewAndProduceUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemPreviewAndTechUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMineralAndOtherItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMissileItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMoveToShipAndDetailUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowMoveToStoreAndDetailUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNoButtonUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNormalItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOnlyDestroyUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOnlyDetailUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOnlyMoveToShipUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOnlyMoveToStoreUI()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowProduceBlueprintItemInfo(bool showGoTechButton, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowShipItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowUseMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowWeaponItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void UpateShipInfo(LBStaticHiredCaptain.ShipInfo captainShip, Dictionary<string, UnityEngine.Object> resDict, bool showOpenAnim)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrentSelectedCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDamageComposeUI(List<float> damageComposeList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildDependeceInfo(ConfigDataGuildProduceTempleteInfo config, List<int> errorCodeList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(ILBStoreItemClient item, Dictionary<string, UnityEngine.Object> resDict, bool isAutoShow = true, bool canShare = true, bool showGoRelativeTechButton = true)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMaterialListInfo(List<CostInfo> costItemInfoList, bool is4Guild, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTechDepenceInfo(ConfigDataProduceBlueprintInfo conf, bool showGoTechButton, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public enum ItemInfoButtonPosition
        {
            Up,
            Down
        }

        public enum ItemInfoUIMode
        {
            OnlyMoveToShip,
            MoveToShipAndDetail,
            OnlyMoveToStore,
            MoveToStoreAndDetail,
            OnlyDestroy,
            OnlyDetail,
            DestroyAndDetail,
            NoButton,
            MoveToHangarAndDetail,
            PreviewAndProduce,
            PreviewAndTech,
            PreviewAndChoose,
            AddToCrackList,
            GoToCrack,
            Use,
            DetailAndChange
        }
    }
}

