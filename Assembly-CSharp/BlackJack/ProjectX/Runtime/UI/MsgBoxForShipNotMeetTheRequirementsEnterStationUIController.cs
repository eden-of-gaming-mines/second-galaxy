﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class MsgBoxForShipNotMeetTheRequirementsEnterStationUIController : UIControllerBase
    {
        protected Dictionary<ShipType, CommonUIStateController> m_allowInShipTypeDict;
        private const string AllowInImageUIState_AllowIn = "AllowIn";
        private const string AllowInImageUIState_NotAllowIn = "NotAllowIn";
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainCtrl;
        [AutoBind("./Detail/InfoGroup/JumpDistanceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_jumpDistanceText;
        [AutoBind("./Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./Detail/InfoGroup/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowTypeButton;
        [AutoBind("./Detail/AllowInShipGroupUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_allowInShipGroupUIDummy;
        [AutoBind("./Detail/InfoGroup/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowTypeFrigate;
        [AutoBind("./Detail/InfoGroup/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowTypeDestroyer;
        [AutoBind("./Detail/InfoGroup/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowTypeCruiser;
        [AutoBind("./Detail/InfoGroup/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowTypeBattleCruiser;
        [AutoBind("./Detail/InfoGroup/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowTypeBattleShip;
        [AutoBind("./Detail/InfoGroup/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowTypeIndustryShip;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnShowMsgBoxStart;
        private static DelegateBridge __Hotfix_UpdateAllowInShipTypeList;
        private static DelegateBridge __Hotfix_CreateMainWindownEffectInfo;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateMainWindownEffectInfo(bool isShow = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnShowMsgBoxStart(GDBSolarSystemInfo solarSystemInfo, int spaceSationId, int distance, bool isJumpToBase, List<ShipType> shipTypeList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAllowInShipTypeList(List<ShipType> shipTypeList)
        {
        }
    }
}

