﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class ReturnRewardUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool> EventOnRequestOverSeaActivityManagerUIPause;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRefreshRedPoint;
        private bool m_ntfTriggeredRewardRefresh;
        private List<DailyLoginRewardInfo> m_loginRewardList;
        private ReturnRewardUIController m_returnRewardUICtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private static string ParamKeyIsShow;
        private static string ParamKeyIsHide;
        private static string ParamKeyIsPlayAniImmedite;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTaskWithPrepare;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_ClearAllContextAndRes;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ShowReturnRewardUI;
        private static DelegateBridge __Hotfix_HideReturnRewardUI;
        private static DelegateBridge __Hotfix_SendGetRewardReq;
        private static DelegateBridge __Hotfix_OnPlayerActivityInfoNtf;
        private static DelegateBridge __Hotfix_OnGetRewardBtnClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_PauseGiftSimpleInfoPanel;
        private static DelegateBridge __Hotfix_GetLoginRewardInfoByDayIndex;
        private static DelegateBridge __Hotfix_GetReturnRewardPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_Tick4LoginReward;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnRequestOverSeaActivityManagerUIPause;
        private static DelegateBridge __Hotfix_remove_EventOnRequestOverSeaActivityManagerUIPause;
        private static DelegateBridge __Hotfix_add_EventOnRefreshRedPoint;
        private static DelegateBridge __Hotfix_remove_EventOnRefreshRedPoint;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnRefreshRedPoint
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool> EventOnRequestOverSeaActivityManagerUIPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public ReturnRewardUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearAllContextAndRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public DailyLoginRewardInfo GetLoginRewardInfoByDayIndex(int dayIndex)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetReturnRewardPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public void HideReturnRewardUI(bool immedite, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetRewardBtnClick(ConfigDataDailyLoginInfo rewardCfgInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerActivityInfoNtf(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void PauseGiftSimpleInfoPanel(Action evntAfterPause, bool isIgnoreAnim = false)
        {
        }

        [MethodImpl(0x8000)]
        private void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGetRewardReq(int day, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ILBStoreItemClient rewardItem, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowReturnRewardUI(bool immedite, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTaskWithPrepare(Action<bool> onPrepareEnd, Action onResLoadEnd = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void Tick4LoginReward()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideReturnRewardUI>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal ReturnRewardUITask $this;

            internal void <>m__0(UIProcess p, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnGetRewardBtnClick>c__AnonStorey2
        {
            internal ConfigDataDailyLoginInfo rewardCfgInfo;
            internal ReturnRewardUITask $this;

            internal void <>m__0(bool ret)
            {
                if (ret)
                {
                    if (this.rewardCfgInfo.ID != 7)
                    {
                        TipWindowUITask.ShowTipWindow(QuestUIHelper.GetRewardStringWithInfo(OverSeaActivityUIHelper.ConvertLoginRewardInfo2QuestRewardInfo(this.rewardCfgInfo)), false);
                    }
                    this.$this.EnablePipelineStateMask(ReturnRewardUITask.PipeLineStateMaskType.Refresh);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                    if (this.$this.EventOnRefreshRedPoint != null)
                    {
                        this.$this.EventOnRefreshRedPoint();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGetRewardReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                LoginRewardReqNetTask task2 = task as LoginRewardReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.m_result == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey3
        {
            internal ILBStoreItemClient rewardItem;
            internal Vector3 pos;
            internal ReturnRewardUITask $this;

            internal void <>m__0(UIIntent intent)
            {
                string mode = "OnlyDetail";
                if (!ClientStoreItemBaseHelper.IsItemHasDetailInfo(this.rewardItem))
                {
                    mode = "NoButton";
                }
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.rewardItem, intent, true, this.pos, mode, ItemSimpleInfoUITask.PositionType.UseInput, false, null, null, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            IsShow,
            IsHide,
            IsPlayAniImmedite,
            Refresh
        }
    }
}

