﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonUIEffectGradual : Graphic
    {
        [Header("绘制方向，手动指定")]
        public DrawingDirection m_drawingDirection;
        private List<KeyValuePair<float, Color>> m_drawingColorInfoList;
        public ColorInfoForTest[] m_colorInfoForTestList;
        [CompilerGenerated]
        private static Comparison<KeyValuePair<float, Color>> <>f__am$cache0;
        private static DelegateBridge __Hotfix_SetUIGradualColorList;
        private static DelegateBridge __Hotfix_OnPopulateMesh;
        private static DelegateBridge __Hotfix_DrawGradualColorToUI;
        private static DelegateBridge __Hotfix_GetUIVertex;

        [MethodImpl(0x8000)]
        private void DrawGradualColorToUI(VertexHelper vh)
        {
        }

        [MethodImpl(0x8000)]
        private UIVertex GetUIVertex(Vector2 point, Color color)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPopulateMesh(VertexHelper vh)
        {
        }

        [MethodImpl(0x8000), ExecuteInEditMode]
        public void SetUIGradualColorList(List<KeyValuePair<float, Color>> colorList)
        {
        }

        [Serializable]
        public class ColorInfoForTest
        {
            public float m_colorStartPos;
            public Color m_color;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        public enum DrawingDirection
        {
            Horizontal,
            Vertical
        }
    }
}

