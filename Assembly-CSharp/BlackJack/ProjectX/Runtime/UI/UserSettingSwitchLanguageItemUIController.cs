﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class UserSettingSwitchLanguageItemUIController : UIControllerBase
    {
        private ConfigDataLanguageInfo m_languageTypeInfo;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<LanguageType> EventOnLanguageItemClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_languageToggle;
        [AutoBind("./LanguageText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_languageText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnLanguageToggleSelected;
        private static DelegateBridge __Hotfix_UpdateItemData;
        private static DelegateBridge __Hotfix_UpdateLanguageNameField;
        private static DelegateBridge __Hotfix_Force2Select;
        private static DelegateBridge __Hotfix_add_EventOnLanguageItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnLanguageItemClick;

        public event Action<LanguageType> EventOnLanguageItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Force2Select()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLanguageToggleSelected(UIControllerBase obj, bool b)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemData(ConfigDataLanguageInfo data)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLanguageNameField()
        {
        }
    }
}

