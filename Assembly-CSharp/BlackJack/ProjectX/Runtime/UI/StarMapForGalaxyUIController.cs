﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapForGalaxyUIController : UIControllerBase
    {
        protected List<StarMapStarfieldNodeUIController> m_starfieldNodeList;
        [HideInInspector]
        public StarMapOrientedTargetSelectWndUIController m_orientedTargetSelectWndUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Panel/ButtonGroup/OrientationButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_orientationButton;
        [AutoBind("./Panel/ButtonGroup/SignalSearchButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_signalSearchButton;
        [AutoBind("./Panel/ButtonGroup/MainAndBackButtonGroup/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backButton;
        [AutoBind("./Panel/ButtonGroup/MainAndBackButtonGroup/MainButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_mainButton;
        [AutoBind("./Panel/WndGroup/OrientedTargetSelectWndUIPrefabRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_orientedTargetSelectWndUIPrefabRoot;
        [AutoBind("./Panel/StarfieldNodeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starfieldNodeGroup;
        private static DelegateBridge __Hotfix_ClearAllStarfieldNodeFromStarMap;
        private static DelegateBridge __Hotfix_AddNewStarfieldNodeToStarMap;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_CreateMainUIProcess;

        [MethodImpl(0x8000)]
        public void AddNewStarfieldNodeToStarMap(int starfieldId, string starfieldName, float securityLevel, float posX, float posY, Action<UIControllerBase> onStarfieldNodeClick)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearAllStarfieldNodeFromStarMap()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }
    }
}

