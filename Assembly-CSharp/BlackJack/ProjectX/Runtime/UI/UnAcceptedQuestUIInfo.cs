﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class UnAcceptedQuestUIInfo
    {
        private readonly int m_configId;
        private readonly ConfigDataQuestInfo m_questInfo;
        private readonly int m_level;
        private readonly int m_sceneSolarsytemId;
        private readonly int m_factionId;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_get_QuestType;
        private static DelegateBridge __Hotfix_get_ConfigId;
        private static DelegateBridge __Hotfix_get_ConfigInfo;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_get_SceneSolarSystemId;
        private static DelegateBridge __Hotfix_get_FactionId;
        private static DelegateBridge __Hotfix_op_Equality;
        private static DelegateBridge __Hotfix_op_Inequality;
        private static DelegateBridge __Hotfix_Equals;
        private static DelegateBridge __Hotfix_GetHashCode;

        [MethodImpl(0x8000)]
        public UnAcceptedQuestUIInfo(QuestWaitForAcceptInfo waitForAcceptInfo)
        {
        }

        [MethodImpl(0x8000)]
        public UnAcceptedQuestUIInfo(int configId, ConfigDataQuestInfo configInfo = null, int level = 0)
        {
        }

        [MethodImpl(0x8000)]
        public override bool Equals(object obj)
        {
        }

        [MethodImpl(0x8000)]
        public override int GetHashCode()
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator ==(UnAcceptedQuestUIInfo infoA, UnAcceptedQuestUIInfo infoB)
        {
        }

        [MethodImpl(0x8000)]
        public static bool operator !=(UnAcceptedQuestUIInfo infoA, UnAcceptedQuestUIInfo infoB)
        {
        }

        public BlackJack.ConfigData.QuestType QuestType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int ConfigId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ConfigDataQuestInfo ConfigInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int SceneSolarSystemId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int FactionId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

