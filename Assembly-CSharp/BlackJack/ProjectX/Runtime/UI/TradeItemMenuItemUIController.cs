﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class TradeItemMenuItemUIController : CommonMenuItemUIController
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ItemToggle;
        [AutoBind("./ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ClickImage;
        [AutoBind("./ClickButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text ItemNameText;
        [AutoBind("./NumberGroup/MumberText", AutoBindAttribute.InitState.NotInit, false)]
        private Text NumberText;
        [AutoBind("./NumberGroup", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_numStateUICtrl;
        [AutoBind("./FactionCreditGroup", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController FactionCreditGroupCtrl;
        [AutoBind("./FactionCreditGroup/TextGroup/HintText", AutoBindAttribute.InitState.NotInit, false)]
        private Text FactionCreditLevelText;
        private static DelegateBridge __Hotfix_GetItemData;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnToggleClick;
        private static DelegateBridge __Hotfix_OnButtonClick;
        private static DelegateBridge __Hotfix_OnSelectImp;
        private static DelegateBridge __Hotfix_OnMenuItemFill;
        private static DelegateBridge __Hotfix_SwitchMenuItemExpandImp;
        private static DelegateBridge __Hotfix_UpdateMenuItem;
        private static DelegateBridge __Hotfix_OnFreeToUnusedPool;
        private static DelegateBridge __Hotfix_OnPoolObjectReturnPool;
        private static DelegateBridge __Hotfix_UpdateOwnNum;
        private static DelegateBridge __Hotfix_UpdateFactionCredit;
        private static DelegateBridge __Hotfix_UpdateClickState;

        [MethodImpl(0x8000)]
        public TradeItemMenuItemData GetItemData()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFreeToUnusedPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnMenuItemFill(object data, Dictionary<string, Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPoolObjectReturnPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSelectImp()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnToggleClick(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SwitchMenuItemExpandImp()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateClickState(TradeItemMenuItemData itemData)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFactionCredit(TradeItemMenuItemData itemData)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMenuItem(TradeItemMenuItemData itemData)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateOwnNum(TradeItemMenuItemData itemData)
        {
        }
    }
}

