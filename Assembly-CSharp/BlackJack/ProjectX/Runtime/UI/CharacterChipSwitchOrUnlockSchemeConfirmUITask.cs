﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class CharacterChipSwitchOrUnlockSchemeConfirmUITask : UITaskBase
    {
        public const string TaskModeSwitch = "Switch";
        public const string TaskModeUnLock = "UnLock";
        public static string ParamKeyChipSchemeIndex;
        public static string ParamKeySwitchCost;
        public static string ParamKeyCancelAction;
        public static string ParamKeyConfirmAction;
        private int m_chipSchemeIndex;
        private int m_cost;
        private Action m_confirmAction;
        private Action m_cancelAction;
        private CharacterChipSwitchOrUnlockSchemeConfirmUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCharacterChipSwitchSchemeConfirmUITask;
        private static DelegateBridge __Hotfix_StartCharacterChipUnLockConfirmUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CharacterChipSwitchOrUnlockSchemeConfirmUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static CharacterChipSwitchOrUnlockSchemeConfirmUITask StartCharacterChipSwitchSchemeConfirmUITask(int schemeIndex, Action onConfrimAction, Action onCancelAction)
        {
        }

        [MethodImpl(0x8000)]
        public static CharacterChipSwitchOrUnlockSchemeConfirmUITask StartCharacterChipUnLockConfirmUITask(int schemeIndex, int cost, Action onConfrimAction, Action onCancelAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

