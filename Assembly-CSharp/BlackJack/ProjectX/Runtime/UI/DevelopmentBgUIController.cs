﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using TouchScript.Gestures;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class DevelopmentBgUIController : UIControllerBase
    {
        private const string TriggerIdle = "Conversion_Idle";
        private const string TriggerProcess = "Conversion_Ready";
        private const string TriggerReady = "Conversion_ReadyDone";
        private const string TriggerTransformation = "Conversion_Activate";
        [AutoBind("./AnimControl/Canvas/Image_BaseMap", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_resultItemBgImage;
        [AutoBind("./AnimControl", AutoBindAttribute.InitState.NotInit, false)]
        public UIAnimationEventHandler m_animationHandler;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TapGesture m_tapGesture;
        [AutoBind("./Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        [AutoBind("./AnimControl/Canvas/Image_CornerMark ", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_resultItemCornerMarkImage;
        [AutoBind("./AnimControl/Canvas/Image_Object", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_resultItemImage;
        [AutoBind("./AnimControl/Canvas/Image_PreView", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_previewImage;
        [AutoBind("./AnimControl", AutoBindAttribute.InitState.NotInit, false)]
        public Animator m_animator;
        private static DelegateBridge __Hotfix_UpdateAnimationState;
        private static DelegateBridge __Hotfix_UpdatePreivewIcon;
        private static DelegateBridge __Hotfix_UpdateResultInfo;

        [MethodImpl(0x8000)]
        public void UpdateAnimationState(DevelopmentBgUITask.BackGroundStateType type)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePreivewIcon(string previewIconPath, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateResultInfo(FakeLBStoreItem resultItemInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

