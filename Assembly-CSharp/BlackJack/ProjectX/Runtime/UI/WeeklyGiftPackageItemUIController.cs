﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class WeeklyGiftPackageItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<QuestRewardInfo, int, float> EventOnRewardItemClick;
        private Action<int> EventOnBuyButtonClick;
        private DateTime m_nextUpdateTime;
        private DateTime m_nextUpdateDayTime;
        private const string GiftRewardItemName = "WeeklyGiftPackageRewardItem";
        private LBRechargeGiftPackage m_lbGiftPackage;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        [AutoBind("./ButtonGroup/BuyLimit/NoLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NoBuyLimit;
        [AutoBind("./ButtonGroup/BuyLimit", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BuyLimitAmountCtrl;
        [AutoBind("./ButtonGroup/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buyBtnCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollItemCtrl;
        [AutoBind("./ButtonGroup/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./ButtonGroup/BuyButton/BuyGroup/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PriceText;
        [AutoBind("./ButtonGroup/BuyButton/BuyGroup/IconGroup/IconText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrencyText;
        [AutoBind("./ButtonGroup/TimeTextGroup/OutDateDayText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PackageOutDateDayText;
        [AutoBind("./ButtonGroup/TimeTextGroup/OutDateText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText PackageOutDateText;
        [AutoBind("./ButtonGroup/BuyLimit/LeftCountValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PurchaseLeftCountValueText;
        [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonGroupStateCtrl;
        [AutoBind("./RewardItemList", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool RewardItemObjectPool;
        [AutoBind("./RewardItemList", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect RewardItemListLoopScrollRecet;
        [AutoBind("./TotalReward/DescText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TotalRewardValueText;
        [AutoBind("./ImmediateReward/TitleText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ImmediateRewardValueText;
        [AutoBind("./DiscountRoot/DiscountVauleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DiscountVauleText;
        [AutoBind("./GiftPackageTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GiftPackageTitleText;
        [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BgImage;
        [CompilerGenerated]
        private static Func<QuestRewardInfo, bool> <>f__am$cache0;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegistBuyButtonClickEvent;
        private static DelegateBridge __Hotfix_UpdatePackageItem;
        private static DelegateBridge __Hotfix_UpdateWeelyGiftPackageRewardList;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitGiftPackageRewardItemPool;
        private static DelegateBridge __Hotfix_OnGiftPackageRewardItemCreated;
        private static DelegateBridge __Hotfix_OnGiftPackageRewardItemFill;
        private static DelegateBridge __Hotfix_OnGiftPackageRewardItemClick;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateBuyBtnAndLimitState;
        private static DelegateBridge __Hotfix_OnBuyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;

        public event Action<QuestRewardInfo, int, float> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void InitGiftPackageRewardItemPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageRewardItemCreated(string key, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageRewardItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegistBuyButtonClickEvent(Action<int> onButtonClick)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBuyBtnAndLimitState(int left, int limitCount, string price, string currencyType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePackageItem(LBRechargeGiftPackage package, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeelyGiftPackageRewardList(int count)
        {
        }
    }
}

