﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapRightPopupWndUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect m_viewItemScrollView;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_scrollViewStateCtrl;
        [AutoBind("./Viewport", AutoBindAttribute.InitState.NotInit, false)]
        public RectMask2D m_scrollRectMask;
        [AutoBind("./Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_scrollViewContentTrans;
        private static DelegateBridge __Hotfix_ScrollToBottom;
        private static DelegateBridge __Hotfix_ScrollToTop;
        private static DelegateBridge __Hotfix_SetMenuItemScrollPos;
        private static DelegateBridge __Hotfix_SetScrollViewValid;
        private static DelegateBridge __Hotfix_GetScrollViewPosOnValid;
        private static DelegateBridge __Hotfix_SetMenuItemScrollToViewBottom;
        private static DelegateBridge __Hotfix_SetScrollViewVisible;

        [MethodImpl(0x8000)]
        public Vector3 GetScrollViewPosOnValid()
        {
        }

        [MethodImpl(0x8000)]
        public void ScrollToBottom()
        {
        }

        [MethodImpl(0x8000)]
        public void ScrollToTop()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuItemScrollPos(float pos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuItemScrollToViewBottom(MenuItemUIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetScrollViewValid(bool isValid)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess SetScrollViewVisible(bool isVisible, bool isImmediateHideOrShow = false)
        {
        }
    }
}

