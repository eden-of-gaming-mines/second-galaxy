﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ChapterStoryUIController : UIControllerBase
    {
        private List<ChapterStorySubQuestUIController> m_chapterStorySubQuestUICtrlList;
        private List<CommonItemIconUIController> m_rewardItemUICtrlList;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        private int m_currChapterGoalQuestId;
        public int m_currStoryQuest;
        private const string RewardState_Normal = "Normal";
        private const string RewardState_Gray = "Gray";
        private const string PrefabAsset_SubQuestItem = "SubQuestItem";
        private const string PrefabAsset_RewardItem = "RewardItem";
        private const string PrefabAsset_RewardTipItem = "RewardTip";
        private const int MaxRewardCount = 5;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController, int> EventOnRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, bool> EventOnChapterSubItemButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, bool, bool> EventOnLossWarningButtonClick;
        [AutoBind("./Title/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ChapterGoalNameText;
        [AutoBind("./StoryQuestPanel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./StoryQuestPanel/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button StoryQuestButton;
        [AutoBind("./StoryQuestPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StroyQuestNameText;
        [AutoBind("./StoryQuestPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StoryQuestDescText;
        [AutoBind("./Reward/TextGroup/Value", AutoBindAttribute.InitState.NotInit, false)]
        public Text RewardValueText;
        [AutoBind("./Reward/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button RewardButton;
        [AutoBind("./Reward/RewardItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform RewardItemGroup;
        [AutoBind("./Reward", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardStateCtrl;
        [AutoBind("./SubQuestItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect SubQuestScrollRect;
        [AutoBind("./SubQuestItemGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SubQuestItemGroup;
        [AutoBind("./SubQuestItemGroup/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SubQuestTempleteItemRoot;
        [AutoBind("./ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemSimpleInfoDummy;
        [AutoBind("./RewardTipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform RewardTipDummy;
        [AutoBind("./StoryQuestPanel/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossWarningButton;
        [AutoBind("./LossWarningDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LossWarningDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdataChapterStoryInfo;
        private static DelegateBridge __Hotfix_UpdateChapterSubQuestInfo;
        private static DelegateBridge __Hotfix_GetNewSubQuestItem;
        private static DelegateBridge __Hotfix_UpdateMainQuestInfo;
        private static DelegateBridge __Hotfix_UpdataChapterGoalQuestInfo;
        private static DelegateBridge __Hotfix_UpdateSigleSubQuest;
        private static DelegateBridge __Hotfix_GetLossWarningWindowDummyPos;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnChapterSubItemButtonClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_GetChapterStorySubQuestUIControllerById;
        private static DelegateBridge __Hotfix_SetScrollToTop;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnChapterSubItemButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnChapterSubItemButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;

        public event Action<int, bool> EventOnChapterSubItemButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, bool, bool> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController, int> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public ChapterStorySubQuestUIController GetChapterStorySubQuestUIControllerById(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLossWarningWindowDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        private ChapterStorySubQuestUIController GetNewSubQuestItem()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChapterSubItemButtonClick(int questId, bool isBranchQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetScrollToTop()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdataChapterGoalQuestInfo(LogicBlockQuestBase m_lbQust, int waitOrAlreadyCompelteCount, LBProcessingQuestBase chapterGoalquest, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdataChapterStoryInfo(LogicBlockQuestBase lbQust, int waitOrAlreadyCompelteCount, LBProcessingQuestBase chapterGoalquest, int storyQuestId, List<int> subQuestList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChapterSubQuestInfo(LogicBlockQuestBase lbQust, List<int> subQuestList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMainQuestInfo(LogicBlockQuestBase m_lbQust, int storyQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSigleSubQuest(LogicBlockQuestBase m_lbQust, List<int> subQuestList, int questID, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

