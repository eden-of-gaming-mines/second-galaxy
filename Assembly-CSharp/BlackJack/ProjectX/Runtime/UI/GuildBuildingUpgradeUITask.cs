﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public sealed class GuildBuildingUpgradeUITask : GuildUITaskBase
    {
        private GuildBuildingUpgradeUIController m_guildBuildingUpgradeUIController;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private GuildBuildingInfo m_guildBuildingInfo;
        private int m_solarSystemId;
        private Action<bool> m_actionOnUpgradeConfirm;
        public const string ParamKeyGuildBuildingInfo = "GuildBuildingInfo";
        public const string ParamKeySolarSystemId = "SolarSystemId";
        public const string ParamKeyActionOnUpgradeConfirm = "ActionOnUpgradeConfirm";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildBuildingUpgradeUITask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnCostItemClick;
        private static DelegateBridge __Hotfix_OnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroupButtonClick;
        private static DelegateBridge __Hotfix_OnTipButtonClick;
        private static DelegateBridge __Hotfix_OnTipWindowBackButtonClick;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildBuildingUpgradeUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroupButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCostItemClick(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipWindowBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildBuildingUpgradeUITask(int solarSystemId, GuildBuildingInfo buildingInfo, Action<bool> onUpgradeConfirm)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }
    }
}

