﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildCreateReqNetworkTask : NetWorkTransactionTask
    {
        private int m_result;
        private readonly string m_guildName;
        private readonly string m_codeName;
        private readonly int m_languageType;
        private readonly int m_joinPllicy;
        private readonly ProGuildLogoInfo m_logoInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildCreateAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public GuildCreateReqNetworkTask(string guildName, string codeName, int languageType, int joinPllicy, ProGuildLogoInfo logoInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCreateAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

