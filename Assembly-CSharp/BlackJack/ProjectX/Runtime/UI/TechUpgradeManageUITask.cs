﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class TechUpgradeManageUITask : UITaskBase
    {
        public static string ParamKey_SelectedTechCategory;
        public static string ParamKey_SelectedTechUIType;
        public static string ParamKey_SelectedTechId;
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private TechUpgradeManageUIController m_mainCtrl;
        private IUIBackgroundManager m_backgroundManager;
        private TechUpgradeSimpleInfoUIController m_simpleInfoPanelCtrl;
        private int m_selTechCategoryId;
        private int m_selTechUITypeId;
        private int m_selTechId;
        private bool m_isClearIntentParam;
        private List<ConfigDataTechInfo> m_currTechConfigInfoList;
        private Dictionary<TechType, List<ConfigDataTechUITypeInfo>> m_techCategoryToTechUITypeListInfoDict;
        private Dictionary<TechType, int> m_category2TotalTechLevelCountDict;
        private Dictionary<TechType, int> m_category2LearntTechLevelCountDict;
        private Dictionary<int, int> m_techType2TotalTechLevelCountDict;
        private bool m_isUpdatingView;
        private ItemSimpleInfoUITask m_itemSimpleInfoTask;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string Mode_Normal = "NormalMode";
        public const string Mode_SimpleInfo = "SimpleInfoMode";
        private const int MinSpeedUpItemIndex = 2;
        public const string TaskName = "TechUpgradeManageUITask";
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTechUpgradeManageUITask;
        private static DelegateBridge __Hotfix_GetTechCategoryAndTypeById;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitlizeTechCategoryAndLevelCountInfo;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_GetHideTechTreeAndShowTechOverViewProcess;
        private static DelegateBridge __Hotfix_GetHideOverviewAndShowTechTreeProcess;
        private static DelegateBridge __Hotfix_GetSpeedUpItemIdWithIndex;
        private static DelegateBridge __Hotfix_GetCacheDataFromIntent;
        private static DelegateBridge __Hotfix_SendSpeedUpWithItemNetworkReq;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_GetAllTechInfoByTechType;
        private static DelegateBridge __Hotfix_OnModelButtonClick;
        private static DelegateBridge __Hotfix_OnCategoryToggleSelected;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_CloseUpgradePanel;
        private static DelegateBridge __Hotfix_OnTechCategoryToggleClick;
        private static DelegateBridge __Hotfix_OnTechTypeToggleClick;
        private static DelegateBridge __Hotfix_OnTechTreeNodeClick;
        private static DelegateBridge __Hotfix_OnTechTreeNodeClickImp;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnBGButtonClick;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnUnlockItemButtonClick;
        private static DelegateBridge __Hotfix_StartTechUpgradeSpeedUpUITask;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnStopButtonClick;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnUpgradeButtonClickImp;
        private static DelegateBridge __Hotfix_SimpleInfoPanel_OnUpgradeInfoBGButtonClick;
        private static DelegateBridge __Hotfix_OnTechUpgradeNtf;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetWeaponCatagrayRectTrans;
        private static DelegateBridge __Hotfix_GetMissleToggleRectTrans;
        private static DelegateBridge __Hotfix_GetCatagarayRectTansform;
        private static DelegateBridge __Hotfix_GetTypeRectTansform;
        private static DelegateBridge __Hotfix_ExpendCatagray;
        private static DelegateBridge __Hotfix_SelectType;
        private static DelegateBridge __Hotfix_ClickTechNodeByIndex;
        private static DelegateBridge __Hotfix_ClickUpdateButton;

        [MethodImpl(0x8000)]
        public TechUpgradeManageUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickTechNodeByIndex(int index, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickUpdateButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseUpgradePanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void ExpendCatagray(int catagray, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public List<ConfigDataTechInfo> GetAllTechInfoByTechType(int techUIType)
        {
        }

        [MethodImpl(0x8000)]
        private void GetCacheDataFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCatagarayRectTansform(TechType catagray)
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetHideOverviewAndShowTechTreeProcess()
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetHideTechTreeAndShowTechOverViewProcess(bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetMissleToggleRectTrans()
        {
        }

        [MethodImpl(0x8000)]
        public static int GetSpeedUpItemIdWithIndex(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetTechCategoryAndTypeById(int techId, out int techTypeCategory, out int techUIType)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetTypeRectTansform(TechType catagray, int index)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetWeaponCatagrayRectTrans()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitlizeTechCategoryAndLevelCountInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCategoryToggleSelected(int category)
        {
        }

        [MethodImpl(0x8000)]
        private void OnModelButtonClick(TechType category)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechCategoryToggleClick(TechCategoryToggleUIController categoryCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechTreeNodeClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechTreeNodeClickImp(int techId, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechTypeToggleClick(int techTypeId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechUpgradeNtf(int techId, int techLevel, bool isByTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SelectType(int index, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendSpeedUpWithItemNetworkReq(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnDetailButtonClick(ConfigDataTechInfo techInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnSpeedUpButtonClick(ConfigDataTechInfo techInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnSpeedUpItemClick(int itemIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnStopButtonClick(ConfigDataTechInfo techInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnUnlockItemButtonClick(StoreItemType itemType, int itemId, Vector3 itemPos, Vector2 itemSize)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnUpgradeButtonClick(ConfigDataTechInfo techInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnUpgradeButtonClickImp(ConfigDataTechInfo techInfo, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SimpleInfoPanel_OnUpgradeInfoBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public static TechUpgradeManageUITask StartTechUpgradeManageUITask(UIIntent mainUIIntent, int techId = 0, Action<bool> onPipeLineEnd = null, IUIBackgroundManager backgroundManager = null, string mode = "NormalMode")
        {
        }

        [MethodImpl(0x8000)]
        private void StartTechUpgradeSpeedUpUITask(ConfigDataTechInfo techInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnTechUpgradeNtf>c__AnonStorey5
        {
            internal UIIntentReturnable intent;
            internal TechUpgradeManageUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask(this.intent.PrevTaskIntent, null);
            }
        }

        [CompilerGenerated]
        private sealed class <SimpleInfoPanel_OnBGButtonClick>c__AnonStorey0
        {
            internal UIIntentReturnable intent;
            internal TechUpgradeManageUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask(this.intent.PrevTaskIntent, null);
            }

            internal void <>m__1(UIProcess process, bool result)
            {
                this.$this.EnablePipelineStateMask(TechUpgradeManageUITask.PipeLineStateMaskType.NotPlayAnim);
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <SimpleInfoPanel_OnSpeedUpItemClick>c__AnonStorey1
        {
            internal int itemId;
            internal TechUpgradeManageUITask $this;

            internal void <>m__0()
            {
                this.$this.SendSpeedUpWithItemNetworkReq(this.itemId);
            }

            internal void <>m__1()
            {
                this.$this.EnableUIInput(true);
            }
        }

        [CompilerGenerated]
        private sealed class <SimpleInfoPanel_OnStopButtonClick>c__AnonStorey3
        {
            internal ConfigDataTechInfo techInfo;
            internal TechUpgradeManageUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.m_currIntent.TargetMode = "NormalMode";
                TechUpgradeCancelUITask.StartTechCancelUITask(this.$this.m_currIntent, this.techInfo.ID);
            }
        }

        [CompilerGenerated]
        private sealed class <SimpleInfoPanel_OnUpgradeButtonClickImp>c__AnonStorey4
        {
            internal ConfigDataTechInfo techInfo;
            internal Action<bool> onEnd;
            internal TechUpgradeManageUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                this.$this.m_currIntent.TargetMode = "NormalMode";
                TechUpgradeInfoUITask.StartTechUpgradeInfoUITask(this.$this.m_currIntent, this.techInfo.ID, this.onEnd, this.$this.m_backgroundManager);
            }
        }

        [CompilerGenerated]
        private sealed class <StartTechUpgradeSpeedUpUITask>c__AnonStorey2
        {
            internal ConfigDataTechInfo techInfo;
            internal TechUpgradeManageUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.m_currIntent.TargetMode = "NormalMode";
                TechUpgradeSpeedUpUITask.StartTechUpgradeSpeedUpUITask(this.$this.m_currIntent, this.techInfo.ID);
            }
        }

        protected enum PipeLineStateMaskType
        {
            TechCategoryToggleChanged,
            TechUITypeToggleChanged,
            NotPlayAnim
        }

        public class SimpleInfoTechUIInfo
        {
            public string m_techName;
            public string m_techIconPath;
            public int m_learnLv;
            public int m_maxLv;
            public TechUpgradeInfo m_upgradeInfo;
            public TechUpgradeManageUITask.SimplePanelState m_panelState;
            public ConfigDataTechInfo m_techConfigInfo;
            public bool m_inited;
            private static DelegateBridge _c__Hotfix_ctor;

            public SimpleInfoTechUIInfo(ConfigDataTechInfo techConfigInfo)
            {
                if (techConfigInfo != null)
                {
                    this.m_techConfigInfo = techConfigInfo;
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    this.m_techName = GameManager.Instance.StringTableManager.GetStringInDefaultStringTable(this.m_techConfigInfo.NameStrKey);
                    this.m_techIconPath = CommonLogicUtil.GetIconSpriteResFullPath("Assets/GameProject/RuntimeAssets/UI/UIImages/PassiveSkill/", this.m_techConfigInfo.IconArtResString);
                    this.m_maxLv = this.m_techConfigInfo.LevelInfoIdList.Count;
                    this.m_learnLv = 0;
                    LBTech techById = playerContext.GetLBTech().GetTechById(this.m_techConfigInfo.ID);
                    this.m_learnLv = (techById == null) ? 0 : techById.GetLevel();
                    this.m_upgradeInfo = playerContext.GetLBTech().GetUpgradeInfo(this.m_techConfigInfo.ID);
                    if (this.m_upgradeInfo != null)
                    {
                        this.m_panelState = TechUpgradeManageUITask.SimplePanelState.Upgrade;
                    }
                    else if (this.m_learnLv == this.m_maxLv)
                    {
                        this.m_panelState = TechUpgradeManageUITask.SimplePanelState.FullLevel;
                    }
                    else
                    {
                        int errCode = 0;
                        this.m_panelState = !playerContext.GetLBTech().CheckTechUpgradeValid(this.m_techConfigInfo.ID, this.m_learnLv + 1, out errCode) ? TechUpgradeManageUITask.SimplePanelState.UnSatisfy : TechUpgradeManageUITask.SimplePanelState.Normal;
                    }
                    this.m_inited = true;
                    DelegateBridge bridge = _c__Hotfix_ctor;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, techConfigInfo);
                    }
                }
            }
        }

        public enum SimplePanelState
        {
            Upgrade,
            Normal,
            FullLevel,
            UnSatisfy
        }
    }
}

