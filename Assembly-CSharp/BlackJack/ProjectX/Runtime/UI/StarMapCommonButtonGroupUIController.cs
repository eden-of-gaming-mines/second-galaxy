﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class StarMapCommonButtonGroupUIController : UIControllerBase
    {
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backToPreviousStarMapButton;
        [AutoBind("./OrientationButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_orientationButton;
        [AutoBind("./PVEScanProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_pveScanProbeButton;
        [AutoBind("./PVEScanProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_pveScanProbeCtrl;
        [AutoBind("./PVEScanProbeButton/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_pveScanProbeRecoverTimeText;
        [AutoBind("./PVEScanProbeButton/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_pveScanProbeCountText;
        [AutoBind("./PVEScanProbeButton/CDImageProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_pveCDImageProgressBar;
        [AutoBind("./PVEScanProbeButton/NoneImageProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_pveNoneCDImageProgressBar;
        [AutoBind("./ManualSignalScanProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_manualSignalScanProbeButton;
        [AutoBind("./ManualSignalScanProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_manualSignalScanProbeCtrl;
        [AutoBind("./ManualSignalScanProbeButton/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_manualSignalProbeRecoverTimeText;
        [AutoBind("./ManualSignalScanProbeButton/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_manualSignalProbeCountText;
        [AutoBind("./ManualSignalScanProbeButton/CDImageProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_manualSignalCDImageProgressBar;
        [AutoBind("./ManualSignalScanProbeButton/NoneImageProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_manualSignalNoneCDImageProgressBar;
        [AutoBind("./PVPScanProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_pvpScanProbeButton;
        [AutoBind("./PVPScanProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_pvpScanProbeCtrl;
        [AutoBind("./PVPScanProbeButton/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_pvpScanProbeRecoverTimeText;
        [AutoBind("./PVPScanProbeButton/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_pvpScanProbeCountText;
        [AutoBind("./PVPScanProbeButton/CDImageProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_pvpCDImageProgressBar;
        [AutoBind("./PVPScanProbeButton/NoneImageProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_pvpNoneCDImageProgressBar;
        [AutoBind("./GuildSentrySignalProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildSentrySignalProbeButton;
        [AutoBind("./GuildSentrySignalProbeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildSentrySignalProbeStateCtrl;
        [AutoBind("./GuildSentrySignalProbeButton/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildSentrySignalProbeRecoverTimeText;
        [AutoBind("./GuildSentrySignalProbeButton/CDImageProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_guildSentrySignalProbeProgressBar;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetButtonGroupMode;
        private static DelegateBridge __Hotfix_HidePVEScanProbe;
        private static DelegateBridge __Hotfix_SetPVEScanProbeInfo;
        private static DelegateBridge __Hotfix_SetPVEScanProbeRecoverTime;
        private static DelegateBridge __Hotfix_HidePVPScanProbe;
        private static DelegateBridge __Hotfix_SetPVPScanProbeInfo;
        private static DelegateBridge __Hotfix_SetPVPScanProbeRecoverTime;
        private static DelegateBridge __Hotfix_HideManualScanProbe;
        private static DelegateBridge __Hotfix_SetManualSignalScanProbeInfo;
        private static DelegateBridge __Hotfix_SetManualSignalProbeRecoverTime;
        private static DelegateBridge __Hotfix_HideGuildSentrySignalProbe;
        private static DelegateBridge __Hotfix_DisableGuildSentrySignalProbe;
        private static DelegateBridge __Hotfix_SetGuildSentrySignalProbeInfo;
        private static DelegateBridge __Hotfix_SetGuildSentrySignalProbeRecoverTime;

        [MethodImpl(0x8000)]
        public void DisableGuildSentrySignalProbe()
        {
        }

        [MethodImpl(0x8000)]
        public void HideGuildSentrySignalProbe()
        {
        }

        [MethodImpl(0x8000)]
        public void HideManualScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        public void HidePVEScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        public void HidePVPScanProbe()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonGroupMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildSentrySignalProbeInfo(int countDownMs, int totalMs)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildSentrySignalProbeRecoverTime(int countDownMs, int totalMs)
        {
        }

        [MethodImpl(0x8000)]
        private void SetManualSignalProbeRecoverTime(int countDownMs, int totalMs, bool hasProbe)
        {
        }

        [MethodImpl(0x8000)]
        public void SetManualSignalScanProbeInfo(int currCount, int countDownMs, int totalMs)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPVEScanProbeInfo(int currCount, int countDownMs, int totalMs)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPVEScanProbeRecoverTime(int countDownMs, int totalMs, bool hasProbe)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPVPScanProbeInfo(int currCount, int countDownMs, int totalMs)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPVPScanProbeRecoverTime(int countDownMs, int totalMs, bool hasProbe)
        {
        }
    }
}

