﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GuildFleetManagementUIController : UIControllerBase
    {
        private GuildFleetCreateUIController m_guildFleetCreateUICtrl;
        private const int m_moreThanRowArrangeUpward = 3;
        private const int m_perLineIteamsNum = 2;
        public const string m_popupWindowState_Join = "Join";
        public const string m_popupWindowState_Leave = "Leave";
        public const string m_popupWindowState_Administration = "Administration";
        public const string m_popupWindowState_Close = "Close";
        private const int m_FleetItemCount = 10;
        private List<GuildFleetItemController> m_itemsList;
        public Action<int, GuildFleetSimpleInfo> EventOnItemDetailClick;
        public Action EventOnItemCreateFleetClick;
        public Action<string> EventOnCreateGuildFleetConfirm;
        public Action EventOnCreateGuildFleetCancel;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./ShipTroopsItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemRootTransform;
        [AutoBind("./ShipTroopsItemGroup/ShipTroopsItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_fleetItem;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_popupWindow;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Join", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_popupWindowRect_Join;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Leave", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_popupWindowRect_Leave;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Administration", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_popupWindowRect_Administration;
        [AutoBind("./ClosePopupWindowButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closePopupWindowButton;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Join/ParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_ParticularsButton_Join;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Join/JoinButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_JoinButton_Join;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Leave/ParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_ParticularsButton_Leave;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Leave/LeaveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_LeaveButton_Leave;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Administration/ParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_ParticularsButton_Administration;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Administration/LeaveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_LeaveButton_Administration;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Administration/JoinButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_JoinButton_Administration;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Administration/DissolveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_DissolveButton_Administration;
        [AutoBind("./ShipTroopsItemGroupButtonUIPrefab/FrameImage/BGImage/Administration/RenameButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_RenameButton_Administration;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetCreateOrRenameFleetUIProcess;
        private static DelegateBridge __Hotfix_SetAdministrationModeIsInFleet;
        private static DelegateBridge __Hotfix_InitPanelInfo;
        private static DelegateBridge __Hotfix_GetGuildInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_InitCreateFleetPopupWindow;
        private static DelegateBridge __Hotfix_OpenPopupWindow;
        private static DelegateBridge __Hotfix_ClosePopupWindow;
        private static DelegateBridge __Hotfix_InitPanel;
        private static DelegateBridge __Hotfix_IsArrangeUpward;

        [MethodImpl(0x8000)]
        public void ClosePopupWindow()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetCreateOrRenameFleetUIProcess(string targetMode)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetGuildInfoPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public void InitCreateFleetPopupWindow(int fleetSeqId = -1, string curFleetName = "")
        {
        }

        [MethodImpl(0x8000)]
        private void InitPanel()
        {
        }

        [MethodImpl(0x8000)]
        public void InitPanelInfo(List<GuildFleetSimpleInfo> dataList, Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsArrangeUpward(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OpenPopupWindow(string targetState, int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAdministrationModeIsInFleet(bool isInFleet)
        {
        }

        [CompilerGenerated]
        private sealed class <InitPanelInfo>c__AnonStorey0
        {
            internal int index;
            internal GuildFleetSimpleInfo data;
            internal GuildFleetManagementUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIControllerBase ctrl)
            {
            }
        }
    }
}

