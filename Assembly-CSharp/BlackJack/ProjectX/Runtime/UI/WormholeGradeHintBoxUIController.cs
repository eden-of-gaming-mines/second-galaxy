﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class WormholeGradeHintBoxUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_goOnButton;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_returnButton;
        [AutoBind("./BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_titleText;
        [AutoBind("./TextGroup/Text2", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_gradeText;
        private static DelegateBridge __Hotfix_SetRecommendGrade;
        private static DelegateBridge __Hotfix_CreateShowOrCloseUIEffectInfo;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateShowOrCloseUIEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRecommendGrade(string grade)
        {
        }
    }
}

