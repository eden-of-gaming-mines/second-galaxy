﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Runtime;
    using BlackJack.ProjectX.Runtime.SDK;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class GuildStoreUITask : GuildUITaskBase
    {
        private int InputStringValidSize;
        public const string TaskModeWallet = "WalletDetail";
        public const string TaskModeStore = "StoreDetail";
        private int m_maxDonateCount;
        private ulong m_bindMoney;
        private ulong m_tradeMoney;
        private ulong m_readMoney;
        public const string ParamKeySelectTabType = "ParamKeySelectTabType";
        public const string ParamKeySelectDropDown = "ParamKeySelectDropDown";
        private bool m_hasStorePermission;
        private string m_announcementStr;
        private int m_occupySolarSystemCount;
        private ulong m_informationPointAddValue;
        private int m_selectIdx;
        private int m_currSelectDropDown;
        private GuildCurrencyTipType m_guildCurrencyTipType;
        private GuildItemListUIController.GuildStoreTabType m_lastSelectTabType;
        private GuildItemCategoryMenuItemUIController m_currSelectCategoryCtrl;
        private GuildStoreDefaultUIFilter m_guildStoreDefaultUIFilter;
        private Dictionary<GuildItemListUIController.GuildStoreTabType, List<GuildItemTypeMenuItemData>> m_guildType2ItemType;
        private List<ILBStoreItemClient> m_guildStoreShowItemList;
        private List<ProGuildDonateRankingItemInfo> m_guildDonateRankingList;
        private List<GuildCurrencyLogInfo> m_guildDonateLogList;
        private GuildStoreUIController m_guildStoreUIController;
        private GuildItemListUIController m_guildItemListUIController;
        private GuildWalletUIController m_guildWalletUIController;
        private DonateTradeMoneyPanelUIController m_donateTradeMoneyPanelUIController;
        private GuildItemMenuTreeUIController m_guildItemMenuTreeUIController;
        private GuildCurrencyDetailInfoPanelUIController m_guildCurrencyDetailInfoPanelUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpateViewWallet;
        private static DelegateBridge __Hotfix_UpdateViewStore;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnShipWalletMenuItemButtonClick;
        private static DelegateBridge __Hotfix_OnDonateButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnAddButtonClick;
        private static DelegateBridge __Hotfix_OnRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnBuyButtonClick;
        private static DelegateBridge __Hotfix_OnLogButtonClick;
        private static DelegateBridge __Hotfix_OnInformationPointDetailButtonClick;
        private static DelegateBridge __Hotfix_OnTradeMoneyDetailButtonClick;
        private static DelegateBridge __Hotfix_OnTacticalPointDetailButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnItemListItemClick;
        private static DelegateBridge __Hotfix_OnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnAnnoucementEditEnd;
        private static DelegateBridge __Hotfix_CheckInputStringValidWithTips;
        private static DelegateBridge __Hotfix_OnMenuItemClick;
        private static DelegateBridge __Hotfix_OnItemShareButtonClick;
        private static DelegateBridge __Hotfix_StartGuildStoreUITask;
        private static DelegateBridge __Hotfix_UnregisterUIEvent;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_CloseUIAndPause;
        private static DelegateBridge __Hotfix_ReturnToPreTask;
        private static DelegateBridge __Hotfix_OnMenuItemCategoryClick;
        private static DelegateBridge __Hotfix_OnNpcShopItemTypeMenuItemClick;
        private static DelegateBridge __Hotfix_UpdateAfterSelectedChanged;
        private static DelegateBridge __Hotfix_SendGuildWalletInfoReq;
        private static DelegateBridge __Hotfix_SendGuildWalletAnnouncementSetReq;
        private static DelegateBridge __Hotfix_SendGuildDonateTradeMoneyReq;
        private static DelegateBridge __Hotfix_SendGuildItemStoreInfoReq;
        private static DelegateBridge __Hotfix_SendGuildMoneyInfoReq;
        private static DelegateBridge __Hotfix_InitAllItemDic;
        private static DelegateBridge __Hotfix_ClearSelectedItemType;
        private static DelegateBridge __Hotfix_GenerateDataList;
        private static DelegateBridge __Hotfix_UpdateMenuItemCategorySelectedState;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildStoreUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public static bool CheckInputStringValidWithTips(string inputString, int lengthLimit, InputType inputType, InputStringCheckFlag flag = 0x1f)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSelectedItemType()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseUIAndPause(Action onEnd, bool showAnim = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private List<object> GenerateDataList(IEnumerable dataSrcList)
        {
        }

        [MethodImpl(0x8000)]
        private void InitAllItemDic()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAnnoucementEditEnd(string announcement)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInformationPointDetailButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListItemClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemCategoryClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShopItemTypeMenuItemClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipWalletMenuItemButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTacticalPointDetailButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeMoneyDetailButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnToPreTask(bool showAnim = true)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildDonateTradeMoneyReq(int donateTradeMoney, Action<bool> onEnd, bool showErrorTip = true)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendGuildItemStoreInfoReq(Action<bool> onEnd, bool showErrorTip = true, bool forceSend = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendGuildMoneyInfoReq(Action<bool> onEnd, bool showErrorTip = true, bool isForceSend = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildWalletAnnouncementSetReq(string announcement, Action<bool> onEnd, bool showErrorTip = true)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildWalletInfoReq(Action<bool> onEnd, bool showErrorTip = true)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildStoreUITask(UIIntent returnIntent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpateViewWallet()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAfterSelectedChanged(int npcShopItemType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMenuItemCategorySelectedState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewStore()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CloseUIAndPause>c__AnonStorey2
        {
            internal Action onEnd;
            internal GuildStoreUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnConfirmButtonClick>c__AnonStorey0
        {
            internal int donateValue;
            internal GuildStoreUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.SendGuildDonateTradeMoneyReq(this.donateValue, delegate (bool daonateRes) {
                    if (daonateRes)
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_GuildDonateSucceed, false, new object[0]);
                        this.$this.SendGuildWalletInfoReq(delegate (bool res) {
                            if (res)
                            {
                                this.$this.EnablePipelineStateMask(GuildStoreUITask.PipeLineStateMaskType.UpdateWalletInfo);
                                this.$this.StartUpdatePipeLine(null, false, false, true, null);
                            }
                        }, true);
                    }
                }, true);
            }

            internal void <>m__1(bool daonateRes)
            {
                if (daonateRes)
                {
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_GuildDonateSucceed, false, new object[0]);
                    this.$this.SendGuildWalletInfoReq(delegate (bool res) {
                        if (res)
                        {
                            this.$this.EnablePipelineStateMask(GuildStoreUITask.PipeLineStateMaskType.UpdateWalletInfo);
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }
                    }, true);
                }
            }

            internal void <>m__2(bool res)
            {
                if (res)
                {
                    this.$this.EnablePipelineStateMask(GuildStoreUITask.PipeLineStateMaskType.UpdateWalletInfo);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnItemDetailButtonClick>c__AnonStorey1
        {
            internal UIControllerBase ctrl;
            internal GuildStoreUITask $this;

            internal void <>m__0()
            {
                ItemDetailInfoUITask.ShowItemDetailUIUseRetuenableIntent(((ItemSimpleInfoUIController) this.ctrl).CurrentSelectItem, this.$this.CurrentIntent, null, true);
            }
        }

        [CompilerGenerated]
        private sealed class <OnNpcShopItemTypeMenuItemClick>c__AnonStorey3
        {
            internal int npcShopItemType;
            internal GuildStoreUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.CurrentIntent.TargetMode = "StoreDetail";
                    this.$this.EnablePipelineStateMask(GuildStoreUITask.PipeLineStateMaskType.IsUpdateAllStoreItem);
                    this.$this.UpdateAfterSelectedChanged(this.npcShopItemType);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildDonateTradeMoneyReq>c__AnonStorey6
        {
            internal Action<bool> onEnd;
            internal bool showErrorTip;

            internal void <>m__0(Task task)
            {
                GuildDonateTradeMoneyReqNetTask task2 = (GuildDonateTradeMoneyReqNetTask) task;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if ((task2.m_result != 0) && this.showErrorTip)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildItemStoreInfoReq>c__AnonStorey7
        {
            internal Action<bool> onEnd;
            internal bool showErrorTip;

            internal void <>m__0(Task task)
            {
                GuildItemStoreInfoReqNetTask task2 = (GuildItemStoreInfoReqNetTask) task;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if ((task2.m_result != 0) && this.showErrorTip)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildMoneyInfoReq>c__AnonStorey8
        {
            internal Action<bool> onEnd;
            internal bool showErrorTip;

            internal void <>m__0(Task task)
            {
                GuildCurrencyInfoReqNetTask task2 = (GuildCurrencyInfoReqNetTask) task;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if ((task2.m_result != 0) && this.showErrorTip)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildWalletAnnouncementSetReq>c__AnonStorey5
        {
            internal Action<bool> onEnd;
            internal bool showErrorTip;
            internal string announcement;
            internal GuildStoreUITask $this;

            internal void <>m__0(Task task)
            {
                GuildWalletAnnouncementSetReqNetTask task2 = (GuildWalletAnnouncementSetReqNetTask) task;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else
                {
                    if ((task2.m_result != 0) && this.showErrorTip)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                    else if (task2.m_result == 0)
                    {
                        this.$this.m_announcementStr = CommonLogicUtil.ReplaceSensitiveWord(this.announcement, SDKHelper.IsForExternal(), '*');
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.m_result == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildWalletInfoReq>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal bool showErrorTip;
            internal GuildStoreUITask $this;

            internal void <>m__0(Task task)
            {
                GuildWalletInfoReqNetTask task2 = (GuildWalletInfoReqNetTask) task;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.m_result != 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                    if (this.showErrorTip)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                    }
                }
                else
                {
                    this.$this.m_announcementStr = task2.m_walletAnnouncement;
                    this.$this.m_occupySolarSystemCount = task2.m_solarSystemCount;
                    this.$this.m_informationPointAddValue = task2.m_informationPointAddValue;
                    this.$this.m_guildDonateRankingList.Clear();
                    this.$this.m_guildDonateRankingList.AddRange(task2.m_guildDonateRankingList);
                    this.$this.m_guildDonateLogList.Clear();
                    this.$this.m_guildDonateLogList.AddRange(task2.m_guildDonateLogList);
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
            }
        }

        private enum GuildCurrencyTipType
        {
            None,
            TradeMoney,
            InformationPoint,
            TacticalPoint
        }

        private enum PipeLineStateMaskType
        {
            IsSelectedItemTypeChanged,
            IsUpdateAllStoreItem,
            IsSelectedItemChanged,
            IsShowDonatePanel,
            UpdateWalletInfo,
            ResetToggleState
        }
    }
}

