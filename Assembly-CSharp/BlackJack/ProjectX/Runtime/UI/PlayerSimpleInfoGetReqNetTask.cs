﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class PlayerSimpleInfoGetReqNetTask : NetWorkTransactionTask
    {
        private List<string> m_gameUserIdList;
        private List<string> m_needReqIdList;
        private List<int> m_reqIdVersionList;
        private int m_reqIndex;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_GetReqListByIndex;
        private static DelegateBridge __Hotfix_OnPlayerSimpleInfoGetAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public PlayerSimpleInfoGetReqNetTask(List<string> gameUserIdList)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetReqListByIndex(int index, out List<string> needReqIdList, out List<int> reqIdVersionList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerSimpleInfoGetAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

