﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class DelegateQuestSimpleInfoListUITask : UITaskBase
    {
        private bool m_isNeedLastItemTips;
        private List<DelegateMissionInfo> m_historyDelegateSimpleInfoList;
        private List<NpcCaptainInfo> m_showCaptainInfoList;
        private bool m_isFilterPanelShow;
        private List<SignalType> m_filterList;
        private DelegateQuestSimpleInfoListUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "DelegateQuestSimpleInfoListUITask";
        [CompilerGenerated]
        private static Comparison<DelegateMissionInfo> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartDelegateQuestSimpleInfoListUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnDelegateMissionItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerNameButtonClick;
        private static DelegateBridge __Hotfix_OnShowOrHideFilterButtonClick;
        private static DelegateBridge __Hotfix_OnFilterItemClick;
        private static DelegateBridge __Hotfix_OnDelegateQuestComplete;
        private static DelegateBridge __Hotfix_OnDelegateQuestCancel;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public DelegateQuestSimpleInfoListUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissionItemClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDelegateQuestCancel(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDelegateQuestComplete(int result, List<ItemInfo> rewardList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFilterItemClick(SignalType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerNameButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShowOrHideFilterButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public static DelegateQuestSimpleInfoListUITask StartDelegateQuestSimpleInfoListUITask(UIIntent returnIntent, Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnFilterItemClick>c__AnonStorey1
        {
            internal SignalType type;

            internal bool <>m__0(SignalType e) => 
                (e == this.type);
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCache>c__AnonStorey0
        {
            internal DelegateMissionInfo delegateItem;

            internal bool <>m__0(SignalType e) => 
                (e == this.delegateItem.m_sourceSignal.m_type);
        }

        public enum PipeLineMaskType
        {
            FilterChange,
            DelegateItemListCacheChange
        }
    }
}

