﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CrackQueueAddBoxReqNetTask : NetWorkTransactionTask
    {
        private int m_boxItemIdx;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <CrackQueueAddBoxResult>k__BackingField;
        private int m_itemIndex;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCrackQueueAddBoxAck;
        private static DelegateBridge __Hotfix_set_CrackQueueAddBoxResult;
        private static DelegateBridge __Hotfix_get_CrackQueueAddBoxResult;
        private static DelegateBridge __Hotfix_get_ItemIndex;

        [MethodImpl(0x8000)]
        public CrackQueueAddBoxReqNetTask(int boxItemIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCrackQueueAddBoxAck(int result, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int CrackQueueAddBoxResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

