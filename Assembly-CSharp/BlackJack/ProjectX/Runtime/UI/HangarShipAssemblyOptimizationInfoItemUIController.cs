﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipAssemblyOptimizationInfoItemUIController : UIControllerBase
    {
        private readonly List<GameObject> m_itemTextGOs;
        private readonly List<GameObject> m_descTextItemGOs;
        [AutoBind("./Title/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_titleText;
        [AutoBind("./ItemTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemTextGroup;
        [AutoBind("./DescTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_descTextGroup;
        [AutoBind("./ItemTextGroup/ItemTextItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemTextItem;
        [AutoBind("./DescTextGroup/DescTextItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_descTextItem;
        private static DelegateBridge __Hotfix_UpdateItemInformation;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInformation(HangarShipAssemblyOptimizationInfoPanelUIController.ItemSupportInfo info)
        {
        }
    }
}

