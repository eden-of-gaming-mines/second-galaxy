﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class DragableButtonUIController : UIControllerBase, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
    {
        private bool m_pointMoved;
        private PointerEventData m_pressEventData;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Vector2> EventPressPosition;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector2> EventMoveToNewPosition;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector2> EventReleasePosition;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventClicked;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnPointerDown;
        private static DelegateBridge __Hotfix_OnPointerUp;
        private static DelegateBridge __Hotfix_ScreenPosition;
        private static DelegateBridge __Hotfix_add_EventPressPosition;
        private static DelegateBridge __Hotfix_remove_EventPressPosition;
        private static DelegateBridge __Hotfix_add_EventMoveToNewPosition;
        private static DelegateBridge __Hotfix_remove_EventMoveToNewPosition;
        private static DelegateBridge __Hotfix_add_EventReleasePosition;
        private static DelegateBridge __Hotfix_remove_EventReleasePosition;
        private static DelegateBridge __Hotfix_add_EventClicked;
        private static DelegateBridge __Hotfix_remove_EventClicked;

        public event Action EventClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Vector2> EventMoveToNewPosition
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Vector2> EventPressPosition
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Vector2> EventReleasePosition
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnPointerDown(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerUp(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 ScreenPosition(Vector2 eventPosition)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

