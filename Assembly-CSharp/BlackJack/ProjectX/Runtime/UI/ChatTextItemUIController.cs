﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ChatTextItemUIController : ChatItemControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, ChatInfo> EventOnPlayerHeadButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ChatInfo> EventOnTranslateButtonClick;
        private EmojiParseController m_emojiHelper;
        private bool m_alwaysEnableTranslate;
        [AutoBind("./PlayerName/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlayerNameText;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController uiStateCtrl;
        [AutoBind("./SystemMassage", AutoBindAttribute.InitState.NotInit, false)]
        public Text SystemText;
        [AutoBind("./PlayerName/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommanderItemDummy;
        [AutoBind("./ChatDetail/ChatBGImage/ChatText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ChatText;
        [AutoBind("./ChatDetail/ChatBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BGImage;
        [AutoBind("./ChatDetail/ChatBGImage/TranslateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TranslateButton;
        [AutoBind("./ChatDetail/ChatBGImage/TranslateButton/TranslateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TranslateButtonText;
        [AutoBind("./ChatDetail/ChatBGImage/TranslateButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TranslateButtonGo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitEmojiParseHelper;
        private static DelegateBridge __Hotfix_GetEmojiParseHelper;
        private static DelegateBridge __Hotfix_RegisterButtonClickEvent;
        private static DelegateBridge __Hotfix_UnRegisterButtonClickEvent;
        private static DelegateBridge __Hotfix_RegisterTranslateButtonClickEvent;
        private static DelegateBridge __Hotfix_OnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_UpdateChatItemInfo;
        private static DelegateBridge __Hotfix_UpdateTranslateButton;
        private static DelegateBridge __Hotfix_GetChatText;
        private static DelegateBridge __Hotfix_OnTranslateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTranslateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTranslateButtonClick;

        public event Action<UIControllerBase, ChatInfo> EventOnPlayerHeadButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ChatInfo> EventOnTranslateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private string GetChatText()
        {
        }

        [MethodImpl(0x8000)]
        public EmojiParseController GetEmojiParseHelper()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEmojiParseHelper(EmojiParseController helper)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerHeadButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTranslateButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterButtonClickEvent(Action<UIControllerBase, ChatInfo> playerHeadAction)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterTranslateButtonClickEvent(Action<ChatInfo> translateButtonAction)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegisterButtonClickEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChatItemInfo(ChatInfo chatInfo, Dictionary<string, UnityEngine.Object> m_dynamicCache, bool isSelf)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTranslateButton(bool enable)
        {
        }
    }
}

