﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum ActionPlanTabType
    {
        Invalid,
        Story,
        Explore,
        Quest,
        BranchStory,
        FactionCredit
    }
}

