﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class BattlePassChallangeItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        public int m_QuestConfigId;
        public int m_QuestInstanceId;
        public string CurrLockState;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind("./ChallengeNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ChallengeNameText;
        [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./ExText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ExText;
        [AutoBind("./BarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BarImage;
        [AutoBind("./GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GetButton;
        [AutoBind("./AccomplishGetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AccomplishGetButton;
        [CompilerGenerated]
        private static Predicate<QuestRewardInfo> <>f__am$cache0;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetQuestStateByConfId;
        private static DelegateBridge __Hotfix_SetItemStateByAck;
        private static DelegateBridge __Hotfix_GetQuestRequireTotalCount;
        private static DelegateBridge __Hotfix_GetQuestAwardExp;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private int GetQuestAwardExp(int questConfId)
        {
        }

        [MethodImpl(0x8000)]
        private int GetQuestRequireTotalCount(int questConfId)
        {
        }

        [MethodImpl(0x8000)]
        public string GetQuestStateByConfId(int questId, int curWeekIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool isCareItemClick)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemStateByAck()
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(int curWeekIndex, int questId, Dictionary<string, Object> resData)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

