﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipAmmoReloadUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnAmmoReloadSlotItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnAmmoReloadSingleSlotRefillButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnAmmoChangeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<long, int> EventOnAmmoCountChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAmmoDetailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBStoreItemClient> EventOnAmmoShareButtonClick;
        private const string AddAmmoAssetName = "AmmoReloadItemUIPrefab";
        private const string ItemSimpleInfoAssetName = "ItemSimpleInfoUIPrefab";
        private bool m_isShow;
        private const int AddAmmoMaxCount = 3;
        private List<HangarShipAmmoReloadItemUIController> m_ammoReloadItemCtrls;
        private ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        private ILBStoreItemClient m_currSelectItem;
        private int m_currSelectIndex;
        protected static string PanelState_Open;
        protected static string PanelState_Close;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./AmmoReloadPanel/AmmoReloadItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform AmmoReloadGroup;
        [AutoBind("./AmmoReloadPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./AmmoReloadPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./AmmoReloadPanel/ConfirmButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyPriceText;
        [AutoBind("./AmmoReloadPanel/ItemSimpleInfoUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemSimpleInfoUIDummy;
        [AutoBind("./AmmoReloadPanel/BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemSimpleInfoBackGroundButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateAmmoReload;
        private static DelegateBridge __Hotfix_UpdateSingleleAmmoReload;
        private static DelegateBridge __Hotfix_UpdateItemSimpleInfo;
        private static DelegateBridge __Hotfix_CreateAmmoRelodeWndWindowProcess;
        private static DelegateBridge __Hotfix_CreateAmmoRelodeCustomUIProcess;
        private static DelegateBridge __Hotfix_CreateItemSimpleInfoWndWindowProcess;
        private static DelegateBridge __Hotfix_IsShow;
        private static DelegateBridge __Hotfix_ShowAmmoEquipedEffect;
        private static DelegateBridge __Hotfix_GetAmmoReloadNumber;
        private static DelegateBridge __Hotfix_GetAmmoTotalNumber;
        private static DelegateBridge __Hotfix_GetAmmoIconRectBySlotIndex;
        private static DelegateBridge __Hotfix_OnAmmoReloadSlotItemClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadSingleSlotRefillButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoChangeButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoDetailButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoShareButtonClick;
        private static DelegateBridge __Hotfix_GetAmmoCountChangeList;
        private static DelegateBridge __Hotfix_GetAmmoChangeButtonRect;
        private static DelegateBridge __Hotfix_OnAmmoCountChange;
        private static DelegateBridge __Hotfix_SetDepotCount;
        private static DelegateBridge __Hotfix_add_EventOnAmmoReloadSlotItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnAmmoReloadSlotItemClick;
        private static DelegateBridge __Hotfix_add_EventOnAmmoReloadSingleSlotRefillButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAmmoReloadSingleSlotRefillButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAmmoChangeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAmmoChangeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAmmoCountChanged;
        private static DelegateBridge __Hotfix_remove_EventOnAmmoCountChanged;
        private static DelegateBridge __Hotfix_add_EventOnAmmoDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAmmoDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAmmoShareButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAmmoShareButtonClick;
        private static DelegateBridge __Hotfix_get_CurrSelectItem;
        private static DelegateBridge __Hotfix_set_CurrSelectItem;

        public event Action<int> EventOnAmmoChangeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<long, int> EventOnAmmoCountChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnAmmoDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnAmmoReloadSingleSlotRefillButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnAmmoReloadSlotItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient> EventOnAmmoShareButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateAmmoRelodeCustomUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, Action onStart, Action onEnd, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateAmmoRelodeWndWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateItemSimpleInfoWndWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetAmmoChangeButtonRect()
        {
        }

        [MethodImpl(0x8000)]
        public List<int> GetAmmoCountChangeList(out bool ammoCountChanged)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetAmmoIconRectBySlotIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private int GetAmmoReloadNumber(List<int> ammoReloadNumberList, int index)
        {
        }

        [MethodImpl(0x8000)]
        private long GetAmmoTotalNumber(List<long> ammoTotalNumberList, int index)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShow()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoChangeButtonClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoCountChange(long currAmmoCount, int ammoIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoDetailButtonClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadSingleSlotRefillButtonClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadSlotItemClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoShareButtonClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDepotCount(List<long> depotCount)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAmmoEquipedEffect(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAmmoReload(ILBStaticPlayerShip staticShip, List<int> ammoReloadNumberList, List<long> ammoTotalNumberList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemSimpleInfo(LBStaticWeaponEquipSlotGroup slotGroup, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleleAmmoReload(ILBStaticPlayerShip staticShip, int slotIndex, int ammoReloadNumber, long ammoTotalNumber, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        public ILBStoreItemClient CurrSelectItem
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreateAmmoRelodeCustomUIProcess>c__AnonStorey0
        {
            internal Action onStart;
            internal bool isShow;
            internal bool immediateComplete;
            internal bool allowToRefreshSameState;
            internal Action onEnd;
            internal HangarShipAmmoReloadUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Action<bool> end)
            {
            }

            private sealed class <CreateAmmoRelodeCustomUIProcess>c__AnonStorey1
            {
                internal Action<bool> end;
                internal HangarShipAmmoReloadUIController.<CreateAmmoRelodeCustomUIProcess>c__AnonStorey0 <>f__ref$0;

                [MethodImpl(0x8000)]
                internal void <>m__0(bool res)
                {
                }
            }
        }
    }
}

