﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class RankingListSingleRankingUIController : UIControllerBase
    {
        private const string LogoAssetName = "LogoUIPrefab";
        private const string StateRanking = "Ranking";
        private const string StateNoRanking = "NoRanking";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<RankingListType, RankingTargetInfo> EventOnRankingItemDetailButtonClick;
        private List<RankingTargetInfo> m_rankingInfoList;
        private RankingListType m_type;
        private Dictionary<string, UnityEngine.Object> m_dynamicResDict;
        private CommonCaptainIconUIController m_selfCaptainIconUICtrlForKillRecordCount;
        private CommonCaptainIconUIController m_selfCaptainIconUICtrlForKillRecordBindMoney;
        private CommonCaptainIconUIController m_selfCaptainIconUICtrlForContinueKill;
        private CommonCaptainIconUIController m_selfCaptainIconUICtrlForCharacterGrade;
        private GuildLogoController m_guildLogoController;
        private GuildLogoController m_allianceLogoController;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RankingItemRoot;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("./ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemObjPool;
        [AutoBind("./TieleGroup/TieleGroup01/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SelfCaptainDummyTransForKillRecordCount;
        [AutoBind("./TieleGroup/TieleGroup01/SelfRankingInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingUIStateCtrlForKillRecordCount;
        [AutoBind("./TieleGroup/TieleGroup01/SelfRankingInfoRoot/SelfRankingInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingRecordStateCtrlForKillRecordCount;
        [AutoBind("./TieleGroup/TieleGroup01/SelfRankingInfoRoot/SelfRankingInfo/RankingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SelfRankingNumTextForKillRecordCount;
        [AutoBind("./TieleGroup/TieleGroup01/SelfRankingInfoRoot/SelfRankingLevelLimitInfo/LevelLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelLimitTextForKillRecordCount;
        [AutoBind("./TieleGroup/TieleGroup01/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillRecordScoreText;
        [AutoBind("./TieleGroup/TieleGroup02/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SelfCaptainDummyTransForKillRecordBindMoney;
        [AutoBind("./TieleGroup/TieleGroup02/SelfRankingInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingUIStateCtrlForKillRecordBindMoney;
        [AutoBind("./TieleGroup/TieleGroup02/SelfRankingInfoRoot/SelfRankingInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingRecordStateCtrlForKillRecordBindMoney;
        [AutoBind("./TieleGroup/TieleGroup02/SelfRankingInfoRoot/SelfRankingInfo/RankingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SelfRankingNumTextForKillRecordBindMoney;
        [AutoBind("./TieleGroup/TieleGroup02/SelfRankingInfoRoot/SelfRankingLevelLimitInfo/LevelLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelLimitTextForKillRecordBindMoney;
        [AutoBind("./TieleGroup/TieleGroup02/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillRecordBindMoneyScoreText;
        [AutoBind("./TieleGroup/TieleGroup03/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SelfCaptainDummyTransForContinueKill;
        [AutoBind("./TieleGroup/TieleGroup03/SelfRankingInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingUIStateCtrlForContinueKill;
        [AutoBind("./TieleGroup/TieleGroup03/SelfRankingInfoRoot/SelfRankingInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingRecordStateCtrlForContinueKill;
        [AutoBind("./TieleGroup/TieleGroup03/SelfRankingInfoRoot/SelfRankingInfo/RankingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SelfRankingNumTextForContinueKill;
        [AutoBind("./TieleGroup/TieleGroup03/SelfRankingInfoRoot/SelfRankingLevelLimitInfo/LevelLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelLimitTextForContinueKill;
        [AutoBind("./TieleGroup/TieleGroup03/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ContinueKillScoreText;
        [AutoBind("./TieleGroup/TieleGroup04/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SelfCaptainDummyTransForCharacterGrade;
        [AutoBind("./TieleGroup/TieleGroup04/SelfRankingInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingUIStateCtrlForCharacterGrade;
        [AutoBind("./TieleGroup/TieleGroup04/SelfRankingInfoRoot/SelfRankingInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingRecordStateCtrlForCharacterGrade;
        [AutoBind("./TieleGroup/TieleGroup04/SelfRankingInfoRoot/SelfRankingInfo/RankingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SelfRankingNumTextForCharacterGrade;
        [AutoBind("./TieleGroup/TieleGroup04/SelfRankingInfoRoot/SelfRankingLevelLimitInfo/LevelLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelLimitTextForCharacterGrade;
        [AutoBind("./TieleGroup/TieleGroup04/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CharacterGradeScoreText;
        [AutoBind("./TieleGroup/GuildTitleGroup/GuildDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildDummy;
        [AutoBind("./TieleGroup/GuildTitleGroup/SelfRankingInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingUIStateCtrlForGuild;
        [AutoBind("./TieleGroup/GuildTitleGroup/SelfRankingInfo/RankingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SelfRankingNumTextForGuild;
        [AutoBind("./TieleGroup/GuildTitleGroup/ScoreNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildEvaluateScoreText;
        [AutoBind("./TieleGroup/AllianceTitleGroup/AllianceDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AllianceDummy;
        [AutoBind("./TieleGroup/AllianceTitleGroup/SelfRankingInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelfRankingUIStateCtrlForAlliance;
        [AutoBind("./TieleGroup/AllianceTitleGroup/SelfRankingInfo/RankingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SelfRankingNumTextForAlliance;
        [AutoBind("./TieleGroup/AllianceTitleGroup/ScoreNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AllianceEvaluateScoreText;
        [AutoBind("./TieleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TitleStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateSingleRankingListInfo;
        private static DelegateBridge __Hotfix_SetSingleRankingListTitleState;
        private static DelegateBridge __Hotfix_SetPersonalSingleRankingListTitlInfo;
        private static DelegateBridge __Hotfix_SetGuildRankingListTitle;
        private static DelegateBridge __Hotfix_SetAllianceRankingListTitle;
        private static DelegateBridge __Hotfix_UpdateRankingItemInfo;
        private static DelegateBridge __Hotfix_OnRankingListItemFill;
        private static DelegateBridge __Hotfix_OnRankingItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnNewItemCreateInPool;
        private static DelegateBridge __Hotfix_add_EventOnRankingItemDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRankingItemDetailButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action<RankingListType, RankingTargetInfo> EventOnRankingItemDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewItemCreateInPool(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRankingItemDetailButtonClick(RankingListType type, RankingTargetInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRankingListItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAllianceRankingListTitle(RankingListInfo rankingListInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildRankingListTitle(RankingListInfo rankingListInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPersonalSingleRankingListTitlInfo(RankingListInfo rankingListInfo, CommonCaptainIconUIController captainIconCtrl, CommonUIStateController selfRankingUIStateCtrl, CommonUIStateController selfRankingRecordStateCtrl, Text levelLimitText, Text rankingNumText, Text scoreText)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSingleRankingListTitleState(RankingListType type, RankingListInfo rankingListInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRankingItemInfo(RankingListType type, RankingListInfo rankingInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleRankingListInfo(RankingListType type, RankingListInfo rankingListInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

