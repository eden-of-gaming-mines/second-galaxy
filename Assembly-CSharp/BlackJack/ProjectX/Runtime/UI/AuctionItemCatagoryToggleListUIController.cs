﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AuctionItemCatagoryToggleListUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<NpcShopItemCategory> EventOnItemCategoryButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemTypeToggleSelected;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnFinishedProductButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBluePrintButtonClick;
        private bool m_isBluePrintOpened;
        private bool m_isFinishProductOpened;
        private NpcShopItemType m_curSelectItemType;
        private NpcShopItemCategoryToggleUIController m_lastSelectedTypeInCategory;
        private readonly List<NpcShopItemCategoryToggleUIController> m_finishedProductItemCategoryToggleList;
        private readonly List<NpcShopItemCategoryToggleUIController> m_bluePrintItemCategoryToggleList;
        [AutoBind("./Viewport/Content/FinishedProductToggle/ToggleRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_finishProductToggleRoot;
        [AutoBind("./Viewport/Content/BluePrintToggle/ToggleRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_bluePrintToggleRoot;
        [AutoBind("./Viewport/Content/FinishedProductToggle/Vertex", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_finishedProductState;
        [AutoBind("./Viewport/Content/BluePrintToggle/Vertex", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_bluePrintState;
        [AutoBind("./Viewport/Content/FinishedProductToggle/Vertex", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_finishProductToggle;
        [AutoBind("./Viewport/Content/BluePrintToggle/Vertex", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bluePrintToggle;
        [AutoBind("./Viewport/Content/FinishedProductToggle/Vertex/UpAndDownGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_finishedProductArrowState;
        [AutoBind("./Viewport/Content/BluePrintToggle/Vertex/UpAndDownGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_bluePrintArrowState;
        [AutoBind("./Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup m_toggle;
        private const string NpcShopItemCategoryToggleUIPrefab = "NpcShopItemCategoryToggleUIPrefab";
        [CompilerGenerated]
        private static Predicate<NpcShopItemCategoryToggleUIController> <>f__am$cache0;
        [CompilerGenerated]
        private static Predicate<NpcShopItemCategoryToggleUIController> <>f__am$cache1;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateaCategoryAndTypeSelectState;
        private static DelegateBridge __Hotfix_ClearAllTypeToggleSelectState;
        private static DelegateBridge __Hotfix_SwitchCateGoryToggleState;
        private static DelegateBridge __Hotfix_SwitchBluePrintOrFinishedProductToggleState;
        private static DelegateBridge __Hotfix_CreateAllCategoryToggles;
        private static DelegateBridge __Hotfix_SetShopItemToggle;
        private static DelegateBridge __Hotfix_InstantiateShopItemToggleUIController;
        private static DelegateBridge __Hotfix_FindItemToggleCtrl;
        private static DelegateBridge __Hotfix_FindOpenedItemToggleCtrl;
        private static DelegateBridge __Hotfix_ChangeFinishedProductState;
        private static DelegateBridge __Hotfix_ChangeBluePrintState;
        private static DelegateBridge __Hotfix_OnFinishedProductButtonClick;
        private static DelegateBridge __Hotfix_OnBluePrintButtonClick;
        private static DelegateBridge __Hotfix_OnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_OnShopItemTypeToggleClick;
        private static DelegateBridge __Hotfix_add_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemCategoryButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_add_EventOnFinishedProductButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFinishedProductButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBluePrintButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBluePrintButtonClick;

        public event Action EventOnBluePrintButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFinishedProductButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopItemCategory> EventOnItemCategoryButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemTypeToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void ChangeBluePrintState(bool open)
        {
        }

        [MethodImpl(0x8000)]
        private void ChangeFinishedProductState(bool open)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearAllTypeToggleSelectState(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void CreateAllCategoryToggles(Dictionary<NpcShopItemCategory, List<int>> bluePrintToggleList, Dictionary<NpcShopItemCategory, List<int>> finishedProductToggleList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemCategoryToggleUIController FindItemToggleCtrl(NpcShopItemCategory itemCategory)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemCategoryToggleUIController FindOpenedItemToggleCtrl()
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemCategoryToggleUIController InstantiateShopItemToggleUIController(RectTransform rootTrs)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBluePrintButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFinishedProductButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCategoryButtonClick(NpcShopItemCategory type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopItemTypeToggleClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShopItemToggle(int index, NpcShopItemCategory shopItem, List<int> sonTypeList, Dictionary<string, UnityEngine.Object> resDict, bool isBlue)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchBluePrintOrFinishedProductToggleState(bool isBluePrint)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchCateGoryToggleState(NpcShopItemCategory category, bool isBluePrint)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateaCategoryAndTypeSelectState(NpcShopItemCategory category, NpcShopItemType type, bool isBluePrint)
        {
        }

        [CompilerGenerated]
        private sealed class <FindItemToggleCtrl>c__AnonStorey0
        {
            internal NpcShopItemCategory itemCategory;

            internal bool <>m__0(NpcShopItemCategoryToggleUIController item) => 
                (item.m_shopItemCategory == this.itemCategory);

            internal bool <>m__1(NpcShopItemCategoryToggleUIController item) => 
                (item.m_shopItemCategory == this.itemCategory);
        }
    }
}

