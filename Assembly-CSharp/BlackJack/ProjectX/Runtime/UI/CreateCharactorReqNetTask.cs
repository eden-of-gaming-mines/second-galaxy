﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CreateCharactorReqNetTask : NetWorkTransactionTask
    {
        private string m_charactorName;
        private GrandFaction m_faction;
        private ProfessionType m_profession;
        private int m_resId;
        private GenderType m_gender;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CreateCharactorResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCreateCharactor;
        private static DelegateBridge __Hotfix_set_CreateCharactorResult;
        private static DelegateBridge __Hotfix_get_CreateCharactorResult;

        [MethodImpl(0x8000)]
        public CreateCharactorReqNetTask(string charactorName, GrandFaction faction, ProfessionType profession, int resId, GenderType gender)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCreateCharactor(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int CreateCharactorResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

