﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using UnityEngine;

    public class StarMapDrawingSolarSystemDesc : MonoBehaviour
    {
        [Header("绘制的恒星系图中，连线的颜色")]
        public Color32 m_lineColor;
        [Header("默认的星图摄像机离中心点的偏移量")]
        public float m_defaultCameraOffset;
        [Header("默认的星图摄像机云台的上下旋转")]
        public float m_defaultCameraPlatformRotateAngleForAxisX;
        [Header("默认的星图摄像机云台的左右旋转")]
        public float m_defaultCameraPlatformRotateAngleForAxisY;
        [Header("摄像机离中心点偏移量的最小值")]
        public float m_cameraOffsetMin;
        [Header("摄像机离中心点偏移量的最大值")]
        public float m_cameraOffsetMax;
        [Header("恒星系左右旋转的速度倍率参数")]
        public float m_horizontalRotationSpeedParam;
        [Header("恒星系上下翻转的速度倍率参数")]
        public float m_verticalRotationSpeedParam;
        [Header("恒星系上下翻转的最大角度")]
        public float m_verticalRotationAngleMax;
        [Header("恒星系上下翻转的最小角度 ")]
        public float m_verticalRotationAngleMin;
        [Header("恒星系缩放的速度倍率参数")]
        public float m_scaleSpeedParam;
        [Header("当选中某个恒星系对象时，摄像头拉近显示时，摄像机动画总播放时间，单位s")]
        public float m_focusTargetCameraAnimationDuration;
        [Header("半径为1的轨道圆绘制时，由多少段线段组成。不同尺寸的圆需要按比例增加或减少线段数目")]
        public int m_lineSegmentCountForUnit;
        [Header("轨道线的宽度")]
        public float m_lineWidth;
        [Header("显示的恒星icon的缩放")]
        public float m_sunScale;
    }
}

