﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    internal class CommonItemDetailInfoShipUIController : UIControllerBase
    {
        [AutoBind("./Viewport/Content/ResourceInfo/CPUInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CPUNumber;
        [AutoBind("./Viewport/Content/ResourceInfo/PowerInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PowerNumber;
        [AutoBind("./Viewport/Content/ResourceInfo/EnergyInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EnergyValueNumber;
        [AutoBind("./Viewport/Content/ResourceInfo/AutomationEnergyInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AutoRestoreEnergyValueNumber;
        [AutoBind("./Viewport/Content/ResourceInfo/LowSlotInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LowSlotNumber;
        [AutoBind("./Viewport/Content/ResourceInfo/VelocityInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CruiseSpeedMaxNumber;
        [AutoBind("./Viewport/Content/ResourceInfo/Sensitivity/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SensitivityNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ShieldResistanceInfo/ElectricalDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldElectricalDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ShieldResistanceInfo/ElectricalDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldElectricalDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/ShieldResistanceInfo/EnergyDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldENDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ShieldResistanceInfo/EnergyDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldENDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/ShieldResistanceInfo/FireDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldFireDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ShieldResistanceInfo/FireDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldFireDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/ArmorResistanceInfo/ElectricalDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmorElectricalDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ArmorResistanceInfo/ElectricalDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorElectricalDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/ArmorResistanceInfo/EnergyDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmorFireDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ArmorResistanceInfo/EnergyDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorFireDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/ArmorResistanceInfo/FireDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmorENDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ArmorResistanceInfo/FireDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorENDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/JamingComposeInfo/ElectricalDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElecResistElectricalDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/JamingComposeInfo/ElectricalDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ElecResistElectricalDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/JamingComposeInfo/EnergyDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElecResistFireDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/JamingComposeInfo/EnergyDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ElecResistFireDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/JamingComposeInfo/FireDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElecResistENDamageResistNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/JamingComposeInfo/FireDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ElecResistENDamageResistImage;
        [AutoBind("./Viewport/Content/DefenseInfo/ShieldInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/AutomationShieldInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldRestoreNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/ArmorInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmorNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/Volume/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text VolumeNumber;
        [AutoBind("./Viewport/Content/DefenseInfo/WrapStabilityInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text JumpSteadyNumber;
        [AutoBind("./Viewport/Content/BaseDataInfo/CapacityInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StoreSizeNumber;
        [AutoBind("./Viewport/Content/BaseDataInfo/PackedSizeInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PackedSizeNumber;
        [AutoBind("./Viewport/Content/BaseDataInfo/TechInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechText;
        [AutoBind("./Viewport/Content/BaseDataInfo/QualityInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QualityText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateShipDetailInfoUI_1;
        private static DelegateBridge __Hotfix_SetShipDetailInfoUI;
        private static DelegateBridge __Hotfix_UpdateShipDetailInfoUI_0;
        private static DelegateBridge __Hotfix_SetShipDetailInfoUIByConfig;
        private static DelegateBridge __Hotfix_SetCPUMaxNumber;
        private static DelegateBridge __Hotfix_SetPowerMaxNumber;
        private static DelegateBridge __Hotfix_SetEnergyMaxNumber;
        private static DelegateBridge __Hotfix_SetEnergyGrowNumber;
        private static DelegateBridge __Hotfix_SetLowSlotDesc;
        private static DelegateBridge __Hotfix_SetSensitiveNumber;
        private static DelegateBridge __Hotfix_SetShieldNumber;
        private static DelegateBridge __Hotfix_SetSheildGrowNumber;
        private static DelegateBridge __Hotfix_SetArmorNumber;
        private static DelegateBridge __Hotfix_SetShieldElectricalDamageResistNumber;
        private static DelegateBridge __Hotfix_SetShieldFireDamageResistNumber;
        private static DelegateBridge __Hotfix_SetShieldENDamageResistNumber;
        private static DelegateBridge __Hotfix_SetArmorElectricalDamageResistNumber;
        private static DelegateBridge __Hotfix_SetArmorFireDamageResistNumber;
        private static DelegateBridge __Hotfix_SetArmorENDamageResistNumber;
        private static DelegateBridge __Hotfix_SetCruiseSpeedMaxNumber;
        private static DelegateBridge __Hotfix_SetStoreSizeNumber;
        private static DelegateBridge __Hotfix_SetPackedSizeNumber;
        private static DelegateBridge __Hotfix_SetTechText;
        private static DelegateBridge __Hotfix_SetQualityText;
        private static DelegateBridge __Hotfix_SetJumpSteadyNumber;
        private static DelegateBridge __Hotfix_SetVolumBaseNumber;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorElectricalDamageResistNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorENDamageResistNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorFireDamageResistNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetArmorNumber(int armorNumber)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCPUMaxNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCruiseSpeedMaxNumber(double number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnergyGrowNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnergyMaxNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetJumpSteadyNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLowSlotDesc(int lowSlotNumber)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPackedSizeNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPowerMaxNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetQualityText(SubRankType subRankType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSensitiveNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSheildGrowNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShieldElectricalDamageResistNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShieldENDamageResistNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShieldFireDamageResistNumber(float number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShieldNumber(int shieldNumber)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipDetailInfoUI(LBStaticPlayerShipClient shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipDetailInfoUIByConfig(ConfigDataSpaceShipInfo shipConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStoreSizeNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTechText(RankType rankType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVolumBaseNumber(int number)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipDetailInfoUI(ILBItem shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipDetailInfoUI(LBStaticPlayerShipClient shipInfo)
        {
        }
    }
}

