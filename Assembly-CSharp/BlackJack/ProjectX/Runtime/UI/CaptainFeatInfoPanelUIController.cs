﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainFeatInfoPanelUIController : UIControllerBase
    {
        private List<CaptainFeatPropertyInfoUIController> m_featPropertyList;
        private const string IconState_Init = "Init";
        private const string IconState_Addition = "Addition";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatTipsPanelCtrl;
        [AutoBind("./FeatIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FeatIconCtrl;
        [AutoBind("./FeatIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FeatIcon;
        [AutoBind("./FeatNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatName;
        [AutoBind("./LvGroup/LvTextGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatLevel;
        [AutoBind("./LvGroup/LeaveUpGroup/FirstLvTextGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelUpPreLevel;
        [AutoBind("./LvGroup/LeaveUpGroup/LastLvTextGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelUpNewLevel;
        [AutoBind("./FeatTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatTypeText;
        [AutoBind("./FeatDetailGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatDetailGroup;
        [AutoBind("./FeatDetailGroup/ScrollView/Viewport/Content/FeatDetail1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FeatDetailItem;
        [AutoBind("./EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController EffectCtrl;
        [AutoBind("./FeatDetailGroup/ScrollView/Viewport/Content/BuffForPlayer", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_onlyForCommanderTipGo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowCaptainFeatInfo;
        private static DelegateBridge __Hotfix_SetFeatInfoPanelState;
        private static DelegateBridge __Hotfix_ShowNewFeatEffect;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFeatInfoPanelState(FeatInfoPanelState panelState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCaptainFeatInfo(LBNpcCaptainFeats featInfo, bool isInitFeat, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewFeatEffect()
        {
        }

        public enum FeatInfoPanelState
        {
            Normal,
            Gray,
            New,
            Upgrade
        }
    }
}

