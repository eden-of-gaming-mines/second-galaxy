﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class BlackMarketManagerUITask : UITaskBase
    {
        private BlackMarketManagerUIController m_blackMarketManagerUIController;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private static Action<string> EventOnEnterBlackMark;
        private BlackMarketResourceShopUITask m_resourceShopUITask;
        private MonthCardUITask m_monthCardUITask;
        private WeelyGiftPackageUITask m_weeklyGiftPackageUITask;
        private DailyGiftPackageUITask m_dailyGiftPackageUITask;
        private SpecialGiftPackageUITask m_specialGiftPackageUITask;
        private RechargeItemShopUITask m_rechargeItemShopUITask;
        private BlackMarketIrShopUITask m_irShopUITask;
        private bool? m_isPrepareSuccessForCommonData;
        private bool? m_isPrepareDataSuccessForCurMode;
        private bool m_isBlackMarketResourceShopResLoadComplete;
        private bool m_isMonthCardResLoadComplete;
        private bool m_isWeelyGiftPackageResLoadComplete;
        private bool m_isDailyGiftPackageResLoadComplete;
        private bool m_isSpecialGiftPackageResLoadComplete;
        private bool m_isRechargeItemShopResLoadComplete;
        private bool m_isBlackMarketIrShopResLoadComplete;
        private const string ParamKeyResourceShopCategroy = "ResourceShopCategroy";
        private const string ParamKeyResourceShopDefaultItem = "ResourceShopDefaultItem";
        private const string ParamKeyResourceShopDefaultItemType = "ResourceShopDefaultItemType";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map2;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map3;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map4;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map5;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartBlackMarketManagerUITaskWithPrepare;
        private static DelegateBridge __Hotfix_StartBlackMarketManagerUItaskToIrShopWithPrepare;
        private static DelegateBridge __Hotfix_StartBlackMarketManagerUItaskToResourceShopWithPrepare;
        private static DelegateBridge __Hotfix_StartBlackMarketManagerUItaskToResourceShopPointTypeWithPrepare;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateMoney;
        private static DelegateBridge __Hotfix_UpdateSpecailGiftPackage;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnRequestSwitchTask;
        private static DelegateBridge __Hotfix_OnPauseBlackMarketManagerUITask;
        private static DelegateBridge __Hotfix_OnRequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_OnResourcesShopButtonClick;
        private static DelegateBridge __Hotfix_OnRechargeButtonClick;
        private static DelegateBridge __Hotfix_OnWeeklyGiftPackButtonClick;
        private static DelegateBridge __Hotfix_OnSpecialGiftPackageButtonClick;
        private static DelegateBridge __Hotfix_OnMonthCardButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnDailyGiftPackButtonClick;
        private static DelegateBridge __Hotfix_OnIrShopButtonClick;
        private static DelegateBridge __Hotfix_OnIrMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnGuildTradeMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnPersonalTradeMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnBindMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_PrepareDataForCurMode;
        private static DelegateBridge __Hotfix_PrepareForCommonData;
        private static DelegateBridge __Hotfix_OnPrepareDataEnd;
        private static DelegateBridge __Hotfix_SwitchToUIMode;
        private static DelegateBridge __Hotfix_GetHideAllSubTaskImmediateUIProcess;
        private static DelegateBridge __Hotfix_HaveAnySpecailGiftPackage;
        private static DelegateBridge __Hotfix_PrepareStartResourceShop;
        private static DelegateBridge __Hotfix_OnBlackMarkResourceShipResLoadComplete;
        private static DelegateBridge __Hotfix_GetResourceShopPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_PrepareStartIrShop;
        private static DelegateBridge __Hotfix_OnBlackMarkIrShipResLoadComplete;
        private static DelegateBridge __Hotfix_GetIrShopPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_PrepareDataForMonthCard;
        private static DelegateBridge __Hotfix_OnMonthCardPanelResLoadComplete;
        private static DelegateBridge __Hotfix_GetMonthCardPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_PrepareStartWeelyGiftPackage;
        private static DelegateBridge __Hotfix_OnWeelyGiftPackageResLoadComplete;
        private static DelegateBridge __Hotfix_GetWeeklyGiftPackagePanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_PrepareStartDailyGiftPackage;
        private static DelegateBridge __Hotfix_OnDailyGiftPackageResLoadComplete;
        private static DelegateBridge __Hotfix_GetDailyGiftPackagePanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_PrepareStartSpecialGiftPackage;
        private static DelegateBridge __Hotfix_OnSpecailGiftPackageResLoadComplete;
        private static DelegateBridge __Hotfix_GetSpecailGiftPackagePanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_PrepareStartRechargeItemShop;
        private static DelegateBridge __Hotfix_OnRechargeItemShopResLoadComplete;
        private static DelegateBridge __Hotfix_GetRechargeItemShopPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_add_EventOnEnterBlackMark;
        private static DelegateBridge __Hotfix_remove_EventOnEnterBlackMark;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public static  event Action<string> EventOnEnterBlackMark
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public BlackMarketManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetDailyGiftPackagePanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetHideAllSubTaskImmediateUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetIrShopPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetMonthCardPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetRechargeItemShopPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetResourceShopPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetSpecailGiftPackagePanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess GetWeeklyGiftPackagePanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected bool HaveAnySpecailGiftPackage()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBlackMarkIrShipResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBlackMarkResourceShipResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDailyGiftPackageResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDailyGiftPackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildTradeMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIrMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIrShopButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMonthCardPanelResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPauseBlackMarketManagerUITask(Action onPauseEnd, bool ignoreCloseAnimation = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPersonalTradeMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPrepareDataEnd(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRechargeButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRechargeItemShopResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRequestRefreshMoneyInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRequestSwitchTask(Action<UIIntent> onRequstEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void OnResourcesShopButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSpecailGiftPackageResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpecialGiftPackageButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeeklyGiftPackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeelyGiftPackageResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareDataForCurMode(UIIntent intent, string mode, Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareDataForMonthCard(Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareForCommonData(string mode, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareStartDailyGiftPackage(Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareStartIrShop(UIIntent intent, Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareStartRechargeItemShop(Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareStartResourceShop(UIIntent intent, Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareStartSpecialGiftPackage(Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareStartWeelyGiftPackage(Action<bool> onPrepareEnd, Action<bool> onPipelineEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartBlackMarketManagerUItaskToIrShopWithPrepare(UIIntent returnToIntent, Action<bool> onPrepareEnd, ResourceShopItemCategory category, ILBItem defaultItem)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartBlackMarketManagerUItaskToResourceShopPointTypeWithPrepare(UIIntent returnToIntent, Action<bool> onPrepareEnd, ResourceShopItemCategory category, BlackMarketShopItemType defaultItemType)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartBlackMarketManagerUItaskToResourceShopWithPrepare(UIIntent returnToIntent, Action<bool> onPrepareEnd, ResourceShopItemCategory category, ILBItem defaultItem)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartBlackMarketManagerUITaskWithPrepare(UIIntent returnToIntent, string mode, Action<bool> onPrepareEnd, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SwitchToUIMode(string mode, int category = -1, int defaultItemType = -1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMoney()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSpecailGiftPackage()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetDailyGiftPackagePanelShowOrHideProcess>c__AnonStoreyC
        {
            internal bool isImmediate;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_dailyGiftPackageUITask.ShowWeeklyGiftPackage(this.isImmediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_dailyGiftPackageUITask.HideWeeklyGiftPackage(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetIrShopPanelShowOrHideProcess>c__AnonStorey6
        {
            internal bool isImmediate;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_irShopUITask.ShowBlackMarkResourceShop(this.isImmediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_irShopUITask.HideBlackMarkResourceShop(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetMonthCardPanelShowOrHideProcess>c__AnonStorey8
        {
            internal bool isImmediate;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_monthCardUITask.ShowMonthCardPanel(this.isImmediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_monthCardUITask.HideMonthCardPanel(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetRechargeItemShopPanelShowOrHideProcess>c__AnonStorey10
        {
            internal bool isImmediate;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_rechargeItemShopUITask.ShowRechargeItemShop(this.isImmediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_rechargeItemShopUITask.HideRechargeItemShop(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetResourceShopPanelShowOrHideProcess>c__AnonStorey4
        {
            internal bool isImmediate;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_resourceShopUITask.ShowBlackMarkResourceShop(this.isImmediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_resourceShopUITask.HideBlackMarkResourceShop(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetSpecailGiftPackagePanelShowOrHideProcess>c__AnonStoreyE
        {
            internal bool isImmediate;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_specialGiftPackageUITask.ShowSpecailGiftPackage(this.isImmediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_specialGiftPackageUITask.HideSpecialGiftPackage(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetWeeklyGiftPackagePanelShowOrHideProcess>c__AnonStoreyA
        {
            internal bool isImmediate;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_weeklyGiftPackageUITask.ShowWeeklyGiftPackage(this.isImmediate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_weeklyGiftPackageUITask.HideWeeklyGiftPackage(this.isImmediate, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <OnPauseBlackMarketManagerUITask>c__AnonStorey0
        {
            internal Action onPauseEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(UIProcess process, bool b)
            {
                this.$this.Pause();
                if (this.onPauseEnd != null)
                {
                    this.onPauseEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareDataForMonthCard>c__AnonStorey7
        {
            internal Action<bool> onPrepareEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(res);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_monthCardUITask == null))
                {
                    this.$this.m_monthCardUITask = UIManager.Instance.FindUITaskWithName(typeof(MonthCardUITask).Name, true) as MonthCardUITask;
                    this.$this.m_monthCardUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestSwitchTask);
                    this.$this.m_monthCardUITask.EventOnRequestBlackMarketManagerUIPause += new Action<Action, bool>(this.$this.OnPauseBlackMarketManagerUITask);
                    this.$this.m_monthCardUITask.EventOnRequestRefreshMoneyInfo += new Action(this.$this.OnRequestRefreshMoneyInfo);
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForCommonData>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool ret)
            {
                BlackMarkUIHelper.SendRechargeGiftPackageListReq(delegate (bool lret) {
                    this.$this.m_isPrepareSuccessForCommonData = true;
                    this.$this.OnPrepareDataEnd(this.onEnd);
                });
            }

            internal void <>m__1(bool ret)
            {
                this.$this.m_isPrepareSuccessForCommonData = true;
                this.$this.OnPrepareDataEnd(this.onEnd);
            }

            internal void <>m__2(bool lret)
            {
                this.$this.m_isPrepareSuccessForCommonData = true;
                this.$this.OnPrepareDataEnd(this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareStartDailyGiftPackage>c__AnonStoreyB
        {
            internal Action<bool> onPrepareEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(res);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_dailyGiftPackageUITask == null))
                {
                    this.$this.m_dailyGiftPackageUITask = UIManager.Instance.FindUITaskWithName(typeof(DailyGiftPackageUITask).Name, true) as DailyGiftPackageUITask;
                    this.$this.m_dailyGiftPackageUITask.EventOnRequestRefreshMoneyInfo += new Action(this.$this.OnRequestRefreshMoneyInfo);
                    this.$this.m_dailyGiftPackageUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestSwitchTask);
                    this.$this.m_dailyGiftPackageUITask.EventOnRequestBlackMarketManagerUIPause += new Action<Action, bool>(this.$this.OnPauseBlackMarketManagerUITask);
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareStartIrShop>c__AnonStorey5
        {
            internal Action<bool> onPrepareEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(res);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_irShopUITask == null))
                {
                    this.$this.m_irShopUITask = UIManager.Instance.FindUITaskWithName(typeof(BlackMarketIrShopUITask).Name, true) as BlackMarketIrShopUITask;
                    this.$this.m_irShopUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestSwitchTask);
                    this.$this.m_irShopUITask.EventOnRequestBlackMarketManagerUIPause += new Action<Action, bool>(this.$this.OnPauseBlackMarketManagerUITask);
                    this.$this.m_irShopUITask.EventOnRequestRefreshMoneyInfo += new Action(this.$this.OnRequestRefreshMoneyInfo);
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareStartRechargeItemShop>c__AnonStoreyF
        {
            internal Action<bool> onPrepareEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(res);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_rechargeItemShopUITask == null))
                {
                    this.$this.m_rechargeItemShopUITask = UIManager.Instance.FindUITaskWithName(typeof(RechargeItemShopUITask).Name, true) as RechargeItemShopUITask;
                    this.$this.m_rechargeItemShopUITask.EventOnRequestRefreshMoneyInfo += new Action(this.$this.OnRequestRefreshMoneyInfo);
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareStartResourceShop>c__AnonStorey3
        {
            internal Action<bool> onPrepareEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(res);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_resourceShopUITask == null))
                {
                    this.$this.m_resourceShopUITask = UIManager.Instance.FindUITaskWithName(typeof(BlackMarketResourceShopUITask).Name, true) as BlackMarketResourceShopUITask;
                    this.$this.m_resourceShopUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestSwitchTask);
                    this.$this.m_resourceShopUITask.EventOnRequestBlackMarketManagerUIPause += new Action<Action, bool>(this.$this.OnPauseBlackMarketManagerUITask);
                    this.$this.m_resourceShopUITask.EventOnRequestRefreshMoneyInfo += new Action(this.$this.OnRequestRefreshMoneyInfo);
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareStartSpecialGiftPackage>c__AnonStoreyD
        {
            internal Action<bool> onPrepareEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(res);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_specialGiftPackageUITask == null))
                {
                    this.$this.m_specialGiftPackageUITask = UIManager.Instance.FindUITaskWithName(typeof(SpecialGiftPackageUITask).Name, true) as SpecialGiftPackageUITask;
                    this.$this.m_specialGiftPackageUITask.EventOnRequestRefreshMoneyInfo += new Action(this.$this.OnRequestRefreshMoneyInfo);
                    this.$this.m_specialGiftPackageUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestSwitchTask);
                    this.$this.m_specialGiftPackageUITask.EventOnRequestBlackMarketManagerUIPause += new Action<Action, bool>(this.$this.OnPauseBlackMarketManagerUITask);
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareStartWeelyGiftPackage>c__AnonStorey9
        {
            internal Action<bool> onPrepareEnd;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(res);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_weeklyGiftPackageUITask == null))
                {
                    this.$this.m_weeklyGiftPackageUITask = UIManager.Instance.FindUITaskWithName(typeof(WeelyGiftPackageUITask).Name, true) as WeelyGiftPackageUITask;
                    this.$this.m_weeklyGiftPackageUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestSwitchTask);
                    this.$this.m_weeklyGiftPackageUITask.EventOnRequestBlackMarketManagerUIPause += new Action<Action, bool>(this.$this.OnPauseBlackMarketManagerUITask);
                    this.$this.m_weeklyGiftPackageUITask.EventOnRequestRefreshMoneyInfo += new Action(this.$this.OnRequestRefreshMoneyInfo);
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <SwitchToUIMode>c__AnonStorey2
        {
            internal string mode;
            internal UIIntentCustom intentCustom;
            internal BlackMarketManagerUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.m_currIntent.TargetMode = this.mode;
                    this.$this.EnablePipelineStateMask(BlackMarketManagerUITask.PipeLineStateMaskType.CategoryTypeChanged);
                    this.$this.StartUpdatePipeLine(this.intentCustom, false, false, true, null);
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            CategoryTypeChanged
        }
    }
}

