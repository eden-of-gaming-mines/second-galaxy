﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterChipSlotItemUIController : UIControllerBase
    {
        private int m_chipSlotIndex;
        private ConfigDataCharChipInfo m_chipConfInfo;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnChipSlotButtonClick;
        [AutoBind("./Quality", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_chipSubRankCtrl;
        [AutoBind("./ItemState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_chipSlotIconCtrl;
        [AutoBind("./ItemState/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_chipIconImage;
        [AutoBind("./ItemState/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_rankImage;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_chipSlotButton;
        [AutoBind("./EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_effctCtrl;
        [AutoBind("./ItemState/BetterGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RecommendUIIStateCtr;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowSetEffect;
        private static DelegateBridge __Hotfix_SetChipSlotInfo;
        private static DelegateBridge __Hotfix_SetUIEffectForChipSlotInfo;
        private static DelegateBridge __Hotfix_SetUIEffectForChipSlotState;
        private static DelegateBridge __Hotfix_SetRecommendFlagState;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnChipSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnChipSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnChipSlotButtonClick;

        public event Action<int> EventOnChipSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChipSlotButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipSlotInfo(int chipSlotIndex, ConfigDataCharChipInfo chipConfInfo, bool isLocked, bool canEuiped, Sprite iconRes, Sprite rankIcon)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRecommendFlagState(bool val)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUIEffectForChipSlotInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void SetUIEffectForChipSlotState(bool isLock, bool canEquiped)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSetEffect()
        {
        }
    }
}

