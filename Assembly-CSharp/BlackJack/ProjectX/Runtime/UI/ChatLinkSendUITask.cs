﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization.Formatters.Binary;

    public class ChatLinkSendUITask : UITaskBase
    {
        private string m_reciveGameUserId;
        public const string LinkChatInfoKey = "ChatInfo";
        public ChatLinkSendController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static readonly BinaryFormatter m_formatter = new BinaryFormatter();
        public const string TaskName = "ChatLinkSendUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SendItemChatLink;
        private static DelegateBridge __Hotfix_SendKillRecordChatLink;
        private static DelegateBridge __Hotfix_SendShipCustomTemplateInfoChatLink;
        private static DelegateBridge __Hotfix_SendDelegateMissionItemChatLink;
        private static DelegateBridge __Hotfix_SendChatContentGuildChatLink;
        private static DelegateBridge __Hotfix_SendChatContentPlayerChatLink;
        private static DelegateBridge __Hotfix_SendChatContentSailReport;
        private static DelegateBridge __Hotfix_SendChatContentTeamInvite;
        private static DelegateBridge __Hotfix_SendChatContentGuildBattleReportLink;
        private static DelegateBridge __Hotfix_CreateChatContentItem;
        private static DelegateBridge __Hotfix_CreateChatContentKillRecord;
        private static DelegateBridge __Hotfix_CreateChatContentDelegateMissionInfo;
        private static DelegateBridge __Hotfix_CreateChatContentShipCustomTemplateInfo;
        private static DelegateBridge __Hotfix_CreateChatContentGuild;
        private static DelegateBridge __Hotfix_CreateChatContentPlayer;
        private static DelegateBridge __Hotfix_CreateChatContentSailReport;
        private static DelegateBridge __Hotfix_CreateChatGuildBattleReport;
        private static DelegateBridge __Hotfix_ReportInfo2ByteArray;
        private static DelegateBridge __Hotfix_ByteArray2ReportInfo;
        private static DelegateBridge __Hotfix_StartChatLinkSendUITask;
        private static DelegateBridge __Hotfix_SetChatInfoBasicData;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContext;
        private static DelegateBridge __Hotfix_OnIssueButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_SendChatMsgByToggle;
        private static DelegateBridge __Hotfix_OnTeamToggleSelected;
        private static DelegateBridge __Hotfix_OnWisperToggleSelected;
        private static DelegateBridge __Hotfix_OnAllianceToggleSelected;
        private static DelegateBridge __Hotfix_OnSolarSystemToggleSelected;
        private static DelegateBridge __Hotfix_OnStarFieldToggleSelected;
        private static DelegateBridge __Hotfix_OnLocalToggleSelected;
        private static DelegateBridge __Hotfix_OnGuildToggleSelected;
        private static DelegateBridge __Hotfix_SendStarFieldChatMsgReq;
        private static DelegateBridge __Hotfix_SendSolarSystemChatMsgReq;
        private static DelegateBridge __Hotfix_SendTeamChatMsgReq;
        private static DelegateBridge __Hotfix_SendLocalChatMsgReq;
        private static DelegateBridge __Hotfix_SendWisperChatMsgReq;
        private static DelegateBridge __Hotfix_SendAllianceChatMsgReq;
        private static DelegateBridge __Hotfix_SendGuildChatMsgReq;
        private static DelegateBridge __Hotfix_SendChatMsgReqByChannel;
        private static DelegateBridge __Hotfix_CheckChatMsgIsCanSend;
        private static DelegateBridge __Hotfix_get_m_lbChat;
        private static DelegateBridge __Hotfix_get_m_playerContext;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ChatLinkSendUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public static List<SailReportSolarSystemInfo> ByteArray2ReportInfo(List<byte[]> byteArrayList)
        {
        }

        [MethodImpl(0x8000)]
        private int CheckChatMsgIsCanSend(ChatChannel channel)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearContext()
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentDelegateMission CreateChatContentDelegateMissionInfo(DelegateMissionInfo missionInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentGuild CreateChatContentGuild(GuildBasicInfo guildBasicInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentItem CreateChatContentItem(StoreItemType itemType, int confId)
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentKillRecord CreateChatContentKillRecord(KillRecordInfo killRecordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentPlayer CreateChatContentPlayer(string anotherName, string anotherGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentSailReport CreateChatContentSailReport()
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentShip CreateChatContentShipCustomTemplateInfo(ShipCustomTemplateInfo templateInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatContentGuildBattleReport CreateChatGuildBattleReport(ulong reportId, int solarSystemId, GuildBattleType battleType, GuildBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLocalToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarFieldToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWisperToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        public static List<byte[]> ReportInfo2ByteArray(List<SailReportSolarSystemInfo> reportInfos)
        {
        }

        [MethodImpl(0x8000)]
        private void SendAllianceChatMsgReq(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendChatContentGuildBattleReportLink(ulong reportId, int solarSystemId, GuildBattleType battleType, GuildBuildingType buildingType, UIIntent returnToUIItent)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendChatContentGuildChatLink(GuildBasicInfo guildInfo, UIIntent returnToUIIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendChatContentPlayerChatLink(string anotherName, string anotherGameUserId, UIIntent returnToUIItent = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendChatContentSailReport(UIIntent returnToUIIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendChatContentTeamInvite(UIIntent returnToUIItent)
        {
        }

        [MethodImpl(0x8000)]
        private bool SendChatMsgByToggle(ChatInfo sendChatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SendChatMsgReqByChannel(ChatChannel channel, ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendDelegateMissionItemChatLink(DelegateMissionInfo missionInfo, UIIntent returnToUIIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildChatMsgReq(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendItemChatLink(StoreItemType itemType, int confId, UIIntent returnToUIIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendKillRecordChatLink(KillRecordInfo recordInfo, UIIntent returnToUIIntent)
        {
        }

        [MethodImpl(0x8000)]
        private void SendLocalChatMsgReq(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static ChatLinkSendUITask SendShipCustomTemplateInfoChatLink(ShipCustomTemplateInfo templateInfo, UIIntent returnToUIIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SendSolarSystemChatMsgReq(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SendStarFieldChatMsgReq(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SendTeamChatMsgReq(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SendWisperChatMsgReq(ChatInfo chatInfo, string reciveGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        protected static void SetChatInfoBasicData(ChatInfo chatInfo, ChatContentType chatContentType)
        {
        }

        [MethodImpl(0x8000)]
        protected static ChatLinkSendUITask StartChatLinkSendUITask(UIIntent returnToUIIntent, ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockChatClient m_lbChat
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext m_playerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

