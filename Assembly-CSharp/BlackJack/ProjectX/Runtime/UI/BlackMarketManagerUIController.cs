﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class BlackMarketManagerUIController : UIControllerBase
    {
        [AutoBind("./BlackMarketCategoryGroup/ResourcesShopButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ResourcesShopButtonCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/ResourcesShopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ResourcesShopButton;
        [AutoBind("./BlackMarketCategoryGroup/FunctionalItemsButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FunctionalItemsButtonCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/FunctionalItemsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FunctionalItemsButton;
        [AutoBind("./BlackMarketCategoryGroup/RechargeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RechargeButtonCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/RechargeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RechargeButton;
        [AutoBind("./BlackMarketCategoryGroup/WeeklyGiftPackButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WeeklyGiftPackButtonCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/GiftPackageButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GiftPackageButtonCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/WeeklyGiftPackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WeeklyGiftPackButton;
        [AutoBind("./BlackMarketCategoryGroup/GiftPackageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GiftPackageButton;
        [AutoBind("./BlackMarketCategoryGroup/MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MonthCardButtonCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MonthCardButton;
        [AutoBind("./TitleBGImage/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BlackMarkTitleText;
        [AutoBind("./TitleBGImage/MoneyGroup/CTitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CMoneyNumberText;
        [AutoBind("./TitleBGImage/MoneyGroup/CTitleMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CTitleMoneyGroupGameObject;
        [AutoBind("./TitleBGImage/MoneyGroup/YTitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text YMoneyNumberText;
        [AutoBind("./TitleBGImage/MoneyGroup/YTitleMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject YTitleMoneyGroupGameObject;
        [AutoBind("./TitleBGImage/MoneyGroup/GTitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GMoneyNumberText;
        [AutoBind("./TitleBGImage/MoneyGroup/GTitleMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GTitleMoneyGroupGameObject;
        [AutoBind("./TitleBGImage/MoneyGroup/STitleMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SMoneyNumberText;
        [AutoBind("./TitleBGImage/MoneyGroup/STitleMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject STitleMoneyGroupGameObject;
        [AutoBind("./TitleBGImage/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BlackMarketUIPrefabCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/DailyGiftPackageButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DailyGiftPackButtonCommonUIStateController;
        [AutoBind("./BlackMarketCategoryGroup/DailyGiftPackageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DailyGiftPackgeButton;
        [AutoBind("./TitleBGImage/MoneyGroup/YTitleMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx IrMoneyAddButton;
        [AutoBind("./TitleBGImage/MoneyGroup/GTitleMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildTradeMoneyAddButton;
        [AutoBind("./TitleBGImage/MoneyGroup/STitleMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PersonalTradeMoneyAddButton;
        [AutoBind("./TitleBGImage/MoneyGroup/CTitleMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BindMoneyAddButton;
        [AutoBind("./BlackMarketCategoryGroup/MonthCardButton/RedImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MonthCardButtonRedPoint;
        [CompilerGenerated]
        private static Dictionary<string, int> <>f__switch$map1;
        private static DelegateBridge __Hotfix_GetShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_UpdateMoneyType;
        private static DelegateBridge __Hotfix_UpdateMoney;
        private static DelegateBridge __Hotfix_SelectByMode;
        private static DelegateBridge __Hotfix_EnableSpecialGiftPackageButton;
        private static DelegateBridge __Hotfix_UpdateBlackMarkManager;
        private static DelegateBridge __Hotfix_SetAvailableList;
        private static DelegateBridge __Hotfix_GetMenuItemByMode;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public void EnableSpecialGiftPackageButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateController GetMenuItemByMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetShowOrHideUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SelectByMode(string selectedMode, List<string> modeList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAvailableList(List<string> modeList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBlackMarkManager()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMoney(ulong bindMoney, ulong tradeMoney, ulong realMoney, ulong guildTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMoneyType(string mode, ResourceShopItemCategory shopCategory = 1)
        {
        }
    }
}

