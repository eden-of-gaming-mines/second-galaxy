﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ScreenFadeInOutUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_fadeImage;
        [AutoBind("./LoadingImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_loadImage;
        [AutoBind("./LoadingImage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_loadProcessImage;
        protected string UIState_FadeIn;
        protected string UIState_FadeOut;
        protected string UIState_ImmediateFadeIn;
        protected string UIState_ImmediateFadeOut;
        protected string UIState_ManualFadeIn;
        protected string UIState_ManualFadeOut;
        private static DelegateBridge __Hotfix_StartFadeInOut;
        private static DelegateBridge __Hotfix_RegisteStateEndFunc;
        private static DelegateBridge __Hotfix_UpdateLoadProcessValue;
        private static DelegateBridge __Hotfix_ShowLoadingImage;
        private static DelegateBridge __Hotfix_GetImmediateFadeTime;
        private static DelegateBridge __Hotfix_GetFixedFadeTime;

        [MethodImpl(0x8000)]
        public float GetFixedFadeTime()
        {
        }

        [MethodImpl(0x8000)]
        public float GetImmediateFadeTime()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisteStateEndFunc(Action onStateEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLoadingImage(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void StartFadeInOut(bool isFadeIn, bool isImmediate, Color fadeColor, float m_manualFadeTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLoadProcessValue(float val)
        {
        }
    }
}

