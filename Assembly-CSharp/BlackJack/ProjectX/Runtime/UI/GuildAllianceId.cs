﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildAllianceId
    {
        private uint m_guildId;
        public ushort m_guildShortId;
        private uint m_allianceId;
        public ushort m_allianceShortId;
        private static readonly Dictionary<int, uint> m_guildAllianceShortIdDict = new Dictionary<int, uint>();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_GuildId;
        private static DelegateBridge __Hotfix_set_GuildId;
        private static DelegateBridge __Hotfix_get_AllianceId;
        private static DelegateBridge __Hotfix_set_AllianceId;
        private static DelegateBridge __Hotfix_IntIdToShort;
        private static DelegateBridge __Hotfix_CheckShortIdConflicted;

        [MethodImpl(0x8000)]
        private static bool CheckShortIdConflicted(int keyId, uint intId)
        {
        }

        [MethodImpl(0x8000)]
        private static ushort IntIdToShort(uint intId, ushort maxCount, int offset, int loopBit)
        {
        }

        public uint GuildId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public uint AllianceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

