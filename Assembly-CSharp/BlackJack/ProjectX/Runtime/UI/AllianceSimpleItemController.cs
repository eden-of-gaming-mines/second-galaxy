﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceSimpleItemController : UIControllerBase, IScrollItem
    {
        private Action<uint> m_onClickEvent;
        private Action<uint> m_onClickApplyEvent;
        private Action<uint> m_onClickIgnoreEvent;
        private uint m_allianceId;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailBtn;
        [AutoBind("./AllianceNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceName;
        [AutoBind("./GuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceLeaderGuildName;
        [AutoBind("./GuildText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceGuildCount;
        [AutoBind("./SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_allianceLogo;
        [AutoBind("./CounryText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_regionName;
        [AutoBind("./CounryImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_regionIcon;
        [AutoBind("./JoinButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_inviteBtn;
        [AutoBind("./IgnoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_ignoreBtn;
        [AutoBind("./NoInvitation", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_noInvitation;
        private const string GuildNameFormat = "[{0}]{1}";
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetData;
        private static DelegateBridge __Hotfix_OnClick;
        private static DelegateBridge __Hotfix_OnApplyClick;
        private static DelegateBridge __Hotfix_OnIgnoreClick;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplyClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnIgnoreClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetData(GuildAllianceInviteInfo info, Dictionary<string, UnityEngine.Object> resDict, Action<uint> clickEvent, Action<uint> clickApplyEvent = null, Action<uint> clickIgnoreEvent = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }
    }
}

