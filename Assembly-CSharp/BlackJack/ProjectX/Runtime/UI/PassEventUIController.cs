﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class PassEventUIController : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
    {
        private static DelegateBridge __Hotfix_OnPointerClick;
        private static DelegateBridge __Hotfix_OnPointerDown;
        private static DelegateBridge __Hotfix_OnPointerUp;
        private static DelegateBridge __Hotfix_PassEventNextFrame;
        private static DelegateBridge __Hotfix_PassEvent;

        [MethodImpl(0x8000)]
        public void OnPointerClick(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerDown(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPointerUp(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        protected void PassEvent<T>(PointerEventData data, ExecuteEvents.EventFunction<T> function) where T: IEventSystemHandler
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator PassEventNextFrame(PointerEventData eventData)
        {
        }

        [CompilerGenerated]
        private sealed class <PassEventNextFrame>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal PointerEventData eventData;
            internal PassEventUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

