﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BattlePassMainTabChallangeUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action OnQuestItemCompleteEvent;
        private const int POOL_SIZE = 10;
        private int m_CurWeekIndex;
        private const float ACC_PROGRESS_BASE_VAL = 0.25f;
        protected ConfigDataBattlePassChallangeQuestGroupInfo m_CurBattlePassChallangeQuestGroupInfo;
        private Dictionary<string, UnityEngine.Object> m_resData;
        protected const string TabChallangeItemName = "BattlePassChallangeItemUIPrefab";
        private GameObject m_TabChallangeItemTemplateGo;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./TemplateRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TemplateRoot;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect LoopScrollView;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool SelfEasyObjectPool;
        [AutoBind("./NextButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NextButton;
        [AutoBind("./NextButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NextButtonUIState;
        [AutoBind("./LastButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LastButton;
        [AutoBind("./LastButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LastButtonUIState;
        [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./WeekImage/WeekText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeekText;
        [AutoBind("./AwardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AwardGroupUIState;
        [AutoBind("./AwardGroup/ChallengeGroup/CircleGroup/Normal/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeButton;
        [AutoBind("./AwardGroup/ChallengeGroup/CircleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CircleGroupUIState;
        [AutoBind("./AwardGroup/ChallengeGroup/CircleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GetButton;
        [AutoBind("./AwardGroup/ChallengeGroup/CircleGroup/MoleculeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoleculeText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTemplateItem;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolTablChallangeItemCreated;
        private static DelegateBridge __Hotfix_OnItemButtonClick;
        private static DelegateBridge __Hotfix_SendQuestCompleteComfirmReq;
        private static DelegateBridge __Hotfix_RefillCells;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateChallangeAccRewardState;
        private static DelegateBridge __Hotfix_GetAccStateStr;
        private static DelegateBridge __Hotfix_OnTablRewardItemFill;
        private static DelegateBridge __Hotfix_add_OnQuestItemCompleteEvent;
        private static DelegateBridge __Hotfix_remove_OnQuestItemCompleteEvent;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action OnQuestItemCompleteEvent
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CreateTemplateItem()
        {
        }

        [MethodImpl(0x8000)]
        private string GetAccStateStr(float progress)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolTablChallangeItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemButtonClick(BattlePassChallangeItemUIController item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTablRewardItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RefillCells(ConfigDataBattlePassChallangeQuestGroupInfo groupInfo, int curWeekIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void SendQuestCompleteComfirmReq(BattlePassChallangeItemUIController item, int instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChallangeAccRewardState(int comepleteCount, int totalCount, bool getAll)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(bool getAll, int index, int weekIndexMax, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnEasyPoolTablChallangeItemCreated>c__AnonStorey0
        {
            internal BattlePassChallangeItemUIController commonItemCtrl;
            internal BattlePassMainTabChallangeUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendQuestCompleteComfirmReq>c__AnonStorey1
        {
            internal QuestCompleteComfirmReqTask netWorkTask;
            internal int instanceId;
            internal BattlePassChallangeItemUIController item;
            internal BattlePassMainTabChallangeUIController $this;

            internal void <>m__0(Task task)
            {
                if (this.netWorkTask.IsNetworkError)
                {
                    Debug.LogError("QuestCompleteComfirmReqTask  Error " + this.instanceId);
                }
                else
                {
                    this.item.SetItemStateByAck();
                    if (this.$this.OnQuestItemCompleteEvent != null)
                    {
                        this.$this.OnQuestItemCompleteEvent();
                    }
                }
            }
        }
    }
}

