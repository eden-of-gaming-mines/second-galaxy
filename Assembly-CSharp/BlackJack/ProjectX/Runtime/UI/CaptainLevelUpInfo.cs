﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CaptainLevelUpInfo
    {
        public ulong m_captainInstanceId;
        public int m_preLevel;
        public int m_currentLevel;
        public List<LBNpcCaptainFeats> m_preFeats;
        public List<LBNpcCaptainFeats> m_newFeats;
        public List<LBNpcCaptainFeats> m_upgradeFeats;
        private static DelegateBridge _c__Hotfix_ctor;

        [MethodImpl(0x8000)]
        public CaptainLevelUpInfo(ulong captainInsId, int preLevel, int currentLevel, List<LBNpcCaptainFeats> preFeats, List<LBNpcCaptainFeats> newFeats, List<LBNpcCaptainFeats> upFeats)
        {
        }
    }
}

