﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class UploadClientLogReasonUIController : UIControllerBase
    {
        private string m_currState;
        private const string StateNormal = "Normal";
        private const string StateChoose = "Choose";
        [AutoBind("./IssueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_issueText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_issueBtn;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetButtonState;
        private static DelegateBridge __Hotfix_GetIssueString;
        private static DelegateBridge __Hotfix_OnIssueBtnClick;

        [MethodImpl(0x8000)]
        public string GetIssueString()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonState(bool choose)
        {
        }
    }
}

