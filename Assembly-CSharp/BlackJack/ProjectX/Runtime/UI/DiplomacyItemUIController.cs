﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DiplomacyItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, DiplomacyState> EventOnToggleSelect;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnGuildDiplomacyRemoveButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnGuildDiplomacyNoPermissionButtonClick;
        private const string ItemStatePlayer = "Player";
        private const string ItemStateGuild = "Guild";
        private const string ItemStateAlliance = "Alliance";
        private const string ItemInfoStateRemove = "Remove";
        private const string ItemInfoStateSetRelation = "SetRelation";
        private const string ItemInfoStateSameOrganization = "SameOrganization";
        private const string ItemInfoStateSelf = "Self";
        private const string StateNormal = "Normal";
        private const string StateGray = "Gray";
        private const string AssetPathCommonCaptain = "CommonCaptainUIPrefab";
        private const string AssetPathCommonGuildLogo = "CommonGuildLogo";
        private const string StateEnemy = "Enemy";
        private const string StateFriend = "Friend";
        private const string StateNeutral = "Neutral";
        private const string StateSameGuild = "SameGuild";
        private const string StateSameAlliance = "SameAlliance";
        private DiplomacyItemInfo m_diplomacyItemInfo;
        public ScrollItemBaseUIController m_scrollCtrl;
        public CommonCaptainIconUIController m_captainIconUICtrl;
        public GuildLogoController m_guildLogoUICtrl;
        public GuildLogoController m_allianceLogoUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ItemInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemInfoStateCtrl;
        [AutoBind("./Title", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_title;
        [AutoBind("./ItemInfo/PlayerIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_playerIconDummy;
        [AutoBind("./ItemInfo/GuildLogoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_guildLogoDummy;
        [AutoBind("./ItemInfo/AllianceLogoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_allianceLogoDummy;
        [AutoBind("./ItemInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./ItemInfo/EvaluateScore/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterEvaluateScore;
        [AutoBind("./ItemInfo/RelativeOrganization/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_relativeOrganizationText;
        [AutoBind("./ItemInfo/RelativeOrganization", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_relativeOrganizationStateCtrl;
        [AutoBind("./ItemInfo/Member/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_memberCountText;
        [AutoBind("./ItemInfo/Country/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_regionNameText;
        [AutoBind("./ItemInfo/Country/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_regionIconImage;
        [AutoBind("./ItemInfo/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_removeButton;
        [AutoBind("./ItemInfo/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_removeButtonStateCtrl;
        [AutoBind("./ItemInfo/RelationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_relationSetStateCtrl;
        [AutoBind("./ItemInfo/RelationGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_friendToggle;
        [AutoBind("./ItemInfo/RelationGroup/Neutral", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_neutralToggle;
        [AutoBind("./ItemInfo/RelationGroup/Enemy", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_enemyToggle;
        [AutoBind("./ItemInfo/RelationGroup/NoPermission", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_noPermissionButton;
        [AutoBind("./ItemInfo/RelationshipImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_relationshipImageStateCtrl;
        [AutoBind("./ItemInfo/GuildDiplomacyState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildDiplomacyStateCtrtl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_SetDiplomacyType;
        private static DelegateBridge __Hotfix_SetRelationshipImageState;
        private static DelegateBridge __Hotfix_GetSimpleCharacterPos;
        private static DelegateBridge __Hotfix_GetCharacterIconSize;
        private static DelegateBridge __Hotfix_IsSameDiplomacyItem;
        private static DelegateBridge __Hotfix_SetExtraItemInfo;
        private static DelegateBridge __Hotfix_OnFriendToggleValueChange;
        private static DelegateBridge __Hotfix_OnNeutralToggleValueChange;
        private static DelegateBridge __Hotfix_OnEnemyToggleValueChange;
        private static DelegateBridge __Hotfix_OnItemRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnNoPermissionRemoveButtonClick;
        private static DelegateBridge __Hotfix_SetName;
        private static DelegateBridge __Hotfix_SetPlayerName;
        private static DelegateBridge __Hotfix_SetAllianceName;
        private static DelegateBridge __Hotfix_SetRelativeOrganizationName;
        private static DelegateBridge __Hotfix_SetLanguageType;
        private static DelegateBridge __Hotfix_SetEvaluateScore;
        private static DelegateBridge __Hotfix_SetMemberCount;
        private static DelegateBridge __Hotfix_SetItemInfoState;
        private static DelegateBridge __Hotfix_add_EventOnToggleSelect;
        private static DelegateBridge __Hotfix_remove_EventOnToggleSelect;
        private static DelegateBridge __Hotfix_add_EventOnGuildDiplomacyRemoveButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildDiplomacyRemoveButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildDiplomacyNoPermissionButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildDiplomacyNoPermissionButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action<UIControllerBase> EventOnGuildDiplomacyNoPermissionButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnGuildDiplomacyRemoveButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, DiplomacyState> EventOnToggleSelect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Vector2 GetCharacterIconSize()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSimpleCharacterPos()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSameDiplomacyItem(DiplomacyItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyToggleValueChange(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFriendToggleValueChange(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemRemoveButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNeutralToggleValueChange(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNoPermissionRemoveButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAllianceName(string allianceName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDiplomacyType(DiplomacyState type)
        {
        }

        [MethodImpl(0x8000)]
        private void SetEvaluateScore(int score)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExtraItemInfo(bool isGuildDiplomacy, string playerUserId, uint guildId, uint allianceId, uint selfGuildId, uint selfAllianceId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetItemInfoState(bool isSearch, bool isSameOrganization, DiplomacyState type, bool hasPromissionDiplomacyUpdate)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLanguageType(GuildAllianceLanguageType languageType, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMemberCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        private void SetName(string codeName, string fullName)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPlayerName(string playerName, string codeName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRelationshipImageState(DiplomacyItemType type, DiplomacyState state)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRelativeOrganizationName(string codeName, string organizationName)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(DiplomacyItemInfo diplomacyItemInfo, bool isSearchResult, Dictionary<string, UnityEngine.Object> resDict, uint selfGuildId = 0, uint selfAllianceId = 0, bool hasPromissionDiplomacyUpdate = true, bool isGuildDiplomacy = true)
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

