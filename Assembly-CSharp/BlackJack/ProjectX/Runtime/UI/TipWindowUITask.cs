﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class TipWindowUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnShowTipsEnd;
        private TipWindowUIController m_mainCtrl;
        private float m_lastTime;
        private readonly LinkedList<TipInfoData> m_tipInfoList;
        private readonly LinkedList<TipInfoData> m_remainTipInfoList;
        private TipInfoData m_currTipInfo;
        private static string m_normalMode;
        private static string m_paramKeyTipInfoData;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "TipWindowUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ClearMsg;
        private static DelegateBridge __Hotfix_ShowTipWindow;
        private static DelegateBridge __Hotfix_ShowTipWindowWithStringTableId;
        private static DelegateBridge __Hotfix_ShowTipWindowWithErrorCode;
        private static DelegateBridge __Hotfix_ShowSystemFunctionNotOpenTipWindow;
        private static DelegateBridge __Hotfix_GetMsgCount;
        private static DelegateBridge __Hotfix_BringTipWindowToTop;
        private static DelegateBridge __Hotfix_ClearTipWindowEndEvent;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_PushTipInfo;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateTipWindowInfo;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_EnableUIInput_0;
        private static DelegateBridge __Hotfix_EnableUIInput_1;
        private static DelegateBridge __Hotfix_add_EventOnShowTipsEnd;
        private static DelegateBridge __Hotfix_remove_EventOnShowTipsEnd;
        private static DelegateBridge __Hotfix_get_PlyCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnShowTipsEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public TipWindowUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringTipWindowToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearMsg()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearTipWindowEndEvent()
        {
        }

        [MethodImpl(0x8000)]
        public override void EnableUIInput(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public override void EnableUIInput(bool isEnable, bool isGlobalEnable)
        {
        }

        [MethodImpl(0x8000)]
        public int GetMsgCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void PushTipInfo(TipInfoData tipInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowSystemFunctionNotOpenTipWindow(SystemFuncType funcType, bool isImportant = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowTipWindow(string tipInfo, bool isImportant = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowTipWindowWithErrorCode(int errCode, bool isError = true, bool isImportant = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowTipWindowWithStringTableId(StringTableId stringTableId, bool isImportant = false, params object[] args)
        {
        }

        [MethodImpl(0x8000)]
        private static void StartTask(TipInfoData tipInfoData)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTipWindowInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlyCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private class TipInfoData
        {
            public readonly string m_content;
            public readonly TipWindowUITask.TipType m_tipType;
            public readonly bool m_isImportant;
            private static DelegateBridge _c__Hotfix_ctor;

            [MethodImpl(0x8000)]
            public TipInfoData(string content, TipWindowUITask.TipType tipType, bool isImportant = false)
            {
            }
        }

        public enum TipType
        {
            Normal,
            Error
        }
    }
}

