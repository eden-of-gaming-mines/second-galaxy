﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    internal class ProjectXGMCommandProcess
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_PreproccessCmd;
        private static DelegateBridge __Hotfix_ProcessHangarQuaryPropertiesCmd;
        private static DelegateBridge __Hotfix_ProcessSpaceQuaryPropertiesCmd;
        private static DelegateBridge __Hotfix_ProcessSpaceShipCmd;
        private static DelegateBridge __Hotfix_ProcessTargetSpaceShipCmd;
        private static DelegateBridge __Hotfix_ProcessCaptainQuaryPropertiesCmd;

        [MethodImpl(0x8000)]
        public void PreproccessCmd(string gmCommond)
        {
        }

        [MethodImpl(0x8000)]
        private bool ProcessCaptainQuaryPropertiesCmd(List<KeyValuePair<string, string>> parameterList, ref string gmCommond)
        {
        }

        [MethodImpl(0x8000)]
        private bool ProcessHangarQuaryPropertiesCmd(List<KeyValuePair<string, string>> parameterList, ref string gmCommond)
        {
        }

        [MethodImpl(0x8000)]
        private bool ProcessSpaceQuaryPropertiesCmd(List<KeyValuePair<string, string>> parameterList, ref string gmCommond)
        {
        }

        [MethodImpl(0x8000)]
        private bool ProcessSpaceShipCmd(List<KeyValuePair<string, string>> parameterList, ref string gmCommond)
        {
        }

        [MethodImpl(0x8000)]
        private bool ProcessTargetSpaceShipCmd(List<KeyValuePair<string, string>> parameterList, ref string gmCommond)
        {
        }
    }
}

