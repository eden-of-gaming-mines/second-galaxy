﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public sealed class GuildFlagShipUnpackConfirmUIController : UIControllerBase
    {
        [AutoBind("./ButtonGroup/GoOnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GoOnButton;
        [AutoBind("./ButtonGroup/StopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StopButton;
        [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateController;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_Hide;

        [MethodImpl(0x8000)]
        public void Hide(bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void Show(string timeStr)
        {
        }
    }
}

