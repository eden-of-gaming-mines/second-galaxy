﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipHangarAssignUITask : UITaskBase
    {
        private IUIBackgroundManager m_backgroundManager;
        private ShipHangarAssignUIController m_mainCtrl;
        private ItemStoreDefaultUIFilter m_itemStoreFilter;
        private List<ILBStoreItemClient> m_cachedStoreItemList;
        private DateTime m_lastItemStoreUpdateTime;
        private int m_selectedItemIndex;
        private int m_selectedShipHangarIndex;
        private AuctionBuyItemFilterType m_currAuctionFilter;
        public static string ParamKey_IsRefreshAll;
        public static string ParamKey_ShipHangerIndex;
        public static string ParamKey_BackgroundManager;
        public static string ParamKey_ShipHangarAssignMode;
        public static string ParamKey_GuildShipHangarInstanceId;
        public static string ParamKey_WantedShipRankType;
        private ShipHangarAssignModeType m_CurShipHangarAssignModeType;
        private ulong m_GuildShipHangarInstanceId;
        private RankType m_GuildHangarRankType;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ShipHangarAssignUITask";
        private bool m_isInRepeatableGuide;
        private ShipType m_wantedShipType;
        private int m_currStep;
        private List<ILBStoreItemClient> m_suitableItemList;
        public static string ParamKey_IsRepeatableGuide;
        public static string ParamKey_WantedShipType;
        [CompilerGenerated]
        private static Comparison<ILBStoreItemClient> <>f__mg$cache0;
        [CompilerGenerated]
        private static Comparison<ILBStoreItemClient> <>f__mg$cache1;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartShipHangarAssignUITask;
        private static DelegateBridge __Hotfix_StartShipHangarAssignGuildUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsAuctionFilterConditionOpen;
        private static DelegateBridge __Hotfix_UpdateItemListUI;
        private static DelegateBridge __Hotfix_ShowSelectedItemInfo;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_OnGoShopButtonClick;
        private static DelegateBridge __Hotfix_OnGoTradingPlaceButton;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnItemListClick;
        private static DelegateBridge __Hotfix_OnItemListClickImp;
        private static DelegateBridge __Hotfix_OnFilterButtonClick;
        private static DelegateBridge __Hotfix_OnFilterBackGroundButtonClick;
        private static DelegateBridge __Hotfix_InitFilterState;
        private static DelegateBridge __Hotfix_OnFilterChange;
        private static DelegateBridge __Hotfix_GetAuctionConditionFilter;
        private static DelegateBridge __Hotfix_OnItemListUIDropdownChanged;
        private static DelegateBridge __Hotfix_OnItemWatchButtonClick;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnItemAssembleButtonClick;
        private static DelegateBridge __Hotfix_OnItemAssembleButtonClickImp;
        private static DelegateBridge __Hotfix_ClickFirstItem;
        private static DelegateBridge __Hotfix_ClickShipAssignConfirmButton;
        private static DelegateBridge __Hotfix_CheckRankType;
        private static DelegateBridge __Hotfix_GuildShipUnpackMain;
        private static DelegateBridge __Hotfix_GuildShipUnpackCheck;
        private static DelegateBridge __Hotfix_SendGuildFlagShipHangarUnpackShipReq;
        private static DelegateBridge __Hotfix_OnGuildUnpackStopButtonClick;
        private static DelegateBridge __Hotfix_OnGuildUnpackGoOnButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagShipUnPackError;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_StartHangarUnpackShipReq;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetFirstShipItemRect;
        private static DelegateBridge __Hotfix_StartReaptableGuide;
        private static DelegateBridge __Hotfix_StartReaptableUserGuide;
        private static DelegateBridge __Hotfix_StopReaptableGuide;
        private static DelegateBridge __Hotfix_ShipComparer;
        private static DelegateBridge __Hotfix_get_m_playerCtx;

        [MethodImpl(0x8000)]
        public ShipHangarAssignUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckRankType(ConfigDataSpaceShipInfo flagShipConfInfo, out int errCode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstItem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickShipAssignConfirmButton()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool GetAuctionConditionFilter(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstShipItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void GuildShipUnpackCheck()
        {
        }

        [MethodImpl(0x8000)]
        private void GuildShipUnpackMain()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitFilterState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAuctionFilterConditionOpen()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterBackGroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterChange(AuctionBuyItemFilterType auctionBuyItemFilter)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoShopButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoTradingPlaceButton()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipUnPackError(int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildUnpackGoOnButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildUnpackStopButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemAssembleButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemAssembleButtonClickImp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListClickImp(int idx, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUIDropdownChanged(int dropdownIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemWatchButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFlagShipHangarUnpackShipReq(int storeItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private int ShipComparer(ILBStoreItemClient itemA, ILBStoreItemClient itemB)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowSelectedItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void StartHangarUnpackShipReq(int hangarIndex, ulong selectedItemInsId)
        {
        }

        [MethodImpl(0x8000)]
        private void StartReaptableGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartReaptableUserGuide(RectTransform operateRect, Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartShipHangarAssignGuildUITask(UIIntent cusIntent, ulong instanceId, int hangarIndex, bool isInRepeatUserGuide, RankType wantedRankType, IUIBackgroundManager backGroudManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static ShipHangarAssignUITask StartShipHangarAssignUITask(UIIntent returnIntent, int hangarIndex, bool isInRepeatUserGuide, ShipType wantedShipType, IUIBackgroundManager backGroudManager = null, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StopReaptableGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemListUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext m_playerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GuildShipUnpackCheck>c__AnonStorey0
        {
            internal ILBStoreItemClient item;
            internal ShipHangarAssignUITask $this;

            internal void <>m__0(bool end)
            {
                if (end)
                {
                    this.$this.SendGuildFlagShipHangarUnpackShipReq(this.item.GetStoreItemIndex());
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartReaptableGuide>c__AnonStorey1
        {
            internal int idx;
            internal CommonItemIconUIController m_selectedCtrl;
            internal ShipHangarAssignUITask.<StartReaptableGuide>c__AnonStorey2 <>f__ref$2;

            internal void <>m__0()
            {
                this.m_selectedCtrl = this.<>f__ref$2.$this.m_mainCtrl.m_itemStoreListUICtrl.GetActiveItemByIndex(this.idx);
                this.<>f__ref$2.$this.StartReaptableUserGuide(this.m_selectedCtrl.transform as RectTransform, this.<>f__ref$2.camera);
            }
        }

        [CompilerGenerated]
        private sealed class <StartReaptableGuide>c__AnonStorey2
        {
            internal Camera camera;
            internal ShipHangarAssignUITask $this;
        }

        protected enum PipeLineStateMaskType
        {
            IsRefreshItemList,
            IsRefreshSingleItem,
            IsUpdateSelectItemSimpleInfo,
            IsNeedLoadDynamicRes,
            IsResetTypeFilter
        }

        public enum ShipHangarAssignModeType
        {
            CustomShipHangar,
            GuildShipHangar
        }
    }
}

