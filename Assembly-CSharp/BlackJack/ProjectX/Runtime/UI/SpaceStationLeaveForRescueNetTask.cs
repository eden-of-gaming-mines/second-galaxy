﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceStationLeaveForRescueNetTask : NetWorkTransactionTask
    {
        public ulong m_shipInstanceId;
        public ulong m_signalInsId;
        private int m_solarSystemId;
        private uint m_sceneInstanceId;
        public int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnSpaceStationLeaveForRescueAck;
        private static DelegateBridge __Hotfix_OnSpaceEnterNtf;

        [MethodImpl(0x8000)]
        public SpaceStationLeaveForRescueNetTask(ulong shipInstanceId, ulong signalInsId, int solarSystemId, uint sceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceEnterNtf(bool isLeaveStation)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationLeaveForRescueAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

