﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapDrawingGlobalConfig
    {
        [Header("星域星图完全透明时的缩放比")]
        public float m_scaleValueForStarMapAlphaMin;
        [Header("星域星图完全不透明时的缩放比")]
        public float m_scaleValueForStarMapAlphaMax;
        [Header("默认缩放比")]
        public float m_defaultScaleValue;
        [Header("定位操作后的缩放比")]
        public float m_orientationScaleValue;
        [Header("最小缩放值")]
        public float m_minScaleValue;
        [Header("最大缩放值")]
        public float m_maxScaleValue;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

