﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BattlePassRewardItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        public bool m_isPreviewLv;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BattlePassRewardItemUIController, CommonItemIconUIController, int, bool, QuestRewardInfo> OnEventRewardIconItemClick;
        public ConfigDataBattlePassLevelInfo m_BattlePassLevelInfoConf;
        private CommonItemIconUIController m_HonorItemUICtrl;
        private CommonItemIconUIController m_TrumpItemUICtrlA;
        private CommonItemIconUIController m_TrumpItemUICtrlB;
        protected const string CommonItemName = "CommonItemUIPrefab";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController ScrollItemCtrl;
        [AutoBind("./HonorIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController HonorUIStateCtrl;
        [AutoBind("./HonorIconImage/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform HonorItemDummy;
        [AutoBind("./HonorIconImage/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController HonorItemDummyUIState;
        [AutoBind("./TrumpBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TrumpUIStateCtrl;
        [AutoBind("./TrumpBgImage/ItemDummy01", AutoBindAttribute.InitState.NotInit, false)]
        public Transform TrumpItemDummy01;
        [AutoBind("./TrumpBgImage/ItemDummy01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TrumpItemDummy01UIState;
        [AutoBind("./TrumpBgImage/ItemDummy02", AutoBindAttribute.InitState.NotInit, false)]
        public Transform TrumpItemDummy02;
        [AutoBind("./TrumpBgImage/ItemDummy02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TrumpItemDummy02UIState;
        [AutoBind("./HonorIconImage/ClassBgImage/ClassText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ClassText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_InitControl;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemTrumpClickA;
        private static DelegateBridge __Hotfix_OnItemTrumpClickB;
        private static DelegateBridge __Hotfix_ClearIconSelected;
        private static DelegateBridge __Hotfix_SetGetFlagByAck;
        private static DelegateBridge __Hotfix_CreateCommonItem;
        private static DelegateBridge __Hotfix_GetFakeLBStoreItem;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetItemUIState;
        private static DelegateBridge __Hotfix_IsShowHintEffect;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_OnEventRewardIconItemClick;
        private static DelegateBridge __Hotfix_remove_OnEventRewardIconItemClick;

        public event Action<BattlePassRewardItemUIController, CommonItemIconUIController, int, bool, QuestRewardInfo> OnEventRewardIconItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearIconSelected()
        {
        }

        [MethodImpl(0x8000)]
        private CommonItemIconUIController CreateCommonItem(Transform parent)
        {
        }

        [MethodImpl(0x8000)]
        private FakeLBStoreItem GetFakeLBStoreItem(QuestRewardInfo rewardInfo, out ItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private string GetItemUIState(ConfigDataBattlePassLevelInfo lvInfo, bool isUpgrade, bool isSpecia)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool isCareItemClick)
        {
        }

        [MethodImpl(0x8000)]
        public void InitControl()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsShowHintEffect(ConfigDataBattlePassLevelInfo lvInfo, bool isUpgrade, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemTrumpClickA(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemTrumpClickB(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGetFlagByAck(bool isUpgrade)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(ConfigDataBattlePassLevelInfo lvInfo, int childSelectedIndex, bool isSpecia, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

