﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class DrivingLicenseInfoUITask : UITaskBase
    {
        private DrivingLicenseNodeInfoUIController m_currSlectedNode;
        private Dictionary<ShipType, List<ConfigDataDrivingLicenseInfo>> m_currDrivingSkillInfoDic2ShipType;
        private GrandFaction m_selectedGrandfactionType;
        private RankType m_selectedRankType;
        private RankType m_noticeRankType;
        private int m_needSkillID;
        private int m_needSkillLevel;
        private List<ShipType> m_shipTypeList;
        private List<GrandFaction> m_grandFactionList;
        private List<RankType> m_rankTypeList;
        private List<float> m_processValueList;
        private DrivingLicenseInfoUIController m_mainCtrl;
        private DrivingToggleListUIController m_toggleListCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private const string ParmaStringKey_GranFaction = "GrandFaction";
        private const string ParmaStringKey_Tech = "Tech";
        private const string ParmaStringKey_ShipType = "Type";
        private const string ParmaStringKey_NeedNoticePlayer = "ParmaStringKey_NeedNoticePlayer";
        private const string ParmaStringKey_NeedSkillID = "ParmaStringKey_NeedSkillID";
        private const string ParmaStringKey_NeedSkillLevel = "ParmaStringKey_NeedSkillLevel";
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private IUIBackgroundManager m_backgroundManager;
        public const string TaskName = "DrivingLicenseInfoUITask";
        private bool m_isDuringNotForceUserGuide;
        private LBProcessingQuestBase m_userGuideQuestInfo;
        private GuideState m_currGuideState;
        private ShipType m_userGuideSelectShipType;
        private const string ParamKey_UserGuideQuestInfo = "UserGuideQuestInfo";
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_Refresh;
        private static DelegateBridge __Hotfix_UpdateDataCache_Init;
        private static DelegateBridge __Hotfix_UpdateDataCache_Clear;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_StartDrivingLicenseUITask;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnNodeClick;
        private static DelegateBridge __Hotfix_OnGotoButtonClick;
        private static DelegateBridge __Hotfix_ShowUnLockDrivingLicenseTip;
        private static DelegateBridge __Hotfix_OnFactionToggleSelected;
        private static DelegateBridge __Hotfix_OnRankT1ToggleSelected;
        private static DelegateBridge __Hotfix_OnRankT2ToggleSelected;
        private static DelegateBridge __Hotfix_OnRankT3ToggleSelected;
        private static DelegateBridge __Hotfix_OnBackgroudButtonClick;
        private static DelegateBridge __Hotfix_HideNodeTips;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_IsNeedRefresh;
        private static DelegateBridge __Hotfix_OnDrivingLicenseUpgradeNtf;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_DisablePipelineStateMask;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetFactionToggleRect;
        private static DelegateBridge __Hotfix_GetSkillNodeRectBySkillConfId;
        private static DelegateBridge __Hotfix_GetDestroyerGoToCheckButtonRect;
        private static DelegateBridge __Hotfix_GetDestroyerGoToCheckButton;
        private static DelegateBridge __Hotfix_OnClickDestroyerCheckButton;
        private static DelegateBridge __Hotfix_MoveScrollRectToLoction;
        private static DelegateBridge __Hotfix_EnableScollview;
        private static DelegateBridge __Hotfix_SetFactionToggle;
        private static DelegateBridge __Hotfix_UpdatePanel;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbCharBasicProperties;
        private static DelegateBridge __Hotfix_get_LBDrivingClient;
        private static DelegateBridge __Hotfix_get_LBSkillClient;
        private static DelegateBridge __Hotfix_StartDrivingLicenseUITaskForNotForceGuide;
        private static DelegateBridge __Hotfix_GetNextGuideState;
        private static DelegateBridge __Hotfix_UpdateDataCache_NotForceUserGuide;
        private static DelegateBridge __Hotfix_ShowNotForceUserGuide;
        private static DelegateBridge __Hotfix_OnClickReturn4NotForceUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;

        [MethodImpl(0x8000)]
        public DrivingLicenseInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void DisablePipelineStateMask(PipeLineMask state)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineMask state)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableScollview(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private DrivingLiscesceNodeListItemUIController GetDestroyerGoToCheckButton()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetDestroyerGoToCheckButtonRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFactionToggleRect(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private GuideState GetNextGuideState()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSkillNodeRectBySkillConfId(int skillConfId)
        {
        }

        [MethodImpl(0x8000)]
        private void HideNodeTips()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedRefresh()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineMask state)
        {
        }

        [MethodImpl(0x8000)]
        public void MoveScrollRectToLoction(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackgroudButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnClickDestroyerCheckButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4NotForceUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDrivingLicenseUpgradeNtf(int skillID, int skillLevel)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFactionToggleSelected(GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNodeClick(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRankT1ToggleSelected(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRankT2ToggleSelected(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRankT3ToggleSelected(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetFactionToggle(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private static void ShowUnLockDrivingLicenseTip(int needSkillID, int needSkillLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static Task StartDrivingLicenseUITask(UIIntent preIntent, ConfigDataDrivingLicenseInfo drivingLisceneInfo, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static DrivingLicenseInfoUITask StartDrivingLicenseUITaskForNotForceGuide(UIIntent preIntent, LBProcessingQuestBase questInfo, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_Init()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_NotForceUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_Refresh()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePanel(GrandFaction faction, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCharacterBasicPropertiesBase LbCharBasicProperties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockDrivingLicenseClient LBDrivingClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCharacterSkillClient LBSkillClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCache_Init>c__AnonStorey0
        {
            internal ShipType shipType;

            internal bool <>m__0(ShipType e) => 
                (e == this.shipType);
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCache_Init>c__AnonStorey1
        {
            internal GrandFaction grandFaction;

            internal bool <>m__0(GrandFaction e) => 
                (e == this.grandFaction);
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCache_Init>c__AnonStorey2
        {
            internal RankType rankType;

            internal bool <>m__0(RankType e) => 
                (e == this.rankType);
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCache_Init>c__AnonStorey3
        {
            internal GrandFaction grandFaction;
            internal RankType rankType;

            internal bool <>m__0(GrandFaction e) => 
                (e == this.grandFaction);

            internal bool <>m__1(RankType e) => 
                (e == this.rankType);
        }

        private enum GuideState
        {
            None,
            Start,
            GotoCheck
        }

        public enum PipeLineMask
        {
            Init = 1,
            Resume = 2,
            GrandFactionChanged = 3,
            RankSelectChanged = 4,
            NoticeNeedSkill = 5,
            SelectedNode = 6
        }
    }
}

