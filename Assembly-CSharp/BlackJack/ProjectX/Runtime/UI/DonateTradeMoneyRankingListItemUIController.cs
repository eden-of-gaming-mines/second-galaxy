﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class DonateTradeMoneyRankingListItemUIController : UIControllerBase
    {
        public CommonCaptainIconUIController m_captainIconUICtrl;
        private const string CaptainUIAssetName = "CaptainIconUIPrefab";
        [AutoBind("./CommonCaptainUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform CommonCaptainUIDummy;
        [AutoBind("./DonateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DonateText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        private static DelegateBridge __Hotfix_UpdateRankingListItemUI;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRankingListItemUI(int index, ulong donateMoney, ProPlayerSimplestInfo player, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

