﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildWarUIController : UIControllerBase
    {
        private const string PoolName = "GuildWarItemPool";
        private List<GuildBattleSimpleReportInfo> m_dataList;
        private Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        public Action<GuildBattleSimpleReportInfo> m_eventOnItemClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemRoot;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_loopVerticalScrollRect;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitPanelInfo;
        private static DelegateBridge __Hotfix_GetGuildInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnRankingListItemFill;

        [MethodImpl(0x8000)]
        public UIProcess GetGuildInfoPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public void InitPanelInfo(List<GuildBattleSimpleReportInfo> dataList, int startIndex, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject targetItem)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRankingListItemFill(UIControllerBase ctrl)
        {
        }
    }
}

