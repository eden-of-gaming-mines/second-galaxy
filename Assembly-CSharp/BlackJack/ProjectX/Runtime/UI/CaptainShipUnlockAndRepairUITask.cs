﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CaptainShipUnlockAndRepairUITask : UITaskBase
    {
        private LBStaticHiredCaptain m_selCaptain;
        private LBStaticHiredCaptain.ShipInfo m_selShipInfo;
        private List<ShipCompListConfInfo> m_unlockOrRepairShipCompList;
        private List<ShipCompListConfInfo> m_unlockOrRepairLackShipCompList;
        private float m_needMoney;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType, bool> EventOnShowItemSimpleInfo;
        private CaptainShipUnlockAndRepairUIController m_mainCtrl;
        private CaptainShipUnLockAndRepairItemBuyPanelUIController m_itemBuyPanelUICtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnReturnToShipManangerUITask;
        public const string TaskMode_UnlockShip = "UnlockShip";
        public const string TaskMode_RepairShip = "RepairShip";
        public static string ParamKey_MainUIIntent;
        public static string ParamKey_SelectedCaptain;
        public static string ParamKey_SelectedShip;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowOrHideUI;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnUnlockButtonClick;
        private static DelegateBridge __Hotfix_OnRepairButtonClick;
        private static DelegateBridge __Hotfix_OnModuleItemClick;
        private static DelegateBridge __Hotfix_OnShipItemClick;
        private static DelegateBridge __Hotfix_OnBuyPanelCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBuyPanelConfirmButtonClick;
        private static DelegateBridge __Hotfix_GetParamValueFromIntent;
        private static DelegateBridge __Hotfix_ReturnToCaptainShipManageUITask;
        private static DelegateBridge __Hotfix_SendCaptainUnlockShipReq;
        private static DelegateBridge __Hotfix_SendCaptainRepairShipReq;
        private static DelegateBridge __Hotfix_ShowSetCurrShipConfirmMsgBox;
        private static DelegateBridge __Hotfix_StartSetCurrentShipReqTask;
        private static DelegateBridge __Hotfix_add_EventOnShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_remove_EventOnShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_add_EventOnReturnToShipManangerUITask;
        private static DelegateBridge __Hotfix_remove_EventOnReturnToShipManangerUITask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<bool> EventOnReturnToShipManangerUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType, bool> EventOnShowItemSimpleInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CaptainShipUnlockAndRepairUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void GetParamValueFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadStaticRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyPanelCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyPanelConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnModuleItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRepairButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipItemClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnUnlockButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnToCaptainShipManageUITask(bool changeShipState)
        {
        }

        [MethodImpl(0x8000)]
        private void SendCaptainRepairShipReq(bool needStartPipeLineWhenFailed = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SendCaptainUnlockShipReq(bool needStartPipeLineWhenFailed = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideUI(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowSetCurrShipConfirmMsgBox()
        {
        }

        [MethodImpl(0x8000)]
        private void StartSetCurrentShipReqTask(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ReturnToCaptainShipManageUITask>c__AnonStorey0
        {
            internal bool changeShipState;
            internal CaptainShipUnlockAndRepairUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                if (this.$this.EventOnReturnToShipManangerUITask != null)
                {
                    this.$this.EventOnReturnToShipManangerUITask(this.changeShipState);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendCaptainRepairShipReq>c__AnonStorey2
        {
            internal bool needStartPipeLineWhenFailed;
            internal CaptainShipUnlockAndRepairUITask $this;

            internal void <>m__0(Task task)
            {
                CaptainRepairShipReqNetTask task2 = task as CaptainRepairShipReqNetTask;
                if (task2.RepairShipResult == 0)
                {
                    TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_CaptainShipRepairSuccess, false, new object[0]);
                    this.$this.ReturnToCaptainShipManageUITask(true);
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.RepairShipResult, true, false);
                    if (this.needStartPipeLineWhenFailed)
                    {
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendCaptainUnlockShipReq>c__AnonStorey1
        {
            internal bool needStartPipeLineWhenFailed;
            internal CaptainShipUnlockAndRepairUITask $this;

            internal void <>m__0(Task task)
            {
                CaptainUnlockShipReqNetTask task2 = (CaptainUnlockShipReqNetTask) task;
                if (!task2.IsNetworkError)
                {
                    if (task2.UnlockShipResult == 0)
                    {
                        this.$this.ShowSetCurrShipConfirmMsgBox();
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_CaptainShipUnlockSuccess, false, new object[0]);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.UnlockShipResult, true, false);
                        if (this.needStartPipeLineWhenFailed)
                        {
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartSetCurrentShipReqTask>c__AnonStorey3
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                CaptainSetCurrShipReqNetTask task2 = task as CaptainSetCurrShipReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.SetReqResult == 0)
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_CaptainShipSetCurrentShipSuccess, false, new object[0]);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.SetReqResult, true, false);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(task2.SetReqResult == 0);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsShowBuyItemPanel
        }
    }
}

