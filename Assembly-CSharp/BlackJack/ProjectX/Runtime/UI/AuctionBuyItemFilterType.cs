﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    [Flags]
    public enum AuctionBuyItemFilterType
    {
        None = 0,
        T1 = 1,
        T2 = 2,
        T3 = 4,
        R1 = 8,
        R2 = 0x10,
        R3 = 0x20,
        R4 = 0x40,
        R5 = 0x80,
        S = 0x100,
        M = 0x200,
        L = 0x400,
        N = 0x800
    }
}

