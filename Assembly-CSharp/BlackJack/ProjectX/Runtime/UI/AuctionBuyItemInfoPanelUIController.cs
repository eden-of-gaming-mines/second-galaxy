﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AuctionBuyItemInfoPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnBuyCountInputFiledEditEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnClickItemIcon;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConcelButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAddCountButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCutDownButtonClick;
        private AuctionItemMarketInfoUIController m_auctionItemMarketInfoUICtrl;
        private CommonItemIconUIController m_itemIconCtrl;
        private List<ProAuctionOrderBriefInfo> m_productInfoList;
        private int m_buyCount;
        private ulong m_estimateCost;
        private bool m_isPanelShow;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ProductInformationDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_productInfoDummy;
        [AutoBind("./BuyItemInfoPanel/ItemInfoGroup/TradeNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeNameText;
        [AutoBind("./BuyItemInfoPanel/ItemInfoGroup/Quality/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_qualityRankImage;
        [AutoBind("./BuyItemInfoPanel/ItemInfoGroup/Tech/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_techRankImage;
        [AutoBind("./BuyItemInfoPanel/ItemInfoGroup/Tech/TechTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_techTitleText;
        [AutoBind("./BuyItemInfoPanel/ItemInfoGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemDummy;
        [AutoBind("./BuyItemInfoPanel/BuyCountGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_addCountButton;
        [AutoBind("./BuyItemInfoPanel/BuyCountGroup/CutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cutCountButton;
        [AutoBind("./BuyItemInfoPanel/BuyCountGroup/BuyNumberInputField/BuyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_buyCountText;
        [AutoBind("./BuyItemInfoPanel/TotalCost/EstimateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_totalCostText;
        [AutoBind("./BuyItemInfoPanel/TotalCost/EstimateText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_totalCostStateCtrl;
        [AutoBind("./BuyItemInfoPanel/AveragePrice/AveragePriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_averagePriceText;
        [AutoBind("./BuyItemInfoPanel/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_cofirmCancelButtonGroup;
        [AutoBind("./BuyItemInfoPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./BuyItemInfoPanel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./BuyItemInfoPanel/BuyCountGroup/BuyNumberInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_buyCountInputField;
        [AutoBind("./BuyItemInfoPanel/ItemInfoGroup/Quality/SubRankBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankState;
        [AutoBind("./ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSimpleInfoDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateBuyItem;
        private static DelegateBridge __Hotfix_UpdateBuyItemCount;
        private static DelegateBridge __Hotfix_UpdateProductInformation;
        private static DelegateBridge __Hotfix_GetItemBuyCost;
        private static DelegateBridge __Hotfix_IsBuyItemInfoPanelShow;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPos;
        private static DelegateBridge __Hotfix_GetBuyItemInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_CalEstimateCost;
        private static DelegateBridge __Hotfix_OnItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnBuyCountInputFiledEditEnd;
        private static DelegateBridge __Hotfix_remove_EventOnBuyCountInputFiledEditEnd;
        private static DelegateBridge __Hotfix_add_EventOnClickItemIcon;
        private static DelegateBridge __Hotfix_remove_EventOnClickItemIcon;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnConcelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConcelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAddCountButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddCountButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCutDownButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCutDownButtonClick;

        public event Action EventOnAddCountButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnBuyCountInputFiledEditEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnClickItemIcon
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConcelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCutDownButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private ulong CalEstimateCost(int count)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetBuyItemInfoPanelUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetItemBuyCost()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPos()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBuyItemInfoPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuyItem(int itemId, int buyCount, int totalAuctionCount, ulong tradeMoneyCount, int itemIndex, List<ProAuctionOrderBriefInfo> productInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuyItemCount(int count, bool onlyItemCount, ulong currTradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProductInformation(List<ProAuctionOrderBriefInfo> productInfo, int buyCount, int sellingTotalCount, ulong tradeMoneyCount)
        {
        }
    }
}

