﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CommonItemDetailInfoLimitInfoItemUIController : UIControllerBase
    {
        [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LimitInfoText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetInfoText;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetInfoText(string info)
        {
        }
    }
}

