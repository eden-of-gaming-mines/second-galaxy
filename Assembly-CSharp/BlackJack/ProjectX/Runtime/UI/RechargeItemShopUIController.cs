﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class RechargeItemShopUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnRechargeItemBuyButtonClick;
        private readonly List<RechargeItemShopListItemUIController> m_shopItemCtrlList;
        private List<LBRechargeItem> m_rechargeItemList;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        [AutoBind("./PoolRoot", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_shopItemRoot;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RechargeItemGroupStateCtrl;
        private static DelegateBridge __Hotfix_GetMainPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_SetAllRechargeItemEffectShowOrHide;
        private static DelegateBridge __Hotfix_UpdateRechargeItemShop;
        private static DelegateBridge __Hotfix_UpdateRechargeItemById;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnRechargeItemBuyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRechargeItemBuyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRechargeItemBuyButtonClick;

        public event Action<int> EventOnRechargeItemBuyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainPanelShowOrHideProcess(bool isShow, bool immedite = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRechargeItemBuyButtonClick(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllRechargeItemEffectShowOrHide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRechargeItemById(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRechargeItemShop(List<LBRechargeItem> rechargeItemList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

