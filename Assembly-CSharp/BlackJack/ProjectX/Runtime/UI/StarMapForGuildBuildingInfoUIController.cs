﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class StarMapForGuildBuildingInfoUIController : UIControllerBase
    {
        [AutoBind("./BuildingInfoBgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuildingInfoBgButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BuildingInfoPanelStateCtrl;
        [AutoBind("./FrameImage/BGImage/DetailInfo/BuildNextLevelDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildNextLevelDescText;
        [AutoBind("./FrameImage/BGImage/DetailInfo/BuildCurrLevelDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildCurrLevelDescText;
        [AutoBind("./FrameImage/BGImage/DetailInfo/BuildFunctionDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildFunctionDescText;
        [AutoBind("./FrameImage/BGImage/DetailInfo/BuildingFunctionTitleGroup/BuildingFunctionTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingFunctionTitleText;
        [AutoBind("./FrameImage/BGImage/DetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DetailInfoStateCtrl;
        private static DelegateBridge __Hotfix_ShowBuildingInfoPanel;
        private static DelegateBridge __Hotfix_SwitchBuildingInfoPanelVisibleState;
        private static DelegateBridge __Hotfix_UpdateBuildingInfoPanel;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBuildingInfoPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchBuildingInfoPanelVisibleState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuildingInfoPanel(GuildBuildingInfo buildingInfo)
        {
        }
    }
}

