﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildWarItemUIController : UIControllerBase
    {
        public ScrollItemBaseUIController m_scrollItemCtrl;
        public Action<GuildBattleSimpleReportInfo> EventOnItemClick;
        private GuildBattleSimpleReportInfo m_curData;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_button;
        [AutoBind("./StarNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_startNameText;
        [AutoBind("./StarNameText/FacilityText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_startFacilityText;
        [AutoBind("./SelfInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_selfInfo;
        [AutoBind("./SelfInfo/SignGroupOwn", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_logoOwn;
        [AutoBind("./SelfInfo/GuildNumberTextOwn", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildNameOwn;
        [AutoBind("./GuildNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildNameText;
        [AutoBind("./BuildingImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_buildingImage;
        [AutoBind("./ResultGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_resultStateCtrl;
        [AutoBind("./AttackAndDefenseGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_attackDefenseStateCtrl;
        [AutoBind("./SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_guildLogoCtrl;
        [AutoBind("./RightGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rightPartStateCtrl;
        [AutoBind("./RightGroup/StageGroup", AutoBindAttribute.InitState.NotInit, false)]
        public WarReportBattleStatusUIController m_warReportBattleStatusUIController;
        [AutoBind("./RightGroup/RecordGroup/GraphGroup/KillBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_killBarImage;
        [AutoBind("./RightGroup/RecordGroup/GraphGroup/LosssBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_losssBarImage;
        [AutoBind("./RightGroup/RecordGroup/GraphGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_numberText;
        [AutoBind("./RightGroup/RecordGroup/KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_killNumberText;
        [AutoBind("./RightGroup/RecordGroup/LossNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_lossNumberText;
        private static DelegateBridge __Hotfix_SetItemInfo;
        private static DelegateBridge __Hotfix_SetItemNullState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetResPathByBuildingType;
        private static DelegateBridge __Hotfix_SetKillInfo;

        [MethodImpl(0x8000)]
        private Sprite GetResPathByBuildingType(GuildBuildingType buildingType, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemInfo(GuildBattleSimpleReportInfo data, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool showSelfInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemNullState(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetKillInfo()
        {
        }
    }
}

