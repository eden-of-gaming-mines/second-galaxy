﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class QuestListUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UnAcceptedQuestUIInfo> EventOnUnAcceptQuestImmediateCommunicationButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool, bool> EventOnRequestQuestUIClose;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        private bool m_isTaskInit;
        public const string SelectedQuestInstanceId_ParamKey = "SelectedQuestInstanceId";
        public const string SelectedUnAcceptQuestInfo_ParamKey = "SelectedUnAcceptQuestInfoId";
        public const string IsPlayPanelOpenAaim_ParamKey = "IsPlayPanelOpenAaim";
        public const string SelectedUnAcceptQuestConfigId_ParamKey = "SelectedUnAcceptQuestConfigId";
        public const string SelectedUnAcceptQuestLevel_ParamKey = "SelectedUnAcceptQuestLevel";
        public const string SelectedUnAcceptQuestSSId_ParamKey = "SelectedUnAcceptQuestSSId";
        public const string SelectedUnAcceptQuestFactionId_ParamKey = "SelectedUnAcceptQuestFactionId";
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private QuestListUIController m_questListCtrl;
        private AllowInShipGroupUIController m_allowInShipCtrl;
        private LBProcessingQuestBase m_currSelectedAcceptedQuest;
        private UnAcceptedQuestUIInfo m_currSelectedUnacceptedQuestInfo;
        private List<UnAcceptedQuestUIInfo> m_unacceptedQuestInfoList;
        private bool m_isUpdatingView;
        private bool m_isPlayPanelOpenAnim;
        private IUIBackgroundManager m_backgroundManager;
        public const string Mode_AcceptedQuest = "AcceptedQuest";
        public const string Mode_UnAcceptedQuest = "UnAcceptedQuest";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected static string QuestListUIPrefabAssetPath = "Assets/GameProject/RuntimeAssets/UI/Prefabs/Common_ABS/Quest/QuestListUIPrefabNew.prefab";
        public const string TaskName = "QuestListUITask";
        private bool m_isRepeatableUserGuideForExplore;
        private bool m_isRepeatableUserGuide;
        public const string ParamKeyIsRepeatableUserGuide = "IsRepeatableUserGuide";
        private GuideState m_currGuideState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_StartQuestListUITask;
        private static DelegateBridge __Hotfix_ShowQuestPanel;
        private static DelegateBridge __Hotfix_HideQuestPanel;
        private static DelegateBridge __Hotfix_SetPanelPostion;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetFirstStoryQuestItem;
        private static DelegateBridge __Hotfix_GetUnacceptQuestItemRectById;
        private static DelegateBridge __Hotfix_GetAcceptQuestItemSelectState;
        private static DelegateBridge __Hotfix_IsQuestTypeItemsExpand;
        private static DelegateBridge __Hotfix_ExpandQuestTypeItems;
        private static DelegateBridge __Hotfix_SetBackGroundManager;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_RegisterPlayerContextEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayerContextEvent;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectDynamicResWithQuest;
        private static DelegateBridge __Hotfix_CollectDynamicResWithQuestInfo;
        private static DelegateBridge __Hotfix_GetQuestAcceptNpcBodyResPath;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterUIEvents;
        private static DelegateBridge __Hotfix_UnregisterUIEvents;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnAllowInShipTypeButtonClick;
        private static DelegateBridge __Hotfix_OnCloseQuestButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnQuestDetailScrollViewClick;
        private static DelegateBridge __Hotfix_OnContactQuestNpcButtonClick;
        private static DelegateBridge __Hotfix_OnViewStarMapButtonClickForAccpetQuest;
        private static DelegateBridge __Hotfix_OnViewStarMapButtonClickForUnacceptQuest;
        private static DelegateBridge __Hotfix_OnAbortQuestButtonClick;
        private static DelegateBridge __Hotfix_OnQuestStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnAbortQuestCancelButtonClick;
        private static DelegateBridge __Hotfix_OnAbortQuestConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnQuestRecoverStarMapButtonClick_1;
        private static DelegateBridge __Hotfix_OnQuestRecoverStarMapButtonClick_0;
        private static DelegateBridge __Hotfix_OnQuestRetryButtonClick;
        private static DelegateBridge __Hotfix_OnFactionQuestTraceButtonClick;
        private static DelegateBridge __Hotfix_OnQuestCategoryToggleChanged;
        private static DelegateBridge __Hotfix_OnAcceptQuestListItemClick;
        private static DelegateBridge __Hotfix_OnUnAcceptQuestListItemClick;
        private static DelegateBridge __Hotfix_OnQuestCommitItemClick;
        private static DelegateBridge __Hotfix_OnQuestCommitItem3DTouch;
        private static DelegateBridge __Hotfix_OnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_OnQuestRandomRewardItemClick;
        private static DelegateBridge __Hotfix_OnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_OnQuestRandomRewardItem3DTouch;
        private static DelegateBridge __Hotfix_OnQuestRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnUnacceptQuestLossWarningButtonClick;
        private static DelegateBridge __Hotfix_OnAcceptQuestLossWarningButtonClick;
        private static DelegateBridge __Hotfix_OnQuestWaitForAcceptListAdd;
        private static DelegateBridge __Hotfix_OnQuestCompleteConfirm;
        private static DelegateBridge __Hotfix_GetShowSelectedAcceptedQuestInfoProcess;
        private static DelegateBridge __Hotfix_GetShowSelectedUnacceptedQuestInfoProcess;
        private static DelegateBridge __Hotfix_GetHideAllDetailUIProcess;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_GetItemInfoByQuestInfo;
        private static DelegateBridge __Hotfix_GetItemInfoByDropInfo;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseActionPlanUIPanel;
        private static DelegateBridge __Hotfix_UnAcceptedQuestUIInfoComparable;
        private static DelegateBridge __Hotfix_LeaveCurrBackground;
        private static DelegateBridge __Hotfix_add_EventOnUnAcceptQuestImmediateCommunicationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUnAcceptQuestImmediateCommunicationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRequestQuestUIClose;
        private static DelegateBridge __Hotfix_remove_EventOnRequestQuestUIClose;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_InitRepeatableDataFromIntent;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;
        private static DelegateBridge __Hotfix_StartRepeatableExploreUserGuide;
        private static DelegateBridge __Hotfix_get_IsRepeatableUserGuide;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_SelectUnaccpetQuestById;
        private static DelegateBridge __Hotfix_EnableScrollView;

        public event Action<Action, bool, bool> EventOnRequestQuestUIClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UnAcceptedQuestUIInfo> EventOnUnAcceptQuestImmediateCommunicationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public QuestListUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseActionPlanUIPanel(Action eventAfterPause = null, bool isIgnoreAnim = true)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResWithQuest(LBProcessingQuestBase quest, List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResWithQuestInfo(ConfigDataQuestInfo questInfo, List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableScrollView(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void ExpandQuestTypeItems(QuestType qstType)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetAcceptQuestItemSelectState(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstStoryQuestItem()
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetHideAllDetailUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected ItemInfo GetItemInfoByDropInfo(ItemDropInfoUIShowItemList dropInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected ItemInfo GetItemInfoByQuestInfo(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private string GetQuestAcceptNpcBodyResPath(ConfigDataQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetShowSelectedAcceptedQuestInfoProcess()
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetShowSelectedUnacceptedQuestInfoProcess()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetUnacceptQuestItemRectById(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public void HideQuestPanel(bool isImmediate, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitRepeatableDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestTypeItemsExpand(QuestType qstType)
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveCurrBackground()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAbortQuestButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAbortQuestCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAbortQuestConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAcceptQuestListItemClick(int questInsID)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAcceptQuestLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipTypeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseQuestButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnContactQuestNpcButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFactionQuestTraceButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCategoryToggleChanged(QuestListCategoryUIController.CategoryQuestType catQuestType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCommitItem3DTouch(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCommitItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCompleteConfirm(int rst, int configId, List<QuestRewardInfo> awardList, bool enableMsg)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestDetailScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRandomRewardItem3DTouch(ItemDropInfoUIShowItemList dropInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRandomRewardItemClick(ItemDropInfoUIShowItemList dropInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRecoverStarMapButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestRecoverStarMapButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRetryButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardBonusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardItem3DTouch(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardItemClick(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestStrikeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestWaitForAcceptListAdd(int questId, int questFactionId, int questLevel, int questSolarSystemId, bool fromQuestCancel, LBQuestEnv questEnv)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnAcceptQuestListItemClick(UnAcceptedQuestUIInfo unAcceptedQuest)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUnacceptQuestLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        private void OnViewStarMapButtonClickForAccpetQuest(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnViewStarMapButtonClickForUnacceptQuest(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPlayerContextEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        public void SelectUnaccpetQuestById(int questConfigId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackGroundManager(IUIBackgroundManager backgroundManager)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPostion(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ItemInfo itemInfo, int index, int ctrlIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestPanel(bool isImmediate = false, int questInsId = 0, object unaccptQuest = null, Action<bool> onEnd = null, bool isRepeatableUserGuide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        public static QuestListUITask StartQuestListUITask(string mode, bool isPlayOpenAnim = true, Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableExploreUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private int UnAcceptedQuestUIInfoComparable(UnAcceptedQuestUIInfo a, UnAcceptedQuestUIInfo b)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterPlayerContextEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsRepeatableUserGuide
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <BeforeChooseShipForStrike>c__AnonStoreyB
        {
            internal Action<UIIntent, bool> onEnd;
            internal QuestListUITask $this;

            internal void <>m__0()
            {
                this.$this.RequestSwitchTask(intent => this.onEnd(intent, true));
            }

            internal void <>m__1(UIIntent intent)
            {
                this.onEnd(intent, true);
            }
        }

        [CompilerGenerated]
        private sealed class <BeforeEnterSpaceStationUITask>c__AnonStoreyA
        {
            internal Action onEnd;
            internal QuestListUITask $this;

            internal void <>m__0()
            {
                this.$this.RequestSwitchTask(intent => this.onEnd());
            }

            internal void <>m__1(UIIntent intent)
            {
                this.onEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <HideQuestPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal QuestListUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnFactionQuestTraceButtonClick>c__AnonStorey5
        {
            internal ConfigDataQuestInfo info;
            internal QuestListUITask $this;

            internal void <>m__0()
            {
                this.$this.RequestSwitchTask(returnIntent => FactionCreditTraceDataUITask.StartFactionCreditTraceDataUITask(returnIntent, this.info.ID, this.$this.m_currSelectedAcceptedQuest.GetInstanceId(), null));
            }

            internal void <>m__1(UIIntent returnIntent)
            {
                FactionCreditTraceDataUITask.StartFactionCreditTraceDataUITask(returnIntent, this.info.ID, this.$this.m_currSelectedAcceptedQuest.GetInstanceId(), null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestRandomRewardItem3DTouch>c__AnonStorey7
        {
            internal ItemDropInfoUIShowItemList dropInfo;
            internal QuestListUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                ItemDetailInfoUITask.ShowItemDetailUIUseRetuenableIntent(new FakeLBStoreItem(this.$this.GetItemInfoByDropInfo(this.dropInfo)), returnIntent, this.$this.m_backgroundManager, true);
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestRecoverStarMapButtonClick>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal QuestListUITask $this;

            internal void <>m__0()
            {
                this.$this.RequestSwitchTask(delegate (UIIntent returnIntent) {
                    this.$this.LeaveCurrBackground();
                    PuzzleGameUITask.StartPuzzleGameUITask(returnIntent, this.$this.m_currSelectedAcceptedQuest.GetInstanceId(), this.onEnd);
                });
            }

            internal void <>m__1(UIIntent returnIntent)
            {
                this.$this.LeaveCurrBackground();
                PuzzleGameUITask.StartPuzzleGameUITask(returnIntent, this.$this.m_currSelectedAcceptedQuest.GetInstanceId(), this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestRetryButtonClick>c__AnonStorey4
        {
            internal UIControllerBase ctrl;
            internal QuestListUITask $this;

            internal void <>m__0(Task task)
            {
                QuestRetryNetWorkTask task2 = task as QuestRetryNetWorkTask;
                if (task2.Result == 0)
                {
                    this.$this.OnQuestStrikeButtonClick(this.ctrl);
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestRewardItem3DTouch>c__AnonStorey6
        {
            internal QuestRewardInfo rewardInfo;
            internal QuestListUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                ItemDetailInfoUITask.ShowItemDetailUIUseRetuenableIntent(new FakeLBStoreItem(this.$this.GetItemInfoByQuestInfo(this.rewardInfo)), returnIntent, this.$this.m_backgroundManager, true);
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestStrikeButtonClick>c__AnonStorey2
        {
            internal int questInstanceId;
            internal QuestListUITask $this;
            private static Action<UIIntent> <>f__am$cache0;

            internal void <>m__0()
            {
                if (!QuestUtilHelper.TryStartAutoCompleteQuestDialogInStation(this.questInstanceId, null))
                {
                    CommonStrikeUtil.StrikeForQuest(this.$this, this.questInstanceId, this.$this.PlayerCtx.GetLBCharacter().IsStayWithMotherShip(), () => this.$this.CloseActionPlanUIPanel(delegate {
                        if (<>f__am$cache0 == null)
                        {
                            <>f__am$cache0 = returnIntent => UIManager.Instance.ReturnUITask((returnIntent as UIIntentReturnable).PrevTaskIntent, null);
                        }
                        this.$this.RequestSwitchTask(<>f__am$cache0);
                    }, true), null, null);
                }
                else
                {
                    Debug.Log("OnQuestStrikeButtonClick start auto complete dialog");
                    SpaceStationUITask.ResetSpaceStationUIToNormalState(null, true);
                }
            }

            internal void <>m__1()
            {
                this.$this.CloseActionPlanUIPanel(delegate {
                    if (<>f__am$cache0 == null)
                    {
                        <>f__am$cache0 = returnIntent => UIManager.Instance.ReturnUITask((returnIntent as UIIntentReturnable).PrevTaskIntent, null);
                    }
                    this.$this.RequestSwitchTask(<>f__am$cache0);
                }, true);
            }

            internal void <>m__2()
            {
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = returnIntent => UIManager.Instance.ReturnUITask((returnIntent as UIIntentReturnable).PrevTaskIntent, null);
                }
                this.$this.RequestSwitchTask(<>f__am$cache0);
            }

            private static void <>m__3(UIIntent returnIntent)
            {
                UIManager.Instance.ReturnUITask((returnIntent as UIIntentReturnable).PrevTaskIntent, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnStrikeEnd>c__AnonStorey9
        {
            internal ProjectXPlayerContext ctx;
            internal QuestListUITask $this;

            internal void <>m__0()
            {
                this.$this.RequestSwitchTask(delegate (UIIntent intent) {
                    if (this.ctx.GetLBCharacter().IsInSpace())
                    {
                        CommonStrikeUtil.ReturnToSolarSystemUITask();
                    }
                });
            }

            internal void <>m__1(UIIntent intent)
            {
                if (this.ctx.GetLBCharacter().IsInSpace())
                {
                    CommonStrikeUtil.ReturnToSolarSystemUITask();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnViewStarMapButtonClickForUnacceptQuest>c__AnonStorey1
        {
            internal int solarSystemId;
            internal QuestListUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                if (this.$this.PlayerCtx.CurrSolarSystemId == this.solarSystemId)
                {
                    StarMapManagerUITask.StartStarMapForSolarSystemUITask("StarMapForSolarSystemMode_NormalMode", this.solarSystemId, returnIntent, null, null, null, null);
                }
                else
                {
                    StarMapManagerUITask.StartStarMapForStarfieldUITask(returnIntent, this.solarSystemId, this.$this.PlayerCtx.CurrSolarSystemId, this.solarSystemId, GEBrushType.GEBrushType_SecurityLevel, null, 0, false, null, false, 0, 0UL, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey8
        {
            internal FakeLBStoreItem currFakeItem;
            internal Vector3 simpleItemInfoPanelPos;
            internal string mode;
            internal QuestListUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.currFakeItem, returnIntent, true, this.simpleItemInfoPanelPos, this.mode, ItemSimpleInfoUITask.PositionType.OnLeft, true, null, backgroundManager, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        private enum GuideState
        {
            None,
            Start
        }

        protected enum PipeLineStateMaskType
        {
            IsFirstUpdatePipeLine,
            IsItemSelectionChanged
        }
    }
}

