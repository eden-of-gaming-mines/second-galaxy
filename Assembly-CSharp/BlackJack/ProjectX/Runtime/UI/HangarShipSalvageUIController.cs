﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class HangarShipSalvageUIController : UIControllerBase
    {
        private const string StatePVEShow = "PVEShow";
        private const string StatePVPShow = "PVPShow";
        private const string StateBuyShipSalvageItemShow = "BuyContractShow";
        private const string StateUseShipSalvageItemShow = "UseContractShow";
        private const string StateClose = "Close";
        private const string StateShow = "Show";
        private const string StateDetailShow = "PanelOpen";
        private const string StateDetailClose = "PanelClose";
        private const string StateNormal = "Normal";
        private const string StateGray = "Gray";
        private const string ShipItemAssetName = "ShipItemUIPrefab";
        private const string SalvageItemAssetName = "CommonItemUIPrefab";
        private int m_shipSalvageId;
        private int m_buySalvageItemPrice;
        private string m_noRecordString;
        private HangarShipListItemUIController m_destroyedShipUICtrl;
        private CommonItemIconUIController m_useSalvageItemItemUICtrl;
        private CommonItemIconUIController m_buySalvageItemItemUICtrl;
        [AutoBind("./ContractInfoPanel/ContentTextGroup/IRNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_irNumberText;
        [AutoBind("./ContractInfoPanel/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_buySalvageItemDummy;
        [AutoBind("./ContractInfoPanel/MoneyGroup/IRMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_realMoneyCount;
        [AutoBind("./ContractInfoPanel/MoneyGroup/IRMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_realMoneyAddButton;
        [AutoBind("./ContractInfoPanel/MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeMoneyCount;
        [AutoBind("./ContractInfoPanel/MoneyGroup/CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bindMoneyCount;
        [AutoBind("./ContractInfoPanel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buySalvageItemCancelButton;
        [AutoBind("./ContractInfoPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buySalvageItemConfirmButton;
        [AutoBind("./SupplementInfoPanel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_useSalvageItemCancelButton;
        [AutoBind("./SupplementInfoPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_useSalvageItemConfirmButton;
        [AutoBind("./SupplementInfoPanel/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_useSalvageItemDummy;
        [AutoBind("./ShipLossyDetailPanel/DetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipBackgroundButton;
        [AutoBind("./ShipLossyDetailPanel/DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_detailInfoPanelCommonUIStateController;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/HangarShipListItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_destoryShipDummy;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/LossButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lossButtonStateCtrl;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/KillButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_killRecordButtonStateCtrl;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/LossButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_lossButton;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_removeButton;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/KillButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_killButton;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_destoryLocationText;
        [AutoBind("./ShipLossyDetailPanel/PvPContentGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_destoryTimeText;
        [AutoBind("./ShipLossyDetailPanel/PveContentGroup/SalvageButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_salvageButtonCommonUIStateController;
        [AutoBind("./ShipLossyDetailPanel/PveContentGroup/SalvageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_salvageButton;
        [AutoBind("./ShipLossyDetailPanel/PveContentGroup/AbandonButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_abandonButton;
        [AutoBind("./ShipLossyDetailPanel/PveContentGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_addSalvageDailyCountButton;
        [AutoBind("./ShipLossyDetailPanel/PveContentGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_addSalvageDailyCountButtonStateCtrl;
        [AutoBind("./ShipLossyDetailPanel/PveContentGroup/SalvageTitleText/SalvageText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_salvageDailyCountText;
        [AutoBind("./ShipLossyDetailPanel/BGImages/TitleText/TipsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipsButton;
        [AutoBind("./ShipLossyDetailPanel/BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shipSalvageStateCtrl;
        private static DelegateBridge __Hotfix_GetMainWidowUIProcess;
        private static DelegateBridge __Hotfix_GetTipWindowUIProcess;
        private static DelegateBridge __Hotfix_UpdateShipSalavageUI;
        private static DelegateBridge __Hotfix_ShowPvpPanel;
        private static DelegateBridge __Hotfix_ShowPvePanel;
        private static DelegateBridge __Hotfix_ShowBuyPanel;
        private static DelegateBridge __Hotfix_ShowUsePanel;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_get_PlayerContext;

        [MethodImpl(0x8000)]
        public UIProcess GetMainWidowUIProcess(ILBStaticPlayerShip ship, UIMode mode, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTipWindowUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBuyPanel(Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowPvePanel(ILBStaticPlayerShip ship, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowPvpPanel(ILBStaticPlayerShip ship, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowUsePanel(Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipSalavageUI(ILBStaticPlayerShip ship, UIMode mode, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        private ProjectXPlayerContext PlayerContext
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum UIMode
        {
            ShipSalvageInfoState,
            UseShipSalvageItem,
            BuyShipSalvageItem,
            Close
        }
    }
}

