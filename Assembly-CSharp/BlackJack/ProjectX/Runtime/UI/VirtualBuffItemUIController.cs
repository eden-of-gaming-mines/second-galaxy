﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class VirtualBuffItemUIController : UIControllerBase
    {
        private List<VirtualBuffEffectPropertyUIController> m_propertyCtrlList;
        [AutoBind("./BuffInfo/BgImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        [AutoBind("./BuffInfo/BgImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./BuffInfo/BuffDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_buffDescText;
        [AutoBind("./BuffEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_buffPropertyRoot;
        [AutoBind("./BuffEffectGroup/BuffEffect", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_buffEffectItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateVirtualBuffItemUI;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateVirtualBuffItemUI(VirtualBuffDesc virtualBuffDesc, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

