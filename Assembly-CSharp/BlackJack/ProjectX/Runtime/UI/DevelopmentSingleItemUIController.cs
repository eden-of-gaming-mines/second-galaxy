﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class DevelopmentSingleItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Vector2Int <ItemIndex>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        private const string ChooseStateShow = "Show";
        private const string ChooseStateClose = "Close";
        private CommonItemIconUIController m_itemCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemClickButton;
        [AutoBind("./ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_chooseStateCtrl;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemDummyTf;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateViewOnItem;
        private static DelegateBridge __Hotfix_UpdateChooseState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;

        public event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init(GameObject templateGo, Vector2Int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChooseState(string chooseStateName)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnItem(string stateName, string chooseStateName, FakeLBStoreItem fakeItem, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public Vector2Int ItemIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

