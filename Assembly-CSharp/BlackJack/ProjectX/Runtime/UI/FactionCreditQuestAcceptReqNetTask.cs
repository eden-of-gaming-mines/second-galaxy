﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class FactionCreditQuestAcceptReqNetTask : NetWorkTransactionTask
    {
        private int m_questId;
        private int m_questFactionId;
        private int m_sceneSolarSystemId;
        private int m_level;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <QuestInstanceId>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <AcceptQuestResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAcceptQuest;
        private static DelegateBridge __Hotfix_set_QuestInstanceId;
        private static DelegateBridge __Hotfix_get_QuestInstanceId;
        private static DelegateBridge __Hotfix_set_AcceptQuestResult;
        private static DelegateBridge __Hotfix_get_AcceptQuestResult;

        [MethodImpl(0x8000)]
        public FactionCreditQuestAcceptReqNetTask(int questId, int questFactionId, int sceneSolarSystemId, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAcceptQuest(int result, int insId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int QuestInstanceId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int AcceptQuestResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

