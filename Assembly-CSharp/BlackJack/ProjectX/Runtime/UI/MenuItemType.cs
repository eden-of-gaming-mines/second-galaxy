﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum MenuItemType
    {
        CelestialPlanet,
        CelestialStar,
        DelegateSignal,
        PvpSignal,
        PvpSignalRoot,
        QuestSignal,
        Quest,
        SpaceStation,
        Stargate,
        Wormhole,
        UnknownWormhole,
        Infect,
        Rescue,
        GuildSentrySignal,
        SpaceSignal,
        None
    }
}

