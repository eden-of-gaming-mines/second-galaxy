﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class BaseRedeployAccelerateConfirmDialogUIController : BaseRedeployDialogUIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <AccelerateItemId>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <AccelerateItemCount>k__BackingField;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        private static DelegateBridge __Hotfix_SetAccelerateItemInfo;
        private static DelegateBridge __Hotfix_get_CancelButtonName;
        private static DelegateBridge __Hotfix_get_ConfirmButtonName;
        private static DelegateBridge __Hotfix_get_AccelerateItemId;
        private static DelegateBridge __Hotfix_set_AccelerateItemId;
        private static DelegateBridge __Hotfix_get_AccelerateItemCount;
        private static DelegateBridge __Hotfix_set_AccelerateItemCount;

        [MethodImpl(0x8000)]
        public void SetAccelerateItemInfo(int itemId, int itemCount)
        {
        }

        protected override string CancelButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override string ConfirmButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int AccelerateItemId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int AccelerateItemCount
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

