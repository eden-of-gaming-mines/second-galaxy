﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GameFunctionLockAndUnlockUITask : UITaskBase
    {
        public static string GameFunctionLockAndUnlockUITaskMode_Default = "GameFunctionLockAndUnlockUITaskMode_Default";
        private LinkedList<UISceneLayer> m_unusedLayerList;
        private Dictionary<Task, List<string>> m_task2LayerListDict;
        private Dictionary<string, UISceneLayer> m_layerName2LayerDict;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private const string m_layerResPath = "Assets/GameProject/RuntimeAssets/UI/Prefabs/Common_ABS/GameFunctionLockEffectUIPrefab.prefab";
        public const string TaskName = "GameFunctionLockAndUnlockUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_ClearAllContextAndRes;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_SetLockStateForGameFunction;
        private static DelegateBridge __Hotfix_SetVisible_0;
        private static DelegateBridge __Hotfix_SetVisible_1;
        private static DelegateBridge __Hotfix_BringToTop;
        private static DelegateBridge __Hotfix_GetLayerFullName;
        private static DelegateBridge __Hotfix_OnSourceTaskPause;
        private static DelegateBridge __Hotfix_OnSourceTaskStop;
        private static DelegateBridge __Hotfix_ClearLayerInfoForTask;
        private static DelegateBridge __Hotfix_ActivateLayerForSourceTask;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        public GameFunctionLockAndUnlockUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void ActivateLayerForSourceTask(string layerName, UISceneLayer layer, Task sourceTask)
        {
        }

        [MethodImpl(0x8000)]
        public void BringToTop(Task sourceTask)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearAllContextAndRes()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearLayerInfoForTask(Task sourceTask)
        {
        }

        [MethodImpl(0x8000)]
        private string GetLayerFullName(Task sourceTask, string layerName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSourceTaskPause(Task sourceTask)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSourceTaskStop(Task sourceTask)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLockStateForGameFunction(GameFunctionLockEffectType effectType, Task sourceTask, string layerName, List<string> functionNameList, List<RectTransform> showAreaRectTransformList, List<RectTransform> clickAreaRectTransformList, bool isLocked, Action onEnd = null, List<SystemFuncType> systemFuncList = null)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetVisible(Task sourceTask, bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisible(Task sourceTask, string layerName, bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static GameFunctionLockAndUnlockUITask Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

