﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipAddWeaponEquip2SlotReqNetTask : NetWorkTransactionTask
    {
        public int? m_ackResult;
        private readonly int m_hangarIndex;
        public bool m_isItemStoreContextChanged;
        private readonly int m_slotGroupIndex;
        private readonly int m_slotGroupType;
        private readonly int m_srcItemStoreIndex;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnHangarShipAddWeaponEquip2SlotAck;

        [MethodImpl(0x8000)]
        public HangarShipAddWeaponEquip2SlotReqNetTask(int hangarIndex, int slotGroupType, int slotGroupIndex, int srcItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHangarShipAddWeaponEquip2SlotAck(int result, bool isItemStoreContextChanged)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

