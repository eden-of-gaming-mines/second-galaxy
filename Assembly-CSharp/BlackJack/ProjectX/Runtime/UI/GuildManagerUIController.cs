﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildManagerUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildManagerTabType> EventOnGuideManagerTabChanged;
        protected static string m_buttonNormalState;
        protected static string m_buttonSelectState;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ButtonGroup/GuildInfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildInfoButton;
        [AutoBind("./ButtonGroup/GuildInfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildInfoButtonStateCtrl;
        [AutoBind("./ButtonGroup/GuildBenefitsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildBenefitsButton;
        [AutoBind("./ButtonGroup/GuildBenefitsButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildBenefitsButtonStateCtrl;
        [AutoBind("./ButtonGroup/GuildBenefitsButton/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildBenefitsCornerMarkStateCtrl;
        [AutoBind("./ButtonGroup/GuildActionButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_actionButton;
        [AutoBind("./ButtonGroup/GuildActionButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_actionButtonStateCtrl;
        [AutoBind("./ButtonGroup/GuildActionButton/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_actionCornerObj;
        [AutoBind("./ButtonGroup/GuildActionButton/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_actionCount;
        [AutoBind("./ButtonGroup/ManagementButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_fleetManagementButton;
        [AutoBind("./ButtonGroup/ManagementButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_fleetManagementStateCtrl;
        [AutoBind("./ButtonGroup/ManagementButton/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_fleetSubscriptStateCtrl;
        [AutoBind("./ButtonGroup/ManagementButton/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_fleetSubscriptText;
        [AutoBind("./ButtonGroup/GuildWarButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildWarButton;
        [AutoBind("./ButtonGroup/GuildWarButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildWarButtonStateCtrl;
        [AutoBind("./ButtonGroup/GuildWarButton/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildWarButtonPointGroupStateCtrl;
        [AutoBind("./ButtonGroup/GuildWarButton/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_guildWarButtonPointGroupText;
        [AutoBind("./TitleText/HelpIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public SystemDescriptionController m_systemDescriptionCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_SetToggleGroupVisible;
        private static DelegateBridge __Hotfix_UpdateGuildWarButtonPointGroup;
        private static DelegateBridge __Hotfix_UpdateGuildActionCount;
        private static DelegateBridge __Hotfix_UpdateGuildBenefitsRedPoint;
        private static DelegateBridge __Hotfix_OnGuildInfoButtonClick;
        private static DelegateBridge __Hotfix_OnGuildBenefitsButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFleetManagementButton;
        private static DelegateBridge __Hotfix_OnGuildWarButton;
        private static DelegateBridge __Hotfix_OnGuildActionButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuideManagerTabChanged;
        private static DelegateBridge __Hotfix_remove_EventOnGuideManagerTabChanged;

        public event Action<GuildManagerTabType> EventOnGuideManagerTabChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetShowOrHideUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildActionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildBenefitsButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildFleetManagementButton(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildWarButton(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleGroupVisible(GuildManagerTabType visibleType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildActionCount(uint guildId, bool showGuildInfoToOtherState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBenefitsRedPoint(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildWarButtonPointGroup(uint guildId, bool showGuildInfoToOtherState)
        {
        }
    }
}

