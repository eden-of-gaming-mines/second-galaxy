﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildMainPanelInfoReqNetTask : NetWorkTransactionTask
    {
        private readonly uint m_guildId;
        private readonly ushort m_basicInfoVersion;
        private readonly ushort m_simpleRuntimeInfoVersion;
        private readonly uint m_momentBriefInfoVersion;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildMainPanelInfoAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public GuildMainPanelInfoReqNetTask(uint guildId, ushort basicInfoVersion, ushort simpleRuntimeInfoVersion, uint momentBriefInfoVersion)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMainPanelInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

