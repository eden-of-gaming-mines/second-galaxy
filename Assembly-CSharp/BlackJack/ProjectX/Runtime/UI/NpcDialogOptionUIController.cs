﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class NpcDialogOptionUIController : UIControllerBase
    {
        private NpcDialogOptInfo m_dialogOptInfo;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image OptionIcon;
        [AutoBind("./TextGroup/OptionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OptionText;
        private static DelegateBridge __Hotfix_InitDialogOption_0;
        private static DelegateBridge __Hotfix_UpdateDialogOptContent;
        private static DelegateBridge __Hotfix_InitDialogOption_1;

        [MethodImpl(0x8000)]
        public void InitDialogOption(NpcDialogOptInfo optInfo, int idx, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void InitDialogOption(string strContent, NpcDialogOptType optType, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDialogOptContent(NpcDialogOptInfo optInfo, int idx, Dictionary<string, Object> resDict)
        {
        }
    }
}

