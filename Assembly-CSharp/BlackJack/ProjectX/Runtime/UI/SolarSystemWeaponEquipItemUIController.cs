﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemWeaponEquipItemUIController : UIControllerBase
    {
        protected int m_currAmmoCount;
        protected int m_ammoMaxCount;
        protected bool m_isFlagShipTeleport;
        private const string m_normalState = "Nomal";
        private const string m_reloadState = "Reload";
        private const string m_LaunchCDState = "LaunchCD";
        private const string m_disableForEnergyState = "DisableForEnergy";
        private const string m_targetOutOfRange = "TargetOutOfRange";
        private const string m_emptyState = "Empty";
        private const string m_lockedState = "Locked";
        private const string m_disableForAmmoState = "DisableForAmmo";
        private const string m_autoAddAmmoState = "AutoAddAmmo";
        private const string m_autoAddAmmoNotVisibleState = "NotVisible";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WeaponEquipInfoButton;
        [AutoBind("./Quality", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SubRankImageCtrl;
        [AutoBind("./CDStates", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CDStatesRoot;
        [AutoBind("./CDStates/WeaponCDProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image CDProgressBar;
        [AutoBind("./CDStates/CDText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText LaunchCDText;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./LaunchableEffect", AutoBindAttribute.InitState.NotInit, false)]
        public EffectRectUVController LaunchableEffect;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        public CommonUIStateController AutoAddAmmoCtrl;
        public FrequentChangeText AutoAddAmmoText;
        private Image m_ammoCountBg;
        private FrequentChangeText m_ammoCountText;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnFlagShipTeleportButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnWeaponButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnWeaponButtonLongPressStart;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnWeaponButtonLongPressEnd;
        private SolarSystemUITask.WeaponEquipGroupItemUIInfo m_weaponEquipInfo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnWeaponEquipButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnWeaponEquipButtonLongPressEnd;
        private static DelegateBridge __Hotfix_InitWeaponEquipUIInfo;
        private static DelegateBridge __Hotfix_InitFlagShipTeleportUIInfo;
        private static DelegateBridge __Hotfix_UpdateWeaponGroupInfoUI;
        private static DelegateBridge __Hotfix_UpdateFlagShipTeleportInfoUI;
        private static DelegateBridge __Hotfix_PlayAutoAmmoReloadEffect;
        private static DelegateBridge __Hotfix_GetWeaponEquipInfo;
        private static DelegateBridge __Hotfix_SetWeaponEuipSubRankUI;
        private static DelegateBridge __Hotfix_SetUIState;
        private static DelegateBridge __Hotfix_SetLaunchCDValue;
        private static DelegateBridge __Hotfix_SetReloadProgress;
        private static DelegateBridge __Hotfix_SetAmmoCountInfo;
        private static DelegateBridge __Hotfix_SetWeaponIcon;
        private static DelegateBridge __Hotfix_PlayLaunchableEffect;
        private static DelegateBridge __Hotfix_IsFlagShip;
        private static DelegateBridge __Hotfix_get_AmmoCountBg;
        private static DelegateBridge __Hotfix_get_AmmoCountText;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnFlagShipTeleportButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFlagShipTeleportButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWeaponButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWeaponButtonLongPressStart;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponButtonLongPressStart;
        private static DelegateBridge __Hotfix_add_EventOnWeaponButtonLongPressEnd;
        private static DelegateBridge __Hotfix_remove_EventOnWeaponButtonLongPressEnd;

        public event Action<UIControllerBase> EventOnFlagShipTeleportButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnWeaponButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnWeaponButtonLongPressEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnWeaponButtonLongPressStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SolarSystemUITask.WeaponEquipGroupItemUIInfo GetWeaponEquipInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void InitFlagShipTeleportUIInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void InitWeaponEquipUIInfo(SolarSystemUITask.WeaponEquipGroupItemUIInfo info, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsFlagShip()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipButtonLongPressEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipButtonLongPressStart()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayAutoAmmoReloadEffect(ItemInfo ammoReloadInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayLaunchableEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAmmoCountInfo(int currCount, int maxCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLaunchCDValue(float totalCDTime, float remainTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetReloadProgress(float totalReloadTime, float remainTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIState(string state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWeaponEuipSubRankUI(SubRankType subRankType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWeaponIcon(Sprite iconSprite)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFlagShipTeleportInfoUI()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeaponGroupInfoUI()
        {
        }

        private Image AmmoCountBg
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private FrequentChangeText AmmoCountText
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

