﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ChatLocationInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_JumpButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBGButtonClick;
        public StarMapSSDetailInfoWndUIController locationInfoCtrl;
        private const string prefabName = "ChatLocationPanelPrefab";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_JumpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_EnterStarMapForSSButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_JumpButtonClick(UIControllerBase ctrl)
        {
        }
    }
}

