﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CommonWeaponEquipSimpleInfoUITask : UITaskBase
    {
        public const string CommonWeaponEquipSimpleInfoUIMode_Normal = "CommonWeaponEquipSimpleInfoUIMode_Normal";
        public const string UIIntentParam_WeaponEquipGroupInfo = "UIIntentParam_WeaponEquipGroupInfo";
        public const string UIIntentParam_IsFlagShip = "UIIntentParam_IsFlagShip";
        public const string UIIntentParam_WindowWorldPos = "UIIntentParam_WindowWorldPos";
        public const string UIIntentParam_WithButtonGroup = "UIIntentParam_WithButtonGroup";
        public const string UIIntentParam_ShipConfInfo = "UIIntentParam_ShipConfInfo";
        public const string UIIntentParam_WithRecommendItem = "UIIntentParam_WithRecommendItem";
        private CommonWeaponEquipSimpleInfoUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private bool m_isFlagShip;
        private object m_weaponEquipInfo;
        private ConfigDataSpaceShipInfo m_shipConfInfo;
        private bool m_withButtonGroup;
        private Vector3 m_windowWorldPos;
        private bool m_closeWindowFlag;
        private Action m_onWindowClosed;
        private bool m_needRecommendFlag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDetailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnUnloadButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnReplaceButtonClick;
        public const string TaskName = "CommonWeaponEquipSimpleInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_NormalMode;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnUnloadButtonClick;
        private static DelegateBridge __Hotfix_OnReplaceButtonClick;
        private static DelegateBridge __Hotfix_CloseSimpleInfoWindow;
        private static DelegateBridge __Hotfix_UnregisterButtonClickEvents;
        private static DelegateBridge __Hotfix_CloseSimpleInfoWindowImpl;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnUnloadButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUnloadButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnReplaceButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnReplaceButtonClick;

        public event Action EventOnDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnReplaceButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnUnloadButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonWeaponEquipSimpleInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void CloseSimpleInfoWindow(Action onWindowClosed, bool isImmediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseSimpleInfoWindowImpl(Action onWindowClosed, bool isImmediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnReplaceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnloadButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterButtonClickEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_NormalMode()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CloseSimpleInfoWindowImpl>c__AnonStorey0
        {
            internal Action onWindowClosed;
            internal CommonWeaponEquipSimpleInfoUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess p, bool b)
            {
            }
        }
    }
}

