﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class BaseRedeployUITask : UITaskBase
    {
        private BaseRedeployUIController m_mainCtrl;
        private BaseRedeployDialogUIController m_dialogUICtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "BaseRedeployUITask";
        private const int MinSpeedUpItemIndex = 2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartBaseRedeployUITaskForBaseRedeployProcessing;
        private static DelegateBridge __Hotfix_StartBaseRedeployUITaskForRedeployNeedStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBaseRedeployMainUICloseButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployMainUISpeedUpByRealMoneyButtonClick;
        private static DelegateBridge __Hotfix_OnSpeedUpWithRealMoneyCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployMainUICompleteButtonClick;
        private static DelegateBridge __Hotfix_SetAccelerateRedeployUI;
        private static DelegateBridge __Hotfix_IsItPossibleToEnableBaseRedeployAcceleration;
        private static DelegateBridge __Hotfix_OnBaseRedeployMainUIStopRedeployButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployMainUICancelRedeployButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployMainUISpeedUpItemButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployAnyDialogCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployStartRedeployDialogCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployAccelerateDialogConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployCancelRedeployDialogConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployStopRedeployDialogConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployFleetRecallDialogConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployStartRedeployDialogConfirmButtonClick_0;
        private static DelegateBridge __Hotfix_OnBaseRedeployStartRedeployDialogConfirmButtonClick_1;
        private static DelegateBridge __Hotfix_OnBaseRedeploySpeedUpWithMoneyDialogConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeploySpeedUpWithMoneyDialogRedeployTimeOut;
        private static DelegateBridge __Hotfix_OnBaseRedeployFleetRecallFailDialogConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployFinishRedeployDialogConfirmButtonClick;
        private static DelegateBridge __Hotfix_GetSpeedUpItemIdWithIndex;
        private static DelegateBridge __Hotfix_GetSpeedUpItemIndexWithID;
        private static DelegateBridge __Hotfix_SendSpeedUpWithItemNetworkReq;
        private static DelegateBridge __Hotfix_CheckCurrStationIsDestStation;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public BaseRedeployUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckCurrStationIsDestStation()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private int GetSpeedUpItemIdWithIndex(int itemIdx)
        {
        }

        [MethodImpl(0x8000)]
        private int GetSpeedUpItemIndexWithID(int itemID)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsItPossibleToEnableBaseRedeployAcceleration(Action<bool> callbackAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployAccelerateDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployAnyDialogCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployCancelRedeployDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployFinishRedeployDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployFleetRecallDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployFleetRecallFailDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployMainUICancelRedeployButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployMainUICloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployMainUICompleteButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployMainUISpeedUpByRealMoneyButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployMainUISpeedUpItemButtonClick(int itemIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployMainUIStopRedeployButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeploySpeedUpWithMoneyDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeploySpeedUpWithMoneyDialogRedeployTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployStartRedeployDialogCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployStartRedeployDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBaseRedeployStartRedeployDialogConfirmButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployStopRedeployDialogConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpWithRealMoneyCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SendSpeedUpWithItemNetworkReq(int itemId, int itemCount, Action<bool> callbackAction = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAccelerateRedeployUI()
        {
        }

        [MethodImpl(0x8000)]
        public static BaseRedeployUITask StartBaseRedeployUITaskForBaseRedeployProcessing(UIIntent mainUIIntent)
        {
        }

        [MethodImpl(0x8000)]
        public static BaseRedeployUITask StartBaseRedeployUITaskForRedeployNeedStart(UIIntent mainUIIntent, GDBSolarSystemInfo destSolarSystem, int destSpaceStationId, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnBaseRedeployFinishRedeployDialogConfirmButtonClick>c__AnonStorey2
        {
            internal bool currStationIsDestStation;
            internal BaseRedeployUITask $this;
            private static Action<bool> <>f__am$cache0;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true, true);
                if (this.$this.State == Task.TaskState.Running)
                {
                    BaseRedeployConfirmReqNetTask task2 = task as BaseRedeployConfirmReqNetTask;
                    if (task2.RedeployConfirmResult == 0)
                    {
                        this.$this.PlayUIProcess(this.$this.m_dialogUICtrl.HideDialog(false), true, (p, b) => this.$this.PlayUIProcess(this.$this.m_mainCtrl.GetCloseWindowProcess(true), true, delegate (UIProcess _p, bool _b) {
                            this.$this.Pause();
                            if (this.currStationIsDestStation)
                            {
                                UIManager.Instance.ReturnUITask((this.$this.m_currIntent as UIIntentReturnable).PrevTaskIntent, null);
                            }
                            else
                            {
                                UIManager.Instance.StopUITask("SpaceStationUITask");
                                if (<>f__am$cache0 == null)
                                {
                                    <>f__am$cache0 = delegate (bool res) {
                                        if (res)
                                        {
                                            UIManager.Instance.PauseUITask("StarMapManagerUITask");
                                        }
                                        else
                                        {
                                            Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                                        }
                                    };
                                }
                                SpaceStationUITask.StartSpaceStationUITaskWithPrepare(<>f__am$cache0, null, EnterSpaceStationType.FromStation, null);
                            }
                        }, false), false);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.RedeployConfirmResult, true, false);
                        this.$this.PlayUIProcess(this.$this.m_dialogUICtrl.HideDialog(false), true, (p, b) => this.$this.StartUpdatePipeLine(null, false, false, true, null), false);
                    }
                }
            }

            internal void <>m__1(UIProcess p, bool b)
            {
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }

            internal void <>m__2(UIProcess p, bool b)
            {
                this.$this.PlayUIProcess(this.$this.m_mainCtrl.GetCloseWindowProcess(true), true, delegate (UIProcess _p, bool _b) {
                    this.$this.Pause();
                    if (this.currStationIsDestStation)
                    {
                        UIManager.Instance.ReturnUITask((this.$this.m_currIntent as UIIntentReturnable).PrevTaskIntent, null);
                    }
                    else
                    {
                        UIManager.Instance.StopUITask("SpaceStationUITask");
                        if (<>f__am$cache0 == null)
                        {
                            <>f__am$cache0 = delegate (bool res) {
                                if (res)
                                {
                                    UIManager.Instance.PauseUITask("StarMapManagerUITask");
                                }
                                else
                                {
                                    Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                                }
                            };
                        }
                        SpaceStationUITask.StartSpaceStationUITaskWithPrepare(<>f__am$cache0, null, EnterSpaceStationType.FromStation, null);
                    }
                }, false);
            }

            internal void <>m__3(UIProcess _p, bool _b)
            {
                this.$this.Pause();
                if (this.currStationIsDestStation)
                {
                    UIManager.Instance.ReturnUITask((this.$this.m_currIntent as UIIntentReturnable).PrevTaskIntent, null);
                }
                else
                {
                    UIManager.Instance.StopUITask("SpaceStationUITask");
                    if (<>f__am$cache0 == null)
                    {
                        <>f__am$cache0 = delegate (bool res) {
                            if (res)
                            {
                                UIManager.Instance.PauseUITask("StarMapManagerUITask");
                            }
                            else
                            {
                                Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                            }
                        };
                    }
                    SpaceStationUITask.StartSpaceStationUITaskWithPrepare(<>f__am$cache0, null, EnterSpaceStationType.FromStation, null);
                }
            }

            private static void <>m__4(bool res)
            {
                if (res)
                {
                    UIManager.Instance.PauseUITask("StarMapManagerUITask");
                }
                else
                {
                    Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnBaseRedeployFleetRecallDialogConfirmButtonClick>c__AnonStorey0
        {
            internal bool currStationIsDestStation;
            internal BaseRedeployUITask $this;
            private static Action<bool> <>f__am$cache0;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true, true);
                if (this.$this.State == Task.TaskState.Running)
                {
                    BaseRedeployConfirmReqNetTask task2 = task as BaseRedeployConfirmReqNetTask;
                    if (task2.RedeployConfirmResult == 0)
                    {
                        this.$this.PlayUIProcess(this.$this.m_dialogUICtrl.HideDialog(false), true, (p, b) => this.$this.PlayUIProcess(this.$this.m_mainCtrl.GetCloseWindowProcess(true), true, delegate (UIProcess _p, bool _b) {
                            this.$this.Pause();
                            if (this.currStationIsDestStation)
                            {
                                UIManager.Instance.ReturnUITask(((UIIntentReturnable) this.$this.m_currIntent).PrevTaskIntent, null);
                            }
                            else
                            {
                                UIManager.Instance.StopUITask("SpaceStationUITask");
                                if (<>f__am$cache0 == null)
                                {
                                    <>f__am$cache0 = delegate (bool res) {
                                        if (res)
                                        {
                                            UIManager.Instance.PauseUITask("StarMapManagerUITask");
                                        }
                                        else
                                        {
                                            Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                                        }
                                    };
                                }
                                SpaceStationUITask.StartSpaceStationUITaskWithPrepare(<>f__am$cache0, null, EnterSpaceStationType.FromStation, null);
                            }
                        }, false), false);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.RedeployConfirmResult, true, false);
                        this.$this.PlayUIProcess(this.$this.m_dialogUICtrl.HideDialog(false), true, (p, b) => this.$this.StartUpdatePipeLine(null, false, false, true, null), false);
                    }
                }
            }

            internal void <>m__1(UIProcess p, bool b)
            {
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }

            internal void <>m__2(UIProcess p, bool b)
            {
                this.$this.PlayUIProcess(this.$this.m_mainCtrl.GetCloseWindowProcess(true), true, delegate (UIProcess _p, bool _b) {
                    this.$this.Pause();
                    if (this.currStationIsDestStation)
                    {
                        UIManager.Instance.ReturnUITask(((UIIntentReturnable) this.$this.m_currIntent).PrevTaskIntent, null);
                    }
                    else
                    {
                        UIManager.Instance.StopUITask("SpaceStationUITask");
                        if (<>f__am$cache0 == null)
                        {
                            <>f__am$cache0 = delegate (bool res) {
                                if (res)
                                {
                                    UIManager.Instance.PauseUITask("StarMapManagerUITask");
                                }
                                else
                                {
                                    Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                                }
                            };
                        }
                        SpaceStationUITask.StartSpaceStationUITaskWithPrepare(<>f__am$cache0, null, EnterSpaceStationType.FromStation, null);
                    }
                }, false);
            }

            internal void <>m__3(UIProcess _p, bool _b)
            {
                this.$this.Pause();
                if (this.currStationIsDestStation)
                {
                    UIManager.Instance.ReturnUITask(((UIIntentReturnable) this.$this.m_currIntent).PrevTaskIntent, null);
                }
                else
                {
                    UIManager.Instance.StopUITask("SpaceStationUITask");
                    if (<>f__am$cache0 == null)
                    {
                        <>f__am$cache0 = delegate (bool res) {
                            if (res)
                            {
                                UIManager.Instance.PauseUITask("StarMapManagerUITask");
                            }
                            else
                            {
                                Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                            }
                        };
                    }
                    SpaceStationUITask.StartSpaceStationUITaskWithPrepare(<>f__am$cache0, null, EnterSpaceStationType.FromStation, null);
                }
            }

            private static void <>m__4(bool res)
            {
                if (res)
                {
                    UIManager.Instance.PauseUITask("StarMapManagerUITask");
                }
                else
                {
                    Debug.LogError("StartSpaceStationUITask error: start SpaceStationMainTask fail");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnBaseRedeployStartRedeployDialogConfirmButtonClick>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal BaseRedeployUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true, true);
                if (this.$this.State == Task.TaskState.Running)
                {
                    BaseRedeployStartReqNetTask task2 = task as BaseRedeployStartReqNetTask;
                    if (!task2.IsNetworkError)
                    {
                        if (task2.BaseRedeployResult == 0)
                        {
                            this.$this.PlayUIProcess(this.$this.m_dialogUICtrl.HideDialog(false), true, delegate (UIProcess p, bool b) {
                                this.$this.StartUpdatePipeLine(null, false, false, true, null);
                                this.onEnd(true);
                            }, false);
                        }
                        else
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.BaseRedeployResult, true, false);
                            this.$this.PlayUIProcess(this.$this.m_dialogUICtrl.HideDialog(false), true, delegate (UIProcess p, bool b) {
                                this.$this.Pause();
                                UIManager.Instance.ReturnUITask((this.$this.m_currIntent as UIIntentReturnable).PrevTaskIntent, null);
                                this.onEnd(false);
                            }, false);
                        }
                    }
                }
            }

            internal void <>m__1(UIProcess p, bool b)
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask((this.$this.m_currIntent as UIIntentReturnable).PrevTaskIntent, null);
                this.onEnd(false);
            }

            internal void <>m__2(UIProcess p, bool b)
            {
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
                this.onEnd(true);
            }
        }

        [CompilerGenerated]
        private sealed class <SendSpeedUpWithItemNetworkReq>c__AnonStorey3
        {
            internal int itemId;
            internal Action<bool> callbackAction;
            internal BaseRedeployUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true, true);
                if (this.$this.State == Task.TaskState.Running)
                {
                    BaseRedeploySpeedUpReqNetTask task2 = task as BaseRedeploySpeedUpReqNetTask;
                    if (task2.RedeploySpeedUpResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.RedeploySpeedUpResult, true, false);
                    }
                    else
                    {
                        int speedUpItemIndexWithID = this.$this.GetSpeedUpItemIndexWithID(this.itemId);
                        if (speedUpItemIndexWithID != -1)
                        {
                            this.$this.m_mainCtrl.UpdateItemCountText(speedUpItemIndexWithID, this.itemId);
                        }
                    }
                    if (this.callbackAction != null)
                    {
                        this.callbackAction(task2.RedeploySpeedUpResult == 0);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            SpeedUpWithItem
        }
    }
}

