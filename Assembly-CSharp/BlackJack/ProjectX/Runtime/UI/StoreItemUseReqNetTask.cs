﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class StoreItemUseReqNetTask : NetWorkTransactionTask
    {
        private int m_storeItemIndex;
        private long m_storeItemCount;
        private int m_ackResult;
        private List<StoreItemUpdateInfo> m_storeItemUpdateInfoList;
        private List<CurrencyUpdateInfo> m_currencyUpdateInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnStoreItemUseAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_set_AckResult;
        private static DelegateBridge __Hotfix_get_StoreItemUpdateInfoList;
        private static DelegateBridge __Hotfix_get_CurrencyUpdateInfoList;

        [MethodImpl(0x8000)]
        public StoreItemUseReqNetTask(int storeItemIndex, long storeItemCount)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStoreItemUseAck(StoreItemUseAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public List<StoreItemUpdateInfo> StoreItemUpdateInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<CurrencyUpdateInfo> CurrencyUpdateInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

