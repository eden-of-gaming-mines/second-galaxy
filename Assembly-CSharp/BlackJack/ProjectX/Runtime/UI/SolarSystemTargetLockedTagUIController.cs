﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class SolarSystemTargetLockedTagUIController : SolarSystemTargetTagBaseUIController
    {
        [AutoBind("./NameAndTargetNotice/TargetSpecialIconRoot/TargetNoticeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetNoticeCtrl;
        [AutoBind("./NameAndTargetNotice/TargetSpecialIconRoot/TargetPVPRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetPvpCtrl;
        [AutoBind("./ProgressBarImage/ArmorProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        private SolarSystemShipProgressUIController m_armorProgressBarCtrl;
        [AutoBind("./ProgressBarImage/ShieldProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        private SolarSystemShipProgressUIController m_shieldProgressBarCtrl;
        [AutoBind("./NameAndTargetNotice/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TargetNameText;
        [AutoBind("./ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProgressBarStateCtrl;
        [AutoBind("./DistanceText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText DistanceText;
        private static DelegateBridge __Hotfix_UpdateTagInfo;
        private static DelegateBridge __Hotfix_SetTargetName;
        private static DelegateBridge __Hotfix_SetProgressBarStateByObjType;
        private static DelegateBridge __Hotfix_GetTargetNoticeStateCtrl;
        private static DelegateBridge __Hotfix_GetTargetPvpFlagStateCtrl;

        [MethodImpl(0x8000)]
        protected override CommonUIStateController GetTargetNoticeStateCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override CommonUIStateController GetTargetPvpFlagStateCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public void SetProgressBarStateByObjType(SpaceObjectType objType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTagInfo(double dist, float shield = 1f, float armor = 1f)
        {
        }
    }
}

