﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    internal class CommonItemDetailInfoPassiveEquipmentUIController : CommonItemDetailInfoHasEquipedEffectUIBase
    {
        public string m_propertyUIPrefabAssetName;
        [AutoBind("./Viewport/Content/EquipedEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EquipedEffectRoot;
        [AutoBind("./Viewport/Content/NegativeEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NegativeEffectRoot;
        [AutoBind("./Viewport/Content/Base/TechLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechLevelValue;
        [AutoBind("./Viewport/Content/Base/QualityRank/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QualityRankValue;
        [AutoBind("./Viewport/Content/Base/PackedVolume/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PackedVolumeValue;
        [AutoBind("./Viewport/Content/Base/SizeType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SizeTypeText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdatePassiveEquipmentUI;
        private static DelegateBridge __Hotfix_IsAllPassiveEquipment;
        private static DelegateBridge __Hotfix_UpdateBaseDataUI;

        [MethodImpl(0x8000)]
        private bool IsAllPassiveEquipment(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBaseDataUI(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePassiveEquipmentUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }
    }
}

