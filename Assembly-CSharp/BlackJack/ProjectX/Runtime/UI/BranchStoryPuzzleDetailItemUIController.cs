﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BranchStoryPuzzleDetailItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector3> EventOnShowTipButtonClick;
        private bool m_isInit;
        private bool m_isExpand;
        private BranchStoryUITask.BranchStorySubQuestInfo m_currItemInfo;
        [AutoBind("./ThreadText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestDescText;
        [AutoBind("./FinishGroup/Image/SiteText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CompleteDescText;
        [AutoBind("./ThreadTitleImage/ArrowUpImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArrowUpImage;
        [AutoBind("./ThreadTitleImage/ArrowDownImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArrowDownImage;
        [AutoBind("./ThreadTitleImage/FinishImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FinishImage;
        [AutoBind("./ThreadTitleImage/FinishText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FinishText;
        [AutoBind("./ThreadTitleImage/InfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShowTipButton;
        [AutoBind("./ThreadTitleImage/BranchDetailInfoPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TipDummy;
        [AutoBind("./FinishGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FinishGroup;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemUIStateCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdatePuzzleQuestDetailInfo;
        private static DelegateBridge __Hotfix_GetItemUIProcess;
        private static DelegateBridge __Hotfix_OnShowTipButtonClick;
        private static DelegateBridge __Hotfix_ExpandDetailPanel4Item;
        private static DelegateBridge __Hotfix_add_EventOnShowTipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShowTipButtonClick;
        private static DelegateBridge __Hotfix_get_CurrItemInfo;

        public event Action<Vector3> EventOnShowTipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void ExpandDetailPanel4Item(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetItemUIProcess(bool isQuestComplete, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        private void OnShowTipButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePuzzleQuestDetailInfo(BranchStoryUITask.BranchStorySubQuestInfo puzzleQuestInfo)
        {
        }

        public BranchStoryUITask.BranchStorySubQuestInfo CurrItemInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

