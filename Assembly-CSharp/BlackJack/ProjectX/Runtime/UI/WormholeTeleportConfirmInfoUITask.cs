﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class WormholeTeleportConfirmInfoUITask : UITaskBase
    {
        private WormholeTeleportConfirmInfoUIController m_mainCtrl;
        private ConfigDataWormholeStarGroupInfo m_wormholeInfo;
        private bool m_isRareWormhole;
        private uint m_interactionNpcId;
        private int m_playerCount;
        private Action m_onWarningPanelConfirm;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKeyWormholeStarGroupInfo = "WormholeStarGroupInfo";
        public const string ParamKeyWormholeIsRare = "WormholeIsRare";
        public const string ParamKeyInteractionNpcId = "InteractionNpcId";
        public const string ParamKeyWormholePlayerCount = "PlayerCount";
        public const string ParamKeyWormholeOnConfirmAction = "onConfirmAction";
        private const string PanelState_EnterWormhole = "EnterWormhole";
        private const string PanelState_EnterDarkRoom = "EnterDarkRoom";
        private const string PanelState_LeaveWormhole = "LeaveWormhole";
        private const string PanelState_WormholeBuffWarning = "WormholeBuffWarning";
        private const string PanelState_LeaveWormholeTopLevelBuf = "LeaveWormholeTopLevelBuf";
        private const string PanelState_Close = "Close";
        public const string WormholeEnterInfoUITaskModeWormholeGate = "WormholeGate";
        public const string WormholeEnterInfoUITaskModeWormholeBackPoint = "WormholeBackPoint";
        public const string WormholeEnterInfoUITaskModeWormholeDarkRoom = "WormholeDarkRoom";
        public const string WormholeEnterInfoUITaskModeWormholeWarning = "WormholeWarning";
        public const string TaskName = "WormholeTeleportConfirmInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowWormholeTopLevelBuffWarningPanel;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_WormholeGateMode;
        private static DelegateBridge __Hotfix_UpdateDataCache_WormholeBackPointMode;
        private static DelegateBridge __Hotfix_UpdateDataCache_WormholeDarkRoom;
        private static DelegateBridge __Hotfix_UpdateDataCache_WormholeBufWarning;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_WormholeGateMode;
        private static DelegateBridge __Hotfix_UpdateView_WormholeBackPointMode;
        private static DelegateBridge __Hotfix_UpdateView_WormholeDarkRoom;
        private static DelegateBridge __Hotfix_UpdateView_WormholeTopLevelBufWarning;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_IsWormholeEnvBufFullLevel;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public WormholeTeleportConfirmInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsWormholeEnvBufFullLevel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowWormholeTopLevelBuffWarningPanel(Action onConfirm = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_WormholeBackPointMode(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_WormholeBufWarning(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_WormholeDarkRoom(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_WormholeGateMode(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_WormholeBackPointMode()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_WormholeDarkRoom()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_WormholeGateMode()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_WormholeTopLevelBufWarning()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

