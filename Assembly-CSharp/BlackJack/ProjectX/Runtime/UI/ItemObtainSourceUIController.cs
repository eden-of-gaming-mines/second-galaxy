﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ItemObtainSourceUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ItemObtainSourceType> EventOnItemObtainSourceButtonClick;
        protected GameObject m_itemObtainSourceListItemTemplate;
        protected List<ItemObtainSourceListItemUIController> m_itemList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController m_stateCtrl;
        [AutoBind("./BGImage/Panel/Content", AutoBindAttribute.InitState.NotInit, false)]
        protected Transform m_itemObtainSourceListContent;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateItemObtainSourceList;
        private static DelegateBridge __Hotfix_CreateObtainSourceWindowProcess;
        private static DelegateBridge __Hotfix_CreateItemObtainSourceListItem;
        private static DelegateBridge __Hotfix_OnItemObtainSourceListItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemObtainSourceButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemObtainSourceButtonClick;

        public event Action<ItemObtainSourceType> EventOnItemObtainSourceButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected ItemObtainSourceListItemUIController CreateItemObtainSourceListItem()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateObtainSourceWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemObtainSourceListItemClick(ItemObtainSourceType srcType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemObtainSourceList(Dictionary<string, UnityEngine.Object> resDict, List<ConfigDataItemObtainSourceTypeInfo> itemObtainSourceTypeInfoList)
        {
        }
    }
}

