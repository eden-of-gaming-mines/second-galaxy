﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipDetailInfoPanelUIController : UIControllerBase
    {
        [AutoBind("./ShipDetail/ValueTexts/AttackValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text AttackValueText;
        [AutoBind("./ShipDetail/ValueTexts/MaxRangeValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text MaxRangeValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ShieldValueText;
        [AutoBind("./ShipDetail/ValueTexts/ArmorValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ArmorValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldElectricValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ShieldElectricValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldKineticValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ShieldKineticValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldHeatValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ShieldHeatValueText;
        [AutoBind("./ShipDetail/ValueTexts/VolumnValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text VolumnValueText;
        [AutoBind("./ShipDetail/ValueTexts/JumpSteadyValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text JumpSteadyValueText;
        [AutoBind("./ShipDetail/ValueTexts/SpeedValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text SpeedValueText;
        [AutoBind("./ShipDetail/ValueTexts/DepotValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text DepotValueText;
        [AutoBind("./ShipDetail/ValueTexts/ElectricValueText", AutoBindAttribute.InitState.NotInit, false)]
        protected Text ElectricValueText;
        [AutoBind("./ShipDetail/ShieldDefense/ElectricDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ShieldElectricProgBar;
        [AutoBind("./ShipDetail/ShieldDefense/KineticDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ShieldKineticProgBar;
        [AutoBind("./ShipDetail/ShieldDefense/HeatDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ShieldHeatProgBar;
        [AutoBind("./ShipDetail/ArmorDefense/ElectricDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ArmorElectricProgBar;
        [AutoBind("./ShipDetail/ArmorDefense/KineticDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ArmorKineticProgBar;
        [AutoBind("./ShipDetail/ArmorDefense/HeatDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        protected Image ArmorHeatProgBar;
        [AutoBind("./ShipRadarChart", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject ShipRadarChart;
        [AutoBind("./ShipRadarChart", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController ShipRadarChartStateCtrl;
        [AutoBind("./ShipRadarChart/CPUandPower/CPU/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image CPUProgressBar;
        [AutoBind("./ShipRadarChart/CPUandPower/CPU/CPUCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor CPUCompareProgressBar;
        [AutoBind("./ShipRadarChart/CPUandPower/CPU/CPUFullCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor CPUFullCompareProgressBar;
        [AutoBind("./ShipRadarChart/CPUandPower/CPU/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CPUNumber;
        [AutoBind("./ShipRadarChart/CPUandPower/Power/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image PowerProgressBar;
        [AutoBind("./ShipRadarChart/CPUandPower/Power/PowCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor PowerCompareProgressBar;
        [AutoBind("./ShipRadarChart/CPUandPower/Power/PowFullCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor PowerFullCompareProgressBar;
        [AutoBind("./ShipRadarChart/CPUandPower/Power/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PowerNumber;
        [AutoBind("./ShipRadarChart/ShipUseTypeNameText/ShipUseTypeValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipUseTypeValueText;
        [AutoBind("./ShipRadarChart/CPUandPower/Power", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AssemblyOptimizationStateCtrl;
        [AutoBind("./ShipRadarChart/DisposeShipButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button DisposeShipButton;
        protected CommonUIStateController UIStateController;
        [AutoBind("./TitleIconButton", AutoBindAttribute.InitState.NotInit, false)]
        protected ButtonEx TitleIconButton;
        protected ILBStaticPlayerShip m_OwnerShip;
        protected bool IsCollectShip;
        protected string currMode;
        protected const string TextMode = "DetailPanel";
        protected const string GraphMode = "SpiderDiagramPanel";
        protected const string GraphModeNormal = "Bar";
        protected const string GraphModeDestoryed = "Button";
        protected CommonShipRadarChartUIController m_shipRadarChartCtrl;
        protected const float minNormalValue = 0.1f;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnTitleButtonClick;
        private static DelegateBridge __Hotfix_UpdateShipDetailInfo;
        private static DelegateBridge __Hotfix_UpdateGraphPanelInfoWithoutCPUAndPowerEffect;
        private static DelegateBridge __Hotfix_HideCPUAndPowerItemEffect;
        private static DelegateBridge __Hotfix_UpdateGraphPanelInfoWithCPUAndPowerEffect;
        private static DelegateBridge __Hotfix_UpdateProgressBarInfo;
        private static DelegateBridge __Hotfix_UpdateTextPanelInfo;
        private static DelegateBridge __Hotfix_ShowLowPowerUtilizationState;

        [MethodImpl(0x8000)]
        private void HideCPUAndPowerItemEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLowPowerUtilizationState(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGraphPanelInfoWithCPUAndPowerEffect(ILBStaticPlayerShip shipInfo, float itemCPUCost, float itemPowerCost)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGraphPanelInfoWithoutCPUAndPowerEffect(ILBStaticPlayerShip shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateProgressBarInfo(float srcTotalValue, float srcCurrentValue, float itemDeltaValue, Text text, GameObject progressBarGo, GameObject comparProgreesBarGo, GameObject fullComparProgressBarGo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipDetailInfo(ILBStaticPlayerShip shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTextPanelInfo(ILBStaticPlayerShip shipInfo)
        {
        }
    }
}

