﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CreateOrSearchGuildUIController : UIControllerBase
    {
        private const string PoolName = "GuildArmyGroupPool";
        private const int NameValue = 1;
        public Action<uint> m_eventOnGotoGuildButtonClick;
        public Action<uint, Action<bool>> m_eventOnItemClick;
        private List<ProGuildSimplestInfo> m_guildList;
        private List<uint> m_selfApplyList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private bool m_guildNameLegitimate;
        private bool m_guildCodeNumberLegitimate;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ToggleGroup/SearchToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_searchToggle;
        [AutoBind("./ToggleGroup/EstablishToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_establishToggle;
        [AutoBind("./ToggleGroup/CreateGuildMaskButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_createGuildMaskButton;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_searchGroup;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_establishGroup;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_onDisableStateButton;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_searchGroupStateCtrl;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_searchInputField;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/InputField/Placeholder", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchInputPlaceholder;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/InputField/Placeholder_Name", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchInputPlaceholderName;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/InputField/Placeholder_Code", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchInputPlaceholderCode;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/SearchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_searchButton;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_objectPool;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_loopVerticalScroll;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/SortGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_searchTypeDropdown;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_guildCountrySearchDropdown;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public GuildRegionController m_searchGuildLanguageTypeController;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/Dropdown/MaskImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_searchRegionMask;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_establishGroupStateCtrl;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/EstablishButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_costTextStateCtrl;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/NameGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildNameStateCtrl;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/NameGroup/NameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_guildNameInputField;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/NumberInputField", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildCodeNumberStateCtrl;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/NumberInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_guildCodeNumberInputField;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_guildCountryDropdown;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public GuildRegionController m_guildLanguageTypeController;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/EstablishButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_createGuildButton;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/ToggleGroup/FreedomToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_freedomToggle;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/ToggleGroup/AuditToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_auditToggle;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/EstablishButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_costText;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/HintInsufficientLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_hintText;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/CompileButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_compileButton;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/TempButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tempButton;
        private static DelegateBridge __Hotfix_GetMainStateEffectInfo;
        private static DelegateBridge __Hotfix_UpdateItemList;
        private static DelegateBridge __Hotfix_SetTogglePanelStateCtrl;
        private static DelegateBridge __Hotfix_CheckCreateGuildParameter;
        private static DelegateBridge __Hotfix_ShowCreateErrorNotice;
        private static DelegateBridge __Hotfix_SetCreatePanelInfo;
        private static DelegateBridge __Hotfix_ResetCreateGuildPanel;
        private static DelegateBridge __Hotfix_get_SearchGuildName;
        private static DelegateBridge __Hotfix_get_GuildName;
        private static DelegateBridge __Hotfix_get_GuildCodeNumber;
        private static DelegateBridge __Hotfix_get_LanguageType;
        private static DelegateBridge __Hotfix_get_SearchLanguageType;
        private static DelegateBridge __Hotfix_get_IsGuildNameSearchType;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SearchTextChanged;
        private static DelegateBridge __Hotfix_EventOnPoolObjectCreated;
        private static DelegateBridge __Hotfix_ItemFillEvent;
        private static DelegateBridge __Hotfix_CheckNameInput;
        private static DelegateBridge __Hotfix_CheckCodeNumberInput;
        private static DelegateBridge __Hotfix_LoadLocalizedString;
        private static DelegateBridge __Hotfix_OnDropDownChange;
        private static DelegateBridge __Hotfix_get_m_needLevel;
        private static DelegateBridge __Hotfix_get_m_guildMaxCount;
        private static DelegateBridge __Hotfix_get_m_needCost;

        [MethodImpl(0x8000)]
        private void CheckCodeNumberInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckCreateGuildParameter()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckNameInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnPoolObjectCreated(string str, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetMainStateEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void ItemFillEvent(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private static void LoadLocalizedString(Text text)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropDownChange(int value)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetCreateGuildPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void SearchTextChanged(string key)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCreatePanelInfo(int level, ulong tradeMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTogglePanelStateCtrl(bool isSearchPanel)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCreateErrorNotice(int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateItemList(List<ProGuildSimplestInfo> guildList, List<uint> selfApplyList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int startIndex = 0)
        {
        }

        public string SearchGuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string GuildName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string GuildCodeNumber
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int LanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int SearchLanguageType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsGuildNameSearchType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private int m_needLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private int m_guildMaxCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private int m_needCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckNameInput>c__AnonStorey0
        {
            internal string str;

            [MethodImpl(0x8000)]
            internal bool <>m__0(string s)
            {
            }
        }
    }
}

