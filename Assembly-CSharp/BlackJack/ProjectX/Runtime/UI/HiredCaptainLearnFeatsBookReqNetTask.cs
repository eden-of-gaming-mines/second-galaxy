﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class HiredCaptainLearnFeatsBookReqNetTask : NetWorkTransactionTask
    {
        private ulong m_captainInstanceId;
        private int m_featsBookItemIndex;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <LearnResult>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private List<LBNpcCaptainFeats> <PreFeatsList>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private List<LBNpcCaptainFeats> <NewFeatsList>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private List<LBNpcCaptainFeats> <UpgradeFeatsList>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnLearnFeatsBookAck;
        private static DelegateBridge __Hotfix_set_LearnResult;
        private static DelegateBridge __Hotfix_get_LearnResult;
        private static DelegateBridge __Hotfix_set_PreFeatsList;
        private static DelegateBridge __Hotfix_get_PreFeatsList;
        private static DelegateBridge __Hotfix_set_NewFeatsList;
        private static DelegateBridge __Hotfix_get_NewFeatsList;
        private static DelegateBridge __Hotfix_set_UpgradeFeatsList;
        private static DelegateBridge __Hotfix_get_UpgradeFeatsList;

        [MethodImpl(0x8000)]
        public HiredCaptainLearnFeatsBookReqNetTask(ulong captainInstanceId, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLearnFeatsBookAck(int result, List<LBNpcCaptainFeats> preFeatsList, List<LBNpcCaptainFeats> newFeatsList, List<LBNpcCaptainFeats> upgradeFeatsList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int LearnResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public List<LBNpcCaptainFeats> PreFeatsList
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public List<LBNpcCaptainFeats> NewFeatsList
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public List<LBNpcCaptainFeats> UpgradeFeatsList
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

