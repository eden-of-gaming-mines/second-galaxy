﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class TechUpgradeSpeedUpUITask : UITaskBase
    {
        public const string ParamKey_SelectedTechId = "SelectedTechId";
        private int m_currentTechId;
        private int m_currentTechLevel;
        private TechUpgradeSpeedUpUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "TechUpgradeSpeedUpUITask";
        private const int MinSpeedUpItemIndex = 2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTechUpgradeSpeedUpUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_CloseSpeedUpPanel;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeInfoBGButtonClick;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeImmediatelyButtonClick;
        private static DelegateBridge __Hotfix_OnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_SendSpeedUpWithItemNetworkReq;
        private static DelegateBridge __Hotfix_OnTechUpgradeNtf;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public TechUpgradeSpeedUpUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseSpeedUpPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpItemClick(int itemIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechUpgradeNtf(int techId, int techLevel, bool isByTime)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeImmediatelyButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeInfoBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void SendSpeedUpWithItemNetworkReq(int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static TechUpgradeSpeedUpUITask StartTechUpgradeSpeedUpUITask(UIIntent returnToIntent, int techId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnSpeedUpItemClick>c__AnonStorey0
        {
            internal int itemId;
            internal TechUpgradeSpeedUpUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            internal void <>m__1()
            {
                this.$this.EnableUIInput(true);
            }
        }
    }
}

