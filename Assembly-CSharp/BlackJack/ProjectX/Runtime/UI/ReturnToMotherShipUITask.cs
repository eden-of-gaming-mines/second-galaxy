﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ReturnToMotherShipUITask : UITaskBase
    {
        public const string ReturnToMotherShipUITask_NormalMode = "ReturnToMotherShipUITask_NormalMode";
        public const string ReturnMotherShipUITaskParam_TargetSolarSystem = "TargetSolarSystem";
        public const string ReturnMotherShipUITaskParam_TargetSpaceStationId = "TargetSpaceStationId";
        public const string ReturnMotherShipUITaskParam_WindowShownFinished = "WindowShownFinished";
        public const string ReturnMotherShipUITaskParam_ConfirmAction = "ConfirmAction";
        public const string ReturnMotherShipUITaskParam_CancelAction = "CancelAction";
        public const string ReturnMotherShipUITaskParam_EnterMotherShip = "EnterMotherShip";
        private ReturnToMotherShipUIController m_mainCtrl;
        private GDBSolarSystemInfo m_targetSolarSystemInfo;
        private int m_targetSpaceStationId;
        private Action m_onWindowShownFinished;
        private Action m_onConfirmButtonClick;
        private Action m_onCancelButtonClick;
        private bool m_returnToMotherShip;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ReturnToMotherShipUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowReturnToMSUIAsync;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_NormalMode;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_NormalMode;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ReturnToMotherShipUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowReturnToMSUIAsync(int solarSystemId, int spaceStationId, Action onWindowShownFinished = null, Action onConfirmButtonClick = null, Action onCancelButtonClick = null, bool returnBase = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache_NormalMode(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateView_NormalMode()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ShowReturnToMSUIAsync>c__AnonStorey0
        {
            internal int spaceStationId;
            internal Action onWindowShownFinished;
            internal Action onConfirmButtonClick;
            internal Action onCancelButtonClick;
            internal bool returnBase;

            [MethodImpl(0x8000)]
            internal void <>m__0(GDBSolarSystemInfo solarSystemInfo)
            {
            }
        }
    }
}

