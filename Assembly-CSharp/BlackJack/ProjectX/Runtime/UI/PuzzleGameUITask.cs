﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class PuzzleGameUITask : UITaskBase
    {
        private int m_solaySystemId;
        private int m_questInstanceId;
        private int m_nextConfigId;
        private int m_nextQuestLevel;
        private bool m_isPuzzleBGUITaskResourceLoadComplete;
        private bool m_isLockAnxi;
        private bool m_isComplete;
        private int m_assistButtonClickCount;
        private int m_cost;
        private PuzzleGameBGUITask m_bgTask;
        private PuzzleGameUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamsKeyQuestInstanceId = "QuestInstanceId";
        public const string TaskName = "PuzzleGameUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartPuzzleGameUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnPuzzleGameBGUITaskAllResourceLoadComplete;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_StartBGTask;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_SendQuestPuzzleGameReq;
        private static DelegateBridge __Hotfix_ShowTargetSolarSystem;
        private static DelegateBridge __Hotfix_DiableSolarSystemLight;
        private static DelegateBridge __Hotfix_ClickAssistButton;
        private static DelegateBridge __Hotfix_OnCloseButtonClick_1;
        private static DelegateBridge __Hotfix_OnCloseButtonClick_0;
        private static DelegateBridge __Hotfix_OnLockButtonClick;
        private static DelegateBridge __Hotfix_OnAssistButtonClick;
        private static DelegateBridge __Hotfix_OnAssistButtonClickImp;
        private static DelegateBridge __Hotfix_OnMatchingRateChange;
        private static DelegateBridge __Hotfix_OnMatchingComplete;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public PuzzleGameUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickAssistButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void DiableSolarSystemLight(bool disable)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAssistButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAssistButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLockButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMatchingComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMatchingRateChange(float rate)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPuzzleGameBGUITaskAllResourceLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SendQuestPuzzleGameReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowTargetSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        private void StartBGTask()
        {
        }

        [MethodImpl(0x8000)]
        public static bool StartPuzzleGameUITask(UIIntent returnToIntent, int questInstanceId, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnAssistButtonClickImp>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal PuzzleGameUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.m_bgTask.CompletePuzzleGame();
                }
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnCloseButtonClick>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal PuzzleGameUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                UIIntentCustom prevTaskIntent = (UIIntentCustom) ((UIIntentReturnable) this.$this.m_currIntent).PrevTaskIntent;
                if (this.$this.m_isComplete)
                {
                    int num = ((LogicBlockQuestClient) this.$this.PlayerCtx.GetLBQuest()).GetQuestInstanceIdWithParam(this.$this.m_nextConfigId, this.$this.m_solaySystemId, this.$this.m_nextQuestLevel);
                    if (num > 0)
                    {
                        prevTaskIntent.SetParam("QuestInstanceId", num);
                    }
                }
                UIManager.Instance.ReturnUITask(prevTaskIntent, this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <SendQuestPuzzleGameReq>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal PuzzleGameUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }
    }
}

