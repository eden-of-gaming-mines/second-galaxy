﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class DelegateMissionInvadeInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PlayerSimpleInfo> EventOnPlayerNameClick;
        private DelegateMissionInvadeEventInfo m_invadeEventInfo;
        [AutoBind("./InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public TextExpand InfoText;
        [AutoBind("./TextGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text InvadeInfoTime;
        [AutoBind("./TextGroup/DateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text InvadeInfoDate;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_SetInvadeInfo_0;
        private static DelegateBridge __Hotfix_SetInvadeInfo_1;
        private static DelegateBridge __Hotfix_GetInvadeEventLogString;
        private static DelegateBridge __Hotfix_SetLossEventLogInfo;
        private static DelegateBridge __Hotfix_GetGuildCodeDisplay;
        private static DelegateBridge __Hotfix_OnPlayerNameClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerNameClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerNameClick;

        public event Action<PlayerSimpleInfo> EventOnPlayerNameClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private string GetGuildCodeDisplay(string guildCodeName)
        {
        }

        [MethodImpl(0x8000)]
        private string GetInvadeEventLogString(DelegateMissionInvadeEventInfo invadeEventInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerNameClick(string typeStr)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterUIEvent(Action<PlayerSimpleInfo> onLinkClick)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInvadeInfo(DelegateMissionInvadeEventInfo invadeEventInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInvadeInfo(PlayerSimpleInfo invadePlayerInfo, DateTime tm)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLossEventLogInfo(List<SimpleItemInfoWithCount> lossList, DateTime endTime)
        {
        }
    }
}

