﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LossWarningWindowUIController : UIControllerBase
    {
        private Vector2 m_panelSize;
        private Rect m_viewSize;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./BgBackGroudImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backgroundButton;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_detailTrans;
        [AutoBind("./Detail/FrameImage/BGImage/BGImage2/DetailInfo/MoveInfoInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_autoMoveTitle;
        [AutoBind("./Detail/FrameImage/BGImage/BGImage2/DetailInfo/MoveInfoExtraAttributesTextTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_autoMoveDesc;
        [AutoBind("./Detail/FrameImage/BGImage/BGImage2/DetailInfo/SafetyInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_safetyTitle;
        [AutoBind("./Detail/FrameImage/BGImage/BGImage2/DetailInfo/SafetyExtraAttributesTextTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_safetyDesc;
        [AutoBind("./Detail/FrameImage/BGImage/BGImage2/DetailInfo/DangerInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_dangerTitle;
        [AutoBind("./Detail/FrameImage/BGImage/BGImage2/DetailInfo/DangerExtraAttributesTextTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_dangerDesc;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateLossWarningPanel;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_get_PanelSize;
        private static DelegateBridge __Hotfix_get_ViewSize;

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 pos, float space)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLossWarningPanel(bool isAutoMove, bool isSafety, bool isDanger, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public Vector2 PanelSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Rect ViewSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

