﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceCreateUIController : UIControllerBase
    {
        private List<GuildAllianceInviteInfo> m_allianceInfoList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnModeChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<AllianceSimpleItemController, GuildAllianceInviteInfo> EventOnFillData;
        private const string Show = "Show";
        private const string Close = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_title;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ToggleGroup/SearchToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_searchToggle;
        [AutoBind("./ToggleGroup/EstablishToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_establishToggle;
        [AutoBind("./ToggleGroup/CreateAllianceMaskButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_createAllianceMaskButton;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_searchGroup;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_establishGroup;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_guildCountryDropdown;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/Dropdown", AutoBindAttribute.InitState.NotInit, false)]
        public GuildRegionController m_guildLanguageTypeController;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/EstablishButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_createGuildButton;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/CompileButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_compileButton;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/NameGroup/NameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_allianceNameInputField;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/NameGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allianceNameStateCtrl;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/EstablishButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_costText;
        [AutoBind("./SearchAndEstablishGroup/EstablishGroup/EstablishGroup/EstablishButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_costTextStateCtrl;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_objectPool;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("./SearchAndEstablishGroup/SearchGroup/NoInvitation", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_noInvitation;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateAllianceList;
        private static DelegateBridge __Hotfix_EventOnPoolObjectCreated;
        private static DelegateBridge __Hotfix_ItemFillEvent;
        private static DelegateBridge __Hotfix_ClearEvents;
        private static DelegateBridge __Hotfix_GetMainStateEffectInfo;
        private static DelegateBridge __Hotfix_SetMode;
        private static DelegateBridge __Hotfix_SetDefaultLanguage;
        private static DelegateBridge __Hotfix_get_Language;
        private static DelegateBridge __Hotfix_SetCost;
        private static DelegateBridge __Hotfix_CheckNameInput;
        private static DelegateBridge __Hotfix_get_AllianceName;
        private static DelegateBridge __Hotfix_get_NeedCost;
        private static DelegateBridge __Hotfix_add_EventOnModeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnModeChanged;
        private static DelegateBridge __Hotfix_add_EventOnFillData;
        private static DelegateBridge __Hotfix_remove_EventOnFillData;

        public event Action<AllianceSimpleItemController, GuildAllianceInviteInfo> EventOnFillData
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnModeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CheckNameInput(string str)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnPoolObjectCreated(string str, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetMainStateEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void ItemFillEvent(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCost()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDefaultLanguage(GuildAllianceLanguageType language)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMode(Mode mode)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllianceList(List<GuildAllianceInviteInfo> allianceList, string keyWord = null)
        {
        }

        public int Language
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public string AllianceName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private static int NeedCost
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckNameInput>c__AnonStorey0
        {
            internal string str;

            internal bool <>m__0(string s) => 
                (this.str[0] != ' ');
        }

        public enum Mode
        {
            Create,
            Join
        }
    }
}

