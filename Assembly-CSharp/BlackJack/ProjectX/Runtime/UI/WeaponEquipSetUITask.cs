﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class WeaponEquipSetUITask : UITaskBase
    {
        private bool m_isRecommendChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<AuctionBuyItemFilterType, int, string> EventOnReturnToPreTask;
        private WeaponEquipSetUIController m_mainCtrl;
        private ILBStaticShip m_ILBStaticShipInterface;
        private ILBStaticShip m_ILBFakeStaticShipInterface;
        private LBStaticWeaponEquipSlotGroup m_lbStaticSlotGroup;
        private LBStaticWeaponEquipSlotGroup m_lbFakeStaticSlotGroup;
        private FakeLBStoreItem m_fakeItem;
        private int m_currSelectItemIndex;
        private DepotTpye m_currDepotType;
        private bool m_isEditAmmo;
        private bool m_isDuringUpdateView;
        private bool m_isWeaponEquipEquiped;
        private string m_returnTaskName;
        private List<ILBStoreItemClient> m_cachedItemStoreItemList;
        private DateTime m_cacheItemStoreUpdateTime;
        private List<ILBStoreItemClient> m_cachedShipItemStoreItemList;
        private List<ILBStoreItemClient> m_cachedTempShipItemStoreItemList;
        private ItemStoreDefaultUIFilter m_itemStoreFilter;
        private GuildStoreDefaultUIFilter m_GuildStoreFilter;
        private AuctionBuyItemFilterType m_currAuctionFilter;
        private IUIBackgroundManager m_backgroundManager;
        private bool m_IsGuildShipHangarStore;
        public static string ParamKey_StaticShip;
        public static string ParamKey_WeaponEquipSlotGroup;
        public static string ParamKey_IsAmmo;
        public static string ParamKey_PreTaskLastMode;
        public static string ParamKey_SelectedItemFilterType;
        public static string ParamKey_SelectDropDownValue;
        public static string ParamKey_RecommendFlagChanged;
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private ulong m_GuildShipHangarInstanceId;
        public static string ParamKey_IsGuildShipHangarStore;
        public static string ParamKey_GuildShipHangarInstanceId;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "WeaponEquipSetUITask";
        private bool m_isInRepeatUserGuide;
        public static string ParameKey_IsInRepeatUserGuide;
        [CompilerGenerated]
        private static Comparison<ILBStoreItemClient> <>f__mg$cache0;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache1;
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SetCurrShipHangarBGModel;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsAuctionFilterConditionOpen;
        private static DelegateBridge __Hotfix_GetItemStoreItemClients;
        private static DelegateBridge __Hotfix_UpdateItemStoreItemList;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClickImp;
        private static DelegateBridge __Hotfix_OnSortButtonClick;
        private static DelegateBridge __Hotfix_OnFilterChange;
        private static DelegateBridge __Hotfix_OnFilterBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnItemStoreTypeChangeButtonClick;
        private static DelegateBridge __Hotfix_OnAssemblyNetTaskReqReturnSuccessed;
        private static DelegateBridge __Hotfix_OnItemListClick_0;
        private static DelegateBridge __Hotfix_OnItemListClick_1;
        private static DelegateBridge __Hotfix_OnItemDetailInfoToggleChange;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_OnItemListUIDropdownChanged;
        private static DelegateBridge __Hotfix_OnGoNpcShopButtonClick;
        private static DelegateBridge __Hotfix_OnGoAuctionButtonClick;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetStoreFirstItemRect;
        private static DelegateBridge __Hotfix_ClickFirstItem;
        private static DelegateBridge __Hotfix_GetStoreItemRectByConfig;
        private static DelegateBridge __Hotfix_ClickItemByConfig;
        private static DelegateBridge __Hotfix_ClickConfirmButton;
        private static DelegateBridge __Hotfix_IsStayWithMotherShip;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_GetCurrSelectItem;
        private static DelegateBridge __Hotfix_InitDepotType;
        private static DelegateBridge __Hotfix_SetItemToFakeShipSlotGroup;
        private static DelegateBridge __Hotfix_GetFakeShipWeaponEquipSlotGroup;
        private static DelegateBridge __Hotfix_SendAmmoReloadReq;
        private static DelegateBridge __Hotfix_SendAddWeaponEquipReq;
        private static DelegateBridge __Hotfix_CheckIsAbleAddWeaponEquip;
        private static DelegateBridge __Hotfix_ReturnToShipHangarUITask;
        private static DelegateBridge __Hotfix_SetToRefreshShipHangarWhenReturn;
        private static DelegateBridge __Hotfix_GetTitleName;
        private static DelegateBridge __Hotfix_GetFilterTitleName;
        private static DelegateBridge __Hotfix_GetItemBuyNpcShopItemType;
        private static DelegateBridge __Hotfix_EnterOtherTask;
        private static DelegateBridge __Hotfix_GetHighSlotItemFilters;
        private static DelegateBridge __Hotfix_GetAuctionConditionFilter;
        private static DelegateBridge __Hotfix_GetWeaponAmmoItemFilters;
        private static DelegateBridge __Hotfix_GetMiddleSlotItemFilters;
        private static DelegateBridge __Hotfix_GetLowSlotItemFilters;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_StoreItemComparerByRecommend;
        private static DelegateBridge __Hotfix_GetRecommendFlagForSort;
        private static DelegateBridge __Hotfix_GetStoreItemSizeType;
        private static DelegateBridge __Hotfix_GetRecommendFlag;
        private static DelegateBridge __Hotfix_ClearAllRecomendFlag;
        private static DelegateBridge __Hotfix_OnItemFilled;
        private static DelegateBridge __Hotfix_OnAssemblyNetTaskReqReturnSuccessedGuildFlagShip;
        private static DelegateBridge __Hotfix_ReturnToShipHangarUITaskGuild;
        private static DelegateBridge __Hotfix_SendAddWeaponEquipByGuildFlagReq;
        private static DelegateBridge __Hotfix_OnGuildFlagShipAddModuleEquip2SlotError;
        private static DelegateBridge __Hotfix_add_EventOnReturnToPreTask;
        private static DelegateBridge __Hotfix_remove_EventOnReturnToPreTask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartRepeatUserGuide;

        public event Action<AuctionBuyItemFilterType, int, string> EventOnReturnToPreTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public WeaponEquipSetUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected int CheckIsAbleAddWeaponEquip(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearAllRecomendFlag()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickConfirmButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstItem(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickItemByConfig(StoreItemType itemType, int configId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void EnterOtherTask()
        {
        }

        [MethodImpl(0x8000)]
        protected bool GetAuctionConditionFilter(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        protected ILBItem GetCurrSelectItem()
        {
        }

        [MethodImpl(0x8000)]
        protected LBStaticWeaponEquipSlotGroup GetFakeShipWeaponEquipSlotGroup()
        {
        }

        [MethodImpl(0x8000)]
        protected string GetFilterTitleName()
        {
        }

        [MethodImpl(0x8000)]
        protected List<Func<ILBStoreItemClient, bool>> GetHighSlotItemFilters()
        {
        }

        [MethodImpl(0x8000)]
        private void GetItemBuyNpcShopItemType(out NpcShopItemCategory category, out NpcShopItemType type)
        {
        }

        [MethodImpl(0x8000)]
        private List<ILBStoreItemClient> GetItemStoreItemClients()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected List<Func<ILBStoreItemClient, bool>> GetLowSlotItemFilters()
        {
        }

        [MethodImpl(0x8000)]
        protected List<Func<ILBStoreItemClient, bool>> GetMiddleSlotItemFilters()
        {
        }

        [MethodImpl(0x8000)]
        private bool GetRecommendFlag(StoreItemType itemType, int configId, bool isBind)
        {
        }

        [MethodImpl(0x8000)]
        private int GetRecommendFlagForSort(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetStoreFirstItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetStoreItemRectByConfig(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        private ShipSizeType GetStoreItemSizeType(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetTitleName()
        {
        }

        [MethodImpl(0x8000)]
        protected List<Func<ILBStoreItemClient, bool>> GetWeaponAmmoItemFilters(ConfigDataWeaponInfo weaponConfInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDepotType()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAuctionFilterConditionOpen()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsStayWithMotherShip()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAssemblyNetTaskReqReturnSuccessed(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAssemblyNetTaskReqReturnSuccessedGuildFlagShip(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnConfirmButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterBackGroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterChange(AuctionBuyItemFilterType auctionBuyItemFilter)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoAuctionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoNpcShopButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipAddModuleEquip2SlotError(int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemDetailInfoToggleChange()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFilled(ILBStoreItemClient item, CommonItemIconUIController ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemListClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemListClick(int index, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUIDropdownChanged(int dropdownIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemStoreTypeChangeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void ReturnToShipHangarUITask(UIIntentReturnable uiIntent, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void ReturnToShipHangarUITaskGuild(UIIntentReturnable uiIntent, Action<bool> onEnd, bool isNeedRefreshGuildShipHangar = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendAddWeaponEquipByGuildFlagReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendAddWeaponEquipReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendAmmoReloadReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrShipHangarBGModel()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetItemToFakeShipSlotGroup()
        {
        }

        [MethodImpl(0x8000)]
        private void SetToRefreshShipHangarWhenReturn(UIIntentReturnable uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public int StoreItemComparerByRecommend(ILBStoreItemClient itemA, ILBStoreItemClient itemB)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemStoreItemList(List<ILBStoreItemClient> itemClients, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetWeaponAmmoItemFilters>c__AnonStorey4
        {
            internal ConfigDataWeaponInfo weaponConfInfo;

            internal bool <>m__0(ILBStoreItemClient storeItem)
            {
                switch (this.weaponConfInfo.Category)
                {
                    case WeaponCategory.WeaponCategory_Railgun:
                    {
                        if (storeItem.GetItemType() != StoreItemType.StoreItemType_NormalAmmo)
                        {
                            break;
                        }
                        ConfigDataAmmoInfo configInfo = storeItem.GetConfigInfo<ConfigDataAmmoInfo>();
                        return ((configInfo.Category == WeaponCategory.WeaponCategory_Railgun) && this.weaponConfInfo.AvailableAmmoType.Contains(configInfo.ID));
                    }
                    case WeaponCategory.WeaponCategory_Plasma:
                    {
                        if (storeItem.GetItemType() != StoreItemType.StoreItemType_NormalAmmo)
                        {
                            break;
                        }
                        ConfigDataAmmoInfo configInfo = storeItem.GetConfigInfo<ConfigDataAmmoInfo>();
                        return ((configInfo.Category == WeaponCategory.WeaponCategory_Plasma) && this.weaponConfInfo.AvailableAmmoType.Contains(configInfo.ID));
                    }
                    case WeaponCategory.WeaponCategory_Cannon:
                    {
                        if (storeItem.GetItemType() != StoreItemType.StoreItemType_NormalAmmo)
                        {
                            break;
                        }
                        ConfigDataAmmoInfo configInfo = storeItem.GetConfigInfo<ConfigDataAmmoInfo>();
                        return ((configInfo.Category == WeaponCategory.WeaponCategory_Cannon) && this.weaponConfInfo.AvailableAmmoType.Contains(configInfo.ID));
                    }
                    case WeaponCategory.WeaponCategory_Laser:
                    {
                        if (storeItem.GetItemType() != StoreItemType.StoreItemType_NormalAmmo)
                        {
                            break;
                        }
                        ConfigDataAmmoInfo configInfo = storeItem.GetConfigInfo<ConfigDataAmmoInfo>();
                        return ((configInfo.Category == WeaponCategory.WeaponCategory_Laser) && this.weaponConfInfo.AvailableAmmoType.Contains(configInfo.ID));
                    }
                    case WeaponCategory.WeaponCategory_Drone:
                        return ((storeItem.GetItemType() == StoreItemType.StoreItemType_DroneAmmo) && this.weaponConfInfo.AvailableAmmoType.Contains(storeItem.GetConfigInfo<ConfigDataDroneInfo>().ID));

                    case WeaponCategory.WeaponCategory_Missile:
                        return ((storeItem.GetItemType() == StoreItemType.StoreItemType_MissileAmmo) && this.weaponConfInfo.AvailableAmmoType.Contains(storeItem.GetConfigInfo<ConfigDataMissileInfo>().ID));

                    default:
                        break;
                }
                return false;
            }
        }

        [CompilerGenerated]
        private sealed class <OnAssemblyNetTaskReqReturnSuccessed>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal WeaponEquipSetUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.$this.EventOnReturnToPreTask != null)
                {
                    this.$this.EventOnReturnToPreTask(this.$this.m_currAuctionFilter, this.$this.m_itemStoreFilter.CurrDropdownValue, this.$this.m_returnTaskName);
                }
                this.$this.Pause();
                UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                if (((currIntent != null) && (currIntent.PrevTaskIntent != null)) && (currIntent.PrevTaskIntent.TargetTaskName == "ShipHangarUITask"))
                {
                    this.$this.ReturnToShipHangarUITask(currIntent, this.onEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnAssemblyNetTaskReqReturnSuccessedGuildFlagShip>c__AnonStorey5
        {
            internal Action<bool> onEnd;
            internal WeaponEquipSetUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                if (((currIntent != null) && (currIntent.PrevTaskIntent != null)) && (currIntent.PrevTaskIntent.TargetTaskName == "GuildFlagShipHangarUITask"))
                {
                    this.$this.ReturnToShipHangarUITaskGuild(currIntent, this.onEnd, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendAddWeaponEquipByGuildFlagReq>c__AnonStorey6
        {
            internal ILBStoreItemClient currItem;
            internal Action<bool> onEnd;
            internal WeaponEquipSetUITask $this;

            internal void <>m__0(bool end)
            {
                if (end)
                {
                    GuildStaticFlagShipInstanceInfo readOnlyShipInstanceInfo = ((IStaticFlagShipDataContainer) this.$this.m_ILBStaticShipInterface.GetShipDataContainer()).GetReadOnlyShipInstanceInfo();
                    GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask task = new GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask(this.$this.m_GuildShipHangarInstanceId, readOnlyShipInstanceInfo.m_shipHangarIndex, readOnlyShipInstanceInfo.m_storeItemIndex, this.$this.m_lbStaticSlotGroup.GetGroupIndex(), this.currItem.GetStoreItemIndex(), readOnlyShipInstanceInfo.m_moduleSlotList[this.$this.m_lbStaticSlotGroup.GetGroupIndex()]);
                    task.EventOnStop += delegate (Task task) {
                        GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask task2 = task as GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask;
                        if (!task2.IsNetworkError && (task2.m_ackResult != null))
                        {
                            int? ackResult = task2.m_ackResult;
                            if ((ackResult.GetValueOrDefault() == 0) && (ackResult != null))
                            {
                                this.$this.OnAssemblyNetTaskReqReturnSuccessedGuildFlagShip(this.onEnd);
                            }
                            else
                            {
                                if (this.onEnd != null)
                                {
                                    this.onEnd(false);
                                }
                                this.$this.OnGuildFlagShipAddModuleEquip2SlotError(task2.m_ackResult.Value);
                            }
                        }
                    };
                    task.Start(null, null);
                }
            }

            internal void <>m__1(Task task)
            {
                GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask task2 = task as GuildFlagShipHangarShipAddModuleEquip2SlotReqNetTask;
                if (!task2.IsNetworkError && (task2.m_ackResult != null))
                {
                    int? ackResult = task2.m_ackResult;
                    if ((ackResult.GetValueOrDefault() == 0) && (ackResult != null))
                    {
                        this.$this.OnAssemblyNetTaskReqReturnSuccessedGuildFlagShip(this.onEnd);
                    }
                    else
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                        this.$this.OnGuildFlagShipAddModuleEquip2SlotError(task2.m_ackResult.Value);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendAddWeaponEquipReq>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal WeaponEquipSetUITask $this;

            internal void <>m__0(Task task)
            {
                HangarShipAddWeaponEquip2SlotReqNetTask task2 = task as HangarShipAddWeaponEquip2SlotReqNetTask;
                if (!task2.IsNetworkError && (task2.m_ackResult != null))
                {
                    int? ackResult = task2.m_ackResult;
                    if ((ackResult.GetValueOrDefault() == 0) && (ackResult != null))
                    {
                        this.$this.OnAssemblyNetTaskReqReturnSuccessed(this.onEnd);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_ackResult.Value, true, false);
                        if (task2.m_isItemStoreContextChanged)
                        {
                            this.$this.SetToRefreshShipHangarWhenReturn(this.$this.m_currIntent as UIIntentReturnable);
                        }
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendAmmoReloadReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal WeaponEquipSetUITask $this;

            internal void <>m__0(Task task)
            {
                HangarShipReloadAmmoReqNetTask task2 = task as HangarShipReloadAmmoReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AckResult == 0)
                    {
                        this.$this.OnAssemblyNetTaskReqReturnSuccessed(this.onEnd);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                        Debug.LogError($"HangarShipReloadAmmoReqNetTaskStop error: AckResult != 0 errorCode : {task2.AckResult}");
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey0
        {
            internal bool startRepeatUserGuide;
            internal WeaponEquipSetUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.startRepeatUserGuide)
                {
                    this.$this.StartRepeatUserGuide();
                }
            }
        }

        protected enum DepotTpye
        {
            ItemStore,
            ShipItemStore,
            GuildShipHangarStore
        }

        protected enum PipeLineStateMaskType
        {
            IsRefreshItemList,
            IsUpdateSelectItemInfo,
            IsDepotTypeChange,
            IsFilterCoditionChange
        }
    }
}

