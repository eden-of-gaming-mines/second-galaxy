﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class PuzzleGameBGUITask : UITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<float> EventOnMatcingChange;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnMatcingComplete;
        public const string ParamsKeySolarsystemId = "SolarsystemId";
        private int m_solarsystemId;
        private bool m_isLockAxi;
        private bool m_isComplete;
        private PuzzleGameBGUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "PuzzleGameBGUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartPuzzleGameBGUITask;
        private static DelegateBridge __Hotfix_LockAxi;
        private static DelegateBridge __Hotfix_GetMatchingRate;
        private static DelegateBridge __Hotfix_GetTargetSolarSystemScreenPos;
        private static DelegateBridge __Hotfix_GetCenterPos;
        private static DelegateBridge __Hotfix_CompletePuzzleGame;
        private static DelegateBridge __Hotfix_AddHintSolasystemCount;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_get_IsUIInputEnable;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_OnScreenSlippingOperationHappend;
        private static DelegateBridge __Hotfix_OnMatchingEnd;
        private static DelegateBridge __Hotfix_add_EventOnMatcingChange;
        private static DelegateBridge __Hotfix_remove_EventOnMatcingChange;
        private static DelegateBridge __Hotfix_add_EventOnMatcingComplete;
        private static DelegateBridge __Hotfix_remove_EventOnMatcingComplete;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<float> EventOnMatcingChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnMatcingComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public PuzzleGameBGUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void AddHintSolasystemCount(int addCount = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void CompletePuzzleGame()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCenterPos()
        {
        }

        [MethodImpl(0x8000)]
        public float GetMatchingRate()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetTargetSolarSystemScreenPos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        public void LockAxi(bool isLock)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMatchingEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenSlippingOperationHappend(float slippingX, float slippingY)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartPuzzleGameBGUITask(int solarSyatemId, Action redirectPipLineOnLoadAllResCompleted = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected bool IsUIInputEnable
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

