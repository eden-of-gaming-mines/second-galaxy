﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterSkillCategoryToggleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CharacterSkillCategoryToggleUIController> EventOnToggleSelected;
        public List<CharacterSkillTypeToggleUIController> m_skillTypeToggleList;
        [AutoBind("./CategoryTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_toggleText;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_toggle;
        [AutoBind("./SkillTypeToggleRoot/SkillItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SkillItemPrefab;
        [AutoBind("./SkillTypeToggleRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SkillTypeItemRoot;
        [AutoBind("./CategoryTitle/EffectImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ToggleEffectCtrl;
        [AutoBind("./CategoryTitle/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RedPoint;
        private PropertyCategory m_skillCategory;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetToggleName;
        private static DelegateBridge __Hotfix_SetSkillCategory;
        private static DelegateBridge __Hotfix_SetToggleSelected;
        private static DelegateBridge __Hotfix_RegEventOnToggleSelected;
        private static DelegateBridge __Hotfix_IsCategoryRootOpen;
        private static DelegateBridge __Hotfix_SwitchToggleState;
        private static DelegateBridge __Hotfix_PlayToggleSelectEffect;
        private static DelegateBridge __Hotfix_ShowRedPoint;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnToggleClick;
        private static DelegateBridge __Hotfix_add_EventOnToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnToggleSelected;
        private static DelegateBridge __Hotfix_get_SkillCategory;

        public event Action<CharacterSkillCategoryToggleUIController> EventOnToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCategoryRootOpen()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleClick(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayToggleSelectEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnToggleSelected(Action<CharacterSkillCategoryToggleUIController> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillCategory(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleSelected(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRedPoint(bool show, List<int> canLearnSkillList)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchToggleState(bool isOpen)
        {
        }

        public PropertyCategory SkillCategory
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

