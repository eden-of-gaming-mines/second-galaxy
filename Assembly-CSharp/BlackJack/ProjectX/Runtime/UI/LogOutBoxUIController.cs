﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class LogOutBoxUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./TextGroup/Text1", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_text;
        [AutoBind("./TextGroup/Text2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_redTextTip;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./Buttons/ConfirmButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_confirmText;
        [AutoBind("./BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_title;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetBoxMessageInfo;
        private static DelegateBridge __Hotfix_CreateShowOrHideProcess;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateShowOrHideProcess(bool isShow = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBoxMessageInfo(string info, string title, string confirm)
        {
        }
    }
}

