﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapOrientedTargetSimpleInfoUIController : UIControllerBase
    {
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_uiStateController;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_targetItemText;
        [AutoBind("./NameBGImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image m_targetItemIcon;
        private static DelegateBridge __Hotfix_SetTargetItemInfo;
        private static DelegateBridge __Hotfix_ShowOrHideTargetItemInfoUI;

        [MethodImpl(0x8000)]
        public void SetTargetItemInfo(string itemName, Sprite itemIcon)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideTargetItemInfoUI(bool show)
        {
        }
    }
}

