﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class WeaponEquipSetUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnItemDetailToggleChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnGotoTechButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnTypeDropDownValueChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<AuctionBuyItemFilterType> EventOnFilterChange;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnFilterPanelBackGroundButtonClick;
        public ItemStoreListUIController m_itemStoreListUICtrl;
        public HangarShipWeaponEquipDetailInfoUIController m_weaponEquipDetailInfoUICtrl;
        public ItemFilterUIController m_itemFilterUICtrl;
        public List<CommonDependenceAndRelativeTechItemUICtrl> m_dependenceTechList;
        private GameObject m_techItem;
        [AutoBind("./CPUandPowerResultPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_ShipPropertyPanelUIState;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./FilterMask", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FilterMaskImage;
        [AutoBind("./CPUandPowerResultPanel/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./CPUandPowerResultPanel/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./ItemStoreListPanel/ItemStoreTypeChangeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemStoreTypeChangeButton;
        [AutoBind("./ItemStoreListPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemStoreTitleText;
        [AutoBind("./ItemStoreListPanel/ItemStoreListDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemStoreListDummy;
        [AutoBind("./ItemStoreListPanel/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SortButton;
        [AutoBind("./ItemStoreListPanel/SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sortButtonState;
        [AutoBind("./ItemStoreListPanel/SortButton/TextShowAll", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_SortButtonText;
        [AutoBind("./FilterPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_filterPanelDummy;
        [AutoBind("./ItemStoreListPanel/TypeFilterDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_typeFilterDropDown;
        [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buyButtonGroupStateCtrl;
        [AutoBind("./ButtonGroup/ShopButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_gotoNpcShopButton;
        [AutoBind("./ButtonGroup/AuctionButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_gotoAuctionShopButton;
        [AutoBind("./ItemDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDetailInfoPanel;
        [AutoBind("./ItemDetailInfoPanel/BG", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDetailBg;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/CPU/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CPUNumberText;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/CPU/CPUProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image CPUProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/CPU/CPUCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor CPUCompareProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/CPU/CPUFullCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor CPUFullCompareProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/POW/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text POWNumberText;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/POW/PowProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image PowProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/POW/PowCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor PowCompareProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/POW/PowFullCompareProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public TweenColor PowFullCompareProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ShieldResistanceInfo/ElectricalDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldElectricalDamageResistanceNumber;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ShieldResistanceInfo/ElectricalDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldElectricalDamageResistanceProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ShieldResistanceInfo/EnergyDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldEnergyDamageResistanceNumber;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ShieldResistanceInfo/EnergyDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldEnergyDamageResistanceProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ShieldResistanceInfo/FireDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldFireDamageResistanceNumber;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ShieldResistanceInfo/FireDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldFireDamageResistanceProgressBar;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/DPS", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DPS;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/Ability", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Ability;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/SheildRecovery", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SheildRecovery;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/Sensitivity", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Sensitivity;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/RangeMax", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RangeMax;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/Shield", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Shield;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/Armor", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Armor;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/Volum", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Volum;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/JumpSteady", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject JumpSteady;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/Speed", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Speed;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/ItemStoreSize", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemStoreSize;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/Electric", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Electric;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/ShieldElectric", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShieldElectric;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/ShieldHot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShieldHot;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/ShieldKinetic", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShieldKinetic;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/CPUInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CPUMax;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/POWInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject POWMax;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/AutomationShieldInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AutomationShieldInfo;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/AutomationEnergyInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AutomationEnergyInfo;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/DebuffTimeEffect", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DebuffTimeEffect;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup/ResultInfo/TechInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechInfo;
        [AutoBind("./CPUandPowerResultPanel/NumericalGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CpuAndPowerExceptButtonState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetDepotTypeMode;
        private static DelegateBridge __Hotfix_GetStoreFirstItemRect;
        private static DelegateBridge __Hotfix_ShowItemStoreTypeChangeButton;
        private static DelegateBridge __Hotfix_SetItemStoreTitleName;
        private static DelegateBridge __Hotfix_SetItemStoreFilterName;
        private static DelegateBridge __Hotfix_SetSortButtonState;
        private static DelegateBridge __Hotfix_SetAllFilterButtonState;
        private static DelegateBridge __Hotfix_UpdateItemList;
        private static DelegateBridge __Hotfix_ShowItemDetailInfoPanel;
        private static DelegateBridge __Hotfix_ShowBuyItemButtonGroup;
        private static DelegateBridge __Hotfix_GetFilterPanelUIProcess;
        private static DelegateBridge __Hotfix_UpdateSelectItemDetailInfo;
        private static DelegateBridge __Hotfix_IsFilterPanelShow;
        private static DelegateBridge __Hotfix_UpdateShipComparePanel;
        private static DelegateBridge __Hotfix_InitTypeFilterDropDownList;
        private static DelegateBridge __Hotfix_ShowItemTypeDropDownButton;
        private static DelegateBridge __Hotfix_UpdateProgressBarInfo;
        private static DelegateBridge __Hotfix_UpdateShieldDamageResistNumber;
        private static DelegateBridge __Hotfix_UpdatePropertyItemUI;
        private static DelegateBridge __Hotfix_UpdateHaveUnitPropertyItemUI;
        private static DelegateBridge __Hotfix_GetPropertyChangeIntStr;
        private static DelegateBridge __Hotfix_UpdateTechDepenceInfo;
        private static DelegateBridge __Hotfix_OnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_OnTypeDropDownValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnItemDetailToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnItemDetailToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTypeDropDownValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnTypeDropDownValueChanged;
        private static DelegateBridge __Hotfix_OnFilterChange;
        private static DelegateBridge __Hotfix_OnFilterPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnFilterChange;
        private static DelegateBridge __Hotfix_remove_EventOnFilterChange;
        private static DelegateBridge __Hotfix_add_EventOnFilterPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFilterPanelBackGroundButtonClick;

        public event Action<AuctionBuyItemFilterType> EventOnFilterChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFilterPanelBackGroundButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGotoTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnItemDetailToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnTypeDropDownValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetFilterPanelUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState, string panelState)
        {
        }

        [MethodImpl(0x8000)]
        public string GetPropertyChangeIntStr(float value)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetStoreFirstItemRect()
        {
        }

        [MethodImpl(0x8000)]
        public void InitTypeFilterDropDownList(List<string> menuList, int value)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFilterPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailInfoUITabChanged()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterChange(AuctionBuyItemFilterType auctionBuyItemFilter)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterPanelBackGroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTypeDropDownValueChanged(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllFilterButtonState(AuctionBuyItemFilterType filterType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDepotTypeMode(bool isGuildShipMode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemStoreFilterName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemStoreTitleName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSortButtonState(bool isOpen, bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowBuyItemButtonGroup(bool isshow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowItemDetailInfoPanel(bool isshow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowItemStoreTypeChangeButton(bool isshow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowItemTypeDropDownButton(bool show)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateHaveUnitPropertyItemUI(GameObject uiGO, string valueStr, string CompareValueStr, float changeValue)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemList(List<ILBStoreItemClient> itemList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int startIndex = 0, int selectedIndex = -1)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateProgressBarInfo(float srcTotalValue, float srcCurrentValue, float desTotalValue, float desCurrentValue, Text text, GameObject progressBarGo, GameObject comparProgreesBarGo, GameObject fullComparProgressBarGo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePropertyItemUI(GameObject uiGO, float value, float valueCompar, DigitFormatUtil.DigitFormatType formatType = 0, bool isShipVolumn = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectItemDetailInfo(ILBItem selectItem, int selectIndex, ILBItem compareItem, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShieldDamageResistNumber(float value, string valueStr, float changeValue, Text valuePercentText, Image processBarImage)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipComparePanel(ILBStaticShip srcShip, ILBStaticShip destShip)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTechDepenceInfo(List<ConfIdLevelInfo> dependenceTechList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

