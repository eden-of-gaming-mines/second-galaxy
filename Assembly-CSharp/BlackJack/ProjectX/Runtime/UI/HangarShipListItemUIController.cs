﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipListItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        public HangarShipListItemState m_currItemState;
        public ScrollItemBaseUIController m_scrollCtrl;
        private string UIState_Normal;
        private string UIState_Selected;
        private string UIState_Empty;
        private string UIState_Locked;
        private string UIState_Team;
        private string CaptainShipUIState_Normal;
        private string CaptainShipUIState_Malfunction;
        private string CaptainShipUIState_Driving;
        private string CaptainShipUIState_UnActivate;
        private string CaptainShipUIState_MalfunctionAndDriving;
        private string CaptainShipUIState_CanActivate;
        private string CaptainShipUIState_CaptainLvNotEnough;
        private string CaptainShipUIState_ShipTypeNotFix;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./UnlockCondition/UnLock/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextUnLockLevel;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipName;
        [AutoBind("./ShipIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipIcon;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipBGImage;
        [AutoBind("./SlotTypeBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SlotTypeBGImage;
        [AutoBind("./SlotTypeBGImage/SlotTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SlotTypeImage;
        [AutoBind("./RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipRankImage;
        [AutoBind("./SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipSubRankStateCtrl;
        [AutoBind("./ShipManageGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainShipItemStateCtrl;
        [AutoBind("./ShipManageGroup/Condition/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnlockNeededLvText;
        [AutoBind("./CantStrikeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CantStrikeInfoRoot;
        [AutoBind("./DamageShipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipDestoryedGroup;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ButtonItem;
        [AutoBind("./BetterGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RecommendUIStateCtr;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_SetModeByShipHangarInfo;
        private static DelegateBridge __Hotfix_SetShipToNormalMode;
        private static DelegateBridge __Hotfix_SetShipToSelectMode_0;
        private static DelegateBridge __Hotfix_IsShipHangarSpaceEmpty;
        private static DelegateBridge __Hotfix_EnableCantStrikeInfo;
        private static DelegateBridge __Hotfix_EnableShipDestoryedGroup;
        private static DelegateBridge __Hotfix_GetShipHangarState;
        private static DelegateBridge __Hotfix_ShowShipLocedMode;
        private static DelegateBridge __Hotfix_SetShipListItemInfo;
        private static DelegateBridge __Hotfix_SetShipName_0;
        private static DelegateBridge __Hotfix_SetShipIcon_1;
        private static DelegateBridge __Hotfix_SetShipBGImage_0;
        private static DelegateBridge __Hotfix_SetCaptainShipListItemInfo;
        private static DelegateBridge __Hotfix_SetShipName_1;
        private static DelegateBridge __Hotfix_SetShipIcon_0;
        private static DelegateBridge __Hotfix_SetShipBGImage_1;
        private static DelegateBridge __Hotfix_SetCaptainShipState;
        private static DelegateBridge __Hotfix_SetCaptainShipTypeNotFix;
        private static DelegateBridge __Hotfix_SetShipToSelectMode_1;
        private static DelegateBridge __Hotfix_SetTeamMemberShipInfo;
        private static DelegateBridge __Hotfix_SetRecommendFlag;
        private static DelegateBridge __Hotfix_ShowShipEmptyMode;
        private static DelegateBridge __Hotfix_ShowShipInfoMode;

        [MethodImpl(0x8000)]
        public void EnableCantStrikeInfo(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableShipDestoryedGroup(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private HangarShipListItemState GetShipHangarState(ILBStaticPlayerShip shipHangarInfo, bool isLoced)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool careButtonClick = true)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShipHangarSpaceEmpty()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainShipListItemInfo(LBStaticHiredCaptain.ShipInfo captainShipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int shipState, bool isSelected = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCaptainShipState(LBStaticHiredCaptain.ShipInfo captainShipInfo, int shipState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainShipTypeNotFix()
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetModeByShipHangarInfo(ILBStaticPlayerShip shipHangarInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int nextUnLockLevel, bool isLoced, bool showDestoryState = true)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRecommendFlag(bool val)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipBGImage(ILBStaticPlayerShip shipHangarInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipBGImage(LBStaticHiredCaptain.ShipInfo captainShipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipIcon(LBStaticHiredCaptain.ShipInfo captainShipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipIcon(Dictionary<string, UnityEngine.Object> dynamicResCacheDict, ILBStaticPlayerShip shipHangarInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipListItemInfo(ILBStaticPlayerShip shipHangarInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipName(ILBStaticPlayerShip shipHangarInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipName(LBStaticHiredCaptain.ShipInfo captainShipInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipToNormalMode(ILBStaticPlayerShip shipHangarInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipToSelectMode(ILBStaticPlayerShip shipHangarInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipToSelectMode(LBStaticHiredCaptain.ShipInfo shipHangarInfo, bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTeamMemberShipInfo(Sprite shipIcon, Sprite shipTypeIcon, Sprite shipBGImage, RankType rank, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowShipEmptyMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowShipInfoMode()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowShipLocedMode(int nextUnLockLevel)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        public enum HangarShipListItemState
        {
            ShipInfo,
            Empty,
            Locked
        }
    }
}

