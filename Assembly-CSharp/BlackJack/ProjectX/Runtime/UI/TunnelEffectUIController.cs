﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TunnelEffectUIController : UIControllerBase
    {
        public int m_displayType;
        [AutoBind("./TunnelEffect", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tunnelEffectTrans;
        [AutoBind("./TunnelEffect", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_tunnelEffectImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CanvasGroup m_canvasGroup;
        public GDBSolarSystemSimpleInfo m_tunnelAtStar;
        public float m_tunnelEffectRange;
        private float m_tunnelRotation;
        private float m_tunnelExtendRange;
        public List<StarMapForStarfieldSSPointUIController> m_tunnelEffectStars;
        private GameObject m_tunnelEffectFlollowed1;
        private GameObject m_tunnelEffectFlollowed2;
        private Image m_tunnelEffectFlollowed1Image;
        private Image m_tunnelEffectFlollowed2Image;
        private static DelegateBridge __Hotfix_SetEffect;
        private static DelegateBridge __Hotfix_SetAlpha;
        private static DelegateBridge __Hotfix_Update;

        [MethodImpl(0x8000)]
        public void SetAlpha(float alpha)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetEffect(int centerStar, float effectRange)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }
    }
}

