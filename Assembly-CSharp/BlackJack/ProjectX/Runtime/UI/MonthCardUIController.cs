﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class MonthCardUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnMonthCardBuyButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnMonthCardDailyRewardButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, ILBStoreItemClient> EventOnMonthCardDailyRewardItemClick;
        private List<LBRechargeMonthlyCard> m_monthCardItemList;
        private List<MonthCardItemUIController> m_monthCardItemCtrlList;
        private const int MonthCardItemMaxCount = 2;
        private const string MonthCardItemAssetName = "MonthCardItemUIPrefab";
        [AutoBind("./RightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform RightDummyRoot;
        [AutoBind("./LeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LeftDummyRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MonthCardStateCtrl;
        private static DelegateBridge __Hotfix_GetMainPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_UpdateMonthCardItemList;
        private static DelegateBridge __Hotfix_UpdateMonthCardDailyRewardButtonState;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelLocation;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateMonthCardItems;
        private static DelegateBridge __Hotfix_GetCardItemRootByIndex;
        private static DelegateBridge __Hotfix_OnMonthCardBuyButtonClick;
        private static DelegateBridge __Hotfix_OnMonthCardDailyRewardButtonClick;
        private static DelegateBridge __Hotfix_OnMonthCardDailyRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnMonthCardBuyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMonthCardBuyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMonthCardDailyRewardButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMonthCardDailyRewardButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMonthCardDailyRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMonthCardDailyRewardItemClick;

        public event Action<int> EventOnMonthCardBuyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnMonthCardDailyRewardButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, ILBStoreItemClient> EventOnMonthCardDailyRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CreateMonthCardItems()
        {
        }

        [MethodImpl(0x8000)]
        private RectTransform GetCardItemRootByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelLocation(int monthCardIndex)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainPanelShowOrHideProcess(bool isShow, bool immedite = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardBuyButtonClick(int cardIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardDailyRewardButtonClick(int cardIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardDailyRewardItemClick(int cardIndex, ILBStoreItemClient rewardItem)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMonthCardDailyRewardButtonState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMonthCardItemList(List<LBRechargeMonthlyCard> monthCardList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

