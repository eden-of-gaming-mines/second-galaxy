﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonWeaponEquipSimpleInfoItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnDetailButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnUnloadButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnReplaceButtonClick;
        protected List<string> m_equipDescBasePropertyValueList;
        protected CommonItemIconUIController m_itemCtrl;
        protected static string CommonItemAssetName = "CommonItemUIPrefab";
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_weaponEquipTypeCtrl;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_weaponEquipItemIcon;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect m_scrollViewRect;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemDummy;
        [AutoBind("./Tech/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_rankImage;
        [AutoBind("./Quality/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_subRankImage;
        [AutoBind("./Quality/SubRankBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankBGImageCtrl;
        [AutoBind("./TitleGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_weaponEquipNameText;
        [AutoBind("./Scroll View/Viewport/Detail/TotalDamageInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_totalDamageValueText;
        [AutoBind("./Scroll View/Viewport/Detail/DamageCompose/ElectronicDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_electronicDamageText;
        [AutoBind("./Scroll View/Viewport/Detail/DamageCompose/ElectronicDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_electronicDamageProgressBar;
        [AutoBind("./Scroll View/Viewport/Detail/DamageCompose/KineticDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_kineticDamageText;
        [AutoBind("./Scroll View/Viewport/Detail/DamageCompose/KineticDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_kineticDamageProgressBar;
        [AutoBind("./Scroll View/Viewport/Detail/DamageCompose/HeatDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_heatDamageText;
        [AutoBind("./Scroll View/Viewport/Detail/DamageCompose/HeatDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_heatDamageProgressBar;
        [AutoBind("./Scroll View/Viewport/Detail/DamageCompose", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_damageGroup;
        [AutoBind("./Scroll View/Viewport/Detail/Desc/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_descText;
        [AutoBind("./Property", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_extraProperty;
        [AutoBind("./Scroll View/Viewport/Detail/ExtraPropertiesTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_extraPropertiesTitle;
        [AutoBind("./Scroll View/Viewport/Detail/NegativeEffectTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_negativePropertiesTitle;
        [AutoBind("./Scroll View/Viewport/Detail/ExtraProperties", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_extraPropertiesRoot;
        [AutoBind("./Scroll View/Viewport/Detail/NegativeEffectts", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_negativePropertiesRoot;
        [AutoBind("./Scroll View/Viewport/Detail/RangeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_maxFireRangeRoot;
        [AutoBind("./Scroll View/Viewport/Detail/RangeInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_maxFireRangeNameText;
        [AutoBind("./Scroll View/Viewport/Detail/RangeInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_maxFireRangeValueText;
        [AutoBind("./Scroll View/Viewport/Detail/EnergyCost/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_energyCostValueText;
        [AutoBind("./Scroll View/Viewport/Detail/CD/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_fireCDValueText;
        [AutoBind("./Buttons/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailButton;
        [AutoBind("./Buttons/UnloadButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_unloadButton;
        [AutoBind("./Buttons/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_changeButton;
        [AutoBind("./Buttons/ChangeButton/BetterGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonRecommendUIState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateShipStaticSlotGroupSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipInSpaceEquipSlotSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipInSpaceWeaponSlotSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipSuperWeaponSlotSimpleInfo;
        private static DelegateBridge __Hotfix_UpdateShipSuperEquipSlotSimpleInfo;
        private static DelegateBridge __Hotfix_SetDamageComposeInSpace;
        private static DelegateBridge __Hotfix_SetDamageComposeInStation;
        private static DelegateBridge __Hotfix_SetDamageCompose;
        private static DelegateBridge __Hotfix_SetGroupBasicInfo;
        private static DelegateBridge __Hotfix_SetExtraProperties;
        private static DelegateBridge __Hotfix_CreateNormalPropertiesItemListWithInfo;
        private static DelegateBridge __Hotfix_CreateNegativePropertiesItemListWithInfo;
        private static DelegateBridge __Hotfix_SetRankAndSubRankInfo;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnUnloadButtonClick;
        private static DelegateBridge __Hotfix_OnReplaceButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnUnloadButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUnloadButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnReplaceButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnReplaceButtonClick;

        public event Action EventOnDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnReplaceButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnUnloadButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateNegativePropertiesItemListWithInfo(List<PropertyValueStringPair> infoList)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateNormalPropertiesItemListWithInfo(List<PropertyValueStringPair> infoList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReplaceButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnloadButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDamageCompose(object configData, WeaponCategory weaponCategory)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDamageComposeInSpace(LBWeaponGroupBase group)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDamageComposeInStation(LBStaticWeaponEquipSlotGroup group)
        {
        }

        [MethodImpl(0x8000)]
        private void SetExtraProperties(ConfigDataShipEquipInfo configInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGroupBasicInfo(LBWeaponEquipGroupBase group, bool isEquip, object configInfo, bool isFlagShip)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRankAndSubRankInfo(object itemConfig, Dictionary<string, UnityEngine.Object> resDict, bool hideRankAndSubRank = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipInSpaceEquipSlotSimpleInfo(LBEquipGroupBase group, bool isFlagShip, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipInSpaceWeaponSlotSimpleInfo(LBWeaponGroupBase group, bool isFlagShip, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipStaticSlotGroupSimpleInfo(LBStaticWeaponEquipSlotGroup group, bool isFlagShip, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipSuperEquipSlotSimpleInfo(ConfigDataShipSuperEquipInfo superEquipInfo, ConfigDataSpaceShipInfo shipConfInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipSuperWeaponSlotSimpleInfo(ConfigDataSuperWeaponInfo superWeaponInfo, ConfigDataSpaceShipInfo shipConfInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

