﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class FactionCreditDetailUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private int m_WaitedCreditInfoId;
        private int m_PreWaitedInfoId;
        private int m_NextWaitedCreditInfoId;
        private bool m_IsCreditedAll;
        private bool m_CurrentCreditCanSign;
        private List<ConfigDataFactionCreditLevelRewardInfo> m_EnoughLevelRewardInfoList;
        private List<ConfigDataFactionCreditLevelRewardInfo> m_CurLevelRewardInfoList;
        private List<int> m_CreditedLevelRewardInfoList;
        private List<ConfigDataNpcShopItemInfo> m_NpcWaitedItemInfoList;
        private Dictionary<int, Dictionary<FactionCreditLevel, List<ConfigDataFactionCreditNpcInfo>>> m_NpcInfoByFactionIDAndCreditLevel;
        private IUIBackgroundManager m_backgroundManager;
        private int m_FactionId;
        private FactionCreditLevel m_FactionLevel;
        private static string m_tempGameUserIdForFaction;
        private static Dictionary<int, SelectFactionCreditUIData> m_TempSelectFactionCreditUIData;
        public static string InitAllOverviewMode;
        public static string ParamKey_FactionID;
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private FactionCreditDetailUIController m_mainCtrl;
        private FactionCreditFactionDetailInfoUIController m_FactionDetailUICtrl;
        public const string TaskName = "FactionCreditDetailUITask";
        private bool m_isNeedResumeOnStrikeEnd;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_StartFactionCreditItemUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitFromStartOrResum;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_OnTablRewardItemFill;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_OnDropItemClick;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnAgreementButtonClick;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_OnAgreementButtonBackClick;
        private static DelegateBridge __Hotfix_OnAgreementSignButtonClick;
        private static DelegateBridge __Hotfix_GetFactionLevelByFactionIDProxy;
        private static DelegateBridge __Hotfix_OnLittleFactionButtonClick;
        private static DelegateBridge __Hotfix_OnAgreementNpcClick;
        private static DelegateBridge __Hotfix_OnMainBackButtonClick;
        private static DelegateBridge __Hotfix_OnMainCloseButtonClick;
        private static DelegateBridge __Hotfix_OnMainShopButtonClick;
        private static DelegateBridge __Hotfix_OnMainLeaveButtonClick;
        private static DelegateBridge __Hotfix_EnterOutSideByState;
        private static DelegateBridge __Hotfix_OnGoToStarMapButtonClick;
        private static DelegateBridge __Hotfix_GetHideOrShowActionMainUIProcess;
        private static DelegateBridge __Hotfix_InitFactionLevelToNpcInfoByFactionID;
        private static DelegateBridge __Hotfix_GetFactionCreditNpcInfoList_1;
        private static DelegateBridge __Hotfix_GetFactionCreditNpcInfoList_0;
        private static DelegateBridge __Hotfix_ClearFactionLevelToNpcInfo;
        private static DelegateBridge __Hotfix_GetEnouthCreditLevelRewardInfo_1;
        private static DelegateBridge __Hotfix_GetPreLevelReardInfoID;
        private static DelegateBridge __Hotfix_GetNextLevelReardInfoID;
        private static DelegateBridge __Hotfix_GetFactionCreditLevelInfo;
        private static DelegateBridge __Hotfix_GetUnlockShopData;
        private static DelegateBridge __Hotfix_GetReardListByFactionLv;
        private static DelegateBridge __Hotfix_GetEnouthCreditLevelRewardInfo_0;
        private static DelegateBridge __Hotfix_get_m_CurSelectedCreditLevel;
        private static DelegateBridge __Hotfix_set_m_CurSelectedCreditLevel;
        private static DelegateBridge __Hotfix_SetSelectFactionCreditUIData_1;
        private static DelegateBridge __Hotfix_SetSelectFactionCreditUIData_0;
        private static DelegateBridge __Hotfix_GetSelectFactionCreditUIData_1;
        private static DelegateBridge __Hotfix_GetSelectFactionCreditUIData_0;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_GetFristNpcItemPath;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;

        [MethodImpl(0x8000)]
        public FactionCreditDetailUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearFactionLevelToNpcInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void EnterOutSideByState(int enterState)
        {
        }

        [MethodImpl(0x8000)]
        private void GetEnouthCreditLevelRewardInfo(FactionCreditLevel factionLv)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetEnouthCreditLevelRewardInfo(GrandFaction gFaction)
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataFactionCreditLevelInfo GetFactionCreditLevelInfo(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        private Dictionary<FactionCreditLevel, List<ConfigDataFactionCreditNpcInfo>> GetFactionCreditNpcInfoList(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        private List<ConfigDataFactionCreditNpcInfo> GetFactionCreditNpcInfoList(int factionId, FactionCreditLevel curLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static FactionCreditLevel GetFactionLevelByFactionIDProxy(int factionId, bool real = false)
        {
        }

        [MethodImpl(0x8000)]
        public string GetFristNpcItemPath()
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess GetHideOrShowActionMainUIProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        private int GetNextLevelReardInfoID(bool result, int currId)
        {
        }

        [MethodImpl(0x8000)]
        private int GetPreLevelReardInfoID(int currId)
        {
        }

        [MethodImpl(0x8000)]
        private void GetReardListByFactionLv(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        public static FactionCreditLevel GetSelectFactionCreditUIData(int factionId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetSelectFactionCreditUIData(int factionId, FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        private List<ConfigDataNpcShopItemInfo> GetUnlockShopData(ConfigDataFactionCreditLevelRewardInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitFactionLevelToNpcInfoByFactionID()
        {
        }

        [MethodImpl(0x8000)]
        private void InitFromStartOrResum(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAgreementButtonBackClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAgreementButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAgreementNpcClick(FactionCreditLevel cl, UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAgreementSignButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropItemClick(ConfigDataNpcShopItemInfo itemInfo, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoToStarMapButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLittleFactionButtonClick(int factionId, FactionCreditLevel lv, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMainBackButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMainCloseButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMainLeaveButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMainShopButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTablRewardItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public static void SetSelectFactionCreditUIData(int factionId, FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        public static void SetSelectFactionCreditUIData(int factionId, FactionCreditLevel lv, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        public static bool StartFactionCreditItemUITask(UIIntent returnToIntent, int factionId, Action<bool> onEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private FactionCreditLevel m_CurSelectedCreditLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum PipeLineStateMaskType
        {
            MainPanel,
            AgreementPanel,
            FactionIdChanged,
            FactionLevelChanged,
            Init
        }

        private class SelectFactionCreditUIData
        {
            private int m_FactionID;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private FactionCreditLevel <SelCreditLEvel>k__BackingField;
            private readonly Dictionary<FactionCreditLevel, int> DetailNpcIndex;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_UpdateNpcItemIndex;
            private static DelegateBridge __Hotfix_GetNpcItemIndex;
            private static DelegateBridge __Hotfix_Clear;
            private static DelegateBridge __Hotfix_get_SelCreditLEvel;
            private static DelegateBridge __Hotfix_set_SelCreditLEvel;

            [MethodImpl(0x8000)]
            public SelectFactionCreditUIData(int factionId)
            {
            }

            [MethodImpl(0x8000)]
            public void Clear()
            {
            }

            [MethodImpl(0x8000)]
            public int GetNpcItemIndex(FactionCreditLevel lv)
            {
            }

            [MethodImpl(0x8000)]
            public void UpdateNpcItemIndex(FactionCreditLevel lv, int itemIndex)
            {
            }

            public FactionCreditLevel SelCreditLEvel
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }
        }
    }
}

