﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class NpcTalkerDialogUITask : NpcTalkerDialogBase
    {
        public static string NpcTalkerDialogCustomParamKey_SolarSystemId;
        public static string NpcTalkerDialogCustomParamKey_NpcDNId;
        public static string NpcTalkerDialogCustomParamKey_DialogInfo;
        public static string NpcTalkerDialogCustomParamKey_MainUIIntent;
        public static string NpcTalkerDialogCustomParamKey_ClostPanelAction;
        public static string NpcTalkerDialogCustomParamKey_PlayNpcAudio;
        public static string NpcTalkerDialogCustomParamKey_PlayNpcGreetingAudio;
        protected Action<Task> m_clostPanelAction;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "NpcTalkerDialogUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNpcTalkerDialog;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitParamFromIntent;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_ResumeTaskFromOptionTask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public NpcTalkerDialogUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParamFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void ResumeTaskFromOptionTask()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartNpcTalkerDialog(UIIntent mainIntent, NpcDNId npcDNId, NpcDialogInfo dialogInfo, Action<Task> closePanelAction = null, bool playNpcAudio = true, bool playGreetingAudio = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

