﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BranchStoryRealDetailUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, QuestRewardInfo> EventOnRewardItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnQuestGalaxyButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnStrkeButtonClick;
        private BranchStoryRewardListUIController m_rewardCtrl;
        private List<BranchStoryRealDetailItemUIController> m_branchStoryRealItemCtrlList;
        private DateTime m_setScrollViewEndTime;
        [AutoBind("./Viewport/Content/PlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RealQuestItem;
        [AutoBindWithParent("../AwardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RewardPanel;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect ScrollView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateRealQuestListDetailInfo;
        private static DelegateBridge __Hotfix_SetScrollViewToEnd;
        private static DelegateBridge __Hotfix_SetRewardScrollViewToStart;
        private static DelegateBridge __Hotfix_GetRealQuestListShowUIProcess;
        private static DelegateBridge __Hotfix_GetDestItemCtrl;
        private static DelegateBridge __Hotfix_EnableRewardPanel;
        private static DelegateBridge __Hotfix_GetSimpleInfoPosition;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnQuestGalaxyButtonClick;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_SetScrollViewEnd;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestGalaxyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestGalaxyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStrkeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStrkeButtonClick;

        public event Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnQuestGalaxyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, QuestRewardInfo> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnStrkeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void EnableRewardPanel(bool show)
        {
        }

        [MethodImpl(0x8000)]
        protected BranchStoryRealDetailItemUIController GetDestItemCtrl(int puzzleQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetRealQuestListShowUIProcess(List<BranchStoryUITask.BranchStorySubQuestInfo> questList, int endShowQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public void GetSimpleInfoPosition(int itemIndex, out Vector3 pos, out ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestGalaxyButtonClick(BranchStoryUITask.BranchStorySubQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(int index, QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStrikeButtonClick(BranchStoryUITask.BranchStorySubQuestInfo questInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRewardScrollViewToStart()
        {
        }

        [MethodImpl(0x8000)]
        private void SetScrollViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void SetScrollViewToEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRealQuestListDetailInfo(string categoryStr, List<BranchStoryUITask.BranchStorySubQuestInfo> subQuestList, Dictionary<string, UnityEngine.Object> resDict, bool isAllComplete, bool resetExpandState)
        {
        }
    }
}

