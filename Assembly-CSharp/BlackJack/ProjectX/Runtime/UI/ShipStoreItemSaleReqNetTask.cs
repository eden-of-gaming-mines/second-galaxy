﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ShipStoreItemSaleReqNetTask : NetWorkTransactionTask
    {
        private List<LBShipStoreItemClient> m_itemInfoList;
        private List<long> m_sellItemCountList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <Result>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnShipStoreItemSaleAck;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;

        [MethodImpl(0x8000)]
        public ShipStoreItemSaleReqNetTask(List<LBShipStoreItemClient> itemInfoList, List<long> sellItemCountList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipStoreItemSaleAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

