﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipTemplateMenuTreeRootUIController : CommonMenuRootUIController
    {
        protected List<ShipType> m_shipTypeList;
        protected Dictionary<ShipType, List<int>> m_shipLevelDict;
        protected Dictionary<int, List<ShipCustomTemplateInfo>> m_shipTemplateInfoDict;
        protected Dictionary<string, UnityEngine.Object> m_assetDict;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ShipCustomTemplateInfo> EventOnShipTemplateInfoMenuItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonMenuItemUIController> EventOnMenuItemShipLevelExpand;
        private ShipTemplateInfoMenuItemUIController m_selectedItem;
        private ShipTemplateInfoMenuItemUIController m_savedTempItem;
        [AutoBind("./UnusedMenuItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_unusedMenuItemRoot;
        public ToggleGroup m_menuLeafItemToggleGroup;
        protected const int MenuRootLevel = 0;
        protected const int MenuShipTypeLevel = 1;
        protected const int MenuShipLevelLevel = 2;
        protected const int MenuShipTemplateItemLevel = 3;
        private static DelegateBridge __Hotfix_UpdateAllShipTemplateMenuTree;
        private static DelegateBridge __Hotfix_UpdateOneShipLevelTree;
        private static DelegateBridge __Hotfix_ExpandMenuItemToShipTemplateItem;
        private static DelegateBridge __Hotfix_CheckLastSelectedItem;
        private static DelegateBridge __Hotfix_SetCurrTemplateInfoUnderShipLevelMenuItem;
        private static DelegateBridge __Hotfix_SetToggles;
        private static DelegateBridge __Hotfix_ClearCacheData;
        private static DelegateBridge __Hotfix_UpdateMenuCacheDataByShipTemplateInfo;
        private static DelegateBridge __Hotfix_UpdateShipTypeList;
        private static DelegateBridge __Hotfix_UpdateShipLevelDict;
        private static DelegateBridge __Hotfix_UpdateShipTemplateInfoDict;
        private static DelegateBridge __Hotfix_GenerateMenuTree;
        private static DelegateBridge __Hotfix_OnMenuItemClick;
        private static DelegateBridge __Hotfix_OnMenuItemShipTemplateClick;
        private static DelegateBridge __Hotfix_OnMenuItemShipLevelClick;
        private static DelegateBridge __Hotfix_OnMenuItemShipTypeClick;
        private static DelegateBridge __Hotfix_add_EventOnShipTemplateInfoMenuItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipTemplateInfoMenuItemClick;
        private static DelegateBridge __Hotfix_add_EventOnMenuItemShipLevelExpand;
        private static DelegateBridge __Hotfix_remove_EventOnMenuItemShipLevelExpand;

        public event Action<CommonMenuItemUIController> EventOnMenuItemShipLevelExpand
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ShipCustomTemplateInfo> EventOnShipTemplateInfoMenuItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CheckLastSelectedItem(ShipCustomTemplateInfo currTemplateInfo, ShipCustomTemplateInfo currSelectedInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearCacheData()
        {
        }

        [MethodImpl(0x8000)]
        public void ExpandMenuItemToShipTemplateItem(ShipCustomTemplateInfo templateInfo, ShipCustomTemplateInfo currtemplateInfo, Dictionary<string, UnityEngine.Object> assetDict, bool isSetSelected = true)
        {
        }

        [MethodImpl(0x8000)]
        protected void GenerateMenuTree(Dictionary<string, UnityEngine.Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMenuItemClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMenuItemShipLevelClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMenuItemShipTemplateClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMenuItemShipTypeClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrTemplateInfoUnderShipLevelMenuItem(CommonMenuItemUIController menuItemCtrl, ShipCustomTemplateInfo currTemplateInfo, ShipCustomTemplateInfo currSelectedInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggles()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllShipTemplateMenuTree(Dictionary<int, List<ShipCustomTemplateInfo>> preCustomTemplateDict, List<ShipCustomTemplateInfo> customTemplateList, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateMenuCacheDataByShipTemplateInfo(ShipCustomTemplateInfo shipTemplateInfo, List<ShipType> shipTypeList, Dictionary<ShipType, List<int>> shipLevelDict, Dictionary<int, List<ShipCustomTemplateInfo>> shipTempalteInfoDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateOneShipLevelTree(List<ShipCustomTemplateInfo> preCustomTemplateList, List<ShipCustomTemplateInfo> customTemplateList, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateShipLevelDict(ShipType shipType, int shipConfId, Dictionary<ShipType, List<int>> shipLevelDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateShipTemplateInfoDict(int shipConfId, ShipCustomTemplateInfo shipTemplateInfo, Dictionary<int, List<ShipCustomTemplateInfo>> shipTempalteInfoDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateShipTypeList(ShipType shipType, List<ShipType> shipTypeList)
        {
        }

        [CompilerGenerated]
        private sealed class <UpdateShipTypeList>c__AnonStorey0
        {
            internal ShipType shipType;

            internal bool <>m__0(ShipType e) => 
                (e == this.shipType);
        }
    }
}

