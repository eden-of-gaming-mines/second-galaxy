﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public sealed class OverSeaActivityManagerUITask : UITaskBase
    {
        private bool? m_isPrepareSuccessForCommonData;
        private bool? m_isPrepareDataSuccessForCurMode;
        private bool m_returnRewardResLoaded;
        private bool m_activityBindResLoaded;
        private bool m_dailySignResLoaded;
        public const string UITaskName = "OverSeaActivityManagerUITask";
        private const string ParamEnterFromSpaceStation = "EnterFromSpaceStation";
        private bool m_isBindTabShow;
        private OverSeaActivityManagerUIController m_mainCtrl;
        private ReturnRewardUITask m_loginUITask;
        private OverSeaActivityCmDailySignUITask m_signInUITask;
        private OverSeaActivityBindUITask m_bindUITask;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartUITaskWithPrePare;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnPrepareDataEnd;
        private static DelegateBridge __Hotfix_PrepareForCommonData;
        private static DelegateBridge __Hotfix_OnRequestIntent;
        private static DelegateBridge __Hotfix_OnRequestOverSeaActivityManagerUITaskPause;
        private static DelegateBridge __Hotfix_OnRefreshRedPoint;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnLoginTabButtonClick;
        private static DelegateBridge __Hotfix_OnSignInTabButtonClick;
        private static DelegateBridge __Hotfix_OnBindingTabButtonClick;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_SwitchMode;
        private static DelegateBridge __Hotfix_StartCurrentSubUITask;
        private static DelegateBridge __Hotfix_OnReturnRewardResLoaded;
        private static DelegateBridge __Hotfix_OnDailySignResLoaded;
        private static DelegateBridge __Hotfix_OnActivityBindResLoaded;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_GetHideAllSubUITaskProcessImmediately;
        private static DelegateBridge __Hotfix_GetSubUITaskProcess;
        private static DelegateBridge __Hotfix_GetLoginUITaskPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetSignInUITaskPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetBindUITaskPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public OverSeaActivityManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetBindUITaskPanelShowOrHideProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetHideAllSubUITaskProcessImmediately()
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetLoginUITaskPanelShowOrHideProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetSignInUITaskPanelShowOrHideProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetSubUITaskProcess(string modeName, bool isShow, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnActivityBindResLoaded()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindingTabButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDailySignResLoaded()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoginTabButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrepareDataEnd(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRefreshRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestIntent(Action<UIIntent> action)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestOverSeaActivityManagerUITaskPause(Action onPauseEnd, bool ignoreCloseAnimation)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReturnRewardResLoaded()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSignInTabButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareForCommonData(string mode, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StartCurrentSubUITask(string mode, Action<bool> onPrepareEnd, Action<bool> onEnd, bool needRedirect)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartUITaskWithPrePare(UIIntent returnToIntent, bool enterFromSpaceStation, string mode = "ModeLogin", Action<bool> onPrepareEnd = null, Action redirectPipelineOnResloadEnd = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SwitchMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetBindUITaskPanelShowOrHideProcess>c__AnonStorey4
        {
            internal bool immediateComplete;
            internal OverSeaActivityManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_bindUITask.ShowBindPanel(this.immediateComplete, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_bindUITask.HideBindPanel(this.immediateComplete, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetLoginUITaskPanelShowOrHideProcess>c__AnonStorey2
        {
            internal bool immediateComplete;
            internal OverSeaActivityManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_loginUITask.ShowReturnRewardUI(this.immediateComplete, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_loginUITask.HideReturnRewardUI(this.immediateComplete, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetSignInUITaskPanelShowOrHideProcess>c__AnonStorey3
        {
            internal bool immediateComplete;
            internal OverSeaActivityManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_signInUITask.ShowOverSeaActivityCmDailySignUITask(this.immediateComplete, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_signInUITask.HideOverSeaActivityCmDailySignUITask(this.immediateComplete, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <OnRequestOverSeaActivityManagerUITaskPause>c__AnonStorey0
        {
            internal Action onPauseEnd;
            internal OverSeaActivityManagerUITask $this;

            internal void <>m__0(UIProcess process, bool b)
            {
                this.$this.Pause();
                if (this.onPauseEnd != null)
                {
                    this.onPauseEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartCurrentSubUITask>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
            internal OverSeaActivityManagerUITask $this;

            internal void <>m__0(bool ret)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(ret);
                if (this.$this.m_isPrepareDataSuccessForCurMode.Value && (this.$this.m_loginUITask == null))
                {
                    this.$this.m_loginUITask = UIManager.Instance.FindUITaskWithName(typeof(ReturnRewardUITask).Name, false) as ReturnRewardUITask;
                    if (this.$this.m_loginUITask != null)
                    {
                        this.$this.m_loginUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestIntent);
                        this.$this.m_loginUITask.EventOnRefreshRedPoint += new Action(this.$this.OnRefreshRedPoint);
                        this.$this.m_loginUITask.EventOnRequestOverSeaActivityManagerUIPause += new Action<Action, bool>(this.$this.OnRequestOverSeaActivityManagerUITaskPause);
                    }
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }

            internal void <>m__1(bool ret)
            {
                this.$this.m_isPrepareDataSuccessForCurMode = new bool?(ret);
                if (this.$this.m_signInUITask == null)
                {
                    this.$this.m_signInUITask = UIManager.Instance.FindUITaskWithName("OverSeaActivityCmDailySignUITask", false) as OverSeaActivityCmDailySignUITask;
                    if (this.$this.m_signInUITask != null)
                    {
                        this.$this.m_signInUITask.EventOnRequestSwitchTask += new Action<Action<UIIntent>>(this.$this.OnRequestIntent);
                        this.$this.m_signInUITask.EventOnRefreshRedPoint += new Action(this.$this.OnRefreshRedPoint);
                        this.$this.m_signInUITask.EventOnRequestOverSeaActivityManagerUIPause += new Action<Action, bool>(this.$this.OnRequestOverSeaActivityManagerUITaskPause);
                    }
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }

            internal void <>m__2(bool ret)
            {
                if (this.$this.m_bindUITask == null)
                {
                    this.$this.m_bindUITask = UIManager.Instance.FindUITaskWithName(typeof(OverSeaActivityBindUITask).Name, false) as OverSeaActivityBindUITask;
                    if (this.$this.m_bindUITask != null)
                    {
                        this.$this.m_bindUITask.EventOnRequestIntent += new Action<Action<UIIntent>>(this.$this.OnRequestIntent);
                        this.$this.m_bindUITask.EventOnRefreshRedPoint += new Action(this.$this.OnRefreshRedPoint);
                        this.$this.m_bindUITask.EventOnRequestCloseAllPanels += new Action<Action, bool>(this.$this.OnRequestOverSeaActivityManagerUITaskPause);
                    }
                }
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        public static class OverSeaActivityMode
        {
            public const string ModeLogin = "ModeLogin";
            public const string ModeSignIn = "ModeSignIn";
            public const string ModeBind = "ModeBind";
        }

        private enum PipeLineStateMaskType
        {
            EnterFromSpaceStation,
            LoginShowRedPoint,
            SignInShowRedPoint,
            BindShowRedPoint
        }
    }
}

