﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainConfirmPanelUIController : UIControllerBase
    {
        public CaptainWingManSetConfirmItemUIController m_currCaptainCtrl;
        public CaptainWingManSetConfirmItemUIController m_replacedWingmanCtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBStoreItemClient, Vector3> EventOnWingManShipItemClick;
        [AutoBind("./SetWingManConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SetWingManConfirmPanel;
        [AutoBind("./SetWingManConfirmPanel/Wingman1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CurrCaptainDummy;
        [AutoBind("./SetWingManConfirmPanel/Wingman2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ReplacedWingManDummy;
        [AutoBind("./SetWingManConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WingManReplaceConfirmButton;
        [AutoBind("./SetWingManConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WingManReplaceCancelButton;
        [AutoBind("./SetWingManConfirmPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text InfoText;
        [AutoBind("./SetWingManConfirmPanel/ShipInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ShipInfoDummy;
        [AutoBind("./LeaningConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LeaningConfirmPanelCtrl;
        [AutoBind("./LeaningConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LeaningConfirmButton;
        [AutoBind("./LeaningConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LeaningCancelButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Hide;
        private static DelegateBridge __Hotfix_UpdateWingManSetConfirmPanel;
        private static DelegateBridge __Hotfix_UpdateLeaningConfirmPanel;
        private static DelegateBridge __Hotfix_ShowShipSimpleInfo;
        private static DelegateBridge __Hotfix_add_EventOnWingManShipItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnWingManShipItemClick;

        public event Action<ILBStoreItemClient, Vector3> EventOnWingManShipItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Hide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipSimpleInfo(ILBStoreItemClient ship)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLeaningConfirmPanel(bool isForget)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWingManSetConfirmPanel(List<LBStaticHiredCaptain> wingManList, LBStaticHiredCaptain currCaptain, Dictionary<string, UnityEngine.Object> resDict, int index)
        {
        }
    }
}

