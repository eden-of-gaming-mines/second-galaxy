﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class StarMapSolarSystemInfoPanelUIController : UIControllerBase
    {
        private GuildLogoController m_guildLogoCtrl;
        private const string ShowDetailPanel = "PanelOpen";
        private const string HideDetailPanel = "PanelClose";
        [AutoBind("./FactionIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image FactionIconImage;
        [AutoBind("./SolarSystemDetailInfoPanel/SolarSystemDetailInfoPanelBgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SolarSystemDetailInfoPanelBgButton;
        [AutoBind("./SolarSystemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemNameText;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FlourishProcessGroup/FlourishProcessImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FlourishProcessImage;
        [AutoBind("./SolarSystemInfoBgImage/FlourishLevelImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FlourishLevelImage;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FactionDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemDetailInfoFactionDescText;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FactionGroup/FactionValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemDetailInfoFactionValueText;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/SecurityDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemDetailInfoSecurityDescText;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/SecurityGroup/SecurityValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemDetailInfoSecurityValueText;
        [AutoBind("./SolarSystemDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SolarSystemDetailInfoPanelStateCtrl;
        [AutoBind("./DetailInfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SolarSystemInfoDetailInfoButton;
        [AutoBind("./GuildLogoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildLogoGroupGo;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FlourishTitle/FlourishValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FlourishValueText;
        [AutoBind("./FlourishLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FlourishLevelText;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/ProsperityGroup/ProsperityValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProsperityText;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/ProsperityGroup/ProsperityValueText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProsperityStateCtrl;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FlourishProcessGroup/OneText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_flourishLevelOneTextStateCtrl;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FlourishProcessGroup/TwoText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_flourishLevelTwoTextStateCtrl;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FlourishProcessGroup/ThreeText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_flourishLevelThreeTextStateCtrl;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FlourishProcessGroup/FourText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_flourishLevelFourTextStateCtrl;
        [AutoBind("./SolarSystemDetailInfoPanel/FrameImage/BGImage/Scroll View/Viewport/Content/DetailInfo/FlourishProcessGroup/FiveText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_flourishLevelFiveTextStateCtrl;
        private static DelegateBridge __Hotfix_UpdateGuildSolarSystemInfo;
        private static DelegateBridge __Hotfix_ClearOnPasue;
        private static DelegateBridge __Hotfix_IsGuildSolarSystemDetailInfoPanelShow;
        private static DelegateBridge __Hotfix_SwitchGuildSolarSystemDetailInfoPanelVisibleState;
        private static DelegateBridge __Hotfix_ShowGuildSolarSystemDetailInfoPanel;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetFlourishLevelTextColor;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public void ClearOnPasue()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildSolarSystemDetailInfoPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetFlourishLevelTextColor(float level)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowGuildSolarSystemDetailInfoPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchGuildSolarSystemDetailInfoPanelVisibleState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildSolarSystemInfo(GDBSolarSystemInfo gdbSolarSystemInfo, GuildSolarSystemInfoClient guildSolarSystemInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

