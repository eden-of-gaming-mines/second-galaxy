﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    internal class CommonItemDetailInfoWeaponUIController : CommonItemDetailInfoHasEquipedEffectUIBase
    {
        public string m_propertyUIPrefabAssetName;
        [AutoBind("./Viewport/Content/EquipedEffect", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropertyItemRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WeaponStateCtrl;
        [AutoBind("./Viewport/Content/CapabilityPanel/AmmoCostInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoCost;
        [AutoBind("./Viewport/Content/CapabilityPanel/AmmoCostInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoCostDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/AmmoCostInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoCostUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DPHInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OneGroupDamageNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DPHInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OneGroupDamageDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DPHInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OneGroupDamageUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/LaunchNumber/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OneGroupLaunchNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/LaunchNumber/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OneGroupLaunchDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/LaunchNumber/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OneGroupLaunchUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/IntervalInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CD4NextWaveNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/IntervalInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CD4NextWaveDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/IntervalInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CD4NextWaveUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneIntervalInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CD4NextWaveDroneNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneIntervalInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CD4NextWaveDroneDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneIntervalInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CD4NextWaveDroneUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/CriticalChanceInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CriticalNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/CriticalChanceInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CriticalDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/CriticalChanceInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CriticalUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/AccuracyInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AccuracyNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/AccuracyInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AccuracyDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/AccuracyInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AccuracyUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneAmountInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DroneSizeNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneAmountInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DroneSizeDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DroneAmountInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DroneSizeUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DurationModifyInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DroneFightTimeNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/DurationModifyInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DroneFightTimeDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/DurationModifyInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DroneFightTimeUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FireRangeMaxNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireRangeMaxDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/RangeInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireRangeMaxUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/MagazineInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoClipSizeNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/MagazineInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoClipSizeDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/MagazineInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoClipSizeUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/AimingInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FireCtrlAccuracyNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/AimingInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireCtrlAccuracyDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/AimingInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FireCtrlAccuracyUpArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/EnergyConsumeInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LaunchCostEnergyNumber;
        [AutoBind("./Viewport/Content/CapabilityPanel/EnergyConsumeInfo/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LaunchCostEnergyDownArrow;
        [AutoBind("./Viewport/Content/CapabilityPanel/EnergyConsumeInfo/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LaunchCostEnergyUpArrow;
        [AutoBind("./Viewport/Content/BasedataPanel/TechLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechLevelValue;
        [AutoBind("./Viewport/Content/BasedataPanel/QualityRank/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QualityRankValue;
        [AutoBind("./Viewport/Content/BasedataPanel/PackedSize/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PackedSizeNumber;
        [AutoBind("./Viewport/Content/BasedataPanel/SizeType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SizeTypeText;
        [AutoBind("./Viewport/Content/CapabilityPanel/TrackingSpeed/ArrowUp", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TradeSpeedArrowUp;
        [AutoBind("./Viewport/Content/CapabilityPanel/TrackingSpeed/ArrowDown", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TradeSpeedArrowDown;
        [AutoBind("./Viewport/Content/CapabilityPanel/TrackingSpeed", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TradeSpeedRoot;
        [AutoBind("./Viewport/Content/CapabilityPanel/TrackingSpeed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeSpeedValueText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateWeaponPropertiesUI;
        private static DelegateBridge __Hotfix_UpdateCommonUI;
        private static DelegateBridge __Hotfix_UpdateRailgunAndPlasmaUI;
        private static DelegateBridge __Hotfix_UpdateLaserUI;
        private static DelegateBridge __Hotfix_UpdateMissileLancherUI;
        private static DelegateBridge __Hotfix_UpdateDronrLancherUI;
        private static DelegateBridge __Hotfix_UpdateFireCtrlAccuracy;
        private static DelegateBridge __Hotfix_UpdateAmmonClipSize;
        private static DelegateBridge __Hotfix_UpdateOneGroupDamage;
        private static DelegateBridge __Hotfix_UpdateCD4NextWaveGroupValue;
        private static DelegateBridge __Hotfix_UpdateCD4NextWaveDroneValue;
        private static DelegateBridge __Hotfix_UpdateDroneSize;
        private static DelegateBridge __Hotfix_UpdateCriticalValue;
        private static DelegateBridge __Hotfix_UpdateFireRaneMaxValue;
        private static DelegateBridge __Hotfix_UpdateLaunchCostEnergy;
        private static DelegateBridge __Hotfix_UpdateAmmoCost;
        private static DelegateBridge __Hotfix_UpdateLaunchNumber;
        private static DelegateBridge __Hotfix_UpdateTradeSpeed;
        private static DelegateBridge __Hotfix_CalAmmoCost;

        [MethodImpl(0x8000)]
        private uint CalAmmoCost(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAmmoCost(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAmmonClipSize(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCD4NextWaveDroneValue(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCD4NextWaveGroupValue(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCommonUI(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCriticalValue(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDroneSize(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDronrLancherUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFireCtrlAccuracy(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFireRaneMaxValue(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLaserUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLaunchCostEnergy(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLaunchNumber(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMissileLancherUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateOneGroupDamage(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRailgunAndPlasmaUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTradeSpeed(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWeaponPropertiesUI(ILBItem itemInfo, ILBItem itemInfoCompare)
        {
        }
    }
}

