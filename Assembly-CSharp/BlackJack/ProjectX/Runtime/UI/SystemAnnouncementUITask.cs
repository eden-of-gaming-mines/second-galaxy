﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class SystemAnnouncementUITask : UITaskBase
    {
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private AnnouceMentInfo m_currAnnouceMentInfo;
        private float defultSpeed;
        private float defultTime;
        private List<AnnouceMentInfo> announcementStrList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnAnnouncementAdd;
        public const string TaskName = "SystemAnnouncementUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_CheckLayerDescArray;
        private static DelegateBridge __Hotfix_GetNextAnnoucement;
        private static DelegateBridge __Hotfix_IsNeedShowAnnoucement;
        private static DelegateBridge __Hotfix_ClearCurrAnnoucement;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_RegisterEvent;
        private static DelegateBridge __Hotfix_UnregisterEvent;
        private static DelegateBridge __Hotfix_OnSystemAnnouncementNtf;
        private static DelegateBridge __Hotfix_AddAnnoucementToList;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnAnnouncementAdd;
        private static DelegateBridge __Hotfix_remove_EventOnAnnouncementAdd;

        public event Action<bool> EventOnAnnouncementAdd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SystemAnnouncementUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AddAnnoucementToList(int id, string msg)
        {
        }

        [MethodImpl(0x8000)]
        protected override void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
        {
        }

        [MethodImpl(0x8000)]
        public void ClearCurrAnnoucement()
        {
        }

        [MethodImpl(0x8000)]
        public void GetNextAnnoucement(out float time, out string msg)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedShowAnnoucement()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSystemAnnouncementNtf(int id, string content, List<FormatStringParamInfo> formatStringParamInfos)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private class AnnouceMentInfo
        {
            public string msg;
            public int currCount;
            public ConfigDataSystemMessage config;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

