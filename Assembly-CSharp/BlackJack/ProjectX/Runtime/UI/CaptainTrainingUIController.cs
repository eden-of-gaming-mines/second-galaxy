﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainTrainingUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnMemoirsBookClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnMemoirsBookLongPressingStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnMemoirsBookLongPressing;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnMemoirsBookLongPressingEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCaptainExpLevelUp;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnExpAnimationEnd;
        public List<CaptainMemoirsBookUIController> m_memoirsBookCtrlList;
        private long m_currExp;
        private int m_currCaptainLevel;
        private long m_addedExp;
        private int m_currLevelUpExp;
        private int m_maxCaptainLevel;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Panel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./Panel/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./Panel/QualityGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainSubRankText;
        [AutoBind("./Panel/ProfessionGroup/OccupationIcon/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionIconImage;
        [AutoBind("./Panel/ProfessionGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProfessionText;
        [AutoBind("./Panel/Level/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainLevelText;
        [AutoBind("./Panel/ExpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrentExpText;
        [AutoBind("./Panel/ExpProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ExpProgressBar;
        [AutoBind("./Panel/MemoirsBookGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MemoirsBookGroup;
        [AutoBind("./Panel/ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateCaptainTrainingInfo;
        private static DelegateBridge __Hotfix_UpdateExp;
        private static DelegateBridge __Hotfix_UpdateMemoirBookCount;
        private static DelegateBridge __Hotfix_AddExpToCaptain;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelDummyPos;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnMemoirsBookClick;
        private static DelegateBridge __Hotfix_OnMemoirsBookLongPressingStart;
        private static DelegateBridge __Hotfix_OnMemoirsBookLongPressing;
        private static DelegateBridge __Hotfix_OnMemoirsBookLongPressingEnd;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookClick;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookClick;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookLongPressingStart;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookLongPressingStart;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookLongPressing;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookLongPressing;
        private static DelegateBridge __Hotfix_add_EventOnMemoirsBookLongPressingEnd;
        private static DelegateBridge __Hotfix_remove_EventOnMemoirsBookLongPressingEnd;
        private static DelegateBridge __Hotfix_add_EventOnCaptainExpLevelUp;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainExpLevelUp;
        private static DelegateBridge __Hotfix_add_EventOnExpAnimationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnExpAnimationEnd;

        public event Action EventOnCaptainExpLevelUp
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnExpAnimationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnMemoirsBookClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnMemoirsBookLongPressing
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnMemoirsBookLongPressingEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnMemoirsBookLongPressingStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AddExpToCaptain(int expAdded, Action expAnimEndAction = null)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookClick(CaptainMemoirsBookUIController bookCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookLongPressing(CaptainMemoirsBookUIController bookCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookLongPressingEnd(CaptainMemoirsBookUIController bookCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnMemoirsBookLongPressingStart(CaptainMemoirsBookUIController bookCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainTrainingInfo(LBStaticHiredCaptain selCaptain, int maxLevel, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateExp()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMemoirBookCount(int idx, long currCount)
        {
        }
    }
}

