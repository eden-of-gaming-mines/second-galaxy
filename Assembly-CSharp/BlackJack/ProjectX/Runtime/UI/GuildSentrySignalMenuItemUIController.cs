﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildSentrySignalMenuItemUIController : MenuItemUIControllerBase
    {
        private GuildSentryInterestScene m_guildSentryInterestScene;
        private CommonUIStateController[] m_shipLimitCtrlList;
        private List<GuildSentrySignalShipTypeDetailItemUIController> m_shipTypeDetailInfos;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAllowInShipButtonClick;
        public List<ShipType> m_shipTypeLimits;
        private const string AllowInImageUIState_AllowIn = "AllowIn";
        private const string AllowInImageUIState_NotAllowIn = "NotAllowIn";
        private EasyObjectPool m_easyPool;
        private const string ShipTypeDetailInfoItemName = "GuildSentrySignalShipTypeDetailItem";
        [AutoBind("./TargetInfo/Info/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemNameText;
        [AutoBind("./TargetInfo/Info/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemNameStateCtrl;
        [AutoBind("./Detail/SignalDetail/Location/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_locationNameText;
        [AutoBind("./TargetInfo/Info/TargetShipCount/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_targetShipCountText;
        [AutoBind("./Detail/SignalDetail/DeviationDistance/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_trackingDeviationDistance;
        [AutoBind("./Detail/AllowInTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_allowInShipTypeTitleRoot;
        [AutoBind("./Detail/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_allowInShipTypeRoot;
        [AutoBind("./Detail/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowInShipTypeButton;
        [AutoBind("./Detail/StrikeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_strikeButton;
        [AutoBind("./Detail/ShipTypeInfoDetail/ItemBG", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_shipTypeInfoDetaiRoot;
        [AutoBind("./Detail/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeFrigate;
        [AutoBind("./Detail/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeDestroyer;
        [AutoBind("./Detail/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCruiser;
        [AutoBind("./Detail/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleCruiser;
        [AutoBind("./Detail/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleShip;
        [AutoBind("./Detail/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeIndustryShip;
        private static DelegateBridge __Hotfix_SetGuildSentrySignalnfo;
        private static DelegateBridge __Hotfix_SetEasyPool;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetGuildSentryInterestScene;
        private static DelegateBridge __Hotfix_OnAllowInShipTypeButtonClick;
        private static DelegateBridge __Hotfix_PrepareShipTypeDetailInfo;
        private static DelegateBridge __Hotfix_SetStrikeButtonState;
        private static DelegateBridge __Hotfix_UpdateItemNameState;
        private static DelegateBridge __Hotfix_get_ShipLimitCtrlList;
        private static DelegateBridge __Hotfix_add_EventOnAllowInShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAllowInShipButtonClick;

        public event Action EventOnAllowInShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildSentryInterestScene GetGuildSentryInterestScene()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipTypeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void PrepareShipTypeDetailInfo(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEasyPool(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildSentrySignalnfo(GuildSentryInterestScene guildSentrySignalInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetStrikeButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemNameState(int level)
        {
        }

        private CommonUIStateController[] ShipLimitCtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

