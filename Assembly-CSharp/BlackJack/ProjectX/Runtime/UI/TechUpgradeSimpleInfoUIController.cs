﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TechUpgradeSimpleInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<StoreItemType, int, Vector3, Vector2> EventOnUnlockItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ConfigDataTechInfo> EventOnDetailButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ConfigDataTechInfo> EventOnSpeedUpButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ConfigDataTechInfo> EventOnStopButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ConfigDataTechInfo> EventOnUpgradeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSpeedUpItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnUpgradInfoBGButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCloseButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBGButtonClick;
        private List<TechUpgradeSimpleInfoLevelUpEffectItemUIController> m_levelUpEffecItemCtrlList;
        private TechUpgradeInfo m_upgradeInfo;
        private ConfigDataTechInfo m_techInfo;
        private List<GameObject> m_levelPropInfoList;
        private List<CommonItemIconUIController> m_unlockItemUICtrlList;
        private List<Text> m_speedupValueTextList;
        private List<CommonItemIconUIController> m_speedupItemCtrlList;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./Panel/TechItem/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DetailButton;
        [AutoBind("./SpeedPanel/UpgradeImmediatelyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SpeedUpButton;
        [AutoBind("./SpeedPanel/StopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StopButton;
        [AutoBind("./Panel/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeButton;
        [AutoBind("./Panel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./SpeedPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SpeedUpItemGroup;
        [AutoBind("./Panel/TechItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SkillIcon;
        [AutoBind("./SpeedPanel/ProcessBar/ProcessBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image UpgradeProcessbar;
        [AutoBind("./Panel/TechItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SkillNameText;
        [AutoBind("./Panel/Scroll View/Viewport/Content/EffectItem/BgImage/EffectUpItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EffectNameText;
        [AutoBind("./Panel/TechItem/LevelChange/OldLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrLevelText;
        [AutoBind("./Panel/TechItem/LevelChange/NewLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextLevelText;
        [AutoBind("./Panel/TechItem/FullLevel /ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FullLevelText;
        [AutoBind("./Panel/Scroll View/Viewport/Content/EffectItem/BgImage/EffectUpItem/LevelChange/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrEffectText;
        [AutoBind("./Panel/Scroll View/Viewport/Content/EffectItem/BgImage/EffectUpItem/LevelChange/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextEffectText;
        [AutoBind("./Panel/Scroll View/Viewport/Content/EffectItem/BgImage/EffectUpItem/FullLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FullEffectText;
        [AutoBind("./Panel/Scroll View/Viewport/Content/UpgradeGruop", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnlockItemGroupGameObject;
        [AutoBind("./Panel/Scroll View/Viewport/Content/UpgradeGruop/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnlockItemGroup;
        [AutoBind("./Panel/Scroll View/Viewport/Content/UpgradeGruop/ItemGroup/ItemDummy", AutoBindAttribute.InitState.Inactive, false)]
        public GameObject UnlockItemDummy;
        [AutoBind("./Panel/Scroll View/Viewport/Content/UpgradeGruop/TitleImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UnLockTitleStateCtrl;
        [AutoBind("./SpeedPanel/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./SpeedPanel/UpgradeImmediatelyButton/MoneyTextGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RealMoneyValueText;
        [AutoBind("./UpgradeInfo/ItemGroup/Item1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropItem;
        [AutoBind("./UpgradeInfo/BgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeInfoBGButton;
        [AutoBind("./UpgradeInfo/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropInfoListRoot;
        [AutoBind("./UpgradeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PropInfoRootStateCtrl;
        [AutoBind("./UnderPartState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SimpleInfoPanelStateCtrl;
        [AutoBind("./Panel/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpgradeButtonStateCtrl;
        [AutoBind("./Panel/Scroll View/Viewport/Content/EffectItem/BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechLevelUpEffectRoot;
        [AutoBind("./Panel/Scroll View/Viewport/Content/EffectItem/BgImage/EffectUpItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EffectItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateTechSimpleInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_ShowOrHideTechDetailLevelPropInfo;
        private static DelegateBridge __Hotfix_UpdateTechDetailLevelPropInfo;
        private static DelegateBridge __Hotfix_GetSimpleInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_GetTechInfo;
        private static DelegateBridge __Hotfix_SetUpgradButtonState;
        private static DelegateBridge __Hotfix_SetSpeedUpItemInfo;
        private static DelegateBridge __Hotfix_SetPropValue;
        private static DelegateBridge __Hotfix_SetUnlockItem;
        private static DelegateBridge __Hotfix_UpdateTechLevelUpEffectItemList;
        private static DelegateBridge __Hotfix_GetPropertyInfoList4TechLevelInfo;
        private static DelegateBridge __Hotfix_GetPropValueStr;
        private static DelegateBridge __Hotfix_GetTechBuffDescStr;
        private static DelegateBridge __Hotfix_SetPanelState;
        private static DelegateBridge __Hotfix_GetUnlockItemCtrl;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_OnStopButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradInfoBGButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_OnUnlockItemClick;
        private static DelegateBridge __Hotfix_add_EventOnUnlockItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnUnlockItemClick;
        private static DelegateBridge __Hotfix_add_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpeedUpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStopButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStopButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_add_EventOnUpgradInfoBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUpgradInfoBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_SpeedUpItemCtrlList;

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ConfigDataTechInfo> EventOnDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ConfigDataTechInfo> EventOnSpeedUpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSpeedUpItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ConfigDataTechInfo> EventOnStopButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<StoreItemType, int, Vector3, Vector2> EventOnUnlockItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ConfigDataTechInfo> EventOnUpgradeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnUpgradInfoBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private List<CommonPropertyInfo> GetPropertyInfoList4TechLevelInfo(ConfigDataTechLevelInfo techLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetPropValueStr(ConfigDataTechLevelInfo techLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetSimpleInfoPanelUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        private string GetTechBuffDescStr(ConfigDataTechLevelInfo techLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        public ConfigDataTechInfo GetTechInfo()
        {
        }

        [MethodImpl(0x8000)]
        private CommonItemIconUIController GetUnlockItemCtrl(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStopButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnUnlockItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradInfoBGButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPanelState(TechUpgradeManageUITask.SimplePanelState nodeState)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPropValue(int learntLv, ConfigDataTechInfo techInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSpeedUpItemInfo(int idx, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUnlockItem(int learntLv, ConfigDataTechInfo techInfo, bool isShipTech, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUpgradButtonState(TechUpgradeManageUITask.SimplePanelState panelState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideTechDetailLevelPropInfo(bool isShow, ConfigDataTechInfo techInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTechDetailLevelPropInfo(ConfigDataTechInfo techInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTechLevelUpEffectItemList(List<CommonPropertyInfo> currLevelPropertyList, List<CommonPropertyInfo> nextLevelPropertyList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTechSimpleInfo(TechUpgradeManageUITask.SimpleInfoTechUIInfo techUIInfo, bool isShipTech, Dictionary<string, UnityEngine.Object> dict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private List<CommonItemIconUIController> SpeedUpItemCtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

