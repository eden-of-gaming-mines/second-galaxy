﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildTradePortInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnTradePortListItemClick;
        private CommonItemIconUIController m_purchaseOrderCommonItemUICtrl;
        private CommonItemIconUIController m_transportInfoCommonItemUICtrl;
        public Dictionary<string, UnityEngine.Object> m_assetDic;
        private Dictionary<int, GuildTradePortExtraInfo> m_cacheGuildTradePortExtraInfoDict;
        private List<int> m_cacheGuildTradePortExtraInfoKeys;
        private int m_curGuildPortIndex = -1;
        [AutoBind("./RightGroup/TitleImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_titleStateCtrl;
        [AutoBind("./LeftGroup/TradePortList/TradePortInfoGroup/CategoryTitle/CornerImage/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeCountText;
        [AutoBind("./LeftGroup/TradePortList/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        [AutoBind("./LeftGroup/TradePortList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool TradePortListItemPool;
        [AutoBind("./LeftGroup/TradePortList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect TradePortListScrollRect;
        [AutoBind("./LeftGroup/TradePortList", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TradePortListUIStateCtrl;
        [AutoBind("./RightGroup/TransportationGroup/LocationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TransportationLocationBtn;
        [AutoBind("./RightGroup/TransportationGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TransportationGroupCommonUIStateController;
        [AutoBind("./RightGroup/TransportationGroup/NormalGroup/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TransportInfoCommonItemDummy;
        [AutoBind("./RightGroup/TransportationGroup/NormalGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportInfoItemNameText;
        [AutoBind("./RightGroup/TransportationGroup/NormalGroup/ValueNumberText/MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TransportInfoMoneyIconImage;
        [AutoBind("./RightGroup/TransportationGroup/NormalGroup/ValueNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportInfoSumPriceText;
        [AutoBind("./RightGroup/TransportationGroup/NormalGroup/GoalText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportInfoGoalPortNameText;
        [AutoBind("./RightGroup/TransportationGroup/NormalGroup/TransportationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportInfoProgressText;
        [AutoBind("./RightGroup/GoodsPurchaseGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GoodsGroupCommonUIStateController;
        [AutoBind("./RightGroup/GoodsPurchaseGroup/ButtonGroup/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PurchaseOrderIssueButton;
        [AutoBind("./RightGroup/GoodsPurchaseGroup/ButtonGroup/EditorButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PurchaseOrderEditButton;
        [AutoBind("./RightGroup/GoodsPurchaseGroup/NormalGroup/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PurchaseOrderCommonItemDummy;
        [AutoBind("./RightGroup/GoodsPurchaseGroup/NormalGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PurchaseOrderItemName;
        [AutoBind("./RightGroup/GoodsPurchaseGroup/NormalGroup/ValueNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PurchaseOrderSumPriceText;
        [AutoBind("./RightGroup/GoodsPurchaseGroup/NormalGroup/PurchaseCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PurchaseOrderItemCountText;
        [AutoBind("./RightGroup/TitleImage/ClassText/ClassNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradePortLevelText;
        [AutoBind("./RightGroup/TitleImage/TitleNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradePortNameText;
        [AutoBind("./RightGroup/TitleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TradePortImage;
        private GameObject m_itemTemplateGo;
        private const string GuildTradePortListItemPoolName = "GuildTradePortListItem";
        private const string GuildTradePortListItemAssetName = "GuildTradePortListItem";
        private static DelegateBridge __Hotfix_CreateTradePortListUIProcee;
        private static DelegateBridge __Hotfix_UpdateTradePortExtraInfoList;
        private static DelegateBridge __Hotfix_SetTradePortListItemSelected;
        private static DelegateBridge __Hotfix_UpdatePurchaseOrderInfo;
        private static DelegateBridge __Hotfix_UpdateTransportInfo;
        private static DelegateBridge __Hotfix_UpdateTradePortTradeInfo;
        private static DelegateBridge __Hotfix_GetFristTradePort;
        private static DelegateBridge __Hotfix_OnTradePortListItemClick;
        private static DelegateBridge __Hotfix_OnTradePortItemFill;
        private static DelegateBridge __Hotfix_SetAllTradePortListItemUnselected;
        private static DelegateBridge __Hotfix_GetTradePortListItemByIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTradePortListPool;
        private static DelegateBridge __Hotfix_add_EventOnTradePortListItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnTradePortListItemClick;

        public event Action<int> EventOnTradePortListItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CreateTradePortListPool(int poolSize)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateTradePortListUIProcee(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetFristTradePort()
        {
        }

        [MethodImpl(0x8000)]
        private GuildTradePortListItemUIController GetTradePortListItemByIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradePortItemFill(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradePortListItemClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAllTradePortListItemUnselected()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTradePortListItemSelected(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePurchaseOrderInfo(GuildTradePurchaseInfo guildTradePurchaseInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTradePortExtraInfoList(Dictionary<int, GuildTradePortExtraInfo> guildTradePortExtraInfoDict, Dictionary<string, UnityEngine.Object> assetDic, int selectedIndex = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTradePortTradeInfo(int index, GuildTradePurchaseInfo guildTradePurchaseInfo, GuildTradeTransportInfo guildTradeTransportInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTransportInfo(GuildTradeTransportInfo guildTradeTransportInfo)
        {
        }
    }
}

