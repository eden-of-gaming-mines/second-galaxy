﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class TradeEntryPanelUIController : UIControllerBase
    {
        [AutoBind("./ContentGroup/TradeGuildButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TradeGuildButton;
        [AutoBind("./ContentGroup/TradePersonageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TradePersonageButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateController;
        [AutoBind("./ContentGroup/TradeGuildButton/TradeGuildGroup/GoodsText/GoodsText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildTradePurchaseOrderCountText;
        [AutoBind("./ContentGroup/TradeGuildButton/TradeGuildGroup/TransportationText/TransportationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildTradeTransportOrderCountText;
        private static DelegateBridge __Hotfix_GetUIProcess;
        private static DelegateBridge __Hotfix_UpdateGuildTradeOrderCount;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public UIProcess GetUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildTradeOrderCount(int purchaseOrderCount, int transportOrderCount)
        {
        }
    }
}

