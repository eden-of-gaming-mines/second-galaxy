﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestMenuItemUIController : MenuItemUIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnQuestRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnQuestRewardItem3DTouch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick;
        private LBProcessingQuestBase m_processingQuest;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private List<VirtualBuffDesc> m_virtualBuffDescs;
        private CommonRewardMoneyItemsUIController m_questMoneyRewardCtrl;
        private CommonRewardMoneyItemsUIController m_questExtraMoneyRewardCtrl;
        private CommonUIStateController[] m_shipLimitCtrlList;
        private bool m_isQuestMustCompleteInTime;
        private const string AllowInImageUIState_AllowIn = "AllowIn";
        private const string AllowInImageUIState_NotAllowIn = "NotAllowIn";
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        [AutoBind("./TargetInfo/Info/TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questNameText;
        [AutoBind("./TargetInfo/Info/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questTimeText;
        [AutoBind("./TargetInfo/Info/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        [AutoBind("./TargetInfo/Info/ShipTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_shipTypeImage;
        [AutoBind("./TargetInfo/Info/TextGroup/LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questLevelText;
        [AutoBind("./TargetInfo/Info/TextGroup/LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_questLevelTextStateCtrl;
        [AutoBind("./Detail/MissionDetail/MissionText/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questMissionDetail01;
        [AutoBind("./Detail/MissionDetail/MissionText2/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questMissionDetail02;
        [AutoBind("./Detail/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeFrigate;
        [AutoBind("./Detail/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeDestroyer;
        [AutoBind("./Detail/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCruiser;
        [AutoBind("./Detail/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleCruiser;
        [AutoBind("./Detail/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleShip;
        [AutoBind("./Detail/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeIndustryShip;
        [AutoBind("./Detail/AssignButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_assignButton;
        [AutoBind("./Detail/AssignButton/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_assignRectButton;
        [AutoBind("./Detail/Reward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardObj;
        [AutoBind("./Detail/ExtraReward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_extraRewardObj;
        [AutoBind("./Detail/Reward/TextTitle/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_rewardBonusButton;
        [AutoBind("./Detail/Reward/TextTitle/RewardBonus", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rewardBonusStateCtrl;
        [AutoBind("./Detail/Reward/TextTitle/RewardBonus/BonusText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_rewardBonusText;
        [AutoBind("./Detail/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowInShipTypeButton;
        [AutoBind("./Detail/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossWarningButton;
        [AutoBind("./Detail/LossWarningButton/SafetyButton", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LossWarningPos;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_GetLBQuest;
        private static DelegateBridge __Hotfix_SetEasyPool;
        private static DelegateBridge __Hotfix_SetQuestInfo;
        private static DelegateBridge __Hotfix_SetQuseItemInfo;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetQuestVirtualBuff;
        private static DelegateBridge __Hotfix_GetLossWarningButtonPos;
        private static DelegateBridge __Hotfix_GetLossWarningButtonSize;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_OnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_SetRewardBouus;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_get_ShipLimitCtrlList;

        public event Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnQuestRewardItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnQuestRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public LBProcessingQuestBase GetLBQuest()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetLossWarningButtonPos()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector2 GetLossWarningButtonSize()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public List<VirtualBuffDesc> GetQuestVirtualBuff()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardItem3DTouch(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEasyPool(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public void SetQuestInfo(LBProcessingQuestBase questInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetQuseItemInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void SetRewardBouus()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        private CommonUIStateController[] ShipLimitCtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

