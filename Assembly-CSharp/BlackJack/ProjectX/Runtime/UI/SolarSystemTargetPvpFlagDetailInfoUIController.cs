﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemTargetPvpFlagDetailInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPvpStateCoolDownTimeOut;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLimitUseStarGateStateCoolDownTimeOut;
        protected string m_currCrimeState;
        protected bool m_isPvp;
        protected DateTime? m_crimeStateOutTime;
        protected DateTime? m_pvpStateOutTime;
        protected DateTime? m_limitUseStarGateOutTime;
        protected float m_updateDelayLeftTime;
        protected const float UpdateDelayTime = 0.5f;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BackGroundButton;
        [AutoBind("./InfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject InfoPanelRoot;
        [AutoBind("./InfoPanel/FrameImage/CrimeState", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CrimeStateItemRoot;
        [AutoBind("./InfoPanel/FrameImage/CrimeState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrimeStateUIStateCtrl;
        [AutoBind("./InfoPanel/FrameImage/CrimeState/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrimeStateTitleText;
        [AutoBind("./InfoPanel/FrameImage/CrimeState/Desc", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrimeStateDescText;
        [AutoBind("./InfoPanel/FrameImage/CrimeState/Title/OutTime/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrimeStateOutTimeText;
        [AutoBind("./InfoPanel/FrameImage/CrimeState/CrimeFlag/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image CrimeStateFlagIconImage;
        [AutoBind("./InfoPanel/FrameImage/LimitUseStarGate", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LimitUseStarGateRoot;
        [AutoBind("./InfoPanel/FrameImage/LimitUseStarGate/Title/OutTime/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LimitUseStarGateOutTimeText;
        [AutoBind("./InfoPanel/FrameImage/LimitLeavePvpState", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PVPStateItemRoot;
        [AutoBind("./InfoPanel/FrameImage/LimitLeavePvpState/Title/OutTime/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PVPOutTimeText;
        [AutoBind("./InfoPanel/FrameImage/TransportationState", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_smuggleInfoGo;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_SetPanelPos;
        private static DelegateBridge __Hotfix_UpdateTargetPvpFlagInfo;
        private static DelegateBridge __Hotfix_ClearOnPause;
        private static DelegateBridge __Hotfix_CreateMainWindownEffectInfo;
        private static DelegateBridge __Hotfix_GetCrimeTitleByCrimeState;
        private static DelegateBridge __Hotfix_GetCrimeDescByCrimeState;
        private static DelegateBridge __Hotfix_GetCrimeIconPathByCrimeState;
        private static DelegateBridge __Hotfix_FormatCoolDownTimeString;
        private static DelegateBridge __Hotfix_add_EventOnPvpStateCoolDownTimeOut;
        private static DelegateBridge __Hotfix_remove_EventOnPvpStateCoolDownTimeOut;
        private static DelegateBridge __Hotfix_add_EventOnLimitUseStarGateStateCoolDownTimeOut;
        private static DelegateBridge __Hotfix_remove_EventOnLimitUseStarGateStateCoolDownTimeOut;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action EventOnLimitUseStarGateStateCoolDownTimeOut
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPvpStateCoolDownTimeOut
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearOnPause()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateMainWindownEffectInfo(bool isShow = true)
        {
        }

        [MethodImpl(0x8000)]
        protected string FormatCoolDownTimeString(TimeSpan time)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetCrimeDescByCrimeState(string state)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetCrimeIconPathByCrimeState(string state)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetCrimeTitleByCrimeState(string state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPos(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTargetPvpFlagInfo(string crimeState, DateTime? crimeOutTime, bool isPvp, DateTime? pvpOutTime, DateTime? limitUseStarGateOutTime, bool isSmuggler, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

