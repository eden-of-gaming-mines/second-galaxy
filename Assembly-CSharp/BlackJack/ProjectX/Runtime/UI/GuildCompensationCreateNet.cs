﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildCompensationCreateNet : NetWorkTransactionTask
    {
        private readonly ulong m_instanceId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private GuildCompensationCreateAck <Ack>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnAck;
        private static DelegateBridge __Hotfix_set_Ack;
        private static DelegateBridge __Hotfix_get_Ack;

        [MethodImpl(0x8000)]
        public GuildCompensationCreateNet(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAck(GuildCompensationCreateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public GuildCompensationCreateAck Ack
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

