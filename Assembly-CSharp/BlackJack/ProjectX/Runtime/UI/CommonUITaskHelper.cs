﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonUITaskHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_StartCommonUITasksWhenEnterGame;
        private static DelegateBridge __Hotfix_StopConflictUITask4Login;
        private static DelegateBridge __Hotfix_ClearUIActionWindowItem;
        private static DelegateBridge __Hotfix_ClearFullScreenAction;
        private static DelegateBridge __Hotfix_UnLockUIBlock;
        private static DelegateBridge __Hotfix_StopCommonUITasks4Login;
        private static DelegateBridge __Hotfix_ReturnToBasicUITask;
        private static DelegateBridge __Hotfix_ToAddCurrency;
        private static DelegateBridge __Hotfix_ApplyUserSettings;

        [MethodImpl(0x8000)]
        public static void ApplyUserSettings()
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearFullScreenAction()
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearUIActionWindowItem()
        {
        }

        [MethodImpl(0x8000)]
        public static void ReturnToBasicUITask(Action<bool> onEnd = null, bool clearProcess = true)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartCommonUITasksWhenEnterGame()
        {
        }

        [MethodImpl(0x8000)]
        public static void StopCommonUITasks4Login()
        {
        }

        [MethodImpl(0x8000)]
        public static void StopConflictUITask4Login()
        {
        }

        [MethodImpl(0x8000)]
        public static void ToAddCurrency(CurrencyType type, UIIntent intent, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void UnLockUIBlock()
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

