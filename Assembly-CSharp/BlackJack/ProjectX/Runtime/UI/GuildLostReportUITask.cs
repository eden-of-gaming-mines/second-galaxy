﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildLostReportUITask : GuildUITaskBase
    {
        private BuildingLostReport m_buildingLostReport;
        private int m_buildingLevel;
        private SovereignLostReport m_sovereignLostReport;
        private FlagShipLostReport m_flagShipLostReport;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildLostReportUITask> EventOnBeforeReturnPreTask;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private GuildLostReportUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKeyLostReportId = "LostReportId";
        public const string ParamKeyLostReportInfo = "LostReportInfo";
        public const string ParamKeyLostBuildingLevel = "LostBuildingLevel";
        public const string TaskName = "GuildLostReportUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartSovereignLostReportUITask;
        private static DelegateBridge __Hotfix_StartBuildingLostReportUITask_0;
        private static DelegateBridge __Hotfix_StartBuildingLostReportUITask_1;
        private static DelegateBridge __Hotfix_StartFlagShipLostReportUITask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnGuildLeaveNtf;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoUIPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoUIPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBeforeReturnPreTask;
        private static DelegateBridge __Hotfix_remove_EventOnBeforeReturnPreTask;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbGuild;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<GuildLostReportUITask> EventOnBeforeReturnPreTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildLostReportUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoUIPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLeaveNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoUIPanel(ItemInfo itemInfo, GuildLostItemUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildLostReportUITask StartBuildingLostReportUITask(UIIntent returnToIntent, BuildingLostReport reportInfo, int buildingLevel)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartBuildingLostReportUITask(UIIntent returnToIntent, ulong reportId, int buildingLevel, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildLostReportUITask StartFlagShipLostReportUITask(UIIntent returnToIntent, IStaticFlagShipDataContainer ship)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildLostReportUITask StartSovereignLostReportUITask(UIIntent returnToIntent, SovereignLostReport reportInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockGuildClient LbGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
            internal UIIntent intent;
            internal ulong lossReportId;
            internal GuildLostReportUITask $this;

            internal void <>m__0(Task task)
            {
                LostReportGetReqNetTask task2 = task as LostReportGetReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    this.onPrepareEnd(false);
                }
                else if (task2.Result == 0)
                {
                    ((UIIntentCustom) this.intent).SetParam("LostReportInfo", this.$this.LbGuild.GetBuildingLostReport(this.lossReportId));
                    this.onPrepareEnd(true);
                }
                else
                {
                    this.onPrepareEnd(false);
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartFlagShipLostReportUITask>c__AnonStorey0
        {
            internal ShipSlotGroupInfo item;

            internal bool <>m__0(CostInfo old)
            {
                int num1;
                if ((old.CostType != CostType.CostType_Item) || (old.P1 != this.item.m_itemType))
                {
                    num1 = 0;
                }
                else
                {
                    num1 = (int) (old.P2 == this.item.m_configId);
                }
                return (bool) num1;
            }
        }

        public class FlagShipLostReport
        {
            public ulong m_instanceId;
            public List<CostInfo> m_lostItemList;
            public DateTime m_generateTime;
            public string m_shipName;
            public string m_iconPath;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

