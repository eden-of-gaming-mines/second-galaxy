﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ChatItemtInSpaceUIController : UIControllerBase
    {
        [AutoBind("./ChannelTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ChatText;
        private const string ContentFormat = "{0}:{1}";
        private static DelegateBridge __Hotfix_UpdateItemInfo;

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(ChatChannel chatChannel, ChatInfo chatInfo, EmojiParseController emojiHelper)
        {
        }
    }
}

