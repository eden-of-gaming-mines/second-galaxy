﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CrackStoreUIController : UIControllerBase
    {
        public ItemStoreListUIController m_crackBoxListCtrl;
        private ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        private ILBStoreItemClient m_currSelectedItem;
        private const string SortState_Selected = "Selected";
        private const string SortState_UnSelected = "UnSelected";
        private const string SortOrder_Up = "SortUp";
        private const string SortOrder_Down = "SortDown";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCrackBoxItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBStoreItemClient> EventOnAddCrackBoxClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBStoreItemClient> EventOnShareButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<BoxSortState> EventOnCrackBoxSort;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StorePanelStateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CloseButton;
        [AutoBind("./ItemListDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemStoreListDummy;
        [AutoBind("./ItemsimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemsimpleInfoDummy;
        [AutoBind("./SortPanel/GetTime", AutoBindAttribute.InitState.NotInit, false)]
        public Button SortTypeCommon;
        [AutoBind("./SortPanel/GetTime", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortTypeCommonSelectedCtrl;
        [AutoBind("./SortPanel/GetTime/Arrow", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortTypeCommonOrderCtrl;
        [AutoBind("./SortPanel/Size", AutoBindAttribute.InitState.NotInit, false)]
        public Button SortTypeSize;
        [AutoBind("./SortPanel/Size", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortTypeSizeSelectedCtrl;
        [AutoBind("./SortPanel/Size/Arrow", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortTypeSizeOrderCtrl;
        [AutoBind("./SortPanel/Rank", AutoBindAttribute.InitState.NotInit, false)]
        public Button SortTypeRank;
        [AutoBind("./SortPanel/Rank", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortTypeRankSelectedCtrl;
        [AutoBind("./SortPanel/Rank/Arrow", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortTypeRankOrderCtrl;
        [AutoBind("./EmptyImageGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_emptyImageStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateCrackBoxList;
        private static DelegateBridge __Hotfix_UpdateSelectedItemInfo;
        private static DelegateBridge __Hotfix_GetStartIndex;
        private static DelegateBridge __Hotfix_SetSortButtonSlected;
        private static DelegateBridge __Hotfix_GetAddToListButtonRect;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_OnCrackBoxItemClick;
        private static DelegateBridge __Hotfix_OnSortTypeSizeButtonClick;
        private static DelegateBridge __Hotfix_OnSortTypeCommonButtonClick;
        private static DelegateBridge __Hotfix_OnSortTypeRankButtonClick;
        private static DelegateBridge __Hotfix_OnAddToCrackListButtonClick;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCrackBoxItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCrackBoxItemClick;
        private static DelegateBridge __Hotfix_add_EventOnAddCrackBoxClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddCrackBoxClick;
        private static DelegateBridge __Hotfix_add_EventOnShareButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShareButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCrackBoxSort;
        private static DelegateBridge __Hotfix_remove_EventOnCrackBoxSort;

        public event Action<ILBStoreItemClient> EventOnAddCrackBoxClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCrackBoxItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<BoxSortState> EventOnCrackBoxSort
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient> EventOnShareButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetAddToListButtonRect()
        {
        }

        [MethodImpl(0x8000)]
        public int GetStartIndex()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddToCrackListButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackBoxItemClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeCommonButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeRankButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeSizeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetSortButtonSlected(BoxSortState state)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCrackBoxList(List<ILBStoreItemClient> itemList, Dictionary<string, UnityEngine.Object> resDict, int startIndex = 0, ILBStoreItemClient item = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectedItemInfo(List<ILBStoreItemClient> itemList, Dictionary<string, UnityEngine.Object> resDict, int selectedIndex)
        {
        }

        public enum BoxSortState
        {
            Common,
            Rank,
            Size
        }
    }
}

