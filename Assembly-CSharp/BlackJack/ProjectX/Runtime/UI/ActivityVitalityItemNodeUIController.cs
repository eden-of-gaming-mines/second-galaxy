﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ActivityVitalityItemNodeUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ConfigDataVitalityRewardInfo, int, Vector3> EventOnRewardNodeButtonClick;
        protected ConfigDataVitalityRewardInfo m_rewardInfo;
        protected int m_rewardIndex;
        protected ProjectXPlayerContext m_plyCtx;
        public const string NodeMode_Received = "Received";
        public const string NodeMode_CanReceive = "CanReceive";
        public const string NodeMode_CanNotReceive = "CanNotReceive";
        [AutoBind("./ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoDummy;
        [AutoBind("./RewardIcon/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ItemIconImage;
        [AutoBind("./RewardIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemCountText;
        [AutoBind("./VitalityNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text VitalityNumberText;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NodeButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NodeUICtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateRewardNodeInfo;
        private static DelegateBridge __Hotfix_OnRewardNodeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardNodeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardNodeButtonClick;

        public event Action<ConfigDataVitalityRewardInfo, int, Vector3> EventOnRewardNodeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRewardNodeButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardNodeInfo(ConfigDataVitalityRewardInfo rewardInfo, int vitalityCount, int rewardIndex, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

