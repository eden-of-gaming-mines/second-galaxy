﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AuctionItemTypeListReqNetTask : NetWorkTransactionTask
    {
        protected int m_itemType;
        protected AuctionItemTypeListAck m_ack;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAuctionItemTypeList;
        private static DelegateBridge __Hotfix_get_Ack;

        [MethodImpl(0x8000)]
        public AuctionItemTypeListReqNetTask(NpcShopItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAuctionItemTypeList(AuctionItemTypeListAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public AuctionItemTypeListAck Ack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

