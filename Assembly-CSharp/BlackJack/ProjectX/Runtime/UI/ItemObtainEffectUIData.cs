﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using System;

    public class ItemObtainEffectUIData : UIControllerBase
    {
        public float m_fullClearTime_InSpace;
        public float m_fullClearTime_InStation;
        public float m_showItemIntervalTime;
    }
}

