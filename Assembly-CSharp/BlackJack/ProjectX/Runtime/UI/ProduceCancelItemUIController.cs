﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ProduceCancelItemUIController : UIControllerBase
    {
        public CostInfo m_currCostInfo;
        public bool m_isInitComplete;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        public CommonItemIconUIController m_commonItemCtrl;
        [AutoBind("./CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonItemDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_RegItemClickEvent;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;

        private event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(CostInfo costInfo, float returnFactor, bool isGuildMode, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

