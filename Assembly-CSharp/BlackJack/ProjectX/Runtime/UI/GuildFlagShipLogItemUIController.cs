﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildFlagShipLogItemUIController : UIControllerBase
    {
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollItemCtrl;
        [AutoBind("./ContentGroup/ContentText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_contentText;
        [AutoBind("./ContentGroup/FixationGroup/PlaceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_placeText;
        [AutoBind("./ContentGroup/FixationGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeText;
        [AutoBind("./ContentGroup/FixationGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_logTypeNameText;
        [AutoBind("./ContentGroup/FixationGroup/TitleImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_logTypeBgStateCtrl;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateViewOnLogItem;
        private static DelegateBridge __Hotfix_GetItemColorStateByLogType;

        [MethodImpl(0x8000)]
        private static string GetItemColorStateByLogType(GuildFlagShipOptLogType type)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewOnLogItem(GuildFlagShipOptLogInfo logInfo)
        {
        }
    }
}

