﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SolarSystemDropBoxTargetTagUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tagCtrl;
        private static DelegateBridge __Hotfix_UpdateDropBoxTagInfo;

        [MethodImpl(0x8000)]
        public void UpdateDropBoxTagInfo(Vector3 pos, SubRankType subRank, bool isVisible)
        {
        }
    }
}

