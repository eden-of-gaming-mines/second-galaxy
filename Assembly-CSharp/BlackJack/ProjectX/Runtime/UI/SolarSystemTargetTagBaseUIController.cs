﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemTargetTagBaseUIController : PrefabControllerBase
    {
        protected SpaceObjectType m_currSpaceObjectType;
        protected string m_currHudSubscriptStateState;
        protected string m_currHudColorStateState;
        protected Sprite m_curSprite;
        protected double m_distance;
        protected DateTime m_updateDistanceDateTime;
        protected const float UpdateDistaceDelayTime = 1f;
        public const string m_HudSubscriptState_Team = "Team";
        public const string m_HudSubscriptState_Ship = "Ship";
        public const string m_HudSubscriptState_Empty = "Empty";
        public const string m_HudColorState_Enemy = "Enemy";
        public const string m_HudColorState_Neutral = "Neutral";
        public const string m_HudColorState_NeutralFriend = "NeutralFriend";
        public const string m_HudColorState_Friend = "Friend";
        public const string m_HudColorState_Nomal = "Nomal";
        private string m_hudColorState;
        [AutoBind("./TargetBuildingImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TargetBuildingIconStateCtrl;
        [AutoBind("./TargetCelestialImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TargetCelestialIconStateCtrl;
        [AutoBind("./TargetGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetGroupStateCtrl;
        [AutoBind("./TargetGroup/TargetImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetImageStateCtrl;
        [AutoBind("./TargetGroup/TargetImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_targetImage;
        [AutoBind("./TargetGroup/TeamImage-", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageSubtractionSignCtrl;
        [AutoBind("./TargetGroup/ShipImage=", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageequalSignCtrl;
        [AutoBind("./TargetGroup/CaptainImage+", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_teamImageAddSignCtrl;
        private static DelegateBridge __Hotfix_SetTargetIconStyle;
        private static DelegateBridge __Hotfix_SetHudIcon;
        private static DelegateBridge __Hotfix_SetTargetNoticeType;
        private static DelegateBridge __Hotfix_SetTargetPvpFlagState;
        private static DelegateBridge __Hotfix_SetHudColorState;
        private static DelegateBridge __Hotfix_SetBuildingCelestialHudIcon;
        private static DelegateBridge __Hotfix_IsNeedUpdateDistance;
        private static DelegateBridge __Hotfix_GetTargetNoticeStateCtrl;
        private static DelegateBridge __Hotfix_GetTargetPvpFlagStateCtrl;

        [MethodImpl(0x8000)]
        protected virtual CommonUIStateController GetTargetNoticeStateCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual CommonUIStateController GetTargetPvpFlagStateCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsNeedUpdateDistance()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuildingCelestialHudIcon(bool isBuilding, string targetState, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetHudColorState(string HudColorState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHudIcon(string HudSubscriptState, string HudColorState, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetIconStyle(SpaceObjectType objType, bool isFakeBuildingNpc, string HudSubscriptState, string HudColorState, Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetNoticeType(string targetNoticeState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetPvpFlagState(string stateName)
        {
        }
    }
}

