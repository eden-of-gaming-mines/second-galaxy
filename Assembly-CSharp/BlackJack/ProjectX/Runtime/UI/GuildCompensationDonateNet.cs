﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildCompensationDonateNet : NetWorkTransactionTask
    {
        private readonly ulong m_instanceId;
        private readonly int m_index;
        private readonly LossCompensation m_loss;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private GuildCompensationDonateAck <Ack>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnAck;
        private static DelegateBridge __Hotfix_set_Ack;
        private static DelegateBridge __Hotfix_get_Ack;

        [MethodImpl(0x8000)]
        public GuildCompensationDonateNet(ulong instanceId, int index, LossCompensation loss)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAck(GuildCompensationDonateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public GuildCompensationDonateAck Ack
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

