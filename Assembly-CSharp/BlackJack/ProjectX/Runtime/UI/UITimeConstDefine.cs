﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;

    public class UITimeConstDefine
    {
        public static int MilliSecondToSecond;
        public static int SecondToMinute;
        public static int MinuteToHour;
        public static int HourToDay;
        public static int DayToYear;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

