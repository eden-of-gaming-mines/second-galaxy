﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ActivityDetailInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ActivityInfo> EventOnGoImmediatelyButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ItemDropInfoUIShowItemList, int> EventOnDropItemClick;
        protected ActivityInfo m_currActivityInfo;
        protected GameObject m_itemObj;
        protected List<ItemDropInfoUIShowItemList> m_rewardItemList;
        protected const string ItemAssetName = "CommonItemUIPrefab";
        protected const int MaxRewardItemCount = 5;
        protected const string Mode_DailyOpen = "DailyOpen";
        protected const string Mode_DailyNotStart = "DailyNotStart";
        protected const string Mode_TimeLimitNotStart = "TimeLimitNotStart";
        protected const string Mode_TimeLimitOpen = "TimeLimitOpen";
        protected const string Mode_TimeLimitNotOpen = "TimeLimitNotOpen";
        public List<CommonItemIconUIController> m_itemIconUICtrlList;
        public ProjectXPlayerContext m_plyCtx;
        [AutoBind("./TopImagePanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./TopImagePanel/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DetailText;
        [AutoBind("./DownRewardPanel/VitalityText/VitalityNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text VitalityNumberText;
        [AutoBind("./DownRewardPanel/ItemSimpleInfoDummy0", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleDummy0;
        [AutoBind("./DownRewardPanel/ItemSimpleInfoDummy1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleDummy1;
        [AutoBind("./DownRewardPanel/NeedEnergyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NeedActivityPointNumberText;
        [AutoBind("./TopImagePanel/ActivityImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ActivityImage;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtrl;
        [AutoBind("./TopImagePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./DownRewardPanel/ItemGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind("./DownRewardPanel/GoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GoButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetActivityDetailPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_UpdateActivityDetailInfo;
        private static DelegateBridge __Hotfix_GetSimpleInfoPosition;
        private static DelegateBridge __Hotfix_UpdateActivityRewardInfo;
        private static DelegateBridge __Hotfix_UpdateActivityBigImage;
        private static DelegateBridge __Hotfix_UpdateCostActivityPoint;
        private static DelegateBridge __Hotfix_UpdateTitleAndDescInfo;
        private static DelegateBridge __Hotfix_UpateDetailShowState;
        private static DelegateBridge __Hotfix_UpdateIncreasedVitality;
        private static DelegateBridge __Hotfix_OnGoImmediatelyButtonClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnGoImmediatelyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGoImmediatelyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDropItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDropItemClick;

        public event Action<ItemDropInfoUIShowItemList, int> EventOnDropItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ActivityInfo> EventOnGoImmediatelyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetActivityDetailPanelShowOrHideProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        public void GetSimpleInfoPosition(int itemIndex, out Vector3 pos, out ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGoImmediatelyButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpateDetailShowState(ActivityInfo activityInfo, ConfigDataActivityInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateActivityBigImage(string subPath, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateActivityDetailInfo(ActivityInfo activityInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateActivityRewardInfo(List<ItemDropInfoUIShowItemList> rewardItemList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCostActivityPoint(int pointNum)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateIncreasedVitality(int num)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateTitleAndDescInfo(string nameKey, string descKey)
        {
        }
    }
}

