﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipHangarBGUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLongPressd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLongPressReleased;
        private ShipModelOverviewUIController m_mainCtrl;
        protected bool m_isSelectShipChanged;
        protected ILBStaticPlayerShip m_currSelectShip;
        protected int m_updateWeaponGroupIndex;
        protected string m_currShipResPath;
        public const string ShipHangarMode = "ShipHangarShipMode";
        public const string UpdateWeaponGroup = "UpdateWeaponGroup";
        public const string CaptainManageMode = "CaptainManageMode";
        public const string CaptainShipManageMode = "CaptainShipManageMode";
        public static string ParamKey_SelectHangarShip;
        public static string ParamKey_SelectShipResPath;
        public static string ParamKey_UpdateGroupIndex;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ShipHangarBGUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ClearShipModel;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadFromShipList;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitLayerStateOnUpdateView;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnLongPressed;
        private static DelegateBridge __Hotfix_OnLongPressRelease;
        private static DelegateBridge __Hotfix_PauseShipHangarBgTask;
        private static DelegateBridge __Hotfix_ResumeShipHangarBgTask;
        private static DelegateBridge __Hotfix_SetCurrSelectHangarShip;
        private static DelegateBridge __Hotfix_UpdateCurrSelectHangarShipWeaponGroup;
        private static DelegateBridge __Hotfix_UpdateCaptainManageBGShip;
        private static DelegateBridge __Hotfix_UpdateCaptainShipManageBGShip;
        private static DelegateBridge __Hotfix_BlockInput;
        private static DelegateBridge __Hotfix_ResetShipModelView;
        private static DelegateBridge __Hotfix_GetCurrShipFromUIIntent;
        private static DelegateBridge __Hotfix_GetShipResPathFromIntent;
        private static DelegateBridge __Hotfix_GetShipModelAssetByShip;
        private static DelegateBridge __Hotfix_GetShipModelAssetByResPath;
        private static DelegateBridge __Hotfix_GetWeaponAssetByResPath;
        private static DelegateBridge __Hotfix_PrepareShipViewModel;
        private static DelegateBridge __Hotfix_GetWeaponEquipArtResource;
        private static DelegateBridge __Hotfix_add_EventOnLongPressd;
        private static DelegateBridge __Hotfix_remove_EventOnLongPressd;
        private static DelegateBridge __Hotfix_add_EventOnLongPressReleased;
        private static DelegateBridge __Hotfix_remove_EventOnLongPressReleased;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnLongPressd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLongPressReleased
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public ShipHangarBGUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BlockInput(bool islock)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearShipModel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private List<string> CollectDynamicResForLoadFromShipList()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected ILBStaticPlayerShip GetCurrShipFromUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetShipModelAssetByResPath(string shipResPath)
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetShipModelAssetByShip(ILBStaticPlayerShip lbShip)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetShipResPathFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetWeaponAssetByResPath(string resPath)
        {
        }

        [MethodImpl(0x8000)]
        private string GetWeaponEquipArtResource(LBStaticWeaponEquipSlotGroup lbStaticGroup, GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLayerStateOnUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressed()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressRelease()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static void PauseShipHangarBgTask()
        {
        }

        [MethodImpl(0x8000)]
        private KeyValuePair<float, Bounds> PrepareShipViewModel(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetShipModelView()
        {
        }

        [MethodImpl(0x8000)]
        public static void ResumeShipHangarBgTask()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrSelectHangarShip(ILBStaticPlayerShip ship, bool needResetRotation = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainManageBGShip(string shipResPath)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainShipManageBGShip(string shipResPath = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrSelectHangarShipWeaponGroup(int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsNeedResetRotation,
            IsStartOrResume
        }
    }
}

