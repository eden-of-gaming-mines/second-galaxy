﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ReturnToMotherShipUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCancelButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_mainCtrl;
        [AutoBind("./Detail/InfoGroup/TargetSpaceStationNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_targetSpaceStationNameText;
        [AutoBind("./Detail/InfoGroup/JumpDistanceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_jumpDistanceText;
        [AutoBind("./Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetDestSpaceStationInfoToUI;
        private static DelegateBridge __Hotfix_CreateUIShowOrHideProcess;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelButtonClick;

        public event Action EventOnCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess CreateUIShowOrHideProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDestSpaceStationInfoToUI(string spaceStationName, int jumpCount, bool enterBase)
        {
        }
    }
}

