﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CommonBackAndCloseUITask : UITaskBase
    {
        protected Vector3 m_cachedPosition;
        private CommonBackAndCloseButtonUIController m_mainCtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnBackButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCloseButtonClick;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public static string ShowLongAnnoucementBarMode;
        public static string ShowShortAnnoucementBarMode;
        protected bool m_currUIShowState;
        private const string INTENT_PARAM_INITSHOWSTATE = "InitialShowState";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartByParent;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_StartShowOrHideCommonBackAndCloseUI;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_CurrUIShowState;
        private static DelegateBridge __Hotfix_set_CurrUIShowState;

        public event Action<UIControllerBase> EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonBackAndCloseUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public static CommonBackAndCloseUITask StartByParent(Action<UIControllerBase> backButtonClick, Action<UIControllerBase> closeButtonClick, string taskMode = "", Action OnTaskResLoadFinished = null, bool initShowState = true)
        {
        }

        [MethodImpl(0x8000)]
        public void StartShowOrHideCommonBackAndCloseUI(Action onShowCommonBackAndCloseUIEnd, bool isShowCommonBackAndCloseUIState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool CurrUIShowState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

