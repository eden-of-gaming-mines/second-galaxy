﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CharacterKillRecordDetailInfoUITask : UITaskBase
    {
        public FakeLBStoreItem m_currFakeItem;
        public ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        public KillRecordInfo m_currKillRecord;
        public UIIntent m_returnToUIIntent;
        public CharacterKillRecordDetailInfoUICtrl m_mainCtrl;
        protected bool m_isFromShipSalavgeUI;
        protected bool m_showCompensationInfo;
        protected ProjectXPlayerContext m_playerCtx;
        public const string KillRecordInfoKey = "KillRecordInfo";
        public const string KillRecordShowCompensationInfoKey = "ShowCompensationCompensationInfo";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CharacterKillRecordDetailInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCharacterKillRecordDetailInfoUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_OnScrollViewClick;
        private static DelegateBridge __Hotfix_OnSendLinkButtonClick;
        private static DelegateBridge __Hotfix_OnWinnerShipIconButtonClick;
        private static DelegateBridge __Hotfix_OnLosserShipIconButtonClick;
        private static DelegateBridge __Hotfix_OnWinnerCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_OnLosserCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_OnLostItemClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnIssueButtonClick;
        private static DelegateBridge __Hotfix_PushLayer;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnKillRecordCompensationUpdateNtf;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CharacterKillRecordDetailInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueButtonClick(KillRecordInfo record)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillRecordCompensationUpdateNtf(KillRecordCompensationUpdateNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLosserCaptainIconButtonClick(UIControllerBase uCtrl, KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLosserShipIconButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLostItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSendLinkButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWinnerCaptainIconButtonClick(UIControllerBase uCtrl, KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWinnerShipIconButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void PushLayer()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public static CharacterKillRecordDetailInfoUITask StartCharacterKillRecordDetailInfoUITask(UIIntent returnIntent, KillRecordInfo recordInfo, bool isShowCompensationInfoKey = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnIssueButtonClick>c__AnonStorey0
        {
            internal KillRecordInfo record;
            internal ProjectXPlayerContext ctx;
            internal CharacterKillRecordDetailInfoUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(UIProcess p, bool r)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__2(Task task)
            {
            }
        }
    }
}

