﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ProduceItemStoreTypeToggleUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnSkillTypeToggleValueChanged;
        public int m_type;
        private bool m_isTriggerEvent = true;
        private const string NormalMode = "Normal";
        private const string NoMode = "NoBlueprint";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TypeToggle;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetToNormalMode;
        private static DelegateBridge __Hotfix_SetToNoBlueprintMode;
        private static DelegateBridge __Hotfix_SetType;
        private static DelegateBridge __Hotfix_SetTypeNameText;
        private static DelegateBridge __Hotfix_RegTypeToggleEvent;
        private static DelegateBridge __Hotfix_SetToggleState;
        private static DelegateBridge __Hotfix_OnTypeToggleValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnSkillTypeToggleValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSkillTypeToggleValueChanged;

        public event Action<int> EventOnSkillTypeToggleValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnTypeToggleValueChanged(UIControllerBase uiCtrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void RegTypeToggleEvent(Action<int> onSkillToggleSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleState(bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToNoBlueprintMode()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToNormalMode()
        {
        }

        [MethodImpl(0x8000)]
        public void SetType(int type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTypeNameText(string skillName)
        {
        }
    }
}

