﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TechUpgradeInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CurrencyType> EventOnCurrencyAddButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TechCostItemUIController> EventOnGetItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TechCostItemUIController> EventOnItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCaptainIconClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGoToPreTechButton;
        private List<TechCostItemUIController> m_upgradeCostItemCtrlList;
        private CommonCaptainIconUIController m_captainIconCtrl;
        private bool m_isAllConditionCheckOk;
        private int m_dependTechId;
        private List<TechUpgradeInfoLevelUpEffectItemUIController> m_levelUpEffecItemCtrlList;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./UpgradePanel/Buttons/UpgradeImmediatelyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeImmediatelyButton;
        [AutoBind("./UpgradePanel/Buttons/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeButton;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/CaptainPanel/DeleteCaptainPanel/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DeleteButton;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/SkillBGImage/TechIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TechIconImage;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/PreconditionTechLevel/Icon", AutoBindAttribute.InitState.NotInit, false)]
        public Image DependTechIconImage;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/TechNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechNameText;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/LevelUpGroup/CurrentLvTextGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrentLevelText;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/LevelUpGroup/NextLvTextGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextLeveltext;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/EffectUpGroup/EffectUpItem/TechPropertyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechPropNameText;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/EffectUpGroup/EffectUpItem/EffectUpGroup/CurrentValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrentTechPropValueText;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/EffectUpGroup/EffectUpItem/EffectUpGroup/NextValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextTechPropValueText;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/PreconditionTechLevel/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GoToPreTechButton;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/CommanderLevelCondition", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CommanderLevelConditionStateCtrl;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/CommanderLevelCondition/CurrentLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CommanderLevelConditionText;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/PreconditionTechLevel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PreconditionLevelRoot;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/PreconditionTechLevel/PreTechName", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreconditionTechNameText;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/PreconditionTechLevel/Icon", AutoBindAttribute.InitState.NotInit, false)]
        public Image PreconfitionTechIcon;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/PreconditionTechLevel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PreconditionTechLvStateCtrl;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/PreconditionGroup/PreconditionTechLevel/LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PreconditionLevelText;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/ConsumeResourceRoot/ConsumeResourceGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ResourceItemGroup;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/CaptainPanel/AddCaptainPanel/AddCaptain", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ChooseCaptainButton;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/CaptainPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainPanelStateCtrl;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/CaptainPanel/DeleteCaptainPanel/CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainIconDummy;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/CaptainPanel/DeleteCaptainPanel/Name", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/CaptainPanel/DeleteCaptainPanel/ChooseCaptainButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ChooseCaptainButton2;
        [AutoBind("./UpgradePanel/Buttons/UpgradeImmediatelyButton/MoneyNumberGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpgradeImmediatelyCostText;
        [AutoBind("./UpgradePanel/Buttons/UpgradeButton/TimerNumberGroup/TimerNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpgradeTimeText;
        [AutoBind("./UpgradePanel/Buttons/UpgradeButton/TimerNumberGroup/ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TimeCpationEffectImage;
        [AutoBind("./UpgradePanel/Buttons/UpgradeImmediatelyButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpgradeImmediateButtonStateCtrl;
        [AutoBind("./UpgradePanel/Buttons/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpgradeNormalButtonStateCtrl;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/EffectUpGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UpgradeEffectRoot;
        [AutoBind("./UpgradePanel/TechItemInfoGroup/EffectUpGroup/EffectUpItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject EffectItem;
        [AutoBind("./MoneyGroup/CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BindMoneyText;
        [AutoBind("./MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeMoneyText;
        [AutoBind("./MoneyGroup/IRMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RealMoneyText;
        [AutoBind("./MoneyGroup/CMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BindMoneyAddButton;
        [AutoBind("./MoneyGroup/SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TradeMoneyAddButton;
        [AutoBind("./MoneyGroup/IRMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RealMoneyAddButton;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/MonthGroup/MonthCardRemind", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_monthCardPriviledgeStateCtrl;
        [AutoBind("./UpgradePanel/UpgradePreparePanel/MonthGroup/MonthCardRemind", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_monthCardPriviledgeButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetTechUpgradInfo;
        private static DelegateBridge __Hotfix_SetOtherTechUpgradeInfo;
        private static DelegateBridge __Hotfix_SetMonthCardPriviledgeState;
        private static DelegateBridge __Hotfix_PlaySpeedUpEffect;
        private static DelegateBridge __Hotfix_UpdateUpgradeEffectPropertyInfo;
        private static DelegateBridge __Hotfix_UpdateTechLevelUpEffectItemList;
        private static DelegateBridge __Hotfix_GetPropertyInfoList4TechLevelInfo;
        private static DelegateBridge __Hotfix_GetCostItemUICtrl;
        private static DelegateBridge __Hotfix_GetCaptainIconCtrl;
        private static DelegateBridge __Hotfix_OnBindMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnTradeMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnRealMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeGetItemClick;
        private static DelegateBridge __Hotfix_OnUpgradeItemClick;
        private static DelegateBridge __Hotfix_OnCaptainIconClick;
        private static DelegateBridge __Hotfix_OnGoToPreTechButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnCurrencyAddButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCurrencyAddButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGetItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnGetItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainIconClick;
        private static DelegateBridge __Hotfix_add_EventOnGoToPreTechButton;
        private static DelegateBridge __Hotfix_remove_EventOnGoToPreTechButton;

        public event Action EventOnCaptainIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CurrencyType> EventOnCurrencyAddButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TechCostItemUIController> EventOnGetItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGoToPreTechButton
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TechCostItemUIController> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private CommonCaptainIconUIController GetCaptainIconCtrl()
        {
        }

        [MethodImpl(0x8000)]
        private TechCostItemUIController GetCostItemUICtrl(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private List<CommonPropertyInfo> GetPropertyInfoList4TechLevelInfo(ConfigDataTechLevelInfo techLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoToPreTechButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRealMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeGetItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlaySpeedUpEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMonthCardPriviledgeState(string state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOtherTechUpgradeInfo(int techId, ulong captainInsId, bool hasCanUsedCaptain, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTechUpgradInfo(int techId, ulong captainInsId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTechLevelUpEffectItemList(List<CommonPropertyInfo> currLevelPropertyList, List<CommonPropertyInfo> nextLevelPropertyList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUpgradeEffectPropertyInfo(int currLevel, ConfigDataTechInfo techInfo)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

