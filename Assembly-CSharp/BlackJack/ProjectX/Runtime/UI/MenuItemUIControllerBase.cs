﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class MenuItemUIControllerBase : UIControllerBase
    {
        [AutoBind("./TargetInfo", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_targetInfoTrans;
        [AutoBind("./TargetInfo", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_menuItemIconButton;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_detailInfoRoot;
        [AutoBind("./TargetInfo/BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_bgImageObj;
        [AutoBind("./TargetInfo/Info/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_itemIconImage;
        [AutoBind("./TargetInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_menuItemStateCtrl;
        private ulong m_instanceId;
        private float m_allTweensPlayDelayTime;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SwitchDetailInfoVisibleState;
        private static DelegateBridge __Hotfix_IsDetailInfoVisible;
        private static DelegateBridge __Hotfix_SetInstanceId;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_GetItemIconSprite;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_ShowMenuItemDelay;
        private static DelegateBridge __Hotfix_ShowMenuItemImmediately;
        private static DelegateBridge __Hotfix_HideMenuItem;
        private static DelegateBridge __Hotfix_SetRemainingTimeInfo;
        private static DelegateBridge __Hotfix_DisableRemainingTimeInfo;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_GetLossWarningButtonPos;
        private static DelegateBridge __Hotfix_GetLossWarningButtonSize;

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void DisableRemainingTimeInfo()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public Sprite GetItemIconSprite()
        {
        }

        [MethodImpl(0x8000)]
        public virtual Vector3 GetLossWarningButtonPos()
        {
        }

        [MethodImpl(0x8000)]
        public virtual Vector2 GetLossWarningButtonSize()
        {
        }

        [MethodImpl(0x8000)]
        public virtual string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public virtual MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public void HideMenuItem(bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsDetailInfoVisible()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetInstanceId(ulong id)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetRemainingTimeInfo(int days, int hours, int minutes, int seconds)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMenuItemDelay(float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMenuItemImmediately()
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchDetailInfoVisibleState()
        {
        }
    }
}

