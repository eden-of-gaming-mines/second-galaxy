﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarfieldBlockPolygonController : Graphic
    {
        private Transform m_parentTrans;
        private Vector2[] m_verticalsForGlobal;
        private int[] m_triangles;
        private Color[] m_verticalColors;
        private float m_currScale;
        private Vector2 m_currOffset;
        private static DelegateBridge __Hotfix_InitPolygonMeshInfo;
        private static DelegateBridge __Hotfix_DoTransform;
        private static DelegateBridge __Hotfix_OnPopulateMesh;

        [MethodImpl(0x8000)]
        public void DoTransform(float scale, Vector2 offset)
        {
        }

        [MethodImpl(0x8000)]
        public void InitPolygonMeshInfo(Transform parentTrans, Vector2[] verticals, int[] triangles, Color[] colors)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPopulateMesh(VertexHelper vh)
        {
        }
    }
}

