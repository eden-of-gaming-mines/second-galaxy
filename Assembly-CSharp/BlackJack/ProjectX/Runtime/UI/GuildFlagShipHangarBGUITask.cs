﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GuildFlagShipHangarBGUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLongPressd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLongPressReleased;
        private ShipModelOverviewUIController m_mainCtrl;
        protected bool m_isSelectShipChanged;
        protected ILBStaticShip m_currSelectShip;
        protected bool m_isSelectHangarSolarSystemIdChanged;
        protected int m_currSelectHangarSolarSystemId;
        protected string m_currSolarSystemSkyBoxResPath;
        protected int m_updateWeaponGroupIndex;
        protected string m_currShipResPath;
        public const string ParamKeySelectHangarShip = "SelectStaticShip";
        public const string ParamKeySelectShipResPath = "SelectShipResPath";
        public const string ParamKeySelectHangarSolarSystemId = "SelectHangarSolarSystemId";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildFlagShipHangarBGUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ClearShipModel;
        private static DelegateBridge __Hotfix_StartGuildFlagShipHangarBGUITask;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadFromShipList;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitLayerStateOnUpdateView;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnLongPressed;
        private static DelegateBridge __Hotfix_OnLongPressRelease;
        private static DelegateBridge __Hotfix_PauseGuildFlagShipHangarBgTask;
        private static DelegateBridge __Hotfix_ResumeGuildFlagShipHangarBgTask;
        private static DelegateBridge __Hotfix_SetCurrSelectHangarShip;
        private static DelegateBridge __Hotfix_GetCurrShipFromUIIntent;
        private static DelegateBridge __Hotfix_GetShipResPathFromIntent;
        private static DelegateBridge __Hotfix_GetHangarSolarSystemIdFromIntent;
        private static DelegateBridge __Hotfix_GetShipModelAssetByShip;
        private static DelegateBridge __Hotfix_GetShipModelAssetByResPath;
        private static DelegateBridge __Hotfix_GetWeaponAssetByResPath;
        private static DelegateBridge __Hotfix_PrepareShipViewModel;
        private static DelegateBridge __Hotfix_GetWeaponEquipArtResource;
        private static DelegateBridge __Hotfix_SetSkyBoxResPath;
        private static DelegateBridge __Hotfix_add_EventOnLongPressd;
        private static DelegateBridge __Hotfix_remove_EventOnLongPressd;
        private static DelegateBridge __Hotfix_add_EventOnLongPressReleased;
        private static DelegateBridge __Hotfix_remove_EventOnLongPressReleased;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnLongPressd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLongPressReleased
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarBGUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearShipModel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private List<string> CollectDynamicResForLoadFromShipList()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected ILBStaticShip GetCurrShipFromUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetHangarSolarSystemIdFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetShipModelAssetByResPath(string shipResPath)
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetShipModelAssetByShip(ILBStaticShip lbShip)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetShipResPathFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected GameObject GetWeaponAssetByResPath(string resPath)
        {
        }

        [MethodImpl(0x8000)]
        private string GetWeaponEquipArtResource(LBStaticWeaponEquipSlotGroup lbStaticGroup, GrandFaction grandFaction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLayerStateOnUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressed()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressRelease()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static void PauseGuildFlagShipHangarBgTask()
        {
        }

        [MethodImpl(0x8000)]
        private KeyValuePair<float, Bounds> PrepareShipViewModel(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public static void ResumeGuildFlagShipHangarBgTask()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrSelectHangarShip(ILBStaticShip ship, int solarSystemId, bool needResetRotation = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSkyBoxResPath(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFlagShipHangarBGUITask StartGuildFlagShipHangarBGUITask(ILBStaticFlagShip ship, int solarSystemId, Action redirectPipLineOnLoadAllResCompleted)
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartLoadDynamicRes>c__AnonStorey0
        {
            internal GDBSolarSystemInfo solarSystemInfo;
            internal GuildFlagShipHangarBGUITask $this;

            internal void <>m__0(GDBSolarSystemInfo ssInfo)
            {
                this.solarSystemInfo = ssInfo;
                this.$this.SetSkyBoxResPath(this.solarSystemInfo);
                if (string.IsNullOrEmpty(this.$this.m_currSolarSystemSkyBoxResPath))
                {
                    this.$this.m_loadingDynamicResCorutineCount--;
                    this.$this.OnLoadDynamicResCompleted();
                }
                else
                {
                    HashSet<string> pathList = new HashSet<string> {
                        this.$this.m_currSolarSystemSkyBoxResPath
                    };
                    ResourceManager.Instance.StartLoadAssetsCorutine(pathList, this.$this.m_dynamicResCacheDict, delegate {
                        this.$this.m_loadingDynamicResCorutineCount--;
                        this.$this.OnLoadDynamicResCompleted();
                    }, false);
                }
                this.$this.<StartLoadDynamicRes>__BaseCallProxy0();
            }

            internal void <>m__1()
            {
                this.$this.m_loadingDynamicResCorutineCount--;
                this.$this.OnLoadDynamicResCompleted();
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsNeedResetRotation,
            IsStartOrResume
        }
    }
}

