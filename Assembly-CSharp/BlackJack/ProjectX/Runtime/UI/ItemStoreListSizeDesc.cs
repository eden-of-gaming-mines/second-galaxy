﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using UnityEngine;

    public class ItemStoreListSizeDesc : MonoBehaviour
    {
        [Header("列表区域尺寸")]
        public Vector2 ListRectSize;
        [Header("列表Item间隔尺寸")]
        public Vector2 SpacingSize;
        [Header("是否重新设置toggle区域大小")]
        public bool IsResizeToggleRect;
        [Header("Toggle区域尺寸")]
        public Vector2 ToggleRectSize;
        [Header("ToggleCell尺寸")]
        public Vector2 ToggleCellSize;
        [Header("每行列个数")]
        public int constraintCount;
    }
}

