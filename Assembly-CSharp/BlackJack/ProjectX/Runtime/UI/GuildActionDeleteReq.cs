﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildActionDeleteReq : NetWorkTransactionTask
    {
        private ulong m_instanceId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private GuildActionDeleteAck <ActionCreateAck>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnGuildActionDeleteAck;
        private static DelegateBridge __Hotfix_set_ActionCreateAck;
        private static DelegateBridge __Hotfix_get_ActionCreateAck;

        [MethodImpl(0x8000)]
        public GuildActionDeleteReq(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildActionDeleteAck(GuildActionDeleteAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public GuildActionDeleteAck ActionCreateAck
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

