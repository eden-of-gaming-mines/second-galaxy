﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class SpecialGiftPackageShipRewardItemUIController : UIControllerBase
    {
        private CommonItemIconUIController m_itemCtrl;
        private FakeLBStoreItem m_fakeItem;
        private Action<ILBStoreItemClient> ActionOnButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemClickButton;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescText;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemDummyRoot;
        private static DelegateBridge __Hotfix_UpdateRewardItem;
        private static DelegateBridge __Hotfix_RegisterClickEvent;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnClickButtonClicked;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickButtonClicked(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterClickEvent(Action<ILBStoreItemClient> onClick)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardItem(QuestRewardInfo rewardInfo, string descStr, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }
    }
}

