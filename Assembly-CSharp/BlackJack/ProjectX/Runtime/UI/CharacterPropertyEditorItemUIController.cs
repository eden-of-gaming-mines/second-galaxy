﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterPropertyEditorItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<PropertyCategory> EventOnSubPointButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PropertyCategory> EventOnAddPointButtonClick;
        private PropertyCategory m_propertyCategory;
        private List<CharacterPropertyEffectItemUIController> m_characterPropertyEffectItemUICtrlList;
        [AutoBind("./PointCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_pointCountText;
        [AutoBind("./SubPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_subPointButton;
        [AutoBind("./SubPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subPointButtonEffectCtrl;
        [AutoBind("./AddPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_addPointButton;
        [AutoBind("./AddPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_addPointButtonEffectCtrl;
        [AutoBind("./Effects", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_effectsRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitEditorItemPropertyCategory;
        private static DelegateBridge __Hotfix_SetPropertyPointInfo;
        private static DelegateBridge __Hotfix_SetEffectInfo;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnSubPointButtonClick;
        private static DelegateBridge __Hotfix_OnAddPointButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSubPointButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSubPointButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAddPointButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddPointButtonClick;

        public event Action<PropertyCategory> EventOnAddPointButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PropertyCategory> EventOnSubPointButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void InitEditorItemPropertyCategory(PropertyCategory propertyCategory)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddPointButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubPointButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEffectInfo(float realValue, float effectValue, int index = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyPointInfo(int currPointCount, int minPointCount, int maxPointCount, int freePointCount)
        {
        }
    }
}

