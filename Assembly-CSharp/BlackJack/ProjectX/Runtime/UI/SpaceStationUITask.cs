﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Resource;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceStationUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private bool m_initStateRecord;
        private bool m_closedRecommendWeaponTip;
        private bool m_closedRecommendChipTip;
        private int m_unReadChatMsgCountToShow;
        private bool m_isStopChatMsg;
        private DateTime m_nextCalculatChatShowSpeedTime;
        private readonly Queue<ChatUIInfo> m_chatListCache;
        private int m_currNewMsgCount;
        private float m_currChatDisplaySpeed;
        private bool m_isApplicationPause;
        private const float CalculatTimeSpan = 200f;
        private const float BaseChatShowSpeed = 2f;
        public int m_currentPlayerListStarIndex;
        private int m_playerListTotalCount;
        private PlayerListSortType m_currentPlayerListSortType;
        private CharacterSimpleInfoUITask m_charSimpleInfoUITask;
        private bool m_isAutoReloadComplete;
        private bool m_isAutoUnLoad;
        private bool m_isDynamicNpcGot;
        private bool m_isPlayerListReqEnd;
        private bool m_isGiftPackgeListReqEnd;
        private bool m_isSdkGoodsListReqEnd;
        private int m_unLoadItemCount;
        public static string m_paramKeyResetToOrginalState;
        public static string m_paramKeyEnterType;
        private EnterSpaceStationType m_enterType;
        protected PanelState m_currPanelState;
        private SpaceStationUIController m_spaceStationUICtrl;
        private SpaceStationIntroduceUIController m_spaceStationIntroduceUICtrl;
        private SpaceStationBGTask m_spaceStationBGTask;
        private MenuPanelInSpaceStationUITask m_menuPanelUITask;
        private MotherShipUITask m_motherShipUITask;
        private ItemObtainEffectUITask m_itemObtainEffectUITask;
        protected bool m_isSpaceStionBGTaskReadyToShow;
        protected bool m_isMenuPanelUITaskReadyToShow;
        protected bool m_isItemObtainEffectUITaskReadyToShow;
        protected bool m_isMotherShipUITaskReadyToShow;
        public ChatUIInfo m_lastChatInfo;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNeedQuestCompleteEffect;
        private LBProcessingQuestBase m_readyForNpcDialogCompleteQuest;
        private readonly List<FakeLBStoreItem> m_RecommendItemListEx;
        protected static string RecommendItemSotreUIQueueName;
        protected bool m_hasEnterMonthCardThisTimeInStation;
        protected const int FirstEnterSpaceStationUserGuideStepId = 0x15;
        protected const int FirstEnterSpaceStationUserGuideGroupId = 11;
        protected const int EnterSecondSpaceStationUserGuideStepId = 0x17;
        protected const int EnterSecondSpaceStationUserGuideGroupId = 13;
        protected const int EnterSecondSpaceStationQuestConfigId = 0x4e2a;
        private DateTime m_nextUserInfoRefreshTime;
        private double m_defultUserInfoIntervalTime;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "SpaceStationUITask";
        private bool m_isGuildMode;
        private readonly List<ProPlayerSimplestInfo> m_playerInfoList;
        private UIManager.UIActionQueueItem m_questCompleteQueueItem;
        private bool m_isStationInfoShowQueueItemCompleted;
        private bool m_isQuestCompleteDialogtQueueItemCompleted;
        private bool m_isQuestAcceptDialogQueueItemCompleted;
        protected const int StationInfoItemShowTime = 0xfa0;
        protected const int QuestInfoItemShowTime = 0xfa0;
        protected const int QuestCompleteEffectShowTime = 4;
        protected const string EnterStationUIBlockName = "EnterStationTipUI";
        protected const string SpaceStationInfoQueueItemName = "SpaceStationInfoQueueItemName";
        protected const string QuestCompleteQueueItemName = "QuestCompleteQueueItemName";
        protected const string UserGuideItemName = "UserGuideActionItemName";
        private readonly List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        public const string ParamKeyRepeatableUserGuideForExplore = "RepeatableUserGuideForExplore";
        public const string ParamKeyRepeatableUserGuideForExploreInstID = "RepeatableUserGuideForExploreInstID";
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache1;
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache2;
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache3;
        [CompilerGenerated]
        private static Predicate<LBDelegateMission> <>f__am$cache4;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache5;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache6;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache7;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cache8;
        [CompilerGenerated]
        private static Action <>f__am$cache9;
        [CompilerGenerated]
        private static Func<bool> <>f__am$cacheA;
        [CompilerGenerated]
        private static Action <>f__am$cacheB;
        [CompilerGenerated]
        private static Action <>f__am$cacheC;
        [CompilerGenerated]
        private static Action <>f__am$cacheD;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cacheE;
        [CompilerGenerated]
        private static Action <>f__am$cacheF;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CheckKey;
        private static DelegateBridge __Hotfix_StartSpaceStationUITaskWithPrepare;
        private static DelegateBridge __Hotfix_ResetSpaceStationUIToNormalState_1;
        private static DelegateBridge __Hotfix_ResetSpaceStationUIToNormalState_0;
        private static DelegateBridge __Hotfix_ResetSpaceStationUIForQuest;
        private static DelegateBridge __Hotfix_ResetUITaskWithTryStartQuestCompletedDialog;
        private static DelegateBridge __Hotfix_StartSpaceStationUITaskImp;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_Clear4EnterSpaceStation;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_Init;
        private static DelegateBridge __Hotfix_UpdateView_StationUI;
        private static DelegateBridge __Hotfix_UpdateView_StationBGInfo;
        private static DelegateBridge __Hotfix_UpdateView_SubPanelInfo;
        private static DelegateBridge __Hotfix_UpdateView_Recharge;
        private static DelegateBridge __Hotfix_ResetSubPanelState;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInSpaceStationBgTask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInMenuPanelUITask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedForItemObtainEffectPanel;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInMotherShipUITask;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_OnTeamChatNtf;
        private static DelegateBridge __Hotfix_OnWisperChatNtf;
        private static DelegateBridge __Hotfix_OnLocalChatNtf;
        private static DelegateBridge __Hotfix_OnSolarSystemChatNtf;
        private static DelegateBridge __Hotfix_OnStarFieldChatNtf;
        private static DelegateBridge __Hotfix_OnAllianceChatNtf;
        private static DelegateBridge __Hotfix_OnGuildChatNtf;
        private static DelegateBridge __Hotfix_OnChatMessgeNtf;
        private static DelegateBridge __Hotfix_CheckNpcDialogQuestForComplete;
        private static DelegateBridge __Hotfix_OnQuestWaitForAcceptListAdd;
        private static DelegateBridge __Hotfix_OnQuestWaitForAcceptListRemove;
        private static DelegateBridge __Hotfix_OnQuestAccept;
        private static DelegateBridge __Hotfix_OnQuestCancel;
        private static DelegateBridge __Hotfix_OnQuestCompletConfirmAck;
        private static DelegateBridge __Hotfix_OnChapterQuestCutsceneStartNtf;
        private static DelegateBridge __Hotfix_OnChapterQuestCutsceneEnd;
        private static DelegateBridge __Hotfix_OnSelfMissionInvateStart;
        private static DelegateBridge __Hotfix_OnSelfMissionInvateEnd;
        private static DelegateBridge __Hotfix_OnGuildMemberMissionInvateStart;
        private static DelegateBridge __Hotfix_OnGuildMemberMissionInvateEnd;
        private static DelegateBridge __Hotfix_OnAddEmergencySignal;
        private static DelegateBridge __Hotfix_OnRemoveEmergencySignal;
        private static DelegateBridge __Hotfix_InitRescueButtonState;
        private static DelegateBridge __Hotfix_OnCrackCompleteNtf;
        private static DelegateBridge __Hotfix_UpdateInvestigateButton;
        private static DelegateBridge __Hotfix_OpenWebInvestigation;
        private static DelegateBridge __Hotfix_PDSDK_OnDoQuestionSucceed;
        private static DelegateBridge __Hotfix_PDSDK_OnDoQuestionFailed;
        private static DelegateBridge __Hotfix_OnDelegateMissionStateUpdateNtf;
        private static DelegateBridge __Hotfix_OnProductionLineComplete;
        private static DelegateBridge __Hotfix_OnSpaceStationActionButtonClick;
        private static DelegateBridge __Hotfix_OnUserInfoButtonClick;
        private static DelegateBridge __Hotfix_OnSpaceStationRescueButtonClick;
        private static DelegateBridge __Hotfix_OnRescueButtonState_Emergency;
        private static DelegateBridge __Hotfix_OnRescueButtonState_Alliance;
        private static DelegateBridge __Hotfix_OnSpaceStationBackgroundButtonClick;
        private static DelegateBridge __Hotfix_OnStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnStrikeButtonClickImp;
        private static DelegateBridge __Hotfix_OnAnnouncementUIButtonClick;
        private static DelegateBridge __Hotfix_OnChatMsgClick;
        private static DelegateBridge __Hotfix_OnChatMsgClickImp;
        private static DelegateBridge __Hotfix_OnMenuFunctionButtonClick;
        private static DelegateBridge __Hotfix_OnMenuFunctionButtonClickImp;
        private static DelegateBridge __Hotfix_OnGuildButtonClick_1;
        private static DelegateBridge __Hotfix_OnPlayerListFunctionButtonClick;
        private static DelegateBridge __Hotfix_OnMotherShipPanelCloseButtonClick;
        private static DelegateBridge __Hotfix_OnMotherShipPanelBuildingButtonClick;
        private static DelegateBridge __Hotfix_OnMotherShipPanelBuildingButtonClickImp;
        private static DelegateBridge __Hotfix_OnMotherShipPanelDelegateFleetItemClick;
        private static DelegateBridge __Hotfix_OnMotherShipPanelDelegateSubPanelClick;
        private static DelegateBridge __Hotfix_OnMotherShipPanelDelegateSubPanelRecordButtonClick;
        private static DelegateBridge __Hotfix_OnApplicationPause;
        private static DelegateBridge __Hotfix_OnStationBuildingClicked;
        private static DelegateBridge __Hotfix_OnLeaveStationClicked;
        private static DelegateBridge __Hotfix_OnBillboardClicked;
        private static DelegateBridge __Hotfix_OnShopClicked;
        private static DelegateBridge __Hotfix_OnRankingClicked;
        private static DelegateBridge __Hotfix_OnHeadQuarterClick;
        private static DelegateBridge __Hotfix_OnTradingCenterClick;
        private static DelegateBridge __Hotfix_OnBlackMarketClicked;
        private static DelegateBridge __Hotfix_OnBagButtonClick;
        private static DelegateBridge __Hotfix_OnWareHouseButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapButtonClickImp;
        private static DelegateBridge __Hotfix_OnMailButtonClick;
        private static DelegateBridge __Hotfix_OnCharacterButtonClick;
        private static DelegateBridge __Hotfix_OnCharacterButtonClickImp;
        private static DelegateBridge __Hotfix_OnTeamButtonClick;
        private static DelegateBridge __Hotfix_OnActivityButtonClick;
        private static DelegateBridge __Hotfix_OnActivityButtonClickImp;
        private static DelegateBridge __Hotfix_OnGuildButtonClick_0;
        private static DelegateBridge __Hotfix_OnGuildButtonClickImp;
        private static DelegateBridge __Hotfix_OnSettingButtonClick;
        private static DelegateBridge __Hotfix_OnTradeButtonClick;
        private static DelegateBridge __Hotfix_OnTradeButtonClickImp;
        private static DelegateBridge __Hotfix_OnRelationButtonClick;
        private static DelegateBridge __Hotfix_OnViewModeButtonClick;
        private static DelegateBridge __Hotfix_OnTechButtonClick;
        private static DelegateBridge __Hotfix_OnTechButtonClickImp;
        private static DelegateBridge __Hotfix_OnCrewDormitoryButtonClick_0;
        private static DelegateBridge __Hotfix_OnCrewDormitoryButtonClick_1;
        private static DelegateBridge __Hotfix_OnItemStoreButtonClick_0;
        private static DelegateBridge __Hotfix_OnItemStoreButtonClick_1;
        private static DelegateBridge __Hotfix_OnHangarButtonClick;
        private static DelegateBridge __Hotfix_OnHangarButtonClickImp;
        private static DelegateBridge __Hotfix_OnCrackButtonClick_0;
        private static DelegateBridge __Hotfix_OnCrackButtonClick_1;
        private static DelegateBridge __Hotfix_OnDevelopmentButtonClick;
        private static DelegateBridge __Hotfix_OnGuildStoreButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapForGuildButtonClick;
        private static DelegateBridge __Hotfix_OnProduceForGuildButtonClick;
        private static DelegateBridge __Hotfix_OnGuildWarDamageButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagShipManagementButtonClick;
        private static DelegateBridge __Hotfix_OnGuildPurchaseButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFlagShipOptLogButtonClick;
        private static DelegateBridge __Hotfix_HideCharacterSimpleInfoPanel;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_ChatButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_OnPanelClose;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerListItemClick;
        private static DelegateBridge __Hotfix_OnPlayerListRefreshButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerListSortTypeChange;
        private static DelegateBridge __Hotfix_OnStarMapUITaskStart;
        private static DelegateBridge __Hotfix_OnQuestListUITaskResume;
        private static DelegateBridge __Hotfix_OnIntrodeceDetailPanelChange;
        private static DelegateBridge __Hotfix_OnBackGroudClick;
        private static DelegateBridge __Hotfix_OnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_OnBattlePassButtonClick;
        private static DelegateBridge __Hotfix_OnMonthCardButtonClick;
        private static DelegateBridge __Hotfix_OnGiftPackageButtonClick;
        private static DelegateBridge __Hotfix_OnOperatingActivityButtonClick;
        private static DelegateBridge __Hotfix_OperatingActivityRedPointRefresh;
        private static DelegateBridge __Hotfix_UpdateActivityRedPoint;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_UpdateBuildingTagInfo;
        private static DelegateBridge __Hotfix_AddMsgToChat;
        private static DelegateBridge __Hotfix_HideMenuPanel;
        private static DelegateBridge __Hotfix_OpenRoomAndTalkToQuestNpc;
        private static DelegateBridge __Hotfix_OpenChatUITask;
        private static DelegateBridge __Hotfix_EnableActiveBtnEffect;
        private static DelegateBridge __Hotfix_GetEnterFirstSpaceStationCutSceneAssetPath;
        private static DelegateBridge __Hotfix_GetEnterSecondSpaceStationCutSceneAssetPath;
        private static DelegateBridge __Hotfix_SetMaskStateForResume;
        private static DelegateBridge __Hotfix_GetFadeInOutUIProcess;
        private static DelegateBridge __Hotfix_NeedShowGiftPackageBtn;
        private static DelegateBridge __Hotfix_ChangeMenuFunctionPanelShowState;
        private static DelegateBridge __Hotfix_ClosePanelStart;
        private static DelegateBridge __Hotfix_ChangeMenuGuildModeShowState;
        private static DelegateBridge __Hotfix_ChangePlayerListFunctionPanelShowState;
        private static DelegateBridge __Hotfix_GetAllUIPanelHideProcessImmediate;
        private static DelegateBridge __Hotfix_GetMenuPanelOpenOrHideUIProcess;
        private static DelegateBridge __Hotfix_GetMotherShipPanelOpenOrHideUIProcess;
        private static DelegateBridge __Hotfix_IsAnyUIPanelShow;
        private static DelegateBridge __Hotfix_SetSpaceStationBGUIBlockState;
        private static DelegateBridge __Hotfix_RegisterUIControllerEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIControllerEvent;
        private static DelegateBridge __Hotfix_OnOpenReport;
        private static DelegateBridge __Hotfix_OnOverSeaActivityButtonClick;
        private static DelegateBridge __Hotfix_RegisterSpaceStationBGTaskEvent;
        private static DelegateBridge __Hotfix_UnRegisterSpaceStationBGTaskEvent;
        private static DelegateBridge __Hotfix_RegisterMenuPanelEvent;
        private static DelegateBridge __Hotfix_UnRegisterMenuPanelEvent;
        private static DelegateBridge __Hotfix_RegisterRescueEvent;
        private static DelegateBridge __Hotfix_UnRegisterRescueEvent;
        private static DelegateBridge __Hotfix_RegisterSignalEvent;
        private static DelegateBridge __Hotfix_UnRegisterSignalEvent;
        private static DelegateBridge __Hotfix_RegisterCrackEvent;
        private static DelegateBridge __Hotfix_UnRegisterCrackEvent;
        private static DelegateBridge __Hotfix_RegisterDelegateMissionStateUpdateEvent;
        private static DelegateBridge __Hotfix_UnRegisterDelegateMissionStateUpdateEvent;
        private static DelegateBridge __Hotfix_RegisterProduceCompleteEvent;
        private static DelegateBridge __Hotfix_UnRegisterProduceCompleteEvent;
        private static DelegateBridge __Hotfix_StartSpaceStaionBGTask;
        private static DelegateBridge __Hotfix_StartMenuPanelUITask;
        private static DelegateBridge __Hotfix_StartItemObtainEffectUITask;
        private static DelegateBridge __Hotfix_StartMotherShipUITask;
        private static DelegateBridge __Hotfix_IsWithMotherShipInSameStation;
        private static DelegateBridge __Hotfix_RegisterMotherShipUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterMotherShipUIEvent;
        private static DelegateBridge __Hotfix_GetSolarSystemPlayerListInfo;
        private static DelegateBridge __Hotfix_AutoReLoadAmmo;
        private static DelegateBridge __Hotfix_RequestGuildCompensationList;
        private static DelegateBridge __Hotfix_AutoUnLoad;
        private static DelegateBridge __Hotfix_GetStationDynamicNpcTalker;
        private static DelegateBridge __Hotfix_RequestGiftPackageList;
        private static DelegateBridge __Hotfix_OnAllReqPrepareEnterSpaceStationSucceed;
        private static DelegateBridge __Hotfix_ReSortPlayerList;
        private static DelegateBridge __Hotfix_ComparePlayerList;
        private static DelegateBridge __Hotfix_CompareForName;
        private static DelegateBridge __Hotfix_ShowSpaceStationEnterInfo;
        private static DelegateBridge __Hotfix_UpdateSpaceStationIntroduceInfo;
        private static DelegateBridge __Hotfix_ResetChatMsgTipView;
        private static DelegateBridge __Hotfix_OnChatUITaskPause;
        private static DelegateBridge __Hotfix_UpdateChatMsgDisPlayTime;
        private static DelegateBridge __Hotfix_RegisterChatNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterChatNetWorkEvent;
        private static DelegateBridge __Hotfix_RegisterQuestEvent;
        private static DelegateBridge __Hotfix_UnRegisterQuestEvent;
        private static DelegateBridge __Hotfix_UpdateUnReadChatMsgTipInfo;
        private static DelegateBridge __Hotfix_UpdateMenuFunctionButtonRedPoint;
        private static DelegateBridge __Hotfix_UpdateGuildButtonRedPoint;
        private static DelegateBridge __Hotfix_UpdateActionButtonInfo;
        private static DelegateBridge __Hotfix_UpdateBGTaskHeadQuarterQuestState;
        private static DelegateBridge __Hotfix_ExistCrackedBoxList;
        private static DelegateBridge __Hotfix_ExistCompleteProduceLine;
        private static DelegateBridge __Hotfix_ExistCompleteDelagateMission;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_RegisterRecommendUIEvent;
        private static DelegateBridge __Hotfix_UnRegisteRecommendEvent;
        private static DelegateBridge __Hotfix_IsNeedUpdateRecommend;
        private static DelegateBridge __Hotfix_CollectRecommendItemDataCache;
        private static DelegateBridge __Hotfix_IsAnyImportantRecommendMarkInfo;
        private static DelegateBridge __Hotfix_CollectRecommendItemRes;
        private static DelegateBridge __Hotfix_CreateShowRecommondItemStoreUIQuene;
        private static DelegateBridge __Hotfix_OnRecommendItemClick;
        private static DelegateBridge __Hotfix_OnRecommendItemCloseClick;
        private static DelegateBridge __Hotfix_ResetRecommendFlag;
        private static DelegateBridge __Hotfix_RegisterBlackMarkUIEvent;
        private static DelegateBridge __Hotfix_UnregisterBlackMarkUIEvent;
        private static DelegateBridge __Hotfix_OnEnterBlackMark;
        private static DelegateBridge __Hotfix_CreateSpaceStationBackgroundManager;
        private static DelegateBridge __Hotfix_RegisterPDSDKEvent;
        private static DelegateBridge __Hotfix_UnRegisterPDSDKEvent;
        private static DelegateBridge __Hotfix_GetUserInfoState;
        private static DelegateBridge __Hotfix_PDSDK_OnNoticeCenterStateSuccess;
        private static DelegateBridge __Hotfix_PDSDK_OnNoticeCenterStateFailed;
        private static DelegateBridge __Hotfix_PDSDK_OnEventOnInfoWebViewSuccess;
        private static DelegateBridge __Hotfix_RegistDailyRefreshEvent;
        private static DelegateBridge __Hotfix_UnRegistDailyRefreshEvent;
        private static DelegateBridge __Hotfix_OnDailyRefreshNtf;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbProduce;
        private static DelegateBridge __Hotfix_UpdateLoadingProcessValue;
        private static DelegateBridge __Hotfix_SetBattlePassButtonState;
        private static DelegateBridge __Hotfix_get_MotherShipTask;
        private static DelegateBridge __Hotfix_add_EventOnNeedQuestCompleteEffect;
        private static DelegateBridge __Hotfix_remove_EventOnNeedQuestCompleteEffect;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_ShowSpaceStationNameTipWindow;
        private static DelegateBridge __Hotfix_ShowQuestCompletePipeLine;
        private static DelegateBridge __Hotfix_ShowQuestAcceptDialogPanel;
        private static DelegateBridge __Hotfix_ShowSailReportPanel;
        private static DelegateBridge __Hotfix_ShowUserGuideTipWindow;
        private static DelegateBridge __Hotfix_ShowAppScoreUITask;
        private static DelegateBridge __Hotfix_ShowSpecialGiftPackageRecommedPanel;
        private static DelegateBridge __Hotfix_ShowQuestNameTip;
        private static DelegateBridge __Hotfix_ShowQuestCompleteDialogPanel;
        private static DelegateBridge __Hotfix_RegisterNeedQuestCompleteEffectEvent;
        private static DelegateBridge __Hotfix_ShowQuestCompleteEffect;
        private static DelegateBridge __Hotfix_StartUserGuideItem;
        private static DelegateBridge __Hotfix_PlayFuctionOpenState;
        private static DelegateBridge __Hotfix_UpdateFunctionOpenStateInview;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableBattlePass;
        private static DelegateBridge __Hotfix_UnLockBattlePass;
        private static DelegateBridge __Hotfix_EnableOperatingActivity;
        private static DelegateBridge __Hotfix_UnLockOperatingActivity;
        private static DelegateBridge __Hotfix_IsAllowToTriggerQuestAcceptDialog;
        private static DelegateBridge __Hotfix_ClearForQuestDialog;
        private static DelegateBridge __Hotfix_IsStationInitEnterPipeLineEnd;
        private static DelegateBridge __Hotfix_RepeatGuideStepOne;
        private static DelegateBridge __Hotfix_OnRepeatableClickEnd;
        private static DelegateBridge __Hotfix_OnRepeatableClickEndExplore;
        private static DelegateBridge __Hotfix_ShowMotherShipUIPanel;
        private static DelegateBridge __Hotfix_GetHeadQuarterPos43DStation;
        private static DelegateBridge __Hotfix_GetAuctionPos43DStation;
        private static DelegateBridge __Hotfix_GetShopPos43DStation;
        private static DelegateBridge __Hotfix_GetRankingPos43DStation;
        private static DelegateBridge __Hotfix_EnterHeadQuarteUI;
        private static DelegateBridge __Hotfix_EnterAuctionUI;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_ClickMenuPanelButton;
        private static DelegateBridge __Hotfix_ClickShipHangarButton;
        private static DelegateBridge __Hotfix_ClickItemStoreButton;
        private static DelegateBridge __Hotfix_ClickCrackButton;
        private static DelegateBridge __Hotfix_ClickCrewDormitoryButton;
        private static DelegateBridge __Hotfix_ClickMenuPanelActivityButton;
        private static DelegateBridge __Hotfix_ClickAvitvityButton;
        private static DelegateBridge __Hotfix_ClickStarMapButton;
        private static DelegateBridge __Hotfix_ClickGuildButton;
        private static DelegateBridge __Hotfix_ClickStrikeButton;
        private static DelegateBridge __Hotfix_ClickCharacterInfoPanel;
        private static DelegateBridge __Hotfix_ClickProduceButton;
        private static DelegateBridge __Hotfix_ClickTechButton;
        private static DelegateBridge __Hotfix_ClickTradeButton;
        private static DelegateBridge __Hotfix_IsUIShowEnd;

        public event Action EventOnNeedQuestCompleteEffect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SpaceStationUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void AddMsgToChat(string msg, ChatChannel channel)
        {
        }

        [MethodImpl(0x8000)]
        private void AutoReLoadAmmo(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void AutoUnLoad(Action onEnd, UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        private void ChangeMenuFunctionPanelShowState(bool show, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        private void ChangeMenuGuildModeShowState(bool show, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        private void ChangePlayerListFunctionPanelShowState(bool show, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_ChatButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_OnPanelClose()
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckKey()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckNpcDialogQuestForComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected static void Clear4EnterSpaceStation()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearForQuestDialog()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickAvitvityButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickCharacterInfoPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickCrackButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickCrewDormitoryButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickGuildButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickItemStoreButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickMenuPanelActivityButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickMenuPanelButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickProduceButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickShipHangarButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickStarMapButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickStrikeButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickTechButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickTradeButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ClosePanelStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectRecommendItemDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectRecommendItemRes(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static HashSet<string> CollectResPathForReserve()
        {
        }

        [MethodImpl(0x8000)]
        private int CompareForName(ProPlayerSimplestInfo a, ProPlayerSimplestInfo b)
        {
        }

        [MethodImpl(0x8000)]
        private int ComparePlayerList(ProPlayerSimplestInfo a, ProPlayerSimplestInfo b)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateShowRecommondItemStoreUIQuene()
        {
        }

        [MethodImpl(0x8000)]
        private SpaceStationBGUIBackgroupManager CreateSpaceStationBackgroundManager()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableActiveBtnEffect(bool active)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableBattlePass(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableOperatingActivity(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void EnterAuctionUI(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void EnterHeadQuarteUI(Action<bool> onEnd, int talkToNpcId)
        {
        }

        [MethodImpl(0x8000)]
        private bool ExistCompleteDelagateMission()
        {
        }

        [MethodImpl(0x8000)]
        private bool ExistCompleteProduceLine()
        {
        }

        [MethodImpl(0x8000)]
        private bool ExistCrackedBoxList()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetAllUIPanelHideProcessImmediate(bool isIncludMenuPanel = true)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetAuctionPos43DStation()
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetEnterFirstSpaceStationCutSceneAssetPath()
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetEnterSecondSpaceStationCutSceneAssetPath()
        {
        }

        [MethodImpl(0x8000)]
        private CustomUIProcess GetFadeInOutUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetHeadQuarterPos43DStation()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess GetMenuPanelOpenOrHideUIProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess GetMotherShipPanelOpenOrHideUIProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetRankingPos43DStation()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetShopPos43DStation()
        {
        }

        [MethodImpl(0x8000)]
        private void GetSolarSystemPlayerListInfo(Action succeedAction)
        {
        }

        [MethodImpl(0x8000)]
        private void GetStationDynamicNpcTalker(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void GetUserInfoState()
        {
        }

        [MethodImpl(0x8000)]
        private void HideCharacterSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        public void HideMenuPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitRescueButtonState()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAllowToTriggerQuestAcceptDialog()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAnyImportantRecommendMarkInfo()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAnyUIPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void IsNeedUpdateRecommend()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsStationInitEnterPipeLineEnd()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsUIShowEnd()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsWithMotherShipInSameStation()
        {
        }

        [MethodImpl(0x8000)]
        private bool NeedShowGiftPackageBtn()
        {
        }

        [MethodImpl(0x8000)]
        private void OnActivityButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnActivityButtonClickImp(Action<bool> onEnd, ActionPlanTabType actionTabType = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddEmergencySignal(ulong instanceID)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceChatNtf(ChatInfo chatInfo, ChatExtraOutputLocationType extraOutputType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllReqPrepareEnterSpaceStationSucceed(Action<bool> OnComplete)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAnnouncementUIButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnApplicationPause(bool isPause)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroudClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBagButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployInfoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBattlePassButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBillboardClicked(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBlackMarketClicked(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChapterQuestCutsceneEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChapterQuestCutsceneStartNtf(int questId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatMessgeNtf(ChatChannel channel, ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatMsgClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatMsgClickImp(ChatChannel channel, CharactorObserverAck gameUserInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatUITaskPause(Task task)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackCompleteNtf(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrewDormitoryButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrewDormitoryButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDailyRefreshNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissionStateUpdateNtf(ulong missionInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDevelopmentButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnterBlackMark(string mode)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildChatNtf(ChatInfo chatInfo, ChatExtraOutputLocationType extraOutputType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipManagementButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipOptLogButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMemberMissionInvateEnd(ulong missionInsId, int manualQuestSolarsystemId, uint manualQuestSceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMemberMissionInvateStart(ulong missionInsId, int manualQuestSolarsystemId, uint manualQuestSceneInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildPurchaseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildStoreButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildWarDamageButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnHeadQuarterClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnIntrodeceDetailPanelChange(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaveStationClicked()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadAllResCompletedForItemObtainEffectPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoadAllResCompletedInMenuPanelUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoadAllResCompletedInMotherShipUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoadAllResCompletedInSpaceStationBgTask()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLocalChatNtf(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuFunctionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuFunctionButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMotherShipPanelBuildingButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMotherShipPanelBuildingButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMotherShipPanelCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMotherShipPanelDelegateFleetItemClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMotherShipPanelDelegateSubPanelClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMotherShipPanelDelegateSubPanelRecordButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenReport(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOperatingActivityButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOverSeaActivityButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerListFunctionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerListItemClick(UIControllerBase uctrl, ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerListRefreshButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerListSortTypeChange(PlayerListSortType currentSortTypeIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnProduceForGuildButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnProductionLineComplete(LBProductionLine productionLine)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestAccept(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCancel(LBProcessingQuestBase quest)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestCompletConfirmAck(int result, int questConfigId, List<QuestRewardInfo> rewardList, bool disableReturnNotice)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestListUITaskResume()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestWaitForAcceptListAdd(int questId, int questFactionId, int questLevel, int questSolarSystemId, bool fromQuestCancel, LBQuestEnv questEnv)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestWaitForAcceptListRemove(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRankingClicked(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecommendItemClick(FakeLBStoreItem fakeItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecommendItemCloseClick(FakeLBStoreItem recommendfakeItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRelationButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveEmergencySignal()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRepeatableClickEnd(bool isInRect, int isQuickJump, int itemType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRepeatableClickEndExplore(bool isInRect, int questInstId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueButtonState_Alliance()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRescueButtonState_Emergency()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfMissionInvateEnd(ulong missionInsId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSelfMissionInvateStart(ulong missionInsId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSettingButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void OnShopClicked(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemChatNtf(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationActionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationBackgroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationRescueButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarFieldChatNtf(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapForGuildButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnStarMapUITaskStart()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStationBuildingClicked(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStrikeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStrikeButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamChatNtf(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTechButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTradingCenterClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUserInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnViewModeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWareHouseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWisperChatNtf(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OpenChatUITask(ChatChannel channel, CharactorObserverAck gameUserInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OpenRoomAndTalkToQuestNpc(int questInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OpenWebInvestigation(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OperatingActivityRedPointRefresh()
        {
        }

        [MethodImpl(0x8000)]
        private void PDSDK_OnDoQuestionFailed()
        {
        }

        [MethodImpl(0x8000)]
        private void PDSDK_OnDoQuestionSucceed()
        {
        }

        [MethodImpl(0x8000)]
        private void PDSDK_OnEventOnInfoWebViewSuccess()
        {
        }

        [MethodImpl(0x8000)]
        private void PDSDK_OnNoticeCenterStateFailed()
        {
        }

        [MethodImpl(0x8000)]
        private void PDSDK_OnNoticeCenterStateSuccess()
        {
        }

        [MethodImpl(0x8000)]
        private void PlayFuctionOpenState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegistDailyRefreshEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterBlackMarkUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent(CharacterSimpleInfoUITask task)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterChatNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCrackEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterDelegateMissionStateUpdateEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterMenuPanelEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterMotherShipUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterNeedQuestCompleteEffectEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPDSDKEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterProduceCompleteEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterQuestEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterRecommendUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterRescueEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterSignalEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterSpaceStationBGTaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIControllerEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RepeatGuideStepOne()
        {
        }

        [MethodImpl(0x8000)]
        private void RequestGiftPackageList(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RequestGuildCompensationList()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetChatMsgTipView()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetRecommendFlag(bool includeUi = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void ResetSpaceStationUIForQuest(int questInstanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void ResetSpaceStationUIToNormalState(UIIntent intent, Action<bool> onResetEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void ResetSpaceStationUIToNormalState(Action<bool> onResetEnd, bool clearProcess = true)
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetSubPanelState()
        {
        }

        [MethodImpl(0x8000)]
        public static void ResetUITaskWithTryStartQuestCompletedDialog()
        {
        }

        [MethodImpl(0x8000)]
        private void ReSortPlayerList()
        {
        }

        [MethodImpl(0x8000)]
        private void SetBattlePassButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void SetMaskStateForResume()
        {
        }

        [MethodImpl(0x8000)]
        private void SetSpaceStationBGUIBlockState()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowAppScoreUITask()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMotherShipUIPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowQuestAcceptDialogPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowQuestCompleteDialogPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowQuestCompleteEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowQuestCompletePipeLine()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowQuestNameTip(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowSailReportPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowSpaceStationEnterInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowSpaceStationNameTipWindow()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowSpecialGiftPackageRecommedPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowUserGuideTipWindow()
        {
        }

        [MethodImpl(0x8000)]
        private void StartItemObtainEffectUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void StartMenuPanelUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void StartMotherShipUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void StartSpaceStaionBGTask()
        {
        }

        [MethodImpl(0x8000)]
        protected static void StartSpaceStationUITaskImp(Action<bool> onPrepareEnd, Action onUnloadResourceCompleted = null, EnterSpaceStationType enterType = 3)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartSpaceStationUITaskWithPrepare(Action<bool> onPrepareEnd, Action onUnloadResourceCompleted = null, EnterSpaceStationType enterType = 3, Action onHidePreTask = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartUserGuideItem()
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockBattlePass(Action onActionEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockOperatingActivity(Action onActionEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegistDailyRefreshEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterBlackMarkUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterChatNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterCrackEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterDelegateMissionStateUpdateEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisteRecommendEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterMenuPanelEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterMotherShipUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterPDSDKEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterProduceCompleteEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterQuestEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterRescueEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterSignalEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterSpaceStationBGTaskEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIControllerEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateActionButtonInfo(bool isCountChange = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateActivityRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBGTaskHeadQuarterQuestState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuildingTagInfo(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType buildingType, Vector3 goViewportPos)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChatMsgDisPlayTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFunctionOpenStateInview(bool forceInit = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildButtonRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateInvestigateButton()
        {
        }

        [MethodImpl(0x8000)]
        protected static void UpdateLoadingProcessValue(float value, EnterSpaceStationType enterType)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMenuFunctionButtonRedPoint()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateSpaceStationIntroduceInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUnReadChatMsgTipInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_Init()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_Recharge()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_StationBGInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_StationUI()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_SubPanelInfo()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockGuildClient LbProduce
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public MotherShipUITask MotherShipTask
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <AutoReLoadAmmo>c__AnonStoreyF
        {
            internal Action onEnd;
            internal SpaceStationUITask $this;

            internal void <>m__0(Task task)
            {
                AutoReloadAmmoReqNetTask task2 = task as AutoReloadAmmoReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AckResult != 0)
                    {
                        Debug.LogError($"AutoReloadAmmoReqNetTask error:  returnTask.m_ackResult != 0 errorCode = {task2.AckResult}");
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                    }
                    this.$this.m_isAutoReloadComplete = true;
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <AutoUnLoad>c__AnonStorey10
        {
            internal Action onEnd;
            internal SpaceStationUITask $this;

            internal void <>m__0(Task task)
            {
                HangarShipTransformItemToForStationEnterReqNetTask task2 = task as HangarShipTransformItemToForStationEnterReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AckResult == 0)
                    {
                        this.$this.m_unLoadItemCount = task2.UnloadItemCount;
                    }
                    else
                    {
                        Debug.LogError($"HangarShipTransformItemToForStationEnterReqNetTask AckResult = {task2.AckResult}");
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                        this.$this.m_unLoadItemCount = 0;
                    }
                    this.$this.m_isAutoUnLoad = true;
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChangeMenuFunctionPanelShowState>c__AnonStorey9
        {
            internal bool show;
            internal SpaceStationUITask $this;

            internal void <>m__0(UIProcess currProcess, bool isCompleted)
            {
                if (this.show)
                {
                    this.$this.m_motherShipUITask.SetAnnouncementTextInfo();
                }
                else
                {
                    this.$this.m_motherShipUITask.StopAnnouncementTextInfo();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChangeMenuGuildModeShowState>c__AnonStoreyA
        {
            internal bool show;
            internal SpaceStationUITask $this;

            internal void <>m__0(UIProcess currProcess, bool isCompleted)
            {
                if (this.show)
                {
                    this.$this.m_motherShipUITask.SetAnnouncementTextInfo();
                }
                else
                {
                    this.$this.m_motherShipUITask.StopAnnouncementTextInfo();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ChangePlayerListFunctionPanelShowState>c__AnonStoreyB
        {
            internal bool show;
            internal SpaceStationUITask $this;

            internal void <>m__0(UIProcess currProcess, bool isCompleted)
            {
                if (this.show)
                {
                    this.$this.m_spaceStationUICtrl.m_playerListUICtrl.UpdatePlayerInfo(this.$this.m_playerInfoList, this.$this.m_dynamicResCacheDict);
                }
                bool isshow = this.$this.m_currPanelState == SpaceStationUITask.PanelState.PlayerListOpen;
                this.$this.m_spaceStationUICtrl.UpdatePlayerListFunctionButtonState(isshow);
                this.$this.m_spaceStationUICtrl.UpdateCommonButtonsState(!isshow);
                this.$this.m_spaceStationIntroduceUICtrl.ShowOrHideIntroducePanel(!this.show);
            }
        }

        [CompilerGenerated]
        private sealed class <CreateShowRecommondItemStoreUIQuene>c__AnonStorey13
        {
            internal UIManager.UIActionQueueItem item;
            internal SpaceStationUITask $this;

            internal void <>m__0()
            {
                this.$this.m_spaceStationUICtrl.UpdateRecommendItem(this.$this.m_RecommendItemListEx, this.$this.m_closedRecommendWeaponTip, this.$this.m_closedRecommendChipTip, this.$this.m_dynamicResCacheDict);
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <GetFadeInOutUIProcess>c__AnonStorey7
        {
            internal Action<bool> processEnd;

            internal void <>m__0()
            {
                if (this.processEnd != null)
                {
                    this.processEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetMenuPanelOpenOrHideUIProcess>c__AnonStoreyC
        {
            internal bool isShow;
            internal bool isImmediately;

            internal void <>m__0(Action<bool> onEnd)
            {
                MenuPanelInSpaceStationUITask.StartShowOrHideMenuPanel(onEnd, this.isShow, this.isImmediately);
            }
        }

        [CompilerGenerated]
        private sealed class <GetMotherShipPanelOpenOrHideUIProcess>c__AnonStoreyD
        {
            internal bool isImmediately;
            internal SpaceStationUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                MotherShipUITask.ShowMotherShipPanel(this.$this.m_isGuildMode, this.isImmediately, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                MotherShipUITask.HideMotherShipPanel(this.isImmediately, onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <GetSolarSystemPlayerListInfo>c__AnonStoreyE
        {
            internal Action succeedAction;
            internal SpaceStationUITask $this;

            internal void <>m__0(Task task)
            {
                GetSolarSystemPlayerListReqNetTask task2 = task as GetSolarSystemPlayerListReqNetTask;
                if (!task2.IsNetworkError)
                {
                    int ackResult = task2.m_ackResult;
                    if (ackResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(ackResult, true, false);
                        Debug.LogError("SpaceStationUiTask::GetSolarSystemPlayerListInfo GetSolarSystemPlayerListReqNetTask Fail: result " + ackResult);
                    }
                    else
                    {
                        this.$this.GetPipeLineCtx();
                        this.$this.m_currPipeLineCtx.AddUpdateMask(4);
                        this.$this.m_currentPlayerListStarIndex = task2.m_playerListAckInfo.StartIndex;
                        this.$this.m_playerListTotalCount = task2.m_playerListAckInfo.SolarSystemPlayerCount + 1;
                        this.$this.m_playerInfoList.Clear();
                        this.$this.m_playerInfoList.Add(CommonLogicUtil.Convert2ProPlayerSimplestInfo(this.$this.PlayerCtx.GetPlayerSimplestInfo()));
                        foreach (ProPlayerSimplestInfo info in task2.m_playerListAckInfo.PlayerSimpleInfo)
                        {
                            if (info.PlayerGameUserId != this.$this.PlayerCtx.m_gameUserId)
                            {
                                this.$this.m_playerInfoList.Add(info);
                                continue;
                            }
                            this.$this.m_playerListTotalCount--;
                        }
                    }
                    this.$this.m_isPlayerListReqEnd = true;
                    this.succeedAction();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <GetStationDynamicNpcTalker>c__AnonStorey11
        {
            internal Action onEnd;
            internal SpaceStationUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.m_isDynamicNpcGot = true;
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <NeedShowGiftPackageBtn>c__AnonStorey8
        {
            internal LogicBlockRechargeGiftPackageClient lbGiftPackage;

            internal bool <>m__0(LBRechargeGiftPackage pkg) => 
                this.lbGiftPackage.IsEligibleToBuy(pkg);

            internal bool <>m__1(LBRechargeGiftPackage pkg) => 
                (this.lbGiftPackage.IsEligibleToBuy(pkg) && (pkg.m_confInfo.Theme != GiftPackageTheme.GiftPackageTheme_Guild));
        }

        [CompilerGenerated]
        private sealed class <OnGiftPackageButtonClick>c__AnonStorey6
        {
            internal LogicBlockRechargeGiftPackageClient lbGiftPackage;
            internal SpaceStationUITask $this;

            internal void <>m__0(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }

            internal bool <>m__1(LBRechargeGiftPackage pkg) => 
                (this.lbGiftPackage.IsEligibleToBuy(pkg) && (pkg.m_confInfo.Theme != GiftPackageTheme.GiftPackageTheme_Guild));

            internal void <>m__2(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }

            internal bool <>m__3(LBRechargeGiftPackage pkg) => 
                this.lbGiftPackage.IsEligibleToBuy(pkg);

            internal void <>m__4(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }

            internal void <>m__5(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnGuildButtonClickImp>c__AnonStorey5
        {
            internal Action<bool> onEnd;
            internal SpaceStationUITask $this;

            internal void <>m__0(bool bo)
            {
                if (bo)
                {
                    this.$this.Pause();
                }
                if (this.onEnd != null)
                {
                    this.onEnd(bo);
                }
            }

            internal void <>m__1(bool res)
            {
                if (res)
                {
                    this.$this.Pause();
                }
                else if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnRankingClicked>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal SpaceStationUITask $this;

            internal void <>m__0(bool result)
            {
                if (result)
                {
                    this.$this.Pause();
                }
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey3
        {
            internal Action<bool> onPrepareEnd;
            internal SpaceStationUITask $this;

            internal void <>m__0()
            {
                this.$this.OnAllReqPrepareEnterSpaceStationSucceed(this.onPrepareEnd);
            }

            internal void <>m__1()
            {
                this.$this.OnAllReqPrepareEnterSpaceStationSucceed(this.onPrepareEnd);
            }

            internal void <>m__2()
            {
                this.$this.OnAllReqPrepareEnterSpaceStationSucceed(this.onPrepareEnd);
            }

            internal void <>m__3()
            {
                this.$this.OnAllReqPrepareEnterSpaceStationSucceed(this.onPrepareEnd);
            }

            internal void <>m__4()
            {
                this.$this.OnAllReqPrepareEnterSpaceStationSucceed(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatGuideStepOne>c__AnonStorey1A
        {
            internal int isQuickJump;
            internal int itemType;
            internal SpaceStationUITask $this;

            internal void <>m__0(bool isInRect)
            {
                this.$this.OnRepeatableClickEnd(isInRect, this.isQuickJump, this.itemType);
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatGuideStepOne>c__AnonStorey1B
        {
            internal int questInstId;
            internal SpaceStationUITask.<RepeatGuideStepOne>c__AnonStorey1A <>f__ref$26;

            internal void <>m__0(bool isInRect)
            {
                this.<>f__ref$26.$this.OnRepeatableClickEndExplore(isInRect, this.questInstId);
            }
        }

        [CompilerGenerated]
        private sealed class <RequestGiftPackageList>c__AnonStorey12
        {
            internal Action onEnd;
            internal SpaceStationUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_isGiftPackgeListReqEnd = true;
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }

            internal void <>m__1(bool res)
            {
                this.$this.m_isSdkGoodsListReqEnd = true;
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ResetSpaceStationUIForQuest>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal int questInstanceId;

            internal void <>m__0(bool res)
            {
                if (res || (this.onEnd == null))
                {
                    (UIManager.Instance.FindUITaskWithName("SpaceStationUITask", true) as SpaceStationUITask).OpenRoomAndTalkToQuestNpc(this.questInstanceId, this.onEnd);
                }
                else
                {
                    this.onEnd(false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowAppScoreUITask>c__AnonStorey16
        {
            internal UIManager.UIActionQueueItem item;
            internal SpaceStationUITask $this;

            internal bool <>m__0() => 
                ((this.$this.m_enterType != EnterSpaceStationType.FromSpaceDeath) && AppScoreUITask.CanDoAppScore());

            internal void <>m__1()
            {
                this.$this.Pause();
                AppScoreUITask.StartAppScoreUITask(this.$this.CurrentIntent, () => this.item.OnEnd());
            }

            internal void <>m__2()
            {
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <ShowQuestAcceptDialogPanel>c__AnonStorey14
        {
            internal UIManager.UIActionQueueItem item;
            internal QuestWaitForAcceptInfo waitingAutoAcceptQuest;
            internal SpaceStationUITask $this;

            internal void <>m__0()
            {
                UIManager.Instance.GlobalUIInputBlockForTicks(10);
                Debug.LogWarning("<color=red>[弹窗]</color>-任务接受对话结束, Ticks: " + Timer.m_currTick);
                this.$this.m_isQuestAcceptDialogQueueItemCompleted = true;
                Debug.LogWarning("<color=red>[弹窗]</color>-进站流程结束，Ticks: " + Timer.m_currTick);
            }

            internal void <>m__1()
            {
                Debug.LogWarning("<color=red>[弹窗]</color>-任务接受对话开始, Ticks: " + Timer.m_currTick);
                QuestUtilHelper.StartQuestAccpetDilaog(this.item, new UnAcceptedQuestUIInfo(this.waitingAutoAcceptQuest), new Action(this.$this.Pause), (rst, qid) => this.$this.Resume(null, null), () => this.$this.Resume(null, null), null);
            }

            internal void <>m__2(int rst, int qid)
            {
                this.$this.Resume(null, null);
            }

            internal void <>m__3()
            {
                this.$this.Resume(null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowQuestNameTip>c__AnonStorey18
        {
            internal Action onEnd;

            internal void <>m__0()
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
                Debug.LogWarning("<color=red>[弹窗]</color>-结束显示任务信息, Ticks: " + Timer.m_currTick);
                UIManager.Instance.GlobalUIInputEnable("EnterStationTipUI", true);
                UIManager.Instance.GlobalUIInputBlockForTicks(10);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowSailReportPanel>c__AnonStorey15
        {
            internal UIManager.UIActionQueueItem item;
            internal SpaceStationUITask $this;

            internal void <>m__0()
            {
                this.$this.Pause();
                SailReportUITask task = SailReportUITask.StartSailReportTask(this.$this.CurrentIntent, null);
                if (task != null)
                {
                    task.EventOnStop += t => this.item.OnEnd();
                }
            }

            internal void <>m__1(Task t)
            {
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <ShowSpecialGiftPackageRecommedPanel>c__AnonStorey17
        {
            internal UIManager.UIActionQueueItem item;
            internal SpaceStationUITask $this;

            internal bool <>m__0()
            {
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return (((playerContext == null) || !playerContext.IsBlockRechargeUI) && (((this.$this.PlayerCtx.GetLBRechargeGiftPackage() as LogicBlockRechargeGiftPackageClient).GetRecommedSpecialRechargeGiftPackageList(null).Count != 0) && !UserGuideUITask.IsInUserGuide()));
            }

            internal void <>m__1()
            {
                this.$this.Pause();
                SpecialGiftPackageRecommendUITask.StartSpecialGiftPackageRecommendUITask(this.$this.CurrentIntent, () => this.item.OnEnd());
            }

            internal void <>m__2()
            {
                this.item.OnEnd();
            }
        }

        [CompilerGenerated]
        private sealed class <StartSpaceStationUITaskImp>c__AnonStorey2
        {
            internal int loadedAssetCount;
            internal int reserveAssetTotalCount;
            internal EnterSpaceStationType enterType;
            internal Action onUnloadResourceCompleted;
            internal HashSet<string> spaceStationReserveSet;
            internal Action onAssetLoaded;
            internal Action<bool> onPrepareEnd;

            internal void <>m__0()
            {
                this.loadedAssetCount++;
                SpaceStationUITask.UpdateLoadingProcessValue(((float) this.loadedAssetCount) / ((float) this.reserveAssetTotalCount), this.enterType);
            }

            internal void <>m__1()
            {
                if (this.onUnloadResourceCompleted != null)
                {
                    this.onUnloadResourceCompleted();
                }
                ResourceManager.Instance.StartReserveAssetsCorutine(this.spaceStationReserveSet, new Dictionary<string, UnityEngine.Object>(), delegate {
                    ResourceManager.Instance.EventOnAssetLoaded -= this.onAssetLoaded;
                    UIIntentCustom intent = new UIIntentCustom("SpaceStationUITask", null);
                    intent.SetParam(SpaceStationUITask.m_paramKeyEnterType, this.enterType);
                    UIManager.Instance.StartUITaskWithPrepare(intent, this.onPrepareEnd, true, null, null);
                }, true, 0x708);
            }

            internal void <>m__2()
            {
                ResourceManager.Instance.EventOnAssetLoaded -= this.onAssetLoaded;
                UIIntentCustom intent = new UIIntentCustom("SpaceStationUITask", null);
                intent.SetParam(SpaceStationUITask.m_paramKeyEnterType, this.enterType);
                UIManager.Instance.StartUITaskWithPrepare(intent, this.onPrepareEnd, true, null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <StartSpaceStationUITaskWithPrepare>c__AnonStorey0
        {
            internal Action onHidePreTask;
            internal Action<bool> onPrepareEnd;
            internal Action onUnloadResourceCompleted;
            internal EnterSpaceStationType enterType;

            internal void <>m__0()
            {
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                if ((playerContext != null) && !playerContext.GetLBUserGuide().IsStepGroupAlreadyCompleted(11))
                {
                    if (this.onHidePreTask != null)
                    {
                        this.onHidePreTask();
                    }
                    UserGuideUITask.SkipStep(0x15, true, null);
                    CutScenePlayerUITask.StartPlayCutSceneAnimation(SpaceStationUITask.GetEnterFirstSpaceStationCutSceneAssetPath(), () => SpaceStationUITask.StartSpaceStationUITaskImp(this.onPrepareEnd, this.onUnloadResourceCompleted, this.enterType), null);
                }
                else
                {
                    if ((playerContext != null) && !playerContext.GetLBUserGuide().IsStepGroupAlreadyCompleted(13))
                    {
                        LBProcessingQuestBase firstProcessingQuestByConfigId = playerContext.GetLBQuest().GetFirstProcessingQuestByConfigId(0x4e2a);
                        if ((firstProcessingQuestByConfigId != null) && (firstProcessingQuestByConfigId.GetCompleteCondNpcDNId().SpaceStationId == playerContext.CurrClentSpaceStationContext.SpaceStationID))
                        {
                            if (this.onHidePreTask != null)
                            {
                                this.onHidePreTask();
                            }
                            UserGuideUITask.SkipStep(0x17, true, null);
                            CutScenePlayerUITask.StartPlayCutSceneAnimation(SpaceStationUITask.GetEnterSecondSpaceStationCutSceneAssetPath(), () => SpaceStationUITask.StartSpaceStationUITaskImp(this.onPrepareEnd, this.onUnloadResourceCompleted, this.enterType), null);
                            return;
                        }
                    }
                    SpaceStationUITask.StartSpaceStationUITaskImp(this.onPrepareEnd, this.onUnloadResourceCompleted, this.enterType);
                }
            }

            internal void <>m__1()
            {
                SpaceStationUITask.StartSpaceStationUITaskImp(this.onPrepareEnd, this.onUnloadResourceCompleted, this.enterType);
            }

            internal void <>m__2()
            {
                SpaceStationUITask.StartSpaceStationUITaskImp(this.onPrepareEnd, this.onUnloadResourceCompleted, this.enterType);
            }
        }

        [CompilerGenerated]
        private sealed class <StartUserGuideItem>c__AnonStorey19
        {
            internal UIManager.UIActionQueueItem item;
            internal SpaceStationUITask $this;

            internal bool <>m__0() => 
                ((this.$this.m_readyForNpcDialogCompleteQuest == null) && !this.$this.IsAnyUIPanelShow());

            internal void <>m__1()
            {
                Debug.LogWarning("<color=red>[弹窗]</color>-新手引导item开始, Ticks: " + Timer.m_currTick);
                UIManager.Instance.GlobalUIInputBlockForTicks(10);
                if ((this.$this.m_readyForNpcDialogCompleteQuest == null) && !this.$this.IsAnyUIPanelShow())
                {
                    UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_EnterStation, new object[0]);
                }
                this.item.OnEnd();
            }
        }

        protected enum PanelState
        {
            Normal,
            PlayerListOpen,
            MenuPanelAndMotherShipPanelOpen,
            MenuPanelGuildMode
        }

        protected enum PipeLineStateMaskType
        {
            IsNpcDialogQuestComplete,
            MenuPanelAndMotherShipStateChanged,
            MenuPanelGuildMode,
            PlayerListStateChanged,
            PlayerListDataCacheChanged,
            NeedHideAllUIPanelImmediately,
            IsResetToOrignalMode,
            IsNeedTriggerUserGuide,
            ClosePanel,
            IsNeedRecommendItemTip
        }

        public enum PlayerListSortType
        {
            NameUp,
            NameDown,
            LevelUp,
            LevelDown
        }
    }
}

