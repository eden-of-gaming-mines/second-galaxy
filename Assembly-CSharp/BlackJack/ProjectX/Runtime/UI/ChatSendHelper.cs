﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ChatSendHelper
    {
        private static ChatSendHelper m_instance;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_SendStarFieldChatMsgReq;
        private static DelegateBridge __Hotfix_SendSolarSystemChatMsgReq;
        private static DelegateBridge __Hotfix_SendTeamChatMsgReq;
        private static DelegateBridge __Hotfix_SendLocalChatMsgReq;
        private static DelegateBridge __Hotfix_SendWisperChatMsgReq;
        private static DelegateBridge __Hotfix_SendGuildChatMsgReq;
        private static DelegateBridge __Hotfix_SendAllianceChatMsgReq;
        private static DelegateBridge __Hotfix_SendChatMsgReqByChannel;
        private static DelegateBridge __Hotfix_CheckChatMsgIsCanSend;
        private static DelegateBridge __Hotfix_ClearInstance;
        private static DelegateBridge __Hotfix_get_Instance;

        [MethodImpl(0x8000)]
        private ChatSendHelper()
        {
        }

        [MethodImpl(0x8000)]
        public int CheckChatMsgIsCanSend(ChatChannel channel, ChatLanguageChannel langueChannel)
        {
        }

        [MethodImpl(0x8000)]
        public static void ClearInstance()
        {
        }

        [MethodImpl(0x8000)]
        public void SendAllianceChatMsgReq(ChatInfo chatInfo, UITaskBase uiTask)
        {
        }

        [MethodImpl(0x8000)]
        public void SendChatMsgReqByChannel(ChatChannel channel, ChatInfo chatInfo, UITaskBase uiTask = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SendGuildChatMsgReq(ChatInfo chatInfo, UITaskBase uiTask)
        {
        }

        [MethodImpl(0x8000)]
        public void SendLocalChatMsgReq(ChatInfo chatInfo, UITaskBase uiTask)
        {
        }

        [MethodImpl(0x8000)]
        public void SendSolarSystemChatMsgReq(ChatInfo chatInfo, UITaskBase uiTask)
        {
        }

        [MethodImpl(0x8000)]
        public void SendStarFieldChatMsgReq(ChatInfo chatInfo, UITaskBase uiTask)
        {
        }

        [MethodImpl(0x8000)]
        public void SendTeamChatMsgReq(ChatInfo chatInfo, UITaskBase uiTask)
        {
        }

        [MethodImpl(0x8000)]
        public void SendWisperChatMsgReq(ChatInfo chatInfo, UITaskBase uiTask)
        {
        }

        public static ChatSendHelper Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendAllianceChatMsgReq>c__AnonStorey6
        {
            internal UITaskBase uiTask;

            internal void <>m__0(Task task)
            {
                if (this.uiTask != null)
                {
                    this.uiTask.EnableUIInput(true);
                }
                if (!((AllianceChatReqNetTask) task).IsNetworkError)
                {
                    int result = ((AllianceChatReqNetTask) task).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        Debug.Log("ChatUITask::SendStarFieldChatMsgReq Fail !");
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildChatMsgReq>c__AnonStorey5
        {
            internal UITaskBase uiTask;

            internal void <>m__0(Task task)
            {
                if (this.uiTask != null)
                {
                    this.uiTask.EnableUIInput(true);
                }
                if (!((GuildChatReqNetTask) task).IsNetworkError)
                {
                    int result = ((GuildChatReqNetTask) task).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        Debug.Log("ChatUITask::SendStarFieldChatMsgReq Fail !");
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendLocalChatMsgReq>c__AnonStorey3
        {
            internal UITaskBase uiTask;

            internal void <>m__0(Task task)
            {
                if (this.uiTask != null)
                {
                    this.uiTask.EnableUIInput(true);
                }
                if (!((LocalChatReqNetTask) task).IsNetworkError)
                {
                    int result = (task as LocalChatReqNetTask).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        Debug.Log("ChatSendHelper::SendLocalChatMsgReq Fail !");
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendSolarSystemChatMsgReq>c__AnonStorey1
        {
            internal UITaskBase uiTask;

            internal void <>m__0(Task task)
            {
                if (this.uiTask != null)
                {
                    this.uiTask.EnableUIInput(true);
                }
                if (!(task as SolarSystemChatReqNetTask).IsNetworkError)
                {
                    int result = (task as SolarSystemChatReqNetTask).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        Debug.Log("ChatSendHelper::SendSolarSystemChatMsgReq Fail !");
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendStarFieldChatMsgReq>c__AnonStorey0
        {
            internal UITaskBase uiTask;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendTeamChatMsgReq>c__AnonStorey2
        {
            internal UITaskBase uiTask;

            internal void <>m__0(Task task)
            {
                if (this.uiTask != null)
                {
                    this.uiTask.EnableUIInput(true);
                }
                if (!(task as TeamChatReqNetTask).IsNetworkError)
                {
                    int result = (task as TeamChatReqNetTask).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        Debug.Log("ChatSendHelper::SendTeamChatMsgReq Fail !");
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendWisperChatMsgReq>c__AnonStorey4
        {
            internal UITaskBase uiTask;

            internal void <>m__0(Task task)
            {
                if (this.uiTask != null)
                {
                    this.uiTask.EnableUIInput(true);
                }
                if (!((WisperChatReqNetTask) task).IsNetworkError)
                {
                    int result = (task as WisperChatReqNetTask).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        Debug.Log("ChatSendHelper::SendWisperChatMsgReq Fail !");
                    }
                }
            }
        }
    }
}

