﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CharacterKillRecordUITask : UITaskBase
    {
        public DateTime m_taskLastKillRecordUpdateTime;
        public ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        protected List<KillRecordInfo> m_killSuccessList;
        protected List<KillRecordInfo> m_beKilledList;
        protected LogicBlockKillRecordClient m_lbKillRecord;
        protected ProjectXPlayerContext m_playerCtx;
        protected CharacterKillRecordUIController m_mainCtrl;
        public const string KillMode = "Kill";
        public const string BeKillMode = "BeKilled";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CharacterKillRecordUITask";
        [CompilerGenerated]
        private static Comparison<KillRecordInfo> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<KillRecordInfo> <>f__am$cache1;
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnKillToggleSelected;
        private static DelegateBridge __Hotfix_OnLoseToggleSelected;
        private static DelegateBridge __Hotfix_OnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnItemIconButtonClick;
        private static DelegateBridge __Hotfix_OnItemSendLinkButtonClick;
        private static DelegateBridge __Hotfix_PushLayer;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_SetToggleSelectedState;
        private static DelegateBridge __Hotfix_OnKillRecordInfoNtf;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnKillRecordCompensationUpdateNtf;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CharacterKillRecordUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemDetailButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemIconButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSendLinkButtonClick(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillRecordCompensationUpdateNtf(KillRecordCompensationUpdateNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnKillRecordInfoNtf(KillRecordInfo recordInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnKillToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLoseToggleSelected()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void PushLayer()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetToggleSelectedState(string mode)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum PipeLineMaskType
        {
            UpdatePipeLineing = 1,
            StartOrResume = 2,
            ToggleSwitch = 3,
            CompensationUpdate = 4
        }
    }
}

