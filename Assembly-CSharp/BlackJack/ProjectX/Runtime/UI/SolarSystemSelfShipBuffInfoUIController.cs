﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemSelfShipBuffInfoUIController : UIControllerBase
    {
        [AutoBind("./Buffs/AttackBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackBuffObj;
        [AutoBind("./Buffs/DefenseBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefenseBuffObj;
        [AutoBind("./Buffs/ElectricBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ElectricBuffObj;
        [AutoBind("./Buffs/MoveBuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MoveBuffObj;
        [AutoBind("./Debuffs/AttackDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttackDebuffObj;
        [AutoBind("./Debuffs/DefenseDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefenseDebuffObj;
        [AutoBind("./Debuffs/ElectricDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ElectricDebuffObj;
        [AutoBind("./Debuffs/MoveDebuff", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MoveDebuffObj;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Button BuffInfoButton;
        private static DelegateBridge __Hotfix_UpdateCurrentSelectedShipBuffUIInfo;

        [MethodImpl(0x8000)]
        public void UpdateCurrentSelectedShipBuffUIInfo(ClientSpaceShip selfShip, ILBSpaceTarget target)
        {
        }
    }
}

