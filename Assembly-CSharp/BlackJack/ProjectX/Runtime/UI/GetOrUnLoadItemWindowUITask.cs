﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GetOrUnLoadItemWindowUITask : UITaskBase
    {
        private List<ILBStoreItemClient> m_itemList;
        private List<CurrencyUpdateInfo> m_currencyList;
        public const string ParamKeyItemList = "ShowItemList";
        public const string ParamKeyCurrencyList = "ShowCurrencyList";
        public const string ParamKeyRemainLastData = "RemainLastData";
        public Action<bool> EventOnGetOrUnloadItemWindowEnterAnotherTask;
        public Action EventOnGetOrUnloadItemWindowReturnToPreTask;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private GetOrUnLoadItemWindowUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GetOrUnLoadItemWindowUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGetOrUnLoadItemWindowUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoEnterAnotherTask;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_CloseWnd;
        private static DelegateBridge __Hotfix_UnregistAllEvent;
        private static DelegateBridge __Hotfix_ResumeUITaskWithLastIntent;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GetOrUnLoadItemWindowUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseWnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoEnterAnotherTask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ResumeUITaskWithLastIntent()
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartGetOrUnLoadItemWindowUITask(List<StoreItemUpdateInfo> storeItemUpdateInfos, List<CurrencyUpdateInfo> currencyUpdateInfos, UIIntent returUIIntent = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

