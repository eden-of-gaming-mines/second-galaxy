﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestListUIController : UIControllerBase
    {
        [HideInInspector]
        public QuestDetailInfoForAcceptedQuestUIController m_acceptedQuestDetailInfoUICtrl;
        [HideInInspector]
        public QuestDetailInfoForUnacceptedQuestUIController m_unacceptedQuestDetailInfoUICtrl;
        private QuestListCategoryUIController m_scienceExploreQuestCategoryCtrl;
        private List<QuestListItemUIController> m_scienceExploreSignalQuestListItemList;
        private QuestListCategoryUIController m_storyQuestCategoryCtrl;
        private List<QuestListItemUIController> m_storyQuestListItemList;
        private QuestListCategoryUIController m_freeQuestCategoryCtrl;
        private List<QuestListItemUIController> m_freeQuestListItemList;
        private QuestListCategoryUIController m_FactionCreditCategoryCtrl;
        private List<QuestListItemUIController> m_FactionCreditListItemList;
        private QuestListCategoryUIController m_unacceptedQuestCategoryCtrl;
        private List<QuestListItemUIController> m_unacceptQuestListItemList;
        private QuestListCategoryUIController m_branchQuestCategoryCtrl;
        private List<QuestListItemUIController> m_branchQuestListItemList;
        private List<QuestListItemUIController> m_unusedQuestListItemList;
        private const string QuestItemPrefab_ResName = "QuestListItemPrefab";
        private const string QuestItemRootState_HaveQuest = "HaveQuest";
        private const string QuestItemRootState_NoQuest = "NoQuest";
        public const string TABMODE_ACCEPTEDQUEST = "AcceptedQuest";
        public const string TABMODE_UNACCEPTEDQUEST = "UnacceptedQuest";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<QuestListCategoryUIController.CategoryQuestType> EventOnQuestCategoryToggleChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnAcceptQuestItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UnAcceptedQuestUIInfo> EventOnUnAcceptQuestItemClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./QuestDetail/DetailScrollView/Viewport/Content/DescGroup/DescriptionTitleGroup/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AllowInShipButton;
        [AutoBind("./QuestDetail/DetailScrollView/AllowInShipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform AllowInShipInfoDummy;
        [AutoBind("./QuestDetail", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AcceptedQuestDetailRoot;
        [AutoBind("./UnacceptedQuestDetail", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnacceptedQuestDetailRoot;
        [AutoBind("./QuestListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup ListItemToggleGroup;
        [AutoBind("./QuestListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect QuestListScrollView;
        [AutoBind("./QuestListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController QuestListScrollViewUIStateCtrl;
        [AutoBind("./QuestListScrollView/Viewport/Content/ScienceExploreQuestGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ScienceExploreQuestGroup;
        [AutoBind("./QuestListScrollView/Viewport/Content/StoryQuestGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StoryQuestGroup;
        [AutoBind("./QuestListScrollView/Viewport/Content/FreeQuestGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FreeQuestGroup;
        [AutoBind("./QuestListScrollView/Viewport/Content/FactionQuestGroup (1)", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FactionCreditGroup;
        [AutoBind("./QuestListScrollView/Viewport/Content/BranchQuestGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BranchQuestGroup;
        [AutoBind("./QuestListScrollView/Viewport/Content/UnacceptedGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnacceptedQuestGroup;
        [AutoBind("./QuestListScrollView/Viewport/Content/UnusedItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnusedQuestItemRoot;
        [AutoBind("./ItemSimpleInfoPanelLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoPanelLeftDummy;
        [AutoBind("./EmptyMessageBoardImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_emptyQuestDetailUIStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitQuestCategoryUIController;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_UpdateQuestListUI;
        private static DelegateBridge __Hotfix_GetShipAllowInInfoAnchorPosition;
        private static DelegateBridge __Hotfix_GetQuestListShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetQuestPanelShowOrHideUIProcess;
        private static DelegateBridge __Hotfix_CreateLeftAcceptedQuestListItems;
        private static DelegateBridge __Hotfix_AddQuestItemsToListRootForAcceptedQuest;
        private static DelegateBridge __Hotfix_AddSingleQuestItemToListRootForAcceptedQuest;
        private static DelegateBridge __Hotfix_AddQuestItemsToListRootForUnAcceptQuest;
        private static DelegateBridge __Hotfix_AddSingleQuestItemToListRootForUnAcceptQuest;
        private static DelegateBridge __Hotfix_AddVacantItemToListRoot;
        private static DelegateBridge __Hotfix_ResetCategoryFoldState;
        private static DelegateBridge __Hotfix_SwitchQuestCategoryFoldState;
        private static DelegateBridge __Hotfix_ExpandQuestCategory;
        private static DelegateBridge __Hotfix_GetCategoryQuestTypeWithConfigQuestType;
        private static DelegateBridge __Hotfix_GetCategoryControllerWithQuestType;
        private static DelegateBridge __Hotfix_GetCategoryControllerWithCategoryType;
        private static DelegateBridge __Hotfix_GetItemListWithQuestType;
        private static DelegateBridge __Hotfix_GetItemListWithCategoryType;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelPos;
        private static DelegateBridge __Hotfix_OnQuestCategoryToggleChanged;
        private static DelegateBridge __Hotfix_OnQuestListItemClick;
        private static DelegateBridge __Hotfix_RecycleAllQuestListItem;
        private static DelegateBridge __Hotfix_GetQuestListItem;
        private static DelegateBridge __Hotfix_GetFirstStoryQuestItem;
        private static DelegateBridge __Hotfix_GetQuestItemRectById;
        private static DelegateBridge __Hotfix_GetAcceptQuestItemSelectState;
        private static DelegateBridge __Hotfix_add_EventOnQuestCategoryToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnQuestCategoryToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnAcceptQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnAcceptQuestItemClick;
        private static DelegateBridge __Hotfix_add_EventOnUnAcceptQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnUnAcceptQuestItemClick;

        public event Action<int> EventOnAcceptQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<QuestListCategoryUIController.CategoryQuestType> EventOnQuestCategoryToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UnAcceptedQuestUIInfo> EventOnUnAcceptQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AddQuestItemsToListRootForAcceptedQuest(List<LBProcessingQuestBase> questList, LBProcessingQuestBase selectedQuest, List<QuestType> qstTypeList, QuestListCategoryUIController questCategoryCtrl, List<QuestListItemUIController> questItemList, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        private void AddQuestItemsToListRootForUnAcceptQuest(List<UnAcceptedQuestUIInfo> unaccpetedQuestList, UnAcceptedQuestUIInfo selectedQuestInfo, QuestListCategoryUIController questCategoryCtrl, List<QuestListItemUIController> questItemList, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        private void AddSingleQuestItemToListRootForAcceptedQuest(LBProcessingQuestBase quest, GameObject itemRoot, List<QuestListItemUIController> itemList, bool isSelected, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        private void AddSingleQuestItemToListRootForUnAcceptQuest(UnAcceptedQuestUIInfo unAcceptQuestInfo, GameObject itemRoot, List<QuestListItemUIController> itemList, bool isSelected, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        private void AddVacantItemToListRoot(GameObject itemRoot, List<QuestListItemUIController> itemList)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateLeftAcceptedQuestListItems(List<LBProcessingQuestBase> acceptedQuestList, List<UnAcceptedQuestUIInfo> unAcceptedQuestList, Dictionary<string, UnityEngine.Object> resDic, LBProcessingQuestBase selectedQuest, UnAcceptedQuestUIInfo unAcceptedQuest)
        {
        }

        [MethodImpl(0x8000)]
        public void ExpandQuestCategory(QuestType qstType)
        {
        }

        [MethodImpl(0x8000)]
        public bool GetAcceptQuestItemSelectState(int questId)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListCategoryUIController GetCategoryControllerWithCategoryType(QuestListCategoryUIController.CategoryQuestType catQuestType)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListCategoryUIController GetCategoryControllerWithQuestType(QuestListCategoryUIController.CategoryQuestType questType)
        {
        }

        [MethodImpl(0x8000)]
        public QuestListCategoryUIController.CategoryQuestType GetCategoryQuestTypeWithConfigQuestType(QuestType qstType)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstStoryQuestItem()
        {
        }

        [MethodImpl(0x8000)]
        private List<QuestListItemUIController> GetItemListWithCategoryType(QuestListCategoryUIController.CategoryQuestType catQuestType)
        {
        }

        [MethodImpl(0x8000)]
        private List<QuestListItemUIController> GetItemListWithQuestType(QuestListCategoryUIController.CategoryQuestType questType)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelPos()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetQuestItemRectById(int questId)
        {
        }

        [MethodImpl(0x8000)]
        private QuestListItemUIController GetQuestListItem()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetQuestListShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetQuestPanelShowOrHideUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetShipAllowInInfoAnchorPosition()
        {
        }

        [MethodImpl(0x8000)]
        private void InitQuestCategoryUIController(GameObject categoryGO, out QuestListCategoryUIController ctrl, QuestListCategoryUIController.CategoryQuestType catQeustType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestCategoryToggleChanged(QuestListCategoryUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestListItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void RecycleAllQuestListItem()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetCategoryFoldState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchQuestCategoryFoldState(QuestListCategoryUIController.CategoryQuestType catQuestType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestListUI(List<LBProcessingQuestBase> acceptedQuestList, List<UnAcceptedQuestUIInfo> unAcceptedQuestList, Dictionary<string, UnityEngine.Object> resDic, LBProcessingQuestBase selectedQuest, UnAcceptedQuestUIInfo unAcceptedQuest)
        {
        }
    }
}

