﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ItemObtainSourceListItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private ItemObtainSourceType <SrcType>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ItemObtainSourceType> EventOnItemClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        [AutoBind("./Title", AutoBindAttribute.InitState.NotInit, false)]
        public Text Title;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateItemObtainSourceList;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_get_SrcType;
        private static DelegateBridge __Hotfix_set_SrcType;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;

        public event Action<ItemObtainSourceType> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemObtainSourceList(Dictionary<string, Object> resDict, ConfigDataItemObtainSourceTypeInfo itemObtainSourceTypeInfo)
        {
        }

        public ItemObtainSourceType SrcType
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

