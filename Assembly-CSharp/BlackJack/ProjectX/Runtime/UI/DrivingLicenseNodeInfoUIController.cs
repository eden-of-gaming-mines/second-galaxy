﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class DrivingLicenseNodeInfoUIController : UIControllerBase
    {
        private int m_level;
        private int m_id;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DrivingLicenseNodeInfoUIController, bool> EventOnNodeSelected;
        private const string State_Normal = "Normal";
        private const string State_Gray = "Gray";
        private const string State_Now = "Now";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_nodeState;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle NodeButton;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateDrivingLicenseNode;
        private static DelegateBridge __Hotfix_SetToggleState;
        private static DelegateBridge __Hotfix_OnNodeClick;
        private static DelegateBridge __Hotfix_get_Level;
        private static DelegateBridge __Hotfix_get_ConfigID;
        private static DelegateBridge __Hotfix_add_EventOnNodeSelected;
        private static DelegateBridge __Hotfix_remove_EventOnNodeSelected;

        public event Action<DrivingLicenseNodeInfoUIController, bool> EventOnNodeSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init(Action<DrivingLicenseNodeInfoUIController, bool> nodeClickEvent = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNodeClick(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleState(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDrivingLicenseNode(ConfigDataDrivingLicenseInfo configInfo, int currSkillLevel, int skillLevel, Dictionary<string, Object> resDict, bool hasCurrState = false)
        {
        }

        public int Level
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int ConfigID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

