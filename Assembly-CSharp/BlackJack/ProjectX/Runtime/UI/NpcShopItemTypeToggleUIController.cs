﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class NpcShopItemTypeToggleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnShopItemTypeToggleSelected;
        public int m_shopItemType;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_itemTypeToggle;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemTypeNameText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetShopItemType;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnShopItemTypeToggleClick;
        private static DelegateBridge __Hotfix_add_EventOnShopItemTypeToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnShopItemTypeToggleSelected;

        public event Action<int> EventOnShopItemTypeToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopItemTypeToggleClick(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShopItemType(int itemType)
        {
        }
    }
}

