﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;

    public class StringFormatUtil
    {
        protected static string m_sUnitMeter;
        protected static string m_sUnitKmeter;
        protected static string m_sUnitAu;
        protected static double m_sVauleAu;
        protected static string m_sUnitMeterpersecond;
        protected static string m_sUnitKmeterpersecond;
        protected static string m_sUnitAupersecond;
        protected static string m_sUnitSecond;
        protected static string m_sUnitMinute;
        protected static string m_sUnitHour;
        protected static string m_sUnitDay;
        protected static string m_sUnitStere;
        protected static string m_sUnitAeon;
        protected static string m_sUnitTemperature;
        protected static string m_sUnitKilogram;
        protected static string m_sUnitGravity;
        public static List<string> m_romeNumList;
        private static readonly Regex htmlEncodedReg;
        protected static List<string> m_replaceStrList;
        [CompilerGenerated]
        private static Func<char, int> <>f__mg$cache0;
        [CompilerGenerated]
        private static MatchEvaluator <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ConvertFloatToRemain2BehindPointString;
        private static DelegateBridge __Hotfix_ConvertFloatToRemain1BehindPointString;
        private static DelegateBridge __Hotfix_ConvertFloatToPercentageString;
        private static DelegateBridge __Hotfix_ConvertFloatToPercentageStringWithoutPrecent;
        private static DelegateBridge __Hotfix_ConvertFloatToPercentage2BehindPointString;
        private static DelegateBridge __Hotfix_ConvertFloatToFloorIntString;
        private static DelegateBridge __Hotfix_ConvertFloatToCeilIntString;
        private static DelegateBridge __Hotfix_ConvertFloatToRoundedIntString;
        private static DelegateBridge __Hotfix_ConvertCountToUnitString_0;
        private static DelegateBridge __Hotfix_ConvertCountToUnitString_1;
        private static DelegateBridge __Hotfix_ConvertCountClampToString;
        private static DelegateBridge __Hotfix_ConvertDistanceFloatWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertDistanceRoundWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertDoubleToBillion;
        private static DelegateBridge __Hotfix_ConvertDistanceWithKmeterUnitToString;
        private static DelegateBridge __Hotfix_ConvertSpeedFloatWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertSpeedRoundWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertVolumeWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertVolumeFloatWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertVolumeRoundWithUnitToString;
        private static DelegateBridge __Hotfix_CovertVolumnToStringWithUnit;
        private static DelegateBridge __Hotfix_ConvertIntToCommaSeparatedNum;
        private static DelegateBridge __Hotfix_ConvertNumberToStringWithUnitPerSecondString;
        private static DelegateBridge __Hotfix_ConvertCurrencyToString;
        private static DelegateBridge __Hotfix_GetUnitStereString;
        private static DelegateBridge __Hotfix_GetUnitMeterString;
        private static DelegateBridge __Hotfix_GetUnitKmeterString;
        private static DelegateBridge __Hotfix_GetUnitAUString;
        private static DelegateBridge __Hotfix_GetUnitMeterPerSecondeString;
        private static DelegateBridge __Hotfix_GetUnitKMeterPerSecondeString;
        private static DelegateBridge __Hotfix_GetUnitAUPerSecondString;
        private static DelegateBridge __Hotfix_GetAUValue;
        private static DelegateBridge __Hotfix_GetUnitSecondString;
        private static DelegateBridge __Hotfix_GetUnitMinuteString;
        private static DelegateBridge __Hotfix_GetUnitHourString;
        private static DelegateBridge __Hotfix_GetUnitDayString;
        private static DelegateBridge __Hotfix_GetAEONStringFormat;
        private static DelegateBridge __Hotfix_GetUnitTemperatureString;
        private static DelegateBridge __Hotfix_GetUnitKilogramString;
        private static DelegateBridge __Hotfix_GetUnitGravityUnitString;
        private static DelegateBridge __Hotfix_CombineValueAndUnit;
        private static DelegateBridge __Hotfix_ClearCache;
        private static DelegateBridge __Hotfix_ReplaceInputSpaceToVisionSpace;
        private static DelegateBridge __Hotfix_GetCharShowLength;
        private static DelegateBridge __Hotfix_GetStrShowLength;
        private static DelegateBridge __Hotfix_GetAbbreviativeString;
        private static DelegateBridge __Hotfix_ConvertStrToColorStr;
        private static DelegateBridge __Hotfix_GetItemCountStringWithColor;
        private static DelegateBridge __Hotfix_GetStringWithCarriageReturn;
        private static DelegateBridge __Hotfix_GetStringInStringTableWithId;
        private static DelegateBridge __Hotfix_GetNpcTalkerDialogCountdownString;
        private static DelegateBridge __Hotfix_GetNameForPropertyCategory;
        private static DelegateBridge __Hotfix_GetRankNameByType;
        private static DelegateBridge __Hotfix_GetSubRankNameByType;
        private static DelegateBridge __Hotfix_GetSubRankStateNameByType;
        private static DelegateBridge __Hotfix_GetNpcShopItemCategoryName;
        private static DelegateBridge __Hotfix_GetNpcShopItemTypeName;
        private static DelegateBridge __Hotfix_GetSlotTypeName;
        private static DelegateBridge __Hotfix_GetQuestTypeName;
        private static DelegateBridge __Hotfix_GetCurrencyTypeName;
        private static DelegateBridge __Hotfix_GetGrandFactionName;
        private static DelegateBridge __Hotfix_GetGrandFactionNameByFactionID;
        private static DelegateBridge __Hotfix_GetFactionCreditLevelName;
        private static DelegateBridge __Hotfix_GetCreditRewardItemInfoString;
        private static DelegateBridge __Hotfix_GetCreditRewardInfoString;
        private static DelegateBridge __Hotfix_GetCaptainSubRankStr;
        private static DelegateBridge __Hotfix_GetEquipLaunchEnergyCost_0;
        private static DelegateBridge __Hotfix_GetEquipLaunchEnergyCost_1;
        private static DelegateBridge __Hotfix_GetNeedPermissionStrByPermission;
        private static DelegateBridge __Hotfix_GetGuildItemStoreTabSpriteIconPath;
        private static DelegateBridge __Hotfix_GetGuildItemStoreTabName;
        private static DelegateBridge __Hotfix_GetStringFromTimeSecond;
        private static DelegateBridge __Hotfix_GetDateStringFromDateTime;
        private static DelegateBridge __Hotfix_GetTimeStringFromDateTime;
        private static DelegateBridge __Hotfix_GetTimeAndDateStringFromDateTimeWithoutSec;
        private static DelegateBridge __Hotfix_GetTimeStringFromTimeMs;
        private static DelegateBridge __Hotfix_GetTimeSpanMaxUnitString;
        private static DelegateBridge __Hotfix_ConvertTimeWithUnitToString;
        private static DelegateBridge __Hotfix_GetTimeStringWithPostfix;
        private static DelegateBridge __Hotfix_ConvertTimeFloatWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertTimeRoundWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertTimeSecondToFormatTimeWithUnit;
        private static DelegateBridge __Hotfix_ConvertTimeRoundWithSecondUnitToString;
        private static DelegateBridge __Hotfix_ConvertTimeFloatWithSecondUnitToString;
        private static DelegateBridge __Hotfix_ConvertTimeFomartWithUnitToString;
        private static DelegateBridge __Hotfix_ConvertTimpSpanToString;
        private static DelegateBridge __Hotfix_ConvertIntToRomeNum;
        private static DelegateBridge __Hotfix_ConvertRetainMaxTimeUnit;
        private static DelegateBridge __Hotfix_ConvertTimeSpanToDayOrHour;
        private static DelegateBridge __Hotfix_ConvertTimeSpanToHour;
        private static DelegateBridge __Hotfix_FormatCoolDownTimeString;
        private static DelegateBridge __Hotfix_HtmlEncode;
        private static DelegateBridge __Hotfix_HtmlDecode;
        private static DelegateBridge __Hotfix_GetStringByTwoNounsConcatenation;
        private static DelegateBridge __Hotfix_GetStringByThreeNounsConcatenation;
        private static DelegateBridge __Hotfix_GetStringByFourNounsConcatenation;
        private static DelegateBridge __Hotfix_GetStringByTitleAndNameConcatenation;
        private static DelegateBridge __Hotfix_ParseFormatString;
        private static DelegateBridge __Hotfix_PrepareQuestDescFormatStringParamInfos;
        private static DelegateBridge __Hotfix_PrepareNpcDialogFormatStrParamInfos;
        private static DelegateBridge __Hotfix_GetParamInfoByIndex;
        private static DelegateBridge __Hotfix_GetParamFormatStr;
        private static DelegateBridge __Hotfix_GetCurrSolarSystemName;
        private static DelegateBridge __Hotfix_GetCurrPlayerName;
        private static DelegateBridge __Hotfix_GetCurrPlayerFullName;
        private static DelegateBridge __Hotfix_GetShipConfigName;
        private static DelegateBridge __Hotfix_GetItemConfigName;
        private static DelegateBridge __Hotfix_GetSolarSystemName_0;
        private static DelegateBridge __Hotfix_GetSolarSystemName_1;
        private static DelegateBridge __Hotfix_GetSpaceStationNameById;
        private static DelegateBridge __Hotfix_GetEnvCustomString;
        private static DelegateBridge __Hotfix_GetQuestAcceptStationName;
        private static DelegateBridge __Hotfix_GetQuestCompleteStationName;
        private static DelegateBridge __Hotfix_GetQuestAcceptSolarSystemName;
        private static DelegateBridge __Hotfix_GetQuestCompleteSolarSystemName;
        private static DelegateBridge __Hotfix_GetIntNumberString;
        private static DelegateBridge __Hotfix_GetFloatNumberString;
        private static DelegateBridge __Hotfix_GetGlobalNpcName;
        private static DelegateBridge __Hotfix_GetStationNpcName;
        private static DelegateBridge __Hotfix_GetQuestNameByConfigId;
        private static DelegateBridge __Hotfix_GetHireCaptainName;
        private static DelegateBridge __Hotfix_GetSpaceStationName;
        private static DelegateBridge __Hotfix_GetSkillName;
        private static DelegateBridge __Hotfix_GetGuildJobName;
        private static DelegateBridge __Hotfix_GetTechName;
        private static DelegateBridge __Hotfix_GetSignalName;
        private static DelegateBridge __Hotfix_GetGuildActionName;
        private static DelegateBridge __Hotfix_GetFormatTimeString;
        private static DelegateBridge __Hotfix_GetFormatSceneName;
        private static DelegateBridge __Hotfix_GetFormatGuildBuildingName;
        private static DelegateBridge __Hotfix_GetGiftPacakgeName;
        private static DelegateBridge __Hotfix_GetMonthCardName;
        private static DelegateBridge __Hotfix_get_Ctx;

        [MethodImpl(0x8000)]
        public static void ClearCache()
        {
        }

        [MethodImpl(0x8000)]
        protected static string CombineValueAndUnit(string value, string unit)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertCountClampToString(int count, int comparevalue, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertCountToUnitString(long count, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertCountToUnitString(ulong count, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertCurrencyToString(DigitFormatUtil.DigitFormatType type, ref ulong source)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertDistanceFloatWithUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertDistanceRoundWithUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertDistanceWithKmeterUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertDoubleToBillion(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToCeilIntString(float value, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToFloorIntString(float value, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToPercentage2BehindPointString(float value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToPercentageString(float value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToPercentageStringWithoutPrecent(float value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToRemain1BehindPointString(float value, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToRemain2BehindPointString(float value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertFloatToRoundedIntString(float value, int decimals = 0, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertIntToCommaSeparatedNum(double i, string headStr = "")
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertIntToRomeNum(int number)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertNumberToStringWithUnitPerSecondString(float number)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertRetainMaxTimeUnit(TimeSpan timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertSpeedFloatWithUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertSpeedRoundWithUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertStrToColorStr(string str, string colorStr)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeFloatWithSecondUnitToString(float time)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeFloatWithUnitToString(float time)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeFomartWithUnitToString(float time)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeRoundWithSecondUnitToString(float time)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeRoundWithUnitToString(float time)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeSecondToFormatTimeWithUnit(float time)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeSpanToDayOrHour(TimeSpan timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeSpanToHour(TimeSpan timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimeWithUnitToString(TimeSpan time, bool showDays = true, bool showHours = true, bool showMinutes = true, bool showSeconds = true)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertTimpSpanToString(TimeSpan timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertVolumeFloatWithUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertVolumeRoundWithUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string ConvertVolumeWithUnitToString(double value)
        {
        }

        [MethodImpl(0x8000)]
        public static string CovertVolumnToStringWithUnit(float volumn)
        {
        }

        [MethodImpl(0x8000)]
        public static string FormatCoolDownTimeString(TimeSpan time)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetAbbreviativeString(string sourceStr, int maxLenth, string endStr = "...")
        {
        }

        [MethodImpl(0x8000)]
        public static string GetAEONStringFormat()
        {
        }

        [MethodImpl(0x8000)]
        public static double GetAUValue()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCaptainSubRankStr(SubRankType subRank, bool withColor)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetCharShowLength(char c)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCreditRewardInfoString(CreditRewardInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCreditRewardItemInfoString(CreditRewardItemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCurrencyTypeName(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetCurrPlayerFullName()
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetCurrPlayerName()
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetCurrSolarSystemName()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDateStringFromDateTime(DateTime dt)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetEnvCustomString(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipLaunchEnergyCost(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetEquipLaunchEnergyCost(StoreItemType itemType, object confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFactionCreditLevelName(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetFloatNumberString(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetFormatGuildBuildingName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetFormatSceneName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetFormatTimeString(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetGiftPacakgeName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetGlobalNpcName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGrandFactionName(GrandFaction faction)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGrandFactionNameByFactionID(int factionID)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetGuildActionName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildItemStoreTabName(GuildItemListUIController.GuildStoreTabType guildStoreTabType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGuildItemStoreTabSpriteIconPath(GuildItemListUIController.GuildStoreTabType guildStoreTabType)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetGuildJobName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetHireCaptainName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetIntNumberString(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetItemConfigName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetItemCountStringWithColor(int needCount, long haveCount, DigitFormatUtil.DigitFormatType formatType = 0)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetMonthCardName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNameForPropertyCategory(PropertyCategory propertyCategory)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNeedPermissionStrByPermission(GuildPermission permission)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcShopItemCategoryName(NpcShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcShopItemTypeName(int type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcTalkerDialogCountdownString(float countdownTime)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetParamFormatStr(CommonFormatStringParamType paramType, FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static FormatStringParamInfo GetParamInfoByIndex(List<FormatStringParamInfo> paramInfos, int index)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetQuestAcceptSolarSystemName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetQuestAcceptStationName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetQuestCompleteSolarSystemName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetQuestCompleteStationName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetQuestNameByConfigId(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetQuestTypeName(QuestType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetRankNameByType(RankType rankType)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetShipConfigName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetSignalName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetSkillName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSlotTypeName(ShipEquipSlotType slot, bool isAmmo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetSolarSystemName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetSolarSystemName(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetSpaceStationName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSpaceStationNameById(int solarSystemId, int spaceStaionId)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetStationNpcName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringByFourNounsConcatenation(string str1, string str2, string str3, string str4)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringByThreeNounsConcatenation(string str1, string str2, string str3)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringByTitleAndNameConcatenation(string strTitle, string strName)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringByTwoNounsConcatenation(string str1, string str2)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringFromTimeSecond(float seconds)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringInStringTableWithId(StringTableId id, params object[] paramList)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStringWithCarriageReturn(string srcStr)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetStrShowLength(string str)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSubRankNameByType(SubRankType subRankType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSubRankStateNameByType(SubRankType subRankType)
        {
        }

        [MethodImpl(0x8000)]
        private static string GetTechName(FormatStringParamInfo paramInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTimeAndDateStringFromDateTimeWithoutSec(DateTime dt)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTimeSpanMaxUnitString(TimeSpan ts)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTimeStringFromDateTime(DateTime dt)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTimeStringFromTimeMs(uint ms)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTimeStringWithPostfix(TimeSpan time)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitAUPerSecondString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitAUString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitDayString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitGravityUnitString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitHourString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitKilogramString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitKMeterPerSecondeString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitKmeterString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitMeterPerSecondeString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitMeterString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitMinuteString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitSecondString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitStereString()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetUnitTemperatureString()
        {
        }

        [MethodImpl(0x8000)]
        public static string HtmlDecode(string src)
        {
        }

        [MethodImpl(0x8000)]
        public static string HtmlEncode(string src)
        {
        }

        [MethodImpl(0x8000)]
        public static string ParseFormatString(string formatStr, List<CommonFormatStringParamType> paramTypes, List<FormatStringParamInfo> paramInfos)
        {
        }

        [MethodImpl(0x8000)]
        public static List<FormatStringParamInfo> PrepareNpcDialogFormatStrParamInfos(List<CommonFormatStringParamType> paramTypes, List<FormatStringParamInfo> paramInfos, ConfigDataNpcTalkerInfo npcTalkerInfo, int questId)
        {
        }

        [MethodImpl(0x8000)]
        public static List<FormatStringParamInfo> PrepareQuestDescFormatStringParamInfos(int questId, List<CommonFormatStringParamType> paramTypes, List<FormatStringParamInfo> paramInfos)
        {
        }

        [MethodImpl(0x8000)]
        public static string ReplaceInputSpaceToVisionSpace(string content)
        {
        }

        protected static ProjectXPlayerContext Ctx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetParamInfoByIndex>c__AnonStorey0
        {
            internal int index;

            internal bool <>m__0(FormatStringParamInfo paramInfo) => 
                (paramInfo.Index == this.index);
        }
    }
}

