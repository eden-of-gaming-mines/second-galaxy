﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CaptainListUITask : UITaskBase
    {
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private List<LBStaticHiredCaptain> m_captainInfoCacheList;
        private CaptainSortType m_currCaptainSortType;
        private LBStaticHiredCaptain m_selectedCaptain;
        private List<ulong> m_wingManIdList;
        private CaptainListUIController m_mainCtrl;
        public static string ParamKey_SelectedCaptain = "SelectedCaptain";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBStaticHiredCaptain> EventOnCaptainSelectionChanged;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_SetShowState;
        private static DelegateBridge __Hotfix_UpdateCaptainLevel;
        private static DelegateBridge __Hotfix_GetCaptainItemRect_0;
        private static DelegateBridge __Hotfix_GetCaptainItemRect_1;
        private static DelegateBridge __Hotfix_RefreshCaptainList;
        private static DelegateBridge __Hotfix_UpdateSingleCaptain;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnCaptainItemClick;
        private static DelegateBridge __Hotfix_OnLevelSortToggleChanged;
        private static DelegateBridge __Hotfix_OnQualitySortToggleChanged;
        private static DelegateBridge __Hotfix_OnCaptainSortTypeChanged;
        private static DelegateBridge __Hotfix_OnCaptainAddExpByItem;
        private static DelegateBridge __Hotfix_OnCaptainSetWingManAck;
        private static DelegateBridge __Hotfix_GetCaptainInfoSortedList;
        private static DelegateBridge __Hotfix_CaptainInfoComparer;
        private static DelegateBridge __Hotfix_GetCurrCaptainFromUIIntent;
        private static DelegateBridge __Hotfix_GetSuitableCaptainListStartIndexFromCurrSelectIndex;
        private static DelegateBridge __Hotfix_RegisterPlayerContextEvent;
        private static DelegateBridge __Hotfix_UnregisteredPlayerContextEvent;
        private static DelegateBridge __Hotfix_GetCaptainIconRect;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_CurrentSelectedCaptain;
        private static DelegateBridge __Hotfix_add_EventOnCaptainSelectionChanged;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainSelectionChanged;

        public event Action<LBStaticHiredCaptain> EventOnCaptainSelectionChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CaptainListUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        private int CaptainInfoComparer(LBStaticHiredCaptain capA, LBStaticHiredCaptain capB)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCaptainIconRect()
        {
        }

        [MethodImpl(0x8000)]
        private void GetCaptainInfoSortedList()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCaptainItemRect(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCaptainItemRect(ulong captainInsId)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStaticHiredCaptain GetCurrCaptainFromUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuitableCaptainListStartIndexFromCurrSelectIndex(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainAddExpByItem(int result, ulong capInsId, long expAdd, int upLevel)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainItemClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainSetWingManAck(int rst)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainSortTypeChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLevelSortToggleChanged(UIControllerBase ctrl, bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQualitySortToggleChanged(UIControllerBase ctrl, bool selected)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshCaptainList()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterPlayerContextEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetShowState(bool show)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisteredPlayerContextEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainLevel(ulong captainInsId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleCaptain(LBStaticHiredCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public LBStaticHiredCaptain CurrentSelectedCaptain
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum CaptainSortType
        {
            CaptainQuality,
            CaptainLevel,
            WingMan
        }

        protected enum PipeLineStateMaskType
        {
            IsNeedLoadDynamicRes,
            IsStartOrResume,
            IsRefreshCaptainList,
            IsUpdateCaptainSelectState,
            IsCaptainSortTypeChanged,
            IsCaptainLevelUp,
            IsCaptainWingManStateChange
        }
    }
}

