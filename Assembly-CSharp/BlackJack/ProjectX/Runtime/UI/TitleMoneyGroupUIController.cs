﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class TitleMoneyGroupUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CurrencyType> EventOnAddCurrencyBtnClick;
        private CurrencyType m_currencyType;
        [AutoBind("./MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrencyNumberText;
        [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CurrencyIconImage;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnAddButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAddCurrencyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddCurrencyBtnClick;

        public event Action<CurrencyType> EventOnAddCurrencyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(CurrencyType currencyType, Dictionary<string, Object> dict)
        {
        }
    }
}

