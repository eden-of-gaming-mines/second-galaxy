﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class WarOverviewUITask : UITaskBase
    {
        public Action EventOnShareButtonClick;
        public Action<int> EventOnWarSolarSystemNameButtonClick;
        public Action EventOnBattleStatusEnd;
        public Action<uint, bool> EventOnGuildNameClick;
        public const string ParamKey_GuildBattleReportInfo = "GuildBattleReportInfo";
        public const string ParamKey_IsImmediate = "IsImmediate";
        public const string ParamKey_ShowPanel = "ShowPanel";
        public const string ParamKey_HidePanel = "HidePanel";
        private GuildBattleReportInfo m_battleReportinfo;
        private WarOverviewUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "WarOverviewUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartWarOverviewUITask;
        private static DelegateBridge __Hotfix_ShowWarOverviewPanel;
        private static DelegateBridge __Hotfix_HideWarOverviewPanel;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ShowWarOverviewPanelInternal;
        private static DelegateBridge __Hotfix_HideOverviewPanelInternal;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_OnTipBGButtonClick;
        private static DelegateBridge __Hotfix_OnBattleStatusEnd;
        private static DelegateBridge __Hotfix_OnGuildBattleStatusItemClick;
        private static DelegateBridge __Hotfix_OnWarSolarSystemNameButtonClick;
        private static DelegateBridge __Hotfix_OnGuildNameClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public WarOverviewUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void HideOverviewPanelInternal(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void HideWarOverviewPanel(bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBattleStatusEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildBattleStatusItemClick(GuildBattleStatus status)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildNameClick(bool isDefend)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWarSolarSystemNameButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowWarOverviewPanel(GuildBattleReportInfo reportInfo, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWarOverviewPanelInternal(GuildBattleReportInfo reportInfo, bool isImmediate, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static WarOverviewUITask StartWarOverviewUITask(GuildBattleReportInfo reportInfo, Action onResLoadEnd, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideOverviewPanelInternal>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal WarOverviewUITask $this;

            internal void <>m__0(UIProcess process, bool res)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowWarOverviewPanel>c__AnonStorey0
        {
            internal GuildBattleReportInfo reportInfo;
            internal bool isImmediate;
            internal Action<bool> onEnd;

            internal void <>m__0(bool res)
            {
                (UIManager.Instance.FindUITaskWithName("WarOverviewUITask", true) as WarOverviewUITask).ShowWarOverviewPanelInternal(this.reportInfo, this.isImmediate, this.onEnd);
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsImmediate,
            ShowPanel,
            HidePanel
        }
    }
}

