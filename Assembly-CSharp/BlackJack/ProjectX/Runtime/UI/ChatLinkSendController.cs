﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class ChatLinkSendController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnIssueButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCancelButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStarFieldToggleChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSolarSystemToggleChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnGuildToggleChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnAllianceToggleChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnWisperToggleChanged;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLocalToggleChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTeamToggleChanged;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CommonUIStateCtrl;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./DetailPanel/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx IssueButton;
        [AutoBind("./DetailPanel/ChannelGroup/StarFieldChannel/ToggleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx StarFieldToggle;
        [AutoBind("./DetailPanel/ChannelGroup/SolarSystemChannel/ToggleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SolarSystemToggle;
        [AutoBind("./DetailPanel/ChannelGroup/LocalChannel/ToggleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx LocalToggle;
        [AutoBind("./DetailPanel/ChannelGroup/GuildChannel/ToggleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx GuildToggle;
        [AutoBind("./DetailPanel/ChannelGroup/AllianceChannel/ToggleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx AllianceToggle;
        [AutoBind("./DetailPanel/ChannelGroup/TeamChannel/ToggleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TeamToggle;
        [AutoBind("./DetailPanel/ChannelGroup/WisperChannel/ToggleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx WisperToggle;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_IsStarFieldChannelCheck;
        private static DelegateBridge __Hotfix_IsSolarSystemChannelCheck;
        private static DelegateBridge __Hotfix_IsGuildChannelCheck;
        private static DelegateBridge __Hotfix_ClearToggleSelectedState;
        private static DelegateBridge __Hotfix_IsAllianceChannelCheck;
        private static DelegateBridge __Hotfix_IsLocalChannelCheck;
        private static DelegateBridge __Hotfix_IsWisperChannelCheck;
        private static DelegateBridge __Hotfix_IsTeamChannelCheck;
        private static DelegateBridge __Hotfix_SetStarFieldToggleState;
        private static DelegateBridge __Hotfix_SetSolarSystemToggleState;
        private static DelegateBridge __Hotfix_SetGuildToggleState;
        private static DelegateBridge __Hotfix_SetAllianceToggleState;
        private static DelegateBridge __Hotfix_SetLocalToggleState;
        private static DelegateBridge __Hotfix_SetWisperToggleState;
        private static DelegateBridge __Hotfix_SetTeamToggleState;
        private static DelegateBridge __Hotfix_OnIssueButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnStarFieldToggleChanged;
        private static DelegateBridge __Hotfix_OnSolarSystemToggleChanged;
        private static DelegateBridge __Hotfix_OnGuildToggleChanged;
        private static DelegateBridge __Hotfix_OnAllianceToggleChanged;
        private static DelegateBridge __Hotfix_OnLocalToggleChanged;
        private static DelegateBridge __Hotfix_OnWisperToggleChanged;
        private static DelegateBridge __Hotfix_OnTeamToggleChanged;
        private static DelegateBridge __Hotfix_GetChatLinkSendShowOrHideProcess;
        private static DelegateBridge __Hotfix_add_EventOnIssueButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnIssueButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStarFieldToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnStarFieldToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnSolarSystemToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSolarSystemToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnGuildToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnGuildToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnAllianceToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnAllianceToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnWisperToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnWisperToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnLocalToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnLocalToggleChanged;
        private static DelegateBridge __Hotfix_add_EventOnTeamToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnTeamToggleChanged;

        public event Action EventOnAllianceToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnIssueButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLocalToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSolarSystemToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStarFieldToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTeamToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWisperToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearToggleSelectedState()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetChatLinkSendShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAllianceChannelCheck()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsGuildChannelCheck()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLocalChannelCheck()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemChannelCheck()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsStarFieldChannelCheck()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsTeamChannelCheck()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsWisperChannelCheck()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceToggleChanged(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildToggleChanged(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLocalToggleChanged(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemToggleChanged(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarFieldToggleChanged(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamToggleChanged(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWisperToggleChanged(UIControllerBase uCtrl, bool currValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllianceToggleState(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildToggleState(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLocalToggleState(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemToggleState(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarFieldToggleState(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTeamToggleState(bool value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWisperToggleState(bool value)
        {
        }
    }
}

