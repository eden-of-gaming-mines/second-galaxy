﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using System;

    public class ChatItemControllerBase : UIControllerBase
    {
        public ChatInfo m_chatInfo;
        public CommonCaptainIconUIController CommanderItemCtrl;
        public const string m_commonItemPrefabName = "CommonCaptainUIPrefab";
    }
}

