﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipReloadAmmoReqNetTask : NetWorkTransactionTask
    {
        private int m_ackResulst;
        public int m_ammoAddCount;
        private readonly int m_hangarIndex;
        private readonly int m_slotGroupIndex;
        private readonly int m_srcItemIndex;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_HangarShipReloadAmmoAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_set_AckResult;

        [MethodImpl(0x8000)]
        public HangarShipReloadAmmoReqNetTask(int hangarIndex, int slotGroupIndex, int srcItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void HangarShipReloadAmmoAck(int result, int ammoAddCount)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }
    }
}

