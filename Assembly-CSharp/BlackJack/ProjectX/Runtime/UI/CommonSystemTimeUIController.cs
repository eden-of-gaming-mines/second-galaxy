﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public sealed class CommonSystemTimeUIController : UIControllerBase
    {
        private const string MainPanelOpen = "Show";
        private const string MainPanelClose = "Close";
        private const string DetailPanelOpen = "PanelOpen";
        private const string DetailPanelClose = "PanelClose";
        private const double MinUpdateIntervalTime = 1.0;
        private bool m_curDetailPanelOpen;
        private bool m_inProcess;
        private DateTime m_nextRefreshTime;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./TopBgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_topButton;
        [AutoBind("./TopBgButton/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_mainGalaxyTimeText;
        [AutoBind("./DetilsInFoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_detailPanelStateCtrl;
        [AutoBind("./DetilsInFoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailPanelCloseButton;
        [AutoBind("./DetilsInFoPanel/FrameImage/BGImage/DetailInfo/TimeText01/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_detailGalaxyTimeText;
        [AutoBind("./DetilsInFoPanel/FrameImage/BGImage/DetailInfo/TimeText02/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_detailLocalTimeText;
        private static DelegateBridge __Hotfix_GetPanelProcess;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnTopButtonClick;
        private static DelegateBridge __Hotfix_OnDetailPanelCloseButtonClick;

        [MethodImpl(0x8000)]
        public UIProcess GetPanelProcess(bool isShow, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailPanelCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTopButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [CompilerGenerated]
        private sealed class <GetPanelProcess>c__AnonStorey0
        {
            internal bool immedia;
            internal bool refresh;
            internal CommonSystemTimeUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Action<bool> onEnd)
            {
            }

            private sealed class <GetPanelProcess>c__AnonStorey1
            {
                internal Action<bool> onEnd;
                internal CommonSystemTimeUIController.<GetPanelProcess>c__AnonStorey0 <>f__ref$0;

                [MethodImpl(0x8000)]
                internal void <>m__0(bool ret)
                {
                }
            }
        }
    }
}

