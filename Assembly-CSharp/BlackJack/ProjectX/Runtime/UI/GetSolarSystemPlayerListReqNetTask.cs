﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GetSolarSystemPlayerListReqNetTask : NetWorkTransactionTask
    {
        public int m_ackResult;
        public SolarSystemPlayerListAck m_playerListAckInfo;
        private readonly int m_startIndex;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGetSolarSystemPlayerListAck;

        [MethodImpl(0x8000)]
        public GetSolarSystemPlayerListReqNetTask(int startIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetSolarSystemPlayerListAck(SolarSystemPlayerListAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

