﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TeamMemberInfoItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<TeamMemberInfoItemUIController> EventOnCharacterInfoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TeamMemberInfoItemUIController> EventOnShipInfoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TeamMemberInfoItemUIController> EventOnSolarSystemInfoButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<TeamMemberInfoItemUIController> EventOnMoveToPlayerPosButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_playerInfoStateCtrl;
        [AutoBind("./PlayerInfo/CharacterInfoRoot/CharacterIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_characterIconImage;
        [AutoBind("./PlayerInfo/CharacterInfoRoot/ProfessionImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_professionImage;
        [AutoBind("./PlayerInfo/CharacterInfoRoot/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterNameText;
        [AutoBind("./PlayerInfo/CharacterInfoRoot/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_characterLevelText;
        [AutoBind("./PlayerInfo/ShipInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shipDriveStateCtrl;
        [AutoBind("./PlayerInfo/ShipInfoRoot/HangarShipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipImageRoot;
        [AutoBind("./PlayerInfo/ShipInfoRoot/ShipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipTypeText;
        [AutoBind("./PlayerInfo/ShipInfoRoot/ShipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipNameText;
        [AutoBind("./PlayerInfo/SolarSystemInfoRoot/SolarSystemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_solarSystemNameText;
        [AutoBind("./PlayerInfo/PlayerStateRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_playerPositionStateCtrl;
        [AutoBind("./PlayerInfo/CharacterInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_characterInfoButton;
        [AutoBind("./PlayerInfo/ShipInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_shipInfoButton;
        [AutoBind("./PlayerInfo/SolarSystemInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_solarSystemInfoButton;
        [AutoBind("./GoToButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_moveToPlayerPosButton;
        [AutoBind("./PlayerInfo/CharacterInfoRoot/CaptainIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_captainIconDummy;
        public HangarShipListItemUIController m_teamMemberShipInfoItem;
        public CommonCaptainIconUIController m_captainIconCtrl;
        public string m_teamMemberGameUserId;
        public bool m_isSelfInfoItemUI;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPlayerGameUserId;
        private static DelegateBridge __Hotfix_SetSelfOrAnotherPlayer;
        private static DelegateBridge __Hotfix_SetCaptainIcon;
        private static DelegateBridge __Hotfix_SetPlayerShipExistence;
        private static DelegateBridge __Hotfix_SetShipInfo;
        private static DelegateBridge __Hotfix_SetSolarSystemName;
        private static DelegateBridge __Hotfix_SetCharacterPosState;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnCharacterInfoButtonClick;
        private static DelegateBridge __Hotfix_OnShipInfoButtonClick;
        private static DelegateBridge __Hotfix_OnSolarSystemInfoButtonClick;
        private static DelegateBridge __Hotfix_OnMoveToPlayerPosButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCharacterInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCharacterInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnShipInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSolarSystemInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSolarSystemInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMoveToPlayerPosButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMoveToPlayerPosButtonClick;

        public event Action<TeamMemberInfoItemUIController> EventOnCharacterInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TeamMemberInfoItemUIController> EventOnMoveToPlayerPosButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TeamMemberInfoItemUIController> EventOnShipInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TeamMemberInfoItemUIController> EventOnSolarSystemInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharacterInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMoveToPlayerPosButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSystemInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainIcon(TeamMainUIController.TeamMemberUIItemInfo info, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharacterPosState(bool isInSpace)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlayerGameUserId(string id)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlayerShipExistence(bool isShipExist)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelfOrAnotherPlayer(bool isSelfPlayer)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipInfo(Sprite shipSprite, Sprite shipTypeIcon, Sprite shipBGImage, string name, string shipTypeName, RankType rank, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemName(string name)
        {
        }
    }
}

