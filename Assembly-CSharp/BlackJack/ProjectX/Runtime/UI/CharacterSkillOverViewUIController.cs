﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CharacterSkillOverViewUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PropertyCategory> EventOnCategoryPanelClick;
        [AutoBind("./WeaponGroup/ActivateTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponSkillNumberText;
        [AutoBind("./DefenseGroup/ActivateTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DefenseSkillNumberText;
        [AutoBind("./EngineeringGroup/ActivateTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EngineeringSkillNumberText;
        [AutoBind("./ShipGroup/ActivateTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DrivingSkillNumberText;
        [AutoBind("./WeaponGroup/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponDescText;
        [AutoBind("./DefenseGroup/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DefenseDescText;
        [AutoBind("./EngineeringGroup/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EngineeringDescText;
        [AutoBind("./ShipGroup/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DrivingDescText;
        [AutoBind("./WeaponGroup/Button/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image WeaponProgressBar;
        [AutoBind("./DefenseGroup/Button/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image DefenseProgressBar;
        [AutoBind("./EngineeringGroup/Button/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image EngineeringProgressBar;
        [AutoBind("./ShipGroup/Button/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image DrivingProgressBar;
        [AutoBind("./WeaponGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WeaponPanelEffectCtrl;
        [AutoBind("./DefenseGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DefensePanelEffectCtrl;
        [AutoBind("./EngineeringGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ElectricPanelEffectCtrl;
        [AutoBind("./ShipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DrivingPanelEffectCtrl;
        [AutoBind("./WeaponGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WeaponButton;
        [AutoBind("./DefenseGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DefenseButton;
        [AutoBind("./EngineeringGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ElectricButton;
        [AutoBind("./ShipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DrivingButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetLearntSkillNumber;
        private static DelegateBridge __Hotfix_SetSkillLearntProgerssBar;
        private static DelegateBridge __Hotfix_SetPropertyCategoryDesc;
        private static DelegateBridge __Hotfix_PlayCategoryPanelSelectEffect;
        private static DelegateBridge __Hotfix_OnWeaponButtonClick;
        private static DelegateBridge __Hotfix_OnDefenseButtonClick;
        private static DelegateBridge __Hotfix_OnElectricButtonClick;
        private static DelegateBridge __Hotfix_OnDrivingButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCategoryPanelClick;
        private static DelegateBridge __Hotfix_remove_EventOnCategoryPanelClick;

        public event Action<PropertyCategory> EventOnCategoryPanelClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDefenseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDrivingButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnElectricButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWeaponButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCategoryPanelSelectEffect(PropertyCategory category)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLearntSkillNumber(PropertyCategory category, int num)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyCategoryDesc(PropertyCategory category, string desc)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillLearntProgerssBar(PropertyCategory category, float value)
        {
        }
    }
}

