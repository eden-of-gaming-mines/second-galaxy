﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ShipCustomGuildTemplateUpdateNameReqNetTask : NetWorkTransactionTask
    {
        public bool m_updated;
        private readonly int m_index;
        private readonly string m_name;
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildShipTemplateUpdated;

        [MethodImpl(0x8000)]
        private ShipCustomGuildTemplateUpdateNameReqNetTask(int index, string name)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildShipTemplateUpdated(bool updated)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(int index, string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

