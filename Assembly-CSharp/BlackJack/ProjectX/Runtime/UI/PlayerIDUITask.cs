﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class PlayerIDUITask : UITaskBase
    {
        private PlayerIDUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "PlayerIDUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowPlayerIDWindow;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_BringTipWindowToTop;
        private static DelegateBridge __Hotfix_OnCopyButtonClick;
        private static DelegateBridge __Hotfix_OnDetermineButtonClick;
        private static DelegateBridge __Hotfix_get_PlyCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public PlayerIDUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void BringTipWindowToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCopyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetermineButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowPlayerIDWindow(UIIntent returnToUIIntent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlyCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

