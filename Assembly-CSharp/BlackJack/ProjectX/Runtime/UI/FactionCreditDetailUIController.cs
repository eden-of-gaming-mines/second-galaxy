﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class FactionCreditDetailUIController : UIControllerBase
    {
        private FactionCreditLevel m_SelFactionCreditLevel = FactionCreditLevel.FactionCreditLevel_Neutral;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> OnEventNpcInfoMenuItem;
        private int m_factionId;
        private const int POOL_SIZE = 10;
        protected const string TabRewardItemName = "FactionCreditNpcInfoMenuItemUIPrefab";
        private GameObject m_TabRewardItemTemplateGo;
        private Dictionary<string, UnityEngine.Object> m_resData;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, FactionCreditLevel, int> OnEventLittleNpcFlagClick;
        [AutoBind("./Main", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./Main/LicenseInfoTitle/FlagGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FactionFlagGroupState;
        [AutoBind("./Main/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./Main/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./Main/LicenseInfoTitle/PointImage/HonorText/HonorNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HonorNumberText;
        [AutoBind("./Main/BarGroup/AwardBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image AwardBarImage;
        [AutoBind("./Main/BarGroup/DegreePointImage01/DegreeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DegreeNumberText01;
        [AutoBind("./Main/BarGroup/DegreePointImage02/DegreeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DegreeNumberText02;
        [AutoBind("./Main/BarGroup/DegreePointImage03/DegreeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DegreeNumberText03;
        [AutoBind("./Main/BarGroup/DegreePointImage04/DegreeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DegreeNumberText04;
        [AutoBind("./Main/BarGroup/DegreePointImage05/DegreeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DegreeNumberText05;
        [AutoBind("./Main/BarGroup/DegreePointImage06/DegreeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DegreeNumberText06;
        [AutoBind("./Main/BarGroup/FriendlyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FriendlyText;
        [AutoBind("./Main/BarGroup/FriendlyText/BarNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BarNumberText;
        [AutoBind("./Main/BarGroup/AgreementButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AgreementButton;
        [AutoBind("./Main/BarGroup/AgreementButton/CornerImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CornerImage;
        [AutoBind("./Main/BarGroup/AgreementButton/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestCountText;
        [AutoBind("./Main/ToggleContent", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ToggleContent;
        [AutoBind("./Main/ToggleContent/Grade01Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Grade01Toggle;
        [AutoBind("./Main/ToggleContent/Grade01Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Grade01ToggleState;
        [AutoBind("./Main/ToggleContent/Grade02Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Grade02Toggle;
        [AutoBind("./Main/ToggleContent/Grade02Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Grade02ToggleState;
        [AutoBind("./Main/ToggleContent/Grade03Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Grade03Toggle;
        [AutoBind("./Main/ToggleContent/Grade03Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Grade03ToggleState;
        [AutoBind("./Main/ToggleContent/Grade04Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Grade04Toggle;
        [AutoBind("./Main/ToggleContent/Grade04Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController Grade04ToggleState;
        [AutoBind("./Main/AgencyGroup/NpcGroup/ShipType/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipFrigate;
        [AutoBind("./Main/AgencyGroup/NpcGroup/ShipType/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipDestroyer;
        [AutoBind("./Main/AgencyGroup/NpcGroup/ShipType/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipCruiser;
        [AutoBind("./Main/AgencyGroup/NpcGroup/ShipType/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipBattleCruiser;
        [AutoBind("./Main/AgencyGroup/NpcGroup/ShipType/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipBattleShip;
        [AutoBind("./Main/AgencyGroup/NpcGroup/ShipType/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipIndustryShip;
        [AutoBind("./Main/AgencyGroup/NpcGroup/LeaveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LeaveButton;
        [AutoBind("./Main/AgencyGroup/NpcGroup/ShopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShopButton;
        [AutoBind("./Main/AgencyGroup/NpcGroup/NPCNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NPCNameText;
        [AutoBind("./Main/AgencyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AgencyGroupUIState;
        [AutoBind("./Main/AgencyGroup/NpcGroup/NpcTitleText/NpcInfoTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NpcInfoTitleText;
        [AutoBind("./Main/AgencyGroup/NpcGroup/NPCImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image NPCImage;
        [AutoBind("./Main/AgencyGroup/NpcGroup/Scroll View/Viewport/Content/NPCContentText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NPCContentText;
        [AutoBind("./Main/AgencyGroup/NpcGroup/PlaceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlaceText;
        [AutoBind("./Main/AgencyGroup/NpcGroup/DistanceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DistanceText;
        [AutoBind("./Main/LicenseInfoTitle/ColourImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CreditLevelColourImageState;
        [AutoBind("./Main/AgencyGroup/NpcGroup/PlaceText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PlaceTextToStartMap;
        [AutoBind("./Main/AgencyGroup/FlagGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect LoopScrollView;
        [AutoBind("./Main/AgencyGroup/FlagGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool SelfEasyObjectPool;
        [AutoBind("./Main/TemplateRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TemplateRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTemplateItemEx;
        private static DelegateBridge __Hotfix_Refill;
        private static DelegateBridge __Hotfix_GetChooseFlagItemIndex;
        private static DelegateBridge __Hotfix_SetChooseFlag;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolTablRewardItemCreated;
        private static DelegateBridge __Hotfix_OnTablRewardItemFill;
        private static DelegateBridge __Hotfix_OnButtonNpcInfoMenuItemClick;
        private static DelegateBridge __Hotfix_InitCreditLevelButtonState;
        private static DelegateBridge __Hotfix_SetCreditLevelButtonChooseState;
        private static DelegateBridge __Hotfix_ClearLevelButtonState;
        private static DelegateBridge __Hotfix_SetDefaultToggleState;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetFactionCreditLevelInfo;
        private static DelegateBridge __Hotfix_UpdateViewLittleNpc;
        private static DelegateBridge __Hotfix_UpdateShipType;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_add_OnEventNpcInfoMenuItem;
        private static DelegateBridge __Hotfix_remove_OnEventNpcInfoMenuItem;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_OnEventLittleNpcFlagClick;
        private static DelegateBridge __Hotfix_remove_OnEventLittleNpcFlagClick;

        public event Action<int, FactionCreditLevel, int> OnEventLittleNpcFlagClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> OnEventNpcInfoMenuItem
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearLevelButtonState(FactionCreditLevel selfLv, bool stop = false)
        {
        }

        [MethodImpl(0x8000)]
        private GameObject CreateTemplateItemEx(string name, Transform parent, bool active)
        {
        }

        [MethodImpl(0x8000)]
        public int GetChooseFlagItemIndex(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataFactionCreditLevelInfo GetFactionCreditLevelInfo(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        public void InitCreditLevelButtonState(FactionCreditLevel selfLv, FactionCreditLevel selectLv)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonNpcInfoMenuItemClick(UIControllerBase crl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolTablRewardItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTablRewardItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Refill(int count, int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChooseFlag(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCreditLevelButtonChooseState(FactionCreditLevel selectLv, FactionCreditLevel selfLv)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDefaultToggleState(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipType(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(FactionCreditLevel factionLevel, int factionId, ConfigDataFactionCreditLevelInfo fcLvInfo, float curServerVal, long tradeMoney, List<ConfigDataFactionCreditNpcInfo> littleFactionList, List<ConfigDataFactionCreditLevelRewardInfo> regionListLv, int enoughCount, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewLittleNpc(ConfigDataFactionCreditNpcInfo config, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

