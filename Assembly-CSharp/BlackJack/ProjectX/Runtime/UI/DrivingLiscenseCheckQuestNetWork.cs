﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class DrivingLiscenseCheckQuestNetWork : NetWorkTransactionTask
    {
        private int m_skillId;
        private int m_level;
        private int m_ackResult;
        private int m_questInstanceId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnDrivingAssessmentStartAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_get_QuestInstanceId;

        [MethodImpl(0x8000)]
        public DrivingLiscenseCheckQuestNetWork(int skillID, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDrivingAssessmentStartAck(int result, int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int QuestInstanceId
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

