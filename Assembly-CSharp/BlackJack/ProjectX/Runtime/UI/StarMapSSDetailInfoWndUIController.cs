﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class StarMapSSDetailInfoWndUIController : UIControllerBase
    {
        private GDBSolarSystemInfo m_currSolarSystemInfo;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backgroundButton;
        [AutoBind("./WatchStarMapButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_watchStarMapButton;
        [AutoBind("./JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        [AutoBind("./SendToChatButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sendChatLinkMsgButton;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_solarSystemNameText;
        [AutoBind("./SolarSystemInfoTexts/FactionInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_solarSystemFactionText;
        [AutoBind("./SolarSystemInfoTexts/StarfieldInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_belowStarfieldNameText;
        [AutoBind("./SolarSystemInfoTexts/StargroupInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_belowStargroupNameText;
        [AutoBind("./SolarSystemInfoTexts/SecurityLevelInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_securityLevelText;
        [AutoBind("./SolarSystemInfoTexts/SignalLevelInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_signalLevelText;
        [AutoBind("./SolarSystemInfoTexts/DistanceInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_distanceText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetSolarSystemInfo;
        private static DelegateBridge __Hotfix_GetSolarSystemInfo;
        private static DelegateBridge __Hotfix_SetSolarSystemName;
        private static DelegateBridge __Hotfix_SetSolarSystemFaction;
        private static DelegateBridge __Hotfix_SetBelowStarfieldName;
        private static DelegateBridge __Hotfix_SetBelowStargroupName;
        private static DelegateBridge __Hotfix_SetSecurityLevelInfo;
        private static DelegateBridge __Hotfix_SetSignalLevel;
        private static DelegateBridge __Hotfix_SetDistanceInfo;
        private static DelegateBridge __Hotfix_CreateSSDetailWindowProcess;

        [MethodImpl(0x8000)]
        public UIProcess CreateSSDetailWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public GDBSolarSystemInfo GetSolarSystemInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBelowStarfieldName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBelowStargroupName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDistanceInfo(int jumpCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSecurityLevelInfo(float level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSignalLevel(int val)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemFaction(string faction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemInfo(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemName(string name)
        {
        }
    }
}

