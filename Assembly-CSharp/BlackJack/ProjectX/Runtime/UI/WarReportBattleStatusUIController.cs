﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class WarReportBattleStatusUIController : UIControllerBase
    {
        public Action<GuildBattleStatus> EventOnGuildBattleStatusIconClick;
        private GuildBattleType? m_currBattleType;
        private GuildBattleStatus m_currBattleStatus = GuildBattleStatus.BattleEnd;
        private DateTime m_lastFightingEndTime;
        private DateTime m_nextFightingTime;
        private DateTime m_nextTickTime;
        [AutoBind("./StageGroup01", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PrepareStatusButton;
        [AutoBind("./StageGroup02", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FirstHalfStatusButton;
        [AutoBind("./StageGroup03", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ReinforcementStatusButton;
        [AutoBind("./StageGroup04", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SecondHalfStatusButton;
        [AutoBind("./StageGroup05", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DeclarationStatusButton;
        [AutoBind("./StageGroup01/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBar1;
        [AutoBind("./StageGroup02/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBar2;
        [AutoBind("./StageGroup03/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBar3;
        [AutoBind("./StageGroup04/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBar4;
        [AutoBind("./StageGroup05/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBar5;
        [AutoBind("./StageGroup01/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform BeginImage1;
        [AutoBind("./StageGroup03/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform BeginImage3;
        [AutoBind("./StageGroup05/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform BeginImage5;
        [AutoBind("./StageGroup01", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleStage1ItemStateCtrl;
        [AutoBind("./StageGroup02", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleStage2ItemStateCtrl;
        [AutoBind("./StageGroup03", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleStage3ItemStateCtrl;
        [AutoBind("./StageGroup04", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleStage4ItemStateCtrl;
        [AutoBind("./StageGroup05", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleStage5ItemStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleTypeStateCtrl;
        private const string WarState = "Underway";
        private const string CdState = "Cd";
        private const string NormalState = "NotStarted";
        private const string EndState = "End";
        private const string BuildingBattleState = "Stage4";
        private const string SovereignBattleState = "Stage5";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateGuildBattleInfo;
        private static DelegateBridge __Hotfix_SetState4BuildingBattle;
        private static DelegateBridge __Hotfix_SetState4SovereignBattle;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnPrepareStatusButtonClick;
        private static DelegateBridge __Hotfix_OnFirstHalfStatusButtonClick;
        private static DelegateBridge __Hotfix_OnReinforcementStatusButtonClick;
        private static DelegateBridge __Hotfix_OnSecondHalfStatusButtonClick;
        private static DelegateBridge __Hotfix_OnDeclarationStatusButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeclarationStatusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFirstHalfStatusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrepareStatusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReinforcementStatusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSecondHalfStatusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetState4BuildingBattle(GuildBattleStatus status)
        {
        }

        [MethodImpl(0x8000)]
        private void SetState4SovereignBattle(GuildBattleStatus status)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBattleInfo(GuildBattleType battleType, GuildBattleStatus status, DateTime lastStatusEndTime, DateTime nextStatusStartTime, GuildBuildingType buildType)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

