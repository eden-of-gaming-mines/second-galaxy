﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildActionQuestItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        private GuildActionInfo m_actionInfo;
        private ConfigDataGuildActionInfo m_actionData;
        public ScrollItemBaseUIController m_srollItem;
        public int m_curEnterSolarSysId;
        [AutoBind("./DefeatedBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DefeatObj;
        [AutoBind("./PeopleNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PeopleNumberText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./ExperienceBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ExperienceBarImage;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemState;
        [AutoBind("./RecommendNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RecommendNumberText;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_GetConfigId;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_UpdateView_1;
        private static DelegateBridge __Hotfix_UpdateView_0;
        private static DelegateBridge __Hotfix_UpdateCommon;
        private static DelegateBridge __Hotfix_SetIssueSelected;
        private static DelegateBridge __Hotfix_SetUnderwaySelected;
        private static DelegateBridge __Hotfix_UpdateUnderwayItemState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;

        [MethodImpl(0x8000)]
        public int GetConfigId()
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIssueSelected(bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUnderwaySelected(bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCommon(ConfigDataGuildActionInfo data, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUnderwayItemState(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(GuildActionInfo info, bool isSelected, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(ConfigDataGuildActionInfo data, bool isSelected, int curEnterSolarSysId, Dictionary<string, UnityEngine.Object> resData)
        {
        }
    }
}

