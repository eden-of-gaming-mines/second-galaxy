﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildCompensationDetailUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDonateAllConfirmClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LostItem> EventOnDonateSingleConfirmClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LostItem> EventOnDonateBtnClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCommonIconClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, UIControllerBase, LossCompensation> EventOnCaptionIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CurrencyType> EventOnAddCurrencyBtnClick;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private LossCompensation m_lossCompInfo;
        private List<LostItem> m_sortedLostItemList;
        private bool m_isSelfCompensation;
        public GuildCompensationKillInfoController m_victimCtrl;
        public GuildCompensationKillInfoController m_killerCtrl;
        public GuildCompensationDonateHintUIController m_donateHintCtrl;
        public GuildCompensationHintUIController m_cofirmHintCtrl;
        public List<GuildCompensationContributItemUIController> m_contributeCtrlList;
        public GameObject m_itemIconTemplate;
        public CommonCountDownWithDaysUIController m_countDownUICtrl;
        [AutoBind("./GuildHintRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform GuildHintRoot;
        [AutoBind("./DonateHint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DonateHintGbj;
        [AutoBind("./VictimDetailRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject VictimDetailObj;
        [AutoBind("./KillerDetailRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject KillerDetailObj;
        [AutoBind("./ContributionGroup/ScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemRoot;
        [AutoBind("./ContributionGroup/TodayTimesText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TodayTimesText;
        [AutoBind("./CompensationOverview/SmashTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SmashTimeText;
        [AutoBind("./CompensationOverview/SmashPositionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SmashPositionText;
        [AutoBind("./CompensationOverview/LeftTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LeftTimeText;
        [AutoBind("./CompensationOverview/TotalLossText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TotalLossText;
        [AutoBind("./CompensationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CompensationButton;
        [AutoBind("./RefuseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RefuseButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./ContributionGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool EasyPool;
        [AutoBind("./ContributionGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ScrollRect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RootState;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowDonateList;
        private static DelegateBridge __Hotfix_CloseDonateList;
        private static DelegateBridge __Hotfix_ShowDonateHintPanel;
        private static DelegateBridge __Hotfix_OnContributeItemCreateed;
        private static DelegateBridge __Hotfix_OnItemNeedFill;
        private static DelegateBridge __Hotfix_OnDonateBtnClick;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_OnWinnerCaptionIconClick;
        private static DelegateBridge __Hotfix_OnLoserCaptionIconClick;
        private static DelegateBridge __Hotfix_OnDonateSingleConfirmClick;
        private static DelegateBridge __Hotfix_OnDonateAllConfirmClick;
        private static DelegateBridge __Hotfix_OnAddCurrencyBtnClick;
        private static DelegateBridge __Hotfix_UpdateTotalLoss;
        private static DelegateBridge __Hotfix_add_EventOnDonateAllConfirmClick;
        private static DelegateBridge __Hotfix_remove_EventOnDonateAllConfirmClick;
        private static DelegateBridge __Hotfix_add_EventOnDonateSingleConfirmClick;
        private static DelegateBridge __Hotfix_remove_EventOnDonateSingleConfirmClick;
        private static DelegateBridge __Hotfix_add_EventOnDonateBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnDonateBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptionIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptionIconClick;
        private static DelegateBridge __Hotfix_add_EventOnAddCurrencyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnAddCurrencyBtnClick;

        public event Action<CurrencyType> EventOnAddCurrencyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, UIControllerBase, LossCompensation> EventOnCaptionIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCommonIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDonateAllConfirmClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LostItem> EventOnDonateBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LostItem> EventOnDonateSingleConfirmClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CloseDonateList()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetProcess(bool show, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddCurrencyBtnClick(CurrencyType currencyType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnContributeItemCreateed(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateAllConfirmClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDonateSingleConfirmClick(LostItem item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemNeedFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoserCaptionIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWinnerCaptionIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowDonateHintPanel(LostItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowDonateList(List<LostItem> loss)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTotalLoss()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(LossCompensation comp, List<LostItem> sortedLostList, Dictionary<string, UnityEngine.Object> dict)
        {
        }

        [CompilerGenerated]
        private sealed class <ShowDonateHintPanel>c__AnonStorey1
        {
            internal LostItem itemInfo;
            internal GuildCompensationDetailUIController $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.m_donateHintCtrl.UpdateView(this.itemInfo, this.$this.m_resDict);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowDonateList>c__AnonStorey0
        {
            internal List<LostItem> loss;
            internal GuildCompensationDetailUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess p, bool r)
            {
            }
        }
    }
}

