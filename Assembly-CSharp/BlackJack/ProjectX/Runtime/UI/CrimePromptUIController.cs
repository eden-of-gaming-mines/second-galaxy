﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CrimePromptUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./DetailPanel/CheckGroup/CheckGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_checkBoxStateCtrl;
        [AutoBind("./DetailPanel/CheckGroup/CheckGroup/CheckBoxButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_checkBoxButton;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_cancelButtonStateCtrl;
        [AutoBind("./DetailPanel/CancelButton/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        private static DelegateBridge __Hotfix_CreateShowOrHideProcess;
        private static DelegateBridge __Hotfix_UpdateCheckBoxState;
        private static DelegateBridge __Hotfix_UpdateCancelButtonState;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateShowOrHideProcess(bool isShow = true, bool isRangeAttack = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCancelButtonState(bool isGray)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCheckBoxState(bool isCheck)
        {
        }
    }
}

