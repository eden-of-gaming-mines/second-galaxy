﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public sealed class GuildFlagShipHangarShipListItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        public ScrollItemBaseUIController m_scrollCtrl;
        private IStaticFlagShipDataContainer m_shipInfo;
        private bool m_deloying;
        private DateTime m_nextRefreshDeloyingTime;
        private DateTime m_deloyCompleteTime;
        private float m_refreshIntervalTime;
        private const string ShipStateNormal = "Normal";
        private const string ShipStateEmpty = "Empty";
        private const string ShipStateAdd = "Add";
        private const string ShipStateNoPermission = "NoPermission";
        private const string ShipStateLocked = "Unlock";
        private const string ShipStateDestroy = "Damage";
        private const string ShipStateDeploy = "Deploy";
        private const string ShipStateLeave = "Leave";
        private const string ShipStateName = "Name";
        private const string SelectedState = "Show";
        private const string UnselectedState = "Close";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnShipUnpackingComplete;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BGImage;
        [AutoBind("./ShipIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipIconImage;
        [AutoBind("./ClickFrameImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelectedStateCtrl;
        [AutoBind("./EnergyBgImage/EnergyBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image EnergyBarImage;
        [AutoBind("./DeployGroup/DeployTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText DeployTimeTex;
        [AutoBind("./SlotTypeBGImage/ShipTypeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipTypeImage;
        [AutoBind("./UnlockCondition/UnLock/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnLockText;
        [AutoBind("./RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image RankImage;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./NameImage/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipHangarShipListItemInfo_1;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipHangarShipListItemInfo_2;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipHangarShipListItemInfo_0;
        private static DelegateBridge __Hotfix_SetSelectState;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateFuelCount;
        private static DelegateBridge __Hotfix_UpdateUnpackingTime;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnShipUnpackingComplete;
        private static DelegateBridge __Hotfix_remove_EventOnShipUnpackingComplete;

        public event Action<UIControllerBase> EventOnShipUnpackingComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool careButtonClick = true, Action<UIControllerBase> onShipUnpackingComplete = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectState(bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFuelCount()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildFlagShipHangarShipListItemInfo(Dictionary<string, Object> dynamicResCacheDict, int unlockSlotCount, int buildingLevel, bool addWhenEmpty)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFlagShipHangarShipListItemInfo(ILBStaticFlagShip hangarShip, Dictionary<string, Object> dynamicResCacheDict, int unlockSlotCount, int buildingLevel, bool addWhenEmpty = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFlagShipHangarShipListItemInfo(IStaticFlagShipDataContainer hangarShip, Dictionary<string, Object> dynamicResCacheDict, int unlockSlotCount, int buildingLevel, bool addWhenEmpty = false)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUnpackingTime(DateTime currTime)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

