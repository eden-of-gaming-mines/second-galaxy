﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class NewShipGetUITask : UITaskBase
    {
        public const string ParamKey_StaticShip = "StaticShip";
        public const string ParamKey_ShipConfigId = "ShipConfigId";
        public const string UIMode_StaticShip = "StaticShipMode";
        public const string UIMode_ShipItem = "ShipItemMode";
        private NewShipGetUIController m_mainCtrl;
        private ThreeDModelViewController m_itemModelViewCtrl;
        private ILBStaticPlayerShip m_newGetStaticShip;
        private int m_newGetShipConfigId;
        private ConfigDataSpaceShipInfo m_newGetShipConfigInfo;
        private GameObject m_cachedShipModelGo;
        private int m_selHighSlotIndex;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private FakeLBStoreItem m_currFakeItem;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBGButtonClick;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNewShipGetUITask_0;
        private static DelegateBridge __Hotfix_StartNewShipGetUITask_1;
        private static DelegateBridge __Hotfix_RegisterBGButtonEvent;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCacheStaticShipMode;
        private static DelegateBridge __Hotfix_UpdateDataCacheShipItemMode;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectDynamicResForStaticShipMode;
        private static DelegateBridge __Hotfix_CollectDynamicResForShipItemMode;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewStaticShipMode;
        private static DelegateBridge __Hotfix_UpdateViewShipItemMode;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnShipHighSlotWeaponClick;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowHighSlotItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskPasue;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_GetShipModelResPath;
        private static DelegateBridge __Hotfix_GetShipModelAsset;
        private static DelegateBridge __Hotfix_PrepareShipViewModel;
        private static DelegateBridge __Hotfix_PrefabItemViewModel;
        private static DelegateBridge __Hotfix_get_m_itemModelViewLayer;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public NewShipGetUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected List<string> CollectDynamicResForShipItemMode()
        {
        }

        [MethodImpl(0x8000)]
        protected List<string> CollectDynamicResForStaticShipMode()
        {
        }

        [MethodImpl(0x8000)]
        private GameObject GetShipModelAsset()
        {
        }

        [MethodImpl(0x8000)]
        private string GetShipModelResPath()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskPasue(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipHighSlotWeaponClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private Bounds PrefabItemViewModel(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private Bounds PrepareShipViewModel(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterBGButtonEvent(Action action)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowHighSlotItemSimpleInfoPanel(int slotIdx)
        {
        }

        [MethodImpl(0x8000)]
        public static NewShipGetUITask StartNewShipGetUITask(ILBStaticPlayerShip newShip)
        {
        }

        [MethodImpl(0x8000)]
        public static NewShipGetUITask StartNewShipGetUITask(int shipConfigId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCacheShipItemMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCacheStaticShipMode()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewShipItemMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewStaticShipMode()
        {
        }

        private SceneLayerBase m_itemModelViewLayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

