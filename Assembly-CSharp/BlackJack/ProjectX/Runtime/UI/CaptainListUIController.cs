﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainListUIController : UIControllerBase
    {
        public int m_selCaptainIndex;
        public List<LBStaticHiredCaptain> m_captainInfoList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private List<ulong> m_wingManIdList;
        private string m_captainListUIItemPoolName;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCaptainUIItemClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./CaptainListScrollView/RankGroup/Level", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx LevelSort;
        [AutoBind("./CaptainListScrollView/RankGroup/Quality", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx QualitySort;
        [AutoBind("./CaptainListScrollView/RankGroup/CaptainNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNumberText;
        [AutoBind("./CaptainListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect CaptainListScrollRect;
        [AutoBind("./CaptainListScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool CaptainListUIItemPool;
        [AutoBind("./CaptainListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TempleteItemRoot;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateCaptainInfoList;
        private static DelegateBridge __Hotfix_UpdateSingleCaptainItemSelected;
        private static DelegateBridge __Hotfix_UpdateSingleCaptainLevelUp;
        private static DelegateBridge __Hotfix_UpdateSingleCaptainState;
        private static DelegateBridge __Hotfix_GetCaptainItemRect;
        private static DelegateBridge __Hotfix_ResetSortToggleState;
        private static DelegateBridge __Hotfix_ShowOrHideCaptainList;
        private static DelegateBridge __Hotfix_OnCaptainUIItemClick;
        private static DelegateBridge __Hotfix_OnCaptainUIItemFill;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_add_EventOnCaptainUIItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainUIItemClick;

        public event Action<int> EventOnCaptainUIItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetCaptainItemRect(ulong captainInsId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainUIItemClick(UIControllerBase captainItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainUIItemFill(UIControllerBase captainItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetSortToggleState(CaptainListUITask.CaptainSortType sort)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideCaptainList(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCaptainInfoList(List<LBStaticHiredCaptain> captainList, List<ulong> wingManIdList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int selectIndex = 0, int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleCaptainItemSelected(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleCaptainLevelUp(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleCaptainState(int selectIndex)
        {
        }
    }
}

