﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapStargroupNameConfig
    {
        [Header("星座名称开始显示的缩放比")]
        public float m_stargroupNameTextVisibleScaleMin;
        [Header("星座名称显示透明度达到完全不透明时的缩放比")]
        public float m_stargroupNameTextVisibleScaleMax;
        [Header("星座名称Text完全不透明时候的透明度，取0-1")]
        public float m_stargroupNameTextAlphaMax;
        [Header("星座名称的背景完全不透明时候的透明度，取0-1")]
        public float m_stargroupNameBGAlphaMax;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

