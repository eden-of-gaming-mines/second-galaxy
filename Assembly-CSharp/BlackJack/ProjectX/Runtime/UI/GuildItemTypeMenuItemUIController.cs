﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildItemTypeMenuItemUIController : CommonMenuItemUIController
    {
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx GuildItemTypeMenuItemToggleEx;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnToggleClick;
        private static DelegateBridge __Hotfix_OnSelectImp;
        private static DelegateBridge __Hotfix_OnMenuItemFill;
        private static DelegateBridge __Hotfix_OnFreeToUnusedPool;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFreeToUnusedPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnMenuItemFill(object data, Dictionary<string, Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnSelectImp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleClick(bool isOn)
        {
        }
    }
}

