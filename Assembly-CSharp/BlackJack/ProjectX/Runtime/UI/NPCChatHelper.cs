﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class NPCChatHelper
    {
        private static Random m_random = new Random();
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetDialogAudioResPathById;
        private static DelegateBridge __Hotfix_GetDialogDuringTime;
        private static DelegateBridge __Hotfix_GetIsRemoteNpc;
        private static DelegateBridge __Hotfix_GetNpcEmotion4DialogId;
        private static DelegateBridge __Hotfix_GetNpcTalkerInfoByNpcDNId;
        private static DelegateBridge __Hotfix_GetNpcTalkerInfoForDialog;
        private static DelegateBridge __Hotfix_GetNpcNameByNpcDNId;
        private static DelegateBridge __Hotfix_GetNpcNameByTalkerInfo;
        private static DelegateBridge __Hotfix_GetDialogContent;
        private static DelegateBridge __Hotfix_GetDialogAnswer;
        private static DelegateBridge __Hotfix_GetNpcHeadIconPathByNpcDNId;
        private static DelegateBridge __Hotfix_GetNpcHeadIconPathByTalkerInfo;
        private static DelegateBridge __Hotfix_GetNpcBodyIconPathByNpcDNId;
        private static DelegateBridge __Hotfix_GetNpcBodyIconPathByTalkerInfo;
        private static DelegateBridge __Hotfix_PlayNpcGreetingAudio;
        private static DelegateBridge __Hotfix_PlayNpcLeaveTalkAudio;
        private static DelegateBridge __Hotfix_PlayHiredCaptainGreetingAudio;
        private static DelegateBridge __Hotfix_GetNpcConfigIdFromNpcDNId;
        private static DelegateBridge __Hotfix_GetNpcConfigIdForDialog;
        private static DelegateBridge __Hotfix_GetQuestEnvirString;
        private static DelegateBridge __Hotfix_GetQuestCompletedSolarSystemName;

        [MethodImpl(0x8000)]
        public static string GetDialogAnswer(int dialogId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDialogAudioResPathById(int dialogId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDialogContent(int dialogId, ConfigDataNpcTalkerInfo talkerInfo = null, int questId = 0, List<FormatStringParamInfo> defaultParamInfos = null)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetDialogDuringTime(int dialogId, out bool isAutoClose)
        {
        }

        [MethodImpl(0x8000)]
        public static bool GetIsRemoteNpc(int dialogId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcBodyIconPathByNpcDNId(int solarSystemId, int spaceStaionId, int npcConfigId, int questEnvTypeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcBodyIconPathByTalkerInfo(ConfigDataNpcTalkerInfo talkerInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected static int GetNpcConfigIdForDialog(int dialogId, NpcDNId serverDNId = null, NpcDNId questDNId = null, int questEnvTypeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected static int GetNpcConfigIdFromNpcDNId(NpcDNId npcDnId, int questEnvTypeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static NpcEmotionType GetNpcEmotion4DialogId(int dialogId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcHeadIconPathByNpcDNId(int solarSystemId, int spaceStaionId, int npcConfigId, int questEnvTypeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcHeadIconPathByTalkerInfo(ConfigDataNpcTalkerInfo talkerInfo, NpcEmotionType emotionType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcNameByNpcDNId(int solarSystemId, int spaceStaionId, int npcConfigId, int questEnvTypeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcNameByTalkerInfo(ConfigDataNpcTalkerInfo talkerInfo, bool isGlobal)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataNpcTalkerInfo GetNpcTalkerInfoByNpcDNId(NpcDNId npcDnId, out bool isGlobalNpc, int questEnvTypeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataNpcTalkerInfo GetNpcTalkerInfoForDialog(int dialogId, out bool isGlobalNpc, NpcDNId replaceNpc = null, NpcDNId questDNId = null, int questEnvTypeId = 0)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetQuestCompletedSolarSystemName(int questId, ProjectXPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        protected static string GetQuestEnvirString(int questId, int envStringIndex, ProjectXPlayerContext playerCtx)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlayHiredCaptainGreetingAudio(NpcCaptainStaticInfo captainInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlayNpcGreetingAudio(ConfigDataNpcTalkerInfo talkerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void PlayNpcLeaveTalkAudio(ConfigDataNpcTalkerInfo talkerInfo)
        {
        }

        public enum DialogParamType
        {
            PlayerName,
            NpcTalkerName,
            SolarSystemName,
            EnvCustomString
        }
    }
}

