﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using TouchScript.Gestures;
    using TouchScript.Layers;
    using UnityEngine;
    using UnityStandardAssets.ImageEffects;
    using Vectrosity;

    public class StarMapForSolarSystem3DSceneController : UIControllerBase
    {
        private StarMapForSolarSystemObjectProviderController m_SSObjectProvider;
        private Animator m_scanningAnimator;
        private UIAnimationEventHandler m_scanningAnimatorEventHandler;
        private Material m_starMapLineMaterial;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnScannerAnimationFinished;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapDrawingSolarSystemDesc m_drawingConfig;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapForSolarSystemCameraFocusDesc m_cameraFocusConfig;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapForSolarSystemToStarfieldFadeInOutDesc m_switchToStarfieldFadeInOutConfig;
        [AutoBind("./3DSceneObjectsProviderRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_threeDSceneObjectsProviderRoot;
        [AutoBind("./SolarSystem3DViewRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_SSDrawingRoot;
        [AutoBind("./CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapForSolarSystemCameraTransformController m_cameraTransCtrl;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public ScreenTransformGesture m_transformGesture;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TapGesture m_tapGesture;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public PressGesture m_pressGesture;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public ReleaseGesture m_releaseGesture;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public FullscreenLayer m_fullScreenLayer;
        [AutoBind("./CameraPlatformRoot/CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public ColorCorrectionCurves m_infectImageEffect;
        private bool m_isInScaleAnim;
        private bool m_isProcessEnd;
        private float m_animStartTime;
        private float m_animEndTime;
        private float m_processEndTime;
        private float m_startOffset;
        private float m_endOffset;
        public Action<bool> m_onProcessEnd;
        private VectorLine m_starMapVectorLine;
        private GDBSolarSystemInfo m_currSolarSystemInfo;
        private List<GameObject> m_celestialObjectList;
        private List<double> m_celestialUniverseOrbitRadiusList;
        private List<float> m_celestialDrawingOrbitRadiusList;
        private List<GDBPlanetInfo> m_planetInfoList;
        private Dictionary<int, GameObject> m_guildBuildingDict;
        private double m_starMapDrawingScaleRatio;
        private bool m_isStarMapDrawingFinished;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float> EventOnTransformGestureScale;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float, float> EventOnTransformGestureSlipping;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEmptyAreaClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPressed;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnReleased;
        private static DelegateBridge __Hotfix_Initialize;
        private static DelegateBridge __Hotfix_SetLineMaterial;
        private static DelegateBridge __Hotfix_PlayScanningAnimator;
        private static DelegateBridge __Hotfix_StopScanningAnimator;
        private static DelegateBridge __Hotfix_EnableInfectImageEffect;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnScanningAnimationFinished;
        private static DelegateBridge __Hotfix_add_EventOnScannerAnimationFinished;
        private static DelegateBridge __Hotfix_remove_EventOnScannerAnimationFinished;
        private static DelegateBridge __Hotfix_InitializeStarMapCamera;
        private static DelegateBridge __Hotfix_StartCameraLookAt3DScenePositionProcess;
        private static DelegateBridge __Hotfix_StartCameraLookAtBuildingGo;
        private static DelegateBridge __Hotfix_StartCameraLookAtGameObject;
        private static DelegateBridge __Hotfix_StartRestoreCameraToDefaultProcess;
        private static DelegateBridge __Hotfix_RotateStarMapCamera;
        private static DelegateBridge __Hotfix_MakeCameraCloseToCenter;
        private static DelegateBridge __Hotfix_MakeCameraBeAwayFromCenter;
        private static DelegateBridge __Hotfix_GetCameraFocusParamsByCelestialType;
        private static DelegateBridge __Hotfix_StartFadeInOutProcess;
        private static DelegateBridge __Hotfix_ResetScaleAnim;
        private static DelegateBridge __Hotfix_TickForScaleAnim;
        private static DelegateBridge __Hotfix_GetSwitchToStarfieldFadeInTimeLength;
        private static DelegateBridge __Hotfix_GetSwitchToStarfieldFadeOutTimeLength;
        private static DelegateBridge __Hotfix_GetSwitchToStarfieldEndOffset;
        private static DelegateBridge __Hotfix_GetEnterStarMapStartOffset;
        private static DelegateBridge __Hotfix_TransformFromUniversePosToScreenPos;
        private static DelegateBridge __Hotfix_TransformFromUniversePositionTo3DScenePosition;
        private static DelegateBridge __Hotfix_TransformFrom3DScenePositionToScreenPos;
        private static DelegateBridge __Hotfix_Get3DSceneObjectById;
        private static DelegateBridge __Hotfix_GetPlanetInfoById;
        private static DelegateBridge __Hotfix_IsSolarSystemStarMapDrawingFinished;
        private static DelegateBridge __Hotfix_DestoryAllLines;
        private static DelegateBridge __Hotfix_InitializeStarMapBySSInfo;
        private static DelegateBridge __Hotfix_ComparationForGDBPlanets;
        private static DelegateBridge __Hotfix_DrawStarMapFromSSInfo;
        private static DelegateBridge __Hotfix_Clear3DSceneStarMap;
        private static DelegateBridge __Hotfix_DrawStarMapFromSSInfo_DrawStar;
        private static DelegateBridge __Hotfix_DrawStarMapFromSSInfo_DrawAllPlanets;
        private static DelegateBridge __Hotfix_DrawStarMapFromSSInfo_DrawOrbitLines;
        private static DelegateBridge __Hotfix_GetDrawingOrbitRadiusForStarMap_0;
        private static DelegateBridge __Hotfix_GetDrawingOrbitRadiusForStarMap_1;
        private static DelegateBridge __Hotfix_GetSunOrbitSpaceMinForStarMapDrawing;
        private static DelegateBridge __Hotfix_GetPlanetScaleForStarMapDrawing;
        private static DelegateBridge __Hotfix_GetPlanetOrbitSpaceMinForStarMapDrawing;
        private static DelegateBridge __Hotfix_GetPlanetOrbitSpaceMaxForStarMapDrawing;
        private static DelegateBridge __Hotfix_GetStarMapDrawingScaleRatio;
        private static DelegateBridge __Hotfix_GetGuildBuildingGo;
        private static DelegateBridge __Hotfix_CreateGuildBuildingGo;
        private static DelegateBridge __Hotfix_HideAllGuildBuildingGo;
        private static DelegateBridge __Hotfix_ShowAllCelestial;
        private static DelegateBridge __Hotfix_SyncCelestialDirForCamera;
        private static DelegateBridge __Hotfix_get_StarMapDrawingScaleRatio;
        private static DelegateBridge __Hotfix_OnScreenTransformGestureTransformed;
        private static DelegateBridge __Hotfix_OnTapGestureTapped;
        private static DelegateBridge __Hotfix_OnPressGesturePressed;
        private static DelegateBridge __Hotfix_OnReleaseGestureReleased;
        private static DelegateBridge __Hotfix_add_EventOnTransformGestureScale;
        private static DelegateBridge __Hotfix_remove_EventOnTransformGestureScale;
        private static DelegateBridge __Hotfix_add_EventOnTransformGestureSlipping;
        private static DelegateBridge __Hotfix_remove_EventOnTransformGestureSlipping;
        private static DelegateBridge __Hotfix_add_EventOnEmptyAreaClick;
        private static DelegateBridge __Hotfix_remove_EventOnEmptyAreaClick;
        private static DelegateBridge __Hotfix_add_EventOnPressed;
        private static DelegateBridge __Hotfix_remove_EventOnPressed;
        private static DelegateBridge __Hotfix_add_EventOnReleased;
        private static DelegateBridge __Hotfix_remove_EventOnReleased;

        public event Action EventOnEmptyAreaClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPressed
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnReleased
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnScannerAnimationFinished
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float> EventOnTransformGestureScale
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float, float> EventOnTransformGestureSlipping
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void Clear3DSceneStarMap()
        {
        }

        [MethodImpl(0x8000)]
        private int ComparationForGDBPlanets(GDBPlanetInfo elem1, GDBPlanetInfo elem2)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject CreateGuildBuildingGo(GuildBuildingType buildingType, int buildLevel, GameObject asset)
        {
        }

        [MethodImpl(0x8000)]
        public void DestoryAllLines()
        {
        }

        [MethodImpl(0x8000)]
        private void DrawStarMapFromSSInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void DrawStarMapFromSSInfo_DrawAllPlanets()
        {
        }

        [MethodImpl(0x8000)]
        private void DrawStarMapFromSSInfo_DrawOrbitLines()
        {
        }

        [MethodImpl(0x8000)]
        private void DrawStarMapFromSSInfo_DrawStar()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableInfectImageEffect(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject Get3DSceneObjectById(int id)
        {
        }

        [MethodImpl(0x8000)]
        public void GetCameraFocusParamsByCelestialType(bool isSunCelestial, GEPlanetType planetType, out float horizontalOffsetAngle, out float verticalOffsetAngle, out float distanceToPlanet)
        {
        }

        [MethodImpl(0x8000)]
        public List<float> GetDrawingOrbitRadiusForStarMap(List<GDBPlanetInfo> gdbPlanetInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public List<float> GetDrawingOrbitRadiusForStarMap(List<Vector3D> planetLocationList, List<GEPlanetType> planetTypeList)
        {
        }

        [MethodImpl(0x8000)]
        public float GetEnterStarMapStartOffset()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetGuildBuildingGo(GuildBuildingType buildingType, int buildLevel)
        {
        }

        [MethodImpl(0x8000)]
        public GDBPlanetInfo GetPlanetInfoById(int id)
        {
        }

        [MethodImpl(0x8000)]
        public float GetPlanetOrbitSpaceMaxForStarMapDrawing(GEPlanetType type)
        {
        }

        [MethodImpl(0x8000)]
        public float GetPlanetOrbitSpaceMinForStarMapDrawing(GEPlanetType type)
        {
        }

        [MethodImpl(0x8000)]
        public float GetPlanetScaleForStarMapDrawing(GDBPlanetInfo planet)
        {
        }

        [MethodImpl(0x8000)]
        public double GetStarMapDrawingScaleRatio()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSunOrbitSpaceMinForStarMapDrawing()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSwitchToStarfieldEndOffset()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSwitchToStarfieldFadeInTimeLength()
        {
        }

        [MethodImpl(0x8000)]
        public float GetSwitchToStarfieldFadeOutTimeLength()
        {
        }

        [MethodImpl(0x8000)]
        public void HideAllGuildBuildingGo()
        {
        }

        [MethodImpl(0x8000)]
        public void Initialize(GDBSolarSystemInfo solarSystemInfo, float cameraOffset, Vector2 cameraRotation, Material skyBox)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeStarMapBySSInfo(GDBSolarSystemInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void InitializeStarMapCamera(float cameraOffset, Vector2 cameraRotation, Material skyBox)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSolarSystemStarMapDrawingFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void MakeCameraBeAwayFromCenter(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public void MakeCameraCloseToCenter(float offset)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPressGesturePressed(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReleaseGestureReleased(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScanningAnimationFinished()
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenTransformGestureTransformed(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTapGestureTapped(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayScanningAnimator()
        {
        }

        [MethodImpl(0x8000)]
        public void ResetScaleAnim()
        {
        }

        [MethodImpl(0x8000)]
        public void RotateStarMapCamera(float angleX, float angleY)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLineMaterial(Material lineMaterial)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAllCelestial(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraLookAt3DScenePositionProcess(Vector3 objPos, bool isCelestialSun, GEPlanetType planetType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraLookAtBuildingGo(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraLookAtGameObject(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public void StartFadeInOutProcess(float startOffset, float endOffset, bool fadeIn, Action<bool> onProcessEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void StartRestoreCameraToDefaultProcess(float cameraOffset, Vector2 cameraRotation)
        {
        }

        [MethodImpl(0x8000)]
        public void StopScanningAnimator()
        {
        }

        [MethodImpl(0x8000)]
        private void SyncCelestialDirForCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForScaleAnim()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 TransformFrom3DScenePositionToScreenPos(Vector3 unityPos)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 TransformFromUniversePositionTo3DScenePosition(Vector3D pos)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 TransformFromUniversePosToScreenPos(Vector3D pos)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        private double StarMapDrawingScaleRatio
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

