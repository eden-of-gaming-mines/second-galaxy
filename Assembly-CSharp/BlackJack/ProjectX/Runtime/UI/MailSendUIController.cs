﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class MailSendUIController : UIControllerBase
    {
        private string m_targetGameUserId;
        private const int MailNameCharacterLimit = 0x19;
        private const int MailDetailCharacterLimit = 360;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<string, string, string> EventOnMailSendButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<string, string> EventOnGroupMailSendButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnMailSendCancelButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./PlayerIcon/CharacterIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_charIconImage;
        [AutoBind("./PlayerIcon/JobIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_professionImage;
        [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_charNameText;
        [AutoBind("./MailNameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_mailNameInputField;
        [AutoBind("./MailDetailScrollView/Viewport/Content/MailDetailInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_mailDetailInputField;
        [AutoBind(" ./ButtonGroup/SendButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SendButton;
        [AutoBind(" ./ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./MyNameInfoPanel/CharacterNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_fromCharNameText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnSendButtonClick;
        private static DelegateBridge __Hotfix_OnDetailInputFieldEndEdit;
        private static DelegateBridge __Hotfix_OnTitleInputFieldEndEdit;
        private static DelegateBridge __Hotfix_UpdateMailSendInfo;
        private static DelegateBridge __Hotfix_CheckMailNameInputField;
        private static DelegateBridge __Hotfix_CheckMailDetailInputField;
        private static DelegateBridge __Hotfix_UpdateGroupMailSendInfo;
        private static DelegateBridge __Hotfix_GetMailTitle;
        private static DelegateBridge __Hotfix_GetMailContent;
        private static DelegateBridge __Hotfix_add_EventOnMailSendButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMailSendButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGroupMailSendButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGroupMailSendButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMailSendCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMailSendCancelButtonClick;

        public event Action<string, string> EventOnGroupMailSendButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<string, string, string> EventOnMailSendButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnMailSendCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CheckMailDetailInputField(string str)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckMailNameInputField(string str)
        {
        }

        [MethodImpl(0x8000)]
        private string GetMailContent()
        {
        }

        [MethodImpl(0x8000)]
        private string GetMailTitle()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailInputFieldEndEdit(string inputContent)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleInputFieldEndEdit(string inputContent)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateGroupMailSendInfo(string sendFromGameUserName)
        {
        }

        [MethodImpl(0x8000)]
        internal void UpdateMailSendInfo(Sprite targetAvatarIcon, Sprite professionIcon, string sendToGameUserName, string sendToGameUserId, string sendFromGameUserName)
        {
        }
    }
}

