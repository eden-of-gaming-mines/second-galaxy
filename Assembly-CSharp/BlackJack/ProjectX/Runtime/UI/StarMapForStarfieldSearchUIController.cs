﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapForStarfieldSearchUIController : UIControllerBase
    {
        private const int SEARCH_FRAME_PERIOD = 15;
        private int m_lastSearchEventFrameCount;
        private const int PoolSize = 10;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemClicked;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Func<string, List<int>> EventOnSearchClicked;
        [AutoBind("./InputField/SearchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SearchButton;
        [AutoBind("./InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField InputFieldForSearch;
        [AutoBind("./FieldScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemSearchListScrollRectUIState;
        [AutoBind("./FieldScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ItemSearchListScrollRect;
        [AutoBind("./FieldScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler SearchListScrollViewClick;
        [AutoBind("./FieldScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ItemSearchListUIItemPool;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        private List<int> m_findedNameIDList;
        private GameObject m_itemTemplateGo;
        private const string ItemSearchListUIItemPoolName = "ItemSearchListItem";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTemplateItemEx;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolTablRewardItemCreated;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnValueChangeInputStr;
        private static DelegateBridge __Hotfix_CheckAndUpdateCurrFrameSearcheEvent;
        private static DelegateBridge __Hotfix_OnSerachItemFill;
        private static DelegateBridge __Hotfix_OnSearchItemClick;
        private static DelegateBridge __Hotfix_UpdateItemSearchList;
        private static DelegateBridge __Hotfix_ClearSearchControl;
        private static DelegateBridge __Hotfix_OnSearchButtonClick;
        private static DelegateBridge __Hotfix_IsOpened;
        private static DelegateBridge __Hotfix_GetInputString;
        private static DelegateBridge __Hotfix_add_EventOnItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnSearchClicked;
        private static DelegateBridge __Hotfix_remove_EventOnSearchClicked;

        public event Action<int> EventOnItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Func<string, List<int>> EventOnSearchClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private bool CheckAndUpdateCurrFrameSearcheEvent()
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearSearchControl(bool isClearInput = true)
        {
        }

        [MethodImpl(0x8000)]
        private GameObject CreateTemplateItemEx(string name, Transform parent, bool active)
        {
        }

        [MethodImpl(0x8000)]
        public string GetInputString()
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsOpened()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolTablRewardItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchButtonClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchItemClick(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSerachItemFill(UIControllerBase uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnValueChangeInputStr(string str)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemSearchList(List<int> findedNameIDList)
        {
        }
    }
}

