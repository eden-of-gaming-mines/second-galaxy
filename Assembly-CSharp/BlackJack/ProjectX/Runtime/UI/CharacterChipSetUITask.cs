﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CharacterChipSetUITask : UITaskBase
    {
        public static string CharacterChipSetUITaskMode_Normal = "CharacterChipSetUITaskMode_Normal";
        private CharacterChipSetUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private int m_initWindowFlagIndex;
        private int m_needRefreshChipItemSelectedStateFlagIndex;
        private int m_needRefreshChipItemList;
        private LBCharChipSlot m_charChipSlot;
        private List<ILBStoreItemClient> m_availableChipItemList;
        private int m_currSelectItemIndex;
        private DateTime m_cacheItemStoreUpdateTime;
        public const string TaskName = "CharacterChipSetUITask";
        [CompilerGenerated]
        private static Predicate<int> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_ChipStoreItemList;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_EquippedChip;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AllChipItemList;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_NormalMode;
        private static DelegateBridge __Hotfix_UpdateView_NormalMode_UpdateTitle;
        private static DelegateBridge __Hotfix_UpdateView_NormalMode_UpdateChipItemList;
        private static DelegateBridge __Hotfix_UpdateView_NormalMode_UpdateSelectedChip;
        private static DelegateBridge __Hotfix_UpdateView_NormalMode_UpdateCurrChip;
        private static DelegateBridge __Hotfix_UpdateView_ShowAllUIWindows;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnChipListItemClick;
        private static DelegateBridge __Hotfix_OnChipExchangeConfirmButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_FilterForSelectChipStoreItemList;
        private static DelegateBridge __Hotfix_UpdateChipBasicPropertyInfoToUI;
        private static DelegateBridge __Hotfix_UpdateDefaultChipExtraPropertyInfoToUI;
        private static DelegateBridge __Hotfix_HideChipExtraPropertyInfo;
        private static DelegateBridge __Hotfix_UpdateChipExtraPropertyInfoToUI;
        private static DelegateBridge __Hotfix_UpdateChipSuitPropertyInfoToUI;
        private static DelegateBridge __Hotfix_ShowNecessaryWindows;
        private static DelegateBridge __Hotfix_HideWindows;
        private static DelegateBridge __Hotfix_WindowShownHideEndCallback;
        private static DelegateBridge __Hotfix_OnItemFilled;
        private static DelegateBridge __Hotfix_ClearAllRecomendFlag;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LBChipClient;

        [MethodImpl(0x8000)]
        public CharacterChipSetUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearAllRecomendFlag()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AllChipItemList(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_EquippedChip(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private bool FilterForSelectChipStoreItemList(ILBStoreItemClient storeItem)
        {
        }

        [MethodImpl(0x8000)]
        private void HideChipExtraPropertyInfo(CharacterChipInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void HideWindows(Action onShownEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChipExchangeConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnChipListItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemFilled(ILBStoreItemClient item, CommonItemIconUIController ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowNecessaryWindows(Action onShownEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChipBasicPropertyInfoToUI(ConfigDataCharChipInfo chipConfInfo, CharacterChipInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChipExtraPropertyInfoToUI(LBCharChipSlot chipSlot, CharacterChipInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChipSuitPropertyInfoToUI(ConfigDataCharChipInfo chipConfInfo, CharacterChipInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_ChipStoreItemList()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDefaultChipExtraPropertyInfoToUI(CharacterChipInfoItemUIController uiCtrl, bool isHaveExtraProp)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_NormalMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_NormalMode_UpdateChipItemList()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_NormalMode_UpdateCurrChip()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_NormalMode_UpdateSelectedChip()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_NormalMode_UpdateTitle()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_ShowAllUIWindows()
        {
        }

        [MethodImpl(0x8000)]
        private void WindowShownHideEndCallback(ref bool updateFlag, ref bool checkFlag1, ref bool checkFlag2, Action onEnd)
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCharacterChipClient LBChipClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideWindows>c__AnonStorey2
        {
            internal Action onShownEnd;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.onShownEnd != null)
                {
                    this.onShownEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowNecessaryWindows>c__AnonStorey1
        {
            internal Action onShownEnd;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.onShownEnd != null)
                {
                    this.onShownEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCache_ChipStoreItemList>c__AnonStorey0
        {
            internal LogicBlockItemStoreClient lbItemStore;

            [MethodImpl(0x8000)]
            internal int <>m__0(ILBStoreItemClient itemA, ILBStoreItemClient itemB)
            {
            }
        }
    }
}

