﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ActionPlanManagerUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ActionPlanTabType> EventOnActionPlanTabClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventFactionCreditTabClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./QuestListUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_questListUIDummyPoint;
        [AutoBind("./BranchDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_branchUIDummyPoint;
        [AutoBind("./ActivityUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_activityUIDummyPoint;
        [AutoBind("./ToggleGroup/StoryToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_storyTabUIStateCtrl;
        [AutoBind("./ToggleGroup/ExploreToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_exploreTabUIStateCtrl;
        [AutoBind("./ToggleGroup/QuestToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_questTabUIStateCtrl;
        [AutoBind("./ToggleGroup/BranchToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_branchTabUIStateCtrl;
        [AutoBind("./ToggleGroup/FactionCreditToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FactionCreditTabUIStateCtrl;
        [AutoBind("./ToggleGroup/StoryToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_storyTabButton;
        [AutoBind("./ToggleGroup/ExploreToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_exploreTabButton;
        [AutoBind("./ToggleGroup/QuestToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_questTabButton;
        [AutoBind("./ToggleGroup/BranchToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_branchTabButton;
        [AutoBind("./ToggleGroup/FactionCreditToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FactionCreditToggleTabButton;
        [AutoBind("./ToggleGroup/StoryToggle/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_storyTabCornerTipGroup;
        [AutoBind("./ToggleGroup/QuestToggle/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_questTabTipGroupStateCtrl;
        [AutoBind("./ToggleGroup/BranchToggle/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_branchTabTipGroupStateCtrl;
        [AutoBind("./ToggleGroup/FactionCreditToggle/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_FactionCreditTabTipGroupStateCtrl;
        [AutoBind("./ToggleGroup/StoryToggle/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_storyTabCountTip;
        [AutoBind("./ToggleGroup/ExploreToggle/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_exploreTabCountTip;
        [AutoBind("./ToggleGroup/QuestToggle/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_questTabCountTip;
        [AutoBind("./ToggleGroup/BranchToggle/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_branchTabCountTip;
        [AutoBind("./ToggleGroup/FactionCreditToggle/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_factionTabCountTip;
        [AutoBind("./ToggleGroup/StoryToggle/PointGroup/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_storyTabRedPointTip;
        [AutoBind("./ToggleGroup/ExploreToggle/PointGroup/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_exploreTabRedPointTip;
        [AutoBind("./ToggleGroup/StoryToggle/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_storyTabCornerImageUIStateCtrl;
        [AutoBind("./ToggleGroup/BranchToggle/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_branchTabCornerTipGroup;
        [AutoBind("./ToggleGroup/FactionCreditToggle/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_factionTabCountTipGroup;
        [AutoBind("./TitleText/HelpIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public SystemDescriptionController m_systemDescriptionCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetBgUIProcess;
        private static DelegateBridge __Hotfix_SetSelectedTab;
        private static DelegateBridge __Hotfix_UpdateQuestCornerTip;
        private static DelegateBridge __Hotfix_UpdateStoryCornerTip;
        private static DelegateBridge __Hotfix_UpdateBranchStoryCornerTip;
        private static DelegateBridge __Hotfix_UpdateExploreCornerTip;
        private static DelegateBridge __Hotfix_UpdateFactionCreditCornerTip;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnActionPlanStoryTabClick;
        private static DelegateBridge __Hotfix_OnActionPlanExploreTabClick;
        private static DelegateBridge __Hotfix_OnActionPlanQuestTabClick;
        private static DelegateBridge __Hotfix_OnActionPlanBranchStoryTabClick;
        private static DelegateBridge __Hotfix_OnActionFactionCreditToggleTabClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnActionPlanTabClick;
        private static DelegateBridge __Hotfix_remove_EventOnActionPlanTabClick;
        private static DelegateBridge __Hotfix_add_EventFactionCreditTabClick;
        private static DelegateBridge __Hotfix_remove_EventFactionCreditTabClick;

        public event Action EventFactionCreditTabClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ActionPlanTabType> EventOnActionPlanTabClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetBgUIProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public void OnActionFactionCreditToggleTabClick(UIControllerBase go)
        {
        }

        [MethodImpl(0x8000)]
        public void OnActionPlanBranchStoryTabClick(UIControllerBase go)
        {
        }

        [MethodImpl(0x8000)]
        public void OnActionPlanExploreTabClick(UIControllerBase go)
        {
        }

        [MethodImpl(0x8000)]
        public void OnActionPlanQuestTabClick(UIControllerBase go)
        {
        }

        [MethodImpl(0x8000)]
        public void OnActionPlanStoryTabClick(UIControllerBase go)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseButtonClick(UIControllerBase go)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedTab(ActionPlanTabType actionPlanTabType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBranchStoryCornerTip(int tipCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateExploreCornerTip(bool hasUnreceivedActivityReward)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFactionCreditCornerTip(int canAcceptedQuestCount, bool hasWeeklyReward = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateQuestCornerTip(int acceptedQuestCount, int unacceptedQuestCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStoryCornerTip(bool hasUnreceivedStoryReward, int unfinishSubQuestCount)
        {
        }
    }
}

