﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class InSpaceShipSingleItemRemoveReqNetTask : NetWorkTransactionTask
    {
        private StoreItemType m_storeItemType;
        private int m_storeItemConfigId;
        private int m_storeItemCount;
        private bool m_storeItemIsBind;
        private bool m_isFreezing;
        private int m_result;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsDeleteSrcItem>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnStoreItemSingleRemoveAck;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_IsDeleteSrcItem;
        private static DelegateBridge __Hotfix_set_IsDeleteSrcItem;

        [MethodImpl(0x8000)]
        public InSpaceShipSingleItemRemoveReqNetTask(StoreItemType itemType, int itemConfigId, int storeItemCount, bool storeItemIsBind, bool isFreezing)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStoreItemSingleRemoveAck(int result, bool isDeleteSrcItem)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsDeleteSrcItem
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

