﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildSolarSystemInfoReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_solarSystemId;
        private readonly int m_dynamicDataVersion;
        private readonly int m_buildingDataVersion;
        private bool m_isBasicInfoComplete;
        private bool m_isBuildingInfoComplete;
        private bool m_isBattleInfoComplete;
        private bool m_forceRefresh;
        public int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemBasicInfoAck;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemBuildingInfoAck;
        private static DelegateBridge __Hotfix_OnGuildSolarSystemBattleInfoAck;
        private static DelegateBridge __Hotfix_IsAllAckReturn;

        [MethodImpl(0x8000)]
        public GuildSolarSystemInfoReqNetTask(int solarSytemId, int dynamicDataVersion, int buildingDataVersion, bool forceRefresh = false, bool skipRelogin = false)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAllAckReturn()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSolarSystemBasicInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSolarSystemBattleInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSolarSystemBuildingInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

