﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class SpecialGiftPackageShipItemUIController : UIControllerBase
    {
        [AutoBind("./SimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SimpleInfoDummy;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBRechargeGiftPackage, ILBStoreItemClient, Vector3> EventOnRewardItemIconClick;
        private DateTime m_nextUpdateTime;
        private DateTime m_nextUpdateDayTime;
        private readonly List<SpecialGiftPackageFeatureInfoUIController> m_featureInfoCtrlList;
        private readonly List<SpecialGiftPackageShipRewardItemUIController> m_rewardItemCtrlList;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private LBRechargeGiftPackage m_lbGiftPackage;
        private const string RewardItemName = "RewardItem";
        private const string FeatureItemName = "FeatureItem";
        [AutoBind("./DiscountGroup/OffText", AutoBindAttribute.InitState.NotInit, false)]
        private Text DescText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        private MarchingBytes.EasyObjectPool EasyObjectPool;
        [AutoBind("./BuyCountLimitText/TimeLeftLimit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TimeLeftLimitRoot;
        [AutoBind("./FeatureInfoItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FeatureInfoItemRoot;
        [AutoBind("./BuyButton/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonPriceText;
        [AutoBind("./BuyButton/IocnGroup/IconText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonCurrencyText;
        [AutoBind("./BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./TotalValueGroup/TitleText/TotalValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TotalValueText;
        [AutoBind("./DiscountGroup/DiscountNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DiscountNumberText;
        [AutoBind("./BuyCountLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyCountLimitText;
        [AutoBind("./BuyCountLimitText/TimeLeftLimit/TimeLeftLimitDayText/TimeLeftLimitText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText TimeLeftLimitText;
        [AutoBind("./TileText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TileText;
        [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BgImage;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_rewardItemRoot;
        [CompilerGenerated]
        private static Func<QuestRewardInfo, bool> <>f__am$cache0;
        private static DelegateBridge __Hotfix_Update4GiftPackageShipItem;
        private static DelegateBridge __Hotfix_GetSimpleInfoIconPosition;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdatePackageContent;
        private static DelegateBridge __Hotfix_UpdateFeatureInfo;
        private static DelegateBridge __Hotfix_InitEasyPoolObject;
        private static DelegateBridge __Hotfix_InitePoolAndScrollView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemIconClick;

        public event Action<LBRechargeGiftPackage, ILBStoreItemClient, Vector3> EventOnRewardItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSimpleInfoIconPosition()
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPoolObject(string itemName)
        {
        }

        [MethodImpl(0x8000)]
        private void InitePoolAndScrollView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolInfo, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void Update4GiftPackageShipItem(LBRechargeGiftPackage giftPackage, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFeatureInfo(List<int> featureList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePackageContent(List<QuestRewardInfo> rewardInfos, List<int> descIdList)
        {
        }
    }
}

