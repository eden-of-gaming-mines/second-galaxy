﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;

    public class ServerInfoListJson
    {
        public string m_serverStateDesc;
        public string m_serverNameDesc;
        public string m_privilegeUserList;
        public string m_gmUserList;
        public List<ServerInfo> m_serverInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

