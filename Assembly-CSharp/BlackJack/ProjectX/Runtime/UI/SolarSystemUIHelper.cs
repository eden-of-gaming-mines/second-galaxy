﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public static class SolarSystemUIHelper
    {
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_GetTargetListItemIconBySceneConf;
        private static DelegateBridge __Hotfix_GetSceneScreenEffectResPathByType;
        private static DelegateBridge __Hotfix_GetGlobalSceneName;
        private static DelegateBridge __Hotfix_GetDropBoxItemDescStr;
        private static DelegateBridge __Hotfix_GetCrimeStateName;
        private static DelegateBridge __Hotfix_GetPVPFlagStateName;
        private static DelegateBridge __Hotfix_GetSmuggleStateName;
        private static DelegateBridge __Hotfix_GetTargetNoticeStateName;
        private static DelegateBridge __Hotfix_GetInfectInfoBySolarSystemInfo;
        private static DelegateBridge __Hotfix_GetWormholeStargroupName;
        private static DelegateBridge __Hotfix_GetHudResList;
        private static DelegateBridge __Hotfix_GetHudResKey;
        private static DelegateBridge __Hotfix_GetHudSprite;
        private static DelegateBridge __Hotfix_GetHudTargetIconUIState;
        private static DelegateBridge __Hotfix_IsMyHiredCaptain;
        private static DelegateBridge __Hotfix_CollectSceneTypeIconResPathInSpace;
        private static DelegateBridge __Hotfix_ConfirmLeaveScene;
        private static DelegateBridge __Hotfix_GetStarGateArtResFullPath;
        private static DelegateBridge __Hotfix_GetSimpleObjectArtResFullPath;
        private static DelegateBridge __Hotfix_GetPlanetArtResFullPath;
        private static DelegateBridge __Hotfix_GetOtherArtResFullPath;
        private static DelegateBridge __Hotfix_GetSceneAnimationResFullPath;
        private static DelegateBridge __Hotfix_GetStargateName;
        private static DelegateBridge __Hotfix_IsFollowGuildFleetFormation;

        [MethodImpl(0x8000)]
        public static void CollectSceneTypeIconResPathInSpace(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void ConfirmLeaveScene(Action onConfirm, Action onCancel = null, int questInstanceId = -1)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCrimeStateName(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetDropBoxItemDescStr(StoreItemType itemType, int itemConfigId, long count)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetGlobalSceneName(GlobalSceneInfo sceneInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetHudResKey(ShipType shipType, bool isNpc = false)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        public static IEnumerable<string> GetHudResList()
        {
        }

        [MethodImpl(0x8000)]
        public static Sprite GetHudSprite(ShipType shipType, Dictionary<string, MapSceneTaskBase.DynamicResCacheItem> resDic, bool isNpc = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetHudTargetIconUIState(ILBSpaceTarget spaceShip, SpaceObjectType targetObjType, TargetListViewState viewState, bool isTeamMember, bool isHiredCaptain, bool isGuildMember, bool isHud, out string hudSubscriptState, out string hudColorState)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataInfectInfo GetInfectInfoBySolarSystemInfo(GDBSolarSystemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetOtherArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPlanetArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetPVPFlagStateName(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSceneAnimationResFullPath(string resPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSceneScreenEffectResPathByType(SceneScreenEffectType effectType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSimpleObjectArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSmuggleStateName()
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStarGateArtResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetStargateName(int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTargetListItemIconBySceneConf(ConfigDataSceneInfo sceneInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTargetNoticeStateName(ILBSpaceTarget target)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetWormholeStargroupName(ConfigDataWormholeStarGroupInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsFollowGuildFleetFormation()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsMyHiredCaptain(ILBSpaceTarget ship)
        {
        }

        private static ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetHudResList>c__Iterator0 : IEnumerable, IEnumerable<string>, IEnumerator, IDisposable, IEnumerator<string>
        {
            internal string $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            [MethodImpl(0x8000), DebuggerHidden]
            IEnumerator<string> IEnumerable<string>.GetEnumerator()
            {
            }

            [DebuggerHidden]
            IEnumerator IEnumerable.GetEnumerator() => 
                this.System.Collections.Generic.IEnumerable<string>.GetEnumerator();

            string IEnumerator<string>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

