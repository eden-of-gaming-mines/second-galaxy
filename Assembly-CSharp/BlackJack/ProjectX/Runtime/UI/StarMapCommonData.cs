﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class StarMapCommonData
    {
        private readonly GDBSolarSystemSimpleInfo[] m_solarSystemInfoListForPosZIncreasing;
        private readonly Rect m_allFieldInMap;
        private readonly Dictionary<int, ForcePosition> m_forces;
        private readonly Dictionary<int, ulong> m_coveredDict;
        private static StarMapCommonData m_instance;
        [CompilerGenerated]
        private static Comparison<GDBSolarSystemSimpleInfo> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_get_Instance;
        private static DelegateBridge __Hotfix_TryInitialize;
        private static DelegateBridge __Hotfix_get_SolarSystemInfoListForPosZIncreasing;
        private static DelegateBridge __Hotfix_get_AllFieldInMap;
        private static DelegateBridge __Hotfix_get_Forces;
        private static DelegateBridge __Hotfix_SearchFirstSolarSystemIndexForLocationZNotLessThan;
        private static DelegateBridge __Hotfix_SearchLastSolarSystemIndexForLocationZNotMoreThan;
        private static DelegateBridge __Hotfix_TransformSolarSystemPosToUnityPos_1;
        private static DelegateBridge __Hotfix_TransformSolarSystemPosToUnityPos_0;
        private static DelegateBridge __Hotfix_get_CoveredDict;

        [MethodImpl(0x8000)]
        private StarMapCommonData()
        {
        }

        [MethodImpl(0x8000), CompilerGenerated]
        private static int <StarMapCommonData>m__0(GDBSolarSystemSimpleInfo s1, GDBSolarSystemSimpleInfo s2)
        {
        }

        [MethodImpl(0x8000)]
        public static int SearchFirstSolarSystemIndexForLocationZNotLessThan(float comparedZ, bool isGDBMetric = false)
        {
        }

        [MethodImpl(0x8000)]
        public static int SearchLastSolarSystemIndexForLocationZNotMoreThan(float comparedZ, bool isGDBMetric = false)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2 TransformSolarSystemPosToUnityPos(GDBSolarSystemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static Vector2 TransformSolarSystemPosToUnityPos(GDBSolarSystemSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        public static void TryInitialize()
        {
        }

        public static StarMapCommonData Instance
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static GDBSolarSystemSimpleInfo[] SolarSystemInfoListForPosZIncreasing
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static Rect AllFieldInMap
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static Dictionary<int, ForcePosition> Forces
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public static Dictionary<int, ulong> CoveredDict
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

