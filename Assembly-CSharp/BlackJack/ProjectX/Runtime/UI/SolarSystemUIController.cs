﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(CanvasGroup))]
    public class SolarSystemUIController : UIControllerBase
    {
        public string CurrentMode;
        public SolarSystemUIManipulationGuideUIController CurrentUIManipulationGuideUICtrl;
        public SolarSystemTargetListUIController CurrentSolarSystemTargetListUICtrl;
        public SolarSystemSelectedTargetUIController CurrentSolarSystemSelectedTargetUICtrl;
        public SolarSystemTargetInteractionUIController CurrentSolarSystemTargetInteractionUICtrl;
        public SolarSystemSelfShipUIController CurrentSolarSystemSelfShipUICtrl;
        public SolarSystemWeaponEquipUIController CurrentSolarSystemWeaponEquipUICtrl;
        public SolarSystemFunButtonsUIController CurrentSolarSystemFunButtonsUICtrl;
        public RescueSignalButtonUIControllerBase CurrentRescueSignalButtonUICtrl;
        public SolarSystemTacticalEquipInfoUIController CurrentSolarSystemTacticalEquipInfoUIController;
        public SolarSystemShipInfoUIController ShipButtonGroupCtrl;
        [AutoBind("./UIManipulationGuidePanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UIManipulationGuidePanel;
        [AutoBind("./TargetListPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TargetListUIPanel;
        [AutoBind("./PlayerSelfInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SelfShipInfoUIPanel;
        [AutoBind("./CanvasVolatile/WeaponEquipUIPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WeaponEquipUIPanelVolatile;
        [AutoBind("./WeaponEquipUIPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WeaponEquipUIPanel;
        [AutoBind("./CurrentSelectedTargetPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CurrentSelectedTargetPanel;
        [AutoBind("./CanvasVolatile/InteractCountDownPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject InteractCountDownPanel;
        [AutoBind("./FunButtonsPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FunButtonsPanel;
        [AutoBind("./CanvasMask/ShipButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipButtonGroup;
        [AutoBind("./CanvasMask/ShipButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipButtonGroupStateCtrl;
        [AutoBind("./CanvasVolatile/PlayerSelfInfoPanel/ShieldProgressBarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Button ShieldProgressBarGroup;
        [AutoBind("./CanvasVolatile/PlayerSelfInfoPanel/ArmorProgressBarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Button ArmorProgressBarGroup;
        [AutoBind("./CanvasVolatile/PlayerSelfInfoPanel/EnergyProgressBarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Button EnergyProgressBarGroup;
        [AutoBind("./CanvasVolatile/PlayerSelfInfoPanel/SpeedProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Button SpeedProgressBar;
        [AutoBind("./CanvasMask/BuffExplainGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TacticalEquipInfoPanel;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetUIToPlayMode;
        private static DelegateBridge __Hotfix_SetUIToViewMode;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIToPlayMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIToViewMode(string mode)
        {
        }
    }
}

