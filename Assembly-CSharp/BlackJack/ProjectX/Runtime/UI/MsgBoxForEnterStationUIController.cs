﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class MsgBoxForEnterStationUIController : UIControllerBase
    {
        private const string ModeJumpToBase = "JumpToBase";
        private const string ModeJumpToStation = "JumpToStation";
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("./TextGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ValueText;
        [AutoBind("./TextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TextGroupStateCtrl;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ConfirmButton;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CancelButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnShowMsgBoxStart;
        private static DelegateBridge __Hotfix_CreateMainWindownEffectInfo;

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateMainWindownEffectInfo(bool isShow = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnShowMsgBoxStart(GDBSolarSystemInfo solarSystemInfo, int spaceSationId, int distance, bool isJumpToBase, bool isReturnToBaseForFunc)
        {
        }
    }
}

