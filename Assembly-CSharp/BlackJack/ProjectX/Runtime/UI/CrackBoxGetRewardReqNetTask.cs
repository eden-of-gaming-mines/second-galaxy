﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CrackBoxGetRewardReqNetTask : NetWorkTransactionTask
    {
        public int m_boxItemIndex;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <CrackBoxGetRewardResult>k__BackingField;
        public List<StoreItemUpdateInfo> CrackBoxRewardInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCrackBoxGetRewardAck;
        private static DelegateBridge __Hotfix_set_CrackBoxGetRewardResult;
        private static DelegateBridge __Hotfix_get_CrackBoxGetRewardResult;

        [MethodImpl(0x8000)]
        public CrackBoxGetRewardReqNetTask(int boxItemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCrackBoxGetRewardAck(int result, List<StoreItemUpdateInfo> rewardList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int CrackBoxGetRewardResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

