﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class InfectMenuItemUIController : MenuItemUIControllerBase
    {
        private CommonRewardUIController m_infectRewardUICtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnInfectFinalBattleAssignButtonClick;
        private DateTime m_infectHealingTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnQuestRewardItemClick;
        public Action EventOnTimeout;
        [AutoBind("./TargetInfo/Info/InfectPercentInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_infectPercentText;
        [AutoBind("./TargetInfo/Info/TextGroup/InfectTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_infectTimeText;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_infectInfoUIStateCtrl;
        [AutoBind("./Detail/Reward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_infectRewardUIObj;
        [AutoBind("./Detail/AssignButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_assignButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_SetInfectDetailInfo;
        private static DelegateBridge __Hotfix_GetInfectTime;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_SetEasyPool;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnInfectFinalBattleAssignButtonClick;
        private static DelegateBridge __Hotfix_OnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnInfectFinalBattleAssignButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnInfectFinalBattleAssignButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnQuestRewardItemClick;

        public event Action EventOnInfectFinalBattleAssignButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnQuestRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private string GetInfectTime(TimeSpan time)
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnInfectFinalBattleAssignButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEasyPool(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInfectDetailInfo(float infectProgress, DateTime selfHealingTime, ConfigDataItemDropInfo rewardDropInfo, bool isFinalBattle, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

