﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildActionUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnIssueQuestItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnUnderwayQuestItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGalaxyClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnIssueConfirmClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnStrikeBtnClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnStarMapBtnClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLossWarningClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnShowIssueHintCtrlClick;
        private Dictionary<int, List<GuildActionInfo>> m_underwayInfos;
        private List<ConfigDataGuildActionInfo> m_issueInfos;
        private int m_curIssueSelectedConfigId;
        private ulong m_curUnderwaySelectedInstanceId;
        private Dictionary<string, UnityEngine.Object> m_resData;
        private GuildActionUITask.Tab m_curTab;
        private int m_curEnterSolarSysId;
        private List<GuildActionSolarSysItemUIController> m_solarSystemCtrls;
        public GuildActionQuestDetailUIController m_questDetailCtrl;
        public GameObject m_solarSysItemTemplate;
        public GameObject m_questItemTemplate;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainState;
        [AutoBind("./Top", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TopObj;
        [AutoBind("./Top/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./Top/ButtonGroup/UnderwayButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnderwayTabBtn;
        [AutoBind("./Top/ButtonGroup/UnderwayButton/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnderwayQuestCount;
        [AutoBind("./Top/ButtonGroup/UnderwayButton/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UnderwayRedState;
        [AutoBind("./Top/ButtonGroup/UnderwayButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UnderwayTabBtnState;
        [AutoBind("./Top/ButtonGroup/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IssueTabBtnState;
        [AutoBind("./Top/ButtonGroup/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx IssueTabBtn;
        [AutoBind("./Top/ButtonGroup/IssueButton/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CanIssueQuestCount;
        [AutoBind("./Top/ButtonGroup/IssueButton/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IssueRedState;
        [AutoBind("./ContentGroup/IssueQuestList", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool IssueEasyPool;
        [AutoBind("./ContentGroup/IssueQuestList", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect IssueScrollRect;
        [AutoBind("./ContentGroup/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemTemplateRoot;
        [AutoBind("./ContentGroup/UnderwayQuestList/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SolarSysItemRoot;
        [AutoBind("./EmptyGroup/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GoIssueButton;
        [AutoBind("./EmptyGroup/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GoIssueBtnState;
        [AutoBind("./ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_contentState;
        [AutoBind("./ContentGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemDummy;
        [AutoBind("./ContentGroup/DetailPanelRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_detailRoot;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ShowIssueHintCtrl;
        private static DelegateBridge __Hotfix_FoldAllSolarSysItem;
        private static DelegateBridge __Hotfix_ChangeSolarSysItemState;
        private static DelegateBridge __Hotfix_FoldSolarSysItem;
        private static DelegateBridge __Hotfix_SetIssueSelectedQuest;
        private static DelegateBridge __Hotfix_SetUnderwaySelectedQuest;
        private static DelegateBridge __Hotfix_GetProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateIssue;
        private static DelegateBridge __Hotfix_UpdateUnderway;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnQuestItemFill;
        private static DelegateBridge __Hotfix_OnIssueQuestItemClick;
        private static DelegateBridge __Hotfix_OnConfirmIssueBtnClick;
        private static DelegateBridge __Hotfix_OnGalaxyClick;
        private static DelegateBridge __Hotfix_OnUnderwayQuestItemClick;
        private static DelegateBridge __Hotfix_OnStrikeBtnClick;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnStarMapBtnClick;
        private static DelegateBridge __Hotfix_OnLossWarningClick;
        private static DelegateBridge __Hotfix_OnShowIssueHintCtrlClick;
        private static DelegateBridge __Hotfix_GetActionInfoByInstanceId;
        private static DelegateBridge __Hotfix_add_EventOnIssueQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnIssueQuestItemClick;
        private static DelegateBridge __Hotfix_add_EventOnUnderwayQuestItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnUnderwayQuestItemClick;
        private static DelegateBridge __Hotfix_add_EventOnGalaxyClick;
        private static DelegateBridge __Hotfix_remove_EventOnGalaxyClick;
        private static DelegateBridge __Hotfix_add_EventOnIssueConfirmClick;
        private static DelegateBridge __Hotfix_remove_EventOnIssueConfirmClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnStrikeBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnStarMapBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnStarMapBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningClick;
        private static DelegateBridge __Hotfix_add_EventOnShowIssueHintCtrlClick;
        private static DelegateBridge __Hotfix_remove_EventOnShowIssueHintCtrlClick;

        public event Action<int> EventOnGalaxyClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnIssueConfirmClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnIssueQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLossWarningClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnShowIssueHintCtrlClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnStarMapBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnStrikeBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnUnderwayQuestItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ChangeSolarSysItemState(int solarSysId)
        {
        }

        [MethodImpl(0x8000)]
        private void FoldAllSolarSysItem()
        {
        }

        [MethodImpl(0x8000)]
        public void FoldSolarSysItem(int solarSysId, bool fold)
        {
        }

        [MethodImpl(0x8000)]
        private GuildActionInfo GetActionInfoByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetProcess(UIProcess.ProcessExecMode mode, bool show, bool immedite)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmIssueBtnClick(int configId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGalaxyClick(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueQuestItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossWarningClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string poolName, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShowIssueHintCtrlClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapBtnClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStrikeBtnClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnderwayQuestItemClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIssueSelectedQuest(int configId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUnderwaySelectedQuest(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowIssueHintCtrl()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateIssue(List<ConfigDataGuildActionInfo> issueInfos)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUnderway(string mode, Dictionary<int, List<GuildActionInfo>> infos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(Dictionary<int, List<GuildActionInfo>> underwayInfos, List<ConfigDataGuildActionInfo> issueInfos, string mode, GuildActionUITask.Tab tab, int curEnterSolarSysId, int curIssueConfigId, ulong curUnderwayInstanceId, int curSelectedSolarSysId, Dictionary<string, UnityEngine.Object> resData)
        {
        }
    }
}

