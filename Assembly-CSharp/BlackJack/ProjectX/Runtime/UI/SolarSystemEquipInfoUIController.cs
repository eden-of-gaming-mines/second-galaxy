﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemEquipInfoUIController : UIControllerBase
    {
        public LBEquipGroupBase CurrentEquipGroupInfo;
        private uint m_LastLanuchTime;
        private const string m_State_Normal = "Normal";
        private const string m_State_Disable = "Disable";
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./CDProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image CDProgressBar;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        private static DelegateBridge __Hotfix_UpdateEquipGroupInfo;
        private static DelegateBridge __Hotfix_SetLastLanuchTime;
        private static DelegateBridge __Hotfix_SetToNormalUIState;
        private static DelegateBridge __Hotfix_SetToDisableUIState;
        private static DelegateBridge __Hotfix_SetCDValue;
        private static DelegateBridge __Hotfix_SetIconSprite;

        [MethodImpl(0x8000)]
        public void SetCDValue(float progressValue)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIconSprite(Sprite iconSprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLastLanuchTime(uint time)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToDisableUIState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToNormalUIState()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateEquipGroupInfo(LBEquipGroupBase equip, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }
    }
}

