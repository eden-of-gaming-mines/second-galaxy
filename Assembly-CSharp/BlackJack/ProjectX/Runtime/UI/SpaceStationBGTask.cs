﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class SpaceStationBGTask : UITaskBase
    {
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public SpaceStationUI3DSceneController m_spaceStationUI3DSceneCtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType> EventOnStationBuildingClicked;
        private int m_pressedEventFrameIndex;
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        public const string TaskName = "SpaceStationBGTask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PauseStationBgTask;
        private static DelegateBridge __Hotfix_ResumeStationBgTask;
        private static DelegateBridge __Hotfix_SetStationInputEnable;
        private static DelegateBridge __Hotfix_GetSpaceStationBuilding;
        private static DelegateBridge __Hotfix_GetSpaceStateionBuildingScreenPos;
        private static DelegateBridge __Hotfix_SetHeadQuarterQuestState;
        private static DelegateBridge __Hotfix_OnSpaceStationUI3DSceneObjectClick;
        private static DelegateBridge __Hotfix_OnBuildingClicked;
        private static DelegateBridge __Hotfix_RegisterUIControllerEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIControllerEvent;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableBuildingShop;
        private static DelegateBridge __Hotfix_EnableBuildingBillboard;
        private static DelegateBridge __Hotfix_EnableBuildingHeadQuarter;
        private static DelegateBridge __Hotfix_EnableBuildingRanking;
        private static DelegateBridge __Hotfix_EnableBuildingTradingCenter;
        private static DelegateBridge __Hotfix_EnableBuildingBlackMarket;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_BuildUnlockCheck;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_IsPressEventRaisedThisFrame;
        private static DelegateBridge __Hotfix_OnPressEventRaised;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_add_EventOnStationBuildingClicked;
        private static DelegateBridge __Hotfix_remove_EventOnStationBuildingClicked;

        public event Action<SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType> EventOnStationBuildingClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SpaceStationBGTask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BuildUnlockCheck()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBuildingBillboard(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBuildingBlackMarket(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBuildingHeadQuarter(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBuildingRanking(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBuildingShop(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBuildingTradingCenter(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSpaceStateionBuildingScreenPos(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType type)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetSpaceStationBuilding(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPressEventRaisedThisFrame()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBuildingClicked(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType buildingType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnPressEventRaised()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationUI3DSceneObjectClick(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType objType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public static bool PauseStationBgTask()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIControllerEvent()
        {
        }

        [MethodImpl(0x8000)]
        public static void ResumeStationBgTask()
        {
        }

        [MethodImpl(0x8000)]
        public void SetHeadQuarterQuestState(bool hasQuest2Accept, bool hasQuest2Complete)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStationInputEnable(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIControllerEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

