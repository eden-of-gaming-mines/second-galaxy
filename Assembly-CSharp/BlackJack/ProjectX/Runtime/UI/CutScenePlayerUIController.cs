﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CutScenePlayerUIController : UIControllerBase
    {
        protected LayerRenderSettingDesc m_layerRenderSettingDesc;
        protected GameObject m_cutSceneGo;
        protected ISceneAnimationPlayerController m_cutSceneAnimationCtrl;
        protected List<string> m_playingSoundNames;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCutSceneAnimationEnd;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCutSceneAnimationAttached;
        [AutoBind("./CutSceneRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform CutSceneRoot;
        [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroupButton;
        private static DelegateBridge __Hotfix_PlayCutSceneAnimation;
        private static DelegateBridge __Hotfix_StopCutSceneAnimation;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_GetLayerRenderSettingDesc;
        private static DelegateBridge __Hotfix_OnSceneAnimationMessage;
        private static DelegateBridge __Hotfix_OnSceneAnimationEnd;
        private static DelegateBridge __Hotfix_OnAnimationEvent_PlayAudio;
        private static DelegateBridge __Hotfix_DelayEnableCamera;
        private static DelegateBridge __Hotfix_add_EventOnCutSceneAnimationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnCutSceneAnimationEnd;
        private static DelegateBridge __Hotfix_add_EventOnCutSceneAnimationAttached;
        private static DelegateBridge __Hotfix_remove_EventOnCutSceneAnimationAttached;

        public event Action EventOnCutSceneAnimationAttached
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCutSceneAnimationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator DelayEnableCamera()
        {
        }

        [MethodImpl(0x8000)]
        public LayerRenderSettingDesc GetLayerRenderSettingDesc()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAnimationEvent_PlayAudio(object obj)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSceneAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSceneAnimationMessage(string msg, object obj)
        {
        }

        [MethodImpl(0x8000)]
        public void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayCutSceneAnimation(bool useDefaultCamera, string cutSceneAssetName, Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void StopCutSceneAnimation()
        {
        }

        [CompilerGenerated]
        private sealed class <DelayEnableCamera>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal CutScenePlayerUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

