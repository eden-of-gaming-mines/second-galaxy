﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildFleetManagementUITask : GuildUITaskBase
    {
        private GuildFleetManagementUIController m_mainCtrl;
        private const string ParamKeyPlayPanelOpenAnim = "PlayPanelOpenAnim";
        private GuildFleetSimpleInfo m_curData;
        private bool m_isCreateMode;
        private bool m_needPlayOpenAnim;
        public Action EventCloseParentUITask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool> EventOnRequestGuideInfoUIPause;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildFleetSimpleInfo> EventOnGuildFleetDetailButtonClick;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildFleetManagementUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildFleetManagementUITask_0;
        private static DelegateBridge __Hotfix_StartGuildFleetManagementUITask_1;
        private static DelegateBridge __Hotfix_HideGuildFleetManagementUITask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_SendGuildFleetListSimpleInfoReq;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_EventOnGuildFleetItemDetailClick;
        private static DelegateBridge __Hotfix_GetPopupWindowState;
        private static DelegateBridge __Hotfix_OpenGuildFleetInfoPanel;
        private static DelegateBridge __Hotfix_EventOnGuildCreateFleetItemClick;
        private static DelegateBridge __Hotfix_GetCurCreateFleetSeqId;
        private static DelegateBridge __Hotfix_EventOnCreateGuildFleetConfirm;
        private static DelegateBridge __Hotfix_SendCreateFleetReq;
        private static DelegateBridge __Hotfix_SendRenameFleetReq;
        private static DelegateBridge __Hotfix_EventOnCreateGuildFleetCancel;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_OnParticularsButtonClick;
        private static DelegateBridge __Hotfix_OnLeaveButtonClick;
        private static DelegateBridge __Hotfix_GetFleetMemberPositionStr;
        private static DelegateBridge __Hotfix_SendGuildFleetMemberLeaveReq;
        private static DelegateBridge __Hotfix_OnJoinButtonClick;
        private static DelegateBridge __Hotfix_SendGuildFleetMemberJoinReq;
        private static DelegateBridge __Hotfix_OnDissolveButtonClick;
        private static DelegateBridge __Hotfix_SendGuildFleetDismissReq;
        private static DelegateBridge __Hotfix_OnRenameButtonClick;
        private static DelegateBridge __Hotfix_get_lbGuild;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnRequestGuideInfoUIPause;
        private static DelegateBridge __Hotfix_remove_EventOnRequestGuideInfoUIPause;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnGuildFleetDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFleetDetailButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<GuildFleetSimpleInfo> EventOnGuildFleetDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool> EventOnRequestGuideInfoUIPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildFleetManagementUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnCreateGuildFleetCancel()
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnCreateGuildFleetConfirm(string guildFleetName)
        {
        }

        [MethodImpl(0x8000)]
        public void EventOnGuildCreateFleetItemClick()
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnGuildFleetItemDetailClick(int index, GuildFleetSimpleInfo data)
        {
        }

        [MethodImpl(0x8000)]
        private int GetCurCreateFleetSeqId()
        {
        }

        [MethodImpl(0x8000)]
        private string GetFleetMemberPositionStr(string separator = null)
        {
        }

        [MethodImpl(0x8000)]
        private string GetPopupWindowState(GuildFleetSimpleInfo data)
        {
        }

        [MethodImpl(0x8000)]
        public void HideGuildFleetManagementUITask(bool isImmediate, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDissolveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJoinButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnParticularsButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRenameButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OpenGuildFleetInfoPanel(GuildFleetSimpleInfo data)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void SendCreateFleetReq(string guildFleetName)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetDismissReq()
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetListSimpleInfoReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetMemberJoinReq()
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetMemberLeaveReq()
        {
        }

        [MethodImpl(0x8000)]
        private void SendRenameFleetReq(string guildFleetName)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildFleetManagementUITask StartGuildFleetManagementUITask(Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void StartGuildFleetManagementUITask(bool isPlayPanelOpenAnim = true, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockGuildClient lbGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideGuildFleetManagementUITask>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal GuildFleetManagementUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess process, bool result)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetListSimpleInfoReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                GuildFleetListSimpleInfoReqNetTask task2 = task as GuildFleetListSimpleInfoReqNetTask;
                if ((task2 == null) || task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
            }
        }
    }
}

