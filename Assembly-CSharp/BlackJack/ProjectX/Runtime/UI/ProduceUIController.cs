﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnWatchButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCancelButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBindItemProduceConfirmButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBindItemProduceCancelButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBindItemProduceNoTipButtonClick;
        public ProduceQueueUIController ProduceQueueCtrl;
        public ProduceMaterialCostInfoUIController ProduceMaterialCostCtrl;
        public ProduceStateInfoUIController ProduceStateCtrl;
        public ProduceSupCaptionUIController ProduceSupCaptainCtrl;
        public ProduceBGUIController ProduceBGUICtrl;
        protected const string CheckState = "Check";
        protected const string NoCheckState = "NOCheck";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./ProduceBindingHintPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_bindItemProduceTipCtrl;
        [AutoBind("./ProduceBindingHintPanel/CheckGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_noTipButtonCtrlCtrl;
        [AutoBind("./ProduceBindingHintPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BindItemProduceComfirmButton;
        [AutoBind("./ProduceBindingHintPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BindItemProduceCancelButton;
        [AutoBind("./ProduceBindingHintPanel/CheckGroup/FrameButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NoTipButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./WatchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WatchButton;
        [AutoBind("./ProduceConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./ProduceConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./ProduceConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ProduceConfirmPanel;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProduceNameText;
        [AutoBind("./NameText/HelpIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public SystemDescriptionController m_systemDescriptionCtrl;
        [AutoBind("./MoneyGroup/CMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BindMoneyGroup;
        [AutoBind("./MoneyGroup/CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BindMoneyValueText;
        [AutoBind("./MoneyGroup/CMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BindMoneyAddButton;
        [AutoBind("./MoneyGroup/GuilgSMoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildGradeMoneyGroup;
        [AutoBind("./MoneyGroup/GuilgSMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildTradeMoneyValueText;
        [AutoBind("./MoneyGroup/GuilgSMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildTradeMoneyAddButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitPanelState;
        private static DelegateBridge __Hotfix_UpdateCurrencyInfo;
        private static DelegateBridge __Hotfix_ShowOrHideNoCaptainProduceConfirmPanel;
        private static DelegateBridge __Hotfix_GetBindItemProduceConfirmPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetFirstEmptyProduceLineRect;
        private static DelegateBridge __Hotfix_GetAddBlueprintRect;
        private static DelegateBridge __Hotfix_SetNoTipCheckState;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnWatchButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBindItemProduceCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBindItemProduceNoTipButtonClick;
        private static DelegateBridge __Hotfix_OnBindItemProduceConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWatchButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWatchButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBindItemProduceConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBindItemProduceConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBindItemProduceCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBindItemProduceCancelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBindItemProduceNoTipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBindItemProduceNoTipButtonClick;

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBindItemProduceCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBindItemProduceConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBindItemProduceNoTipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWatchButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public RectTransform GetAddBlueprintRect()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetBindItemProduceConfirmPanelShowOrHideProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstEmptyProduceLineRect()
        {
        }

        [MethodImpl(0x8000)]
        public void InitPanelState(bool isGuild)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBindItemProduceCancelButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBindItemProduceConfirmButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBindItemProduceNoTipButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCancelButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnConfirmButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWatchButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNoTipCheckState(bool check)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideNoCaptainProduceConfirmPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCurrencyInfo(ulong currency, bool isGuild)
        {
        }
    }
}

