﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildWalletAnnouncementSetReqNetTask : NetWorkTransactionTask
    {
        public int m_result;
        private string m_walletAnnouncement;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildWalletAnnouncementSetAck;

        [MethodImpl(0x8000)]
        public GuildWalletAnnouncementSetReqNetTask(string announcement, bool blockedUI = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildWalletAnnouncementSetAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

