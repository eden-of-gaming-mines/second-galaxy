﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class CommonDelayDesc : MonoBehaviour
    {
        public List<DelayDesc> DelayList;

        [Serializable]
        public class DelayDesc
        {
            public string StateName;
            public float DelayTime;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

