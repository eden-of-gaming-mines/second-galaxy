﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class CharactorObserveReqNetTas : NetWorkTransactionTask
    {
        private readonly string m_destGameUserId;
        public int m_ackResult;
        public CharactorObserverAck m_charactorObserverAckInfo;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCharactorObserverAck;

        [MethodImpl(0x8000)]
        public CharactorObserveReqNetTas(string destGameUserId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCharactorObserverAck(CharactorObserverAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

