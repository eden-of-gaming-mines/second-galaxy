﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class DelegateMissionBalanceReqNetTask : NetWorkTransactionTask
    {
        private ulong m_missionInstanceId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private List<ItemInfo> <RewardList>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ReqResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnMissionBalance;
        private static DelegateBridge __Hotfix_set_RewardList;
        private static DelegateBridge __Hotfix_get_RewardList;
        private static DelegateBridge __Hotfix_set_ReqResult;
        private static DelegateBridge __Hotfix_get_ReqResult;

        [MethodImpl(0x8000)]
        public DelegateMissionBalanceReqNetTask(ulong missionInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMissionBalance(int result, List<ItemInfo> rewardList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public List<ItemInfo> RewardList
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int ReqResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

