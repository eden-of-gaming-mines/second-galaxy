﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum GuildManagerTabType
    {
        GuildInfo,
        GuildAction,
        GuildBenefits,
        GuildFleetManagement,
        GuildWar
    }
}

