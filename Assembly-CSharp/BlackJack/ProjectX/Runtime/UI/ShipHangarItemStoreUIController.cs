﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipHangarItemStoreUIController : UIControllerBase
    {
        private string m_itemStoreListUIPrefabAssetName;
        private string m_itemSimpleInfoUIPrefabAssetName;
        [AutoBind("./ItemStoreListPanel/ItemStoreListUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MotherShipItemStoreUIDummy;
        [AutoBind("./ShipItemStoreListPanel/ItemStoreListUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipItemStoreUIDummy;
        [AutoBind("./ItemSimpleInfoUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoUIPrefabDummy;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./UnloadAllButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnloadAllButton;
        [AutoBind("./ItemStoreListPanel/ItemStoreListUIPrefabDummy/ErrorImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MotherShipItemStoreErrorRoot;
        [AutoBind("./ItemStoreListPanel/ItemStoreListUIPrefabDummy/ErrorImage/ErrorText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MotherShipItemStoreErrorText;
        [AutoBind("./ShipItemStoreListPanel/ItemStoreListUIPrefabDummy/ErrorImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipItemStoreErrorRoot;
        [AutoBind("./ShipItemStoreListPanel/ItemStoreListUIPrefabDummy/ErrorImage/ErrorText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipItemStoreErrorText;
        [AutoBind("./ArrowRight", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ArrowRight;
        [AutoBind("./ArrowLeft", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ArrowLeft;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        public ItemStoreListUIController m_motherShipItemStoreUICtrl;
        public ItemStoreListUIController m_hangarShipItemStoreUICtrl;
        public ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowMotherShipItemStoreErrorText;
        private static DelegateBridge __Hotfix_ShowShipItemStoreErrorText;
        private static DelegateBridge __Hotfix_ShowUnloadAllButton;
        private static DelegateBridge __Hotfix_ShowMoveItemArrow;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMotherShipItemStoreErrorText(bool isshow, string msg = "")
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMoveItemArrow(bool isshow, bool isleft)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipItemStoreErrorText(bool isshow, string msg = "")
        {
        }

        [MethodImpl(0x8000)]
        public void ShowUnloadAllButton(bool isshow)
        {
        }
    }
}

