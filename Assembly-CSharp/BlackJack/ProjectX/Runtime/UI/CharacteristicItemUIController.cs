﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CharacteristicItemUIController : UIControllerBase
    {
        [AutoBind("./ProfitValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProfitValue;
        [AutoBind("./PropertyName", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyName;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Image BgImage;
        private static DelegateBridge __Hotfix_SetCharacteristicProfitValue;
        private static DelegateBridge __Hotfix_SetCharacteristicName;
        private static DelegateBridge __Hotfix_SetCharacteristicBgImage;

        [MethodImpl(0x8000)]
        public void SetCharacteristicBgImage(bool hasBg)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharacteristicName(string propName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCharacteristicProfitValue(string val)
        {
        }
    }
}

