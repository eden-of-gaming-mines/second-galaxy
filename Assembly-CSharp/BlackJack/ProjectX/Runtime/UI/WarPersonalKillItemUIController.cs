﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class WarPersonalKillItemUIController : UIControllerBase
    {
        private CommonCaptainIconUIController m_captainIconUICtrl;
        public Action<GuildBattlePlayerKillLostStatInfo, UIControllerBase> EventOnPlayerIconClick;
        private GuildBattlePlayerKillLostStatInfo m_playerInfo;
        private bool m_isInit;
        private RectTransform m_separatorTransform;
        [AutoBind("./CaptainOperationDummyUp", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TipDummyUp;
        [AutoBind("./CaptainOperationDummyDown", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TipDummyDown;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RankingStateCtrl;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NameButton;
        [AutoBind("./BgImageRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemShowStateCtrl;
        [AutoBind("./RankingText04", AutoBindAttribute.InitState.NotInit, false)]
        public Text RankingTextOther;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlayerNameText;
        [AutoBind("./KillNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillNumText;
        [AutoBind("./KillValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DestroyValueText;
        [AutoBind("./LossValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LossValueText;
        [AutoBind("./FormGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FactionStateCtrl;
        [AutoBind("./CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform CommonCaptainUIInfoTrans;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_ReigsterUIEvent;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_OnPlayerIconClick_1;
        private static DelegateBridge __Hotfix_OnPlayerIconClick_0;
        private static DelegateBridge __Hotfix_GetCaptainOperationDummyPostion;

        [MethodImpl(0x8000)]
        public Vector3 GetCaptainOperationDummyPostion()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(RectTransform separatorTransform)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerIconClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void ReigsterUIEvent(Action<GuildBattlePlayerKillLostStatInfo, UIControllerBase> itemClick)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(int ranking, GuildBattlePlayerKillLostStatInfo info, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

