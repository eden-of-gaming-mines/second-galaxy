﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipHangarShipListUIController : UIControllerBase
    {
        public List<IStaticFlagShipDataContainer> m_shipList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private string m_shipListUIItemPoolName;
        private int m_unlockSlotCount;
        private int m_buildingLevel;
        public int m_guildFlagShipSelectIndex;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGuildFlagShipListItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnGuildFlagShipListItemUnpackComplete;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ShipListScrollViewEasyObjectPool;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ShipListScrollViewLoopVerticalScrollRect;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemRootRectTransform;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipHangarShipListUI;
        private static DelegateBridge __Hotfix_UpdateSingleShipHangerSelected;
        private static DelegateBridge __Hotfix_UpdateSingleShipHangar;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnGuildFlagShipUIItemClick;
        private static DelegateBridge __Hotfix_OnGuildFlagShipUIItemFill;
        private static DelegateBridge __Hotfix_OnGuildFalgShipUnpackingEnd;
        private static DelegateBridge __Hotfix_add_EventOnGuildFlagShipListItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFlagShipListItemClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildFlagShipListItemUnpackComplete;
        private static DelegateBridge __Hotfix_remove_EventOnGuildFlagShipListItemUnpackComplete;

        public event Action<int> EventOnGuildFlagShipListItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGuildFlagShipListItemUnpackComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFalgShipUnpackingEnd(UIControllerBase flagShipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipUIItemClick(UIControllerBase shipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipUIItemFill(UIControllerBase flagShipItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFlagShipHangarShipListUI(List<IStaticFlagShipDataContainer> shipList, int unlockSlotCount, int buildingLevel, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int selectedIndex, int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleShipHangar(List<IStaticFlagShipDataContainer> shipList, int unlockSlotCount, int buildingLevel, int updateSlotIndex, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleShipHangerSelected(int selectIndex)
        {
        }
    }
}

