﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapDrawingLineConfig
    {
        [Header("绘制的星域图中，连线的最小宽度")]
        public float m_lineWidthMin;
        [Header("绘制的星域图中，连线的最大宽度")]
        public float m_lineWidthMax;
        [Header("绘制的星域图中，详细导航路线的宽度与普通连线的比例")]
        public float m_detailNavigationLineWidthRatio;
        [Header("连线取最小宽度时对应的缩放比")]
        public float m_scaleValueForLineWidthMin;
        [Header("连线取最大宽度时对应的缩放比（必须大于m_scaleValueForLineWidthMin）")]
        public float m_scaleValueForLineWidthMax;
        [Header("星域图中，连线的颜色")]
        public Color m_starMapLineColor;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

