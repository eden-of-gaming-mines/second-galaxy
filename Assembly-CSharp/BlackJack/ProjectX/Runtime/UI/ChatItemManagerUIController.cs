﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ChatItemManagerUIController : UIControllerBase
    {
        private ChatContentType m_contentType;
        private bool m_isSelf;
        private GameObject m_currPrefab;
        private bool m_isHasChildPrefab;
        private const string RightLinkPrefabName = "RightChatLinkPrefab";
        private const string RightTextPrefabName = "RightChatTextPrefab";
        private const string RightSoundPrefabName = "RightChatSoundPrefab";
        private const string LeftLinkPrefabName = "LeftChatLinkPrefab";
        private const string LeftTextPrefabName = "LeftChatTextPrefab";
        private const string LeftSoundPrefabName = "LeftChatSoundPrefab";
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_scrollItemCtrl;
        public ChatItemControllerBase m_chatTextItemCtrl;
        private static DelegateBridge __Hotfix_GetItemPrefabByContentType;
        private static DelegateBridge __Hotfix_GetPrefabName;

        [MethodImpl(0x8000)]
        public GameObject GetItemPrefabByContentType(ChatContentType contentType, bool isSelf)
        {
        }

        [MethodImpl(0x8000)]
        private string GetPrefabName(ChatContentType contentType, bool isSelf)
        {
        }
    }
}

