﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class PropertyModifyInfoItemUIController : UIControllerBase
    {
        [AutoBind("./PropertyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyNameText;
        [AutoBind("./PropertyValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_propertyValueText;
        private static DelegateBridge __Hotfix_SetPropertyName;
        private static DelegateBridge __Hotfix_SetPropertyValue;
        private static DelegateBridge __Hotfix_HidePropertyInfo;

        [MethodImpl(0x8000)]
        public void HidePropertyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPropertyValue(string valueStr)
        {
        }
    }
}

