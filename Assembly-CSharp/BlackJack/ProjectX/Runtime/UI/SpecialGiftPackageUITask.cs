﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpecialGiftPackageUITask : UITaskBase
    {
        private SpecialGiftPackageUIController m_specialGiftPackageUIController;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRequestRefreshMoneyInfo;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action, bool> EventOnRequestBlackMarketManagerUIPause;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private List<LBRechargeGiftPackage> m_specialGiftPackageList;
        private bool? m_isPrepare4SdkProductListSuccess;
        private bool? m_isPrepare4RechargeGiftPackageSuccess;
        private int m_currUpdatePackageIndex;
        private int m_lastUpdatePackageId;
        private DateTime m_expireTimeRefreshReqCooldownTime;
        private static string ParamKeyIsShow;
        private static string ParamKeyIsHide;
        private static string ParamKeyIsPlayAniImmedite;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Comparison<LBRechargeGiftPackage> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTaskWithPrepare;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnPrepareDataEnd;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache4LastPackageId;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_RegisterPlayCtxEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayCtxEvent;
        private static DelegateBridge __Hotfix_OnRechargeGiftPackageDeliverNtf;
        private static DelegateBridge __Hotfix_OnGiftPackageItemBuyButtonClick;
        private static DelegateBridge __Hotfix_ShowSpecailGiftPackage;
        private static DelegateBridge __Hotfix_HideSpecialGiftPackage;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_Tick4GfitPackageExpireTime;
        private static DelegateBridge __Hotfix_RefreshPackageListWithCurrPackgetKeepLocation;
        private static DelegateBridge __Hotfix_OnGiftContentItemIconClick;
        private static DelegateBridge __Hotfix_RequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_PauseGiftSimpleInfoPanel;
        private static DelegateBridge __Hotfix_get_LBSdkInterface;
        private static DelegateBridge __Hotfix_get_LBGiftPackageClient;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnRequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_remove_EventOnRequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_add_EventOnRequestBlackMarketManagerUIPause;
        private static DelegateBridge __Hotfix_remove_EventOnRequestBlackMarketManagerUIPause;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<Action, bool> EventOnRequestBlackMarketManagerUIPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRequestRefreshMoneyInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SpecialGiftPackageUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void HideSpecialGiftPackage(bool immedite, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftContentItemIconClick(int packageIndex, ILBStoreItemClient rewardItem, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGiftPackageItemBuyButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrepareDataEnd(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRechargeGiftPackageDeliverNtf(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void PauseGiftSimpleInfoPanel(Action evntAfterPause, bool isIgnoreAnim = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshPackageListWithCurrPackgetKeepLocation()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPlayCtxEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestRefreshMoneyInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(int packageIndex, ILBStoreItemClient rewardItem, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSpecailGiftPackage(bool immedite, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTaskWithPrepare(Action<bool> onPrepareEnd, Action onResLoadEnd = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void Tick4GfitPackageExpireTime()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterPlayCtxEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDataCache4LastPackageId()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockClientSdkInterface LBSdkInterface
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockRechargeGiftPackageClient LBGiftPackageClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideSpecialGiftPackage>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal SpecialGiftPackageUITask $this;

            internal void <>m__0(UIProcess p, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
            internal SpecialGiftPackageUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    Debug.LogError("SpecialGiftPackageUITask  RequestSdkRechargeGoodsInfoList failed");
                }
                this.$this.m_isPrepare4SdkProductListSuccess = new bool?(res);
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }

            internal void <>m__1(bool res)
            {
                if (!res)
                {
                    Debug.LogError("SpecialGiftPackageUITask  SendRechargeGiftPackageListReq failed");
                }
                this.$this.m_isPrepare4RechargeGiftPackageSuccess = new bool?(res);
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey2
        {
            internal ILBStoreItemClient rewardItem;
            internal Vector3 pos;
            internal SpecialGiftPackageUITask $this;

            internal void <>m__0(UIIntent intent)
            {
                string mode = "OnlyDetail";
                if (!ClientStoreItemBaseHelper.IsItemHasDetailInfo(this.rewardItem))
                {
                    mode = "NoButton";
                }
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.rewardItem, intent, true, this.pos, mode, ItemSimpleInfoUITask.PositionType.UseInput, false, null, null, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            IsShow,
            IsHide,
            IsPlayAniImmedite,
            NeedLoadDynamicRes,
            RefreshAllPackage,
            RefreshWithLastPackageId
        }
    }
}

