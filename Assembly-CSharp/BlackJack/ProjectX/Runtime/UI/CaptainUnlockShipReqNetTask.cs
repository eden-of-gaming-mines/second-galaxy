﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CaptainUnlockShipReqNetTask : NetWorkTransactionTask
    {
        private ulong m_captainInstanceId;
        private int m_shipId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <UnlockShipResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnUnlockShip;
        private static DelegateBridge __Hotfix_set_UnlockShipResult;
        private static DelegateBridge __Hotfix_get_UnlockShipResult;

        [MethodImpl(0x8000)]
        public CaptainUnlockShipReqNetTask(ulong captainInsId, int shipId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUnlockShip(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int UnlockShipResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

