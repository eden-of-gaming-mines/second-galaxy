﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapForStarfieldBrushInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GEBrushType> EventOnBrushTypeChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTipClick;
        private List<GEBrushType> m_brushTypeList;
        private bool m_ignoreBrushTypeChangedEvent;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_legendUICtrl;
        [AutoBind("./RightButtonDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_brushTypeSelectedDropdown;
        [AutoBind("./NormalTextDesc/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_textLegendInfoText;
        [AutoBind("./GradualColor/Image", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIEffectGradual m_gradualEffectCtrl;
        [AutoBind("./GradualColor/LeftText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_gradualLegendLeftText;
        [AutoBind("./GradualColor/RightText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_gradualLegendRightText;
        [AutoBind("./TextBgImage/TextCloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipBGButton;
        [AutoBind("./InfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipOpenButton;
        [AutoBind("./TextBgImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tipInfoText;
        [AutoBind("./TextBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipInfoStateCtrl;
        private const string TextLegendUIStateName = "NormalTextDesc";
        private const string SecurityLevelLegendUIStateName = "SecurityLevel";
        private const string MineralTypeLegendUIStateName = "MineralType";
        private const string WormholeLegendUIStateName = "Wormhole";
        private const string GradualColorLegendUIStateName = "GradualColor";
        private const string InfectLegendUIStateName = "Infect";
        private const string GuildBattleStateUIStateName = "FightGroup";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_HideDropDownList;
        private static DelegateBridge __Hotfix_SetBrushType;
        private static DelegateBridge __Hotfix_EnableToggles;
        private static DelegateBridge __Hotfix_GetGradualLegendTextInfoFromBrushInfo;
        private static DelegateBridge __Hotfix_SetGradualColorByBrushInfo;
        private static DelegateBridge __Hotfix_GetRectTransformByBrushType;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBrushTypeChanged;
        private static DelegateBridge __Hotfix_OnTipInfoOpenButtonClick;
        private static DelegateBridge __Hotfix_OnTipInfoBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBrushTypeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnBrushTypeChanged;
        private static DelegateBridge __Hotfix_add_EventOnTipClick;
        private static DelegateBridge __Hotfix_remove_EventOnTipClick;

        public event Action<GEBrushType> EventOnBrushTypeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTipClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableToggles(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public KeyValuePair<string, string> GetGradualLegendTextInfoFromBrushInfo(ConfigDataGEBrushInfo brushInfo)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetRectTransformByBrushType(GEBrushType brushType)
        {
        }

        [MethodImpl(0x8000)]
        public void HideDropDownList()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBrushTypeChanged(int brushIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipInfoBGButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipInfoOpenButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBrushType(GEBrushType brushType, bool isOnlyUpdateUI = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGradualColorByBrushInfo(ConfigDataGEBrushInfo brushInfo)
        {
        }
    }
}

