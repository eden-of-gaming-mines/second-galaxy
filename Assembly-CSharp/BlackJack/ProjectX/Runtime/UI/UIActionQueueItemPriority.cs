﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;

    public class UIActionQueueItemPriority
    {
        public static int StationInfoTipPriority;
        public static int QuestNameShowPriority;
        public static int QuestCompleteDialogPriority;
        public static int QuestCompleteEffectPriority;
        public static int CharacterLevelUpPriorty;
        public static int FunctionUnLockAnimPriorty;
        public static int NewShipGetPriorty;
        public static int NewCaptainGetPriorty;
        public static int SailReportPriority;
        public static int AppScorePriority;
        public static int ChapterCutScenePriorty;
        public static int QuestAcceptDialogPriority;
        public static int InSpaceSimpleDialogPriorty;
        public static int InSpaceNpcDialogPriorty;
        public static int UserGuidePriorty;
        public static int RecommendSpecialGiftPackagePriorty;
        public static int InSpaceReturnToStationPriority;
        public static int RecommondItemStorePriority;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

