﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SolarSystemTargetPvpFlagDetailInfoUITask : UITaskBase
    {
        public const string SolarSystemTargetPvpFlagDetailInfoSelf = "SolarSystemTargetPVPFlagDetailInfo_Self";
        public const string SolarSystemTargetPvpFlagDetailInfoTarget = "SolarSystemTargetPVPFlagDetailInfo_Target";
        private SolarSystemTargetPvpFlagDetailInfoUIController m_mainCtrl;
        private PlayerPVPInfo m_playerPvpInfo;
        private float m_refreshDelayTime;
        private const float RefreshDelayTime = 10f;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKeyTargetShip = "TargetShip";
        public const string ParamKeyPosition = "Position";
        private ClientSpaceShip m_targetShip;
        private Vector3 m_targetPvpFlagPanlePos;
        public const string TaskName = "SolarSystemTargetPvpFlagDetailInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_InitPipeLineCtxStateFromUIIntent;
        private static DelegateBridge __Hotfix_RegisterTargetPvpEvent;
        private static DelegateBridge __Hotfix_UnregisterTargetPvpEvent;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache_Self;
        private static DelegateBridge __Hotfix_UpdateDataCache_Other;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnIncCriminalLevel;
        private static DelegateBridge __Hotfix_OnResetJustCrime;
        private static DelegateBridge __Hotfix_ResetCriminalHunter;
        private static DelegateBridge __Hotfix_OnCriminalHunterTimeOut;
        private static DelegateBridge __Hotfix_OnJustCrimeTimeOut;
        private static DelegateBridge __Hotfix_RefreshPvpInfo;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_OnPvpStateCooldownTimeOut;
        private static DelegateBridge __Hotfix_SetMainWindowState;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public SolarSystemTargetPvpFlagDetailInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPipeLineCtxStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCriminalHunterTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnIncCriminalLevel(float level)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnJustCrimeTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPvpStateCooldownTimeOut()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnResetJustCrime(DateTime time)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected void RefreshPvpInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterTargetPvpEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void ResetCriminalHunter(DateTime time)
        {
        }

        [MethodImpl(0x8000)]
        private void SetMainWindowState(bool isShow = true, Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterTargetPvpEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_Other()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_Self()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetMainWindowState>c__AnonStorey0
        {
            internal Action action;

            [MethodImpl(0x8000)]
            internal void <>m__0(UIProcess uiProcess, bool isComplete)
            {
            }
        }
    }
}

