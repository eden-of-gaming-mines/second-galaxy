﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class StarMapForStarfieldButtonWndUIController : UIControllerBase
    {
        [AutoBind("./WndRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_wndRootTrans;
        [AutoBind("./ButtonGroupRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_buttonGroupTrans;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSetUpButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackToPreviousStarMapButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnOrientationButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackToPreviousUIButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnHomeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPVEScanProbeButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPVPScanProbeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnManualScanProbeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnGuildSentrySingalProbeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_JumpButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_SendToChatButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnOrientedTargetSelectWnd_CommanderButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnOrientedTargetSelectWnd_MotherShipButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnOrientedTargetSelectWnd_GuildBaseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnOrientedTargetSelectWnd_BackgroundButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSSDetailInfoWnd_BackgroundClick;
        public StarMapOrientedTargetSelectWndUIController m_orientedTargetSelectWndUICtrl;
        public StarMapSSDetailInfoWndUIController m_SSDetailInfoWndCtrl;
        public StarMapCommonButtonGroupUIController m_commonButtonGroupCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnBackToPreviousUIButtonClick;
        private static DelegateBridge __Hotfix_OnHomeButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_BackgroundClick;
        private static DelegateBridge __Hotfix_OnSSDetailInfoWnd_SendToChatButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_GuildBaseButtonClick;
        private static DelegateBridge __Hotfix_OnOrientedTargetSelectWnd_BackgroudButtonClick;
        private static DelegateBridge __Hotfix_OnSetUpButtonClick;
        private static DelegateBridge __Hotfix_OnBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnOrientationButtonClick;
        private static DelegateBridge __Hotfix_OnPVEScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnPVPScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnManualScanProbeButtonClick;
        private static DelegateBridge __Hotfix_OnGuildSentrySignalProbeButtonClick;
        private static DelegateBridge __Hotfix_ShowSSDetailInfoWnd;
        private static DelegateBridge __Hotfix_HideSSDetailInfoWnd;
        private static DelegateBridge __Hotfix_ShowOrientedTargetSelectWnd;
        private static DelegateBridge __Hotfix_HideOrientedTargetSelectWnd;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_add_EventOnSetUpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSetUpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackToPreviousStarMapButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientationButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientationButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackToPreviousUIButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackToPreviousUIButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnHomeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnHomeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPVEScanProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPVEScanProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPVPScanProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPVPScanProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnManualScanProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnManualScanProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildSentrySingalProbeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildSentrySingalProbeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSDetailInfoWnd_JumpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSDetailInfoWnd_SendToChatButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSDetailInfoWnd_SendToChatButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientedTargetSelectWnd_CommanderButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientedTargetSelectWnd_MotherShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientedTargetSelectWnd_GuildBaseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientedTargetSelectWnd_GuildBaseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnOrientedTargetSelectWnd_BackgroundButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOrientedTargetSelectWnd_BackgroundButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSDetailInfoWnd_BackgroundClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSDetailInfoWnd_BackgroundClick;

        public event Action EventOnBackToPreviousStarMapButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBackToPreviousUIButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnGuildSentrySingalProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnHomeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnManualScanProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientationButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientedTargetSelectWnd_BackgroundButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientedTargetSelectWnd_CommanderButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientedTargetSelectWnd_GuildBaseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOrientedTargetSelectWnd_MotherShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPVEScanProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPVPScanProbeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSetUpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSSDetailInfoWnd_BackgroundClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_EnterStarMapForSSButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_JumpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemInfo> EventOnSSDetailInfoWnd_SendToChatButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideOrientedTargetSelectWnd()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideSSDetailInfoWnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToPreviousStarMapButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackToPreviousUIButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSentrySignalProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHomeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnManualScanProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_BackgroudButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_CommanderButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_GuildBaseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOrientedTargetSelectWnd_MotherShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVEScanProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPVPScanProbeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSetUpButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_BackgroundClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_EnterStarMapForSSButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_JumpButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSDetailInfoWnd_SendToChatButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrientedTargetSelectWnd()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowSSDetailInfoWnd(GDBStarfieldsInfo starfieldInfo, GDBStargroupInfo stargroupInfo, GDBSolarSystemInfo solarSystemInfo)
        {
        }
    }
}

