﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GMPanelUITask : UITaskBase
    {
        private string m_currState;
        private GMPanelController.NPCState m_npcState;
        private const string StateNormal = "Enter";
        private const string StateGM = "GM";
        private const string StateNPC = "NPC";
        private const string StateDisplay = "Display";
        private GMPanelController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private const string TaskName = "GMPanelUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OpenGMPanel;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_BringTipWindowToTop;
        private static DelegateBridge __Hotfix_CloseButtonClickFun;
        private static DelegateBridge __Hotfix_SubmitButtonClickFun;
        private static DelegateBridge __Hotfix_OnGMButtonClick;
        private static DelegateBridge __Hotfix_OnNPCButtonClick;
        private static DelegateBridge __Hotfix_OnDisplayButtonClick;
        private static DelegateBridge __Hotfix_OnGlobalNpcToggleChanged;
        private static DelegateBridge __Hotfix_OnNpcResToggleChanged;
        private static DelegateBridge __Hotfix_OnNpcAudioToggleChanged;
        private static DelegateBridge __Hotfix_OnReviewStateChanged;
        private static DelegateBridge __Hotfix_OnHuaweiStateChanged;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GMPanelUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringTipWindowToTop()
        {
        }

        [MethodImpl(0x8000)]
        public void CloseButtonClickFun(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisplayButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGlobalNpcToggleChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGMButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHuaweiStateChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcAudioToggleChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNPCButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcResToggleChanged(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReviewStateChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public static void OpenGMPanel()
        {
        }

        [MethodImpl(0x8000)]
        public void SubmitButtonClickFun(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum PipeLineStateMaskType
        {
            PanelStateChanged,
            NpcStateChanged
        }
    }
}

