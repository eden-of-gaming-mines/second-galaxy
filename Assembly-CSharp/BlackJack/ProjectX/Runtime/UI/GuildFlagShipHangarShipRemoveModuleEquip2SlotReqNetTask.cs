﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask : NetWorkTransactionTask
    {
        private ulong m_shipHangarInstanseId;
        private int m_shipSlotIndex;
        private int m_equipSlotIndex;
        private int m_shipStoreItemIndex;
        private int m_equipSlotItemIndex;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildFlagShipHangarShipRemoveModuleEquip2SlotAck;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;

        [MethodImpl(0x8000)]
        public GuildFlagShipHangarShipRemoveModuleEquip2SlotReqNetTask(ulong shipHangarInstanseId, int shipSlotIndex, int hangarShipStoreItemIndex, int equipSlotIndex, int equipItemStoreIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFlagShipHangarShipRemoveModuleEquip2SlotAck(int result, bool isStoreContexChanged)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

