﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class CaptainWingManInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <WingManIndex>k__BackingField;
        private WingManItemButtonState m_wingManState;
        private CommonCaptainIconUIController m_captainIconCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WingManItemButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WingManInfoStateCtrl;
        [AutoBind("./Captain/CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainIconDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetCaptainWingManInfo;
        private static DelegateBridge __Hotfix_get_WingManIndex;
        private static DelegateBridge __Hotfix_set_WingManIndex;
        private static DelegateBridge __Hotfix_get_WingManState;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainWingManInfo(LBStaticHiredCaptain captain, bool isCurrCap, bool isSelected, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public int WingManIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public WingManItemButtonState WingManState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

