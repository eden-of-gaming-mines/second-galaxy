﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class MailItemDetailUICtrl : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnAttchementItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnAttchementItem3DTouch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnMailDetailScrollViewClick;
        private float lastUpdatetime;
        private bool hasTimeOut;
        public LBStoredMailClient m_mailInfoClient;
        private List<CommonItemIconUIController> ItemUICtrlList;
        private string buttonState_Enable;
        private string buttonState_Disable;
        private const string ItemPrefabName = "CommonItemUIPrefab";
        [AutoBind("./MailDetailScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler MailDetailScollViewEventHandler;
        [AutoBind("./ButtonGroup/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DeleteButton;
        [AutoBind("./ButtonGroup/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DeleteButtonState;
        [AutoBind("./ButtonGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GetAttachementButton;
        [AutoBind("./ButtonGroup/RePlyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RePlyMailButton;
        [AutoBind("./ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        public GameObject ItemUICtrlPrefab;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel/MoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MoneyGroup;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel/ItemGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummyPoint;
        [AutoBind("./MailDetailScrollView/Viewport/Content/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DetailText;
        [AutoBind("./TitleTextGroup/TitleNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./TitleTextGroup/TimeTextRoot/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./TitleTextGroup/TimeTextRoot/EnglishTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EnglishTimeText;
        [AutoBind("./TitleTextGroup/TimeTextRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TimeTextRootUIState;
        [AutoBind("./DateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DateText;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel/MoneyGroup/BindMoney/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text BindMoneyText;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel/MoneyGroup/TradeMoney/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeMoneyText;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel/MoneyGroup/RealMoney/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text RealMoneyText;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel/MoneyGroup/CreditCurrency_One/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text CreditCurrencyOneText;
        [AutoBind("./MailDetailScrollView/Viewport/Content/AttachementPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AttachementPanel;
        [AutoBind("./MailDetailScrollView/Viewport/Content/PlayerInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PlayerInfoPanel;
        [AutoBind("./MailDetailScrollView/Viewport/Content/SystemInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SystemInfoPanel;
        [AutoBind("./MailDetailScrollView/Viewport/Content/PlayerInfoPanel/PlayerIcon/CharacterIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SendPlayerIconImgae;
        [AutoBind("./MailDetailScrollView/Viewport/Content/PlayerInfoPanel/PlayerIcon/JobIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionIconImage;
        [AutoBind("./MailDetailScrollView/Viewport/Content/PlayerInfoPanel/CharacterNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SnedPlayerNameText;
        [AutoBind("./MailDetailScrollView/Viewport/Content/SystemInfoPanel/CharacterNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SystemSnedPlayerNameText;
        [AutoBind("./TranslateButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TranslateButtonRoot;
        [AutoBind("./TranslateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TranslateButton;
        [AutoBind("./TranslateButton/TranslateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TranslateButtonText;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_IsSentByPlayer;
        private static DelegateBridge __Hotfix_UpdateMailInfoDetail;
        private static DelegateBridge __Hotfix_UpdateAttchementInfo;
        private static DelegateBridge __Hotfix_UpdateTimeOutText;
        private static DelegateBridge __Hotfix_GetAttchementItemPositionByIndex;
        private static DelegateBridge __Hotfix_GetAttchementItemSizeByIndex;
        private static DelegateBridge __Hotfix_ClearMoneyText;
        private static DelegateBridge __Hotfix_SetMoneyText4CurrencyType;
        private static DelegateBridge __Hotfix_IsShowTranslateButton;
        private static DelegateBridge __Hotfix_OnAttchementItemClick;
        private static DelegateBridge __Hotfix_OnAttchementItem3DTouch;
        private static DelegateBridge __Hotfix_OnMailDetailScrollViewClick;
        private static DelegateBridge __Hotfix_add_EventOnAttchementItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnAttchementItemClick;
        private static DelegateBridge __Hotfix_add_EventOnAttchementItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnAttchementItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnMailDetailScrollViewClick;
        private static DelegateBridge __Hotfix_remove_EventOnMailDetailScrollViewClick;
        private static DelegateBridge __Hotfix_get_m_playerCtx;

        public event Action<int> EventOnAttchementItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnAttchementItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnMailDetailScrollViewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void ClearMoneyText()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetAttchementItemPositionByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetAttchementItemSizeByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsSentByPlayer(MailInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsShowTranslateButton(LBStoredMail lbStoredMail)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttchementItem3DTouch(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAttchementItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMailDetailScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void SetMoneyText4CurrencyType(ItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAttchementInfo(List<ItemInfo> itemList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMailInfoDetail(LBStoredMail lbStoredMail, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool showTranslate)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTimeOutText()
        {
        }

        private ProjectXPlayerContext m_playerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

