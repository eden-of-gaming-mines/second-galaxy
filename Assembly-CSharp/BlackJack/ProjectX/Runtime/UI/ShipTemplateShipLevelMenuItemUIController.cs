﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ShipTemplateShipLevelMenuItemUIController : CommonMenuItemUIController
    {
        [AutoBind("./ShipLevelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipLevelButton;
        [AutoBind("./ShipLevelButton/NameGroup/TypeNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipLevelText;
        [AutoBind("./ShipLevelButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnButtonClick;
        private static DelegateBridge __Hotfix_OnMenuItemFill;
        private static DelegateBridge __Hotfix_SwitchMenuItemExpandImp;
        private static DelegateBridge __Hotfix_OnFreeToUnusedPool;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnFreeToUnusedPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnMenuItemFill(object data, Dictionary<string, Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SwitchMenuItemExpandImp()
        {
        }
    }
}

