﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class GuildItemListUIController : UIControllerBase
    {
        private const string ItemStoreListAssetName = "ItemStoreList";
        private const string ItemSimpleInfoAssetName = "ItemSimpleInfoUIPrefab";
        private ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        private ItemStoreListUIController m_itemStoreListUICtrl;
        private List<ILBStoreItemClient> m_cachedItemList;
        private Dictionary<string, UnityEngine.Object> m_cachedResDict;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnItemClicked;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemDoubleClicked;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItem3DTouch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemDetailButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnItemShareButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemGroupStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemGroupCommonUIStateController;
        [AutoBind("./ItemStoreListDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemStoreListDummy;
        [AutoBind("./ItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemSimpleInfoDummy;
        private static DelegateBridge __Hotfix_GetItemListUIProcess;
        private static DelegateBridge __Hotfix_UpdateItemStoreList;
        private static DelegateBridge __Hotfix_UpdateSingleSelectItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateIemSimpleInfo;
        private static DelegateBridge __Hotfix_OnItemListUIItemClick;
        private static DelegateBridge __Hotfix_OnItemListUIItem3DTouch;
        private static DelegateBridge __Hotfix_OnItemListUIItemDoubleClick;
        private static DelegateBridge __Hotfix_OnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_OnItemShareButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnItemDoubleClicked;
        private static DelegateBridge __Hotfix_remove_EventOnItemDoubleClicked;
        private static DelegateBridge __Hotfix_add_EventOnItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemShareButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemShareButtonClick;

        public event Action<int> EventOnItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnItemDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemDoubleClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnItemShareButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetItemListUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemDetailButtonClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUIItem3DTouch(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUIItemClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUIItemDoubleClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemShareButtonClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateIemSimpleInfo(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemStoreList(List<ILBStoreItemClient> itemList, Dictionary<string, UnityEngine.Object> resDict, int startIndex = 0, int selectedIndex = -1)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleSelectItem(int selectIndex)
        {
        }

        public enum GuildStoreTabType
        {
            All,
            SpaceShip,
            Weapon,
            Ammo,
            Device,
            Component,
            Building,
            Flagship,
            Other,
            Max
        }
    }
}

