﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class CrimePromptUITask : UITaskBase
    {
        private bool m_needNotice;
        private CrimePromptUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private const string ConfirmCallBack = "ConfirmCallBackAction";
        public const string TaskName = "CrimePromptUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_PushAllNeedUILayer;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnCheckBoxButtonClick;
        private static DelegateBridge __Hotfix_ShowCrimePrompt;
        private static DelegateBridge __Hotfix_UpdateCheckBoxState;
        private static DelegateBridge __Hotfix_get_UserSettingCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CrimePromptUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCancelButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCheckBoxButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnConfirmButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void PushAllNeedUILayer()
        {
        }

        [MethodImpl(0x8000)]
        public static CrimePromptUITask ShowCrimePrompt(Action callBack)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCheckBoxState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockUserSettingClient UserSettingCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

