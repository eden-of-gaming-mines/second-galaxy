﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class PvpSignalMenuItemUIController : MenuItemUIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnRewardItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick;
        private CommonRewardUIController m_signalRewardCtrl;
        public DiplomacyState m_diplomacyState;
        private const string AllowInImageUIState_AllowIn = "AllowIn";
        private const string AllowInImageUIState_NotAllowIn = "NotAllowIn";
        private CommonCaptainIconUIController m_playerCtrl;
        [AutoBind("./TargetInfo/Info/TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_playerNameText1;
        [AutoBind("./TargetInfo/Info/IconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_iconDummy;
        [AutoBind("./TargetInfo/Info/UnionAndArmyName", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceName;
        [AutoBind("./TargetInfo/Info/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_signalLevel;
        [AutoBind("./Detail/ShipCountTitleText/ShipNumValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipNumValue;
        [AutoBind("./Detail/SceneTitleText/SceneText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sceneName;
        [AutoBind("./Detail/ShipTypeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shipTypeGroup;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeFrigate;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeDestroyer;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleCruiser;
        [AutoBind("./Detail/AllowIn/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeBattleShip;
        [AutoBind("./Detail/AllowIn/AllowInShipType/Carrier", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allowInShipTypeCarrier;
        [AutoBind("./Detail/ButtonGroup/OperateButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_operateButton;
        [AutoBind("./Detail/ButtonGroup/OperateButton/MenuPos", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_menuPos;
        [AutoBind("./Detail/ButtonGroup/AssignButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_assignButton;
        [AutoBind("./Detail/ButtonGroup/AssignButton/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_assignRectButton;
        [AutoBind("./Detail/Reward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardObj;
        [AutoBind("./Detail/ShipTypeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_captainShipListRootTrans;
        [AutoBind("./Detail/ShipTypeGroup/ShipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_captainShipInfoTextTemp;
        [AutoBind("./Detail/AllowIn/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allowInShipTypeButton;
        [AutoBind("./Detail/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_lossWarningButton;
        [AutoBind("./Detail/LossWarningButton/SafetyButton", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_lossWarningPos;
        [AutoBind("./Detail/Reward/Random", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_randReward;
        [AutoBind("./Detail/Reward/RewardItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardItemGroup;
        private CommonUIStateController[] m_allowInShipIconList;
        private readonly List<Text> m_captainShipInfoUIItemList;
        private LBPVPSignal m_pvpSignal;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        public uint m_dynamicSceneInstanceId;
        public int m_relativeSignalId;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_GetLBSignal;
        private static DelegateBridge __Hotfix_SetEasyPool;
        private static DelegateBridge __Hotfix_DisplayInfo;
        private static DelegateBridge __Hotfix_SetSignalInfo;
        private static DelegateBridge __Hotfix_SetRewardItemList;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetLossWarningButtonPos;
        private static DelegateBridge __Hotfix_GetLossWarningButtonSize;
        private static DelegateBridge __Hotfix_IsNeedShowReward;
        private static DelegateBridge __Hotfix_GetLowestReward;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_SetCaptainShipInfoUIItemList;
        private static DelegateBridge __Hotfix_SetCaptainShipInfo;
        private static DelegateBridge __Hotfix_get_AllowInShipIconList;

        public event Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private string DisplayInfo(LBPVPSignal signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public LBPVPSignal GetLBSignal()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector3 GetLossWarningButtonPos()
        {
        }

        [MethodImpl(0x8000)]
        public override Vector2 GetLossWarningButtonSize()
        {
        }

        [MethodImpl(0x8000)]
        private List<ItemInfo> GetLowestReward(List<ItemInfo> rewardList, SignalType type)
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedShowReward()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetCaptainShipInfo(int scenePlayerShipId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCaptainShipInfoUIItemList(PVPSignalInfo signalInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEasyPool(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRewardItemList()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSignalInfo(LBPVPSignal signalInfo, Dictionary<string, UnityEngine.Object> resDict, ProjectXPlayerContext ctx)
        {
        }

        private CommonUIStateController[] AllowInShipIconList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

