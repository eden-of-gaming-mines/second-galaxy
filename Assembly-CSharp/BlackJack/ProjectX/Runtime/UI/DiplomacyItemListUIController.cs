﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DiplomacyItemListUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DiplomacyState, DiplomacyItemUIController> EventOnRelationShipChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnGuildDiplomacyItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, bool> EventOnRemoveButtonClick;
        private const string DiplomacyItemPoolName = "DiplomacyItem";
        public List<DiplomacyItemInfo> m_diplomacyItemList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private bool m_isSearch;
        private uint m_selfGuildId;
        private uint m_selfAllianceId;
        private bool m_hasPromissionDiplomacyUpdate;
        private bool m_isGuildDiplomacy;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_itemListScrollRect;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemListUIItemPool;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetMode;
        private static DelegateBridge __Hotfix_InitData;
        private static DelegateBridge __Hotfix_UpdateGuildDiplomacyItemInfoList;
        private static DelegateBridge __Hotfix_UpdateSigleItemInfo;
        private static DelegateBridge __Hotfix_UpdateSignalItemInfoWithCtrl;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyItemFill;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyItemClick;
        private static DelegateBridge __Hotfix_OnDiplomacyRelationChange;
        private static DelegateBridge __Hotfix_OnRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnNoPermissionSetDiplomacyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRelationShipChanged;
        private static DelegateBridge __Hotfix_remove_EventOnRelationShipChanged;
        private static DelegateBridge __Hotfix_add_EventOnGuildDiplomacyItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildDiplomacyItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRemoveButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRemoveButtonClick;

        public event Action<UIControllerBase> EventOnGuildDiplomacyItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DiplomacyState, DiplomacyItemUIController> EventOnRelationShipChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, bool> EventOnRemoveButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void InitData(uint guildId, uint allianceId, bool hasPromissionDiplomacyUpdate, bool isGuildDiplomacy)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyRelationChange(UIControllerBase ctrl, DiplomacyState typeType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildDiplomacyItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildDiplomacyItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNoPermissionSetDiplomacyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMode(bool isSearch)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildDiplomacyItemInfoList(List<DiplomacyItemInfo> guildDiplomacyItemList, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSigleItemInfo(DiplomacyItemInfo item, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSignalItemInfoWithCtrl(DiplomacyItemUIController itemCtrl, Dictionary<string, UnityEngine.Object> dynamicResCacheDic)
        {
        }
    }
}

