﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class FactionCreditUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> OnEventFactionItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <FactionID>k__BackingField;
        public Dictionary<int, FactionCreditItemUIController> m_FactionCreditItemUICtrlDict;
        private const string SubFactionCreditItemUIPrefab = "SubFactionCreditItem";
        private DateTime m_nextUpdateTime;
        private DateTime m_nextUpdateDayTime;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtr;
        [AutoBind("./ScheduleGroud/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RewardButton;
        [AutoBind("./ScheduleGroud/GetButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardButtonUIStateCtr;
        [AutoBind("./ScheduleGroud/ScheduleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RewardProgressText;
        [AutoBind("./ScheduleGroud/ScheduleBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image WeekProgress;
        [AutoBind("./ScheduleGroud/ScheduleTipsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PanelTipsOpenBtn;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelTipsStateCtr;
        [AutoBind("./DetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PanelTipsCloseBtn;
        [AutoBind("./DetailInfoPanel/FrameImage/BGImage/DetailInfo/DescText04/DayValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text DayValue;
        [AutoBind("./DetailInfoPanel/FrameImage/BGImage/DetailInfo/DescText04/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText TimeValue;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform SubFactionCreditItemGroup;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnPanelUIButtonClick;
        private static DelegateBridge __Hotfix_UpdateViewWeekReward;
        private static DelegateBridge __Hotfix_UpdateViewChildFaction;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_add_OnEventFactionItemClick;
        private static DelegateBridge __Hotfix_remove_OnEventFactionItemClick;
        private static DelegateBridge __Hotfix_get_FactionID;
        private static DelegateBridge __Hotfix_set_FactionID;

        public event Action<int> OnEventFactionItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmeditae)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPanelUIButtonClick(FactionCreditItemUIController ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewChildFaction(int factionId, ConfigDataFactionInfo config, float val, int maxVal, long money, string lvName, Dictionary<string, UnityEngine.Object> reslist)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateViewWeekReward(bool isGet, float progress, string progressStr)
        {
        }

        public int FactionID
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnBindFiledsCompleted>c__AnonStorey0
        {
            internal FactionCreditItemUIController ctrl;
            internal FactionCreditUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }
    }
}

