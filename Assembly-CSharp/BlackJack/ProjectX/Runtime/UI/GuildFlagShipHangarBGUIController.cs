﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GuildFlagShipHangarBGUIController : UIControllerBase
    {
        protected GameObject m_modelViewGameObject;
        protected List<List<GameObject>> m_weaponGroupList;
        protected float m_shipBlinkLeftTime;
        protected const float ShipBlinkTime = 1f;
        public const string UIModelViewLayerMask = "UIModelViewLayer";
        public const string ShipLayerMask = "ShipLayer";
        [AutoBind("./ModelViewDummyRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_modelViewDummyRoot;
        [AutoBind("./Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefab;
        private static DelegateBridge __Hotfix_EquipWeaponToShip;
        private static DelegateBridge __Hotfix_Clear3DModel;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefabImp;
        private static DelegateBridge __Hotfix_SetCameraSkyBox;
        private static DelegateBridge __Hotfix_StartShipModelBlink;
        private static DelegateBridge __Hotfix_SetViewModelLayerMask;
        private static DelegateBridge __Hotfix_SetGameObjectLayerMask;
        private static DelegateBridge __Hotfix_UnloadWeaponByGroupIndex;

        [MethodImpl(0x8000)]
        public void Clear3DModel()
        {
        }

        [MethodImpl(0x8000)]
        public void EquipWeaponToShip(GameObject weaponAsset, int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void InitWith3DModelPrefab(GameObject modelCloneSource, Func<GameObject, KeyValuePair<float, Bounds>> prepareViewGameObjectFunc = null)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator InitWith3DModelPrefabImp(GameObject modelCloneSource, Func<GameObject, KeyValuePair<float, Bounds>> prepareViewGameObjectFunc)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraSkyBox(Material skyBoxMat)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGameObjectLayerMask(GameObject go, int layer)
        {
        }

        [MethodImpl(0x8000)]
        private void SetViewModelLayerMask()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator StartShipModelBlink()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnloadWeaponByGroupIndex(int groupIndex)
        {
        }

        [CompilerGenerated]
        private sealed class <InitWith3DModelPrefabImp>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal GameObject modelCloneSource;
            internal Func<GameObject, KeyValuePair<float, Bounds>> prepareViewGameObjectFunc;
            internal GuildFlagShipHangarBGUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <StartShipModelBlink>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal PrefabShipDesc <shipDsec>__1;
            internal Material <mat>__1;
            internal float <value>__2;
            internal List<List<GameObject>>.Enumerator $locvar0;
            internal GuildFlagShipHangarBGUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.$this.m_modelViewGameObject == null)
                        {
                            goto TR_0001;
                        }
                        else
                        {
                            this.<shipDsec>__1 = this.$this.m_modelViewGameObject.GetComponent<PrefabShipDesc>();
                            this.<mat>__1 = this.<shipDsec>__1.m_shipBody.GetComponent<SkinnedMeshRenderer>().material;
                            this.$this.m_shipBlinkLeftTime = 1f;
                        }
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.$this.m_shipBlinkLeftTime > 0f)
                {
                    this.$this.m_shipBlinkLeftTime -= Time.deltaTime;
                    this.<value>__2 = Mathf.Lerp(0f, 1f, 1f - (this.$this.m_shipBlinkLeftTime / 1f));
                    this.<mat>__1.SetFloat("_BlinkFX_LogicVar", this.<value>__2);
                    this.$locvar0 = this.$this.m_weaponGroupList.GetEnumerator();
                    try
                    {
                        while (this.$locvar0.MoveNext())
                        {
                            List<GameObject> current = this.$locvar0.Current;
                            foreach (GameObject obj2 in current)
                            {
                                obj2.GetComponentInChildren<SkinnedMeshRenderer>(true).material.SetFloat("_BlinkFX_LogicVar", this.<value>__2);
                            }
                        }
                    }
                    finally
                    {
                        this.$locvar0.Dispose();
                    }
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                goto TR_0001;
            TR_0000:
                return false;
            TR_0001:
                this.$PC = -1;
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

