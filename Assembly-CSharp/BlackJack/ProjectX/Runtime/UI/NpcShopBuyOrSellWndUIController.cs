﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopBuyOrSellWndUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnConfirmButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBGButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./DetailPanel/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemNameText;
        [AutoBind("./DetailPanel/ItemDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemDescText;
        [AutoBind("./DetailPanel/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemIconDummy;
        [AutoBind("./DetailPanel/SubPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_subButton;
        [AutoBind("./DetailPanel/AddPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_addButton;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmButtonStateCtrl;
        [AutoBind("./DetailPanel/CountSlider", AutoBindAttribute.InitState.NotInit, false)]
        public Slider m_itemCountSlider;
        [AutoBind("./DetailPanel/ItemCountInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_itemCountInputField;
        [AutoBind("./DetailPanel/PriceGroup/MoneyImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_moneyIconImage;
        [AutoBind("./DetailPanel/PriceGroup/MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyNumText;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgButton;
        public NpcShopMainUITask.NpcShopItemUIInfo m_shopItemInfo;
        private CommonItemIconUIController m_itemIconUICtrl;
        private float m_priceForOneSellOrBuy;
        private long m_minValue;
        private long m_maxValue;
        private long m_currentValue;
        private int m_errorCode;
        private bool m_isNeedSyncFromInputFieldToSlider = true;
        private bool m_isBuyMode;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitItemInfo;
        private static DelegateBridge __Hotfix_SetItemCountInfo;
        private static DelegateBridge __Hotfix_GetCurrBuyOrSellItemInfo;
        private static DelegateBridge __Hotfix_CreatNpcShopBuyOrSellWndUIProcess;
        private static DelegateBridge __Hotfix_SetCurrentValue;
        private static DelegateBridge __Hotfix_UpdateViewOnCurrentValue;
        private static DelegateBridge __Hotfix_OnSubButtonClick;
        private static DelegateBridge __Hotfix_OnSubButtonLongPressing;
        private static DelegateBridge __Hotfix_OnAddButtonClick;
        private static DelegateBridge __Hotfix_OnAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnItemCountSliderValueChanged;
        private static DelegateBridge __Hotfix_OnItemInputFieldValueChanged;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatNpcShopBuyOrSellWndUIProcess(bool isShow, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void GetCurrBuyOrSellItemInfo(out NpcShopMainUITask.NpcShopItemUIInfo itemInfo, out long count)
        {
        }

        [MethodImpl(0x8000)]
        public void InitItemInfo(NpcShopMainUITask.NpcShopItemUIInfo shopItemInfo, bool isBuyMode, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCountSliderValueChanged(float value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemInputFieldValueChanged(string str)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrentValue(long value)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemCountInfo(long minValue, long maxValue, long initValue, int errorCode)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewOnCurrentValue()
        {
        }
    }
}

