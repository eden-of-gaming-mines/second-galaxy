﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class SpecialGiftPackageFeatureInfoUIController : UIControllerBase
    {
        [AutoBind("./FeatureInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatureInfoText;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Image ItemDummyRoot;
        private static DelegateBridge __Hotfix_UpdateFeatureInfo;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFeatureInfo(ConfigDataGiftPackageFeatureInfo featureInfo, Dictionary<string, Object> resDict)
        {
        }
    }
}

