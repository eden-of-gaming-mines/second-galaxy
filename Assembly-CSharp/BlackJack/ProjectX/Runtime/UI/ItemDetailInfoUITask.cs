﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ItemDetailInfoUITask : UITaskBase
    {
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private ItemDetailInfoUIController m_mainCtrl;
        private CommonItemDetailInfoUIController m_commonDetailInfoCtrl;
        private ThreeDModelViewController m_itemModelViewCtrl;
        private bool m_currFogEnabled;
        private IUIBackgroundManager m_backgroundManager;
        public static string ParamKey_BackgroundManager;
        private ILBStoreItemClient m_storeItem;
        private ILBStoreItemClient m_storeItemCompare;
        private bool m_isItemDetailInfoPanelHide;
        private bool m_isCommonItemDetailInfoPanelHide;
        private bool m_isRefreshAll;
        private bool m_isShowGoTechButton;
        private UIIntent m_mainIntent;
        private bool m_isDuringUpdateView;
        public static string ParamKey_BaseItem;
        public static string ParamKey_CompareItem;
        public static string ParamKey_ShowGoTechButton;
        public static string ParamKey_MainIntent;
        public static string ParamKey_HasFog;
        private GameObject m_cachedShipModelGo;
        public const string TaskName = "ItemDetailInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitControllerAndRegisterEvents;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_GetShipModelAsset;
        private static DelegateBridge __Hotfix_PrepareShipViewModel;
        private static DelegateBridge __Hotfix_InitDataFormUIIntent;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnItemDetailUITabChanged;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_ShowItemDetailUIUseRetuenableIntent;
        private static DelegateBridge __Hotfix_ShowItemDetailUIUseCustomIntent;
        private static DelegateBridge __Hotfix_ShowItemDetailUI;
        private static DelegateBridge __Hotfix_IsItemShow3DModel;
        private static DelegateBridge __Hotfix_PrefabItemViewModel;
        private static DelegateBridge __Hotfix_OnItemDetailInfoUIPanelHide;
        private static DelegateBridge __Hotfix_OnCommonItemDetailUIPanelHide;
        private static DelegateBridge __Hotfix_OnAllUIPanelHide;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_MainLayer;
        private static DelegateBridge __Hotfix_get_m_commonDetailInfoLayer;
        private static DelegateBridge __Hotfix_get_m_itemModelViewLayer;

        [MethodImpl(0x8000)]
        public ItemDetailInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private GameObject GetShipModelAsset()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitControllerAndRegisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFormUIIntent(UIIntent uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsItemShow3DModel()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAllUIPanelHide()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCommonItemDetailUIPanelHide()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemDetailInfoUIPanelHide()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemDetailUITabChanged(CommonItemDetailInfoUIController.DetailInfoTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private Bounds PrefabItemViewModel(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private Bounds PrepareShipViewModel(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private static ItemDetailInfoUITask ShowItemDetailUI(ILBStoreItemClient itemInfo, UIIntent mainIntent, IUIBackgroundManager backgroundManager, bool showGoTechButton, bool isReturnable)
        {
        }

        [MethodImpl(0x8000)]
        public static ItemDetailInfoUITask ShowItemDetailUIUseCustomIntent(ILBStoreItemClient itemInfo, UIIntent mainIntent, IUIBackgroundManager backgroundManager = null, bool showGoTechButton = true)
        {
        }

        [MethodImpl(0x8000)]
        public static ItemDetailInfoUITask ShowItemDetailUIUseRetuenableIntent(ILBStoreItemClient itemInfo, UIIntent mainIntent, IUIBackgroundManager backgroundManager = null, bool showGoTechButton = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override SceneLayerBase MainLayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected SceneLayerBase m_commonDetailInfoLayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private SceneLayerBase m_itemModelViewLayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

