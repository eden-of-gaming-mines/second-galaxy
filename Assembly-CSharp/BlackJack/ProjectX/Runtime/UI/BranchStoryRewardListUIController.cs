﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BranchStoryRewardListUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnStrikeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, QuestRewardInfo> EventOnRewardItemClick;
        private List<QuestRewardInfo> m_questRewardInfoList;
        private List<CommonItemIconUIController> m_rewardItemList;
        private BranchStoryUITask.BranchStorySubQuestInfo m_currQuestInfo;
        private BranchStoryUITask.BranchStorySubQuestInfo m_lastQuestInfo;
        private GameObject m_rewardItem;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RewardGroupRoot;
        [AutoBind("./StrikeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StrikeButton;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ScrollRect;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public UnityEngine.UI.ScrollRect ScrollView;
        [AutoBind("./ItemSimpleDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemSimpleDummy;
        [AutoBind("./FinishImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardPanelStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateRewardList;
        private static DelegateBridge __Hotfix_GetSimpleInfoPosition;
        private static DelegateBridge __Hotfix_OnStrikeButton;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnStrikeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;

        public event Action<int, QuestRewardInfo> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<BranchStoryUITask.BranchStorySubQuestInfo> EventOnStrikeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void GetSimpleInfoPosition(int itemIndex, out Vector3 pos, out ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStrikeButton(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRewardList(BranchStoryUITask.BranchStorySubQuestInfo currQuestInfo, BranchStoryUITask.BranchStorySubQuestInfo lastQuestInfo, bool isAllComplete, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

