﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ServerInfoListUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<string> EventOnServerInfoItemClick;
        private List<ServerInfoListItemUIController> m_serverInfoItemCtrlList;
        private static string ServerInfoListItem = "ServerInfoListItem";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_content;
        [AutoBind("./BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        private static DelegateBridge __Hotfix_UpdateServerList;
        private static DelegateBridge __Hotfix_ShowServerListPanel;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolObjectCreate;
        private static DelegateBridge __Hotfix_OnServerInfoItemClick;
        private static DelegateBridge __Hotfix_add_EventOnServerInfoItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnServerInfoItemClick;

        public event Action<string> EventOnServerInfoItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolObjectCreate(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnServerInfoItemClick(string serverId)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowServerListPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateServerList(List<ServerInfo> serverInfoList, string lastServerId)
        {
        }
    }
}

