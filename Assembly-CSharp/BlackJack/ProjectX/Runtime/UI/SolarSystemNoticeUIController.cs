﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(CanvasGroup))]
    public class SolarSystemNoticeUIController : UIControllerBase
    {
        private float m_currTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAnnoucementPlayEnd;
        private Coroutine m_urgentNoticeDelayHideCoroutine;
        private Coroutine m_normalNoticeDelayHideCoroutine;
        private int m_currQuestRewardIndex;
        private List<QuestRewardInfo> m_rewardList;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        public SolarSystemQuestFinishMsgBoxUIController m_questFinishMsgBoxUICtrl;
        public SolarSystemBuffNoticeContainerUIController m_buffNoticeContainerUICtrl;
        public SolarSystemSceneProcessUIController m_sceneProcessUICtrl;
        public CommonNewAreaInfoUIController m_newAreaInfoUICtrl;
        public CommonQuestInfoUIController m_questInfoUICtrl;
        public SolarSystemNavigationUIController CurrentSolarSystemNavigationUICtrl;
        public const string WingShipUIStateStr_LeaveStation = "LeaveStation";
        public const string WingShipUIStateStr_Arrived = "Arrived";
        public const string WingShipUIStateStr_Destroy = "Destory";
        public const string WingShipUIStateStr_Evacuate = "Evacuate";
        private bool m_isQuestMsgShowing;
        [AutoBind("./NormalNoticeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NormalNoticeRoot;
        [AutoBind("./NormalNoticeGroup/NormalNotice", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController NormalNoticeCtrl;
        [AutoBind("./NormalNoticeGroup/NormalNotice/NormalNoticeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NormalNoticText;
        [AutoBind("./UrgentNoticeImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UrgentNoticeObj;
        [AutoBind("./CanvasVolatile/UrgentNoticeImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UrgentNoticeObjVolatile;
        [AutoBind("./CanvasVolatile/UrgentNoticeImage/UrgentNoticeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UrgentNoticeText;
        [AutoBind("./NPCChatUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform NPCChatUIPrefabDummy;
        [AutoBind("./QuestInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject QuestInfoDummy;
        [AutoBind("./BackToStationNotic", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_questFinishNoticeRoot;
        [AutoBind("./ShipMoveState/MoveState", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipMoveStateRoot;
        [AutoBind("./ShipMoveState/MoveState/ShipMoveStateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipMoveStateText;
        [AutoBind("./ShipMoveState/MoveState/ShipMoveStateText", AutoBindAttribute.InitState.NotInit, false)]
        public TweenScale ShipMoveStateTextTween;
        [AutoBind("./ShipMoveState/TargetName", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TargetNameRoot;
        [AutoBind("./ShipMoveState/TargetName/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TargetNameText;
        [AutoBind("./PvpKillNotic", AutoBindAttribute.InitState.NotInit, false)]
        public EffectPlayManager m_pvpKillNoticeEffectPlayer;
        [AutoBind("./BuffNoticPanelRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_buffNoticeRoot;
        [AutoBind("./CanvasVolatile/SceneProcessBarRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SceneProcessBarRoot;
        [AutoBind("./NewAreaArriveDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NewAreaArriveDummy;
        [AutoBind("./InterveneNoticPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WingShipNoticePanel;
        [AutoBind("./InterveneNoticPanel/DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WingShipTextCtrl;
        [AutoBind("./InterveneNoticPanel/DetailGroup/CaptainAndTextGroup/CaptainIconGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image WingShipIconImage;
        [AutoBind("./InterveneNoticPanel/DetailGroup/CaptainAndTextGroup/LeaveStationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingShipLeaveStationText;
        [AutoBind("./InterveneNoticPanel/DetailGroup/CaptainAndTextGroup/ArrivedText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingShipArrivedText;
        [AutoBind("./InterveneNoticPanel/DetailGroup/CaptainAndTextGroup/DestoryText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingShipDestroyText;
        [AutoBind("./InterveneNoticPanel/DetailGroup/CaptainAndTextGroup/EvacuateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingShipEvacuateText;
        [AutoBind("./GlobalNotice/TextMask/NoticeText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AnnouncementTextCtrl;
        [AutoBind("./GlobalNotice/TextMask/NoticeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AnnouncementText;
        [AutoBind("./GlobalNotice", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AnnouncementGameObject;
        [AutoBind("./GlobalNotice/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Button AnnouncementButton;
        [AutoBind("./NavigationUIPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NavigationPanel;
        private WormholeDebuffWarningUIController m_fullScreenEffectForWormholeDebuffWarning;
        [AutoBind("./FullScreenEffectInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_fullScreenEffectInfoPanel;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_HideAll;
        private static DelegateBridge __Hotfix_ShowShipMoveCmdState;
        private static DelegateBridge __Hotfix_HideShipMoveCmdState;
        private static DelegateBridge __Hotfix_ShowWingShipState;
        private static DelegateBridge __Hotfix_ShowNormalNoticeText_0;
        private static DelegateBridge __Hotfix_ShowNormalNoticeText_1;
        private static DelegateBridge __Hotfix_IsQuestMsgShowing;
        private static DelegateBridge __Hotfix_HideNoramlNoticeText;
        private static DelegateBridge __Hotfix_EnableNormalNoticePanel;
        private static DelegateBridge __Hotfix_ShowUrgentNoticeText;
        private static DelegateBridge __Hotfix_HideUrgetnNoticeText;
        private static DelegateBridge __Hotfix_HideQuestFinishMsgBox;
        private static DelegateBridge __Hotfix_ShowQuestFinishMsgBox;
        private static DelegateBridge __Hotfix_ShowGotoNextScienceExploreQuestMsgBox;
        private static DelegateBridge __Hotfix_ShowQuestMsg;
        private static DelegateBridge __Hotfix_HideQuestMsg;
        private static DelegateBridge __Hotfix_ShowQuestCompleteEffect;
        private static DelegateBridge __Hotfix_GetNPCChatDummyPos;
        private static DelegateBridge __Hotfix_SetWingShipStateText;
        private static DelegateBridge __Hotfix_OnQuestCompleteAnimFinished;
        private static DelegateBridge __Hotfix_ShowPvpKillNotice;
        private static DelegateBridge __Hotfix_HidePvpKillNotice;
        private static DelegateBridge __Hotfix_DelayHide;
        private static DelegateBridge __Hotfix_ShowSceneProcess;
        private static DelegateBridge __Hotfix_UpdateSceneProcess;
        private static DelegateBridge __Hotfix_HideSceneProcessUI;
        private static DelegateBridge __Hotfix_ShowNewArriveSolarSystemInfo;
        private static DelegateBridge __Hotfix_HideAnnouncement;
        private static DelegateBridge __Hotfix_IsNeedGetNextAnnouncement;
        private static DelegateBridge __Hotfix_SetAnnouncementTextInfo_1;
        private static DelegateBridge __Hotfix_SetAnnouncementTextInfo_0;
        private static DelegateBridge __Hotfix_AnnoucementEnd;
        private static DelegateBridge __Hotfix_add_EventOnAnnoucementPlayEnd;
        private static DelegateBridge __Hotfix_remove_EventOnAnnoucementPlayEnd;
        private static DelegateBridge __Hotfix_HideWingShipPanel;
        private static DelegateBridge __Hotfix_ShowWingShipPanel;
        private static DelegateBridge __Hotfix_ShowTipWindowExInfo;
        private static DelegateBridge __Hotfix_GetOrCreateFullScreenEffectForWormholeDebuffWarning;
        private static DelegateBridge __Hotfix_GetCurrFullScreenEffectForWormholeDebuffWarning;

        public event Action EventOnAnnoucementPlayEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AnnoucementEnd(bool result)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator DelayHide(GameObject target, float time)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableNormalNoticePanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public WormholeDebuffWarningUIController GetCurrFullScreenEffectForWormholeDebuffWarning()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetNPCChatDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        public WormholeDebuffWarningUIController GetOrCreateFullScreenEffectForWormholeDebuffWarning()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess HideAll()
        {
        }

        [MethodImpl(0x8000)]
        public void HideAnnouncement()
        {
        }

        [MethodImpl(0x8000)]
        public void HideNoramlNoticeText(bool immediately = false)
        {
        }

        [MethodImpl(0x8000)]
        public void HidePvpKillNotice()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess HideQuestFinishMsgBox(bool immediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess HideQuestMsg()
        {
        }

        [MethodImpl(0x8000)]
        public void HideSceneProcessUI(bool immediately)
        {
        }

        [MethodImpl(0x8000)]
        public void HideShipMoveCmdState()
        {
        }

        [MethodImpl(0x8000)]
        public void HideUrgetnNoticeText()
        {
        }

        [MethodImpl(0x8000)]
        public void HideWingShipPanel()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsNeedGetNextAnnouncement()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsQuestMsgShowing()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestCompleteAnimFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncementTextInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncementTextInfo(string msg, float time)
        {
        }

        [MethodImpl(0x8000)]
        private void SetWingShipStateText()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess ShowGotoNextScienceExploreQuestMsgBox(bool isShow, bool immediateShow = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewArriveSolarSystemInfo(int solarSystemId, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNormalNoticeText()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNormalNoticeText(string msg, float durationTime = -1f)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowPvpKillNotice()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestCompleteEffect(bool isGuildAction, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess ShowQuestFinishMsgBox(bool isShow, string msg, bool immediateShow = false, bool withBack = true)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess ShowQuestMsg(string nameStr, string detailStr, bool forcePlay = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSceneProcess(InSpaceSceneProcessBarNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipMoveCmdState(string stateMsg, string targetName = "", bool hideTween = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowTipWindowExInfo(string msg, string[] paramList)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowUrgentNoticeText(string msg, float durationTime = -1f)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWingShipPanel()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWingShipState(string wingShipState, bool needResetShipIcon, Sprite shipIcon)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSceneProcess(InSpaceSceneProcessBarUpdateNtf updateNtf)
        {
        }

        [CompilerGenerated]
        private sealed class <DelayHide>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal float time;
            internal GameObject target;
            internal SolarSystemNoticeUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

