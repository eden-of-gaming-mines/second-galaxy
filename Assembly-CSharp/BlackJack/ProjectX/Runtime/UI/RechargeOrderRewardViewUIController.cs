﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class RechargeOrderRewardViewUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, ILBStoreItemClient> EventOnRewardItemClick;
        private readonly List<FakeLBStoreItem> m_rewardFakeItemList;
        private readonly List<MonthCardPrivilegeItemUIController> m_privilegeCtrlList;
        private readonly List<CommonItemIconUIController> m_rewardItemCtrlList;
        private GameObject m_commonItemTemplateGo;
        private GameObject m_monthCardPrivilegeItemTemplateGo;
        private RechargeOrderInfo m_rechargeOrder;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private const string CommonItemAssetName = "CommonItemUIPrefab";
        private const string PrivilegeItemAssetName = "PrivilegeItemUIPrefab";
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemRewardConfirmButton;
        [AutoBind("./RewardGroup/ItemGroupRoot/ItemRewardRoot/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemRewardListContentRoot;
        [AutoBind("./RewardGroup/ItemGroupRoot/CurrencyRoot/CurrencyVauleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrencyVauleText;
        [AutoBind("./RewardGroup/ItemGroupRoot/CurrencyRoot/CurrencyImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CurrencyImage;
        [AutoBind("./DescRoot/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescText;
        [AutoBind("./RewardGroup/ItemGroupRoot/ItemRewardRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRewardRoot;
        [AutoBind("./RewardGroup/ItemGroupRoot/CurrencyRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CurrencyRoot;
        [AutoBind("./DescRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DescRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MainStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public MarchingBytes.EasyObjectPool EasyObjectPool;
        [AutoBind("./TemplateRoot", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform TemplateRoot;
        [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController RewardGroupStateCtrl;
        [AutoBind("./RewardGroup/PrivilegeGroupRoot/PrivilegeItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform PrivilegeItemRoot;
        [AutoBind("./SimpleInfoLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform SimpleInfoLeftDummy;
        [AutoBind("./SimpleInfoRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform SimpleInfoRightDummy;
        [AutoBind("./RewardGroup/ItemGroupRoot/ItemRewardRoot/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        private GridLayoutGroup RewardItemLayoutGroup;
        [AutoBind("./RewardGroup/ItemGroupRoot/CurrencyRoot", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController CurrencyStateCtrl;
        [AutoBind("./RewardGroup/ItemGroupRoot/ItemRewardRoot", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController ItemRewardStateCtrl;
        private static DelegateBridge __Hotfix_UpdateRechargeOrderRewardPanel;
        private static DelegateBridge __Hotfix_GetSimpleInfoWorldLocationByItemIndex;
        private static DelegateBridge __Hotfix_Update4RechargeItemOrder;
        private static DelegateBridge __Hotfix_Update4MonthCardOrder;
        private static DelegateBridge __Hotfix_UpdatePrivilegeList;
        private static DelegateBridge __Hotfix_UpdateRewardItemList;
        private static DelegateBridge __Hotfix_Update4GiftPackageOrder;
        private static DelegateBridge __Hotfix_SetCurrencyValue;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTemplateItem;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolDailyRewardItemCreated;
        private static DelegateBridge __Hotfix_OnRewardItemFill;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_GetCurrencyValueFromRewardList;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;

        public event Action<int, ILBStoreItemClient> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CreateTemplateItem()
        {
        }

        [MethodImpl(0x8000)]
        private int GetCurrencyValueFromRewardList(List<QuestRewardInfo> rewardList)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSimpleInfoWorldLocationByItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolDailyRewardItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrencyValue(int currencyVaule)
        {
        }

        [MethodImpl(0x8000)]
        private void Update4GiftPackageOrder()
        {
        }

        [MethodImpl(0x8000)]
        private void Update4MonthCardOrder()
        {
        }

        [MethodImpl(0x8000)]
        private void Update4RechargeItemOrder()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePrivilegeList(List<MonthlyCardPriviledgeInfo> priviledgeInfos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRechargeOrderRewardPanel(RechargeOrderInfo rechargeOrder, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRewardItemList(List<QuestRewardInfo> rewardItemList)
        {
        }
    }
}

