﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildCompensationKillInfoController : UIControllerBase
    {
        public CommonItemIconUIController m_itemIconCtrl;
        public CommonCaptainIconUIController m_captainCtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCommonIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnCaptionIconClick;
        private const string GuildCodeFormat = "[{0}]";
        [AutoBind("./UnionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnionNameText;
        [AutoBind("./ArmyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmyNameText;
        [AutoBind("./CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemDummy;
        [AutoBind("./CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform CaptainDummy;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnCommonIconClick;
        private static DelegateBridge __Hotfix_OnCaptionIconClick;
        private static DelegateBridge __Hotfix_add_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCommonIconClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptionIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptionIconClick;

        public event Action<UIControllerBase> EventOnCaptionIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnCommonIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptionIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(KillRecordOpponentInfo opponentInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

