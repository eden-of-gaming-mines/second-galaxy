﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class SwitchGuildSolarSystemPanelItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ItemIndex>k__BackingField;
        private const string StateNormal = "Normal";
        private const string StateLoss = "Loss";
        [AutoBind("./SolarSystemInfo/WarState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WarStateCtrl;
        [AutoBind("./SolarSystemInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./SolarSystemInfo/FlourishLevel", AutoBindAttribute.InitState.NotInit, false)]
        public Text FlourishLevelText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemButton;
        [AutoBind("./SolarSystemInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SolarSystemStateCtrl;
        private static DelegateBridge __Hotfix_UpdateSolarSystemInfo;
        private static DelegateBridge __Hotfix_UpdateSolarSystemInfo_Loss;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;

        public event Action<int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemInfo(int flourishLevel, string solarSystemName, bool isInWar, bool isCurrSolarSystem, bool isGuildBase)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSolarSystemInfo_Loss(int solarSystemId)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

