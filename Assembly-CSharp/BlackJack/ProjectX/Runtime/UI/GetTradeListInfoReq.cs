﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GetTradeListInfoReq : NetWorkTransactionTask
    {
        private int m_result;
        private readonly bool m_isLegal;
        private readonly int m_tradeItemId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnTradeListGetAck;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public GetTradeListInfoReq(bool isLegal, int tradeItemId = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeListGetAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

