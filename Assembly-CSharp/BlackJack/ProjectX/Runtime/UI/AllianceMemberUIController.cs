﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AllianceMemberUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<AllianceMemberItemUIController, int> EventMemberItemFillData;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<AllianceGuildForInviteItemUIController, int> EventSearchGuildResultItemFillData;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Mode> EventOnChangeMode;
        private const string Show = "Show";
        private const string Close = "Close";
        private const string Member = "Member";
        private const string GuildList = "GuildList";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateController;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ToggleGroup/MemberToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_memberToggle;
        [AutoBind("./ToggleGroup/GuildListToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_guildListToggle;
        [AutoBind("./ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_contentGroup;
        [AutoBind("./ContentGroup/MemberGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_memberObjectPool;
        [AutoBind("./ContentGroup/MemberGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_memberScrollRect;
        [AutoBind("./ContentGroup/GuildListGroup/SearchButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_searchBtn;
        [AutoBind("./ContentGroup/GuildListGroup/SortGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown m_searchType;
        [AutoBind("./ContentGroup/GuildListGroup/InputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_keyInputField;
        [AutoBind("./ContentGroup/GuildListGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_searchObjectPool;
        [AutoBind("./ContentGroup/GuildListGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_searchScrollRect;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_EventOnMemberPoolObjectCreated;
        private static DelegateBridge __Hotfix_EventOnSearchPoolObjectCreated;
        private static DelegateBridge __Hotfix_MemberItemFillEvent;
        private static DelegateBridge __Hotfix_SearchGuildResultItemFillEvent;
        private static DelegateBridge __Hotfix_ClearAllEvents;
        private static DelegateBridge __Hotfix_add_EventMemberItemFillData;
        private static DelegateBridge __Hotfix_remove_EventMemberItemFillData;
        private static DelegateBridge __Hotfix_add_EventSearchGuildResultItemFillData;
        private static DelegateBridge __Hotfix_remove_EventSearchGuildResultItemFillData;
        private static DelegateBridge __Hotfix_SetMode;
        private static DelegateBridge __Hotfix_SetKeyText;
        private static DelegateBridge __Hotfix_GetKeyText;
        private static DelegateBridge __Hotfix_SetSearchType;
        private static DelegateBridge __Hotfix_GetSearchType;
        private static DelegateBridge __Hotfix_GetMainStateEffectInfo;
        private static DelegateBridge __Hotfix_SetMemberCount;
        private static DelegateBridge __Hotfix_SetSearchResultCount;
        private static DelegateBridge __Hotfix_add_EventOnChangeMode;
        private static DelegateBridge __Hotfix_remove_EventOnChangeMode;

        public event Action<AllianceMemberItemUIController, int> EventMemberItemFillData
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Mode> EventOnChangeMode
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<AllianceGuildForInviteItemUIController, int> EventSearchGuildResultItemFillData
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearAllEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnMemberPoolObjectCreated(string str, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnSearchPoolObjectCreated(string str, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public string GetKeyText()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo GetMainStateEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public int GetSearchType()
        {
        }

        [MethodImpl(0x8000)]
        private void MemberItemFillEvent(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SearchGuildResultItemFillEvent(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetKeyText(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMemberCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMode(Mode mode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSearchResultCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSearchType(int type)
        {
        }

        public enum Mode
        {
            MemberGuild,
            AllGuild
        }
    }
}

