﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class GuildMessageBoardUITask : GuildUITaskBase
    {
        private readonly List<GuildMessageInfo> m_guildMessageList;
        private CharacterSimpleInfoUITask m_characterSimpleInfoUITask;
        private CharactorObserverAck m_characterObserverAck;
        public const string ParamKeyGuildId = "GuildId";
        public const string ParamKeyNeedUpdateUI = "NeedUpdateUI";
        private int m_startIndex;
        private uint m_guildId;
        private bool m_showAll;
        private bool m_hasDeletePermission;
        private GuildMessageBoardUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildMessageBoardUITask";
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildMessageUITaskWithPrepare;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitDataFromIntet;
        private static DelegateBridge __Hotfix_SendGuildMessageGetReq;
        private static DelegateBridge __Hotfix_SendGuildMessageWriteReq;
        private static DelegateBridge __Hotfix_SendMessageDeleteReq;
        private static DelegateBridge __Hotfix_SendCharactorObserveReq;
        private static DelegateBridge __Hotfix_RegistUIEvent;
        private static DelegateBridge __Hotfix_UnRegistUIEvent;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_CloseMessageUITask;
        private static DelegateBridge __Hotfix_SaveDataForSelfGuildMessageRedPoints;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnSendButtonClick;
        private static DelegateBridge __Hotfix_OnMessageIconClick;
        private static DelegateBridge __Hotfix_OnPlayerNameClick;
        private static DelegateBridge __Hotfix_OnDeleteTextClick;
        private static DelegateBridge __Hotfix_OnDeleteButtonClick;
        private static DelegateBridge __Hotfix_OnLoadMoreButtonClick;
        private static DelegateBridge __Hotfix_OnCharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_OnTeamInviteButtonClick;
        private static DelegateBridge __Hotfix_OnInviteWindowUITaskPause;
        private static DelegateBridge __Hotfix_OnSendMailButtonClick;
        private static DelegateBridge __Hotfix_OnEidtJobButtonClick;
        private static DelegateBridge __Hotfix_OnExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_OnCharacterPanelClose;
        private static DelegateBridge __Hotfix_IsSelfGuildInvalid;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildMessageBoardUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseMessageUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntet(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsSelfGuildInvalid()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCharacterPanelClose()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteButtonClick(GuildMessageItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteTextClick(GuildMessageItemUIController ctrl, string str)
        {
        }

        [MethodImpl(0x8000)]
        public void OnEidtJobButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void OnExpelGuildButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInviteWindowUITaskPause(Task task)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadMoreButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMessageIconClick(GuildMessageItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerNameClick(GuildMessageItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnSendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnTeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegistUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SaveDataForSelfGuildMessageRedPoints(uint guildId)
        {
        }

        [MethodImpl(0x8000)]
        private void SendCharactorObserveReq(string playerGameUserId, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildMessageGetReq(uint guildId, Action<bool> onEnd, bool forceUpdate = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildMessageWriteReq(uint guildId, string content, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendMessageDeleteReq(ulong instanceId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildMessageUITaskWithPrepare(uint guildId, UIIntent returnIntent, Action<bool> onPrepareEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegistUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnDeleteButtonClick>c__AnonStorey4
        {
            internal ulong messageInstanceId;
            internal int index;
            internal GuildMessageItemUIController ctrl;
            internal GuildMessageBoardUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    foreach (GuildMessageInfo info in ((LogicBlockGuildClient) this.$this.PlayerCtx.GetLBGuild()).GetGuildMessageInfo(this.$this.m_guildId).listInfo.m_messageList)
                    {
                        if (info.m_instanceId == this.messageInstanceId)
                        {
                            this.$this.m_guildMessageList[this.index].m_delete = info.m_delete;
                            this.$this.m_guildMessageList[this.index].m_deleteAdminGameUserId = info.m_deleteAdminGameUserId;
                            this.$this.m_guildMessageList[this.index].m_deleteAdminName = info.m_deleteAdminName;
                        }
                    }
                    this.ctrl.UpdateMessageInfo(this.$this.m_guildMessageList[this.index], this.$this.m_showAll, this.$this.m_hasDeletePermission, this.$this.m_dynamicResCacheDict);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSendMailButtonClick>c__AnonStorey5
        {
            internal ProPlayerSimplestInfo playerInfo;
            internal GuildMessageBoardUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                MailUITask.StartTask(this.$this.m_currIntent, MailUITask.Mode.SendPrivate, this.playerInfo, null);
            }
        }

        [CompilerGenerated]
        private sealed class <SendCharactorObserveReq>c__AnonStorey3
        {
            internal Vector3 pos;
            internal GuildMessageBoardUITask $this;

            internal void <>m__0(Task returnTask)
            {
                CharactorObserveReqNetTas tas = returnTask as CharactorObserveReqNetTas;
                if ((tas != null) && !tas.IsNetworkError)
                {
                    int ackResult = tas.m_ackResult;
                    if (ackResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(ackResult, true, false);
                    }
                    else
                    {
                        ProPlayerSimplestInfo playerInfo = new ProPlayerSimplestInfo();
                        this.$this.m_characterObserverAck = tas.m_charactorObserverAckInfo;
                        playerInfo.PlayerGameUserId = this.$this.m_characterObserverAck.DestGameUserId;
                        playerInfo.Name = this.$this.m_characterObserverAck.CharacterName;
                        playerInfo.AvatarId = this.$this.m_characterObserverAck.AvatarId;
                        playerInfo.Level = this.$this.m_characterObserverAck.CharacterLevel;
                        playerInfo.GuildId = this.$this.m_characterObserverAck.GuildId;
                        this.$this.m_characterSimpleInfoUITask = CharacterSimpleInfoUITask.ShowCharacterSimpleInfoPanel("StartByPlayerList", playerInfo, this.pos, Vector2.zero, 0f, CharacterSimpleInfoUITask.PostionType.Right, CharacterSimpleInfoUITask.DiplomacyPostionType.Left, this.$this.m_currIntent, null, false, null);
                        if (this.$this.m_characterSimpleInfoUITask != null)
                        {
                            this.$this.m_characterSimpleInfoUITask.EventOnCharacterDetailButtonClick += new Action<ProPlayerSimplestInfo>(this.$this.OnCharacterDetailButtonClick);
                            this.$this.m_characterSimpleInfoUITask.EventOnExpelGuildButtonClick += new Action<ProPlayerSimplestInfo>(this.$this.OnExpelGuildButtonClick);
                            this.$this.m_characterSimpleInfoUITask.EventOnEidtJobButtonClick += new Action<ProPlayerSimplestInfo>(this.$this.OnEidtJobButtonClick);
                            this.$this.m_characterSimpleInfoUITask.EventOnPanelClose += new Action(this.$this.OnCharacterPanelClose);
                            this.$this.m_characterSimpleInfoUITask.EventOnSendMailButtonClick += new Action<ProPlayerSimplestInfo>(this.$this.OnSendMailButtonClick);
                            this.$this.m_characterSimpleInfoUITask.EventOnTeamInviteButtonClick += new Action<ProPlayerSimplestInfo>(this.$this.OnTeamInviteButtonClick);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildMessageGetReq>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal uint guildId;
            internal GuildMessageBoardUITask $this;

            internal void <>m__0(Task task)
            {
                if (((GuildMessageGetReqNetTask) task).IsNetworkError)
                {
                    this.onEnd(false);
                }
                else
                {
                    int result = ((GuildMessageGetReqNetTask) task).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onEnd(false);
                    }
                    else
                    {
                        this.$this.SaveDataForSelfGuildMessageRedPoints(this.guildId);
                        this.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildMessageWriteReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (!((GuildMessageWriteReqNetTask) task).IsNetworkError)
                {
                    int result = ((GuildMessageWriteReqNetTask) task).Result;
                    if (result == 0)
                    {
                        this.onEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendMessageDeleteReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (!((GuildMessageDeleteReqNetTask) task).IsNetworkError)
                {
                    int result = ((GuildMessageDeleteReqNetTask) task).Result;
                    if (result == 0)
                    {
                        this.onEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            UpdateDataCache
        }
    }
}

