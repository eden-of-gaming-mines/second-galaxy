﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class QuestCompleteCondUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_ctrlTrans;
        [AutoBind("./CompleteCondRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_completeCondTemp;
        [AutoBind("./CriticalCondRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_criticalCompleteCondTemp;
        [AutoBind("./CompleteInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_completeInfoTrans;
        private List<GameObject> m_completeCondUIObjList;
        private List<GameObject> m_criticalCompleteCondUIObjList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetQuestCompleteCondInfo;
        private static DelegateBridge __Hotfix_GetOrCreateCompleteCondUIObj;
        private static DelegateBridge __Hotfix_GetOrCreateCriticalCompleteCondUIObj;
        private static DelegateBridge __Hotfix_DisableAllUIObj;

        [MethodImpl(0x8000)]
        private void DisableAllUIObj()
        {
        }

        [MethodImpl(0x8000)]
        private GameObject GetOrCreateCompleteCondUIObj(int index)
        {
        }

        [MethodImpl(0x8000)]
        private GameObject GetOrCreateCriticalCompleteCondUIObj(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetQuestCompleteCondInfo(string completeStr, List<string> criticalStrList)
        {
        }

        [CompilerGenerated]
        private sealed class <SetQuestCompleteCondInfo>c__AnonStorey0
        {
            internal string completeStr;

            [MethodImpl(0x8000)]
            internal int <>m__0(string a, string b)
            {
            }
        }
    }
}

