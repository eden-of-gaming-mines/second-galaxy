﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildBuildingDeployUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCostItemClick;
        private CommonItemIconUIController m_delpoyCostItemCtrl;
        private int m_costItemId;
        private const string StatePanelOpen = "PanelOpen";
        private const string StatePanelClose = "PanelClose";
        [AutoBind("./ContentGroup/BuildingIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BuildingIconImage;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroupButton;
        [AutoBind("./ContentGroup/Buttons/DeployButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DeployButton;
        [AutoBind("./ContentGroup/GuildBuildingDeployGroup/DeployCostGroup/DeployCostTitleText/DeployCostNumberText/CommonItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform DeployCostItemDummy;
        [AutoBind("./ContentGroup/GuildBuildingDeployGroup/DeployCostGroup/DeployCostTitleText/DeployCostNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DeployCostNumberText;
        [AutoBind("./ContentGroup/GuildBuildingDeployGroup/DeployTimeGroup/DeployTimeTitleText/DeployTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DeployTimeText;
        [AutoBind("./ContentGroup/GuildBuildingDeployGroup/DeployConditionGroup/DeployConditionTitleText/DeployConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DeployConditionText;
        [AutoBind("./ContentGroup/GuildBuildingDeployGroup/BuildingDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingDescText;
        [AutoBind("./ContentGroup/GuildBuildingDeployGroup/BuildingNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingNameText;
        [AutoBind("./ContentGroup/GuildBuildingDeployGroup/DeployConditionGroup/DeployConditionTitleText/DeployConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_conditionStateCtrl;
        [AutoBind("./BGImages/TitleText/TipsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipButton;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_tipWindowStateCtrl;
        [AutoBind("./DetailInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tipWindowCloseButton;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingDeployInfo;
        private static DelegateBridge __Hotfix_GetTipWindowUIProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnCostItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCostItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCostItemClick;

        public event Action<int> EventOnCostItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTipWindowUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCostItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildBuildingDeployInfo(GuildBuildingInfo buildingInfo, int solarSystemId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

