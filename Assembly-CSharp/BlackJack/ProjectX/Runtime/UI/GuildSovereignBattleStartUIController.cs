﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildSovereignBattleStartUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<QuestRewardInfo> EventOnOccupiedItemIconClick;
        private GameObject m_commonItemRes;
        private List<QuestRewardInfo> m_rewardInfoList;
        private List<CommonItemIconUIController> m_rewardItemCtrlList;
        public Action m_EventOnProgressFull;
        private Image m_curCdImage;
        private RectTransform m_cdLightImage;
        private SovereignBattleSimpleInfo m_sovereignBattleSimpleInfo;
        private GuildSolarSystemInfoClient m_solarSystemClientInfo;
        private ButtonType m_curChooseType;
        private GuildLogoController m_declareGuildLogo;
        private GuildLogoController m_leftGuildLogo;
        private GuildLogoController m_rightGuildLogo;
        private const string TitleStateWar = "War";
        private const string TitleStateWarForNpcGuild = "War2";
        private const string TitleStateDeclareWar = "DeclareWar";
        private const string TitleStateDeclareWarForNpcGuild = "DeclareWar2";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./TitleWarGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_titleStateCtrl;
        [AutoBind("./TitleWarGroup/DeclareWarGroup/LeftSovereigntyGroup/SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject m_declareWarGuildLogoObj;
        [AutoBind("./TitleWarGroup/DeclareWarGroup/LeftSovereigntyGroup/LeftSovereigntyTextGroup/GuilgNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_declareWarGuildCodeName;
        [AutoBind("./TitleWarGroup/DeclareWarGroup/LeftSovereigntyGroup/LeftSovereigntyTextGroup/LeftNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_declareWarGuildName;
        [AutoBind("./TitleWarGroup/ContentGroup/LeftSovereigntyGroup/SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject m_leftGuildLogoObj;
        [AutoBind("./TitleWarGroup/ContentGroup/LeftSovereigntyGroup/LeftGuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_leftGuildCodeName;
        [AutoBind("./TitleWarGroup/ContentGroup/LeftSovereigntyGroup/LeftNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_leftGuildName;
        [AutoBind("./TitleWarGroup/ContentGroup/LeftSovereigntyGroup/ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx m_leftGuildClickButtonEx;
        [AutoBind("./TitleWarGroup/ContentGroup/RightSovereigntyGroup/SignGroup", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject m_rightGuildLogoObj;
        [AutoBind("./TitleWarGroup/ContentGroup/RightSovereigntyGroup/RightGuildNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_rightGuildCodeName;
        [AutoBind("./TitleWarGroup/ContentGroup/RightSovereigntyGroup/RightNameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_rightGuildName;
        [AutoBind("./TitleWarGroup/ContentGroup/RightSovereigntyGroup/ClickImage", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx m_rightGuildClickButtonEx;
        [AutoBind("./TitleWarGroup/ShipTeamInfoPanel/ShipInfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_recommendedConfigurationButton;
        [AutoBind("./ShipInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_recommendedConfigurationPanelStateCtrl;
        [AutoBind("./ShipInfoPanel/FrameImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_recommendedConfigurationPanelTypeStateCtrl;
        [AutoBind("./ShipInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_recommendedConfigurationPanelCloseButton;
        [AutoBind("./ShipInfoPanel/FrameImage/BGImage/DetailInfo1/InfoText1", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_commanderLevelText;
        [AutoBind("./ShipInfoPanel/FrameImage/BGImage/DetailInfo2/Info2/ShipTypeBG", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_subRankBgImage;
        [AutoBind("./ShipInfoPanel/FrameImage/BGImage/DetailInfo2/Info2/ShipTypeBG/ShipType", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_subRankImage;
        [AutoBind("./ShipInfoPanel/FrameImage/BGImage/DetailInfo2/Info2/TechBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_techImage;
        [AutoBind("./ShipInfoPanel/FrameImage/BGImage/DetailInfo3/InfoText3", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_shipNumber;
        [AutoBind("./ShipInfoPanel/FrameImage/BGImage/DetailInfo4/InfoText4", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_flagShipNumber;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./TitleWarGroup/DeclareWarGroup/DeclareWarImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_declareWarButton;
        [AutoBind("./TitleWarGroup/DeclareWarGroup/DeclareWarImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_declareWarButtonStateCtrl;
        [AutoBind("./DescribeGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_TimeText;
        [AutoBind("./DescribeGroup/TimeText/TimeTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_TimeText1;
        [AutoBind("./DescribeGroup/TimeText/TimeTitleText (1)", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_TimeText2;
        [AutoBind("./DescribeGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_titleText;
        [AutoBind("./DescribeGroup/Scroll View/Viewport/Content/DescribeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_describeText;
        [AutoBind("./BookmarkGroup/SummarizeButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SummarizeButton;
        [AutoBind("./BookmarkGroup/SummarizeButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SummarizeButtonStateCtrl;
        [AutoBind("./BookmarkGroup/SummarizeButtonGroup/ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SummarizeButtonChooseStateCtrl;
        [AutoBind("./BookmarkGroup/SummarizeButtonGroup/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SummarizeButtonCD;
        [AutoBind("./BookmarkGroup/SummarizeButtonGroup/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform SummarizeButtonCDLight;
        [AutoBind("./BookmarkGroup/PrepareButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PrepareButton;
        [AutoBind("./BookmarkGroup/PrepareButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PrepareButtonStateCtrl;
        [AutoBind("./BookmarkGroup/PrepareButtonGroup/ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PrepareButtonChooseStateCtrl;
        [AutoBind("./BookmarkGroup/PrepareButtonGroup/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image PrepareButtonCD;
        [AutoBind("./BookmarkGroup/PrepareButtonGroup/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform PrepareButtonCDLight;
        [AutoBind("./BookmarkGroup/FightButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FightButton;
        [AutoBind("./BookmarkGroup/FightButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FightButtonSateCtrl;
        [AutoBind("./BookmarkGroup/FightButtonGroup/ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FightButtonChooseSateCtrl;
        [AutoBind("./BookmarkGroup/FightButtonGroup/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FightButtonCD;
        [AutoBind("./BookmarkGroup/FightButtonGroup/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FightButtonCDLight;
        [AutoBind("./BookmarkGroup/TruceButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TruceButton;
        [AutoBind("./BookmarkGroup/TruceButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TruceButtonStateCtrl;
        [AutoBind("./BookmarkGroup/TruceButtonGroup/ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TruceButtonChooseStateCtrl;
        [AutoBind("./BookmarkGroup/TruceButtonGroup/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TruceButtonCD;
        [AutoBind("./BookmarkGroup/TruceButtonGroup/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TruceButtonCDLight;
        [AutoBind("./BookmarkGroup/DecisiveBattleButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DecisiveBattleButton;
        [AutoBind("./BookmarkGroup/DecisiveBattleButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DecisiveBattleButtonStateCtrl;
        [AutoBind("./BookmarkGroup/DecisiveBattleButtonGroup/ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DecisiveBattleButtonChooseStateCtrl;
        [AutoBind("./BookmarkGroup/DecisiveBattleButtonGroup/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image DecisiveBattleCD;
        [AutoBind("./BookmarkGroup/DecisiveBattleButtonGroup/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform DecisiveBattleCDLight;
        [AutoBind("./BookmarkGroup/DeclareButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DeclareButton;
        [AutoBind("./BookmarkGroup/DeclareButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DeclareButtonStateCtrl;
        [AutoBind("./BookmarkGroup/DeclareButtonGroup/ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DeclareButtonChooseStateCtrl;
        [AutoBind("./BookmarkGroup/DeclareButtonGroup/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image DeclareButtoncd;
        [AutoBind("./BookmarkGroup/DeclareButtonGroup/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform DeclareButtoncdLight;
        [AutoBind("./BookmarkGroup/AvoigWarButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AvoigWarButton;
        [AutoBind("./BookmarkGroup/AvoigWarButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AvoigWarButtonStateCtrl;
        [AutoBind("./BookmarkGroup/AvoigWarButtonGroup/ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AvoigWarButtonChooseStateCtrl;
        [AutoBind("./BookmarkGroup/AvoigWarButtonGroup/UnderwayIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image AvoigWarButtonCD;
        [AutoBind("./BookmarkGroup/AvoigWarButtonGroup/BeginImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform AvoigWarButtonCDLight;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/RewardGroup/ButtonIcon", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OccupiedRewardTipButton;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/RewardGroup/ItemGroup/Viewport/Content/ItemDummmy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonItemDummy;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/RewardGroup/ItemGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemScrollContent;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/RewardGroup/ItemGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public PrefabResourceContainer CommonItemRes;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/SupplyGroup/CurrencyGroup/Viewport/Content/CurrencyItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SovereignDailySupplyValueText;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/SummaryGroup/SummaryScrollText/Viewport/Content/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SummarizeDescText;
        [AutoBind("./SummaryDetailPanelGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SummarizeDescInfo;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/RewardGroup/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SummarizeDescInfoItemGroup;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/RewardGroup/CurrencyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SummarizeDescInfoCurrencyGroup;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/RewardGroup/CurrencyGroup/Viewport/Content/CurrencyItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SummarizeDescInfoCurrencyText;
        [AutoBind("./SummaryDetailPanelGroup/SummaryDetail/SupplyGroup/ButtonIcon", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SupplyTipButton;
        [AutoBind("./DescribeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OtherBattleStateDescInfo;
        [AutoBind("./SummaryDetailPanelGroup/RewardDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RewardTipPanelCtrl;
        [AutoBind("./SummaryDetailPanelGroup/SupplyDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SupplyTipPanelCtrl;
        [AutoBind("./SimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SimpleInfoDummy;
        [AutoBind("./SummaryDetailPanelGroup/RewardDetailInfoPanel/DetailInfoPanelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OccupiedRewardTipBgButton;
        [AutoBind("./SummaryDetailPanelGroup/SupplyDetailInfoPanel/DetailInfoPanelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SupplyTipBgButton;
        [AutoBind("./HintGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hintGroupStateCtrl;
        [AutoBind("./HintGroup/ContentGroup/HintText02", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_hintGroupHintText;
        [AutoBind("./HintGroup/ContentGroup/Buttons/ConfirmButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hintGroupConfirmButtonStateCtrl;
        [AutoBind("./HintGroup/ContentGroup/Buttons/ConfirmButtonGroup/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_hintGroupCostText;
        [AutoBind("./HintGroup/ContentGroup/Buttons/ConfirmButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_hintGroupConfirmButton;
        [AutoBind("./HintGroup/ContentGroup/Buttons/ConfirmButtonGroup/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_hintGroupConfirmBlackButton;
        [AutoBind("./HintGroup/ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_hintGroupCancelButton;
        [AutoBind("./HintGroup/MoneyGroup/SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GuildTradeMoneyAddButton;
        [AutoBind("./HintGroup/MoneyGroup/SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildTradeMoneyValueText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetPanelData;
        private static DelegateBridge __Hotfix_SetHintGroupInfo;
        private static DelegateBridge __Hotfix_GetGuildInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_GetHintGroupUIProcess;
        private static DelegateBridge __Hotfix_GetDeclareWarButtonUIProcess;
        private static DelegateBridge __Hotfix_GetRewardDetailPanelUIProcess;
        private static DelegateBridge __Hotfix_GetSovereignDailySupplyDetailPanelUIProcess;
        private static DelegateBridge __Hotfix_UpdatViewOfRecommendedConfigurationPanel;
        private static DelegateBridge __Hotfix_ShowOrHideRecommendedConfigurationPanelUIProcess;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateOccupiedReward;
        private static DelegateBridge __Hotfix_UpdateSovereignDailySupply;
        private static DelegateBridge __Hotfix_UpdateCountdownTime;
        private static DelegateBridge __Hotfix_IsCountDownMode;
        private static DelegateBridge __Hotfix_IsNeedShowTextCountdown;
        private static DelegateBridge __Hotfix_IsNeedShowButtonCountdown;
        private static DelegateBridge __Hotfix_SetDeclareGuildMode;
        private static DelegateBridge __Hotfix_SetDeclareNpcGuildMode;
        private static DelegateBridge __Hotfix_SetGuildCodeName;
        private static DelegateBridge __Hotfix_SetGuildWarMode;
        private static DelegateBridge __Hotfix_SetTitleContentText;
        private static DelegateBridge __Hotfix_SetButtonGroupState;
        private static DelegateBridge __Hotfix_SetButtonState;
        private static DelegateBridge __Hotfix_OnOccupiedItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnOccupiedItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnOccupiedItemIconClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_m_curUnderwayButton;

        public event Action<QuestRewardInfo> EventOnOccupiedItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetDeclareWarButtonUIProcess(bool isEnable, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetGuildInfoPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetHintGroupUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetRewardDetailPanelUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSovereignDailySupplyDetailPanelUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void IsCountDownMode(bool value)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedShowButtonCountdown()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedShowTextCountdown()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnOccupiedItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetButtonGroupState()
        {
        }

        [MethodImpl(0x8000)]
        private void SetButtonState(CommonUIStateController stateCtrl, CommonUIStateController chooseStateCtrl, Image cdImage, RectTransform cdLightImage, ButtonType selfButtonType)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDeclareGuildMode(GuildSolarSystemInfoClient guildSolarSystemInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDeclareNpcGuildMode(GuildSolarSystemInfoClient guildSolarSystemInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private string SetGuildCodeName(string CodeName)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildWarMode(SovereignBattleSimpleInfo sovereignBattleSimpleInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHintGroupInfo(bool isItCanDeclareWar, string solaryName, GuildSolarSystemInfoClient guildSolarSystemInfo, long costMoney, ulong currMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelData(ButtonType curChooseType, GuildSolarSystemInfoClient guildSolarSystemInfo, SovereignBattleSimpleInfo sovereignBattleSimpleInfo, ConfigDataGuildBenefitsConfigInfo occupiedRewardInfo, ConfigDataGuildBenefitsConfigInfo sovereignDailySupplyInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTitleContentText(ButtonType curChooseType)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideRecommendedConfigurationPanelUIProcess(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCountdownTime()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateOccupiedReward(ConfigDataGuildBenefitsConfigInfo benefitsInfo, bool isOccupiedByNpc, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSovereignDailySupply(ConfigDataGuildBenefitsConfigInfo benefitsInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatViewOfRecommendedConfigurationPanel(ConfigDataNpcSovereignBattleRecommendedConfigurationInfo info, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ButtonType m_curUnderwayButton
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum ButtonType
        {
            SummarizeButtonState,
            PrepareButtonState,
            FightButtonSate,
            TruceButtonState,
            DecisiveBattleButtonState,
            DeclareButtonState,
            AvoigWarButtonState
        }
    }
}

