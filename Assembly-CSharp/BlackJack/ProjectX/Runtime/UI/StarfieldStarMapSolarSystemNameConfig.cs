﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapSolarSystemNameConfig
    {
        [Header("恒星系名称开始显示的缩放比")]
        public float m_solarSystemNameTextVisibleScaleMin;
        [Header("恒星系名称显示透明度达到完全不透明时的缩放比")]
        public float m_solarSystemNameTextVisibleScaleMax;
        [Header("恒星系名称完全不透明时候的透明度，取0-1")]
        public float m_solarSystemNameTextAlphaMax;
        [Header("恒星系名称的背景完全不透明时候的透明度，取0-1")]
        public float m_solarSystemNameBGAlphaMax;
        [Header("当恒星系的图标变成最大缩放比时，恒星系名称Text控件的缩放比")]
        public float m_solarSystemNameTextScaleMax;
        [Header("当恒星系的图标变成最小缩放比时，恒星系名称Text控件的缩放比")]
        public float m_solarSystemNameTextScaleMin;
        [Header("恒星系名称的Text的显示位置偏移量")]
        public Vector2 m_solarSystemNameTextOffset;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

