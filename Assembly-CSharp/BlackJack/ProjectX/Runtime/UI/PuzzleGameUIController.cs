﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class PuzzleGameUIController : UIControllerBase
    {
        private Image[] m_matingRateImages;
        private RectTransform[] m_rightDummys;
        private RectTransform[] m_leftDummys;
        private const float MatchingRateImageAlphaMin = 0.196f;
        private const float MatchingRateImageAlphaMax = 1f;
        private int m_assistMoneyCount;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_closeButton;
        [AutoBind("./BackGroundImage", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backGroundButton;
        [AutoBind("./LockButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_lockButton;
        [AutoBind("./LockButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lockButtonStateCtrl;
        [AutoBind("./SolarsystemName", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_solarsystemNameStateCtrl;
        [AutoBind("./SolarsystemName", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_solarsystemNameRoot;
        [AutoBind("./SolarsystemName/Contentgroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tagetSolarsystemNameText;
        [AutoBind("./Line/LineImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_lineImageTrans;
        [AutoBind("./Line/StartPointImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_startPointTrans;
        [AutoBind("./Line", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lineStateCtrl;
        [AutoBind("./UnderwayGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_completeStateCtrl;
        [AutoBind("./UnderwayGroup/AssistButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_assistButton;
        [AutoBind("./UnderwayGroup/AssistButton/MoneyTextGroup/MoneyBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_moneyIconImage;
        [AutoBind("./UnderwayGroup/AssistButton/MoneyTextGroup/MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyCountText;
        [AutoBind("./UnderwayGroup/ProgressGroup/MatchingImage01", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_matchingImage1;
        [AutoBind("./UnderwayGroup/ProgressGroup/MatchingImage02", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_matchingImage2;
        [AutoBind("./UnderwayGroup/ProgressGroup/MatchingImage03", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_matchingImage3;
        [AutoBind("./UnderwayGroup/ProgressGroup/MatchingImage04", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_matchingImage4;
        [AutoBind("./UnderwayGroup/ProgressGroup/MatchingImage05", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_matchingImage5;
        [AutoBind("./LeftUpSolarsystemNameDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftUpSolarsystemNameDummy;
        [AutoBind("./LeftMiddleSolarsystemNameDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftMiddleSolarsystemNameDummy;
        [AutoBind("./LeftDownSolarsystemNameDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftDownSolarsystemNameDummy;
        [AutoBind("./RightUpSolarsystemNameDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightUpSolarsystemNameDummy;
        [AutoBind("./RightMiddleSolarsystemNameDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightMiddleSolarsystemNameDummy;
        [AutoBind("./RightDownSolarsystemNameDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightDownSolarsystemNameDummy;
        [AutoBind("./UnderwayGroup/HintGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hintStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetMatchingRate;
        private static DelegateBridge __Hotfix_SetLockButtonState;
        private static DelegateBridge __Hotfix_SetCompelteState;
        private static DelegateBridge __Hotfix_SetTargetSolarSystemPanel;
        private static DelegateBridge __Hotfix_SetHintState;
        private static DelegateBridge __Hotfix_SetAssistMoney;
        private static DelegateBridge __Hotfix_GetAssistMoney;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_GetSolarsystemNamePanelUIProcess;
        private static DelegateBridge __Hotfix_SetSolarSystemNamePos;
        private static DelegateBridge __Hotfix_SetLinePos;
        private static DelegateBridge __Hotfix_get_MatingRateImages;
        private static DelegateBridge __Hotfix_get_RightDummys;
        private static DelegateBridge __Hotfix_get_LeftDummys;

        [MethodImpl(0x8000)]
        public int GetAssistMoney()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetMainUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetSolarsystemNamePanelUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAssistMoney(int assistCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCompelteState(bool isComplete)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHintState(int assistCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetLinePos(Vector2 startPos, bool isRight)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLockButtonState(bool isLocked)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMatchingRate(float rate)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSolarSystemNamePos(Vector2 pos, bool isRight)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetSolarSystemPanel(Vector2 pos, Vector2 centerPos, string solarsystemName)
        {
        }

        private Image[] MatingRateImages
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private RectTransform[] RightDummys
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private RectTransform[] LeftDummys
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

