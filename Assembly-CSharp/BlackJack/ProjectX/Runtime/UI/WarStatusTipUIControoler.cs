﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class WarStatusTipUIControoler : UIControllerBase
    {
        public Action EventOnBGButtonClick;
        public Action EventOnBattleStatusEnd;
        private bool m_isEnd;
        private bool m_isNotStart;
        private GuildBattleStatus m_showBattleStatus = GuildBattleStatus.BattleEnd;
        private DateTime m_lastFightingEndTime;
        private DateTime m_nextFightingTime;
        private DateTime m_nextTickTime;
        private const uint RedStateShowLimitTime = 0x36ee80;
        [AutoBind("./BgButtonImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./FrameImage/BGImage/AnnounceDetailInfo/TimeText/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PrepareTimeStateCtrl;
        [AutoBind("./FrameImage/BGImage/TruceDetailInfo/TimeText/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ReinforcementTimeStateCtrl;
        [AutoBind("./FrameImage/BGImage/DeclareDetailInfo/TimeText/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DeclarationTimeStateCtrl;
        [AutoBind("./FrameImage/BGImage/AnnounceDetailInfo/TimeText/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PrepareTimeText;
        [AutoBind("./FrameImage/BGImage/TruceDetailInfo/TimeText/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ReinforcementTimeText;
        [AutoBind("./FrameImage/BGImage/DeclareDetailInfo/TimeText/TimeNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DeclarationTimeText;
        [AutoBind("./FrameImage/BGImage/AnnounceDetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PrepareStateCtrl;
        [AutoBind("./FrameImage/BGImage/FightDetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FirstHalfStateCtrl;
        [AutoBind("./FrameImage/BGImage/TruceDetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ReinforcementStateCtrl;
        [AutoBind("./FrameImage/BGImage/DecisiveBattleDetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SecondHalfStateCtrl;
        [AutoBind("./FrameImage/BGImage/DeclareDetailInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DeclarationStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        private const string PrepareState = "Announce";
        private const string FirstHalfState = "Fight";
        private const string ReinforcementState = "Truce";
        private const string SecondHalfState = "DecisiveBattle";
        private const string DeclarationState = "Declare";
        private const string CloseState = "Close";
        private const string UnderwayState = "Underway";
        private const string TimeState = "Time";
        private const string NotStartedState = "NoStarted";
        private const string EndState = "End";
        private const string RedState = "Red";
        private const string NormalState = "Normal";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowTipPanel;
        private static DelegateBridge __Hotfix_HideTipPanel;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_CalcBattleStatusRestTime;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        private TimeSpan CalcBattleStatusRestTime()
        {
        }

        [MethodImpl(0x8000)]
        public void HideTipPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowTipPanel(GuildBattleStatus showStatus, GuildBattleStatus currStatus, DateTime startTime, DateTime endTime)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

