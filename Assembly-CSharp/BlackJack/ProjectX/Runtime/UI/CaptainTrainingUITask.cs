﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CaptainTrainingUITask : UITaskBase
    {
        public const string ParamKey_CaptainInstanceId = "CaptainInstanceId";
        private CaptainTrainingUIController m_mainCtrl;
        private LBStaticHiredCaptain m_selCaptain;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType, bool> EventOnShowItemSimpleInfo;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseItemSimpleInfo;
        private int m_currentSelMemoirId;
        private long m_currentSelMemoirCount;
        private long m_currentSelMemoirTotalCount;
        private bool m_isSelCountExceedTotalCount;
        private bool m_isExpOverflow;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCaptainTrainStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCaptainTrainEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action OnCloseUI;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCaptainTrainingUITask;
        private static DelegateBridge __Hotfix_BringToTop;
        private static DelegateBridge __Hotfix_GetThirdMemoirsBookRect;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetFirstMemoirsBookRect;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnMemoirBookButtonClick;
        private static DelegateBridge __Hotfix_ShowExpProgressAndSendAddExpNetReq;
        private static DelegateBridge __Hotfix_OnMemoirBookButtonLongPressingStart;
        private static DelegateBridge __Hotfix_OnMemoirBookButtonLongPressing;
        private static DelegateBridge __Hotfix_OnMemoirBookButtonLongPressingEnd;
        private static DelegateBridge __Hotfix_SendHiredCaptainAddExpByItemNetReq;
        private static DelegateBridge __Hotfix_OnCaptainExpLevelUp;
        private static DelegateBridge __Hotfix_GetCacheDataFromUIIntent;
        private static DelegateBridge __Hotfix_GetMemoirBookIdWithIndex;
        private static DelegateBridge __Hotfix_CheckHiredCaptainCanAddExp;
        private static DelegateBridge __Hotfix_CheckHiredCaptainLevelExceedCharacterLevel;
        private static DelegateBridge __Hotfix_ShowSelectedBookItemInfo;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_add_EventOnShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_remove_EventOnShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_add_EventOnCloseItemSimpleInfo;
        private static DelegateBridge __Hotfix_remove_EventOnCloseItemSimpleInfo;
        private static DelegateBridge __Hotfix_add_EventOnCaptainTrainStart;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainTrainStart;
        private static DelegateBridge __Hotfix_add_EventOnCaptainTrainEnd;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainTrainEnd;
        private static DelegateBridge __Hotfix_add_OnCloseUI;
        private static DelegateBridge __Hotfix_remove_OnCloseUI;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnCaptainTrainEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCaptainTrainStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseItemSimpleInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient, Vector3, string, ItemSimpleInfoUITask.PositionType, bool> EventOnShowItemSimpleInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action OnCloseUI
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CaptainTrainingUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringToTop()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckHiredCaptainCanAddExp()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckHiredCaptainLevelExceedCharacterLevel(long bookCnt)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void GetCacheDataFromUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstMemoirsBookRect()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private int GetMemoirBookIdWithIndex(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetThirdMemoirsBookRect()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainExpLevelUp()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemoirBookButtonClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemoirBookButtonLongPressing(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemoirBookButtonLongPressingEnd(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemoirBookButtonLongPressingStart(int idx)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void SendHiredCaptainAddExpByItemNetReq()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowExpProgressAndSendAddExpNetReq(int idx, int exp)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowSelectedBookItemInfo(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public static CaptainTrainingUITask StartCaptainTrainingUITask(ulong captainInsId, Action onCloseAction, Action<bool> onPiplineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

