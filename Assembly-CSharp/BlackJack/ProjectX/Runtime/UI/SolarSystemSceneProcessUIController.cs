﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using Dest.Math;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemSceneProcessUIController : UIControllerBase
    {
        private ILBSpaceContext m_spaceCtx;
        private ClientNpcSpaceShip m_tgtNpcObj;
        private Vector3D m_tgtPosition;
        private uint m_startTime;
        private uint m_endTime;
        private InSpaceSceneProcessBarNtf m_sceneProcessInfo;
        private bool m_isProcessClear;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ProcessUIStateCtrl;
        [AutoBind("./ProcessMessageText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SceneProcessMsgText;
        [AutoBind("./PorgressBarBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SceneProcessBarRoot;
        [AutoBind("./PorgressBarBGImage/PorgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image SceneProcessBar;
        [AutoBind("./ProcessOrTime/ProcessOrTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SceneProcessValueText;
        [AutoBind("./ProcessOrTime/Time", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SceneProcessCoolDownTimeImage;
        private static DelegateBridge __Hotfix_ShowProcessUI;
        private static DelegateBridge __Hotfix_ShowSceneProcess;
        private static DelegateBridge __Hotfix_UpdateSceneProcess;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_SpaceCtx;

        [MethodImpl(0x8000)]
        public void ShowProcessUI(bool isShow, bool immediately = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowSceneProcess(InSpaceSceneProcessBarNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSceneProcess(InSpaceSceneProcessBarUpdateNtf ntf)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ILBSpaceContext SpaceCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <ShowProcessUI>c__AnonStorey0
        {
            internal bool isShow;
            internal SolarSystemSceneProcessUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool bo)
            {
            }
        }
    }
}

