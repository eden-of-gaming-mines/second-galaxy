﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class StargateMenuItemUIController : MenuItemUIControllerBase
    {
        [AutoBind("./TargetInfo/Info/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_stargateNameText;
        [AutoBind("./Detail/DestinationItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_destSolarSystemNameText;
        [AutoBind("./Detail/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        private GDBStargateInfo m_gdbStargateInfo;
        private static DelegateBridge __Hotfix_SetStargateInfoByGDBData;
        private static DelegateBridge __Hotfix_GetGDBStargateInfo;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;

        [MethodImpl(0x8000)]
        public GDBStargateInfo GetGDBStargateInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public void SetStargateInfoByGDBData(GDBStargateInfo info)
        {
        }
    }
}

