﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class TechCostItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private CostInfo <CostItemInfo>k__BackingField;
        [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIconImage;
        [AutoBind("./ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ItemIconImage;
        [AutoBind("./ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ReduceEffectImage;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public Button ItemButton;
        [AutoBind("./MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetCostItemInfo;
        private static DelegateBridge __Hotfix_get_CostItemInfo;
        private static DelegateBridge __Hotfix_set_CostItemInfo;

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCostItemInfo(CostInfo realCostInfo, CostInfo configCostInfo, Dictionary<string, Object> resDict)
        {
        }

        public CostInfo CostItemInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

