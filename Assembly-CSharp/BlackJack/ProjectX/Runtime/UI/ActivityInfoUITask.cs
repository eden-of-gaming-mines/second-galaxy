﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ActivityInfoUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool, bool> EventOnRequestExploreUIClose;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ActionPlanTabType, bool, int, object> EventOnRequestSwitchTab;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GotoType, bool, object> EventOnGoToTask;
        private int m_setToggleIndex;
        private bool m_isCurrActivityExist;
        private ActivityInfo m_currSelectedActivityInfo;
        private List<ActivityInfo> m_allActivityList;
        private List<PlayerActivityInfo> m_playActivityList;
        private List<ConfigDataVitalityRewardInfo> m_vitalityRewardList;
        private List<int> m_waitForCompleteSubQuestList;
        public List<int> m_mainStoryList;
        public int m_waitOrAlreadyCompelteCount;
        private int m_currStoryQuest;
        private float m_refreshDelayTime;
        private const float m_sRefreshDelayTime = 10f;
        private bool m_isTaskInit;
        private bool m_isBlockToggleEvent;
        private bool m_isPlayPanelOpenAnim;
        private ActivityInfoUIController m_mainCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private IUIBackgroundManager m_backgroundManager;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string IsPlayPanelOpenAaim_ParamKey = "IsPlayPanelOpenAaim";
        public const string TaskName = "ActivityInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartActivityUITask;
        private static DelegateBridge __Hotfix_ShowAcvityPanel;
        private static DelegateBridge __Hotfix_HideActivityPanel;
        private static DelegateBridge __Hotfix_SetPanelPostion;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_SetBackGroundManager;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewExplore;
        private static DelegateBridge __Hotfix_UpdateDailyExpView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnActivityToggleItemSelected;
        private static DelegateBridge __Hotfix_OnVitalityRewardNodeButtonClick;
        private static DelegateBridge __Hotfix_OnGoButtonClick;
        private static DelegateBridge __Hotfix_OnDropItemClick;
        private static DelegateBridge __Hotfix_OnDailyExpExplainButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_OnActivityInfoNtf;
        private static DelegateBridge __Hotfix_OnPlayerActivityInfoNtf;
        private static DelegateBridge __Hotfix_OnDailyRefreshNtf;
        private static DelegateBridge __Hotfix_StartOrResumeOrNtfExtraProcessForExplore;
        private static DelegateBridge __Hotfix_IsActivityListNeedRefresh;
        private static DelegateBridge __Hotfix_IsActivityDetailNeedRefresh;
        private static DelegateBridge __Hotfix_IsVitalityRewardNeedRefresh;
        private static DelegateBridge __Hotfix_IsSelectedActivity;
        private static DelegateBridge __Hotfix_SortActivityList;
        private static DelegateBridge __Hotfix_CheckActivityOpen;
        private static DelegateBridge __Hotfix_ActivityInfoComparer;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_ShowSystemFunctionNotOpenTipWindow;
        private static DelegateBridge __Hotfix_CloseExplorePanel;
        private static DelegateBridge __Hotfix_RequestSwitchTask;
        private static DelegateBridge __Hotfix_RequestSwitchTab;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetWormHoleActivityRect;
        private static DelegateBridge __Hotfix_add_EventOnRequestExploreUIClose;
        private static DelegateBridge __Hotfix_remove_EventOnRequestExploreUIClose;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTab;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTab;
        private static DelegateBridge __Hotfix_add_EventOnGoToTask;
        private static DelegateBridge __Hotfix_remove_EventOnGoToTask;
        private static DelegateBridge __Hotfix_get_m_lbActivity;
        private static DelegateBridge __Hotfix_get_WaitForCompleteSubQuestList;
        private static DelegateBridge __Hotfix_get_CurrStoryQuest;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<GotoType, bool, object> EventOnGoToTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool, bool> EventOnRequestExploreUIClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ActionPlanTabType, bool, int, object> EventOnRequestSwitchTab
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public ActivityInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private int ActivityInfoComparer(ActivityInfo activityA, ActivityInfo activityB)
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckActivityOpen(ActivityInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseExplorePanel(Action evntAfterPause, bool isIgnoreAnim = false, bool isReapeatableUserGuide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetWormHoleActivityRect()
        {
        }

        [MethodImpl(0x8000)]
        public void HideActivityPanel(bool isImmediate, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsActivityDetailNeedRefresh()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsActivityListNeedRefresh()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsSelectedActivity()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsVitalityRewardNeedRefresh()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnActivityInfoNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void OnActivityToggleItemSelected(ActivityInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundClick(Action onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDailyExpExplainButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDailyRefreshNtf()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropItemClick(ItemDropInfoUIShowItemList itemInfo, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoButtonClick(ActivityInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerActivityInfoNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnVitalityRewardNodeButtonClick(ConfigDataVitalityRewardInfo info, int rewardIndex, Vector3 simpleInfoPos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTab(ActionPlanTabType type, int param, bool isReaptableUserGuide = true, object param2 = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBackGroundManager(IUIBackgroundManager backgroundManager)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPostion(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAcvityPanel(bool isImmediate = false, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        protected bool ShowSystemFunctionNotOpenTipWindow(SystemFuncType funcType)
        {
        }

        [MethodImpl(0x8000)]
        protected List<ActivityInfo> SortActivityList(List<ActivityInfo> oldActivityInfoList)
        {
        }

        [MethodImpl(0x8000)]
        public static ActivityInfoUITask StartActivityUITask(bool isPlayOpenAnim = true, Action onResLoadEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartOrResumeOrNtfExtraProcessForExplore()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDailyExpView()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewExplore()
        {
        }

        private LogicBlockActivityClient m_lbActivity
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<int> WaitForCompleteSubQuestList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int CurrStoryQuest
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideActivityPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal ActivityInfoUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnVitalityRewardNodeButtonClick>c__AnonStorey1
        {
            internal ConfigDataVitalityRewardInfo info;
            internal ActivityInfoUITask $this;

            internal void <>m__0(Task task)
            {
                if (!(task as ActivityVitalityGetRewardReqTask).IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    int result = (task as ActivityVitalityGetRewardReqTask).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                    }
                    else
                    {
                        if (!this.$this.m_currPipeLineCtx.IsNeedUpdate(3))
                        {
                            this.$this.m_currPipeLineCtx.AddUpdateMask(3);
                            this.$this.m_currPipeLineCtx.AddUpdateMask(6);
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }
                        string stringInStringTableWithId = StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_PickDropBoxItemSuccess, new object[0]);
                        if (this.info.ItemType == StoreItemType.StoreItemType_Currency)
                        {
                            TipWindowUITask.ShowTipWindow(string.Format(stringInStringTableWithId, $"{StringFormatUtil.GetCurrencyTypeName((CurrencyType) this.info.ConfigId)}  {this.info.Count}"), false);
                        }
                        else if (!this.$this.PlayerCtx.GetLBCharacter().IsInSpace())
                        {
                            TipWindowUITask.ShowTipWindow(string.Format(stringInStringTableWithId, SolarSystemUIHelper.GetDropBoxItemDescStr(this.info.ItemType, this.info.ConfigId, (long) this.info.Count)), false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowItemSimpleInfoPanel>c__AnonStorey2
        {
            internal FakeLBStoreItem item;
            internal Vector3 pos;
            internal string mode;
            internal ItemSimpleInfoUITask.PositionType type;
            internal ActivityInfoUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.item, returnIntent, true, this.pos, this.mode, this.type, false, null, backgroundManager, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.$this.OnItemSimpleInfoUITaskEnterAnotherUITask);
                }
            }
        }

        public enum PipeLineMask
        {
            Init = 1,
            Resume = 2,
            Refresh = 3,
            ModeChage = 4,
            OnlyRefreshDetailInfo = 5,
            OnlyRefreshVitalityInfo = 6,
            ActivityChange = 7,
            PlayActivityChange = 8
        }
    }
}

