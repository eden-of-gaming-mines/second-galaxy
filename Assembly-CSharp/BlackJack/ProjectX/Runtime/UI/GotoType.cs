﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum GotoType
    {
        StarMapDelegateMineral,
        StarMapDelegateFight,
        StarMapFreeQuest,
        StarMapWormhole,
        StarMapInfect,
        StarMapInvade,
        NpcShopPurchase,
        CharacterLevel,
        CharacterChipSet,
        CharacterPropertiesPointAdd,
        CharacterDriving,
        CharacterSkill,
        ShipHangarUnpackShip,
        ShipHangarShipAmmoSet,
        ShipHangarShipWeaponEquipSlotSet,
        CaptainTraining,
        CaptainLearning,
        CaptainShipUnlockShip,
        TechUpgrade,
        CrackComplete,
        ProduceComplete,
        ProduceItem,
        ScienceExplore,
        PersonalTrade,
        SpaceSignal,
        FactionQuest
    }
}

