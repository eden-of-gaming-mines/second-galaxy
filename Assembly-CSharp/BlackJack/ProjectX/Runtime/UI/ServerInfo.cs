﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ServerInfo
    {
        public ServerState m_serverState;
        public string m_serverId;
        public Dictionary<string, string> m_serverNameLocalizetionDict;
        public string m_serverDomain;
        public string m_serverIp;
        public string m_serverPort;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetServerName;
        private static DelegateBridge __Hotfix_IsServerVaild;

        [MethodImpl(0x8000)]
        public string GetServerName()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsServerVaild()
        {
        }

        public enum ServerState
        {
            None,
            Normal,
            UnderMaintenance,
            Hot,
            Crowded
        }
    }
}

