﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapDrawingConfig
    {
        [Header("全局配置")]
        public StarfieldStarMapDrawingGlobalConfig m_starMapDrawingGlobalConfig;
        [Header("恒星系的点配置")]
        public StarfieldStarMapDrawingPointConfig m_starMapDrawingPointConfig;
        [Header("恒星系之间的连线配置")]
        public StarfieldStarMapDrawingLineConfig m_starMapDrawingLineConfig;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

