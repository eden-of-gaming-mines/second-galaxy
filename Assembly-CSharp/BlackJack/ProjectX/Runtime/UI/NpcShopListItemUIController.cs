﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopListItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<NpcShopListItemUIController> EventOnItemIconClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NpcShopListItemUIController> EventOnItemIcon3DTouch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NpcShopListItemUIController> EventOnItemLongPressStart;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemButton;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemCtrl;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemDummyTrans;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemNameText;
        [AutoBind("./CDTime/CDTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_cdTimeText;
        [AutoBind("./CountTitleText/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemCountText;
        [AutoBind("./MoneyBGImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_moneyIconImage;
        [AutoBind("./MoneyBGImage/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyNumberText;
        [AutoBind("./PossessTitleText/CountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_alreadyHaveCount;
        [AutoBind("./ScientificHint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_depentTechNotSatisfy;
        [AutoBind("./LockImage/TextGroup/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_factionCreditHitText;
        public GameObject m_itemIconPrefab;
        public CommonItemIconUIController m_itemIconCtrl;
        public ScrollItemBaseUIController m_scrollItemCtrl;
        public NpcShopMainUITask.NpcShopItemUIInfo m_itemUIInfo;
        public Dictionary<string, UnityEngine.Object> m_resDict;
        private bool m_isLockedByFactionCredit;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateCommonIconItem;
        private static DelegateBridge __Hotfix_SetItemInfoToUI;
        private static DelegateBridge __Hotfix_GetItemPrice;
        private static DelegateBridge __Hotfix_GetItemIconPosition;
        private static DelegateBridge __Hotfix_GetItemIconWidth;
        private static DelegateBridge __Hotfix_IsLockedByFactionCredit;
        private static DelegateBridge __Hotfix_SetItemSellStateInfo;
        private static DelegateBridge __Hotfix_GetFactionCreditLevelInfoFromCreditLevel;
        private static DelegateBridge __Hotfix_SetTechConditionInfo;
        private static DelegateBridge __Hotfix_OnItemIconClick;
        private static DelegateBridge __Hotfix_OnItemIcon3DTouch;
        private static DelegateBridge __Hotfix_OnItemLongPressStart;
        private static DelegateBridge __Hotfix_add_EventOnItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnItemIcon3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnItemIcon3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnItemLongPressStart;
        private static DelegateBridge __Hotfix_remove_EventOnItemLongPressStart;

        public event Action<NpcShopListItemUIController> EventOnItemIcon3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopListItemUIController> EventOnItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopListItemUIController> EventOnItemLongPressStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GameObject CreateCommonIconItem()
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataFactionCreditLevelInfo GetFactionCreditLevelInfoFromCreditLevel(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemIconPosition()
        {
        }

        [MethodImpl(0x8000)]
        public float GetItemIconWidth()
        {
        }

        [MethodImpl(0x8000)]
        public float GetItemPrice(NpcShopMainUITask.NpcShopItemUIInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsLockedByFactionCredit()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIcon3DTouch(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemInfoToUI(NpcShopMainUITask.NpcShopItemUIInfo itemInfo, bool isShowTechDissatisfyObj, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetItemSellStateInfo(long itemCount, ConfigDataNpcShopItemInfo config)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTechConditionInfo(ConfigDataNpcShopItemInfo config)
        {
        }
    }
}

