﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ScreenRecorderUITask : UITaskBase
    {
        protected bool m_isPreViewing;
        protected DateTime m_recordStartTime;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private bool m_isWaitingForPreview;
        private ScreenRecorderUIController m_mainCtrl;
        private const string TaskName = "ScreenRecorderUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_StopTask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_OnLogout;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnRecordClick;
        private static DelegateBridge __Hotfix_OnCastClick;
        private static DelegateBridge __Hotfix_OnStopLiveClick;
        private static DelegateBridge __Hotfix_OnStopRecord;
        private static DelegateBridge __Hotfix_OnEnableMicrophoneChanged;
        private static DelegateBridge __Hotfix_OnEnableCameraChanged;
        private static DelegateBridge __Hotfix_StartScreenRecorder;
        private static DelegateBridge __Hotfix_OnScreenRecorderStart;
        private static DelegateBridge __Hotfix_StopScreenRecorder;
        private static DelegateBridge __Hotfix_OnScreenRecorderStop;
        private static DelegateBridge __Hotfix_UpdateScreenRecord;
        private static DelegateBridge __Hotfix_StartBroadCast;
        private static DelegateBridge __Hotfix_StartBroadCastCallback;
        private static DelegateBridge __Hotfix_OnBroadCastStart;
        private static DelegateBridge __Hotfix_StopBroadCast;
        private static DelegateBridge __Hotfix_OnBroadCastStop;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ScreenRecorderUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBroadCastStart()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBroadCastStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCastClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnableCameraChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnableMicrophoneChanged(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLogout(bool b, bool switchAccount)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRecordClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnScreenRecorderStart()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnScreenRecorderStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStopLiveClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStopRecord()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartBroadCast()
        {
        }

        [MethodImpl(0x8000)]
        private void StartBroadCastCallback(bool started, string error)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartScreenRecorder()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopBroadCast()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopScreenRecorder()
        {
        }

        [MethodImpl(0x8000)]
        public static void StopTask()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateScreenRecord()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

