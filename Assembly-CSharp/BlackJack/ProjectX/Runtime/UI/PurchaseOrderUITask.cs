﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class PurchaseOrderUITask : GuildUITaskBase
    {
        private ShipCustomTemplateInfo m_templateInfo;
        private const string TemplateInfo = "TemplateInfo";
        private const string Show = "Show";
        private const string Close = "Close";
        private ItemSimpleInfoUITask m_itemTask;
        private PurchaseOrderFormBoxUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "PurchaseOrderUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_TradeItemIconPath;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnTipsInfoOpen;
        private static DelegateBridge __Hotfix_OnTipsInfoClose;
        private static DelegateBridge __Hotfix_OnNewOrderConfirm;
        private static DelegateBridge __Hotfix_OperateOrderQueue;
        private static DelegateBridge __Hotfix_SendOrder;
        private static DelegateBridge __Hotfix_OnNewOrderCancel;
        private static DelegateBridge __Hotfix_CloseWindow;
        private static DelegateBridge __Hotfix_RegisterEvents;
        private static DelegateBridge __Hotfix_UnregisterEvents;
        private static DelegateBridge __Hotfix_OnItemDetailOpen;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public PurchaseOrderUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseWindow()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemDetailOpen(FakeLBStoreItem item, Vector3 tipsPosition)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewOrderCancel(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewOrderConfirm(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipsInfoClose(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipsInfoOpen(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OperateOrderQueue(Queue<KeyValuePair<int, int>> orders)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        private void SendOrder(Queue<KeyValuePair<int, int>> orders, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(UIIntent preIntent, ShipCustomTemplateInfo templateInfo)
        {
        }

        [MethodImpl(0x8000)]
        private static string TradeItemIconPath(StoreItemType itemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OperateOrderQueue>c__AnonStorey1
        {
            internal Queue<KeyValuePair<int, int>> orders;
            internal PurchaseOrderUITask $this;

            internal void <>m__0()
            {
                this.$this.SendOrder(this.orders, new Action(this.$this.CloseWindow));
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal bool storeInfoReqComplete;
            internal bool listReqComplete;
            internal bool priceReqComplete;
            internal Action<bool> onPrepareEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(bool res)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__2(bool res)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendOrder>c__AnonStorey2
        {
            internal Queue<KeyValuePair<int, int>> orders;
            internal Action onEnd;
            internal PurchaseOrderUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.SendOrder(this.orders, this.onEnd);
            }
        }
    }
}

