﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CommonItemUseUITask : UITaskBase
    {
        private List<StoreItemUpdateInfo> m_useResultItemList;
        private List<CurrencyUpdateInfo> m_useResultCurrencyList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private NormalItemType <CurrNormalItemType>k__BackingField;
        private CommonItemUseUIController m_mainCtrl;
        private ILBStoreItemClient m_currItem;
        protected long m_itemTotalCount;
        protected long m_itemCurrCount;
        protected long m_maxUseCount;
        protected bool m_isDuringUpdateView;
        protected DateTime m_addButtonLongPressStartTime;
        protected DateTime m_removeButtonLongPressStartTime;
        public static string ParamKey_CurrItem = "CurrItem";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public Action<bool> m_onConfirmCallbackEnd;
        public const string TaskName = "CommonItemUseUITask";
        private bool m_isRepeatableUserGuide;
        public const string ParamKeyIsRepeatableUserGuide = "IsRepeatableUserGuide";
        private GuideState m_currGuideState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnComfirmButtonClick_0;
        private static DelegateBridge __Hotfix_OnComfirmButtonClick_1;
        private static DelegateBridge __Hotfix_OnAddItemButtonClick;
        private static DelegateBridge __Hotfix_OnAddItemButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnAddItemButtonLongPressed;
        private static DelegateBridge __Hotfix_OnRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnRemoveButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnRemoveButtonLongPressed;
        private static DelegateBridge __Hotfix_OnMaxButtonClick;
        private static DelegateBridge __Hotfix_OnItemCountSliderValueChanged;
        private static DelegateBridge __Hotfix_SetItemCurrCount;
        private static DelegateBridge __Hotfix_StartCommonItemUseUITask;
        private static DelegateBridge __Hotfix_InitPipeLineCtxStateFromUIIntent;
        private static DelegateBridge __Hotfix_ReturnPreUITask;
        private static DelegateBridge __Hotfix_get_UseResultItemList;
        private static DelegateBridge __Hotfix_get_UseResultCurrencyList;
        private static DelegateBridge __Hotfix_get_CurrNormalItemType;
        private static DelegateBridge __Hotfix_set_CurrNormalItemType;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuideCommonItemUseUITask;
        private static DelegateBridge __Hotfix_InitRepeatableDataFromIntent;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuide;
        private static DelegateBridge __Hotfix_ResetGuideState;
        private static DelegateBridge __Hotfix_get_IsRepeatableUserGuide;

        [MethodImpl(0x8000)]
        public CommonItemUseUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPipeLineCtxStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitRepeatableDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAddItemButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAddItemButtonLongPressed(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAddItemButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnComfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnComfirmButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemCountSliderValueChanged(float value)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMaxButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRemoveButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRemoveButtonLongPressed(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnRemoveButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void ResetGuideState()
        {
        }

        [MethodImpl(0x8000)]
        protected void ReturnPreUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetItemCurrCount(long count, bool showErrorTips = false)
        {
        }

        [MethodImpl(0x8000)]
        public static CommonItemUseUITask StartCommonItemUseUITask(ILBStoreItemClient item, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public static CommonItemUseUITask StartRepeatableUserGuideCommonItemUseUITask(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public List<StoreItemUpdateInfo> UseResultItemList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<CurrencyUpdateInfo> UseResultCurrencyList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public NormalItemType CurrNormalItemType
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsRepeatableUserGuide
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnComfirmButtonClick>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal CommonItemUseUITask $this;

            internal void <>m__0(Task task)
            {
                StoreItemUseReqNetTask task2 = task as StoreItemUseReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AckResult != 0)
                    {
                        Debug.LogError($"StoreItemUseReqNetTask error: returnTask.AckResult = {task2.AckResult}");
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                    }
                    else
                    {
                        this.$this.m_useResultCurrencyList.AddRange(task2.CurrencyUpdateInfoList);
                        this.$this.m_useResultItemList.AddRange(task2.StoreItemUpdateInfoList);
                        this.$this.m_onConfirmCallbackEnd = this.onEnd;
                        this.$this.ReturnPreUITask();
                    }
                }
            }
        }

        private enum GuideState
        {
            None,
            Start
        }
    }
}

