﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TechUpgradeSpeedUpUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnUpgradInfoBGButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnUpgradeImmediateButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDetailButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSpeedUpItemClick;
        protected TechUpgradeInfo m_upgradeInfo;
        private readonly List<Text> m_speedupValueTextList;
        private List<CommonItemIconUIController> m_speedupItemCtrlList;
        private List<GameObject> m_levelPropInfoList;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./Panel/TechItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./Panel/TechItem/LevelChange/OldLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CurrLevelText;
        [AutoBind("./Panel/TechItem/LevelChange/NewLevel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NextLevelText;
        [AutoBind("./Panel/ProcessBar/ProcessBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image UpgradeProcessbar;
        [AutoBind("./Panel/UpgradeImmediatelyButton/MoneyTextGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyNumberText;
        [AutoBind("./Panel/UpgradeImmediatelyButton/GrayImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_grayImageButton;
        [AutoBind("./Panel/TechItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechNameText;
        [AutoBind("./Panel/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RestTimeText;
        [AutoBind("./Panel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./Panel/UpgradeImmediatelyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeImmediatelyButton;
        [AutoBind("./Panel/UpgradeImmediatelyButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UpgradeImmediatelyButtonCtrl;
        [AutoBind("./Panel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SpeedUpItemGroup;
        [AutoBind("./UpgradeInfo/ItemGroup/Item1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropItem;
        [AutoBind("./UpgradeInfo/BgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UpgradeInfoBGButton;
        [AutoBind("./UpgradeInfo/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropInfoListRoot;
        [AutoBind("./UpgradeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropRoot;
        [AutoBind("./UpgradeInfo", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PropPanelShowCtrl;
        [AutoBind("./Panel/TechItem/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DetailButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateSpeedUpPanelInfo;
        private static DelegateBridge __Hotfix_ShowOrHideTechDetailLevelPropInfo;
        private static DelegateBridge __Hotfix_UpdateTechDetailLevelPropInfo;
        private static DelegateBridge __Hotfix_GetPropValueStr;
        private static DelegateBridge __Hotfix_GetTechBuffDescStr;
        private static DelegateBridge __Hotfix_SetSpeedUpItemInfo;
        private static DelegateBridge __Hotfix_OnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeImmediateButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradInfoBGButtonClick;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnGrayImageButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnUpgradInfoBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUpgradInfoBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnUpgradeImmediateButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUpgradeImmediateButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDetailButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_get_SpeedUpItemCtrlList;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDetailButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSpeedUpItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnUpgradeImmediateButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnUpgradInfoBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private string GetPropValueStr(ConfigDataTechLevelInfo techLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetTechBuffDescStr(ConfigDataTechLevelInfo techLevelInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGrayImageButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeImmediateButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradInfoBGButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSpeedUpItemInfo(int idx, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideTechDetailLevelPropInfo(bool isShow, ConfigDataTechInfo techInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSpeedUpPanelInfo(int techId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTechDetailLevelPropInfo(ConfigDataTechInfo techInfo)
        {
        }

        private List<CommonItemIconUIController> SpeedUpItemCtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

