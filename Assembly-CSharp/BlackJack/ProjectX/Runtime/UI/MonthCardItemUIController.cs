﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class MonthCardItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnMonthCardBuyButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnMonthCardDailyRewardButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int, ILBStoreItemClient> EventOnMonthCardDailyRewardItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnPrivilegeDescTipButtonClick;
        private LBRechargeMonthlyCard m_monthCardInfo;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private readonly List<MonthCardPrivilegeItemUIController> m_cardPrivilegeItemCtrlList;
        private readonly List<FakeLBStoreItem> m_dailyRewardFakeItemList;
        private int m_cardItemIndex;
        private GameObject m_commonItemTemplateGo;
        private GameObject m_monthCardPrivilegeItemTemplateGo;
        private DateTime m_updateMonthCardLeftTimeCooldownTime;
        private const string CommonItemAssetName = "CommonItemUIPrefab";
        private const string MonthCardPrivilegeItemAssetName = "MonthCardPrivilegeItemUIPrefab";
        private static string m_leftHourStr;
        private static string m_leftDayStr;
        [AutoBind("./ExplainTipRoot/ExplainTipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ExplainTipButton;
        [AutoBind("./ExplainTipRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ExplainTipRoot;
        [AutoBind("./ButtonGroup/DailyRewardButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DailyRewardButtonStateCtrl;
        [AutoBind("./ButtonGroup/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BuyButtonStateCtrl;
        [AutoBind("./ButtonGroup/DailyRewardButton/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MonthCardLeftTimeText;
        [AutoBind("./ButtonGroup/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyButton;
        [AutoBind("./ButtonGroup/DailyRewardButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DailyRewardButton;
        [AutoBind("./ButtonGroup/BuyButton/BuyGroup/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonPriceText;
        [AutoBind("./ButtonGroup/BuyButton/BuyGroup/IconGroup/IconText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyButtonCurrencyText;
        [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonGroupStateCtrl;
        [AutoBind("./PrivilegeRoot/DailyRewardRoot/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect DailyRewardItemsScrollView;
        [AutoBind("./PrivilegeRoot/PrivilegeItemGroupRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform PrivilegeItemRoot;
        [AutoBind("./PrivilegeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PrivilegeStateCtrl;
        [AutoBind("./ImmediateReward/RewardValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ImmediateRewardValueText;
        [AutoBind("./ImmediateReward/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ImmediateRewardIconImage;
        [AutoBind("./TotalReward/RewardValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TotalRewardValueText;
        [AutoBind("./TotalReward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TotalReward;
        [AutoBind("./TotalReward/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TotalRewardIconImage;
        [AutoBind("./CardTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CardTitleText;
        [AutoBind("./DurationTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DurationTimeText;
        [AutoBind("./TemplateRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform TemplateRoot;
        [AutoBind("./ButtonGroup/RemainingTimeText/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RemainTimeText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool MonthCardItemEasyObjectPool;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Image BgImage;
        [AutoBind("./RightItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform RightItemSimpleInfoDummy;
        [AutoBind("./LeftItemSimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LeftItemSimpleInfoDummy;
        [AutoBind("./ExplainTipRoot/ExplainTipDetailDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform PrivilegeDescTipRoot;
        [AutoBind("./ImmediateReward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ImmediateRewardRoot;
        private static DelegateBridge __Hotfix_UpdateMonthCardItem;
        private static DelegateBridge __Hotfix_UpdateButtonGroupState;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateTemplateItem;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolDailyRewardItemCreated;
        private static DelegateBridge __Hotfix_OnEasyPoolPrivilegeItemCreated;
        private static DelegateBridge __Hotfix_OnDailyRewardItemClick;
        private static DelegateBridge __Hotfix_OnDailyRewardItemFill;
        private static DelegateBridge __Hotfix_UpdateCardPrivilegePanel;
        private static DelegateBridge __Hotfix_UpdateCardPrivilegeItemList;
        private static DelegateBridge __Hotfix_UpdateDailyRewardItemList;
        private static DelegateBridge __Hotfix_UpdateMonthCardLeftTime;
        private static DelegateBridge __Hotfix_OnMonthCardBuyButtonClick;
        private static DelegateBridge __Hotfix_OnMonthCardDailyRewardButtonClick;
        private static DelegateBridge __Hotfix_OnMonthCardPrivilegeDescTipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMonthCardBuyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMonthCardBuyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMonthCardDailyRewardButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMonthCardDailyRewardButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMonthCardDailyRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMonthCardDailyRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnPrivilegeDescTipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPrivilegeDescTipButtonClick;
        private static DelegateBridge __Hotfix_get_LBMonthCardClient;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LeftHourStr;
        private static DelegateBridge __Hotfix_set_LeftHourStr;
        private static DelegateBridge __Hotfix_get_LeftDayStr;
        private static DelegateBridge __Hotfix_set_LeftDayStr;

        public event Action<int> EventOnMonthCardBuyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnMonthCardDailyRewardButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, ILBStoreItemClient> EventOnMonthCardDailyRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnPrivilegeDescTipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CreateTemplateItem()
        {
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDailyRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDailyRewardItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolDailyRewardItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolPrivilegeItemCreated(GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardBuyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardDailyRewardButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardPrivilegeDescTipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateButtonGroupState()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCardPrivilegeItemList()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCardPrivilegePanel()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDailyRewardItemList()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMonthCardItem(int index, LBRechargeMonthlyCard monthCardInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMonthCardLeftTime()
        {
        }

        private LogicBlockRechargeMonthlyCardClient LBMonthCardClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private static string LeftHourStr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private static string LeftDayStr
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

