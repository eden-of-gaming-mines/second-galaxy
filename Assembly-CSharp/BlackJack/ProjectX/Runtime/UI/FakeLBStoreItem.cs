﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class FakeLBStoreItem : ILBStoreItemClient, ILBItem
    {
        protected StoreItemType m_itemType;
        protected object m_configInfo;
        protected int m_configId;
        protected long m_count;
        protected bool m_isBind;
        protected bool m_isFreezing;
        protected ulong m_instanceId;
        protected int m_storeItemIndex;
        private static DelegateBridge _c__Hotfix_ctor_2;
        private static DelegateBridge _c__Hotfix_ctor_1;
        private static DelegateBridge _c__Hotfix_ctor_0;
        private static DelegateBridge __Hotfix_GetItemType;
        private static DelegateBridge __Hotfix_GetConfgId;
        private static DelegateBridge __Hotfix_GetConfigInfo;
        private static DelegateBridge __Hotfix_GetInstanceId;
        private static DelegateBridge __Hotfix_SetInstanceId;
        private static DelegateBridge __Hotfix_GetItemCount;
        private static DelegateBridge __Hotfix_SetItemCount;
        private static DelegateBridge __Hotfix_GetStoreItemIndex;
        private static DelegateBridge __Hotfix_SetStoreItemIndex;
        private static DelegateBridge __Hotfix_IsBind;
        private static DelegateBridge __Hotfix_IsFreezing;

        [MethodImpl(0x8000)]
        public FakeLBStoreItem(ItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem(StoreItemType itemType, int itemId, bool isBind, bool isFreezing = false, long itemCount = 1L)
        {
        }

        [MethodImpl(0x8000)]
        public FakeLBStoreItem(object confInfo, StoreItemType itemType, bool isBind, bool isFreezing = false, long itemCount = 1L)
        {
        }

        [MethodImpl(0x8000)]
        public int GetConfgId()
        {
        }

        [MethodImpl(0x8000)]
        public T GetConfigInfo<T>() where T: class
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInstanceId()
        {
        }

        [MethodImpl(0x8000)]
        public long GetItemCount()
        {
        }

        [MethodImpl(0x8000)]
        public StoreItemType GetItemType()
        {
        }

        [MethodImpl(0x8000)]
        public int GetStoreItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsBind()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFreezing()
        {
        }

        [MethodImpl(0x8000)]
        public void SetInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemCount(long count)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStoreItemIndex(int index)
        {
        }
    }
}

