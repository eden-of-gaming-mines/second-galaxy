﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMemberItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        public ScrollItemBaseUIController m_scrollItemBase;
        public CommonCaptainIconUIController m_headCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_state;
        [AutoBind("./Grade", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_textGrade;
        [AutoBind("./Contribution", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_textContribution;
        [AutoBind("./Galaxy", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_textGalaxy;
        [AutoBind("./Name", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_textName;
        [AutoBind("./State", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_textState;
        [AutoBind("./CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_headDummy;
        [AutoBind("./CommonCaptainDummy/CommonCaptainUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_headObj;
        [AutoBind("./ContributionRankingImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_contributionRankImage;
        [AutoBind("./JobGroup/Job1", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_imageJob1;
        [AutoBind("./JobGroup/Job2", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_imageJob2;
        [AutoBind("./JobGroup/Job3", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_imageJob3;
        public List<Image> m_jobImages;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_SetOnLineState;
        private static DelegateBridge __Hotfix_SetGuildJob;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildJob(List<GuildJobType> jobs, Dictionary<string, UnityEngine.Object> resData)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetOnLineState(bool isOnLine, DateTime lastOnlineTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(GuildMemberInfo info, bool isSelf, GuildMemberInfo[] mostThreeContribute, Dictionary<string, UnityEngine.Object> resData)
        {
        }
    }
}

