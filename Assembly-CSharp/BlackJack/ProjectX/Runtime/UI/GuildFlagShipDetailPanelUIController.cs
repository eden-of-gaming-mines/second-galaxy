﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipDetailPanelUIController : UIControllerBase
    {
        private string m_currMode;
        private const string TextMode = "DetailPanel";
        private const string CaptainMode = "SpiderDiagramPanel";
        private const string CaptainStateNone = "RegisterCaptain";
        private const string CaptainStateSelf = "UnRegisterCaptainSelf";
        private const string CaptainStateOther = "UnRegisterCaptainOther";
        private const string ShipStateInSpace = "InSpace";
        private const string ShipStateInStation = "InStation";
        private const string ShipStateUnpacking = "Unpacking";
        private const string CaptainUIAssetName = "CaptainItem";
        private bool isUnpacking;
        private CommonCaptainIconUIController m_registerCaptainIconUICtrl;
        [AutoBind("./CaptainDetail/BottonImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CaptainStateCtrl;
        [AutoBind("./CaptainDetail/BarGroup/BarNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EnergyValueText;
        [AutoBind("./CaptainDetail/BarGroup/BarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image EnergyProcessImage;
        [AutoBind("./CaptainDetail/ButtonGroup/UnregisterOtherButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnregisterOtherButton;
        [AutoBind("./CaptainDetail/ButtonGroup/UnresgisterSelfButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnresgisterSelfButton;
        [AutoBind("./CaptainDetail/ButtonGroup/StarMapButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StarMapButton;
        [AutoBind("./CaptainDetail/StateTitle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FlagShipStateUICtrl;
        [AutoBind("./CaptainDetail/ButtonGroup/RegisterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RegisterButton;
        [AutoBind("./CaptainDetail/ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ButtonGroupCommonUIStateController;
        [AutoBind("./CaptainDetail/LocationTitleText/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LocationText;
        [AutoBind("./CaptainDetail/LocationTitleText/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public Button LocationButton;
        [AutoBind("./CaptainDetail/CaptainItem/GuildNamerText", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildNamerText;
        [AutoBind("./CaptainDetail/CaptainItem/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainNameText;
        [AutoBind("./CaptainDetail/CaptainItem/CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform CaptainDummy;
        [AutoBind("./ShipDetail/ValueTexts/AttackValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AttackValueText;
        [AutoBind("./ShipDetail/ValueTexts/MaxRangeValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MaxRangeValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldValueText;
        [AutoBind("./ShipDetail/ValueTexts/ArmorValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmorValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldElectricValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldElectricValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldKineticValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldKineticValueText;
        [AutoBind("./ShipDetail/ValueTexts/ShieldHeatValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldHeatValueText;
        [AutoBind("./ShipDetail/ValueTexts/VolumnValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text VolumnValueText;
        [AutoBind("./ShipDetail/ValueTexts/JumpSteadyValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text JumpSteadyValueText;
        [AutoBind("./ShipDetail/ValueTexts/SpeedValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SpeedValueText;
        [AutoBind("./ShipDetail/ValueTexts/DepotValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DepotValueText;
        [AutoBind("./ShipDetail/ValueTexts/ElectricValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElectricValueText;
        [AutoBind("./ShipDetail/ShieldDefense/ElectricDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldElectricProgBar;
        [AutoBind("./ShipDetail/ShieldDefense/KineticDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldKineticProgBar;
        [AutoBind("./ShipDetail/ShieldDefense/HeatDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShieldHeatProgBar;
        [AutoBind("./ShipDetail/ArmorDefense/ElectricDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorElectricProgBar;
        [AutoBind("./ShipDetail/ArmorDefense/KineticDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorKineticProgBar;
        [AutoBind("./ShipDetail/ArmorDefense/HeatDF/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ArmorHeatProgBar;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateController;
        [AutoBind("./TitleIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TitleIconButton;
        private static DelegateBridge __Hotfix_UpdateFlagShipDetailInfo;
        private static DelegateBridge __Hotfix_IsCurrShipUnpacking;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateTextPanelInfo;
        private static DelegateBridge __Hotfix_UpdateCaptainPanelInfo;
        private static DelegateBridge __Hotfix_OnTitleIconButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public bool IsCurrShipUnpacking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTitleIconButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCaptainPanelInfo(ILBStaticFlagShip shipInfo, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateFlagShipDetailInfo(ILBStaticFlagShip ship, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTextPanelInfo(ILBStaticFlagShip shipInfo)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

