﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class TradeUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnOpenShopButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int, int> EventOnGotoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TradeUITaskTabType> EventOnTabChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnDropdownChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnDropdownItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnTradeFilterRootBtnClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildTradePurchaseInfo> EventOnTransportButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnRegionButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBgImageClick;
        private bool m_responseToUIEvent;
        private int m_curSelectedDropDownItem;
        private ConfigDataNormalItemInfo m_curPersonalTradeItemInfoConfig;
        private ConfigDataNormalItemInfo m_curGuildTradeItemInfoConfig;
        private TradeListItemUpdateType m_curTradeListItemUpdateType;
        private List<TradeListItemSellInfo> m_personalTradeItemSellersInfo;
        private List<TradeListItemInfo> m_personalTradeItemPurchasersInfo;
        private List<GuildTradePurchaseInfo> m_guildTradeItemPurchasersInfo;
        private Dictionary<int, TradeUITask.TradePortJumNumInfo> m_nearestPortDic;
        private Dictionary<string, UnityEngine.Object> m_assetDict;
        private Dictionary<int, TradeUITask.TradeFilterInfo> m_cachedFilterInfoDict;
        [AutoBind("./DropDownBGBtn", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGBtn;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TitleTextUIStateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BGImages/ShopButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ExchangeShopButton;
        [AutoBind("./TitleToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ToggleGroupUIStateCtrl;
        [AutoBind("./RightButtonDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DropdownRootBtn;
        [AutoBind("./RightButtonDropdown/Label", AutoBindAttribute.InitState.NotInit, false)]
        public Text DropdownRootText;
        [AutoBind("./RightButtonDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DropdownRootUIStateCtrl;
        [AutoBind("./RightButtonDropdown/Template", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect DropdownScrollRect;
        [AutoBind("./RightButtonDropdown/Template", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool DropdownEasyPool;
        [AutoBind("./RightButtonDropdown/Template/Item", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DropdownItem;
        [AutoBind("./TradePanel/LeftToggleGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup LeafMenuItemToggleGroup;
        [AutoBind("./TradePanel/ItemGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool TradeListItemPool;
        [AutoBind("./TradePanel/ItemGroup/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        [AutoBind("./TradePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TradePanelUIStateCtrl;
        [AutoBind("./TradePanel/ItemGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect TradeListScrollRect;
        [AutoBind("./TitleToggleGroup/PurchaseToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle PurchaseToggle;
        [AutoBind("./TitleToggleGroup/SellToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle SellToggle;
        [AutoBind("./TitleToggleGroup/SellContrabandToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle IlLegalToggle;
        [AutoBind("./PersonalEmptyImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PersonalEmptyStateCtrl;
        [AutoBind("./GuildEmptyImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildEmptyStateCtrl;
        [AutoBind("./PersonalEmptyImage/EmptyTimeText03", AutoBindAttribute.InitState.NotInit, false)]
        public Text IlLegalCountDownText;
        [AutoBind("./TitleToggleGroup/GuildToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle GuildToggle;
        [AutoBind("./TradePanel/ItemGroup/IntroduceTitleGroup/IconNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeItemNameText;
        [AutoBind("./TradePanel/ItemGroup/IntroduceTitleGroup/Scroll View/Viewport/Content/IntroduceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeItemIntroduceText;
        [AutoBind("./TradePanel/ItemGroup/IntroduceTitleGroup/Scroll View/Viewport/Content/PlaceOfOriginText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeItemPlaceOfOriginText;
        [AutoBind("./TradePanel/ItemGroup/IntroduceTitleGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TradeItemIconImage;
        [AutoBind("./TradePanel/ItemGroup/IntroduceTitleGroup/ExistingText/ExistingNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TradeItemOwnNumText;
        [AutoBind("./TradePanel/ItemGroup/IntroduceTitleGroup/QualityImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TradeItemIconQualityUIStatCtrl;
        [AutoBind("./BGImages/PersonalTradeTitleText/HelpIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public SystemDescriptionController m_personalSystemDescriptionCtrl;
        [AutoBind("./BGImages/GuildTradeTitleText/HelpIconButton", AutoBindAttribute.InitState.NotInit, false)]
        public SystemDescriptionController m_guildSystemDescriptionCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TradeUIStateCtrl;
        private GameObject m_itemTemplateGo;
        private const string DropdownItemName = "DropdownItem";
        private const string TradeListItemPoolName = "TradeListItem";
        private const string TradeListItemAssetName = "TradeListItem";
        private const string TradeItemMenuItemName = "MenuItem";
        private static DelegateBridge __Hotfix_EnableUIResponse;
        private static DelegateBridge __Hotfix_SetTradePanelState;
        private static DelegateBridge __Hotfix_SetTab;
        private static DelegateBridge __Hotfix_ClearTradeList;
        private static DelegateBridge __Hotfix_ClearEmptyGroup;
        private static DelegateBridge __Hotfix_ClearTradeShopButton;
        private static DelegateBridge __Hotfix_UpdatePersonalTradeListForSell;
        private static DelegateBridge __Hotfix_UpdatePersonalTradeListForPurchase;
        private static DelegateBridge __Hotfix_ShowOrHideRefreshTimePanel;
        private static DelegateBridge __Hotfix_UpdateGuildTradeListForPurchase;
        private static DelegateBridge __Hotfix_UpdateDropDownFatherItem;
        private static DelegateBridge __Hotfix_UpdateDropdownMenu;
        private static DelegateBridge __Hotfix_UnExpandDropdownMenu;
        private static DelegateBridge __Hotfix_UpdateSingleDropdownItem;
        private static DelegateBridge __Hotfix_ClearAndHideDropdownMenu;
        private static DelegateBridge __Hotfix_GetFristTradeItem;
        private static DelegateBridge __Hotfix_GetRootUIShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetTradePanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_GetToggleGroupProcess;
        private static DelegateBridge __Hotfix_UpdateTitle;
        private static DelegateBridge __Hotfix_UpdateToggleGroup;
        private static DelegateBridge __Hotfix_UpdateTradeItemInfoInUI;
        private static DelegateBridge __Hotfix_CreateTradeListPool;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnTradeListItemFill;
        private static DelegateBridge __Hotfix_OnGotoButtonClick;
        private static DelegateBridge __Hotfix_OnOpenShopButtonClick;
        private static DelegateBridge __Hotfix_OnTransportButtonClick;
        private static DelegateBridge __Hotfix_OnRegionButtonClick;
        private static DelegateBridge __Hotfix_EventOnPoolObjectReturnPool;
        private static DelegateBridge __Hotfix_EventOnPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnSellToggleValueChange;
        private static DelegateBridge __Hotfix_OnIlLegalToggleValueChange;
        private static DelegateBridge __Hotfix_OnPurchaseToggleValueChange;
        private static DelegateBridge __Hotfix_OnGuildToggleValueChange;
        private static DelegateBridge __Hotfix_OnDropdownValueChanged;
        private static DelegateBridge __Hotfix_OnDropDownRootBtnClick;
        private static DelegateBridge __Hotfix_OnBgImageClick;
        private static DelegateBridge __Hotfix_OnDropdownItemNeedFill;
        private static DelegateBridge __Hotfix_OnDropdownItemClick;
        private static DelegateBridge __Hotfix_RegisterUIEvent;
        private static DelegateBridge __Hotfix_UnRegisterUIEvent;
        private static DelegateBridge __Hotfix_add_EventOnOpenShopButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnOpenShopButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGotoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTabChanged;
        private static DelegateBridge __Hotfix_remove_EventOnTabChanged;
        private static DelegateBridge __Hotfix_add_EventOnDropdownChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDropdownChanged;
        private static DelegateBridge __Hotfix_add_EventOnDropdownItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDropdownItemClick;
        private static DelegateBridge __Hotfix_add_EventOnTradeFilterRootBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnTradeFilterRootBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnTransportButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTransportButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRegionButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnRegionButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBgImageClick;
        private static DelegateBridge __Hotfix_remove_EventOnBgImageClick;

        public event Action EventOnBgImageClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnDropdownChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnDropdownItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int, int> EventOnGotoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnOpenShopButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnRegionButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TradeUITaskTabType> EventOnTabChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTradeFilterRootBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildTradePurchaseInfo> EventOnTransportButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearAndHideDropdownMenu()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearEmptyGroup()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearTradeList()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearTradeShopButton()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateTradeListPool(int poolSize)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableUIResponse(bool value)
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnPoolObjectCreated(string poolInfo, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void EventOnPoolObjectReturnPool(string poolInfo, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetFristTradeItem()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetRootUIShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetToggleGroupProcess(bool isShow, string mode)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetTradePanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgImageClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownItemNeedFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropDownRootBtnClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownValueChanged(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoButtonClick(int solarSystemId, int stationId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildToggleValueChange(bool value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIlLegalToggleValueChange(bool value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenShopButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPurchaseToggleValueChange(bool value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRegionButtonClick(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellToggleValueChange(bool value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeListItemFill(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransportButtonClick(GuildTradePurchaseInfo guildTradePurchaseInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTab(TradeUITaskTabType tabType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTradePanelState(bool isShow, bool isImmeditate)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideRefreshTimePanel(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void UnExpandDropdownMenu()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDropDownFatherItem(TradeUITask.TradeFilterInfo filterInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDropdownMenu(Dictionary<int, TradeUITask.TradeFilterInfo> dropDownItemDict, int curSelectIndex, bool isExpand)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildTradeListForPurchase(ConfigDataNormalItemInfo itemConfig, List<GuildTradePurchaseInfo> tradeListItemInfos, Dictionary<int, TradeUITask.TradePortJumNumInfo> nearestPortDic, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePersonalTradeListForPurchase(ConfigDataNormalItemInfo itemConfig, List<TradeListItemInfo> tradeListItemInfos, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePersonalTradeListForSell(ConfigDataNormalItemInfo itemConfig, List<TradeListItemSellInfo> tradeNpcConfigs, Dictionary<string, UnityEngine.Object> assetDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSingleDropdownItem(TradeUITask.TradeFilterInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTitle(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateToggleGroup(string mode)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTradeItemInfoInUI(ConfigDataNormalItemInfo itemConfig, bool isGuildTradeItem = false)
        {
        }

        private enum TradeListItemUpdateType
        {
            PersonalTradeItemSell,
            PersonalTradeItemPurchase,
            GuildTradeItemPurchase
        }
    }
}

