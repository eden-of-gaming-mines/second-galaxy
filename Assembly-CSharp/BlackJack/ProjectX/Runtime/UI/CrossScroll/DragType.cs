﻿namespace BlackJack.ProjectX.Runtime.UI.CrossScroll
{
    using System;

    public enum DragType
    {
        None,
        Horizontal,
        Vertical,
        CenterHor,
        CenterVer
    }
}

