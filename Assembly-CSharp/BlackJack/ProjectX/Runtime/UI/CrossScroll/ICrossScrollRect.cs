﻿namespace BlackJack.ProjectX.Runtime.UI.CrossScroll
{
    using System;
    using UnityEngine.EventSystems;

    public interface ICrossScrollRect : IBeginDragHandler, IEndDragHandler, IDragHandler, IInitializePotentialDragHandler, IEventSystemHandler
    {
        void Init(CrossScrollRectType type);
        void RegisterOnBeginDragEvent(Action<PointerEventData> beginDrag);
        void RegisterOnDragEvent(Action<PointerEventData> drag);
        void RegisterOnEndDragEvent(Action<PointerEventData> endDrag);
        void RegisterOnInitializePotentialDrag(Action<PointerEventData> initDrag);
        void SetTriggerType(CrossScrollRectType triggerType);
        void StopMovement();
        void UnregisterOnBeginDragEvent(Action<PointerEventData> beginDrag);
        void UnregisterOnDragEvent(Action<PointerEventData> drag);
        void UnregisterOnEndDragEvent(Action<PointerEventData> endDrag);
        void UnregisterOnInitializePotentialDrag(Action<PointerEventData> initDrag);

        bool IsMoveX { get; set; }

        bool IsCalDragCallback { get; set; }
    }
}

