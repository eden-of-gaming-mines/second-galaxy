﻿namespace BlackJack.ProjectX.Runtime.UI.CrossScroll
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class CrossScrollController : MonoBehaviour
    {
        public ICrossScrollRect m_horSr;
        public ICrossScrollRect m_verSr;
        public ICrossScrollRect m_centerSr;
        private DragType m_curDragType;
        private static DelegateBridge __Hotfix_OnInit;
        private static DelegateBridge __Hotfix_OnHorInitializePotentialDrag;
        private static DelegateBridge __Hotfix_OnVerInitializePotentialDrag;
        private static DelegateBridge __Hotfix_OnCenterInitializePotentialDrag;
        private static DelegateBridge __Hotfix_OnHorizontalBeginDrag;
        private static DelegateBridge __Hotfix_OnVerticalBeginDrag;
        private static DelegateBridge __Hotfix_OnCenterBeginDrag;
        private static DelegateBridge __Hotfix_OnVerticalDrag;
        private static DelegateBridge __Hotfix_OnHorizontalDrag;
        private static DelegateBridge __Hotfix_OnCenterDrag;
        private static DelegateBridge __Hotfix_OnDrag;
        private static DelegateBridge __Hotfix_OnCenterEndDrag;
        private static DelegateBridge __Hotfix_OnHorEndDrag;
        private static DelegateBridge __Hotfix_OnVerEndDrag;

        [MethodImpl(0x8000)]
        private void OnCenterBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCenterDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCenterEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCenterInitializePotentialDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHorEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHorInitializePotentialDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHorizontalBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHorizontalDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void OnInit(ICrossScrollRect horSr, ICrossScrollRect verSr, ICrossScrollRect centerSr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnVerEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnVerInitializePotentialDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnVerticalBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        private void OnVerticalDrag(PointerEventData eventData)
        {
        }
    }
}

