﻿namespace BlackJack.ProjectX.Runtime.UI.CrossScroll
{
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class CrossHorScrollRect : ScrollRect, ICrossScrollRect, IBeginDragHandler, IEndDragHandler, IDragHandler, IInitializePotentialDragHandler, IEventSystemHandler
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsMoveX>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsCalDragCallback>k__BackingField;
        private CrossScrollRectType m_type;
        private CrossScrollRectType m_triggerType;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<PointerEventData> EventOnInitializePotentialDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData> EventOnBeginDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData> EventOnEndDrag;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<PointerEventData> EventOnDrag;
        private static DelegateBridge __Hotfix_SetTriggerType;
        private static DelegateBridge __Hotfix_get_IsMoveX;
        private static DelegateBridge __Hotfix_set_IsMoveX;
        private static DelegateBridge __Hotfix_get_IsCalDragCallback;
        private static DelegateBridge __Hotfix_set_IsCalDragCallback;
        private static DelegateBridge __Hotfix_OnInitializePotentialDrag;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_OnDrag;
        private static DelegateBridge __Hotfix_OnEndDrag;
        private static DelegateBridge __Hotfix_RegisterOnInitializePotentialDrag;
        private static DelegateBridge __Hotfix_RegisterOnBeginDragEvent;
        private static DelegateBridge __Hotfix_RegisterOnDragEvent;
        private static DelegateBridge __Hotfix_RegisterOnEndDragEvent;
        private static DelegateBridge __Hotfix_UnregisterOnInitializePotentialDrag;
        private static DelegateBridge __Hotfix_UnregisterOnBeginDragEvent;
        private static DelegateBridge __Hotfix_UnregisterOnDragEvent;
        private static DelegateBridge __Hotfix_UnregisterOnEndDragEvent;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_add_EventOnInitializePotentialDrag;
        private static DelegateBridge __Hotfix_remove_EventOnInitializePotentialDrag;
        private static DelegateBridge __Hotfix_add_EventOnBeginDrag;
        private static DelegateBridge __Hotfix_remove_EventOnBeginDrag;
        private static DelegateBridge __Hotfix_add_EventOnEndDrag;
        private static DelegateBridge __Hotfix_remove_EventOnEndDrag;
        private static DelegateBridge __Hotfix_add_EventOnDrag;
        private static DelegateBridge __Hotfix_remove_EventOnDrag;

        public event Action<PointerEventData> EventOnBeginDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData> EventOnDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData> EventOnEndDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<PointerEventData> EventOnInitializePotentialDrag
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init(CrossScrollRectType type)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnBeginDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnEndDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public override void OnInitializePotentialDrag(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterOnBeginDragEvent(Action<PointerEventData> beginDrag)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterOnDragEvent(Action<PointerEventData> drag)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterOnEndDragEvent(Action<PointerEventData> endDrag)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterOnInitializePotentialDrag(Action<PointerEventData> initDrag)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTriggerType(CrossScrollRectType triggerType)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterOnBeginDragEvent(Action<PointerEventData> beginDrag)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterOnDragEvent(Action<PointerEventData> drag)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterOnEndDragEvent(Action<PointerEventData> endDrag)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterOnInitializePotentialDrag(Action<PointerEventData> initDrag)
        {
        }

        public bool IsMoveX
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsCalDragCallback
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

