﻿namespace BlackJack.ProjectX.Runtime.UI.CrossScroll
{
    using System;

    public enum CrossScrollRectType
    {
        Horizontal,
        Center,
        Vertical
    }
}

