﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class CGPlayerUITask : UITaskBase
    {
        private Dictionary<string, string> m_videoSubPath2BundleDict;
        private static CGLogicType curCGLogicType;
        private string m_CGResPath;
        private Action m_endAction;
        private CGPlayerUIController m_mainCtrl;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static string CGResPath;
        private static string EndAction;
        private static string TaskName;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartPlayCG;
        private static DelegateBridge __Hotfix_StopPlayCG;
        private static DelegateBridge __Hotfix_PrintSDKLog;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_GetCRIResourcePath;
        private static DelegateBridge __Hotfix_OnBackgrondButtoneClick;
        private static DelegateBridge __Hotfix_OnBackgrondButtoneDoubleClick;
        private static DelegateBridge __Hotfix_OnCGEnd;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CGPlayerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected string GetCRIResourcePath(string basePath)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackgrondButtoneClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackgrondButtoneDoubleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCGEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private static void PrintSDKLog()
        {
        }

        [MethodImpl(0x8000)]
        public static CGPlayerUITask StartPlayCG(string cgResPath, Action endAction, CGLogicType cgType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void StopPlayCG()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum CGLogicType
        {
            Prelude = 11,
            Chapter1 = 1,
            Chapter2 = 2,
            Chapter3 = 3,
            Chapter4 = 4,
            Chapter5 = 5,
            Chapter6 = 6,
            Chapter7 = 7,
            Chapter8 = 8,
            Chapter9 = 9,
            None = 0
        }
    }
}

