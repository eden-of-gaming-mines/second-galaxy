﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class OpenGuildFleetDetailInfoPanelNetTask : NetWorkTransactionTask
    {
        private readonly ulong m_fleetId;
        private bool m_isGuildFleetDetailInfoAck;
        private bool m_isGuildMemberListAck;
        private bool m_isPlayerGuildFleetSettingAck;
        private int m_fleetDetailResult;
        private int m_memberListResult;
        private int m_playerGuildFleetSettingResult;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildFleetDetailInfoAck;
        private static DelegateBridge __Hotfix_OnGuildMemberListInfoAck;
        private static DelegateBridge __Hotfix_OnPlayerGuildFleetSettingAck;
        private static DelegateBridge __Hotfix_IsNetTaskEnd;
        private static DelegateBridge __Hotfix_get_FleetDetailResult;
        private static DelegateBridge __Hotfix_get_MemberListResult;
        private static DelegateBridge __Hotfix_get_PlayerGuildFleetSettingResult;

        [MethodImpl(0x8000)]
        public OpenGuildFleetDetailInfoPanelNetTask(ulong fleetId)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNetTaskEnd()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFleetDetailInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMemberListInfoAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerGuildFleetSettingAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int FleetDetailResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int MemberListResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int PlayerGuildFleetSettingResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

