﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildActionCreateReq : NetWorkTransactionTask
    {
        private int m_configId;
        private int m_solarSystemId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private GuildActionCreateAck <ActionCreateAck>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnGuildActionCreateAck;
        private static DelegateBridge __Hotfix_set_ActionCreateAck;
        private static DelegateBridge __Hotfix_get_ActionCreateAck;

        [MethodImpl(0x8000)]
        public GuildActionCreateReq(int configId, int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildActionCreateAck(GuildActionCreateAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public GuildActionCreateAck ActionCreateAck
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

