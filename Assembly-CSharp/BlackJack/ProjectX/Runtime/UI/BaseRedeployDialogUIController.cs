﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class BaseRedeployDialogUIController : UIControllerBase
    {
        public BaseRedeployAccelerateConfirmDialogUIController m_baseRedeployAccelerateConfirmDialog;
        public BaseRedeployCancelRedeployDialogUIController m_baseRedeployCancelRedeployDialog;
        public BaseRedeployFleetRecallDialogUIController m_baseRedeployFleetRecallDialog;
        public BaseRedeploySpeedUpWithMoneyDialogUIController m_baseRedeploySpeedUpWithMoneyDialog;
        public BaseRedeployStartRedeployDialogUIController m_baseRedeployStartRedeployDialog;
        public BaseRedeployStopRedeployDialogUIController m_baseRedeployStopRedeployDialog;
        public BaseRedeployFleetRecallFailDialogUIController m_baseRedeployFleetRecallFailDialog;
        public BaseRedeployFinishRedeployDialogUIController m_baseRedeployFinishRedeployDialog;
        private BaseRedeployDialogUIControllerBase m_currActiveDialog;
        [AutoBind("./AccelerateConfirmPanel ", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeployAccelerateConfirmDialogRoot;
        [AutoBind("./CancelConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeployCancelRedeployDialogRoot;
        [AutoBind("./StopConfirmPanel ", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeployStopRedeployDialogRoot;
        [AutoBind("./FleetRecallConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeployFleetRecallDialogRoot;
        [AutoBind("./StartConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeployStartRedeployDialogRoot;
        [AutoBind("./SpeedUpWithMoneyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeploySpeedUpWithMoneyDialogRoot;
        [AutoBind("./FleetRecallFailPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeployFleetRecallFailDialogRoot;
        [AutoBind("./FinishConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_baseRedeployFinishRedeployDialogRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_HideAllDialog;
        private static DelegateBridge __Hotfix_GetShowProcess;
        private static DelegateBridge __Hotfix_HideDialog;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetShowProcess(BaseRedeployDialogUIControllerBase dialog)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess HideAllDialog()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess HideDialog(bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }
    }
}

