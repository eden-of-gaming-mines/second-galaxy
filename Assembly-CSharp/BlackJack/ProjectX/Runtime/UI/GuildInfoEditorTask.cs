﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildInfoEditorTask : GuildUITaskBase
    {
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public DateTime m_dismissEndTime;
        private GuildInfoEditorController m_controller;
        private static readonly string m_resPath = "Assets/GameProject/RuntimeAssets/UI/Prefabs/Guild_ABS/GuildInfoEditorUIPrefab.prefab";
        private GuildClient m_playerGuild;
        private const int NoPermission = -1807;
        private const string PanelStateOpen = "Show";
        private const string PanelStateClose = "Close";
        private float m_nextChangeAnnouncementTime;
        private float m_nextChangeManifestoTime;
        public const string TaskName = "GuildInfoEditorTask";
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_NotOpenMsg;
        private static DelegateBridge __Hotfix_OnSignBtnClick;
        private static DelegateBridge __Hotfix_OnWarButClick;
        private static DelegateBridge __Hotfix_OnMiningButClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnChangeNameClick;
        private static DelegateBridge __Hotfix_get_ChangeNameCostMoney;
        private static DelegateBridge __Hotfix_OnChangeRegionClick;
        private static DelegateBridge __Hotfix_OnChangeAnnouncementClick;
        private static DelegateBridge __Hotfix_OnChangeManifestoClick;
        private static DelegateBridge __Hotfix_OnQuitClick;
        private static DelegateBridge __Hotfix_OnDismissClick;
        private static DelegateBridge __Hotfix_OnUndismissClick;
        private static DelegateBridge __Hotfix_OnOpenMail;
        private static DelegateBridge __Hotfix_OnSwitchJoin;
        private static DelegateBridge __Hotfix_OnGuildCompensationSetAuto;
        private static DelegateBridge __Hotfix_OnGuildCompensationSetAutoNtf;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_InitPipeLineStateFromUIIntent;
        private static DelegateBridge __Hotfix_OpenTipGroup;
        private static DelegateBridge __Hotfix_CloseTipGroup;
        private static DelegateBridge __Hotfix_CheckDismissState;
        private static DelegateBridge __Hotfix_OnYesButtonClick;
        private static DelegateBridge __Hotfix_OnNoButtonClick;
        private static DelegateBridge __Hotfix_Submit;
        private static DelegateBridge __Hotfix_SendNetTask;
        private static DelegateBridge __Hotfix_OnGuildInfoEditNetTaskOk;
        private static DelegateBridge __Hotfix_ResetInput;
        private static DelegateBridge __Hotfix_JoinTypeSwitched;
        private static DelegateBridge __Hotfix_GuildQuit;
        private static DelegateBridge __Hotfix_Undismiss;
        private static DelegateBridge __Hotfix_CloseWindow;
        private static DelegateBridge __Hotfix_GoBack;
        private static DelegateBridge __Hotfix_UpdateViewForAllRedPoints;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_GetPlayerContext;

        [MethodImpl(0x8000)]
        public GuildInfoEditorTask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CheckDismissState()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseTipGroup(bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseWindow()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private ProjectXPlayerContext GetPlayerContext()
        {
        }

        [MethodImpl(0x8000)]
        private void GoBack()
        {
        }

        [MethodImpl(0x8000)]
        private void GuildQuit()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPipeLineStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void JoinTypeSwitched(bool isFree)
        {
        }

        [MethodImpl(0x8000)]
        private void NotOpenMsg(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangeAnnouncementClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangeManifestoClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangeNameClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChangeRegionClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDismissClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCompensationSetAuto(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCompensationSetAutoNtf(GuildCompensationSetAutoNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildInfoEditNetTaskOk(GuildInfoEditNetTask.GuildInfoEditAction action)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMiningButClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNoButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOpenMail(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuitClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSignBtnClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSwitchJoin(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUndismissClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWarButClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnYesButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OpenTipGroup(bool isImmediate = false)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void ResetInput()
        {
        }

        [MethodImpl(0x8000)]
        private void SendNetTask(GuildInfoEditNetTask netSendTask)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void Submit()
        {
        }

        [MethodImpl(0x8000)]
        private void Undismiss()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewForAllRedPoints()
        {
        }

        private int ChangeNameCostMoney
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnSignBtnClick>c__AnonStorey1
        {
            internal LogoHelper.LogoInfo editLogoInfo;
            internal GuildInfoEditorTask $this;

            internal void <>m__0()
            {
                LogoSetReqNetworkTask.Start(this.editLogoInfo, delegate (int result) {
                    if (result != 0)
                    {
                        if (result == -1827)
                        {
                            MsgBoxUITask.ShowMsgBox(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GotoBlackMarket_LackOfRealMoney, new object[0]), () => CommonUITaskHelper.ToAddCurrency(CurrencyType.CurrencyType_RealMoney, this.$this.CurrentIntent, delegate (bool ret) {
                                if (ret)
                                {
                                    this.$this.Pause();
                                }
                            }), null);
                        }
                        else
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        }
                    }
                });
            }

            internal void <>m__1(int result)
            {
                if (result != 0)
                {
                    if (result == -1827)
                    {
                        MsgBoxUITask.ShowMsgBox(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GotoBlackMarket_LackOfRealMoney, new object[0]), () => CommonUITaskHelper.ToAddCurrency(CurrencyType.CurrencyType_RealMoney, this.$this.CurrentIntent, delegate (bool ret) {
                            if (ret)
                            {
                                this.$this.Pause();
                            }
                        }), null);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                    }
                }
            }

            internal void <>m__2()
            {
                CommonUITaskHelper.ToAddCurrency(CurrencyType.CurrencyType_RealMoney, this.$this.CurrentIntent, delegate (bool ret) {
                    if (ret)
                    {
                        this.$this.Pause();
                    }
                });
            }

            internal void <>m__3(bool ret)
            {
                if (ret)
                {
                    this.$this.Pause();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(AllianceInfo info)
            {
            }
        }
    }
}

