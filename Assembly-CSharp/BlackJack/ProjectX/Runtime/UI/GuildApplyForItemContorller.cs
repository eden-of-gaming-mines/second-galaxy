﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuildApplyForItemContorller : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnRatifyBtnClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCancelBtnClick;
        public CommonCaptainIconUIController m_headCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollItemBaseUIController m_srollItem;
        [AutoBind("./RatifyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_ratifyBtn;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelBtn;
        [AutoBind("./Name", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_name;
        [AutoBind("./Score", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_score;
        [AutoBind("./RegTime", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_regTime;
        [AutoBind("./CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_headDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_SetRegTime;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_add_EventOnRatifyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnRatifyBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelBtnClick;

        public event Action<int> EventOnCancelBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnRatifyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRegTime(DateTime regTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(GuildJoinApplyViewInfo info, Dictionary<string, Object> resData)
        {
        }
    }
}

