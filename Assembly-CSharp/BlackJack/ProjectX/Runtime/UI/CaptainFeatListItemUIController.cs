﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainFeatListItemUIController : UIControllerBase
    {
        public ScrollItemBaseUIController ScrollCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        [AutoBind("./FeatGroup/Feat/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FeatIconImage;
        [AutoBind("./FeatGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatNameText;
        [AutoBind("./FeatGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatLevelText;
        [AutoBind("./FeatGroup/LvTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text FeatTypeText;
        [AutoBind("./PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropertyGroup;
        [AutoBind("./PropertyGroup/PropertyInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PropertyInfoItemProto;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetCaptainFeatInfo;
        private static DelegateBridge __Hotfix_GetPropertyInfoItem;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        private GameObject GetPropertyInfoItem(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainFeatInfo(LBNpcCaptainFeats featInfo, bool isInitFeat, bool isUseful, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }
    }
}

