﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceShipItemStoreUITask : ShipHangarItemStoreUITask
    {
        protected ClientSpaceShip m_currSpaceShip;
        public const string ParamKey_ClientSpaceShip = "ClientSpaceShip";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitMainUICtrl;
        private static DelegateBridge __Hotfix_InitMotherShipItemStoreUICtrl;
        private static DelegateBridge __Hotfix_InitSpaceShipItemStoreUICtrl;
        private static DelegateBridge __Hotfix_InitSelectItemInfoUICtrl;
        private static DelegateBridge __Hotfix_GetShipCompItemStoreClient;
        private static DelegateBridge __Hotfix_UpdateShipInfoDataCache;
        private static DelegateBridge __Hotfix_OnCloseButtonClickBacktoSpace;
        private static DelegateBridge __Hotfix_OnItemDestoryButtonClick;

        [MethodImpl(0x8000)]
        public SpaceShipItemStoreUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override LogicBlockShipCompItemStoreClient GetShipCompItemStoreClient()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitMainUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitMotherShipItemStoreUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSelectItemInfoUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitSpaceShipItemStoreUICtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClickBacktoSpace(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemDestoryButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateShipInfoDataCache()
        {
        }

        [CompilerGenerated]
        private sealed class <OnItemDestoryButtonClick>c__AnonStorey0
        {
            internal LBShipStoreItemClient selectItem;
            internal ItemSimpleInfoUIController itemSimpleInfoUICtrl;
            internal SpaceShipItemStoreUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(Task task)
            {
            }
        }
    }
}

