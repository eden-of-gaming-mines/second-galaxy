﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class DiplomacySearchTypeUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DiplomacyManagementUITask.SearchType> EventOnSearchTypeChanged;
        private const string StatePanelOpen = "PanelOpen";
        private const string StatePanelClose = "PanelClose";
        private bool m_isSearchTypePanelShow;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./Player", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_searchPlayerToggle;
        [AutoBind("./GuildCode", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_searchGuildCodeToggle;
        [AutoBind("./GuildName", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_searchGuildNameToggle;
        [AutoBind("./Alliance", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_searchAllianceToggle;
        [AutoBind("./Player/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchPlayerText;
        [AutoBind("./GuildCode/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchGuildCodeText;
        [AutoBind("./GuildName/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchGuildNameText;
        [AutoBind("./Alliance/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_searchAllianceText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetSearchTypePanelUIProcess;
        private static DelegateBridge __Hotfix_IsSearchTypePanelShow;
        private static DelegateBridge __Hotfix_OnPlayerToggleValueChange;
        private static DelegateBridge __Hotfix_OnGuildCodeToggleValueChange;
        private static DelegateBridge __Hotfix_OnGuildNameToggleValueChange;
        private static DelegateBridge __Hotfix_OnAllianceToggleValueChange;
        private static DelegateBridge __Hotfix_add_EventOnSearchTypeChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSearchTypeChanged;

        public event Action<DiplomacyManagementUITask.SearchType> EventOnSearchTypeChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetSearchTypePanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSearchTypePanelShow()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllianceToggleValueChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCodeToggleValueChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildNameToggleValueChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerToggleValueChange(UIControllerBase ctrl, bool isOn)
        {
        }
    }
}

