﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class AuctionItemBuyReqNetTask : NetWorkTransactionTask
    {
        protected int m_itemId;
        protected int m_itemCount;
        protected long m_cost;
        protected AuctionItemBuyAck m_ack;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAuctionItemBuyAck;
        private static DelegateBridge __Hotfix_get_Ack;

        [MethodImpl(0x8000)]
        public AuctionItemBuyReqNetTask(int itemId, int itemCount, long cost)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAuctionItemBuyAck(AuctionItemBuyAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public AuctionItemBuyAck Ack
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

