﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildActionQuestDetailUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnConfirmIssueBtnClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ulong> EventOnStrikeBtnClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnStarMapBtnClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnRewardItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLossWarningClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventShowIssueHintCtrlClick;
        private GuildActionInfo m_actionInfo;
        private ConfigDataGuildActionInfo m_actionData;
        private CommonRewardMoneyItemsUIController m_rewardMoneyCtrl;
        private AllowInShipGroupUIController m_allowInShipCtrl;
        private List<CommonItemIconUIController> m_itemCtrls;
        private Dictionary<ShipType, CommonUIStateController> m_shipLimitStates;
        private GameObject m_itemTemplateObj;
        private GuidActionIssueHintUIController m_issueHintCtrl;
        private float m_updateInterval;
        private float m_nextUpdateTime;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_state;
        [AutoBind("./TopImagePanel/ActionImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_solarSystemBkg;
        [AutoBind("./TopImagePanel/Description", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_description;
        [AutoBind("./TopImagePanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_actionNameText;
        [AutoBind("./Scroll View/Viewport/Content/DoubtlessRewardPanel/TitleImage/TimeBkg/LeftTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_leftTime;
        [AutoBind("./Scroll View/Viewport/Content/DoubtlessRewardPanel/TitleImage/TimeBkg/TimeTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeTitleText;
        [AutoBind("./TopImagePanel/DescriptionTitleGroup/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleCruiserState;
        [AutoBind("./TopImagePanel/DescriptionTitleGroup/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CruiserState;
        [AutoBind("./TopImagePanel/DescriptionTitleGroup/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DestroyerState;
        [AutoBind("./TopImagePanel/DescriptionTitleGroup/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FrigateState;
        [AutoBind("./TopImagePanel/DescriptionTitleGroup/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BattleShipState;
        [AutoBind("./TopImagePanel/DescriptionTitleGroup/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IndustryShipState;
        [AutoBind("./TopImagePanel/DescriptionTitleGroup/AllowInShipType", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AllowInShipButton;
        [AutoBind("./AllowShipTipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform AllowShipTipDummy;
        [AutoBind("./TopImagePanel/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LossWarningState;
        [AutoBind("./TopImagePanel/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_lossWarningButton;
        [AutoBind("./LossWarningDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_lossWarningDummy;
        [AutoBind("./Scroll View/Viewport/Content/DoubtlessRewardPanel/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardMoneyCtrlObj;
        [AutoBind("./Scroll View/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemTemplateRoot;
        [AutoBind("./Scroll View/Viewport/Content/PossibleRewardPanel/BGImage/PossibleMoneyGroup/MoneyReward1/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_currencyImage;
        [AutoBind("./Scroll View/Viewport/Content/PossibleRewardPanel/BGImage/PossibleMoneyGroup/MoneyReward1/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currencyText;
        [AutoBind("./Scroll View/Viewport/Content/PossibleRewardPanel/BGImage/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardItemRoot;
        [AutoBind("./IssueHintRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_issueHintRoot;
        [AutoBind("./ButtonStateGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buttonGroupStates;
        [AutoBind("./ButtonStateGroup/StrikeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_strikeButton;
        [AutoBind("./ButtonStateGroup/StrikeButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_strikeButtonText;
        [AutoBind("./ButtonStateGroup/IssueButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_issueButton;
        [AutoBind("./ButtonStateGroup/IssueButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_issueBtnState;
        [AutoBind("./ButtonStateGroup/IssueButtonGroup/ConfirmButton/ConfirmText/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_costIncreaseImage;
        [AutoBind("./ButtonStateGroup/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_detailButtonState;
        [AutoBind("./ButtonStateGroup/DetailButton/DealParticularsButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_detailButton;
        [AutoBind("./ButtonStateGroup/DetailButton/DetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_detailState;
        [AutoBind("./ButtonStateGroup/DetailButton/DetailInfoPanel/CloseFrameBkgBtn", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_closeFrameBkgBtn;
        [AutoBind("./ButtonStateGroup/DetailButton/DetailInfoPanel/FrameImage/BGImage/DetailInfo/TitleGroup/FixedGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_detailIconImage;
        [AutoBind("./ButtonStateGroup/DetailButton/DetailInfoPanel/FrameImage/BGImage/DetailInfo/TitleGroup/FixedGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_detailNumerText;
        [AutoBind("./ButtonStateGroup/DetailButton/DetailInfoPanel/FrameImage/BGImage/DetailInfo/TitleGroup/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_detailDesText;
        [AutoBind("./ButtonStateGroup/IssueButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmIssueButton;
        [AutoBind("./ButtonStateGroup/IssueButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmIssueButtonState;
        [AutoBind("./ButtonStateGroup/IssueButtonGroup/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_issueCostText;
        [AutoBind("./DistanceGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_galaxyNameText;
        [AutoBind("./DistanceGroup/DistanceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_distanceValueText;
        [AutoBind("./DistanceGroup/StarMapButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_starMapButton;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_ShowIssueHint;
        private static DelegateBridge __Hotfix_UpdateView_0;
        private static DelegateBridge __Hotfix_UpdateView_1;
        private static DelegateBridge __Hotfix_UpdateViewCommon;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetQuestDistance;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnDetailButtonClick;
        private static DelegateBridge __Hotfix_OnStrikeButtonClick;
        private static DelegateBridge __Hotfix_OnIssueButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmIssueButtonClick;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnLossWarningClick;
        private static DelegateBridge __Hotfix_OnAllowInShipClick;
        private static DelegateBridge __Hotfix_OnAllowInShipTipBGClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmIssueBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmIssueBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnStrikeBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnStrikeBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnStarMapBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnStarMapBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningClick;
        private static DelegateBridge __Hotfix_add_EventShowIssueHintCtrlClick;
        private static DelegateBridge __Hotfix_remove_EventShowIssueHintCtrlClick;

        public event Action<int> EventOnConfirmIssueBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLossWarningClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnStarMapBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnStrikeBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventShowIssueHintCtrlClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private string GetQuestDistance(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipTipBGClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmIssueButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDetailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossWarningClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStrikeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Show(bool show, bool immediate)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowIssueHint()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(GuildActionInfo info, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(ConfigDataGuildActionInfo data, int curEnterSolarSysId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewCommon(ConfigDataGuildActionInfo actionInfo, int systemDifficultLevel, List<IdWeightInfo> mineralIds, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

