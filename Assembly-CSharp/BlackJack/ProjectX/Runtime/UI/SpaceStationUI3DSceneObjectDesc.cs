﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SpaceStationUI3DSceneObjectDesc : MonoBehaviour
    {
        public SpaceStatonBuildingType m_SceneObjectType;

        public enum SpaceStatonBuildingType
        {
            Shop,
            HeadQuarter,
            TradingCenter,
            Billboard,
            Ranking,
            Bar,
            BlackMarket,
            Exit
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        public struct SpaceStatonBuildingTypeComparer : IEqualityComparer<SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType>
        {
            public bool Equals(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType x, SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType y) => 
                (x == y);

            public int GetHashCode(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType obj) => 
                ((int) obj);
        }
    }
}

