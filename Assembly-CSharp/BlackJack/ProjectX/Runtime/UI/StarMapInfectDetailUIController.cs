﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class StarMapInfectDetailUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnInfoButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./InfectInfoRoot/InfectProgressRoot/InfectProgressInfo", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_infectProgressText;
        [AutoBind("./InfectInfoRoot/InfectProgressRoot/InfectProgressBar/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_infectProgressImage;
        [AutoBind("./InfectInfoRoot/InfectSelfHealingRoot/InfectSelfHealingTime", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_infectSelfHealingTimeText;
        [AutoBind("./InfectInfoRoot/InfectSelfHealingRoot/InfectSelfHealingNotValid", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_infectSelfHealingNotValidText;
        [AutoBind("./InfectInfoRoot/InfoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_infectInfoButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetSolarSystemInfectUIInfo;
        private static DelegateBridge __Hotfix_OnInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnInfoButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_CreateInfectDetailWindowProcess;

        public event Action EventOnInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateInfectDetailWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemInfectUIInfo(float infectProgress, DateTime selfHealingTime)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

