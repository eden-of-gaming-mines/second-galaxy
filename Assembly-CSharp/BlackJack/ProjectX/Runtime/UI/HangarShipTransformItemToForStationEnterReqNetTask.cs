﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipTransformItemToForStationEnterReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_hangarShipIndex;
        private int m_ackResult;
        private int m_unloadItemCount;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnHangarShipTransformItemToForStationEnterAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_set_AckResult;
        private static DelegateBridge __Hotfix_get_UnloadItemCount;

        [MethodImpl(0x8000)]
        public HangarShipTransformItemToForStationEnterReqNetTask(int hangarShipIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarShipTransformItemToForStationEnterAck(int result, int count)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public int UnloadItemCount
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

