﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemTeleportUIController : UIControllerBase
    {
        [AutoBind("SolarSystemTeleportUIPrefab/FadeTime", AutoBindAttribute.InitState.NotInit, false)]
        public Text FadeTimeTxt;
        private float m_FadeStartTime;
        private float m_LoopMaintainStartTime;
        private const float PeriodInTime = 3f;
        private const float PeriodOutTime = 5f;
        private const float LoopMaintainTime = 3f;
        private const float FadeOutDelayTime = 3f;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_PlayFadeIn;
        private static DelegateBridge __Hotfix_StopFadeIn;
        private static DelegateBridge __Hotfix_IsInFadeInPeriod;
        private static DelegateBridge __Hotfix_GetLoopMaintainPeriodEndTime;
        private static DelegateBridge __Hotfix_StartFadeOutDelay;
        private static DelegateBridge __Hotfix_PlayFadeOut;
        private static DelegateBridge __Hotfix_StopFadeOut;
        private static DelegateBridge __Hotfix_IsInFadeOutPeriod;
        private static DelegateBridge __Hotfix_IsInFadeOutDelayPeriod;
        private static DelegateBridge __Hotfix_PlayTeleportAnim;
        private static DelegateBridge __Hotfix_StopTelportAnim;

        [MethodImpl(0x8000)]
        public float GetLoopMaintainPeriodEndTime()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInFadeInPeriod()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInFadeOutDelayPeriod()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInFadeOutPeriod()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayFadeIn()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayFadeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayTeleportAnim()
        {
        }

        [MethodImpl(0x8000)]
        public void Reset()
        {
        }

        [MethodImpl(0x8000)]
        public void StartFadeOutDelay()
        {
        }

        [MethodImpl(0x8000)]
        public void StopFadeIn()
        {
        }

        [MethodImpl(0x8000)]
        public void StopFadeOut()
        {
        }

        [MethodImpl(0x8000)]
        public void StopTelportAnim()
        {
        }
    }
}

