﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemShipProgressUIController : UIControllerBase
    {
        private bool m_needInitProgressValue = true;
        [AutoBind("./BottomProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_bottomProgressBar;
        [AutoBind("./MiddleProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_middleProgressBar;
        [AutoBind("./TopProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_topProgressBar;
        private const float m_delayTimeBeforeStartProgressTransition = 0.5f;
        private const float m_progressTransitionSpeed = 0.333f;
        private float m_nextTopProgressTransitionTime;
        private float m_nextBottomProgressTransitionTime;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_SetProgressValue;
        private static DelegateBridge __Hotfix_UpdateForProgressTransition;

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        public void SetProgressValue(float currValue)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateForProgressTransition()
        {
        }
    }
}

