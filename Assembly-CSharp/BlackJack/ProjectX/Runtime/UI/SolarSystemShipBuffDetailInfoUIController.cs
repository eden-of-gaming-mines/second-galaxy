﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.ClientSolarSystemNs;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemShipBuffDetailInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBgButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_windowStateCtrl;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemObjPool;
        [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgButton;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRoot;
        [AutoBind("./CanvasMask/DetailScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_scrollViewObj;
        private uint m_nextUpdateTimeForVirtualBuf;
        private ILBSpaceTarget m_target;
        private GameObject m_itemTemplateGo;
        private ClientSpaceShip m_targetShip;
        private List<SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo> m_bufList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateShipBufList;
        private static DelegateBridge __Hotfix_CreateItemListPool;
        private static DelegateBridge __Hotfix_GetValue;
        private static DelegateBridge __Hotfix_SetBuffDetailInfoUIPos;
        private static DelegateBridge __Hotfix_CreateMainWindownEffectInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBuffUIItemFill;
        private static DelegateBridge __Hotfix_OnRealBufInfoFill;
        private static DelegateBridge __Hotfix_OnVirtualBufInfoFill;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBgButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBgButtonClick;

        public event Action EventOnBgButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateItemListPool()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreateMainWindownEffectInfo(bool isShow = true)
        {
        }

        [MethodImpl(0x8000)]
        private void GetValue(string poolName, GameObject storeItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBgButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuffUIItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRealBufInfoFill(LBShipFightBuf bufInfo, SolarSystemShipBuffDetailInfoItemUIController buffInfoUICtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnVirtualBufInfoFill(EquipEffectVirtualBufInfo bufInfo, SolarSystemShipBuffDetailInfoItemUIController buffInfoUICtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuffDetailInfoUIPos(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipBufList(ClientSpaceShip targetShip, List<SolarSytemTargetBuffDetailInfoUITask.BufDetailUIInfo> bufList, ILBSpaceTarget target)
        {
        }
    }
}

