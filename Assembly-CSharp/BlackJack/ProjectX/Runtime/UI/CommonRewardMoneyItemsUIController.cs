﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CommonRewardMoneyItemsUIController : CommonRewardUIController
    {
        private const string m_redColor = "BE3E37FF";
        private const string m_greenColor = "7CB544FF";
        public const string StateRed = "Red";
        public const string StateGreen = "Green";
        public const string StateNormal = "Normal";
        [AutoBind("./MoneyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MoneyGroup;
        private static DelegateBridge __Hotfix_SetQuestRewardInfo;
        private static DelegateBridge __Hotfix_SetCommitItemInfo;
        private static DelegateBridge __Hotfix_SetGuildMineralInfo;
        private static DelegateBridge __Hotfix_GenerateStoreItem;
        private static DelegateBridge __Hotfix_SetItemDropInfo;
        private static DelegateBridge __Hotfix_GetRewardItemSizeByIndex;
        private static DelegateBridge __Hotfix_GetRewardItemPositionByIndex;
        private static DelegateBridge __Hotfix_SetMonsterDropInfo;
        private static DelegateBridge __Hotfix_SetMineralRewardInfo;
        private static DelegateBridge __Hotfix_SetItemDropInfoByDropConfig;

        [MethodImpl(0x8000)]
        private ItemInfo GenerateStoreItem(StoreItemType type, int id, int count, QuestRewardType rewardType)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetRewardItemPositionByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetRewardItemSizeByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCommitItemInfo(ConfigDataQuestInfo confQuestInfo, GameObject NeedsTextTitleGroup, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildMineralInfo(List<QuestRewardInfo> rewardList, List<IdWeightInfo> idList, ref int idx, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemDropInfo(ConfigDataItemDropInfo dropInfo, bool showProbability, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetItemDropInfoByDropConfig(ConfigDataItemDropInfo dropInfo, Dictionary<string, UnityEngine.Object> resDict, bool showProbability = false)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetMineralRewardInfo(ConfigDataMineralTypeInfo mineInfo, int mineCount, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public override void SetMonsterDropInfo(List<SignalInfoMonsterDropInfo> monDropInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetQuestRewardInfo(List<QuestRewardInfo> rewardList, Dictionary<string, UnityEngine.Object> resDict, List<QuestRewardInfo> configRewardList = null, List<IdWeightInfo> idList = null)
        {
        }
    }
}

