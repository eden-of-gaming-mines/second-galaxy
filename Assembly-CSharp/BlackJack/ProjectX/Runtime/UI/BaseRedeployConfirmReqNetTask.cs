﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class BaseRedeployConfirmReqNetTask : NetWorkTransactionTask
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <RedeployConfirmResult>k__BackingField;
        private bool m_isBaseRedeployConfirmAck;
        private bool m_isSpaceStationEnterNtfReceived;
        private readonly bool m_isRedeployToSameBase;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnBaseRedeployConfirmAck;
        private static DelegateBridge __Hotfix_OnSpaceStationEnterNtf;
        private static DelegateBridge __Hotfix_IsNetTaskPreparedForStop;
        private static DelegateBridge __Hotfix_set_RedeployConfirmResult;
        private static DelegateBridge __Hotfix_get_RedeployConfirmResult;

        [MethodImpl(0x8000)]
        public BaseRedeployConfirmReqNetTask(bool isRedeployToSameBase)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNetTaskPreparedForStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBaseRedeployConfirmAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSpaceStationEnterNtf(SpaceStationEnterNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int RedeployConfirmResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

