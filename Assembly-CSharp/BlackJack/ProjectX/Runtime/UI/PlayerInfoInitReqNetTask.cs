﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class PlayerInfoInitReqNetTask : NetWorkTransactionTask
    {
        protected InitReqResultState m_initReqState;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitAck;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitEnd;
        private static DelegateBridge __Hotfix_get_InitReqState;
        private static DelegateBridge __Hotfix_set_InitReqState;

        [MethodImpl(0x8000)]
        public PlayerInfoInitReqNetTask(bool skipRelogin = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerInfoInitAck(object msg)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerInfoInitEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public InitReqResultState InitReqState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public enum InitReqResultState
        {
            None,
            CreateCharactor,
            InitEndNtf
        }
    }
}

