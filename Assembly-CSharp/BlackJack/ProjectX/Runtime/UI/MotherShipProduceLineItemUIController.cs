﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class MotherShipProduceLineItemUIController : UIControllerBase
    {
        private bool isPersonProduce;
        private LBProductionLine m_currProduceLine;
        private GuildProductionLineInfo m_produceInfo;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image Progressbar;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStateCtrl;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateProduceLineItemSimpleInfo_1;
        private static DelegateBridge __Hotfix_UpdateProduceLineItemSimpleInfo_0;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProduceLineItemSimpleInfo(GuildProductionLineInfo produceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProduceLineItemSimpleInfo(LBProductionLine produceLine)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

