﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SpaceStationMenuItemUIController : MenuItemUIControllerBase
    {
        [AutoBind("./TargetInfo/Info/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_spaceStationNameText;
        [AutoBind("./Detail/SpaceStationTypeItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_spaceStationTypeText;
        [AutoBind("./Detail/SpaceStationFactionItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_spaceStationFactionText;
        [AutoBind("./Detail/Buttons/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        [AutoBind("./Detail/Buttons/BaseDeployButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_baseDeployButton;
        [AutoBind("./Detail/Buttons/BaseDeployButton/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_baseDeployBgButtonRect;
        [AutoBind("./Detail/Buttons", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_baseDeployBtnArea;
        private GDBSpaceStationInfo m_gdbSpaceStationInfo;
        private static DelegateBridge __Hotfix_SetSpaceStationInfoByGDBData;
        private static DelegateBridge __Hotfix_GetGDBSpaceStationInfo;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;

        [MethodImpl(0x8000)]
        public GDBSpaceStationInfo GetGDBSpaceStationInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceStationInfoByGDBData(GDBSpaceStationInfo info, GDBSolarSystemInfo solarSystemInfo, bool showBaseDeployButton = false)
        {
        }
    }
}

