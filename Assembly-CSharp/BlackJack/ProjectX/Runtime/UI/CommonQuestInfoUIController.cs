﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonQuestInfoUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Animation QuestInfoAnimation;
        [AutoBind("./QuestTextGroup/MissionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestNameText;
        [AutoBind("./QuestTextGroup/MissionDetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestDetailText;
        [AutoBind("./MissionCompleteText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CompleteText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowQuestInfo;
        private static DelegateBridge __Hotfix_IsAnimationPlaying;
        private static DelegateBridge __Hotfix_StopAnimationPlaying;
        private static DelegateBridge __Hotfix_HideQuestInfo;
        private static DelegateBridge __Hotfix_ShowQuestCompleteEffect;
        private static DelegateBridge __Hotfix_OnDisappearAnimationEnd;

        [MethodImpl(0x8000)]
        public void HideQuestInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnimationPlaying()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnDisappearAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestCompleteEffect(bool isGuildAction = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestInfo(string strQuestName, string strQuestDetail, bool forcePlay = false)
        {
        }

        [MethodImpl(0x8000)]
        public void StopAnimationPlaying()
        {
        }
    }
}

