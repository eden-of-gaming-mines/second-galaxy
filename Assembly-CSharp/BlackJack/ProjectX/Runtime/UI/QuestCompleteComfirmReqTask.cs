﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class QuestCompleteComfirmReqTask : NetWorkTransactionTask
    {
        private int m_questInstanceId;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnQuestCompleteAck;

        [MethodImpl(0x8000)]
        public QuestCompleteComfirmReqTask(int questInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnQuestCompleteAck(int result, int questId, List<QuestRewardInfo> rewardList, bool diableReturnStationNotice)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

