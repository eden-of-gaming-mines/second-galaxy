﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using UnityEngine;

    public class StarMapForSolarSystemToStarfieldFadeInOutDesc : MonoBehaviour
    {
        [Header("恒星系星图渐黑持续时间")]
        public float m_fadeOutTimeLength;
        [Header("恒星系星图渐黑延迟时间")]
        public float m_fadeOutDelayTime;
        [Header("切换到星域星图时渐白持续时间")]
        public float m_fadeInTimeLength;
        [Header("摄像机偏移的曲线")]
        public AnimationCurve m_curveForCameraOffset;
        [Header("由星域星图进入恒星系星图的摄像机起始偏移位置")]
        public float m_cameraOffsetForEnterFromStarfield;
        [Header("由恒星系星图进入星域星图的摄像机最终偏移位置")]
        public float m_cameraOffsetForEnterStarfield;
        [Header("摄像机偏移的时间")]
        public float m_cameraOffsetTime;
    }
}

