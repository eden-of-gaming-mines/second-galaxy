﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class ItemStoreFixedUIFilter : ItemStoreUIFilterBase
    {
        private Dictionary<int, List<Func<ILBStoreItemClient, bool>>> m_fixedFilterList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitFixedFilterList;
        private static DelegateBridge __Hotfix_GetFixedFilterByKey;

        [MethodImpl(0x8000)]
        private List<Func<ILBStoreItemClient, bool>> GetFixedFilterByKey(int key)
        {
        }

        [MethodImpl(0x8000)]
        public void InitFixedFilterList(Dictionary<int, List<Func<ILBStoreItemClient, bool>>> fixedFilterList)
        {
        }
    }
}

