﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using TouchScript.Layers;
    using UnityEngine;

    public class SpaceStationUI3DSceneController : UIControllerBase
    {
        private List<SpaceStationUI3DSceneObjectDesc> m_buildingDescList;
        private bool isFindOther3Dui;
        [AutoBind("./CameraPlatformRoot", AutoBindAttribute.InitState.NotInit, false)]
        private SpaceStationUICameraOperationController m_cameraOperationCtrl;
        [AutoBind("./CameraPlatformRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public FullscreenLayer m_fullScreenLayer;
        [AutoBind("./HeadQuarter_Collider/HeadQuarter/AcceptQuest", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HeadQuarterAcceptQuest;
        [AutoBind("./HeadQuarter_Collider/HeadQuarter_CN/AcceptQuest", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HeadQuarterCNAcceptQuest;
        [AutoBind("./HeadQuarter_Collider/HeadQuarter/CompleteQuest", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HeadQuarterCompleteQuest;
        [AutoBind("./HeadQuarter_Collider/HeadQuarter_CN/CompleteQuest", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HeadQuarterCNCompleteQuest;
        [CompilerGenerated]
        private static Predicate<EffectBillboardController> <>f__am$cache0;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_SetCameraBlurEffectActive;
        private static DelegateBridge __Hotfix_SetHeadQuarterQuestState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetSpaceObjectScreenPosition;
        private static DelegateBridge __Hotfix_get_SpaceStationBuildingList;
        private static DelegateBridge __Hotfix_SetSpace3DUILangue;
        private static DelegateBridge __Hotfix_GetSpaceStationBuildingDesc;
        private static DelegateBridge __Hotfix_SetSpaceStationBuildingUnlock;
        private static DelegateBridge __Hotfix_get_CameraOperationCtrl;

        [MethodImpl(0x8000)]
        public Vector3 GetSpaceObjectScreenPosition(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        public SpaceStationUI3DSceneObjectDesc GetSpaceStationBuildingDesc(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraBlurEffectActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHeadQuarterQuestState(bool hasQuest2Accept, bool hasQuest2Complete)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSpace3DUILangue()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceStationBuildingUnlock(SpaceStationUI3DSceneObjectDesc.SpaceStatonBuildingType type, bool isUnlock)
        {
        }

        public List<SpaceStationUI3DSceneObjectDesc> SpaceStationBuildingList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public SpaceStationUICameraOperationController CameraOperationCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

