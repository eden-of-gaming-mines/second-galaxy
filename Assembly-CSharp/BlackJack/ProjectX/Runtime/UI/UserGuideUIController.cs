﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class UserGuideUIController : UserGuideUIControllerBase
    {
        protected bool m_isShowNPCDialog;
        protected bool m_isPassEvent;
        private int currentNpcChatDummyIndex;
        public NPCChatUIController m_npcChatUICtrl;
        public NpcTalkerDialogUIController m_npcTalkerDialogUICtrl;
        [AutoBind("./BG", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController PassEventUICtrl;
        [AutoBind("./MaskPanelRoot/MaskPanelLeft", AutoBindAttribute.InitState.NotInit, false)]
        protected Button MaskPanelLeftButton;
        [AutoBind("./MaskPanelRoot/MaskPanelRight", AutoBindAttribute.InitState.NotInit, false)]
        protected Button MaskPanelRightButton;
        [AutoBind("./MaskPanelRoot/MaskPanelTop", AutoBindAttribute.InitState.NotInit, false)]
        protected Button MaskPanelTopButton;
        [AutoBind("./MaskPanelRoot/MaskPanelBottom", AutoBindAttribute.InitState.NotInit, false)]
        protected Button MaskPanelBottomButton;
        [AutoBind("./OperationAreaButton", AutoBindAttribute.InitState.NotInit, false)]
        protected RectTransform m_operationAreaButtonRect;
        [AutoBind("./MaskPanelRoot/MaskPanelLeft", AutoBindAttribute.InitState.NotInit, false)]
        protected RectTransform m_maskPanelLeft;
        [AutoBind("./MaskPanelRoot/MaskPanelRight", AutoBindAttribute.InitState.NotInit, false)]
        protected RectTransform m_maskPanelRight;
        [AutoBind("./MaskPanelRoot/MaskPanelTop", AutoBindAttribute.InitState.NotInit, false)]
        protected RectTransform m_maskPanelTop;
        [AutoBind("./MaskPanelRoot/MaskPanelBottom", AutoBindAttribute.InitState.NotInit, false)]
        protected RectTransform m_maskPanelBottom;
        [AutoBind("./MaskPanelRoot/MaskPanelCenter", AutoBindAttribute.InitState.NotInit, false)]
        protected RectTransform m_maskPanelCenter;
        [AutoBind("./MaskPanelRoot", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController m_maskPanelCtrl;
        [AutoBind("./OperationAreaEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController m_opreationAreaEffectStateCtrl;
        [AutoBind("./OperationAreaEffectRoot/OperationAreaEffectRect", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_operationAreaEffectRect;
        [AutoBind("./OperationNoticeCenter", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_opreationNoticeGoCenter;
        [AutoBind("./OperationAreaEffectRoot/OperationAreaEffectRound", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_operationAreaEffectRound;
        [AutoBind("./OperationAreaEffectRoot/OperationAreaEffectHorizDrag", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_operationAreaEffectHorizDrag;
        [AutoBind("./OperationAreaEffectRoot/OperationAreaEffectRound/MaskImage", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_operationAreaEffectRoundBlackMask;
        [AutoBind("./Line", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController m_lineEffectStateCtrl;
        [AutoBind("./Line/LineImage", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_lineImage;
        [AutoBind("./Line/EndPointImage", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_lineEndPoint;
        [AutoBind("./Line/StartPointImage", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_lineStartPoint;
        [AutoBind("./NPCChatDummyRoot", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_npcChatDummyRoot;
        [AutoBind("./NPCTalkerDummyRoot/NPCTalkerDummy1", AutoBindAttribute.InitState.NotInit, false)]
        protected GameObject m_npcTalkerDialogDummy;
        protected const string OpreationAreaEffectState_OnShow = "OnShow";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Tick;
        private static DelegateBridge __Hotfix_Reset;
        private static DelegateBridge __Hotfix_ShowUserGuidePanel;
        private static DelegateBridge __Hotfix_SetOperationAreaButtonEnable;
        private static DelegateBridge __Hotfix_OnShowOperationAreaEffect;
        private static DelegateBridge __Hotfix_ShowMaskPanel;
        private static DelegateBridge __Hotfix_HideOperationAreaEffectImp;
        private static DelegateBridge __Hotfix_HideOperationNoticeImp;
        private static DelegateBridge __Hotfix_OnShowOpreationNotice;
        private static DelegateBridge __Hotfix_GetOperationAreaEffectGo;
        private static DelegateBridge __Hotfix_UpdateNpcDialog;
        private static DelegateBridge __Hotfix_UpdateNPCChatPanel;
        private static DelegateBridge __Hotfix_UpdateNPCTalkerDialog;
        private static DelegateBridge __Hotfix_GetNpcDialogSize;
        private static DelegateBridge __Hotfix_GetOperationNoticeSize;
        private static DelegateBridge __Hotfix_ShowLineEffect;
        private static DelegateBridge __Hotfix_GetPositionType;
        private static DelegateBridge __Hotfix_GetNearestPosBetweenList;
        private static DelegateBridge __Hotfix_ShowLineEffectImp;
        private static DelegateBridge __Hotfix_OnNPCTalkerAnswerButtonClick;
        private static DelegateBridge __Hotfix_OnPassEventClickEvent;
        private static DelegateBridge __Hotfix_PrintfPanelState;
        private static DelegateBridge __Hotfix_get_OperationAreaButtonRect;
        private static DelegateBridge __Hotfix_get_MaskPanelLeft;
        private static DelegateBridge __Hotfix_get_MaskPanelRight;
        private static DelegateBridge __Hotfix_get_MaskPanelTop;
        private static DelegateBridge __Hotfix_get_MaskPanelBottom;
        private static DelegateBridge __Hotfix_get_MaskPanelCenter;
        private static DelegateBridge __Hotfix_get_OperationNoticeGoCenter;
        private static DelegateBridge __Hotfix_get_OperationNoticeGoLeft;
        private static DelegateBridge __Hotfix_get_OperationNoticeGoRight;
        private static DelegateBridge __Hotfix_get_OperationNoticeGoTop;
        private static DelegateBridge __Hotfix_get_OperationNoticeGoBottom;

        [MethodImpl(0x8000)]
        protected void GetNearestPosBetweenList(List<Vector2> startPosList, List<Vector2> endPosList, out Vector2 startPos, out Vector2 endPos)
        {
        }

        [MethodImpl(0x8000)]
        protected override Vector2 GetNpcDialogSize()
        {
        }

        [MethodImpl(0x8000)]
        protected override GameObject GetOperationAreaEffectGo()
        {
        }

        [MethodImpl(0x8000)]
        protected override Vector2 GetOperationNoticeSize(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        protected PositionType GetPositionType(Rect rect, Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void HideOperationAreaEffectImp()
        {
        }

        [MethodImpl(0x8000)]
        protected override void HideOperationNoticeImp()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNPCTalkerAnswerButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPassEventClickEvent(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnShowOperationAreaEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnShowOpreationNotice()
        {
        }

        [MethodImpl(0x8000)]
        public void PrintfPanelState()
        {
        }

        [MethodImpl(0x8000)]
        public override void Reset()
        {
        }

        [MethodImpl(0x8000)]
        protected override void SetOperationAreaButtonEnable(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowLineEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowLineEffectImp(Vector2 startPos, Vector2 endPos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowMaskPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowUserGuidePanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void Tick()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateNPCChatPanel(UserGuideStepInfo.IPageInfo pageInfo, int npcChatDummyIndex, Vector2 offset, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateNpcDialog(UserGuideStepInfo.IPageInfo pageInfo, bool useNPCChat, int npcChatDummyIndex, Vector2 offset, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateNPCTalkerDialog(UserGuideStepInfo.IPageInfo pageInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        protected override RectTransform OperationAreaButtonRect
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override RectTransform MaskPanelLeft
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override RectTransform MaskPanelRight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override RectTransform MaskPanelTop
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override RectTransform MaskPanelBottom
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override RectTransform MaskPanelCenter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override GameObject OperationNoticeGoCenter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override GameObject OperationNoticeGoLeft
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override GameObject OperationNoticeGoRight
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override GameObject OperationNoticeGoTop
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override GameObject OperationNoticeGoBottom
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected enum PositionType
        {
            Left,
            LeftUp,
            Up,
            RightUp,
            Right,
            RightDown,
            Down,
            LeftDown
        }
    }
}

