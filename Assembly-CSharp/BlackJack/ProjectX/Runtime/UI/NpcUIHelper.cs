﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class NpcUIHelper
    {
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_GetFunctionNpcPrefix;
        private static DelegateBridge __Hotfix_CollectNpcDialogOptTypeIconResPath;
        private static DelegateBridge __Hotfix_CollectProfessionTypeIconResPath;
        private static DelegateBridge __Hotfix_GetNpcDialogOptionIconResPath_0;
        private static DelegateBridge __Hotfix_GetNpcDialogOptionIconResPath_1;
        private static DelegateBridge __Hotfix_GetNpcProfessionIconResPath;
        private static DelegateBridge __Hotfix_GetNpcFactionIconResPath;
        private static DelegateBridge __Hotfix_GetAllNpcProfessionIconResPath;
        private static DelegateBridge __Hotfix_GetNpcGenderNameString;
        private static DelegateBridge __Hotfix_GetSquareIconSpriteResFullPath;
        private static DelegateBridge __Hotfix_GetCircularIconSpriteResFullPath;
        private static DelegateBridge __Hotfix_GetNpcBodySpriteResFullPath;

        [MethodImpl(0x8000)]
        public static void CollectNpcDialogOptTypeIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectProfessionTypeIconResPath(List<string> resPathList)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetAllNpcProfessionIconResPath(List<string> resPahts)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetCircularIconSpriteResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetFunctionNpcPrefix(NpcFunctionType type)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcBodySpriteResFullPath(string resPrefix, string confResPath)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcDialogOptionIconResPath(NpcDialogOptInfo dlgInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcDialogOptionIconResPath(NpcDialogOptType optType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcFactionIconResPath(GrandFaction proType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcGenderNameString(GenderType genType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetNpcProfessionIconResPath(ProfessionType proType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSquareIconSpriteResFullPath(string resPrefix, string confResPath)
        {
        }
    }
}

