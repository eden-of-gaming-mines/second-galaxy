﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class WarPersonalKillRankingListUIController : UIControllerBase
    {
        private List<WarPersonalKillItemUIController> m_itemCtrlList;
        private const int MaxRankingPlayerCount = 10;
        private const string StateNormal = "Normal";
        private const string StateEmpty = "Empty";
        public Action<GuildBattlePlayerKillLostStatInfo, UIControllerBase> EventOnPlayerIconClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        [AutoBind("./ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemGroup;
        [AutoBind("./ScrollView/Viewport/Content/KillItemUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemPrefab;
        [AutoBind("./ScrollView/CaptainOperartionSeparator", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform CaptainOperartionSeparatorTransform;
        [AutoBind("/EmptyGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController EmptyGroupStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateKillRankingList;
        private static DelegateBridge __Hotfix_GetPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_SetPanelPosition;
        private static DelegateBridge __Hotfix_OnPlayerIconClick;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelShowOrHideProcess(bool isShow, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerIconClick(GuildBattlePlayerKillLostStatInfo info, UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateKillRankingList(List<GuildBattlePlayerKillLostStatInfo> killRankingList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

