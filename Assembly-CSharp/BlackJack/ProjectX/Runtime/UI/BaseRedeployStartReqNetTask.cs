﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class BaseRedeployStartReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_destSolarSystemId;
        private readonly int m_destStationId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <BaseRedeployResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnBaseRedeployStartAck;
        private static DelegateBridge __Hotfix_set_BaseRedeployResult;
        private static DelegateBridge __Hotfix_get_BaseRedeployResult;

        [MethodImpl(0x8000)]
        public BaseRedeployStartReqNetTask(int destSolarSystemId, int destStationId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBaseRedeployStartAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int BaseRedeployResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

