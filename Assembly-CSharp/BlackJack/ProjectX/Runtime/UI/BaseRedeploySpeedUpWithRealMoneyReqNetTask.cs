﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class BaseRedeploySpeedUpWithRealMoneyReqNetTask : NetWorkTransactionTask
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <RedeploySpeedUpWithMoneyResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnBaseRedeploySpeedUpWithRealMoneyAck;
        private static DelegateBridge __Hotfix_set_RedeploySpeedUpWithMoneyResult;
        private static DelegateBridge __Hotfix_get_RedeploySpeedUpWithMoneyResult;

        [MethodImpl(0x8000)]
        protected void OnBaseRedeploySpeedUpWithRealMoneyAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int RedeploySpeedUpWithMoneyResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

