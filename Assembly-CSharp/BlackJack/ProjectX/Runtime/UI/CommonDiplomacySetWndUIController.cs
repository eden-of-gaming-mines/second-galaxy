﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonDiplomacySetWndUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, DiplomacyState, bool> EventOnDiplomacyStateChanged;
        private string StateNormal;
        private string StateGray;
        private string StatePersonality;
        private string StateGuild;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BGClick", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgButton;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_detailStateCtrl;
        [AutoBind("/Detail/PersonalGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_personalStateCtrl;
        [AutoBind("/Detail/PersonalGroup/ToggleGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_personalFriendToggle;
        [AutoBind("/Detail/PersonalGroup/ToggleGroup/Normal", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_personalNormalToggle;
        [AutoBind("/Detail/PersonalGroup/ToggleGroup/Enemy", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_personalEnemyToggle;
        [AutoBind("/Detail/PersonalGroup/GreyImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_personalGreyButton;
        [AutoBind("/Detail/GuildGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildStateCtrl;
        [AutoBind("/Detail/GuildGroup/ToggleGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_guildFriendToggle;
        [AutoBind("/Detail/GuildGroup/ToggleGroup/Normal", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_guildNormalToggle;
        [AutoBind("/Detail/GuildGroup/ToggleGroup/Enemy", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle m_guildEnemyToggle;
        [AutoBind("/Detail/GuildGroup/GreyImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildGreyButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateDiplomacyPanelInfo;
        private static DelegateBridge __Hotfix_SetPersonanlDiplomaticType;
        private static DelegateBridge __Hotfix_SetGuildDiplomaticType;
        private static DelegateBridge __Hotfix_SetPanelPos;
        private static DelegateBridge __Hotfix_GetDiplomacySetWndUIProcess;
        private static DelegateBridge __Hotfix_OnPersonalFriendToggleValeChange;
        private static DelegateBridge __Hotfix_OnPersonalNeutralToggleValeChange;
        private static DelegateBridge __Hotfix_OnPersonalEnemyToggleValeChange;
        private static DelegateBridge __Hotfix_OnPersonalGreyButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFriendToggleValeChange;
        private static DelegateBridge __Hotfix_OnGuildNeutralToggleValeChange;
        private static DelegateBridge __Hotfix_OnGuildEnemyToggleValeChange;
        private static DelegateBridge __Hotfix_OnGuildGreyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDiplomacyStateChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDiplomacyStateChanged;

        public event Action<bool, DiplomacyState, bool> EventOnDiplomacyStateChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess GetDiplomacySetWndUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildEnemyToggleValeChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFriendToggleValeChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildGreyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildNeutralToggleValeChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPersonalEnemyToggleValeChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPersonalFriendToggleValeChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPersonalGreyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPersonalNeutralToggleValeChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildDiplomaticType(DiplomacyState guildDiplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPos(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPersonanlDiplomaticType(DiplomacyState personanlDiplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDiplomacyPanelInfo(DiplomacyState personanlDiplomacyState, DiplomacyState guilDiplomacyState, bool hasGuild, bool hasGuildDiplomacyUpdatePermission)
        {
        }
    }
}

