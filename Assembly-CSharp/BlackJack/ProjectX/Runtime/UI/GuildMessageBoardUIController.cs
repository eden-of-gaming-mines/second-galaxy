﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildMessageBoardUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildMessageItemUIController> EventOnMessageIconClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildMessageItemUIController> EventOnPlayerNameClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildMessageItemUIController> EventOnDeleteButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildMessageItemUIController, string> EventOnDeleteTextButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLoadMoreButtonClick;
        private const string StatePanelOpen = "Show";
        private const string StatePanelClose = "Close";
        private const string MessageItemPoolName = "MessageItemUIPrefab";
        private const string StateNormal = "Normal";
        private const string StateEmpty = "Empty";
        private List<GuildMessageInfo> m_guildMessageList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private bool m_showAll;
        private bool m_hasDeletePerMission;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./MessageBoardGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_messageStateCtrl;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_closeButton;
        [AutoBind("./BGImages/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backButton;
        [AutoBind("./MessageBoardGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_itemListScrollRect;
        [AutoBind("./MessageBoardGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemListUIItemPool;
        [AutoBind("./MessageBoardGroup/ScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRoot;
        [AutoBind("./MessageBoardGroup/MessageBoarInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_messageInputField;
        [AutoBind("./MessageBoardGroup/MessageBoardImage", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_messageSendButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetStartIndex;
        private static DelegateBridge __Hotfix_ClearSendText;
        private static DelegateBridge __Hotfix_UpdateAllMessage;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_OnMessageIconClick;
        private static DelegateBridge __Hotfix_OnPlayerNameClick;
        private static DelegateBridge __Hotfix_OnLoadMoreButtonClick;
        private static DelegateBridge __Hotfix_OnDeleteButtonClick;
        private static DelegateBridge __Hotfix_OnDeleteTextButtonClick;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyItemFill;
        private static DelegateBridge __Hotfix_add_EventOnMessageIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnMessageIconClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerNameClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerNameClick;
        private static DelegateBridge __Hotfix_add_EventOnDeleteButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDeleteButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDeleteTextButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnDeleteTextButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLoadMoreButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLoadMoreButtonClick;

        public event Action<GuildMessageItemUIController> EventOnDeleteButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildMessageItemUIController, string> EventOnDeleteTextButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLoadMoreButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildMessageItemUIController> EventOnMessageIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildMessageItemUIController> EventOnPlayerNameClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearSendText()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public int GetStartIndex()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteTextButtonClick(UIControllerBase ctrl, string str)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildDiplomacyItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadMoreButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMessageIconClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerNameClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllMessage(List<GuildMessageInfo> messageInfoList, bool showAll, bool hasDeletePerMission, Dictionary<string, UnityEngine.Object> resDic, int startIndex = 0)
        {
        }
    }
}

