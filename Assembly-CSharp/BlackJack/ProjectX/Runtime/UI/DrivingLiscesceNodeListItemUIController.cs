﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DrivingLiscesceNodeListItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DrivingLiscesceNodeListItemUIController> EventOnGoToButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DrivingLicenseNodeInfoUIController, bool> EventOnNodeItemClick;
        private List<DrivingLicenseNodeInfoUIController> m_drivingLiscenseNodeCtrlList;
        private int m_skillId;
        private int m_needSkillID;
        private int m_needSkillLevel;
        private RankType m_needSkillRanktype;
        private int m_skillLevel;
        private int m_requireLevel;
        private const string ButtonState_Lock = "Lock";
        private const string ButtonState_Complete = "Done";
        private const string ButtonState_Normal = "Normal";
        private const string DrvingLearningState_0 = "0Done";
        private const string DrvingLearningState_1 = "1Done";
        private const string DrvingLearningState_2 = "2Done";
        private const string DrvingLearningState_3 = "3Done";
        private const string DrvingLearningState_4 = "4Done";
        private const string DrvingLearningState_5 = "5Done";
        [AutoBind("./Title/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipTypeIcon;
        [AutoBind("./Title/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipTypeName;
        [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx Button;
        [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buttonCtrl;
        [AutoBind("./Button/LockButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text LockedText;
        [AutoBind("./Effect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_levelEffectCtrl;
        [AutoBind("./Node", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NodeItemGoInstance;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateDrivingLiscesceNodeListItem;
        private static DelegateBridge __Hotfix_NoticePlayerNeedSkill;
        private static DelegateBridge __Hotfix_ShowDrivingLevelAnim;
        private static DelegateBridge __Hotfix_OnDrivingNodeClick;
        private static DelegateBridge __Hotfix_OnButtonClick;
        private static DelegateBridge __Hotfix_GetCurrentButtonState;
        private static DelegateBridge __Hotfix_add_EventOnGoToButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGoToButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnNodeItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnNodeItemClick;
        private static DelegateBridge __Hotfix_get_SkillID;
        private static DelegateBridge __Hotfix_get_NeedSkillID;
        private static DelegateBridge __Hotfix_get_NeedSkillLevel;
        private static DelegateBridge __Hotfix_get_NeedSkillRankType;

        public event Action<DrivingLiscesceNodeListItemUIController> EventOnGoToButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DrivingLicenseNodeInfoUIController, bool> EventOnNodeItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetCurrentButtonState(out int level)
        {
        }

        [MethodImpl(0x8000)]
        public void Init(Action<DrivingLiscesceNodeListItemUIController> gotoButtonCickEvent = null, Action<DrivingLicenseNodeInfoUIController, bool> nodeSelectedEvent = null)
        {
        }

        [MethodImpl(0x8000)]
        public void NoticePlayerNeedSkill(int needLevel)
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDrivingNodeClick(DrivingLicenseNodeInfoUIController ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDrivingLevelAnim(int level)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDrivingLiscesceNodeListItem(int skillId, int currLevel, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public int SkillID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int NeedSkillID
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int NeedSkillLevel
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public RankType NeedSkillRankType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

