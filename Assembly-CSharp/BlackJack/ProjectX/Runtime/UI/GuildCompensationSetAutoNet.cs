﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class GuildCompensationSetAutoNet : NetWorkTransactionTask
    {
        private bool m_isAuto;
        public GuildCompensationSetAutoAck Ack;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_OnGuildCompensationSetAutoAck;

        [MethodImpl(0x8000)]
        public GuildCompensationSetAutoNet(bool auto)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildCompensationSetAutoAck(GuildCompensationSetAutoAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

