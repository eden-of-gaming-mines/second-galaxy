﻿namespace BlackJack.ProjectX.Runtime.UI
{
    public interface IClickableScrollItem : IClickable, IScrollItem
    {
    }
}

