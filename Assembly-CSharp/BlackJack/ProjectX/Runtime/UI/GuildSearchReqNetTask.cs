﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildSearchReqNetTask : NetWorkTransactionTask
    {
        private string m_key;
        private bool m_searchName;
        private List<GuildSimplestInfo> m_guildSimplestInfoList;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildSearchAck;
        private static DelegateBridge __Hotfix_get_GuildSimplestInfoList;

        [MethodImpl(0x8000)]
        public GuildSearchReqNetTask(string key, bool searchName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildSearchAck(List<GuildSimplestInfo> guildSimplestInfoList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public List<GuildSimplestInfo> GuildSimplestInfoList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

