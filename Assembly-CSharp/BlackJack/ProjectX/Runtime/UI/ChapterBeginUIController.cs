﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class ChapterBeginUIController : UIControllerBase
    {
        [AutoBind("./Group/TextLine1", AutoBindAttribute.InitState.NotInit, false)]
        private Text TextLine1;
        [AutoBind("./Group/TextLine2", AutoBindAttribute.InitState.NotInit, false)]
        private Text TextLine2;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowLine;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLine(string line1, string line2)
        {
        }
    }
}

