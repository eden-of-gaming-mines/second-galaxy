﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class KillRecordDetaiLostItemInfoUICtrl : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnLostItemClick;
        public bool m_isInitComplete;
        public LostItemInfo m_currItemInfo;
        public CommonItemIconUIController m_commonItemCtrl;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MoneyNumberText;
        [AutoBind("./CornerMarkImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CornerMarkState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegItemClickEvent;
        private static DelegateBridge __Hotfix_UpdateItemInfo;
        private static DelegateBridge __Hotfix_IsShowItemCount;
        private static DelegateBridge __Hotfix_OnLostItemClick;
        private static DelegateBridge __Hotfix_add_EventOnLostItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnLostItemClick;

        protected event Action<UIControllerBase> EventOnLostItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected bool IsShowItemCount(StoreItemType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLostItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemInfo(LostItemInfo itemInfo, ItemCompensationStatus status, bool isShowCompensationInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

