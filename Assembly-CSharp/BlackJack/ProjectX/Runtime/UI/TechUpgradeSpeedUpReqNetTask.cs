﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class TechUpgradeSpeedUpReqNetTask : NetWorkTransactionTask
    {
        private int m_techId;
        private int m_speedUpItemId;
        private int m_speedUpItemCount;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <TechUpgradeSpeedUpResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnTechUpgradeSpeedUpWithItemAck;
        private static DelegateBridge __Hotfix_set_TechUpgradeSpeedUpResult;
        private static DelegateBridge __Hotfix_get_TechUpgradeSpeedUpResult;

        [MethodImpl(0x8000)]
        public TechUpgradeSpeedUpReqNetTask(int techId, int itemId, int itemCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnTechUpgradeSpeedUpWithItemAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int TechUpgradeSpeedUpResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

