﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AutoReloadAmmoBuyPanelUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnBuyPanelAmmoIconClick;
        private bool m_isShow;
        protected List<AmmoSlotInfo> m_ammoSlotInfoList;
        private const string AmmoItemAssetName = "CommonItemUIPrefab";
        private const int AmmoSlotMaxCount = 3;
        protected static string PanelState_Open;
        protected static string PanelState_Close;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buyPanelStateCtrl;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyPanelConfirmButton;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BuyPanelCancelButton;
        [AutoBind("./DetailPanel/ItemGroup/Slot1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoSlot1Root;
        [AutoBind("./DetailPanel/ItemGroup/Slot1/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform AmmoSlot1ItemDummy;
        [AutoBind("./DetailPanel/ItemGroup/Slot1/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoSlot1ItemNameText;
        [AutoBind("./DetailPanel/ItemGroup/Slot2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoSlot2Root;
        [AutoBind("./DetailPanel/ItemGroup/Slot2/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform AmmoSlot2ItemDummy;
        [AutoBind("./DetailPanel/ItemGroup/Slot2/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoSlot2ItemNameText;
        [AutoBind("./DetailPanel/ItemGroup/Slot3", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AmmoSlot3Root;
        [AutoBind("./DetailPanel/ItemGroup/Slot3/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform AmmoSlot3ItemDummy;
        [AutoBind("./DetailPanel/ItemGroup/Slot3/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoSlot3ItemNameText;
        [AutoBind("./DetailPanel/ConfirmButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuyPriceText;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmButtonStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_IsShow;
        private static DelegateBridge __Hotfix_CreateBuyWindowProcess;
        private static DelegateBridge __Hotfix_UpdateAmmoBuyPanel;
        private static DelegateBridge __Hotfix_OnBuyPanelAmmoIconClick;
        private static DelegateBridge __Hotfix_add_EventOnBuyPanelAmmoIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnBuyPanelAmmoIconClick;

        public event Action<int> EventOnBuyPanelAmmoIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateBuyWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuyPanelAmmoIconClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAmmoBuyPanel(ILBStaticPlayerShip staticShip, List<AmmoInfo> ammoInfoList, long buyPrice, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        public class AmmoSlotInfo
        {
            public GameObject m_slotRoot;
            public Transform m_itemDummy;
            public CommonItemIconUIController m_itemCtrl;
            public Text m_itemName;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

