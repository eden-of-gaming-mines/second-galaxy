﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CrackSlotUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <SlotIndex>k__BackingField;
        private SlotState m_currSlotState = SlotState.Empty;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CrackSlotUIController> EventOnSlotButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CrackSlotUIController> EventOnCloseButtonClick;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SlotStateCtrl;
        [AutoBind("./Item", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelectedCtrl;
        [AutoBind("./Item", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SlotButton;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./ProgressBarGroup/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBarImage;
        [AutoBind("./ProgressBarGroup/Effect", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ProgressBarLightImage;
        [AutoBind("./TimeGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RemainTimeText;
        [AutoBind("./RealMoneyGroup/RealMoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RealMoneyText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetSlotState;
        private static DelegateBridge __Hotfix_SetSlotBoxIcon;
        private static DelegateBridge __Hotfix_SetRealMoneyValue;
        private static DelegateBridge __Hotfix_SetRemaningTime;
        private static DelegateBridge __Hotfix_SetProgress;
        private static DelegateBridge __Hotfix_SetSelectedMode;
        private static DelegateBridge __Hotfix_OnSlotButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_get_SlotIndex;
        private static DelegateBridge __Hotfix_set_SlotIndex;
        private static DelegateBridge __Hotfix_get_CurrSlotState;
        private static DelegateBridge __Hotfix_add_EventOnSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;

        public event Action<CrackSlotUIController> EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CrackSlotUIController> EventOnSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSlotButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetProgress(float progress)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRealMoneyValue(int monyeVal)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRemaningTime(string timeStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedMode(bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSlotBoxIcon(Sprite icon)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSlotState(SlotState slotState)
        {
        }

        public int SlotIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public SlotState CurrSlotState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum SlotState
        {
            Cracked,
            Cracking,
            WaitForCrack,
            Empty,
            Locked
        }
    }
}

