﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ChatScrollViewWindowUIController : UIControllerBase
    {
        private int m_lastUpdateChatInfoListMaxIndex;
        private EmojiParseController m_emojiParseHelper;
        private Action<UIControllerBase, ChatInfo> m_linkButtonClickAction;
        private Action<UIControllerBase, ChatInfo> m_playerHeadButtonClickAction;
        private Action<UIControllerBase, ChatInfo> m_soundButtonClickAction;
        private Action<ChatInfo> m_translateButtonClickAction;
        private Action<ChatInfo> m_audio2StringButtonClickAction;
        private Action<ChatInfo> m_audioItemTranslateStringButtonClickAction;
        public Dictionary<string, UnityEngine.Object> m_resCache;
        public readonly List<ChatInfo> m_chatListCache;
        private List<GameObject> m_visibleGoList;
        private const string PlyaeritemPoolName = "ChatCommonItemPrefab";
        private const string PlayerItemAssetName = "ChatCommonItemPrefab";
        private const string EmojiInfoTxtName = "EmojiInfo";
        private const string EmojiShowAssetName = "EmojiShowImage";
        private ChatInfo m_currPlayChatInfo;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ChatLoopVerticalScrollRect m_loopScrollView;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_objPool;
        [AutoBind("./Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_content;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnChatItemFill;
        private static DelegateBridge __Hotfix_OnPoolItemCreated;
        private static DelegateBridge __Hotfix_OnPlayerHeadButtonClicked;
        private static DelegateBridge __Hotfix_GetClickedItemIndex;
        private static DelegateBridge __Hotfix_SetClickHighlight;
        private static DelegateBridge __Hotfix_UpdateChatItemInfo;
        private static DelegateBridge __Hotfix_UpdateChatListUIInfo;
        private static DelegateBridge __Hotfix_UpdateChatCacheInfo;
        private static DelegateBridge __Hotfix_RefreshChatListViewWithCurrData;
        private static DelegateBridge __Hotfix_CheckLanguage;
        private static DelegateBridge __Hotfix_IsScrollViewInEnd;
        private static DelegateBridge __Hotfix_SetCurrPlayChatInfo;
        private static DelegateBridge __Hotfix_GetCurrPlayChatInfo;
        private static DelegateBridge __Hotfix_SetItemClickEvent;

        [MethodImpl(0x8000)]
        private static bool CheckLanguage(ChatInfo chatInfo, ChatLanguageChannel chatLanguage)
        {
        }

        [MethodImpl(0x8000)]
        private int GetClickedItemIndex(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public ChatInfo GetCurrPlayChatInfo()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsScrollViewInEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerHeadButtonClicked(UIControllerBase uCtrl, ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshChatListViewWithCurrData()
        {
        }

        [MethodImpl(0x8000)]
        public void SetClickHighlight(UIControllerBase uCtrl, bool highlight)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrPlayChatInfo(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemClickEvent(Action<UIControllerBase, ChatInfo> linkButton, Action<UIControllerBase, ChatInfo> soundButton, Action<UIControllerBase, ChatInfo> playerHeadButton, Action<ChatInfo> translateButtonAction, Action<ChatInfo> audio2StringButtonAction, Action<ChatInfo> audioItemTranslateButtonAction)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChatCacheInfo(List<ChatInfo> channelChatList, ChatLanguageChannel chatLanguage)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateChatItemInfo(GameObject itemPrefab, ChatInfo chatInfo, bool isSelf)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChatListUIInfo(List<ChatInfo> channelChatList, ChatLanguageChannel chatLanguage, ChatInfo lastChatInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool isScroll)
        {
        }
    }
}

