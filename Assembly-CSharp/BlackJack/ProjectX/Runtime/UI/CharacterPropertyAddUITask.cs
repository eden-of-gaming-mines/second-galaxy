﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class CharacterPropertyAddUITask : UITaskBase
    {
        public static string CharacterPropertyAddUITaskMode_Normal = "CharacterPropertyAddUITaskMode_Normal";
        private CharacterPropertyAddPanelUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CharacterPropertyAddUITask> EventOnPropertyAddPanelClose;
        private CharacterClearPropertyPointsUITask m_characterClearPropertyPointsUITask;
        private LogicBlockCharacterBasicPropertiesBase m_lbCharBasicProperties;
        private int[] m_previewBasicPropertyPointAfterEditing;
        private int m_previewFreePropertyPointAfterEditing;
        private int m_initWindowFlagIndex;
        public const string TaskName = "CharacterPropertyAddUITask";
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnSubPointButtonClick;
        private static DelegateBridge __Hotfix_OnAddPointButtonClick;
        private static DelegateBridge __Hotfix_OnClearPointsButtonClick;
        private static DelegateBridge __Hotfix_OnClearPropertyPointsUITaskPause;
        private static DelegateBridge __Hotfix_OnResetPointsButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_GetAddPointForBasicProperty;
        private static DelegateBridge __Hotfix_IsAnyPropertyPointChanged;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_add_EventOnPropertyAddPanelClose;
        private static DelegateBridge __Hotfix_remove_EventOnPropertyAddPanelClose;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbCharBasicProperties;

        public event Action<CharacterPropertyAddUITask> EventOnPropertyAddPanelClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CharacterPropertyAddUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private int GetAddPointForBasicProperty(PropertyCategory propertyCategory)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAnyPropertyPointChanged()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddPointButtonClick(PropertyCategory propertyCategory)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClearPointsButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnClearPropertyPointsUITaskPause(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnResetPointsButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubPointButtonClick(PropertyCategory propertyCategory)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockCharacterBasicPropertiesBase LbCharBasicProperties
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

