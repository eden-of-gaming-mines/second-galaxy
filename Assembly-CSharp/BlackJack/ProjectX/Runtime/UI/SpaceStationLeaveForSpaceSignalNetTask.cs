﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class SpaceStationLeaveForSpaceSignalNetTask : NetWorkTransactionTask
    {
        public ulong m_shipInstanceId;
        public ulong m_signalInsId;
        public int m_solarSystemId;
        public int m_strikeForSpaceSignalAckResult;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnSpaceStationLeaveForSingalAck;
        private static DelegateBridge __Hotfix_OnSpaceEnterNtf;

        [MethodImpl(0x8000)]
        public SpaceStationLeaveForSpaceSignalNetTask(ulong shipInstanceId, int solarSystemId, ulong signalInsId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceEnterNtf(bool isLeaveStation)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpaceStationLeaveForSingalAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

