﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class BranchStoryStartReqNetTask : NetWorkTransactionTask
    {
        private int m_branchStoryId;
        private int m_firstSubQuestId;
        private int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnAcceptQuest;
        private static DelegateBridge __Hotfix_get_Result;

        [MethodImpl(0x8000)]
        public BranchStoryStartReqNetTask(int branchStoryId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAcceptQuest(int result, int insId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

