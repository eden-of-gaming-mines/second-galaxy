﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterKillRecordDetailInfoUICtrl : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBGButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCloseButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo> EventOnWinnerShipIconButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<KillRecordInfo> EventOnLosserShipIconButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, KillRecordInfo> EventOnWinnerCaptainIconButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, KillRecordInfo> EventOnLosserCaptainIconButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<KillRecordInfo> EventOnSendLinkButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnScrollViewClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<KillRecordInfo> EventOnIssueButtonClick;
        public KillRecordInfo m_currKillRecord;
        public CommonItemIconUIController m_winnerShipIconCtrl;
        public CommonCaptainIconUIController m_winnerCaptainIconCtrl;
        public CommonItemIconUIController m_losserShipIconCtrl;
        public CommonCaptainIconUIController m_losserCaptainIconCtrl;
        public List<KillRecordDetaiLostItemInfoUICtrl> m_lostItemCtrlList;
        public const string m_normalKillMode = "Normal";
        public const string m_captainKillMode = "CaptainKill";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./DetailRoot/LostItemRoot/LostItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler ScrollViewHandler;
        [AutoBind("./DetailRoot/LostItemRoot/LostItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect ScrollView;
        [AutoBind("./DetailRoot/LostItemRoot/LostItemScrollView/Viewport/Content/LostItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LostItem;
        [AutoBind("./DetailRoot/LostItemRoot/TitleGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AllMoneyNumText;
        [AutoBind("./DetailRoot", AutoBindAttribute.InitState.NotInit, false)]
        public PrefabResourceContainer resContainer;
        [AutoBind("./DetailRoot/BeKilledDetailRoot/BaseNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BaseNameText;
        [AutoBind("./DetailRoot/BeKilledDetailRoot/SiteText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./DetailRoot/BeKilledDetailRoot/ArmyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BeKilledGuildNameText;
        [AutoBind("./DetailRoot/BeKilledDetailRoot/UnionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BeKilledAllianceNameText;
        [AutoBind("./DetailRoot/BeKilledDetailRoot/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BeKilledNameText;
        [AutoBind("./DetailRoot/BeKilledDetailRoot/CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BeKilledCaptainDummy;
        [AutoBind("./DetailRoot/BeKilledDetailRoot/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BeKilledItemDummy;
        [AutoBind("./DetailRoot/KillerDetailRoot/ArmyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillGuildNameText;
        [AutoBind("./DetailRoot/KillerDetailRoot/UnionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillAllianceNameText;
        [AutoBind("./DetailRoot/KillerDetailRoot/CaptainNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KillNameText;
        [AutoBind("./DetailRoot/KillerDetailRoot/CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject KillCaptainDummy;
        [AutoBind("./DetailRoot/KillerDetailRoot/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject KillItemDummy;
        [AutoBind("./DetailRoot/KillerDetailRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController KillStateUICtrl;
        [AutoBind("./DetailRoot/SimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SimpleInfoDummy;
        [AutoBind("./DetailRoot/LostItemRoot/ItemSimpleUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LostItemSimpleInfoDummy;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./DetailRoot/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./DetailRoot/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./DetailRoot/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShareButton;
        [AutoBind("./DetailRoot/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx IssueButton;
        [AutoBind("./DetailRoot/IssueButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController IssueButtonState;
        private const string GuildCodeFormat = "[{0}]";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateKillRecordDetailInfo;
        private static DelegateBridge __Hotfix_GetAllLostItemPrice;
        private static DelegateBridge __Hotfix_UpdateLostItemListInfo;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnWinnerShipIconButtonClick;
        private static DelegateBridge __Hotfix_OnWinnerCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_OnLosserShipIconButtonClick;
        private static DelegateBridge __Hotfix_OnLosserCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_OnSendLinkButtonClick;
        private static DelegateBridge __Hotfix_OnScrollViewClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnIssueButtonClick;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWinnerShipIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWinnerShipIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLosserShipIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLosserShipIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWinnerCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWinnerCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLosserCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLosserCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSendLinkButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSendLinkButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnScrollViewClick;
        private static DelegateBridge __Hotfix_remove_EventOnScrollViewClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnIssueButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnIssueButtonClick;

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnIssueButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, KillRecordInfo> EventOnLosserCaptainIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnLosserShipIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnScrollViewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnSendLinkButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, KillRecordInfo> EventOnWinnerCaptainIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<KillRecordInfo> EventOnWinnerShipIconButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected double GetAllLostItemPrice(List<LostItemInfo> itemList)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBGButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnIssueButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLosserCaptainIconButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLosserShipIconButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSendLinkButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWinnerCaptainIconButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnWinnerShipIconButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateKillRecordDetailInfo(KillRecordInfo killRecordInfo, bool showCompensationInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateLostItemListInfo(List<LostItemInfo> itemInfoList, List<ItemCompensationStatus> compStatus, bool showCompensationInfo, string loseUserId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

