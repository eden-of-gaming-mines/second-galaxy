﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using TouchScript.Gestures;
    using TouchScript.Layers;
    using UnityEngine;

    public class PuzzleGameBGUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float, float> EventOnTransformGestureSlipping;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnMatcingEnd;
        private GameObject[] m_lineInstantiateObjs;
        private GameObject[] m_pointInstantiateObjs;
        private Transform[] m_lineRoots;
        private Transform[] m_pointRoots;
        private List<Animator>[] m_pointAnimates;
        private List<Animator>[] m_lineAnimates;
        private List<LineRenderer>[] m_lines;
        private List<float>[] m_linePointLists;
        private bool m_isMoveSoundPlayed;
        private Vector3 m_rotation;
        private int m_solarSystemId;
        private Vector3 m_centerPos;
        private List<Vector2> m_solarSystemPosList;
        private List<SolarSystemLinkInfo> m_solarSystemLinkInfos;
        private List<int> m_solarSystemIdList;
        private List<bool> m_solarSystemHintStateList;
        private List<int> m_solarSystemHintAddIndexList;
        private List<int> m_solarSystemNotHintIndexList;
        private const string AnimatorStateParam = "ConstellationPuzzleState";
        private const int AnimatorParamNormalToComplete = 3;
        private const int AnimatorParamHintToComplete = 2;
        private const int AnimatorParamNormalToHint = 1;
        private bool m_needLerp;
        private int m_lerpCount;
        private DateTime m_nextMuteMoveTime;
        private System.Random m_random;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public ScreenTransformGesture m_tansGesture;
        [AutoBind("./ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public FullscreenLayer m_fullscreenLayer;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public PuzzleGame3DSceneDesc m_puzzleGameDesc;
        [AutoBind("./MoveSound", AutoBindAttribute.InitState.NotInit, false)]
        public CRIAudioSourceHelperImpl m_criAudioSourceHelperImpl;
        [AutoBind("./PuzzleGame/Star/Sphere", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_solarsystemRoot;
        [AutoBind("./PuzzleGame/Star/Sphere/Points", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_solarsystemPointRoot;
        [AutoBind("./PuzzleGame/Star/Sphere/Points/Sphere", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_solarSystemPoint;
        [AutoBind("./PuzzleGame/Star/Sphere/Lines", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_solarsystemLinkLineRoot;
        [AutoBind("./PuzzleGame/Star/Sphere/Lines/Line", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_solarsystemLinkLine;
        [AutoBind("./PuzzleGame/Plane/Answer/Points", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_answerPointRoot;
        [AutoBind("./PuzzleGame/Plane/Answer/Points/Sphere", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_answerPoint;
        [AutoBind("./PuzzleGame/Plane/Answer/Lines", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_answerLinkLineRoot;
        [AutoBind("./PuzzleGame/Plane/Answer/Lines/Line", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_answerLinkLine;
        [AutoBind("./PuzzleGame/Plane/Shadow/Points", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_shadowPointRoot;
        [AutoBind("./PuzzleGame/Plane/Shadow/Points/Sphere", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shadowPoint;
        [AutoBind("./PuzzleGame/Plane/Shadow/Lines", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_shadowLinkLineRoot;
        [AutoBind("./PuzzleGame/Plane/Shadow/Lines/Line", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_shadowLinkLine;
        [AutoBind("./PuzzleGame/CastLines", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_castLineRoot;
        [AutoBind("./PuzzleGame/CastLines/Line", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_castLine;
        private static DelegateBridge __Hotfix_OnEnable;
        private static DelegateBridge __Hotfix_OnDisable;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_GetMatchingRate_0;
        private static DelegateBridge __Hotfix_GetCompleteMatchingRate;
        private static DelegateBridge __Hotfix_RotateSolarsystem;
        private static DelegateBridge __Hotfix_SetHintSolarSystem;
        private static DelegateBridge __Hotfix_CompletePuzzleGame;
        private static DelegateBridge __Hotfix_GetTargetSolarSystemPos;
        private static DelegateBridge __Hotfix_GetCenterPos;
        private static DelegateBridge __Hotfix_SetMoveVoice;
        private static DelegateBridge __Hotfix_StopMoveSound;
        private static DelegateBridge __Hotfix_ManipulationTransformedHandler;
        private static DelegateBridge __Hotfix_ClearData;
        private static DelegateBridge __Hotfix_InitSolarsystemPos;
        private static DelegateBridge __Hotfix_InitSolarsystemLink;
        private static DelegateBridge __Hotfix_InitSolarsystemRotation;
        private static DelegateBridge __Hotfix_UpdateShadow;
        private static DelegateBridge __Hotfix_SetCastLinePos;
        private static DelegateBridge __Hotfix_SetPoint;
        private static DelegateBridge __Hotfix_SetLines;
        private static DelegateBridge __Hotfix_SetLineEffect;
        private static DelegateBridge __Hotfix_SetHintState;
        private static DelegateBridge __Hotfix_SetCompleteState;
        private static DelegateBridge __Hotfix_GetMatchingRate_1;
        private static DelegateBridge __Hotfix_GetRotateOffsetInOneDirection;
        private static DelegateBridge __Hotfix_SetVolume;
        private static DelegateBridge __Hotfix_add_EventOnTransformGestureSlipping;
        private static DelegateBridge __Hotfix_remove_EventOnTransformGestureSlipping;
        private static DelegateBridge __Hotfix_add_EventOnMatcingEnd;
        private static DelegateBridge __Hotfix_remove_EventOnMatcingEnd;
        private static DelegateBridge __Hotfix_get_LineInstantiateObjs;
        private static DelegateBridge __Hotfix_get_PointInstantiateObjs;
        private static DelegateBridge __Hotfix_get_LineRoots;
        private static DelegateBridge __Hotfix_get_PointRoots;
        private static DelegateBridge __Hotfix_get_PointAnimates;
        private static DelegateBridge __Hotfix_get_LineAnimates;
        private static DelegateBridge __Hotfix_get_Lines;
        private static DelegateBridge __Hotfix_get_LinePointLists;

        public event Action EventOnMatcingEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float, float> EventOnTransformGestureSlipping
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void ClearData()
        {
        }

        [MethodImpl(0x8000)]
        public void CompletePuzzleGame()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCenterPos()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCompleteMatchingRate()
        {
        }

        [MethodImpl(0x8000)]
        public float GetMatchingRate()
        {
        }

        [MethodImpl(0x8000)]
        private float GetMatchingRate(Vector3 rotation)
        {
        }

        [MethodImpl(0x8000)]
        private float GetRotateOffsetInOneDirection(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetTargetSolarSystemPos()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSolarsystemLink(GDBStarfieldsInfo starfieldsInfo, GDBStargroupInfo stargroupInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSolarsystemPos(GDBStargroupInfo stargroupInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSolarsystemRotation()
        {
        }

        [MethodImpl(0x8000)]
        private void ManipulationTransformedHandler(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisable()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnable()
        {
        }

        [MethodImpl(0x8000)]
        public void RotateSolarsystem(Vector3 rotate)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCastLinePos()
        {
        }

        [MethodImpl(0x8000)]
        private void SetCompleteState()
        {
        }

        [MethodImpl(0x8000)]
        public void SetHintSolarSystem(int addHintCount)
        {
        }

        [MethodImpl(0x8000)]
        private void SetHintState()
        {
        }

        [MethodImpl(0x8000)]
        private static void SetLineEffect(List<float> linePoints, bool useWorldPos, LineRenderer line, Vector3 startPos, Vector3 endPos)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLines(PuzzleGameObj objType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMoveVoice(float rotateDegree)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPoint(PuzzleGameObj objType, Transform posRoot = null)
        {
        }

        [MethodImpl(0x8000)]
        private void SetVolume(float volume)
        {
        }

        [MethodImpl(0x8000)]
        public void StopMoveSound()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShadow()
        {
        }

        private GameObject[] LineInstantiateObjs
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private GameObject[] PointInstantiateObjs
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private Transform[] LineRoots
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private Transform[] PointRoots
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private List<Animator>[] PointAnimates
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private List<Animator>[] LineAnimates
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private List<LineRenderer>[] Lines
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private List<float>[] LinePointLists
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private enum PuzzleGameObj
        {
            SolarSystem,
            Shadow,
            Answer,
            CastLine,
            Max
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SolarSystemLinkInfo
        {
            public int m_startSolarSystemIndex;
            public int m_endSolarSystemIndex;
        }
    }
}

