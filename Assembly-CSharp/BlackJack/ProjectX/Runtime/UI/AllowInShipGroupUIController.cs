﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class AllowInShipGroupUIController : CommonPopupWndUIControllerBase
    {
        protected Dictionary<ShipType, CommonUIStateController> m_allowInShipTypeDict;
        private const string AllowInImageUIState_AllowIn = "Normal";
        private const string AllowInImageUIState_NotAllowIn = "Grey";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        protected CommonUIStateController m_stateCtrl;
        [AutoBind("./InfoGroup/FF", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeFrigate;
        [AutoBind("./InfoGroup/DD", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeDestroyer;
        [AutoBind("./InfoGroup/CA", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeCruiser;
        [AutoBind("./InfoGroup/BC", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleCruiser;
        [AutoBind("./InfoGroup/BB", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleShip;
        [AutoBind("./InfoGroup/IN", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeIndustryShip;
        [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowAllowInShipTypePanel;
        private static DelegateBridge __Hotfix_SetPostion;
        private static DelegateBridge __Hotfix_GetEffectProcess;

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetEffectProcess()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPostion(Vector2 anchorPosition)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess ShowAllowInShipTypePanel(List<ShipType> shipTypeList)
        {
        }
    }
}

