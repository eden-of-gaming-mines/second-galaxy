﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CommonDependenceAndRelativeTechItemUICtrl : UIControllerBase
    {
        protected ProjectXPlayerContext m_plyCtx;
        private static string m_levelStr;
        private const string StateDissatisfy = "Red";
        private const string StateSatisfy = "Green";
        private const string CanUpgrade = "CanUpgrade";
        private const string FullLevel = "FullLevel";
        private const string StateDissatisfyNoButton = "RedWithoutGoButton";
        private const string CanUpgradeNoButton = "CanUpgradeWithoutGoButton";
        private int m_techId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGotoButtonClick;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./IconBG/Icon", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        [AutoBind("./LvText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_lvText;
        [AutoBind("./GotoButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_skipButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateDependenceTechInfo;
        private static DelegateBridge __Hotfix_UpdateRelativeTechInfo;
        private static DelegateBridge __Hotfix_OnGotoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnGotoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoButtonClick;

        public event Action<int> EventOnGotoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDependenceTechInfo(ConfIdLevelInfo requireTechInfo, bool showGoButton, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRelativeTechInfo(int techId, bool showGoButton, Dictionary<string, Object> resDict)
        {
        }
    }
}

