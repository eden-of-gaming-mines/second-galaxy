﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class GuildListSorter
    {
        private static DelegateBridge __Hotfix_SortAllianceMember;
        private static DelegateBridge __Hotfix_SortGuildList;

        [MethodImpl(0x8000)]
        public static void SortAllianceMember(this AllianceInfo allianceInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void SortGuildList(this List<GuildSimplestInfo> guildList, List<uint> invitedGuildIds)
        {
        }

        [CompilerGenerated]
        private sealed class <SortAllianceMember>c__AnonStorey0
        {
            internal uint leaderGuildId;
            internal uint selfGuildId;

            [MethodImpl(0x8000)]
            internal int <>m__0(AllianceMemberInfo guildA, AllianceMemberInfo guildB)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SortGuildList>c__AnonStorey1
        {
            internal List<uint> invitedGuildIds;

            internal int <>m__0(GuildSimplestInfo guildA, GuildSimplestInfo guildB)
            {
                if ((guildA.m_allianceId != 0) || (guildB.m_allianceId != 0))
                {
                    return guildA.m_allianceId.CompareTo(guildB.m_allianceId);
                }
                if (this.invitedGuildIds.Contains(guildA.m_guild) && !this.invitedGuildIds.Contains(guildB.m_guild))
                {
                    return -1;
                }
                if (!this.invitedGuildIds.Contains(guildB.m_guild) || this.invitedGuildIds.Contains(guildA.m_guild))
                {
                    return guildA.m_guild.CompareTo(guildB.m_guild);
                }
                return 1;
            }
        }
    }
}

