﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SpaceStationPlayerListUIController : UIControllerBase
    {
        private SpaceStationUITask.PlayerListSortType m_currentSortType;
        public const string m_uiStateOpen = "PanelOpen";
        public const string m_uiStateClose = "PanelClose";
        private List<ProPlayerSimplestInfo> m_playerListCache;
        private Dictionary<string, Object> m_cachedResDict;
        private const string m_plyaeritemPoolName = "PlayerListItem";
        private const string m_playerItemAssetName = "PlayerListItemInfo";
        private const string m_playerItemNormalState = "normal";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, ProPlayerSimplestInfo> EventOnPlayerListItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnPlayerListRefreshButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<SpaceStationUITask.PlayerListSortType> EventOnPlayerListSortTypeChange;
        private List<ButtonEx> m_sortTypeButtonList;
        [AutoBind("./PlayerListItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_ScrollRect;
        [AutoBind("./PlayerListItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_ObjPool;
        [AutoBind("./SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SortButton;
        [AutoBind("./RefreshButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RefreshButton;
        [AutoBind("./SortTypePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SortTypeChangePanel;
        [AutoBind("./SortTypePanel/NameUpButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NameUpButton;
        [AutoBind("./SortTypePanel/NameDownButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx NameDownButton;
        [AutoBind("./SortTypePanel/LevelUpButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LevelUpButton;
        [AutoBind("./SortTypePanel/LevelDownButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LevelDownButton;
        private const string GuildCodeFormat = "[{0}]";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnSortTypeButtonClick_NameUp;
        private static DelegateBridge __Hotfix_OnSortTypeButtonClick_NameDown;
        private static DelegateBridge __Hotfix_OnSortTypeButtonClick_LevelUpButton;
        private static DelegateBridge __Hotfix_OnSortTypeButtonClick_LevelDownButton;
        private static DelegateBridge __Hotfix_OnSortTypeChange;
        private static DelegateBridge __Hotfix_OnRefreshButtonClick;
        private static DelegateBridge __Hotfix_UpdatePlayerInfo;
        private static DelegateBridge __Hotfix_ClearAllPlayerListItemSelectState;
        private static DelegateBridge __Hotfix_OnPlayerListItemFill;
        private static DelegateBridge __Hotfix_OnPlayerListItemClick;
        private static DelegateBridge __Hotfix_OnSortButtonClick;
        private static DelegateBridge __Hotfix_UpdateSortPanelUIState;
        private static DelegateBridge __Hotfix_add_EventOnPlayerListItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerListItemClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerListRefreshButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerListRefreshButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerListSortTypeChange;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerListSortTypeChange;
        private static DelegateBridge __Hotfix_get_SortTypeButtonList;

        public event Action<UIControllerBase, ProPlayerSimplestInfo> EventOnPlayerListItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPlayerListRefreshButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<SpaceStationUITask.PlayerListSortType> EventOnPlayerListSortTypeChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearAllPlayerListItemSelectState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerListItemClick(UIControllerBase uctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerListItemFill(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRefreshButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeButtonClick_LevelDownButton(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeButtonClick_LevelUpButton(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeButtonClick_NameDown(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeButtonClick_NameUp(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeChange(SpaceStationUITask.PlayerListSortType sortType)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePlayerInfo(List<ProPlayerSimplestInfo> playerInfoList, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSortPanelUIState()
        {
        }

        private List<ButtonEx> SortTypeButtonList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

