﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BaseRedeployUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStopRedeployButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnFinishRedeployButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCancelRedeployButtonClick;
        private MSRedeployInfo m_redeployInfo;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./Panel/Redeploy/PreparePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_redeployPreparedStateCtrl;
        [AutoBind("./Panel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./Panel/Redeploy/PreparePanel/InProgress/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_stopRedeployButton;
        [AutoBind("./Panel/Redeploy/PreparePanel/Complete/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_finishRedeployButton;
        [AutoBind("./Panel/Redeploy/PreparePanel/Complete/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelRedeployButton;
        [AutoBind("./Panel/Redeploy/DestinationInfoGroup/StarfieldsNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starfieldsNameText;
        [AutoBind("./Panel/Redeploy/DestinationInfoGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_solarSystemNameText;
        [AutoBind("./Panel/Redeploy/DestinationInfoGroup/StationNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_stationNameText;
        [AutoBind("./Panel/Redeploy/DestinationInfoGroup/SafeValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_securityLevelText;
        [AutoBind("./Panel/Redeploy/DestinationInfoGroup/FactionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_factionNameText;
        [AutoBind("./Panel/Redeploy/DestinationInfoGroup/DistanceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_distanceValueText;
        [AutoBind("./Panel/Redeploy/PreparePanel/InProgress/RedeployPrepareGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_redeployRemainingTimeTextForBaseUI;
        [AutoBind("./Panel/Redeploy/PreparePanel/ProgressBar/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_redeployProgressBarImageForBaseUI;
        private const string m_deployUIStateNormal = "Redeploy";
        private const string m_deployUIStateAccelerate = "Accelerate";
        private List<CommonUIStateController> m_itemGroupStateCtrlList;
        private List<CommonItemIconUIController> m_speedupItemCtrlList;
        private List<Text> m_speedupValueTextList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnSpeedUpItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnFinishRedeployByMoneyButtonClick;
        [AutoBind("./Panel/Accelerate/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_accelerateItemGroup;
        [AutoBind("./Panel/Accelerate/FinishImmediatelyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_finishRedeployButtonByMoney;
        [AutoBind("./Panel/Accelerate/FinishImmediatelyButton/Cost/MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_finishRedeployCostMoneyText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetToBaseRedeployUI;
        private static DelegateBridge __Hotfix_SetToAccelerateRedeployUI;
        private static DelegateBridge __Hotfix_GetShowWindowProcess;
        private static DelegateBridge __Hotfix_GetCloseWindowProcess;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_TickForRedeployStateInfoForUI;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted_BaseUI;
        private static DelegateBridge __Hotfix_SetRedeployTimeInfoForBaseUI;
        private static DelegateBridge __Hotfix_SetRedeployPreparedCtrlState;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnStopRedeployButtonClick;
        private static DelegateBridge __Hotfix_OnFinishRedeployButtonClick;
        private static DelegateBridge __Hotfix_OnCancelRedeployButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnStopRedeployButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnStopRedeployButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnFinishRedeployButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFinishRedeployButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelRedeployButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelRedeployButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_UpdateItemCountText;
        private static DelegateBridge __Hotfix_OnBindFieldCompleted_AccelerateUI;
        private static DelegateBridge __Hotfix_TickForAccelerateInfoUI;
        private static DelegateBridge __Hotfix_SetRedeployTimeInfoForAccelerateUI;
        private static DelegateBridge __Hotfix_SetRedeployCostMoneyInfoForAccelerateUI;
        private static DelegateBridge __Hotfix_SetSpeedUpItemInfo;
        private static DelegateBridge __Hotfix_GetSpeedUpItemIdWithIndex;
        private static DelegateBridge __Hotfix_OnRedeploySpeedUpItemClick;
        private static DelegateBridge __Hotfix_OnFinishRedeployByMoneyButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_add_EventOnFinishRedeployByMoneyButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFinishRedeployByMoneyButtonClick;

        public event Action EventOnCancelRedeployButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFinishRedeployButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFinishRedeployByMoneyButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSpeedUpItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStopRedeployButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetCloseWindowProcess(bool immediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetShowWindowProcess(Action stateStartEvent = null)
        {
        }

        [MethodImpl(0x8000)]
        private int GetSpeedUpItemIdWithIndex(int itemIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindFieldCompleted_AccelerateUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindFiledsCompleted_BaseUI()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelRedeployButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFinishRedeployButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFinishRedeployByMoneyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnRedeploySpeedUpItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStopRedeployButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRedeployCostMoneyInfoForAccelerateUI(int moneyCost)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRedeployPreparedCtrlState(bool isComplete)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRedeployTimeInfoForAccelerateUI(DateTime startTime, DateTime endTime, DateTime originalEndTime)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRedeployTimeInfoForBaseUI(DateTime startTime, DateTime endTime, DateTime originalEndTime)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSpeedUpItemInfo(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToAccelerateRedeployUI(MSRedeployInfo redeployInfo, int realMoneyCost, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToBaseRedeployUI(MSRedeployInfo redeployInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void TickForAccelerateInfoUI()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForRedeployStateInfoForUI()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemCountText(int itemIndex, int itemID)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

