﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public enum RescueSignalType
    {
        None,
        Emergency,
        GuildRescue,
        SelfRescue
    }
}

