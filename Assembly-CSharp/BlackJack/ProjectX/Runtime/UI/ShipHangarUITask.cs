﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ShipHangarUITask : UITaskBase
    {
        private List<LBStaticPlayerShipClient> m_RecommendShipTemp;
        private ShipHangarBGUITask m_bgTask;
        private CommonWeaponEquipSimpleInfoUITask m_weaponEquipSimpleInfoTask;
        private WeaponEquipSetUITask m_weaponEquipSetUITask;
        private bool m_isShipHangarBGUITaskResourceLoadComplete;
        private SceneLayerBase m_weaponEquipDetailInfoLayer;
        private SceneLayerBase m_ammoReloadLayer;
        private SceneLayerBase m_shipSalvageLayer;
        private ShipHangarUIController m_mainCtrl;
        private HangarShipListUIController m_hangarShipListUICtrl;
        private HangarShipWeaponEquipSlotPanelUIController m_weaponEquipSlotPanelUICtrl;
        private HangarShipInfoPanelUIController m_shipInfoPanelUICtrl;
        private HangarShipDetailInfoPanelUIController m_shipDetailInfoPanelCtrl;
        private HangarShipWeaponEquipDetailInfoPanelUIController m_weaponEquipDetailInfoUICtrl;
        private HangarShipAmmoReloadUIController m_ammoReloadUICtrl;
        private HangarShipLowSlotGroupAssemblyUIController m_lowSlotGroupAssemblyUICtrl;
        private AutoReloadAmmoBuyPanelUIController m_autoReloadAmmoBuyPanelUICtrl;
        private List<ILBStaticPlayerShip> m_shipHangarShipInfoCacheList;
        private DateTime m_lastShipHangerListUpdateCacheTime;
        private static int m_currSelectShipListIndex;
        protected ComingSourceType m_comingSoruceType;
        protected TipWndType m_currTipWndType;
        protected SlotEditType m_currSlotEditType;
        protected int m_currSelectSlotIndex;
        protected int m_currRefillAmmoSlotIndex;
        protected bool m_isShowBuyAmmoPanel;
        protected bool m_isShowLowSlotGroupPanel;
        protected bool m_isLongPressReleased;
        protected string m_lastMode;
        protected AuctionBuyItemFilterType m_currWeaponAuctionFilter;
        protected int m_currWeaponItemDropDownValue;
        protected AuctionBuyItemFilterType m_currAmmoAuctionFilter;
        protected AuctionBuyItemFilterType m_currDeviceAuctionFilter;
        protected int m_currDeviceItemDropDownValue;
        protected AuctionBuyItemFilterType m_currComponentAuctionFilter;
        protected int m_currComponentItemDropDownValue;
        public static string ParamKey_SelectHangarShipIndex;
        public static string ParamKey_ShipListRefreshMode;
        public static string ParamKey_IsShipDataChanged;
        public static string ParamKey_ComingSource;
        public static string ParamKey_CheckShipState;
        public static string ParamKey_IsWeaponEquipEquiped;
        public static string ParamKey_ResetSalvageUIMode;
        public static string ShipListRefreshMode_RefreshAll;
        public static string ShipListRefreshMode_RefreshSingle;
        public static string ShipListOverviewMode;
        public static string AmmoReloadMode;
        public static string WeaponEquipDetailInfoMode;
        public static string OnlyThreeDBackGroundMode;
        public static string ShipSalvageMode;
        protected static string ShipHangarUIPrefabAssetPath;
        protected static string ShipWeaponEquipDetailInfoUILayerAssetPath;
        protected static string AmmoReloadUILayerAssetPath;
        protected static string ShipSalvageUILayerAssetPath;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ShipHangarUITask";
        private HangarShipAssemblyOptimizationInfoPanelUIController m_assemblyOptimizationInfoPanelUICtrl;
        private readonly ShipAssemblyOptimizationInfo m_assemblyOptimizationInfo;
        private const float LowPowerOccupationRateThreshold = 0.35f;
        private const string LockLayerName = "ShipHangar";
        private const string LockShipConfigName = "ShipConfig";
        private const string LockShipPackName = "Pack";
        private const string LockShipAmmoReloadLayerName = "AmmoReload";
        protected bool m_needInitFuncLock;
        protected List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        private int m_currStep;
        private int m_lowSlotIndexInRepeatUserGuide;
        private int m_middleSlotIndexInRepeatUserGuide;
        private int m_ammoSlotIndexInRepeatUserGuide;
        private bool m_forceStopRepeatUserGuide;
        private int m_wanttedEditSlot;
        private HangarShipListItemUIController m_wanttedShipItemCtrl;
        private bool m_isInRepeatUserGuide;
        private ShipType m_wantedShipType;
        private ShipHangarRepeatableUserGuideType m_repeatUserGuideType;
        public static string ParameKey_IsInRepeatUserGuide;
        public static string ParamKey_RepeatUserGuideType;
        public static string ParamKey_WantedShipType;
        private HangarShipSalvageUIController m_shipSalvageUICtrl;
        private HangarShipSalvageUIController.UIMode m_shipSalvageUIMode;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_SetCurrentMode;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadFromShipList;
        private static DelegateBridge __Hotfix_CollectDynmaicResForSlotItemBackGroundSprite;
        private static DelegateBridge __Hotfix_CollectDynmaicResForForceIcon;
        private static DelegateBridge __Hotfix_CollectDynamicResForShipRank;
        private static DelegateBridge __Hotfix_CollectDynmaicResForShipBGImage;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterShipListOverviewUI;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewChangeWatchState;
        private static DelegateBridge __Hotfix_UpdateTipWndState;
        private static DelegateBridge __Hotfix_UpdateViewShipListOverviewMode;
        private static DelegateBridge __Hotfix_UpdateViewShipListOverviewModeImp;
        private static DelegateBridge __Hotfix_UpdateViewWeaponEquipDetailInfoMode;
        private static DelegateBridge __Hotfix_UpdateViewAmmoReloadMode;
        private static DelegateBridge __Hotfix_UpdateViewOnlyThreeDView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_InitPipeLineCtxStateFromUIIntent;
        private static DelegateBridge __Hotfix_CreatePipeLineCtx;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnWatchButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnShipFeaturesButtonClick;
        private static DelegateBridge __Hotfix_OnShipSuperWeaponButtonClick;
        private static DelegateBridge __Hotfix_OnShipNormalWeaponButtonClick;
        private static DelegateBridge __Hotfix_OnShipRepairButtonClick;
        private static DelegateBridge __Hotfix_OnShipBoosterButtonClick;
        private static DelegateBridge __Hotfix_OnShipTacticalEquipButtonClick;
        private static DelegateBridge __Hotfix_OnShipRenameButtonClick;
        private static DelegateBridge __Hotfix_OnShipRenameConfirmClick;
        private static DelegateBridge __Hotfix_OnShipRenameCancelClick;
        private static DelegateBridge __Hotfix_OnShipPackButtonClick;
        private static DelegateBridge __Hotfix_OnShipPackConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnShipPackCancelButtonClick;
        private static DelegateBridge __Hotfix_OnShipHangarItemStoreButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadButtonClick;
        private static DelegateBridge __Hotfix_OnShipTemplateButtonClick;
        private static DelegateBridge __Hotfix_OnShipTemplateButtonClickImp;
        private static DelegateBridge __Hotfix_OnHighSlotItemClick;
        private static DelegateBridge __Hotfix_OnHighSlotItemClickImp;
        private static DelegateBridge __Hotfix_OnMiddleSlotItemClick;
        private static DelegateBridge __Hotfix_OnMiddleSlotItemClickImp;
        private static DelegateBridge __Hotfix_OnLowSlotGroupButtonClick;
        private static DelegateBridge __Hotfix_OnLowSlotItemClick;
        private static DelegateBridge __Hotfix_OnLowSlotItemClickImp;
        private static DelegateBridge __Hotfix_OnEditSlotGroup;
        private static DelegateBridge __Hotfix_OnEditSlotGroupAmmo;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoUI;
        private static DelegateBridge __Hotfix_OnAmmoReloadCancelButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadConfirmButtonClick;
        private static DelegateBridge __Hotfix_CloseAmmoReloadUI;
        private static DelegateBridge __Hotfix_OnAmmoReloadAmmoSlotItemClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadSingleSlotRefillButtonClick;
        private static DelegateBridge __Hotfix_GetAmmoSetDataBeforeRefillAmmo;
        private static DelegateBridge __Hotfix_UpdateAmmoRelaodeCount;
        private static DelegateBridge __Hotfix_OnAmmoReloadAutoRefillAllButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadBuyPanelCancelButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadBuyPanelConfirmButtonClick;
        private static DelegateBridge __Hotfix_SendBuyAmmoRequest;
        private static DelegateBridge __Hotfix_OnAmmoReloadBuyPanelAmmoItemClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoChangeButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoDetailButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoShareButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoCountChange;
        private static DelegateBridge __Hotfix_OnLowSlotAssemblyCancelButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipSimpleInfoPanelDetailButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipSimpleInfoPanelRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipSimpleInfoPanelReplaceButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelReplaceButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelReplaceButtonClickImp;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoPanelRemoveButtonClick;
        private static DelegateBridge __Hotfix_OnWeaponEquipDetailInfoToggleChanged;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_OnHangarShipListItemClick;
        private static DelegateBridge __Hotfix_OnHangarShipListItemFill;
        private static DelegateBridge __Hotfix_OnToDriverLicenseButtonClick;
        private static DelegateBridge __Hotfix_OnErrorInfoPanelConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnErrorInfoPanelCancelButtonClick;
        private static DelegateBridge __Hotfix_OnErrorInfoPanelGoButtonClick;
        private static DelegateBridge __Hotfix_OnLongPressed;
        private static DelegateBridge __Hotfix_OnLongPressReleased;
        private static DelegateBridge __Hotfix_OnLongPressReleasedImpl;
        private static DelegateBridge __Hotfix_GetHangarIndexFromIndex;
        private static DelegateBridge __Hotfix_GetPropertyValue;
        private static DelegateBridge __Hotfix_BlockBGInput;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_SetPipeLineStateToRefreshAll;
        private static DelegateBridge __Hotfix_SetPipeLineStateToRefreshSingle;
        private static DelegateBridge __Hotfix_SetPipeLineStateToAmmoDataChanged;
        private static DelegateBridge __Hotfix_SetPipeLineStateToLowSlotGroupDataChanged;
        private static DelegateBridge __Hotfix_SetPipeLineStateToSelectOtherShip;
        private static DelegateBridge __Hotfix_GetNextShipSlotUnLockLevel;
        private static DelegateBridge __Hotfix_HideWeaponEquipInfoPanel;
        private static DelegateBridge __Hotfix_CreateShowWeaponEquipInfoPanelProcess;
        private static DelegateBridge __Hotfix_ShowWeaponEquipInfoPanel;
        private static DelegateBridge __Hotfix_ShowCurrWeaponCPUAndPowerEffect;
        private static DelegateBridge __Hotfix_HideCurrWeaponCPUAndPowerEffect;
        private static DelegateBridge __Hotfix_AddHideLowSlotProcessToMainProcess;
        private static DelegateBridge __Hotfix_ShowLowSlotAssemblyPanel;
        private static DelegateBridge __Hotfix_IsAbleToShipDrivingLicense;
        private static DelegateBridge __Hotfix_IsStayWithMotherShip;
        private static DelegateBridge __Hotfix_OnShipHangarBGUITaskAllResourceLoadComplete;
        private static DelegateBridge __Hotfix_GetShipHangarShipInfoSortedList;
        private static DelegateBridge __Hotfix_ShipHangarShipInfoComparer;
        private static DelegateBridge __Hotfix_GetEditSlotGroup;
        private static DelegateBridge __Hotfix_StartWeaponEquipSetUITask;
        private static DelegateBridge __Hotfix_StartWeaponEquipSetUITaskImp;
        private static DelegateBridge __Hotfix_OnReturnFromWeaponEquipSetUITask;
        private static DelegateBridge __Hotfix_StartDrivingLicenseInfoUITask;
        private static DelegateBridge __Hotfix_CheckIsCurrShipCanAutoReloadAmmo;
        private static DelegateBridge __Hotfix_GetSuitableShipListStartIndexFromCurrSelectIndex;
        private static DelegateBridge __Hotfix_GetAmmoReloadTotalCountInStore;
        private static DelegateBridge __Hotfix_CheckShipState;
        private static DelegateBridge __Hotfix_CheckShipWeaponEquipSlotState;
        private static DelegateBridge __Hotfix_GetShipWeaponEquipSlotUnlockDriveingLicenseLevel;
        private static DelegateBridge __Hotfix_CheckShipDrivingLicenseRequest;
        private static DelegateBridge __Hotfix_CheckIsOperatorNeedReturnBase;
        private static DelegateBridge __Hotfix_CheckShipCostRequest;
        private static DelegateBridge __Hotfix_CheckShipName;
        private static DelegateBridge __Hotfix_ShowWeaponEquipEquipedEffect;
        private static DelegateBridge __Hotfix_ClearWeaponEquipSetFilter;
        private static DelegateBridge __Hotfix_CheckShipDestoryed;
        private static DelegateBridge __Hotfix_StartRecommendData;
        private static DelegateBridge __Hotfix_GetRecommendFlag;
        private static DelegateBridge __Hotfix_GetShipRecommendFlag;
        private static DelegateBridge __Hotfix_CreateShipHangarBgBackGroudManager;
        private static DelegateBridge __Hotfix_get_CurrPipeLineCtx;
        private static DelegateBridge __Hotfix_set_CurrPipeLineCtx;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_UpdatAssemblyOptimizationState;
        private static DelegateBridge __Hotfix_CollectAssemblyOptimizationInformation;
        private static DelegateBridge __Hotfix_IsAssemblyOptimizable;
        private static DelegateBridge __Hotfix_OnAssemblyOptimizationNoticeButtonClick;
        private static DelegateBridge __Hotfix_OnAssmeblyOptimizationInfoPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableShipHangarConfig;
        private static DelegateBridge __Hotfix_UnLockShipHangarConfig;
        private static DelegateBridge __Hotfix_EnableShipPack;
        private static DelegateBridge __Hotfix_UnLockShipPack;
        private static DelegateBridge __Hotfix_EnableAmmoReload;
        private static DelegateBridge __Hotfix_UnLockAmmoReload;
        private static DelegateBridge __Hotfix_ShowOrHideFunctionLock;
        private static DelegateBridge __Hotfix_StartRepeatUserGuide;
        private static DelegateBridge __Hotfix_StartMiddleSlotSetRepeatUserGuide;
        private static DelegateBridge __Hotfix_StartLowSlotSetRepeatUserGuide;
        private static DelegateBridge __Hotfix_StartAmmoSetRepeatUserGuide;
        private static DelegateBridge __Hotfix_StartShipAddRepeatUserGuide;
        private static DelegateBridge __Hotfix_StartShipSpaceSignalScanRepeatUserGuide;
        private static DelegateBridge __Hotfix_StopRepeatUserGuide;
        private static DelegateBridge __Hotfix_UpdateViewShipSalvage;
        private static DelegateBridge __Hotfix_RegisterShipSalvageUIEvent;
        private static DelegateBridge __Hotfix_OnDisposeShipButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnTipButtonClick;
        private static DelegateBridge __Hotfix_OnTipBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnSalavageButtonClick;
        private static DelegateBridge __Hotfix_OnAbandonSalavageButtonClick;
        private static DelegateBridge __Hotfix_OnAddSalvageDailyCountButtonClick;
        private static DelegateBridge __Hotfix_OnKillRecordButtonClick;
        private static DelegateBridge __Hotfix_OnCreateCompensationButtonClick;
        private static DelegateBridge __Hotfix_OnRemoveDestroyShipButtonClick;
        private static DelegateBridge __Hotfix_OnUseSalvageItemConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnUseSalvageItemCancelButtonClick;
        private static DelegateBridge __Hotfix_OnBuySalvageItemConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBuySalvageItemCancelButtonClick;
        private static DelegateBridge __Hotfix_OnRealMoneyAddButtonClick;
        private static DelegateBridge __Hotfix_SendShipSalvageReq;
        private static DelegateBridge __Hotfix_OnShipSalvageSucceed;
        private static DelegateBridge __Hotfix_OnAbandonSalvageShipSucceed;
        private static DelegateBridge __Hotfix_BuyMothcardOrShipSalvageItem;
        private static DelegateBridge __Hotfix_OnHangarShipSalvageCostError;
        private static DelegateBridge __Hotfix_GotoRecharge;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_GetFirstHighSlotWeaponRect;
        private static DelegateBridge __Hotfix_GetFirstEmptyHangarSpace;
        private static DelegateBridge __Hotfix_GetShipInListById;
        private static DelegateBridge __Hotfix_ClickShipWeaponEquipSlot;
        private static DelegateBridge __Hotfix_ClickShipWeaponEquipReplaceButton;
        private static DelegateBridge __Hotfix_ClickFirstEmptyShipSlot;
        private static DelegateBridge __Hotfix_ClickShipTemplateButton;
        private static DelegateBridge __Hotfix_HasEmptyShipSlot;

        [MethodImpl(0x8000)]
        public ShipHangarUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected void AddHideLowSlotProcessToMainProcess(UIProcess mainProcess, bool immediateComplete, bool allowToRefreshSameState, bool isCustomProcess = false, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void BlockBGInput(bool isBlock)
        {
        }

        [MethodImpl(0x8000)]
        private void BuyMothcardOrShipSalvageItem(bool useSimpleBuyPanel)
        {
        }

        [MethodImpl(0x8000)]
        protected bool CheckIsCurrShipCanAutoReloadAmmo(ILBStaticShip staticShip)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckIsOperatorNeedReturnBase()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipCostRequest(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipDestoryed(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipDrivingLicenseRequest(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipName(string shipName)
        {
        }

        [MethodImpl(0x8000)]
        protected void CheckShipState(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected void CheckShipWeaponEquipSlotState(ILBStaticPlayerShip ship, SlotEditType slotType, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected void ClearWeaponEquipSetFilter()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstEmptyShipSlot(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickShipTemplateButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickShipWeaponEquipReplaceButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickShipWeaponEquipSlot(ShipEquipSlotType slotType, int index, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseAmmoReloadUI()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoUI(bool blockUI = true, Action<UIProcess, bool> onProcessEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectAssemblyOptimizationInformation()
        {
        }

        [MethodImpl(0x8000)]
        private List<string> CollectDynamicResForLoadFromShipList()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResForShipRank(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynmaicResForForceIcon(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynmaicResForShipBGImage(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynmaicResForSlotItemBackGroundSprite(List<string> resPath)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected override UITaskPipeLineCtx CreatePipeLineCtx()
        {
        }

        [MethodImpl(0x8000)]
        private ShipHangarBGUIBackgroundManager CreateShipHangarBgBackGroudManager()
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess CreateShowWeaponEquipInfoPanelProcess()
        {
        }

        [MethodImpl(0x8000)]
        private void EnableAmmoReload(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableShipHangarConfig(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableShipPack(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected long GetAmmoReloadTotalCountInStore(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void GetAmmoSetDataBeforeRefillAmmo(out List<int> ammoCountList, int slotIndex, out int currChangeCount)
        {
        }

        [MethodImpl(0x8000)]
        protected LBStaticWeaponEquipSlotGroup GetEditSlotGroup(SlotEditType editType, int index)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstEmptyHangarSpace()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstHighSlotWeaponRect()
        {
        }

        [MethodImpl(0x8000)]
        public int GetHangarIndexFromIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetNextShipSlotUnLockLevel(int currShipSlotCount)
        {
        }

        [MethodImpl(0x8000)]
        public float GetPropertyValue(int index, PropertiesId Id)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetRecommendFlag(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        private List<ILBStaticPlayerShip> GetShipHangarShipInfoSortedList()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetShipInListById(int id)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetShipRecommendFlag(ILBStaticPlayerShip shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetShipWeaponEquipSlotUnlockDriveingLicenseLevel(ILBStaticPlayerShip ship, LBStaticWeaponEquipSlotGroup slotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuitableShipListStartIndexFromCurrSelectIndex(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void GotoRecharge()
        {
        }

        [MethodImpl(0x8000)]
        public bool HasEmptyShipSlot()
        {
        }

        [MethodImpl(0x8000)]
        protected void HideCurrWeaponCPUAndPowerEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void HideWeaponEquipInfoPanel(bool isImmediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPipeLineCtxStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsAbleToShipDrivingLicense()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsAssemblyOptimizable()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadStaticRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsStayWithMotherShip()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAbandonSalavageButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAbandonSalvageShipSucceed()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddSalvageDailyCountButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoChangeButtonClick(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoCountChange(long ammoCount, int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadAmmoSlotItemClick(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadAutoRefillAllButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        public void OnAmmoReloadButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadBuyPanelAmmoItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadBuyPanelCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadBuyPanelConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadSingleSlotRefillButtonClick(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoShareButtonClick(ILBStoreItemClient lbItem)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAssemblyOptimizationNoticeButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAssmeblyOptimizationInfoPanelBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuySalvageItemCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuySalvageItemConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCreateCompensationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisposeShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditSlotGroup(LBStaticWeaponEquipSlotGroup slotGroup, bool isEditTheSameSlot, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEditSlotGroupAmmo(LBStaticWeaponEquipSlotGroup slotGroup)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnErrorInfoPanelCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnErrorInfoPanelConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnErrorInfoPanelGoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarShipListItemClick(UIControllerBase uICtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarShipListItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHangarShipSalvageCostError()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHighSlotItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHighSlotItemClickImp(int index, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKillRecordButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressed()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLongPressReleased()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLongPressReleasedImpl()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotAssemblyCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotGroupButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotItemClickImp(int index, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMiddleSlotItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMiddleSlotItemClickImp(int index, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRealMoneyAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRemoveDestroyShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnReturnFromWeaponEquipSetUITask(AuctionBuyItemFilterType filterType, int dropDownIndex, string returnTaskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSalavageButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipBoosterButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipFeaturesButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipHangarBGUITaskAllResourceLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipHangarItemStoreButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipNormalWeaponButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipPackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipPackCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipPackConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipRenameButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipRenameCancelClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipRenameConfirmClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipRepairButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipSalvageSucceed(int hangarIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipSuperWeaponButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipTacticalEquipButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipTemplateButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnShipTemplateButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToDriverLicenseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUseSalvageItemCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUseSalvageItemConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWatchButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelRemoveButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelReplaceButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoPanelReplaceButtonClickImp(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipDetailInfoToggleChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipSimpleInfoPanelDetailButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipSimpleInfoPanelRemoveButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnWeaponEquipSimpleInfoPanelReplaceButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterShipListOverviewUI()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterShipSalvageUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void SendBuyAmmoRequest(int shipIndex, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void SendShipSalvageReq(bool isSalvage, int hangarIndex, Action<bool> onEnd, Action onMoneyNotEnough = null, bool needCheck = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool SetCurrentMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetPipeLineStateToAmmoDataChanged()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetPipeLineStateToLowSlotGroupDataChanged()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetPipeLineStateToRefreshAll()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetPipeLineStateToRefreshSingle(bool dataChanged)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetPipeLineStateToSelectOtherShip()
        {
        }

        [MethodImpl(0x8000)]
        private int ShipHangarShipInfoComparer(ILBStaticPlayerShip shipA, ILBStaticPlayerShip shipB)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowCurrWeaponCPUAndPowerEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected UIProcess ShowLowSlotAssemblyPanel(bool immediateComplete, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowOrHideFunctionLock(bool showLock)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowWeaponEquipEquipedEffect()
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowWeaponEquipInfoPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartAmmoSetRepeatUserGuide(out RectTransform operateRect, out Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartDrivingLicenseInfoUITask()
        {
        }

        [MethodImpl(0x8000)]
        private bool StartLowSlotSetRepeatUserGuide(out RectTransform operateRect, out Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartMiddleSlotSetRepeatUserGuide(out RectTransform operateRect, out Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRecommendData()
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private bool StartShipAddRepeatUserGuide(out RectTransform operateRect, out Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartShipSpaceSignalScanRepeatUserGuide(out RectTransform operateRect, out Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartWeaponEquipSetUITask(LBStaticWeaponEquipSlotGroup slotGroup, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartWeaponEquipSetUITaskImp(LBStaticWeaponEquipSlotGroup slotGroup, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StopRepeatUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockAmmoReload(Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockShipHangarConfig(Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockShipPack(Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatAssemblyOptimizationState()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAmmoRelaodeCount(int slotCount, int slotIndex = -1, int currChangeCount = 0, bool isNeedUpdatePipeline = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateTipWndState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewAmmoReloadMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewChangeWatchState()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewOnlyThreeDView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewShipListOverviewMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewShipListOverviewModeImp()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewShipSalvage()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewWeaponEquipDetailInfoMode()
        {
        }

        protected BlackJack.ProjectX.Runtime.UI.ShipHangarUITaskPipeLineCtx CurrPipeLineCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnAmmoChangeButtonClick>c__AnonStorey6
        {
            internal int slotIndex;
            internal ShipHangarUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.StartWeaponEquipSetUITask(this.$this.m_shipHangarShipInfoCacheList[ShipHangarUITask.m_currSelectShipListIndex].GetLBWeaponEquip().GetHighSlotGroup(this.slotIndex), null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnAmmoReloadSingleSlotRefillButtonClick>c__AnonStorey3
        {
            internal int slotIndex;
            internal ShipHangarUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                List<int> list;
                <OnAmmoReloadSingleSlotRefillButtonClick>c__AnonStorey4 storey = new <OnAmmoReloadSingleSlotRefillButtonClick>c__AnonStorey4 {
                    <>f__ref$3 = this,
                    currShip = this.$this.m_shipHangarShipInfoCacheList[ShipHangarUITask.m_currSelectShipListIndex]
                };
                this.$this.GetAmmoSetDataBeforeRefillAmmo(out list, this.slotIndex, out storey.currChangeCount);
                HangarShipSetAmmoNetTask task = new HangarShipSetAmmoNetTask(((LBStaticShipBase) storey.currShip).GetHangarIndex(), list);
                task.EventOnStop += new Action<Task>(storey.<>m__0);
                task.Start(null, null);
            }

            private sealed class <OnAmmoReloadSingleSlotRefillButtonClick>c__AnonStorey4
            {
                internal ILBStaticPlayerShip currShip;
                internal int currChangeCount;
                internal ShipHangarUITask.<OnAmmoReloadSingleSlotRefillButtonClick>c__AnonStorey3 <>f__ref$3;

                internal void <>m__0(Task task)
                {
                    AmmoInfo info;
                    long num;
                    LBStaticWeaponEquipSlotGroup highSlotGroup = this.currShip.GetLBWeaponEquip().GetHighSlotGroup(this.<>f__ref$3.slotIndex);
                    int num2 = this.currShip.GetLBWeaponEquipSetupEnv().CheckReloadAmmoUseBindMoney(highSlotGroup, out info, out num);
                    if ((info != null) && (info.m_ammoConfigId != 0))
                    {
                        this.<>f__ref$3.$this.m_currRefillAmmoSlotIndex = this.<>f__ref$3.slotIndex;
                        this.<>f__ref$3.$this.CurrPipeLineCtx.m_ammoReloadSlotIndex = this.<>f__ref$3.slotIndex;
                        this.<>f__ref$3.$this.EnablePipelineStateMask(ShipHangarUITask.PipeLineStateMaskType.IsShowAmmoBuyPanel);
                        this.<>f__ref$3.$this.EnablePipelineStateMask(ShipHangarUITask.PipeLineStateMaskType.IsAutoRefillSingleAmmo);
                        this.<>f__ref$3.$this.m_isShowBuyAmmoPanel = true;
                        this.<>f__ref$3.$this.UpdateAmmoRelaodeCount(this.currShip.GetLBWeaponEquip().GetHighSlotGroupCount(), this.<>f__ref$3.slotIndex, this.currChangeCount, true);
                    }
                    else
                    {
                        this.<>f__ref$3.$this.UpdateAmmoRelaodeCount(this.currShip.GetLBWeaponEquip().GetHighSlotGroupCount(), this.<>f__ref$3.slotIndex, this.currChangeCount, true);
                        if ((num2 == 0) && FunctionOpenStateUtil.CheckFunctionOpenNotIncludeAnim(SystemFuncType.SystemFuncType_TradeCenter, true))
                        {
                            this.<>f__ref$3.$this.CurrPipeLineCtx.m_returnFromTrading = true;
                            AmmoInfo ammoInfo = highSlotGroup.GetAmmoInfo();
                            ShipHangarBGUIBackgroundManager backgroundManager = this.<>f__ref$3.$this.CreateShipHangarBgBackGroudManager();
                            AuctionUITask.StartAuctionUITaskWithPrepare(this.<>f__ref$3.$this.m_currIntent, ammoInfo.m_ammoType, ammoInfo.m_ammoConfigId, delegate (bool res) {
                                if (res)
                                {
                                    this.<>f__ref$3.$this.Pause();
                                    this.<>f__ref$3.$this.m_bgTask.SetCurrSelectHangarShip(null, false);
                                }
                            }, null, backgroundManager);
                        }
                    }
                }

                internal void <>m__1(bool res)
                {
                    if (res)
                    {
                        this.<>f__ref$3.$this.Pause();
                        this.<>f__ref$3.$this.m_bgTask.SetCurrSelectHangarShip(null, false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnBuySalvageItemConfirmButtonClick>c__AnonStoreyB
        {
            internal int hangarIndex;
            internal ShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.OnShipSalvageSucceed(this.hangarIndex);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnCreateCompensationButtonClick>c__AnonStorey9
        {
            internal KillRecordInfo killRecord;
            private static Action<Task> <>f__am$cache0;

            internal void <>m__0()
            {
                GuildCompensationCreateNet net = new GuildCompensationCreateNet(this.killRecord.m_instanceId);
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = delegate (Task task) {
                        GuildCompensationCreateNet net = task as GuildCompensationCreateNet;
                        if (net != null)
                        {
                            if (net.Ack.Result != 0)
                            {
                                TipWindowUITask.ShowTipWindowWithErrorCode(net.Ack.Result, true, false);
                            }
                            else
                            {
                                TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_GuildCompensationIssueSucess, false, new object[0]);
                            }
                        }
                    };
                }
                net.EventOnStop += <>f__am$cache0;
                net.Start(null, null);
            }

            private static void <>m__1(Task task)
            {
                GuildCompensationCreateNet net = task as GuildCompensationCreateNet;
                if (net != null)
                {
                    if (net.Ack.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(net.Ack.Result, true, false);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_GuildCompensationIssueSucess, false, new object[0]);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSalavageButtonClick>c__AnonStorey8
        {
            internal int hangarIndex;
            internal ShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.OnShipSalvageSucceed(this.hangarIndex);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnShipHangarItemStoreButtonClick>c__AnonStorey1
        {
            internal ILBStaticPlayerShip staticShip;
            internal ShipHangarUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                ((UIIntentCustom) this.$this.m_currIntent).SetParam(ShipHangarUITask.ParamKey_ShipListRefreshMode, ShipHangarUITask.ShipListRefreshMode_RefreshSingle);
                ((UIIntentCustom) this.$this.m_currIntent).SetParam(ShipHangarUITask.ParamKey_IsShipDataChanged, true);
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.m_currIntent, "ShipHangarItemStoreUITask", null);
                intent.SetParam(ShipHangarItemStoreUITask.ParamKey_IsRefreshAll, true);
                intent.SetParam(ShipHangarItemStoreUITask.ParamKey_HangarShipIndex, ((LBStaticShipBase) this.staticShip).GetHangarIndex());
                if (UIManager.Instance.StartUITask(intent, true, false, null, null) == null)
                {
                    Debug.LogError("OnShipHangarItemStoreButtonClick: start ShipHangarItemStoreUITask failed");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnShipTemplateButtonClickImp>c__AnonStorey2
        {
            internal ILBStaticPlayerShip currShip;
            internal Action<bool> onEnd;
            internal ShipHangarUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                ((UIIntentCustom) this.$this.m_currIntent).SetParam(ShipHangarUITask.ParamKey_ShipListRefreshMode, ShipHangarUITask.ShipListRefreshMode_RefreshSingle);
                ((UIIntentCustom) this.$this.m_currIntent).SetParam(ShipHangarUITask.ParamKey_IsShipDataChanged, true);
                ShipCustomTemplateUITask.StartShipCustomTemplateUITask(this.currShip, this.$this.m_currIntent, this.onEnd, this.$this.CreateShipHangarBgBackGroudManager());
            }
        }

        [CompilerGenerated]
        private sealed class <OnUseSalvageItemConfirmButtonClick>c__AnonStoreyA
        {
            internal int hangarIndex;
            internal ShipHangarUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.OnShipSalvageSucceed(this.hangarIndex);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendBuyAmmoRequest>c__AnonStorey5
        {
            internal HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask netTask;
            internal int slotIndex;
            internal ShipHangarUITask $this;

            internal void <>m__0(Task task)
            {
                HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask netTask = this.netTask;
                if (!netTask.IsNetworkError)
                {
                    if (netTask.AckResult != 0)
                    {
                        Debug.LogError($"OnAmmoReloadBuyPanelConfirmButtonClick error: HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask
                        ackResult != 0, errCode= {netTask.AckResult}");
                        TipWindowUITask.ShowTipWindowWithErrorCode(netTask.AckResult, true, false);
                    }
                    else
                    {
                        this.$this.m_isShowBuyAmmoPanel = false;
                        this.$this.CurrPipeLineCtx.m_ammoReloadAddCount = netTask.m_ammoAddCount;
                        this.$this.CurrPipeLineCtx.m_ammoReloadSlotIndex = this.slotIndex;
                        this.$this.CurrPipeLineCtx.m_ammoReloadTotalCount = this.$this.GetAmmoReloadTotalCountInStore(this.slotIndex);
                        this.$this.EnablePipelineStateMask(ShipHangarUITask.PipeLineStateMaskType.IsUpdateAmmoReloadSingleSlot);
                        this.$this.StartUpdatePipeLine(null, true, false, true, null);
                        this.$this.m_currRefillAmmoSlotIndex = -1;
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendShipSalvageReq>c__AnonStoreyC
        {
            internal Action<bool> onEnd;
            internal Action onMoneyNotEnough;

            internal void <>m__0(Task task)
            {
                HangarShipSalvageReqNetTask task2 = task as HangarShipSalvageReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.AckResult == 0)
                    {
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                    else if (task2.AckResult == -215)
                    {
                        if (this.onMoneyNotEnough != null)
                        {
                            this.onMoneyNotEnough();
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartWeaponEquipSetUITask>c__AnonStorey7
        {
            internal LBStaticWeaponEquipSlotGroup slotGroup;
            internal Action<bool> onEnd;
            internal ShipHangarUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.StartWeaponEquipSetUITaskImp(this.slotGroup, this.onEnd);
            }

            internal void <>m__1(UIProcess p, bool r)
            {
                this.$this.StartWeaponEquipSetUITaskImp(this.slotGroup, this.onEnd);
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateViewAmmoReloadMode>c__AnonStorey0
        {
            internal bool isUpdateAmmoReloadSingleSlot;
            internal ILBStaticPlayerShip currShip;
            internal int m_ammoReloadSlotIndex;
            internal int m_ammoReloadAddCount;
            internal long m_ammoReloadTotalCount;
            internal bool isUpdateAmmoReloadAllSlot;
            internal List<int> m_autoReloadAmmoAddCountList;
            internal List<long> m_ammoReloadTotalCountList;
            internal bool m_isWeaponEquiped;
            internal ShipHangarUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.m_weaponEquipDetailInfoUICtrl.gameObject.SetActive(false);
            }

            internal void <>m__1()
            {
                if (this.isUpdateAmmoReloadSingleSlot)
                {
                    this.$this.m_ammoReloadUICtrl.UpdateSingleleAmmoReload(this.currShip, this.m_ammoReloadSlotIndex, this.m_ammoReloadAddCount, this.m_ammoReloadTotalCount, this.$this.m_dynamicResCacheDict);
                }
                else if (this.isUpdateAmmoReloadAllSlot)
                {
                    this.$this.m_ammoReloadUICtrl.UpdateAmmoReload(this.currShip, this.m_autoReloadAmmoAddCountList, this.m_ammoReloadTotalCountList, this.$this.m_dynamicResCacheDict);
                }
                if (this.m_isWeaponEquiped)
                {
                    this.$this.PostDelayTimeExecuteAction(new Action(this.$this.ShowWeaponEquipEquipedEffect), 1f);
                }
            }

            internal void <>m__2(UIProcess p, bool r)
            {
                this.$this.StartRepeatUserGuide();
            }
        }

        public enum ComingSourceType
        {
            SpaceStation,
            ShipStrike
        }

        protected enum PipeLineStateMaskType
        {
            IsNeedLoadDynamicRes,
            IsOnlyChangeShipOverViewWatchState,
            IsOnlyShowTipWnd,
            IsEditTheSameSlot,
            IsRefreshShipList,
            IsResetSelectHangarShipIndex,
            IsRefreshSingleShip,
            IsUpdateShipListItemSelected,
            IsUpdateShipInfoPanel,
            IsUpdateShipDetailInfoPanel,
            IsUpdateAmmoReloadAllSlot,
            IsUpdateAmmoReloadSingleSlot,
            IsShipDetailShowRadar,
            IsUpdateWeaponEquipSlotPanel,
            ChangeLowSlotAssemblyPanelState,
            IsShowShipRenamePanel,
            IsShowShipPackPanel,
            IsShowAmmoBuyPanel,
            IsAutoRefillSingleAmmo,
            IsCheckShipState,
            IsCheckShipWeaponEquipSlotState,
            IsWeaponEquipEquiped,
            IsUpdateAssemblyOptimizationState
        }

        public class ShipAssemblyOptimizationInfo
        {
            public List<int> m_emptyHightSlotIndexList = new List<int>();
            public List<int> m_unreasonableHightSlotIndexList = new List<int>();
            public List<int> m_emptyMidSlotIndexList = new List<int>();
            public List<int> m_unreasonableMidSlotIndexList = new List<int>();
            public bool m_lowSlotGroupContainsEmptySlot;
            public bool m_lowPowerUtilization;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Clear;
            private static DelegateBridge __Hotfix_IsOptimizable;

            public ShipAssemblyOptimizationInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public void Clear()
            {
                DelegateBridge bridge = __Hotfix_Clear;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    this.m_emptyHightSlotIndexList.Clear();
                    this.m_unreasonableHightSlotIndexList.Clear();
                    this.m_emptyMidSlotIndexList.Clear();
                    this.m_unreasonableMidSlotIndexList.Clear();
                    this.m_lowSlotGroupContainsEmptySlot = false;
                    this.m_lowPowerUtilization = false;
                }
            }

            public bool IsOptimizable()
            {
                int lowPowerUtilization;
                DelegateBridge bridge = __Hotfix_IsOptimizable;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                if (((this.m_emptyHightSlotIndexList.Count != 0) || ((this.m_unreasonableHightSlotIndexList.Count != 0) || ((this.m_emptyMidSlotIndexList.Count != 0) || (this.m_unreasonableMidSlotIndexList.Count != 0)))) || this.m_lowSlotGroupContainsEmptySlot)
                {
                    lowPowerUtilization = 1;
                }
                else
                {
                    lowPowerUtilization = (int) this.m_lowPowerUtilization;
                }
                return (bool) lowPowerUtilization;
            }
        }

        protected enum SlotEditType
        {
            None,
            HighSlot,
            MiddleSlot,
            LowSlot,
            Ammo
        }

        protected enum TipWndType
        {
            NoTipWnd,
            SuperWeaponTipWnd,
            NormalWeaponTipWnd,
            RepairTipWnd,
            BoosterTipWnd,
            TacticalEquipTipWnd,
            ShipFeatureTipWnd,
            WeaponEquipInfoTipWnd
        }
    }
}

