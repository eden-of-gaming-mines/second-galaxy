﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class HangarShipRemoveWeaponEquip2SlotReqNetTask : NetWorkTransactionTask
    {
        private int m_hangarIndex;
        private int m_slotGroupType;
        private int m_slotGroupIndex;
        private int m_updateItemCount;
        private int m_ackResult;
        public bool m_isItemStoreContextChanged;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnHangarShipRemoveWeaponEquip2SlotAck;
        private static DelegateBridge __Hotfix_get_AckResult;
        private static DelegateBridge __Hotfix_set_AckResult;

        [MethodImpl(0x8000)]
        public HangarShipRemoveWeaponEquip2SlotReqNetTask(int hangarIndex, int slotGroupType, int slotGroupIndex, int updateItemCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHangarShipRemoveWeaponEquip2SlotAck(int result, bool isItemStoreContextChanged)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int AckResult
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

