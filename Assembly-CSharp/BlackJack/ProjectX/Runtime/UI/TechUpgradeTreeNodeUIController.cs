﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class TechUpgradeTreeNodeUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <TechId>k__BackingField;
        private TechUpgradeInfo m_upgradeInfo;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TechItemButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TechItemStateCtrl;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image TechIconImage;
        [AutoBind("./ProcessBar/ProcessBarBg", AutoBindAttribute.InitState.NotInit, false)]
        public Image BlackCoverImage;
        [AutoBind("./ProcessBar/ProcessBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LineCoverImage;
        [AutoBind("./SkillLevelNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TechLvNumberText;
        [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UpgradeTimeText;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SkillNameText;
        private static DelegateBridge __Hotfix_SetNodeState;
        private static DelegateBridge __Hotfix_SetTechInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_get_TechId;
        private static DelegateBridge __Hotfix_set_TechId;
        private static DelegateBridge __Hotfix_ChechDependTechList;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        protected bool ChechDependTechList(int techId, int level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetNodeState(TechTreeNodeState nodeState)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTechInfo(ConfigDataTechInfo confTechInfo, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        public int TechId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum TechTreeNodeState
        {
            Normal,
            Grey,
            Upgrade,
            GreyAdd,
            NormalAdd
        }
    }
}

