﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemShipInfoUIController : UIControllerBase
    {
        private int m_rangeMax;
        private int m_shield;
        private int m_shieldEx;
        private int m_armor;
        private int m_electric;
        private int m_fuel;
        private double m_speed;
        [AutoBind("./ScrollView/Viewport/Content/DPS/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DPSText;
        [AutoBind("./ScrollView/Viewport/Content/RangeMax/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RangeMaxText;
        [AutoBind("./ScrollView/Viewport/Content/Shield/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShieldText;
        [AutoBind("./ScrollView/Viewport/Content/Armor/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ArmorText;
        [AutoBind("./ScrollView/Viewport/Content/Electric/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElectricText;
        [AutoBind("./ScrollView/Viewport/Content/Speed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText SpeedText;
        [AutoBind("./ScrollView/Viewport/Content/DamageCompose/ElectronicDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ElectronicDamageProgressBar;
        [AutoBind("./ScrollView/Viewport/Content/DamageCompose/ElectronicDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ElectronicDamageValueText;
        [AutoBind("./ScrollView/Viewport/Content/DamageCompose/KineticDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image KineticDamageProgressBar;
        [AutoBind("./ScrollView/Viewport/Content/DamageCompose/KineticDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text KineticDamageValueText;
        [AutoBind("./ScrollView/Viewport/Content/DamageCompose/HeatDamage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image HeatDamageProgressBar;
        [AutoBind("./ScrollView/Viewport/Content/DamageCompose/HeatDamage/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HeatDamageValueText;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        private static DelegateBridge __Hotfix_UpdateShipInfo;
        private static DelegateBridge __Hotfix_UpdateFlagShipInfo;
        private static DelegateBridge __Hotfix_UpdateShipDPS;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public void UpdateFlagShipInfo(int rangeMax, int shield, int shieldEx, int armor, int fuel, double speed, float electronicDamageProgressBar, float kineticDamageProgressBar, float heatDamageProgressBar)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipDPS(int DPS)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipInfo(int rangeMax, int shield, int shieldEx, int armor, int electric, double speed, float electronicDamageProgressBar, float kineticDamageProgressBar, float heatDamageProgressBar)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

