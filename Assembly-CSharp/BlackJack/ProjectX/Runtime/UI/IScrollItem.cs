﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    public interface IScrollItem
    {
        int GetItemIndex();
        void RegisterItemNeedFillEvent(Action<UIControllerBase> action);
        void SetItemIndex(int index);
        void UnregisterItemNeedFillEvent(Action<UIControllerBase> action);
    }
}

