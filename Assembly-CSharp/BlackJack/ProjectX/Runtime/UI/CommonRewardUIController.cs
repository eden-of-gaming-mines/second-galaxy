﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CommonRewardUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnRewardItem3DTouch;
        protected List<CommonItemIconUIController> m_rewardItemCtrlList;
        private EasyObjectPool m_easyObjectPool;
        protected const string QuestRewardItemPrefabName = "CommonItemUIPrefab";
        private GameObject commonItemIconPrefab;
        [AutoBind("./RewardItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RewardItemGroup;
        [AutoBind("./ItemMode", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ItemMode;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetEasyPool;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetMineralRewardInfo;
        private static DelegateBridge __Hotfix_SetMonsterDropInfo;
        private static DelegateBridge __Hotfix_SetItemDropInfoByDropId;
        private static DelegateBridge __Hotfix_SetItemDropInfoByDropConfig;
        private static DelegateBridge __Hotfix_SetPVPSignalReward;
        private static DelegateBridge __Hotfix_GetChildCommonItemIconUIController;
        private static DelegateBridge __Hotfix_HideSurplusItemActive;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItem3DTouch;

        public event Action<CommonItemIconUIController> EventOnRewardItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected CommonItemIconUIController GetChildCommonItemIconUIController(int idx)
        {
        }

        [MethodImpl(0x8000)]
        protected void HideSurplusItemActive(int hideChildCount)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItem3DTouch(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEasyPool(EasyObjectPool pool)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetItemDropInfoByDropConfig(ConfigDataItemDropInfo dropInfo, Dictionary<string, UnityEngine.Object> resDict, bool showProbability = false)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetItemDropInfoByDropId(int dropId, Dictionary<string, UnityEngine.Object> resDict, bool showProbability = false)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetMineralRewardInfo(ConfigDataMineralTypeInfo mineInfo, int mineCount, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void SetMonsterDropInfo(List<SignalInfoMonsterDropInfo> monDropInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPVPSignalReward(List<ItemInfo> rewardList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

