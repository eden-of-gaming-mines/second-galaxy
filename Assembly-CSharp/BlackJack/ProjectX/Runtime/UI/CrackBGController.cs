﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CrackBGController : UIControllerBase
    {
        private const string BoxAnimatorState = "BoxState";
        private const string PlatformAnimatorState = "State";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnPlatformMoveEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnStartCrackEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnEmptyToWaitEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBoxOpenAnimationEnd;
        private Animator m_boxModelAnimator;
        protected bool m_isBlockInupt;
        private bool m_isOPenBoxWithPlatInfo;
        private PlatformAnimMode m_currPlatFormAnimMode;
        private BoxAnimMode m_wantedBoxAnimMode;
        [AutoBind("./SceneRender/Root/Bone01/BoxDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject BoxModelDummyRoot;
        [AutoBind("./SceneRender", AutoBindAttribute.InitState.NotInit, false)]
        public Animator PlatformAnimator;
        protected GameObject m_modelViewGameObject;
        public static string UIModelViewLayerMask = "UIModelViewLayer";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_StopSound;
        private static DelegateBridge __Hotfix_BlockInput;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefab;
        private static DelegateBridge __Hotfix_Clear3DModel;
        private static DelegateBridge __Hotfix_SetCrackState;
        private static DelegateBridge __Hotfix_SetPlatformStateWithDelay;
        private static DelegateBridge __Hotfix_StopAllPlatFormSound;
        private static DelegateBridge __Hotfix_SetBoxState;
        private static DelegateBridge __Hotfix_SetBoxStateWithDelay;
        private static DelegateBridge __Hotfix_StopAllBoxSound;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefabImpl;
        private static DelegateBridge __Hotfix_SetViewModelLayerMask;
        private static DelegateBridge __Hotfix_SetGameObjectLayerMask;
        private static DelegateBridge __Hotfix_OnPlatformReadyForOpenBox;
        private static DelegateBridge __Hotfix_OnPlatformMoveEnd;
        private static DelegateBridge __Hotfix_OnCrackBoxEndStart;
        private static DelegateBridge __Hotfix_OnPlatformStartEnd;
        private static DelegateBridge __Hotfix_OnPlatformStartStart;
        private static DelegateBridge __Hotfix_OnPlatformLodeEnd;
        private static DelegateBridge __Hotfix_OnPlatformLodeStart;
        private static DelegateBridge __Hotfix_OnBoxOpenAnimationEnd;
        private static DelegateBridge __Hotfix_add_EventOnPlatformMoveEnd;
        private static DelegateBridge __Hotfix_remove_EventOnPlatformMoveEnd;
        private static DelegateBridge __Hotfix_add_EventOnStartCrackEnd;
        private static DelegateBridge __Hotfix_remove_EventOnStartCrackEnd;
        private static DelegateBridge __Hotfix_add_EventOnEmptyToWaitEnd;
        private static DelegateBridge __Hotfix_remove_EventOnEmptyToWaitEnd;
        private static DelegateBridge __Hotfix_add_EventOnBoxOpenAnimationEnd;
        private static DelegateBridge __Hotfix_remove_EventOnBoxOpenAnimationEnd;

        public event Action EventOnBoxOpenAnimationEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEmptyToWaitEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnPlatformMoveEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStartCrackEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void BlockInput(bool isBlock)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void Clear3DModel()
        {
        }

        [MethodImpl(0x8000)]
        public void InitWith3DModelPrefab(GameObject modelCloneSource)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator InitWith3DModelPrefabImpl(GameObject modelCloneSource)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnBoxOpenAnimationEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCrackBoxEndStart()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlatformLodeEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlatformLodeStart()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlatformMoveEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlatformReadyForOpenBox()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlatformStartEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void OnPlatformStartStart()
        {
        }

        [MethodImpl(0x8000)]
        public void SetBoxState(BoxAnimMode animMode)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator SetBoxStateWithDelay(int fristState, int scenodState, float delayTime)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCrackState(PlatformAnimMode animMode, bool isShowPaltFormChangeAnimation = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGameObjectLayerMask(GameObject go, int layer)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator SetPlatformStateWithDelay(List<int> stateList, List<float> stateDruation)
        {
        }

        [MethodImpl(0x8000)]
        private void SetViewModelLayerMask()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopAllBoxSound()
        {
        }

        [MethodImpl(0x8000)]
        protected void StopAllPlatFormSound()
        {
        }

        [MethodImpl(0x8000)]
        public void StopSound()
        {
        }

        [CompilerGenerated]
        private sealed class <InitWith3DModelPrefabImpl>c__Iterator2 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal GameObject modelCloneSource;
            internal CrackBGController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        if (this.$this.m_modelViewGameObject != null)
                        {
                            this.$this.m_modelViewGameObject.GetComponent<UIAnimationEventHandler>().EventOnAnimationFinished -= new Action(this.$this.OnBoxOpenAnimationEnd);
                            UnityEngine.Object.Destroy(this.$this.m_modelViewGameObject);
                            this.$this.m_modelViewGameObject = null;
                        }
                        if (this.modelCloneSource != null)
                        {
                            this.$this.m_modelViewGameObject = UnityEngine.Object.Instantiate<GameObject>(this.modelCloneSource);
                            this.$this.m_boxModelAnimator = this.$this.m_modelViewGameObject.GetComponent<Animator>();
                            this.$this.SetBoxState(this.$this.m_wantedBoxAnimMode);
                            this.$this.m_modelViewGameObject.GetComponent<UIAnimationEventHandler>().EventOnAnimationFinished += new Action(this.$this.OnBoxOpenAnimationEnd);
                            this.$this.m_modelViewGameObject.transform.SetParent(this.$this.BoxModelDummyRoot.transform, false);
                            this.$this.SetViewModelLayerMask();
                        }
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <SetBoxStateWithDelay>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal int fristState;
            internal float delayTime;
            internal int scenodState;
            internal CrackBGController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$this.m_boxModelAnimator.SetInteger("BoxState", this.fristState);
                        this.$current = new WaitForSeconds(this.delayTime);
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        this.$this.m_boxModelAnimator.SetInteger("BoxState", this.scenodState);
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <SetPlatformStateWithDelay>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal int <i>__1;
            internal List<float> stateDruation;
            internal List<int> stateList;
            internal CrackBGController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<i>__1 = 0;
                        break;

                    case 1:
                        this.$this.PlatformAnimator.SetInteger("State", this.stateList[this.<i>__1]);
                        this.<i>__1++;
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.<i>__1 < this.stateDruation.Count)
                {
                    this.$current = new WaitForSeconds(this.stateDruation[this.<i>__1]);
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                this.$PC = -1;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public enum BoxAnimMode
        {
            Closed,
            CloseToWillOpen,
            WillOpen,
            Open
        }

        public enum PlatformAnimMode
        {
            NoBox,
            StartCrack,
            Cracking,
            StandToCracked,
            Cracked,
            OpenBox,
            EmptyToWait,
            WaitForCrack
        }
    }
}

