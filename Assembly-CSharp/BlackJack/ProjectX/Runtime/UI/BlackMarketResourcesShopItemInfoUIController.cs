﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class BlackMarketResourcesShopItemInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmButtonClick;
        public ConfigDataBlackMarketShopItemInfo m_shopItemInfo;
        private CommonItemIconUIController m_itemIconUICtrl;
        private int m_priceForOneBuy;
        private int m_orderCountForOneBuy;
        private int m_minValue;
        private int m_maxValue;
        private int m_currentValue;
        private bool m_isNeedSyncFromInputFieldToSlider;
        private int m_longPressingEventInvokeCount;
        [AutoBind("./PriceText/MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIconImage;
        [AutoBind("./DetailPanel/ConfirmButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./SumText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SumText;
        [AutoBind("./PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PriceText;
        [AutoBind("./NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberText;
        [AutoBind("./DetailPanel/ItemCountInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField m_itemCountInputField;
        [AutoBind("./DetailPanel/CountSlider", AutoBindAttribute.InitState.NotInit, false)]
        public Slider m_itemCountSlider;
        [AutoBind("./DetailPanel/AddPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_addButton;
        [AutoBind("./DetailPanel/SubPointButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_subButton;
        [AutoBind("./DetailPanel/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemIconDummy;
        [AutoBind("./DetailPanel/ItemDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemDescText;
        [AutoBind("./DetailPanel/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemNameText;
        [AutoBind("./DetailPanel/ConfirmButtonGroup/ConfirmButton/MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_moneyNumText;
        [AutoBind("./DetailPanel/ConfirmButtonGroup/ConfirmButton/MoneyText/MoneyIconBgImage/MoneyImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_moneyIconImage;
        [AutoBind("./DetailPanel/ConfirmButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmButtonStateCtrl;
        private static DelegateBridge __Hotfix_UpdateSelectItemInfo;
        private static DelegateBridge __Hotfix_GetCurrBuyItemCount;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetCurrentValue;
        private static DelegateBridge __Hotfix_UpdateViewOnCurrentValue;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnAddButtonClick;
        private static DelegateBridge __Hotfix_OnAddButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnAddButtonLongPressing;
        private static DelegateBridge __Hotfix_OnAddButtonLongPressEnd;
        private static DelegateBridge __Hotfix_OnSubButtonClick;
        private static DelegateBridge __Hotfix_OnSubButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnSubButtonLongPressing;
        private static DelegateBridge __Hotfix_OnSubButtonLongPressEnd;
        private static DelegateBridge __Hotfix_OnItemCountSliderValueChanged;
        private static DelegateBridge __Hotfix_OnItemInputFieldValueChanged;
        private static DelegateBridge __Hotfix_GetChangeCountByLongPressingEventInvokeCount;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private static int GetChangeCountByLongPressingEventInvokeCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrBuyItemCount()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemCountSliderValueChanged(float value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemInputFieldValueChanged(string str)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubButtonLongPressEnd(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubButtonLongPressing(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSubButtonLongPressStart(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrentValue(int value)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectItemInfo(ConfigDataBlackMarketShopItemInfo shopItemInfo, Dictionary<string, UnityEngine.Object> resDict, int maxValue, int defaultValue = 1)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewOnCurrentValue()
        {
        }
    }
}

