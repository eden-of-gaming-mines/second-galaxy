﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ItemStoreUIFilterBase
    {
        protected ItemStoreListUIController m_itemStoreListUICtrl;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_Clear;

        [MethodImpl(0x8000)]
        public virtual void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void Init(ItemStoreListUIController uiCtrl)
        {
        }
    }
}

