﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class MotherShipGalaxyItemUIController : UIControllerBase
    {
        private GuildBuildingInfo m_curGuildBuilding;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image Progressbar;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateProduceLineItemSimpleInfo;
        private static DelegateBridge __Hotfix_GetBuildingInfoStateName;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        private string GetBuildingInfoStateName(GuildBuildingInfo buildingInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProduceLineItemSimpleInfo(GuildBuildingInfo guildBuildingInfo)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

