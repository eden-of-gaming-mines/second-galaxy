﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class LogOutBoxUITask : UITaskBase
    {
        private bool m_isPVP;
        private DateTime? m_pveOutTime;
        private float m_refreshDelayTime;
        private const float RefreshDelayTime = 0.4f;
        protected bool m_isUpdateOver;
        protected DateTime m_startTime;
        protected int m_normalWaitTime;
        protected int m_extraWaitTime;
        private LogOutBoxUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private const string ExtraWaitTime = "ExtraWaitTime";
        public const string LogOutModeOfNormal = "LogOutModeOfNormal";
        public const string LogOutModeOfInSpace = "LogOutModeOfInSpace";
        public const string LoginSceneExitMode = "LoginSceneExitMode";
        public const string TaskName = "LogOutBoxUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_PushAllNeedUILayer;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_ShowLogOutGameBox_1;
        private static DelegateBridge __Hotfix_ShowLogOutGameBox_0;
        private static DelegateBridge __Hotfix_CloseLogOutGameBox;
        private static DelegateBridge __Hotfix_SetMessageBoxInfo;
        private static DelegateBridge __Hotfix_IsInSpace;
        private static DelegateBridge __Hotfix_TickPvpOutTime;
        private static DelegateBridge __Hotfix_CalcLeftTime;
        private static DelegateBridge __Hotfix_GetLogoutTime;
        private static DelegateBridge __Hotfix_GetFlagShipAutoPackTime;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public LogOutBoxUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CalcLeftTime(LogicBlockShipCompPVPBase lbPVP)
        {
        }

        [MethodImpl(0x8000)]
        public static void CloseLogOutGameBox()
        {
        }

        [MethodImpl(0x8000)]
        private string GetFlagShipAutoPackTime()
        {
        }

        [MethodImpl(0x8000)]
        private string GetLogoutTime(bool refresh = false)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private static bool IsInSpace()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCancelButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnConfirmButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void PushAllNeedUILayer()
        {
        }

        [MethodImpl(0x8000)]
        private void SetMessageBoxInfo(string info, string title, string confirm)
        {
        }

        [MethodImpl(0x8000)]
        public static LogOutBoxUITask ShowLogOutGameBox(int extraWaitTime = 0)
        {
        }

        [MethodImpl(0x8000)]
        public static LogOutBoxUITask ShowLogOutGameBox(string mode, int extraWaitTime = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void TickPvpOutTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

