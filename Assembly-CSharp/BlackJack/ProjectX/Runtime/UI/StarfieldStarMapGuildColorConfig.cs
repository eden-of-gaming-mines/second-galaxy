﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapGuildColorConfig
    {
        [Header("星域划分色块完全透明时的缩放比")]
        public float m_scaleValueForStarfieldGuildColorAlphaMin;
        [Header("星域划分色块透明度最大时的缩放比")]
        public float m_scaleValueForStarfieldGuildColorAlphaMax;
        [Header("星域划分色块完全不透明时的透明度，0-1，1是完全不透明")]
        public float m_starfieldGuildColorAlphaMax;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

