﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class MailUITask : UITaskBase
    {
        private ModeMain m_mainMode;
        private ModeCategory m_subMode;
        public const string ParamKeySend2PlayerGameUserId = "Send2PlayerGameUserID";
        public const string ParamKeySend2PlayerGameUserName = "Send2PlayerGameUserName";
        public const string ParamKeySend2PlayerGameUserAvatarId = "Send2PlayerGameUserAvatarID";
        public const string ParamKeySend2PlayerGameUserProfesssion = "Send2PlayerGameUserProfesssion";
        private const string OpenMode = "OpenMode";
        private readonly HashSet<ulong> m_selectedItemHashSet;
        private MailUIController m_mailUICtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private bool m_needConfirmExitTypingMail;
        private LBStoredMail m_currentDetailMailInfo;
        private bool m_currDetailMailShowTranslate;
        private List<LBStoredMail> m_currMailList;
        private readonly List<LBStoredMail> m_currentEditorSelectedMailList;
        private readonly List<int> m_removeMailIndexList;
        private readonly List<int> m_getMailIndexList;
        private int m_sendToGameUserAvaterId;
        private ProfessionType m_sendToGameUserProfession;
        private string m_sendToGameUserId;
        private string m_sendToGameUserName;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "MailUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateMailList;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnMailNewArrivalNtf;
        private static DelegateBridge __Hotfix_RegisterTranslateEvent;
        private static DelegateBridge __Hotfix_UnRegisterTranslateEvent;
        private static DelegateBridge __Hotfix_OnStringTranslateComplete;
        private static DelegateBridge __Hotfix_OnMailSendButtonClick;
        private static DelegateBridge __Hotfix_OnGroupMailSendButtonClick;
        private static DelegateBridge __Hotfix_OnMailSendCancelButtonClick;
        private static DelegateBridge __Hotfix_OnMailTableTypeChange;
        private static DelegateBridge __Hotfix_OnMailItemClick;
        private static DelegateBridge __Hotfix_OnMailUICloseButtonClick;
        private static DelegateBridge __Hotfix_OnDeleteAllMailButtonClick;
        private static DelegateBridge __Hotfix_OnGetAllMailButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnMailUIEditDeleteButtonClick;
        private static DelegateBridge __Hotfix_OnMailUIEditCancelButtonClick;
        private static DelegateBridge __Hotfix_OnMailUIEditButtonClick;
        private static DelegateBridge __Hotfix_OnMailSelectedChange;
        private static DelegateBridge __Hotfix_OnMailAttchementItemClick;
        private static DelegateBridge __Hotfix_OnMailAttchementItem3DTouch;
        private static DelegateBridge __Hotfix_OnMailDetailScrollViewClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnTranslateButtonClick;
        private static DelegateBridge __Hotfix_OnMailDetailUIButtonClick_RePlyMail;
        private static DelegateBridge __Hotfix_OnMailDetailUIButtonClick_Delete;
        private static DelegateBridge __Hotfix_OnMailDetailUIButtonClick_GetAttachement;
        private static DelegateBridge __Hotfix_OnMailDetailUIButtonClick_Confirm;
        private static DelegateBridge __Hotfix_SetMode;
        private static DelegateBridge __Hotfix_CheckIsPlayerInBanned;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UpdateSelectedToggle;
        private static DelegateBridge __Hotfix_UpdateUnreadMailCount;
        private static DelegateBridge __Hotfix_TryRemoveMailListAndUpdateView;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_TryGetMailListAndUpdateView;
        private static DelegateBridge __Hotfix_GetPlayer2PlayerMailFilter;
        private static DelegateBridge __Hotfix_GetSystemMailFilter;
        private static DelegateBridge __Hotfix_GetGuildMailFilter;
        private static DelegateBridge __Hotfix_GetAllianceMailFilter;
        private static DelegateBridge __Hotfix_GetAuctionMailFilter;
        private static DelegateBridge __Hotfix_GetDisplayMailFilter;
        private static DelegateBridge __Hotfix_GetUnReadMailFilter;
        private static DelegateBridge __Hotfix_IsMailHasAttachment;
        private static DelegateBridge __Hotfix_get_m_playerCtx;
        private static DelegateBridge __Hotfix_get_LBMail;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public MailUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckIsPlayerInBanned()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private bool GetAllianceMailFilter(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetAuctionMailFilter(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetDisplayMailFilter(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetGuildMailFilter(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetPlayer2PlayerMailFilter(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetSystemMailFilter(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool GetUnReadMailFilter(LBStoredMail mailInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsMailHasAttachment(LBStoredMail mail)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteAllMailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetAllMailButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGroupMailSendButtonClick(string title, string content)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailAttchementItem3DTouch(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailAttchementItemClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailDetailScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailDetailUIButtonClick_Confirm(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailDetailUIButtonClick_Delete(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailDetailUIButtonClick_GetAttachement(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailDetailUIButtonClick_RePlyMail(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailItemClick(LBStoredMail lbMail)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailNewArrivalNtf(MailNewArrivalNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailSelectedChange(UIControllerBase ctrl, LBStoredMail mailInfo, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailSendButtonClick(string targetGameUserId, string mailTitle, string mailContent)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailSendCancelButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailTableTypeChange(ModeCategory currentSubMode)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailUICloseButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailUIEditButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailUIEditCancelButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailUIEditDeleteButtonClick(UIControllerBase obj)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnStringTranslateComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTranslateButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterTranslateEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SetMode(Mode mode)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ItemInfo itemInfo, int index)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(UIIntent prevIntent, Mode mode, ProPlayerSimplestInfo playerInfo = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void TryGetMailListAndUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void TryRemoveMailListAndUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterTranslateEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMailList()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSelectedToggle()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUnreadMailCount()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext m_playerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockMailClient LBMail
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnMailDetailUIButtonClick_Delete>c__AnonStorey2
        {
            internal LBStoredMailClient mailInfoClient;
            internal MailUITask $this;

            internal void <>m__0()
            {
                this.$this.EnableUIInput(false);
                this.$this.m_removeMailIndexList.Add(this.mailInfoClient.GetStoredIndex());
                this.$this.TryRemoveMailListAndUpdateView();
            }
        }

        [CompilerGenerated]
        private sealed class <OnMailTableTypeChange>c__AnonStorey1
        {
            internal MailUITask.ModeCategory currentSubMode;
            internal MailUITask $this;

            internal void <>m__0()
            {
                this.$this.m_mainMode = MailUITask.ModeMain.View;
                this.$this.m_subMode = this.currentSubMode;
                this.$this.StartUpdatePipeLine(null, false, false, true, null);
            }

            internal void <>m__1()
            {
                this.$this.UpdateSelectedToggle();
                this.$this.m_needConfirmExitTypingMail = true;
            }
        }

        [CompilerGenerated]
        private sealed class <StartTask>c__AnonStorey0
        {
            internal Action<bool> onPipelineEnd;

            internal void <>m__0(bool res)
            {
                if (this.onPipelineEnd != null)
                {
                    this.onPipelineEnd(res);
                }
            }
        }

        public class MailComparer : IComparer<LBStoredMail>
        {
            private static MailUITask.MailComparer m_instance;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Compare;
            private static DelegateBridge __Hotfix_get_Instance;

            public MailComparer()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public int Compare(LBStoredMail x, LBStoredMail y)
            {
                DelegateBridge bridge = __Hotfix_Compare;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp78(this, x, y);
                }
                if (x != null)
                {
                    if (y == null)
                    {
                        return 1;
                    }
                    MailInfo mailInfo = x.GetReadonlyStoredMailInfo().m_mailInfo;
                    MailInfo info2 = y.GetReadonlyStoredMailInfo().m_mailInfo;
                    if (mailInfo.m_isReaded && !info2.m_isReaded)
                    {
                        return 1;
                    }
                    if (mailInfo.m_isReaded || !info2.m_isReaded)
                    {
                        return ((mailInfo.m_sendTime >= info2.m_sendTime) ? ((mailInfo.m_sendTime <= info2.m_sendTime) ? ((x.GetMailInstanceId() >= y.GetMailInstanceId()) ? 1 : -1) : -1) : 1);
                    }
                }
                return -1;
            }

            public static MailUITask.MailComparer Instance
            {
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Instance;
                    if (bridge != null)
                    {
                        return bridge.__Gen_Delegate_Imp5006();
                    }
                    if (m_instance == null)
                    {
                        m_instance = new MailUITask.MailComparer();
                    }
                    return m_instance;
                }
            }
        }

        public enum Mode
        {
            SendPrivate,
            SendGuild,
            SendAlliance,
            View
        }

        public enum ModeCategory
        {
            All,
            Private,
            System,
            Guild,
            Alliance,
            Trade
        }

        private enum ModeMain
        {
            Send,
            View,
            Detail
        }
    }
}

