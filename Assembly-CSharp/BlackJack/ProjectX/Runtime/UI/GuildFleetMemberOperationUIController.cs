﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class GuildFleetMemberOperationUIController : UIControllerBase
    {
        private float m_localPosX;
        private Rect m_viewRect;
        [AutoBind("./FrameImage/BGImage/DetailInfo/LeaveForButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LeaveForButton;
        [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FrameImageRectTransform;
        [AutoBind("./OperationPanelBGButton", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController OperationPanelBGButtonPassAllEventUIController;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController OperatePanelStateCtrl;
        [AutoBind("./FrameImage/BGImage/DetailInfo/PassOnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PassOnButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/SetJobButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SetJobButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/OutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx OutButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/KickOutButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx KickOutButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/MailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MailButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/TeamButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TeamButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/SendMsgButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx SendMsgButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/PlayerDetailButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PlayerDetailButton;
        [AutoBind("./FrameImage/BGImage/DetailInfo/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx JumpButton;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberOperationPanelUIProcess;
        private static DelegateBridge __Hotfix_UpdateGuildFleetMemberOperationPanel;
        private static DelegateBridge __Hotfix_SetPanelPos;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetViewRect;
        private static DelegateBridge __Hotfix_GetPanelSize;

        [MethodImpl(0x8000)]
        public UIProcess GetGuildFleetMemberOperationPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 GetPanelSize()
        {
        }

        [MethodImpl(0x8000)]
        private Rect GetViewRect()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPos(Vector3 up, Vector3 down)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetMemberOperationPanel(bool isFleetManager, GuildFleetMemberInfo selfInfo, GuildFleetMemberInfo selectMemberInfo)
        {
        }
    }
}

