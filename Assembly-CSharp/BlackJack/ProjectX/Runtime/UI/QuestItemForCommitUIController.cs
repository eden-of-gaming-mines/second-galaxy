﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class QuestItemForCommitUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnRewardItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnRewardItem3DTouch;
        private CommonItemIconUIController m_itemCtrl;
        private string CommonItemPrefabResName;
        [AutoBind("./ItemGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy1;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetItemInfo;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItem3DTouch;

        public event Action<CommonItemIconUIController> EventOnRewardItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItem3DTouch(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemInfo(StoreItemType itemType, int itemId, bool isBind, bool isFreezing, long haveItemCount, int needItemCount, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

