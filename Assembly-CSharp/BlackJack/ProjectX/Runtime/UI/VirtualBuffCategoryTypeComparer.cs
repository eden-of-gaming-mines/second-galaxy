﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;

    [StructLayout(LayoutKind.Sequential, Size=1)]
    public struct VirtualBuffCategoryTypeComparer : IEqualityComparer<VirtualBuffCategory>
    {
        public bool Equals(VirtualBuffCategory x, VirtualBuffCategory y) => 
            (x == y);

        public int GetHashCode(VirtualBuffCategory obj) => 
            ((int) obj);
    }
}

