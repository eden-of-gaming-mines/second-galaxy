﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Runtime.SolarSystem;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using TouchScript.Gestures;
    using TouchScript.Layers;
    using UnityEngine;

    public class ShipModelOverviewUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLongPressd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLongPressReleased;
        private ShipModelOverviewMode m_currOverviewMode;
        protected int m_longPressedCount;
        protected bool m_isBlockInupt;
        protected float m_screenWidth;
        protected float m_screenHeight;
        protected GameObject m_modelViewGameObject;
        protected Bounds m_modelViewObjectBounds;
        protected float m_modelShipScale;
        protected float m_currScreenPortion;
        protected List<List<GameObject>> m_weaponGroupList;
        protected DateTime m_longPressStartTime;
        protected const float LongPressTime = 1f;
        protected bool m_isInLongPressed;
        protected bool m_isLongPressEventRaised;
        protected float m_shipBlinkLeftTime;
        protected const float ShipBlinkTime = 1f;
        protected float m_cameraDelayMoveLeftTime;
        protected const float CameraDelayMoveTime = 5f;
        protected bool m_isNeedCameraDelayMove;
        protected bool m_isNeedResetCameraRotaion;
        protected float m_cameraDelayMoveDistance;
        protected float m_cameraDelayRotationX;
        protected float m_cameraDelayRotationY;
        protected float m_cameraDelayOffsetX;
        protected Vector3 m_cameraDelayMoveSpeed;
        public const float RotateSpeed = 100f;
        public const float PitchSpeed = 60f;
        public const float ScaleSensitivity = 0.3f;
        public const float MaxScreenPortion = 1.85f;
        public const float DefaultScreenPortionMinForShipHangarMode = 0.5f;
        public const float DefaultScreenPortionMaxForShipHangarMode = 1f;
        public const float DefaultXRotationForShipHangarMode = 15f;
        public const float DefaultYRotationForShipHangarMode = 225f;
        public const float DefaultScreenPortionMinForCaptainManageMode = 0.7f;
        public const float DefaultScreenPortionMaxForCaptainManageMode = 1.2f;
        public const float DefaultXRotationForCaptainManageMode = 12f;
        public const float DefaultYRotationForCaptainManageMode = 225f;
        public const float DefaultScreenPortionMinForCaptainShipManageMode = 0.5f;
        public const float DefaultScreenPortionMaxForCaptainShipManageMode = 1.5f;
        public const float DefaultXRotationForCaptainShipManageMode = 15f;
        public const float DefaultYRotationForCaptainShipManageMode = 225f;
        public const float DefaultScreenPortionMinForFlagShipHangarMode = 1f;
        public const float DefaultScreenPortionMaxForFlagShipHangarMode = 1.6f;
        public const float DefaultXRotationForFlagShipHangarMode = 15f;
        public const float DefaultYRotationForFlagShipHangarMode = 225f;
        public const float MinScreenPortion = 0.3f;
        public const float CameraXOffsetMin = -0.2f;
        public const float CameraXOffsetMax = -0.6f;
        public static string UIModelViewLayerMask;
        public static string ShipLayerMask;
        [AutoBind("./ModelViewDummyRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ModelViewDummyRoot;
        [AutoBind("./CameraPlatformRoot/CameraPlatform", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraPlatform;
        [AutoBind("./CameraPlatformRoot/CameraPlatform/YRotationRootForCamera", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_yRotationRootForCamera;
        [AutoBind("./CameraPlatformRoot/CameraPlatform/YRotationRootForCamera/XRotationRootForCamera", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_xRotationRootForCamera;
        [AutoBind("./CameraPlatformRoot/CameraPlatform/YRotationRootForCamera/XRotationRootForCamera/CameraDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraDummy;
        [AutoBind("./CameraPlatformRoot/CameraRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraRoot;
        [AutoBind("./CameraPlatformRoot/CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        [AutoBind("./CameraPlatformRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TouchScript.Gestures.ScreenTransformGesture ScreenTransformGesture;
        [AutoBind("./CameraPlatformRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TouchScript.Gestures.PressGesture PressGesture;
        [AutoBind("./CameraPlatformRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public TouchScript.Gestures.ReleaseGesture ReleaseGesture;
        [AutoBind("./CameraPlatformRoot/ManipulationGesture", AutoBindAttribute.InitState.NotInit, false)]
        public FullscreenLayer m_fullScreenLayer;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_BlockInput;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefab;
        private static DelegateBridge __Hotfix_EquipWeaponToShip;
        private static DelegateBridge __Hotfix_Clear3DModel;
        private static DelegateBridge __Hotfix_InitWith3DModelPrefabImp;
        private static DelegateBridge __Hotfix_GetObjectBound;
        private static DelegateBridge __Hotfix_ResetCameraOffsetAndRotation;
        private static DelegateBridge __Hotfix_SetCameraSkyBox;
        private static DelegateBridge __Hotfix_ManipulationTransformedHandler;
        private static DelegateBridge __Hotfix_OnPressBegin;
        private static DelegateBridge __Hotfix_OnPressRelease;
        private static DelegateBridge __Hotfix_DoShipScale;
        private static DelegateBridge __Hotfix_GetCameraPositionFromScreenPortion;
        private static DelegateBridge __Hotfix_GetCemeraXOffsetFromScreenPortion;
        private static DelegateBridge __Hotfix_GetPixelsPerMeter;
        private static DelegateBridge __Hotfix_SetViewModelLayerMask;
        private static DelegateBridge __Hotfix_SetGameObjectLayerMask;
        private static DelegateBridge __Hotfix_StartShipModelBlink;
        private static DelegateBridge __Hotfix_UnloadWeaponByGroupIndex;
        private static DelegateBridge __Hotfix_SetCameraOffset;
        private static DelegateBridge __Hotfix_GetCameraOffset;
        private static DelegateBridge __Hotfix_TransformCameraByRotating;
        private static DelegateBridge __Hotfix_SetCameraRotating;
        private static DelegateBridge __Hotfix_UpdateCameraDockData;
        private static DelegateBridge __Hotfix_UpdateLongPressRecord;
        private static DelegateBridge __Hotfix_add_EventOnLongPressd;
        private static DelegateBridge __Hotfix_remove_EventOnLongPressd;
        private static DelegateBridge __Hotfix_add_EventOnLongPressReleased;
        private static DelegateBridge __Hotfix_remove_EventOnLongPressReleased;

        public event Action EventOnLongPressd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLongPressReleased
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void BlockInput(bool isBlock)
        {
        }

        [MethodImpl(0x8000)]
        public void Clear3DModel()
        {
        }

        [MethodImpl(0x8000)]
        private void DoShipScale(float deltaScale)
        {
        }

        [MethodImpl(0x8000)]
        public void EquipWeaponToShip(GameObject weaponAsset, int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected Vector3 GetCameraOffset()
        {
        }

        [MethodImpl(0x8000)]
        private float GetCameraPositionFromScreenPortion(float screenPortion)
        {
        }

        [MethodImpl(0x8000)]
        private float GetCemeraXOffsetFromScreenPortion(float screenPortion)
        {
        }

        [MethodImpl(0x8000)]
        private Bounds GetObjectBound()
        {
        }

        [MethodImpl(0x8000)]
        private float GetPixelsPerMeter()
        {
        }

        [MethodImpl(0x8000)]
        public void InitWith3DModelPrefab(ShipModelOverviewMode overviewMode, GameObject modelCloneSource, Func<GameObject, KeyValuePair<float, Bounds>> PrepareViewGameObjectFunc = null, bool resetRotation = false, bool resetImmediately = false)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        protected IEnumerator InitWith3DModelPrefabImp(ShipModelOverviewMode overviewMode, GameObject modelCloneSource, Func<GameObject, KeyValuePair<float, Bounds>> PrepareViewGameObjectFunc, bool resetRotation, bool resetImmediately = false)
        {
        }

        [MethodImpl(0x8000)]
        private void ManipulationTransformedHandler(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPressBegin(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPressRelease(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetCameraOffsetAndRotation(ShipModelOverviewMode overviewMode, bool needResetRotaion = true, bool resetImmediately = false)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraOffset(float xOffset, float zOffset)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetCameraRotating(float rotateAngleForAxisX, float rotateAngleForAxisY)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraSkyBox(Material skyBoxMat)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGameObjectLayerMask(GameObject go, int layer)
        {
        }

        [MethodImpl(0x8000)]
        private void SetViewModelLayerMask()
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator StartShipModelBlink()
        {
        }

        [MethodImpl(0x8000)]
        protected void TransformCameraByRotating(float rotateAngleForAxisX, float rotateAngleForAxisY)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnloadWeaponByGroupIndex(int groupIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateCameraDockData()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateLongPressRecord()
        {
        }

        [CompilerGenerated]
        private sealed class <InitWith3DModelPrefabImp>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal bool resetImmediately;
            internal GameObject modelCloneSource;
            internal Func<GameObject, KeyValuePair<float, Bounds>> PrepareViewGameObjectFunc;
            internal ShipModelOverviewUIController.ShipModelOverviewMode overviewMode;
            internal bool resetRotation;
            internal ShipModelOverviewUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                        if (this.$this.m_modelViewGameObject != null)
                        {
                            foreach (List<GameObject> list in this.$this.m_weaponGroupList)
                            {
                                list.Clear();
                            }
                            UnityEngine.Object.Destroy(this.$this.m_modelViewGameObject);
                            this.$this.m_isNeedCameraDelayMove = true;
                            this.$this.m_cameraDelayMoveLeftTime = 5f;
                            this.$this.m_modelViewGameObject = null;
                        }
                        if (this.resetImmediately)
                        {
                            this.$this.m_isNeedCameraDelayMove = false;
                        }
                        if (this.modelCloneSource != null)
                        {
                            this.$this.m_modelViewGameObject = UnityEngine.Object.Instantiate<GameObject>(this.modelCloneSource);
                            this.$this.m_modelViewGameObject.transform.SetParent(this.$this.ModelViewDummyRoot.transform, false);
                            this.$this.SetViewModelLayerMask();
                        }
                        if ((this.PrepareViewGameObjectFunc == null) || (this.$this.m_modelViewGameObject == null))
                        {
                            this.$this.m_modelViewObjectBounds = this.$this.GetObjectBound();
                            this.$this.m_modelShipScale = 1f;
                        }
                        else
                        {
                            KeyValuePair<float, Bounds> pair = this.PrepareViewGameObjectFunc(this.$this.m_modelViewGameObject);
                            this.$this.m_modelViewObjectBounds = pair.Value;
                            this.$this.m_modelShipScale = pair.Key;
                        }
                        this.$this.m_cameraPlatform.transform.position = this.$this.m_modelViewObjectBounds.center;
                        this.$this.ResetCameraOffsetAndRotation(this.overviewMode, this.resetRotation, this.resetImmediately);
                        this.$this.StartCoroutine(this.$this.StartShipModelBlink());
                        this.$PC = -1;
                        break;

                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [CompilerGenerated]
        private sealed class <StartShipModelBlink>c__Iterator1 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal PrefabShipDesc <shipDsec>__1;
            internal Material <mat>__1;
            internal float <value>__2;
            internal List<List<GameObject>>.Enumerator $locvar0;
            internal ShipModelOverviewUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        if (this.$this.m_modelViewGameObject == null)
                        {
                            goto TR_0001;
                        }
                        else
                        {
                            this.<shipDsec>__1 = this.$this.m_modelViewGameObject.GetComponent<PrefabShipDesc>();
                            this.<mat>__1 = this.<shipDsec>__1.m_shipBody.GetComponent<SkinnedMeshRenderer>().material;
                            this.$this.m_shipBlinkLeftTime = 1f;
                        }
                        break;

                    case 1:
                        break;

                    default:
                        goto TR_0000;
                }
                if (this.$this.m_shipBlinkLeftTime > 0f)
                {
                    this.$this.m_shipBlinkLeftTime -= Time.deltaTime;
                    this.<value>__2 = Mathf.Lerp(0f, 1f, 1f - (this.$this.m_shipBlinkLeftTime / 1f));
                    this.<mat>__1.SetFloat("_BlinkFX_LogicVar", this.<value>__2);
                    this.$locvar0 = this.$this.m_weaponGroupList.GetEnumerator();
                    try
                    {
                        while (this.$locvar0.MoveNext())
                        {
                            List<GameObject> current = this.$locvar0.Current;
                            foreach (GameObject obj2 in current)
                            {
                                obj2.GetComponentInChildren<SkinnedMeshRenderer>(true).material.SetFloat("_BlinkFX_LogicVar", this.<value>__2);
                            }
                        }
                    }
                    finally
                    {
                        this.$locvar0.Dispose();
                    }
                    this.$current = null;
                    if (!this.$disposing)
                    {
                        this.$PC = 1;
                    }
                    return true;
                }
                goto TR_0001;
            TR_0000:
                return false;
            TR_0001:
                this.$PC = -1;
                goto TR_0000;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        public enum ShipModelOverviewMode
        {
            ShipHangarMode,
            CaptainManageMode,
            CaptainShipManageMode,
            FlagShipHangarMode
        }
    }
}

