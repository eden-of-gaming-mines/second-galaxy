﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public sealed class GuildSentrySignalHudUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnHudItemClicked;
        private ulong m_instanceId;
        [AutoBind("./ClickArea", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_hudButton;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildSentrySignalHudStateCtrl;
        private static DelegateBridge __Hotfix_UpdateGuildSentrySignalHud;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnHudItemClick;
        private static DelegateBridge __Hotfix_add_EventOnHudItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnHudItemClicked;

        public event Action<ulong> EventOnHudItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHudItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildSentrySignalHud(GuildSentryInterestScene scene)
        {
        }
    }
}

