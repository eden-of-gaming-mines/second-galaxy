﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StarMapInfectFinalBattleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnJumpToFinalBattleButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./InfectInfoRoot/InfectFinalBattleInfoRoot/JumpToFinalBattleButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_jumpToFinalBattleButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnJumpToFinalBattleButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnJumpToFinalBattleButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnJumpToFinalBattleButtonClick;
        private static DelegateBridge __Hotfix_CreateInfectFinalBattleWindowProcess;

        public event Action EventOnJumpToFinalBattleButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateInfectFinalBattleWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpToFinalBattleButtonClick(UIControllerBase ctrl)
        {
        }
    }
}

