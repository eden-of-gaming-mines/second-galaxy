﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class RepeatableUserGuideUIController : UIControllerBase
    {
        protected bool m_operateAreaHasClicked;
        protected PointerEventData m_currEventData;
        protected Action<PointerEventData> m_currPassAction;
        protected Camera m_targetCamera;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool> EventOnOperateAreaClick;
        [AutoBind("./BG", AutoBindAttribute.InitState.NotInit, false)]
        public PassAllEventUIController PassEventCtrl;
        [AutoBind("./OperationAreaRect", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform OperationAreaRect;
        [AutoBind("./OperationNotice", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject OperationNotice;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateOperaArea;
        private static DelegateBridge __Hotfix_PassEventToBelow;
        private static DelegateBridge __Hotfix_SetRectTransformSize;
        private static DelegateBridge __Hotfix_ShowOperationNotice;
        private static DelegateBridge __Hotfix_OnPointClick;
        private static DelegateBridge __Hotfix_OnBeginDrag;
        private static DelegateBridge __Hotfix_add_EventOnOperateAreaClick;
        private static DelegateBridge __Hotfix_remove_EventOnOperateAreaClick;

        public event Action<bool> EventOnOperateAreaClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected void OnBeginDrag(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPointClick(PointerEventData eventData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        public void PassEventToBelow()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetRectTransformSize(RectTransform trans, Vector2 newSize)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowOperationNotice(Vector2 center)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateOperaArea(Vector2 center, Vector2 size, Camera camera)
        {
        }
    }
}

