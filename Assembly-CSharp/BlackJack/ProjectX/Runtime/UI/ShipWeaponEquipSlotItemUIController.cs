﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipWeaponEquipSlotItemUIController : UIControllerBase
    {
        public CommonItemIconUIController m_commonItemIconUICtrl;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSlotButtonClick;
        private string UIState_Empty;
        private string UIState_Normal;
        private string UISTate_Lock;
        private string m_commonItemAssetName;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_commonItemUIPrefabDummy;
        [AutoBind("./AddImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image AddImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./LockEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lockStateCtrl;
        [AutoBind("./EquipedEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_equipedEffectStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Button SlotItemButton;
        [AutoBind("./CommonSlotItemQuestionImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AssemblyOptimizationStateCtrl;
        private int m_itemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_EnableButtonClick;
        private static DelegateBridge __Hotfix_UpdateSlotGroup;
        private static DelegateBridge __Hotfix_UpdateLowSlotGroups;
        private static DelegateBridge __Hotfix_ShowEquipedEffect;
        private static DelegateBridge __Hotfix_ShowAssemblyOptimizationState;
        private static DelegateBridge __Hotfix_SetSlotCommonInfo;
        private static DelegateBridge __Hotfix_GetShipHangerAddImageResPath;
        private static DelegateBridge __Hotfix_OnSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSlotButtonClick;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;

        public event Action<int> EventOnSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void EnableButtonClick(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private string GetShipHangerAddImageResPath(AddImageType addImageType, bool isAvailable)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSlotButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSlotCommonInfo(LBStaticWeaponEquipSlotGroup weaponSlotGroup, AddImageType slotType, ILBStaticShip shipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAssemblyOptimizationState(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEquipedEffect()
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateLowSlotGroups(LBStaticWeaponEquipSlotGroup weaponSlotGroup, ILBStaticShip shipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public virtual void UpdateSlotGroup(LBStaticWeaponEquipSlotGroup weaponSlotGroup, ILBStaticShip shipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private enum AddImageType
        {
            HighSlotGroupAddImage,
            MiddleSlotGroupAddImage,
            LowSlotGroupAddImage,
            LowSlotGroupsAddImage
        }
    }
}

