﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemTargetInteractionUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, InteractType> EventOnTargetInteractEnd;
        protected float m_countDownTotalTime;
        protected float m_countDownLeftTime;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StateCtrl;
        [AutoBind("./ProgressBarBGImage/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProgressBarImage;
        [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text InteractDesc;
        private InteractType m_currInteractType;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_StartTargetInteraction;
        private static DelegateBridge __Hotfix_StopTargetInteraction;
        private static DelegateBridge __Hotfix_IsDuringInteraction;
        private static DelegateBridge __Hotfix_add_EventOnTargetInteractEnd;
        private static DelegateBridge __Hotfix_remove_EventOnTargetInteractEnd;

        public event Action<bool, InteractType> EventOnTargetInteractEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public bool IsDuringInteraction()
        {
        }

        [MethodImpl(0x8000)]
        public void StartTargetInteraction(float countDownTime, string desc, InteractType type)
        {
        }

        [MethodImpl(0x8000)]
        public void StopTargetInteraction(InteractType type)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        public enum InteractType
        {
            NpcShip,
            EquipCharge
        }
    }
}

