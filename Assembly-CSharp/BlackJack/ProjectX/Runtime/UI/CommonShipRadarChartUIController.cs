﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class CommonShipRadarChartUIController : UIControllerBase
    {
        protected const float minNormalValue = 0.1f;
        protected Dictionary<ShipAbilityType, List<int>> m_shipAbilityLevelDict;
        [AutoBind("./RadarChart", AutoBindAttribute.InitState.NotInit, false)]
        public UIRadarChart ShipRadarChart;
        [AutoBind("./AttackOrMining/AttackText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AttackTitle;
        [AutoBind("./AttackOrMining/MiningText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MiningTitle;
        [AutoBind("./AttackOrMining/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AttackOrMiningValueText;
        [AutoBind("./Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RangeValueText;
        [AutoBind("./Energy/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text EnergyValueText;
        [AutoBind("./Defense/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DefenseValueText;
        [AutoBind("./Speed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SpeedValueText;
        private static DelegateBridge __Hotfix_UpdateShipRadarChartInfo_1;
        private static DelegateBridge __Hotfix_UpdateShipRadarChartInfo_2;
        private static DelegateBridge __Hotfix_UpdateShipRadarChartInfo_0;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShipAbilityValueCompare;
        private static DelegateBridge __Hotfix_UpdateRadarChart;
        private static DelegateBridge __Hotfix_GetNormalValue_1;
        private static DelegateBridge __Hotfix_GetNormalValue_0;

        [MethodImpl(0x8000)]
        private float GetNormalValue(float value, List<int> abilityList)
        {
        }

        [MethodImpl(0x8000)]
        private float GetNormalValue(float value, float maxValue)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected int ShipAbilityValueCompare(int a, int b)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRadarChart(LogicBlockShipCompStaticBasicCtx lbStaticCtx)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipRadarChartInfo(ConfigDataSpaceShipInfo confInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipRadarChartInfo(LBStaticHiredCaptainShipClient shipInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipRadarChartInfo(ILBStaticPlayerShip playerShip)
        {
        }

        protected enum ShipAbilityType
        {
            Attack,
            Range,
            Defence,
            Speed,
            Energy,
            Collect
        }

        [StructLayout(LayoutKind.Sequential, Size=1)]
        protected struct ShipAbilityTypeComparer : IEqualityComparer<CommonShipRadarChartUIController.ShipAbilityType>
        {
            public bool Equals(CommonShipRadarChartUIController.ShipAbilityType x, CommonShipRadarChartUIController.ShipAbilityType y) => 
                (x == y);

            public int GetHashCode(CommonShipRadarChartUIController.ShipAbilityType obj) => 
                ((int) obj);
        }
    }
}

