﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using System;

    [Flags]
    public enum SolarSystemVisibleMask
    {
        None = 0,
        ShowAllianceMember = 1,
        ShowNeutralListFriend = 2,
        ShowNeutralListNeutral = 4,
        ShowPortal = 8,
        ShowNoFriendlyWingShip = 0x10,
        ShowFleetMember = 0x20,
        ShowNoFleetMember = 0x40,
        ShowFriendlyWingShip = 0x80,
        ShowCelestial = 0x100
    }
}

