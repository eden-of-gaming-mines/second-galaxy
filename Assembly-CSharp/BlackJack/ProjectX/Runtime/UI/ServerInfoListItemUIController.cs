﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ServerInfoListItemUIController : UIControllerBase
    {
        private Action<string> m_serverInfoListItemClickAction;
        private string m_serverId;
        [AutoBind("./ServerStateImage/ServerStatePointImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ServerStateImageStateCtrl;
        [AutoBind("./RecentlyText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RecentlyTextGo;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ServerInfoListItemButton;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        private static DelegateBridge __Hotfix_UpdateServerInfoItem;
        private static DelegateBridge __Hotfix_RegisterItemClickAction;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnServerInfoListItemClick;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnServerInfoListItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickAction(Action<string> onClickAction)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateServerInfoItem(ServerInfo serverInfo, bool isRecently)
        {
        }
    }
}

