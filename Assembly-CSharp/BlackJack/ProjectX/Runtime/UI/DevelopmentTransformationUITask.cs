﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class DevelopmentTransformationUITask : UITaskBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, Action> EventOnRequestCloseAllPanels;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, bool, Action> EventOnRequestShowOrHideAllPanels;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRequestReturnToPreviousPanel;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<UIIntent>> EventOnRequestGetIntent;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<IUIBackgroundManager>> EventOnRequestGetUIBackgroundManager;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DevelopmentBgUITask.BackGroundStateType, string, FakeLBStoreItem, Action<bool>> EventOnRequestBgTaskChange;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnNotifyTransformationSuccess;
        public const string TaskName = "DevelopmentTransformationUITask";
        public const string ParamKeyResetTask = "ParamKeyResetTask";
        public const string ParamKeySetNodeInfos = "ParamKeySetNodeInfos";
        public const string ParamKeySetProjectInfo = "ParamKeySetProjectInfo";
        public const int MaxItemDisplayLineCount = 3;
        public const int MaxItemDisplayColumnCount = 3;
        public const string ItemGroupStateLine01 = "Line01";
        public const string ItemGroupStateLine02 = "Line02";
        public const string ItemGroupStateLine03 = "Line03";
        private const string ModeDefineBasePanel = "BasePanel";
        private const string ModeDefineItemSelectPanel = "ItemSelectPanel";
        private const string ModeDefineConfirmationHintPanel = "ConfirmationHintPanel";
        private const string ModeDefineShortOfItemHintPanel = "ShortOfItemHintPanel";
        private const string ModeDefineResultHintPanel = "ResultHintPanel";
        private const string ModeDefinePreviewHintPanel = "PreviewHintPanel";
        private const string PanelStateShow = "Show";
        private const string PanelStateClose = "Close";
        private const string ItemStateEmpty = "Empty";
        private const string ItemStateNormal = "Normal";
        private const string ItemStateAdd = "Add";
        private const string ItemStateStockout = "Stockout";
        private const string ItemChooseStateShow = "Show";
        private const string ItemChooseStateClose = "Close";
        private const string LaunchButtonStateNormal = "Normal";
        private const string LaunchButtonStateBlack = "Black";
        private const string CurrencyStateNormal = "Normal";
        private const string CurrencyStateRed = "Red";
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private ConfigDataDevelopmentNodeDescInfo[] m_nodeInfos;
        private ConfigDataDevelopmentProjectInfo m_projectInfo;
        private FakeLBStoreItem m_catalystItem;
        private long m_catalystItemOccupied;
        private FakeLBStoreItem m_currencyItem;
        private ulong m_currencyOccupied;
        private string m_currentItemGroupState;
        private readonly List<FakeLBStoreItem>[] m_candidateItemLists;
        private readonly ItemDisplayInfo[][] m_displayedItemMatrix;
        private readonly List<FakeLBStoreItem> m_itemSelectPanelScrollViewItems;
        private int m_itemSelectPanelScrollViewSelectIndex;
        private Vector2Int m_displayItemSelectIndex;
        private FakeLBStoreItem m_transformationResultItem;
        private readonly List<FakeLBStoreItem> m_previewHintPanelItemList;
        private DevelopmentTransformationUIController m_developmentTransformationUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowOrHideTransformationPanel;
        private static DelegateBridge __Hotfix_OpenPreviewHintPanel;
        private static DelegateBridge __Hotfix_OpenResultHintPanel;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnRequestCloseAllPanels;
        private static DelegateBridge __Hotfix_OnRequestShowOrHideAllPanels;
        private static DelegateBridge __Hotfix_OnRequestReturnToPreviousPanel;
        private static DelegateBridge __Hotfix_OnRequestGetIntent;
        private static DelegateBridge __Hotfix_OnRequestGetUIBackgroundManager;
        private static DelegateBridge __Hotfix_OnRequestBgTaskChange;
        private static DelegateBridge __Hotfix_OnNotifyTransformationSuccess;
        private static DelegateBridge __Hotfix_OnRequestItemSimpleInfoUITaskEnterAnotherTask;
        private static DelegateBridge __Hotfix_OnCurrentSubPanelCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBasePanelModifyButtonClick;
        private static DelegateBridge __Hotfix_OnBasePanelLaunchButtonClick;
        private static DelegateBridge __Hotfix_OnBasePanelAutoFillButtonClick;
        private static DelegateBridge __Hotfix_OnBasePanelCatalystItemClick;
        private static DelegateBridge __Hotfix_OnBasePanelDisplayedItemClick;
        private static DelegateBridge __Hotfix_OnItemSelectPanelCancelButtonClick;
        private static DelegateBridge __Hotfix_OnItemSelectPanelConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnItemSelectPanelScrollViewItemClick;
        private static DelegateBridge __Hotfix_OnConfirmationPanelStartTransformationButtonClick;
        private static DelegateBridge __Hotfix_OnShortOfItemHintPanelTransferToAuctionPanelButtonClick;
        private static DelegateBridge __Hotfix_OnResultHintPanelBackToBasePanelButtonClick;
        private static DelegateBridge __Hotfix_OnResultHintPanelItemClick;
        private static DelegateBridge __Hotfix_OnPreviewHintPanelItemClick;
        private static DelegateBridge __Hotfix_GetAvaliableUpdateItem;
        private static DelegateBridge __Hotfix_InitParams;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_ClearBasePanelData;
        private static DelegateBridge __Hotfix_ClearItemSelectPanelData;
        private static DelegateBridge __Hotfix_ClearResultHintPanelData;
        private static DelegateBridge __Hotfix_ClearPreviewHintPanelData;
        private static DelegateBridge __Hotfix_SetViewUpdatePipeLineStateMaskTypeByMode;
        private static DelegateBridge __Hotfix_GetPanelProcess;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadOnBasePanel;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadOnItemSelectPanel;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadOnResultHintPanel;
        private static DelegateBridge __Hotfix_CollectDynamicResForLoadOnPreviewHintPanel;
        private static DelegateBridge __Hotfix_OpenSimpleInfoPanel;
        private static DelegateBridge __Hotfix_UpdateViewOnBasePanel;
        private static DelegateBridge __Hotfix_UpdateViewOnItemSelectPanel;
        private static DelegateBridge __Hotfix_UpdateViewOnConfirmationHintPanel;
        private static DelegateBridge __Hotfix_UpdateViewOnResultHintPanel;
        private static DelegateBridge __Hotfix_UpdateViewOnPreviewHintPanel;
        private static DelegateBridge __Hotfix_SyncCandidateListsAndDisplayedItemsByLine;
        private static DelegateBridge __Hotfix_RemoveItemFromList;
        private static DelegateBridge __Hotfix_GetAvailableItemAndRemmoveFormList;
        private static DelegateBridge __Hotfix_IsCurrencySufficient;
        private static DelegateBridge __Hotfix_IsItemsSufficient;
        private static DelegateBridge __Hotfix_GetAuctionNpcShopItemTypeByProject;
        private static DelegateBridge __Hotfix_ClearItemSimpleInfoUITask;
        private static DelegateBridge __Hotfix_ClearSoundsInTransforamtionUI;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnRequestCloseAllPanels;
        private static DelegateBridge __Hotfix_remove_EventOnRequestCloseAllPanels;
        private static DelegateBridge __Hotfix_add_EventOnRequestShowOrHideAllPanels;
        private static DelegateBridge __Hotfix_remove_EventOnRequestShowOrHideAllPanels;
        private static DelegateBridge __Hotfix_add_EventOnRequestReturnToPreviousPanel;
        private static DelegateBridge __Hotfix_remove_EventOnRequestReturnToPreviousPanel;
        private static DelegateBridge __Hotfix_add_EventOnRequestGetIntent;
        private static DelegateBridge __Hotfix_remove_EventOnRequestGetIntent;
        private static DelegateBridge __Hotfix_add_EventOnRequestGetUIBackgroundManager;
        private static DelegateBridge __Hotfix_remove_EventOnRequestGetUIBackgroundManager;
        private static DelegateBridge __Hotfix_add_EventOnRequestBgTaskChange;
        private static DelegateBridge __Hotfix_remove_EventOnRequestBgTaskChange;
        private static DelegateBridge __Hotfix_add_EventOnNotifyTransformationSuccess;
        private static DelegateBridge __Hotfix_remove_EventOnNotifyTransformationSuccess;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnNotifyTransformationSuccess
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DevelopmentBgUITask.BackGroundStateType, string, FakeLBStoreItem, Action<bool>> EventOnRequestBgTaskChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, Action> EventOnRequestCloseAllPanels
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestGetIntent
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<IUIBackgroundManager>> EventOnRequestGetUIBackgroundManager
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRequestReturnToPreviousPanel
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, bool, Action> EventOnRequestShowOrHideAllPanels
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public DevelopmentTransformationUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearBasePanelData()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearItemSelectPanelData()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearItemSimpleInfoUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearPreviewHintPanelData()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearResultHintPanelData()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSoundsInTransforamtionUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResForLoadOnBasePanel(ref List<string> resultRes)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResForLoadOnItemSelectPanel(ref List<string> resultRes)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResForLoadOnPreviewHintPanel(ref List<string> resultRes)
        {
        }

        [MethodImpl(0x8000)]
        private void CollectDynamicResForLoadOnResultHintPanel(ref List<string> resultRes)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private NpcShopItemType GetAuctionNpcShopItemTypeByProject()
        {
        }

        [MethodImpl(0x8000)]
        private static FakeLBStoreItem GetAvailableItemAndRemmoveFormList(int count, ref List<FakeLBStoreItem> itemList)
        {
        }

        [MethodImpl(0x8000)]
        private FakeLBStoreItem GetAvaliableUpdateItem(StoreItemUpdateInfo item)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetPanelProcess(string mode, string stateName, bool immediateComplate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParams(UIIntentCustom intentCustom)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsCurrencySufficient()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsItemsSufficient()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBasePanelAutoFillButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBasePanelCatalystItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBasePanelDisplayedItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBasePanelLaunchButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBasePanelModifyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmationPanelStartTransformationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCurrentSubPanelCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSelectPanelCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSelectPanelConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSelectPanelScrollViewItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNotifyTransformationSuccess()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreviewHintPanelItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestBgTaskChange(DevelopmentBgUITask.BackGroundStateType type, string previewIconPath, FakeLBStoreItem resultInfo, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestCloseAllPanels(bool immediateComplete, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestGetIntent(Action<UIIntent> action)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestGetUIBackgroundManager(Action<IUIBackgroundManager> onGetBackgroundManagerAction)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestItemSimpleInfoUITaskEnterAnotherTask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestReturnToPreviousPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestShowOrHideAllPanels(bool isShow, bool immediateComplate, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnResultHintPanelBackToBasePanelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnResultHintPanelItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShortOfItemHintPanelTransferToAuctionPanelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OpenPreviewHintPanel()
        {
        }

        [MethodImpl(0x8000)]
        public void OpenResultHintPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void OpenSimpleInfoPanel(ILBStoreItemClient item, Vector3 worldPos, ItemSimpleInfoUITask.PositionType positionType = 0, bool showObtainSource = false, bool enableBGClickClose = true)
        {
        }

        [MethodImpl(0x8000)]
        private static bool RemoveItemFromList(FakeLBStoreItem item, ref List<FakeLBStoreItem> itemList)
        {
        }

        [MethodImpl(0x8000)]
        private void SetViewUpdatePipeLineStateMaskTypeByMode(string mode)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideTransformationPanel(bool isShow, bool immediateComplete, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SyncCandidateListsAndDisplayedItemsByLine(int lineIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewOnBasePanel()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewOnConfirmationHintPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewOnItemSelectPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewOnPreviewHintPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateViewOnResultHintPanel()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OpenSimpleInfoPanel>c__AnonStorey1
        {
            internal ILBStoreItemClient item;
            internal Vector3 worldPos;
            internal ItemSimpleInfoUITask.PositionType positionType;
            internal bool showObtainSource;
            internal bool enableBGClickClose;
            internal DevelopmentTransformationUITask $this;

            internal void <>m__0(UIIntent intent)
            {
                <OpenSimpleInfoPanel>c__AnonStorey2 storey = new <OpenSimpleInfoPanel>c__AnonStorey2 {
                    <>f__ref$1 = this,
                    intent = intent
                };
                this.$this.OnRequestGetUIBackgroundManager(new Action<IUIBackgroundManager>(storey.<>m__0));
            }

            private sealed class <OpenSimpleInfoPanel>c__AnonStorey2
            {
                internal UIIntent intent;
                internal DevelopmentTransformationUITask.<OpenSimpleInfoPanel>c__AnonStorey1 <>f__ref$1;

                internal void <>m__0(IUIBackgroundManager backgroundManager)
                {
                    string mode = "OnlyDetail";
                    if (!ClientStoreItemBaseHelper.IsItemHasDetailInfo(this.<>f__ref$1.item))
                    {
                        mode = "NoButton";
                    }
                    IUIBackgroundManager manager = backgroundManager;
                    this.<>f__ref$1.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.<>f__ref$1.item, this.intent, true, this.<>f__ref$1.worldPos, mode, this.<>f__ref$1.positionType, this.<>f__ref$1.showObtainSource, null, manager, true, true, this.<>f__ref$1.enableBGClickClose);
                    if (this.<>f__ref$1.$this.m_itemSimpleInfoUITask != null)
                    {
                        this.<>f__ref$1.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                        this.<>f__ref$1.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += new Action<string>(this.<>f__ref$1.$this.OnRequestItemSimpleInfoUITaskEnterAnotherTask);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowOrHideTransformationPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            internal void <>m__0(UIProcess process, bool b)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(b);
                }
            }
        }

        public class DevelopmentProjectStartReqNetWorkTask : NetWorkTransactionTask
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <Result>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <ProjectId>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private List<ItemInfo> <ItemInfos>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private List<StoreItemUpdateInfo> <ResultItems>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnDevelopmentProjectStartAck;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;
            private static DelegateBridge __Hotfix_get_ProjectId;
            private static DelegateBridge __Hotfix_set_ProjectId;
            private static DelegateBridge __Hotfix_get_ItemInfos;
            private static DelegateBridge __Hotfix_set_ItemInfos;
            private static DelegateBridge __Hotfix_get_ResultItems;
            private static DelegateBridge __Hotfix_set_ResultItems;

            public DevelopmentProjectStartReqNetWorkTask(int projectId, List<ItemInfo> itemList) : base(10f, true, false, false)
            {
                this.ProjectId = projectId;
                this.ItemInfos = itemList;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp104(this, projectId, itemList);
                }
            }

            private void OnDevelopmentProjectStartAck(int result, int projectId, List<StoreItemUpdateInfo> resultList)
            {
                DelegateBridge bridge = __Hotfix_OnDevelopmentProjectStartAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp2961(this, result, projectId, resultList);
                }
                else
                {
                    this.Result = result;
                    this.ProjectId = projectId;
                    this.ResultItems = resultList;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnDevelopmentProjectStartAck += new Action<int, int, List<StoreItemUpdateInfo>>(this.OnDevelopmentProjectStartAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendDevelopmentProjectStartReq(this.ProjectId, this.ItemInfos));
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnDevelopmentProjectStartAck -= new Action<int, int, List<StoreItemUpdateInfo>>(this.OnDevelopmentProjectStartAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }

            public int ProjectId
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_ProjectId;
                    return ((bridge == null) ? this.<ProjectId>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_ProjectId;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<ProjectId>k__BackingField = value;
                    }
                }
            }

            public List<ItemInfo> ItemInfos
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_ItemInfos;
                    return ((bridge == null) ? this.<ItemInfos>k__BackingField : bridge.__Gen_Delegate_Imp1587(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_ItemInfos;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<ItemInfos>k__BackingField = value;
                    }
                }
            }

            public List<StoreItemUpdateInfo> ResultItems
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_ResultItems;
                    return ((bridge == null) ? this.<ResultItems>k__BackingField : bridge.__Gen_Delegate_Imp4434(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_ResultItems;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<ResultItems>k__BackingField = value;
                    }
                }
            }
        }

        public class ItemDisplayInfo
        {
            public string m_itemDisplayState;
            public string m_itemChooseState;
            public FakeLBStoreItem m_itemInfo;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_Clear;

            [MethodImpl(0x8000)]
            public void Clear()
            {
            }
        }

        public class ItemDisplayInfoMatrixWrapper
        {
            public DevelopmentTransformationUITask.ItemDisplayInfo[][] m_matrix;
            private static DelegateBridge _c__Hotfix_ctor;

            public ItemDisplayInfoMatrixWrapper()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            UpdateDataCache,
            UpdateViewOnBasePanelDescriptionArea,
            UpdateViewOnBasePanelCatalystItemArea,
            UpdateViewOnBasePanelItemArea,
            UpdateViewOnBasePanelLaunchButtonArea,
            UpdateViewOnItemSelectPanelScrollViewAreaAndItemSelectState,
            UpdateViewOnItemSelectPanelOnlyItemSelectState,
            UpdateViewOnItemSelectPanelItemArea,
            UpdateViewOnConfirmationHintPanel,
            UpdateViewOnResultHintPanelAllDisplayedItems,
            UpdateViewOnPreviewHintPanelAllDisplayedItems,
            UpdateViewOnBackGroundStateChange,
            UpdateViewOnSoundChange,
            PanelChange
        }
    }
}

