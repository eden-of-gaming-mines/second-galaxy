﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ItemSimpleInfoBlueprintMaterialUICtrl : UIControllerBase
    {
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./DetailGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ValueText;
        [AutoBind("./Image/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image IconImage;
        [AutoBind("./Image/Bg", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_bgState;
        private static DelegateBridge __Hotfix_UpdateMaterialInfo;
        private static DelegateBridge __Hotfix_UpdateCurrencyInfo;

        [MethodImpl(0x8000)]
        public void UpdateCurrencyInfo(CostInfo costInfo, Sprite sprite, bool is4Guild)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMaterialInfo(CostInfo costInfo, Sprite sprite)
        {
        }
    }
}

