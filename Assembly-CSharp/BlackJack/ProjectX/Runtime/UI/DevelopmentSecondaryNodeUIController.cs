﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class DevelopmentSecondaryNodeUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private ConfigDataDevelopmentNodeDescInfo <NodeInfo>k__BackingField;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_nodeClickButton;
        [AutoBind("./ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_chooseStateCtrl;
        [AutoBind("./GrayImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_disableStateCtrl;
        [AutoBind("./QualityImage01", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_iconImage;
        private static DelegateBridge __Hotfix_UpdateViewOnNode;
        private static DelegateBridge __Hotfix_get_NodeInfo;
        private static DelegateBridge __Hotfix_set_NodeInfo;

        [MethodImpl(0x8000)]
        public void UpdateViewOnNode(ConfigDataDevelopmentNodeDescInfo nodeInfo, string chooseStateName, string disableStateName, Dictionary<string, Object> resDict)
        {
        }

        public ConfigDataDevelopmentNodeDescInfo NodeInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

