﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class BranchStoryPuzzleDetailUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Vector3> EventOnItemShowTipButtonClick;
        private List<BranchStoryPuzzleDetailItemUIController> m_puzzleQuestItemDetailCtrlList;
        private DateTime m_setScrollViewEndTime;
        [AutoBind("./Viewport/Content/ThreadGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject PuzzleQuestItem;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect ScrollView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdatePuzzleQuestListDetailInfo;
        private static DelegateBridge __Hotfix_SetScrollViewToEnd;
        private static DelegateBridge __Hotfix_GetPuzzleQuestListShowUIProcess;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetDestItemCtrl;
        private static DelegateBridge __Hotfix_SetScrollViewEnd;
        private static DelegateBridge __Hotfix_OnItemShowTipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemShowTipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemShowTipButtonClick;

        public event Action<Vector3> EventOnItemShowTipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected BranchStoryPuzzleDetailItemUIController GetDestItemCtrl(int puzzleQuestId)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPuzzleQuestListShowUIProcess(List<BranchStoryUITask.BranchStorySubQuestInfo> questList, int endShowQuestId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemShowTipButtonClick(Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void SetScrollViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void SetScrollViewToEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePuzzleQuestListDetailInfo(BranchStoryUITask.BranchStoryItemInfo branchStoryInfo, bool isResetExpandState)
        {
        }
    }
}

