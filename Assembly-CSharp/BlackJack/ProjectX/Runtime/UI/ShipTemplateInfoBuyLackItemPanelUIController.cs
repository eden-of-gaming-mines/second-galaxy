﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipTemplateInfoBuyLackItemPanelUIController : UIControllerBase
    {
        private readonly List<CommonItemIconUIController> m_itemCtrlList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnItemClick;
        protected const string PoolName = "CommonItemUIPrefab";
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buyLackItemConfirmButton;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buyLackItemCancelButton;
        [AutoBind("./DetailPanel/ConfirmButton/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bindMoneyText;
        [AutoBind("./UnusedItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        [AutoBind("./DetailPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_lackItemListRoot;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buyLackItemStateCtrl;
        private static DelegateBridge __Hotfix_CreateBuyLackWindowShowProcess;
        private static DelegateBridge __Hotfix_CreateBuyLackWindowCloseProcess;
        private static DelegateBridge __Hotfix_UpdateBuyLackItemPanel;
        private static DelegateBridge __Hotfix_GetItemISimpleInfoPos;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemCreated;
        private static DelegateBridge __Hotfix_PrepareSlotGroupCtrlList;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;

        public event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateBuyLackWindowCloseProcess(bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateBuyLackWindowShowProcess(bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemISimpleInfoPos(RectTransform sourceTrans, out ItemSimpleInfoUITask.PositionType posType)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemCreated(string poolName, GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        protected void PrepareSlotGroupCtrlList(List<CommonItemIconUIController> ctrlList, int needCount, Transform ctrlRoot)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuyLackItemPanel(List<ItemInfo> itemList, double price, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }
    }
}

