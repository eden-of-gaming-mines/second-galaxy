﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using Dest.Math;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using TouchScript.Gestures;
    using UnityEngine;
    using UnityEngine.UI;
    using Vectrosity;

    public class StarMapForStarfieldUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapDrawingStarfieldDesc m_drawingConfig;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapForStarfieldToSolarSystemFadeInOutDesc m_switchToSolarSystemFadeInOutConfig;
        [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_panelTrans;
        [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
        public TransformGesture m_transformGesture;
        [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_panelButton;
        [AutoBind("./Panel/StarMapNavigationLineRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starMapNavigationLineRoot;
        [AutoBind("./Panel/StarMapTradeShipTraceRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starMapTradeShipTraceRoot;
        [AutoBind("./Panel/StarMapRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starMapRoot;
        [AutoBind("./Panel/StarMapRoot/StarMapLineRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starMapLineRoot;
        [AutoBind("./Panel/StarMapRoot/StarMapSSPointRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starMapSSPointRoot;
        [AutoBind("./Panel/StarMapRoot/StarMapSSSimplePointRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starMapSSSimplePointRoot;
        [AutoBind("./Panel/StarMapRoot/StarMapDetailNavigationLineRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starMapDetailNavigationLineRoot;
        [AutoBind("./Panel/LeftBottomRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_leftBottomTrans;
        [AutoBind("./Panel/RightTopRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_rightTopTrans;
        [AutoBind("./Panel/DrawingSSLeftBottomRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_drawingSSLeftBottomTrans;
        [AutoBind("./Panel/DrawingSSRightTopRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_drawingSSRightTopTrans;
        [AutoBind("./Panel/StarfieldBrushInfoUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_starfieldBrushInfoUITrans;
        [AutoBind("./Panel/OrientationUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_orientationUIRoot;
        [AutoBind("./Panel/WndRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_wndRootTrans;
        [AutoBind("./Panel/SolarSystemNameTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_solarSystemNameTextRoot;
        [AutoBind("./Panel/StargroupNameTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_stargroupNameTextRoot;
        [AutoBind("./Panel/StarfieldNameTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starfieldNameTextRoot;
        [AutoBind("./Panel/SolarSystemQuestIconGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_solarSystemQuestIconRoot;
        [AutoBind("./Panel/StarfieldColorBlockRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starfieldColorBlockRoot;
        [AutoBind("./Panel/StarfieldColorBlockRoot", AutoBindAttribute.InitState.NotInit, false)]
        public StarfieldBlockPolygonController m_starfieldColorBlockCtrl;
        [AutoBind("./Panel/StarfieldColorBlockBorderLineRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starfieldColorBlockBorderLineRoot;
        [AutoBind("./Panel/StarfieldColorBlockBorderLineRoot", AutoBindAttribute.InitState.NotInit, false)]
        public StarfieldBlockPolygonController m_starfieldColorBlockBorderLineCtrl;
        [AutoBind("./Panel/GuildView", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapForStarfieldGuildViewController m_guildView;
        [AutoBind("./Panel/AllianceView", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapForStarfieldGuildViewController m_allianceView;
        [AutoBind("./Panel/SolarSystemNameTextGroup/TextInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_solarSystemNameObj;
        [AutoBind("./Panel/StargroupNameTextGroup/TextInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_stargroupNameObj;
        [AutoBind("./Panel/StarfieldNameTextGroup/TextInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_starfieldNameObj;
        [AutoBind("./Panel/SolarSystemQuestIconGroup/QuestIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_solarSystemQuestIcon;
        [AutoBind("./Panel/DestinationEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_destinationEffectStateCtrl;
        [AutoBind("./Panel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./Panel/GuildIconView", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_guildIconRoot;
        [AutoBind("./Panel/GuildIconView/GuildIcon", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapIconController m_guildIconTemplate;
        [AutoBind("./Panel/AllianceIconView", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_allianceIconRoot;
        [AutoBind("./Panel/AllianceIconView/AllianceIcon", AutoBindAttribute.InitState.NotInit, false)]
        public StarMapIconController m_allianceIconTemplate;
        [AutoBind("./Panel/StarMapSearchUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_starMapSearchUITrans;
        [AutoBind("./Panel/TunnelEffect", AutoBindAttribute.InitState.NotInit, false)]
        public TunnelEffectUIController m_tunnelEffectCtrl;
        [AutoBind("./Panel/TunnelEffect", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tunnelEffectTrans;
        public SSNodePopMenuUIController m_SSNodePopMenuCtrl;
        public StarMapForStarfieldBrushInfoUIController m_brushInfoUICtrl;
        private Canvas m_canvas;
        private Camera m_camrea;
        private float m_currScale;
        private Vector2 m_currCenterPos;
        private Vector2 m_starMapCanvasLeftBottomLocalPos;
        private Vector2 m_starMapCanvasRightTopLocalPos;
        private AAB2 m_starMapDrawingAABB;
        private int m_selQuestSolarSystemId;
        private int m_destinateSolarSystemId;
        private List<GDBSolarSystemSimpleInfo> m_navigationSolarSystemInfoList;
        private HashSet<int> m_navigationSSIdSet;
        private Vector2 m_starfieldDrawingLeftBottomPos;
        private Vector2 m_starfieldDrawingRightTopPos;
        private bool m_starfieldMapNeedInit;
        public BaseRedeploySimpleInfoUIController m_baseRedeployInfoUIController;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBaseRedeployInfoButtonClick;
        public StarMapForStarfieldSearchUIController m_searchFieldUICtr;
        public const float MapScale = 7f;
        private List<GDBSolarSystemSimpleInfo>[] m_tradeShipTraces;
        public int m_showTunnelType;
        private DateTime m_requestDataDelayTime;
        private List<GameObject> m_starfieldNameTextObjs;
        private List<GameObject> m_stargroupNameTextObjs;
        private List<GameObject> m_solarSystemNameTextObjs;
        protected bool m_isFadeInOutProcessActive;
        protected float m_cameraMoveStartTime;
        protected float m_cameraMoveEndTime;
        protected bool m_hasCameraMoveFinished;
        protected Vector2 m_cameraMoveStartPos;
        protected Vector2 m_cameraMoveEndPos;
        protected float m_cameraScaleStartTime;
        protected float m_cameraScaleEndTime;
        protected bool m_hasCameraScaleFinished;
        protected float m_cameraScaleStartValue;
        protected float m_cameraScaleEndValue;
        protected float m_processEndTime;
        protected bool m_onProcessEndPrepared;
        public Action<bool> m_onProcessEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnStarMapItemLocationUIClick;
        public List<StarMapItemLocationUIController> m_locationUIItemList;
        private List<LogicBlockStarMapInfoClient.LocationItemInfo> m_locationUIInfoList;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnDrawingSolarSystemListChanged;
        private bool m_isNeedDrawStarfieldStarMap;
        private bool m_isCollectDrawingSolarSystemList;
        private bool m_isIgnoreDrawingSolarSystemListChanged;
        private GEBrushType m_brushType;
        private Material m_starMapLineMaterial;
        private Material m_starMapPointMaterial;
        private Material m_starMapNavigationLineMaterial;
        private Material m_starMapStarfieldColorBlockMaterial;
        private Material m_starMapStarfieldBorderLineMaterial;
        private VectorLine m_starfieldLines;
        private VectorLine m_starfieldSSPoints;
        private VectorLine m_starfieldSimpleNavigationLine;
        private VectorLine m_starfieldDetailNavigationLine;
        private readonly List<VectorLine> m_starfieldTradeShipsLine;
        private readonly Color[] m_tradeShipTraceColor;
        private GameObject m_starfieldPointTempObj;
        private StarMapForStarfieldSSPointUIController m_starfieldPointTempCtrl;
        private Vector2[] m_starfieldColorBlockMeshVerticalCanvasPosList;
        private int[] m_starfieldColorBlockMeshTriangles;
        private Color[] m_starfieldColorBlockMeshColors;
        private Vector2[] m_starfieldBorderLineMeshVerticalCanvasPosList;
        private int[] m_starfieldBorderLineMeshTriangles;
        private Color[] m_starfieldBorderLineMeshColors;
        private readonly List<GDBSolarSystemSimpleInfo> m_drawingSolarSystemList;
        private List<Vector2> m_drawingSolarSystemPosList;
        private List<Color32> m_drawingSolarSystemColorList;
        private List<StarMapForStarfieldSSPointUIController> m_drawingSolarSystemPoints;
        private Dictionary<int, StarMapForStarfieldSSPointUIController> m_drawingSolarSystemPointDict;
        private HashSet<int> m_drawingSolarSystemSimplePointSet;
        private List<KeyValuePair<GDBSolarSystemSimpleInfo, GDBSolarSystemSimpleInfo>> m_drawingNormalSolarSystemLinkInfoList;
        private List<KeyValuePair<GDBSolarSystemSimpleInfo, GDBSolarSystemSimpleInfo>> m_drawingNavigationSolarSystemLinkInfoList;
        private Dictionary<int, HashSet<int>> m_drawingSolarSystemLinkInfoDict;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GDBSolarSystemSimpleInfo> EventOnSolarSystemNodeClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GDBSolarSystemSimpleInfo> EventOnSolarSystemNode3DTouch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnEmptyAreaClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSSNodePopMenu_DetailInfoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSSNodePopMenu_JumpButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSSNodePopMenu_EnterStarMapForSSButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong, ulong, int> EventOnSSNodePopMenu_TunnelJumpButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnSSNodePopMenu_BaseRedeployButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float> EventOnTransformGestureScale;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<float, float> EventOnTransformGestureSlipping;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCloseButtonClick;
        private List<GuildTeleportTunnelEffectInfoClient> m_tunnelList;
        private GDBSolarSystemSimpleInfo m_tunnelCenterSolar;
        private float m_tunnelRange;
        private ulong m_tunnelId;
        private static DelegateBridge __Hotfix_GetSolarSystemPointPrefabById;
        private static DelegateBridge __Hotfix_GetPopupSolarSystemInfoWndEnterStarMapButtonTrans;
        private static DelegateBridge __Hotfix_GetPopupSolarSystemInfoWndJumpButtonTrans;
        private static DelegateBridge __Hotfix_CheckPopupSolarSystemInfoShowFinished;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetUICanvas;
        private static DelegateBridge __Hotfix_SetUICamera;
        private static DelegateBridge __Hotfix_InitStarMapForStarfield;
        private static DelegateBridge __Hotfix_SetStarMapDrawingRes;
        private static DelegateBridge __Hotfix_UpdateStarMapCenterPos;
        private static DelegateBridge __Hotfix_SetTrace;
        private static DelegateBridge __Hotfix_ClearTrace;
        private static DelegateBridge __Hotfix_UpdateStarMapBrushType;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_LateUpdate;
        private static DelegateBridge __Hotfix_ShowBaseRedeployInfo;
        private static DelegateBridge __Hotfix_SetTunnelEffect;
        private static DelegateBridge __Hotfix_GetCurrStarMapScale;
        private static DelegateBridge __Hotfix_GetCurrStarMapCenterPos;
        private static DelegateBridge __Hotfix_CalcStarfieldDrawingPosRange;
        private static DelegateBridge __Hotfix_GetPointAfterTranslationAndScaling;
        private static DelegateBridge __Hotfix_GetPointBeforeTranslationAndScaling;
        private static DelegateBridge __Hotfix_ShowOrHideDestinateEffect;
        private static DelegateBridge __Hotfix_OnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_add_EventOnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBaseRedeployInfoButtonClick;
        private static DelegateBridge __Hotfix_UpdateStarfieldNameTextVisibilty;
        private static DelegateBridge __Hotfix_UpdateStargroupNameTextVisibilty;
        private static DelegateBridge __Hotfix_UpdateSolarSystemNameTextVisibility;
        private static DelegateBridge __Hotfix_UpdateQuestIcon;
        private static DelegateBridge __Hotfix_UpdateDestinationIcon;
        private static DelegateBridge __Hotfix_TransformWorldPosToPanelLocalPos;
        private static DelegateBridge __Hotfix_IsPosVisibleOnDrawingRange;
        private static DelegateBridge __Hotfix_GetStarfieldFadeOutProcessExecutor;
        private static DelegateBridge __Hotfix_GetStarfieldFadeInProcessExecutor_1;
        private static DelegateBridge __Hotfix_GetStarfieldFadeInProcessExecutor_0;
        private static DelegateBridge __Hotfix_StartStarfieldFadeInOutProcess;
        private static DelegateBridge __Hotfix_SetFinalCameraStateDirectly;
        private static DelegateBridge __Hotfix_GetFadeOutEffectTimeLength;
        private static DelegateBridge __Hotfix_GetFadeInEffectTimeLength;
        private static DelegateBridge __Hotfix_TickForSwitchToSolarSystem;
        private static DelegateBridge __Hotfix_TickForSwitchToSolarSystem_CameraMove;
        private static DelegateBridge __Hotfix_TickForSwitchToSolarSystem_CameraScale;
        private static DelegateBridge __Hotfix_TickForSwitchToSolarSystem_ProcessEnd;
        private static DelegateBridge __Hotfix_CheckSwitchToSolarSystemPrepared;
        private static DelegateBridge __Hotfix_UpdateOrientationUIInfo;
        private static DelegateBridge __Hotfix_UpdateAllLocationUIItems;
        private static DelegateBridge __Hotfix_CreateStarMapLocationUIItem;
        private static DelegateBridge __Hotfix_OnStarMapItemLocationUIClick;
        private static DelegateBridge __Hotfix_add_EventOnStarMapItemLocationUIClick;
        private static DelegateBridge __Hotfix_remove_EventOnStarMapItemLocationUIClick;
        private static DelegateBridge __Hotfix_GetDrawingSolarSystemList;
        private static DelegateBridge __Hotfix_SetStarfieldNeedRedraw;
        private static DelegateBridge __Hotfix_SetStarfieldNeedRedraw4Input;
        private static DelegateBridge __Hotfix_StarfieldMapDrawingPipeLine;
        private static DelegateBridge __Hotfix_IsShowAllStarfieldDrawingRoot;
        private static DelegateBridge __Hotfix_UpdateStarfieldColorBlock;
        private static DelegateBridge __Hotfix_SetAllStarfieldDrawingRootActive;
        private static DelegateBridge __Hotfix_CalcVisibleSolarSystemPointPosList;
        private static DelegateBridge __Hotfix_CalcSolarSystemPointColors;
        private static DelegateBridge __Hotfix_GetColorBySolarSystemInfo;
        private static DelegateBridge __Hotfix_CreateSolarSystemPoints;
        private static DelegateBridge __Hotfix_CreateSolarSystemPointsWithPrefab;
        private static DelegateBridge __Hotfix_CreateSolarSystemPointsWithSimplePoint;
        private static DelegateBridge __Hotfix_CreateSolarSystemLines;
        private static DelegateBridge __Hotfix_CalcSolarSystemLinkInfoList;
        private static DelegateBridge __Hotfix_IsNavigationSolarSystemPair;
        private static DelegateBridge __Hotfix_CalcSolarSystemLinkInfoUnityPosList;
        private static DelegateBridge __Hotfix_CalcSolarSystemLinkInfoUnityPosListWithPoints;
        private static DelegateBridge __Hotfix_CalcSolarSystemLinkInfoUnityPosListWithSimplePoints;
        private static DelegateBridge __Hotfix_CalcSolarSystemLinkLineColors;
        private static DelegateBridge __Hotfix_UpdateVectorLineForStarfieldLines;
        private static DelegateBridge __Hotfix_UpdateVectorLineForDetailNavigationLines;
        private static DelegateBridge __Hotfix_GetLineWidthByCurrScaleValue;
        private static DelegateBridge __Hotfix_UpdateSimpleNavigationLine;
        private static DelegateBridge __Hotfix_UpdateTradeShipTraceLine;
        private static DelegateBridge __Hotfix_TransformSolarSystemPosToUnityPos_1;
        private static DelegateBridge __Hotfix_TransformSolarSystemPosToUnityPos_0;
        private static DelegateBridge __Hotfix_GetPointWidthByCurrScaleValue;
        private static DelegateBridge __Hotfix_GetStarMapAlphaByCurrScaleValue;
        private static DelegateBridge __Hotfix_GetStarMapPrefabAlphaByCurrScaleValue;
        private static DelegateBridge __Hotfix_GetStarMapSimplePointAlphaByCurrScaleValue;
        private static DelegateBridge __Hotfix_GetNavigationLineAlphaByCurrScaleValue;
        private static DelegateBridge __Hotfix_GetStarfieldGuildColorAlphaByCurrScaleValue;
        private static DelegateBridge __Hotfix_GetStarfieldColorBlockAlphaByCurrScaleValue;
        private static DelegateBridge __Hotfix_GetStarfieldBorderLineAlphaByCurrScaleValue;
        private static DelegateBridge __Hotfix_InitStarfieldColorBlockInfo;
        private static DelegateBridge __Hotfix_InitStarfieldBorderLineInfo;
        private static DelegateBridge __Hotfix_GenerateStarfieldColorBlockOnCanvas;
        private static DelegateBridge __Hotfix_GenerateStarfieldBorderLineOnCanvas;
        private static DelegateBridge __Hotfix_GetLineColorPairByTwoSolarSystems;
        private static DelegateBridge __Hotfix_IsExistSameSolarSystemLinkInfoOnCache;
        private static DelegateBridge __Hotfix_NormalizeSolarSystemLinePosByDrawingRange;
        private static DelegateBridge __Hotfix_IsNeedDrawingSSPrefabPoint;
        private static DelegateBridge __Hotfix_IsNeedDrawingSSSimplePoint;
        private static DelegateBridge __Hotfix_add_EventOnDrawingSolarSystemListChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDrawingSolarSystemListChanged;
        private static DelegateBridge __Hotfix_OnSSPointButtonClick;
        private static DelegateBridge __Hotfix_OnSSPoint3DTouch;
        private static DelegateBridge __Hotfix_GetTransformById;
        private static DelegateBridge __Hotfix_OnScreenTransformGestureTransformed;
        private static DelegateBridge __Hotfix_OnEmptyPosClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_DetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_JumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_TunnelJumpButtonClick;
        private static DelegateBridge __Hotfix_OnSSNodePopMenu_BaseRedeployButton;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_SetCurrentTunnel;
        private static DelegateBridge __Hotfix_SetJumpPanel;
        private static DelegateBridge __Hotfix_ShowSSNodeClickPopMenu;
        private static DelegateBridge __Hotfix_HideSSNodeClickPopMenu;
        private static DelegateBridge __Hotfix_IsSSNodeClickPopMenuEnable;
        private static DelegateBridge __Hotfix_UpdateSSNodeClickPopMenuPos;
        private static DelegateBridge __Hotfix_DoScale;
        private static DelegateBridge __Hotfix_DoSlipping;
        private static DelegateBridge __Hotfix_add_EventOnSolarSystemNodeClick;
        private static DelegateBridge __Hotfix_remove_EventOnSolarSystemNodeClick;
        private static DelegateBridge __Hotfix_add_EventOnSolarSystemNode3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnSolarSystemNode3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnEmptyAreaClick;
        private static DelegateBridge __Hotfix_remove_EventOnEmptyAreaClick;
        private static DelegateBridge __Hotfix_add_EventOnSSNodePopMenu_DetailInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSNodePopMenu_DetailInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSNodePopMenu_JumpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSNodePopMenu_JumpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSNodePopMenu_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSNodePopMenu_EnterStarMapForSSButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSNodePopMenu_TunnelJumpButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSNodePopMenu_TunnelJumpButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSSNodePopMenu_BaseRedeployButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSSNodePopMenu_BaseRedeployButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTransformGestureScale;
        private static DelegateBridge __Hotfix_remove_EventOnTransformGestureScale;
        private static DelegateBridge __Hotfix_add_EventOnTransformGestureSlipping;
        private static DelegateBridge __Hotfix_remove_EventOnTransformGestureSlipping;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;

        public event Action EventOnBaseRedeployInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnDrawingSolarSystemListChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnEmptyAreaClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemSimpleInfo> EventOnSolarSystemNode3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GDBSolarSystemSimpleInfo> EventOnSolarSystemNodeClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSSNodePopMenu_BaseRedeployButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSSNodePopMenu_DetailInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSSNodePopMenu_EnterStarMapForSSButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnSSNodePopMenu_JumpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong, ulong, int> EventOnSSNodePopMenu_TunnelJumpButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnStarMapItemLocationUIClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float> EventOnTransformGestureScale
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<float, float> EventOnTransformGestureSlipping
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CalcSolarSystemLinkInfoList()
        {
        }

        [MethodImpl(0x8000)]
        private List<Vector2> CalcSolarSystemLinkInfoUnityPosList(List<KeyValuePair<GDBSolarSystemSimpleInfo, GDBSolarSystemSimpleInfo>> solarSystemLineList)
        {
        }

        [MethodImpl(0x8000)]
        private List<Vector2> CalcSolarSystemLinkInfoUnityPosListWithPoints(List<KeyValuePair<GDBSolarSystemSimpleInfo, GDBSolarSystemSimpleInfo>> solarSystemLineList)
        {
        }

        [MethodImpl(0x8000)]
        private List<Vector2> CalcSolarSystemLinkInfoUnityPosListWithSimplePoints(List<KeyValuePair<GDBSolarSystemSimpleInfo, GDBSolarSystemSimpleInfo>> solarSystemLineList)
        {
        }

        [MethodImpl(0x8000)]
        private List<Color32> CalcSolarSystemLinkLineColors(List<KeyValuePair<GDBSolarSystemSimpleInfo, GDBSolarSystemSimpleInfo>> solarSystemLineList)
        {
        }

        [MethodImpl(0x8000)]
        private void CalcSolarSystemPointColors()
        {
        }

        [MethodImpl(0x8000)]
        private void CalcStarfieldDrawingPosRange(ref Vector2 leftBottomPos, ref Vector2 rightTopPos)
        {
        }

        [MethodImpl(0x8000)]
        private bool CalcVisibleSolarSystemPointPosList()
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckPopupSolarSystemInfoShowFinished()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckSwitchToSolarSystemPrepared()
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearTrace()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateSolarSystemLines()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateSolarSystemPoints()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateSolarSystemPointsWithPrefab()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateSolarSystemPointsWithSimplePoint()
        {
        }

        [MethodImpl(0x8000)]
        protected StarMapItemLocationUIController CreateStarMapLocationUIItem()
        {
        }

        [MethodImpl(0x8000)]
        public void DoScale(float scale)
        {
        }

        [MethodImpl(0x8000)]
        public void DoSlipping(float offsetX, float offsetY)
        {
        }

        [MethodImpl(0x8000)]
        private void GenerateStarfieldBorderLineOnCanvas()
        {
        }

        [MethodImpl(0x8000)]
        private void GenerateStarfieldColorBlockOnCanvas()
        {
        }

        [MethodImpl(0x8000)]
        private Color GetColorBySolarSystemInfo(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetCurrStarMapCenterPos()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCurrStarMapScale()
        {
        }

        [MethodImpl(0x8000)]
        public List<GDBSolarSystemSimpleInfo> GetDrawingSolarSystemList()
        {
        }

        [MethodImpl(0x8000)]
        public float GetFadeInEffectTimeLength()
        {
        }

        [MethodImpl(0x8000)]
        public float GetFadeOutEffectTimeLength()
        {
        }

        [MethodImpl(0x8000)]
        private KeyValuePair<Color, Color> GetLineColorPairByTwoSolarSystems(GDBSolarSystemSimpleInfo s1, GDBSolarSystemSimpleInfo s2)
        {
        }

        [MethodImpl(0x8000)]
        private float GetLineWidthByCurrScaleValue(float ratio = 1f)
        {
        }

        [MethodImpl(0x8000)]
        private float GetNavigationLineAlphaByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 GetPointAfterTranslationAndScaling(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 GetPointBeforeTranslationAndScaling(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private float GetPointWidthByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetPopupSolarSystemInfoWndEnterStarMapButtonTrans()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetPopupSolarSystemInfoWndJumpButtonTrans()
        {
        }

        [MethodImpl(0x8000)]
        public StarMapForStarfieldSSPointUIController GetSolarSystemPointPrefabById(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private float GetStarfieldBorderLineAlphaByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        private float GetStarfieldColorBlockAlphaByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess.UIProcessExecutor GetStarfieldFadeInProcessExecutor(int orientedSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess.UIProcessExecutor GetStarfieldFadeInProcessExecutor(Vector2 startCenterPos, float startScale, Vector2 endCenterPos, float endScale)
        {
        }

        [MethodImpl(0x8000)]
        public CustomUIProcess.UIProcessExecutor GetStarfieldFadeOutProcessExecutor(int targetSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private float GetStarfieldGuildColorAlphaByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        private float GetStarMapAlphaByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        private float GetStarMapPrefabAlphaByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        private float GetStarMapSimplePointAlphaByCurrScaleValue()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetTransformById(int id)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideSSNodeClickPopMenu(bool immediateHide = false)
        {
        }

        [MethodImpl(0x8000)]
        private void InitStarfieldBorderLineInfo(GameObject meshObj)
        {
        }

        [MethodImpl(0x8000)]
        private void InitStarfieldColorBlockInfo(GameObject meshObj)
        {
        }

        [MethodImpl(0x8000)]
        public void InitStarMapForStarfield(int selQuestSolarSystemId, int destinateSolarSystemId, GEBrushType brushType, GameObject colorBlockMeshObj, GameObject colorBlockBorderLineMeshObj, float initScale = 1f, float initCenterX = 0f, float initCenterY = 0f, List<GDBSolarSystemSimpleInfo> navigationInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsExistSameSolarSystemLinkInfoOnCache(int srcSolarSystemId, int destSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        private int IsNavigationSolarSystemPair(GDBSolarSystemSimpleInfo s1, GDBSolarSystemSimpleInfo s2)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedDrawingSSPrefabPoint()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsNeedDrawingSSSimplePoint()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPosVisibleOnDrawingRange(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsShowAllStarfieldDrawingRoot()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSSNodeClickPopMenuEnable()
        {
        }

        [MethodImpl(0x8000)]
        private void LateUpdate()
        {
        }

        [MethodImpl(0x8000)]
        private bool NormalizeSolarSystemLinePosByDrawingRange(ref Vector2 pos1, ref Vector2 pos2)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBaseRedeployInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEmptyPosClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnScreenTransformGestureTransformed(object sender, EventArgs e)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_BaseRedeployButton(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_DetailInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_EnterStarMapForSSButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_JumpButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSNodePopMenu_TunnelJumpButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSPoint3DTouch(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSPointButtonClick(GDBSolarSystemSimpleInfo solarSystemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapItemLocationUIClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAllStarfieldDrawingRootActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrentTunnel(ulong tunnelId, float enableRange = 0f, GDBSolarSystemSimpleInfo centerSolar = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetFinalCameraStateDirectly()
        {
        }

        [MethodImpl(0x8000)]
        public void SetJumpPanel(GDBSolarSystemSimpleInfo currentSolarSystem, List<GuildTeleportTunnelEffectInfoClient> tunnelList = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarfieldNeedRedraw(bool collectDrawingSolarSystemList = true, bool ignoreSolarSystemListChanged = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetStarfieldNeedRedraw4Input()
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarMapDrawingRes(Material lineMaterial, Material pointMaterial, Material navigationLineMaterial, Material starfieldColorBlockMaterial, Material starfieldBorderLineMaterial)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTrace(List<int> traceToList = null, int from = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTunnelEffect(int tunnelStar, ulong tunnelId, float effectRange = 0f)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUICamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUICanvas(Canvas canvas)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowBaseRedeployInfo(MSRedeployInfo redeployInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOrHideDestinateEffect(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowSSNodeClickPopMenu(int solarSystemId, Dictionary<string, UnityEngine.Object> resDict, bool immediateShow = false)
        {
        }

        [MethodImpl(0x8000)]
        private void StarfieldMapDrawingPipeLine()
        {
        }

        [MethodImpl(0x8000)]
        public void StartStarfieldFadeInOutProcess(Vector2 startCenterPos, float startScale, Vector2 endCenterPos, float endScale, Action<bool> onProcessEnd, bool isFadein)
        {
        }

        [MethodImpl(0x8000)]
        private void TickForSwitchToSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForSwitchToSolarSystem_CameraMove()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForSwitchToSolarSystem_CameraScale()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForSwitchToSolarSystem_ProcessEnd()
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 TransformSolarSystemPosToUnityPos(GDBSolarSystemInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private Vector2 TransformSolarSystemPosToUnityPos(GDBSolarSystemSimpleInfo info)
        {
        }

        [MethodImpl(0x8000)]
        private Vector3 TransformWorldPosToPanelLocalPos(Vector3 worldPos)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAllLocationUIItems()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDestinationIcon()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateOrientationUIInfo(List<LogicBlockStarMapInfoClient.LocationItemInfo> uiItemInfoList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateQuestIcon()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSimpleNavigationLine()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSolarSystemNameTextVisibility()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSSNodeClickPopMenuPos()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateStarfieldColorBlock()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateStarfieldNameTextVisibilty()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateStargroupNameTextVisibilty()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarMapBrushType(GEBrushType brushType, Dictionary<string, UnityEngine.Object> resDict = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStarMapCenterPos(Vector2 centerPos, float initScale = 45f, List<int> traceToList = null, int from = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTradeShipTraceLine()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateVectorLineForDetailNavigationLines(List<Vector2> linkLinePosList, List<Color32> linkLineColorList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateVectorLineForStarfieldLines(List<Vector2> linkLinePosList, List<Color32> linkLineColorList)
        {
        }

        [CompilerGenerated]
        private sealed class <GetStarfieldFadeInProcessExecutor>c__AnonStorey1
        {
            internal Vector2 startCenterPos;
            internal float startScale;
            internal Vector2 endCenterPos;
            internal float endScale;
            internal StarMapForStarfieldUIController $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.StartStarfieldFadeInOutProcess(this.startCenterPos, this.startScale, this.endCenterPos, this.endScale, onEnd, true);
            }
        }

        [CompilerGenerated]
        private sealed class <GetStarfieldFadeInProcessExecutor>c__AnonStorey2
        {
            internal int orientedSolarSystemId;
            internal StarMapForStarfieldUIController $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                GDBSolarSystemSimpleInfo gDBSolarSystemSimpleInfo = ConfigDataHelper.GDBDataLoader.GetGDBSolarSystemSimpleInfo(this.orientedSolarSystemId);
                this.$this.StartStarfieldFadeInOutProcess(this.$this.TransformSolarSystemPosToUnityPos(gDBSolarSystemSimpleInfo), this.$this.m_switchToSolarSystemFadeInOutConfig.m_targetCameraScaleValue, this.$this.m_currCenterPos, this.$this.m_currScale, onEnd, true);
            }
        }

        [CompilerGenerated]
        private sealed class <GetStarfieldFadeOutProcessExecutor>c__AnonStorey0
        {
            internal int targetSolarSystemId;
            internal StarMapForStarfieldUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Action<bool> onEnd)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetJumpPanel>c__AnonStorey3
        {
            internal GDBSolarSystemSimpleInfo currentSolarSystem;

            internal bool <>m__0(GuildTeleportTunnelEffectInfoClient info) => 
                (info.m_solarSystemId == this.currentSolarSystem.Id);

            internal bool <>m__1(GuildTeleportTunnelEffectInfoClient info) => 
                (info.m_solarSystemId == this.currentSolarSystem.Id);
        }
    }
}

