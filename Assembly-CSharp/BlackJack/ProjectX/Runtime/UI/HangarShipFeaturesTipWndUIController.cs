﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipFeaturesTipWndUIController : UIControllerBase
    {
        private string m_characteristicItemAssetName;
        private GameObject m_shipCharacteristicItemGoInstance;
        private ILBStaticPlayerShip m_lastShip;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shipFeatureInfoStateCtrl;
        [AutoBind("./InfoGroup/TitleTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LicenseTitleGroup;
        [AutoBind("./InfoGroup/TitleTextGroup/DriveText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipInfoTitleText;
        [AutoBind("./InfoGroup/TitleTextGroup/LvGroup/LVText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DiverLisenceLevelText;
        [AutoBind("./InfoGroup/LevelUp", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LicenseUpgradeProfit;
        [AutoBind("./InfoGroup/AddEffect", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform SpecialBuf;
        [AutoBind("./InfoGroup/TitleTextGroup/LvGroup/ToDriverLicenseButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ToDriverLicenseButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowOrHideShipFeatureTipWnd;
        private static DelegateBridge __Hotfix_UpdateShipFeatureInfo;
        private static DelegateBridge __Hotfix_CreateCharacteristicItemListWithInfo;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public UIProcess Close()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateCharacteristicItemListWithInfo(List<PropertyValueStringPair> infoList, Transform rootTrans)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHideShipFeatureTipWnd(ILBStaticPlayerShip staticShip, bool showToDriverLicenseButton, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, out bool showTip)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipFeatureInfo(ILBStaticPlayerShip staticShip, bool showToDriverLicenseButton)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

