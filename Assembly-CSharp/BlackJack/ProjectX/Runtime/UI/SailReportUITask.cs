﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SailReportUITask : UITaskBase
    {
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private SailReportController m_controller;
        public const string ParamKeyNotPlayOpenSound = "NotPlayOpenSound";
        private bool m_isPlayOpenSound;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnReturnFromAnotherTask;
        private static readonly string m_resPath = "Assets/GameProject/RuntimeAssets/UI/Prefabs/InSpace_ABS/NavigationalReport/SailReportUIPrefab.prefab";
        public const string TaskName = "SailReportUITask";
        private bool m_animPlayed;
        private ItemSimpleInfoUITask m_itemTask;
        private const string InfoKey = "Info";
        private const int IgnoreAllPipeLineUpdate = 1;
        [CompilerGenerated]
        private static Action<SailReportEventController> <>f__am$cache0;
        [CompilerGenerated]
        private static Action<SailReportEventController> <>f__am$cache1;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartSailReportTask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UnregisterEvents;
        private static DelegateBridge __Hotfix_OpenItemDetail;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_CloseSailReportPanel;
        private static DelegateBridge __Hotfix_ShareReport;
        private static DelegateBridge __Hotfix_OnReturnFromAnotherTask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_CheckIntent;
        private static DelegateBridge __Hotfix_add_EventOnReturnFromAnotherTask;
        private static DelegateBridge __Hotfix_remove_EventOnReturnFromAnotherTask;
        private static DelegateBridge __Hotfix_get_m_player;

        public event Action EventOnReturnFromAnotherTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SailReportUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public bool CheckIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseSailReportPanel(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromUIIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnReturnFromAnotherTask(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OpenItemDetail(FakeLBStoreItem item, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void ShareReport(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public static SailReportUITask StartSailReportTask(UIIntent returnIntent, ChatContentSailReport chatInfo = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext m_player
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

