﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class RepeatableUserGuideUITask : UITaskBase
    {
        protected RepeatableUserGuideUIController m_mainCtrl;
        private Vector2 m_operationAreaCenter;
        private Vector2 m_operationAreaSize;
        private Camera m_targetCamera;
        protected Action<bool> m_onEnd;
        protected bool m_isPassEvent;
        protected static string UIParam_OperationAreaCamera;
        protected static string UIParam_OperationAreaCenter;
        protected static string UIParam_OperationAreaSize;
        protected static string UIParam_ActionOnEnd;
        protected static string UIParam_IsPassEvent;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuideUITask_0;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuideUITask_1;
        private static DelegateBridge __Hotfix_GetOperationAreaInfo;
        private static DelegateBridge __Hotfix_OnOperateAreaClick;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_get_OperationAreaCenter;
        private static DelegateBridge __Hotfix_get_OperationAreaSize;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public RepeatableUserGuideUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected static void GetOperationAreaInfo(Camera canvasCamera, RectTransform trans, out Vector2 center, out Vector2 size)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent uiIntent)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnOperateAreaClick(bool clickInOperateArea)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartRepeatableUserGuideUITask(Camera canvasCamera, RectTransform trans, Action<bool> onEnd, bool isPassEvent = true)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartRepeatableUserGuideUITask(Vector2 screenPosCenter, Vector2 size, Action<bool> onEnd, bool isPassEvent = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public Vector2 OperationAreaCenter
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public Vector2 OperationAreaSize
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

