﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class BlackMarketResourcesShopUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnResourceShopItemFill;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnResourceShopItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ResourceShopItemCategory> EventOnShopCategoryButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnResourceShopItemIconClick;
        private readonly Dictionary<ResourceShopItemCategory, ResourceShopCategoryButtonUIController> m_categoryButtonDict;
        private List<GameObject> m_itemList;
        [AutoBind("./ResourcesBuyGroup/ItemSimpleInfoRightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoRightDummyGameObject;
        [AutoBind("./ResourcesBuyGroup/ItemSimpleInfoLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoLeftDummyGameObject;
        [AutoBind("./ResourcesBuyGroup/ResourcesBuyItemList/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect ScrollViewLoopVerticalScrollRect;
        [AutoBind("./ResourcesBuyGroup/ResourcesBuyItemList/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ScrollViewEasyObjectPool;
        [AutoBind("./ResourcesBuyGroup/ItemSimpleInfoLeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ItemSimpleInfoLeftDummyRectTransform;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MainPanelStateCtrl;
        [AutoBind("./ResourcesBuyGroup/ResourcesBuyButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform ButtonGroupRoot;
        private static DelegateBridge __Hotfix_InitShopCategoryList;
        private static DelegateBridge __Hotfix_SetSelectResourceShopCategory;
        private static DelegateBridge __Hotfix_SetShopCategoryButtonEnable;
        private static DelegateBridge __Hotfix_UpdateShopItemList;
        private static DelegateBridge __Hotfix_RefreshVisibleShopItems;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoDummyPosition;
        private static DelegateBridge __Hotfix_GetMainPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnNewItemCreateInPool;
        private static DelegateBridge __Hotfix_OnUIItemFill;
        private static DelegateBridge __Hotfix_OnUIItemClick;
        private static DelegateBridge __Hotfix_OnUIItemIconClick;
        private static DelegateBridge __Hotfix_OnShopCategoryButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnResourceShopItemFill;
        private static DelegateBridge __Hotfix_remove_EventOnResourceShopItemFill;
        private static DelegateBridge __Hotfix_add_EventOnResourceShopItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnResourceShopItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShopCategoryButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnShopCategoryButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnResourceShopItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnResourceShopItemIconClick;

        public event Action<UIControllerBase> EventOnResourceShopItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnResourceShopItemFill
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnResourceShopItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ResourceShopItemCategory> EventOnShopCategoryButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoDummyPosition(bool onLeft)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainPanelShowOrHideProcess(bool isShow, bool immedite = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public void InitShopCategoryList(List<ResourceShopItemCategory> categoryList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNewItemCreateInPool(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopCategoryButtonClick(ResourceShopItemCategory category)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUIItemClick(UIControllerBase logItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUIItemFill(UIControllerBase logItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUIItemIconClick(UIControllerBase logItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RefreshVisibleShopItems()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectResourceShopCategory(ResourceShopItemCategory resourceShopCategory)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShopCategoryButtonEnable(ResourceShopItemCategory resourceShopCategory, bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShopItemList(int count)
        {
        }
    }
}

