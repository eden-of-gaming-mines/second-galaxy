﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class MailGetAllMailReqNetTask : NetWorkTransactionTask
    {
        private readonly List<int> m_storedIndexList;
        private int m_result;
        private List<StoreItemUpdateInfo> m_itemInfos;
        private List<CurrencyUpdateInfo> m_currencyInfos;
        private bool m_getReward;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnMailGetAllMailAck;
        private static DelegateBridge __Hotfix_get_Result;
        private static DelegateBridge __Hotfix_set_Result;
        private static DelegateBridge __Hotfix_get_ItemInfos;
        private static DelegateBridge __Hotfix_get_CurrencyInfos;
        private static DelegateBridge __Hotfix_get_GetReward;

        [MethodImpl(0x8000)]
        public MailGetAllMailReqNetTask(List<int> storedIndexList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailGetAllMailAck(MailGetAllMailAck ack)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int Result
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public List<StoreItemUpdateInfo> ItemInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public List<CurrencyUpdateInfo> CurrencyInfos
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool GetReward
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

