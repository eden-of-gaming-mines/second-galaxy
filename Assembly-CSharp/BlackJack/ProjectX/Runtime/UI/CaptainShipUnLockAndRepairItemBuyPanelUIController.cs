﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainShipUnLockAndRepairItemBuyPanelUIController : UIControllerBase
    {
        private bool m_isShow;
        protected List<ItemSlotInfo> m_itemSlotInfoList;
        private const string ItemAssetName = "CommonItemUIPrefab";
        protected const string PanelStateOpen = "PanelOpen";
        protected const string PanelStateClose = "PanelClose";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buyPanelStateCtrl;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buyPanelConfirmButton;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buyPanelCancelButton;
        [AutoBind("./DetailPanel/ItemGroup/Slot1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemSlot1Root;
        [AutoBind("./DetailPanel/ItemGroup/Slot1/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_slot1ItemDummy;
        [AutoBind("./DetailPanel/ItemGroup/Slot1/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_slot1ItemNameText;
        [AutoBind("./DetailPanel/ItemGroup/Slot2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemSlot2Root;
        [AutoBind("./DetailPanel/ItemGroup/Slot2/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_slot2ItemDummy;
        [AutoBind("./DetailPanel/ItemGroup/Slot2/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_slot2ItemNameText;
        [AutoBind("./DetailPanel/ItemGroup/Slot3", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemSlot3Root;
        [AutoBind("./DetailPanel/ItemGroup/Slot3/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_slot3ItemDummy;
        [AutoBind("./DetailPanel/ItemGroup/Slot3/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_slot3ItemNameText;
        [AutoBind("./DetailPanel/ConfirmButton/MoneyTextGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_buyPriceText;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmButtonStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_IsShow;
        private static DelegateBridge __Hotfix_CreateBuyWindowProcess;
        private static DelegateBridge __Hotfix_UpdateShipConfComponentBuyPanel;

        [MethodImpl(0x8000)]
        public UIProcess CreateBuyWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipConfComponentBuyPanel(List<ShipCompListConfInfo> compList, ulong buyPrice, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        protected class ItemSlotInfo
        {
            public GameObject m_slotRoot;
            public Transform m_itemDummy;
            public CommonItemIconUIController m_itemCtrl;
            public Text m_itemName;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

