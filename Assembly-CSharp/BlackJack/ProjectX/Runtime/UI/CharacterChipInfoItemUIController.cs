﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterChipInfoItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnExchangeChipButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./ChipInfo/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_chipItemIconTrans;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmButtonStateCtrl;
        [AutoBind("./ChipInfo/ChipNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_chipNameText;
        [AutoBind("./ChipInfo/ChipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_chipTypeText;
        [AutoBind("./ChipInfo/Tech/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_chipRankImage;
        [AutoBind("./ChipInfo/Quality/SubRankBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_chipSubRankBgStateCtrl;
        [AutoBind("./ChipInfo/Quality/SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_chipSubRankImage;
        [AutoBind("./ChipInfo/PropertyInfo/BaseProperty1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_basePropertyInfoObj1;
        [AutoBind("./ChipInfo/PropertyInfo/BaseProperty2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_basePropertyInfoObj2;
        [AutoBind("./ChipInfo/PropertyInfo/ExtraProperty", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_extraPropertyInfoObj;
        [AutoBind("./ChipInfo/PropertyInfo/ExtraPropertyTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_extraPropertyTitle;
        [AutoBind("./ChipInfo/PropertyInfo/SuitInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_suitEffectCtrl;
        [AutoBind("./ChipInfo/PropertyInfo/SuitInfoRoot/SuitProperty", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_suitPropertyInfoCtrl;
        [AutoBind("./ChipInfo/PropertyInfo/SuitInfoRoot/SuitNameTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_suitNameStateCtrl;
        [AutoBind("./ChipInfo/PropertyInfo/SuitInfoRoot/SuitNameTextGroup/SuitNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_suitNameText;
        [AutoBind("./ChipInfo/PropertyInfo/SuitInfoRoot/SuitNameTextGroup/SuitChipNumInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_chipNumText;
        [AutoBind("./AlreadyAssembleBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_alreadyEquippedObj;
        private List<PropertyModifyInfoItemUIController> m_basePropertyModifyInfoList;
        private PropertyModifyInfoItemUIController m_extraPropertyModifyInfo;
        private SuitPropertyModifyInfoItemUIController m_suitPropertyModifyInfo;
        private CommonItemIconUIController m_chipItemIconCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetConfirmButtonState;
        private static DelegateBridge __Hotfix_SetIsEquippedInfo;
        private static DelegateBridge __Hotfix_SetChipName;
        private static DelegateBridge __Hotfix_SetChipType;
        private static DelegateBridge __Hotfix_SetChipRank;
        private static DelegateBridge __Hotfix_SetChipSubRank;
        private static DelegateBridge __Hotfix_SetChipItemIcon;
        private static DelegateBridge __Hotfix_SetBasePropertyModifyInfo;
        private static DelegateBridge __Hotfix_SetExtraPropertyModifyInfo;
        private static DelegateBridge __Hotfix_HideExtraPropertyModifyInfo;
        private static DelegateBridge __Hotfix_SetSuitEffectExist;
        private static DelegateBridge __Hotfix_SetSuitNameInfo;
        private static DelegateBridge __Hotfix_SetSuitEquippedCount;
        private static DelegateBridge __Hotfix_SetSuitPropertyModifyInfo;
        private static DelegateBridge __Hotfix_CreatPanelAnimtionProcess;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnExchangeChipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnExchangeChipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnExchangeChipButtonClick;

        public event Action EventOnExchangeChipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatPanelAnimtionProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public void HideExtraPropertyModifyInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnExchangeChipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBasePropertyModifyInfo(List<string> propertyNameList, List<string> propertyModifyValueStrList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipItemIcon(object itemConfigData, bool isBind, bool isFreezing, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipRank(Sprite sprite)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipSubRank(Sprite sprite, string state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChipType(string type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetConfirmButtonState(bool isExchange)
        {
        }

        [MethodImpl(0x8000)]
        public void SetExtraPropertyModifyInfo(string propertyName, string propertyModifyValueStr)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsEquippedInfo(bool isEquipped)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuitEffectExist(bool isExistSuitEffect)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuitEquippedCount(int currEquippedCount, int maxEquippedCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuitNameInfo(string suitName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSuitPropertyModifyInfo(string propertyName, string propertyModifyValueStr, bool isActive)
        {
        }
    }
}

