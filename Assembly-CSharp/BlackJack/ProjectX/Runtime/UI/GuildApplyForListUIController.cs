﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildApplyForListUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnRatifyBtnClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCancelBtnClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnTimeSortClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnScoreSortClick;
        private List<GuildJoinApplyViewInfo> m_applyList;
        private Dictionary<string, UnityEngine.Object> m_resData;
        [AutoBind("./PeopleGroup/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRoot;
        [AutoBind("./PeopleGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_loopScrollView;
        [AutoBind("./PeopleGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyPool;
        [AutoBind("./PeopleGroup/EmptyImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_empty;
        [AutoBind("./TitleGroup/Time", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_timeSortBtn;
        [AutoBind("./TitleGroup/Scrore", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_scoreSortBtn;
        [AutoBind("./TitleGroup/Time", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_timeState;
        [AutoBind("./TitleGroup/Scrore", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_scoreState;
        private string m_poolItemName;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnMemberItemFill;
        private static DelegateBridge __Hotfix_OnRatifyBtnClick;
        private static DelegateBridge __Hotfix_OnCancelBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnRatifyBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnRatifyBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelBtnClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelBtnClick;
        private static DelegateBridge __Hotfix_add_EventOnTimeSortClick;
        private static DelegateBridge __Hotfix_remove_EventOnTimeSortClick;
        private static DelegateBridge __Hotfix_add_EventOnScoreSortClick;
        private static DelegateBridge __Hotfix_remove_EventOnScoreSortClick;

        public event Action<int> EventOnCancelBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnRatifyBtnClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnScoreSortClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTimeSortClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelBtnClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRatifyBtnClick(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(List<GuildJoinApplyViewInfo> applyList, bool timeUpSort, bool scoreUpSort, Dictionary<string, UnityEngine.Object> resData)
        {
        }
    }
}

