﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class NpcListDialogUITask : NpcTalkerDialogBase
    {
        private SpaceStationRoomType m_roomType;
        private int m_talkToNpcId;
        private List<LBNpcTalkerBase> m_npcTalkerList;
        private NpcListDialogUIController m_npcListCtrl;
        public const string NpcListDialogCustomParamKey_BackgroundManager = "BackgroundManager";
        public static string NpcListDialogCustomParamKey_RoomType;
        public static string NpcListDialogCustomParamKey_MainUIIntent;
        public static string NpcListDialogCustomParamKey_TalkToNpcId;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "NpcListDialogUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowNpcListDialog;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_InitParamFromIntent;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateNpcListUI;
        private static DelegateBridge __Hotfix_NpcTalkerComparer;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnNpcItemClick;
        private static DelegateBridge __Hotfix_SendInStationNextDailogNetworkReq;
        private static DelegateBridge __Hotfix_SendInSpaceNextDialogNetworkReq;
        private static DelegateBridge __Hotfix_GetCurrentNpcTalkerDialogInfoFromServer;
        private static DelegateBridge __Hotfix_ReturnToMainUI;
        private static DelegateBridge __Hotfix_FindTalkToNpc;
        private static DelegateBridge __Hotfix_FindQuestCompleteNpc;
        private static DelegateBridge __Hotfix_FindQuestAcceptNpc;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_RepeateGuideStepStart;
        private static DelegateBridge __Hotfix_RepeatGuideStepOne;

        [MethodImpl(0x8000)]
        public NpcListDialogUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private LBNpcTalkerBase FindQuestAcceptNpc(out int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        private LBNpcTalkerBase FindQuestCompleteNpc(out int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        private LBNpcTalkerBase FindTalkToNpc(out int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void GetCurrentNpcTalkerDialogInfoFromServer(Action<bool> onNetRequestEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParamFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private int NpcTalkerComparer(LBNpcTalkerBase npcA, LBNpcTalkerBase npcB)
        {
        }

        [MethodImpl(0x8000)]
        public void OnNpcItemClick(int npcId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void RepeateGuideStepStart()
        {
        }

        [MethodImpl(0x8000)]
        public void RepeatGuideStepOne()
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnToMainUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void SendInSpaceNextDialogNetworkReq(uint sceneInstanceId, int nextDlgId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void SendInStationNextDailogNetworkReq(int nextDlgId)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowNpcListDialog(SpaceStationRoomType roomType, UIIntent mainIntent, int fixedTalkerId, Action<bool> onEnd, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private CommonUIStateEffectProcess UpdateNpcListUI(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetCurrentNpcTalkerDialogInfoFromServer>c__AnonStorey3
        {
            internal Action<bool> onNetRequestEnd;
            internal NpcListDialogUITask $this;

            internal void <>m__0(Task task)
            {
                InStationNpcDialogStartReqNetTask task2 = task as InStationNpcDialogStartReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.StartNpcDialogReqResult == 0)
                    {
                        this.$this.m_currentDialogInfo = task2.DialogInfo;
                    }
                    else
                    {
                        Debug.LogError($"InStationNpcDialogStartReqNetTask return error !! errorCode:{task2.StartNpcDialogReqResult}");
                    }
                    if (this.onNetRequestEnd != null)
                    {
                        this.onNetRequestEnd(task2.StartNpcDialogReqResult == 0);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RepeatGuideStepOne>c__AnonStorey4
        {
            internal UIIntentCustom intent;
            internal NpcListDialogUITask $this;

            internal void <>m__0(bool isInRect)
            {
                if (isInRect)
                {
                    this.$this.PlayUIProcess(this.$this.m_npcTalkerUICtrl.GetPanelShorOrHideUIProcess(false, false), true, delegate (UIProcess process, bool result) {
                        this.$this.Pause();
                        UIIntentCustom intent = new UIIntentCustom("NpcShopMainUITask", "NpcShopMainUITaskMode_Buy");
                        intent.SetParam("ItemType", this.intent.GetStructParam<int>("ItemType"));
                        intent.SetParam("QuickJump", this.intent.GetStructParam<int>("QuickJump"));
                        UIManager.Instance.StartUITask(intent, true, false, null, null);
                    }, false);
                }
                else
                {
                    this.$this.Pause();
                    if (this.$this.m_mainUIIntent != null)
                    {
                        UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                    }
                }
            }

            internal void <>m__1(UIProcess process, bool result)
            {
                this.$this.Pause();
                UIIntentCustom intent = new UIIntentCustom("NpcShopMainUITask", "NpcShopMainUITaskMode_Buy");
                intent.SetParam("ItemType", this.intent.GetStructParam<int>("ItemType"));
                intent.SetParam("QuickJump", this.intent.GetStructParam<int>("QuickJump"));
                UIManager.Instance.StartUITask(intent, true, false, null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <SendInSpaceNextDialogNetworkReq>c__AnonStorey2
        {
            internal InSpaceNpcDialogNextReqNetTask returnTask;
            internal NpcListDialogUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                this.$this.Pause();
                NpcDNId npcDNId = new NpcDNId(0, 0, this.returnTask.NpcId, false);
                NpcTalkerDialogUITask.StartNpcTalkerDialog(this.$this.m_mainUIIntent, npcDNId, this.returnTask.DialogInfo, null, true, true);
            }

            internal void <>m__1(UIProcess process, bool result)
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
            }
        }

        [CompilerGenerated]
        private sealed class <SendInStationNextDailogNetworkReq>c__AnonStorey1
        {
            internal InStationNpcDialogNextReqNetTask returnTask;
            internal NpcListDialogUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                NpcDNId id = new NpcDNId(playerContext.CurrSolarSystemId, playerContext.GetLBCharacter().GetSpaceStationId(), this.returnTask.NpcId, false);
                this.$this.Pause();
                bool playGreetingAudio = !NpcDNId.Equals(this.$this.m_currentNpcDNId, id);
                NpcTalkerDialogUITask.StartNpcTalkerDialog(this.$this.m_mainUIIntent, id, this.returnTask.DialogInfo, null, true, playGreetingAudio);
            }

            internal void <>m__1(UIProcess process, bool result)
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowNpcListDialog>c__AnonStorey0
        {
            internal UIIntent mainIntent;

            internal void <>m__0(bool rst)
            {
                if (!rst)
                {
                    Debug.LogWarning("Start NpcListDialogUITask failed!");
                    if (this.mainIntent != null)
                    {
                        UIManager.Instance.ReturnUITask(this.mainIntent, null);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsNpcSelectionChanged
        }
    }
}

