﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class TechUpgradeInfoUITask : UITaskBase
    {
        public const string ParamKey_SelectedTechId = "SelectedTechId";
        public const string ParamKey_AssistCaptainInsId = "AssistCaptainInsId";
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private const string MonthCardPriviledgeLockOn = "LockOn";
        private const string MonthCardPriviledgeLockOff = "LockOff";
        private IUIBackgroundManager m_backgroundManager;
        private bool m_isNeedAutoChooseCaptain;
        private int m_currentTechId;
        private int m_currentTechLevel;
        private ulong m_assistCaptainInsId;
        private bool m_monthCardPriviledgeObtained;
        private TechUpgradeInfoUIController m_mainCtrl;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "TechUpgradeInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTechUpgradeInfoUITask;
        private static DelegateBridge __Hotfix_ClickUpgradeButton;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnGoToPreTechButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeImmediatelyButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeButtonClickImp;
        private static DelegateBridge __Hotfix_OnAddCaptainButtonClick;
        private static DelegateBridge __Hotfix_OnDeleteChooseCaptainButtonClick;
        private static DelegateBridge __Hotfix_OnGetMoreItemButtonClick;
        private static DelegateBridge __Hotfix_OnItemButtonClick;
        private static DelegateBridge __Hotfix_OnMonthCardPriviledgeClick;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoUIPanel;
        private static DelegateBridge __Hotfix_CloseItemSimepleInfoUIPanel;
        private static DelegateBridge __Hotfix_OnCaptainIconClick;
        private static DelegateBridge __Hotfix_StartChooseCaptainUITask;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public TechUpgradeInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickUpgradeButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseItemSimepleInfoUIPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddCaptainButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainIconClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDeleteChooseCaptainButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetMoreItemButtonClick(TechCostItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoToPreTechButtonClick(int preTechId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemButtonClick(TechCostItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMonthCardPriviledgeClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUpgradeImmediatelyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowItemSimpleInfoUIPanel(FakeLBStoreItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void StartChooseCaptainUITask()
        {
        }

        [MethodImpl(0x8000)]
        public static TechUpgradeInfoUITask StartTechUpgradeInfoUITask(UIIntent mainUIIntent, int techId, Action<bool> onEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnUpgradeButtonClickImp>c__AnonStorey1
        {
            internal List<TechUpgradeInfo> techUpgradeList;
            internal TechUpgradeInfoUITask.<OnUpgradeButtonClickImp>c__AnonStorey2 <>f__ref$2;

            internal void <>m__0(UIProcess currProcess, bool isCompleted)
            {
                UIIntentReturnable currIntent = this.<>f__ref$2.$this.m_currIntent as UIIntentReturnable;
                if (((currIntent != null) && (currIntent.PrevTaskIntent != null)) && (this.techUpgradeList.Count > 0))
                {
                    this.<>f__ref$2.$this.Pause();
                    IUIBackgroundManager backgroundManager = this.<>f__ref$2.$this.m_backgroundManager;
                    TechUpgradeManageUITask.StartTechUpgradeManageUITask(this.<>f__ref$2.$this.m_currIntent, this.techUpgradeList[0].m_id, null, backgroundManager, "SimpleInfoMode");
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnUpgradeButtonClickImp>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal TechUpgradeInfoUITask $this;

            internal void <>m__0(Task task)
            {
                TechUpgradeStartReqNetTask task2 = task as TechUpgradeStartReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.UpgradeStartResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.UpgradeStartResult, true, false);
                        if (this.onEnd != null)
                        {
                            this.onEnd(false);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_TechUpgrade_StartUpgrade, new object[0]), false);
                        UIPlayingEffectInfo info = new UIPlayingEffectInfo(this.$this.m_mainCtrl.m_commonUIStateCtrl, "Close", false, true);
                        UIPlayingEffectInfo[] effectList = new UIPlayingEffectInfo[] { info };
                        CommonUIStateEffectProcess process = UIProcessFactory.CreateCommonUIStateEffectProcess(UIProcess.ProcessExecMode.Parallel, effectList);
                        this.$this.PlayUIProcess(process, true, delegate (UIProcess currProcess, bool isCompleted) {
                            this.$this.Pause();
                            UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                            if (currIntent != null)
                            {
                                if (currIntent.PrevTaskIntent.TargetTaskName == "TechUpgradeManageUITask")
                                {
                                    currIntent.PrevTaskIntent.TargetMode = "NormalMode";
                                }
                                UIManager.Instance.ReturnUITask(currIntent.PrevTaskIntent, this.onEnd);
                            }
                        }, false);
                    }
                }
            }

            internal void <>m__1(UIProcess currProcess, bool isCompleted)
            {
                this.$this.Pause();
                UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                if (currIntent != null)
                {
                    if (currIntent.PrevTaskIntent.TargetTaskName == "TechUpgradeManageUITask")
                    {
                        currIntent.PrevTaskIntent.TargetMode = "NormalMode";
                    }
                    UIManager.Instance.ReturnUITask(currIntent.PrevTaskIntent, this.onEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnUpgradeImmediatelyButtonClick>c__AnonStorey0
        {
            internal int destLevel;
            internal TechUpgradeInfoUITask $this;

            internal void <>m__0(Task task)
            {
                TechUpgradeByRealMoneyReqNetTask task2 = task as TechUpgradeByRealMoneyReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.TechUpgradeByRealMoneyResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.TechUpgradeByRealMoneyResult, true, false);
                    }
                    else
                    {
                        ConfigDataTechInfo configDataTechInfo = ConfigDataHelper.ConfigDataLoader.GetConfigDataTechInfo(this.$this.m_currentTechId);
                        if (this.destLevel < configDataTechInfo.LevelInfoIdList.Count)
                        {
                            this.$this.StartUpdatePipeLine(null, false, false, true, null);
                        }
                        else
                        {
                            UIPlayingEffectInfo info2 = new UIPlayingEffectInfo(this.$this.m_mainCtrl.m_commonUIStateCtrl, "Close", false, true);
                            UIPlayingEffectInfo[] effectList = new UIPlayingEffectInfo[] { info2 };
                            CommonUIStateEffectProcess process = UIProcessFactory.CreateCommonUIStateEffectProcess(UIProcess.ProcessExecMode.Parallel, effectList);
                            this.$this.PlayUIProcess(process, true, delegate (UIProcess currProcess, bool isCompleted) {
                                this.$this.Pause();
                                UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                                if (currIntent != null)
                                {
                                    UIManager.Instance.ReturnUITask(currIntent.PrevTaskIntent, null);
                                }
                            }, false);
                        }
                    }
                }
            }

            internal void <>m__1(UIProcess currProcess, bool isCompleted)
            {
                this.$this.Pause();
                UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                if (currIntent != null)
                {
                    UIManager.Instance.ReturnUITask(currIntent.PrevTaskIntent, null);
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            TechUpgradeAssistCaptainChanged,
            TechUpgradeSpeedUp,
            NotDefaultChooseFirstCaptain
        }
    }
}

