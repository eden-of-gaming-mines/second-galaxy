﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SignalStrikeUIController : UIControllerBase
    {
        public LBSignalDelegateBase m_delSignal;
        public List<LBStaticHiredCaptain> m_captainInfoList;
        public Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        public List<ulong> m_selectedCaptainIdList;
        private const string CaptainListUIItemPoolName = "CaptainListItem";
        private const string SignalTimeMode_Collect = "Collect";
        private const string SignalTimeMode_Transport = "Transport";
        private const string SignalTimeMode_Fight = "Fight";
        private const string FinishTimeMode_Normal = "Normal";
        private const string FinishTimeMode_OverTime = "OverTime";
        private const string FinishTimeMode_UnKnown = "UnKnown";
        private const string FinishTimeMode_NoCollectionAbility = "NoCollectionAbility";
        private const string AllowInImageUIState_AllowIn = "AllowIn";
        private const string AllowInImageUIState_NotAllowIn = "NotAllowIn";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnDropdownChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCaptainItemSelectionButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAllowInShipButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./OnlyShowChosenToggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle OnlyShowChosenToggle;
        [AutoBind("./DropdownButton", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown CaptainSortTypeDropdown;
        [AutoBind("./QuickFormButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx QuickFormButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./CaptainScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect CaptainListScrollRect;
        [AutoBind("./CaptainScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool CaptainListUIItemPool;
        [AutoBind("./SolarSystemText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SignalSolarSystemText;
        [AutoBind("./QuestNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SignalNameText;
        [AutoBind("./AllowIn/AllowInShipType/Frigate", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeFrigate;
        [AutoBind("./AllowIn/AllowInShipType/Destroyer", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeDestroyer;
        [AutoBind("./AllowIn/AllowInShipType/Cruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeCruiser;
        [AutoBind("./AllowIn/AllowInShipType/BattleCruiser", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleCruiser;
        [AutoBind("./AllowIn/AllowInShipType/BattleShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeBattleShip;
        [AutoBind("./AllowIn/AllowInShipType/IndustryShip", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllowInShipTypeIndustryShip;
        [AutoBind("./AllowIn/AllowInShipGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform AllowInShipGroupDummy;
        [AutoBind("./AllowIn/AllowInShipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AllowInShipButton;
        [AutoBind("./ChosenCaptainCount/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ChosenCaptainCountText;
        [AutoBind("./ChosenCaptainCount/MaxText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CaptainMaxCountText;
        [AutoBind("./SignalTimeDetailGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SignalTimeStateCtrl;
        [AutoBind("./SignalTimeDetailGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Button SignalTimeDetailButton;
        [AutoBind("./SignalTimeDetailGroup/FinishTime", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController FinishTimeStateCtrl;
        [AutoBind("./SignalTimeDetailGroup/FinishTime/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SignalFinishTimeText;
        [AutoBind("./SignalTimeDetailGroup/FinishTime/OverTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text OverTimeText;
        [AutoBind("./SignalTimeDetailGroup/CollectInfo/NumberTextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MineralNumberTextStateCtrl;
        [AutoBind("./SignalTimeDetailGroup/CollectInfo/NumberTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CollectStorageNumberText;
        [AutoBind("./SignalTimeDetailGroup/CollectInfo/NumberTextGroup/MaxText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MineralNumberText;
        [AutoBind("./SignalTimeDetailGroup/CollectInfo/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image CollectProgressBar;
        [AutoBind("./SignalTimeDetailGroup/CollectInfo/ProgressText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CollectProgressText;
        [AutoBind("./SignalTimeDetailGroup/TransportInfo/NumberTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportStorageNumberText;
        [AutoBind("./SignalTimeDetailGroup/TransportInfo/NumberTextGroup/MaxText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportMaxNumberText;
        [AutoBind("./SignalTimeDetailGroup/TransportInfo/TripTimesText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TransportTripText;
        [AutoBind("./TipsWindowPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TipsWindowPanel;
        [AutoBind("./TipsWindowPanel/DetailPanel/TipsInfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TipsText;
        [AutoBind("./TipsWindowPanel/DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TipsCancelButton;
        [AutoBind("./TipsWindowPanel/DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TipsConfirmButton;
        [AutoBind("./TipsWindowPanel/BGImages/WarningText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TipsUpgradeCaptain;
        [AutoBind("./HitTipWindows", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController HitTipWindowsStateCtrl;
        [AutoBind("./HitTipWindows", AutoBindAttribute.InitState.NotInit, false)]
        public Button HitTipWindowsBGButton;
        [AutoBind("./HitTipWindows/FrameImage/BGImage/TextGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_hitStateCtrl;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateSignalStrikeInfo;
        private static DelegateBridge __Hotfix_UpdateStrikeShipSelectionState;
        private static DelegateBridge __Hotfix_SetSortType;
        private static DelegateBridge __Hotfix_CreateMainUIProcess;
        private static DelegateBridge __Hotfix_ShowTipsWindow;
        private static DelegateBridge __Hotfix_CreateHitTipUIProcess;
        private static DelegateBridge __Hotfix_RefreshCaptainListInfo;
        private static DelegateBridge __Hotfix_UpdateOtherSignalStrikeInfo;
        private static DelegateBridge __Hotfix_UpdateDelegateMissionFinishTimeInfo;
        private static DelegateBridge __Hotfix_SetDelegateFightFinishTimeInfo;
        private static DelegateBridge __Hotfix_SetDelegateCollectFinishTimeInfo;
        private static DelegateBridge __Hotfix_SetDelegateTransportFinishTimeInfo;
        private static DelegateBridge __Hotfix_SetFinishTimeValue;
        private static DelegateBridge __Hotfix_SetHintTipState;
        private static DelegateBridge __Hotfix_OnCaptainItemSelectionButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainUIItemClick;
        private static DelegateBridge __Hotfix_OnCaptainUIItemFill;
        private static DelegateBridge __Hotfix_OnDropdownValueChanged;
        private static DelegateBridge __Hotfix_OnAllowInShipGroupButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnDropdownChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDropdownChanged;
        private static DelegateBridge __Hotfix_add_EventOnCaptainItemSelectionButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainItemSelectionButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnAllowInShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnAllowInShipButtonClick;

        public event Action EventOnAllowInShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCaptainItemSelectionButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnDropdownChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateHitTipUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainUIProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipGroupButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainItemSelectionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainUIItemClick(UIControllerBase captainItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainUIItemFill(UIControllerBase captainItemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropdownValueChanged(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void RefreshCaptainListInfo(LBSignalDelegateBase lbSignal, List<LBStaticHiredCaptain> captainList, List<ulong> selectedCaptainIdList, Dictionary<string, UnityEngine.Object> resDict, int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDelegateCollectFinishTimeInfo(LBSignalDelegateBase delSignal, List<LBStaticHiredCaptain> selectedCaptainList)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDelegateFightFinishTimeInfo(LBSignalDelegateBase delSignal, List<LBStaticHiredCaptain> selectedCaptainList)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDelegateTransportFinishTimeInfo(LBSignalDelegateBase delSignal, List<LBStaticHiredCaptain> selectedCaptainList)
        {
        }

        [MethodImpl(0x8000)]
        private void SetFinishTimeValue(double estimateTime)
        {
        }

        [MethodImpl(0x8000)]
        private void SetHintTipState(SignalType type)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSortType(int type)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowTipsWindow(bool isShow, string tips = null, bool showUpgradeCaptain = false)
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDelegateMissionFinishTimeInfo(LBSignalDelegateBase delSignal, List<ulong> selectedCaptainIdList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateOtherSignalStrikeInfo(LBSignalDelegateBase delSignal, List<ulong> selectedCaptainIdList, int maxShipCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSignalStrikeInfo(LBSignalDelegateBase lbSignal, List<LBStaticHiredCaptain> captainList, List<ulong> selectedCaptainIdList, int maxShipCount, Dictionary<string, UnityEngine.Object> resDict, int startIndex = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateStrikeShipSelectionState(LBSignalDelegateBase lbSignal, List<ulong> selectedCaptainIdList, int maxShipCount)
        {
        }
    }
}

