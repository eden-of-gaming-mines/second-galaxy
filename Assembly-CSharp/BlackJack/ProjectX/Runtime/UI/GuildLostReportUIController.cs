﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildLostReportUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        public CommonItemIconUIController m_commonItemCtrl;
        private const string StateSolarSystem = "SolarSystem";
        private const string StateBuilding = "Building";
        private const string StateFlagShip = "FlagShip";
        private List<CostInfo> m_costInfoWithoutCurrency;
        [AutoBind("./ContentGroup/TextContentGroup/ItemGroup/LossItemGroup/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemGroup;
        [AutoBind("./ContentGroup/TextContentGroup/ItemGroup/LossItemGroup/Viewport/Content/ResourceItem", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemPrefab;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./TargetIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image LossTargetIconImage;
        [AutoBind("./BGImages/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelStateCtrl;
        [AutoBind("./ContentGroup/TextContentGroup/LossTarget/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LossTargetNameText;
        [AutoBind("./ContentGroup/TextContentGroup/LossTime/LossTimeTitleText/LossTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TimeText;
        [AutoBind("./ContentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ContentStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateLostItemListInfo_2;
        private static DelegateBridge __Hotfix_UpdateLostItemListInfo_0;
        private static DelegateBridge __Hotfix_UpdateLostItemListInfo_1;
        private static DelegateBridge __Hotfix_UpdateLossTime;
        private static DelegateBridge __Hotfix_UpdateLostItemUI;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLossTime(DateTime lostTime)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLostItemListInfo(SovereignLostReport reportInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLostItemListInfo(GuildLostReportUITask.FlagShipLostReport reportInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLostItemListInfo(BuildingLostReport reportInfo, int buildingLevel, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateLostItemUI(List<CostInfo> costInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

