﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CaptainWingManSetConfirmItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ILBStoreItemClient> EventOnShipItemClick;
        private CommonCaptainIconUIController m_captainIconCtrl;
        private CommonItemIconUIController m_shipIconCtrl;
        private ILBStoreItemClient m_ship;
        [AutoBind("./CaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CaptainIconDummy;
        [AutoBind("./ShipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipIconDummy;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingManNameText;
        [AutoBind("./ScorceNum", AutoBindAttribute.InitState.NotInit, false)]
        public Text ScoreText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetCaptainWingManItemInfo;
        private static DelegateBridge __Hotfix_OnShipItemClick;
        private static DelegateBridge __Hotfix_add_EventOnShipItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnShipItemClick;

        public event Action<ILBStoreItemClient> EventOnShipItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCaptainWingManItemInfo(LBStaticHiredCaptain captain, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

