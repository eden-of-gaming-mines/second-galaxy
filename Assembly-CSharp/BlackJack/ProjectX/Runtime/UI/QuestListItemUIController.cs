﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestListItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <QuestInstanceId>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private BlackJack.ProjectX.Runtime.UI.UnAcceptedQuestUIInfo <UnAcceptedQuestUIInfo>k__BackingField;
        private bool m_isSelected;
        private bool m_isVancant;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<QuestListItemUIController> EventOnItemToggleChanged;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx ItemToggle;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIStateCtrl;
        [AutoBind("./QuestNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestName;
        [AutoBind("./QuestNameText/LimitedGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TimeImage;
        [AutoBind("./QuestNameText/ChallengeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ChallengeTipImage;
        [AutoBind("./QuestLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestLevel;
        [AutoBind("./QuestLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_questLevelStateCtrl;
        [AutoBind("./DefeatedBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FailTip;
        [AutoBind("./LoseEfficacyBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject InvalidTip;
        [AutoBind("./JumpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text JumpText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetAcceptQuestListItemInfo;
        private static DelegateBridge __Hotfix_SetUnAcceptQuestListItemInfo;
        private static DelegateBridge __Hotfix_SetSelectedState;
        private static DelegateBridge __Hotfix_SetVacantState;
        private static DelegateBridge __Hotfix_SetItemTip;
        private static DelegateBridge __Hotfix_OnItemToggleValueChanged;
        private static DelegateBridge __Hotfix_set_QuestInstanceId;
        private static DelegateBridge __Hotfix_get_QuestInstanceId;
        private static DelegateBridge __Hotfix_get_UnAcceptedQuestUIInfo;
        private static DelegateBridge __Hotfix_set_UnAcceptedQuestUIInfo;
        private static DelegateBridge __Hotfix_get_IsSelected;
        private static DelegateBridge __Hotfix_get_IsVancant;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnItemToggleChanged;
        private static DelegateBridge __Hotfix_remove_EventOnItemToggleChanged;

        public event Action<QuestListItemUIController> EventOnItemToggleChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemToggleValueChanged(UIControllerBase ctrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAcceptQuestListItemInfo(int questInsId, int questLevel, string questName, string questJumpCount, bool isSelected, bool isTimeLimited, bool isFail, bool isChallenge, bool canRetry)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetItemTip(bool isFail, bool canRetry)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedState(bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUnAcceptQuestListItemInfo(BlackJack.ProjectX.Runtime.UI.UnAcceptedQuestUIInfo unAcceptedQuestUIInfo, int questLevel, string questName, string questJumpCount, bool isSelected, bool isTimeLimited, bool isFail, bool isChallenge, bool canRetry)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVacantState(bool isVacant)
        {
        }

        public int QuestInstanceId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public BlackJack.ProjectX.Runtime.UI.UnAcceptedQuestUIInfo UnAcceptedQuestUIInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsSelected
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsVancant
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

