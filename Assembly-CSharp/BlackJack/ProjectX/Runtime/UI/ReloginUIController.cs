﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ReloginUIController : UIControllerBase
    {
        private Vector3 m_rtn2LoginBtnOrignalLocation;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateController;
        [AutoBind("./RetryLoginButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button RetryLoginButton;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CancelButton;
        [AutoBind("./ReturnToLoginButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ReturnToLoginButton;
        [AutoBind("./CenterButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CenterButtonDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_ShowPromptToLoginAgain;
        private static DelegateBridge __Hotfix_ShowLoadingUI;
        private static DelegateBridge __Hotfix_ShowWaitForReloginConfirmUI;
        private static DelegateBridge __Hotfix_CloseUI;
        private static DelegateBridge __Hotfix_ShowWaitForReloginProcessingUI;
        private static DelegateBridge __Hotfix_ShowWaitReturnToLoginConfirmUI;

        [MethodImpl(0x8000)]
        public void CloseUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowLoadingUI()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowPromptToLoginAgain()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWaitForReloginConfirmUI()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWaitForReloginProcessingUI()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWaitReturnToLoginConfirmUI()
        {
        }
    }
}

