﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class GuildFleetDetailsUITask : GuildUITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        private const string ParamKeyFleetId = "FleetId";
        private string m_noNeedPositionStr;
        private bool m_isCloseOperationPanelByBg;
        private ulong m_fleetId;
        private int m_nearByMemberCount;
        private int m_onLineMemberCount;
        private bool m_isFleetManager;
        private GuildFleetMemberDetailInfo m_selfDetailInfo;
        private GuildFleetMemberDetailInfo m_currSelectMemberInfo;
        private readonly List<GuildFleetMemberDetailInfo> m_fleetMemberDetailInfoList;
        private GuildFleetDetailsUIController.GuildFleetDetailSortType m_currSortType;
        private GuildFleetMemberDetailInfo m_commanderMemberInfo;
        private GuildFleetMemberDetailInfo m_navigatorMemberInfo;
        private GuildFleetMemberDetailInfo m_fireControllerMemberInfo;
        private GuildFleetDetailsUIController m_guildFleetDetailsUIController;
        private GuildFleetMemberOperationUIController m_guildFleetMemberOperationUIController;
        private GuildFleetSettingJobUIController m_guildFleetSettingJobUIController;
        private GuildFleetConfirmPanelUIController m_guildFleetConfirmPanelUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildFleetDetailsUITask";
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartFleetDetailUITask;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_SendGuildFleetDetailInfoReq;
        private static DelegateBridge __Hotfix_SendGuildFleetMemberRetireReq;
        private static DelegateBridge __Hotfix_SendGuildFleetSetCommanderReq;
        private static DelegateBridge __Hotfix_SendGuildFleetSetInternalPositionsReq;
        private static DelegateBridge __Hotfix_SendGuildFleetTransferPositionReq;
        private static DelegateBridge __Hotfix_SendGuildFleetSetFormationTypeReq;
        private static DelegateBridge __Hotfix_SendGuildFleetMemberLeaveReq;
        private static DelegateBridge __Hotfix_SendGuildFleetKickOuteReq;
        private static DelegateBridge __Hotfix_SendGuildFleetSetAllowToJoinFleetManualReq;
        private static DelegateBridge __Hotfix_SendPlayerGuildFleetSettingUpdateReq;
        private static DelegateBridge __Hotfix_SendGuildFleetMembersJumpingReq;
        private static DelegateBridge __Hotfix_GetGuildFleetMemberDynamicInfo;
        private static DelegateBridge __Hotfix_GuildFleetMemberComparer;
        private static DelegateBridge __Hotfix_GuildFleetMemberComparerName;
        private static DelegateBridge __Hotfix_GuildFleetMemberComparerShipName;
        private static DelegateBridge __Hotfix_GuildFleetMemberComparerLocation;
        private static DelegateBridge __Hotfix_GuildFleetMemberComparerPosition;
        private static DelegateBridge __Hotfix_EnterAnotherTaskFromMemberOpreationPanel;
        private static DelegateBridge __Hotfix_CheckErrorCodeForNetTask;
        private static DelegateBridge __Hotfix_CheckFleetPosition;
        private static DelegateBridge __Hotfix_UnregistEvent;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_OnInternalPositionChanged;
        private static DelegateBridge __Hotfix_RetirePositon;
        private static DelegateBridge __Hotfix_OnGuildFleetPersonalSettingChanged;
        private static DelegateBridge __Hotfix_OnPassOnButtonClick;
        private static DelegateBridge __Hotfix_OnSetJobButtonClick;
        private static DelegateBridge __Hotfix_OnLeaveButtonClick;
        private static DelegateBridge __Hotfix_OnKickOutButtonClick;
        private static DelegateBridge __Hotfix_OnLeaveForButtonClick;
        private static DelegateBridge __Hotfix_OnMailButtonClick;
        private static DelegateBridge __Hotfix_OnTeamButtonClick;
        private static DelegateBridge __Hotfix_OnSendMsgButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerDetailButtonClick;
        private static DelegateBridge __Hotfix_OnJumpButtonClick;
        private static DelegateBridge __Hotfix_OnReturnButtonClick;
        private static DelegateBridge __Hotfix_ReturnToPreTask;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnCloseSetPositionPanelButtonClick;
        private static DelegateBridge __Hotfix_OnTransferPositionCancelButtonClick;
        private static DelegateBridge __Hotfix_OnTransferPositionConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnParticularButtonClick;
        private static DelegateBridge __Hotfix_OnGuildFleetPozitionDescBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnPromptPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnFormationDescButtonClick;
        private static DelegateBridge __Hotfix_OnMemberItemClick;
        private static DelegateBridge __Hotfix_OnMemberItemFilled;
        private static DelegateBridge __Hotfix_OnSortTypeChangeClick;
        private static DelegateBridge __Hotfix_OnSetToPosition;
        private static DelegateBridge __Hotfix_OnAllowToJoinFleetManualValueChange;
        private static DelegateBridge __Hotfix_OnFleetFormationValueChange;
        private static DelegateBridge __Hotfix_OnOperationPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnRejectFormationButtonClick;
        private static DelegateBridge __Hotfix_OnRejectJumpButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbGuild;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;

        [MethodImpl(0x8000)]
        public GuildFleetDetailsUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckErrorCodeForNetTask(int errorCode, bool checkToUpdate = false)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckFleetPosition(FleetPosition position = 0, bool showNoTip = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void EnterAnotherTaskFromMemberOpreationPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private GuildFleetMemberDynamicInfo GetGuildFleetMemberDynamicInfo(string gameUserId, List<GuildFleetMemberDynamicInfo> infoList)
        {
        }

        [MethodImpl(0x8000)]
        private int GuildFleetMemberComparer(GuildFleetMemberDetailInfo memberA, GuildFleetMemberDetailInfo memberB)
        {
        }

        [MethodImpl(0x8000)]
        private int GuildFleetMemberComparerLocation(int locationA, bool isANearby, int locationB, bool isBNearby)
        {
        }

        [MethodImpl(0x8000)]
        private int GuildFleetMemberComparerName(string nameA, string nameB)
        {
        }

        [MethodImpl(0x8000)]
        private int GuildFleetMemberComparerPosition(GuildFleetMemberInfo memberA, GuildFleetMemberInfo memberB)
        {
        }

        [MethodImpl(0x8000)]
        private int GuildFleetMemberComparerShipName(int shipA, bool aInSpace, int shipB, bool bInSpace)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowToJoinFleetManualValueChange(UIControllerBase ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseSetPositionPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFleetFormationValueChange(int formation)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFormationDescButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFleetPersonalSettingChanged(GuildFleetPersonalSetting changeSetting)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildFleetPozitionDescBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInternalPositionChanged()
        {
        }

        [MethodImpl(0x8000)]
        private void OnJumpButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnKickOutButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaveButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaveForButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMailButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemClick(GuildFleetMemberInfoUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberItemFilled(GuildFleetMemberInfoUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnOperationPanelBackGroundButtonClick(PointerEventData pointData, Action<PointerEventData> passAction)
        {
        }

        [MethodImpl(0x8000)]
        private void OnParticularButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPassOnButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerDetailButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPromptPanelBackGroundButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRejectFormationButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRejectJumpButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnReturnButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSendMsgButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSetJobButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSetToPosition(FleetPosition position, bool set)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortTypeChangeClick(GuildFleetDetailsUIController.GuildFleetDetailSortType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransferPositionCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransferPositionConfirmButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RetirePositon(FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnToPreTask()
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetDetailInfoReq(ulong fleetId, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetKickOuteReq(ulong fleetId, string gameUserId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetMemberLeaveReq(ulong fleetId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetMemberRetireReq(ulong fleetId, string gameUserId, List<uint> fleetPosition, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetMembersJumpingReq(ulong fleetId, string gameUserId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetSetAllowToJoinFleetManualReq(ulong fleetId, bool allowToJoinFleetManual, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetSetCommanderReq(ulong fleetId, string gameUserId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetSetFormationTypeReq(ulong fleetId, int formationType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetSetInternalPositionsReq(ulong fleetId, GuildFleetMemberInfo changedMember, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildFleetTransferPositionReq(ulong fleetId, string gameUserId, FleetPosition fleetPosition, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendPlayerGuildFleetSettingUpdateReq(GuildFleetPersonalSetting setting, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPosition(FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartFleetDetailUITask(UIIntent returnIntent, ulong guildFleetId, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregistEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockGuildClient LbGuild
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <EnterAnotherTaskFromMemberOpreationPanel>c__AnonStoreyC
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                this.onEnd(r);
                this.$this.m_currSelectMemberInfo = null;
            }
        }

        [CompilerGenerated]
        private sealed class <OnMemberItemClick>c__AnonStorey12
        {
            internal GuildFleetMemberInfoUIController ctrl;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0()
            {
                Vector3[] fourCornersArray = new Vector3[4];
                this.ctrl.GetMemberInfoRectTansform().GetWorldCorners(fourCornersArray);
                this.$this.m_guildFleetMemberOperationUIController.SetPanelPos(fourCornersArray[1], fourCornersArray[0]);
            }
        }

        [CompilerGenerated]
        private sealed class <OnPlayerDetailButtonClick>c__AnonStoreyF
        {
            internal CharactorObserveReqNetTas charactorObserveReqNetTask;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(bool res)
            {
                CharactorObserverAck charactorObserverAckInfo = this.charactorObserveReqNetTask.m_charactorObserverAckInfo;
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.CurrentIntent, "CharacterInfoUITask", "CharacterUITaskMode_AnotherPlayer");
                intent.SetParam("CharactorObserverAck", charactorObserverAckInfo);
                UIManager.Instance.StartUITask(intent, true, false, null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnStrikeEnd>c__AnonStorey13
        {
            internal ProjectXPlayerContext ctx;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.Pause();
                if (this.ctx.GetLBCharacter().IsInSpace())
                {
                    CommonStrikeUtil.ReturnToSolarSystemUITask();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnTransferPositionConfirmButtonClick>c__AnonStorey11
        {
            internal FleetPosition positions;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_currSelectMemberInfo = null;
                if (res)
                {
                    TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GuildFleet_BeRetired, new object[0]), GuildUIHelper.GetGuildFleetPositionName(this.positions)), false);
                    this.$this.EnablePipelineStateMask(GuildFleetDetailsUITask.PipeLineStateMaskType.UpdateAll);
                    this.$this.EnablePipelineStateMask(GuildFleetDetailsUITask.PipeLineStateMaskType.UpdateMemberList);
                    this.$this.StartUpdatePipeLine(null, false, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                OpenGuildFleetDetailInfoPanelNetTask task2 = (OpenGuildFleetDetailInfoPanelNetTask) task;
                if (task2.IsNetworkError)
                {
                    if (this.onPrepareEnd != null)
                    {
                        this.onPrepareEnd(false);
                    }
                }
                else
                {
                    if (task2.FleetDetailResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.FleetDetailResult, true, false);
                    }
                    else if (task2.MemberListResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.MemberListResult, true, false);
                    }
                    else if (task2.PlayerGuildFleetSettingResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.PlayerGuildFleetSettingResult, true, false);
                    }
                    else
                    {
                        this.$this.m_currPipeLineCtx = this.$this.GetPipeLineCtx();
                        this.$this.EnablePipelineStateMask(GuildFleetDetailsUITask.PipeLineStateMaskType.UpdateAll);
                        this.$this.EnablePipelineStateMask(GuildFleetDetailsUITask.PipeLineStateMaskType.SortTypeChanged);
                        this.$this.EnablePipelineStateMask(GuildFleetDetailsUITask.PipeLineStateMaskType.UpdateMemberList);
                    }
                    if (this.onPrepareEnd != null)
                    {
                        int num1;
                        if ((task2.FleetDetailResult != 0) || (task2.MemberListResult != 0))
                        {
                            num1 = 0;
                        }
                        else
                        {
                            num1 = (int) (task2.PlayerGuildFleetSettingResult == 0);
                        }
                        this.onPrepareEnd((bool) num1);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RetirePositon>c__AnonStoreyE
        {
            internal string gameUserId;
            internal List<uint> positions;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    if (this.gameUserId == this.$this.PlayerCtx.m_gameUserId)
                    {
                        TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GuildFleet_BeRetired, new object[0]), GuildUIHelper.GetFleetMemberPositionStr(this.positions, " ")), false);
                    }
                    this.$this.OnInternalPositionChanged();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ReturnToPreTask>c__AnonStorey10
        {
            internal UIIntentReturnable intent;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    this.$this.PlayUIProcess(this.$this.m_guildFleetDetailsUIController.CreateMainWindowProcess(true, true, false), true, (r, p) => this.$this.Pause(), false);
                }
            }

            internal void <>m__1(UIProcess process, bool success)
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask(this.intent.PrevTaskIntent, null);
            }

            internal void <>m__2(UIProcess r, bool p)
            {
                this.$this.Pause();
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetDetailInfoReq>c__AnonStorey1
        {
            internal Action onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetDetailInfoReqNetTask task2 = (GuildFleetDetailInfoReqNetTask) task;
                if (!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    if (task2.Result != 0)
                    {
                        this.$this.ReturnToPreTask();
                    }
                    else if (this.onEnd != null)
                    {
                        this.onEnd();
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetKickOuteReq>c__AnonStorey8
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetKickOutReqNetTask task2 = (GuildFleetKickOutReqNetTask) task;
                if (((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && this.$this.CheckErrorCodeForNetTask(task2.Result, true)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetMemberLeaveReq>c__AnonStorey7
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetMemberLeaveReqNetTask task2 = (GuildFleetMemberLeaveReqNetTask) task;
                if ((!task2.IsNetworkError && this.$this.CheckErrorCodeForNetTask(task2.Result, true)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetMemberRetireReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetMemberRetireReqNetTask task2 = (GuildFleetMemberRetireReqNetTask) task;
                if (((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && this.$this.CheckErrorCodeForNetTask(task2.Result, false)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetMembersJumpingReq>c__AnonStoreyB
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetMembersJumpingReqNetTask task2 = (GuildFleetMembersJumpingReqNetTask) task;
                if ((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetSetAllowToJoinFleetManualReq>c__AnonStorey9
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true);
                GuildFleetSetAllowToJoinFleetManualReqNetTask task2 = (GuildFleetSetAllowToJoinFleetManualReqNetTask) task;
                if ((!task2.IsNetworkError && this.$this.CheckErrorCodeForNetTask(task2.Result, true)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetSetCommanderReq>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetSetCommanderReqNetTask task2 = (GuildFleetSetCommanderReqNetTask) task;
                if (((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && this.$this.CheckErrorCodeForNetTask(task2.Result, true)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetSetFormationTypeReq>c__AnonStorey6
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true);
                GuildFleetSetFormationTypeReqNetTask task2 = (GuildFleetSetFormationTypeReqNetTask) task;
                if (((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && this.$this.CheckErrorCodeForNetTask(task2.Result, true)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetSetInternalPositionsReq>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetSetInternalPositionsReqNetTask task2 = (GuildFleetSetInternalPositionsReqNetTask) task;
                if (((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && this.$this.CheckErrorCodeForNetTask(task2.Result, true)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildFleetTransferPositionReq>c__AnonStorey5
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildFleetTransferPositionReqNetTask task2 = (GuildFleetTransferPositionReqNetTask) task;
                if (((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && this.$this.CheckErrorCodeForNetTask(task2.Result, true)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendPlayerGuildFleetSettingUpdateReq>c__AnonStoreyA
        {
            internal Action<bool> onEnd;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(Task task)
            {
                PlayerGuildFleetSettingUpdateReqNetTask task2 = (PlayerGuildFleetSettingUpdateReqNetTask) task;
                if ((!task2.IsNetworkError && (this.$this.State == Task.TaskState.Running)) && (this.onEnd != null))
                {
                    this.onEnd(task2.Result == 0);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SetPosition>c__AnonStoreyD
        {
            internal GuildFleetMemberInfo memberInfo;
            internal GuildFleetDetailsUITask $this;

            internal void <>m__0(bool res)
            {
                if (res)
                {
                    if (this.$this.m_currSelectMemberInfo.m_gameUserId == this.$this.PlayerCtx.m_gameUserId)
                    {
                        TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GuildFleet_BeAppointed, new object[0]), GuildUIHelper.GetGuildFleetPositionName(FleetPosition.FleetPosition_Commander)), false);
                    }
                    this.$this.OnInternalPositionChanged();
                }
            }

            internal void <>m__1(bool res)
            {
                if (res)
                {
                    List<FleetPosition> allFleetPositions = this.memberInfo.GetAllFleetPositions();
                    if ((allFleetPositions.Count > 0) && (this.memberInfo.m_memberGameUserId == this.$this.PlayerCtx.m_gameUserId))
                    {
                        TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_GuildFleet_BeAppointed, new object[0]), GuildUIHelper.GetFleetMemberPositionStr(allFleetPositions, " ")), false);
                    }
                    this.$this.OnInternalPositionChanged();
                }
            }
        }

        public class GuildFleetMemberDetailInfo
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private GuildFleetMemberInfo <BasicInfo>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private GuildFleetMemberDynamicInfo <DynamicInfo>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private GuildMemberRuntimeInfo <RuntimeInfo>k__BackingField;
            public string m_gameUserId;
            public bool m_isInView;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_BasicInfo;
            private static DelegateBridge __Hotfix_set_BasicInfo;
            private static DelegateBridge __Hotfix_get_DynamicInfo;
            private static DelegateBridge __Hotfix_set_DynamicInfo;
            private static DelegateBridge __Hotfix_get_RuntimeInfo;
            private static DelegateBridge __Hotfix_set_RuntimeInfo;

            public GuildFleetMemberDetailInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public GuildFleetMemberInfo BasicInfo
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_BasicInfo;
                    return ((bridge == null) ? this.<BasicInfo>k__BackingField : bridge.__Gen_Delegate_Imp1732(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_BasicInfo;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<BasicInfo>k__BackingField = value;
                    }
                }
            }

            public GuildFleetMemberDynamicInfo DynamicInfo
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_DynamicInfo;
                    return ((bridge == null) ? this.<DynamicInfo>k__BackingField : bridge.__Gen_Delegate_Imp1734(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_DynamicInfo;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<DynamicInfo>k__BackingField = value;
                    }
                }
            }

            public GuildMemberRuntimeInfo RuntimeInfo
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_RuntimeInfo;
                    return ((bridge == null) ? this.<RuntimeInfo>k__BackingField : bridge.__Gen_Delegate_Imp1708(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_RuntimeInfo;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<RuntimeInfo>k__BackingField = value;
                    }
                }
            }
        }

        private enum PipeLineStateMaskType
        {
            UpdateAll,
            SortTypeChanged,
            UpdateMemberList,
            ShowFleetSetInternalPositionsPanel,
            UpdateFleetSetInternalPositionsPanel
        }
    }
}

