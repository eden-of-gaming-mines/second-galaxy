﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public sealed class GuildBenefitsUITask : UITaskBase
    {
        public const string ParamKeyRefreshAllAreas = "ParamKeyRefreshAllAreas";
        public const string ParamKeyPlayAnimationOnPanelOpen = "ParamKeyPlayAnimationOnPanelOpen";
        public const string TaskName = "GuildBenefitsUITask";
        public const string GuildBenefitsPanelStateShow = "Show";
        public const string GuildBenefitsPanelStateClose = "Close";
        public const string StatePanelOpen = "PanelOpen";
        public const string StatePanelClose = "PanelClose";
        public const string InformationPointItemStateCanReceive = "CanReceive";
        public const string InformationPointItemStateReceived = "Received";
        public const string InformationPointItemStateCanNotReceive = "CanNotReceive";
        public const string CommonItemAreaStateNormal = "Normal";
        public const string CommonItemAreaStateEmpty = "Empty";
        public const string SovereigntyItemStateNormal = "Normal";
        public const string SovereigntyitemStateEmpty = "Empty";
        public const string SovereigntyItemButtonStateNormal = "Normal";
        public const string SovereigntyItemButtonStateGray = "Gray";
        public const string SovereigntyItemButtonStateGet = "Get";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool> EventOnRequestGuildManagerUIPause;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<UIIntent>> EventOnRequestSwitchTask;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<IUIBackgroundManager>> EventOnRequestBackgroundManager;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventHandlerOnBenefitsChange;
        private long m_informationPointSelfCurrentWeek;
        private long m_informationPointGuildCurrentWeek;
        private int m_guildOccupiedSolarSystemCount;
        private readonly List<ConfigDataGuildBenefitsConfigInfo> m_guildInformationPointDefaultList;
        private List<GuildInformationPointBenefitsInfo> m_guildInformationPointBenefitsList;
        private readonly List<string> m_guildInformationPointItemStateList;
        private readonly List<GuildCommonBenefitsInfo> m_guildCommonBenefitsList;
        private GuildSovereigntyBenefitsInfo m_guildSovereigntyBenefits;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private GuildBenefitsUIController m_guildBenefitsUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Comparison<ConfigDataGuildBenefitsConfigInfo> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<GuildInformationPointBenefitsInfo> <>f__am$cache1;
        [CompilerGenerated]
        private static Comparison<GuildCommonBenefitsInfo> <>f__am$cache2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowGuildBenefitsPanel;
        private static DelegateBridge __Hotfix_HideGuildBenefitsPanel;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_InitParams;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_InvokePrepareOnEndFunction;
        private static DelegateBridge __Hotfix_UpdateInformationPointArea;
        private static DelegateBridge __Hotfix_UpdateCommonArea;
        private static DelegateBridge __Hotfix_UpdateSovereigntyArea;
        private static DelegateBridge __Hotfix_SendMessageOnItemReceivingImp;
        private static DelegateBridge __Hotfix_OpenSimpleInfoPanel;
        private static DelegateBridge __Hotfix_OnRequestGuildManagerUIPause;
        private static DelegateBridge __Hotfix_OnRequestSwitchTask;
        private static DelegateBridge __Hotfix_OnRequestBackgroundManager;
        private static DelegateBridge __Hotfix_OnBenefitsChangeNotify;
        private static DelegateBridge __Hotfix_OnInformationPointTipOpenButtonClick;
        private static DelegateBridge __Hotfix_OnInformationPointTipCloseButtonClick;
        private static DelegateBridge __Hotfix_OnSovereigntyTipOpenButtonClick;
        private static DelegateBridge __Hotfix_OnSovereigntyTipCloseButtonClick;
        private static DelegateBridge __Hotfix_OnInformationPointItemClick;
        private static DelegateBridge __Hotfix_OnCommonItemObtainClick;
        private static DelegateBridge __Hotfix_OnCommonItemRewardsPreviewClick;
        private static DelegateBridge __Hotfix_OnSovereigntyItemObtainClick;
        private static DelegateBridge __Hotfix_OnSovereigntyShopButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnRequestGuildManagerUIPause;
        private static DelegateBridge __Hotfix_remove_EventOnRequestGuildManagerUIPause;
        private static DelegateBridge __Hotfix_add_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_remove_EventOnRequestSwitchTask;
        private static DelegateBridge __Hotfix_add_EventOnRequestBackgroundManager;
        private static DelegateBridge __Hotfix_remove_EventOnRequestBackgroundManager;
        private static DelegateBridge __Hotfix_add_EventHandlerOnBenefitsChange;
        private static DelegateBridge __Hotfix_remove_EventHandlerOnBenefitsChange;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventHandlerOnBenefitsChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<IUIBackgroundManager>> EventOnRequestBackgroundManager
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action, bool> EventOnRequestGuildManagerUIPause
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<UIIntent>> EventOnRequestSwitchTask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildBenefitsUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void HideGuildBenefitsPanel(bool immediateComplete, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParams(UIIntentCustom intentCustom)
        {
        }

        [MethodImpl(0x8000)]
        private void InvokePrepareOnEndFunction(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBenefitsChangeNotify()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemObtainClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCommonItemRewardsPreviewClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInformationPointItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInformationPointTipCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInformationPointTipOpenButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestBackgroundManager(Action<IUIBackgroundManager> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestGuildManagerUIPause(Action evntAfterPause, bool isIgnoreAnim)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestSwitchTask(Action<UIIntent> onRequestEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSovereigntyItemObtainClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSovereigntyShopButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSovereigntyTipCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSovereigntyTipOpenButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OpenSimpleInfoPanel(FakeLBStoreItem item, Transform dummyTrans)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendMessageOnItemReceivingImp(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowGuildBenefitsPanel(bool immediateComplete, Action<bool> onPrepareEnd, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCommonArea()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateInformationPointArea()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSovereigntyArea()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideGuildBenefitsPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal GuildBenefitsUITask $this;

            internal void <>m__0(UIProcess process, bool isComplete)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnSovereigntyShopButtonClick>c__AnonStorey3
        {
            internal UIIntent intent;

            internal void <>m__0(IUIBackgroundManager backgroundManager)
            {
                NpcShopMainUITask.StartNpcShopMainUITaskWithOutDialog_Defult(NpcShopFlag.NpcShopFlag_GuildGalaNpcShop, this.intent, null, backgroundManager, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OpenSimpleInfoPanel>c__AnonStorey2
        {
            internal FakeLBStoreItem item;
            internal Vector3 position;
            internal string mode;
            internal GuildBenefitsUITask $this;

            internal void <>m__0(UIIntent returnIntent)
            {
                this.$this.m_itemSimpleInfoUITask = ItemSimpleInfoUITask.StartItemSimpleInfoUITask(this.item, returnIntent, true, this.position, this.mode, ItemSimpleInfoUITask.PositionType.UseInput, false, null, null, true, true, true);
                if (this.$this.m_itemSimpleInfoUITask != null)
                {
                    this.$this.m_itemSimpleInfoUITask.UnregisterAllEvent();
                    this.$this.m_itemSimpleInfoUITask.EventOnEnterAnotherTask += taskName => this.$this.OnRequestGuildManagerUIPause(() => this.$this.m_itemSimpleInfoUITask = null, false);
                }
            }

            internal void <>m__1(string taskName)
            {
                this.$this.OnRequestGuildManagerUIPause(() => this.$this.m_itemSimpleInfoUITask = null, false);
            }

            internal void <>m__2()
            {
                this.$this.m_itemSimpleInfoUITask = null;
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey1
        {
            internal Action<bool> onPrepareEnd;
            internal GuildBenefitsUITask $this;

            internal void <>m__0(Task task)
            {
                GuildBenefitsUITask.SendGuildBenefitsListReqNetWorkTask task2 = task as GuildBenefitsUITask.SendGuildBenefitsListReqNetWorkTask;
                if ((task2 != null) && (task2.Result != 0))
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
                this.$this.EnablePipelineStateMask(GuildBenefitsUITask.PipeLineStateMaskType.ReceiveGuildBenefitsListAck);
                if ((task2 != null) && (task2.Result == 0))
                {
                    this.$this.EnablePipelineStateMask(GuildBenefitsUITask.PipeLineStateMaskType.SuccessOnGuildBenefitsListAck);
                }
                this.$this.InvokePrepareOnEndFunction(this.onPrepareEnd);
            }

            internal void <>m__1(Task task)
            {
                GuildBenefitsUITask.SendGuildAndSelfDynamicInfoReqNetWorkTask task2 = task as GuildBenefitsUITask.SendGuildAndSelfDynamicInfoReqNetWorkTask;
                if ((task2 != null) && (task2.Result != 0))
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
                this.$this.EnablePipelineStateMask(GuildBenefitsUITask.PipeLineStateMaskType.ReceiveGuildAndSelfDynamicInfoAck);
                if ((task2 != null) && (task2.Result == 0))
                {
                    this.$this.EnablePipelineStateMask(GuildBenefitsUITask.PipeLineStateMaskType.SuccessOnGuildAndSelfDynamicInfoAck);
                }
                this.$this.InvokePrepareOnEndFunction(this.onPrepareEnd);
            }

            internal void <>m__2(Task task)
            {
                GuildOccupiedSolarSystemInfoReqNetTask task2 = task as GuildOccupiedSolarSystemInfoReqNetTask;
                if ((task2 != null) && (task2.m_result != 0))
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.m_result, true, false);
                }
                this.$this.EnablePipelineStateMask(GuildBenefitsUITask.PipeLineStateMaskType.ReceiveGuildOccupiedSolarSystemInfoReqNetTask);
                if ((task2 != null) && (task2.m_result == 0))
                {
                    this.$this.EnablePipelineStateMask(GuildBenefitsUITask.PipeLineStateMaskType.SuccessOnGuildOccupiedSolarSystemInfoReqNetTask);
                }
                this.$this.InvokePrepareOnEndFunction(this.onPrepareEnd);
            }
        }

        private enum PipeLineStateMaskType
        {
            ReceiveGuildBenefitsListAck,
            SuccessOnGuildBenefitsListAck,
            ReceiveGuildAndSelfDynamicInfoAck,
            SuccessOnGuildAndSelfDynamicInfoAck,
            ReceiveGuildOccupiedSolarSystemInfoReqNetTask,
            SuccessOnGuildOccupiedSolarSystemInfoReqNetTask,
            UpdateDataCache,
            UpdateCommonBenefitsArea,
            InitCommonBenefitsAreaContent,
            UpdateSovereigntyBenefitsArea,
            UpdateInformationPointBenefitsArea
        }

        public class SendGuildAndSelfDynamicInfoReqNetWorkTask : NetWorkTransactionTask
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private int <Result>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnGuildAndSelfDynamicInfoAck;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;

            public SendGuildAndSelfDynamicInfoReqNetWorkTask() : base(10f, true, false, false)
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            private void OnGuildAndSelfDynamicInfoAck(int result)
            {
                DelegateBridge bridge = __Hotfix_OnGuildAndSelfDynamicInfoAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp31(this, result);
                }
                else
                {
                    this.Result = result;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    (GameManager.Instance.PlayerContext as ProjectXPlayerContext).EventOnGuildAndSelfDynamicInfoAck += new Action<int>(this.OnGuildAndSelfDynamicInfoAck);
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendGuildAndSelfDynamicInfoReq());
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    (GameManager.Instance.PlayerContext as ProjectXPlayerContext).EventOnGuildAndSelfDynamicInfoAck -= new Action<int>(this.OnGuildAndSelfDynamicInfoAck);
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }
        }

        public class SendGuildBenefitsListReqNetWorkTask : NetWorkTransactionTask
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <Result>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnGuildBenefitsListAck;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;

            public SendGuildBenefitsListReqNetWorkTask() : base(10f, true, false, false)
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            private void OnGuildBenefitsListAck(GuildBenefitsListAck guildBenefitsListAck)
            {
                DelegateBridge bridge = __Hotfix_OnGuildBenefitsListAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, guildBenefitsListAck);
                }
                else
                {
                    this.Result = guildBenefitsListAck.Result;
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildBenefitsListAck += new Action<GuildBenefitsListAck>(this.OnGuildBenefitsListAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendBenefitsListReq());
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildBenefitsListAck -= new Action<GuildBenefitsListAck>(this.OnGuildBenefitsListAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }
        }

        public class SendGuildBenefitsReceiveReqNetWorkTask : NetWorkTransactionTask
        {
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private int <Result>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private ulong <BenefitsInstanceId>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private GuildBenefitsType <BenefitsType>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private List<StoreItemUpdateInfo> <StoreItemUpdateInfos>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private List<CurrencyUpdateInfo> <CurrencyUpdateInfos>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_StartNetWorking;
            private static DelegateBridge __Hotfix_RegisterNetworkEvent;
            private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
            private static DelegateBridge __Hotfix_OnGuildBenefitsReceiveAck;
            private static DelegateBridge __Hotfix_get_Result;
            private static DelegateBridge __Hotfix_set_Result;
            private static DelegateBridge __Hotfix_get_BenefitsInstanceId;
            private static DelegateBridge __Hotfix_set_BenefitsInstanceId;
            private static DelegateBridge __Hotfix_get_BenefitsType;
            private static DelegateBridge __Hotfix_set_BenefitsType;
            private static DelegateBridge __Hotfix_get_StoreItemUpdateInfos;
            private static DelegateBridge __Hotfix_set_StoreItemUpdateInfos;
            private static DelegateBridge __Hotfix_get_CurrencyUpdateInfos;
            private static DelegateBridge __Hotfix_set_CurrencyUpdateInfos;

            public SendGuildBenefitsReceiveReqNetWorkTask(ulong benefitsInstanceId) : base(10f, true, false, false)
            {
                this.BenefitsInstanceId = benefitsInstanceId;
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp323(this, benefitsInstanceId);
                }
            }

            private void OnGuildBenefitsReceiveAck(GuildBenefitsReceiveAck guildBenefitsReceiveAck)
            {
                DelegateBridge bridge = __Hotfix_OnGuildBenefitsReceiveAck;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp5(this, guildBenefitsReceiveAck);
                }
                else
                {
                    this.Result = guildBenefitsReceiveAck.Result;
                    this.BenefitsInstanceId = guildBenefitsReceiveAck.BenefitsInstanceId;
                    this.BenefitsType = (GuildBenefitsType) guildBenefitsReceiveAck.BenefitsType;
                    this.StoreItemUpdateInfos = new List<StoreItemUpdateInfo>();
                    foreach (ProStoreItemUpdateInfo info in guildBenefitsReceiveAck.StoreItemUpdateList)
                    {
                        this.StoreItemUpdateInfos.Add(CommonLogicUtil.Convert2StoreItemUpdateInfo(info));
                    }
                    this.CurrencyUpdateInfos = new List<CurrencyUpdateInfo>();
                    foreach (ProCurrencyUpdateInfo info2 in guildBenefitsReceiveAck.CurrencyUpdateList)
                    {
                        this.CurrencyUpdateInfos.Add(CommonLogicUtil.Convert2CurrencyUpdateInfo(info2));
                    }
                    base.Stop();
                }
            }

            protected override void RegisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_RegisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.RegisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildBenefitsReceiveAck += new Action<GuildBenefitsReceiveAck>(this.OnGuildBenefitsReceiveAck);
                    }
                }
            }

            protected override bool StartNetWorking()
            {
                DelegateBridge bridge = __Hotfix_StartNetWorking;
                if (bridge != null)
                {
                    return bridge.__Gen_Delegate_Imp9(this);
                }
                base.StartNetWorking();
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                return ((playerContext != null) && playerContext.SendGuildBenefitsReceiveReq(this.BenefitsInstanceId));
            }

            protected override void UnregisterNetworkEvent()
            {
                DelegateBridge bridge = __Hotfix_UnregisterNetworkEvent;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
                else
                {
                    base.UnregisterNetworkEvent();
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (playerContext != null)
                    {
                        playerContext.EventOnGuildBenefitsReceiveAck -= new Action<GuildBenefitsReceiveAck>(this.OnGuildBenefitsReceiveAck);
                    }
                }
            }

            public int Result
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Result;
                    return ((bridge == null) ? this.<Result>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_Result;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Result>k__BackingField = value;
                    }
                }
            }

            public ulong BenefitsInstanceId
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_BenefitsInstanceId;
                    return ((bridge == null) ? this.<BenefitsInstanceId>k__BackingField : bridge.__Gen_Delegate_Imp1270(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_BenefitsInstanceId;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp323(this, value);
                    }
                    else
                    {
                        this.<BenefitsInstanceId>k__BackingField = value;
                    }
                }
            }

            public GuildBenefitsType BenefitsType
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_BenefitsType;
                    return ((bridge == null) ? this.<BenefitsType>k__BackingField : bridge.__Gen_Delegate_Imp1119(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_BenefitsType;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp1120(this, value);
                    }
                    else
                    {
                        this.<BenefitsType>k__BackingField = value;
                    }
                }
            }

            public List<StoreItemUpdateInfo> StoreItemUpdateInfos
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_StoreItemUpdateInfos;
                    return ((bridge == null) ? this.<StoreItemUpdateInfos>k__BackingField : bridge.__Gen_Delegate_Imp4434(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_StoreItemUpdateInfos;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<StoreItemUpdateInfos>k__BackingField = value;
                    }
                }
            }

            public List<CurrencyUpdateInfo> CurrencyUpdateInfos
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_CurrencyUpdateInfos;
                    return ((bridge == null) ? this.<CurrencyUpdateInfos>k__BackingField : bridge.__Gen_Delegate_Imp4575(this));
                }
                [CompilerGenerated]
                private set
                {
                    DelegateBridge bridge = __Hotfix_set_CurrencyUpdateInfos;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<CurrencyUpdateInfos>k__BackingField = value;
                    }
                }
            }
        }
    }
}

