﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class QuestCommitItemDialogNetTask : NetWorkTransactionTask
    {
        private int m_questInstanceId;
        private int m_questCondIndex;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <ItemCommitResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnCommitItemResult;
        private static DelegateBridge __Hotfix_set_ItemCommitResult;
        private static DelegateBridge __Hotfix_get_ItemCommitResult;

        [MethodImpl(0x8000)]
        public QuestCommitItemDialogNetTask(int questInsId, int condIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCommitItemResult(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int ItemCommitResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

