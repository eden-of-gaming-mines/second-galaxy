﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public sealed class GuildActionUITask : GuildUITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action, bool> EventOnPauseGuildManagerUITask;
        private Dictionary<int, List<GuildActionInfo>> m_underwayDics;
        private List<ConfigDataGuildActionInfo> m_issueInfos;
        private Tab m_curTab;
        private int m_curIssueConfigId;
        private ulong m_curUndewayInstanceID;
        private int m_curSolarSysId;
        private int m_curEnterSolarSysId;
        private Action BeforeChangeToStarMapAction;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private GuildActionUIController m_guildActionUIController;
        private IUIBackgroundManager m_backgroundManager;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        public const string ModeInStarMap = "ModeInStarMap";
        public const string ModeInGuildManager = "ModeInGuildManager";
        public const string CurStaySolarSysId = "CurSolarSysId";
        public const string BeforeChangeToStarMap = "BeforeChangeToStarMap";
        public const string TaskName = "GuildActionUITask";
        [CompilerGenerated]
        private static Comparison<Tuple<int, int>> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTaskWithPrepare;
        private static DelegateBridge __Hotfix_StartTask;
        private static DelegateBridge __Hotfix_StartGuidActionUITask;
        private static DelegateBridge __Hotfix_CloseActionPanel;
        private static DelegateBridge __Hotfix_RequestGuildActionInfo;
        private static DelegateBridge __Hotfix_IsPerssionFit;
        private static DelegateBridge __Hotfix_IsActionFailed;
        private static DelegateBridge __Hotfix_IsSovereignFit;
        private static DelegateBridge __Hotfix_IsSolarSystemSovereignLevelFit;
        private static DelegateBridge __Hotfix_GetGuildActionSelfCurrencyReward;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_GetGuildMineralResPath;
        private static DelegateBridge __Hotfix_GetGuildRewardResPath;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnIssueBtnClick;
        private static DelegateBridge __Hotfix_OnGoButtonClick;
        private static DelegateBridge __Hotfix_OnStarMapButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnIssueTabBtnClick;
        private static DelegateBridge __Hotfix_OnUnderwayTabBtnClick;
        private static DelegateBridge __Hotfix_OnSolarSysItemClick;
        private static DelegateBridge __Hotfix_OnUnderwayQuestItemClick;
        private static DelegateBridge __Hotfix_OnIssueQuestItemClick;
        private static DelegateBridge __Hotfix_OnGoIssueButtonClick;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnLossWarningClick;
        private static DelegateBridge __Hotfix_OnShowIssueHintCtrlClick;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_GetActionInfoByInstanceId;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_InitDefaultData;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_add_EventOnPauseGuildManagerUITask;
        private static DelegateBridge __Hotfix_remove_EventOnPauseGuildManagerUITask;
        private static DelegateBridge __Hotfix_RegiserPauseGuildManagerUITask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        private event Action<Action, bool> EventOnPauseGuildManagerUITask
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildActionUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void CloseActionPanel(bool immedite, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private GuildActionInfo GetActionInfoByInstanceId(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public static void GetGuildActionSelfCurrencyReward(ConfigDataGuildActionInfo actionInfo, int systemDifficultLevel, out string currencyIconPath, out int currencyCount)
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildMineralResPath(int solarSysId, int rewardListId, List<string> resPaths)
        {
        }

        [MethodImpl(0x8000)]
        private void GetGuildRewardResPath(int dropRewardId, List<string> resPaths)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitDefaultData()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsActionFailed(GuildActionInfo action)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPerssionFit(ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSolarSystemSovereignLevelFit(int guildConfigId, int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsSovereignFit(int guildConfigId, int solarSystemId, ref int errCode)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoButtonClick(ulong actionInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGoIssueButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueBtnClick(int configId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueQuestItemClick(int configId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnIssueTabBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossWarningClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(CommonItemIconUIController commonItemIconCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShowIssueHintCtrlClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSolarSysItemClick(int galaxyId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapButtonClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnderwayQuestItemClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnUnderwayTabBtnClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void RegiserPauseGuildManagerUITask(Action<Action, bool> pauseEvent)
        {
        }

        [MethodImpl(0x8000)]
        public static void RequestGuildActionInfo(Action<bool> onAckEnd, bool showErrorTip = true)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ItemInfo itemInfo, Vector3 pos, ItemSimpleInfoUITask.PositionType type)
        {
        }

        [MethodImpl(0x8000)]
        public void StartGuidActionUITask(string mode, int solarSystemId, UIIntent preIntent, Action<bool> onPipeLineEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildActionUITask StartTask(string mode, UIIntent parentIntent, int solarSystemId, Action onResLoadEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTaskWithPrepare(string mode, UIIntent parentIntent, int solarSystemId, Action beforeChangToStarMap = null, Action<bool> onPipelineEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <BeforeChooseShipForStrike>c__AnonStorey8
        {
            internal Action<UIIntent, bool> onEnd;
            internal UIIntent preIntent;

            internal void <>m__0()
            {
                if (this.onEnd != null)
                {
                    this.onEnd(this.preIntent, true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <BeforeEnterSpaceStationUITask>c__AnonStorey7
        {
            internal Action onEnd;

            internal void <>m__0()
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CloseActionPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal GuildActionUITask $this;

            internal void <>m__0(UIProcess p, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnGoButtonClick>c__AnonStorey2
        {
            internal UITaskPipeLineCtx pipe;
            internal GuildActionInfo action;
            internal ulong actionInstanceId;
            internal GuildActionUITask $this;
            private static Action <>f__am$cache0;

            internal void <>m__0(Task deleteTask)
            {
                GuildActionDeleteReq req = deleteTask as GuildActionDeleteReq;
                if ((req != null) && (this.$this.State == Task.TaskState.Running))
                {
                    if (req.ActionCreateAck.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(req.ActionCreateAck.Result, true, false);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithStringTableId(StringTableId.StringTableId_GuildActionForgetSucess, false, new object[0]);
                        this.pipe.AddUpdateMask(7);
                        this.$this.StartUpdatePipeLine(this.$this.CurrentIntent, false, false, true, null);
                    }
                }
            }

            internal void <>m__1(Task task)
            {
                GuildActionInfoUpdateReqNetTask task2 = task as GuildActionInfoUpdateReqNetTask;
                if ((task2 != null) && (this.$this.State == Task.TaskState.Running))
                {
                    if (task2.Ack.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Ack.Result, true, false);
                        if (task2.Ack.Result != -1949)
                        {
                            return;
                        }
                    }
                    if (task2.Ack.Result == -1949)
                    {
                        if (task2.Ack.GuildActionInfo == null)
                        {
                            this.pipe.AddUpdateMask(4);
                        }
                        else
                        {
                            this.pipe.AddUpdateMask(5);
                        }
                        this.$this.StartUpdatePipeLine(this.$this.CurrentIntent, false, false, true, null);
                    }
                    else
                    {
                        ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                        if ((playerContext == null) || !playerContext.GetLBCharacter().IsInSpace())
                        {
                            CommonStrikeUtil.StrikeForGuildAction(this.$this, this.action.m_solarSystemId, this.actionInstanceId, this.$this.m_backgroundManager, null, null);
                        }
                        else
                        {
                            if (<>f__am$cache0 == null)
                            {
                                <>f__am$cache0 = () => CommonUITaskHelper.ReturnToBasicUITask(null, true);
                            }
                            CommonStrikeUtil.StrikeForGuildAction(this.$this, this.action.m_solarSystemId, this.actionInstanceId, this.$this.m_backgroundManager, <>f__am$cache0, null);
                        }
                    }
                }
            }

            private static void <>m__2()
            {
                CommonUITaskHelper.ReturnToBasicUITask(null, true);
            }
        }

        [CompilerGenerated]
        private sealed class <OnGoIssueButtonClick>c__AnonStorey5
        {
            internal ProjectXPlayerContext ctx;
            internal GuildActionUITask $this;

            internal void <>m__0()
            {
                UIIntent prevTaskIntent = (this.$this.CurrentIntent as UIIntentReturnable).PrevTaskIntent;
                bool flag = SpaceStationBGTask.PauseStationBgTask();
                ((UIIntentCustom) prevTaskIntent).SetParam("ResumeStationBgWhenResume", flag);
                StarMapManagerUITask.StartStarMapForGuildUITask(prevTaskIntent, this.ctx.CurrSolarSystemId, false, StarMapForGuildUITask.SubTaskPanel.Action, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnStarMapButtonClick>c__AnonStorey3
        {
            internal GuildActionInfo actionInfo;
            internal GuildActionUITask $this;

            internal void <>m__0()
            {
                ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                UIIntent prevTaskIntent = (this.$this.CurrentIntent as UIIntentReturnable).PrevTaskIntent;
                bool flag = SpaceStationBGTask.PauseStationBgTask();
                ((UIIntentCustom) prevTaskIntent).SetParam("ResumeStationBgWhenResume", flag);
                if (playerContext.CurrSolarSystemId == this.actionInfo.m_solarSystemId)
                {
                    StarMapManagerUITask.StartStarMapForSolarSystemUITask("StarMapForSolarSystemMode_NormalMode", this.actionInfo.m_solarSystemId, prevTaskIntent, null, null, null, null);
                }
                else
                {
                    StarMapManagerUITask.StartStarMapForStarfieldUITask(prevTaskIntent, this.actionInfo.m_solarSystemId, playerContext.CurrSolarSystemId, this.actionInfo.m_solarSystemId, GEBrushType.GEBrushType_SecurityLevel, null, 0, true, null, false, 0, 0UL, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnStarMapButtonClick>c__AnonStorey4
        {
            internal Action beforeChangeToStarMap;
            internal GuildActionUITask.<OnStarMapButtonClick>c__AnonStorey3 <>f__ref$3;

            internal void <>m__0(bool ret)
            {
                if (ret)
                {
                    ProjectXPlayerContext playerContext = GameManager.Instance.PlayerContext as ProjectXPlayerContext;
                    if (this.<>f__ref$3.$this.m_curEnterSolarSysId == this.<>f__ref$3.actionInfo.m_solarSystemId)
                    {
                        UIIntentReturnable currentIntent = this.<>f__ref$3.$this.CurrentIntent as UIIntentReturnable;
                        UIManager.Instance.ReturnUITask(currentIntent.PrevTaskIntent, null);
                    }
                    else
                    {
                        if (this.beforeChangeToStarMap != null)
                        {
                            this.beforeChangeToStarMap();
                        }
                        StarMapManagerUITask.StartStarMapForStarfieldUITask(((this.<>f__ref$3.$this.CurrentIntent as UIIntentReturnable).PrevTaskIntent as UIIntentReturnable).PrevTaskIntent, this.<>f__ref$3.actionInfo.m_solarSystemId, playerContext.CurrSolarSystemId, this.<>f__ref$3.actionInfo.m_solarSystemId, GEBrushType.GEBrushType_SecurityLevel, null, 0, true, null, false, 0, 0UL, null);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnStrikeEnd>c__AnonStorey6
        {
            internal ProjectXPlayerContext ctx;
            internal GuildActionUITask $this;

            internal void <>m__0()
            {
                this.$this.Pause();
                if (this.ctx.GetLBCharacter().IsInSpace())
                {
                    CommonStrikeUtil.ReturnToSolarSystemUITask();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RequestGuildActionInfo>c__AnonStorey1
        {
            internal Action<bool> onAckEnd;
            internal bool showErrorTip;

            internal void <>m__0(Task task)
            {
                GuildActionInfoReq req = task as GuildActionInfoReq;
                if ((req == null) || (req.ActionInfoAck == null))
                {
                    if (this.onAckEnd != null)
                    {
                        this.onAckEnd(false);
                    }
                }
                else if (req.ActionInfoAck.Result == 0)
                {
                    if (this.onAckEnd != null)
                    {
                        this.onAckEnd(true);
                    }
                }
                else
                {
                    if (this.showErrorTip)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(req.ActionInfoAck.Result, true, false);
                    }
                    if (this.onAckEnd != null)
                    {
                        this.onAckEnd(false);
                    }
                }
            }
        }

        public enum Pipeline
        {
            ChangeTab,
            ChangeIssueSelected,
            ChangeGalaxySelected,
            ChangeGalaxyQuestSelected,
            HasActionComplete,
            HasActionFailed,
            SelectActionIsComplete,
            GiveUpAction
        }

        public enum Tab
        {
            Issue,
            Underway
        }

        private class Tuple<T1, T2>
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private T1 <P1>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private T2 <P2>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_set_P1;
            private static DelegateBridge __Hotfix_get_P1;
            private static DelegateBridge __Hotfix_set_P2;
            private static DelegateBridge __Hotfix_get_P2;

            public Tuple(T1 p1, T2 p2)
            {
                this.P1 = p1;
                this.P2 = p2;
                DelegateBridge bridge = GuildActionUITask.Tuple<T1, T2>._c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.InvokeSessionStart();
                    bridge.InParam<GuildActionUITask.Tuple<T1, T2>>((GuildActionUITask.Tuple<T1, T2>) this);
                    bridge.InParam<T1>(p1);
                    bridge.InParam<T2>(p2);
                    bridge.Invoke(0);
                    bridge.InvokeSessionEnd();
                }
            }

            public T1 P1
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = GuildActionUITask.Tuple<T1, T2>.__Hotfix_get_P1;
                    if (bridge == null)
                    {
                        return this.<P1>k__BackingField;
                    }
                    bridge.InvokeSessionStart();
                    bridge.InParam<GuildActionUITask.Tuple<T1, T2>>((GuildActionUITask.Tuple<T1, T2>) this);
                    bridge.Invoke(1);
                    return bridge.InvokeSessionEndWithResult<T1>();
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = GuildActionUITask.Tuple<T1, T2>.__Hotfix_set_P1;
                    if (bridge == null)
                    {
                        this.<P1>k__BackingField = value;
                    }
                    else
                    {
                        bridge.InvokeSessionStart();
                        bridge.InParam<GuildActionUITask.Tuple<T1, T2>>((GuildActionUITask.Tuple<T1, T2>) this);
                        bridge.InParam<T1>(value);
                        bridge.Invoke(0);
                        bridge.InvokeSessionEnd();
                    }
                }
            }

            public T2 P2
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = GuildActionUITask.Tuple<T1, T2>.__Hotfix_get_P2;
                    if (bridge == null)
                    {
                        return this.<P2>k__BackingField;
                    }
                    bridge.InvokeSessionStart();
                    bridge.InParam<GuildActionUITask.Tuple<T1, T2>>((GuildActionUITask.Tuple<T1, T2>) this);
                    bridge.Invoke(1);
                    return bridge.InvokeSessionEndWithResult<T2>();
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = GuildActionUITask.Tuple<T1, T2>.__Hotfix_set_P2;
                    if (bridge == null)
                    {
                        this.<P2>k__BackingField = value;
                    }
                    else
                    {
                        bridge.InvokeSessionStart();
                        bridge.InParam<GuildActionUITask.Tuple<T1, T2>>((GuildActionUITask.Tuple<T1, T2>) this);
                        bridge.InParam<T2>(value);
                        bridge.Invoke(0);
                        bridge.InvokeSessionEnd();
                    }
                }
            }
        }
    }
}

