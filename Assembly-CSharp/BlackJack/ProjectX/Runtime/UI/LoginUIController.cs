﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class LoginUIController : UIControllerBase
    {
        private bool m_isAnnoucementActivity;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnLogoAnimationFinished;
        private string m_currMode;
        private bool m_isServerInMaintenance;
        public const string UIMODE_LOGINWITHIPINPUT = "LoginWithIPInput";
        public const string UIMODE_LOGINWITHSETTING = "LoginWithSettings";
        public const string UIMODE_LOGINWITHSERVERLISTINEDITOR = "LoginWithServerListInEditor";
        public const string UIMODE_SHOWLOGOANIMATION = "ShowLogoAnimation";
        public const string UIMODE_LOGINWITH_SDK = "ShowLogoAnimation_SDK";
        public const string UIMODE_HIDEALL = "HideAll";
        private const string LoginUIState_InputThreeAppear = "InputThreeAppear";
        private const string LoginUIState_InputThreeDisppear = "InputThreeDisppear";
        private const string LoginUIState_EnterButtonAppear = "EnterButtonAppear";
        private const string LoginUIState_EnterButtonDisable = "EnterButtonDisable";
        private const string LoginUIState_ServerListWithNameInput = "NameButtonAppear";
        [AutoBind("./ButtonAndInput", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController ButtonAndInputRoot;
        [AutoBind("./ButtonAndInput/EnterGameButton", AutoBindAttribute.InitState.NotInit, false)]
        private Button EnterGameButton;
        [AutoBind("./ButtonAndInput/EnterGameButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        private Text EnterGameButtonText;
        [AutoBind("./ButtonAndInput/SendLogBtn", AutoBindAttribute.InitState.NotInit, false)]
        private Button SendLogBtn;
        [AutoBind("./ButtonAndInput/InputPanel/InputIPAddress", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject ServerIPRoot;
        [AutoBind("./ButtonAndInput/InputPanel/InputIPAddress/InputField", AutoBindAttribute.InitState.NotInit, false)]
        private InputField ServerIPInput;
        [AutoBind("./ButtonAndInput/InputPanel/InputPort", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject ServerPortRoot;
        [AutoBind("./ButtonAndInput/InputPanel/InputPort/InputField", AutoBindAttribute.InitState.NotInit, false)]
        private InputField ServerPortInput;
        [AutoBind("./ButtonAndInput/InputPanel/InputAccount", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject AccountInputRoot;
        [AutoBind("./ButtonAndInput/InputPanel/InputAccount/InputField", AutoBindAttribute.InitState.NotInit, false)]
        private InputField AccountInput;
        [AutoBind("./ButtonAndInput/InputAccount01/InputField", AutoBindAttribute.InitState.NotInit, false)]
        private InputField AccountInputInEditorServerList;
        [AutoBind("./BoardingMsg", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject BoardingMsgRoot;
        [AutoBind("./BoardingMsg/BoardingText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BoardingMsg;
        [AutoBind("./SGLogoEffect", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SGLogoEffect;
        [AutoBind("./MovieButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MovieButton;
        [AutoBind("./LoadingImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LoadingImageGo;
        [AutoBind("./MsgBoxUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject NetWorkNotReachableRoot;
        [AutoBind("./MsgBoxUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController NetWorkNotReachableCtrl;
        [AutoBind("./MsgBoxUIPrefab/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx RetryConnectButton;
        [AutoBind("./MsgBoxUIPrefab/TextGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
        private Text ErrorText;
        [AutoBind("./ButtonAndInput/SwitchAccountButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx SwitchAccountButton;
        [AutoBind("./ButtonAndInput/Buttongroup/CodescanButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx m_codeScanButton;
        [AutoBind("./AnnouncementGroup/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        private ScrollRect ANDetailScrollView;
        [AutoBind("./AnnouncementGroup/ButtonScroll View", AutoBindAttribute.InitState.NotInit, false)]
        private ScrollRect ANBtnScrollView;
        [AutoBind("./AnnouncementGroup/ButtonScroll View/AnnouncementButton", AutoBindAttribute.InitState.NotInit, false)]
        private GameObject ANBtnPrefab;
        [AutoBind("./AnnouncementGroup/Scroll View/Viewport/Content/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        private Text ANDetailContent;
        [AutoBind("./AnnouncementGroup/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx ANCloseBtnClick;
        [AutoBind("./AnnouncementGroup/ButtonScroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        private ToggleGroup ANBtnToggleGroup;
        [AutoBind("./AnnouncementGroup", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController AnnouncementAnimation;
        [AutoBind("./AnnouncementGroup/BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx AnnoucementBGButton;
        [AutoBind("./AnnouncementGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AnnoucementConfirmButton;
        [AutoBind("./ButtonAndInput/InMaintenanceText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_inMaintenanceText;
        [AutoBind("./ButtonAndInput/Buttongroup/AnnouncementButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AnnoucementButton;
        [AutoBind("./AnnouncementGroup/Scroll View/Viewport/Content/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        private Text AnnoucementText;
        [AutoBind("./ButtonAndInput/VersionsText", AutoBindAttribute.InitState.NotInit, false)]
        private Text VersionsText;
        [AutoBind("./DebugMsgLabel/Text", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_debugMsgText;
        [AutoBind("./ButtonAndInput/Buttongroup/LanguageButton", AutoBindAttribute.InitState.NotInit, false)]
        private ButtonEx LanguageButton;
        [AutoBind("./LanguageConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_languageConfirmBoxUIState;
        [AutoBind("./LanguageConfirmPanel/Buttons/UseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_languageConfirmBoxReloginButton;
        [AutoBind("./LanguageConfirmPanel/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_languageConfirmBoxCancelButton;
        [AutoBind("./ButtonAndInput/ServerInfo", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_serverInfoButton;
        [AutoBind("./ButtonAndInput/ServerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_serverNameText;
        [AutoBind("./ButtonAndInput/ServerInfo/StateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_serverStateText;
        [AutoBind("./ButtonAndInput/ServerInfo/StateText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_serverStateGo;
        [AutoBind("./ButtonAndInput/ServerInfo/StateText/PointBgImage/PointImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_serverStateImageStateCtrl;
        [AutoBind("./LoginFailedMask", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_maskBtn;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetButtonGroupStates;
        private static DelegateBridge __Hotfix_SwitchMode;
        private static DelegateBridge __Hotfix_OutputDebugMsg;
        private static DelegateBridge __Hotfix_EnableLoadingEffect;
        private static DelegateBridge __Hotfix_ShowNetworkErrorMsgBox;
        private static DelegateBridge __Hotfix_SetVersionText;
        private static DelegateBridge __Hotfix_GetPasswordInput;
        private static DelegateBridge __Hotfix_GetAccountInput;
        private static DelegateBridge __Hotfix_SetAccountInput;
        private static DelegateBridge __Hotfix_SetServerInMaintenance;
        private static DelegateBridge __Hotfix_GetServerIp;
        private static DelegateBridge __Hotfix_GetServerPort;
        private static DelegateBridge __Hotfix_SetServerIpAndPort;
        private static DelegateBridge __Hotfix_SetEnterGameButtonText;
        private static DelegateBridge __Hotfix_UpdateLoginMessage;
        private static DelegateBridge __Hotfix_UpdateServerInfo;
        private static DelegateBridge __Hotfix_SetSwitchLanguagePanelShowOrHide;
        private static DelegateBridge __Hotfix_SetAnnouncement;
        private static DelegateBridge __Hotfix_SetErrorAnnouncement;
        private static DelegateBridge __Hotfix_SetAnnounce;
        private static DelegateBridge __Hotfix_SetAnnouncementActivity;
        private static DelegateBridge __Hotfix_IsAnnouncementActivity;
        private static DelegateBridge __Hotfix_EnableEnterGameButton;
        private static DelegateBridge __Hotfix_EnableLogoAnimation;
        private static DelegateBridge __Hotfix_OnLogoAnimationFinished;
        private static DelegateBridge __Hotfix_OnEnterGameButtonShow;
        private static DelegateBridge __Hotfix_SetInMaintenanceTextVisble;
        private static DelegateBridge __Hotfix_add_EventOnLogoAnimationFinished;
        private static DelegateBridge __Hotfix_remove_EventOnLogoAnimationFinished;

        public event Action EventOnLogoAnimationFinished
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableEnterGameButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableLoadingEffect(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnableLogoAnimation(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public string GetAccountInput(bool serverListModeInEditor = false)
        {
        }

        [MethodImpl(0x8000)]
        public string GetPasswordInput()
        {
        }

        [MethodImpl(0x8000)]
        public string GetServerIp()
        {
        }

        [MethodImpl(0x8000)]
        public int GetServerPort()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnnouncementActivity()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnEnterGameButtonShow(bool res)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLogoAnimationFinished()
        {
        }

        [MethodImpl(0x8000)]
        public void OutputDebugMsg(string msg)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAccountInput(string loginAccount, bool serverListModeInEditor = false)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAnnounce(List<LoginAnnouncement> announceList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncement(string announceText)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncementActivity(bool isActivity)
        {
        }

        [MethodImpl(0x8000)]
        public void SetButtonGroupStates(bool showCodeScan, bool showLanguage, bool showAnnouncement)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnterGameButtonText(string btTxt)
        {
        }

        [MethodImpl(0x8000)]
        private void SetErrorAnnouncement(string errAnnounceStr)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetInMaintenanceTextVisble(bool visible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetServerInMaintenance(bool isInMaintenance)
        {
        }

        [MethodImpl(0x8000)]
        public void SetServerIpAndPort(string serverIp, int serverPort)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSwitchLanguagePanelShowOrHide(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVersionText(string text)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNetworkErrorMsgBox(string msg = "", Action onConfirm = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchMode(string mode, bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLoginMessage(string msg)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateServerInfo(ServerInfo serverInfo)
        {
        }

        [CompilerGenerated]
        private sealed class <SetAnnounce>c__AnonStorey1
        {
            internal LoginUIController.LoginAnnouncement item;
            internal LoginUIController $this;

            internal void <>m__0(bool isOn)
            {
                if (isOn)
                {
                    this.$this.ANDetailScrollView.set_normalizedPosition(Vector2.one);
                    this.$this.ANDetailContent.text = this.item.Content;
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowNetworkErrorMsgBox>c__AnonStorey0
        {
            internal Action onConfirm;
            internal LoginUIController $this;

            internal void <>m__0(UIControllerBase ctrl)
            {
                if (this.$this.NetWorkNotReachableCtrl != null)
                {
                    this.$this.NetWorkNotReachableCtrl.SetToUIStateExtra("Close", false, false, null);
                }
                this.$this.SwitchMode("HideAll", true);
                if (this.onConfirm != null)
                {
                    this.onConfirm();
                }
            }
        }

        public class LoginAnnouncement
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private string <Title>k__BackingField;
            [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
            private string <Content>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private AnnounceType <CurrentType>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_Title;
            private static DelegateBridge __Hotfix_set_Title;
            private static DelegateBridge __Hotfix_get_Content;
            private static DelegateBridge __Hotfix_set_Content;
            private static DelegateBridge __Hotfix_get_CurrentType;
            private static DelegateBridge __Hotfix_set_CurrentType;

            [MethodImpl(0x8000)]
            public LoginAnnouncement(AnnounceType type, string title, string content)
            {
            }

            public string Title
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public string Content
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public AnnounceType CurrentType
            {
                [MethodImpl(0x8000), CompilerGenerated]
                get
                {
                }
                [MethodImpl(0x8000), CompilerGenerated]
                set
                {
                }
            }

            public enum AnnounceType
            {
                Notice = 1,
                Activity = 2,
                None = 3
            }
        }
    }
}

