﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.EventSystems;

    public class AuctionStoreUITask : UITaskBase
    {
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public static string ParamKeyReShelveItem;
        public static string ParamKeyUseCache;
        public const string ParamKeyBackgroundManager = "BackgroundManager";
        private IUIBackgroundManager m_backgroundManager;
        private AuctionItemDetailInfo m_reShelveItemFromOutSide;
        private DateTime m_cacheUpdateTime;
        private DateTime m_inShipCacheUpDateTime;
        private bool m_useCache;
        private int m_selectedItemIndex;
        private AuctionStoreUIController m_mainCtrl;
        private ItemStoreDefaultUIFilter m_itemStoreFilter;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private bool m_enterItemDetailUITask;
        private List<ILBStoreItemClient> m_cachedStoreItemList;
        private List<int> m_periodList;
        private int m_curPeriodIndex;
        private float m_sellCountPressStartTime;
        private float m_sellCountNextTriggerTime;
        private const float m_sellCountPressInterval = 1f;
        private float m_sellPricePressStartTime;
        private float m_sellPriceNextTriggerTime;
        private const float m_sellPricePressInterval = 1f;
        private const float m_growthRate = 1.02f;
        private const float m_ReductionRate = 0.98f;
        private const float m_twoSeconds = 2f;
        private const int m_zeroToTwoSecondCountChange = 1;
        private const float m_fourSeconds = 4f;
        private const int m_twoToFourSecondCountChange = 10;
        private const float m_sixSeconds = 6f;
        private const int m_fourToSixSecondCountChange = 100;
        private const int m_afterSixSecondCountChange = 0x3e8;
        private const float m_longPressingTriggerTimeGap = 0.1f;
        private const float m_longPressThreshhold = 0.2f;
        public const string TaskName = "AuctionStoreUITask";
        [CompilerGenerated]
        private static Comparison<ILBStoreItemClient> <>f__mg$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTransactionStoreUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_CheckItemStoreUpdateTime;
        private static DelegateBridge __Hotfix_CheckShipCompItemUpdateTime;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectItemDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateItemListUI;
        private static DelegateBridge __Hotfix_UpdateSelectedItemSingleInfoPanel;
        private static DelegateBridge __Hotfix_UpdatePlayerMoney;
        private static DelegateBridge __Hotfix_ClearSelect;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_ShowDetailInfoForSelectedItem;
        private static DelegateBridge __Hotfix_CheckIsUnReshelveItem;
        private static DelegateBridge __Hotfix_ItemListUITabChangedImp;
        private static DelegateBridge __Hotfix_ItemListClickImp;
        private static DelegateBridge __Hotfix_Item3DTouchImp;
        private static DelegateBridge __Hotfix_InitSellData;
        private static DelegateBridge __Hotfix_InitSellDataFromReshelveItem;
        private static DelegateBridge __Hotfix_CheckAuctionParameter;
        private static DelegateBridge __Hotfix_SendAuctionItemInfoReq_0;
        private static DelegateBridge __Hotfix_SendAuctionItemInfoReq_1;
        private static DelegateBridge __Hotfix_GetTipUIProcess;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnItemListUITabChanged;
        private static DelegateBridge __Hotfix_OnItemListClick;
        private static DelegateBridge __Hotfix_OnItem3DTouch;
        private static DelegateBridge __Hotfix_OnTradeinformationButtonClick;
        private static DelegateBridge __Hotfix_OnSellCountAddClick;
        private static DelegateBridge __Hotfix_OnSellCountReduceClick;
        private static DelegateBridge __Hotfix_OnSellButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnSellButtonLongPressing;
        private static DelegateBridge __Hotfix_OnPriceButtonLongPressStart;
        private static DelegateBridge __Hotfix_OnPriceButtonLongPressing;
        private static DelegateBridge __Hotfix_OnSellPriceAddClick;
        private static DelegateBridge __Hotfix_OnSellPriceReduceClick;
        private static DelegateBridge __Hotfix_OnLimitDateAddClick;
        private static DelegateBridge __Hotfix_OnLimitDateReduceClick;
        private static DelegateBridge __Hotfix_OnItemIconButtonClick;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_PauseWhenEnterItemDetailUITask;
        private static DelegateBridge __Hotfix_OnPriceHintButtonClick;
        private static DelegateBridge __Hotfix_OnPeriodHintButtonClick;
        private static DelegateBridge __Hotfix_OnCustodyHintButtonClick;
        private static DelegateBridge __Hotfix_OnTodayHintButtonClick;
        private static DelegateBridge __Hotfix_OnFreezingButtonClick;
        private static DelegateBridge __Hotfix_OnHintBackGroundButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public AuctionStoreUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckAuctionParameter()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckIsUnReshelveItem(Action onConfirm = null, Action onCancel = null)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckItemStoreUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipCompItemUpdateTime()
        {
        }

        [MethodImpl(0x8000)]
        private void ClearSelect()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void CollectItemDynamicResForLoad(List<string> resList, ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetTipUIProcess(string state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSellData(ILBStoreItemClient ItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void InitSellDataFromReshelveItem(ConfigDataAuctionItemInfo auctionItemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void Item3DTouchImp(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void ItemListClickImp(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void ItemListUITabChangedImp(ItemStoreListUIController.ItemStoreTabType itemTabType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCloseButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCustodyHintButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFreezingButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHintBackGroundButtonClick(PointerEventData pointerEventData, Action<PointerEventData> passEvent)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItem3DTouch(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemIconButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUITabChanged(ItemStoreListUIController.ItemStoreTabType itemTabType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLimitDateAddClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLimitDateReduceClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPeriodHintButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPriceButtonLongPressing(bool isAdd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPriceButtonLongPressStart()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPriceHintButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellButtonLongPressing(bool isAdd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellButtonLongPressStart()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellCountAddClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellCountReduceClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellPriceAddClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSellPriceReduceClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTodayHintButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeinformationButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void PauseWhenEnterItemDetailUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void SendAuctionItemInfoReq(ILBStoreItemClient lbItem)
        {
        }

        [MethodImpl(0x8000)]
        private void SendAuctionItemInfoReq(int itemID)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDetailInfoForSelectedItem(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTransactionStoreUITask(UIIntent returnToIntent, AuctionItemDetailInfo reShelveItem = null, string mode = "", Action<bool> onPipeLineEnd = null, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemListUI()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePlayerMoney()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSelectedItemSingleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CheckIsUnReshelveItem>c__AnonStorey0
        {
            internal Action onConfirm;
            internal AuctionStoreUITask $this;

            internal void <>m__0()
            {
                AuctionItemCallBackReqNetTask task = new AuctionItemCallBackReqNetTask(this.$this.m_reShelveItemFromOutSide.m_auctionItemInsId);
                task.EventOnStop += delegate (Task task) {
                    if (!((AuctionItemCallBackReqNetTask) task).IsNetworkError)
                    {
                        int result = ((AuctionItemCallBackReqNetTask) task).Result;
                        if (result != 0)
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        }
                        else
                        {
                            this.$this.m_reShelveItemFromOutSide = null;
                            if (this.onConfirm != null)
                            {
                                this.onConfirm();
                            }
                        }
                    }
                };
                task.Start(null, null);
            }

            internal void <>m__1(Task task)
            {
                if (!((AuctionItemCallBackReqNetTask) task).IsNetworkError)
                {
                    int result = ((AuctionItemCallBackReqNetTask) task).Result;
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                    }
                    else
                    {
                        this.$this.m_reShelveItemFromOutSide = null;
                        if (this.onConfirm != null)
                        {
                            this.onConfirm();
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <InitSellDataFromReshelveItem>c__AnonStorey1
        {
            internal ConfigDataAuctionItemInfo auctionItemInfo;

            internal bool <>m__0(ILBStoreItemClient item)
            {
                int num1;
                StoreItemType itemType = item.GetItemType();
                int itemConfigId = ClientStoreItemBaseHelper.GetItemConfigId(itemType, item.GetConfigInfo<object>());
                if (item.IsFreezing() || (itemType != this.auctionItemInfo.ItemType))
                {
                    num1 = 0;
                }
                else
                {
                    num1 = (int) (itemConfigId == this.auctionItemInfo.ItemId);
                }
                return (bool) num1;
            }
        }

        [CompilerGenerated]
        private sealed class <OnItem3DTouch>c__AnonStorey4
        {
            internal int idx;
            internal AuctionStoreUITask $this;

            internal void <>m__0()
            {
                this.$this.Item3DTouchImp(this.idx);
            }
        }

        [CompilerGenerated]
        private sealed class <OnItemListClick>c__AnonStorey3
        {
            internal int idx;
            internal AuctionStoreUITask $this;

            internal void <>m__0()
            {
                this.$this.ItemListClickImp(this.idx);
            }
        }

        [CompilerGenerated]
        private sealed class <OnItemListUITabChanged>c__AnonStorey2
        {
            internal ItemStoreListUIController.ItemStoreTabType itemTabType;
            internal AuctionStoreUITask $this;

            internal void <>m__0()
            {
                this.$this.ItemListUITabChangedImp(this.itemTabType);
            }

            internal void <>m__1()
            {
                this.$this.m_mainCtrl.m_itemStoreListUIController.UpdateToggleState(this.$this.m_itemStoreFilter.CurrItemTabType, false);
            }
        }

        [CompilerGenerated]
        private sealed class <OnTradeinformationButtonClick>c__AnonStorey5
        {
            internal string itemName;
            internal AuctionStoreUITask $this;

            internal void <>m__0(Task task)
            {
                AuctionItemOnShelveReqNetworkTask task2 = task as AuctionItemOnShelveReqNetworkTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.Result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    }
                    else
                    {
                        this.$this.m_reShelveItemFromOutSide = null;
                        this.$this.ClearSelect();
                        TipWindowUITask.ShowTipWindow(string.Format(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_AuctionItemOnShelveSuccess, new object[0]), this.itemName), false);
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsRefreshAllItem,
            IsRefreshSingleItem,
            IsItemFilterChanged
        }
    }
}

