﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class HangarShipAssemblyOptimizationInfoPanelUIController : UIControllerBase
    {
        private readonly List<HangarShipAssemblyOptimizationInfoItemUIController> m_itemInfoUICtrlList;
        [AutoBind("./FrameImage/InfoItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_infoItemGroup;
        [AutoBind("./FrameImage/InfoItemGroup/InfoElement", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_infoElement;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backGroundButton;
        private static DelegateBridge __Hotfix_UpdatePanelInformation;
        private static DelegateBridge __Hotfix_UpdateAllItemInformation;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAllItemInformation(List<ItemSupportInfo> infos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePanelInformation(ILBStaticPlayerShip playerShip, ShipHangarUITask.ShipAssemblyOptimizationInfo optimizationInfo)
        {
        }

        public enum AssemblyOptimizationInfoType
        {
            None,
            WeaponItem,
            EquipItem
        }

        public class ItemSupportInfo
        {
            public string m_titleText;
            public HangarShipAssemblyOptimizationInfoPanelUIController.AssemblyOptimizationInfoType m_type;
            public List<string> m_itemTextList = new List<string>();
            public List<string> m_descTextList = new List<string>();
            private static DelegateBridge _c__Hotfix_ctor;

            public ItemSupportInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

