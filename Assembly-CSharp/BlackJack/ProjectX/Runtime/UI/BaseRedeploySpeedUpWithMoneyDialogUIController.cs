﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class BaseRedeploySpeedUpWithMoneyDialogUIController : BaseRedeployDialogUIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRedeployTimeOut;
        private MSRedeployInfo m_redeployInfo;
        private bool m_needTriggerRedeployTimeOutEvent;
        [AutoBind("./DetailInfo/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./DetailInfo/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./DetailInfo/RedeployInfo/RedeployPrepareGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_redeployRemainingTimeText;
        [AutoBind("./DetailInfo/RedeployInfo/ProgressBar/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_redeployProgressBarImage;
        [AutoBind("./DetailInfo/TextGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_finishRedeployCostMoneyText;
        private static DelegateBridge __Hotfix_SetRedeployBaseInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_TickForRedeployInfoToUI;
        private static DelegateBridge __Hotfix_get_CancelButtonName;
        private static DelegateBridge __Hotfix_get_ConfirmButtonName;
        private static DelegateBridge __Hotfix_SetRedeployTimeInfo;
        private static DelegateBridge __Hotfix_SetRedeployCostMoneyInfo;
        private static DelegateBridge __Hotfix_add_EventOnRedeployTimeOut;
        private static DelegateBridge __Hotfix_remove_EventOnRedeployTimeOut;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action EventOnRedeployTimeOut
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void SetRedeployBaseInfo(MSRedeployInfo redeployInfo, int moneyCost)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRedeployCostMoneyInfo(int moneyCost)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRedeployTimeInfo(DateTime startTime, DateTime endTime, DateTime originalEndTime)
        {
        }

        [MethodImpl(0x8000)]
        private void TickForRedeployInfoToUI()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        protected override string CancelButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override string ConfirmButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

