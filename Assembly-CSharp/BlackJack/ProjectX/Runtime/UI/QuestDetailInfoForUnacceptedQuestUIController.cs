﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class QuestDetailInfoForUnacceptedQuestUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool, bool> EventOnLossWarningButtonClick;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnacceptedQuestDetail;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UnacceptedQuestPanelCtrl;
        [AutoBind("./QuestTitleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image UnacceptedQuestTitleImage;
        [AutoBind("./ContactButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ContactButtonStateCtrl;
        [AutoBind("./ContactButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ContactButton;
        [AutoBind("./ContactButton/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ContactButtonText;
        [AutoBind("./TitleIcon/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnacceptedQuestTitleText;
        [AutoBind("./DetailImageGroup/NPCImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image UnacceptedQuestNpcImage;
        [AutoBind("./QuestNameText/QuestLevelGroup/QuestLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnacceptedQuestLevelText;
        [AutoBind("./QuestNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnacceptedQuestNameText;
        [AutoBind("./ContactButton/AcceptableConditionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnacceptedConditionText;
        [AutoBind("./DetailTextGroup/CompleteCondRoot/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnaccpetedQuestDescText;
        [AutoBind("./DistanceGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestGalaxyNameText;
        [AutoBind("./DistanceGroup/DistanceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DistanceValueText;
        [AutoBind("./DistanceGroup", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx StarMapButton;
        [AutoBind("./DistanceGroup/StarMapButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StarMapButtonImage;
        [AutoBind("./DetailImageGroup/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LossWarningButton;
        [AutoBind("./LossWarningDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform LossWarningDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetUnacceptedQuestDetailPanelShowOrHideProcess;
        private static DelegateBridge __Hotfix_SetSelectedUnacceptedQuestInfo;
        private static DelegateBridge __Hotfix_SetStarMapButtonState;
        private static DelegateBridge __Hotfix_GetLossWarningWindowDummyPos;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;

        public event Action<bool, bool, bool> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLossWarningWindowDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetUnacceptedQuestDetailPanelShowOrHideProcess(bool isShow, bool isImmediate)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedUnacceptedQuestInfo(ConfigDataQuestInfo confQuest, Dictionary<string, UnityEngine.Object> resDic, int level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarMapButtonState()
        {
        }
    }
}

