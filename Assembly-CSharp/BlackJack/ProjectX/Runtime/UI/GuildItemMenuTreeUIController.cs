﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildItemMenuTreeUIController : CommonMenuRootUIController
    {
        private Dictionary<string, UnityEngine.Object> m_assetDict;
        private List<GuildItemListUIController.GuildStoreTabType> m_guildItemTabTypeList;
        private Dictionary<GuildItemListUIController.GuildStoreTabType, List<GuildItemTypeMenuItemData>> m_tabType2ItemTypeDic;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonMenuItemUIController> EventOnMenuItemClick;
        public readonly int m_menuItemCategoryLevel;
        public readonly int m_menuItemTypeLevel;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup GuildItemMenuTreeToggleGroup;
        [AutoBind("./UnusedMenuItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform UnusedMenuItemRoot;
        private static DelegateBridge __Hotfix_CreateAllToggle;
        private static DelegateBridge __Hotfix_GetGuildItemCategoryMenuItemUIControllerByType;
        private static DelegateBridge __Hotfix_UpdateMenuItemCategorySelectedState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetChildMenuItemAssetName;
        private static DelegateBridge __Hotfix_OnMenuItemClick;
        private static DelegateBridge __Hotfix_add_EventOnMenuItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMenuItemClick;

        public event Action<CommonMenuItemUIController> EventOnMenuItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CreateAllToggle(Dictionary<GuildItemListUIController.GuildStoreTabType, List<GuildItemTypeMenuItemData>> category2ItemTypeDic, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        protected override string GetChildMenuItemAssetName()
        {
        }

        [MethodImpl(0x8000)]
        public GuildItemCategoryMenuItemUIController GetGuildItemCategoryMenuItemUIControllerByType(GuildItemListUIController.GuildStoreTabType tab)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnMenuItemClick(CommonMenuItemUIController menuCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateMenuItemCategorySelectedState(GuildItemListUIController.GuildStoreTabType lastTab, bool isStoreMode, bool hasPermission)
        {
        }
    }
}

