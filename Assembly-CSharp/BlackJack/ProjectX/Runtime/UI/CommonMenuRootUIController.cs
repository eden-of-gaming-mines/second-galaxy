﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonMenuRootUIController : CommonMenuItemUIController
    {
        protected CommonMenuItemUIController.CommonMenuTreeInfo m_initMenuTreeInfo;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private ToggleGroup <LeafMenuItemToggleGroup>k__BackingField;
        private static DelegateBridge __Hotfix_InitRootInfo;
        private static DelegateBridge __Hotfix_GetMenuTreeInfo;
        private static DelegateBridge __Hotfix_GenerateDataList;
        private static DelegateBridge __Hotfix_get_LeafMenuItemToggleGroup;
        private static DelegateBridge __Hotfix_set_LeafMenuItemToggleGroup;

        [MethodImpl(0x8000)]
        protected List<object> GenerateDataList(IEnumerable dataSrcList)
        {
        }

        [MethodImpl(0x8000)]
        protected override CommonMenuItemUIController.CommonMenuTreeInfo GetMenuTreeInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void InitRootInfo(Transform unusedMenuItemRoot, int leafItemLevel, ToggleGroup toggleGroup, Action<CommonMenuItemUIController> onMenuItemClick)
        {
        }

        protected ToggleGroup LeafMenuItemToggleGroup
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

