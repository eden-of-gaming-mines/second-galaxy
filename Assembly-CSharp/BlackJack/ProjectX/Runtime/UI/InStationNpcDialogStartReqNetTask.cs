﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class InStationNpcDialogStartReqNetTask : NetWorkTransactionTask
    {
        private int m_npcId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <StartNpcDialogReqResult>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ReturnNpcId>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private NpcDialogInfo <DialogInfo>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnNpcDialogStart;
        private static DelegateBridge __Hotfix_set_StartNpcDialogReqResult;
        private static DelegateBridge __Hotfix_get_StartNpcDialogReqResult;
        private static DelegateBridge __Hotfix_set_ReturnNpcId;
        private static DelegateBridge __Hotfix_get_ReturnNpcId;
        private static DelegateBridge __Hotfix_set_DialogInfo;
        private static DelegateBridge __Hotfix_get_DialogInfo;

        [MethodImpl(0x8000)]
        public InStationNpcDialogStartReqNetTask(int npcId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNpcDialogStart(int result, int npcId, NpcDialogInfo dlgInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int StartNpcDialogReqResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int ReturnNpcId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public NpcDialogInfo DialogInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

