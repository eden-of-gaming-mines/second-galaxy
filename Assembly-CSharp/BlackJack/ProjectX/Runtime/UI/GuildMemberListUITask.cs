﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildMemberListUITask : GuildUITaskBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnUpdateMemberCount;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, GuildMemberInfo> EventOnMemberClick;
        private GuildMemberListUIController m_memberListCtrl;
        private List<GuildMemberInfo> m_members;
        private uint m_memberInfoVersion;
        private bool m_isMemberVersionChanged;
        private bool m_pauseTaskOnPostUpdateView;
        private GuildMemberInfo[] m_mostContributeThree;
        private UITaskBase.LayerDesc[] layer;
        private UITaskBase.UIControllerDesc[] ctrl;
        public const string TaskName = "GuildMemberListUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask_1;
        private static DelegateBridge __Hotfix_StartTask_0;
        private static DelegateBridge __Hotfix_PlayShowProcess;
        private static DelegateBridge __Hotfix_ExpelGuildSucess;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterCtrlEvent;
        private static DelegateBridge __Hotfix_UnregisterCtrlEvent;
        private static DelegateBridge __Hotfix_RegisterPlayerEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayerEvent;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_RequestMemberListInfo;
        private static DelegateBridge __Hotfix_OnGuildMemberJobUpdateNtf;
        private static DelegateBridge __Hotfix_OnJobSortButtonClick;
        private static DelegateBridge __Hotfix_OnGradeSortButtonClick;
        private static DelegateBridge __Hotfix_OnGalaxySortButtonClick;
        private static DelegateBridge __Hotfix_OnContributionSortButtonClick;
        private static DelegateBridge __Hotfix_OnLastOnlineSortButtonClick;
        private static DelegateBridge __Hotfix_OnSortButtonClick;
        private static DelegateBridge __Hotfix_OnMemberListItemClick;
        private static DelegateBridge __Hotfix_ChangeSortState;
        private static DelegateBridge __Hotfix_IsPlayerSelf;
        private static DelegateBridge __Hotfix_GuildJobSortByID;
        private static DelegateBridge __Hotfix_GetGuildLBClient;
        private static DelegateBridge __Hotfix_add_EventOnUpdateMemberCount;
        private static DelegateBridge __Hotfix_remove_EventOnUpdateMemberCount;
        private static DelegateBridge __Hotfix_add_EventOnMemberClick;
        private static DelegateBridge __Hotfix_remove_EventOnMemberClick;
        private static DelegateBridge __Hotfix_set_MainArrowState;
        private static DelegateBridge __Hotfix_get_MainArrowState;
        private static DelegateBridge __Hotfix_set_MainTitleType;
        private static DelegateBridge __Hotfix_get_MainTitleType;
        private static DelegateBridge __Hotfix_set_GuildMemberFirstActiveIndex;
        private static DelegateBridge __Hotfix_get_GuildMemberFirstActiveIndex;
        private static DelegateBridge __Hotfix_set_EditPlayerUserId;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action<UIControllerBase, GuildMemberInfo> EventOnMemberClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnUpdateMemberCount
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public GuildMemberListUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void ChangeSortState(LogicBlockGuildClient.MemberTitleType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public void ExpelGuildSucess()
        {
        }

        [MethodImpl(0x8000)]
        private LogicBlockGuildClient GetGuildLBClient()
        {
        }

        [MethodImpl(0x8000)]
        private int GuildJobSortByID(GuildJobType lhs, GuildJobType rhs)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPlayerSelf(GuildMemberInfo member)
        {
        }

        [MethodImpl(0x8000)]
        private void OnContributionSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGalaxySortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGradeSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildMemberJobUpdateNtf(GuildMemberJobUpdateNtf msg)
        {
        }

        [MethodImpl(0x8000)]
        private void OnJobSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLastOnlineSortButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberListItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortButtonClick(LogicBlockGuildClient.MemberTitleType type)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void PlayShowProcess(bool show, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCtrlEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPlayerEvent()
        {
        }

        [MethodImpl(0x8000)]
        public static void RequestMemberListInfo(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberListUITask StartTask(Action loadResCompeted)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterCtrlEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterPlayerEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockGuildClient.MemberTitleArrowState MainArrowState
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private LogicBlockGuildClient.MemberTitleType MainTitleType
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private int GuildMemberFirstActiveIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private string EditPlayerUserId
        {
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PlayShowProcess>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            internal void <>m__0(UIProcess pro, bool ret)
            {
                if (this.onEnd != null)
                {
                    this.onEnd(ret);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <RequestMemberListInfo>c__AnonStorey2
        {
            internal Action<bool> onPrepareEnd;

            internal void <>m__0(Task task)
            {
                GuildMemberListInfoReqNetTask task2 = task as GuildMemberListInfoReqNetTask;
                if (!task2.IsNetworkError && (task2 != null))
                {
                    if (task2.Result == 0)
                    {
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(false);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCache>c__AnonStorey1
        {
            internal LogicBlockGuildClient guildClient;

            internal int <>m__0(GuildMemberInfo lhs, GuildMemberInfo rhs) => 
                this.guildClient.CompareMember(lhs, rhs, LogicBlockGuildClient.MemberTitleType.Contribution, LogicBlockGuildClient.MemberTitleArrowState.Down);
        }

        public enum MemberTitleState
        {
            Empty,
            Up,
            Down
        }

        public class OutData
        {
            public GuildJobType mainSortJob = GuildJobType.GuildJobType_Leader;
            public LogicBlockGuildClient.MemberTitleArrowState mainSortJobState = LogicBlockGuildClient.MemberTitleArrowState.Down;
            public int firstItemIndex;
            public bool selfAtStart;
            private static DelegateBridge _c__Hotfix_ctor;

            public OutData()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }

        public enum PipeLineMaskType
        {
            ChangeToggleType,
            MemberRemove,
            Sort,
            ComeFromJob,
            JobChange
        }
    }
}

