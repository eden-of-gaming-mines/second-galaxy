﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ActivityToggleListUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ActivityInfo> EventOnActivityToggleSelected;
        protected List<ActivityToggleUIController> m_toggleCtrlList;
        protected GameObject m_itemTemplateObj;
        public const string ActivityItemPrefabName = "ActivityItemPrefab";
        [AutoBind("./Viewport/Content/", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleGroup ItemToggleGroup;
        [AutoBind("./Viewport/Content/DailyListGroup/DailyList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DailyActivityRoot;
        [AutoBind("./Viewport/Content/TimeLimitListGroup/TimeLimitList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TimeLimitActivityRoot;
        [AutoBind("./Viewport/Content/UnUsedRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UnUsedRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CreateAllActivityToggle;
        private static DelegateBridge __Hotfix_SetToggleSelected;
        private static DelegateBridge __Hotfix_UpdateAcivityItemListInfo;
        private static DelegateBridge __Hotfix_OnActivityToggleSelected;
        private static DelegateBridge __Hotfix_GetActivityTransformByIndex;
        private static DelegateBridge __Hotfix_add_EventOnActivityToggleSelected;
        private static DelegateBridge __Hotfix_remove_EventOnActivityToggleSelected;

        public event Action<ActivityInfo> EventOnActivityToggleSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CreateAllActivityToggle(List<ActivityInfo> activityList)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetActivityTransformByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnActivityToggleSelected(ActivityInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleSelected(int index = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateAcivityItemListInfo(List<ActivityInfo> activityList, List<PlayerActivityInfo> playActivityInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

