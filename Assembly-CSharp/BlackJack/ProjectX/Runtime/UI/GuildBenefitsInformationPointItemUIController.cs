﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildBenefitsInformationPointItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private string <StateName>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private object <ItemInfo>k__BackingField;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemStateCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemClickButton;
        [AutoBind("./RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemRedPointGo;
        [AutoBind("./RewardIcon/BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemSubRankLevelStateCtrl;
        [AutoBind("./RewardIcon/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_itemIconImage;
        [AutoBind("./RewardIcon/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemCountText;
        [AutoBind("./RewardIcon/RewardText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_itemthresholdValue;
        private static DelegateBridge __Hotfix_UpdateInformationPointItem_1;
        private static DelegateBridge __Hotfix_UpdateInformationPointItem_0;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_get_StateName;
        private static DelegateBridge __Hotfix_set_StateName;
        private static DelegateBridge __Hotfix_get_ItemInfo;
        private static DelegateBridge __Hotfix_set_ItemInfo;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateInformationPointItem(string state, ConfigDataGuildBenefitsConfigInfo defaultInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateInformationPointItem(string state, GuildInformationPointBenefitsInfo informationPointInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public string StateName
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public object ItemInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

