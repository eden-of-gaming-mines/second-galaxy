﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class StarMapGuildOccupyInfoMultiReqNetTask : NetWorkTransactionTask
    {
        private readonly HashSet<int> m_starFieldIdSet;
        private bool m_isDataChange;
        private int m_processedCount;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnStarMapGuildOccupyInfoAck;
        private static DelegateBridge __Hotfix_get_IsDataChange;

        [MethodImpl(0x8000)]
        public StarMapGuildOccupyInfoMultiReqNetTask(HashSet<int> starFieldIdSet, bool needBlockGlobalUI = false)
        {
        }

        [MethodImpl(0x8000)]
        private void OnStarMapGuildOccupyInfoAck(int result, int starFieldId, bool isDataChange)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public bool IsDataChange
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

