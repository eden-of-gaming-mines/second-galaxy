﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NewShipGetUIController : UIControllerBase
    {
        public Camera UICamera;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnHighSlotButtonClick;
        protected CommonShipRadarChartUIController m_shipRadarChartCtrl;
        private CommonItemIconUIController m_shipUICtrl;
        private string m_shipAssetName;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./ItemModelViewBG", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemModelViewBG;
        [AutoBind("./ShipShowPanel/ShipRadarChart/ShipGroup/ShipTypeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipTypeText;
        [AutoBind("./ShipShowPanel/ShipRadarChart/ShipGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipNameText;
        [AutoBind("./ShipShowPanel/ShipRadarChart/ShipGroup/RankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ShipTechImage;
        [AutoBind("./ShipShowPanel/ShipRadarChart/ShipGroup/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ShipDummy;
        [AutoBind("./ShipShowPanel/ShipRadarChart", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipRadarChart;
        [AutoBind("./ShipDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipDescText;
        [AutoBind("./SimpleInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SimpleInfoDummy;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitUICamera;
        private static DelegateBridge __Hotfix_ShowNewShipInfo_0;
        private static DelegateBridge __Hotfix_ShowNewShipInfo_1;
        private static DelegateBridge __Hotfix_Get3DModelViewScreenRect;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_OnHighSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnHighSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnHighSlotButtonClick;

        public event Action<int> EventOnHighSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public Rect Get3DModelViewScreenRect()
        {
        }

        [MethodImpl(0x8000)]
        public void InitUICamera(Camera camera)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHighSlotButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewShipInfo(ILBStaticPlayerShip newShip, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNewShipInfo(int shipConfigId, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

