﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProduceQueueUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBProductionLine, int> EventOnSelfProduceQueueItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBProductionLine> EventOnSelfProduceQueueItemComplete;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<GuildProductionLineInfo, int> EventOnGuildProduceQueueItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<GuildProductionLineInfo> EventOnGuildProduceQueueItemComplete;
        public GameObject m_itemObjectForSelf;
        public List<LBProductionLine> m_currProduceItemListForSelf;
        public List<ProduceQueueItemUIController> m_queueItemCtrlListForSelf;
        protected const int MinPipeLineCountforSelf = 5;
        public GameObject m_itemObjectForGuild;
        public List<GuildProductionLineInfo> m_currProduceItemListForGuild;
        public List<GuildProduceQueueItemUIController> m_queueItemCtrlListForGuild;
        protected const int UnLockPipeLineShowCountforGuild = 1;
        [AutoBind("./ToggleScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect ScrollView;
        [AutoBind("./ToggleScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Content;
        private bool m_isGuildProduceMode;
        private static DelegateBridge __Hotfix_InitForSelfProduceMode;
        private static DelegateBridge __Hotfix_InitForGuildProduceMode;
        private static DelegateBridge __Hotfix_UpdateProduceQueueInfoForSelf;
        private static DelegateBridge __Hotfix_AutoCalibrationButtonSelectedForSelf;
        private static DelegateBridge __Hotfix_GetFirstEmptyProduceLineRect;
        private static DelegateBridge __Hotfix_UpdateProduceQueueInfoForGuild;
        private static DelegateBridge __Hotfix_AutoCalibrationButtonSelectedForGuild;
        private static DelegateBridge __Hotfix_OnSelfProduceQueueItemClick;
        private static DelegateBridge __Hotfix_OnSelfProduceQueueItemComplete;
        private static DelegateBridge __Hotfix_OnGuildProduceQueueItemClick;
        private static DelegateBridge __Hotfix_OnGuildProduceQueueItemComplete;
        private static DelegateBridge __Hotfix_add_EventOnSelfProduceQueueItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnSelfProduceQueueItemClick;
        private static DelegateBridge __Hotfix_add_EventOnSelfProduceQueueItemComplete;
        private static DelegateBridge __Hotfix_remove_EventOnSelfProduceQueueItemComplete;
        private static DelegateBridge __Hotfix_add_EventOnGuildProduceQueueItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnGuildProduceQueueItemClick;
        private static DelegateBridge __Hotfix_add_EventOnGuildProduceQueueItemComplete;
        private static DelegateBridge __Hotfix_remove_EventOnGuildProduceQueueItemComplete;

        public event Action<GuildProductionLineInfo, int> EventOnGuildProduceQueueItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<GuildProductionLineInfo> EventOnGuildProduceQueueItemComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProductionLine, int> EventOnSelfProduceQueueItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBProductionLine> EventOnSelfProduceQueueItemComplete
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void AutoCalibrationButtonSelectedForGuild(GuildProductionLineInfo selectedProduceLine)
        {
        }

        [MethodImpl(0x8000)]
        public void AutoCalibrationButtonSelectedForSelf(LBProductionLine selectedProduceLine)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstEmptyProduceLineRect()
        {
        }

        [MethodImpl(0x8000)]
        public void InitForGuildProduceMode()
        {
        }

        [MethodImpl(0x8000)]
        public void InitForSelfProduceMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildProduceQueueItemClick(GuildProductionLineInfo produceLine, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildProduceQueueItemComplete(GuildProductionLineInfo produceLine)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSelfProduceQueueItemClick(LBProductionLine produceLine, int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnSelfProduceQueueItemComplete(LBProductionLine produceLine)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProduceQueueInfoForGuild(List<GuildProductionLineInfo> guildProduceLineList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProduceQueueInfoForSelf(List<LBProductionLine> produceLineList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

