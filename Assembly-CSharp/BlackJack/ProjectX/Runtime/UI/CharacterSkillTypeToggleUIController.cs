﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterSkillTypeToggleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnSkillTypeToggleValueChanged;
        public int m_skillType;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SkillTypeToggle;
        [AutoBind("./SkillNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SkillNameText;
        [AutoBind("./RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RedPoint;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetSkillType;
        private static DelegateBridge __Hotfix_SetSkillTypeNameText;
        private static DelegateBridge __Hotfix_RegSkillTypeToggleEvent;
        private static DelegateBridge __Hotfix_SetToggleIsOn;
        private static DelegateBridge __Hotfix_ShowRedPoint;
        private static DelegateBridge __Hotfix_OnSkillTypeToggleValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnSkillTypeToggleValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnSkillTypeToggleValueChanged;

        public event Action<int> EventOnSkillTypeToggleValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnSkillTypeToggleValueChanged(UIControllerBase uiCtrl, bool isSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void RegSkillTypeToggleEvent(Action<int> onSkillToggleSelected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillType(int skillType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSkillTypeNameText(string skillName)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleIsOn()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRedPoint(bool show)
        {
        }
    }
}

