﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class StarMapJumpTargetUIController : UIControllerBase
    {
        public ulong m_tunnelId;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnSelected;
        [AutoBind("./StarText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starName;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_driverName;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx m_toggle;
        [AutoBind("./TargetType", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_typeIcon;
        private const string StatTunnelBuilding = "TunnelBuilding";
        private const string StatFlagShip = "FlagShip";
        private const string StatRandom = "Random";
        private static DelegateBridge __Hotfix_SetInfo;
        private static DelegateBridge __Hotfix_SetSolarSystemId;
        private static DelegateBridge __Hotfix_OnSelectedChanged;
        private static DelegateBridge __Hotfix_add_EventOnSelected;
        private static DelegateBridge __Hotfix_remove_EventOnSelected;

        public event Action<ulong> EventOnSelected
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void OnSelectedChanged(bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInfo(GuildTeleportTunnelEffectInfoClient info, bool selected)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSolarSystemId(int solarSystemId)
        {
        }
    }
}

