﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SolarSystemFunButtonsUIController : UIControllerBase
    {
        private string m_ViewModeState_View;
        private string m_ViewModeState_Play;
        private string m_recordAnimation_Show;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnSoundButtonDown;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnSoundButtonUp;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnDropItemSelect;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnChatPanelButtonClick;
        protected int m_actionCount;
        private const string emojiInfoTxtName = "EmojiInfo";
        private const string emojiShowAssetName = "EmojiShowImage";
        private EmojiParseController m_emojiParseHelper;
        public EventTriggerListener MicTrigger;
        [AutoBind("./MenuChatViewModeButton/Chat/ChatMicToggle/StarrDropdown", AutoBindAttribute.InitState.NotInit, false)]
        public Dropdown ChatChannelDropDown;
        [AutoBind("./MenuChatViewModeButton/Chat", AutoBindAttribute.InitState.NotInit, false)]
        public EmojiParseDesc m_emojiParseDesc;
        [AutoBindWithParent("../SendSpeekMessageState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RecordAnimatUIStateCtrl;
        [AutoBindWithParent("../CanvasMask/FunButtonsPanel/MenuChatViewModeButton/Chat/ChatDetails/ScrollView/UnUsedChildRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform UnUsedChildRoot;
        [AutoBindWithParent("../CanvasMask/FunButtonsPanel/MenuChatViewModeButton/Chat", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ChatPanelButton;
        [AutoBind("./MenuChatViewModeButton/Chat", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ChatPanelStateCtrl;
        [AutoBind("./MenuChatViewModeButton/Chat/ChatMicToggle/TeamToggle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MicObj;
        [AutoBindWithParent("../CanvasMask/FunButtonsPanel/MenuChatViewModeButton/Chat/ChatDetails/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect ScrollView;
        [AutoBind("./ViewModeButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ViewModeButtonRoot;
        [AutoBind("./ViewModeButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ViewModeUIState;
        [AutoBind("./ViewModeButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ModeChangeButton;
        [AutoBind("./MenuChatViewModeButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MenuChatViewModeButtonGo;
        [AutoBind("./MenuChatViewModeButton/MenuButton", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx MenuToggleButton;
        [AutoBind("./MenuChatViewModeButton/MenuButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController MenuToggleButtonStateCtrl;
        [AutoBind("./MenuChatViewModeButton/MenuButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MenuButtonRedPoint;
        [AutoBind("./MenuChatViewModeButton/QuestButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ActionButton;
        [AutoBind("./MenuChatViewModeButton/QuestButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ActionButtonStateCtrl;
        [AutoBind("./MenuChatViewModeButton/QuestButton/NumberBG", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ActionButtonConerTipStateCtrl;
        [AutoBind("./MenuChatViewModeButton/QuestButton/NumberBG/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text ActionCountText;
        [AutoBind("./MenuChatViewModeButton/DepotButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemStoreToggle;
        [AutoBind("./MenuChatViewModeButton/DepotButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStoreToggleStateCtrl;
        [AutoBind("./MenuPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform MenuPanelDummy;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/MenuChatViewModeButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject VolatileMenuChatViewModeButtonGo;
        [AutoBind("./MenuChatViewModeButton/MenuButton/FightingBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject FightingBgImage;
        [AutoBind("./ScreenShootButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ScreenShootButton;
        [AutoBind("./ScreenShootButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ScreenShootButtonGo;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnWingShipButtonClick;
        private string m_curState;
        public const string WingShipStateStr_Intervene = "Intervene";
        public const string WingShipStateStr_Standby = "Standby";
        public const string WingShipStateStr_NotValid = "CanNotUse";
        public const string WingShipStateStr_SceneNotAllow = "NotAllow";
        public const string WingShipStateStr_CD = "CD";
        [AutoBind("./HiredCaptainButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx WingShipButton;
        [AutoBind("./HiredCaptainButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WingShipButtonStateCtrl;
        [AutoBind("./HiredCaptainButton/ShowAndCloseState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WingShipShowStateCtrl;
        [AutoBind("./HiredCaptainButton/WingShipStateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingShipStateText;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WingmanButtonVolatile;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton/CharacterIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CharacterIconImage;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton/CDProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image WingShipCDProgessImage;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton/CDProgressBarImage/TimeBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text WingShipCDTimeInfoText;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton/HpProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image WingShipHpProgressImage;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton/QualityImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image ProfessionImage;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton/Quality", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController QualityStateCtrl;
        [AutoBindWithParent("../CanvasVolatile/FunButtonsPanel/HiredCaptainButton/InterveneAnimationBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public TweenAlpha IterveneBgTween;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateChatScrollView;
        private static DelegateBridge __Hotfix_SetMenuToggleEnabled;
        private static DelegateBridge __Hotfix_StartRecordAnimation;
        private static DelegateBridge __Hotfix_StopRecordAnimation;
        private static DelegateBridge __Hotfix_ChangeViewModeButtonState;
        private static DelegateBridge __Hotfix_EnableViewModeButton;
        private static DelegateBridge __Hotfix_EnableScreenShootButton;
        private static DelegateBridge __Hotfix_EnableFuncMenuUIPanel;
        private static DelegateBridge __Hotfix_CreateFuncMenuUIPanelUnlockEffectProcess;
        private static DelegateBridge __Hotfix_EnableChatUIPanel;
        private static DelegateBridge __Hotfix_ShowOrHideFunButtonUI;
        private static DelegateBridge __Hotfix_ShowOrHideChatButtonUI;
        private static DelegateBridge __Hotfix_ShowOrHideWingShipButton;
        private static DelegateBridge __Hotfix_GetWingShipButtonVisible;
        private static DelegateBridge __Hotfix_EnableMenuButton;
        private static DelegateBridge __Hotfix_EnableItemStoreButton;
        private static DelegateBridge __Hotfix_EnableQuickActionButton;
        private static DelegateBridge __Hotfix_ShowShipItemStoreGetItemEffect;
        private static DelegateBridge __Hotfix_UpdateActionButtonInfo;
        private static DelegateBridge __Hotfix_SetMenuButtonRedPoint;
        private static DelegateBridge __Hotfix_SetChatScrollViewToEnd;
        private static DelegateBridge __Hotfix_OnChatPanelButtonClick;
        private static DelegateBridge __Hotfix_OnSoundButtonDown;
        private static DelegateBridge __Hotfix_OnSoundButtonUp;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_OnDropDownValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnSoundButtonDown;
        private static DelegateBridge __Hotfix_remove_EventOnSoundButtonDown;
        private static DelegateBridge __Hotfix_add_EventOnSoundButtonUp;
        private static DelegateBridge __Hotfix_remove_EventOnSoundButtonUp;
        private static DelegateBridge __Hotfix_add_EventOnDropItemSelect;
        private static DelegateBridge __Hotfix_remove_EventOnDropItemSelect;
        private static DelegateBridge __Hotfix_add_EventOnChatPanelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnChatPanelButtonClick;
        private static DelegateBridge __Hotfix_SetWingShipButtonIcon;
        private static DelegateBridge __Hotfix_SetWingShipCaptainProfession;
        private static DelegateBridge __Hotfix_SetWingShipSubRankUI;
        private static DelegateBridge __Hotfix_SetWingShipButtonState;
        private static DelegateBridge __Hotfix_GetWingShipButtonState;
        private static DelegateBridge __Hotfix_SetWingShipCDInfo;
        private static DelegateBridge __Hotfix_SetTextVisible;
        private static DelegateBridge __Hotfix_SetWingShipArmorAndShield;
        private static DelegateBridge __Hotfix_SetWingShipButtonImpl;
        private static DelegateBridge __Hotfix_OnWingShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWingShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnWingShipButtonClick;

        public event Action EventOnChatPanelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnDropItemSelect
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSoundButtonDown
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnSoundButtonUp
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWingShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ChangeViewModeButtonState(bool isPlayMode)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateFuncMenuUIPanelUnlockEffectProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableChatUIPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableFuncMenuUIPanel(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableItemStoreButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableMenuButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableQuickActionButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableScreenShootButton(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableViewModeButton(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        public string GetWingShipButtonState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetWingShipButtonVisible()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatPanelButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDropDownValueChanged(int value)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonDown(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSoundButtonUp(GameObject go)
        {
        }

        [MethodImpl(0x8000)]
        private void OnWingShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator SetChatScrollViewToEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuButtonRedPoint(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuToggleEnabled(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTextVisible(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWingShipArmorAndShield(float maxArmor, float currArmor, float maxShield, float currShield)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWingShipButtonIcon(string iconPath, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetWingShipButtonImpl(string state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWingShipButtonState(string state)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWingShipCaptainProfession(string iconPath, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public bool SetWingShipCDInfo(uint startTime, uint currTime, uint timeInterval)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWingShipSubRankUI(SubRankType subRankType)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideChatButtonUI(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideFunButtonUI(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHideWingShipButton(bool isVisible)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipItemStoreGetItemEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void StartRecordAnimation()
        {
        }

        [MethodImpl(0x8000)]
        public void StopRecordAnimation()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateActionButtonInfo(bool isRedPoint, bool isRedCount, int count)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChatScrollView(ChatChannel chatChannel, ChatInfo chatInfo)
        {
        }

        [CompilerGenerated]
        private sealed class <SetChatScrollViewToEnd>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal SolarSystemFunButtonsUIController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            [MethodImpl(0x8000)]
            public bool MoveNext()
            {
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }
    }
}

