﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class TradeUIHelper
    {
        public const string UnknownStr = "???";
        private static DelegateBridge __Hotfix_GetJumpNum_0;
        private static DelegateBridge __Hotfix_GetJumpNum_1;
        private static DelegateBridge __Hotfix_GetTradeNpcLocationStr_0;
        private static DelegateBridge __Hotfix_GetTradeNpcLocationStr_1;
        private static DelegateBridge __Hotfix_GetTradePortName;
        private static DelegateBridge __Hotfix_GetSolarSystemName;
        private static DelegateBridge __Hotfix_IsPlayerAt;
        private static DelegateBridge __Hotfix_GetGuildTradeItemInfo;
        private static DelegateBridge __Hotfix_IsPersonalTradeItemLegal;
        private static DelegateBridge __Hotfix_GetTradeIllegalListItemInfosByItemId;
        private static DelegateBridge __Hotfix_GetTradeListItemBasicSorter;
        private static DelegateBridge __Hotfix_GetTradeListSellItemBasicSorter;
        private static DelegateBridge __Hotfix_GetPersonalTradeItemOwnNum;
        private static DelegateBridge __Hotfix_GetGuildTradeItemOwnNum;
        private static DelegateBridge __Hotfix_GetSingleItemOccupySize;
        private static DelegateBridge __Hotfix_GetMaxTransportNumBySpace;
        private static DelegateBridge __Hotfix_GetGuildTradePortLevel;
        private static DelegateBridge __Hotfix_GetGuildTradePortShipCapacity;
        private static DelegateBridge __Hotfix_GetFactionCreditLevelInfoFromCreditLevel;

        [MethodImpl(0x8000)]
        public static ConfigDataFactionCreditLevelInfo GetFactionCreditLevelInfoFromCreditLevel(FactionCreditLevel lv)
        {
        }

        [MethodImpl(0x8000)]
        public static ConfigDataGuildTradeItemInfo GetGuildTradeItemInfo(StoreItemType storeItemType, int itemId)
        {
        }

        [MethodImpl(0x8000)]
        public static long GetGuildTradeItemOwnNum(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildTradePortLevel(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetGuildTradePortShipCapacity(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetJumpNum(int destinationSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetJumpNum(int startSolarSystemId, int targetSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetMaxTransportNumBySpace(float singleSize, float maxSize)
        {
        }

        [MethodImpl(0x8000)]
        public static long GetPersonalTradeItemOwnNum(StoreItemType itemType, int configId)
        {
        }

        [MethodImpl(0x8000)]
        public static float GetSingleItemOccupySize(int configId, StoreItemType itemType)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetSolarSystemName(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static List<TradeListItemInfo> GetTradeIllegalListItemInfosByItemId(List<TradeListItemInfo> tradeIllegaListItemInfos, int tradeItemId)
        {
        }

        [MethodImpl(0x8000)]
        public static Func<TradeListItemInfo, TradeListItemInfo, int> GetTradeListItemBasicSorter(TradeItemSortType sortType, bool isUp)
        {
        }

        [MethodImpl(0x8000)]
        public static Func<TradeListItemSellInfo, TradeListItemSellInfo, int> GetTradeListSellItemBasicSorter(TradeItemSortType sortType, bool isUp)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTradeNpcLocationStr(int npcLocateSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTradeNpcLocationStr(int startSolarSystemId, int npcLocateSolarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static string GetTradePortName(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPersonalTradeItemLegal(int tradeItemId)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsPlayerAt(int solarSystemId, int stationId)
        {
        }

        [CompilerGenerated]
        private sealed class <GetTradeListItemBasicSorter>c__AnonStorey0
        {
            internal bool isUp;

            [MethodImpl(0x8000)]
            internal int <>m__0(TradeListItemInfo item1, TradeListItemInfo item2)
            {
            }

            [MethodImpl(0x8000)]
            internal int <>m__1(TradeListItemInfo item1, TradeListItemInfo item2)
            {
            }

            [MethodImpl(0x8000)]
            internal int <>m__2(TradeListItemInfo item1, TradeListItemInfo item2)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetTradeListSellItemBasicSorter>c__AnonStorey1
        {
            internal bool isUp;

            internal int <>m__0(TradeListItemSellInfo item1, TradeListItemSellInfo item2)
            {
                int jumpNum = TradeUIHelper.GetJumpNum(item1.m_tradeBuyNpcShopInfo.SolarSystemId);
                int num2 = TradeUIHelper.GetJumpNum(item2.m_tradeBuyNpcShopInfo.SolarSystemId);
                return (!this.isUp ? (num2 - jumpNum) : (jumpNum - num2));
            }

            internal int <>m__1(TradeListItemSellInfo item1, TradeListItemSellInfo item2) => 
                (!this.isUp ? (item2.m_price - item1.m_price) : (item1.m_price - item2.m_price));
        }
    }
}

