﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class GuidActionIssueHintUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_state;
        [AutoBind("./ContentGroup/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_hintText;
        [AutoBind("./ContentGroup/GuildItemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_ownMoneyCount;
        [AutoBind("./ContentGroup/IssueItemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_needMoneyCount;
        [AutoBind("./ContentGroup/Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmBtn;
        [AutoBind("./ContentGroup/Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelBtn;
        private static DelegateBridge __Hotfix_Show;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateOwnMoney;
        private static DelegateBridge __Hotfix_OnConfirmClick;
        private static DelegateBridge __Hotfix_OnCancelClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmClick;

        public event Action EventOnConfirmClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfirmClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void Show(bool show)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateOwnMoney()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(ConfigDataGuildActionInfo data, ulong cost)
        {
        }
    }
}

