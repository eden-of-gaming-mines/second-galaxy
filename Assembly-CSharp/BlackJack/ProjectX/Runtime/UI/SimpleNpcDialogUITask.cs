﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class SimpleNpcDialogUITask : UITaskBase
    {
        private bool m_isAutoClose;
        private float m_closeTime;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRequestToStopDialog;
        public Action<bool> EventSetCountdownState;
        public Action<string> EventOnCountdownInProgress;
        public Action EventOnDialogAutoClosed;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnNeedFadeInOutForDialogClose;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStartFullScreen;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnStopFullScreen;
        public const string ParamKey_StartDialogId = "StartDialogId";
        public const string ParamKey_EndDialogId = "EndDialogId";
        public const string ParamKey_QuestNpcDNId = "QuestNpcDNId";
        public const string ParamKey_QuestId = "QuestId";
        public const string ParamKey_ReplaceNpc = "ReplaceNpc";
        public const string ParamKey_NeedFadeInOut = "NeedFadeInOut";
        public const string ParamKey_NeedPlayNpcAudio = "PlayNpcAudio";
        private int m_startDialogId;
        private int m_endDialogId;
        private bool m_isEndDialogButtonContent;
        private NpcDNId m_questNpcDNId;
        private int m_questId;
        private NpcDNId m_replaceNpc;
        private NpcTalkerDialogUIController m_npcTalkerUICtrl;
        private bool m_needFadeInOut;
        private int m_currDialogId;
        private bool m_needPlayNpcAudio;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "SimpleNpcDialogUITask";
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_ShowQuestTalkToCompleteDialog;
        private static DelegateBridge __Hotfix_ShowInSpaceSimpleDialog;
        private static DelegateBridge __Hotfix_SendInSpaceSimpleNpcDialogEndReq;
        private static DelegateBridge __Hotfix_PauseSimpleNpcDialogUITaskInSpace;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitParamsFromIntent;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_OnLoadDynamicResCompleted;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnDialogOptionButtonClick;
        private static DelegateBridge __Hotfix_RegisterShipEnterEvent;
        private static DelegateBridge __Hotfix_UnregisterShipEnterEvent;
        private static DelegateBridge __Hotfix_OnShipEnter;
        private static DelegateBridge __Hotfix_StartFullscreenAction;
        private static DelegateBridge __Hotfix_StopFullscreenAction;
        private static DelegateBridge __Hotfix_GetNpcTalkerInfo;
        private static DelegateBridge __Hotfix_UpdateSimpleNpcDialogInfo;
        private static DelegateBridge __Hotfix_ClearDataCache;
        private static DelegateBridge __Hotfix_add_EventOnRequestToStopDialog;
        private static DelegateBridge __Hotfix_remove_EventOnRequestToStopDialog;
        private static DelegateBridge __Hotfix_add_EventOnNeedFadeInOutForDialogClose;
        private static DelegateBridge __Hotfix_remove_EventOnNeedFadeInOutForDialogClose;
        private static DelegateBridge __Hotfix_add_EventOnStartFullScreen;
        private static DelegateBridge __Hotfix_remove_EventOnStartFullScreen;
        private static DelegateBridge __Hotfix_add_EventOnStopFullScreen;
        private static DelegateBridge __Hotfix_remove_EventOnStopFullScreen;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnNeedFadeInOutForDialogClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRequestToStopDialog
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStartFullScreen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnStopFullScreen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public SimpleNpcDialogUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataNpcTalkerInfo GetNpcTalkerInfo(out bool isGlobalNpc)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParamsFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDialogOptionButtonClick(int optionIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadDynamicResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipEnter(ILBSpaceTarget targetShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void PauseSimpleNpcDialogUITaskInSpace()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterShipEnterEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SendInSpaceSimpleNpcDialogEndReq(Action onDialogEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static SimpleNpcDialogUITask ShowInSpaceSimpleDialog(int startDialogId, int endDialogId, NpcDNId replaceNpc = null, int questId = 0, Action onEndAction = null, bool needFadeInOut = false)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowQuestTalkToCompleteDialog(int questInsId, int condIdx, Action<int> onEndAction = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartFullscreenAction()
        {
        }

        [MethodImpl(0x8000)]
        private void StopFullscreenAction()
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterShipEnterEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSimpleNpcDialogInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendInSpaceSimpleNpcDialogEndReq>c__AnonStorey2
        {
            internal Action onDialogEnd;
            internal SimpleNpcDialogUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                int startDialogId = this.$this.m_startDialogId;
                if (this.$this.m_needPlayNpcAudio)
                {
                    bool isGlobalNpc = true;
                    NPCChatHelper.PlayNpcLeaveTalkAudio(this.$this.GetNpcTalkerInfo(out isGlobalNpc));
                }
                this.$this.StopFullscreenAction();
                this.$this.Pause();
                if (this.onDialogEnd != null)
                {
                    this.onDialogEnd();
                }
                (GameManager.Instance.PlayerContext as ProjectXPlayerContext).SendInSpaceSimpleNpcDialogEndReq(startDialogId);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowInSpaceSimpleDialog>c__AnonStorey1
        {
            internal SimpleNpcDialogUITask npcSimpleDialogUITask;
            internal Action onEndAction;

            internal void <>m__0()
            {
                this.npcSimpleDialogUITask.SendInSpaceSimpleNpcDialogEndReq(this.onEndAction);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowQuestTalkToCompleteDialog>c__AnonStorey0
        {
            internal int questInsId;
            internal int condIdx;
            internal SimpleNpcDialogUITask npcSimpleDialogUITask;
            internal Action<int> onEndAction;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(Task task)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey3
        {
            internal ConfigDataNpcTalkerInfo npcTalkerInfo;

            internal void <>m__0()
            {
                NPCChatHelper.PlayNpcGreetingAudio(this.npcTalkerInfo);
            }
        }
    }
}

