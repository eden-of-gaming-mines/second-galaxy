﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ShipInfoItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnItemClick;
        public ScrollItemBaseUIController ScrollCtrl;
        private List<SlotGroupInfoUIController> m_slotGroupUICtrlList;
        protected HangarShipListItemUIController m_shipItemCtrl;
        public static string ErrorState_Hide;
        public static string ErrorState_Show;
        private bool m_isShipDestoryed;
        [AutoBind("./ShipStrikeErrorRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipStrikeErrorStateCtrl;
        [AutoBind("./ShipStrikeErrorRoot/ErrorText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ShipStrikeErrorText;
        [AutoBind("./ShipIconDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipIconDummy;
        [AutoBind("./ToggleFrameImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ToggleImage;
        [AutoBind("./HighSlot1", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HighSlot1;
        [AutoBind("./HighSlot2", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HighSlot2;
        [AutoBind("./HighSlot3", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HighSlot3;
        [AutoBind("./DPS/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DPSValueText;
        [AutoBind("./HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text HPValueText;
        [AutoBind("./Speed/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SpeedValueText;
        [AutoBind("./Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text RangeValueText;
        [AutoBind("./ButtonGroup/AssembleButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AssembleButton;
        [AutoBind("./ButtonGroup/AutoReloadAmmoButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AutoReloadAmmoButton;
        [AutoBind("./ButtonGroup/TradeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx TradeButton;
        [AutoBind("./ButtonGroup/DisposeButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DisposeButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitShipInfo;
        private static DelegateBridge __Hotfix_SetTradeButtonActive;
        private static DelegateBridge __Hotfix_ShowShipStrikeErrorState;
        private static DelegateBridge __Hotfix_SetIsSelected;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_get_SlotGroupUICtrlList;

        protected event Action<UIControllerBase> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void InitShipInfo(int idx, ILBStaticPlayerShip ship, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIsSelected(bool isSel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTradeButtonActive(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowShipStrikeErrorState(string stateName, string errorInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        private List<SlotGroupInfoUIController> SlotGroupUICtrlList
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

