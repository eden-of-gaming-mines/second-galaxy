﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SolarSystemTargetNormalWithNameTagUIController : SolarSystemTargetTagBaseUIController
    {
        [AutoBind("./NameAndTargetNotice/TargetSpecialIconRoot/TargetNoticeRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetNoticeCtrl;
        [AutoBind("./NameAndTargetNotice/TargetSpecialIconRoot/TargetPVPRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_targetPvpCtrl;
        [AutoBind("./NameAndTargetNotice/TargetNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TargetNameText;
        [AutoBind("./DistanceText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText DistanceText;
        private static DelegateBridge __Hotfix_UpdateTagInfo;
        private static DelegateBridge __Hotfix_SetTargetName;
        private static DelegateBridge __Hotfix_GetTargetPvpFlagStateCtrl;
        private static DelegateBridge __Hotfix_GetTargetNoticeStateCtrl;

        [MethodImpl(0x8000)]
        protected override CommonUIStateController GetTargetNoticeStateCtrl()
        {
        }

        [MethodImpl(0x8000)]
        protected override CommonUIStateController GetTargetPvpFlagStateCtrl()
        {
        }

        [MethodImpl(0x8000)]
        public void SetTargetName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTagInfo(double dist)
        {
        }
    }
}

