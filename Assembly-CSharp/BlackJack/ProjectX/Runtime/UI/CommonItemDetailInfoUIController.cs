﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonItemDetailInfoUIController : UIControllerBase
    {
        private DetailInfoUIMode m_uiMode;
        public DetailInfoTabType m_currentTabType;
        private string m_characteristicItemAssetName;
        private GameObject m_shipCharacteristicItemGoInstance;
        private string m_limitItemAssetName;
        private GameObject m_limitInfoItemGoInstance;
        private GameObject m_propertiesPanel;
        private string m_shipPropertiesAssetName;
        private GameObject m_shipPropertiesGoInstance;
        private string m_passiveEquipmentPropertiesAssetName;
        private GameObject m_passiveEquipmentPropertiesGoInstance;
        private string m_activeEquipmentPropertiesAssetName;
        private GameObject m_activeEquipmentPropertiesGoInstance;
        private string m_weaponPropertiesAssetName;
        private GameObject m_weaponPropertiesGoInstance;
        private string m_ammoPropertiesAssetName;
        private GameObject m_ammoPropertiesGoInstance;
        private GameObject m_techItemTemplete;
        private string m_techItemAssetName;
        private List<CommonDependenceAndRelativeTechItemUICtrl> m_dependenceTechCtrlList;
        private List<CommonDependenceAndRelativeTechItemUICtrl> m_relativeTechCtrlList;
        private bool m_isToggleReset;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DetailInfoTabType> EventOnDetailInfoUITabChanged;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGotoTechButtonClick;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./Toggles/Tabs/ToggleCharacteristic", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ToggleCharacteristic;
        [AutoBind("./Toggles/Tabs/ToggleProperties", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ToggleProperties;
        [AutoBind("./Toggles/Tabs/ToggleAssembling", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ToggleAssembling;
        [AutoBind("./Toggles/Tabs/ToggleUsing", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ToggleUsing;
        [AutoBind("./Toggles/Tabs/ToggleDescription", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ToggleDescription;
        [AutoBind("./Toggles/Tabs/ToggleTalent", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ToggleTalent;
        [AutoBind("./Toggles/Tabs/ToggleScience", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle ToggleTech;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CharacteristicPanel;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/LicenseTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject License;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/LicenseTitle/LicenseValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text LicenseValue;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/SuperWeapon/MainWeapon/WeaponIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image WeaponIcon;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/SuperWeapon/MainWeapon/WeaponName", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponName;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/SuperWeapon/WeaponDescription", AutoBindAttribute.InitState.NotInit, false)]
        public Text WeaponDescription;
        [AutoBind("./PropertyRootDummy/AssemblingPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AssemblingPanel;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/EquipInfo/CPUCost/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CPUCostValue;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/EquipInfo/PowerCost/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PowerCostValue;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/EquipInfo/Type/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AssemblingItemTypeValue;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/EquipInfo/SlotType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text SlotTypeValue;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/EquipInfo/MaxFitting", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MaxFittingRoot;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/EquipInfo/MaxFitting/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text MaxFittingValue;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/SkillRequirementPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AssemblingSkillRequirementRoot;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/ShipLimitPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AssemblingShipLimitRoot;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/AmmoLimitPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AssemblingAmmoLimitRoot;
        [AutoBind("./PropertyRootDummy/AssemblingPanel/Viewport/Content/AmmoLimitPanel/AmmoLimitsDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AssemblingAmmoLimitsDummy;
        [AutoBind("./PropertyRootDummy/UsingPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UsingPanel;
        [AutoBind("./PropertyRootDummy/UsingPanel/Viewport/Content/AmmoInfo/Type/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UsingItemTypeValue;
        [AutoBind("./PropertyRootDummy/UsingPanel/Viewport/Content/SkillRequirementPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UsingSkillRequirementRoot;
        [AutoBind("./PropertyRootDummy/UsingPanel/Viewport/Content/WeaponLimitPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject UsingWeaponLimitRoot;
        [AutoBind("./PropertyRootDummy/UsingPanel/Viewport/Content/WeaponLimitPanel/WeaponLimitInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UsingWeaponLimitValue;
        [AutoBind("./PropertyRootDummy/DescriptionPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DescriptionPanel;
        [AutoBind("./PropertyRootDummy/DescriptionPanel/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text DescriptionText;
        [AutoBind("./PropertyRootDummy/TalentPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TalentInfoPanel;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/TacticalEquip", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TacticalEquipInfoPanel;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/TacticalEquip/MainWeapon/EquipName", AutoBindAttribute.InitState.NotInit, false)]
        public Text TacticalEquipNameText;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/TacticalEquip/MainWeapon/EquipIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image TacticalEquipIcon;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/TacticalEquip/EquipDescription", AutoBindAttribute.InitState.NotInit, false)]
        public Text TacticalEquipDescText;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/SuperWeapon", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SuperWeaponEquipInfoPanel;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/SuperWeapon/MainWeapon/WeaponName", AutoBindAttribute.InitState.NotInit, false)]
        public Text SuperWeaponEquipNameText;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/SuperWeapon/MainWeapon/WeaponIcon", AutoBindAttribute.InitState.NotInit, false)]
        public Image SuperWeaponEquipIcon;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/SuperWeapon/MainWeapon/CDTime", AutoBindAttribute.InitState.NotInit, false)]
        public Text SuperWeaponEquipCDTime;
        [AutoBind("./PropertyRootDummy/TalentPanel/Viewport/Content/SuperWeapon/WeaponDescription", AutoBindAttribute.InitState.NotInit, false)]
        public Text SuperWeaponEquipDescText;
        [AutoBind("./PropertyRootDummy/TechPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechInfoPanel;
        [AutoBind("./PropertyRootDummy/TechPanel/Viewport/Content/DependenceTechList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DependenceTechList;
        [AutoBind("./PropertyRootDummy/TechPanel/Viewport/Content/RelativeTechList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject RelativeTechList;
        [AutoBind("./PropertyRootDummy/TechPanel/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform TechItemRoot;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/LicenseUpgradeProfit", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LicenseUpgradeProfit;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/LicenseUpgradeProfit/Title/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LicenseUpgradeTitleText;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/Viewport/Content/SpecialBuf", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SpecialBuf;
        [AutoBind("./PropertyRootDummy/CharacteristicPanel/CharacteristicItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CharacteristicItemDummy;
        [AutoBind("./LimitItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LimitItemDummy;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemDummy;
        [AutoBind("./PropertyRootDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform PropertyRootDummy;
        private static int m_shipPropertiesHeight = 0x27b;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_UpdateItemDetailInfo;
        private static DelegateBridge __Hotfix_UpdateUIWithCurrentToggleAndUIMode;
        private static DelegateBridge __Hotfix_UpdateEquipAndWeponInfoUIWithToggleIndex;
        private static DelegateBridge __Hotfix_UpdateShipInfoUIWithToggleIndex;
        private static DelegateBridge __Hotfix_UpdateAmmoInfoUIWithToggleIndex;
        private static DelegateBridge __Hotfix_InitTabStateWithItemType;
        private static DelegateBridge __Hotfix_SetDetailInfoUIMode;
        private static DelegateBridge __Hotfix_UpdateShipTalentInfo_0;
        private static DelegateBridge __Hotfix_UpdateShipCharacteristicInfo_0;
        private static DelegateBridge __Hotfix_CreateCharacteristicItemListWithInfo;
        private static DelegateBridge __Hotfix_UpdateAssemblingInfo;
        private static DelegateBridge __Hotfix_CreateLimitItemsWithInfoList;
        private static DelegateBridge __Hotfix_GetItemTypeDescString;
        private static DelegateBridge __Hotfix_GetAssembleTechRequirement;
        private static DelegateBridge __Hotfix_GetAssembleCPUCost;
        private static DelegateBridge __Hotfix_GetAssemblePowerCost;
        private static DelegateBridge __Hotfix_GetAssembleSlotTypeString;
        private static DelegateBridge __Hotfix_GetAssembleMaximumFittingValue;
        private static DelegateBridge __Hotfix_GetAmmoLimitInfoForWeapon;
        private static DelegateBridge __Hotfix_UpdateUsingInfo;
        private static DelegateBridge __Hotfix_GetWeaponTypeStringKeyForAmmo;
        private static DelegateBridge __Hotfix_UpdatePropertiesInfo;
        private static DelegateBridge __Hotfix_UpdateEquipProperties;
        private static DelegateBridge __Hotfix_UpdateNormalAmmonProperties;
        private static DelegateBridge __Hotfix_UpdateMissileAmmonProperties;
        private static DelegateBridge __Hotfix_UpdateDroneAmmonProperties;
        private static DelegateBridge __Hotfix_UpdateWeaponProperties;
        private static DelegateBridge __Hotfix_UpdateDescriptionInfo_0;
        private static DelegateBridge __Hotfix_UpdateDescriptionInfo_1;
        private static DelegateBridge __Hotfix_UpdateTechInfo;
        private static DelegateBridge __Hotfix_ShowEquipCommonInfoUI;
        private static DelegateBridge __Hotfix_CheckAndCreateShipPropertiesInstance;
        private static DelegateBridge __Hotfix_CheckAndCreateActiveEquipmentPropertiesInstance;
        private static DelegateBridge __Hotfix_CheckAndCreatePassiveEquipmentPropertiesInstance;
        private static DelegateBridge __Hotfix_CheckAndCreateWeaponPropertiesInstance;
        private static DelegateBridge __Hotfix_CheckAndCreateAmmonPropertiesInstance;
        private static DelegateBridge __Hotfix_UpdateUISizeWithItemType;
        private static DelegateBridge __Hotfix_UpdateUIRectWithRectDummy;
        private static DelegateBridge __Hotfix_ReSetRect;
        private static DelegateBridge __Hotfix_OnToggleCharacteristicValueChanged;
        private static DelegateBridge __Hotfix_OnToggleAssemblingValueChanged;
        private static DelegateBridge __Hotfix_OnToggleUsingValueChanged;
        private static DelegateBridge __Hotfix_OnTogglePropertiesValueChanged;
        private static DelegateBridge __Hotfix_OnToggleDescriptionValueChanged;
        private static DelegateBridge __Hotfix_OnToggleTalentValueChanged;
        private static DelegateBridge __Hotfix_OnToggleTechValueChanged;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_remove_EventOnDetailInfoUITabChanged;
        private static DelegateBridge __Hotfix_add_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_UpdateShipDetailInfo;
        private static DelegateBridge __Hotfix_UpdateShipCharacteristicInfo_1;
        private static DelegateBridge __Hotfix_UpdateShipTalentInfo_1;

        public event Action<DetailInfoTabType> EventOnDetailInfoUITabChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGotoTechButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void CheckAndCreateActiveEquipmentPropertiesInstance()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckAndCreateAmmonPropertiesInstance()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckAndCreatePassiveEquipmentPropertiesInstance()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckAndCreateShipPropertiesInstance()
        {
        }

        [MethodImpl(0x8000)]
        private void CheckAndCreateWeaponPropertiesInstance()
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        private void CreateCharacteristicItemListWithInfo(List<PropertyValueStringPair> infoList, Transform rootTrans)
        {
        }

        [MethodImpl(0x8000)]
        private void CreateLimitItemsWithInfoList(List<string> infoList, Transform itemRoot)
        {
        }

        [MethodImpl(0x8000)]
        private List<string> GetAmmoLimitInfoForWeapon(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        private uint GetAssembleCPUCost(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        private int GetAssembleMaximumFittingValue(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        private uint GetAssemblePowerCost(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        private string GetAssembleSlotTypeString(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        private int GetAssembleTechRequirement(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        private string GetItemTypeDescString(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(string uiStateName, bool isImmediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        private string GetWeaponTypeStringKeyForAmmo(ILBItem item)
        {
        }

        [MethodImpl(0x8000)]
        private void InitTabStateWithItemType(ILBItem itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleAssemblingValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleCharacteristicValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleDescriptionValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTogglePropertiesValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleTalentValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleTechValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void OnToggleUsingValueChanged(bool isActive)
        {
        }

        [MethodImpl(0x8000)]
        private void ReSetRect(RectTransform fromRect, RectTransform toRect)
        {
        }

        [MethodImpl(0x8000)]
        private void SetDetailInfoUIMode(DetailInfoUIMode mode)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 worldPos)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowEquipCommonInfoUI()
        {
        }

        [MethodImpl(0x8000)]
        public void Start()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAmmoInfoUIWithToggleIndex(ILBItem item, ILBItem itemCompare, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAssemblingInfo(ILBItem item, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDescriptionInfo(ILBItem item, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDescriptionInfo(LBStaticPlayerShipClient item, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDroneAmmonProperties(ILBItem item, ILBItem itemCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateEquipAndWeponInfoUIWithToggleIndex(ILBItem item, ILBItem itemCompare, bool showGoTechButton, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateEquipProperties(ILBItem item, ILBItem itemComapre)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemDetailInfo(ILBItem item, Dictionary<string, UnityEngine.Object> resDict, ILBItem itemCompare = null, RectTransform itemRectDummy = null, RectTransform propertyRectDummy = null, bool resetToggle = false, DetailInfoTabType currTab = 0, bool showGoTechButton = true)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateMissileAmmonProperties(ILBItem item, ILBItem itemCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateNormalAmmonProperties(ILBItem item, ILBItem itemCompare)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdatePropertiesInfo(ILBItem item, ILBItem itemCompare, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipCharacteristicInfo(ILBItem item, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipCharacteristicInfo(LBStaticPlayerShipClient shipInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipDetailInfo(LBStaticPlayerShipClient shipInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipInfoUIWithToggleIndex(ILBItem item, ILBItem itemCompare, bool showGoButton, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipTalentInfo(ILBItem item, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipTalentInfo(LBStaticPlayerShipClient shipInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTechInfo(ILBItem item, bool showGoButton, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUIRectWithRectDummy(RectTransform itemRectDummy = null, RectTransform propertyRectDummy = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUISizeWithItemType(Vector2 viewPortSize)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUIWithCurrentToggleAndUIMode(ILBItem item, ILBItem itemCompare, Dictionary<string, UnityEngine.Object> resDict, RectTransform itemRectDummy, RectTransform propertyRectDummy, bool showGoTechButton)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUsingInfo(ILBItem item, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateWeaponProperties(ILBItem item, ILBItem itemCompare)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public enum DetailInfoTabType
        {
            None,
            Characteristic,
            Assembling,
            Using,
            Properties,
            Description,
            Talent,
            Tech
        }

        public enum DetailInfoUIMode
        {
            ShowShipInfo,
            ShowWeaponInfo,
            ShowPassiveEquipInfo,
            ShowActiveEquipInfo,
            ShowAmmoInfo,
            ShowOnlyDescription
        }
    }
}

