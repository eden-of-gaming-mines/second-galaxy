﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ChatChannelToggleUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ChatChannel, ChatLanguageChannel> EventOnChatTabTypeChange;
        private const string CanClick = "CanClick";
        private const string CanNotClick = "CanNotClick";
        [AutoBind("./Viewport/ToggleGroup/StarField/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StarFieldTip;
        [AutoBind("./Viewport/ToggleGroup/StarField/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text StarFieldText;
        [AutoBind("./Viewport/ToggleGroup/SolarSystem/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SolarSystemTip;
        [AutoBind("./Viewport/ToggleGroup/SolarSystem/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemText;
        [AutoBind("./Viewport/ToggleGroup/SolarSystemEnghlish/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text SolarSystemEnText;
        [AutoBind("./Viewport/ToggleGroup/SolarSystemEnghlish/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SolarSystemEnTip;
        [AutoBind("./Viewport/ToggleGroup/Local/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LocalTip;
        [AutoBind("./Viewport/ToggleGroup/Local/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text LocalText;
        [AutoBind("./Viewport/ToggleGroup/Alliance/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AllianceTip;
        [AutoBind("./Viewport/ToggleGroup/Alliance/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text AllianceText;
        [AutoBind("./Viewport/ToggleGroup/Guild/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject GuildTip;
        [AutoBind("./Viewport/ToggleGroup/Guild/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text GuildText;
        [AutoBind("./Viewport/ToggleGroup/Team/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TeamTip;
        [AutoBind("./Viewport/ToggleGroup/Team/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text TeamText;
        [AutoBind("./Viewport/ToggleGroup/Wisper/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject WisperTip;
        [AutoBind("./Viewport/ToggleGroup/Wisper/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text WisperText;
        [AutoBind("./Viewport/ToggleGroup/System/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject SystemTip;
        [AutoBind("./Viewport/ToggleGroup/System/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text SystemText;
        [AutoBind("./Viewport/ToggleGroup/StarField", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController StarFieldToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/StarField", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx StarFieldToggle;
        [AutoBind("./Viewport/ToggleGroup/Local", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LocalToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/Local", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx LocalToggle;
        [AutoBind("./Viewport/ToggleGroup/SolarSystem", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SolarSystemToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/SolarSystem", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SolarSystemToggle;
        [AutoBind("./Viewport/ToggleGroup/SolarSystemEnghlish", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SolarSystemEnToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/SolarSystemEnghlish", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SolarSystemEnToggle;
        [AutoBind("./Viewport/ToggleGroup/Alliance", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllianceToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/Alliance/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image AllianceToggleImage;
        [AutoBind("./Viewport/ToggleGroup/Alliance", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx AllianceToggle;
        [AutoBind("./Viewport/ToggleGroup/Guild", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/Guild/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image GuildToggleImage;
        [AutoBind("./Viewport/ToggleGroup/Guild", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx GuildToggle;
        [AutoBind("./Viewport/ToggleGroup/Team", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TeamToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/Team/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image TeamToggleImage;
        [AutoBind("./Viewport/ToggleGroup/Team", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx TeamToggle;
        [AutoBind("./Viewport/ToggleGroup/Wisper", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController WisperToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/Wisper/Image", AutoBindAttribute.InitState.NotInit, false)]
        public Image WisperToggleImage;
        [AutoBind("./Viewport/ToggleGroup/Wisper", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx WisperToggle;
        [AutoBind("./Viewport/ToggleGroup/System", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SystemToggleStateCtrl;
        [AutoBind("./Viewport/ToggleGroup/System", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx SystemToggle;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetDefaultChannel;
        private static DelegateBridge __Hotfix_SwitchToAppointChannel;
        private static DelegateBridge __Hotfix_EnableChatChannelToggle;
        private static DelegateBridge __Hotfix_EnableToggle;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_Wisper;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_Local;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_Team;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_SolarSystem;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_SolarSystemEn;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_StarField;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_Guild;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_Alliance;
        private static DelegateBridge __Hotfix_OnChatTabTypeChange_System;
        private static DelegateBridge __Hotfix_SetOrCancelUnReadChatMsgTip;
        private static DelegateBridge __Hotfix_SetStarFieldTip;
        private static DelegateBridge __Hotfix_SetSolarSystemTip;
        private static DelegateBridge __Hotfix_SetLocalTip;
        private static DelegateBridge __Hotfix_SetAllianceTip;
        private static DelegateBridge __Hotfix_SetGuildTip;
        private static DelegateBridge __Hotfix_SetTeamTip;
        private static DelegateBridge __Hotfix_SetWisperTip;
        private static DelegateBridge __Hotfix_SetSystemTip;
        private static DelegateBridge __Hotfix_add_EventOnChatTabTypeChange;
        private static DelegateBridge __Hotfix_remove_EventOnChatTabTypeChange;

        public event Action<ChatChannel, ChatLanguageChannel> EventOnChatTabTypeChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void EnableChatChannelToggle(ChatChannel chatChannel, bool active)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableToggle(bool isEnable)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_Alliance(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_Guild(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_Local(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_SolarSystem(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_SolarSystemEn(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_StarField(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_System(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_Team(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void OnChatTabTypeChange_Wisper(UIControllerBase uCtrl, bool currentValue)
        {
        }

        [MethodImpl(0x8000)]
        private void SetAllianceTip(bool isNeedTip, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDefaultChannel()
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuildTip(bool isNeedTip, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLocalTip(bool isNeedTip, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOrCancelUnReadChatMsgTip(bool isNeedTip, ChatChannel chatChannel, ChatLanguageChannel langueChannel = 0, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSolarSystemTip(bool isNeedTip, ChatLanguageChannel langueChannel = 0, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetStarFieldTip(bool isNeedTip, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetSystemTip(bool isNeedTip, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetTeamTip(bool isNeedTip, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        private void SetWisperTip(bool isNeedTip, int unReadMsgCount = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchToAppointChannel(ChatChannel channel, ChatLanguageChannel langueChanel = 0)
        {
        }
    }
}

