﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class GuildProduceSpeedUpUITask : GuildUITaskBase
    {
        private bool m_isClosing;
        public const int MinSpeedUpTime = 300;
        public const string ParamKey_GuildProduceLine = "GuildProduceLineSpeedUp";
        private GuildProductionLineInfo m_currProduceLine;
        private GuildProduceSpeedUpUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "GuildProduceSpeedUpUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildProduceSpeedUpUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_RegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_UnRegisterNetWorkEvent;
        private static DelegateBridge __Hotfix_OnGuildLeaveNtf;
        private static DelegateBridge __Hotfix_CloseSpeedUpPanel;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnSpeedUpItemClick;
        private static DelegateBridge __Hotfix_SendSpeedUpReq;
        private static DelegateBridge __Hotfix_SendSpeedUpWithItemNetworkReq;
        private static DelegateBridge __Hotfix_SendGuildCurrencyDataReq;
        private static DelegateBridge __Hotfix_SendGuildProduceDataReq;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildProduceSpeedUpUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseSpeedUpPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildLeaveNtf()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSpeedUpItemClick(GuildProductionSpeedUpLevel level)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildCurrencyDataReq(Action<int> onEnd, bool isForceSend)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildProduceDataReq(Action<int> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendSpeedUpReq(GuildProductionSpeedUpLevel level)
        {
        }

        [MethodImpl(0x8000)]
        private void SendSpeedUpWithItemNetworkReq(int level, Action<int> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildProduceSpeedUpUITask StartGuildProduceSpeedUpUITask(UIIntent returnToIntent, GuildProductionLineInfo lineInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void UnRegisterNetWorkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnSpeedUpItemClick>c__AnonStorey0
        {
            internal GuildProductionSpeedUpLevel level;
            internal GuildProduceSpeedUpUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildCurrencyDataReq>c__AnonStorey2
        {
            internal Action<int> onEnd;

            internal void <>m__0(Task task)
            {
                GuildCurrencyInfoReqNetTask task2 = task as GuildCurrencyInfoReqNetTask;
                if (!task2.IsNetworkError && (this.onEnd != null))
                {
                    this.onEnd(task2.m_result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildProduceDataReq>c__AnonStorey3
        {
            internal Action<int> onEnd;

            internal void <>m__0(Task task)
            {
                GetGuildProductionInfoReqNetTask task2 = task as GetGuildProductionInfoReqNetTask;
                if (!task2.IsNetworkError && (this.onEnd != null))
                {
                    this.onEnd(task2.Result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendSpeedUpWithItemNetworkReq>c__AnonStorey1
        {
            internal Action<int> onEnd;

            internal void <>m__0(Task task)
            {
                GuildProductionLineSpeedUpByGuildTradeMoneyReqNetTask task2 = task as GuildProductionLineSpeedUpByGuildTradeMoneyReqNetTask;
                if (!task2.IsNetworkError && (this.onEnd != null))
                {
                    this.onEnd(task2.Result);
                }
            }
        }
    }
}

