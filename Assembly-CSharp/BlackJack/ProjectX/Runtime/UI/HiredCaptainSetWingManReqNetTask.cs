﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class HiredCaptainSetWingManReqNetTask : NetWorkTransactionTask
    {
        public ulong m_captainInstanceId;
        public int m_srcIndex;
        public int m_dstIndex;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <CaptainSetWingManResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnSetWingManAck;
        private static DelegateBridge __Hotfix_set_CaptainSetWingManResult;
        private static DelegateBridge __Hotfix_get_CaptainSetWingManResult;

        [MethodImpl(0x8000)]
        public HiredCaptainSetWingManReqNetTask(ulong captainInstanceId, int dstIndex, int srcIndex = -1)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSetWingManAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int CaptainSetWingManResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

