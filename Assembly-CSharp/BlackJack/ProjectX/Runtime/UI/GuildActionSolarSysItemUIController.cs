﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildActionSolarSysItemUIController : UIControllerBase
    {
        public Action<ulong> EventOnQuestItemClick;
        public Action<int> EventOnGalaxyClick;
        private List<GuildActionInfo> m_infos;
        private List<GuildActionQuestItemUIController> m_questCtrls;
        public GameObject m_questItemObj;
        [AutoBind("./Galaxy/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NumberText;
        [AutoBind("./Galaxy/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./Galaxy/PointGroup/CornerImage/QuestCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text QuestCountText;
        [AutoBind("./Galaxy/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PointState;
        [AutoBind("./Galaxy", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GalaxyState;
        [AutoBind("./Galaxy", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx GalaxyButton;
        [AutoBind("./QuestGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform QuestRoot;
        [AutoBind("./QuestItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform QuestItemRoot;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_GetGalaxyId;
        private static DelegateBridge __Hotfix_Fold;
        private static DelegateBridge __Hotfix_ChangeFoldState;
        private static DelegateBridge __Hotfix_SetSelected;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetQuestDistance;
        private static DelegateBridge __Hotfix_OnQuestItemClick;
        private static DelegateBridge __Hotfix_OnGalaxyClick;

        [MethodImpl(0x8000)]
        public void ChangeFoldState()
        {
        }

        [MethodImpl(0x8000)]
        public void Fold(bool fold)
        {
        }

        [MethodImpl(0x8000)]
        public int GetGalaxyId()
        {
        }

        [MethodImpl(0x8000)]
        private string GetQuestDistance(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGalaxyClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelected(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView(List<GuildActionInfo> infos, ulong selectedInstancedId, string mode, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

