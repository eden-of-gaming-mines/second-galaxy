﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SailReportController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./TimeGroup/PackUpButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupCauseDamage/TakeTollOnText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_causeDamage;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupSufferDamage/TakeTollOnText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sufferDamage;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupFlyDistance/TakeTollOnText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_flyDistance;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupJumpCount/TakeTollOnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_jumpCount;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupEnterFightCount/TakeTollOnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_fightCount;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupDestroyShipCount/TakeTollOnText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_destroyCount;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupJumpCount", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_jumpCountObj;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupEnterFightCount", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_fightCountObj;
        [AutoBind("./ManifestationGroup/ManifestationItemGroup/TakeTollOnDroupDestroyShipCount", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_destroyCountObj;
        [AutoBind("./EarningsGroup/EXValueImage/EXValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_xp;
        [AutoBind("./EarningsGroup/CValueImage/CValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_credit;
        [AutoBind("./EarningsGroup/SValueImage/SValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_money;
        [AutoBind("./TimeGroup/StartTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_startTime;
        [AutoBind("./TimeGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_flyTime;
        [AutoBind("./SailingGroup/Scroll View/Viewport/Content/CourseGroup/IncidentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_incidentsRoot;
        [AutoBind("./EarningsGroup/ItemGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemGroup;
        [AutoBind("./EarningsGroup/ItemGroup/TipRefPos", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipRefPos;
        [AutoBind("./EarningsGroup/ItemGroup/Scroll View/Viewport/Content/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_itemDummy;
        [AutoBind("./EarningsGroup/ItemGroup/TipPos", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipPosTransform;
        [AutoBind("./EarningsGroup/ItemGroup/TipMaxPos", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_tipMaxPosTransform;
        [AutoBind("./SailingGroup/Scroll View/Viewport/Content/CourseGroup/IncidentGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_eventContainer;
        [AutoBind("./SailingGroup/Scroll View/Viewport/Content/CourseGroup/Last", AutoBindAttribute.InitState.NotInit, false)]
        public LayoutElement m_lastElement;
        [AutoBind("./TimeGroup/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_shareButton;
        private readonly List<SailReportIncidentController> m_incidentList;
        private readonly List<ItemDummy> m_itemDummyList;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<FakeLBStoreItem, Vector3> EventShouldOpenItemDetail;
        private const string Incident = "Incident";
        private const string NoIncident = "NoIncident";
        private const float LastElementBasicWidth = 130f;
        private const float TotalWidth = 800f;
        private const string Show = "Show";
        private const string Close = "Close";
        [CompilerGenerated]
        private static Comparison<ItemInfo> <>f__am$cache0;
        private static DelegateBridge __Hotfix_InsertData_1;
        private static DelegateBridge __Hotfix_InsertData_0;
        private static DelegateBridge __Hotfix_InsertIncidents;
        private static DelegateBridge __Hotfix_SetLastEventWidth;
        private static DelegateBridge __Hotfix_InsertGainItems;
        private static DelegateBridge __Hotfix_ClearItemEvent;
        private static DelegateBridge __Hotfix_ShowWindow;
        private static DelegateBridge __Hotfix_add_EventShouldOpenItemDetail;
        private static DelegateBridge __Hotfix_remove_EventShouldOpenItemDetail;

        public event Action<FakeLBStoreItem, Vector3> EventShouldOpenItemDetail
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void ClearItemEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void InsertData(ChatContentSailReport sailReport, Dictionary<string, UnityEngine.Object> resDict, Action<SailReportEventController> playAnim = null)
        {
        }

        [MethodImpl(0x8000)]
        public void InsertData(LogicBlockSailReportClient sailReport, Dictionary<string, UnityEngine.Object> resDict, Action<SailReportEventController> playAnim = null)
        {
        }

        [MethodImpl(0x8000)]
        private void InsertGainItems(List<ItemInfo> items, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void InsertIncidents(IList<SailReportSolarSystemInfo> reports, Dictionary<string, UnityEngine.Object> resDict, Action<SailReportEventController> playAnim = null)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator SetLastEventWidth()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowWindow(bool isShow, bool playAnim = true, Action<bool> onEnd = null)
        {
        }

        [CompilerGenerated]
        private sealed class <SetLastEventWidth>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal SailReportController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        break;

                    case 1:
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 2;
                        }
                        break;

                    case 2:
                        if (this.$this.m_eventContainer.rect.width < 800f)
                        {
                            this.$this.m_lastElement.preferredWidth = 930f - this.$this.m_eventContainer.rect.width;
                        }
                        else
                        {
                            this.$this.m_lastElement.preferredWidth = 130f;
                        }
                        this.$PC = -1;
                        goto TR_0000;

                    default:
                        goto TR_0000;
                }
                return true;
            TR_0000:
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        private class ItemDummy
        {
            public GameObject m_dummy;
            public CommonItemIconUIController m_controller;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

