﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildItemCategoryMenuItemUIController : CommonMenuItemUIController
    {
        [AutoBind("./ItemCategoryButton/CategoryNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_categoryNameText;
        [AutoBind("./ItemCategoryButton/CategoryIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_categoryIconImage;
        [AutoBind("./ItemCategoryButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemCategoryButtonCommonUIStateController;
        [AutoBind("./ItemCategoryButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ItemCategoryButton;
        private static DelegateBridge __Hotfix_SetChildSelected;
        private static DelegateBridge __Hotfix_SetMenuItemCanClick;
        private static DelegateBridge __Hotfix_SetSelectedStoreTabState;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnButtonClick;
        private static DelegateBridge __Hotfix_OnMenuItemFill;
        private static DelegateBridge __Hotfix_GetChildMenuItemAssetName;

        [MethodImpl(0x8000)]
        protected override string GetChildMenuItemAssetName()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnMenuItemFill(object data, Dictionary<string, Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetChildSelected(int itemType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuItemCanClick(bool hasPermission)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectedStoreTabState(GuildItemListUIController.GuildStoreTabType lastTab, bool isStoremMode)
        {
        }
    }
}

