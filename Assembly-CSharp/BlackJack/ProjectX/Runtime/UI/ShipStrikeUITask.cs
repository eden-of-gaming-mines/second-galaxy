﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ShipStrikeUITask : UITaskBase
    {
        private ShipStrikeUIController m_mainCtrl;
        private AutoReloadAmmoBuyPanelUIController m_autoReloadAmmoBuyPanelUICtrl;
        private AllowInShipGroupUIController m_allowInShipGroupUIController;
        private TradeLoadingPanelUIController m_tradeLoadingPanelUIController;
        private List<ILBStaticPlayerShip> m_cachedShipList;
        private bool m_isShowTradeAuction;
        private int m_buyAmmoReqCount;
        private int m_currSelectShipIndex;
        private DateTime m_lastShiphangarUpdateTime;
        private List<ShipType> m_currAllowInShipTypeList;
        protected Action<ulong, Action<bool>> m_onSelectShipForStrikeFunc;
        protected bool m_useSpaceStationBgUITask;
        private int m_fastLoadTradeItemId;
        private bool m_isAllowHireCaptain;
        private List<GrandFaction> m_validGrandFaction;
        private IUIBackgroundManager m_backgroundManager;
        public const string ParamKey_UseDefaultBgTask = "ParamKey_UseDefaultBgTask";
        public const string ParamKey_AllowInShipTypeList = "ParamKey_AllowInShipTypeList";
        public const string ParamKey_OnSelectShipForStrikeAction = "ParamKey_OnSelectShipForStrikeAction";
        public const string ParamKey_AllowHireCaptain = "ParamKey_AllowHireCaptain";
        public const string ParamKey_ValidGrandFaction = "ParamKey_ValidGrandFaction";
        public const string ParamKey_FastLoadTradeItemID = "ParamKey_FastLoadTradeItemID";
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        protected static string ShipStrikeUILayerAssetName;
        protected static string AllowInShipGroupUILayerAssetName;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string ShipStrikeUITaskMode_Normal = "ShipStrikeUITaskMode_Normal";
        public const string ShipStrikeUITaskMode_HireCaptain = "ShipStrikeUITaskMode_HireCaptain";
        public const string TaskName = "ShipStrikeUITask";
        protected List<ShipType> HireCaptainAllowInShipTypes;
        protected List<CachedHireCaptain> m_hireCaptainList;
        protected int m_selectHireCaptainIndex;
        protected int m_currEditorCaptainIndex;
        private bool m_isRepeatableUserGuide;
        private int m_wantedShipHangarIndex;
        private const string ParamKeyIsRepatebleUserGuide = "IsRepatebleUserGuide";
        private const string ParamKeySelectedShipHangarIndex = "SelectedShipHangarIndex";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartShipStrikeUITask;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCacheForNormal;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectDynamicResForNormal;
        private static DelegateBridge __Hotfix_CollectDynamicResForFastLoadTradeItem;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateViewForNormal;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_UpdateSwitchButtonState;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnShipStrikeButtonClick;
        private static DelegateBridge __Hotfix_ShipForStrike;
        private static DelegateBridge __Hotfix_OnShipInfoItemClick;
        private static DelegateBridge __Hotfix_OnShipAssembleButtonClick;
        private static DelegateBridge __Hotfix_OnShipInfoItemFill;
        private static DelegateBridge __Hotfix_OnShipAutoReloadAmmoButtonClick;
        private static DelegateBridge __Hotfix_OnDisposeButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadBuyPanelConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnAmmoReloadBuyPanelCancelButtonClick;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnSwitchShipPanelButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnSwitchHireCaptainPanelButtonClick;
        private static DelegateBridge __Hotfix_OnAllowInShipTypeButtonClick;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromUIIntent;
        private static DelegateBridge __Hotfix_ShipHangarShipInfoComparer;
        private static DelegateBridge __Hotfix_CheckShipSuitableToStrike;
        private static DelegateBridge __Hotfix_CheckAssistantShipSuitableToStrike;
        private static DelegateBridge __Hotfix_CheckHireCaptainIsEmpty;
        private static DelegateBridge __Hotfix_CheckShipDrivingLicenseRequest;
        private static DelegateBridge __Hotfix_CheckShipGrandFaction;
        private static DelegateBridge __Hotfix_CheckShipCostRequest;
        private static DelegateBridge __Hotfix_CheckShipTypeRequest;
        private static DelegateBridge __Hotfix_GetHangarShipByIndex;
        private static DelegateBridge __Hotfix_FilterForNormalQuest;
        private static DelegateBridge __Hotfix_GetSuitableShipListStartIndexFromCurrSelectIndex;
        private static DelegateBridge __Hotfix_SendBuyAmmoRequest;
        private static DelegateBridge __Hotfix_IsAvailableForHireCaptain;
        private static DelegateBridge __Hotfix_IsFlickerForHireCaptain;
        private static DelegateBridge __Hotfix_HasHiredCaptain;
        private static DelegateBridge __Hotfix_CanFastLoadTradeItem;
        private static DelegateBridge __Hotfix_CheckHasLoadTradeItem;
        private static DelegateBridge __Hotfix_UpdateStrikeButtonState;
        private static DelegateBridge __Hotfix_UpdateStrikeShipList;
        private static DelegateBridge __Hotfix_OnTradeLoadingOkButtonClick;
        private static DelegateBridge __Hotfix_SendMoveItemToHangarShipItemStore;
        private static DelegateBridge __Hotfix_LeaveBackground;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_UpdateDataCacheForHireCaptain;
        private static DelegateBridge __Hotfix_GetHireCaptainAllowInTypeList;
        private static DelegateBridge __Hotfix_CollectDynamicResForHireCaptain;
        private static DelegateBridge __Hotfix_CollectDynamicResForValidGrandFaction;
        private static DelegateBridge __Hotfix_UpdateViewForHireCaptain;
        private static DelegateBridge __Hotfix_HireCaptainComparer;
        private static DelegateBridge __Hotfix_GetCaptainInfoByWindIndex;
        private static DelegateBridge __Hotfix_IsCaptainShipAbleToStrike;
        private static DelegateBridge __Hotfix_SendCaptainSelectDrivingShip;
        private static DelegateBridge __Hotfix_StartCaptainManagerUITask;
        private static DelegateBridge __Hotfix_CheckToEnterCaptainUI;
        private static DelegateBridge __Hotfix_CloseCaptainSelectShipPanel;
        private static DelegateBridge __Hotfix_SendHiredCaptainSetWingManReq;
        private static DelegateBridge __Hotfix_OnHireCaptainItemClick;
        private static DelegateBridge __Hotfix_OnHireCaptainItemFill;
        private static DelegateBridge __Hotfix_OnCaptainSelectShipButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainIconButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainSelectShipItemClick;
        private static DelegateBridge __Hotfix_OnHireCaptainShipSelectPanelBgButton;
        private static DelegateBridge __Hotfix_OnCaptainUpButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainDownButtonClick;
        private static DelegateBridge __Hotfix_StartShipStrikeRepeatableUITask;
        private static DelegateBridge __Hotfix_InitRepatableUserGuideData;
        private static DelegateBridge __Hotfix_UpdateRepatableUserGuideDataCache;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuide;
        private static DelegateBridge __Hotfix_OnClickReturn4RepeatableUserGuide;
        private static DelegateBridge __Hotfix_StopRepeatableUserGuide;

        [MethodImpl(0x8000)]
        public ShipStrikeUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private bool CanFastLoadTradeItem()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckAssistantShipSuitableToStrike()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckHasLoadTradeItem()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckHireCaptainIsEmpty()
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipCostRequest(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipDrivingLicenseRequest(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipGrandFaction(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipSuitableToStrike(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckShipTypeRequest(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckToEnterCaptainUI()
        {
        }

        [MethodImpl(0x8000)]
        protected void CloseCaptainSelectShipPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectDynamicResForFastLoadTradeItem(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectDynamicResForHireCaptain(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectDynamicResForNormal(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectDynamicResForValidGrandFaction(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private bool FilterForNormalQuest(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        protected CachedHireCaptain GetCaptainInfoByWindIndex(int windIndex)
        {
        }

        [MethodImpl(0x8000)]
        private ILBStaticPlayerShip GetHangarShipByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private List<ShipType> GetHireCaptainAllowInTypeList()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuitableShipListStartIndexFromCurrSelectIndex(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected bool HasHiredCaptain()
        {
        }

        [MethodImpl(0x8000)]
        protected int HireCaptainComparer(CachedHireCaptain captainA, CachedHireCaptain captainB)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitDataFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitRepatableUserGuideData(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected HireCaptainErrorCode IsAvailableForHireCaptain()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsCaptainShipAbleToStrike(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsFlickerForHireCaptain()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveBackground()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllowInShipTypeButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadBuyPanelCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAmmoReloadBuyPanelConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBGButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCancelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainDownButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainIconButtonClick(CachedHireCaptain captain)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainSelectShipButtonClick(int windManIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainSelectShipItemClick(int shipIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCaptainUpButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4RepeatableUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDisposeButtonClick(int shipIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHireCaptainItemClick(StrikeHireCaptainItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHireCaptainItemFill(StrikeHireCaptainItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnHireCaptainShipSelectPanelBgButton(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipAssembleButtonClick(int shipIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipAutoReloadAmmoButtonClick(int shipIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipInfoItemClick(int shipIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipInfoItemFill(ShipInfoItemUIController itemCtrl, int shipIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipStrikeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSwitchHireCaptainPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSwitchShipPanelButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeLoadingOkButtonClick(int tradeItemId, long loadingNum, long preloadNum)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        protected void SendBuyAmmoRequest(int shipIndex, int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendCaptainSelectDrivingShip(ulong captainInstanceId, int shipId, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendHiredCaptainSetWingManReq(CachedHireCaptain currCaptain, int desrCaptainIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendMoveItemToHangarShipItemStore(ILBStoreItemClient item, long count)
        {
        }

        [MethodImpl(0x8000)]
        private void ShipForStrike(ILBStaticPlayerShip ship)
        {
        }

        [MethodImpl(0x8000)]
        private int ShipHangarShipInfoComparer(ILBStaticPlayerShip shipA, ILBStaticPlayerShip shipB)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartCaptainManagerUITask(bool repairShip, bool unlockShip, int shipIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public static void StartShipStrikeRepeatableUITask(UIIntent preIntent, Action<ulong, Action<bool>> onSelectShip4Strike, bool useDefaultBgTask = true, int hangarIndex = -1, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartShipStrikeUITask(UIIntent preIntent, List<ShipType> allowInShipTypes, Action<ulong, Action<bool>> onSelectShip4Strike, List<GrandFaction> validGrandFaction, bool useDefaultBgTask = true, bool allowHireCaptain = true, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StopRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCacheForHireCaptain()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCacheForNormal()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRepatableUserGuideDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateStrikeButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateStrikeShipList(int startIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSwitchButtonState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewForHireCaptain()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateViewForNormal()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetCaptainInfoByWindIndex>c__AnonStorey5
        {
            internal int windIndex;

            internal bool <>m__0(ShipStrikeUITask.CachedHireCaptain info) => 
                (info.Index == this.windIndex);
        }

        [CompilerGenerated]
        private sealed class <OnCaptainIconButtonClick>c__AnonStorey8
        {
            internal ShipStrikeUITask.CachedHireCaptain captain;
            internal ShipStrikeUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.LeaveBackground();
                this.$this.Pause();
                CaptainManagerUITask.StartReturnableCaptainManagerUITask(this.$this.m_currIntent, this.captain.LBHireCaptain, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnShipAutoReloadAmmoButtonClick>c__AnonStorey1
        {
            internal int shipIdx;
            internal ShipStrikeUITask $this;

            internal void <>m__0(Task task)
            {
                AutoReloadAmmoReqNetTask task2 = task as AutoReloadAmmoReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AckResult != 0)
                    {
                        Debug.LogError($"AutoReloadAmmoReqNetTask error:  returnTask.m_ackResult != 0 errorCode = {task2.AckResult}");
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                    }
                    else
                    {
                        this.$this.EnablePipelineStateMask(ShipStrikeUITask.PipeLineStateMaskType.IsReloadAmmo);
                        this.$this.EnablePipelineStateMask(ShipStrikeUITask.PipeLineStateMaskType.IsUpdateCurrSelectShip);
                    }
                    this.$this.OnShipInfoItemClick(this.shipIdx);
                    this.$this.StartUpdatePipeLine(this.$this.m_currIntent, true, false, true, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnShipStrikeButtonClick>c__AnonStorey0
        {
            internal ILBStaticPlayerShip ship;
            internal ShipStrikeUITask $this;

            internal void <>m__0()
            {
                this.$this.ShipForStrike(this.ship);
            }

            internal void <>m__1()
            {
                this.$this.ShipForStrike(this.ship);
            }
        }

        [CompilerGenerated]
        private sealed class <SendBuyAmmoRequest>c__AnonStorey2
        {
            internal HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask netTask;
            internal ShipStrikeUITask $this;

            internal void <>m__0(Task task)
            {
                HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask netTask = this.netTask;
                if (!netTask.IsNetworkError)
                {
                    if (netTask.AckResult != 0)
                    {
                        Debug.LogError($"OnAmmoReloadBuyPanelConfirmButtonClick error: HangarShipBindMoneyBuyAmmoAndRoloadReqNetTask
                        ackResult != 0, errCode= {netTask.AckResult}");
                        TipWindowUITask.ShowTipWindowWithErrorCode(netTask.AckResult, true, false);
                    }
                    else
                    {
                        this.$this.m_buyAmmoReqCount--;
                        if (this.$this.m_buyAmmoReqCount == 0)
                        {
                            this.$this.PlayUIProcess(this.$this.m_autoReloadAmmoBuyPanelUICtrl.CreateBuyWindowProcess(false, false, true, UIProcess.ProcessExecMode.Parallel), true, delegate (UIProcess p, bool r) {
                                this.$this.EnablePipelineStateMask(ShipStrikeUITask.PipeLineStateMaskType.IsUpdateCurrSelectShip);
                                this.$this.StartUpdatePipeLine(null, true, false, true, null);
                            }, false);
                        }
                    }
                }
            }

            internal void <>m__1(UIProcess p, bool r)
            {
                this.$this.EnablePipelineStateMask(ShipStrikeUITask.PipeLineStateMaskType.IsUpdateCurrSelectShip);
                this.$this.StartUpdatePipeLine(null, true, false, true, null);
            }
        }

        [CompilerGenerated]
        private sealed class <SendCaptainSelectDrivingShip>c__AnonStorey6
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                CaptainSetCurrShipReqNetTask task2 = task as CaptainSetCurrShipReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.SetReqResult == 0)
                    {
                        this.onEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.SetReqResult, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendMoveItemToHangarShipItemStore>c__AnonStorey3
        {
            internal ILBStaticPlayerShip staticShip;
            internal ShipStrikeUITask $this;

            internal bool <>m__0(ILBStaticPlayerShip ship) => 
                ReferenceEquals(ship, this.staticShip);

            internal void <>m__1(Task task)
            {
                HangarShipTransformSingleItemFormMSStoreReqNetTask task2 = task as HangarShipTransformSingleItemFormMSStoreReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AckResult == 0)
                    {
                        this.$this.UpdateStrikeButtonState();
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                        Debug.LogError("ShipStrikeUITask:: HangarShipTransformSingleItemFormMSStoreReqNetTask    failed code:" + task2.AckResult);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartCaptainManagerUITask>c__AnonStorey7
        {
            internal bool repairShip;
            internal bool unlockShip;
            internal int shipIndex;
            internal ShipStrikeUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                this.$this.LeaveBackground();
                this.$this.Pause();
                if (this.$this.m_useSpaceStationBgUITask)
                {
                    SpaceStationBGTask.PauseStationBgTask();
                }
                if (this.repairShip || this.unlockShip)
                {
                    this.$this.EnablePipelineStateMask(ShipStrikeUITask.PipeLineStateMaskType.IsUpdateCaptainSelectShipPanel);
                }
                ShipStrikeUITask.CachedHireCaptain captainInfoByWindIndex = this.$this.GetCaptainInfoByWindIndex(this.$this.m_currEditorCaptainIndex);
                if (this.repairShip)
                {
                    CaptainShipManageUITask.StartCaptainShipManagerUITask(captainInfoByWindIndex.LBHireCaptain, this.shipIndex, "Repaire", false, this.$this.m_currIntent, null, null);
                }
                else if (this.unlockShip)
                {
                    CaptainShipManageUITask.StartCaptainShipManagerUITask(captainInfoByWindIndex.LBHireCaptain, this.shipIndex, "UnLock", false, this.$this.m_currIntent, null, null);
                }
                else
                {
                    CaptainManagerUITask.StartReturnableCaptainManagerUITask(this.$this.m_currIntent, null, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateDataCacheForHireCaptain>c__AnonStorey4
        {
            internal LBStaticHiredCaptain captain;
            internal int mostSuitableShipID;

            internal void <>m__0(bool res)
            {
                this.captain.SetCurrShip(this.mostSuitableShipID);
            }
        }

        public class CachedHireCaptain
        {
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private LBStaticHiredCaptain <LBHireCaptain>k__BackingField;
            [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
            private int <Index>k__BackingField;
            private static DelegateBridge _c__Hotfix_ctor;
            private static DelegateBridge __Hotfix_get_LBHireCaptain;
            private static DelegateBridge __Hotfix_set_LBHireCaptain;
            private static DelegateBridge __Hotfix_get_Index;
            private static DelegateBridge __Hotfix_set_Index;

            public CachedHireCaptain()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }

            public LBStaticHiredCaptain LBHireCaptain
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_LBHireCaptain;
                    return ((bridge == null) ? this.<LBHireCaptain>k__BackingField : bridge.__Gen_Delegate_Imp3155(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_LBHireCaptain;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp5(this, value);
                    }
                    else
                    {
                        this.<LBHireCaptain>k__BackingField = value;
                    }
                }
            }

            public int Index
            {
                [CompilerGenerated]
                get
                {
                    DelegateBridge bridge = __Hotfix_get_Index;
                    return ((bridge == null) ? this.<Index>k__BackingField : bridge.__Gen_Delegate_Imp70(this));
                }
                [CompilerGenerated]
                set
                {
                    DelegateBridge bridge = __Hotfix_set_Index;
                    if (bridge != null)
                    {
                        bridge.__Gen_Delegate_Imp31(this, value);
                    }
                    else
                    {
                        this.<Index>k__BackingField = value;
                    }
                }
            }
        }

        protected enum HireCaptainErrorCode
        {
            WithPermission,
            FunctionUnOpened,
            SceneDoNotAllowHireCaptain
        }

        protected enum PipeLineStateMaskType
        {
            IsNeedUpdateDataCache,
            IsNeedUpdateDynamicResource,
            IsUpdateShipList,
            IsUpdateCurrSelectShip,
            IsShowCaptainSelectShipPanel,
            IsUpdateCaptainSelectShipPanel,
            IsReloadAmmo
        }
    }
}

