﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class TeamMainUITask : UITaskBase, CommonStrikeUtil.IStrikeSrcUITask
    {
        public static string m_teamMainUITaskModeNormal = "TeamMainUITaskMode_Normal";
        private TeamMainUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private LogicBlockTeamClient m_lbTeamClient;
        private int m_initWindowFlagIndex;
        private TeamMainUIController.TeamMemberUIItemInfo m_selfUIInfo;
        private List<TeamMainUIController.TeamMemberUIItemInfo> m_otherUIInfoList;
        private IUIBackgroundManager m_backgroundManager;
        public const string ParamKeyBackgroundManager = "BackgroundManager";
        public const string TaskName = "TeamMainUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCahce_ClearBeforeUpdate;
        private static DelegateBridge __Hotfix_UpdateDataCache_NormalMode;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad_AllTeamMemberRes;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_MoveToTeamMemberButtonClick;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_CharacterIconButtonClick;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_ShipIconButtonClick;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_SolarSystemInfoButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnInviteButtonClick;
        private static DelegateBridge __Hotfix_OnLeaveTeamButtonClick;
        private static DelegateBridge __Hotfix_OnTeamStarfieldInfoButtonClick;
        private static DelegateBridge __Hotfix_SetMainWindowState;
        private static DelegateBridge __Hotfix_OnStrikeEnd;
        private static DelegateBridge __Hotfix_BeforeChooseShipForStrike;
        private static DelegateBridge __Hotfix_BeforeEnterSpaceStationUITask;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_CreateSelfTeamMemberUIInfo;
        private static DelegateBridge __Hotfix_CreateTeamMemberUIInfo;
        private static DelegateBridge __Hotfix_OnTeamLeaveSuccessed;
        private static DelegateBridge __Hotfix_OnTeamMemberAddNtf;
        private static DelegateBridge __Hotfix_OnTeamMemberLeaveNtf;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LbTeamClient;

        [MethodImpl(0x8000)]
        public TeamMainUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeChooseShipForStrike(Action<UIIntent, bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void BeforeEnterSpaceStationUITask(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void CollectAllDynamicResForLoad_AllTeamMemberRes(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        private TeamMainUIController.TeamMemberUIItemInfo CreateSelfTeamMemberUIInfo()
        {
        }

        [MethodImpl(0x8000)]
        private TeamMainUIController.TeamMemberUIItemInfo CreateTeamMemberUIInfo(TeamMemberInfo info)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnInviteButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaveTeamButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStrikeEnd(bool success)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamLeaveSuccessed()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberAddNtf(string gameUserId, string userName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_CharacterIconButtonClick(TeamMemberInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_MoveToTeamMemberButtonClick(TeamMemberInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_ShipIconButtonClick(TeamMemberInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_SolarSystemInfoButtonClick(TeamMemberInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberLeaveNtf(string gameUserId, string userName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamStarfieldInfoButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        private void SetMainWindowState(bool isShow = true, Action action = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCache_NormalMode()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateDataCahce_ClearBeforeUpdate()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockTeamClient LbTeamClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <BeforeChooseShipForStrike>c__AnonStorey4
        {
            internal Action<UIIntent, bool> onEnd;
            internal TeamMainUITask $this;

            internal void <>m__0()
            {
                this.$this.Pause();
                this.onEnd(this.$this.CurrentIntent, true);
            }
        }

        [CompilerGenerated]
        private sealed class <OnTeamMemberInfoUI_CharacterIconButtonClick>c__AnonStorey0
        {
            internal CharactorObserveReqNetTas task;
            internal TeamMainUITask $this;

            internal void <>m__0()
            {
                this.$this.Pause();
                CharactorObserverAck charactorObserverAckInfo = this.task.m_charactorObserverAckInfo;
                UIIntentReturnable intent = new UIIntentReturnable(this.$this.m_currIntent, "CharacterInfoUITask", "CharacterUITaskMode_AnotherPlayer");
                intent.SetParam("CharactorObserverAck", charactorObserverAckInfo);
                UIManager.Instance.StartUITask(intent, true, false, null, null);
            }
        }

        [CompilerGenerated]
        private sealed class <OnTeamMemberInfoUI_ShipIconButtonClick>c__AnonStorey1
        {
            internal FakeLBStoreItem fakeShip;
            internal TeamMainUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0()
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnTeamMemberInfoUI_SolarSystemInfoButtonClick>c__AnonStorey2
        {
            internal TeamMemberInfoItemUIController uiCtrl;
            internal TeamMainUITask $this;

            internal void <>m__0()
            {
                this.$this.Pause();
                TeamMemberInfo teamMemberByGameUserId = this.$this.PlayerCtx.GetLBTeamClient().GetTeamMemberByGameUserId(this.uiCtrl.m_teamMemberGameUserId);
                if (teamMemberByGameUserId == null)
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(-1258, true, false);
                    UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                    if (currIntent != null)
                    {
                        UIManager.Instance.ReturnUITask(currIntent.PrevTaskIntent, null);
                    }
                }
                else if (!this.$this.PlayerCtx.GetLBFunctionOpenState().IsFunctionOpen(SystemFuncType.SystemFuncType_StarMap))
                {
                    TipWindowUITask.ShowSystemFunctionNotOpenTipWindow(SystemFuncType.SystemFuncType_StarMap, false);
                    UIIntentReturnable currIntent = this.$this.m_currIntent as UIIntentReturnable;
                    if (currIntent != null)
                    {
                        UIManager.Instance.ReturnUITask(currIntent.PrevTaskIntent, null);
                    }
                }
                else
                {
                    if (this.$this.m_backgroundManager != null)
                    {
                        this.$this.m_backgroundManager.OnLeaveBackground("TeamMainUITask");
                    }
                    if (this.$this.PlayerCtx.CurrSolarSystemId == teamMemberByGameUserId.m_currSolarSystemId)
                    {
                        StarMapManagerUITask.StartStarMapForSolarSystemUITask("StarMapForSolarSystemMode_NormalMode", this.$this.PlayerCtx.CurrSolarSystemId, this.$this.m_currIntent, null, null, null, null);
                    }
                    else
                    {
                        StarMapManagerUITask.StartStarMapForStarfieldUITask(this.$this.m_currIntent, 0, this.$this.PlayerCtx.CurrSolarSystemId, teamMemberByGameUserId.m_currSolarSystemId, GEBrushType.GEBrushType_SecurityLevel, null, 0, false, null, false, 0, 0UL, null);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SetMainWindowState>c__AnonStorey3
        {
            internal Action action;

            internal void <>m__0(UIProcess uiProcess, bool isComplete)
            {
                if (this.action != null)
                {
                    this.action();
                }
            }
        }
    }
}

