﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class HangarShipWeaponEquipSlotPanelUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnHighSlotButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnMiddleSlotButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLowSlotButtonClick;
        private Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict;
        private List<ShipWeaponEquipHighSlotItemUIController> m_highSlotGroupCtrls;
        private List<ShipWeaponEquipSlotItemUIController> m_middleSlotGroupCtrls;
        private ShipWeaponEquipSlotItemUIController m_lowSlotGroupCtrl;
        private const int HighSlotGroupMaxCount = 3;
        private const int MiddleSlotGroupMaxCount = 3;
        private string m_highSlotAssetName;
        private string m_middleAndLowSlotAssetName;
        [AutoBind("./LowSlotGroups/LowSlotCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LowSlotCountText;
        [AutoBind("./HighSlotGroups", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_highSlotGroups;
        [AutoBind("./MiddleSlotGroups", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_middleSlotGroups;
        [AutoBind("./LowSlotGroups/LowSlotGroups", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_lowSlotGroups;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemRoot;
        [AutoBind("./LowSlotGroups/BetterGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController RecommendSlotGroupUIStateCtr;
        [AutoBind("./MarImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject HangarShipWeaponEquipSlotPanelGrayImage;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateShipWeaponEquipSlotPanel;
        private static DelegateBridge __Hotfix_GetCurrentSlotRecommendState;
        private static DelegateBridge __Hotfix_ShowHighSlotEquipedEffect;
        private static DelegateBridge __Hotfix_ShowMiddleSlotEquipedEffect;
        private static DelegateBridge __Hotfix_ShowAssemblyOptimizationHighSlotState;
        private static DelegateBridge __Hotfix_ShowAssemblyOptimizationMiddleSlotState;
        private static DelegateBridge __Hotfix_ShowAssemblyOptimizationLowSlotState;
        private static DelegateBridge __Hotfix_SetGrayImage;
        private static DelegateBridge __Hotfix_GetFirstHighSlotRect;
        private static DelegateBridge __Hotfix_GetHighSlotRectBySlotIndex;
        private static DelegateBridge __Hotfix_GetMiddlewSlotRectBySlotIndex;
        private static DelegateBridge __Hotfix_OnHighSlotButtonClick;
        private static DelegateBridge __Hotfix_OnMiddleSlotButtonClick;
        private static DelegateBridge __Hotfix_OnLowSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnHighSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnHighSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnMiddleSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnMiddleSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLowSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLowSlotButtonClick;

        public event Action<int> EventOnHighSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLowSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnMiddleSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private bool GetCurrentSlotRecommendState(LBStaticWeaponEquipSlotGroup weaponSlotGroup, ILBStaticShip shipInfo, bool isPlayerShip)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstHighSlotRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetHighSlotRectBySlotIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetMiddlewSlotRectBySlotIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnHighSlotButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLowSlotButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMiddleSlotButtonClick(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGrayImage(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAssemblyOptimizationHighSlotState(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState, bool isAll, List<int> indexList = null)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAssemblyOptimizationLowSlotState(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAssemblyOptimizationMiddleSlotState(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState, bool isAll, List<int> indexList = null)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowHighSlotEquipedEffect(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMiddleSlotEquipedEffect(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipWeaponEquipSlotPanel(ILBStaticShip shipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool isPlayerShip = true)
        {
        }
    }
}

