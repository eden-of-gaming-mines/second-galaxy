﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    internal class ShipWeaponEquipHighSlotItemUIController : UIControllerBase
    {
        public CommonItemIconUIController m_commonItemIconUICtrl;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnSlotButtonClick;
        private string UIState_Empty;
        private string UIState_Normal;
        private string m_commonItemAssetName;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./LockEffectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_lockStateCtrl;
        [AutoBind("./EquipedEffect", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_equipedEffectStateCtrl;
        [AutoBind("./CommonSlotItem/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform commonItemUIPrefabDummy;
        [AutoBind("./CommonSlotItem/AddImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image AddImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public Button SlotItemButton;
        [AutoBind("./AmmoProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ProgressBar;
        [AutoBind("./AmmoProgressBar/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text AmmoNumber;
        [AutoBind("./AmmoProgressBar/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image AmmoProgressBar;
        [AutoBind("./HighSlotQuestionImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AssemblyOptimizationStateCtrl;
        private int m_itemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_EnableButtonClick;
        private static DelegateBridge __Hotfix_UpdateSlotGroup;
        private static DelegateBridge __Hotfix_ShowEquipedEffect;
        private static DelegateBridge __Hotfix_ShowAssemblyOptimizationState;
        private static DelegateBridge __Hotfix_OnSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnSlotButtonClick;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;

        public event Action<int> EventOnSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void EnableButtonClick(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSlotButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<int> action)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowAssemblyOptimizationState(bool isShown, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEquipedEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSlotGroup(LBStaticWeaponEquipSlotGroup weaponSlotGroup, ILBStaticShip shipInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict, bool isPlayerShip = true)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }
    }
}

