﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class CommonCountDownWithDaysUIController : UIControllerBase
    {
        private DateTime m_nextUpdateTime;
        private DateTime m_nextUpdateDayTime;
        private DateTime m_expireTime;
        [AutoBind("./TimeDayText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_dayText;
        [AutoBind("./TimeDateText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_timeText;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_UpdateCountDownTime;
        private static DelegateBridge __Hotfix_SetExpireTime;

        [MethodImpl(0x8000)]
        public void SetExpireTime(DateTime experiedTime)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateCountDownTime(bool forceUpdate = false)
        {
        }
    }
}

