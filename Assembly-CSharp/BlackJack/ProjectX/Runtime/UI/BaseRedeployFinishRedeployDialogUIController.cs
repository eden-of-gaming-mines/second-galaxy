﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class BaseRedeployFinishRedeployDialogUIController : BaseRedeployDialogUIControllerBase
    {
        [AutoBind("./InfoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_redeployConfirmInfoText;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        private static DelegateBridge __Hotfix_SetRedeployConfirmInfo;
        private static DelegateBridge __Hotfix_get_CancelButtonName;
        private static DelegateBridge __Hotfix_get_ConfirmButtonName;

        [MethodImpl(0x8000)]
        public void SetRedeployConfirmInfo(string info)
        {
        }

        protected override string CancelButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override string ConfirmButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

