﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public static class BlackMarkUIHelper
    {
        private static DelegateBridge __Hotfix_SendRechargeGiftPackageListReq;
        private static DelegateBridge __Hotfix_SendMonthCardListReq;
        private static DelegateBridge __Hotfix_SendRechargeItemListReq;
        private static DelegateBridge __Hotfix_SendRechargeOrderAcknowledgeReq;
        private static DelegateBridge __Hotfix_GetPackageIdByIndex;
        private static DelegateBridge __Hotfix_GetPackageIndexById;
        private static DelegateBridge __Hotfix_ConvertQuestRewardInfo2FakeLBStoreItem;
        private static DelegateBridge __Hotfix_ShowRechargeOrderInfoPanel;
        private static DelegateBridge __Hotfix_IsGiftPackageCanPurchase;
        private static DelegateBridge __Hotfix_CanOpenBlackMarket;

        [MethodImpl(0x8000)]
        public static bool CanOpenBlackMarket(bool showTips)
        {
        }

        [MethodImpl(0x8000)]
        public static FakeLBStoreItem ConvertQuestRewardInfo2FakeLBStoreItem(QuestRewardInfo rewardInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPackageIdByIndex(List<LBRechargeGiftPackage> packageList, int index)
        {
        }

        [MethodImpl(0x8000)]
        public static int GetPackageIndexById(List<LBRechargeGiftPackage> packageList, int id)
        {
        }

        [MethodImpl(0x8000)]
        public static bool IsGiftPackageCanPurchase(LBRechargeGiftPackage giftPackage)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendMonthCardListReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendRechargeGiftPackageListReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendRechargeItemListReq(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void SendRechargeOrderAcknowledgeReq(ulong orderInstanceId, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowRechargeOrderInfoPanel(ulong orderInstanceId, Action onEnd)
        {
        }

        [CompilerGenerated]
        private sealed class <SendMonthCardListReq>c__AnonStorey1
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                RechargeMonthlyCardListReqNetTask task2 = task as RechargeMonthlyCardListReqNetTask;
                if (task2.IsNetworkError)
                {
                    this.onEnd(false);
                }
                else if (task2.Result == 0)
                {
                    this.onEnd(true);
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    this.onEnd(false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendRechargeGiftPackageListReq>c__AnonStorey0
        {
            internal Action<bool> onEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SendRechargeItemListReq>c__AnonStorey2
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                RechargeItemListReqNetTask task2 = task as RechargeItemListReqNetTask;
                if (task2.IsNetworkError)
                {
                    this.onEnd(false);
                }
                else if (task2.Result == 0)
                {
                    this.onEnd(true);
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    this.onEnd(false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendRechargeOrderAcknowledgeReq>c__AnonStorey3
        {
            internal Action onEnd;

            internal void <>m__0(Task task)
            {
                RechargeOrderAcknowledgeReqNetTask task2 = task as RechargeOrderAcknowledgeReqNetTask;
                if (task2.IsNetworkError)
                {
                    this.onEnd();
                }
                else if (task2.Result == 0)
                {
                    this.onEnd();
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    this.onEnd();
                }
            }
        }
    }
}

