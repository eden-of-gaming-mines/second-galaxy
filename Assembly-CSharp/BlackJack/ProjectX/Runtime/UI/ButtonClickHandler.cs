﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.EventSystems;

    public class ButtonClickHandler : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> OnRightClick;
        private UIControllerBase m_ctrl;
        private static DelegateBridge __Hotfix_OnPointerClick;
        private static DelegateBridge __Hotfix_SetHandler;
        private static DelegateBridge __Hotfix_add_OnRightClick;
        private static DelegateBridge __Hotfix_remove_OnRightClick;

        private event Action<UIControllerBase> OnRightClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void OnPointerClick(PointerEventData eventData)
        {
        }

        [MethodImpl(0x8000)]
        public void SetHandler(Action<UIControllerBase> rightClick, UIControllerBase ctrl)
        {
        }
    }
}

