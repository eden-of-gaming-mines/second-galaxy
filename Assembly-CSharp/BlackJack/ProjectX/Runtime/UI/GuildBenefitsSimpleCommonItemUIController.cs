﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildBenefitsSimpleCommonItemUIController : UIControllerBase
    {
        [AutoBind("./SubRankImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_subRankImage;
        [AutoBind("./CommonItemImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_simpleItemImage;
        private static DelegateBridge __Hotfix_UpdateSimpleItem;

        [MethodImpl(0x8000)]
        public void UpdateSimpleItem(string subRankStr, string itemIconStr, Dictionary<string, Object> resDict)
        {
        }
    }
}

