﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class ChatBGUITask : UITaskBase
    {
        public static string m_chatModeAutoPlay = "AutoPlayVoice";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private DateTime m_playerVoiceStopTime;
        private ChatChannel m_playChannel;
        private ChatUITask m_chatUITask;
        private DateTime m_nextClearChatCacheTime;
        private bool m_isAllianceAutoPlay;
        private bool m_isGuildAutoPlay;
        private bool m_isTeamAutoPlay;
        private readonly LinkedList<ChatInfo> m_unReadVoiceMsgCache;
        private const int VoiceChannels = 1;
        private const int ClearAddTime = 1;
        private const int ClearAddTimeLastClearFail = 5;
        public const string TaskName = "ChatBGUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_CheckLayerDescArray;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_TickForPlayerVoice;
        private static DelegateBridge __Hotfix_RegisterEvent;
        private static DelegateBridge __Hotfix_UnregisterEvent;
        private static DelegateBridge __Hotfix_GetVoiceContentAndPlayVoice;
        private static DelegateBridge __Hotfix_PlayPlayerVoice;
        private static DelegateBridge __Hotfix_OnPlayerVoiceEnd;
        private static DelegateBridge __Hotfix_OnAllianceChatNtf;
        private static DelegateBridge __Hotfix_OnGuildChatNtf;
        private static DelegateBridge __Hotfix_OnTeamChatNtf;
        private static DelegateBridge __Hotfix_SetBGPlayChannel;
        private static DelegateBridge __Hotfix_ClearChatUITaskRef;
        private static DelegateBridge __Hotfix_InitChatUITaskRef;
        private static DelegateBridge __Hotfix_ClearUnReadVoiceCache;
        private static DelegateBridge __Hotfix_AddMsgToUnReadVoiceCache;
        private static DelegateBridge __Hotfix_SetAllianceAutoPlayState;
        private static DelegateBridge __Hotfix_SetGuildAutoPlayState;
        private static DelegateBridge __Hotfix_SetTeamAutoPlayState;
        private static DelegateBridge __Hotfix_GetGuildAutoPlayState;
        private static DelegateBridge __Hotfix_GetAllianceAutoPlayState;
        private static DelegateBridge __Hotfix_GetTeamAutoPlayState;
        private static DelegateBridge __Hotfix_UpdateAutoVoicePlay;
        private static DelegateBridge __Hotfix_ClearChatCache;
        private static DelegateBridge __Hotfix_TickForSendChatMsg;
        private static DelegateBridge __Hotfix_get_PlayerVoiceStopTime;
        private static DelegateBridge __Hotfix_set_PlayerVoiceStopTime;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        [MethodImpl(0x8000)]
        public ChatBGUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void AddMsgToUnReadVoiceCache(ChatChannel channel, List<ChatContentVoice> voiceList)
        {
        }

        [MethodImpl(0x8000)]
        protected override void CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
        {
        }

        [MethodImpl(0x8000)]
        private void ClearChatCache()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearChatUITaskRef()
        {
        }

        [MethodImpl(0x8000)]
        public void ClearUnReadVoiceCache()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetAllianceAutoPlayState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetGuildAutoPlayState()
        {
        }

        [MethodImpl(0x8000)]
        public bool GetTeamAutoPlayState()
        {
        }

        [MethodImpl(0x8000)]
        private void GetVoiceContentAndPlayVoice(ChatContentVoice chatInfo, Action onStartPlayVoice = null)
        {
        }

        [MethodImpl(0x8000)]
        public void InitChatUITaskRef(ChatUITask chatUITask)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnAllianceChatNtf(ChatInfo chatInfo, ChatExtraOutputLocationType extraOutputType)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildChatNtf(ChatInfo chatInfo, ChatExtraOutputLocationType extraOutputType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPlayerVoiceEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamChatNtf(ChatInfo chatInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        private void PlayPlayerVoice(ChatContentVoice chatInfo, Action onStartPlayVoice = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void RegisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllianceAutoPlayState(bool isAutoPlay)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBGPlayChannel(ChatChannel channel)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildAutoPlayState(bool isAutoPlay)
        {
        }

        [MethodImpl(0x8000)]
        public void SetTeamAutoPlayState(bool isAutoPlay)
        {
        }

        [MethodImpl(0x8000)]
        protected void TickForPlayerVoice()
        {
        }

        [MethodImpl(0x8000)]
        private void TickForSendChatMsg()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateAutoVoicePlay()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public DateTime PlayerVoiceStopTime
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetVoiceContentAndPlayVoice>c__AnonStorey0
        {
            internal ChatContentVoice chatInfo;
            internal Action onStartPlayVoice;
            internal ChatBGUITask $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Task task)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateAutoVoicePlay>c__AnonStorey1
        {
            internal ChatInfo voiceChatInfo;
            internal ChatBGUITask $this;

            internal void <>m__0()
            {
                if (this.$this.m_chatUITask != null)
                {
                    this.$this.m_chatUITask.m_scrollViewCtrl.SetCurrPlayChatInfo(this.voiceChatInfo);
                    if (this.$this.m_chatUITask.State == Task.TaskState.Running)
                    {
                        this.$this.m_chatUITask.StopCurrPlayVoiceAnimation();
                        this.$this.m_chatUITask.PlayChatVoiceAnimation(this.voiceChatInfo);
                    }
                }
            }
        }
    }
}

