﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CaptainShipManageUITask : UITaskBase
    {
        private static string UIState_Show;
        private static string UIState_Close;
        private static string UIState_Watch;
        private static string UIState_UnWatch;
        private static string UIState_UnLockOrRepaire;
        public const string TaskModeNormal = "Normal";
        public const string TaskModeUnLock = "UnLock";
        public const string TaskModeRepaire = "Repaire";
        private ShipHangarBGUITask m_bgTask;
        private CaptainListUITask m_captainListTask;
        private CaptainShipUnlockAndRepairUITask m_shipUnlockAndRepairTask;
        private LBStaticHiredCaptain m_selCaptain;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private List<int> m_captainShipStateList;
        private int m_selShipIndex;
        private CaptainShipManageUIController m_mainCtrl;
        private bool m_isLongPressRelead;
        private bool m_isCaptainListUITaskResourceLoadComplete;
        private bool m_isCaptainShipBGTaskResouceLoadComplete;
        public static string ParamKey_SelectedCaptain;
        public static string ParamKey_SelectedShipIndex;
        public static string ParamKey_CaptainManagerIntent;
        private UIIntent m_captainManagerIntent;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CaptainShipManageUITask";
        private bool m_isUnLockShipRepeatGuide;
        public const string ParamKey_UnLockShipRepeatGuide = "UnLockShipRepeatGuide";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartCaptainShipManagerUITask;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_RegisterUIEvents;
        private static DelegateBridge __Hotfix_UnregisterUIEvents;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_InitLayerStateOnResume;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInCaptainListUITask;
        private static DelegateBridge __Hotfix_OnLoadAllResCompletedInCaptainShipBGTask;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainShowButtonClick;
        private static DelegateBridge __Hotfix_OnShowShipListButtonClick;
        private static DelegateBridge __Hotfix_OnLongPressed;
        private static DelegateBridge __Hotfix_OnLongPressRelease;
        private static DelegateBridge __Hotfix_OnLongPressReleaseImpl;
        private static DelegateBridge __Hotfix_OnFunctionButtonClick;
        private static DelegateBridge __Hotfix_OnShipListItemClick;
        private static DelegateBridge __Hotfix_OnCaptainSelectionChanged;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfo;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUItask;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_GetSelectedCaptainFromUIIntent;
        private static DelegateBridge __Hotfix_GetSelectedShipIndexFromIntent;
        private static DelegateBridge __Hotfix_GetParamValueFromIntent;
        private static DelegateBridge __Hotfix_StartShipBGUITask;
        private static DelegateBridge __Hotfix_UpdateShipBGUITask;
        private static DelegateBridge __Hotfix_StartCaptainListUITask;
        private static DelegateBridge __Hotfix_StartSetCurrentShipReqTask;
        private static DelegateBridge __Hotfix_StartUnlockOrRepaireShipUITask;
        private static DelegateBridge __Hotfix_PauseAndCloseAllChildTasks;
        private static DelegateBridge __Hotfix_ReturnFromUnLockAndRepaireUITask;
        private static DelegateBridge __Hotfix_GetSuitableShipListStartIndexFromCurrSelectIndex;
        private static DelegateBridge __Hotfix_SetCurrSystemFuncDescButtonState;
        private static DelegateBridge __Hotfix_CreateShipHangarBgBackGroudManager;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_GetRepeatGuideFromIntent;
        private static DelegateBridge __Hotfix_RepeatGuideStepOne;
        private static DelegateBridge __Hotfix_RepeatGuideStepTwo;
        private static DelegateBridge __Hotfix_GetFristReadyForActivateShip;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_IsCurrSelectedShipHasEnoughItemForActivate;

        [MethodImpl(0x8000)]
        public CaptainShipManageUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private ShipHangarBGUIBackgroundManager CreateShipHangarBgBackGroudManager()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristReadyForActivateShip()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void GetParamValueFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        private void GetRepeatGuideFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        private LBStaticHiredCaptain GetSelectedCaptainFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private int GetSelectedShipIndexFromIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected int GetSuitableShipListStartIndexFromCurrSelectIndex(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerStateOnResume()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCurrSelectedShipHasEnoughItemForActivate()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainSelectionChanged(LBStaticHiredCaptain selCaptain)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainShowButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFunctionButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnItemSimpleInfoUITaskEnterAnotherUItask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadAllResCompletedInCaptainListUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLoadAllResCompletedInCaptainShipBGTask()
        {
        }

        [MethodImpl(0x8000)]
        public void OnLongPressed()
        {
        }

        [MethodImpl(0x8000)]
        public void OnLongPressRelease()
        {
        }

        [MethodImpl(0x8000)]
        public void OnLongPressReleaseImpl()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipListItemClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShowShipListButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void PauseAndCloseAllChildTasks(bool isPauseBg = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        public void RepeatGuideStepOne()
        {
        }

        [MethodImpl(0x8000)]
        public void RepeatGuideStepTwo(bool isClick)
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnFromUnLockAndRepaireUITask(bool captainStateChnage)
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrSystemFuncDescButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfo(ILBStoreItemClient item, Vector3 pos, string mode, ItemSimpleInfoUITask.PositionType posType, bool isShowItemObtainSource)
        {
        }

        [MethodImpl(0x8000)]
        private bool StartCaptainListUITask(LBStaticHiredCaptain selCaptain, Action redirectAction = null)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartCaptainShipManagerUITask(LBStaticHiredCaptain captain, int selectShipIndex, string mode = null, bool isInCaptainUnlockShipRepeatGuide = false, UIIntent returnIntent = null, UIIntent captainMangerIntent = null, Action<bool> onPiplineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartSetCurrentShipReqTask()
        {
        }

        [MethodImpl(0x8000)]
        private bool StartShipBGUITask(LBStaticHiredCaptain selCaptain, Action redirectAction = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartUnlockOrRepaireShipUITask(bool isUnLock)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterUIEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateShipBGUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PauseAndCloseAllChildTasks>c__AnonStorey1
        {
            internal bool isPauseBg;
            internal CaptainShipManageUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.isPauseBg)
                {
                    this.$this.m_bgTask.Pause();
                }
                this.$this.m_captainListTask.Pause();
                if (this.$this.m_shipUnlockAndRepairTask != null)
                {
                    this.$this.m_shipUnlockAndRepairTask.Pause();
                }
                this.$this.Pause();
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey0
        {
            internal bool isTaskResumeOrStart;
            internal CaptainShipManageUITask $this;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.isTaskResumeOrStart)
                {
                    UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_CaptainShipUI, new object[0]);
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            ResumeOrStartTask,
            IsShipSelectionChanged,
            CurrentDrivingShipChanged,
            ReturnFromUnLockOrRepaire,
            IsCaptainSelectionChanged,
            IsShowRepairePanel
        }
    }
}

