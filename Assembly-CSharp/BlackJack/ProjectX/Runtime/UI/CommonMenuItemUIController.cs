﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonMenuItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private object <MenuItemData>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonMenuItemUIController> EventOnMenuItemClick;
        protected CommonMenuTreeInfo m_menuTreeInfo;
        protected CommonMenuItemKey m_menuKey;
        protected CommonMenuItemUIController m_parent;
        protected List<CommonMenuItemUIController> m_childs;
        protected bool m_isExpand;
        protected bool m_isSelect;
        private static DelegateBridge __Hotfix_GenerateSelfMenuKeyList;
        private static DelegateBridge __Hotfix_SetSelect;
        private static DelegateBridge __Hotfix_IsSelected;
        private static DelegateBridge __Hotfix_SetMenuItemExpand;
        private static DelegateBridge __Hotfix_SwitchMenuItemExpand;
        private static DelegateBridge __Hotfix_CreateChildMenuItems;
        private static DelegateBridge __Hotfix_GetChilds;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetMenuLevel;
        private static DelegateBridge __Hotfix_SetMenuIndex;
        private static DelegateBridge __Hotfix_SetParent;
        private static DelegateBridge __Hotfix_OnMenuItemFill;
        private static DelegateBridge __Hotfix_OnMenuItemClick;
        private static DelegateBridge __Hotfix_FreeAllChildMenuItem;
        private static DelegateBridge __Hotfix_FreeChildMenuItem;
        private static DelegateBridge __Hotfix_IsLeafMenuItem;
        private static DelegateBridge __Hotfix_AllocChildMenuItemFromUnusedList;
        private static DelegateBridge __Hotfix_CreateChildMenuItemFormAsset;
        private static DelegateBridge __Hotfix_GetMenuTreeInfo;
        private static DelegateBridge __Hotfix_SwitchMenuItemExpandImp;
        private static DelegateBridge __Hotfix_OnFreeToUnusedPool;
        private static DelegateBridge __Hotfix_OnSelectImp;
        private static DelegateBridge __Hotfix_GetChildMenuItemAssetName;
        private static DelegateBridge __Hotfix_get_MenuItemData;
        private static DelegateBridge __Hotfix_set_MenuItemData;
        private static DelegateBridge __Hotfix_add_EventOnMenuItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnMenuItemClick;
        private static DelegateBridge __Hotfix_get_MenuTreeInfo;
        private static DelegateBridge __Hotfix_get_MenuItemKey;
        private static DelegateBridge __Hotfix_get_IsExpand;

        public event Action<CommonMenuItemUIController> EventOnMenuItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected CommonMenuItemUIController AllocChildMenuItemFromUnusedList()
        {
        }

        [MethodImpl(0x8000)]
        protected CommonMenuItemUIController CreateChildMenuItemFormAsset()
        {
        }

        [MethodImpl(0x8000)]
        public void CreateChildMenuItems(List<object> dataList, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected void FreeAllChildMenuItem()
        {
        }

        [MethodImpl(0x8000)]
        protected void FreeChildMenuItem(CommonMenuItemUIController itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void GenerateSelfMenuKeyList(List<CommonMenuItemKey> menuKeyList)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual string GetChildMenuItemAssetName()
        {
        }

        [MethodImpl(0x8000)]
        public List<CommonMenuItemUIController> GetChilds()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual CommonMenuTreeInfo GetMenuTreeInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void Init()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsLeafMenuItem()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsSelected()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnFreeToUnusedPool()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnMenuItemClick()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnMenuItemFill(object data, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void OnSelectImp()
        {
        }

        [MethodImpl(0x8000)]
        protected void SetMenuIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMenuItemExpand(bool isExpand)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetMenuLevel(int level)
        {
        }

        [MethodImpl(0x8000)]
        protected void SetParent(CommonMenuItemUIController parent)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelect(bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchMenuItemExpand()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SwitchMenuItemExpandImp()
        {
        }

        public object MenuItemData
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public CommonMenuTreeInfo MenuTreeInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public CommonMenuItemKey MenuItemKey
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public bool IsExpand
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public class CommonMenuItemKey
        {
            public int m_menuLevel;
            public int m_menuIndex;
            private static DelegateBridge _c__Hotfix_ctor;
        }

        public class CommonMenuTreeInfo
        {
            public Transform m_unusedMenuItemRoot;
            public Action<CommonMenuItemUIController> m_onMenuItemClick;
            public int m_leafItemLevel;
            public ToggleGroup m_leafItemToggleGroup;
            private static DelegateBridge _c__Hotfix_ctor;

            public CommonMenuTreeInfo()
            {
                DelegateBridge bridge = _c__Hotfix_ctor;
                if (bridge != null)
                {
                    bridge.__Gen_Delegate_Imp1(this);
                }
            }
        }
    }
}

