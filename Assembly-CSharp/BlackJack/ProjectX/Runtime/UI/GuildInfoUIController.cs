﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class GuildInfoUIController : UIControllerBase
    {
        private readonly List<GuildInfoGuildMomentsBriefItemUIController> m_guildMomentsBriefItemUICtrlList;
        public GuildSovereignMaintenanceDesController m_maintenanceDesCtrl;
        private const string StateNormal = "Normal";
        private const string StateGray = "Gray";
        private const int MaxRecentMessageGuildMomentsBriefInfoNumber = 3;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./LeftPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_guildStateCtrl;
        [AutoBind("./LeftPanel/Details/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./LeftPanel/Details/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_shareButton;
        [AutoBind("./LeftPanel/Details/MemberCountText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_memberCountText;
        [AutoBind("./LeftPanel/Details/OccupySolorSystemCountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_occupySolorSystemCountText;
        [AutoBind("./LeftPanel/Details/InfluenceGroup/InfluenceText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_evaluateScoreTipButton;
        [AutoBind("./LeftPanel/Details/InfluenceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_evaluateScoreText;
        [AutoBind("./LeftPanel/Details/LanguageText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_languageText;
        [AutoBind("./LeftPanel/Details/JoinPolicyText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_joinPolicyText;
        [AutoBind("./LeftPanel/Details/OfficeText/NameAndJumpText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_officeLocationText;
        [AutoBind("./LeftPanel/Details/OfficeText/NameAndJumpText", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_officeLocationButton;
        [AutoBind("./LeftPanel/Details/OfficeText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_officeStateCtrl;
        [AutoBind("./LeftPanel/Details/OfficeText/ExplainButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_officeExplainButton;
        [AutoBind("./LeftPanel/Details/ManifestoOrAnnouncementDesc/Viewport/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_manifestoOrAnnouncementDescText;
        [AutoBind("./LeftPanel/ButtonGroup/RelationButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_relationshipButtonStateCtrl;
        [AutoBind("./LeftPanel/ButtonGroup/RelationButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_relationshipButton;
        [AutoBind("./LeftPanel/ButtonGroup/MemberButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_memberButton;
        [AutoBind("./LeftPanel/ButtonGroup/MemberButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_memberButtonStateCtrl;
        [AutoBind("./LeftPanel/ButtonGroup/MemberButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_applyRedPoint;
        [AutoBind("./LeftPanel/ButtonGroup/MessageButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_messageButton;
        [AutoBind("./LeftPanel/ButtonGroup/MessageButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_messageRedPoint;
        [AutoBind("./LeftPanel/ButtonGroup/MessageButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_messageButtonStateCtrl;
        [AutoBind("./LeftPanel/ButtonGroup/DiplomacyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_diplomacyButton;
        [AutoBind("./LeftPanel/ButtonGroup/DiplomacyButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_diplomacyButtonStateCtrl;
        [AutoBind("./LeftPanel/ButtonGroup/ManagementButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_managementButton;
        [AutoBind("./LeftPanel/ButtonGroup/ManagementButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_managementRedPoint;
        [AutoBind("./OfficeIntroducePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_officeIntroducePanelstateCtrl;
        [AutoBind("./OfficeIntroducePanel/BGClick", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_officeExplainPanelBGButton;
        [AutoBind("./SetRelationShipPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_setRelationShipPanelDummy;
        [AutoBind("./RightPanel/RecentMessage/LoadMoreButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_recentMessageButton;
        [AutoBind("./RightPanel/RecentMessage/MessageScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_recentMessageContent;
        [AutoBind("./RightPanel/RecentMessage/MessageScrollView/Viewport/Content/DescInfo", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_recentMessageGuildMomentsBriefItem;
        [AutoBind("./RightPanel/Alliance", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allianceCtrl;
        [AutoBind("./RightPanel/Alliance/NoAlliance/DescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_noAllianceTxt;
        [AutoBind("./RightPanel/Alliance/Alliance/Logo", AutoBindAttribute.InitState.NotInit, false)]
        public GuildLogoController m_allianceLogo;
        [AutoBind("./RightPanel/Alliance/Alliance/Name", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceName;
        [AutoBind("./RightPanel/Alliance/BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_allianceBtn;
        [AutoBind("./RightPanel/Alliance/EstablishAlliance/NumberBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_allianceInvitationNotice;
        [AutoBind("./RightPanel/Alliance/EstablishAlliance/NumberBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_allianceInvitationCountText;
        [AutoBind("./InfluenceDetailInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_evaluateScoreTipPanelStateCtrl;
        [AutoBind("./InfluenceDetailInfoPanel/BGClick", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_evaluateScoreTipPanelBackButton;
        [AutoBind("./LeftPanel/Details/OccupySolorSystemCountGroup/OccupySolorSystemCountText/MaintenanceGroup/MaintenanceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_maintenanceText;
        [AutoBind("./LeftPanel/Details/OccupySolorSystemCountGroup/OccupySolorSystemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_maintenanceDetailBtn;
        [AutoBind("./MaintenanceInfoPanelRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_maintenanceCtrlObj;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateGuideBasicInfo;
        private static DelegateBridge __Hotfix_UpdateGuideRuntimeInfo;
        private static DelegateBridge __Hotfix_ChangeGuideBasicInfo;
        private static DelegateBridge __Hotfix_SetManifestoOrAnnouncementDesc;
        private static DelegateBridge __Hotfix_SetName;
        private static DelegateBridge __Hotfix_SetRelationshipButtonState;
        private static DelegateBridge __Hotfix_SetApplyRedPoint;
        private static DelegateBridge __Hotfix_SetMessageRedPoint;
        private static DelegateBridge __Hotfix_SetManagementRedPoint;
        private static DelegateBridge __Hotfix_GetSetRelationShipPanelPos;
        private static DelegateBridge __Hotfix_UpdateRecentMessageGuildMomentsBriefInfos;
        private static DelegateBridge __Hotfix_GetGuildInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_GetOfficeeExplainPanelUIProcess;
        private static DelegateBridge __Hotfix_GetEvaluateScoreTipPanelUIProcess;
        private static DelegateBridge __Hotfix_SetButtonState;
        private static DelegateBridge __Hotfix_SetGuideMemberCount;
        private static DelegateBridge __Hotfix_SetOccupySolorSystemCount;
        private static DelegateBridge __Hotfix_SetEvaluateScore;
        private static DelegateBridge __Hotfix_SetLanguageType;
        private static DelegateBridge __Hotfix_SetJoinPolicy;
        private static DelegateBridge __Hotfix_SetOfficeLocation;
        private static DelegateBridge __Hotfix_SetInvitationCountNotice;

        [MethodImpl(0x8000)]
        public void ChangeGuideBasicInfo(GuildBasicInfo guildeInfo, bool isSelf, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetEvaluateScoreTipPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetGuildInfoPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetOfficeeExplainPanelUIProcess(bool isShow, bool isImmediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSetRelationShipPanelPos()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetApplyRedPoint(bool display)
        {
        }

        [MethodImpl(0x8000)]
        private void SetButtonState(bool isSelf)
        {
        }

        [MethodImpl(0x8000)]
        private void SetEvaluateScore(int score)
        {
        }

        [MethodImpl(0x8000)]
        private void SetGuideMemberCount(int count, int maxCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetInvitationCountNotice(int count)
        {
        }

        [MethodImpl(0x8000)]
        private void SetJoinPolicy(GuildJoinPolicy policy)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLanguageType(GuildAllianceLanguageType languageType, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetManagementRedPoint(bool display)
        {
        }

        [MethodImpl(0x8000)]
        public void SetManifestoOrAnnouncementDesc(string desc)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMessageRedPoint(bool display)
        {
        }

        [MethodImpl(0x8000)]
        public void SetName(string codeName, string guildName)
        {
        }

        [MethodImpl(0x8000)]
        private void SetOccupySolorSystemCount(int count)
        {
        }

        [MethodImpl(0x8000)]
        private void SetOfficeLocation(int solarSystemId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetRelationshipButtonState(bool isGray)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuideBasicInfo(GuildBasicInfo guildeInfo, bool isSelf, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuideRuntimeInfo(GuildSimpleRuntimeInfo guideInfo, int evaluateScore)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateRecentMessageGuildMomentsBriefInfos(List<GuildMomentsInfo> briefInfos)
        {
        }
    }
}

