﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class SpaceStationBuildingTagController : UIControllerBase
    {
        [AutoBind("./BuildingTagNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingNameTag;
        private static DelegateBridge __Hotfix_SetBuildingName;

        [MethodImpl(0x8000)]
        public void SetBuildingName(string name)
        {
        }
    }
}

