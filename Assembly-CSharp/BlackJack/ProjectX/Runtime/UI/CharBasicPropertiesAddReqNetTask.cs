﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class CharBasicPropertiesAddReqNetTask : NetWorkTransactionTask
    {
        private List<int> m_propertiesList;
        private List<int> m_countList;
        public int m_result;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnBasicPropertiesAddAck;

        [MethodImpl(0x8000)]
        public CharBasicPropertiesAddReqNetTask(List<int> propertiesList, List<int> countList)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBasicPropertiesAddAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }
    }
}

