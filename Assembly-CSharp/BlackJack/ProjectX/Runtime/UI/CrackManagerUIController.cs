﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CrackManagerUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCrackSlotButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnCreackReportItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnCrackSlotCloseButtonClick;
        private List<CrackSlotUIController> m_crackSlotCtrlList;
        private CommonItemIconUIController m_crackingBoxInfoWndIconCtrl;
        private List<CrackRewardItemUIController> m_crackReportItemIconCtrlList;
        private const string CrackState_Cracking = "Cracking";
        private const string CrackState_Cracked = "Cracked";
        private const string CrackState_WaitForCrack = "WaitForCrack";
        private const string PanelOpenState = "Show";
        private const string PanelCloseState = "Close";
        private int m_rewardCount;
        private TimeSpan m_waitForCrackTime;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIStateCtrl;
        [AutoBind("./MainUIRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject MainUIRoot;
        [AutoBind("./MainUIRoot/BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./BackGround", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BackGroudStateCtrl;
        [AutoBind("./MainUIRoot/CrackSlotGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CrackSlotGroup;
        [AutoBind("./MainUIRoot/AllTimeGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AllTimeGroupStateCtrl;
        [AutoBind("./MainUIRoot/AllTimeGroup/AllTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AllTimeText;
        [AutoBind("./MainUIRoot/CrackState/Detail/OpenButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CrackStateOpenButton;
        [AutoBind("./MainUIRoot/CrackState/Detail/OpenButtonWirhMoney", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CrackStateOpenButtonWirhMoney;
        [AutoBind("./MainUIRoot/CrackState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackNoticeStateCtrl;
        [AutoBind("./MainUIRoot/CrackState/Detail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackNoticeDetailStateCtrl;
        [AutoBind("./MainUIRoot/CrackState/Detail/OpenButtonWirhMoney/RealMoneyGroup/RealMoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackRealMoneyText;
        [AutoBind("./MainUIRoot/CrackState/Detail/InfoGroup/TimeGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackTimerText;
        [AutoBind("./MainUIRoot/CrackState/Detail/CrackItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackItemNameText;
        [AutoBind("./UnlockCrackSlotWnd", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UnlockCrackSlotWndCtrl;
        [AutoBind("./UnlockCrackSlotWnd/DetailInfo/InfoGroup/Text2", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnlockSlotText;
        [AutoBind("./UnlockCrackSlotWnd/DetailInfo/InfoGroup/MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text UnlockMoneyText;
        [AutoBind("./UnlockCrackSlotWnd/DetailInfo/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnlockConfirmButton;
        [AutoBind("./UnlockCrackSlotWnd/DetailInfo/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx UnlockReturnButton;
        [AutoBind("./CrackingBoxWnd", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackingBoxInfoWndStateCtrl;
        [AutoBind("./CrackingBoxWnd/DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackingBoxCrackStateCtrl;
        [AutoBind("./CrackingBoxWnd/DetailPanel/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CrackingBoxInfoWndReturnButton;
        [AutoBind("./CrackingBoxWnd/DetailPanel/SpeedUpButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CrackingBoxInfoWndSpeedUpButton;
        [AutoBind("./CrackingBoxWnd/DetailPanel/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CrackingBoxIconDummy;
        [AutoBind("./CrackingBoxWnd/DetailPanel/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackingBoxInfoWndNameText;
        [AutoBind("./CrackingBoxWnd/DetailPanel/TimeGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackingBoxInfoWndRemainTimeText;
        [AutoBind("./CrackingBoxWnd/DetailPanel/WarningTextGroup/MoneyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackingBoxInfoWndSpeedMoneyValText;
        [AutoBind("./CrackGetItemReportWnd", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform CrackReportTransform;
        [AutoBind("./CrackGetItemReportWnd", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackReportWndStateCtrl;
        [AutoBind("./CrackGetItemReportWnd/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CrackReportItemGroup;
        [AutoBind("./CrackGetItemReportWnd/BGButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button CrackReportBGButton;
        [AutoBind("./CrackGetItemReportWnd/LeftDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CrackReportWndLeftDummy;
        [AutoBind("./CrackGetItemReportWnd/RightDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CrackReportWndRightDummy;
        private static DelegateBridge __Hotfix_GetBackGroudStateUIProcess;
        private static DelegateBridge __Hotfix_ShowOrHidePanelWithOutAnim;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_UpdateCrackStateInfo;
        private static DelegateBridge __Hotfix_HideCrackStatePanel;
        private static DelegateBridge __Hotfix_UpdateCrackSlotListInfo;
        private static DelegateBridge __Hotfix_GetCrackSlotBoxIcon;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_ShowUnlockSlotByRealMoneyWnd;
        private static DelegateBridge __Hotfix_ShowCrackingBoxInfoWnd;
        private static DelegateBridge __Hotfix_IsCrackBoxInfoPanelShow;
        private static DelegateBridge __Hotfix_IsCrackBoxReportPanelShow;
        private static DelegateBridge __Hotfix_ShowMainUI;
        private static DelegateBridge __Hotfix_GetSeletedSlotState;
        private static DelegateBridge __Hotfix_SetSlectedSlot;
        private static DelegateBridge __Hotfix_ShowCrackBoxResultWnd;
        private static DelegateBridge __Hotfix_GetCrackReportItemIconCtrl;
        private static DelegateBridge __Hotfix_GetItemSimpleInfoPanelPos;
        private static DelegateBridge __Hotfix_GetCrackReportItemPositionByIndex;
        private static DelegateBridge __Hotfix_GetCrackReportItemSizeByIndex;
        private static DelegateBridge __Hotfix_GetSlotRectByIndex;
        private static DelegateBridge __Hotfix_UpdateWaitForCrackTime;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_CreateCrackReportWindowProcess;
        private static DelegateBridge __Hotfix_CreateUnLockSlotWindowProcess;
        private static DelegateBridge __Hotfix_CreateBoxInfoWindowProcess;
        private static DelegateBridge __Hotfix_OnCrackSlotButtonClick;
        private static DelegateBridge __Hotfix_OnCrackSlotCloseButtonClick;
        private static DelegateBridge __Hotfix_OnCrackReportItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCrackSlotButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCrackSlotButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCreackReportItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCreackReportItemClick;
        private static DelegateBridge __Hotfix_add_EventOnCrackSlotCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCrackSlotCloseButtonClick;
        private static DelegateBridge __Hotfix_get_PlayerCtx;

        public event Action<int> EventOnCrackSlotButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCrackSlotCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnCreackReportItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateBoxInfoWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateCrackReportWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateUnLockSlotWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetBackGroudStateUIProcess(string UIState, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private CrackRewardItemUIController GetCrackReportItemIconCtrl(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCrackReportItemPositionByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetCrackReportItemSizeByIndex(int index)
        {
        }

        [MethodImpl(0x8000)]
        private Sprite GetCrackSlotBoxIcon(int itemIndex, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetItemSimpleInfoPanelPos(float process)
        {
        }

        [MethodImpl(0x8000)]
        public CrackSlotUIController.SlotState GetSeletedSlotState(int index)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetSlotRectByIndex(int slotIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void HideCrackStatePanel(bool hide = true)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCrackBoxInfoPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCrackBoxReportPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackReportItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackSlotButtonClick(CrackSlotUIController crackSlotCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCrackSlotCloseButtonClick(CrackSlotUIController crackSlotCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSlectedSlot(int index)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCrackBoxResultWnd(List<StoreItemUpdateInfo> itemInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowCrackingBoxInfoWnd(int slotIndex, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowMainUI()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowOrHidePanelWithOutAnim(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowUnlockSlotByRealMoneyWnd(int moneyVal, int slotIdx)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCrackSlotListInfo(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCrackStateInfo(ConfigDataCrackBoxInfo config, CrackSlotUIController.SlotState state)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateWaitForCrackTime()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <CreateCrackReportWindowProcess>c__AnonStorey0
        {
            internal bool immediateComplete;
            internal bool allowToRefreshSameState;
            internal CrackManagerUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(Action<bool> onEnd)
            {
            }

            private sealed class <CreateCrackReportWindowProcess>c__AnonStorey1
            {
                internal Action<bool> onEnd;
                internal CrackManagerUIController.<CreateCrackReportWindowProcess>c__AnonStorey0 <>f__ref$0;

                [MethodImpl(0x8000)]
                internal void <>m__0(bool res)
                {
                }
            }
        }
    }
}

