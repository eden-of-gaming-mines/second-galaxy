﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CutScenePlayerUITask : UITaskBase
    {
        private CutScenePlayerUIController m_mainCtrl;
        protected CutSceneAnimationInfo m_currCutSceneInfo;
        protected bool m_isPlaying;
        protected Queue<CutSceneAnimationInfo> m_cachedCutSceneAniamtionList;
        public const string ParamKeyCutSceneAssetName = "CutSceneAssetName";
        public const string ParamKeyRenderCamera = "RenderCamera";
        public const string ParamKeyOnEndFunc = "OnEndFunc";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "CutScenePlayerUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartPlayCutSceneAnimation;
        private static DelegateBridge __Hotfix_StopPlayCutSceneAniamtion;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnCutSceneAniamtionEnd;
        private static DelegateBridge __Hotfix_OnBackgrondButtoneDoubleClick;
        private static DelegateBridge __Hotfix_OnCutSceneAnimationAttached;
        private static DelegateBridge __Hotfix_InitPipeLineCtxStateFromUIIntent;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public CutScenePlayerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPipeLineCtxStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackgrondButtoneDoubleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCutSceneAniamtionEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCutSceneAnimationAttached()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public static CutScenePlayerUITask StartPlayCutSceneAnimation(string cutSceneAssetName, Action onEnd, Camera renderCamera = null)
        {
        }

        [MethodImpl(0x8000)]
        public void StopPlayCutSceneAniamtion()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public class CutSceneAnimationInfo
        {
            public string m_cutSceneAssetName;
            public Action m_onEnd;
            public Camera m_reanderCamera;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

