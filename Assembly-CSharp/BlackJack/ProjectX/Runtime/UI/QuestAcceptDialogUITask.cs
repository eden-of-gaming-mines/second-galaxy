﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class QuestAcceptDialogUITask : UITaskBase
    {
        private NpcTalkerDialogUIController m_npcTalkerUICtrl;
        private QuestDetailUIController m_questDetailUICtrl;
        private LBSignalManualQuest m_unAccpetRandomQuestInfo;
        private NpcDNId m_currDialogNpcDNId;
        private ItemSimpleInfoUITask m_itemSimpleInfoUITask;
        private UnAcceptedQuestUIInfo m_unAcceptNormalQuestInfo;
        private int m_currDialogContextId;
        private UIIntent m_mainUIIntent;
        private bool m_needFadeInOut;
        private bool m_isNeedGreetingAudio;
        public const string QuestDialogCustomParamKey_QuestInfo = "QuestInfo";
        public const string QuestDialogCustomParamKey_UnAccpetQuestUIInfo = "QuestId";
        public const string QuestDialogCustomParamKey_NeedFadeInOut = "NeedFadeInOut";
        public const string QuestDialogCustomParamKey_MainUIIntent = "MainUIIntent";
        public const string QuestDialogCustomParamKey_NeedGreetingAudio = "GreetingAudio";
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRequestToQuestAccept;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnRequestToQuestRefuse;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnNeedFadeInOutForDialogClose;
        public const string UIMode_RandomQuestAccept = "UIModeRandomQuestAccept";
        public const string UIMode_NormalQuestAccept = "UIModeNormalQuestAccept";
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "QuestAcceptDialogUITask";
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_InitParamsFromIntent;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_OnLoadDynamicResCompleted;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_StartLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForQuest;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerState;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_UpdateRandomQuestDialogInfo;
        private static DelegateBridge __Hotfix_UpdateNormalQuestDialogInfo;
        private static DelegateBridge __Hotfix_OnDialogOptionButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_ReturnToMainUI;
        private static DelegateBridge __Hotfix_OnNPCTalkerBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnQuestDetailBackGroundButtonClick;
        private static DelegateBridge __Hotfix_OnQuestRewardItemClick;
        private static DelegateBridge __Hotfix_OnQuestRewardItem3DTouch;
        private static DelegateBridge __Hotfix_ReturnFromItemDetailTask;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskEnterAnotherUITask;
        private static DelegateBridge __Hotfix_OnItemSimpleInfoUITaskRetrunFromAnotherUITask;
        private static DelegateBridge __Hotfix_OnQuestRewardBonusButtonClick;
        private static DelegateBridge __Hotfix_ClickFirstDialogOptionButton;
        private static DelegateBridge __Hotfix_StartFullscreenAction;
        private static DelegateBridge __Hotfix_StopFullscreenAction;
        private static DelegateBridge __Hotfix_GetRealAcceptDialogNpcDNId;
        private static DelegateBridge __Hotfix_GetCurrentNpcTalkerInfo;
        private static DelegateBridge __Hotfix_CloseItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowItemSimpleInfoPanel;
        private static DelegateBridge __Hotfix_ShowAllUILayer;
        private static DelegateBridge __Hotfix_SendRandomQuestAcceptReq;
        private static DelegateBridge __Hotfix_SendNormalQuestAcceptReq;
        private static DelegateBridge __Hotfix_SendFactionQuestAcceptReq;
        private static DelegateBridge __Hotfix_OnQuestRefused;
        private static DelegateBridge __Hotfix_StartVirtualBuffDetailUITask;
        private static DelegateBridge __Hotfix_GetQuestDetailPanelOpenProcess;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_PlayNpcGreetingAudio;
        private static DelegateBridge __Hotfix_PlayNpcLeaveTalkAudio;
        private static DelegateBridge __Hotfix_add_EventOnRequestToQuestAccept;
        private static DelegateBridge __Hotfix_remove_EventOnRequestToQuestAccept;
        private static DelegateBridge __Hotfix_add_EventOnRequestToQuestRefuse;
        private static DelegateBridge __Hotfix_remove_EventOnRequestToQuestRefuse;
        private static DelegateBridge __Hotfix_add_EventOnNeedFadeInOutForDialogClose;
        private static DelegateBridge __Hotfix_remove_EventOnNeedFadeInOutForDialogClose;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_ShowRandomQuestAcceptDialog;
        private static DelegateBridge __Hotfix_ShowRandomQuestAcceptDialogImpl;
        private static DelegateBridge __Hotfix_ShowNormalQuestAcceptDialog;
        private static DelegateBridge __Hotfix_ShowNormalQuestAcceptDialogImpl;
        private static DelegateBridge __Hotfix_ShowFactionQuestAcceptDialog;
        private static DelegateBridge __Hotfix_ShowFactionQuestAcceptDialogImpl;

        public event Action EventOnNeedFadeInOutForDialogClose
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRequestToQuestAccept
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnRequestToQuestRefuse
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public QuestAcceptDialogUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFirstDialogOptionButton()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItemSimpleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private List<string> CollectAllDynamicResForQuest(ConfigDataQuestInfo confQuestInfo, List<QuestRewardInfo> rewardList, int level)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private ConfigDataNpcTalkerInfo GetCurrentNpcTalkerInfo(out bool isGlobalNpc)
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess GetQuestDetailPanelOpenProcess(ConfigDataQuestInfo questConfig, int level)
        {
        }

        [MethodImpl(0x8000)]
        private NpcDNId GetRealAcceptDialogNpcDNId()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitLayerState()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParamsFromIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDialogOptionButtonClick(int optionIdx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskEnterAnotherUITask(string taskName)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSimpleInfoUITaskRetrunFromAnotherUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnLoadDynamicResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNPCTalkerBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestDetailBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnQuestRefused(Action questRefuseAction)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardBonusButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardItem3DTouch(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnQuestRewardItemClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void PlayNpcGreetingAudio()
        {
        }

        [MethodImpl(0x8000)]
        protected void PlayNpcLeaveTalkAudio()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnFromItemDetailTask(Task retTask)
        {
        }

        [MethodImpl(0x8000)]
        private void ReturnToMainUI(Action onReturnFinished = null)
        {
        }

        [MethodImpl(0x8000)]
        public void SendFactionQuestAcceptReq(Action<int, int> onQuestAccepted)
        {
        }

        [MethodImpl(0x8000)]
        public void SendNormalQuestAcceptReq(Action<int, int> onQuestAccepted)
        {
        }

        [MethodImpl(0x8000)]
        public void SendRandomQuestAcceptReq(Action<int, int> onQuestAccepted)
        {
        }

        [MethodImpl(0x8000)]
        protected void ShowAllUILayer(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowFactionQuestAcceptDialog(UIIntent mainIntent, UnAcceptedQuestUIInfo questUIInfo, Action<int, int> onAcceptedAction = null, Action onRefuseAction = null, bool isNeedGreetingAudio = true)
        {
        }

        [MethodImpl(0x8000)]
        public static QuestAcceptDialogUITask ShowFactionQuestAcceptDialogImpl(UIIntent mainIntent, UnAcceptedQuestUIInfo unAcceptedQuestUIInfo, Action<int, int> onAcceptedAction = null, Action onRefuseAction = null, bool needFadeInOut = false, bool isNeedGreetingAudio = true)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItemSimpleInfoPanel(ItemInfo itemInfo)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowNormalQuestAcceptDialog(UIIntent mainIntent, UnAcceptedQuestUIInfo questUIInfo, Action<int, int> onAcceptedAction = null, Action onRefuseAction = null, bool isNeedGreetingAudio = true)
        {
        }

        [MethodImpl(0x8000)]
        public static QuestAcceptDialogUITask ShowNormalQuestAcceptDialogImpl(UIIntent mainIntent, UnAcceptedQuestUIInfo unAcceptedQuestUIInfo, Action<int, int> onAcceptedAction = null, Action onRefuseAction = null, bool needFadeInOut = false, bool isNeedGreetingAudio = true)
        {
        }

        [MethodImpl(0x8000)]
        public static void ShowRandomQuestAcceptDialog(UIIntent mainIntent, LBSignalManualQuest questInfo, Action<int, int> onAcceptedAction = null, Action onRefuseAction = null, bool isNeedGreetingAudio = true)
        {
        }

        [MethodImpl(0x8000)]
        public static QuestAcceptDialogUITask ShowRandomQuestAcceptDialogImpl(UIIntent mainIntent, LBSignalManualQuest questInfo, Action<int, int> onAcceptedAction = null, Action onRefuseAction = null, bool needFadeInOut = false, bool isNeedGreetingAudio = true)
        {
        }

        [MethodImpl(0x8000)]
        private void StartFullscreenAction()
        {
        }

        [MethodImpl(0x8000)]
        protected override void StartLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private List<VirtualBuffDesc> StartVirtualBuffDetailUITask(ConfigDataQuestInfo questConfigInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void StopFullscreenAction()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateNormalQuestDialogInfo()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateRandomQuestDialogInfo()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <GetQuestDetailPanelOpenProcess>c__AnonStorey7
        {
            internal int solarSyatemId;
            internal ConfigDataQuestInfo questConfig;
            internal int level;
            internal QuestAcceptDialogUITask $this;

            internal void <>m__0(Action<bool> end)
            {
                <GetQuestDetailPanelOpenProcess>c__AnonStorey8 storey = new <GetQuestDetailPanelOpenProcess>c__AnonStorey8 {
                    <>f__ref$7 = this,
                    end = end
                };
                GetDataForVirtuelBuffReqNetTask task = new GetDataForVirtuelBuffReqNetTask(this.solarSyatemId, false);
                task.EventOnStop += new Action<Task>(storey.<>m__0);
                task.Start(null, null);
            }

            private sealed class <GetQuestDetailPanelOpenProcess>c__AnonStorey8
            {
                internal Action<bool> end;
                internal QuestAcceptDialogUITask.<GetQuestDetailPanelOpenProcess>c__AnonStorey7 <>f__ref$7;

                internal void <>m__0(Task task)
                {
                    if (!((GetDataForVirtuelBuffReqNetTask) task).IsNetworkError && (this.<>f__ref$7.$this.State == Task.TaskState.Running))
                    {
                        this.<>f__ref$7.$this.m_questDetailUICtrl.UpdateQuestDetail(this.<>f__ref$7.questConfig, this.<>f__ref$7.$this.m_dynamicResCacheDict, this.<>f__ref$7.level);
                        this.<>f__ref$7.$this.m_questDetailUICtrl.SetPanelShow(true, false, this.end);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnQuestRewardBonusButtonClick>c__AnonStorey1
        {
            internal ConfigDataQuestInfo configData;
            internal UIControllerBase ctrl;
            internal int level;
            internal QuestAcceptDialogUITask $this;

            internal void <>m__0(Task task)
            {
                if (!((GetDataForVirtuelBuffReqNetTask) task).IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    List<VirtualBuffDesc> virtualBuffList = this.$this.StartVirtualBuffDetailUITask(this.configData);
                    ((QuestDetailUIController) this.ctrl).SetQuestReward(this.configData, this.$this.m_dynamicResCacheDict, virtualBuffList, this.level);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ReturnToMainUI>c__AnonStorey0
        {
            internal Action onReturnFinished;
            internal QuestAcceptDialogUITask $this;

            internal void <>m__0(UIProcess process, bool result)
            {
                UIIntent mainUIIntent = this.$this.m_mainUIIntent;
                this.$this.PlayNpcLeaveTalkAudio();
                this.$this.StopFullscreenAction();
                this.$this.Stop();
                UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_OnNpcDialogEnd, new object[0]);
                if (mainUIIntent != null)
                {
                    UIManager.Instance.ReturnUITask(mainUIIntent, null);
                }
                if (this.onReturnFinished != null)
                {
                    this.onReturnFinished();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendFactionQuestAcceptReq>c__AnonStorey5
        {
            internal Action<int, int> onQuestAccepted;
            internal QuestAcceptDialogUITask $this;

            internal void <>m__0()
            {
                if (this.onQuestAccepted != null)
                {
                    this.onQuestAccepted(0, 0);
                }
            }

            internal void <>m__1(int result, int questInsId)
            {
                <SendFactionQuestAcceptReq>c__AnonStorey6 storey = new <SendFactionQuestAcceptReq>c__AnonStorey6 {
                    <>f__ref$5 = this,
                    result = result,
                    questInsId = questInsId
                };
                if (storey.result == 0)
                {
                    TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_AcceptQuestTip, new object[0]), false);
                }
                if (this.$this.State == Task.TaskState.Running)
                {
                    this.$this.ReturnToMainUI(new Action(storey.<>m__0));
                }
            }

            private sealed class <SendFactionQuestAcceptReq>c__AnonStorey6
            {
                internal int result;
                internal int questInsId;
                internal QuestAcceptDialogUITask.<SendFactionQuestAcceptReq>c__AnonStorey5 <>f__ref$5;

                internal void <>m__0()
                {
                    if (this.<>f__ref$5.onQuestAccepted != null)
                    {
                        this.<>f__ref$5.onQuestAccepted(this.result, this.questInsId);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendNormalQuestAcceptReq>c__AnonStorey3
        {
            internal Action<int, int> onQuestAccepted;
            internal QuestAcceptDialogUITask $this;

            internal void <>m__0()
            {
                if (this.onQuestAccepted != null)
                {
                    this.onQuestAccepted(0, 0);
                }
            }

            internal void <>m__1(int result, int questInsId)
            {
                <SendNormalQuestAcceptReq>c__AnonStorey4 storey = new <SendNormalQuestAcceptReq>c__AnonStorey4 {
                    <>f__ref$3 = this,
                    result = result,
                    questInsId = questInsId
                };
                if (storey.result == 0)
                {
                    TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_AcceptQuestTip, new object[0]), false);
                }
                this.$this.ReturnToMainUI(new Action(storey.<>m__0));
            }

            private sealed class <SendNormalQuestAcceptReq>c__AnonStorey4
            {
                internal int result;
                internal int questInsId;
                internal QuestAcceptDialogUITask.<SendNormalQuestAcceptReq>c__AnonStorey3 <>f__ref$3;

                internal void <>m__0()
                {
                    if (this.<>f__ref$3.onQuestAccepted != null)
                    {
                        this.<>f__ref$3.onQuestAccepted(this.result, this.questInsId);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendRandomQuestAcceptReq>c__AnonStorey2
        {
            internal Action<int, int> onQuestAccepted;
            internal QuestAcceptDialogUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.PlayNpcLeaveTalkAudio();
                this.$this.StopFullscreenAction();
                RandomQuestAcceptReqNetTask task2 = task as RandomQuestAcceptReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.AcceptQuestResult == 0)
                    {
                        TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_AcceptQuestTip, new object[0]), false);
                    }
                    this.$this.Stop();
                    UserGuideUITask.ExecuteUserGuideTriggerPoint(this.$this, UserGuideTriggerPoint.UserGuideTriggerPoint_OnNpcDialogEnd, new object[0]);
                    if (this.onQuestAccepted != null)
                    {
                        this.onQuestAccepted(task2.AcceptQuestResult, task2.ProcessingQuestInstanceId);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowFactionQuestAcceptDialogImpl>c__AnonStoreyB
        {
            internal QuestAcceptDialogUITask questDialogUITask;
            internal Action<int, int> onAcceptedAction;
            internal Action onRefuseAction;

            internal void <>m__0()
            {
                this.questDialogUITask.SendFactionQuestAcceptReq(this.onAcceptedAction);
            }

            internal void <>m__1()
            {
                this.questDialogUITask.OnQuestRefused(this.onRefuseAction);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowNormalQuestAcceptDialogImpl>c__AnonStoreyA
        {
            internal QuestAcceptDialogUITask questDialogUITask;
            internal Action<int, int> onAcceptedAction;
            internal Action onRefuseAction;

            internal void <>m__0()
            {
                this.questDialogUITask.SendNormalQuestAcceptReq(this.onAcceptedAction);
            }

            internal void <>m__1()
            {
                this.questDialogUITask.OnQuestRefused(this.onRefuseAction);
            }
        }

        [CompilerGenerated]
        private sealed class <ShowRandomQuestAcceptDialogImpl>c__AnonStorey9
        {
            internal QuestAcceptDialogUITask questDialogUITask;
            internal Action<int, int> onAcceptedAction;
            internal Action onRefuseAction;

            internal void <>m__0()
            {
                this.questDialogUITask.SendRandomQuestAcceptReq(this.onAcceptedAction);
            }

            internal void <>m__1()
            {
                this.questDialogUITask.OnQuestRefused(this.onRefuseAction);
            }
        }

        protected enum PipeLineStateMaskType
        {
            IsShowNextDialog,
            IsTheLastDialog
        }
    }
}

