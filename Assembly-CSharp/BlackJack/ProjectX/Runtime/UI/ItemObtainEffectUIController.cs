﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ItemObtainEffectUIController : UIControllerBase
    {
        private const string SHOW_STATE = "Show";
        private const string CLOSE_STATE = "Close";
        private List<CommonUIStateController> m_inStationItemList;
        private List<CommonUIStateController> m_inSpaceItemList;
        private List<ItemData> m_waitingForDisplayList;
        public float m_fullClearTime_InSpace;
        public float m_fullClearTime_InStation;
        public bool m_isInSpace;
        public float m_showItemIntervalTime;
        private float m_lastShowItemTime;
        private int m_curIndex;
        private int m_curCount;
        private float m_lastFullTime;
        private const string m_poolName = "ItemObtainEffectUIPool";
        public Action m_filledEvent;
        [AutoBind("./inStation", AutoBindAttribute.InitState.NotInit, false)]
        public Transform inStationNode;
        [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
        private RectTransform m_marginTransform;
        [AutoBind("./Margin/inSpace", AutoBindAttribute.InitState.NotInit, false)]
        public Transform inSpaceNode;
        [AutoBind("./easyObjectPool", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool objectPool;
        private static DelegateBridge __Hotfix_AddItemObtainEffectData;
        private static DelegateBridge __Hotfix_InitCtrollerData;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_CloseShow;
        private static DelegateBridge __Hotfix_CloseItem;
        private static DelegateBridge __Hotfix_ShowItem;
        private static DelegateBridge __Hotfix_Start;
        private static DelegateBridge __Hotfix_OnPoolObjectCreated;
        private static DelegateBridge __Hotfix_Update;

        [MethodImpl(0x8000)]
        public void AddItemObtainEffectData(bool m_isInSpace, StoreItemType itemType, int itemID, Dictionary<string, UnityEngine.Object> m_dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void CloseItem()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseShow()
        {
        }

        [MethodImpl(0x8000)]
        public void InitCtrollerData(ItemObtainEffectUIData itemData)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPoolObjectCreated(string namee, GameObject obj)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowItem()
        {
        }

        [MethodImpl(0x8000)]
        private void Start()
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [CompilerGenerated]
        private sealed class <CloseShow>c__AnonStorey0
        {
            internal CommonUIStateController one;
            internal ItemObtainEffectUIController $this;

            internal void <>m__0(bool bo)
            {
                for (int i = 0; i < this.one.transform.childCount; i++)
                {
                    this.$this.objectPool.ReturnObjectToPool(this.one.transform.GetChild(i).gameObject, true);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <CloseShow>c__AnonStorey1
        {
            internal CommonUIStateController one;
            internal ItemObtainEffectUIController $this;

            internal void <>m__0(bool bo)
            {
                for (int i = 0; i < this.one.transform.childCount; i++)
                {
                    this.$this.objectPool.ReturnObjectToPool(this.one.transform.GetChild(i).gameObject, true);
                }
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct ItemData
        {
            public GameObject m_item;
            public bool m_isInSpace;
            public ItemData(GameObject m_item, bool m_isInSpace)
            {
                this.m_item = m_item;
                this.m_isInSpace = m_isInSpace;
            }
        }
    }
}

