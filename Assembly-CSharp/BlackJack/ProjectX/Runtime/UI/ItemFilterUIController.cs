﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.UI;

    public class ItemFilterUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBackGroundButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<AuctionBuyItemFilterType> EventOnFilterChange;
        private Dictionary<int, CommonUIStateController> m_buttonStateDic;
        private bool m_isShow;
        private AuctionBuyItemFilterType m_currAuctionFilter;
        public const string SortPanelStateNormal = "Normal";
        public const string SortPanelStateOnlyTech = "Shade";
        public const string SortPanelStateWithOutSize = "Shade01";
        public const string SortPanelStateWithOutTech = "Shade01";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./BackGroundImge", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_backGroundButton;
        [AutoBind("./FrameImage/BGImage/SortPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStatePanelMode;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/All", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_allRankFilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/T1", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_rankT1FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/T2", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_rankT2FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/T3", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_rankT3FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/All", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_allSubRankFilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R1", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_subRankR1FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R2", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_subRankR2FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R3", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_subRankR3FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R4", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_subRankR4FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R5", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_subRankR5FilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/All", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_allSizeFilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/S", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sizeSFilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/M", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sizeMFilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/L", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sizeLFilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/N", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_sizeNFilterButton;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/All", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allRankFilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/T1", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rankT1FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/T2", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rankT2FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/RankGroup/T3", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_rankT3FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/All", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allSubRankFilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R1", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankR1FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R2", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankR2FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R3", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankR3FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R4", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankR4FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SubRankGroup/R5", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_subRankR5FilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/All", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_allSizeFilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/S", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sizeSFilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/M", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sizeMFilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/L", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sizeLFilterStateCtrl;
        [AutoBind("./FrameImage/BGImage/SortPanel/SizeGroup/N", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sizeNFilterStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_HasItemFilterType;
        private static DelegateBridge __Hotfix_GetFilterUIProcess;
        private static DelegateBridge __Hotfix_SetAllFilterButtonState;
        private static DelegateBridge __Hotfix_IsFilterPanelShow;
        private static DelegateBridge __Hotfix_GetItemRankFilter;
        private static DelegateBridge __Hotfix_GetItemSubRankFilter;
        private static DelegateBridge __Hotfix_GetItemSizeFilter;
        private static DelegateBridge __Hotfix_SetFilterButtonState_0;
        private static DelegateBridge __Hotfix_SetFilterButtonState_1;
        private static DelegateBridge __Hotfix_ChangeSigleFilter;
        private static DelegateBridge __Hotfix_SetNewFilterByChangingFilter;
        private static DelegateBridge __Hotfix_OnAllRankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnT1RankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnT2RankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnT3RankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnAllSubRankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnR1SubRankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnR2SubRankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnR3SubRankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnR4SubRankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnR5SubRankFilterButtonClick;
        private static DelegateBridge __Hotfix_OnAllSizeFilterButtonClick;
        private static DelegateBridge __Hotfix_OnNSizeFilterButtonClick;
        private static DelegateBridge __Hotfix_OnSSizeFilterButtonClick;
        private static DelegateBridge __Hotfix_OnMSizeFilterButtonClick;
        private static DelegateBridge __Hotfix_OnLSizeFilterButtonClick;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_add_EventOnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnFilterChange;
        private static DelegateBridge __Hotfix_remove_EventOnFilterChange;

        public event Action EventOnBackGroundButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<AuctionBuyItemFilterType> EventOnFilterChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private bool ChangeSigleFilter(AuctionBuyItemFilterType auctionBuyItemFilter, AuctionBuyItemFilterType allCurrFilterType)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetFilterUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState, string panelState = "Normal")
        {
        }

        [MethodImpl(0x8000)]
        public static AuctionBuyItemFilterType GetItemRankFilter(RankType itemRank)
        {
        }

        [MethodImpl(0x8000)]
        public static AuctionBuyItemFilterType GetItemSizeFilter(ShipSizeType shipSizeType)
        {
        }

        [MethodImpl(0x8000)]
        public static AuctionBuyItemFilterType GetItemSubRankFilter(SubRankType itemSubRank)
        {
        }

        [MethodImpl(0x8000)]
        public static bool HasItemFilterType(AuctionBuyItemFilterType sourceFilterType, AuctionBuyItemFilterType containFilterType)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFilterPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllRankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllSizeFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnAllSubRankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBackGroundClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLSizeFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMSizeFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnNSizeFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnR1SubRankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnR2SubRankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnR3SubRankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnR4SubRankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnR5SubRankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSSizeFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnT1RankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnT2RankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnT3RankFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllFilterButtonState(AuctionBuyItemFilterType filterType)
        {
        }

        [MethodImpl(0x8000)]
        private void SetFilterButtonState(AuctionBuyItemFilterType filterType, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        private void SetFilterButtonState(List<AuctionBuyItemFilterType> filterTypeList, bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        private bool SetNewFilterByChangingFilter(AuctionBuyItemFilterType auctionBuyItemFilter)
        {
        }
    }
}

