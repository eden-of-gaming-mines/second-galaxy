﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopItemListUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<NpcShopListItemUIController> EventOnNpcShipItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NpcShopListItemUIController> EventOnNpcShipItemDoubleClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<NpcShopListItemUIController> EventOnNpcShipItemIconClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<NpcShopListItemUIController> EventOnNpcShipItemIcon3DTouch;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<NpcShopListItemUIController> EventOnNpcShipItemLongPressStart;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnItemListScrollViewClick;
        public GameObject m_itemMoveUIPrefab;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollViewEventHandler ItemListScrollViewEventHandler;
        [AutoBind("./UnusedCommonIconRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_unusedCommonIconRoot;
        [AutoBind("./UsedCommonIconRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_usedCommonIconRoot;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_npcShopItemListCtrl;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_scrollRect;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_itemObjPool;
        private List<NpcShopMainUITask.NpcShopItemUIInfo> m_shopItemList;
        private Dictionary<string, UnityEngine.Object> m_shopItemResDict;
        private bool m_needShowTechDissatisfyObj;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_GetItemMoveUIPrefab;
        private static DelegateBridge __Hotfix_SetItemMoveUIToUnusedState;
        private static DelegateBridge __Hotfix_SetItemMoveUIToUsedState;
        private static DelegateBridge __Hotfix_SetShipItemListState;
        private static DelegateBridge __Hotfix_UpdateNpcShopItemList;
        private static DelegateBridge __Hotfix_GetCurrListStartIndex;
        private static DelegateBridge __Hotfix_GetFristItemgameObject;
        private static DelegateBridge __Hotfix_OnShopListItemClick;
        private static DelegateBridge __Hotfix_OnShopListItemDoubleClick;
        private static DelegateBridge __Hotfix_OnShopListItemFill;
        private static DelegateBridge __Hotfix_OnShopListItemIconClick;
        private static DelegateBridge __Hotfix_OnShopListItemIcon3DTouch;
        private static DelegateBridge __Hotfix_OnNpcShipItemLongPressStart;
        private static DelegateBridge __Hotfix_OnItemListScrollViewClick;
        private static DelegateBridge __Hotfix_add_EventOnNpcShipItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnNpcShipItemClick;
        private static DelegateBridge __Hotfix_add_EventOnNpcShipItemDoubleClick;
        private static DelegateBridge __Hotfix_remove_EventOnNpcShipItemDoubleClick;
        private static DelegateBridge __Hotfix_add_EventOnNpcShipItemIconClick;
        private static DelegateBridge __Hotfix_remove_EventOnNpcShipItemIconClick;
        private static DelegateBridge __Hotfix_add_EventOnNpcShipItemIcon3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnNpcShipItemIcon3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnNpcShipItemLongPressStart;
        private static DelegateBridge __Hotfix_remove_EventOnNpcShipItemLongPressStart;
        private static DelegateBridge __Hotfix_add_EventOnItemListScrollViewClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemListScrollViewClick;

        public event Action EventOnItemListScrollViewClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopListItemUIController> EventOnNpcShipItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopListItemUIController> EventOnNpcShipItemDoubleClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopListItemUIController> EventOnNpcShipItemIcon3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopListItemUIController> EventOnNpcShipItemIconClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<NpcShopListItemUIController> EventOnNpcShipItemLongPressStart
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public int GetCurrListStartIndex()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetFristItemgameObject()
        {
        }

        [MethodImpl(0x8000)]
        public GameObject GetItemMoveUIPrefab()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListScrollViewClick()
        {
        }

        [MethodImpl(0x8000)]
        private void OnNpcShipItemLongPressStart(NpcShopListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopListItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopListItemDoubleClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShopListItemFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopListItemIcon3DTouch(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShopListItemIconClick(NpcShopListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemMoveUIToUnusedState(GameObject itemMoveUI)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemMoveUIToUsedState(GameObject itemMoveUI)
        {
        }

        [MethodImpl(0x8000)]
        public void SetShipItemListState(bool isEmpty)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNpcShopItemList(List<NpcShopMainUITask.NpcShopItemUIInfo> itemList, bool isShowTechDissatisfyObj, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }
    }
}

