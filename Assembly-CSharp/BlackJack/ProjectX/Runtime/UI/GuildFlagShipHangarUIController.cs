﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipHangarUIController : UIControllerBase
    {
        private UIMode m_currMode;
        private bool m_isRenamePanelShow;
        private bool m_isShipPackPanelShow;
        private static string PanelStateOpen;
        private static string PanelStateOpenNoShip;
        private static string PanelStateNoShip;
        private static string PanelStateHaveShip;
        private static string PanelStateClose;
        private static string PanelStateCloseNoShip;
        private static string LockingStateLocked;
        private static string LockingStateUnlocked;
        private static string LockingStateNoPermission;
        private const string ShowState = "Show";
        private const string CloseState = "Close";
        private const string CommonItemAssetName = "CommonItem";
        [AutoBind("./ShipPackPanel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PackCancelButton;
        [AutoBind("./ShipPackPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx PackConfirmButton;
        [AutoBind("./ShipPackPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipPackPanelCommonUIStateController;
        [AutoBind("./ShipWeaponEquipSlotGroup/FlagShipEnergyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FlagShipEnergyButton;
        [AutoBind("./ShipWeaponEquipSlotGroup/FlagShipPackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx FlagShipPackButton;
        [AutoBind("./TitleText/LockButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx LockButton;
        [AutoBind("./TitleText/LockButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController LockButtonCommonUIStateController;
        [AutoBind("./TitleText/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx HangarListButton;
        [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text TitleText;
        [AutoBind("./ShipUniqueWeaponEquipTipWindows", AutoBindAttribute.InitState.NotInit, false)]
        public HangarShipUniqueWeaponEquipTipWndUIController m_shipUniqueWeaponEquipTipWindowUICtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button BackGroundButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./ShipRenamePanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipRenameConfirmButton;
        [AutoBind("./ShipRenamePanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ShipRenameCancelButton;
        [AutoBind("./ShipRenamePanel/ShipNameInputField", AutoBindAttribute.InitState.NotInit, false)]
        public InputField ShipRenameInputField;
        [AutoBind("./ShipRenamePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ShipRenameStateCtrl;
        [AutoBind("./CommonEquipInfoPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommonEquipInfoPanelDummy;
        private static DelegateBridge __Hotfix_GetUIMode;
        private static DelegateBridge __Hotfix_IsRenamePanelShow;
        private static DelegateBridge __Hotfix_IsShipPackPanelShow;
        private static DelegateBridge __Hotfix_CheckShipRenameLegal;
        private static DelegateBridge __Hotfix_GetShipRenameStr;
        private static DelegateBridge __Hotfix_EnableBackGroundButton;
        private static DelegateBridge __Hotfix_GetWeaponEquipInfoPanelDummyLocation;
        private static DelegateBridge __Hotfix_UpdateGuildFlagShipHangarName;
        private static DelegateBridge __Hotfix_SetUIMode;
        private static DelegateBridge __Hotfix_CreateMainWndWindowProcess;
        private static DelegateBridge __Hotfix_ShowOrHideUniqueEquipWeaponTip;
        private static DelegateBridge __Hotfix_HideUniqueEquipWeaponTip;
        private static DelegateBridge __Hotfix_ShowOrHideShipPackPanel;
        private static DelegateBridge __Hotfix_UpdateLockState;
        private static DelegateBridge __Hotfix_ShowShipRenamePanel;

        [MethodImpl(0x8000)]
        public bool CheckShipRenameLegal()
        {
        }

        [MethodImpl(0x8000)]
        private UIProcess CreateMainWndWindowProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState, bool isCustomProcess = false)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableBackGroundButton(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public string GetShipRenameStr()
        {
        }

        [MethodImpl(0x8000)]
        public UIMode GetUIMode()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetWeaponEquipInfoPanelDummyLocation()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess HideUniqueEquipWeaponTip()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsRenamePanelShow()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsShipPackPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess SetUIMode(UIMode mode, bool immediateComplete, bool allowToRefreshSameState, bool isCustomProcess = false)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHideShipPackPanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowOrHideUniqueEquipWeaponTip(HangarShipUniqueWeaponEquipTipWndUIController.UniqueWeaponEquipTipState state, ILBStaticFlagShip staticShip, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess ShowShipRenamePanel(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFlagShipHangarName(IGuildFlagShipHangarDataContainer hangar)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateLockState(bool hasPermission, bool isLocked)
        {
        }

        [CompilerGenerated]
        private sealed class <CreateMainWndWindowProcess>c__AnonStorey0
        {
            internal string stateName;
            internal bool immediateComplete;
            internal bool allowToRefreshSameState;
            internal GuildFlagShipHangarUIController $this;

            internal void <>m__0(Action<bool> end)
            {
                this.$this.m_stateCtrl.SetToUIStateExtra(this.stateName, this.immediateComplete, this.allowToRefreshSameState, end);
            }
        }

        public enum UIMode
        {
            OpenAll,
            HideAll,
            OpenAllNoShip,
            HideAllNoShip
        }
    }
}

