﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class CaptainUITask : UITaskBase
    {
        public const string TaskMode_Init = "InitMode";
        public const string TaskMode_Training = "TrainingMode";
        public const string TaskMode_Retire = "RetireMode";
        public const string TaskMode_LearningFeat = "LearningMode";
        public const string TaskMode_SetWingMan = "SetIntervene";
        public const string PipeLineCtxTaskNotifyDescInitShowEnd = "OnInitShowEnd";
        private const string PanelState_Show = "Show";
        private const string PanelState_Close = "Close";
        private const string PanelState_ShowBackButtonOnly = "Show_OnlyBackButton";
        public const string TaskName = "CaptainUITask";
        private CaptainUIController m_mainCtrl;
        private CaptainConfirmPanelUIController m_confirmPanelCtrl;
        private LBStaticHiredCaptain m_selCaptain;
        private LBNpcCaptainFeats m_selCaptainFeat;
        private List<ILBStoreItemClient> m_featsBookList;
        private int m_selFeatsBookIndex;
        private int m_selFeatsBookListIndex;
        private bool m_isUpdateWingManList;
        private LBNpcCaptainFeats m_preCaptainFeat;
        private List<LBStaticHiredCaptain> m_wingManList;
        private int m_selWingManIndex;
        private int m_changedWingManIndex;
        public static string ParamKey_SelectedCaptain = "SelectedCaptain";
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCaptainShipButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<Action<bool>> EventOnCaptainShipButtonClickOnUserGuide;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCaptainTrainingButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<Action<bool>> EventOnCaptainTrainingButtonClickOnUserGuide;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnUpgradeButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCaptainRetireButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCaptainLearnFeatButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCaptainWingManButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<LBNpcCaptainFeats> EventOnCaptainFeatItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnBackButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBGButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ILBStoreItemClient, Vector3> EventOnWingManShipItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnWndShowAnimEnd;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private List<FunctionOpenStateUtil.FunctionOpenStateDisplayInfo> m_functionOpenStateDisplayInfos;
        private const string LockLayerName = "CaptainFunctionLockLayer";
        private const string SetWingManName = "SetWingMan";
        private const string CaptainTrainingName = "CaptainTraining";
        private const string CaptainLearningName = "CaptainLearning";
        private const string CaptainRetireName = "CaptainRetire";
        [CompilerGenerated]
        private static Func<ILBStoreItemClient, bool> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_HideCaptainUI;
        private static DelegateBridge __Hotfix_BringLayerToTop;
        private static DelegateBridge __Hotfix_BringConfirmPanelLayerToTop;
        private static DelegateBridge __Hotfix_SwitchToMode;
        private static DelegateBridge __Hotfix_UpdateSelectedCaptainInfo;
        private static DelegateBridge __Hotfix_GetTrianButtonRect;
        private static DelegateBridge __Hotfix_GetShipShowButton;
        private static DelegateBridge __Hotfix_GetTaskMode;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateUI;
        private static DelegateBridge __Hotfix_SetShipStateButton;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnShipButtonClick;
        private static DelegateBridge __Hotfix_OnBGButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_OnFeatsBookItemClick;
        private static DelegateBridge __Hotfix_OnFeatsBookItemClickImp;
        private static DelegateBridge __Hotfix_OnFeatBookStoreItemFilled;
        private static DelegateBridge __Hotfix_OnCaptainTrainingButtonClick;
        private static DelegateBridge __Hotfix_OnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainWingManButtonClick_1;
        private static DelegateBridge __Hotfix_OnCaptainTrainButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainShipButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainWingManButtonClick_0;
        private static DelegateBridge __Hotfix_OnCaptainWingManItemClick;
        private static DelegateBridge __Hotfix_OnCaptainRetireButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainLearnFeatButtonClick;
        private static DelegateBridge __Hotfix_OnCaptainLearnFeatButtonClickImp;
        private static DelegateBridge __Hotfix_OnFeatsBookLearnButtonClick;
        private static DelegateBridge __Hotfix_OnFeatsBookCloseButtonClick;
        private static DelegateBridge __Hotfix_OnWingManReplaceCancelButtonClick;
        private static DelegateBridge __Hotfix_OnWingManReplaceConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnLearningCancelButtonClick;
        private static DelegateBridge __Hotfix_OnLearningConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnWingManShipItemClick;
        private static DelegateBridge __Hotfix_OnHiredCaptainFeatsUpdateNtf;
        private static DelegateBridge __Hotfix_GetDataCacheFromUIIntent;
        private static DelegateBridge __Hotfix_IsStoreItemFeatsBook;
        private static DelegateBridge __Hotfix_GetFeatsBookSelectedIndex;
        private static DelegateBridge __Hotfix_SendSetWingManNetworkReq;
        private static DelegateBridge __Hotfix_SendFeatBookLearnOrUpgradeReq;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnCaptainShipButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainShipButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainShipButtonClickOnUserGuide;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainShipButtonClickOnUserGuide;
        private static DelegateBridge __Hotfix_add_EventOnCaptainTrainingButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainTrainingButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainTrainingButtonClickOnUserGuide;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainTrainingButtonClickOnUserGuide;
        private static DelegateBridge __Hotfix_add_EventOnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnUpgradeButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainRetireButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainRetireButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainLearnFeatButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainLearnFeatButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainWingManButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainWingManButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnCaptainFeatItemClick;
        private static DelegateBridge __Hotfix_add_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBackButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnWingManShipItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnWingManShipItemClick;
        private static DelegateBridge __Hotfix_add_EventOnWndShowAnimEnd;
        private static DelegateBridge __Hotfix_remove_EventOnWndShowAnimEnd;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_UpdateFunctionOpenStateInview;
        private static DelegateBridge __Hotfix_InitFunctionOpenStateDisplayInfos;
        private static DelegateBridge __Hotfix_EnableSetWingManUI;
        private static DelegateBridge __Hotfix_UnLockSetWingManUI;
        private static DelegateBridge __Hotfix_EnableCaptainTrainingUI;
        private static DelegateBridge __Hotfix_UnLockCaptainTrainingUI;
        private static DelegateBridge __Hotfix_EnableCaptainLearningUI;
        private static DelegateBridge __Hotfix_UnLockCaptainLearningUI;
        private static DelegateBridge __Hotfix_EnableCaptainRetireUI;
        private static DelegateBridge __Hotfix_UnLockCaptainRetireUI;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_IsCaptainCanSetWingManForUserGuide;
        private static DelegateBridge __Hotfix_IsExistFeatBook;
        private static DelegateBridge __Hotfix_IsFeatBookCanLearn;
        private static DelegateBridge __Hotfix_GetFristFeatBookItem;
        private static DelegateBridge __Hotfix_ClickLearnigButton;
        private static DelegateBridge __Hotfix_ClickFristFeatBook;

        public event Action EventOnBackButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnBGButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<LBNpcCaptainFeats> EventOnCaptainFeatItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCaptainLearnFeatButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCaptainRetireButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCaptainShipButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<bool>> EventOnCaptainShipButtonClickOnUserGuide
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCaptainTrainingButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<Action<bool>> EventOnCaptainTrainingButtonClickOnUserGuide
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCaptainWingManButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnUpgradeButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ILBStoreItemClient, Vector3> EventOnWingManShipItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnWndShowAnimEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CaptainUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void BringConfirmPanelLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        public void BringLayerToTop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        public void ClickFristFeatBook(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void ClickLearnigButton(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnableCaptainLearningUI(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableCaptainRetireUI(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableCaptainTrainingUI(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void EnableSetWingManUI(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        protected void GetDataCacheFromUIIntent()
        {
        }

        [MethodImpl(0x8000)]
        protected int GetFeatsBookSelectedIndex(int festId, int festLv)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFristFeatBookItem()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetShipShowButton()
        {
        }

        [MethodImpl(0x8000)]
        public string GetTaskMode()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetTrianButtonRect()
        {
        }

        [MethodImpl(0x8000)]
        public void HideCaptainUI(Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitFunctionOpenStateDisplayInfos()
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsCaptainCanSetWingManForUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsExistFeatBook()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFeatBookCanLearn()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsStoreItemFeatsBook(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBGButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainFeatItemClick(int featId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainLearnFeatButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCaptainLearnFeatButtonClickImp(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainRetireButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainShipButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainTrainButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainTrainingButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainWingManButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainWingManButtonClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnCaptainWingManItemClick(int index, WingManItemButtonState m_state)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFeatBookStoreItemFilled(ILBStoreItemClient item, CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFeatsBookCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFeatsBookItemClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFeatsBookItemClickImp(int idx, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OnFeatsBookLearnButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnHiredCaptainFeatsUpdateNtf(ulong captainInstanseId)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLearningCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLearningConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnShipButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public void OnUpgradeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnWingManReplaceCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnWingManReplaceConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnWingManShipItemClick(ILBStoreItemClient ship, Vector3 pos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void SendFeatBookLearnOrUpgradeReq()
        {
        }

        [MethodImpl(0x8000)]
        private void SendSetWingManNetworkReq(int dstIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShipStateButton()
        {
        }

        [MethodImpl(0x8000)]
        public void SwitchToMode(string strMode, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockCaptainLearningUI(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockCaptainRetireUI(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockCaptainTrainingUI(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        private void UnLockSetWingManUI(Action onEndAction)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateFunctionOpenStateInview()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelectedCaptainInfo(LBStaticHiredCaptain selCaptain)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideCaptainUI>c__AnonStorey0
        {
            internal Action onEnd;

            internal void <>m__0(UIProcess p, bool r)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendSetWingManNetworkReq>c__AnonStorey1
        {
            internal int srcIndex;
            internal int dstIndex;
            internal CaptainUITask $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true, true);
                HiredCaptainSetWingManReqNetTask task2 = task as HiredCaptainSetWingManReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if (task2.CaptainSetWingManResult != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.CaptainSetWingManResult, true, false);
                    }
                    else
                    {
                        if (this.srcIndex == -1)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_CaptainWingMan_SetWingMan, new object[0]), false);
                        }
                        else if (this.dstIndex == -1)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_CaptainWingMan_CancelWingMan, new object[0]), false);
                        }
                        this.$this.EnablePipelineStateMask(CaptainUITask.PipeLineStateMaskType.UpdateWingManState);
                        this.$this.StartUpdatePipeLine(null, false, false, true, null);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            SelectedCaptainChanged,
            SelecteFeat,
            CaptainDetailVisibleUpdate,
            UpdateFeatsBookStore,
            SelecteFeatsBook,
            UpdateSingleFestsBook,
            IsCaptainLearnOrUpgradeFeats,
            IsBGButtonClick,
            IsWingManItemSelectChanged,
            UpdateWingManState,
            IsShowWingManSetConfirm,
            IsShowFeatBookLearnConfirmPanel,
            IsShowFeatBookList,
            IsShowCaptainSetWingManAnimation
        }
    }
}

