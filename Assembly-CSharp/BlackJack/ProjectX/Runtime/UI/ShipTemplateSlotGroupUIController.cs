﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ShipTemplateSlotGroupUIController : CommonMenuItemUIController
    {
        public ShipTemplateSlotItemUIController m_firstSlotItemCtrl;
        public ShipTemplateSlotItemUIController m_secondSlotItemCtrl;
        private ShipEquipSlotType m_slotType;
        private int m_index;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, ShipEquipSlotType, int> EventOnItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, NpcShopItemType, int> EventOnItemBuyClick;
        [AutoBind("./LinkImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LinkImage;
        private static DelegateBridge __Hotfix_UpdateShipTemplateSlotGroup;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnItemBuyClick;
        private static DelegateBridge __Hotfix_SetSlotInfo;
        private static DelegateBridge __Hotfix_add_EventOnItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemBuyClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemBuyClick;

        public event Action<UIControllerBase, NpcShopItemType, int> EventOnItemBuyClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, ShipEquipSlotType, int> EventOnItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemBuyClick(UIControllerBase ctrl, NpcShopItemType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSlotInfo(ShipEquipSlotType type, int index)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipTemplateSlotGroup(ILBStoreItemClient firstItem, int firstItemNeedCount, int totalFristItemNeedCount, bool isFirstItemBuyFromMall, ILBStoreItemClient secondItem, int secondItemNeedCount, int totalSecondItemNeedCount, bool isSecondItemBuyFromMall, Dictionary<string, UnityEngine.Object> assetDict)
        {
        }
    }
}

