﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class VirtualBuffDetailUITask : UITaskBase
    {
        public static Vector3 m_defultPos;
        public static Vector2 m_centerPivot;
        public static Vector2 m_downPivot;
        public static Vector2 m_upPivot;
        public const string ParamKeyVirtualBuffList = "VirtualBuffList";
        public const string ParamKeyUIPos = "UIPos";
        public const string ParamKeyUIPivot = "Pivot";
        private List<VirtualBuffDesc> m_virtualBuffDescs;
        public const string TaskStateAccpetQuest = "AccpetQuest";
        public const string TaskStateUnAccpetQuest = "UnAccpetQuest";
        public const string TaskStateDelegateMission = "DelegateMission";
        public const string TaskStateSolarSystem = "SolarSystem";
        private Vector3 m_wantedPosition;
        private Vector2 m_wantedPivot;
        private VirtualBuffDetailUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "VirtualBuffDetailUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartSolarSystemBuffDetailUITask;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnBackGroundButtonClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public VirtualBuffDetailUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartSolarSystemBuffDetailUITask(List<VirtualBuffDesc> buffList, string mode, Vector3 pos, Vector2 pivot)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

