﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class SolarSystemBuffNoticeItemUIController : UIControllerBase
    {
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buffNoticeStateCtrl;
        [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buffNoticeTypeCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitBuffNoticeUIItem;
        private static DelegateBridge __Hotfix_HideBuffNotice;
        private static DelegateBridge __Hotfix_SetBuffTypeInfo;

        [MethodImpl(0x8000)]
        public void HideBuffNotice(Action<bool> onHideFinished)
        {
        }

        [MethodImpl(0x8000)]
        public void InitBuffNoticeUIItem(BuffTypeUIInfo buffInfo, Vector3 showPos, Vector3 hidePos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetBuffTypeInfo(ShipFightBufType bufType, bool isEnhance)
        {
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct BuffTypeUIInfo
        {
            public ShipFightBufType m_bufType;
            public bool m_isEnhance;
        }
    }
}

