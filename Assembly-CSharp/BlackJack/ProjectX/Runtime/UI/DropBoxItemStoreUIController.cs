﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DropBoxItemStoreUIController : UIControllerBase
    {
        public ItemStoreListUIController m_itemStoreListUICtrl;
        public ItemSimpleInfoUIController m_itemSimpleInfoUICtrl;
        [AutoBind("./ItemStorePanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemStoreNameText;
        [AutoBind("./ItemStorePanel/ItemStoreListUIPrefabDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemStoreListPanelDummy;
        [AutoBind("./ItemSimpleInfoUIPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemSimpleInfoUIPanelDummy;
        [AutoBind("./ItemStorePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStorePanelStateCtrl;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroundButton;
        private const string OpenPanel = "PanelOpen";
        private const string ClosePanel = "PanelClose";
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetDropBoxItemStoreTitleName;
        private static DelegateBridge __Hotfix_ShowOrHideUI;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDropBoxItemStoreTitleName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess ShowOrHideUI(bool isShow)
        {
        }
    }
}

