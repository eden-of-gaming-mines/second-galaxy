﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ProductCategoryController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> OnSelectetType;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<ProductCategoryController> WillOpen;
        public Transform m_subMenuRoot;
        private ProductTypeController m_subMenuTemplate;
        public List<ProductTypeController> m_subMenues;
        private List<int> m_types;
        private bool m_isSelected;
        private bool m_isOpen;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsUse>k__BackingField;
        private const string Open = "Open";
        private const string Close = "Close";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_statCtrl;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_btn;
        [AutoBind("./ToggleImageDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_icon;
        [AutoBind("./CategoryNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        private static DelegateBridge __Hotfix_SetToUse;
        private static DelegateBridge __Hotfix_SetOpen;
        private static DelegateBridge __Hotfix_SetOnSelectListener;
        private static DelegateBridge __Hotfix_SetWillOpenListener;
        private static DelegateBridge __Hotfix_CheckIsSelected;
        private static DelegateBridge __Hotfix_add_OnSelectetType;
        private static DelegateBridge __Hotfix_remove_OnSelectetType;
        private static DelegateBridge __Hotfix_add_WillOpen;
        private static DelegateBridge __Hotfix_remove_WillOpen;
        private static DelegateBridge __Hotfix_get_IsSelected;
        private static DelegateBridge __Hotfix_set_IsSelected;
        private static DelegateBridge __Hotfix_get_IsOpen;
        private static DelegateBridge __Hotfix_set_IsOpen;
        private static DelegateBridge __Hotfix_get_IsUse;
        private static DelegateBridge __Hotfix_set_IsUse;

        public event Action<int> OnSelectetType
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ProductCategoryController> WillOpen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void CheckIsSelected(int selectedType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOnSelectListener(Action<int> listener)
        {
        }

        [MethodImpl(0x8000)]
        public void SetOpen(bool isOpen)
        {
        }

        [MethodImpl(0x8000)]
        public void SetToUse(bool isUse, NpcShopItemCategory category, List<int> typeList = null, Dictionary<string, UnityEngine.Object> resDict = null, int selectedType = 0)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWillOpenListener(Action<ProductCategoryController> listener)
        {
        }

        public bool IsSelected
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsOpen
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            set
            {
            }
        }

        public bool IsUse
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

