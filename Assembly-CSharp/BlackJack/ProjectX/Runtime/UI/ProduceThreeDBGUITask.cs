﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ProduceThreeDBGUITask : UITaskBase
    {
        private bool m_isGuildMode;
        private ProduceLineState m_currState;
        private int m_blueprintId;
        private Sprite m_iconSprite;
        private bool m_isProduceShip;
        private string m_spritePath;
        private ProduceThreeDBGUIController m_selfProduceBGCtrl;
        private ProduceThreeDBGUIController m_guildProduceBGCtrl;
        private const string ProduceConsoleModelPath = "Assets/GameProject/RuntimeAssets/Scenes/Buildings/Manufacturing_workshop_ABS/Manufacturing_Workshop_Pfb.prefab";
        private const string ProduceConsoleModelPathForGuild = "Assets/GameProject/RuntimeAssets/Scenes/Buildings/Manufacturing_workshop_ABS/GuildManufacturing_Workshop_Pfb.prefab";
        private const string ParamsKey_ModelState = "3dModelState";
        private const string ParamsKey_BlueprintId = "BlueprintId";
        public const string ParamsKey_UIMode = "PanelUIMode";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "ProduceThreeDBGUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_StartProduce3DBackgroundUITask;
        private static DelegateBridge __Hotfix_UpdateThreeDBGModelView;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
        private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_IsInitEnd4SelfProduce;
        private static DelegateBridge __Hotfix_IsInitEnd4GuildProduce;
        private static DelegateBridge __Hotfix_get_MainLayer;
        private static DelegateBridge __Hotfix_get_MainCtrl;
        private static DelegateBridge __Hotfix_get_CurrIntent;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ProduceThreeDBGUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInitEnd4GuildProduce()
        {
        }

        [MethodImpl(0x8000)]
        public bool IsInitEnd4SelfProduce()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadStaticRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static ProduceThreeDBGUITask StartProduce3DBackgroundUITask(ProduceLineState state, int bluePrintId, bool isGuildMode, Action action)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateThreeDBGModelView(ProduceLineState state, int blueprintId)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override SceneLayerBase MainLayer
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public ProduceThreeDBGUIController MainCtrl
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public UIIntentCustom CurrIntent
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

