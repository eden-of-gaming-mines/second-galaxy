﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TeamMainUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCloseButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnInviteButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnLeaveTeamButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnTeamStarfieldInfoButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUICharacterInfoButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUIShipInfoButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUISolarSystemInfoButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUIMoveToPlayerPosButtonClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_mainStateCtrl;
        [AutoBind("./TeamStarfieldInfo", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_teamStarfieldInfoButton;
        [AutoBind("./TeamStarfieldNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_teamStarfieldNameText;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./InviteButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_inviteButton;
        [AutoBind("./LeaveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_leaveTeamButton;
        [AutoBind("./SelfPlayerInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_selfPlayerInfoTrans;
        [AutoBind("./OtherTeamPlayersInfoRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_otherTeamPlayersTrans;
        [AutoBind("./TeamPlayerInfoItemTempRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_teamPlayerInfoItemTempTrans;
        private GameObject m_teamPlayerInfoItemTempObj;
        private TeamMemberInfoItemUIController m_selfInfoUICtrl;
        private List<TeamMemberInfoItemUIController> m_otherInfoUICtrlList;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateSelfTeamMemberUIInfo;
        private static DelegateBridge __Hotfix_UpdateOtherTeamMemberUIInfoList;
        private static DelegateBridge __Hotfix_SetStarfieldName;
        private static DelegateBridge __Hotfix_CreatMainWondonEffectInfo;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnInviteButtonClick;
        private static DelegateBridge __Hotfix_OnLeaveTeamButtonClick;
        private static DelegateBridge __Hotfix_OnTeamStarfieldInfoButtonClick;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_CharacterInfoButtonClick;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_ShipInfoButtonClick;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_SolarSystemInfoButtonClick;
        private static DelegateBridge __Hotfix_OnTeamMemberInfoUI_MoveToPlayerPosButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnInviteButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnInviteButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLeaveTeamButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLeaveTeamButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamStarfieldInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamStarfieldInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamMemberInfoUICharacterInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamMemberInfoUICharacterInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamMemberInfoUIShipInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamMemberInfoUIShipInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamMemberInfoUISolarSystemInfoButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamMemberInfoUISolarSystemInfoButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnTeamMemberInfoUIMoveToPlayerPosButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnTeamMemberInfoUIMoveToPlayerPosButtonClick;
        private static DelegateBridge __Hotfix_UpdateTeamMemberUIItemInfo;
        private static DelegateBridge __Hotfix_RegEventForTeamMemberUIItem;

        public event Action EventOnCloseButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnInviteButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnLeaveTeamButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUICharacterInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUIMoveToPlayerPosButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUIShipInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<TeamMemberInfoItemUIController> EventOnTeamMemberInfoUISolarSystemInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnTeamStarfieldInfoButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIPlayingEffectInfo CreatMainWondonEffectInfo(bool isShow)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnInviteButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnLeaveTeamButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_CharacterInfoButtonClick(TeamMemberInfoItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_MoveToPlayerPosButtonClick(TeamMemberInfoItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_ShipInfoButtonClick(TeamMemberInfoItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamMemberInfoUI_SolarSystemInfoButtonClick(TeamMemberInfoItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTeamStarfieldInfoButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void RegEventForTeamMemberUIItem(TeamMemberInfoItemUIController uiCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStarfieldName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateOtherTeamMemberUIInfoList(List<TeamMemberUIItemInfo> infoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSelfTeamMemberUIInfo(TeamMemberUIItemInfo info, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateTeamMemberUIItemInfo(TeamMemberInfoItemUIController uiCtrl, TeamMemberUIItemInfo info, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        public class TeamMemberUIItemInfo
        {
            public string m_gameUserId;
            public string m_characterIconPath;
            public string m_characterName;
            public int m_characterLevel;
            public string m_shipIconPath;
            public string m_shipTypeIconPath;
            public string m_shipBgIconPath;
            public string m_shipName;
            public string m_shipTypeName;
            public RankType m_shipRank;
            public string m_solarSystemName;
            public bool m_isInSpace;
            public string m_professionIconPath;
            public uint m_guildId;
            public string m_guildCodeName;
            private static DelegateBridge _c__Hotfix_ctor;
        }
    }
}

