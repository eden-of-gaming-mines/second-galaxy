﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class AuctionUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<AuctionBuyItemFilterType> EventOnFilterChange;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnFilterPanelBackGroundButtonClick;
        private ItemFilterUIController m_itemFilterUICtrl;
        private SystemDescriptionController m_descriptionCtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_closeButton;
        [AutoBind("./BuyAndSellToggleGroup/BuyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_buyButton;
        [AutoBind("./BuyAndSellToggleGroup/BuyToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_buyButtonState;
        [AutoBind("./BuyAndSellToggleGroup/SellToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sellButton;
        [AutoBind("./BuyAndSellToggleGroup/SellToggle", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sellButtonState;
        [AutoBind("./BuyAndSellToggleGroup/SellToggle/RedImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sellButtonRedPoint;
        [AutoBind("./SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_sortButton;
        [AutoBind("./SortButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_sortButtonState;
        [AutoBind("./SearchItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_searchItemGroup;
        [AutoBind("./SMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tradeMoneyCount;
        [AutoBind("./CMoneyGroup/MoneyNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_bindMoneyCount;
        [AutoBind("./SMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_tradeAddButton;
        [AutoBind("./CMoneyGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bindMoneyButton;
        [AutoBind("./SellItemPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_sellPanel;
        [AutoBind("./BuyItemPanel", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_buyPanel;
        [AutoBind("./FilterPanelDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_filterPanelDummy;
        [AutoBind("./StockoutGroup/CheckGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_showZeroItemButtonStateCtrl;
        [AutoBind("./StockoutGroup/CheckGroup/FrameButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_showZeroItemButton;
        [AutoBind("./RefreshButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_refreshButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetMoney;
        private static DelegateBridge __Hotfix_SetPanelState;
        private static DelegateBridge __Hotfix_SetBuyButtonState;
        private static DelegateBridge __Hotfix_SetSellButtonState;
        private static DelegateBridge __Hotfix_SetSortButtonState;
        private static DelegateBridge __Hotfix_SetAllFilterButtonState;
        private static DelegateBridge __Hotfix_IsFilterPanelShow;
        private static DelegateBridge __Hotfix_SetSellItemRedPoint;
        private static DelegateBridge __Hotfix_ShowZeroItem;
        private static DelegateBridge __Hotfix_SetCurrSystemFuncDescType;
        private static DelegateBridge __Hotfix_OnFilterChange;
        private static DelegateBridge __Hotfix_OnFilterPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_GetFilterPanelUIProcess;
        private static DelegateBridge __Hotfix_add_EventOnFilterChange;
        private static DelegateBridge __Hotfix_remove_EventOnFilterChange;
        private static DelegateBridge __Hotfix_add_EventOnFilterPanelBackGroundButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnFilterPanelBackGroundButtonClick;

        public event Action<AuctionBuyItemFilterType> EventOnFilterChange
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnFilterPanelBackGroundButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetFilterPanelUIProcess(bool isShow, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetMainUIProcess(string state, bool isImmediate, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsFilterPanelShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterChange(AuctionBuyItemFilterType auctionBuyItemFilter)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnFilterPanelBackGroundButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAllFilterButtonState(AuctionBuyItemFilterType filterType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetBuyButtonState(bool isSelect, bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrSystemFuncDescType(SystemFuncDescType funcDescType)
        {
        }

        [MethodImpl(0x8000)]
        public void SetMoney(ulong tradeMoney, ulong bindMoney)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelState(bool isBuy)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSellButtonState(bool isSelect, bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSellItemRedPoint(bool showRedPoint)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSortButtonState(bool isOpen, bool immediateComplete = false)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowZeroItem(bool show)
        {
        }
    }
}

