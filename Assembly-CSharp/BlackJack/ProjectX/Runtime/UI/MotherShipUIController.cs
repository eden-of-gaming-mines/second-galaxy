﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class MotherShipUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnAnnoucementPlayEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ulong> EventOnDelegateMissionFleetItemClick;
        private List<DelegateMissionFleetItemUIController> m_delegateMissionFleetList;
        private List<Turple<Text, Text, Image>> m_techUpgradingUIItemList;
        private List<MotherShipProduceLineItemUIController> m_producingLineItemCtrlList;
        private List<MotherShipProduceLineItemUIController> m_guildProducingLineItemCtrlList;
        private List<MotherShipGalaxyItemUIController> m_galaxyItemCtrlList;
        protected string AssetName_FleetItemPrefab;
        protected string MotherShipUIState_PanelOpen;
        protected string MotherShipUIState_PanelClose;
        protected const string QueneValueString = "<color=#fcb50fFF>{0}</color>/{1}";
        protected const int TechLineMax = 1;
        protected int m_guildProduceLineShowMaxCount;
        private List<Image> ProgressBarList;
        private List<GameObject> ProgressBGList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_commonUIStateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./NormalGroup/TechPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TechPanelUIStateCtrl;
        [AutoBind("./NormalGroup/TechPanel", AutoBindAttribute.InitState.NotInit, false)]
        public Button TechPanelButton;
        [AutoBind("./NormalGroup/TechPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public Button TechPanelOffLineButton;
        [AutoBind("./NormalGroup/TechPanel/BGImages/StudyingQueueingGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StudyingQueueingGroup;
        [AutoBind("./NormalGroup/TechPanel/BGImages/StudyingQueueingGroup/AlignmentTitle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text StudyingQueueingValueText;
        [AutoBind("./NormalGroup/TechPanel/BGImages/StudyingQueueingGroup/AlignmentTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject StudyingQueueingTitleItem;
        [AutoBind("./NormalGroup/ItemStorePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ItemStoreUIStateCtrl;
        [AutoBind("./NormalGroup/ItemStorePanel", AutoBindAttribute.InitState.NotInit, false)]
        public Button ItemStorePanelButton;
        [AutoBind("./NormalGroup/ItemStorePanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public Button ItemStorePanelOffLineButton;
        [AutoBind("./NormalGroup/ItemStorePanel/BGImages/ItemStoreCapacity/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
        public Image ItemStoreProgressBar;
        [AutoBind("./NormalGroup/ItemStorePanel/BGImages/ItemStoreCapacity/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemStoreCapacityValue;
        [AutoBind("./NormalGroup/CrewDormitoryPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrewDormitoryUIStateCtrl;
        [AutoBind("./NormalGroup/CrewDormitoryPanel", AutoBindAttribute.InitState.NotInit, false)]
        public Button CrewDormitoryPanelButton;
        [AutoBind("./NormalGroup/CrewDormitoryPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public Button CrewDormitoryPanelOffLineButton;
        [AutoBind("./NormalGroup/CrewDormitoryPanel/BGImages/CrewNumberGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DormitoryNunberText;
        [AutoBind("./NormalGroup/CrewDormitoryPanel/BGImages/MaintainGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DormitoryMaintainShipItem;
        [AutoBind("./NormalGroup/CrewDormitoryPanel/BGImages/MaintainGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DormitoryMaintainShipNumberText;
        [AutoBind("./NormalGroup/CrewDormitoryPanel/BGImages/NewShipGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DormitoryReadyToActiveShipItem;
        [AutoBind("./NormalGroup/HangarPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController HangarUIStateCtrl;
        [AutoBind("./NormalGroup/HangarPanel", AutoBindAttribute.InitState.NotInit, false)]
        public Button HangarPanelButton;
        [AutoBind("./NormalGroup/HangarPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public Button HangarPanelOffLineButton;
        [AutoBind("./NormalGroup/HangarPanel/BGImages/HangarCapacityValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text HangarCapacityValue;
        [AutoBind("./NormalGroup/BuildingPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BuildingPanelStateCtrl;
        [AutoBind("./NormalGroup/BuildingPanel", AutoBindAttribute.InitState.NotInit, false)]
        public Button BuildingPanelButton;
        [AutoBind("./NormalGroup/BuildingPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public Button BuildingPanelOffLineButton;
        [AutoBind("./NormalGroup/BuildingPanel/BGImages/productionGroup/ProductionTitle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ProduceingLineCount;
        [AutoBind("./NormalGroup/BuildingPanel/BGImages/productionGroup/ProductionList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ProduceLineItem;
        [AutoBind("./NormalGroup/BuildingPanel/BGImages/productionGroup/ProductionTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ProduceTitleItem;
        [AutoBind("./NormalGroup/CrackPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackPanelStateCtrl;
        [AutoBind("./NormalGroup/CrackPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CrackPanelButton;
        [AutoBind("./NormalGroup/CrackPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public Button CrackPanelOffLineButton;
        [AutoBind("./NormalGroup/CrackPanel/BGImages/ReadyGet", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackReadyGetStateCtrl;
        [AutoBind("./NormalGroup/CrackPanel/BGImages/ReadyGet/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackReadyGetCountText;
        [AutoBind("./NormalGroup/CrackPanel/BGImages/CrackCapacity", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController CrackCapacityStateCtrl;
        [AutoBind("./NormalGroup/CrackPanel/BGImages/CrackCapacity/CrackGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackCapacityText;
        [AutoBind("./NormalGroup/CrackPanel/BGImages/CrackCapacity/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text CrackTimeText;
        [AutoBind("./NormalGroup/CrackPanel/BGImages/CrackCapacity", AutoBindAttribute.InitState.NotInit, false)]
        public Transform ProgressBarParentNode;
        [AutoBind("./NormalGroup/DelegateMissonPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DelegateMissonUIStateCtrl;
        [AutoBind("./NormalGroup/DelegateMissonPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DelegateMissonPanelButton;
        [AutoBind("./NormalGroup/DelegateMissonPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DelegateMissonPaneOffLineStatelButton;
        [AutoBind("./NormalGroup/DelegateMissonPanel/BGImages/DispatchImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DelegateMissonRecordButton;
        [AutoBind("./NormalGroup/DelegateMissonPanel/BGImages/FleetGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DelegateMissionFleetGroup;
        [AutoBind("./NormalGroup/DelegateMissonPanel/BGImages/NoItemImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject NoDelegateMissionFleetText;
        [AutoBind("./NormalGroup/DelegateMissonPanel/BGImages/AlignmentTitle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text DelegateMissionValueText;
        [AutoBind("./NormalGroup/ExploitPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController DevelopmentStateCtrl;
        [AutoBind("./NormalGroup/ExploitPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DevelopmentButton;
        [AutoBind("./NormalGroup/ExploitPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx DevelopmentOffLineStateButton;
        [AutoBind("./GuildGroup/GuildStrategyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_GuildStrategyStateCtrl;
        [AutoBind("./GuildGroup/GuildStrategyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildStrategyButton;
        [AutoBind("./GuildGroup/GuildStorePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_GuildStoreStateCtrl;
        [AutoBind("./GuildGroup/GuildStorePanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildStoreButton;
        [AutoBind("./GuildGroup/GuildArmamentOrderPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_GuildArmamentOrderStateCtrl;
        [AutoBind("./GuildGroup/GuildArmamentOrderPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildArmamentOrderButton;
        [AutoBind("./GuildGroup/GuildArmamentOrderPanel/BGImages/ReadyGet/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_GuildArmamentOrderText;
        [AutoBind("./GuildGroup/GuildWarDamageReportPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_GuildWarDamageReportStateCtrl;
        [AutoBind("./GuildGroup/GuildWarDamageReportPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildWarDamageReportButton;
        [AutoBind("./GuildGroup/GuildWarDamageReportPanel/BGImages/MaintainGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_GuildWarValueText;
        [AutoBind("./GuildGroup/GuildGalaxyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_GuildGalaxyStateCtrl;
        [AutoBind("./GuildGroup/GuildGalaxyPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildGalaxyButton;
        [AutoBind("./GuildGroup/GuildGalaxyPanel/BGImages/StarGroup/StarList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_GuildGalaxyItemObj;
        [AutoBind("./GuildGroup/GuildProducePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_GuildProduceStateCtrl;
        [AutoBind("./GuildGroup/GuildProducePanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildProduceButton;
        [AutoBind("./GuildGroup/GuildProducePanel/BGImages/productionGroup/ProductionTitle", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_GuildProduceAboveTextObj;
        [AutoBind("./GuildGroup/GuildProducePanel/BGImages/productionGroup/ProductionTitle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_GuildProduceAboveText;
        [AutoBind("./GuildGroup/GuildFlagshipManagementPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_GuildFlagshipManagementStateCtrl;
        [AutoBind("./GuildGroup/GuildFlagshipManagementPanel/BGImages/OffLineState", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildFlagshipManagementOfflineButton;
        [AutoBind("./GuildGroup/GuildFlagshipManagementPanel", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_GuildFlagshipManagementButton;
        [AutoBind("./GuildGroup/GuildFlagshipManagementPanel/BGImages/AlignmentTitle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_GuildFlagshipManagementInfoText;
        [AutoBind("./GuildGroup/GuildFlagshipManagementPanel/BGImages/DispatchText", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_guildFlagShipOptLogButton;
        [AutoBind("./TopPanel/Announcement/AnnouncementText/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text AnnouncementText;
        [AutoBind("./TopPanel/Announcement/AnnouncementText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController AnnouncementTextStateCtrl;
        [AutoBind("./TopPanel/Announcement", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject AnnouncementGameObject;
        [AutoBind("./TopPanel/Announcement", AutoBindAttribute.InitState.NotInit, false)]
        public Button AnnouncementButton;
        protected const string PanelState_PanelOnLine = "PanelOnLine";
        protected const string PanelState_PanelOffLine = "PanelOffLine";
        protected const string PanelState_PanelChange = "PanelChange";
        [CompilerGenerated]
        private static Comparison<LBProductionLine> <>f__am$cache0;
        [CompilerGenerated]
        private static Comparison<LBProductionLine> <>f__am$cache1;
        [CompilerGenerated]
        private static Comparison<GuildBuildingInfo> <>f__am$cache2;
        [CompilerGenerated]
        private static Comparison<GuildBuildingInfo> <>f__am$cache3;
        [CompilerGenerated]
        private static Comparison<GuildProductionLineInfo> <>f__am$cache4;
        [CompilerGenerated]
        private static Comparison<GuildProductionLineInfo> <>f__am$cache5;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_StartEnterAnimation;
        private static DelegateBridge __Hotfix_EnableCrewDormitoryPanel;
        private static DelegateBridge __Hotfix_CreateCrewDormitoryPanelUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockCrewDormitoryPanel;
        private static DelegateBridge __Hotfix_UpdateCrewDormitoryPanel;
        private static DelegateBridge __Hotfix_EnableShipHangarPanel;
        private static DelegateBridge __Hotfix_CreateShipHangarPanelUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockShipHangarPanel;
        private static DelegateBridge __Hotfix_UpdateShipHangarPanelInfo;
        private static DelegateBridge __Hotfix_EnableItemStorePanel;
        private static DelegateBridge __Hotfix_CreateItemStoreUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockItemStorePanel;
        private static DelegateBridge __Hotfix_UpdateItemStorePanelInfo;
        private static DelegateBridge __Hotfix_EnableDelegateMissionPanel;
        private static DelegateBridge __Hotfix_CreateDelegateMissionUnlockUIProcess;
        private static DelegateBridge __Hotfix_CreateGuildStrategyMissionUnlockUIProcess;
        private static DelegateBridge __Hotfix_CreateGuildGalaxyMissionUnlockUIProcess;
        private static DelegateBridge __Hotfix_CreateGuildProduceMissionUnlockUIProcess;
        private static DelegateBridge __Hotfix_CreateGuildFlagshipManagementMissionUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockDelegateMission;
        private static DelegateBridge __Hotfix_UnlockGuildStrategyMission;
        private static DelegateBridge __Hotfix_UnlockGuildGalaxyMission;
        private static DelegateBridge __Hotfix_UnlockGuildProduceMission;
        private static DelegateBridge __Hotfix_UnlockGuildFlagshipManagementMission;
        private static DelegateBridge __Hotfix_UpdateDelegateMissionPanelInfo;
        private static DelegateBridge __Hotfix_EnableTechPanel;
        private static DelegateBridge __Hotfix_CreateTechPanelUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockTechPanel;
        private static DelegateBridge __Hotfix_UpdateTechPanelInfo;
        private static DelegateBridge __Hotfix_EnableCrackPanel;
        private static DelegateBridge __Hotfix_CreateCrackPanelUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockCrackPanel;
        private static DelegateBridge __Hotfix_UpdateCrackPanelInfo;
        private static DelegateBridge __Hotfix_GetAllRemainingTime;
        private static DelegateBridge __Hotfix_SetProgressBarListFillAmount;
        private static DelegateBridge __Hotfix_EnableProducePanel;
        private static DelegateBridge __Hotfix_CreateProducePanelUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockProducePanel;
        private static DelegateBridge __Hotfix_UpdateProducePanelInfo;
        private static DelegateBridge __Hotfix_EnableDevelopmentPanel;
        private static DelegateBridge __Hotfix_CreateDevelopmentPanelUnlockUIProcess;
        private static DelegateBridge __Hotfix_UnlockDevelopmentPanel;
        private static DelegateBridge __Hotfix_UpdateCompensationCount;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_IsAnnouncementShow;
        private static DelegateBridge __Hotfix_SetAnnouncementTextInfo;
        private static DelegateBridge __Hotfix_CreateMotherShipPanelShowProcess;
        private static DelegateBridge __Hotfix_AnnoucementEnd;
        private static DelegateBridge __Hotfix_OnDelegateMissionFleetItemClick;
        private static DelegateBridge __Hotfix_SetGuildGalaxy;
        private static DelegateBridge __Hotfix_SetGuildProduceInfo;
        private static DelegateBridge __Hotfix_SetGuildFlagshipManagementInfo;
        private static DelegateBridge __Hotfix_SetGuildArmamentOrderInfo;
        private static DelegateBridge __Hotfix_GetTechUpgradeItem;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_add_EventOnAnnoucementPlayEnd;
        private static DelegateBridge __Hotfix_remove_EventOnAnnoucementPlayEnd;
        private static DelegateBridge __Hotfix_add_EventOnDelegateMissionFleetItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnDelegateMissionFleetItemClick;

        public event Action EventOnAnnoucementPlayEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<ulong> EventOnDelegateMissionFleetItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void AnnoucementEnd(bool isCompleted)
        {
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateCrackPanelUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateCrewDormitoryPanelUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateDelegateMissionUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateDevelopmentPanelUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateGuildFlagshipManagementMissionUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateGuildGalaxyMissionUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateGuildProduceMissionUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateGuildStrategyMissionUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateItemStoreUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess CreateMotherShipPanelShowProcess(bool isGuildMode, bool isOpenState, bool isImmediately)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateProducePanelUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateShipHangarPanelUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateTechPanelUnlockUIProcess()
        {
        }

        [MethodImpl(0x8000)]
        public void EnableCrackPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableCrewDormitoryPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableDelegateMissionPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableDevelopmentPanel(bool isOnline)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableItemStorePanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableProducePanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableShipHangarPanel(bool enable)
        {
        }

        [MethodImpl(0x8000)]
        public void EnableTechPanel(bool isOnLine)
        {
        }

        [MethodImpl(0x8000)]
        private TimeSpan GetAllRemainingTime(CrackSlotInfo crackSlotInfo)
        {
        }

        [MethodImpl(0x8000)]
        private Turple<Text, Text, Image> GetTechUpgradeItem(int idx)
        {
        }

        [MethodImpl(0x8000)]
        public bool IsAnnouncementShow()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDelegateMissionFleetItemClick(ulong instanceId)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnnouncementTextInfo(string msg, float time)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildArmamentOrderInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildFlagshipManagementInfo(int flagShipCount, int hangarSlotCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildGalaxy(List<GuildBuildingInfo> dataList)
        {
        }

        [MethodImpl(0x8000)]
        public void SetGuildProduceInfo(List<GuildProductionLineInfo> produceLineData)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPosition(Vector3 position)
        {
        }

        [MethodImpl(0x8000)]
        private void SetProgressBarListFillAmount(int count, int alreadyDoneCount, float progress)
        {
        }

        [MethodImpl(0x8000)]
        public void StartEnterAnimation()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockCrackPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockCrewDormitoryPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockDelegateMission(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockDevelopmentPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildFlagshipManagementMission(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildGalaxyMission(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildProduceMission(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockGuildStrategyMission(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockItemStorePanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockProducePanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockShipHangarPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnlockTechPanel(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCompensationCount()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCrackPanelInfo(LogicBlockCrackBase lbCrack)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCrewDormitoryPanel(int currCrewCount, int crewStoreCapacity, bool readyToActive, int destoryShipCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateDelegateMissionPanelInfo(List<LBDelegateMission> missionList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemStorePanelInfo(ulong usedCount, ulong capacity)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProducePanelInfo(List<LBProductionLine> produceLineList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateShipHangarPanelInfo(ulong usedSlotCount, int slotCapacity)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateTechPanelInfo(List<TechUpgradeInfo> techUpInfoList)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockCrackPanel>c__AnonStorey9
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.CrackPanelStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockCrewDormitoryPanel>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool res)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockDelegateMission>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.DelegateMissonUIStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockDevelopmentPanel>c__AnonStoreyB
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.DevelopmentStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildFlagshipManagementMission>c__AnonStorey7
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_GuildFlagshipManagementStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildGalaxyMission>c__AnonStorey5
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_GuildGalaxyStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildProduceMission>c__AnonStorey6
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_GuildProduceStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockGuildStrategyMission>c__AnonStorey4
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_GuildStrategyStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockItemStorePanel>c__AnonStorey2
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.ItemStoreUIStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockProducePanel>c__AnonStoreyA
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.BuildingPanelStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockShipHangarPanel>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.HangarUIStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UnlockTechPanel>c__AnonStorey8
        {
            internal Action<bool> onEnd;
            internal MotherShipUIController $this;

            internal void <>m__0(bool res)
            {
                this.$this.TechPanelUIStateCtrl.SetToUIStateExtra("PanelOnLine", true, false, null);
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }
    }
}

