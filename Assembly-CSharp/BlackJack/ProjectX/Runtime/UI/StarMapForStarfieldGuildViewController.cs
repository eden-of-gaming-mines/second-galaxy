﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Runtime;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class StarMapForStarfieldGuildViewController : UIControllerBase
    {
        private readonly Queue<ViewBlock> m_viewBlockDrawQueue;
        [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
        public RawImage m_bgImage;
        [AutoBind("./ViewBlock", AutoBindAttribute.InitState.NotInit, false)]
        public RawImage m_blockTemplate;
        private readonly List<GuildAllianceOccupyInfo> m_visibleOccupyList;
        private Transform m_iconRoot;
        private CanvasGroup m_iconCanvasGroup;
        private StarMapIconController m_iconTemplate;
        public readonly List<GuildLogoDisplay> m_logoList;
        public Vector2 m_leftBottom;
        public Vector2 m_rightTop;
        private float m_currScale;
        private Vector2 m_currOffset;
        public float m_mapGlobalScale;
        private readonly List<ViewBlock> m_viewBlocks;
        private readonly Stack<ViewBlock> m_viewBlockPool;
        private readonly List<Thread> m_loadingThread;
        private Dictionary<uint, GuildAllianceOccupyInfo> m_guildOccupyInfoDict;
        private Dictionary<uint, GuildAllianceOccupyInfo> m_allianceOccupyInfoDict;
        private Dictionary<int, GuildAllianceId> m_solarSystemGuildMappingDict;
        private List<GDBSolarSystemSimpleInfo> m_visibleSolarSystems;
        private readonly Dictionary<uint, List<GuildViewHelper.StarArea>> m_mapArea;
        private readonly List<ColorByteArrayContext> m_drawListener;
        private float m_alpha;
        public GuildViewHelper.MapType m_mapType;
        private Dictionary<string, UnityEngine.Object> m_resDict;
        private int m_visibleMinX;
        private int m_visibleMaxX;
        private int m_visibleMinY;
        private int m_visibleMaxY;
        private const float DisplayArrowScale = 9f;
        [CompilerGenerated]
        private static Comparison<GuildAllianceOccupyInfo> <>f__am$cache0;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetIconPanel;
        private static DelegateBridge __Hotfix_InitColorMap;
        private static DelegateBridge __Hotfix_SetResDict;
        private static DelegateBridge __Hotfix_OnStarFieldDataShouldRefresh;
        private static DelegateBridge __Hotfix_SetVisibleSolarSystemRefence;
        private static DelegateBridge __Hotfix_GetVisiblePowerfulGuildAlliance;
        private static DelegateBridge __Hotfix_SetLogos;
        private static DelegateBridge __Hotfix_SetAlpha;
        private static DelegateBridge __Hotfix_SetOn;
        private static DelegateBridge __Hotfix_DoTransform;
        private static DelegateBridge __Hotfix_VisibleAreaChanged;
        private static DelegateBridge __Hotfix_SetShouldDrawQueue;
        private static DelegateBridge __Hotfix_OperateDrawQueue;
        private static DelegateBridge __Hotfix_IsDrawing;
        private static DelegateBridge __Hotfix_AddToListener;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_FindNotCalculatedDataAndTryStart;
        private static DelegateBridge __Hotfix_FindBlock;
        private static DelegateBridge __Hotfix_RequestViewBlock;
        private static DelegateBridge __Hotfix_Recycle;
        private static DelegateBridge __Hotfix_Terminate;
        private static DelegateBridge __Hotfix_Finalize;

        [MethodImpl(0x8000)]
        private void AddToListener(ColorByteArrayContext context)
        {
        }

        [MethodImpl(0x8000)]
        public void DoTransform(float scale, Vector2 offset)
        {
        }

        [MethodImpl(0x8000)]
        protected override void Finalize()
        {
        }

        [MethodImpl(0x8000)]
        public ViewBlock FindBlock(int x, int y)
        {
        }

        [MethodImpl(0x8000)]
        private void FindNotCalculatedDataAndTryStart()
        {
        }

        [MethodImpl(0x8000)]
        private void GetVisiblePowerfulGuildAlliance(int topN)
        {
        }

        [MethodImpl(0x8000)]
        public void InitColorMap(Texture2D tex)
        {
        }

        [MethodImpl(0x8000)]
        private bool IsDrawing(int scaleLevel, int x, int y, int version)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnStarFieldDataShouldRefresh(Dictionary<uint, GuildAllianceOccupyInfo> guildOccupyInfoDict, Dictionary<uint, GuildAllianceOccupyInfo> allianceOccupyInfoDict, Dictionary<int, GuildAllianceId> solarSystemGuildMappingDict)
        {
        }

        [MethodImpl(0x8000)]
        private void OperateDrawQueue()
        {
        }

        [MethodImpl(0x8000)]
        public void Recycle(ViewBlock block)
        {
        }

        [MethodImpl(0x8000)]
        public ViewBlock RequestViewBlock()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAlpha(float alpha)
        {
        }

        [MethodImpl(0x8000)]
        public void SetIconPanel(Transform rootTransform, StarMapIconController template)
        {
        }

        [MethodImpl(0x8000)]
        private void SetLogos()
        {
        }

        [MethodImpl(0x8000)]
        public void SetOn(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void SetResDict(Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void SetShouldDrawQueue(bool newDataArrived = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetVisibleSolarSystemRefence(List<GDBSolarSystemSimpleInfo> source)
        {
        }

        [MethodImpl(0x8000)]
        public void Terminate()
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        [MethodImpl(0x8000)]
        private bool VisibleAreaChanged(float locScale, Action<ViewBlock> onMovedOut = null)
        {
        }

        [CompilerGenerated]
        private sealed class <AddToListener>c__AnonStorey1
        {
            internal ColorByteArrayContext context;

            internal void <>m__0()
            {
                GuildViewHelper.Calculate(this.context);
            }
        }

        [CompilerGenerated]
        private sealed class <SetLogos>c__AnonStorey0
        {
            internal int id;

            internal bool <>m__0(GDBSolarSystemSimpleInfo info) => 
                (info.Id == this.id);
        }
    }
}

