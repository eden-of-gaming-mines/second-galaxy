﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class NpcShopAutomaticSellBoxUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnConfimClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnItemClicked;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnItem3DTouch;
        private bool m_isShowItemCount;
        private List<ILBStoreItemClient> m_cachedItemList;
        private Dictionary<string, UnityEngine.Object> m_cachedResDict;
        private int m_selectedIndex;
        private const int PoolSize = 10;
        private GameObject m_itemTemplateGo;
        private const string ItemAtuoSellPoolName = "CommonItemUI";
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ItemRoot;
        [AutoBind("./SellGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image MoneyIconImage;
        [AutoBind("./SellGroup/PriceText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AutoSellPriceText;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopHorizontalScrollRect ItemSellListScrollRect;
        [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool ItemSearchListUIItemPool;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController UIScrollViewStateCtrl;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_InitEasyPool;
        private static DelegateBridge __Hotfix_OnEasyPoolItemCreated;
        private static DelegateBridge __Hotfix_UpdateItemStoreList;
        private static DelegateBridge __Hotfix_OnItemStoreUIItemFill;
        private static DelegateBridge __Hotfix_OnItemStoreUIItemClick;
        private static DelegateBridge __Hotfix_OnItemStoreUIItem3DTouch;
        private static DelegateBridge __Hotfix_SetCurrMoneyInfo;
        private static DelegateBridge __Hotfix_OnConfimClick;
        private static DelegateBridge __Hotfix_add_EventOnConfimClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfimClick;
        private static DelegateBridge __Hotfix_add_EventOnItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnItem3DTouch;

        public event Action EventOnConfimClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void InitEasyPool()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnConfimClick(UIControllerBase ctr)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEasyPoolItemCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItem3DTouch(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItemClick(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemStoreUIItemFill(UIControllerBase itemCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCurrMoneyInfo(CurrencyType moneyType, ulong moneyNum, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemStoreList(List<ILBStoreItemClient> itemList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

