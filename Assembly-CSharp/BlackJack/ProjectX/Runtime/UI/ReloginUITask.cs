﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class ReloginUITask : ReloginBySessionUITaskBase
    {
        private ReloginUIController m_mainCtrl;
        private short m_autoReconnectCount;
        private const int AutoReconnectMaxCount = 3;
        private const float AutoReconnectCoolDownTime = 2f;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        [CompilerGenerated]
        private static Action <>f__am$cache0;
        [CompilerGenerated]
        private static Action <>f__am$cache1;
        [CompilerGenerated]
        private static Action<bool> <>f__am$cache2;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnNewIntent;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitAck;
        private static DelegateBridge __Hotfix_OnGameServerDisconnected;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_OnPlayerInfoInitEnd;
        private static DelegateBridge __Hotfix_SendNecessaryReqToServer;
        private static DelegateBridge __Hotfix_StartWorldEnterReq;
        private static DelegateBridge __Hotfix_IsNeedCheckLocalDataCache;
        private static DelegateBridge __Hotfix_StartSolarSystemTask;
        private static DelegateBridge __Hotfix_StartSpaceStationUITask;
        private static DelegateBridge __Hotfix_ShowErrorMsg;
        private static DelegateBridge __Hotfix_OnRetryLoginButtonClicked;
        private static DelegateBridge __Hotfix_GetReloginCoolDownTime;
        private static DelegateBridge __Hotfix_StartTryToReconnectAuto;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public ReloginUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override float GetReloginCoolDownTime()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedCheckLocalDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnGameServerDisconnected()
        {
        }

        [MethodImpl(0x8000)]
        public override bool OnNewIntent(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPlayerInfoInitAck(object msg)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPlayerInfoInitEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnRetryLoginButtonClicked(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void SendNecessaryReqToServer()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ShowErrorMsg(string key)
        {
        }

        [MethodImpl(0x8000)]
        private void StartSolarSystemTask()
        {
        }

        [MethodImpl(0x8000)]
        private void StartSpaceStationUITask()
        {
        }

        [MethodImpl(0x8000)]
        private void StartTryToReconnectAuto()
        {
        }

        [MethodImpl(0x8000)]
        private void StartWorldEnterReq()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

