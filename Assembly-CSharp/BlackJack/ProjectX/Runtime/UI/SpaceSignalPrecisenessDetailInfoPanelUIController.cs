﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class SpaceSignalPrecisenessDetailInfoPanelUIController : UIControllerBase
    {
        private const string StateClose = "PanelClose";
        private const string StateOpen = "PanelOpen";
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./BgButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backGroundButton;
        [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_panelRoot;
        private static DelegateBridge __Hotfix_GetSpaceSignalPrecisenessDetailInfoPanelUIProcess;
        private static DelegateBridge __Hotfix_SetPanelPos;

        [MethodImpl(0x8000)]
        public UIProcess GetSpaceSignalPrecisenessDetailInfoPanelUIProcess(bool isShow, bool immediateComplete = false, bool allowToRefreshSameState = false)
        {
        }

        [MethodImpl(0x8000)]
        public void SetPanelPos(Vector3 panelPos)
        {
        }
    }
}

