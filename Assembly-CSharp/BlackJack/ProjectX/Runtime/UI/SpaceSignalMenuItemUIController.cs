﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class SpaceSignalMenuItemUIController : MenuItemUIControllerBase
    {
        private SignalInfo m_signalInfo;
        private ConfigDataSpaceSignalInfo m_signalConfigInfo;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<CommonItemIconUIController> EventOnRewardItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<CommonItemIconUIController> EventOnRewardItem3DTouch;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick;
        public CommonRewardUIController m_signalRewardCtrl;
        public LossWarningButtonUIController m_lossWarningButtonUICtrl;
        [AutoBind("./TargetInfo/Info/TextGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_spaceSignalNameText;
        [AutoBind("./TargetInfo/Info/TextGroup/LevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_sceneLevelText;
        [AutoBind("./Detail/MissionDetail/MissionText/DetailText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_targetText;
        [AutoBind("./Detail/LocationType/LocationText/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_precisenessTextStateCtrl;
        [AutoBind("./Detail/LocationType/LocationText/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_precisenessText;
        [AutoBind("./Detail/LocationType/LocationText/LocationText/ExplainButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_precisenessButton;
        [AutoBind("./Detail/LocationType/LocationText/LocationText/TipDummy", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_precisenessTipDummy;
        [AutoBind("./Detail/Reward", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_rewardObj;
        [AutoBind("./Detail/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        [AutoBind("./Detail/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_lossWarningButton;
        [AutoBind("./Detail/LossWarningButton/SafetyButton", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_lossWarningPos;
        [AutoBind("./Detail/LocationType/LocationText/LocationText", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_precisenessGameObject;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegistAllEvent;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetSpaceSignalItem;
        private static DelegateBridge __Hotfix_SetSpaceSignalRewardItem;
        private static DelegateBridge __Hotfix_GetMenuItemName;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetSignal;
        private static DelegateBridge __Hotfix_GetSpaceSignalPrecisenessTipDummy;
        private static DelegateBridge __Hotfix_OnRewardItemClick;
        private static DelegateBridge __Hotfix_OnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItemClick;
        private static DelegateBridge __Hotfix_add_EventOnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_remove_EventOnRewardItem3DTouch;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;

        public event Action<bool, bool, bool, MenuItemUIControllerBase> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnRewardItem3DTouch
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<CommonItemIconUIController> EventOnRewardItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public SignalInfo GetSignal()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetSpaceSignalPrecisenessTipDummy()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItem3DTouch(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRewardItemClick(CommonItemIconUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public override void RegistAllEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceSignalItem(SignalInfo spaceSignal, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSpaceSignalRewardItem(Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

