﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class DrivingLicenseInfoUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<DrivingLiscesceNodeListItemUIController> EventOnCheckButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<DrivingLicenseNodeInfoUIController, bool> EventOnNodeClick;
        private DrivingAbilityItemListUIController m_abilityItemCtrl;
        private List<DrivingLiscesceNodeListItemUIController> m_drivingLiscesceNodeListItemUICtrlList;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CloseButton;
        [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackButton;
        [AutoBind("./BGImages", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BackGroundButton;
        [AutoBind("./TipWindow", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController TipInfoCtrl;
        [AutoBind("./TipWindow", AutoBindAttribute.InitState.NotInit, false)]
        public Transform TipWindowTransform;
        [AutoBind("./TipWindow/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject ShipAbilityRoot;
        [AutoBind("./LicenseVerticalScrollView/RightUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform RightTransform;
        [AutoBind("./LicenseVerticalScrollView/LeftUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform LeftTransform;
        [AutoBind("./TipWindow/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LicenseNameText;
        [AutoBind("./RankToggleGroup/Content/T1Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle T1Toggle;
        [AutoBind("./RankToggleGroup/Content/T2Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle T2Toggle;
        [AutoBind("./RankToggleGroup/Content/T3Toggle", AutoBindAttribute.InitState.NotInit, false)]
        public Toggle T3Toggle;
        [AutoBind("./LicenseVerticalScrollView/NodeListItem", AutoBindAttribute.InitState.NotInit, false)]
        public Transform DrivingLiscesceNodeListItemGroup;
        [AutoBind("./LicenseVerticalScrollView/NodeListItem/NodeList", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DrivingLiscesceNodeListItem;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateNodeInfo;
        private static DelegateBridge __Hotfix_NoticePlayerNeedSkill;
        private static DelegateBridge __Hotfix_ShowNodeTip;
        private static DelegateBridge __Hotfix_HideNodeTip;
        private static DelegateBridge __Hotfix_ResetRankToggle;
        private static DelegateBridge __Hotfix_GetDestroyerGoToCheckButtonRect;
        private static DelegateBridge __Hotfix_GetDestroyerGoToCheckButton;
        private static DelegateBridge __Hotfix_GetGoToCheckButtonRectByShipType;
        private static DelegateBridge __Hotfix_CreateMainWindowProcess;
        private static DelegateBridge __Hotfix_OnGoToButtonClick;
        private static DelegateBridge __Hotfix_OnNodeClick;
        private static DelegateBridge __Hotfix_add_EventOnCheckButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCheckButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnNodeClick;
        private static DelegateBridge __Hotfix_remove_EventOnNodeClick;

        public event Action<DrivingLiscesceNodeListItemUIController> EventOnCheckButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<DrivingLicenseNodeInfoUIController, bool> EventOnNodeClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateMainWindowProcess(bool isShow, bool immediateComplete, bool allowToRefreshSameState, UIProcess.ProcessExecMode mode = 1)
        {
        }

        [MethodImpl(0x8000)]
        public DrivingLiscesceNodeListItemUIController GetDestroyerGoToCheckButton()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetDestroyerGoToCheckButtonRect()
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetGoToCheckButtonRectByShipType(ShipType shipType)
        {
        }

        [MethodImpl(0x8000)]
        public void HideNodeTip()
        {
        }

        [MethodImpl(0x8000)]
        public void NoticePlayerNeedSkill(int needSkillID, int skillLevel)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGoToButtonClick(DrivingLiscesceNodeListItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNodeClick(DrivingLicenseNodeInfoUIController ctrl, bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void ResetRankToggle(RankType rank)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowNodeTip(ConfigDataDrivingLicenseInfo config, int level, Dictionary<string, UnityEngine.Object> resDict, Action<bool> OnAnimationEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateNodeInfo(List<int> skillList, int currPlayerLevel, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

