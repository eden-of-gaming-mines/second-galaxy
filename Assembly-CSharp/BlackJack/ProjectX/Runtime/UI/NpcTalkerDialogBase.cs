﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class NpcTalkerDialogBase : UITaskBase
    {
        protected int m_currNpcIndex;
        protected NpcDNId m_currentNpcDNId;
        protected NpcDialogInfo m_currentDialogInfo;
        protected int m_currentOptionTopIndex;
        protected UIIntent m_mainUIIntent;
        protected NpcTalkerDialogUIController m_npcTalkerUICtrl;
        protected bool m_isFirstDialogForAudio;
        protected bool m_isPlayNpcAudio;
        private bool m_isAutoClose;
        private float m_closeTime;
        protected IUIBackgroundManager m_backgroundManager;
        public Action<bool> EventSetCountdownState;
        public Action<string> EventOnCountdownInProgress;
        public Action EventOnDialogAutoClosed;
        [CompilerGenerated]
        private static Action<Task> <>f__am$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_RegisterShipEnterEvent;
        private static DelegateBridge __Hotfix_UnregisterShipEnterEvent;
        private static DelegateBridge __Hotfix_OnShipEnter;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateNpcTalkerDialogUI;
        private static DelegateBridge __Hotfix_OnTick;
        private static DelegateBridge __Hotfix_OnDialogOptionClick;
        private static DelegateBridge __Hotfix_OnNpcDialogOptClick;
        private static DelegateBridge __Hotfix_OnNpcDialogNextPageOptionClick;
        private static DelegateBridge __Hotfix_SendNextDialogNetworkReq;
        private static DelegateBridge __Hotfix_SendConfirm2ServerNetworkReq;
        private static DelegateBridge __Hotfix_StartQuestDialogUITask;
        private static DelegateBridge __Hotfix_StartNpcDialogQuestCompleteUITask;
        private static DelegateBridge __Hotfix_StartQuestCommitItemUITask;
        private static DelegateBridge __Hotfix_StartNpcShopUITask;
        private static DelegateBridge __Hotfix_StartTradeNpcSaleShopUITask;
        private static DelegateBridge __Hotfix_SendInStationNextDailogNetworkReq;
        private static DelegateBridge __Hotfix_SendInSpaceNextDialogNetworkReq;
        private static DelegateBridge __Hotfix_ReturnToMainUI;
        private static DelegateBridge __Hotfix_StartAuctionUITask;
        private static DelegateBridge __Hotfix_SendSetGuildBaseSolarSystem;
        private static DelegateBridge __Hotfix_StartBalckMarket;
        private static DelegateBridge __Hotfix_StartFactionCreditQuestUITask;
        private static DelegateBridge __Hotfix_StartQuestDialogUITaskFaction;
        private static DelegateBridge __Hotfix_GetNPCShopOptIndex;
        private static DelegateBridge __Hotfix_OpenNpcShopUITask;
        private static DelegateBridge __Hotfix_OpenAuctionUITask;

        [MethodImpl(0x8000)]
        public NpcTalkerDialogBase(string name)
        {
        }

        [MethodImpl(0x8000)]
        public int GetNPCShopOptIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void OnDialogOptionClick(int optionIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNpcDialogNextPageOptionClick()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNpcDialogOptClick(int optionIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShipEnter(ILBSpaceTarget targetShip)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnTick()
        {
        }

        [MethodImpl(0x8000)]
        public void OpenAuctionUITask(Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public void OpenNpcShopUITask(int optIdx, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterShipEnterEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void ReturnToMainUI()
        {
        }

        [MethodImpl(0x8000)]
        protected void SendConfirm2ServerNetworkReq()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SendInSpaceNextDialogNetworkReq(uint sceneInstanceId, int nextDlgId)
        {
        }

        [MethodImpl(0x8000)]
        protected virtual void SendInStationNextDailogNetworkReq(int nextDlgId)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendNextDialogNetworkReq(int nextDlgId)
        {
        }

        [MethodImpl(0x8000)]
        protected void SendSetGuildBaseSolarSystem()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartAuctionUITask(bool? showZeroItem = new bool?(), Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartBalckMarket()
        {
        }

        [MethodImpl(0x8000)]
        protected void StartFactionCreditQuestUITask(int optIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartNpcDialogQuestCompleteUITask(int optIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartNpcShopUITask(int optIdx, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartQuestCommitItemUITask(int optIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartQuestDialogUITask(int optIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartQuestDialogUITaskFaction(int optIdx)
        {
        }

        [MethodImpl(0x8000)]
        protected void StartTradeNpcSaleShopUITask(bool isLegal, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterShipEnterEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected virtual CommonUIStateEffectProcess UpdateNpcTalkerDialogUI()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [CompilerGenerated]
        private sealed class <StartAuctionUITask>c__AnonStorey7
        {
            internal Action<bool> onEnd;
            internal bool? showZeroItem;
            internal NpcTalkerDialogBase $this;

            internal void <>m__0(UIProcess process, bool isCompleted)
            {
                this.$this.Pause();
                Action<bool> onEnd = this.onEnd;
                AuctionUITask.StartAuctionUITaskWithPrepare(this.$this.m_mainUIIntent, "Mode_Buy", NpcShopItemType.NpcShopItemType_Max, -1, delegate (bool res) {
                    if (res)
                    {
                        if (this.showZeroItem != null)
                        {
                            AuctionUITask.SetShowZeroItemState(this.showZeroItem.Value);
                        }
                    }
                    else
                    {
                        if (this.$this.m_mainUIIntent != null)
                        {
                            UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                        }
                        if (this.onEnd != null)
                        {
                            this.onEnd(true);
                        }
                    }
                }, onEnd, this.$this.m_backgroundManager);
            }

            internal void <>m__1(bool res)
            {
                if (res)
                {
                    if (this.showZeroItem != null)
                    {
                        AuctionUITask.SetShowZeroItemState(this.showZeroItem.Value);
                    }
                }
                else
                {
                    if (this.$this.m_mainUIIntent != null)
                    {
                        UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                    }
                    if (this.onEnd != null)
                    {
                        this.onEnd(true);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartNpcDialogQuestCompleteUITask>c__AnonStorey1
        {
            internal int optIdx;
            internal NpcTalkerDialogBase $this;

            internal void <>m__0(UIProcess process, bool isCompleted)
            {
                this.$this.Pause();
                SimpleNpcDialogUITask.ShowQuestTalkToCompleteDialog(this.$this.m_currentDialogInfo.OptList[this.optIdx].P1, this.$this.m_currentDialogInfo.OptList[this.optIdx].P2, delegate (int result) {
                    if (this.$this.m_mainUIIntent != null)
                    {
                        UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                    }
                });
            }

            internal void <>m__1(int result)
            {
                if (this.$this.m_mainUIIntent != null)
                {
                    UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartNpcShopUITask>c__AnonStorey3
        {
            internal Action<bool> onEnd;
            internal NpcTalkerDialogBase $this;

            internal void <>m__0(UIProcess process, bool isCompleted)
            {
                <StartNpcShopUITask>c__AnonStorey4 storey = new <StartNpcShopUITask>c__AnonStorey4 {
                    <>f__ref$3 = this,
                    isCompleted = isCompleted
                };
                this.$this.Pause();
                IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                NpcShopMainUITask.StartNpcShopMainUITask_Defult(new Action<bool>(storey.<>m__0), null, backgroundManager, null);
            }

            private sealed class <StartNpcShopUITask>c__AnonStorey4
            {
                internal bool isCompleted;
                internal NpcTalkerDialogBase.<StartNpcShopUITask>c__AnonStorey3 <>f__ref$3;

                internal void <>m__0(bool res)
                {
                    if (this.<>f__ref$3.onEnd != null)
                    {
                        this.<>f__ref$3.onEnd(this.isCompleted);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <StartQuestCommitItemUITask>c__AnonStorey2
        {
            internal int optIdx;
            internal NpcTalkerDialogBase $this;

            internal void <>m__0(UIProcess process, bool isCompleted)
            {
                this.$this.Pause();
                QuestCommitItemDialogUITask.ShowQuestCommitItemDialogUITask(this.$this.m_currentDialogInfo.OptList[this.optIdx], new Action(this.$this.ReturnToMainUI));
            }
        }

        [CompilerGenerated]
        private sealed class <StartQuestDialogUITask>c__AnonStorey0
        {
            internal ConfigDataQuestInfo questConfInfo;
            internal NpcTalkerDialogBase $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool result)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__1(UIProcess process, bool isCompleted)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__2(bool result)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__3(UIProcess process, bool isCompleted)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__4(int rst, int insId)
            {
            }

            [MethodImpl(0x8000)]
            internal void <>m__5()
            {
            }

            internal void <>m__6(int rst, int questInsId)
            {
                this.$this.Pause();
            }
        }

        [CompilerGenerated]
        private sealed class <StartQuestDialogUITaskFaction>c__AnonStorey8
        {
            internal ConfigDataQuestInfo questConfInfo;
            internal NpcTalkerDialogBase $this;
            private static Action<bool> <>f__am$cache0;

            internal void <>m__0(int result, int instId)
            {
                if (this.$this.State == Task.TaskState.Running)
                {
                    this.$this.PlayUIProcess(this.$this.m_npcTalkerUICtrl.GetPanelShorOrHideUIProcess(false, true), true, delegate (UIProcess process, bool isCompleted) {
                        this.$this.Pause();
                        UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                    }, false);
                }
            }

            internal void <>m__1(UIProcess process, bool isCompleted)
            {
                QuestAcceptDialogUITask.ShowFactionQuestAcceptDialog(null, new UnAcceptedQuestUIInfo(this.questConfInfo.ID, null, 0), delegate (int rst, int insId) {
                    if (rst != 0)
                    {
                        this.$this.Pause();
                        UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                    }
                    else
                    {
                        int fristFactionQuestInstanceId = ((LogicBlockQuestClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBQuest()).GetFristFactionQuestInstanceId();
                        if (fristFactionQuestInstanceId != 0)
                        {
                            this.$this.Pause();
                            ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBClientCustomData().SetActionPlanData("SelectedQuestInstanceId", (long) fristFactionQuestInstanceId);
                            if (<>f__am$cache0 == null)
                            {
                                <>f__am$cache0 = delegate (bool res) {
                                };
                            }
                            ActionPlanManagerUITask.StartActionPlanUITaskWithQuestInstanceId(this.$this.m_mainUIIntent, fristFactionQuestInstanceId, <>f__am$cache0, false, null);
                        }
                    }
                }, delegate {
                    this.$this.Pause();
                    UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                }, false);
            }

            internal void <>m__2(bool result)
            {
                QuestAcceptDialogUITask.ShowFactionQuestAcceptDialog(null, new UnAcceptedQuestUIInfo(this.questConfInfo.ID, null, 0), (rst, questInsId) => this.$this.Pause(), null, true);
            }

            internal void <>m__3(UIProcess process, bool isCompleted)
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
            }

            internal void <>m__4(int rst, int insId)
            {
                if (rst != 0)
                {
                    this.$this.Pause();
                    UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
                }
                else
                {
                    int fristFactionQuestInstanceId = ((LogicBlockQuestClient) ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBQuest()).GetFristFactionQuestInstanceId();
                    if (fristFactionQuestInstanceId != 0)
                    {
                        this.$this.Pause();
                        ((ProjectXPlayerContext) GameManager.Instance.PlayerContext).GetLBClientCustomData().SetActionPlanData("SelectedQuestInstanceId", (long) fristFactionQuestInstanceId);
                        if (<>f__am$cache0 == null)
                        {
                            <>f__am$cache0 = delegate (bool res) {
                            };
                        }
                        ActionPlanManagerUITask.StartActionPlanUITaskWithQuestInstanceId(this.$this.m_mainUIIntent, fristFactionQuestInstanceId, <>f__am$cache0, false, null);
                    }
                }
            }

            internal void <>m__5()
            {
                this.$this.Pause();
                UIManager.Instance.ReturnUITask(this.$this.m_mainUIIntent, null);
            }

            internal void <>m__6(int rst, int questInsId)
            {
                this.$this.Pause();
            }

            private static void <>m__7(bool res)
            {
            }
        }

        [CompilerGenerated]
        private sealed class <StartTradeNpcSaleShopUITask>c__AnonStorey5
        {
            internal Action<bool> onEnd;
            internal bool isLegal;
            internal NpcTalkerDialogBase $this;

            internal void <>m__0(Task task)
            {
                this.$this.EnableUIInput(true);
                TradeNpcShopInfoReqNetTask task2 = task as TradeNpcShopInfoReqNetTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.Result == 0)
                {
                    this.$this.PlayUIProcess(this.$this.m_npcTalkerUICtrl.GetPanelShorOrHideUIProcess(false, false), true, delegate (UIProcess process, bool isCompleted) {
                        <StartTradeNpcSaleShopUITask>c__AnonStorey6 storey = new <StartTradeNpcSaleShopUITask>c__AnonStorey6 {
                            <>f__ref$5 = this,
                            isCompleted = isCompleted
                        };
                        this.$this.Pause();
                        IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                        NpcShopMainUITask.StartNpcShopMainUITask_TradeSale(new Action<bool>(storey.<>m__0), this.isLegal, null, backgroundManager);
                    }, false);
                }
                else
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
            }

            internal void <>m__1(UIProcess process, bool isCompleted)
            {
                <StartTradeNpcSaleShopUITask>c__AnonStorey6 storey = new <StartTradeNpcSaleShopUITask>c__AnonStorey6 {
                    <>f__ref$5 = this,
                    isCompleted = isCompleted
                };
                this.$this.Pause();
                IUIBackgroundManager backgroundManager = this.$this.m_backgroundManager;
                NpcShopMainUITask.StartNpcShopMainUITask_TradeSale(new Action<bool>(storey.<>m__0), this.isLegal, null, backgroundManager);
            }

            private sealed class <StartTradeNpcSaleShopUITask>c__AnonStorey6
            {
                internal bool isCompleted;
                internal NpcTalkerDialogBase.<StartTradeNpcSaleShopUITask>c__AnonStorey5 <>f__ref$5;

                internal void <>m__0(bool res)
                {
                    if (this.<>f__ref$5.onEnd != null)
                    {
                        this.<>f__ref$5.onEnd(this.isCompleted);
                    }
                }
            }
        }
    }
}

