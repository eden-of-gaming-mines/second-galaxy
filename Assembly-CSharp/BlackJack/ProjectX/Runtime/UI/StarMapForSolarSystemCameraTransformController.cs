﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class StarMapForSolarSystemCameraTransformController : UIControllerBase
    {
        [AutoBind("./CameraPlatform", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraPlatform;
        [AutoBind("./CameraPlatform/YRotationRootForCamera", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_yRotationRootForCamera;
        [AutoBind("./CameraPlatform/YRotationRootForCamera/XRotationRootForCamera", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_xRotationRootForCamera;
        [AutoBind("./CameraPlatform/YRotationRootForCamera/XRotationRootForCamera/CameraDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraDummy;
        [AutoBind("./CameraRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_cameraRoot;
        [AutoBind("./CameraRoot/Camera", AutoBindAttribute.InitState.NotInit, false)]
        public Camera m_camera;
        private DateTime m_cameraSlerpStartTime;
        private TimeSpan m_cameraSlerpTimespan;
        private Vector3? m_cameraSlerpStartPostion;
        private Quaternion? m_cameraSlerpStartRotation;
        private Vector3? m_cameraSlerpOffsetLocalPosition;
        private float m_currCameraRotateAngleX;
        private float m_currCameraRotateAngleY;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnCameraLookAtTargetFinished;
        private static DelegateBridge __Hotfix_GetCamera;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_SetCameraSkyBox;
        private static DelegateBridge __Hotfix_UpdateCameraDockData;
        private static DelegateBridge __Hotfix_TransformCameraByRotating;
        private static DelegateBridge __Hotfix_SetCameraRotation;
        private static DelegateBridge __Hotfix_SetCameraLookAtTargetRotation;
        private static DelegateBridge __Hotfix_GetCurrCameraRotationAngle;
        private static DelegateBridge __Hotfix_GetCurrCameraRotation;
        private static DelegateBridge __Hotfix_SetCameraPlatformPosition;
        private static DelegateBridge __Hotfix_GetCameraPosition;
        private static DelegateBridge __Hotfix_UpdateCameraOffset;
        private static DelegateBridge __Hotfix_StartCameraSlerp;
        private static DelegateBridge __Hotfix_StopCameraSlerp;
        private static DelegateBridge __Hotfix_SetCameraOffsetZ;
        private static DelegateBridge __Hotfix_SetCameraOffsetX;
        private static DelegateBridge __Hotfix_GetCameraOffset;
        private static DelegateBridge __Hotfix_add_EventOnCameraLookAtTargetFinished;
        private static DelegateBridge __Hotfix_remove_EventOnCameraLookAtTargetFinished;

        public event Action EventOnCameraLookAtTargetFinished
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetCamera()
        {
        }

        [MethodImpl(0x8000)]
        public float GetCameraOffset()
        {
        }

        [MethodImpl(0x8000)]
        public Vector3 GetCameraPosition()
        {
        }

        [MethodImpl(0x8000)]
        public Quaternion GetCurrCameraRotation()
        {
        }

        [MethodImpl(0x8000)]
        public Vector2 GetCurrCameraRotationAngle()
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraLookAtTargetRotation(Vector3 targetPos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraOffsetX(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraOffsetZ(float offset)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraPlatformPosition(Vector3 newPos)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraRotation(float rotateAngleForAxisX, float rotateAngleForAxisY)
        {
        }

        [MethodImpl(0x8000)]
        public void SetCameraSkyBox(Material skyBoxMat)
        {
        }

        [MethodImpl(0x8000)]
        public void StartCameraSlerp(float timeSpan)
        {
        }

        [MethodImpl(0x8000)]
        public void StopCameraSlerp()
        {
        }

        [MethodImpl(0x8000)]
        public void TransformCameraByRotating(float rotateAngleForAxisX, float rotateAngleForAxisY)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCameraDockData()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateCameraOffset(float deltaOffset, float minOffset, float maxOffset)
        {
        }
    }
}

