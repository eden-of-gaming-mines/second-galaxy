﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class WormholeTeleportConfirmInfoUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action EventOnCancelButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnConfirmButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnBgButtonClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<bool, bool, bool> EventOnLossWarningButtonClick;
        private LossWarningButtonUIController m_lossWarningButtonUICtrl;
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_panelStateCtrl;
        [AutoBind("./DangerDetail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_enterWormholeName;
        [AutoBind("./DangerDetail/InfoGroup/GoInLevelTitleText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_enterWormholeLevel;
        [AutoBind("./DangerDetail/InfoGroup/TimesCountTitleText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_canUseEnterCount;
        [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./ContinueButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_bgButton;
        [AutoBind("./EnterButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_enterDarkRoomButton;
        [AutoBind("./DangerDetail/LossWarningDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_lossWarningDummy;
        [AutoBind("./DangerDetail/LossWarningButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_lossWarningButton;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetEnterWormholeName;
        private static DelegateBridge __Hotfix_SetEnterWormholeLimitLevel;
        private static DelegateBridge __Hotfix_SetEnterWormholeCount;
        private static DelegateBridge __Hotfix_GetPanelEffectProcess;
        private static DelegateBridge __Hotfix_SetLossWarningButtonState;
        private static DelegateBridge __Hotfix_GetLossWarningWindowDummyPos;
        private static DelegateBridge __Hotfix_OnCancelButtonClick;
        private static DelegateBridge __Hotfix_OnConfirmButtonClick;
        private static DelegateBridge __Hotfix_OnBgButtonClick;
        private static DelegateBridge __Hotfix_OnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnCancelButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnConfirmButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnBgButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnBgButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLossWarningButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLossWarningButtonClick;

        public event Action EventOnBgButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnCancelButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action EventOnConfirmButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<bool, bool, bool> EventOnLossWarningButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public Vector3 GetLossWarningWindowDummyPos()
        {
        }

        [MethodImpl(0x8000)]
        public CommonUIStateEffectProcess GetPanelEffectProcess(string stateName)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBgButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnCancelButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnConfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnLossWarningButtonClick(bool showAutoMove, bool showSafety, bool showDanger)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnterWormholeCount(bool extraCountObtained, int count, int maxCount)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnterWormholeLimitLevel(int level)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnterWormholeName(string name)
        {
        }

        [MethodImpl(0x8000)]
        public void SetLossWarningButtonState(bool isShow)
        {
        }
    }
}

