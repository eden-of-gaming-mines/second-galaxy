﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class BaseRedeployStartRedeployDialogUIController : BaseRedeployDialogUIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <DestSolarSystemId>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <DestSpaceStationId>k__BackingField;
        [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./DestinationInfoGroup/StarfieldsNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_starfieldsNameText;
        [AutoBind("./DestinationInfoGroup/GalaxyNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_solarSystemNameText;
        [AutoBind("./DestinationInfoGroup/StationNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_stationNameText;
        [AutoBind("./DestinationInfoGroup/SafeValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_securityLevelText;
        [AutoBind("./DestinationInfoGroup/FactionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_factionNameText;
        [AutoBind("./DestinationInfoGroup/DistanceValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_distanceValueText;
        [AutoBind("./Time/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_redeployRemainingTimeText;
        private static DelegateBridge __Hotfix_SetRedeployBaseInfo;
        private static DelegateBridge __Hotfix_get_CancelButtonName;
        private static DelegateBridge __Hotfix_get_ConfirmButtonName;
        private static DelegateBridge __Hotfix_SetRedeployTimeInfoForUI;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_DestSolarSystemId;
        private static DelegateBridge __Hotfix_set_DestSolarSystemId;
        private static DelegateBridge __Hotfix_get_DestSpaceStationId;
        private static DelegateBridge __Hotfix_set_DestSpaceStationId;

        [MethodImpl(0x8000)]
        public void SetRedeployBaseInfo(GDBSolarSystemInfo solarSystemInfo, int stationId)
        {
        }

        [MethodImpl(0x8000)]
        private void SetRedeployTimeInfoForUI(string info)
        {
        }

        protected override string CancelButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override string ConfirmButtonName
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        public int DestSolarSystemId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int DestSpaceStationId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

