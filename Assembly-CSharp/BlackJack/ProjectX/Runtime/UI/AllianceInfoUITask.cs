﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class AllianceInfoUITask : UITaskBase
    {
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private AllianceInfoUIController m_mainCtrl;
        private const string AllianceId = "AllianceId";
        public const string TaskName = "AllianceInfoUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask_0;
        private static DelegateBridge __Hotfix_StartTask_1;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UnregisterEvents;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateAllianceInfo;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_IsMember;
        private static DelegateBridge __Hotfix_OnManageAllianceClick;
        private static DelegateBridge __Hotfix_OnMemberClick;
        private static DelegateBridge __Hotfix_OnBattleListClick;
        private static DelegateBridge __Hotfix_Close;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_get_AllianceInfo;

        [MethodImpl(0x8000)]
        public AllianceInfoUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void Close()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsMember(uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBattleListClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnManageAllianceClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnMemberClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(BlackJack.ProjectX.Common.AllianceInfo allianceInfo, UIIntent prevIntent, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(uint allianceId, UIIntent prevIntent, Action<bool> onPrepareEnd = null, Action<bool> onPipeLineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnregisterEvents()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateAllianceInfo(uint allianceId, string allianceName)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private BlackJack.ProjectX.Common.AllianceInfo AllianceInfo
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;

            [MethodImpl(0x8000)]
            internal void <>m__0(AllianceInfo info)
            {
            }
        }
    }
}

