﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class InSpaceRescueSignalButtonUIController : RescueSignalButtonUIControllerBase
    {
        [AutoBindWithParent("../../../CanvasVolatile/FunButtonsPanel/MenuChatViewModeButton/SOSButton", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject LoopEffectRoot;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }
    }
}

