﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class TechCategoryToggleUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <TechCategoryId>k__BackingField;
        private List<ConfigDataTechUITypeInfo> m_typeUIInfoList;
        [AutoBind("./TitleToggle", AutoBindAttribute.InitState.NotInit, false)]
        public ToggleEx CategoryToggle;
        [AutoBind("./TitleToggle/IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CategoryIconImage;
        [AutoBind("/TitleToggle/UpgradeImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image UpgradeImage;
        [AutoBind("./TitleToggle/Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text CategoryNameText;
        [AutoBind("./TechTypeItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject TechTypeItemRoot;
        [AutoBind("./TitleToggle/EffectState", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController ToggleShowStateEffectCtrl;
        private static DelegateBridge __Hotfix_InitTechCategoryInfo;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_AddChildTechUITypeItem;
        private static DelegateBridge __Hotfix_SetToggleState;
        private static DelegateBridge __Hotfix_SetUpgradeState;
        private static DelegateBridge __Hotfix_ShowChildrenItems;
        private static DelegateBridge __Hotfix_PlayToggleSelectEffect;
        private static DelegateBridge __Hotfix_set_TechCategoryId;
        private static DelegateBridge __Hotfix_get_TechCategoryId;
        private static DelegateBridge __Hotfix_get_LBTechClient;

        [MethodImpl(0x8000)]
        public void AddChildTechUITypeItem(GameObject typeItemGO)
        {
        }

        [MethodImpl(0x8000)]
        public void InitTechCategoryInfo(ConfigDataTechTypeInfo techTypeInfo, List<ConfigDataTechUITypeInfo> typeUIInfoList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayToggleSelectEffect()
        {
        }

        [MethodImpl(0x8000)]
        public void SetToggleState(bool isOn)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUpgradeState(bool isUpgrade)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowChildrenItems(bool show)
        {
        }

        [MethodImpl(0x8000)]
        public void Update()
        {
        }

        public int TechCategoryId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        private LogicBlockTechClient LBTechClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }
    }
}

