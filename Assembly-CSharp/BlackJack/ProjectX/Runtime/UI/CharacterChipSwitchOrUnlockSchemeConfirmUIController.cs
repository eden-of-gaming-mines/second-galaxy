﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CharacterChipSwitchOrUnlockSchemeConfirmUIController : UIControllerBase
    {
        private string m_chipSchemeString;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./PagingGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmStateCtrl;
        [AutoBind("./Buttons/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmButton;
        [AutoBind("./Buttons/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_cancelButton;
        [AutoBind("./PagingGroup/CutPagingGroup/SchemeOneText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currSchemeText;
        [AutoBind("./PagingGroup/CutPagingGroup/SchemeTwoText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_switchSchemeText;
        [AutoBind("./PagingGroup/DeblockingPagingGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_unlockStateCtrl;
        [AutoBind("./PagingGroup/DeblockingPagingGroup/ConsumeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unlockCostText;
        [AutoBind("./PagingGroup/DeblockingPagingGroup/DeblockingText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_unlockSchemeText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetMainUIProcess;
        private static DelegateBridge __Hotfix_UpdateSwitchUI;
        private static DelegateBridge __Hotfix_UpdateUnLockUI;
        private static DelegateBridge __Hotfix_GetChipSchemeString2;

        [MethodImpl(0x8000)]
        private string GetChipSchemeString2(int schemeIndex)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess GetMainUIProcess(bool show)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSwitchUI(int currSchemeIndex, int destSchemeIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateUnLockUI(int cost, ulong playerMoney, int unlockSchemeIndex)
        {
        }
    }
}

