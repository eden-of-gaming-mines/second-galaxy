﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class GuildFlagShipHangarListItemUIController : UIControllerBase, IClickableScrollItem, IClickable, IScrollItem
    {
        private const string SelectedState = "Choose";
        private const string UnselectedState = "Normal";
        public GameObject m_templeteGameObject;
        public ScrollItemBaseUIController m_scrollCtrl;
        public List<GuildFlagShipHangarShipListItemUIController> m_shipList;
        private const string ShipItemAssetName = "ShipItem";
        [AutoBind("./DestoryedGroup", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject DestoryedGroupGameObject;
        [AutoBind("./ChooseImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController SelectedStateCtrl;
        [AutoBind("./GuilgHangarShipListItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform FlagShipGroup;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        private static DelegateBridge __Hotfix_UpdateGuildHangarListItem;
        private static DelegateBridge __Hotfix_SetSelectState;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_RegisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemDoubleClickEvent;
        private static DelegateBridge __Hotfix_UnregisterItemClickEvent;
        private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_UnregisterItemNeedFillEvent;
        private static DelegateBridge __Hotfix_GetItemIndex;
        private static DelegateBridge __Hotfix_SetItemIndex;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateHangarShipList;
        private static DelegateBridge __Hotfix_UpdateHangarShipListForNormalHangar;
        private static DelegateBridge __Hotfix_UpdateHangarShipListForDestoryedHangar;

        [MethodImpl(0x8000)]
        public int GetItemIndex()
        {
        }

        [MethodImpl(0x8000)]
        public void Init(bool isCareClick = true)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemDoubleClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void SetItemIndex(int itemIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelectState(bool select)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemClickEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UnregisterItemNeedFillEvent(Action<UIControllerBase> action)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildHangarListItem(IGuildFlagShipHangarDataContainer hangarInfo, Dictionary<string, UnityEngine.Object> dynamicResCacheDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateHangarShipList(IGuildFlagShipHangarDataContainer shipHangar, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateHangarShipListForDestoryedHangar(IGuildFlagShipHangarDataContainer shipHangar, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateHangarShipListForNormalHangar(IGuildFlagShipHangarDataContainer shipHangar, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

