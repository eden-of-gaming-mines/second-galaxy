﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Runtime.CompilerServices;

    public class DevelopmentManagerUITask : UITaskBase
    {
        public const string TaskName = "DevelopmentManagerUITask";
        public const string ParamKeyResetTask = "ParamKeyResetTask";
        private const string ManagerPanelStateShow = "Show";
        private const string ManagerPanelStateClose = "Close";
        private bool m_initSelectionPanelSuccess;
        private bool m_initTransformationPanelSuccess;
        private bool m_initBackGroundSuccess;
        private DevelopmentSelectionUITask m_selectionPanelUITask;
        private DevelopmentTransformationUITask m_transformationPanelUITask;
        private DevelopmentBgUITask m_backGroundUITask;
        private SubPanelType m_subPanelType;
        private DevelopmentManagerUIController m_developmentManagertUIController;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_OnRequestCloseDevelopmentPanel;
        private static DelegateBridge __Hotfix_OnRequestShowOrHideAllPanels;
        private static DelegateBridge __Hotfix_OnRequestTransformationPanelReturnToPreviousPanel;
        private static DelegateBridge __Hotfix_OnRequestOpenTransformationPanel;
        private static DelegateBridge __Hotfix_OnRequestGetIntent;
        private static DelegateBridge __Hotfix_OnRequestBgTaskChange;
        private static DelegateBridge __Hotfix_OnRequestOpenPreviewHintPanel;
        private static DelegateBridge __Hotfix_OnRequestOpenResultHintPanel;
        private static DelegateBridge __Hotfix_OnRequestSetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_OnRequestGetUIBackgroundManager;
        private static DelegateBridge __Hotfix_OnTransformationSuccess;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnBindMoneyAddClick;
        private static DelegateBridge __Hotfix_OnTradeMoneyAddClick;
        private static DelegateBridge __Hotfix_InitParams;
        private static DelegateBridge __Hotfix_OnDevelopmentSelectionUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_OnDevelopmeentTransformationUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_OnDevelopmentBgUITaskResLoadComplete;
        private static DelegateBridge __Hotfix_ShowOrHideAllPanel;
        private static DelegateBridge __Hotfix_CreateBackgroupManager;
        private static DelegateBridge __Hotfix_SetSystemDescriptionButtonState;
        private static DelegateBridge __Hotfix_SetCurrencyValues;
        private static DelegateBridge __Hotfix_StopAllDevelopmentSounds;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_OnBasePanelAutoFillButtonClick_UserGuide;
        private static DelegateBridge __Hotfix_OnBasePanelLaunchButtonClick_UserGuide;

        [MethodImpl(0x8000)]
        public DevelopmentManagerUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private DevelopmentBGUIBackgroupManager CreateBackgroupManager()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void InitParams(UIIntentCustom intentCustom)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBasePanelAutoFillButtonClick_UserGuide(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnBasePanelLaunchButtonClick_UserGuide(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBindMoneyAddClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDevelopmeentTransformationUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDevelopmentBgUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDevelopmentSelectionUITaskResLoadComplete()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestBgTaskChange(DevelopmentBgUITask.BackGroundStateType backGroundType, string previewIconStr, FakeLBStoreItem item, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestCloseDevelopmentPanel(bool immediateComplate, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestGetIntent(Action<UIIntent> onGetIntent)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestGetUIBackgroundManager(Action<IUIBackgroundManager> onGetBackground)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestOpenPreviewHintPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestOpenResultHintPanel()
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestOpenTransformationPanel(object nodeInfos, object nodeInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestSetSystemDescriptionButtonState(bool isShow, SystemFuncDescType funcType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestShowOrHideAllPanels(bool isShow, bool immediateComplate, Action onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRequestTransformationPanelReturnToPreviousPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        private void OnTradeMoneyAddClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnTransformationSuccess()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        private void SetCurrencyValues()
        {
        }

        [MethodImpl(0x8000)]
        private void SetSystemDescriptionButtonState()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowOrHideAllPanel(bool isShow, bool immediateComplate, Action<bool> onFunctionEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void StopAllDevelopmentSounds()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnRequestCloseDevelopmentPanel>c__AnonStorey1
        {
            internal Action onEnd;
            internal DevelopmentManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnRequestShowOrHideAllPanels>c__AnonStorey2
        {
            internal Action onEnd;

            internal void <>m__0(bool res)
            {
                if (this.onEnd != null)
                {
                    this.onEnd();
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowOrHideAllPanel>c__AnonStorey3
        {
            internal bool isShow;
            internal bool immediateComplate;
            internal Action<bool> onFunctionEnd;
            internal DevelopmentManagerUITask $this;

            internal void <>m__0(Action<bool> onEnd)
            {
                this.$this.m_selectionPanelUITask.ShowOrHideSelectionPanel(this.isShow, this.immediateComplate, onEnd);
            }

            internal void <>m__1(Action<bool> onEnd)
            {
                this.$this.m_transformationPanelUITask.ShowOrHideTransformationPanel(this.isShow, this.immediateComplate, onEnd);
            }

            internal void <>m__2(UIProcess process, bool b)
            {
                if (this.onFunctionEnd != null)
                {
                    this.onFunctionEnd(b);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <UpdateView>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal DevelopmentManagerUITask $this;

            internal void <>m__0(bool res)
            {
                this.$this.m_backGroundUITask.UpdateBgTask(DevelopmentBgUITask.BackGroundStateType.Idle, null, null, this.onEnd);
            }
        }

        public enum SubPanelType
        {
            SelectionPanel,
            TransformationPanel
        }
    }
}

