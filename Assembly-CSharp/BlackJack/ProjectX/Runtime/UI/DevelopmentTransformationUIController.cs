﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using MarchingBytes;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    public sealed class DevelopmentTransformationUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnBasePanelCatalystItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnBasePanelDisplayedItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnItemSelectPanelScrollViewItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase> EventOnResultHintPanelItemClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase> EventOnPreviewHintPanelItemClick;
        private const string CommonItem = "CommonItem";
        private static readonly Dictionary<string, Vector2Int[]> m_sPreDefineItemAreaIndex;
        private CommonItemIconUIController m_catalystItemCtrl;
        private CommonItemIconUIController m_resultHintPanelItemCtrl;
        private CurrentScrollRectFillState m_fillState;
        private Transform[] m_lineDisplayTf;
        private DevelopmentItemDisplayLineUIController[] m_itemDisplayLineCtrl;
        private Dictionary<string, UnityEngine.Object> m_cachedDynamicDict;
        private List<FakeLBStoreItem> m_cachedDynamicItemList;
        private int m_selectIndex = -1;
        [AutoBind("./BasePanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_basePanelStateCtrl;
        [AutoBind("./ItemSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemSelectPanelStateCtrl;
        [AutoBind("./ConfirmationHintPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_confirmationHintPanelStateCtrl;
        [AutoBind("./ShortOfItemHintPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_shortOfItemHintPanelStateCtrl;
        [AutoBind("./ResultHintPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_resultHintPanelStateCtrl;
        [AutoBind("./PreviewHintPanel", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_previewHintPanelStateCtrl;
        [AutoBind("./EasyObjectPool", AutoBindAttribute.InitState.NotInit, false)]
        public EasyObjectPool m_easyObjectPool;
        [AutoBind("./EasyObjectPool/ObjectRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_objectRootTf;
        [AutoBind("./EasyObjectPool/ObjectRoot/TemplateObject", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_templateObjectTf;
        [AutoBind("./BasePanel/DescriptionArea/ProjectIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_projectIconImage;
        [AutoBind("./BasePanel/DescriptionArea/ProjectIconText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_projectNameText;
        [AutoBind("./BasePanel/DescriptionArea/SecondaryNodeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_secondaryNodeText;
        [AutoBind("./BasePanel/DescriptionArea/TertiaryNodeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_tertiaryNodeText;
        [AutoBind("./BasePanel/DescriptionArea/FunctionNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_functionNameText;
        [AutoBind("./BasePanel/DescriptionArea/DetailDescText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_detailDescText;
        [AutoBind("./BasePanel/CatalystGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_catalystStateCtrl;
        [AutoBind("./BasePanel/CatalystGroup/CatalystItem/ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_catalystItemTf;
        [AutoBind("./BasePanel/CatalystGroup/CatalystItem/CatalystNumberText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_catalystNumberText;
        [AutoBind("./BasePanel/SimpleItemInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_basePanelSimpleInfoTf;
        [AutoBind("./BasePanel/ItemDisplayGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_itemAreaStateCtrl;
        [AutoBind("./BasePanel/ItemDisplayGroup/LineGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_defaultLineGroupTf;
        [AutoBind("./BasePanel/ItemDisplayGroup/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_defaultItemGroupTf;
        [AutoBind("./BasePanel/LaunchButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_launchButtonGroupStateCtrl;
        [AutoBind("./BasePanel/LaunchButtonGroup/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_currencySpentStateCtrl;
        [AutoBind("./BasePanel/LaunchButtonGroup/ConfirmButton/CostText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_currencySpentText;
        [AutoBind("./BasePanel/LaunchButtonGroup/ConfirmButton/CostText/MoneyIconBgImage/MoneyIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_currencyIconImage;
        [AutoBind("./BasePanel/LaunchButtonGroup/HintBgImage/HintText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_launchButtonHintText;
        [AutoBind("./BasePanel/ModifyButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_basePanelModifyButton;
        [AutoBind("./BasePanel/LaunchButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_basePanelLaunchButton;
        [AutoBind("./BasePanel/AutoFillButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_basePanelAutoFillButton;
        [AutoBind("./ItemSelectPanel/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_itemSelectPanelScrollRect;
        [AutoBind("./ItemSelectPanel/MaskImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemSelectPanelCancelButton;
        [AutoBind("./ItemSelectPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_itemSelectPanelConfirmButton;
        [AutoBind("./ItemSelectPanel/ItemSimpleInfoUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemSelectPanelSimpleInfoTf;
        [AutoBind("./ItemGroupHighLightRoot/LineGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_heightLightLineGroupTf;
        [AutoBind("./ItemGroupHighLightRoot/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_heightLightItemGroupTf;
        [AutoBind("./ConfirmationHintPanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmationHintPanelBackButton;
        [AutoBind("./ConfirmationHintPanel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmationHintPanelCancelButton;
        [AutoBind("./ConfirmationHintPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_confirmationHintPanelConfirmButton;
        [AutoBind("./ConfirmationHintPanel/ContentText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_confirmationHintPanelContextText;
        [AutoBind("./ShortOfItemHintPanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_shortOfItemHintPanelBackButton;
        [AutoBind("./ShortOfItemHintPanel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_shortOfItemHintPanelCancelButton;
        [AutoBind("./ShortOfItemHintPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_shortOfItemHintPanelTransferToAuctionPanelButton;
        [AutoBind("./ResultHintPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_resultHintPanelItemGroupTf;
        [AutoBind("./ResultHintPanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_resultHintPanelBackButton;
        [AutoBind("./ResultHintPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_resultHintPanelConfirmButton;
        [AutoBind("./ResultHintPanel/SimpleItemInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_resultHintPanelSimpleInfoTf;
        [AutoBind("./PreviewHintPanel/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_previewHintPanelBackButton;
        [AutoBind("./PreviewHintPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_previewHintPanelCloseButton;
        [AutoBind("./PreviewHintPanel/ButtonGroup/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_previewHintPanelConfirmButton;
        [AutoBind("./PreviewHintPanel/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
        public LoopVerticalScrollRect m_previewHintPanelScrollRect;
        [AutoBind("./PreviewHintPanel/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public GridLayoutGroup m_previewHintPanelContentGridLayoutGroup;
        [AutoBind("./PreviewHintPanel/SimpleItemInfoDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_previewHintPanelItemTf;
        private static DelegateBridge __Hotfix_CreateBasePanelProcess;
        private static DelegateBridge __Hotfix_CreateItemSelectPanelProcess;
        private static DelegateBridge __Hotfix_CreateConfirmationHintPanelProcess;
        private static DelegateBridge __Hotfix_CreateShortOfItemHintPanelProcess;
        private static DelegateBridge __Hotfix_CreateResultHintPanelProcess;
        private static DelegateBridge __Hotfix_CreatePreviewHintPanelProcess;
        private static DelegateBridge __Hotfix_UpdateBasePanelDescriptionArea;
        private static DelegateBridge __Hotfix_UpdateBasePanelCatalystArea;
        private static DelegateBridge __Hotfix_UpdateBasePanelItemDisplayArea;
        private static DelegateBridge __Hotfix_UpdateBasePanelLaunchButtonArea;
        private static DelegateBridge __Hotfix_UpdateItemSelectPanelScrollViewArea;
        private static DelegateBridge __Hotfix_UpdateItemSelectPanelScrollRectSelectState;
        private static DelegateBridge __Hotfix_UpdateItemSelectPanelItemArea;
        private static DelegateBridge __Hotfix_UpdateConfirmationHintPanel;
        private static DelegateBridge __Hotfix_UpdateResultHintPanelAllDisplayedItems;
        private static DelegateBridge __Hotfix_UpdatePreviewHintPanelAllDisplayedItems;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetParent;
        private static DelegateBridge __Hotfix_OnItemPoolObjectCreated;
        private static DelegateBridge __Hotfix_OnItemPoolObjectFill;
        private static DelegateBridge __Hotfix_OnBasePanelCatalystItemClick;
        private static DelegateBridge __Hotfix_OnBasePanelDisplayedItemClick;
        private static DelegateBridge __Hotfix_OnItemSelectPanelScrollViewItemClick;
        private static DelegateBridge __Hotfix_OnResultHintPanelItemClick;
        private static DelegateBridge __Hotfix_OnPreviewHintPanelItemClick;
        private static DelegateBridge __Hotfix_add_EventOnBasePanelCatalystItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnBasePanelCatalystItemClick;
        private static DelegateBridge __Hotfix_add_EventOnBasePanelDisplayedItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnBasePanelDisplayedItemClick;
        private static DelegateBridge __Hotfix_add_EventOnItemSelectPanelScrollViewItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnItemSelectPanelScrollViewItemClick;
        private static DelegateBridge __Hotfix_add_EventOnResultHintPanelItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnResultHintPanelItemClick;
        private static DelegateBridge __Hotfix_add_EventOnPreviewHintPanelItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnPreviewHintPanelItemClick;

        public event Action<UIControllerBase> EventOnBasePanelCatalystItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnBasePanelDisplayedItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnItemSelectPanelScrollViewItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnPreviewHintPanelItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase> EventOnResultHintPanelItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateBasePanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateConfirmationHintPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateItemSelectPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatePreviewHintPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateResultHintPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreateShortOfItemHintPanelProcess(string stateName, bool immediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBasePanelCatalystItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBasePanelDisplayedItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemPoolObjectCreated(string poolName, GameObject item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemPoolObjectFill(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemSelectPanelScrollViewItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnPreviewHintPanelItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnResultHintPanelItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private static void SetParent(Transform parent, Transform child, bool worldPositionStays = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBasePanelCatalystArea(string stateName, FakeLBStoreItem catalystItem, string countStr, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBasePanelDescriptionArea(ConfigDataDevelopmentNodeDescInfo[] nodeInfos, ConfigDataDevelopmentProjectInfo functionInfo, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBasePanelItemDisplayArea(string itemAreaStateName, ConfigDataDevelopmentProjectInfo functionInfo, DevelopmentTransformationUITask.ItemDisplayInfoMatrixWrapper wrapper, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBasePanelLaunchButtonArea(string launchButtonGroupStateName, string currencySpentStateName, FakeLBStoreItem currencyItem, string hintText, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateConfirmationHintPanel(string descriptionStr)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemSelectPanelItemArea(string itemAreaStateName, int lineIndex, DevelopmentTransformationUITask.ItemDisplayInfo[] itemInfos)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemSelectPanelScrollRectSelectState(int selectIndex)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemSelectPanelScrollViewArea(List<FakeLBStoreItem> itemList, int selectIndex, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdatePreviewHintPanelAllDisplayedItems(List<FakeLBStoreItem> itemList, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateResultHintPanelAllDisplayedItems(FakeLBStoreItem resultItem, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        private enum CurrentScrollRectFillState
        {
            StateInValid,
            ItemSelectPanel,
            PreivewHintPanel
        }
    }
}

