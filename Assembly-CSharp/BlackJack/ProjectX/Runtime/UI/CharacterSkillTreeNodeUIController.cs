﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CharacterSkillTreeNodeUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<ConfigDataPassiveSkillInfo> EventOnSkillTreeNodeClick;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_nodeStateCtrl;
        [AutoBind("", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_nodeButton;
        [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_skillIconImage;
        [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_levelInfoText;
        [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_levelState;
        [AutoBind("./RedPoint", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_redPoint;
        private ConfigDataPassiveSkillInfo m_skillConfigData;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegEventOnSkillTreeNodeClick;
        private static DelegateBridge __Hotfix_SetUIInfoBySkillConfigData;
        private static DelegateBridge __Hotfix_Clear;
        private static DelegateBridge __Hotfix_OnSkillTreeNodeClick;
        private static DelegateBridge __Hotfix_add_EventOnSkillTreeNodeClick;
        private static DelegateBridge __Hotfix_remove_EventOnSkillTreeNodeClick;
        private static DelegateBridge __Hotfix_get_SkillConfigData;
        private static DelegateBridge __Hotfix_set_SkillConfigData;

        public event Action<ConfigDataPassiveSkillInfo> EventOnSkillTreeNodeClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public override void Clear()
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnSkillTreeNodeClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegEventOnSkillTreeNodeClick(Action<ConfigDataPassiveSkillInfo> onClick)
        {
        }

        [MethodImpl(0x8000)]
        public void SetUIInfoBySkillConfigData(ConfigDataPassiveSkillInfo skillInfo, Sprite icon, int skillLevel, bool isLocked, bool showRedPoint)
        {
        }

        public ConfigDataPassiveSkillInfo SkillConfigData
        {
            [MethodImpl(0x8000)]
            get
            {
            }
            [MethodImpl(0x8000)]
            private set
            {
            }
        }

        [CompilerGenerated]
        private sealed class <SetUIInfoBySkillConfigData>c__AnonStorey0
        {
            internal int skillLevel;
            internal ConfigDataPassiveSkillInfo skillInfo;
            internal CharacterSkillTreeNodeUIController $this;

            [MethodImpl(0x8000)]
            internal void <>m__0(bool ret)
            {
            }
        }
    }
}

