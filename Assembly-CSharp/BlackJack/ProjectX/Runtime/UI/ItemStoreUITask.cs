﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common.Client;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class ItemStoreUITask : UITaskBase
    {
        private GetOrUnLoadItemWindowUITask m_getItemWindowUITask;
        private ItemStoreDefaultUIFilter m_itemStoreFilter;
        private ItemStoreUIController m_itemStoreUICtrl;
        private ItemStoreListUIController m_itemListCtrl;
        private ItemSimpleInfoUIController m_itemInfoCtrl;
        private bool m_isDuringUpdateView;
        private List<ILBStoreItemClient> m_cachedStoreItemList;
        private DateTime m_cacheUpdateTime;
        private int m_selectedItemIndex;
        private IUIBackgroundManager m_backgroundManager;
        public static string ParamKey_RefreshItemList;
        public static string ParamKey_RefreshSeletecItem;
        public const string ParamKey_BackgroundManager = "BackgroundManager";
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        protected static string ItemStoreUIPrefabAssetPath;
        public const string TaskName = "ItemStoreUITask";
        private ILBStoreItemClient m_selectItem;
        private bool m_isRepeatableUserGuide;
        private const string ParamKeySelectedItem = "SelectedItem";
        private const string ParamKeyIsRepatebleUserGuide = "IsRepatebleUserGuide";
        private ItemStoreRepeatableState m_currRepeatableState;
        [CompilerGenerated]
        private static Comparison<ILBStoreItemClient> <>f__mg$cache0;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_CollectResPathForReserve;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_ClearContextOnIntentChange;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_ClearContextOnPause;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateItemListUI;
        private static DelegateBridge __Hotfix_ClearContextOnUpdateViewEnd;
        private static DelegateBridge __Hotfix_PostUpdateViewBeforeClearContext;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_GetLayerCamera;
        private static DelegateBridge __Hotfix_ScrollToEnd;
        private static DelegateBridge __Hotfix_GetFirstExploreTicketItem;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnItemListUIDropdownChanged;
        private static DelegateBridge __Hotfix_OnItemListUITabChanged_0;
        private static DelegateBridge __Hotfix_OnItemListUITabChanged_1;
        private static DelegateBridge __Hotfix_OnItemListClick_0;
        private static DelegateBridge __Hotfix_OnItemListClick_1;
        private static DelegateBridge __Hotfix_OnItemListClickImp;
        private static DelegateBridge __Hotfix_OnItem3DTouch;
        private static DelegateBridge __Hotfix_OnShareButtonClick;
        private static DelegateBridge __Hotfix_ShowDetailInfoForSelectedItem;
        private static DelegateBridge __Hotfix_OnGoProduceButtonClick;
        private static DelegateBridge __Hotfix_OnItemPreviewButtonClick;
        private static DelegateBridge __Hotfix_OnGoToCrackButtonClick;
        private static DelegateBridge __Hotfix_OnUseButtonClick_ForceUseOne;
        private static DelegateBridge __Hotfix_OnUseButtonClick;
        private static DelegateBridge __Hotfix_OnAddMoneyClick;
        private static DelegateBridge __Hotfix_UseSelectedItem;
        private static DelegateBridge __Hotfix_SendStoreItemUseReq;
        private static DelegateBridge __Hotfix_OnUseItemTaskClose;
        private static DelegateBridge __Hotfix_ShowExploreTicketCallback;
        private static DelegateBridge __Hotfix_ShowGetResultItemCurrencyPopupwindow;
        private static DelegateBridge __Hotfix_OnItemDetailInfoButtonClick;
        private static DelegateBridge __Hotfix_OnPutInButtonClick;
        private static DelegateBridge __Hotfix_OnGetItemWindowUITaskEnterAnotherTask;
        private static DelegateBridge __Hotfix_OnGetOrUnloadItemWindowReturnToPreTask;
        private static DelegateBridge __Hotfix_OnGotoTechButtonClick;
        private static DelegateBridge __Hotfix_UpdateSelectedItemSingleInfoPanel;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitPipeLineStateFromUIIntent;
        private static DelegateBridge __Hotfix_LeaveCurrBackGround;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;
        private static DelegateBridge __Hotfix_StartItemStoreUITaskWithItem;
        private static DelegateBridge __Hotfix_InitRepatableUserGuideData;
        private static DelegateBridge __Hotfix_StartRepeatableUserGuide;
        private static DelegateBridge __Hotfix_SetUserGuideToNextStep;
        private static DelegateBridge __Hotfix_GoToUserGuideStateForItemStore;
        private static DelegateBridge __Hotfix_OnClickReturn4RepeatableUserGuide;
        private static DelegateBridge __Hotfix_StopRepeatableUserGuide;
        private static DelegateBridge __Hotfix_GetItemStoreTabTypeFromItem;

        [MethodImpl(0x8000)]
        public ItemStoreUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnIntentChange(UIIntent newIntent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnPause()
        {
        }

        [MethodImpl(0x8000)]
        protected override void ClearContextOnUpdateViewEnd()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        public static void CollectResPathForReserve(List<string> resList)
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        public RectTransform GetFirstExploreTicketItem()
        {
        }

        [MethodImpl(0x8000)]
        protected ItemStoreListUIController.ItemStoreTabType GetItemStoreTabTypeFromItem(ILBStoreItemClient item)
        {
        }

        [MethodImpl(0x8000)]
        public Camera GetLayerCamera()
        {
        }

        [MethodImpl(0x8000)]
        private void GoToUserGuideStateForItemStore(ItemStoreRepeatableState state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void InitPipeLineStateFromUIIntent(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        private void InitRepatableUserGuideData(UIIntent intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private void LeaveCurrBackGround()
        {
        }

        [MethodImpl(0x8000)]
        private void OnAddMoneyClick(CurrencyType type)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        private void OnClickReturn4RepeatableUserGuide(bool result)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetItemWindowUITaskEnterAnotherTask(bool needPause)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetOrUnloadItemWindowReturnToPreTask()
        {
        }

        [MethodImpl(0x8000)]
        public void OnGoProduceButtonClick(UIControllerBase uCtl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnGoToCrackButtonClick(UIControllerBase uCtl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnGotoTechButtonClick(int techId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItem3DTouch(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemDetailInfoButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        public void OnItemListClick(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListClick(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListClickImp(int idx, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUIDropdownChanged(int dropdownIndex)
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemListUITabChanged(ItemStoreListUIController.ItemStoreTabType itemTabType)
        {
        }

        [MethodImpl(0x8000)]
        public void OnItemListUITabChanged(ItemStoreListUIController.ItemStoreTabType itemTabType, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void OnItemPreviewButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPutInButtonClick(UIControllerBase controllerBase)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnShareButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUseButtonClick(UIControllerBase uCtl)
        {
        }

        [MethodImpl(0x8000)]
        public void OnUseButtonClick_ForceUseOne(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnUseItemTaskClose(Task task)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateViewBeforeClearContext()
        {
        }

        [MethodImpl(0x8000)]
        public void ScrollToEnd(Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendStoreItemUseReq(ILBStoreItemClient item, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SetUserGuideToNextStep()
        {
        }

        [MethodImpl(0x8000)]
        private void ShowDetailInfoForSelectedItem(int idx)
        {
        }

        [MethodImpl(0x8000)]
        private bool ShowExploreTicketCallback(NormalItemType itemType, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void ShowGetResultItemCurrencyPopupwindow(List<StoreItemUpdateInfo> UseResultItemList, List<CurrencyUpdateInfo> UseResultCurrencyList)
        {
        }

        [MethodImpl(0x8000)]
        public static UITaskBase StartItemStoreUITaskWithItem(UIIntent returnIntent, ILBStoreItemClient item, IUIBackgroundManager backgroundManager = null)
        {
        }

        [MethodImpl(0x8000)]
        private void StartRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        private void StopRepeatableUserGuide()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateItemListUI()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateSelectedItemSingleInfoPanel()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        private void UseSelectedItem(Action<bool> onEnd)
        {
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <OnPutInButtonClick>c__AnonStorey2
        {
            internal int hangarIndex;
            internal ItemStoreUITask $this;

            internal void <>m__0(Task task)
            {
                HangarUnpackShipReqNetTask task2 = task as HangarUnpackShipReqNetTask;
                if (!task2.IsNetworkError)
                {
                    if ((task2 == null) || (task2.Result != 0))
                    {
                        Debug.LogError("ShipHangarAssignUITask:: StartHangarUnpackShipReq      failed code:" + task2.Result);
                    }
                    else
                    {
                        this.$this.EnableUIInput(false);
                        this.$this.m_selectedItemIndex = -1;
                        ((UIIntentCustom) this.$this.m_currIntent).SetParam(ItemStoreUITask.ParamKey_RefreshSeletecItem, true);
                        this.$this.Pause();
                        this.$this.LeaveCurrBackGround();
                        UIIntentReturnable intent = new UIIntentReturnable(this.$this.m_currIntent, "ShipHangarUITask", ShipHangarUITask.ShipListOverviewMode);
                        intent.SetParam(ShipHangarUITask.ParamKey_ShipListRefreshMode, ShipHangarUITask.ShipListRefreshMode_RefreshAll);
                        intent.SetParam(ShipHangarUITask.ParamKey_SelectHangarShipIndex, this.hangarIndex);
                        if (UIManager.Instance.StartUITask(intent, true, false, null, null) == null)
                        {
                            Debug.LogError("Start ShipHangarUITask fail");
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendStoreItemUseReq>c__AnonStorey0
        {
            internal Action<bool> onEnd;
            internal NormalItemType itemType;
            internal ItemStoreUITask $this;

            internal void <>m__0(Task task)
            {
                StoreItemUseReqNetTask task2 = task as StoreItemUseReqNetTask;
                if (task2.IsNetworkError)
                {
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
                else if (task2.AckResult == 0)
                {
                    this.$this.ShowGetResultItemCurrencyPopupwindow(task2.StoreItemUpdateInfoList, task2.CurrencyUpdateInfoList);
                    if (!this.$this.ShowExploreTicketCallback(this.itemType, this.onEnd))
                    {
                        this.$this.StartUpdatePipeLine(this.$this.m_currIntent, false, false, true, null);
                    }
                }
                else
                {
                    Debug.LogError($"StoreItemUseReqNetTask error: returnTask.AckResult = {task2.AckResult}");
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.AckResult, true, false);
                    if (this.onEnd != null)
                    {
                        this.onEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <ShowExploreTicketCallback>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal ItemStoreUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    this.$this.Resume(null, null);
                }
                if (this.onEnd != null)
                {
                    this.onEnd(res);
                }
            }
        }

        internal enum ItemStoreRepeatableState
        {
            Start,
            SelectItem,
            UseItemButtonClick,
            End
        }

        protected enum PipeLineStateMaskType
        {
            IsRefreshAllItem,
            IsRefreshSingleItem,
            IsUpdateSelectItemSimpleInfo,
            IsNeedLoadDynamicRes,
            IsItemFilterChanged,
            IsDropdownChanged
        }
    }
}

