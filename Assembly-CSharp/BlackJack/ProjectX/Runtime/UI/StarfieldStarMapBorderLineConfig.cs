﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using UnityEngine;

    [Serializable]
    public class StarfieldStarMapBorderLineConfig
    {
        [Header("描边完全透明时的缩放比")]
        public float m_scaleValueForBorderLineAlphaMin;
        [Header("描边透明度最大时的缩放比")]
        public float m_scaleValueForBorderLineAlphaMax;
        [Header("描边完全不透明时的透明度，0-1，1是完全不透明")]
        public float m_starfieldBorderLineAlphaMax;
        private static DelegateBridge _c__Hotfix_ctor;
    }
}

