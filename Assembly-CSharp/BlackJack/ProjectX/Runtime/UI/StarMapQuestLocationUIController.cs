﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class StarMapQuestLocationUIController : UIControllerBase
    {
        [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
        private CommonUIStateController m_uiStateController;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        private Text m_questNameText;
        [AutoBind("./NameBGImage", AutoBindAttribute.InitState.NotInit, false)]
        private Image m_questIcon;
        private bool m_currShowState;
        private static DelegateBridge __Hotfix_InitController;
        private static DelegateBridge __Hotfix_ShowQuestDetail;

        [MethodImpl(0x8000)]
        public void InitController(LBProcessingQuestBase questInfo, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowQuestDetail(bool show)
        {
        }
    }
}

