﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ChatLinkItemUIController : ChatItemControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<UIControllerBase, ChatInfo> EventOnLinkButtonClick;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<UIControllerBase, ChatInfo> EventOnPlayerHeadButtonClick;
        [AutoBind("./PlayerName/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PlayerNameText;
        [AutoBind("./PlayerName/CommonCaptainDummy", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject CommanderItemDummy;
        [AutoBind("./ChatDetail/ChatBGImage/LinkText", AutoBindAttribute.InitState.NotInit, false)]
        public Button LinkButton;
        [AutoBind("./ChatDetail/ChatBGImage/LinkText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LinkText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_RegisterButtonClickEvent;
        private static DelegateBridge __Hotfix_UnRegisterButtonClickEvent;
        private static DelegateBridge __Hotfix_UpdateChatItemInfo;
        private static DelegateBridge __Hotfix_StringToColor;
        private static DelegateBridge __Hotfix_OnLinkButtonClick;
        private static DelegateBridge __Hotfix_OnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnLinkButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnLinkButtonClick;
        private static DelegateBridge __Hotfix_add_EventOnPlayerHeadButtonClick;
        private static DelegateBridge __Hotfix_remove_EventOnPlayerHeadButtonClick;

        public event Action<UIControllerBase, ChatInfo> EventOnLinkButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<UIControllerBase, ChatInfo> EventOnPlayerHeadButtonClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected void OnLinkButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnPlayerHeadButtonClick(UIControllerBase uCtrl)
        {
        }

        [MethodImpl(0x8000)]
        public void RegisterButtonClickEvent(Action<UIControllerBase, ChatInfo> linkAction, Action<UIControllerBase, ChatInfo> playerHeadAction)
        {
        }

        [MethodImpl(0x8000)]
        public Color StringToColor(string colorStr)
        {
        }

        [MethodImpl(0x8000)]
        public void UnRegisterButtonClickEvent()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateChatItemInfo(ChatInfo chatInfo, Dictionary<string, UnityEngine.Object> m_dynamicCache, bool isSelf)
        {
        }
    }
}

