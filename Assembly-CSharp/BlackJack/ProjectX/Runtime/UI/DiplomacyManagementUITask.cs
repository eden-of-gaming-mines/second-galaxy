﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Scene;
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine.EventSystems;

    public class DiplomacyManagementUITask : GuildUITaskBase
    {
        private List<DiplomacyItemInfo> m_cachedDiplomacyItemInfoList;
        private List<DiplomacyItemInfo> m_diplomacyItemInfoList;
        private DiplomacyItemUIController m_currSelectedPlayerItem;
        private CharacterSimpleInfoUITask m_characterSimpleInfoUITask;
        private SearchType m_searchType;
        private string m_lastSearchingKey;
        private DiplomacyItemType m_sortFilterType;
        private bool m_isGuildDiplomacy;
        private bool m_isDiplomacyStateChange;
        public const string ParamKeyIsGuildDiplomacy = "IsGuildDiplomacy";
        public const string ParamKeySearchType = "SearchType";
        public const string ParamKeySortFilter = "SortFilter";
        public const string TaskModeFriend = "Friend";
        public const string TaskModeEnemy = "Enemy";
        public const string TaskModeSearch = "Search";
        private bool m_isCloseTips;
        private bool m_isAnyTipShow;
        private DiplomacyManagementUIController m_mainCtrl;
        private UITaskBase.LayerDesc[] m_layerDescArray;
        private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        public const string TaskName = "DiplomacyManagementUITask";
        [CompilerGenerated]
        private static Action<UIProcess, bool> <>f__am$cache0;
        [CompilerGenerated]
        private static Predicate<DiplomacyItemInfo> <>f__am$cache1;
        [CompilerGenerated]
        private static Predicate<DiplomacyItemInfo> <>f__am$cache2;
        [CompilerGenerated]
        private static Predicate<DiplomacyItemInfo> <>f__am$cache3;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartGuildDiplomacyUITaskWithPrepare;
        private static DelegateBridge __Hotfix_StartPersonalDiplomacyUITaskWithPrepare;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_NeedRefreshDiplomacyItemDataList;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_UpdateView_Search;
        private static DelegateBridge __Hotfix_UpdateView_Relation;
        private static DelegateBridge __Hotfix_IsSelfGuildInvalid;
        private static DelegateBridge __Hotfix_OnGuildDiplomacyUpdateNtf;
        private static DelegateBridge __Hotfix_OnFriendButtonClick;
        private static DelegateBridge __Hotfix_OnEnemyButtonClick;
        private static DelegateBridge __Hotfix_OnSearchTabButtonClick;
        private static DelegateBridge __Hotfix_OnBackButtonClick;
        private static DelegateBridge __Hotfix_OnCloseButtonClick;
        private static DelegateBridge __Hotfix_OnExplainButtonClick;
        private static DelegateBridge __Hotfix_OnSortFilterButtonClick;
        private static DelegateBridge __Hotfix_OnSortFilterChange;
        private static DelegateBridge __Hotfix_OnBackGroundClick;
        private static DelegateBridge __Hotfix_OnSearchComfirmButtonClick;
        private static DelegateBridge __Hotfix_OnSearchTypeButtonClick;
        private static DelegateBridge __Hotfix_OnSearchTypeChange;
        private static DelegateBridge __Hotfix_OnDiplomacyTypeChanged;
        private static DelegateBridge __Hotfix_OnDiplomacyRemoveButonClick;
        private static DelegateBridge __Hotfix_OnDiplomacyItemClick;
        private static DelegateBridge __Hotfix_OnDiplomacyTypeChanged_Guild;
        private static DelegateBridge __Hotfix_OnDiplomacyRemoveButonClick_Guild;
        private static DelegateBridge __Hotfix_OnFriendButtonClick_Guild;
        private static DelegateBridge __Hotfix_OnEnemyButtonClick_Guild;
        private static DelegateBridge __Hotfix_OnDiplomacyTypeChanged_Personal;
        private static DelegateBridge __Hotfix_OnDiplomacyRemoveButonClick_Personal;
        private static DelegateBridge __Hotfix_OnFriendButtonClick_Personal;
        private static DelegateBridge __Hotfix_OnEnemyButtonClick_Personal;
        private static DelegateBridge __Hotfix_InitDataFromIntet;
        private static DelegateBridge __Hotfix_UpdateDiplomacyItemInfoList;
        private static DelegateBridge __Hotfix_AddPlayerListToDiplomacyCacheList;
        private static DelegateBridge __Hotfix_AddGuildListToDiplomacyCacheList;
        private static DelegateBridge __Hotfix_AddAllianceListToDiplomacyCacheList;
        private static DelegateBridge __Hotfix_AddOrRemoveItemToDiplomacyList_1;
        private static DelegateBridge __Hotfix_AddOrRemoveItemToDiplomacyList_0;
        private static DelegateBridge __Hotfix_AddOrRemoveGuildItemToDiplomacyList;
        private static DelegateBridge __Hotfix_AddOrRemovePlayerItemToDiplomacyList;
        private static DelegateBridge __Hotfix_AddOrRemoveAllianceItemToDiplomacyList;
        private static DelegateBridge __Hotfix_CalcDiplomacyType;
        private static DelegateBridge __Hotfix_UpdateDiplomacyState;
        private static DelegateBridge __Hotfix_RegistUIEvent;
        private static DelegateBridge __Hotfix_UnRegistUIEvent;
        private static DelegateBridge __Hotfix_RegistNetEvent;
        private static DelegateBridge __Hotfix_UnRegistNetEvent;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_RegisterCharacterSimpleInfoEvent;
        private static DelegateBridge __Hotfix_CharSimpleTask_CharacterDetailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_SendMailButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_TeamInviteButtonClick;
        private static DelegateBridge __Hotfix_CharSimpleTask_ExpelGuildButtonClick;
        private static DelegateBridge __Hotfix_CloseDiplomacyUITask;
        private static DelegateBridge __Hotfix_CharSimpleTask_OnDiplomacyUpdate;
        private static DelegateBridge __Hotfix_SendDiplomacyDetailGetInfoReq;
        private static DelegateBridge __Hotfix_SendGuildDiplomacyDetailInfoReq;
        private static DelegateBridge __Hotfix_SendGuildDiplomacyUpdateReq;
        private static DelegateBridge __Hotfix_CheckCurrGuild;
        private static DelegateBridge __Hotfix_SendPersonalDiplomacyDetailInfoReq;
        private static DelegateBridge __Hotfix_SendPersonalDiplomacyUpdateReq;
        private static DelegateBridge __Hotfix_OnPersonalDiplomacyStateChanged;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public DiplomacyManagementUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AddAllianceListToDiplomacyCacheList(List<AllianceBasicInfo> allianceList, DiplomacyState diplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        private void AddGuildListToDiplomacyCacheList(List<GuildSimplestInfo> guildList, DiplomacyState diplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        private void AddOrRemoveAllianceItemToDiplomacyList(AllianceBasicInfo allianceBasicInfo, DiplomacyState diplomacyState, bool add)
        {
        }

        [MethodImpl(0x8000)]
        private void AddOrRemoveGuildItemToDiplomacyList(GuildSimplestInfo guildSimpleInfo, DiplomacyState diplomacyState, bool add)
        {
        }

        [MethodImpl(0x8000)]
        private void AddOrRemoveItemToDiplomacyList(PlayerSimplestInfo player, GuildSimplestInfo guild, AllianceBasicInfo alliance, DiplomacyState diplomacyState, bool add)
        {
        }

        [MethodImpl(0x8000)]
        private void AddOrRemoveItemToDiplomacyList(ProPlayerSimplestInfo player, ProGuildSimplestInfo guild, ProAllianceBasicInfo alliance, DiplomacyState diplomacyState, bool add)
        {
        }

        [MethodImpl(0x8000)]
        private void AddOrRemovePlayerItemToDiplomacyList(PlayerSimplestInfo playerSimpleInfo, DiplomacyState diplomacyState, bool add)
        {
        }

        [MethodImpl(0x8000)]
        private void AddPlayerListToDiplomacyCacheList(List<PlayerSimplestInfo> playerList, DiplomacyState diplomacyState)
        {
        }

        [MethodImpl(0x8000)]
        private DiplomacyState CalcDiplomacyType(string playerId, uint guildId, uint allianceId)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_CharacterDetailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void CharSimpleTask_ExpelGuildButtonClick(ProPlayerSimpleInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_OnDiplomacyUpdate(string playerUserId, bool isPersonal, DiplomacyState state)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_SendMailButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private void CharSimpleTask_TeamInviteButtonClick(ProPlayerSimplestInfo playerInfo)
        {
        }

        [MethodImpl(0x8000)]
        private bool CheckCurrGuild()
        {
        }

        [MethodImpl(0x8000)]
        private void CloseDiplomacyUITask()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        protected void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntet(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        public override void InitlizeBeforeManagerStartIt()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsSelfGuildInvalid()
        {
        }

        [MethodImpl(0x8000)]
        private bool NeedRefreshDiplomacyItemDataList()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnBackGroundClick(PointerEventData pointerData, Action<PointerEventData> action)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnCloseButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyItemClick(DiplomacyItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyRemoveButonClick(DiplomacyItemUIController ctrl, bool hasPermission)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyRemoveButonClick_Guild(DiplomacyItemUIController ctrl, bool hasPermission, DiplomacyItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyRemoveButonClick_Personal(DiplomacyItemUIController ctrl, DiplomacyItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyTypeChanged(DiplomacyState type, DiplomacyItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyTypeChanged_Guild(DiplomacyState type, DiplomacyItemUIController ctrl, DiplomacyItemInfo item, DiplomacyState oldDiplomaticType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnDiplomacyTypeChanged_Personal(DiplomacyState type, DiplomacyItemUIController ctrl, DiplomacyItemInfo item, DiplomacyState oldDiplomaticType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyButtonClick_Guild()
        {
        }

        [MethodImpl(0x8000)]
        private void OnEnemyButtonClick_Personal()
        {
        }

        [MethodImpl(0x8000)]
        private void OnExplainButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFriendButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnFriendButtonClick_Guild()
        {
        }

        [MethodImpl(0x8000)]
        private void OnFriendButtonClick_Personal()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGuildDiplomacyUpdateNtf(GuildDiplomacyUpdateNtf ntf)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPersonalDiplomacyStateChanged(DiplomacyItemUIController itemCtrl, DiplomacyState state)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchComfirmButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchTabButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchTypeButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSearchTypeChange(SearchType itemType)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortFilterButtonClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void OnSortFilterChange(DiplomacyItemType changeType)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterCharacterSimpleInfoEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegistNetEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RegistUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        private void SendDiplomacyDetailGetInfoReq(bool isFriendList, bool isGuild, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildDiplomacyDetailInfoReq(bool isFriendList, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendGuildDiplomacyUpdateReq(DiplomacyItemInfo item, DiplomacyState wantedDiplomacyState, DeplomacyOptType optType, DiplomacyItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        private void SendPersonalDiplomacyDetailInfoReq(bool isFriendList, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void SendPersonalDiplomacyUpdateReq(DiplomacyItemInfo item, DiplomacyState wantedDiplomacyState, DeplomacyOptType optType, DiplomacyItemUIController ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartGuildDiplomacyUITaskWithPrepare(UIIntent returnIntent, string mode = "Friend", Action<bool> onPrepareEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartPersonalDiplomacyUITaskWithPrepare(UIIntent returnIntent, string mode = "Friend", Action<bool> onPrepareEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegistNetEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void UnRegistUIEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDiplomacyItemInfoList(bool updateCacheList)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateDiplomacyState(DiplomacyItemInfo item)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_Relation(bool isFriend)
        {
        }

        [MethodImpl(0x8000)]
        protected void UpdateView_Search()
        {
        }

        public ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <AddOrRemoveAllianceItemToDiplomacyList>c__AnonStorey3
        {
            internal AllianceBasicInfo allianceBasicInfo;

            internal bool <>m__0(DiplomacyItemInfo item) => 
                ((item.DiplomacyItemType == DiplomacyItemType.Alliance) && (item.AllianceBasicInfo.m_allianceId == this.allianceBasicInfo.m_allianceId));

            internal bool <>m__1(DiplomacyItemInfo item) => 
                ((item.DiplomacyItemType == DiplomacyItemType.Alliance) && (item.AllianceBasicInfo.m_allianceId == this.allianceBasicInfo.m_allianceId));
        }

        [CompilerGenerated]
        private sealed class <AddOrRemoveGuildItemToDiplomacyList>c__AnonStorey1
        {
            internal GuildSimplestInfo guildSimpleInfo;

            internal bool <>m__0(DiplomacyItemInfo item) => 
                ((item.DiplomacyItemType == DiplomacyItemType.Guild) && (item.GuildSimplestInfo.m_guild == this.guildSimpleInfo.m_guild));

            internal bool <>m__1(DiplomacyItemInfo item) => 
                ((item.DiplomacyItemType == DiplomacyItemType.Guild) && (item.GuildSimplestInfo.m_guild == this.guildSimpleInfo.m_guild));
        }

        [CompilerGenerated]
        private sealed class <AddOrRemovePlayerItemToDiplomacyList>c__AnonStorey2
        {
            internal PlayerSimplestInfo playerSimpleInfo;

            internal bool <>m__0(DiplomacyItemInfo item) => 
                ((item.DiplomacyItemType == DiplomacyItemType.Player) && (item.PlayerSimplestInfo.m_gameUserId == this.playerSimpleInfo.m_gameUserId));

            internal bool <>m__1(DiplomacyItemInfo item) => 
                ((item.DiplomacyItemType == DiplomacyItemType.Player) && (item.PlayerSimplestInfo.m_gameUserId == this.playerSimpleInfo.m_gameUserId));
        }

        [CompilerGenerated]
        private sealed class <CharSimpleTask_ExpelGuildButtonClick>c__AnonStorey4
        {
            internal ProPlayerSimpleInfo playerInfo;
            private static Action<Task> <>f__am$cache0;

            internal void <>m__0()
            {
                GuildMemberRemoveReqNetTask task = new GuildMemberRemoveReqNetTask(this.playerInfo.PlayerGameUserId);
                if (<>f__am$cache0 == null)
                {
                    <>f__am$cache0 = delegate (Task task) {
                        GuildMemberRemoveReqNetTask task2 = task as GuildMemberRemoveReqNetTask;
                        if (((task2 != null) && !task2.IsNetworkError) && (task2.Result != 0))
                        {
                            TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        }
                    };
                }
                task.EventOnStop += <>f__am$cache0;
                task.Start(null, null);
            }

            private static void <>m__1(Task task)
            {
                GuildMemberRemoveReqNetTask task2 = task as GuildMemberRemoveReqNetTask;
                if (((task2 != null) && !task2.IsNetworkError) && (task2.Result != 0))
                {
                    TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <OnResume>c__AnonStorey0
        {
            internal UIIntent intent;
            internal Action<bool> onPipelineEnd;
            internal DiplomacyManagementUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    CommonUITaskHelper.ReturnToBasicUITask(null, true);
                }
                else
                {
                    if (this.$this.MainLayer.State != SceneLayerBase.LayerState.InStack)
                    {
                        SceneManager.Instance.PushLayer(this.$this.MainLayer);
                    }
                    this.$this.RegistNetEvent();
                    if (!this.$this.PlayerCtx.GetLBCharacter().IsInSpace())
                    {
                        SceneManager.Instance.Enable3DLayerBlur(true);
                    }
                    this.$this.<OnResume>__BaseCallProxy0(this.intent, this.onPipelineEnd);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildDiplomacyDetailInfoReq>c__AnonStorey5
        {
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (!((GuildDiplomacyDetailInfoReqNetTask) task).IsNetworkError)
                {
                    int result = ((GuildDiplomacyDetailInfoReqNetTask) task).Result;
                    if (result == 0)
                    {
                        this.onEnd(true);
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        this.onEnd(false);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendGuildDiplomacyUpdateReq>c__AnonStorey6
        {
            internal DiplomacyItemUIController ctrl;
            internal DiplomacyItemInfo item;
            internal DiplomacyState currDiplomaticType;
            internal DiplomacyItemType diplomacyItemTyp;
            internal DiplomacyState wantedDiplomacyState;
            internal DiplomacyManagementUITask $this;

            internal void <>m__0(Task task)
            {
                int result = ((GuildDiplomacyUpdateReqNetTask) task).Result;
                if (!((GuildDiplomacyUpdateReqNetTask) task).IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    if (result == 0)
                    {
                        if (this.wantedDiplomacyState == DiplomacyState.Enemy)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetDiplomacyEnemy, new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Friend)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetDiplomacyFriend, new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Neutral)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetDiplomacyNeutral, new object[0]), false);
                        }
                        if (this.ctrl.IsSameDiplomacyItem(this.item))
                        {
                            this.item.DiplomacyState = this.wantedDiplomacyState;
                            this.ctrl.SetRelationshipImageState(this.diplomacyItemTyp, this.wantedDiplomacyState);
                        }
                    }
                    else if (result != -1833)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        if (this.ctrl.IsSameDiplomacyItem(this.item))
                        {
                            this.ctrl.SetDiplomacyType(this.currDiplomaticType);
                            this.ctrl.SetRelationshipImageState(this.diplomacyItemTyp, this.currDiplomaticType);
                        }
                    }
                    else
                    {
                        if (this.wantedDiplomacyState == DiplomacyState.Enemy)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetDiplomacyEnemyRepetitively, new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Friend)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetDiplomacyFriendRepetitively, new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Neutral)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetDiplomacyNeutralRepetitively, new object[0]), false);
                        }
                        if (this.ctrl.IsSameDiplomacyItem(this.item))
                        {
                            this.item.DiplomacyState = this.wantedDiplomacyState;
                            this.ctrl.SetRelationshipImageState(this.diplomacyItemTyp, this.wantedDiplomacyState);
                        }
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendPersonalDiplomacyDetailInfoReq>c__AnonStorey7
        {
            internal bool? getPlayerListOk;
            internal bool? getGuildListOk;
            internal bool? getAllianceListOk;
            internal Action<bool> onEnd;

            internal void <>m__0(Task task)
            {
                if (!((PlayerSimpleInfoGetReqNetTask) task).IsNetworkError)
                {
                    this.getPlayerListOk = new bool?(((PlayerSimpleInfoGetReqNetTask) task).Result == 0);
                    if (((this.getGuildListOk != null) && (this.getAllianceListOk != null)) && (this.onEnd != null))
                    {
                        int num1;
                        if (!this.getPlayerListOk.Value || !this.getGuildListOk.Value)
                        {
                            num1 = 0;
                        }
                        else
                        {
                            num1 = this.getAllianceListOk.Value;
                        }
                        this.onEnd((bool) num1);
                    }
                }
            }

            internal void <>m__1(Task task)
            {
                if (!((GuildSimpleInfoGetReqNetTask) task).IsNetworkError)
                {
                    this.getGuildListOk = new bool?(((GuildSimpleInfoGetReqNetTask) task).Result == 0);
                    if (((this.getPlayerListOk != null) && (this.getAllianceListOk != null)) && (this.onEnd != null))
                    {
                        int num1;
                        if (!this.getPlayerListOk.Value || !this.getGuildListOk.Value)
                        {
                            num1 = 0;
                        }
                        else
                        {
                            num1 = this.getAllianceListOk.Value;
                        }
                        this.onEnd((bool) num1);
                    }
                }
            }

            internal void <>m__2(Task task)
            {
                if (!((AllianceSimpleInfoGetReqNetTask) task).IsNetworkError)
                {
                    this.getAllianceListOk = new bool?(((AllianceSimpleInfoGetReqNetTask) task).Result == 0);
                    if (((this.getPlayerListOk != null) && (this.getGuildListOk != null)) && (this.onEnd != null))
                    {
                        int num1;
                        if (!this.getPlayerListOk.Value || !this.getGuildListOk.Value)
                        {
                            num1 = 0;
                        }
                        else
                        {
                            num1 = this.getAllianceListOk.Value;
                        }
                        this.onEnd((bool) num1);
                    }
                }
            }
        }

        [CompilerGenerated]
        private sealed class <SendPersonalDiplomacyUpdateReq>c__AnonStorey8
        {
            internal DiplomacyItemUIController ctrl;
            internal DiplomacyItemInfo item;
            internal DiplomacyState currDiplomaticType;
            internal DiplomacyItemType diplomacyItemTyp;
            internal DiplomacyState wantedDiplomacyState;
            internal DiplomacyManagementUITask $this;

            internal void <>m__0(Task task)
            {
                int result = ((PersonalDiplomacyUpdateReqNetTask) task).Result;
                if (!((PersonalDiplomacyUpdateReqNetTask) task).IsNetworkError && (this.$this.State == Task.TaskState.Running))
                {
                    if (result != 0)
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(result, true, false);
                        if (this.ctrl.IsSameDiplomacyItem(this.item))
                        {
                            this.ctrl.SetDiplomacyType(this.currDiplomaticType);
                            this.ctrl.SetRelationshipImageState(this.diplomacyItemTyp, this.currDiplomaticType);
                        }
                    }
                    else
                    {
                        if (this.wantedDiplomacyState == DiplomacyState.Enemy)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetPersonalDiplomacyEnemy, new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Friend)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetPersonalDiplomacyFriend, new object[0]), false);
                        }
                        else if (this.wantedDiplomacyState == DiplomacyState.Neutral)
                        {
                            TipWindowUITask.ShowTipWindow(StringFormatUtil.GetStringInStringTableWithId(StringTableId.StringTableId_SetPersonalDiplomacyNeutral, new object[0]), false);
                        }
                        this.$this.OnPersonalDiplomacyStateChanged(this.ctrl, this.wantedDiplomacyState);
                    }
                }
            }
        }

        protected enum PipeLineStateMaskType
        {
            OnSelectedTabChanged,
            OnSortFilterChanged,
            OnSearchTypeChanged,
            OnDiplomacyChanged
        }

        public enum SearchType
        {
            Player,
            Alliance,
            GuildName,
            GuildCode
        }
    }
}

