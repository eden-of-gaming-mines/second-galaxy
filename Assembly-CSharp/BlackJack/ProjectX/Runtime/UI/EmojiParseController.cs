﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using IL;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Text;
    using UnityEngine;
    using UnityEngine.UI;

    public class EmojiParseController : MonoBehaviour
    {
        private TextAsset m_textAsset;
        private RawImage m_rawImageToClone;
        private float m_offsetX;
        private float m_offsetY;
        private float m_fontSize;
        private int m_richTextIndexOffSet;
        private Dictionary<string, Rect> m_emojiRects;
        private string emSpace;
        private static DelegateBridge __Hotfix_InitEmojiParseHelper;
        private static DelegateBridge __Hotfix_SetEmojiAndText4ChatItem;
        private static DelegateBridge __Hotfix_GetConvertedString;
        private static DelegateBridge __Hotfix_ParseEmojiInfo;
        private static DelegateBridge __Hotfix_SetUITextThatHasEmoji;

        [MethodImpl(0x8000)]
        private static string GetConvertedString(string inputString)
        {
        }

        [MethodImpl(0x8000)]
        public void InitEmojiParseHelper(TextAsset textAsset, GameObject rawImage, Vector2 imageSize, float offsetX = 0f, float offsetY = 0f, float fontSize = 25f)
        {
        }

        [MethodImpl(0x8000)]
        private void ParseEmojiInfo(string inputString)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEmojiAndText4ChatItem(Text text, string inputString)
        {
        }

        [MethodImpl(0x8000), DebuggerHidden]
        private IEnumerator SetUITextThatHasEmoji(Text textToEdit, string inputString)
        {
        }

        [CompilerGenerated]
        private sealed class <SetUITextThatHasEmoji>c__Iterator0 : IEnumerator, IDisposable, IEnumerator<object>
        {
            internal List<EmojiParseController.PosStringTuple> <emojiReplacements>__0;
            internal StringBuilder <sb>__0;
            internal int <i>__0;
            internal string inputString;
            internal Text textToEdit;
            internal List<RawImage> <childRawImageList>__0;
            internal IEnumerator $locvar0;
            internal IDisposable $locvar1;
            internal TextGenerator <textGen>__0;
            internal EmojiParseController $this;
            internal object $current;
            internal bool $disposing;
            internal int $PC;

            [DebuggerHidden]
            public void Dispose()
            {
                this.$disposing = true;
                this.$PC = -1;
            }

            public bool MoveNext()
            {
                uint num = (uint) this.$PC;
                this.$PC = -1;
                switch (num)
                {
                    case 0:
                        this.<emojiReplacements>__0 = new List<EmojiParseController.PosStringTuple>();
                        this.<sb>__0 = new StringBuilder();
                        this.<i>__0 = 0;
                        while (this.<i>__0 < this.inputString.Length)
                        {
                            string key = this.inputString.Substring(this.<i>__0, 1);
                            string str2 = string.Empty;
                            string str3 = string.Empty;
                            if (this.<i>__0 < (this.inputString.Length - 1))
                            {
                                str2 = this.inputString.Substring(this.<i>__0, 2);
                            }
                            if (this.<i>__0 < (this.inputString.Length - 3))
                            {
                                str3 = this.inputString.Substring(this.<i>__0, 4);
                            }
                            if (this.$this.m_emojiRects.ContainsKey(str3))
                            {
                                this.<sb>__0.Append(this.$this.emSpace);
                                this.<emojiReplacements>__0.Add(new EmojiParseController.PosStringTuple((this.<sb>__0.Length - 1) - this.$this.m_richTextIndexOffSet, str3));
                                this.<i>__0 += 4;
                            }
                            else if (this.$this.m_emojiRects.ContainsKey(str2))
                            {
                                this.<sb>__0.Append(this.$this.emSpace);
                                this.<emojiReplacements>__0.Add(new EmojiParseController.PosStringTuple((this.<sb>__0.Length - 1) - this.$this.m_richTextIndexOffSet, str2));
                                this.<i>__0 += 2;
                            }
                            else if (!this.$this.m_emojiRects.ContainsKey(key))
                            {
                                this.<sb>__0.Append(this.inputString[this.<i>__0]);
                                this.<i>__0++;
                            }
                            else
                            {
                                this.<sb>__0.Append(this.$this.emSpace);
                                this.<emojiReplacements>__0.Add(new EmojiParseController.PosStringTuple((this.<sb>__0.Length - 1) - this.$this.m_richTextIndexOffSet, key));
                                this.<i>__0++;
                            }
                        }
                        this.textToEdit.text = this.<sb>__0.ToString();
                        this.$current = null;
                        if (!this.$disposing)
                        {
                            this.$PC = 1;
                        }
                        return true;

                    case 1:
                    {
                        this.<childRawImageList>__0 = new List<RawImage>();
                        this.$locvar0 = this.textToEdit.transform.GetEnumerator();
                        try
                        {
                            while (this.$locvar0.MoveNext())
                            {
                                Transform current = (Transform) this.$locvar0.Current;
                                if (current != this.textToEdit.transform)
                                {
                                    current.gameObject.SetActive(false);
                                    this.<childRawImageList>__0.Add(current.GetComponent<RawImage>());
                                }
                            }
                        }
                        finally
                        {
                            this.$locvar1 = this.$locvar0 as IDisposable;
                            if (this.$locvar1 != null)
                            {
                                this.$locvar1.Dispose();
                            }
                        }
                        if (this.textToEdit.transform.childCount < this.<emojiReplacements>__0.Count)
                        {
                            for (int i = this.<emojiReplacements>__0.Count - this.textToEdit.transform.childCount; i > 0; i--)
                            {
                                GameObject obj2 = UnityEngine.Object.Instantiate<GameObject>(this.$this.m_rawImageToClone.gameObject);
                                obj2.SetActive(false);
                                obj2.transform.SetParent(this.textToEdit.transform, false);
                                this.<childRawImageList>__0.Add(obj2.GetComponent<RawImage>());
                            }
                        }
                        this.<textGen>__0 = this.textToEdit.get_cachedTextGeneratorForLayout();
                        int num3 = 0;
                        while (true)
                        {
                            if (num3 >= this.<emojiReplacements>__0.Count)
                            {
                                this.$PC = -1;
                                break;
                            }
                            EmojiParseController.PosStringTuple tuple = this.<emojiReplacements>__0[num3];
                            int pos = tuple.pos;
                            RawImage image = this.<childRawImageList>__0[num3];
                            UIVertex vertex = this.<textGen>__0.verts[pos * 4];
                            UIVertex vertex2 = this.<textGen>__0.verts[pos * 4];
                            Vector3 vector = new Vector3(vertex.position.x + this.$this.m_offsetX, vertex2.position.y + this.$this.m_offsetY, 0f);
                            image.transform.localPosition = vector;
                            EmojiParseController.PosStringTuple tuple2 = this.<emojiReplacements>__0[num3];
                            image.set_uvRect(this.$this.m_emojiRects[tuple2.emoji]);
                            image.gameObject.SetActive(true);
                            num3++;
                        }
                        break;
                    }
                    default:
                        break;
                }
                return false;
            }

            [DebuggerHidden]
            public void Reset()
            {
                throw new NotSupportedException();
            }

            object IEnumerator<object>.Current =>
                this.$current;

            object IEnumerator.Current =>
                this.$current;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct PosStringTuple
        {
            public int pos;
            public string emoji;
            public PosStringTuple(int p, string s)
            {
                this.pos = p;
                this.emoji = s;
            }
        }
    }
}

