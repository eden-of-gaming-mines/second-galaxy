﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class GuildApplyForListReqNetTask : NetWorkTransactionTask
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private GuildJoinApplyListAck <ApplyListAck>k__BackingField;
        private uint m_curVersion;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnGuildMemberStaffingLogInfo;
        private static DelegateBridge __Hotfix_get_ApplyListAck;
        private static DelegateBridge __Hotfix_set_ApplyListAck;

        [MethodImpl(0x8000)]
        public GuildApplyForListReqNetTask(uint version)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnGuildMemberStaffingLogInfo(GuildJoinApplyListAck result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public GuildJoinApplyListAck ApplyListAck
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

