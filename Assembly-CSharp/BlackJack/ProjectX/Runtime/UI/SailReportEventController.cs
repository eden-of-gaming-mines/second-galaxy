﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class SailReportEventController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("./VarietyText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_varietyText;
        [AutoBind("./IncidentItemImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_timeRoundBg;
        [AutoBind("./IncidentBarBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_timeBarBg;
        [AutoBind("./IncidentNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_nameText;
        [AutoBind("./IncidentTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_timeText;
        [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_img;
        [AutoBind("./TextBgImage", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject m_textBgImage;
        [AutoBind("./IncidentBarImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image m_incidentBarImage;
        private static readonly Color m_moreThanHourColor;
        private static readonly Color m_lessThanHourColor;
        private const string StrTimeFormat = "{0}:{1}";
        private const string StrTimeMoreThanHourFormat = "{0}:{1}:{2}";
        private const string Show = "Show";
        private static DelegateBridge __Hotfix_SetEvent;
        private static DelegateBridge __Hotfix_ShowEvent;

        [MethodImpl(0x8000)]
        public void SetEvent(SailReportSolarSystemInfo.SailReportEvent reportEvent, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowEvent()
        {
        }
    }
}

