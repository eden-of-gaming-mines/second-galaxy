﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class CommonItemUseUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<float> EventOnItemCountSliderValueChanged;
        protected CommonItemIconUIController m_commonItemCtrl;
        protected LongPressDesc m_longPressDescList;
        public int m_currentSelectedItemCount = 1;
        [AutoBind("./BlackBGImage", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx BGButton;
        [AutoBind("./DetailPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx ConfirmButton;
        [AutoBind("./DetailPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx CancelButton;
        [AutoBind("./ItemCountOperatePanel/AddButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx AddButton;
        [AutoBind("./ItemCountOperatePanel/RemoveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx RemoveButton;
        [AutoBind("./ItemCountOperatePanel/MaxButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx MaxButton;
        [AutoBind("./ItemCountOperatePanel/ItemCountText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemCountText;
        [AutoBind("./DetailPanel/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text ItemNameText;
        [AutoBind("./CommonItemUIDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform CommonItemDummy;
        [AutoBind("./ItemCountOperatePanel/CountSlider", AutoBindAttribute.InitState.NotInit, false)]
        public Slider CountSlider;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_GetLongPressValueChangSpeed;
        private static DelegateBridge __Hotfix_UpdateItemUsePanelInfo;
        private static DelegateBridge __Hotfix_UpdateItemUseCount;
        private static DelegateBridge __Hotfix_OnItemInfoSliderValueChanged;
        private static DelegateBridge __Hotfix_add_EventOnItemCountSliderValueChanged;
        private static DelegateBridge __Hotfix_remove_EventOnItemCountSliderValueChanged;

        public event Action<float> EventOnItemCountSliderValueChanged
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public int GetLongPressValueChangSpeed(float longPressTime)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemInfoSliderValueChanged(float value)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemUseCount(long itemTotalCount, long itemCurrCount)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateItemUsePanelInfo(ILBStoreItemClient item, long itemTotalCount, long itemCurrCount, Dictionary<string, UnityEngine.Object> resDict)
        {
        }
    }
}

