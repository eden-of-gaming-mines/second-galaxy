﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;

    public class ScreenRecorderUIController : UIControllerBase
    {
        protected bool m_isRecording;
        protected DateTime m_recordingStartTime;
        private const float TimeMulti = 0.01666667f;
        private const float HourValue = 3600f;
        [AutoBind("./VideoGroup/VideoIndependentButton/TimeImage/TimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_timeText;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_screenRecorderStateCtrl;
        [AutoBind("./UIDragButton", AutoBindAttribute.InitState.NotInit, false)]
        public DragableButtonUIController m_dragBtn;
        [AutoBind("./VideoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_btnStat;
        [AutoBind("./VideoGroup/UnfoldImage", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_menuStat;
        [AutoBind("./VideoGroup/StartGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_start;
        [AutoBind("./VideoGroup/VideoIndependentButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_video;
        [AutoBind("./VideoGroup/LiveIndependentButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_live;
        [AutoBind("./VideoGroup/UnfoldImage/RecButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_btnRecord;
        [AutoBind("./VideoGroup/UnfoldImage/LiveButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_btnCast;
        [AutoBind("./VideoGroup/UnfoldImage/SuspendButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_btnStop;
        [AutoBind("./VideoGroup/UnfoldImage/VoiceButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_btnEnableMicrophone;
        [AutoBind("./VideoGroup/UnfoldImage/CameraButton", AutoBindAttribute.InitState.NotInit, false)]
        public ButtonEx m_btnEnableCamera;
        [AutoBind("./VideoGroup/UnfoldImage/VoiceButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_btnEnableMicrophoneStat;
        [AutoBind("./VideoGroup/UnfoldImage/CameraButton", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_btnEnableCameraStat;
        private Vector2 m_size;
        private Vector2 m_containerSize;
        private Vector2 m_dragOffset;
        public RecordingStatus m_recordingStatus;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnStopRecord;
        private const string KeyScreenRecordPosX = "ScreenRecordPosX";
        private const string KeyScreenRecordPosY = "ScreenRecordPosY";
        private const string StatClose = "Close";
        private const string StatLiveShow = "LiveShow";
        private const string StatRecAndLiveShow = "RecAndLiveShow";
        private const string StatStart = "Start";
        private const string StatVideo = "Video";
        private const string StatLive = "Live";
        private const string StatNormal = "Normal";
        private const string StatChoose = "Choose";
        private const string StatShow = "Show";
        private const string StatOn = "On";
        private const string StatOff = "Off";
        private static DelegateBridge __Hotfix_ShowScreenRecorder;
        private static DelegateBridge __Hotfix_SetPosition;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_M_dragBtn_EventClicked;
        private static DelegateBridge __Hotfix_M_dragBtn_EventPressPosition;
        private static DelegateBridge __Hotfix_M_dragBtn_EventReleasePosition;
        private static DelegateBridge __Hotfix_M_dragBtn_EventMoveToNewPosition;
        private static DelegateBridge __Hotfix_SetRecordTime;
        private static DelegateBridge __Hotfix_SetStatus;
        private static DelegateBridge __Hotfix_OnMenuButtonClick;
        private static DelegateBridge __Hotfix_SetEnableMicrophone;
        private static DelegateBridge __Hotfix_SetEnableCamera;
        private static DelegateBridge __Hotfix_add_EventOnStopRecord;
        private static DelegateBridge __Hotfix_remove_EventOnStopRecord;

        public event Action EventOnStopRecord
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private void M_dragBtn_EventClicked()
        {
        }

        [MethodImpl(0x8000)]
        private void M_dragBtn_EventMoveToNewPosition(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void M_dragBtn_EventPressPosition(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        private void M_dragBtn_EventReleasePosition(Vector2 pos)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void OnMenuButtonClick()
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnableCamera(bool isEnabled)
        {
        }

        [MethodImpl(0x8000)]
        public void SetEnableMicrophone(bool isEnabled)
        {
        }

        [MethodImpl(0x8000)]
        private void SetPosition()
        {
        }

        [MethodImpl(0x8000)]
        public void SetRecordTime(double seconds)
        {
        }

        [MethodImpl(0x8000)]
        public void SetStatus(RecordingStatus recordingStatus)
        {
        }

        [MethodImpl(0x8000)]
        public void ShowScreenRecorder(bool isShow)
        {
        }
    }
}

