﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class ItemSlot : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private bool <IsEnough>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private bool <IsActive>k__BackingField;
        private FakeLBStoreItem m_storeItem;
        private CommonItemIconUIController m_iconController;
        private GameObject m_item;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<FakeLBStoreItem, Vector3> OnItemDetailOpen;
        [AutoBind("./ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_name;
        [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemDummy;
        [AutoBind("./PossessText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText m_count;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_SetDataAndActivate;
        private static DelegateBridge __Hotfix_SetDeactive;
        private static DelegateBridge __Hotfix_SetIconClickAction;
        private static DelegateBridge __Hotfix_get_IsEnough;
        private static DelegateBridge __Hotfix_set_IsEnough;
        private static DelegateBridge __Hotfix_get_IsActive;
        private static DelegateBridge __Hotfix_set_IsActive;
        private static DelegateBridge __Hotfix_add_OnItemDetailOpen;
        private static DelegateBridge __Hotfix_remove_OnItemDetailOpen;

        private event Action<FakeLBStoreItem, Vector3> OnItemDetailOpen
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init()
        {
        }

        [MethodImpl(0x8000)]
        public void SetDataAndActivate(ILBItem data, long count, Dictionary<string, UnityEngine.Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        public void SetDeactive()
        {
        }

        [MethodImpl(0x8000)]
        public void SetIconClickAction(Action<FakeLBStoreItem, Vector3> action)
        {
        }

        public bool IsEnough
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        public bool IsActive
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

