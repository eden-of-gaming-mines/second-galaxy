﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class BaseRedeploySpeedUpReqNetTask : NetWorkTransactionTask
    {
        private readonly int m_speedUpItemId;
        private readonly int m_speedUpItemCount;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <RedeploySpeedUpResult>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnBaseRedeploySpeedUpWithItemAck;
        private static DelegateBridge __Hotfix_set_RedeploySpeedUpResult;
        private static DelegateBridge __Hotfix_get_RedeploySpeedUpResult;

        [MethodImpl(0x8000)]
        public BaseRedeploySpeedUpReqNetTask(int itemId, int itemCount)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnBaseRedeploySpeedUpWithItemAck(int result)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int RedeploySpeedUpResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

