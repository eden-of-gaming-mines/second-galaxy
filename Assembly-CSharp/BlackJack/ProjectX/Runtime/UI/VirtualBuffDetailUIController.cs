﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine;
    using UnityEngine.UI;

    public class VirtualBuffDetailUIController : UIControllerBase
    {
        public List<VirtualBuffItemUIController> m_economicVirtualBuffItemUICtrlList;
        public List<VirtualBuffItemUIController> m_militaryVirtualBuffItemUICtrlList;
        public List<VirtualBuffItemUIController> m_personalVirtualBuffItemUICtrlList;
        public List<VirtualBuffItemUIController> m_solarSystemVirtualBuffItemUICtrlList;
        private const string AssetNameBuffItem = "SolarSystemBuffUIItemPrefab";
        private const float MaxContentHighth = 580f;
        private const float DiffrenceBettweenContentAndView = 12f;
        [AutoBind("/BackGroundButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_backGroundButton;
        [AutoBind("/ScrollView/Viewport/Content/MilitaryEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_militaryEffectGroup;
        [AutoBind("/ScrollView/Viewport/Content/EconomicEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_economicEffectGroup;
        [AutoBind("/ScrollView/Viewport/Content/PersonalEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_personalEffectGroup;
        [AutoBind("/ScrollView/Viewport/Content/SolarSystemEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_solarSystemEffectGroup;
        [AutoBind("/ScrollView/Viewport/Content/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
        public Transform m_itemRoot;
        [AutoBind("/", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController m_stateCtrl;
        [AutoBind("/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_detailInfo;
        [AutoBind("/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
        public ScrollRect m_detailInfoScrollRect;
        [AutoBind("/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
        public RectTransform m_content;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_SetWorldPos;
        private static DelegateBridge __Hotfix_SetAnchorPos;
        private static DelegateBridge __Hotfix_ChangePanelSize;
        private static DelegateBridge __Hotfix_CreatWindowUIPrecess;
        private static DelegateBridge __Hotfix_UpdateVirtualBuffDetailForSolarSystem;
        private static DelegateBridge __Hotfix_UpdateVirtualBuffForQuestOrDelegateMission;
        private static DelegateBridge __Hotfix_UpdateVirtualBuffCategory;

        [MethodImpl(0x8000)]
        public void ChangePanelSize()
        {
        }

        [MethodImpl(0x8000)]
        public UIProcess CreatWindowUIPrecess(bool isShow, bool isImmediateComplete, bool allowToRefreshSameState)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void SetAnchorPos(Vector3 pos, Vector2 pivot)
        {
        }

        [MethodImpl(0x8000)]
        public void SetWorldPos(Vector3 pos, Vector2 pivot)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateVirtualBuffCategory(Transform buffRoot, List<VirtualBuffDesc> buffList, List<VirtualBuffItemUIController> buffItemCtrlList, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateVirtualBuffDetailForSolarSystem(List<VirtualBuffDesc> buffList, Dictionary<string, UnityEngine.Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateVirtualBuffForQuestOrDelegateMission(List<VirtualBuffDesc> buffList, Dictionary<string, UnityEngine.Object> resDic)
        {
        }
    }
}

