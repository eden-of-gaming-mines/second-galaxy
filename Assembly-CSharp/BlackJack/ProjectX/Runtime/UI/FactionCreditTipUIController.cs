﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class FactionCreditTipUIController : UIControllerBase
    {
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController PanelUIState;
        [AutoBind("./TitleGroup/TitleImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image FactionImage;
        [AutoBind("./LevelGroup_1/PreGroup/PreLevelTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelChangeText1;
        [AutoBind("./LevelGroup_1/AftGroup/PreLevelTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text LevelChangeText2;
        [AutoBind("./LevelGroup_2/AftGroup/AgreementTitleText", AutoBindAttribute.InitState.NotInit, false)]
        public Text AgreementTitleText;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateView_LevelChanged;
        private static DelegateBridge __Hotfix_UpdateView_SignedAgreement;
        private static DelegateBridge __Hotfix_SetFactionIcon;
        private static DelegateBridge __Hotfix_GetUIProcess;

        [MethodImpl(0x8000)]
        public UIProcess GetUIProcess(bool IsLevelChanged)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void SetFactionIcon(int factionId, Dictionary<string, Object> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView_LevelChanged(int factionId, FactionCreditLevel oldLevel, FactionCreditLevel newLevel, Dictionary<string, Object> resList)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateView_SignedAgreement(int factionId, string agreementName, Dictionary<string, Object> resList)
        {
        }
    }
}

