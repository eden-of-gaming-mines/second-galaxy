﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public class CelestialPlanetMenuItemUIController : MenuItemUIControllerBase
    {
        [AutoBind("./TargetInfo/Info/NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_planetNameText;
        [AutoBind("./Detail/PlanetTypeItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_planetTypeText;
        [AutoBind("./Detail/OrbitRadiusItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_orbitRadiusText;
        [AutoBind("./Detail/OrbitPeriodItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_orbitPeriodText;
        [AutoBind("./Detail/EccentricityItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_eccentricityText;
        [AutoBind("./Detail/PlanetRadiusItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_planetRadiusText;
        [AutoBind("./Detail/PlanetMassRadius/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_planetMassText;
        [AutoBind("./Detail/PlanetGravityItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_planetGravityText;
        [AutoBind("./Detail/EscapeVelocityItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_escapeVelocityText;
        [AutoBind("./Detail/TemperatureItem/ValueText", AutoBindAttribute.InitState.NotInit, false)]
        public Text m_temperatureText;
        [AutoBind("./Detail/JumpButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button m_jumpButton;
        private GDBPlanetInfo m_gdbPlanetInfo;
        private static DelegateBridge __Hotfix_SetPlanetInfoByGDBData;
        private static DelegateBridge __Hotfix_GetGDBPlanetInfo;
        private static DelegateBridge __Hotfix_GetMenuItemType;
        private static DelegateBridge __Hotfix_GetMenuItemName;

        [MethodImpl(0x8000)]
        public GDBPlanetInfo GetGDBPlanetInfo()
        {
        }

        [MethodImpl(0x8000)]
        public override string GetMenuItemName()
        {
        }

        [MethodImpl(0x8000)]
        public override MenuItemType GetMenuItemType()
        {
        }

        [MethodImpl(0x8000)]
        public void SetPlanetInfoByGDBData(GDBSolarSystemInfo solarSystemInfo, GDBPlanetInfo planetInfo)
        {
        }
    }
}

