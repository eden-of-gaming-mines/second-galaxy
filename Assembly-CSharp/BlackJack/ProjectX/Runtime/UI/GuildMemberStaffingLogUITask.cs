﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common.Client;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class GuildMemberStaffingLogUITask : GuildUITaskBase
    {
        private bool m_pauseTaskOnPostUpdateView;
        private GuildMemberStaffingLogController m_staffingLogCtrl;
        private LogicBlockGuildClient.GuildStaffingLogFullInfo m_logList;
        private UITaskBase.LayerDesc[] layer;
        private UITaskBase.UIControllerDesc[] ctrl;
        public const string TaskName = "GuildMemberStaffingLogUITask";
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTask_1;
        private static DelegateBridge __Hotfix_StartTask_0;
        private static DelegateBridge __Hotfix_PlayShowProcess;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_RequstLogInfo;
        private static DelegateBridge __Hotfix_OnGetMoreLogBtnClick;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        [MethodImpl(0x8000)]
        public GuildMemberStaffingLogUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private void OnGetMoreLogBtnClick()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        public void PlayShowProcess(bool show, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void RequstLogInfo(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTask(Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static GuildMemberStaffingLogUITask StartTask(Action onLoadResComplete)
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <RequstLogInfo>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;

            internal void <>m__0(Task task)
            {
                GuildMemberStaffingLogReqNetTask task2 = task as GuildMemberStaffingLogReqNetTask;
                if ((task2 != null) && !task2.IsNetworkError)
                {
                    if (task2.Result == 0)
                    {
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(true);
                        }
                    }
                    else
                    {
                        TipWindowUITask.ShowTipWindowWithErrorCode(task2.Result, true, false);
                        if (this.onPrepareEnd != null)
                        {
                            this.onPrepareEnd(false);
                        }
                    }
                }
            }
        }

        public enum PipeLineMaskType
        {
            GetMore
        }
    }
}

