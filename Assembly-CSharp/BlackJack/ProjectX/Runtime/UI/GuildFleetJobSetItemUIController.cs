﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ConfigData;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildFleetJobSetItemUIController : UIControllerBase
    {
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<FleetPosition, bool> EventOnFleetJobSetItemClick;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <Index>k__BackingField;
        private FleetPosition m_position;
        [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text NameText;
        [AutoBind("./ButtonGroup/ReplaceButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button ReplaceButton;
        [AutoBind("./ButtonGroup/SettingButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button SettingButton;
        [AutoBind("./ButtonGroup/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
        public Button DeleteButton;
        [AutoBind("./PositionText", AutoBindAttribute.InitState.NotInit, false)]
        public Text PositionText;
        [AutoBind("./CharacterImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image CharacterImage;
        [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController GuildFleetJobSetItemUIPrefabCommonUIStateController;
        private static DelegateBridge __Hotfix_Init;
        private static DelegateBridge __Hotfix_UpdateGuildFleetJobSetItemUI;
        private static DelegateBridge __Hotfix_UpdateSetPositionButtons;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnItemClick;
        private static DelegateBridge __Hotfix_OnDischargePosition;
        private static DelegateBridge __Hotfix_add_EventOnFleetJobSetItemClick;
        private static DelegateBridge __Hotfix_remove_EventOnFleetJobSetItemClick;
        private static DelegateBridge __Hotfix_set_Index;
        private static DelegateBridge __Hotfix_get_Index;

        public event Action<FleetPosition, bool> EventOnFleetJobSetItemClick
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public void Init(FleetPosition position)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnDischargePosition()
        {
        }

        [MethodImpl(0x8000)]
        private void OnItemClick()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateGuildFleetJobSetItemUI(GuildFleetDetailsUITask.GuildFleetMemberDetailInfo info, Dictionary<string, Object> resDic)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateSetPositionButtons(bool canRetire, bool canReplace, bool canSet)
        {
        }

        public int Index
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }
    }
}

