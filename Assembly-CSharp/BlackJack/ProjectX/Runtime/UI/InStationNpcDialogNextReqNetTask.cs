﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.TaskNs;
    using BlackJack.ProjectX.Common;
    using IL;
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;

    public class InStationNpcDialogNextReqNetTask : NetWorkTransactionTask
    {
        private int m_dialogId;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <NextNpcDialogReqResult>k__BackingField;
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private int <NpcId>k__BackingField;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private NpcDialogInfo <DialogInfo>k__BackingField;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_RegisterNetworkEvent;
        private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
        private static DelegateBridge __Hotfix_StartNetWorking;
        private static DelegateBridge __Hotfix_OnNextNpcDialog;
        private static DelegateBridge __Hotfix_set_NextNpcDialogReqResult;
        private static DelegateBridge __Hotfix_get_NextNpcDialogReqResult;
        private static DelegateBridge __Hotfix_set_NpcId;
        private static DelegateBridge __Hotfix_get_NpcId;
        private static DelegateBridge __Hotfix_set_DialogInfo;
        private static DelegateBridge __Hotfix_get_DialogInfo;

        [MethodImpl(0x8000)]
        public InStationNpcDialogNextReqNetTask(int dialogId)
        {
        }

        [MethodImpl(0x8000)]
        protected void OnNextNpcDialog(int result, int npcId, NpcDialogInfo dlgInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void RegisterNetworkEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool StartNetWorking()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UnregisterNetworkEvent()
        {
        }

        public int NextNpcDialogReqResult
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public int NpcId
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }

        public NpcDialogInfo DialogInfo
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            private set
            {
            }
        }
    }
}

