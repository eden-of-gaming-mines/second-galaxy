﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using IL;
    using System;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using UnityEngine;
    using UnityEngine.UI;

    internal class CommonItemDetailOnePropertyUICtroller : UIControllerBase
    {
        [AutoBind("./PropertyName", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyName;
        [AutoBind("./PropertyValue", AutoBindAttribute.InitState.NotInit, false)]
        public Text PropertyValue;
        public Text ComparePropertyValue;
        [AutoBind("./Arrow", AutoBindAttribute.InitState.NotInit, false)]
        public GameObject Arrow;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_UpdateProperty;
        private static DelegateBridge __Hotfix_UpdateComparePropertyInfo_1;
        private static DelegateBridge __Hotfix_UpdateComparePropertyInfo_0;

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateComparePropertyInfo(string propertyName, string valueStr, string compareValueStr, float changeValue, bool hideNoChange)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateComparePropertyInfo(string propertyName, float value, float compareValue, bool hideNoChange = false, DigitFormatUtil.DigitFormatType formatType = 0, bool isShipVolumn = false)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateProperty(string propertyName, string valueStr, float changeValue = 0f)
        {
        }
    }
}

