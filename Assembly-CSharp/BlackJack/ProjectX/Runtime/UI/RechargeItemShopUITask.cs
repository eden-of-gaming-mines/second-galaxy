﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.PlayerContext;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;

    public class RechargeItemShopUITask : UITaskBase
    {
        private RechargeItemShopUIController m_rechargeItemShopUIController;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action EventOnRequestRefreshMoneyInfo;
        private List<LBRechargeItem> m_rechargeItemList;
        private bool? m_isPrepare4SdkProductListSuccess;
        private bool? m_isPrepare4RechargeItemShopSuccess;
        private int m_currPurchaseItemId;
        private static string ParamKeyIsShow;
        private static string ParamKeyIsHide;
        private static string ParamKeyIsPlayAniImmedite;
        private readonly UITaskBase.LayerDesc[] m_layerDescArray;
        private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
        private static DelegateBridge _c__Hotfix_ctor;
        private static DelegateBridge __Hotfix_StartTaskWithPrepare;
        private static DelegateBridge __Hotfix_PrepareForStartOrResume;
        private static DelegateBridge __Hotfix_OnPrepareDataEnd;
        private static DelegateBridge __Hotfix_OnStart;
        private static DelegateBridge __Hotfix_OnStop;
        private static DelegateBridge __Hotfix_OnPause;
        private static DelegateBridge __Hotfix_OnResume;
        private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
        private static DelegateBridge __Hotfix_UpdateDataCache;
        private static DelegateBridge __Hotfix_FilterForMonthCard;
        private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
        private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
        private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
        private static DelegateBridge __Hotfix_InitAllUIControllers;
        private static DelegateBridge __Hotfix_AutomationInitUIControllers;
        private static DelegateBridge __Hotfix_UpdateView;
        private static DelegateBridge __Hotfix_PostUpdateView;
        private static DelegateBridge __Hotfix_RegisterPlayCtxEvent;
        private static DelegateBridge __Hotfix_UnregisterPlayCtxEvent;
        private static DelegateBridge __Hotfix_OnRechargeItemDeliverNtf;
        private static DelegateBridge __Hotfix_OnRechargetItemBuyButtonClick;
        private static DelegateBridge __Hotfix_ShowRechargeItemShop;
        private static DelegateBridge __Hotfix_HideRechargeItemShop;
        private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
        private static DelegateBridge __Hotfix_EnablePipelineStateMask;
        private static DelegateBridge __Hotfix_InitDataFromIntent;
        private static DelegateBridge __Hotfix_RequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_add_EventOnRequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_remove_EventOnRequestRefreshMoneyInfo;
        private static DelegateBridge __Hotfix_get_LBSdkInterface;
        private static DelegateBridge __Hotfix_get_LBRechargeItemClient;
        private static DelegateBridge __Hotfix_get_PlayerCtx;
        private static DelegateBridge __Hotfix_get_LayerDescArray;
        private static DelegateBridge __Hotfix_get_UICtrlDescArray;

        public event Action EventOnRequestRefreshMoneyInfo
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        public RechargeItemShopUITask(string name)
        {
        }

        [MethodImpl(0x8000)]
        private void AutomationInitUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        protected override List<string> CollectAllDynamicResForLoad()
        {
        }

        [MethodImpl(0x8000)]
        private void EnablePipelineStateMask(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        private bool FilterForMonthCard(int rechargeItemId)
        {
        }

        [MethodImpl(0x8000)]
        public void HideRechargeItemShop(bool immedite, Action<bool> onEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitAllUIControllers()
        {
        }

        [MethodImpl(0x8000)]
        private void InitDataFromIntent(UIIntentCustom intent)
        {
        }

        [MethodImpl(0x8000)]
        protected override void InitLayerStateOnLoadAllResCompleted()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedLoadDynamicRes()
        {
        }

        [MethodImpl(0x8000)]
        protected override bool IsNeedUpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        private bool IsPipelineStateMaskNeedUpdate(PipeLineStateMaskType state)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnPause()
        {
        }

        [MethodImpl(0x8000)]
        private void OnPrepareDataEnd(Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRechargeItemDeliverNtf(ulong orderInstanceId)
        {
        }

        [MethodImpl(0x8000)]
        private void OnRechargetItemBuyButtonClick(int id)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnResume(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override bool OnStart(UIIntent intent, Action<bool> onPipelineEnd)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnStop()
        {
        }

        [MethodImpl(0x8000)]
        protected override void PostUpdateView()
        {
        }

        [MethodImpl(0x8000)]
        public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
        {
        }

        [MethodImpl(0x8000)]
        private void RegisterPlayCtxEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected void RequestRefreshMoneyInfo()
        {
        }

        [MethodImpl(0x8000)]
        public void ShowRechargeItemShop(bool immedite, Action<bool> onEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        public static void StartTaskWithPrepare(Action<bool> onPrepareEnd, Action onResLoadEnd = null, Action<bool> onPipelineEnd = null)
        {
        }

        [MethodImpl(0x8000)]
        private void UnregisterPlayCtxEvent()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateDataCache()
        {
        }

        [MethodImpl(0x8000)]
        protected override void UpdateView()
        {
        }

        private LogicBlockClientSdkInterface LBSdkInterface
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private LogicBlockRechargeItemClient LBRechargeItemClient
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        private ProjectXPlayerContext PlayerCtx
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.LayerDesc[] LayerDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
        {
            [MethodImpl(0x8000)]
            get
            {
            }
        }

        [CompilerGenerated]
        private sealed class <HideRechargeItemShop>c__AnonStorey1
        {
            internal Action<bool> onEnd;
            internal RechargeItemShopUITask $this;

            internal void <>m__0(UIProcess p, bool result)
            {
                this.$this.Pause();
                if (this.onEnd != null)
                {
                    this.onEnd(result);
                }
            }
        }

        [CompilerGenerated]
        private sealed class <PrepareForStartOrResume>c__AnonStorey0
        {
            internal Action<bool> onPrepareEnd;
            internal RechargeItemShopUITask $this;

            internal void <>m__0(bool res)
            {
                if (!res)
                {
                    Debug.LogError("RechargeItemShopUITask  RequestSdkRechargeGoodsInfoList failed");
                }
                this.$this.m_isPrepare4SdkProductListSuccess = new bool?(res);
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }

            internal void <>m__1(bool res)
            {
                if (!res)
                {
                    Debug.LogError("RechargeItemShopUITask  SendRechargeItemListReq failed");
                }
                this.$this.m_isPrepare4RechargeItemShopSuccess = new bool?(res);
                this.$this.OnPrepareDataEnd(this.onPrepareEnd);
            }
        }

        private enum PipeLineStateMaskType
        {
            IsShow,
            IsHide,
            IsPlayAniImmedite,
            RefreshAllPackage,
            PurchaseItemSuccess
        }
    }
}

