﻿namespace BlackJack.ProjectX.Runtime.UI
{
    using BlackJack.BJFramework.Runtime.Prefab;
    using BlackJack.BJFramework.Runtime.UI;
    using BlackJack.ProjectX.Common;
    using BlackJack.ProjectX.Protocol;
    using IL;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using UnityEngine.UI;

    public sealed class GuildBuildingItemUIController : UIControllerBase
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
        private Action<int> EventOnGuildBuildingItemClicked;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private Action<int> EventOnGuildBuildingItemProcessingEnd;
        [CompilerGenerated, DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private int <ItemIndex>k__BackingField;
        private bool m_isProcessing;
        private GuildBuildingInfo m_guildBuildingInfo;
        private ProGuildBuildingExInfo m_guildBuildingExInfo;
        private ProGuildBattleClientSyncInfo m_battleInfo;
        private GuildAntiTeleportEffectInfo m_antiTeleportInfo;
        private double m_currProcessTimeTotal;
        [AutoBind("./BuildingInfoGroup/BuildingProcessImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BuildingProcessImage;
        [AutoBind("./BuildingInfoGroup/BuildingProcessTimeText", AutoBindAttribute.InitState.NotInit, false)]
        public FrequentChangeText BuildingProcessTimeText;
        [AutoBind("./BuildingInfoGroup/BuildingProcessStateNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingProcessStateNameText;
        [AutoBind("./BuildingInfoGroup/BuildingNormalStateNameText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingNormalStateNameText;
        [AutoBind("./BuildingInfoGroup/BuildingStateText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingStateText;
        [AutoBind("./BuildingInfoGroup/BuildingLevelText", AutoBindAttribute.InitState.NotInit, false)]
        public Text BuildingLevelText;
        [AutoBind("./BuildingInfoGroup/BuildingIconImage", AutoBindAttribute.InitState.NotInit, false)]
        public Image BuildingIconImage;
        [AutoBind("./BuildingInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public CommonUIStateController BuildingItemInfoStateCtrl;
        [AutoBind("./BuildingInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
        public Button BuildingItemButton;
        private AntiTeleportStatus m_antiTeleportStatus;
        public const float ItemHeight = 104f;
        private static DelegateBridge __Hotfix_UpdateBuildingItemInfo;
        private static DelegateBridge __Hotfix_UpdateGuildBuildingItemInfo;
        private static DelegateBridge __Hotfix_UpdateControlHubBuildingItemInfo;
        private static DelegateBridge __Hotfix_SetSelected;
        private static DelegateBridge __Hotfix_Update;
        private static DelegateBridge __Hotfix_GetInfoId;
        private static DelegateBridge __Hotfix_SetAntiTeleportStatus;
        private static DelegateBridge __Hotfix_GetStatus;
        private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
        private static DelegateBridge __Hotfix_OnBuildingItemClick;
        private static DelegateBridge __Hotfix_GetBuildingItemState;
        private static DelegateBridge __Hotfix_UpdateBuildingItemByItemState;
        private static DelegateBridge __Hotfix_GetBuildingStateName_1;
        private static DelegateBridge __Hotfix_GetBuildingStateName_0;
        private static DelegateBridge __Hotfix_add_EventOnGuildBuildingItemClicked;
        private static DelegateBridge __Hotfix_remove_EventOnGuildBuildingItemClicked;
        private static DelegateBridge __Hotfix_add_EventOnGuildBuildingItemProcessingEnd;
        private static DelegateBridge __Hotfix_remove_EventOnGuildBuildingItemProcessingEnd;
        private static DelegateBridge __Hotfix_get_ItemIndex;
        private static DelegateBridge __Hotfix_set_ItemIndex;

        public event Action<int> EventOnGuildBuildingItemClicked
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        public event Action<int> EventOnGuildBuildingItemProcessingEnd
        {
            [MethodImpl(0x8000)] add
            {
            }
            [MethodImpl(0x8000)] remove
            {
            }
        }

        [MethodImpl(0x8000)]
        private BuildingItemState GetBuildingItemState(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetBuildingStateName(GuildBuildingInfo buildingInfo, GuildAntiTeleportEffectInfo antiTeleportInfo)
        {
        }

        [MethodImpl(0x8000)]
        private string GetBuildingStateName(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo)
        {
        }

        [MethodImpl(0x8000)]
        public ulong GetInfoId()
        {
        }

        [MethodImpl(0x8000)]
        private AntiTeleportStatus GetStatus(GuildAntiTeleportEffectInfo antiTeleportInfo)
        {
        }

        [MethodImpl(0x8000)]
        protected override void OnBindFiledsCompleted()
        {
        }

        [MethodImpl(0x8000)]
        private void OnBuildingItemClick(UIControllerBase ctrl)
        {
        }

        [MethodImpl(0x8000)]
        public void SetAntiTeleportStatus(GuildAntiTeleportEffectInfo antiTeleportInfo)
        {
        }

        [MethodImpl(0x8000)]
        public void SetSelected(bool isSelect)
        {
        }

        [MethodImpl(0x8000)]
        private void Update()
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateBuildingItemByItemState(BuildingItemState buildingItemState)
        {
        }

        [MethodImpl(0x8000)]
        public void UpdateBuildingItemInfo(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateControlHubBuildingItemInfo(GuildBuildingInfo buildingInfo, Dictionary<string, Object> resDict)
        {
        }

        [MethodImpl(0x8000)]
        private void UpdateGuildBuildingItemInfo(GuildBuildingInfo buildingInfo, ProGuildBuildingExInfo buildingExInfo, ProGuildBattleClientSyncInfo battleInfo, Dictionary<string, Object> resDict)
        {
        }

        public int ItemIndex
        {
            [MethodImpl(0x8000), CompilerGenerated]
            get
            {
            }
            [MethodImpl(0x8000), CompilerGenerated]
            set
            {
            }
        }

        private enum AntiTeleportStatus
        {
            None,
            Working,
            CoolingDown
        }

        private enum BuildingItemState
        {
            Empty,
            Normal,
            Deploying,
            Processing,
            ProcessingWithOutBar,
            SecretProcessing,
            Loss
        }
    }
}

